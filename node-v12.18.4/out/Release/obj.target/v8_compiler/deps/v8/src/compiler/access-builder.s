	.file	"access-builder.cc"
	.text
	.section	.text._ZN2v88internal8compiler13AccessBuilder22ForExternalTaggedValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder22ForExternalTaggedValueEv
	.type	_ZN2v88internal8compiler13AccessBuilder22ForExternalTaggedValueEv, @function
_ZN2v88internal8compiler13AccessBuilder22ForExternalTaggedValueEv:
.LFB18907:
	.cfi_startproc
	endbr64
	movl	$4294967295, %edx
	pxor	%xmm0, %xmm0
	movb	$0, (%rdi)
	movq	%rdi, %rax
	movq	%rdx, 24(%rdi)
	movl	$1800, %edx
	movl	$0, 4(%rdi)
	movw	%dx, 32(%rdi)
	movb	$0, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18907:
	.size	_ZN2v88internal8compiler13AccessBuilder22ForExternalTaggedValueEv, .-_ZN2v88internal8compiler13AccessBuilder22ForExternalTaggedValueEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder6ForMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder6ForMapEv
	.type	_ZN2v88internal8compiler13AccessBuilder6ForMapEv, @function
_ZN2v88internal8compiler13AccessBuilder6ForMapEv:
.LFB18908:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$0, 4(%rdi)
	movq	$16777217, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$2, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18908:
	.size	_ZN2v88internal8compiler13AccessBuilder6ForMapEv, .-_ZN2v88internal8compiler13AccessBuilder6ForMapEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv
	.type	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv, @function
_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv:
.LFB18909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	176(%rax), %rax
	movb	$0, 34(%r12)
	movl	$8, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$1549, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18909:
	.size	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv, .-_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder17ForBigIntBitfieldEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder17ForBigIntBitfieldEv
	.type	_ZN2v88internal8compiler13AccessBuilder17ForBigIntBitfieldEv, @function
_ZN2v88internal8compiler13AccessBuilder17ForBigIntBitfieldEv:
.LFB18910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	136(%rax), %rax
	movb	$0, 34(%r12)
	movl	$8, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$772, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18910:
	.size	_ZN2v88internal8compiler13AccessBuilder17ForBigIntBitfieldEv, .-_ZN2v88internal8compiler13AccessBuilder17ForBigIntBitfieldEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder24ForBigIntOptionalPaddingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder24ForBigIntOptionalPaddingEv
	.type	_ZN2v88internal8compiler13AccessBuilder24ForBigIntOptionalPaddingEv, @function
_ZN2v88internal8compiler13AccessBuilder24ForBigIntOptionalPaddingEv:
.LFB18911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	136(%rax), %rax
	movb	$0, 34(%r12)
	movl	$12, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$772, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18911:
	.size	_ZN2v88internal8compiler13AccessBuilder24ForBigIntOptionalPaddingEv, .-_ZN2v88internal8compiler13AccessBuilder24ForBigIntOptionalPaddingEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder32ForBigIntLeastSignificantDigit64Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder32ForBigIntLeastSignificantDigit64Ev
	.type	_ZN2v88internal8compiler13AccessBuilder32ForBigIntLeastSignificantDigit64Ev, @function
_ZN2v88internal8compiler13AccessBuilder32ForBigIntLeastSignificantDigit64Ev:
.LFB18912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	192(%rax), %rax
	movb	$0, 34(%r12)
	movl	$16, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$1285, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18912:
	.size	_ZN2v88internal8compiler13AccessBuilder32ForBigIntLeastSignificantDigit64Ev, .-_ZN2v88internal8compiler13AccessBuilder32ForBigIntLeastSignificantDigit64Ev
	.section	.text._ZN2v88internal8compiler13AccessBuilder27ForJSObjectPropertiesOrHashEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder27ForJSObjectPropertiesOrHashEv
	.type	_ZN2v88internal8compiler13AccessBuilder27ForJSObjectPropertiesOrHashEv, @function
_ZN2v88internal8compiler13AccessBuilder27ForJSObjectPropertiesOrHashEv:
.LFB18913:
	.cfi_startproc
	endbr64
	movl	$4294967295, %edx
	pxor	%xmm0, %xmm0
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movq	%rdx, 24(%rdi)
	movl	$1800, %edx
	movl	$8, 4(%rdi)
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$0, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18913:
	.size	_ZN2v88internal8compiler13AccessBuilder27ForJSObjectPropertiesOrHashEv, .-_ZN2v88internal8compiler13AccessBuilder27ForJSObjectPropertiesOrHashEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv
	.type	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv, @function
_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv:
.LFB18914:
	.cfi_startproc
	endbr64
	movl	$4294967295, %edx
	pxor	%xmm0, %xmm0
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movq	%rdx, 24(%rdi)
	movl	$1799, %edx
	movl	$8, 4(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$0, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18914:
	.size	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv, .-_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv
	.type	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv, @function
_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv:
.LFB18915:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$16, 4(%rdi)
	movq	$58720257, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$0, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18915:
	.size	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv, .-_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder27ForJSObjectInObjectPropertyERKNS1_6MapRefEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder27ForJSObjectInObjectPropertyERKNS1_6MapRefEi
	.type	_ZN2v88internal8compiler13AccessBuilder27ForJSObjectInObjectPropertyERKNS1_6MapRefEi, @function
_ZN2v88internal8compiler13AccessBuilder27ForJSObjectInObjectPropertyERKNS1_6MapRefEi:
.LFB18916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movl	%edx, %esi
	subq	$8, %rsp
	call	_ZNK2v88internal8compiler6MapRef25GetInObjectPropertyOffsetEi@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movl	%eax, 4(%r12)
	movl	$1800, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movq	$209682431, 24(%r12)
	movb	$5, 34(%r12)
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18916:
	.size	_ZN2v88internal8compiler13AccessBuilder27ForJSObjectInObjectPropertyERKNS1_6MapRefEi, .-_ZN2v88internal8compiler13AccessBuilder27ForJSObjectInObjectPropertyERKNS1_6MapRefEi
	.section	.text._ZN2v88internal8compiler13AccessBuilder17ForJSObjectOffsetEiNS1_16WriteBarrierKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder17ForJSObjectOffsetEiNS1_16WriteBarrierKindE
	.type	_ZN2v88internal8compiler13AccessBuilder17ForJSObjectOffsetEiNS1_16WriteBarrierKindE, @function
_ZN2v88internal8compiler13AccessBuilder17ForJSObjectOffsetEiNS1_16WriteBarrierKindE:
.LFB18917:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1800, %ecx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	%esi, 4(%rdi)
	movq	$209682431, 24(%rdi)
	movw	%cx, 32(%rdi)
	movb	%dl, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18917:
	.size	_ZN2v88internal8compiler13AccessBuilder17ForJSObjectOffsetEiNS1_16WriteBarrierKindE, .-_ZN2v88internal8compiler13AccessBuilder17ForJSObjectOffsetEiNS1_16WriteBarrierKindE
	.section	.text._ZN2v88internal8compiler13AccessBuilder20ForJSCollectionTableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder20ForJSCollectionTableEv
	.type	_ZN2v88internal8compiler13AccessBuilder20ForJSCollectionTableEv, @function
_ZN2v88internal8compiler13AccessBuilder20ForJSCollectionTableEv:
.LFB18918:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$24, 4(%rdi)
	movq	$16777217, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18918:
	.size	_ZN2v88internal8compiler13AccessBuilder20ForJSCollectionTableEv, .-_ZN2v88internal8compiler13AccessBuilder20ForJSCollectionTableEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder28ForJSCollectionIteratorTableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder28ForJSCollectionIteratorTableEv
	.type	_ZN2v88internal8compiler13AccessBuilder28ForJSCollectionIteratorTableEv, @function
_ZN2v88internal8compiler13AccessBuilder28ForJSCollectionIteratorTableEv:
.LFB22838:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$24, 4(%rdi)
	movq	$16777217, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE22838:
	.size	_ZN2v88internal8compiler13AccessBuilder28ForJSCollectionIteratorTableEv, .-_ZN2v88internal8compiler13AccessBuilder28ForJSCollectionIteratorTableEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder28ForJSCollectionIteratorIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder28ForJSCollectionIteratorIndexEv
	.type	_ZN2v88internal8compiler13AccessBuilder28ForJSCollectionIteratorIndexEv, @function
_ZN2v88internal8compiler13AccessBuilder28ForJSCollectionIteratorIndexEv:
.LFB18920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	416(%rax), %rax
	movb	$0, 34(%r12)
	movl	$32, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18920:
	.size	_ZN2v88internal8compiler13AccessBuilder28ForJSCollectionIteratorIndexEv, .-_ZN2v88internal8compiler13AccessBuilder28ForJSCollectionIteratorIndexEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder34ForJSFunctionPrototypeOrInitialMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder34ForJSFunctionPrototypeOrInitialMapEv
	.type	_ZN2v88internal8compiler13AccessBuilder34ForJSFunctionPrototypeOrInitialMapEv, @function
_ZN2v88internal8compiler13AccessBuilder34ForJSFunctionPrototypeOrInitialMapEv:
.LFB18921:
	.cfi_startproc
	endbr64
	movl	$4294967295, %edx
	pxor	%xmm0, %xmm0
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movq	%rdx, 24(%rdi)
	movl	$1799, %edx
	movl	$56, 4(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18921:
	.size	_ZN2v88internal8compiler13AccessBuilder34ForJSFunctionPrototypeOrInitialMapEv, .-_ZN2v88internal8compiler13AccessBuilder34ForJSFunctionPrototypeOrInitialMapEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder20ForJSFunctionContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder20ForJSFunctionContextEv
	.type	_ZN2v88internal8compiler13AccessBuilder20ForJSFunctionContextEv, @function
_ZN2v88internal8compiler13AccessBuilder20ForJSFunctionContextEv:
.LFB18922:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$32, 4(%rdi)
	movq	$58720257, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18922:
	.size	_ZN2v88internal8compiler13AccessBuilder20ForJSFunctionContextEv, .-_ZN2v88internal8compiler13AccessBuilder20ForJSFunctionContextEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder31ForJSFunctionSharedFunctionInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder31ForJSFunctionSharedFunctionInfoEv
	.type	_ZN2v88internal8compiler13AccessBuilder31ForJSFunctionSharedFunctionInfoEv, @function
_ZN2v88internal8compiler13AccessBuilder31ForJSFunctionSharedFunctionInfoEv:
.LFB18923:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$24, 4(%rdi)
	movq	$16777217, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18923:
	.size	_ZN2v88internal8compiler13AccessBuilder31ForJSFunctionSharedFunctionInfoEv, .-_ZN2v88internal8compiler13AccessBuilder31ForJSFunctionSharedFunctionInfoEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder25ForJSFunctionFeedbackCellEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder25ForJSFunctionFeedbackCellEv
	.type	_ZN2v88internal8compiler13AccessBuilder25ForJSFunctionFeedbackCellEv, @function
_ZN2v88internal8compiler13AccessBuilder25ForJSFunctionFeedbackCellEv:
.LFB18924:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$40, 4(%rdi)
	movq	$58720257, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18924:
	.size	_ZN2v88internal8compiler13AccessBuilder25ForJSFunctionFeedbackCellEv, .-_ZN2v88internal8compiler13AccessBuilder25ForJSFunctionFeedbackCellEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder17ForJSFunctionCodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder17ForJSFunctionCodeEv
	.type	_ZN2v88internal8compiler13AccessBuilder17ForJSFunctionCodeEv, @function
_ZN2v88internal8compiler13AccessBuilder17ForJSFunctionCodeEv:
.LFB18925:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$48, 4(%rdi)
	movq	$16777217, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18925:
	.size	_ZN2v88internal8compiler13AccessBuilder17ForJSFunctionCodeEv, .-_ZN2v88internal8compiler13AccessBuilder17ForJSFunctionCodeEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder37ForJSBoundFunctionBoundTargetFunctionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder37ForJSBoundFunctionBoundTargetFunctionEv
	.type	_ZN2v88internal8compiler13AccessBuilder37ForJSBoundFunctionBoundTargetFunctionEv, @function
_ZN2v88internal8compiler13AccessBuilder37ForJSBoundFunctionBoundTargetFunctionEv:
.LFB18926:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$24, 4(%rdi)
	movq	$7143425, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18926:
	.size	_ZN2v88internal8compiler13AccessBuilder37ForJSBoundFunctionBoundTargetFunctionEv, .-_ZN2v88internal8compiler13AccessBuilder37ForJSBoundFunctionBoundTargetFunctionEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder27ForJSBoundFunctionBoundThisEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder27ForJSBoundFunctionBoundThisEv
	.type	_ZN2v88internal8compiler13AccessBuilder27ForJSBoundFunctionBoundThisEv, @function
_ZN2v88internal8compiler13AccessBuilder27ForJSBoundFunctionBoundThisEv:
.LFB18927:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1800, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$32, 4(%rdi)
	movq	$209682431, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18927:
	.size	_ZN2v88internal8compiler13AccessBuilder27ForJSBoundFunctionBoundThisEv, .-_ZN2v88internal8compiler13AccessBuilder27ForJSBoundFunctionBoundThisEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder32ForJSBoundFunctionBoundArgumentsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder32ForJSBoundFunctionBoundArgumentsEv
	.type	_ZN2v88internal8compiler13AccessBuilder32ForJSBoundFunctionBoundArgumentsEv, @function
_ZN2v88internal8compiler13AccessBuilder32ForJSBoundFunctionBoundArgumentsEv:
.LFB22844:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$40, 4(%rdi)
	movq	$58720257, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE22844:
	.size	_ZN2v88internal8compiler13AccessBuilder32ForJSBoundFunctionBoundArgumentsEv, .-_ZN2v88internal8compiler13AccessBuilder32ForJSBoundFunctionBoundArgumentsEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder27ForJSGeneratorObjectContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder27ForJSGeneratorObjectContextEv
	.type	_ZN2v88internal8compiler13AccessBuilder27ForJSGeneratorObjectContextEv, @function
_ZN2v88internal8compiler13AccessBuilder27ForJSGeneratorObjectContextEv:
.LFB18929:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$32, 4(%rdi)
	movq	$58720257, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18929:
	.size	_ZN2v88internal8compiler13AccessBuilder27ForJSGeneratorObjectContextEv, .-_ZN2v88internal8compiler13AccessBuilder27ForJSGeneratorObjectContextEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder28ForJSGeneratorObjectFunctionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder28ForJSGeneratorObjectFunctionEv
	.type	_ZN2v88internal8compiler13AccessBuilder28ForJSGeneratorObjectFunctionEv, @function
_ZN2v88internal8compiler13AccessBuilder28ForJSGeneratorObjectFunctionEv:
.LFB18930:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$24, 4(%rdi)
	movq	$2097153, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18930:
	.size	_ZN2v88internal8compiler13AccessBuilder28ForJSGeneratorObjectFunctionEv, .-_ZN2v88internal8compiler13AccessBuilder28ForJSGeneratorObjectFunctionEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder28ForJSGeneratorObjectReceiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder28ForJSGeneratorObjectReceiverEv
	.type	_ZN2v88internal8compiler13AccessBuilder28ForJSGeneratorObjectReceiverEv, @function
_ZN2v88internal8compiler13AccessBuilder28ForJSGeneratorObjectReceiverEv:
.LFB22846:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$40, 4(%rdi)
	movq	$58720257, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE22846:
	.size	_ZN2v88internal8compiler13AccessBuilder28ForJSGeneratorObjectReceiverEv, .-_ZN2v88internal8compiler13AccessBuilder28ForJSGeneratorObjectReceiverEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder32ForJSGeneratorObjectContinuationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder32ForJSGeneratorObjectContinuationEv
	.type	_ZN2v88internal8compiler13AccessBuilder32ForJSGeneratorObjectContinuationEv, @function
_ZN2v88internal8compiler13AccessBuilder32ForJSGeneratorObjectContinuationEv:
.LFB18932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	orl	$1, %eax
	movb	$0, 34(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$64, 4(%r12)
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18932:
	.size	_ZN2v88internal8compiler13AccessBuilder32ForJSGeneratorObjectContinuationEv, .-_ZN2v88internal8compiler13AccessBuilder32ForJSGeneratorObjectContinuationEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder35ForJSGeneratorObjectInputOrDebugPosEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder35ForJSGeneratorObjectInputOrDebugPosEv
	.type	_ZN2v88internal8compiler13AccessBuilder35ForJSGeneratorObjectInputOrDebugPosEv, @function
_ZN2v88internal8compiler13AccessBuilder35ForJSGeneratorObjectInputOrDebugPosEv:
.LFB18933:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1800, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$48, 4(%rdi)
	movq	$209682431, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18933:
	.size	_ZN2v88internal8compiler13AccessBuilder35ForJSGeneratorObjectInputOrDebugPosEv, .-_ZN2v88internal8compiler13AccessBuilder35ForJSGeneratorObjectInputOrDebugPosEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder42ForJSGeneratorObjectParametersAndRegistersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder42ForJSGeneratorObjectParametersAndRegistersEv
	.type	_ZN2v88internal8compiler13AccessBuilder42ForJSGeneratorObjectParametersAndRegistersEv, @function
_ZN2v88internal8compiler13AccessBuilder42ForJSGeneratorObjectParametersAndRegistersEv:
.LFB18934:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$72, 4(%rdi)
	movq	$58720257, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18934:
	.size	_ZN2v88internal8compiler13AccessBuilder42ForJSGeneratorObjectParametersAndRegistersEv, .-_ZN2v88internal8compiler13AccessBuilder42ForJSGeneratorObjectParametersAndRegistersEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder30ForJSGeneratorObjectResumeModeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder30ForJSGeneratorObjectResumeModeEv
	.type	_ZN2v88internal8compiler13AccessBuilder30ForJSGeneratorObjectResumeModeEv, @function
_ZN2v88internal8compiler13AccessBuilder30ForJSGeneratorObjectResumeModeEv:
.LFB18935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	orl	$1, %eax
	movb	$0, 34(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$56, 4(%r12)
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18935:
	.size	_ZN2v88internal8compiler13AccessBuilder30ForJSGeneratorObjectResumeModeEv, .-_ZN2v88internal8compiler13AccessBuilder30ForJSGeneratorObjectResumeModeEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder31ForJSAsyncFunctionObjectPromiseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder31ForJSAsyncFunctionObjectPromiseEv
	.type	_ZN2v88internal8compiler13AccessBuilder31ForJSAsyncFunctionObjectPromiseEv, @function
_ZN2v88internal8compiler13AccessBuilder31ForJSAsyncFunctionObjectPromiseEv:
.LFB18936:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$80, 4(%rdi)
	movq	$131073, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18936:
	.size	_ZN2v88internal8compiler13AccessBuilder31ForJSAsyncFunctionObjectPromiseEv, .-_ZN2v88internal8compiler13AccessBuilder31ForJSAsyncFunctionObjectPromiseEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder30ForJSAsyncGeneratorObjectQueueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder30ForJSAsyncGeneratorObjectQueueEv
	.type	_ZN2v88internal8compiler13AccessBuilder30ForJSAsyncGeneratorObjectQueueEv, @function
_ZN2v88internal8compiler13AccessBuilder30ForJSAsyncGeneratorObjectQueueEv:
.LFB18937:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1800, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$80, 4(%rdi)
	movq	$209682431, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18937:
	.size	_ZN2v88internal8compiler13AccessBuilder30ForJSAsyncGeneratorObjectQueueEv, .-_ZN2v88internal8compiler13AccessBuilder30ForJSAsyncGeneratorObjectQueueEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder35ForJSAsyncGeneratorObjectIsAwaitingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder35ForJSAsyncGeneratorObjectIsAwaitingEv
	.type	_ZN2v88internal8compiler13AccessBuilder35ForJSAsyncGeneratorObjectIsAwaitingEv, @function
_ZN2v88internal8compiler13AccessBuilder35ForJSAsyncGeneratorObjectIsAwaitingEv:
.LFB18938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	orl	$1, %eax
	movb	$0, 34(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$88, 4(%r12)
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18938:
	.size	_ZN2v88internal8compiler13AccessBuilder35ForJSAsyncGeneratorObjectIsAwaitingEv, .-_ZN2v88internal8compiler13AccessBuilder35ForJSAsyncGeneratorObjectIsAwaitingEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder16ForJSArrayLengthENS0_12ElementsKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder16ForJSArrayLengthENS0_12ElementsKindE
	.type	_ZN2v88internal8compiler13AccessBuilder16ForJSArrayLengthENS0_12ElementsKindE, @function
_ZN2v88internal8compiler13AccessBuilder16ForJSArrayLengthENS0_12ElementsKindE:
.LFB18939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movl	$1800, %ecx
	movb	$1, (%r12)
	movq	432(%rax), %rdx
	movw	%cx, 32(%r12)
	movl	$24, 4(%r12)
	movq	%rdx, 24(%r12)
	leal	-4(%rbx), %edx
	movb	$5, 34(%r12)
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	cmpb	$1, %dl
	jbe	.L47
	cmpb	$5, %bl
	ja	.L43
	movq	416(%rax), %rax
	movb	$0, 34(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
.L43:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	424(%rax), %rax
	movl	$518, %edx
	popq	%rbx
	movb	$0, 34(%r12)
	movw	%dx, 32(%r12)
	movq	%rax, 24(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18939:
	.size	_ZN2v88internal8compiler13AccessBuilder16ForJSArrayLengthENS0_12ElementsKindE, .-_ZN2v88internal8compiler13AccessBuilder16ForJSArrayLengthENS0_12ElementsKindE
	.section	.text._ZN2v88internal8compiler13AccessBuilder24ForJSArrayBufferBitFieldEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder24ForJSArrayBufferBitFieldEv
	.type	_ZN2v88internal8compiler13AccessBuilder24ForJSArrayBufferBitFieldEv, @function
_ZN2v88internal8compiler13AccessBuilder24ForJSArrayBufferBitFieldEv:
.LFB18940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	96(%rax), %rax
	movb	$0, 34(%r12)
	movl	$40, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$772, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18940:
	.size	_ZN2v88internal8compiler13AccessBuilder24ForJSArrayBufferBitFieldEv, .-_ZN2v88internal8compiler13AccessBuilder24ForJSArrayBufferBitFieldEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder26ForJSArrayBufferViewBufferEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder26ForJSArrayBufferViewBufferEv
	.type	_ZN2v88internal8compiler13AccessBuilder26ForJSArrayBufferViewBufferEv, @function
_ZN2v88internal8compiler13AccessBuilder26ForJSArrayBufferViewBufferEv:
.LFB22840:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$24, 4(%rdi)
	movq	$16777217, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE22840:
	.size	_ZN2v88internal8compiler13AccessBuilder26ForJSArrayBufferViewBufferEv, .-_ZN2v88internal8compiler13AccessBuilder26ForJSArrayBufferViewBufferEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder30ForJSArrayBufferViewByteLengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder30ForJSArrayBufferViewByteLengthEv
	.type	_ZN2v88internal8compiler13AccessBuilder30ForJSArrayBufferViewByteLengthEv, @function
_ZN2v88internal8compiler13AccessBuilder30ForJSArrayBufferViewByteLengthEv:
.LFB18942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	448(%rax), %rax
	movb	$0, 34(%r12)
	movl	$40, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$1285, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18942:
	.size	_ZN2v88internal8compiler13AccessBuilder30ForJSArrayBufferViewByteLengthEv, .-_ZN2v88internal8compiler13AccessBuilder30ForJSArrayBufferViewByteLengthEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder30ForJSArrayBufferViewByteOffsetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder30ForJSArrayBufferViewByteOffsetEv
	.type	_ZN2v88internal8compiler13AccessBuilder30ForJSArrayBufferViewByteOffsetEv, @function
_ZN2v88internal8compiler13AccessBuilder30ForJSArrayBufferViewByteOffsetEv:
.LFB18943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	456(%rax), %rax
	movb	$0, 34(%r12)
	movl	$32, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$1285, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18943:
	.size	_ZN2v88internal8compiler13AccessBuilder30ForJSArrayBufferViewByteOffsetEv, .-_ZN2v88internal8compiler13AccessBuilder30ForJSArrayBufferViewByteOffsetEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder21ForJSTypedArrayLengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder21ForJSTypedArrayLengthEv
	.type	_ZN2v88internal8compiler13AccessBuilder21ForJSTypedArrayLengthEv, @function
_ZN2v88internal8compiler13AccessBuilder21ForJSTypedArrayLengthEv:
.LFB18944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	464(%rax), %rax
	movb	$0, 34(%r12)
	movl	$48, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$1285, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18944:
	.size	_ZN2v88internal8compiler13AccessBuilder21ForJSTypedArrayLengthEv, .-_ZN2v88internal8compiler13AccessBuilder21ForJSTypedArrayLengthEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder26ForJSTypedArrayBasePointerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder26ForJSTypedArrayBasePointerEv
	.type	_ZN2v88internal8compiler13AccessBuilder26ForJSTypedArrayBasePointerEv, @function
_ZN2v88internal8compiler13AccessBuilder26ForJSTypedArrayBasePointerEv:
.LFB18945:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1800, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$64, 4(%rdi)
	movq	$16777217, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$0, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18945:
	.size	_ZN2v88internal8compiler13AccessBuilder26ForJSTypedArrayBasePointerEv, .-_ZN2v88internal8compiler13AccessBuilder26ForJSTypedArrayBasePointerEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder30ForJSTypedArrayExternalPointerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder30ForJSTypedArrayExternalPointerEv
	.type	_ZN2v88internal8compiler13AccessBuilder30ForJSTypedArrayExternalPointerEv, @function
_ZN2v88internal8compiler13AccessBuilder30ForJSTypedArrayExternalPointerEv:
.LFB18946:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$5, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$56, 4(%rdi)
	movq	$33554433, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$0, 34(%rdi)
	movl	$0, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18946:
	.size	_ZN2v88internal8compiler13AccessBuilder30ForJSTypedArrayExternalPointerEv, .-_ZN2v88internal8compiler13AccessBuilder30ForJSTypedArrayExternalPointerEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder24ForJSDataViewDataPointerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder24ForJSDataViewDataPointerEv
	.type	_ZN2v88internal8compiler13AccessBuilder24ForJSDataViewDataPointerEv, @function
_ZN2v88internal8compiler13AccessBuilder24ForJSDataViewDataPointerEv:
.LFB18947:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$5, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$48, 4(%rdi)
	movq	$16777217, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$0, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18947:
	.size	_ZN2v88internal8compiler13AccessBuilder24ForJSDataViewDataPointerEv, .-_ZN2v88internal8compiler13AccessBuilder24ForJSDataViewDataPointerEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder14ForJSDateValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder14ForJSDateValueEv
	.type	_ZN2v88internal8compiler13AccessBuilder14ForJSDateValueEv, @function
_ZN2v88internal8compiler13AccessBuilder14ForJSDateValueEv:
.LFB18948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	528(%rax), %rax
	movb	$5, 34(%r12)
	movl	$24, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$1800, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18948:
	.size	_ZN2v88internal8compiler13AccessBuilder14ForJSDateValueEv, .-_ZN2v88internal8compiler13AccessBuilder14ForJSDateValueEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder14ForJSDateFieldENS0_6JSDate10FieldIndexE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder14ForJSDateFieldENS0_6JSDate10FieldIndexE
	.type	_ZN2v88internal8compiler13AccessBuilder14ForJSDateFieldENS0_6JSDate10FieldIndexE, @function
_ZN2v88internal8compiler13AccessBuilder14ForJSDateFieldENS0_6JSDate10FieldIndexE:
.LFB18949:
	.cfi_startproc
	endbr64
	leal	24(,%rsi,8), %edx
	pxor	%xmm0, %xmm0
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	%edx, 4(%rdi)
	movl	$1800, %edx
	movq	$7263, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18949:
	.size	_ZN2v88internal8compiler13AccessBuilder14ForJSDateFieldENS0_6JSDate10FieldIndexE, .-_ZN2v88internal8compiler13AccessBuilder14ForJSDateFieldENS0_6JSDate10FieldIndexE
	.section	.text._ZN2v88internal8compiler13AccessBuilder23ForJSIteratorResultDoneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder23ForJSIteratorResultDoneEv
	.type	_ZN2v88internal8compiler13AccessBuilder23ForJSIteratorResultDoneEv, @function
_ZN2v88internal8compiler13AccessBuilder23ForJSIteratorResultDoneEv:
.LFB18950:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1800, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$32, 4(%rdi)
	movq	$209682431, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18950:
	.size	_ZN2v88internal8compiler13AccessBuilder23ForJSIteratorResultDoneEv, .-_ZN2v88internal8compiler13AccessBuilder23ForJSIteratorResultDoneEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder24ForJSIteratorResultValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder24ForJSIteratorResultValueEv
	.type	_ZN2v88internal8compiler13AccessBuilder24ForJSIteratorResultValueEv, @function
_ZN2v88internal8compiler13AccessBuilder24ForJSIteratorResultValueEv:
.LFB18951:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1800, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$24, 4(%rdi)
	movq	$209682431, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18951:
	.size	_ZN2v88internal8compiler13AccessBuilder24ForJSIteratorResultValueEv, .-_ZN2v88internal8compiler13AccessBuilder24ForJSIteratorResultValueEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder15ForJSRegExpDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder15ForJSRegExpDataEv
	.type	_ZN2v88internal8compiler13AccessBuilder15ForJSRegExpDataEv, @function
_ZN2v88internal8compiler13AccessBuilder15ForJSRegExpDataEv:
.LFB22852:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1800, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$24, 4(%rdi)
	movq	$209682431, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE22852:
	.size	_ZN2v88internal8compiler13AccessBuilder15ForJSRegExpDataEv, .-_ZN2v88internal8compiler13AccessBuilder15ForJSRegExpDataEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder16ForJSRegExpFlagsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder16ForJSRegExpFlagsEv
	.type	_ZN2v88internal8compiler13AccessBuilder16ForJSRegExpFlagsEv, @function
_ZN2v88internal8compiler13AccessBuilder16ForJSRegExpFlagsEv:
.LFB18953:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1800, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$40, 4(%rdi)
	movq	$209682431, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18953:
	.size	_ZN2v88internal8compiler13AccessBuilder16ForJSRegExpFlagsEv, .-_ZN2v88internal8compiler13AccessBuilder16ForJSRegExpFlagsEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder20ForJSRegExpLastIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder20ForJSRegExpLastIndexEv
	.type	_ZN2v88internal8compiler13AccessBuilder20ForJSRegExpLastIndexEv, @function
_ZN2v88internal8compiler13AccessBuilder20ForJSRegExpLastIndexEv:
.LFB18954:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1800, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$48, 4(%rdi)
	movq	$209682431, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18954:
	.size	_ZN2v88internal8compiler13AccessBuilder20ForJSRegExpLastIndexEv, .-_ZN2v88internal8compiler13AccessBuilder20ForJSRegExpLastIndexEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder17ForJSRegExpSourceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder17ForJSRegExpSourceEv
	.type	_ZN2v88internal8compiler13AccessBuilder17ForJSRegExpSourceEv, @function
_ZN2v88internal8compiler13AccessBuilder17ForJSRegExpSourceEv:
.LFB22850:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1800, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$32, 4(%rdi)
	movq	$209682431, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE22850:
	.size	_ZN2v88internal8compiler13AccessBuilder17ForJSRegExpSourceEv, .-_ZN2v88internal8compiler13AccessBuilder17ForJSRegExpSourceEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv
	.type	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv, @function
_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv:
.LFB18956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	416(%rax), %rax
	movb	$0, 34(%r12)
	movl	$8, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18956:
	.size	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv, .-_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder29ForPropertyArrayLengthAndHashEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder29ForPropertyArrayLengthAndHashEv
	.type	_ZN2v88internal8compiler13AccessBuilder29ForPropertyArrayLengthAndHashEv, @function
_ZN2v88internal8compiler13AccessBuilder29ForPropertyArrayLengthAndHashEv:
.LFB18957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	orl	$1, %eax
	movb	$0, 34(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$8, 4(%r12)
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18957:
	.size	_ZN2v88internal8compiler13AccessBuilder29ForPropertyArrayLengthAndHashEv, .-_ZN2v88internal8compiler13AccessBuilder29ForPropertyArrayLengthAndHashEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder27ForDescriptorArrayEnumCacheEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder27ForDescriptorArrayEnumCacheEv
	.type	_ZN2v88internal8compiler13AccessBuilder27ForDescriptorArrayEnumCacheEv, @function
_ZN2v88internal8compiler13AccessBuilder27ForDescriptorArrayEnumCacheEv:
.LFB18958:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$16, 4(%rdi)
	movq	$16777217, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18958:
	.size	_ZN2v88internal8compiler13AccessBuilder27ForDescriptorArrayEnumCacheEv, .-_ZN2v88internal8compiler13AccessBuilder27ForDescriptorArrayEnumCacheEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder14ForMapBitFieldEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder14ForMapBitFieldEv
	.type	_ZN2v88internal8compiler13AccessBuilder14ForMapBitFieldEv, @function
_ZN2v88internal8compiler13AccessBuilder14ForMapBitFieldEv:
.LFB18959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	96(%rax), %rax
	movb	$0, 34(%r12)
	movl	$14, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$770, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18959:
	.size	_ZN2v88internal8compiler13AccessBuilder14ForMapBitFieldEv, .-_ZN2v88internal8compiler13AccessBuilder14ForMapBitFieldEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder15ForMapBitField2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder15ForMapBitField2Ev
	.type	_ZN2v88internal8compiler13AccessBuilder15ForMapBitField2Ev, @function
_ZN2v88internal8compiler13AccessBuilder15ForMapBitField2Ev:
.LFB18960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	96(%rax), %rax
	movb	$0, 34(%r12)
	movl	$15, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$770, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18960:
	.size	_ZN2v88internal8compiler13AccessBuilder15ForMapBitField2Ev, .-_ZN2v88internal8compiler13AccessBuilder15ForMapBitField2Ev
	.section	.text._ZN2v88internal8compiler13AccessBuilder15ForMapBitField3Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder15ForMapBitField3Ev
	.type	_ZN2v88internal8compiler13AccessBuilder15ForMapBitField3Ev, @function
_ZN2v88internal8compiler13AccessBuilder15ForMapBitField3Ev:
.LFB18961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	136(%rax), %rax
	movb	$0, 34(%r12)
	movl	$16, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$516, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18961:
	.size	_ZN2v88internal8compiler13AccessBuilder15ForMapBitField3Ev, .-_ZN2v88internal8compiler13AccessBuilder15ForMapBitField3Ev
	.section	.text._ZN2v88internal8compiler13AccessBuilder17ForMapDescriptorsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder17ForMapDescriptorsEv
	.type	_ZN2v88internal8compiler13AccessBuilder17ForMapDescriptorsEv, @function
_ZN2v88internal8compiler13AccessBuilder17ForMapDescriptorsEv:
.LFB18962:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$40, 4(%rdi)
	movq	$16777217, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18962:
	.size	_ZN2v88internal8compiler13AccessBuilder17ForMapDescriptorsEv, .-_ZN2v88internal8compiler13AccessBuilder17ForMapDescriptorsEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv
	.type	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv, @function
_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv:
.LFB18963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	128(%rax), %rax
	movb	$0, 34(%r12)
	movl	$12, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$771, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18963:
	.size	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv, .-_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder15ForMapPrototypeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder15ForMapPrototypeEv
	.type	_ZN2v88internal8compiler13AccessBuilder15ForMapPrototypeEv, @function
_ZN2v88internal8compiler13AccessBuilder15ForMapPrototypeEv:
.LFB18964:
	.cfi_startproc
	endbr64
	movl	$4294967295, %edx
	pxor	%xmm0, %xmm0
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movq	%rdx, 24(%rdi)
	movl	$1799, %edx
	movl	$24, 4(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18964:
	.size	_ZN2v88internal8compiler13AccessBuilder15ForMapPrototypeEv, .-_ZN2v88internal8compiler13AccessBuilder15ForMapPrototypeEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder23ForModuleRegularExportsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder23ForModuleRegularExportsEv
	.type	_ZN2v88internal8compiler13AccessBuilder23ForModuleRegularExportsEv, @function
_ZN2v88internal8compiler13AccessBuilder23ForModuleRegularExportsEv:
.LFB18965:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$56, 4(%rdi)
	movq	$16777217, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18965:
	.size	_ZN2v88internal8compiler13AccessBuilder23ForModuleRegularExportsEv, .-_ZN2v88internal8compiler13AccessBuilder23ForModuleRegularExportsEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder23ForModuleRegularImportsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder23ForModuleRegularImportsEv
	.type	_ZN2v88internal8compiler13AccessBuilder23ForModuleRegularImportsEv, @function
_ZN2v88internal8compiler13AccessBuilder23ForModuleRegularImportsEv:
.LFB18966:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$64, 4(%rdi)
	movq	$16777217, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18966:
	.size	_ZN2v88internal8compiler13AccessBuilder23ForModuleRegularImportsEv, .-_ZN2v88internal8compiler13AccessBuilder23ForModuleRegularImportsEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder16ForNameHashFieldEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder16ForNameHashFieldEv
	.type	_ZN2v88internal8compiler13AccessBuilder16ForNameHashFieldEv, @function
_ZN2v88internal8compiler13AccessBuilder16ForNameHashFieldEv:
.LFB18967:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$772, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$8, 4(%rdi)
	movq	$1031, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$0, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18967:
	.size	_ZN2v88internal8compiler13AccessBuilder16ForNameHashFieldEv, .-_ZN2v88internal8compiler13AccessBuilder16ForNameHashFieldEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder15ForStringLengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder15ForStringLengthEv
	.type	_ZN2v88internal8compiler13AccessBuilder15ForStringLengthEv, @function
_ZN2v88internal8compiler13AccessBuilder15ForStringLengthEv:
.LFB18968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	472(%rax), %rax
	movb	$0, 34(%r12)
	movl	$12, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$772, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18968:
	.size	_ZN2v88internal8compiler13AccessBuilder15ForStringLengthEv, .-_ZN2v88internal8compiler13AccessBuilder15ForStringLengthEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder18ForConsStringFirstEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder18ForConsStringFirstEv
	.type	_ZN2v88internal8compiler13AccessBuilder18ForConsStringFirstEv, @function
_ZN2v88internal8compiler13AccessBuilder18ForConsStringFirstEv:
.LFB18969:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$16, 4(%rdi)
	movq	$16417, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18969:
	.size	_ZN2v88internal8compiler13AccessBuilder18ForConsStringFirstEv, .-_ZN2v88internal8compiler13AccessBuilder18ForConsStringFirstEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder19ForConsStringSecondEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder19ForConsStringSecondEv
	.type	_ZN2v88internal8compiler13AccessBuilder19ForConsStringSecondEv, @function
_ZN2v88internal8compiler13AccessBuilder19ForConsStringSecondEv:
.LFB18970:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$24, 4(%rdi)
	movq	$16417, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18970:
	.size	_ZN2v88internal8compiler13AccessBuilder19ForConsStringSecondEv, .-_ZN2v88internal8compiler13AccessBuilder19ForConsStringSecondEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder19ForThinStringActualEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder19ForThinStringActualEv
	.type	_ZN2v88internal8compiler13AccessBuilder19ForThinStringActualEv, @function
_ZN2v88internal8compiler13AccessBuilder19ForThinStringActualEv:
.LFB22854:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$16, 4(%rdi)
	movq	$16417, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE22854:
	.size	_ZN2v88internal8compiler13AccessBuilder19ForThinStringActualEv, .-_ZN2v88internal8compiler13AccessBuilder19ForThinStringActualEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder21ForSlicedStringOffsetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder21ForSlicedStringOffsetEv
	.type	_ZN2v88internal8compiler13AccessBuilder21ForSlicedStringOffsetEv, @function
_ZN2v88internal8compiler13AccessBuilder21ForSlicedStringOffsetEv:
.LFB18972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	orl	$1, %eax
	movb	$0, 34(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$24, 4(%r12)
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18972:
	.size	_ZN2v88internal8compiler13AccessBuilder21ForSlicedStringOffsetEv, .-_ZN2v88internal8compiler13AccessBuilder21ForSlicedStringOffsetEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder21ForSlicedStringParentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder21ForSlicedStringParentEv
	.type	_ZN2v88internal8compiler13AccessBuilder21ForSlicedStringParentEv, @function
_ZN2v88internal8compiler13AccessBuilder21ForSlicedStringParentEv:
.LFB22856:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$16, 4(%rdi)
	movq	$16417, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE22856:
	.size	_ZN2v88internal8compiler13AccessBuilder21ForSlicedStringParentEv, .-_ZN2v88internal8compiler13AccessBuilder21ForSlicedStringParentEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder29ForExternalStringResourceDataEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder29ForExternalStringResourceDataEv
	.type	_ZN2v88internal8compiler13AccessBuilder29ForExternalStringResourceDataEv, @function
_ZN2v88internal8compiler13AccessBuilder29ForExternalStringResourceDataEv:
.LFB18974:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$5, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$24, 4(%rdi)
	movq	$33554433, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$0, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18974:
	.size	_ZN2v88internal8compiler13AccessBuilder29ForExternalStringResourceDataEv, .-_ZN2v88internal8compiler13AccessBuilder29ForExternalStringResourceDataEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder28ForSeqOneByteStringCharacterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder28ForSeqOneByteStringCharacterEv
	.type	_ZN2v88internal8compiler13AccessBuilder28ForSeqOneByteStringCharacterEv, @function
_ZN2v88internal8compiler13AccessBuilder28ForSeqOneByteStringCharacterEv:
.LFB18975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	movb	$1, (%r12)
	movq	96(%rax), %rax
	movb	$0, 18(%r12)
	movl	$16, 4(%r12)
	movq	%rax, 8(%r12)
	movl	$770, %eax
	movw	%ax, 16(%r12)
	movq	%r12, %rax
	movl	$1, 20(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18975:
	.size	_ZN2v88internal8compiler13AccessBuilder28ForSeqOneByteStringCharacterEv, .-_ZN2v88internal8compiler13AccessBuilder28ForSeqOneByteStringCharacterEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder28ForSeqTwoByteStringCharacterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder28ForSeqTwoByteStringCharacterEv
	.type	_ZN2v88internal8compiler13AccessBuilder28ForSeqTwoByteStringCharacterEv, @function
_ZN2v88internal8compiler13AccessBuilder28ForSeqTwoByteStringCharacterEv:
.LFB18976:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	movb	$1, (%r12)
	movq	128(%rax), %rax
	movb	$0, 18(%r12)
	movl	$16, 4(%r12)
	movq	%rax, 8(%r12)
	movl	$771, %eax
	movw	%ax, 16(%r12)
	movq	%r12, %rax
	movl	$1, 20(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18976:
	.size	_ZN2v88internal8compiler13AccessBuilder28ForSeqTwoByteStringCharacterEv, .-_ZN2v88internal8compiler13AccessBuilder28ForSeqTwoByteStringCharacterEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder29ForJSGlobalProxyNativeContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder29ForJSGlobalProxyNativeContextEv
	.type	_ZN2v88internal8compiler13AccessBuilder29ForJSGlobalProxyNativeContextEv, @function
_ZN2v88internal8compiler13AccessBuilder29ForJSGlobalProxyNativeContextEv:
.LFB18977:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$24, 4(%rdi)
	movq	$58720257, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18977:
	.size	_ZN2v88internal8compiler13AccessBuilder29ForJSGlobalProxyNativeContextEv, .-_ZN2v88internal8compiler13AccessBuilder29ForJSGlobalProxyNativeContextEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder32ForJSArrayIteratorIteratedObjectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder32ForJSArrayIteratorIteratedObjectEv
	.type	_ZN2v88internal8compiler13AccessBuilder32ForJSArrayIteratorIteratedObjectEv, @function
_ZN2v88internal8compiler13AccessBuilder32ForJSArrayIteratorIteratedObjectEv:
.LFB18978:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$24, 4(%rdi)
	movq	$75431937, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18978:
	.size	_ZN2v88internal8compiler13AccessBuilder32ForJSArrayIteratorIteratedObjectEv, .-_ZN2v88internal8compiler13AccessBuilder32ForJSArrayIteratorIteratedObjectEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder27ForJSArrayIteratorNextIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder27ForJSArrayIteratorNextIndexEv
	.type	_ZN2v88internal8compiler13AccessBuilder27ForJSArrayIteratorNextIndexEv, @function
_ZN2v88internal8compiler13AccessBuilder27ForJSArrayIteratorNextIndexEv:
.LFB18979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	408(%rax), %rax
	movb	$5, 34(%r12)
	movl	$32, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$1800, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18979:
	.size	_ZN2v88internal8compiler13AccessBuilder27ForJSArrayIteratorNextIndexEv, .-_ZN2v88internal8compiler13AccessBuilder27ForJSArrayIteratorNextIndexEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder22ForJSArrayIteratorKindEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder22ForJSArrayIteratorKindEv
	.type	_ZN2v88internal8compiler13AccessBuilder22ForJSArrayIteratorKindEv, @function
_ZN2v88internal8compiler13AccessBuilder22ForJSArrayIteratorKindEv:
.LFB18980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	560(%rax), %rax
	movb	$0, 34(%r12)
	movl	$40, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18980:
	.size	_ZN2v88internal8compiler13AccessBuilder22ForJSArrayIteratorKindEv, .-_ZN2v88internal8compiler13AccessBuilder22ForJSArrayIteratorKindEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder25ForJSStringIteratorStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder25ForJSStringIteratorStringEv
	.type	_ZN2v88internal8compiler13AccessBuilder25ForJSStringIteratorStringEv, @function
_ZN2v88internal8compiler13AccessBuilder25ForJSStringIteratorStringEv:
.LFB22858:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$24, 4(%rdi)
	movq	$16417, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE22858:
	.size	_ZN2v88internal8compiler13AccessBuilder25ForJSStringIteratorStringEv, .-_ZN2v88internal8compiler13AccessBuilder25ForJSStringIteratorStringEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder24ForJSStringIteratorIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder24ForJSStringIteratorIndexEv
	.type	_ZN2v88internal8compiler13AccessBuilder24ForJSStringIteratorIndexEv, @function
_ZN2v88internal8compiler13AccessBuilder24ForJSStringIteratorIndexEv:
.LFB18982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	472(%rax), %rax
	movb	$0, 34(%r12)
	movl	$32, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18982:
	.size	_ZN2v88internal8compiler13AccessBuilder24ForJSStringIteratorIndexEv, .-_ZN2v88internal8compiler13AccessBuilder24ForJSStringIteratorIndexEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder18ForArgumentsLengthEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder18ForArgumentsLengthEv
	.type	_ZN2v88internal8compiler13AccessBuilder18ForArgumentsLengthEv, @function
_ZN2v88internal8compiler13AccessBuilder18ForArgumentsLengthEv:
.LFB18983:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1800, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$24, 4(%rdi)
	movq	$209682431, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18983:
	.size	_ZN2v88internal8compiler13AccessBuilder18ForArgumentsLengthEv, .-_ZN2v88internal8compiler13AccessBuilder18ForArgumentsLengthEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder18ForArgumentsCalleeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder18ForArgumentsCalleeEv
	.type	_ZN2v88internal8compiler13AccessBuilder18ForArgumentsCalleeEv, @function
_ZN2v88internal8compiler13AccessBuilder18ForArgumentsCalleeEv:
.LFB22848:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1800, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$32, 4(%rdi)
	movq	$209682431, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE22848:
	.size	_ZN2v88internal8compiler13AccessBuilder18ForArgumentsCalleeEv, .-_ZN2v88internal8compiler13AccessBuilder18ForArgumentsCalleeEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder17ForFixedArraySlotEmNS1_16WriteBarrierKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder17ForFixedArraySlotEmNS1_16WriteBarrierKindE
	.type	_ZN2v88internal8compiler13AccessBuilder17ForFixedArraySlotEmNS1_16WriteBarrierKindE, @function
_ZN2v88internal8compiler13AccessBuilder17ForFixedArraySlotEmNS1_16WriteBarrierKindE:
.LFB18985:
	.cfi_startproc
	endbr64
	leal	16(,%rsi,8), %ecx
	pxor	%xmm0, %xmm0
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	%ecx, 4(%rdi)
	movl	$4294967295, %ecx
	movq	%rcx, 24(%rdi)
	movl	$1800, %ecx
	movw	%cx, 32(%rdi)
	movb	%dl, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18985:
	.size	_ZN2v88internal8compiler13AccessBuilder17ForFixedArraySlotEmNS1_16WriteBarrierKindE, .-_ZN2v88internal8compiler13AccessBuilder17ForFixedArraySlotEmNS1_16WriteBarrierKindE
	.section	.text._ZN2v88internal8compiler13AccessBuilder12ForCellValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder12ForCellValueEv
	.type	_ZN2v88internal8compiler13AccessBuilder12ForCellValueEv, @function
_ZN2v88internal8compiler13AccessBuilder12ForCellValueEv:
.LFB18986:
	.cfi_startproc
	endbr64
	movl	$4294967295, %edx
	pxor	%xmm0, %xmm0
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movq	%rdx, 24(%rdi)
	movl	$1800, %edx
	movl	$8, 4(%rdi)
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$0, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18986:
	.size	_ZN2v88internal8compiler13AccessBuilder12ForCellValueEv, .-_ZN2v88internal8compiler13AccessBuilder12ForCellValueEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm
	.type	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm, @function
_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm:
.LFB18987:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$4294967295, %ecx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	leal	16(,%rsi,8), %edx
	movq	%rcx, 24(%rdi)
	movl	%edx, 4(%rdi)
	movl	$1800, %edx
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18987:
	.size	_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm, .-_ZN2v88internal8compiler13AccessBuilder14ForContextSlotEm
	.section	.text._ZN2v88internal8compiler13AccessBuilder26ForContextSlotKnownPointerEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder26ForContextSlotKnownPointerEm
	.type	_ZN2v88internal8compiler13AccessBuilder26ForContextSlotKnownPointerEm, @function
_ZN2v88internal8compiler13AccessBuilder26ForContextSlotKnownPointerEm:
.LFB18988:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$4294967295, %ecx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	leal	16(,%rsi,8), %edx
	movq	%rcx, 24(%rdi)
	movl	%edx, 4(%rdi)
	movl	$1799, %edx
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18988:
	.size	_ZN2v88internal8compiler13AccessBuilder26ForContextSlotKnownPointerEm, .-_ZN2v88internal8compiler13AccessBuilder26ForContextSlotKnownPointerEm
	.section	.text._ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv
	.type	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv, @function
_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv:
.LFB18989:
	.cfi_startproc
	endbr64
	movl	$4294967295, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movq	%rdx, 8(%rdi)
	movl	$1800, %edx
	movl	$16, 4(%rdi)
	movw	%dx, 16(%rdi)
	movb	$5, 18(%rdi)
	movl	$1, 20(%rdi)
	ret
	.cfi_endproc
.LFE18989:
	.size	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv, .-_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv
	.section	.rodata._ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementENS0_12ElementsKindENS0_15LoadSensitivityE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementENS0_12ElementsKindENS0_15LoadSensitivityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementENS0_12ElementsKindENS0_15LoadSensitivityE
	.type	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementENS0_12ElementsKindENS0_15LoadSensitivityE, @function
_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementENS0_12ElementsKindENS0_15LoadSensitivityE:
.LFB18990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4294967295, %ecx
	movl	$1800, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movb	$1, (%rdi)
	movl	$16, 4(%rdi)
	movq	%rcx, 8(%rdi)
	movw	%r8w, 16(%rdi)
	movb	$5, 18(%rdi)
	movl	%edx, 20(%rdi)
	cmpb	$5, %sil
	ja	.L117
	leaq	.L119(%rip), %rdx
	movzbl	%sil, %eax
	movq	%rdi, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementENS0_12ElementsKindENS0_15LoadSensitivityE,"a",@progbits
	.align 4
	.align 4
.L119:
	.long	.L124-.L119
	.long	.L123-.L119
	.long	.L122-.L119
	.long	.L116-.L119
	.long	.L120-.L119
	.long	.L118-.L119
	.section	.text._ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementENS0_12ElementsKindENS0_15LoadSensitivityE
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$1549, %edx
	movq	$7263, 8(%rdi)
	movw	%dx, 16(%rdi)
	movb	$0, 18(%rdi)
.L116:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movl	$1549, %eax
	movq	$8395871, 8(%rdi)
	movw	%ax, 16(%rdi)
	movq	%r12, %rax
	movb	$0, 18(%rdi)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movl	$518, %ecx
	movb	$0, 18(%r12)
	orl	$1, %eax
	movw	%cx, 16(%r12)
	movq	%rax, 8(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	movq	200(%rax), %rax
	movq	%rax, 8(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movq	$209682431, 8(%rdi)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L117:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18990:
	.size	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementENS0_12ElementsKindENS0_15LoadSensitivityE, .-_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementENS0_12ElementsKindENS0_15LoadSensitivityE
	.section	.text._ZN2v88internal8compiler13AccessBuilder16ForStackArgumentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder16ForStackArgumentEv
	.type	_ZN2v88internal8compiler13AccessBuilder16ForStackArgumentEv, @function
_ZN2v88internal8compiler13AccessBuilder16ForStackArgumentEv:
.LFB18991:
	.cfi_startproc
	endbr64
	movl	$1800, %edx
	movb	$0, (%rdi)
	movq	%rdi, %rax
	movl	$8, 4(%rdi)
	movq	$209682431, 8(%rdi)
	movw	%dx, 16(%rdi)
	movb	$0, 18(%rdi)
	movl	$1, 20(%rdi)
	ret
	.cfi_endproc
.LFE18991:
	.size	_ZN2v88internal8compiler13AccessBuilder16ForStackArgumentEv, .-_ZN2v88internal8compiler13AccessBuilder16ForStackArgumentEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder26ForFixedDoubleArrayElementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder26ForFixedDoubleArrayElementEv
	.type	_ZN2v88internal8compiler13AccessBuilder26ForFixedDoubleArrayElementEv, @function
_ZN2v88internal8compiler13AccessBuilder26ForFixedDoubleArrayElementEv:
.LFB18992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	movb	$1, (%r12)
	movq	176(%rax), %rax
	movb	$0, 18(%r12)
	movl	$16, 4(%r12)
	movq	%rax, 8(%r12)
	movl	$1549, %eax
	movw	%ax, 16(%r12)
	movq	%r12, %rax
	movl	$1, 20(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18992:
	.size	_ZN2v88internal8compiler13AccessBuilder26ForFixedDoubleArrayElementEv, .-_ZN2v88internal8compiler13AccessBuilder26ForFixedDoubleArrayElementEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder16ForEnumCacheKeysEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder16ForEnumCacheKeysEv
	.type	_ZN2v88internal8compiler13AccessBuilder16ForEnumCacheKeysEv, @function
_ZN2v88internal8compiler13AccessBuilder16ForEnumCacheKeysEv:
.LFB18993:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$8, 4(%rdi)
	movq	$16777217, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18993:
	.size	_ZN2v88internal8compiler13AccessBuilder16ForEnumCacheKeysEv, .-_ZN2v88internal8compiler13AccessBuilder16ForEnumCacheKeysEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder19ForEnumCacheIndicesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder19ForEnumCacheIndicesEv
	.type	_ZN2v88internal8compiler13AccessBuilder19ForEnumCacheIndicesEv, @function
_ZN2v88internal8compiler13AccessBuilder19ForEnumCacheIndicesEv:
.LFB18994:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movl	$1799, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movl	$16, 4(%rdi)
	movq	$16777217, 24(%rdi)
	movw	%dx, 32(%rdi)
	movb	$3, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18994:
	.size	_ZN2v88internal8compiler13AccessBuilder19ForEnumCacheIndicesEv, .-_ZN2v88internal8compiler13AccessBuilder19ForEnumCacheIndicesEv
	.section	.rodata._ZN2v88internal8compiler13AccessBuilder20ForTypedArrayElementENS0_17ExternalArrayTypeEbNS0_15LoadSensitivityE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unimplemented code"
	.section	.text._ZN2v88internal8compiler13AccessBuilder20ForTypedArrayElementENS0_17ExternalArrayTypeEbNS0_15LoadSensitivityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder20ForTypedArrayElementENS0_17ExternalArrayTypeEbNS0_15LoadSensitivityE
	.type	_ZN2v88internal8compiler13AccessBuilder20ForTypedArrayElementENS0_17ExternalArrayTypeEbNS0_15LoadSensitivityE, @function
_ZN2v88internal8compiler13AccessBuilder20ForTypedArrayElementENS0_17ExternalArrayTypeEbNS0_15LoadSensitivityE:
.LFB18995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$1, %dl
	movq	%rdi, %rax
	setb	%r9b
	sbbl	%edi, %edi
	andl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$11, %esi
	ja	.L133
	leaq	.L135(%rip), %r8
	movl	%esi, %esi
	movslq	(%r8,%rsi,4), %rdx
	addq	%r8, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal8compiler13AccessBuilder20ForTypedArrayElementENS0_17ExternalArrayTypeEbNS0_15LoadSensitivityE,"a",@progbits
	.align 4
	.align 4
.L135:
	.long	.L133-.L135
	.long	.L143-.L135
	.long	.L136-.L135
	.long	.L142-.L135
	.long	.L141-.L135
	.long	.L140-.L135
	.long	.L139-.L135
	.long	.L138-.L135
	.long	.L137-.L135
	.long	.L136-.L135
	.long	.L134-.L135
	.long	.L134-.L135
	.section	.text._ZN2v88internal8compiler13AccessBuilder20ForTypedArrayElementENS0_17ExternalArrayTypeEbNS0_15LoadSensitivityE
	.p2align 4,,10
	.p2align 3
.L136:
	movl	$770, %r11d
	movb	%r9b, (%rax)
	movl	%edi, 4(%rax)
	movq	$1031, 8(%rax)
	movw	%r11w, 16(%rax)
	movb	$0, 18(%rax)
	movl	%ecx, 20(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movl	$514, %edx
	movb	%r9b, (%rax)
	movl	%edi, 4(%rax)
	movq	$1099, 8(%rax)
	movw	%dx, 16(%rax)
	movb	$0, 18(%rax)
	movl	%ecx, 20(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movl	$1548, %esi
	movb	%r9b, (%rax)
	movl	%edi, 4(%rax)
	movq	$7263, 8(%rax)
	movw	%si, 16(%rax)
	movb	$0, 18(%rax)
	movl	%ecx, 20(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movl	$1549, %edx
	movb	%r9b, (%rax)
	movl	%edi, 4(%rax)
	movq	$7263, 8(%rax)
	movw	%dx, 16(%rax)
	movb	$0, 18(%rax)
	movl	%ecx, 20(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movl	$515, %r10d
	movb	%r9b, (%rax)
	movl	%edi, 4(%rax)
	movq	$1099, 8(%rax)
	movw	%r10w, 16(%rax)
	movb	$0, 18(%rax)
	movl	%ecx, 20(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	movb	%r9b, (%rax)
	movl	$771, %r9d
	movl	%edi, 4(%rax)
	movq	$1031, 8(%rax)
	movw	%r9w, 16(%rax)
	movb	$0, 18(%rax)
	movl	%ecx, 20(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movl	$516, %r8d
	movb	%r9b, (%rax)
	movl	%edi, 4(%rax)
	movq	$1099, 8(%rax)
	movw	%r8w, 16(%rax)
	movb	$0, 18(%rax)
	movl	%ecx, 20(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movl	%edi, 4(%rax)
	movl	$772, %edi
	movb	%r9b, (%rax)
	movq	$1031, 8(%rax)
	movw	%di, 16(%rax)
	movb	$0, 18(%rax)
	movl	%ecx, 20(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L134:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L133:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18995:
	.size	_ZN2v88internal8compiler13AccessBuilder20ForTypedArrayElementENS0_17ExternalArrayTypeEbNS0_15LoadSensitivityE, .-_ZN2v88internal8compiler13AccessBuilder20ForTypedArrayElementENS0_17ExternalArrayTypeEbNS0_15LoadSensitivityE
	.section	.text._ZN2v88internal8compiler13AccessBuilder32ForHashTableBaseNumberOfElementsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder32ForHashTableBaseNumberOfElementsEv
	.type	_ZN2v88internal8compiler13AccessBuilder32ForHashTableBaseNumberOfElementsEv, @function
_ZN2v88internal8compiler13AccessBuilder32ForHashTableBaseNumberOfElementsEv:
.LFB18996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	orl	$1, %eax
	movb	$0, 34(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$16, 4(%r12)
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18996:
	.size	_ZN2v88internal8compiler13AccessBuilder32ForHashTableBaseNumberOfElementsEv, .-_ZN2v88internal8compiler13AccessBuilder32ForHashTableBaseNumberOfElementsEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder38ForHashTableBaseNumberOfDeletedElementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder38ForHashTableBaseNumberOfDeletedElementEv
	.type	_ZN2v88internal8compiler13AccessBuilder38ForHashTableBaseNumberOfDeletedElementEv, @function
_ZN2v88internal8compiler13AccessBuilder38ForHashTableBaseNumberOfDeletedElementEv:
.LFB18997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	orl	$1, %eax
	movb	$0, 34(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$24, 4(%r12)
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18997:
	.size	_ZN2v88internal8compiler13AccessBuilder38ForHashTableBaseNumberOfDeletedElementEv, .-_ZN2v88internal8compiler13AccessBuilder38ForHashTableBaseNumberOfDeletedElementEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder24ForHashTableBaseCapacityEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder24ForHashTableBaseCapacityEv
	.type	_ZN2v88internal8compiler13AccessBuilder24ForHashTableBaseCapacityEv, @function
_ZN2v88internal8compiler13AccessBuilder24ForHashTableBaseCapacityEv:
.LFB18998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	orl	$1, %eax
	movb	$0, 34(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$32, 4(%r12)
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18998:
	.size	_ZN2v88internal8compiler13AccessBuilder24ForHashTableBaseCapacityEv, .-_ZN2v88internal8compiler13AccessBuilder24ForHashTableBaseCapacityEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder31ForOrderedHashMapOrSetNextTableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder31ForOrderedHashMapOrSetNextTableEv
	.type	_ZN2v88internal8compiler13AccessBuilder31ForOrderedHashMapOrSetNextTableEv, @function
_ZN2v88internal8compiler13AccessBuilder31ForOrderedHashMapOrSetNextTableEv:
.LFB18999:
	.cfi_startproc
	endbr64
	movl	$4294967295, %edx
	pxor	%xmm0, %xmm0
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movq	%rdx, 24(%rdi)
	movl	$1800, %edx
	movl	$16, 4(%rdi)
	movw	%dx, 32(%rdi)
	movb	$5, 34(%rdi)
	movl	$1, 36(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18999:
	.size	_ZN2v88internal8compiler13AccessBuilder31ForOrderedHashMapOrSetNextTableEv, .-_ZN2v88internal8compiler13AccessBuilder31ForOrderedHashMapOrSetNextTableEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder37ForOrderedHashMapOrSetNumberOfBucketsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder37ForOrderedHashMapOrSetNumberOfBucketsEv
	.type	_ZN2v88internal8compiler13AccessBuilder37ForOrderedHashMapOrSetNumberOfBucketsEv, @function
_ZN2v88internal8compiler13AccessBuilder37ForOrderedHashMapOrSetNumberOfBucketsEv:
.LFB22842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	416(%rax), %rax
	movb	$0, 34(%r12)
	movl	$32, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22842:
	.size	_ZN2v88internal8compiler13AccessBuilder37ForOrderedHashMapOrSetNumberOfBucketsEv, .-_ZN2v88internal8compiler13AccessBuilder37ForOrderedHashMapOrSetNumberOfBucketsEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder45ForOrderedHashMapOrSetNumberOfDeletedElementsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder45ForOrderedHashMapOrSetNumberOfDeletedElementsEv
	.type	_ZN2v88internal8compiler13AccessBuilder45ForOrderedHashMapOrSetNumberOfDeletedElementsEv, @function
_ZN2v88internal8compiler13AccessBuilder45ForOrderedHashMapOrSetNumberOfDeletedElementsEv:
.LFB19013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	416(%rax), %rax
	movb	$0, 34(%r12)
	movl	$24, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19013:
	.size	_ZN2v88internal8compiler13AccessBuilder45ForOrderedHashMapOrSetNumberOfDeletedElementsEv, .-_ZN2v88internal8compiler13AccessBuilder45ForOrderedHashMapOrSetNumberOfDeletedElementsEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder38ForOrderedHashMapOrSetNumberOfElementsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder38ForOrderedHashMapOrSetNumberOfElementsEv
	.type	_ZN2v88internal8compiler13AccessBuilder38ForOrderedHashMapOrSetNumberOfElementsEv, @function
_ZN2v88internal8compiler13AccessBuilder38ForOrderedHashMapOrSetNumberOfElementsEv:
.LFB19016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	movq	416(%rax), %rax
	movb	$0, 34(%r12)
	movl	$16, 4(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19016:
	.size	_ZN2v88internal8compiler13AccessBuilder38ForOrderedHashMapOrSetNumberOfElementsEv, .-_ZN2v88internal8compiler13AccessBuilder38ForOrderedHashMapOrSetNumberOfElementsEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder27ForOrderedHashMapEntryValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder27ForOrderedHashMapEntryValueEv
	.type	_ZN2v88internal8compiler13AccessBuilder27ForOrderedHashMapEntryValueEv, @function
_ZN2v88internal8compiler13AccessBuilder27ForOrderedHashMapEntryValueEv:
.LFB19019:
	.cfi_startproc
	endbr64
	movl	$4294967295, %edx
	movb	$1, (%rdi)
	movq	%rdi, %rax
	movq	%rdx, 8(%rdi)
	movl	$1800, %edx
	movl	$48, 4(%rdi)
	movw	%dx, 16(%rdi)
	movb	$5, 18(%rdi)
	movl	$1, 20(%rdi)
	ret
	.cfi_endproc
.LFE19019:
	.size	_ZN2v88internal8compiler13AccessBuilder27ForOrderedHashMapEntryValueEv, .-_ZN2v88internal8compiler13AccessBuilder27ForOrderedHashMapEntryValueEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder33ForDictionaryNextEnumerationIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder33ForDictionaryNextEnumerationIndexEv
	.type	_ZN2v88internal8compiler13AccessBuilder33ForDictionaryNextEnumerationIndexEv, @function
_ZN2v88internal8compiler13AccessBuilder33ForDictionaryNextEnumerationIndexEv:
.LFB19022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	orl	$1, %eax
	movb	$0, 34(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$40, 4(%r12)
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19022:
	.size	_ZN2v88internal8compiler13AccessBuilder33ForDictionaryNextEnumerationIndexEv, .-_ZN2v88internal8compiler13AccessBuilder33ForDictionaryNextEnumerationIndexEv
	.section	.text._ZN2v88internal8compiler13AccessBuilder28ForDictionaryObjectHashIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13AccessBuilder28ForDictionaryObjectHashIndexEv
	.type	_ZN2v88internal8compiler13AccessBuilder28ForDictionaryObjectHashIndexEv, @function
_ZN2v88internal8compiler13AccessBuilder28ForDictionaryObjectHashIndexEv:
.LFB19023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	pxor	%xmm0, %xmm0
	movb	$1, (%r12)
	orl	$1, %eax
	movb	$0, 34(%r12)
	movq	%rax, 24(%r12)
	movl	$518, %eax
	movw	%ax, 32(%r12)
	movq	%r12, %rax
	movl	$48, 4(%r12)
	movl	$1, 36(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movups	%xmm0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19023:
	.size	_ZN2v88internal8compiler13AccessBuilder28ForDictionaryObjectHashIndexEv, .-_ZN2v88internal8compiler13AccessBuilder28ForDictionaryObjectHashIndexEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler13AccessBuilder22ForExternalTaggedValueEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler13AccessBuilder22ForExternalTaggedValueEv, @function
_GLOBAL__sub_I__ZN2v88internal8compiler13AccessBuilder22ForExternalTaggedValueEv:
.LFB22836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22836:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler13AccessBuilder22ForExternalTaggedValueEv, .-_GLOBAL__sub_I__ZN2v88internal8compiler13AccessBuilder22ForExternalTaggedValueEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler13AccessBuilder22ForExternalTaggedValueEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
