	.file	"loop-variable-optimizer.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB2178:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE2178:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB12810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE12810:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB13036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13036:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB12811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12811:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB13037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE13037:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.rodata._ZN2v88internal8compiler21LoopVariableOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler21LoopVariableOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE, @function
_ZN2v88internal8compiler21LoopVariableOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE:
.LFB10531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	movabsq	$4294967296, %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%rax, (%rdi)
	movl	28(%rsi), %eax
	movq	%rcx, 24(%rdi)
	movups	%xmm0, 8(%rdi)
	cmpq	$268435455, %rax
	ja	.L29
	movq	%rcx, 32(%rdi)
	movq	%rsi, %r13
	movq	%rcx, %r12
	xorl	%r14d, %r14d
	movq	$0, 40(%rdi)
	leaq	0(,%rax,8), %rdx
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	testq	%rax, %rax
	je	.L20
	movq	16(%rcx), %rdi
	movq	24(%rcx), %rax
	movq	%rdx, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L30
	addq	%rdi, %rsi
	movq	%rsi, 16(%rcx)
.L15:
	leaq	(%rdi,%rdx), %r14
	movq	%rdi, 40(%rbx)
	xorl	%esi, %esi
	movq	%r14, 56(%rbx)
	call	memset@PLT
.L20:
	movq	%r14, 48(%rbx)
	movl	28(%r13), %r13d
	movq	%r12, 64(%rbx)
	movq	$0, 72(%rbx)
	movq	%r13, %r14
	movl	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movl	$0, 96(%rbx)
	movq	$0, 104(%rbx)
	testq	%r13, %r13
	jne	.L31
.L19:
	leaq	128(%rbx), %rax
	movq	%r12, 112(%rbx)
	movl	$0, 128(%rbx)
	movq	$0, 136(%rbx)
	movq	%rax, 144(%rbx)
	movq	%rax, 152(%rbx)
	movq	$0, 160(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movq	16(%r12), %rdi
	movq	24(%r12), %rax
	leaq	63(%r13), %rdx
	shrq	$6, %rdx
	salq	$3, %rdx
	subq	%rdi, %rax
	movq	%rdx, %rsi
	cmpq	%rax, %rdx
	ja	.L32
	addq	%rdi, %rsi
	movq	%rsi, 16(%r12)
.L18:
	leaq	(%rdi,%rdx), %rax
	sarq	$6, %r13
	movq	%rdi, 72(%rbx)
	movq	%rax, 104(%rbx)
	leaq	(%rdi,%r13,8), %rax
	movq	%rax, 88(%rbx)
	movl	%r14d, %eax
	andl	$63, %eax
	movl	$0, 80(%rbx)
	movl	%eax, 96(%rbx)
	testq	%rdi, %rdi
	je	.L19
	xorl	%esi, %esi
	call	memset@PLT
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rcx, %rdi
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%r12, %rdi
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L18
.L29:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10531:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE, .-_ZN2v88internal8compiler21LoopVariableOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizerC1EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler21LoopVariableOptimizerC1EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE,_ZN2v88internal8compiler21LoopVariableOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler17InductionVariable13AddLowerBoundEPNS1_4NodeENS2_14ConstraintKindE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"New lower bound for "
.LC2:
	.string	" (loop "
.LC3:
	.string	"): "
.LC4:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal8compiler17InductionVariable13AddLowerBoundEPNS1_4NodeENS2_14ConstraintKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17InductionVariable13AddLowerBoundEPNS1_4NodeENS2_14ConstraintKindE
	.type	_ZN2v88internal8compiler17InductionVariable13AddLowerBoundEPNS1_4NodeENS2_14ConstraintKindE, @function
_ZN2v88internal8compiler17InductionVariable13AddLowerBoundEPNS1_4NodeENS2_14ConstraintKindE:
.LFB10552:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$376, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal21FLAG_trace_turbo_loopE(%rip)
	jne	.L50
	movq	56(%rbx), %r12
	cmpq	64(%rbx), %r12
	je	.L35
.L52:
	movq	%r13, (%r12)
	movl	%r14d, 8(%r12)
	addq	$16, 56(%rbx)
.L33:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	leaq	-320(%rbp), %r15
	leaq	-400(%rbp), %r12
	movq	%r15, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movq	%rcx, -320(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r12, %rdi
	movl	$20, %edx
	movq	%rax, -400(%rbp)
	leaq	.LC1(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	20(%rax), %esi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$7, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movl	20(%rax), %esi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_4NodeE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	movq	%r15, %rdi
	movq	%rax, -400(%rbp)
	movq	%rcx, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	56(%rbx), %r12
	cmpq	64(%rbx), %r12
	jne	.L52
.L35:
	movq	48(%rbx), %r8
	movq	%r12, %r15
	subq	%r8, %r15
	movq	%r15, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L53
	testq	%rax, %rax
	je	.L45
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L54
	movl	$2147483632, %esi
	movl	$2147483632, %r9d
.L38:
	movq	40(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L55
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L41:
	addq	%rax, %r9
	leaq	16(%rax), %rdx
.L39:
	addq	%rax, %r15
	movq	%r13, (%r15)
	movl	%r14d, 8(%r15)
	cmpq	%r8, %r12
	je	.L42
	movq	%r8, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rdx), %rdi
	movl	8(%rdx), %esi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movl	%esi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L43
	subq	%r8, %r12
	leaq	16(%rax,%r12), %rdx
.L42:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%r9, 64(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 48(%rbx)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L54:
	testq	%rdx, %rdx
	jne	.L56
	movl	$16, %edx
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$16, %esi
	movl	$16, %r9d
	jmp	.L38
.L55:
	movq	%r8, -416(%rbp)
	movq	%r9, -408(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-408(%rbp), %r9
	movq	-416(%rbp), %r8
	jmp	.L41
.L51:
	call	__stack_chk_fail@PLT
.L53:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L56:
	cmpq	$134217727, %rdx
	movl	$134217727, %r9d
	cmovbe	%rdx, %r9
	salq	$4, %r9
	movq	%r9, %rsi
	jmp	.L38
	.cfi_endproc
.LFE10552:
	.size	_ZN2v88internal8compiler17InductionVariable13AddLowerBoundEPNS1_4NodeENS2_14ConstraintKindE, .-_ZN2v88internal8compiler17InductionVariable13AddLowerBoundEPNS1_4NodeENS2_14ConstraintKindE
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer14AddCmpToLimitsEPNS1_14FunctionalListINS2_10ConstraintEEEPNS1_4NodeENS1_17InductionVariable14ConstraintKindEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizer14AddCmpToLimitsEPNS1_14FunctionalListINS2_10ConstraintEEEPNS1_4NodeENS1_17InductionVariable14ConstraintKindEb
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer14AddCmpToLimitsEPNS1_14FunctionalListINS2_10ConstraintEEEPNS1_4NodeENS1_17InductionVariable14ConstraintKindEb, @function
_ZN2v88internal8compiler21LoopVariableOptimizer14AddCmpToLimitsEPNS1_14FunctionalListINS2_10ConstraintEEEPNS1_4NodeENS1_17InductionVariable14ConstraintKindEb:
.LFB10558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movzbl	-17(%rdx), %eax
	movq	-8(%rdx), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L59
	leaq	16(%r12), %rdx
	movq	16(%r12), %r12
	addq	$8, %rdx
.L59:
	movq	(%rdx), %r13
	movl	20(%r12), %esi
	leaq	128(%rdi), %r10
	movq	136(%rdi), %rdx
	andl	$16777215, %esi
	testq	%rdx, %rdx
	je	.L57
	movq	%r10, %r9
	movq	%rdx, %rax
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%rax, %r9
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L62
.L61:
	cmpl	32(%rax), %esi
	jle	.L90
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L61
.L62:
	cmpq	%r9, %r10
	je	.L65
	cmpl	32(%r9), %esi
	jl	.L65
	cmpq	$0, 40(%r9)
	je	.L65
.L66:
	movq	24(%rdi), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	testb	%r8b, %r8b
	jne	.L91
	xorl	%r14d, %r14d
	testl	%ecx, %ecx
	sete	%r14b
	cmpq	$39, %rdx
	jbe	.L92
	leaq	40(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L76:
	movq	(%rbx), %rcx
	movl	$1, %edx
	movq	%r13, (%rax)
	movl	%r14d, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rcx, 24(%rax)
	testq	%rcx, %rcx
	je	.L77
	movq	32(%rcx), %rdx
	addq	$1, %rdx
.L77:
	movq	%rdx, 32(%rax)
	movq	%rax, (%rbx)
.L57:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movl	20(%r13), %eax
	movq	%r10, %rsi
	andl	$16777215, %eax
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L94:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L93
.L68:
	cmpl	%eax, 32(%rdx)
	jge	.L94
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L68
.L93:
	cmpq	%rsi, %r10
	je	.L57
	cmpl	%eax, 32(%rsi)
	jg	.L57
	cmpq	$0, 40(%rsi)
	jne	.L66
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L91:
	cmpq	$39, %rdx
	jbe	.L95
	leaq	40(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L73:
	movq	(%rbx), %rsi
	movl	$1, %edx
	movq	%r12, (%rax)
	movl	%ecx, 8(%rax)
	movq	%r13, 16(%rax)
	movq	%rsi, 24(%rax)
	testq	%rsi, %rsi
	je	.L77
	movq	32(%rsi), %rdx
	addq	$1, %rdx
	movq	%rdx, 32(%rax)
	movq	%rax, (%rbx)
	jmp	.L57
.L92:
	movl	$40, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L76
.L95:
	movl	$40, %esi
	movl	%ecx, -36(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-36(%rbp), %ecx
	jmp	.L73
	.cfi_endproc
.LFE10558:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer14AddCmpToLimitsEPNS1_14FunctionalListINS2_10ConstraintEEEPNS1_4NodeENS1_17InductionVariable14ConstraintKindEb, .-_ZN2v88internal8compiler21LoopVariableOptimizer14AddCmpToLimitsEPNS1_14FunctionalListINS2_10ConstraintEEEPNS1_4NodeENS1_17InductionVariable14ConstraintKindEb
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer21FindInductionVariableEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizer21FindInductionVariableEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer21FindInductionVariableEPNS1_4NodeE, @function
_ZN2v88internal8compiler21LoopVariableOptimizer21FindInductionVariableEPNS1_4NodeE:
.LFB10563:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %edx
	movq	136(%rdi), %rax
	leaq	128(%rdi), %rsi
	andl	$16777215, %edx
	testq	%rax, %rax
	je	.L96
	movq	%rsi, %rcx
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L105:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L99
.L98:
	cmpl	32(%rax), %edx
	jle	.L105
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L98
.L99:
	cmpq	%rcx, %rsi
	je	.L96
	cmpl	32(%rcx), %edx
	jl	.L96
	movq	40(%rcx), %rax
.L96:
	ret
	.cfi_endproc
.LFE10563:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer21FindInductionVariableEPNS1_4NodeE, .-_ZN2v88internal8compiler21LoopVariableOptimizer21FindInductionVariableEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer23TryGetInductionVariableEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizer23TryGetInductionVariableEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer23TryGetInductionVariableEPNS1_4NodeE, @function
_ZN2v88internal8compiler21LoopVariableOptimizer23TryGetInductionVariableEPNS1_4NodeE:
.LFB10564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$24, %rsp
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %rsi
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L107
	movq	32(%r13), %r8
	leaq	40(%r13), %rax
.L108:
	movq	(%rax), %r15
	xorl	%edx, %edx
	movq	(%r15), %rax
	movzwl	16(%rax), %edi
	leal	-132(%rdi), %ecx
	movl	%edi, %r9d
	cmpw	$28, %cx
	ja	.L109
	movl	$268566529, %edx
	shrq	%cl, %rdx
	andl	$1, %edx
.L109:
	cmpl	$694, %edi
	sete	%al
	xorl	%r14d, %r14d
	orb	%al, %dl
	je	.L143
.L110:
	movzbl	23(%r15), %r9d
	movq	32(%r15), %rdi
	leaq	32(%r15), %r10
	andl	$15, %r9d
	movq	%rdi, %rdx
	cmpl	$15, %r9d
	jne	.L114
	movq	16(%rdi), %rdx
.L114:
	movq	(%rdx), %rax
	movzwl	16(%rax), %eax
	leal	-705(%rax), %r11d
	cmpw	$1, %r11w
	jbe	.L132
	cmpw	$199, %ax
	jne	.L115
.L132:
	movzbl	23(%rdx), %eax
	movq	32(%rdx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L144
.L115:
	xorl	%eax, %eax
	cmpq	%r13, %rdx
	jne	.L106
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L106
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L120:
	movl	16(%rax), %esi
	movl	%esi, %edx
	shrl	%edx
	andl	$1, %esi
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	movq	(%rdx), %r11
	jne	.L118
	movq	%r11, %rdx
	movq	(%r11), %r11
.L118:
	movq	(%rax), %rax
	cmpw	$36, 16(%r11)
	cmove	%rdx, %r12
	testq	%rax, %rax
	jne	.L120
	testq	%r12, %r12
	je	.L106
	addq	$8, %r10
	addq	$24, %rdi
	cmpl	$15, %r9d
	movq	24(%rbx), %rbx
	cmovne	%r10, %rdi
	movq	24(%rbx), %rdx
	movq	(%rdi), %rax
	movq	%rax, -56(%rbp)
	movq	16(%rbx), %rax
	subq	%rax, %rdx
	cmpq	$111, %rdx
	jbe	.L145
	leaq	112(%rax), %rdx
	movq	%rdx, 16(%rbx)
.L124:
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	movq	%rbx, 72(%rax)
	movq	%rbx, %xmm2
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 64(%rax)
	movups	%xmm0, (%rax)
	movq	%r15, %xmm0
	movhps	-56(%rbp), %xmm0
	movq	$0, 96(%rax)
	movups	%xmm0, 16(%rax)
	movq	%r8, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movl	%r14d, 104(%rax)
	movups	%xmm0, 32(%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 80(%rax)
.L106:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	movq	16(%rdx), %rdx
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L107:
	movq	32(%r13), %rax
	movq	16(%rax), %r8
	addq	$24, %rax
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L143:
	leal	-133(%r9), %ecx
	cmpw	$28, %cx
	ja	.L111
	movl	$268566529, %edx
	shrq	%cl, %rdx
	andl	$1, %edx
.L111:
	cmpl	$695, %edi
	je	.L131
	testb	%dl, %dl
	je	.L127
.L131:
	movl	$1, %r14d
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L145:
	movl	$112, %esi
	movq	%rbx, %rdi
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %r8
	jmp	.L124
.L127:
	xorl	%eax, %eax
	jmp	.L106
	.cfi_endproc
.LFE10564:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer23TryGetInductionVariableEPNS1_4NodeE, .-_ZN2v88internal8compiler21LoopVariableOptimizer23TryGetInductionVariableEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer29ChangeToInductionVariablePhisEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizer29ChangeToInductionVariablePhisEv
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer29ChangeToInductionVariablePhisEv, @function
_ZN2v88internal8compiler21LoopVariableOptimizer29ChangeToInductionVariablePhisEv:
.LFB10566:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	128(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	144(%rdi), %r14
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r14
	je	.L146
	movq	%rdi, %r12
	.p2align 4,,10
	.p2align 3
.L162:
	movq	40(%r14), %rbx
	movq	88(%rbx), %rax
	cmpq	%rax, 80(%rbx)
	jne	.L148
	movq	56(%rbx), %rax
	cmpq	%rax, 48(%rbx)
	je	.L149
.L148:
	movq	(%rbx), %rdi
	movq	24(%rbx), %rcx
	movzbl	23(%rdi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L151
	movq	32(%rdi), %rax
	movl	8(%rax), %edx
.L151:
	movq	8(%r12), %rax
	subl	$1, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	48(%rbx), %rax
	movq	56(%rbx), %r13
	cmpq	%r13, %rax
	je	.L152
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L155:
	movq	(%rbx), %rdi
	movq	(%r15), %rcx
	movzbl	23(%rdi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L165
	movq	32(%rdi), %rdx
	movl	8(%rdx), %edx
.L165:
	movq	8(%r12), %rsi
	subl	$1, %edx
	addq	$16, %r15
	movq	(%rsi), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	cmpq	%r15, %r13
	jne	.L155
.L152:
	movq	80(%rbx), %rax
	movq	88(%rbx), %r13
	cmpq	%r13, %rax
	je	.L156
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L159:
	movq	(%rbx), %rdi
	movq	(%r15), %rcx
	movzbl	23(%rdi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L166
	movq	32(%rdi), %rdx
	movl	8(%rdx), %edx
.L166:
	movq	8(%r12), %rsi
	subl	$1, %edx
	addq	$16, %r15
	movq	(%rsi), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	cmpq	%r15, %r13
	jne	.L159
.L156:
	movq	(%rbx), %rax
	movq	16(%r12), %rdi
	movzbl	23(%rax), %esi
	andl	$15, %esi
	cmpl	$15, %esi
	jne	.L161
	movq	32(%rax), %rax
	movl	8(%rax), %esi
.L161:
	subl	$1, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder20InductionVariablePhiEi@PLT
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L149:
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, -56(%rbp)
	jne	.L162
.L146:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10566:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer29ChangeToInductionVariablePhisEv, .-_ZN2v88internal8compiler21LoopVariableOptimizer29ChangeToInductionVariablePhisEv
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer27ChangeToPhisAndInsertGuardsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizer27ChangeToPhisAndInsertGuardsEv
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer27ChangeToPhisAndInsertGuardsEv, @function
_ZN2v88internal8compiler21LoopVariableOptimizer27ChangeToPhisAndInsertGuardsEv:
.LFB10571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	128(%rdi), %rbx
	subq	$104, %rsp
	movq	144(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-88(%rbp), %rax
	movq	%rax, -104(%rbp)
	cmpq	%rbx, %r12
	je	.L167
	movq	%rdi, %r13
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	je	.L167
.L168:
	movq	40(%r12), %r15
	movq	(%r15), %rdi
	movq	(%rdi), %rax
	cmpw	$37, 16(%rax)
	jne	.L169
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%r15), %rdi
	movl	$3, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	(%r15), %rsi
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L170
	movq	48(%rsi), %rdi
	leaq	48(%rsi), %rax
	cmpq	%rdi, %r14
	je	.L173
.L172:
	subq	$72, %rsi
	testq	%rdi, %rdi
	je	.L174
	movq	%rax, -120(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rsi
.L174:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L173
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L173:
	movq	16(%r13), %rdi
	movl	$2, %edx
	movl	$8, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	(%r15), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	(%r15), %rax
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L175
	addq	$40, %rax
.L176:
	movq	(%rax), %r14
	movq	8(%r14), %rax
	movq	%rax, -88(%rbp)
	movq	(%r15), %rcx
	movq	8(%rcx), %r8
	cmpq	%r8, %rax
	je	.L169
	movq	-104(%rbp), %rdi
	movq	%r8, %rsi
	movq	%r8, -112(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L169
	movq	(%r15), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-112(%rbp), %r8
	movzbl	23(%rax), %ecx
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L180
	addq	$40, %rax
.L181:
	movq	(%rax), %rcx
	movq	8(%r15), %rdi
	movl	$1, %esi
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	-136(%rbp), %r8
	movq	8(%r13), %r9
	movq	16(%r13), %rdi
	movq	%rax, -112(%rbp)
	movq	%r8, %rsi
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TypeGuardENS1_4TypeE@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r14, %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%rcx, -64(%rbp)
	movhps	-112(%rbp), %xmm0
	leaq	-80(%rbp), %rcx
	movq	%r9, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r15), %rsi
	movq	%rax, %r8
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L182
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %rax
	cmpq	%rdi, %r8
	je	.L185
.L184:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L186
	movq	%rax, -128(%rbp)
	movq	%r8, -120(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %r8
	movq	-112(%rbp), %rsi
.L186:
	movq	%r8, (%rax)
	testq	%r8, %r8
	je	.L185
	movq	%r8, %rdi
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-112(%rbp), %r8
.L185:
	movq	(%r15), %rsi
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L187
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %rax
	cmpq	%rdi, %r8
	je	.L169
.L188:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L189
	movq	%r15, %rsi
	movq	%rax, -120(%rbp)
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %r8
.L189:
	movq	%r8, (%rax)
	testq	%r8, %r8
	je	.L169
	movq	%r8, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L168
	.p2align 4,,10
	.p2align 3
.L167:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L213
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movq	32(%rsi), %rsi
	movq	32(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L173
	leaq	32(%rsi), %rax
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L175:
	movq	32(%rax), %rax
	addq	$24, %rax
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L187:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r8
	je	.L169
	leaq	24(%rsi), %rax
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L182:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r8
	je	.L185
	leaq	24(%rsi), %rax
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L180:
	movq	32(%rax), %rax
	addq	$24, %rax
	jmp	.L181
.L213:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10571:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer27ChangeToPhisAndInsertGuardsEv, .-_ZN2v88internal8compiler21LoopVariableOptimizer27ChangeToPhisAndInsertGuardsEv
	.section	.text._ZNSt6vectorIN2v88internal8compiler17InductionVariable5BoundENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler17InductionVariable5BoundENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler17InductionVariable5BoundENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal8compiler17InductionVariable5BoundENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal8compiler17InductionVariable5BoundENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_:
.LFB11832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r12
	cmpq	24(%rdi), %r12
	je	.L215
	movq	(%rsi), %rdx
	movl	8(%rsi), %eax
	movq	%rdx, (%r12)
	movl	%eax, 8(%r12)
	addq	$16, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r12, %r15
	subq	%r14, %r15
	movq	%r15, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L229
	testq	%rax, %rax
	je	.L224
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L230
	movl	$2147483632, %esi
	movl	$2147483632, %r8d
.L218:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L231
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L221:
	addq	%rax, %r8
	leaq	16(%rax), %rcx
.L219:
	movq	0(%r13), %rdi
	movl	8(%r13), %esi
	leaq	(%rax,%r15), %rdx
	movq	%rdi, (%rdx)
	movl	%esi, 8(%rdx)
	cmpq	%r14, %r12
	je	.L222
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L223:
	movq	(%rdx), %rdi
	movl	8(%rdx), %esi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	%rdi, -16(%rcx)
	movl	%esi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L223
	subq	%r14, %r12
	leaq	16(%rax,%r12), %rcx
.L222:
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	%r8, 24(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L232
	movl	$16, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L224:
	movl	$16, %esi
	movl	$16, %r8d
	jmp	.L218
.L231:
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	jmp	.L221
.L229:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L232:
	cmpq	$134217727, %rdx
	movl	$134217727, %r8d
	cmovbe	%rdx, %r8
	salq	$4, %r8
	movq	%r8, %rsi
	jmp	.L218
	.cfi_endproc
.LFE11832:
	.size	_ZNSt6vectorIN2v88internal8compiler17InductionVariable5BoundENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal8compiler17InductionVariable5BoundENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.section	.rodata._ZN2v88internal8compiler17InductionVariable13AddUpperBoundEPNS1_4NodeENS2_14ConstraintKindE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"New upper bound for "
	.section	.text._ZN2v88internal8compiler17InductionVariable13AddUpperBoundEPNS1_4NodeENS2_14ConstraintKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17InductionVariable13AddUpperBoundEPNS1_4NodeENS2_14ConstraintKindE
	.type	_ZN2v88internal8compiler17InductionVariable13AddUpperBoundEPNS1_4NodeENS2_14ConstraintKindE, @function
_ZN2v88internal8compiler17InductionVariable13AddUpperBoundEPNS1_4NodeENS2_14ConstraintKindE:
.LFB10543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$392, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal21FLAG_trace_turbo_loopE(%rip)
	jne	.L241
.L234:
	leaq	-416(%rbp), %rsi
	leaq	72(%rbx), %rdi
	movq	%r12, -416(%rbp)
	movl	%r13d, -408(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler17InductionVariable5BoundENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L242
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	leaq	-320(%rbp), %r14
	leaq	-400(%rbp), %r15
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rax, -320(%rbp)
	movq	%r15, %rdi
	xorl	%eax, %eax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$20, %edx
	movq	%r15, %rdi
	movq	%rax, -400(%rbp)
	leaq	.LC5(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movl	20(%rax), %esi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$7, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-424(%rbp), %r8
	movl	20(%rax), %esi
	movq	%r8, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-424(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_4NodeE@PLT
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r8,%rax), %rdi
	testq	%rdi, %rdi
	je	.L243
	cmpb	$0, 56(%rdi)
	je	.L236
	movsbl	67(%rdi), %esi
.L237:
	movq	%r8, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%r8, -432(%rbp)
	movq	%rdi, -424(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-424(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	-432(%rbp), %r8
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L237
	movq	%r8, -424(%rbp)
	call	*%rax
	movq	-424(%rbp), %r8
	movsbl	%al, %esi
	jmp	.L237
.L242:
	call	__stack_chk_fail@PLT
.L243:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE10543:
	.size	_ZN2v88internal8compiler17InductionVariable13AddUpperBoundEPNS1_4NodeENS2_14ConstraintKindE, .-_ZN2v88internal8compiler17InductionVariable13AddUpperBoundEPNS1_4NodeENS2_14ConstraintKindE
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer13VisitBackedgeEPNS1_4NodeES4_.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer13VisitBackedgeEPNS1_4NodeES4_.part.0, @function
_ZN2v88internal8compiler21LoopVariableOptimizer13VisitBackedgeEPNS1_4NodeES4_.part.0:
.LFB13033:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rdi), %rcx
	movq	48(%rdi), %rax
	movq	%rdx, -56(%rbp)
	movl	20(%rsi), %edx
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L244
	movq	(%rcx,%rdx,8), %rbx
	testq	%rbx, %rbx
	je	.L244
	leaq	128(%rdi), %rax
	movq	%rdi, %r14
	movq	%rax, -64(%rbp)
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L247:
	movq	0(%r13), %rax
	cmpw	$35, 16(%rax)
	je	.L272
.L254:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L244
.L260:
	movq	(%rbx), %r12
	movl	8(%rbx), %r15d
	movq	16(%rbx), %r13
	movq	(%r12), %rax
	cmpw	$35, 16(%rax)
	jne	.L247
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	cmpq	%rax, -56(%rbp)
	jne	.L247
	movl	20(%r12), %esi
	movq	136(%r14), %rax
	andl	$16777215, %esi
	testq	%rax, %rax
	je	.L247
	movq	-64(%rbp), %rdi
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L273:
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L250
.L249:
	cmpl	32(%rax), %esi
	jle	.L273
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L249
.L250:
	cmpq	%rdi, -64(%rbp)
	je	.L247
	cmpl	32(%rdi), %esi
	jl	.L247
	movq	40(%rdi), %rdi
	movl	%r15d, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler17InductionVariable13AddUpperBoundEPNS1_4NodeENS2_14ConstraintKindE
	movq	0(%r13), %rax
	cmpw	$35, 16(%rax)
	jne	.L254
	.p2align 4,,10
	.p2align 3
.L272:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	cmpq	%rax, -56(%rbp)
	jne	.L254
	movl	20(%r13), %ecx
	movq	136(%r14), %rax
	andl	$16777215, %ecx
	testq	%rax, %rax
	je	.L254
	movq	-64(%rbp), %rsi
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%rax, %rsi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L257
.L256:
	cmpl	32(%rax), %ecx
	jle	.L274
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L256
.L257:
	cmpq	%rsi, -64(%rbp)
	je	.L254
	cmpl	32(%rsi), %ecx
	jl	.L254
	movq	40(%rsi), %rdi
	movl	%r15d, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler17InductionVariable13AddLowerBoundEPNS1_4NodeENS2_14ConstraintKindE
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L260
.L244:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13033:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer13VisitBackedgeEPNS1_4NodeES4_.part.0, .-_ZN2v88internal8compiler21LoopVariableOptimizer13VisitBackedgeEPNS1_4NodeES4_.part.0
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer13VisitBackedgeEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizer13VisitBackedgeEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer13VisitBackedgeEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler21LoopVariableOptimizer13VisitBackedgeEPNS1_4NodeES4_:
.LFB10553:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rax
	cmpl	$2, 28(%rax)
	jne	.L275
	jmp	_ZN2v88internal8compiler21LoopVariableOptimizer13VisitBackedgeEPNS1_4NodeES4_.part.0
	.p2align 4,,10
	.p2align 3
.L275:
	ret
	.cfi_endproc
.LFE10553:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer13VisitBackedgeEPNS1_4NodeES4_, .-_ZN2v88internal8compiler21LoopVariableOptimizer13VisitBackedgeEPNS1_4NodeES4_
	.section	.rodata._ZNSt6vectorIN2v88internal8compiler14FunctionalListINS2_21LoopVariableOptimizer10ConstraintEEENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_.str1.1,"aMS",@progbits,1
.LC6:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIN2v88internal8compiler14FunctionalListINS2_21LoopVariableOptimizer10ConstraintEEENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler14FunctionalListINS2_21LoopVariableOptimizer10ConstraintEEENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler14FunctionalListINS2_21LoopVariableOptimizer10ConstraintEEENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	.type	_ZNSt6vectorIN2v88internal8compiler14FunctionalListINS2_21LoopVariableOptimizer10ConstraintEEENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_, @function
_ZNSt6vectorIN2v88internal8compiler14FunctionalListINS2_21LoopVariableOptimizer10ConstraintEEENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_:
.LFB12210:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L387
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	24(%r12), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jb	.L280
	movq	%rdi, %r8
	movq	(%rcx), %xmm0
	subq	%rsi, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L281
	leaq	0(,%rdx,8), %r14
	movq	%rdi, %rdx
	subq	%r14, %rdx
	cmpq	%rdx, %rdi
	je	.L328
	leaq	-8(%rdi), %rax
	movl	$16, %ecx
	subq	%rdx, %rax
	subq	%r14, %rcx
	shrq	$3, %rax
	testq	%rcx, %rcx
	leaq	16(%rdi), %rcx
	setle	%sil
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L329
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L329
	leaq	1(%rax), %r8
	xorl	%eax, %eax
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L284:
	movdqu	(%rdx,%rax), %xmm2
	movups	%xmm2, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L284
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	leaq	(%rdx,%rcx), %rsi
	addq	%rdi, %rcx
	cmpq	%rax, %r8
	je	.L286
	movq	(%rsi), %rax
	movq	%rax, (%rcx)
.L286:
	movq	16(%r12), %rax
.L282:
	addq	%r14, %rax
	movq	%rax, 16(%r12)
	cmpq	%rdx, %rbx
	je	.L287
	subq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%xmm0, -40(%rbp)
	subq	%rdx, %rdi
	call	memmove@PLT
	movq	-40(%rbp), %xmm0
.L287:
	leaq	(%rbx,%r14), %rdx
	cmpq	%rdx, %rbx
	je	.L277
	subq	%rbx, %rdx
	movq	%rbx, %rax
	subq	$8, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rdx, %rdx
	je	.L327
	movq	%rcx, %rdx
	movdqa	%xmm0, %xmm1
	shrq	%rdx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rdx
	addq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L291:
	movups	%xmm1, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L291
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	(%rbx,%rax,8), %r13
	cmpq	%rax, %rcx
	je	.L277
.L327:
	movq	%xmm0, 0(%r13)
.L277:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L387:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	subq	%rax, %rdx
	je	.L330
	cmpq	$1, %rdx
	je	.L331
	movq	%rdx, %rsi
	movdqa	%xmm0, %xmm1
	xorl	%eax, %eax
	shrq	%rsi
	punpcklqdq	%xmm1, %xmm1
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%rax, %rcx
	addq	$1, %rax
	salq	$4, %rcx
	movups	%xmm1, (%rdi,%rcx)
	cmpq	%rax, %rsi
	jne	.L295
	movq	%rdx, %rcx
	andq	$-2, %rcx
	leaq	(%rdi,%rcx,8), %rax
	cmpq	%rcx, %rdx
	je	.L296
.L294:
	movq	%xmm0, (%rax)
.L296:
	leaq	(%rdi,%rdx,8), %rsi
.L293:
	movq	%rsi, 16(%r12)
	cmpq	%rbx, %rdi
	je	.L297
	subq	%rbx, %rdi
	leaq	15(%rbx), %rcx
	movq	%rbx, %rdx
	leaq	-8(%rdi), %r9
	subq	%rsi, %rcx
	movq	%r9, %rax
	shrq	$3, %rax
	cmpq	$30, %rcx
	jbe	.L332
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L332
	leaq	1(%rax), %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L299:
	movdqu	(%rbx,%rax), %xmm3
	movups	%xmm3, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L299
	movq	%rcx, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rax
	leaq	(%rbx,%rax), %rdi
	addq	%rax, %rsi
	cmpq	%r9, %rcx
	je	.L300
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
.L300:
	addq	%r8, 16(%r12)
	movq	%rcx, %rax
.L326:
	movq	%rax, %rcx
	movdqa	%xmm0, %xmm1
	shrq	%rcx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rcx
	addq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L304:
	movups	%xmm1, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L304
	movq	%rax, %rdx
	andq	$-2, %rdx
	leaq	(%rbx,%rdx,8), %r13
	cmpq	%rax, %rdx
	jne	.L327
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L280:
	movq	8(%r12), %rsi
	movl	$268435455, %r8d
	movq	%r8, %rax
	subq	%rsi, %rdi
	sarq	$3, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L390
	cmpq	%rdi, %rdx
	movq	%rdi, %rax
	cmovnb	%rdx, %rax
	addq	%rax, %rdi
	setc	%al
	subq	%rsi, %r13
	movzbl	%al, %eax
	testq	%rax, %rax
	jne	.L333
	testq	%rdi, %rdi
	jne	.L391
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L309:
	leaq	0(,%rdx,8), %r9
	leaq	(%rax,%r13), %r11
	leaq	0(%r13,%r9), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	leaq	8(%rcx), %rsi
	setnb	%dil
	cmpq	%rsi, %r11
	setnb	%sil
	orb	%sil, %dil
	je	.L335
	leaq	-1(%rdx), %rsi
	cmpq	$3, %rsi
	jbe	.L335
	movq	(%rcx), %xmm0
	leaq	-2(%rdx), %rdi
	xorl	%esi, %esi
	shrq	%rdi
	addq	$1, %rdi
	punpcklqdq	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L313:
	movq	%rsi, %r10
	addq	$1, %rsi
	salq	$4, %r10
	movups	%xmm0, (%r11,%r10)
	cmpq	%rsi, %rdi
	ja	.L313
	leaq	(%rdi,%rdi), %rsi
	salq	$4, %rdi
	addq	%r11, %rdi
	cmpq	%rsi, %rdx
	je	.L315
	movq	(%rcx), %rdx
	movq	%rdx, (%rdi)
.L315:
	movq	8(%r12), %rcx
	cmpq	%rcx, %rbx
	je	.L336
	leaq	-8(%rbx), %r10
	leaq	15(%rcx), %rdx
	subq	%rcx, %r10
	subq	%rax, %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L337
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L337
	leaq	1(%rsi), %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L318:
	movdqu	(%rcx,%rdx), %xmm4
	movups	%xmm4, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L318
	movq	%r11, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %rcx
	addq	%rax, %rsi
	cmpq	%rdx, %r11
	je	.L320
	movq	(%rcx), %rdx
	movq	%rdx, (%rsi)
.L320:
	leaq	8(%rax,%r10), %rdi
.L316:
	movq	16(%r12), %rdx
	leaq	(%rdi,%r9), %rcx
	cmpq	%rdx, %rbx
	je	.L321
	subq	%rbx, %rdx
	leaq	-8(%rdx), %r10
	leaq	16(%rdi,%r9), %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %rbx
	leaq	16(%rbx), %rdx
	setnb	%dil
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %dil
	je	.L338
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L338
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L323:
	movdqu	(%rbx,%rdx), %xmm5
	movups	%xmm5, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L323
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rdi
	addq	%rdi, %rbx
	addq	%rcx, %rdi
	cmpq	%rdx, %rsi
	je	.L325
	movq	(%rbx), %rdx
	movq	%rdx, (%rdi)
.L325:
	leaq	8(%rcx,%r10), %rcx
.L321:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%r8, 24(%r12)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	movl	$2147483640, %esi
	movl	$2147483640, %r14d
.L308:
	movq	(%r12), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L392
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L311:
	leaq	(%rax,%r14), %r8
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L335:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L312:
	movq	(%rcx), %rdi
	movq	%rdi, (%r11,%rsi,8)
	addq	$1, %rsi
	cmpq	%rsi, %rdx
	jne	.L312
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L329:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L283:
	movq	(%rdx,%rcx,8), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rax, %rsi
	jne	.L283
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L332:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L298:
	movq	(%rbx,%rcx,8), %rdi
	movq	%rdi, (%rsi,%rcx,8)
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	%rax, %rdi
	jne	.L298
	addq	%r8, 16(%r12)
	addq	$1, %rax
	testq	%r9, %r9
	je	.L327
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L337:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L317:
	movq	(%rcx,%rdx,8), %rdi
	movq	%rdi, (%rax,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L317
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L338:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L322:
	movq	(%rbx,%rdx,8), %rdi
	movq	%rdi, (%rcx,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L322
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L330:
	movq	%rdi, %rsi
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L328:
	movq	%rdi, %rax
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L297:
	addq	%r8, %rsi
	movq	%rsi, 16(%r12)
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L336:
	movq	%rax, %rdi
	jmp	.L316
.L331:
	movq	%rdi, %rax
	jmp	.L294
.L392:
	movq	%r8, %rdi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	jmp	.L311
.L391:
	cmpq	$268435455, %rdi
	cmova	%r8, %rdi
	leaq	0(,%rdi,8), %r14
	movq	%r14, %rsi
	jmp	.L308
.L390:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12210:
	.size	_ZNSt6vectorIN2v88internal8compiler14FunctionalListINS2_21LoopVariableOptimizer10ConstraintEEENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_, .-_ZNSt6vectorIN2v88internal8compiler14FunctionalListINS2_21LoopVariableOptimizer10ConstraintEEENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer17VisitOtherControlEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizer17VisitOtherControlEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer17VisitOtherControlEPNS1_4NodeE, @function
_ZN2v88internal8compiler21LoopVariableOptimizer17VisitOtherControlEPNS1_4NodeE:
.LFB13035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	48(%r12), %rsi
	movq	40(%r12), %rcx
	movl	20(%rax), %edx
	movq	%rsi, %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L394
	movq	(%rcx,%rdx,8), %r13
.L394:
	movl	20(%rbx), %ebx
	andl	$16777215, %ebx
	cmpq	%rbx, %rax
	jbe	.L411
.L395:
	leaq	(%rcx,%rbx,8), %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L398
	movq	32(%rax), %rcx
	testq	%r13, %r13
	je	.L399
	cmpq	%rcx, 32(%r13)
	je	.L412
.L400:
	movq	%r13, (%rdx)
.L393:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L413
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L412:
	.cfi_restore_state
	cmpq	%rax, %r13
	je	.L393
	movq	%r13, %rcx
.L402:
	movq	(%rcx), %rdi
	cmpq	%rdi, (%rax)
	jne	.L400
	movl	8(%rcx), %edi
	cmpl	%edi, 8(%rax)
	jne	.L400
	movq	16(%rcx), %rdi
	cmpq	%rdi, 16(%rax)
	jne	.L400
	movq	24(%rax), %rax
	movq	24(%rcx), %rcx
	cmpq	%rcx, %rax
	jne	.L402
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L399:
	testq	%rcx, %rcx
	jne	.L400
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L411:
	leaq	1(%rbx), %rdx
	movq	$0, -48(%rbp)
	cmpq	%rdx, %rax
	jb	.L414
	jbe	.L395
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L395
	movq	%rax, 48(%r12)
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L398:
	xorl	%ecx, %ecx
	testq	%r13, %r13
	je	.L393
	cmpq	%rcx, 32(%r13)
	jne	.L400
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L414:
	leaq	-48(%rbp), %rcx
	subq	%rax, %rdx
	leaq	32(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler14FunctionalListINS2_21LoopVariableOptimizer10ConstraintEEENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	40(%r12), %rcx
	jmp	.L395
.L413:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13035:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer17VisitOtherControlEPNS1_4NodeE, .-_ZN2v88internal8compiler21LoopVariableOptimizer17VisitOtherControlEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer13VisitLoopExitEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizer13VisitLoopExitEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer13VisitLoopExitEPNS1_4NodeE, @function
_ZN2v88internal8compiler21LoopVariableOptimizer13VisitLoopExitEPNS1_4NodeE:
.LFB10560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	48(%r12), %rsi
	movq	40(%r12), %rcx
	movl	20(%rax), %edx
	movq	%rsi, %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L416
	movq	(%rcx,%rdx,8), %r13
.L416:
	movl	20(%rbx), %ebx
	andl	$16777215, %ebx
	cmpq	%rbx, %rax
	jbe	.L433
.L417:
	leaq	(%rcx,%rbx,8), %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L420
	movq	32(%rax), %rcx
	testq	%r13, %r13
	je	.L421
	cmpq	%rcx, 32(%r13)
	je	.L434
.L422:
	movq	%r13, (%rdx)
.L415:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L435
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	cmpq	%rax, %r13
	je	.L415
	movq	%r13, %rcx
.L424:
	movq	(%rcx), %rdi
	cmpq	%rdi, (%rax)
	jne	.L422
	movl	8(%rcx), %edi
	cmpl	%edi, 8(%rax)
	jne	.L422
	movq	16(%rcx), %rdi
	cmpq	%rdi, 16(%rax)
	jne	.L422
	movq	24(%rax), %rax
	movq	24(%rcx), %rcx
	cmpq	%rcx, %rax
	jne	.L424
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L421:
	testq	%rcx, %rcx
	jne	.L422
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L433:
	leaq	1(%rbx), %rdx
	movq	$0, -48(%rbp)
	cmpq	%rdx, %rax
	jb	.L436
	jbe	.L417
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L417
	movq	%rax, 48(%r12)
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L420:
	xorl	%ecx, %ecx
	testq	%r13, %r13
	je	.L415
	cmpq	%rcx, 32(%r13)
	jne	.L422
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L436:
	leaq	-48(%rbp), %rcx
	subq	%rax, %rdx
	leaq	32(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler14FunctionalListINS2_21LoopVariableOptimizer10ConstraintEEENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	40(%r12), %rcx
	jmp	.L417
.L435:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10560:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer13VisitLoopExitEPNS1_4NodeE, .-_ZN2v88internal8compiler21LoopVariableOptimizer13VisitLoopExitEPNS1_4NodeE
	.section	.text.unlikely._ZN2v88internal8compiler21LoopVariableOptimizer10VisitStartEPNS1_4NodeE,"ax",@progbits
	.align 2
.LCOLDB7:
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer10VisitStartEPNS1_4NodeE,"ax",@progbits
.LHOTB7:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizer10VisitStartEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer10VisitStartEPNS1_4NodeE, @function
_ZN2v88internal8compiler21LoopVariableOptimizer10VisitStartEPNS1_4NodeE:
.LFB10559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	20(%rsi), %ebx
	movq	48(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rcx
	movq	%rsi, %rax
	andl	$16777215, %ebx
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L448
.L438:
	leaq	(%rcx,%rbx,8), %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L437
	cmpq	$0, 32(%rax)
	je	.L442
	movq	$0, (%rdx)
.L437:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L449
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L448:
	.cfi_restore_state
	leaq	1(%rbx), %rdx
	movq	$0, -32(%rbp)
	movq	%rdi, %r12
	cmpq	%rdx, %rax
	jb	.L450
	jbe	.L438
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L438
	movq	%rax, 48(%rdi)
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L450:
	leaq	-32(%rbp), %rcx
	subq	%rax, %rdx
	leaq	32(%rdi), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler14FunctionalListINS2_21LoopVariableOptimizer10ConstraintEEENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	40(%r12), %rcx
	jmp	.L438
.L449:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal8compiler21LoopVariableOptimizer10VisitStartEPNS1_4NodeE
	.cfi_startproc
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer10VisitStartEPNS1_4NodeE.cold, @function
_ZN2v88internal8compiler21LoopVariableOptimizer10VisitStartEPNS1_4NodeE.cold:
.LFSB10559:
.L442:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE10559:
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer10VisitStartEPNS1_4NodeE
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer10VisitStartEPNS1_4NodeE, .-_ZN2v88internal8compiler21LoopVariableOptimizer10VisitStartEPNS1_4NodeE
	.section	.text.unlikely._ZN2v88internal8compiler21LoopVariableOptimizer10VisitStartEPNS1_4NodeE
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer10VisitStartEPNS1_4NodeE.cold, .-_ZN2v88internal8compiler21LoopVariableOptimizer10VisitStartEPNS1_4NodeE.cold
.LCOLDE7:
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer10VisitStartEPNS1_4NodeE
.LHOTE7:
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer30TakeConditionsFromFirstControlEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizer30TakeConditionsFromFirstControlEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer30TakeConditionsFromFirstControlEPNS1_4NodeE, @function
_ZN2v88internal8compiler21LoopVariableOptimizer30TakeConditionsFromFirstControlEPNS1_4NodeE:
.LFB10562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	48(%r12), %rsi
	movq	40(%r12), %rcx
	movl	20(%rax), %edx
	movq	%rsi, %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L452
	movq	(%rcx,%rdx,8), %r13
.L452:
	movl	20(%rbx), %ebx
	andl	$16777215, %ebx
	cmpq	%rbx, %rax
	jbe	.L469
.L453:
	leaq	(%rcx,%rbx,8), %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L456
	movq	32(%rax), %rcx
	testq	%r13, %r13
	je	.L457
	cmpq	%rcx, 32(%r13)
	je	.L470
.L458:
	movq	%r13, (%rdx)
.L451:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L471
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	cmpq	%rax, %r13
	je	.L451
	movq	%r13, %rcx
.L460:
	movq	(%rcx), %rdi
	cmpq	%rdi, (%rax)
	jne	.L458
	movl	8(%rcx), %edi
	cmpl	%edi, 8(%rax)
	jne	.L458
	movq	16(%rcx), %rdi
	cmpq	%rdi, 16(%rax)
	jne	.L458
	movq	24(%rax), %rax
	movq	24(%rcx), %rcx
	cmpq	%rcx, %rax
	jne	.L460
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L457:
	testq	%rcx, %rcx
	jne	.L458
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L469:
	leaq	1(%rbx), %rdx
	movq	$0, -48(%rbp)
	cmpq	%rdx, %rax
	jb	.L472
	jbe	.L453
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L453
	movq	%rax, 48(%r12)
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L456:
	xorl	%ecx, %ecx
	testq	%r13, %r13
	je	.L451
	cmpq	%rcx, 32(%r13)
	jne	.L458
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L472:
	leaq	-48(%rbp), %rcx
	subq	%rax, %rdx
	leaq	32(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler14FunctionalListINS2_21LoopVariableOptimizer10ConstraintEEENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	40(%r12), %rcx
	jmp	.L453
.L471:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10562:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer30TakeConditionsFromFirstControlEPNS1_4NodeE, .-_ZN2v88internal8compiler21LoopVariableOptimizer30TakeConditionsFromFirstControlEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler21LoopVariableOptimizer10VisitMergeEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"Size() > 0"
.LC9:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer10VisitMergeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizer10VisitMergeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer10VisitMergeEPNS1_4NodeE, @function
_ZN2v88internal8compiler21LoopVariableOptimizer10VisitMergeEPNS1_4NodeE:
.LFB10555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	20(%rsi), %r13d
	movq	32(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r13d, %ecx
	movq	%r8, %rax
	shrl	$24, %ecx
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L474
	movq	16(%r8), %rax
.L474:
	movq	48(%r12), %r11
	movq	40(%r12), %r10
	xorl	%ebx, %ebx
	movl	20(%rax), %eax
	movq	%r11, %rdi
	subq	%r10, %rdi
	andl	$16777215, %eax
	sarq	$3, %rdi
	cmpq	%rdi, %rax
	jnb	.L475
	movq	(%r10,%rax,8), %rbx
.L475:
	xorl	%r9d, %r9d
	leaq	24(%r8), %r14
	addq	$40, %rsi
.L492:
	leal	1(%r9), %eax
	cmpl	$15, %ecx
	je	.L476
	cmpl	%eax, %ecx
	jle	.L478
	leaq	(%rsi,%r9,8), %rax
.L480:
	movq	(%rax), %rax
	movl	20(%rax), %eax
	andl	$16777215, %eax
	cmpq	%rax, %rdi
	jbe	.L481
	movq	(%r10,%rax,8), %rax
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L502:
	movq	24(%rax), %rax
.L485:
	testq	%rax, %rax
	je	.L481
	movq	32(%rax), %rdx
	testq	%rbx, %rbx
	je	.L531
	cmpq	%rdx, 32(%rbx)
	jnb	.L489
	cmpq	$0, 32(%rax)
	jne	.L502
.L484:
	leaq	.LC8(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L481:
	xorl	%eax, %eax
	xorl	%edx, %edx
	testq	%rbx, %rbx
	jne	.L489
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L534:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L532
.L489:
	movq	32(%rbx), %r15
	cmpq	%r15, %rdx
	jnb	.L533
	testq	%r15, %r15
	je	.L484
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L531:
	testq	%rdx, %rdx
	jne	.L502
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L476:
	cmpl	%eax, 8(%r8)
	jle	.L478
	leaq	(%r14,%r9,8), %rax
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L532:
	testq	%rax, %rax
	jne	.L484
	.p2align 4,,10
	.p2align 3
.L487:
	addq	$1, %r9
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L533:
	cmpq	%rax, %rbx
	je	.L487
.L488:
	cmpq	$0, 32(%rbx)
	je	.L484
	movq	24(%rbx), %rbx
	testq	%rax, %rax
	je	.L484
	cmpq	$0, 32(%rax)
	je	.L484
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	je	.L487
	testq	%rbx, %rbx
	jne	.L488
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L478:
	andl	$16777215, %r13d
	cmpq	%r13, %rdi
	jbe	.L535
.L493:
	leaq	(%r10,%r13,8), %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L496
	movq	32(%rax), %rcx
	testq	%rbx, %rbx
	je	.L497
.L503:
	cmpq	32(%rbx), %rcx
	je	.L536
.L498:
	movq	%rbx, (%rdx)
.L473:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L537
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L536:
	.cfi_restore_state
	cmpq	%rbx, %rax
	je	.L473
	movq	%rbx, %rcx
.L500:
	movq	(%rcx), %rsi
	cmpq	%rsi, (%rax)
	jne	.L498
	movl	8(%rcx), %esi
	cmpl	%esi, 8(%rax)
	jne	.L498
	movq	16(%rcx), %rsi
	cmpq	%rsi, 16(%rax)
	jne	.L498
	movq	24(%rax), %rax
	movq	24(%rcx), %rcx
	cmpq	%rcx, %rax
	jne	.L500
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L535:
	leaq	1(%r13), %rax
	movq	$0, -64(%rbp)
	cmpq	%rax, %rdi
	jb	.L538
	jbe	.L493
	leaq	(%r10,%rax,8), %rax
	cmpq	%rax, %r11
	je	.L493
	movq	%rax, 48(%r12)
	jmp	.L493
.L496:
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	jne	.L503
	jmp	.L473
.L497:
	testq	%rcx, %rcx
	jne	.L498
	jmp	.L500
.L538:
	subq	%rdi, %rax
	leaq	-64(%rbp), %rcx
	leaq	32(%r12), %rdi
	movq	%r11, %rsi
	movq	%rax, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler14FunctionalListINS2_21LoopVariableOptimizer10ConstraintEEENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	40(%r12), %r10
	jmp	.L493
.L537:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10555:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer10VisitMergeEPNS1_4NodeE, .-_ZN2v88internal8compiler21LoopVariableOptimizer10VisitMergeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer7VisitIfEPNS1_4NodeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizer7VisitIfEPNS1_4NodeEb
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer7VisitIfEPNS1_4NodeEb, @function
_ZN2v88internal8compiler21LoopVariableOptimizer7VisitIfEPNS1_4NodeEb:
.LFB10557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	20(%rsi), %r12d
	movq	32(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%r12d, %eax
	xorl	$251658240, %eax
	testl	$251658240, %eax
	jne	.L540
	movq	16(%rcx), %rcx
.L540:
	movl	20(%rcx), %eax
	movq	32(%rcx), %rdx
	movl	%eax, %ecx
	xorl	$251658240, %ecx
	andl	$251658240, %ecx
	jne	.L541
	movq	16(%rdx), %rdx
.L541:
	movq	48(%rbx), %r9
	movq	40(%rbx), %rdi
	andl	$16777215, %eax
	xorl	%esi, %esi
	movq	%r9, %rcx
	subq	%rdi, %rcx
	sarq	$3, %rcx
	cmpq	%rcx, %rax
	jnb	.L542
	movq	(%rdi,%rax,8), %rsi
.L542:
	movq	%rsi, -56(%rbp)
	movq	(%rdx), %rax
	movzwl	16(%rax), %eax
	cmpw	$685, %ax
	je	.L543
	ja	.L544
	cmpw	$123, %ax
	je	.L582
	jbe	.L587
	cmpw	$124, %ax
	je	.L547
	cmpw	$684, %ax
	jne	.L548
.L582:
	leaq	-56(%rbp), %rsi
	movzbl	%r8b, %r8d
	xorl	%ecx, %ecx
.L583:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21LoopVariableOptimizer14AddCmpToLimitsEPNS1_14FunctionalListINS2_10ConstraintEEEPNS1_4NodeENS1_17InductionVariable14ConstraintKindEb
	movq	48(%rbx), %r9
	movq	40(%rbx), %rdi
	movl	20(%r13), %r12d
	movq	%r9, %rcx
	subq	%rdi, %rcx
	sarq	$3, %rcx
.L548:
	andl	$16777215, %r12d
	cmpq	%rcx, %r12
	jnb	.L588
.L550:
	leaq	(%rdi,%r12,8), %rcx
	movq	-56(%rbp), %rdx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L553
	movq	32(%rax), %rsi
	testq	%rdx, %rdx
	je	.L554
	cmpq	%rsi, 32(%rdx)
	je	.L584
.L555:
	movq	-56(%rbp), %rax
	movq	%rax, (%rcx)
	.p2align 4,,10
	.p2align 3
.L539:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L589
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L590:
	.cfi_restore_state
	movl	8(%rdx), %ebx
	cmpl	%ebx, 8(%rax)
	jne	.L555
	movq	16(%rdx), %rbx
	cmpq	%rbx, 16(%rax)
	jne	.L555
	movq	24(%rax), %rax
	movq	24(%rdx), %rdx
.L584:
	cmpq	%rdx, %rax
	je	.L539
.L557:
	movq	(%rdx), %rbx
	cmpq	%rbx, (%rax)
	je	.L590
	movq	-56(%rbp), %rax
	movq	%rax, (%rcx)
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L587:
	cmpw	$120, %ax
	je	.L582
	cmpw	$121, %ax
	jne	.L548
.L547:
	leaq	-56(%rbp), %rsi
	movzbl	%r8b, %r8d
	movl	$1, %ecx
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L544:
	cmpw	$686, %ax
	je	.L547
	cmpw	$687, %ax
	jne	.L548
	xorl	$1, %r8d
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L554:
	testq	%rsi, %rsi
	jne	.L555
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L588:
	leaq	1(%r12), %rdx
	movq	$0, -48(%rbp)
	cmpq	%rcx, %rdx
	ja	.L591
	jnb	.L550
	leaq	(%rdi,%rdx,8), %rax
	cmpq	%r9, %rax
	je	.L550
	movq	%rax, 48(%rbx)
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L553:
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.L539
	cmpq	%rsi, 32(%rdx)
	jne	.L555
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L543:
	xorl	$1, %r8d
	leaq	-56(%rbp), %rsi
	movl	$1, %ecx
	movzbl	%r8b, %r8d
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L591:
	leaq	-48(%rbp), %r8
	subq	%rcx, %rdx
	leaq	32(%rbx), %rdi
	movq	%r9, %rsi
	movq	%r8, %rcx
	call	_ZNSt6vectorIN2v88internal8compiler14FunctionalListINS2_21LoopVariableOptimizer10ConstraintEEENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	40(%rbx), %rdi
	jmp	.L550
.L589:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10557:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer7VisitIfEPNS1_4NodeEb, .-_ZN2v88internal8compiler21LoopVariableOptimizer7VisitIfEPNS1_4NodeEb
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_
	.type	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_, @function
_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_:
.LFB12223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r15
	je	.L642
	movl	(%rdx), %r14d
	cmpl	32(%rsi), %r14d
	jge	.L603
	movq	32(%rdi), %rbx
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rsi, %rbx
	je	.L595
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	32(%rax), %r14d
	jle	.L605
	movl	$0, %ebx
	cmpq	$0, 24(%rax)
	movq	%rbx, %rax
	cmovne	%r12, %rdx
	cmovne	%r12, %rax
.L595:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L603:
	.cfi_restore_state
	jle	.L614
	movq	40(%rdi), %rdx
	cmpq	%rsi, %rdx
	je	.L641
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	32(%rax), %r14d
	jge	.L616
	movl	$0, %ebx
	cmpq	$0, 24(%r12)
	movq	%rbx, %rax
	cmovne	%rdx, %rax
	cmove	%r12, %rdx
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L642:
	cmpq	$0, 48(%rdi)
	je	.L594
	movq	40(%rdi), %rdx
	movl	(%r14), %eax
	cmpl	%eax, 32(%rdx)
	jl	.L641
.L594:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L625
	movl	(%r14), %esi
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L643:
	movq	16(%rbx), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L598
.L644:
	movq	%rax, %rbx
.L597:
	movl	32(%rbx), %edx
	cmpl	%edx, %esi
	jl	.L643
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L644
.L598:
	movq	%rbx, %r12
	testb	%cl, %cl
	jne	.L596
.L601:
	xorl	%eax, %eax
	cmpl	%esi, %edx
	cmovl	%rax, %rbx
	cmovge	%rax, %r12
.L602:
	addq	$8, %rsp
	movq	%rbx, %rax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L641:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L605:
	.cfi_restore_state
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L608
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L646:
	movq	16(%r12), %rax
	movl	$1, %esi
.L611:
	testq	%rax, %rax
	je	.L609
	movq	%rax, %r12
.L608:
	movl	32(%r12), %ecx
	cmpl	%ecx, %r14d
	jl	.L646
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L625:
	movq	%r12, %rbx
.L596:
	cmpq	%rbx, 32(%r13)
	je	.L627
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%r14), %esi
	movl	32(%rax), %edx
	movq	%rax, %rbx
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L609:
	movq	%r12, %rdx
	testb	%sil, %sil
	jne	.L607
.L612:
	xorl	%eax, %eax
	cmpl	%ecx, %r14d
	cmovg	%rax, %r12
	cmovle	%rax, %rdx
.L613:
	movq	%r12, %rax
	jmp	.L595
.L645:
	movq	%r15, %r12
.L607:
	cmpq	%r12, %rbx
	je	.L631
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %r12
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L616:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L619
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L648:
	movq	16(%rbx), %rax
	movl	$1, %esi
.L622:
	testq	%rax, %rax
	je	.L620
	movq	%rax, %rbx
.L619:
	movl	32(%rbx), %ecx
	cmpl	%ecx, %r14d
	jl	.L648
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L620:
	movq	%rbx, %rdx
	testb	%sil, %sil
	jne	.L618
.L623:
	xorl	%eax, %eax
	cmpl	%ecx, %r14d
	cmovg	%rax, %rbx
	cmovle	%rax, %rdx
.L624:
	movq	%rbx, %rax
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L627:
	movq	%rbx, %r12
	xorl	%ebx, %ebx
	jmp	.L602
.L647:
	movq	%r15, %rbx
.L618:
	cmpq	%rbx, 32(%r13)
	je	.L635
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %rbx
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L631:
	movq	%r12, %rdx
	xorl	%r12d, %r12d
	jmp	.L613
.L635:
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	jmp	.L624
	.cfi_endproc
.LFE12223:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_, .-_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_
	.section	.rodata._ZN2v88internal8compiler21LoopVariableOptimizer24DetectInductionVariablesEPNS1_4NodeE.part.0.str1.1,"aMS",@progbits,1
.LC10:
	.string	"Loop variables for loop %i:"
.LC11:
	.string	" %i"
.LC12:
	.string	"\n"
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer24DetectInductionVariablesEPNS1_4NodeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer24DetectInductionVariablesEPNS1_4NodeE.part.0, @function
_ZN2v88internal8compiler21LoopVariableOptimizer24DetectInductionVariablesEPNS1_4NodeE.part.0:
.LFB13031:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, _ZN2v88internal21FLAG_trace_turbo_loopE(%rip)
	jne	.L685
.L650:
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L651
	movq	0(%r13), %r14
	leaq	128(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L668:
	movl	16(%r13), %ecx
	movq	%r13, %rdi
	movl	%ecx, %edx
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rax
	leaq	0(%r13,%rax,8), %rax
	leaq	32(%rax), %rsi
	addq	$16, %rax
	andl	$1, %ecx
	cmovne	%rsi, %rax
	leaq	(%rax,%rdx,8), %rsi
	call	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L655
	movl	16(%r13), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	0(%r13,%rax,8), %r13
	movq	0(%r13), %rax
	jne	.L656
	movq	%rax, %r13
	movq	(%rax), %rax
.L656:
	cmpw	$35, 16(%rax)
	je	.L686
.L655:
	testq	%r14, %r14
	je	.L651
	movq	%r14, %r13
	movq	(%r14), %r14
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L651:
	cmpb	$0, _ZN2v88internal21FLAG_trace_turbo_loopE(%rip)
	jne	.L687
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L686:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21LoopVariableOptimizer23TryGetInductionVariableEPNS1_4NodeE
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L655
	movl	20(%r13), %r13d
	movq	136(%rbx), %rax
	movq	%r12, %r15
	andl	$16777215, %r13d
	testq	%rax, %rax
	jne	.L659
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L688:
	movq	%rax, %r15
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L660
.L659:
	cmpl	32(%rax), %r13d
	jle	.L688
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L659
.L660:
	cmpq	%r15, %r12
	je	.L658
	cmpl	32(%r15), %r13d
	jge	.L663
.L658:
	movq	112(%rbx), %rdi
	leaq	112(%rbx), %r10
	movq	16(%rdi), %r9
	movq	24(%rdi), %rax
	subq	%r9, %rax
	cmpq	$47, %rax
	jbe	.L689
	leaq	48(%r9), %rax
	movq	%rax, 16(%rdi)
.L665:
	movl	%r13d, 32(%r9)
	movq	%r15, %rsi
	leaq	32(%r9), %rdx
	movq	%r10, %rdi
	movq	$0, 40(%r9)
	movq	%r8, -56(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS7_ERS1_
	movq	-56(%rbp), %r8
	testq	%rdx, %rdx
	movq	%rax, %r15
	je	.L663
	cmpq	%rdx, %r12
	movq	-64(%rbp), %r9
	je	.L671
	testq	%rax, %rax
	je	.L690
.L671:
	movl	$1, %edi
.L667:
	movq	%r9, %rsi
	movq	%r12, %rcx
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	addq	$1, 160(%rbx)
	movq	%r9, %r15
.L663:
	cmpb	$0, _ZN2v88internal21FLAG_trace_turbo_loopE(%rip)
	movq	%r8, 40(%r15)
	je	.L655
	movq	(%r8), %rax
	leaq	.LC11(%rip), %rdi
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L687:
	addq	$24, %rsp
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L685:
	.cfi_restore_state
	movl	20(%rsi), %esi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L650
.L690:
	xorl	%edi, %edi
	movl	32(%rdx), %eax
	cmpl	%eax, 32(%r9)
	setl	%dil
	jmp	.L667
.L689:
	movl	$48, %esi
	movq	%r10, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r10
	movq	%rax, %r9
	jmp	.L665
	.cfi_endproc
.LFE13031:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer24DetectInductionVariablesEPNS1_4NodeE.part.0, .-_ZN2v88internal8compiler21LoopVariableOptimizer24DetectInductionVariablesEPNS1_4NodeE.part.0
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer24DetectInductionVariablesEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizer24DetectInductionVariablesEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer24DetectInductionVariablesEPNS1_4NodeE, @function
_ZN2v88internal8compiler21LoopVariableOptimizer24DetectInductionVariablesEPNS1_4NodeE:
.LFB10565:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	cmpl	$2, 28(%rax)
	jne	.L691
	jmp	_ZN2v88internal8compiler21LoopVariableOptimizer24DetectInductionVariablesEPNS1_4NodeE.part.0
	.p2align 4,,10
	.p2align 3
.L691:
	ret
	.cfi_endproc
.LFE10565:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer24DetectInductionVariablesEPNS1_4NodeE, .-_ZN2v88internal8compiler21LoopVariableOptimizer24DetectInductionVariablesEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer9VisitLoopEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizer9VisitLoopEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer9VisitLoopEPNS1_4NodeE, @function
_ZN2v88internal8compiler21LoopVariableOptimizer9VisitLoopEPNS1_4NodeE:
.LFB10556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpl	$2, 28(%rax)
	jne	.L694
	call	_ZN2v88internal8compiler21LoopVariableOptimizer24DetectInductionVariablesEPNS1_4NodeE.part.0
.L694:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	48(%r12), %rsi
	movq	40(%r12), %rcx
	movl	20(%rax), %edx
	movq	%rsi, %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L695
	movq	(%rcx,%rdx,8), %r13
.L695:
	movl	20(%rbx), %ebx
	andl	$16777215, %ebx
	cmpq	%rbx, %rax
	jbe	.L711
.L696:
	leaq	(%rcx,%rbx,8), %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L699
	movq	32(%rax), %rcx
	testq	%r13, %r13
	je	.L700
	cmpq	%rcx, 32(%r13)
	je	.L712
.L701:
	movq	%r13, (%rdx)
.L693:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L713
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L712:
	.cfi_restore_state
	cmpq	%rax, %r13
	je	.L693
	movq	%r13, %rcx
.L703:
	movq	(%rcx), %rdi
	cmpq	%rdi, (%rax)
	jne	.L701
	movl	8(%rcx), %edi
	cmpl	%edi, 8(%rax)
	jne	.L701
	movq	16(%rcx), %rdi
	cmpq	%rdi, 16(%rax)
	jne	.L701
	movq	24(%rax), %rax
	movq	24(%rcx), %rcx
	cmpq	%rcx, %rax
	jne	.L703
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L700:
	testq	%rcx, %rcx
	jne	.L701
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L711:
	leaq	1(%rbx), %rdx
	movq	$0, -48(%rbp)
	cmpq	%rdx, %rax
	jb	.L714
	jbe	.L696
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L696
	movq	%rax, 48(%r12)
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L699:
	xorl	%ecx, %ecx
	testq	%r13, %r13
	je	.L693
	cmpq	%rcx, 32(%r13)
	jne	.L701
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L714:
	leaq	-48(%rbp), %rcx
	subq	%rax, %rdx
	leaq	32(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler14FunctionalListINS2_21LoopVariableOptimizer10ConstraintEEENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	40(%r12), %rcx
	jmp	.L696
.L713:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10556:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer9VisitLoopEPNS1_4NodeE, .-_ZN2v88internal8compiler21LoopVariableOptimizer9VisitLoopEPNS1_4NodeE
	.section	.text.unlikely._ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE,"ax",@progbits
	.align 2
.LCOLDB13:
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE,"ax",@progbits
.LHOTB13:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE:
.LFB10554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -32
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpw	$10, 16(%rdx)
	ja	.L716
	movzwl	16(%rdx), %eax
	leaq	.L718(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L718:
	.long	.L722-.L718
	.long	.L721-.L718
	.long	.L716-.L718
	.long	.L716-.L718
	.long	.L720-.L718
	.long	.L719-.L718
	.long	.L716-.L718
	.long	.L716-.L718
	.long	.L716-.L718
	.long	.L716-.L718
	.long	.L717-.L718
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L721:
	cmpl	$2, 28(%rdx)
	jne	.L716
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal8compiler21LoopVariableOptimizer24DetectInductionVariablesEPNS1_4NodeE.part.0
	movq	-40(%rbp), %rsi
	.p2align 4,,10
	.p2align 3
.L716:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21LoopVariableOptimizer30TakeConditionsFromFirstControlEPNS1_4NodeE
.L715:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L735
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L717:
	.cfi_restore_state
	call	_ZN2v88internal8compiler21LoopVariableOptimizer10VisitMergeEPNS1_4NodeE
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L722:
	movl	20(%rsi), %ebx
	movq	48(%rdi), %rsi
	movq	40(%rdi), %rcx
	movq	%rsi, %rax
	andl	$16777215, %ebx
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L736
.L725:
	leaq	(%rcx,%rbx,8), %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L715
	cmpq	$0, 32(%rax)
	je	.L729
	movq	$0, (%rdx)
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L720:
	movl	$1, %edx
	call	_ZN2v88internal8compiler21LoopVariableOptimizer7VisitIfEPNS1_4NodeEb
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L719:
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler21LoopVariableOptimizer7VisitIfEPNS1_4NodeEb
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L736:
	leaq	1(%rbx), %rdx
	movq	$0, -32(%rbp)
	cmpq	%rdx, %rax
	jb	.L737
	jbe	.L725
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L725
	movq	%rax, 48(%rdi)
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L737:
	leaq	-32(%rbp), %rcx
	subq	%rax, %rdx
	leaq	32(%rdi), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler14FunctionalListINS2_21LoopVariableOptimizer10ConstraintEEENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	40(%r12), %rcx
	jmp	.L725
.L735:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE
	.cfi_startproc
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE.cold, @function
_ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE.cold:
.LFSB10554:
.L729:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE10554:
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE
	.section	.text.unlikely._ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE.cold, .-_ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE.cold
.LCOLDE13:
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE
.LHOTE13:
	.section	.rodata._ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb.str1.1,"aMS",@progbits,1
.LC14:
	.string	"vector<bool>::_M_fill_insert"
	.section	.text._ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb,"axG",@progbits,_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb
	.type	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb, @function
_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb:
.LFB12391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	testq	%rcx, %rcx
	je	.L738
	movq	%rdi, %rbx
	movq	8(%rdi), %rax
	movq	%rsi, -80(%rbp)
	movq	%rsi, %r12
	movq	24(%rbx), %r9
	movq	40(%rbx), %rsi
	movq	%rcx, %r13
	movl	32(%rdi), %edx
	movl	-56(%rbp), %r14d
	movq	%r9, %rcx
	subq	%rax, %rsi
	subq	%rax, %rcx
	movq	%rsi, %rax
	movq	%rdx, %rdi
	leaq	(%rdx,%rcx,8), %rcx
	salq	$3, %rax
	subq	%rcx, %rax
	cmpq	%r13, %rax
	jb	.L740
	leaq	0(%r13,%rdx), %rax
	testq	%rax, %rax
	leaq	63(%rax), %rcx
	cmovns	%rax, %rcx
	sarq	$6, %rcx
	leaq	(%r9,%rcx,8), %r8
	movq	%rax, %rcx
	sarq	$63, %rcx
	shrq	$58, %rcx
	addq	%rcx, %rax
	andl	$63, %eax
	subq	%rcx, %rax
	jns	.L741
	addq	$64, %rax
	subq	$8, %r8
.L741:
	movabsq	$-9223372036854775808, %r15
	movq	%r9, %rcx
	movl	%r14d, %r11d
	subq	%r12, %rcx
	movq	%r11, -88(%rbp)
	leaq	(%rdx,%rcx,8), %rsi
	subq	%r11, %rsi
	movl	$1, %r11d
	testq	%rsi, %rsi
	jle	.L750
	.p2align 4,,10
	.p2align 3
.L742:
	testl	%edi, %edi
	je	.L745
.L822:
	subl	$1, %edi
	movq	%r11, %r10
	movl	%edi, %ecx
	salq	%cl, %r10
	testl	%eax, %eax
	je	.L747
.L823:
	subl	$1, %eax
	movq	%r11, %rdx
	movl	%eax, %ecx
	salq	%cl, %rdx
.L748:
	movq	(%r8), %rcx
	testq	%r10, (%r9)
	je	.L749
	orq	%rcx, %rdx
	movq	%rdx, (%r8)
	subq	$1, %rsi
	jne	.L742
.L750:
	movq	-88(%rbp), %rax
	addq	%r13, %rax
	leaq	63(%rax), %rdx
	cmovns	%rax, %rdx
	sarq	$6, %rdx
	leaq	(%r12,%rdx,8), %r8
	cqto
	shrq	$58, %rdx
	leaq	(%rax,%rdx), %r15
	andl	$63, %r15d
	subq	%rdx, %r15
	jns	.L744
	addq	$64, %r15
	subq	$8, %r8
.L744:
	movl	%r15d, %r9d
	cmpq	%r12, %r8
	je	.L752
	testl	%r14d, %r14d
	jne	.L816
	movq	%r8, %r10
	subq	%r12, %r10
	cmpb	$0, -72(%rbp)
	je	.L756
.L755:
	movq	-80(%rbp), %rdi
	movq	%r10, %rdx
	movl	$-1, %esi
	movl	%r9d, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	memset@PLT
	testq	%r15, %r15
	movq	-72(%rbp), %r8
	movl	-88(%rbp), %r9d
	je	.L758
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r9d, %ecx
	shrq	%cl, %rax
	orq	%rax, (%r8)
	.p2align 4,,10
	.p2align 3
.L758:
	movl	32(%rbx), %ecx
	movq	24(%rbx), %rdx
	addq	%rcx, %r13
	leaq	63(%r13), %rax
	cmovns	%r13, %rax
	sarq	$6, %rax
	leaq	(%rdx,%rax,8), %rdx
	movq	%r13, %rax
	sarq	$63, %rax
	shrq	$58, %rax
	addq	%rax, %r13
	andl	$63, %r13d
	subq	%rax, %r13
	jns	.L813
	addq	$64, %r13
	subq	$8, %rdx
.L813:
	movq	%rdx, 24(%rbx)
.L814:
	movl	%r13d, 32(%rbx)
.L738:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L740:
	.cfi_restore_state
	movabsq	$17179869120, %rax
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %r13
	ja	.L817
	cmpq	%rcx, %r13
	movq	%rcx, %rdx
	cmovnb	%r13, %rdx
	addq	%rdx, %rcx
	jc	.L766
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	addq	$63, %rcx
	shrq	$6, %rcx
	leaq	0(,%rcx,8), %rax
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
.L767:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L818
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L769:
	movq	8(%rbx), %rsi
	movq	%r12, %rdx
	subq	%rsi, %rdx
	cmpq	%r12, %rsi
	je	.L770
	movq	%r15, %rdi
	movq	%rdx, -96(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %rdx
.L770:
	movl	%r14d, %esi
	leaq	(%r15,%rdx), %rdi
	movq	%rsi, -96(%rbp)
	testq	%rsi, %rsi
	je	.L798
	xorl	%edx, %edx
	movq	%r12, %r9
	movl	$1, %r10d
	movl	%edx, %ecx
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L774:
	addl	$1, %ecx
	subq	$1, %rsi
	je	.L819
.L776:
	movq	%r10, %rdx
	movq	(%rdi), %r11
	salq	%cl, %rdx
	movq	%rdx, %rax
	movq	%r11, %r8
	notq	%rax
	orq	%rdx, %r8
	andq	%r11, %rax
	testq	%rdx, (%r9)
	cmovne	%r8, %rax
	movq	%rax, (%rdi)
	cmpl	$63, %ecx
	jne	.L774
	addq	$8, %r9
	addq	$8, %rdi
	xorl	%ecx, %ecx
	subq	$1, %rsi
	jne	.L776
.L819:
	movl	%ecx, %ecx
	movq	%rcx, %rdx
.L771:
	addq	%rcx, %r13
	leaq	63(%r13), %rax
	cmovns	%r13, %rax
	sarq	$6, %rax
	leaq	(%rdi,%rax,8), %r8
	movq	%r13, %rax
	sarq	$63, %rax
	shrq	$58, %rax
	leaq	0(%r13,%rax), %r9
	andl	$63, %r9d
	subq	%rax, %r9
	jns	.L777
	addq	$64, %r9
	subq	$8, %r8
.L777:
	movl	%r9d, %r13d
	cmpq	%rdi, %r8
	je	.L778
	testl	%edx, %edx
	je	.L779
	movl	%edx, %ecx
	leaq	8(%rdi), %rsi
	movq	$-1, %rax
	movq	%r8, %rdx
	salq	%cl, %rax
	subq	%rsi, %rdx
	cmpb	$0, -72(%rbp)
	movq	(%rdi), %rcx
	je	.L780
	orq	%rcx, %rax
	movq	%rax, (%rdi)
	movq	%rsi, %rdi
.L781:
	movl	$-1, %esi
	movq	%r8, -104(%rbp)
	movq	%r9, -72(%rbp)
	call	memset@PLT
	movq	-72(%rbp), %r9
	movq	-104(%rbp), %r8
	testq	%r9, %r9
	jne	.L820
	.p2align 4,,10
	.p2align 3
.L784:
	movq	24(%rbx), %rax
	movl	32(%rbx), %edx
	subq	%r12, %rax
	leaq	(%rdx,%rax,8), %rdx
	subq	-96(%rbp), %rdx
	testq	%rdx, %rdx
	jle	.L788
	movq	-80(%rbp), %r9
	movl	$1, %edi
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L791:
	addl	$1, %r14d
	cmpl	$63, %r13d
	je	.L821
.L793:
	addl	$1, %r13d
	subq	$1, %rdx
	je	.L788
.L795:
	movq	(%r8), %rsi
	movl	%r13d, %ecx
	movq	%rdi, %rax
	movq	%rdi, %r10
	salq	%cl, %rax
	movl	%r14d, %ecx
	salq	%cl, %r10
	movq	%rsi, %rcx
	orq	%rax, %rcx
	notq	%rax
	andq	%rsi, %rax
	testq	%r10, (%r9)
	cmovne	%rcx, %rax
	movq	%rax, (%r8)
	cmpl	$63, %r14d
	jne	.L791
	addq	$8, %r9
	xorl	%r14d, %r14d
	cmpl	$63, %r13d
	jne	.L793
.L821:
	addq	$8, %r8
	xorl	%r13d, %r13d
	subq	$1, %rdx
	jne	.L795
.L788:
	cmpq	$0, 8(%rbx)
	je	.L796
	movq	$0, 24(%rbx)
	movl	$0, 32(%rbx)
	movq	$0, 8(%rbx)
	movl	$0, 16(%rbx)
.L796:
	movq	-88(%rbp), %rax
	movq	%r15, 8(%rbx)
	movl	$0, 16(%rbx)
	addq	%r15, %rax
	movq	%r8, 24(%rbx)
	movq	%rax, 40(%rbx)
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L749:
	notq	%rdx
	andq	%rcx, %rdx
	movq	%rdx, (%r8)
	subq	$1, %rsi
	je	.L750
	testl	%edi, %edi
	jne	.L822
.L745:
	subq	$8, %r9
	movq	%r15, %r10
	movl	$63, %edi
	testl	%eax, %eax
	jne	.L823
.L747:
	subq	$8, %r8
	movq	%r15, %rdx
	movl	$63, %eax
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L816:
	movl	%r14d, %ecx
	leaq	8(%r12), %rdx
	movq	$-1, %rax
	movq	%r8, %r10
	salq	%cl, %rax
	subq	%rdx, %r10
	cmpb	$0, -72(%rbp)
	movq	(%r12), %rcx
	je	.L754
	orq	%rcx, %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, (%r12)
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L754:
	notq	%rax
	movq	%rdx, -80(%rbp)
	andq	%rcx, %rax
	movq	%rax, (%r12)
.L756:
	movq	-80(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r10, %rdx
	movl	%r9d, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	memset@PLT
	testq	%r15, %r15
	movq	-72(%rbp), %r8
	movl	-88(%rbp), %r9d
	je	.L758
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r9d, %ecx
	shrq	%cl, %rax
	notq	%rax
	andq	%rax, (%r8)
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L752:
	cmpl	%r14d, %r15d
	je	.L758
	movq	$-1, %rdx
	movl	$64, %ecx
	subl	%r15d, %ecx
	movq	%rdx, %rax
	shrq	%cl, %rax
	movl	%r14d, %ecx
	salq	%cl, %rdx
	movq	(%r8), %rcx
	andq	%rdx, %rax
	movq	%rax, %rdx
	orq	%rcx, %rax
	notq	%rdx
	andq	%rcx, %rdx
	cmpb	$0, -72(%rbp)
	cmove	%rdx, %rax
	movq	%rax, (%r8)
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L779:
	movq	%r8, %rdx
	subq	%rdi, %rdx
	cmpb	$0, -72(%rbp)
	jne	.L781
.L782:
	xorl	%esi, %esi
	movq	%r8, -104(%rbp)
	movq	%r9, -72(%rbp)
	call	memset@PLT
	movq	-72(%rbp), %r9
	movq	-104(%rbp), %r8
	testq	%r9, %r9
	je	.L784
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r13d, %ecx
	shrq	%cl, %rax
	notq	%rax
	andq	%rax, (%r8)
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L778:
	cmpl	%edx, %r9d
	je	.L784
	movq	$-1, %rsi
	movl	$64, %ecx
	subl	%r9d, %ecx
	movq	%rsi, %rax
	shrq	%cl, %rax
	movl	%edx, %ecx
	salq	%cl, %rsi
	movq	(%r8), %rcx
	andq	%rsi, %rax
	movq	%rax, %rdx
	orq	%rcx, %rax
	notq	%rdx
	andq	%rcx, %rdx
	cmpb	$0, -72(%rbp)
	cmove	%rdx, %rax
	movq	%rax, (%r8)
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L820:
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r13d, %ecx
	shrq	%cl, %rax
	orq	%rax, (%r8)
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L780:
	notq	%rax
	andq	%rcx, %rax
	movq	%rax, (%rdi)
	movq	%rsi, %rdi
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L818:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L798:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.L771
.L817:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L766:
	movq	$2147483640, -88(%rbp)
	movl	$2147483640, %esi
	jmp	.L767
	.cfi_endproc
.LFE12391:
	.size	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb, .-_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb
	.section	.text._ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB12567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$6, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L838
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L826:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L827
	movq	%r15, %rbx
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L828:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L839
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L829:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L827
.L832:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L828
	cmpq	$63, 8(%rax)
	jbe	.L828
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L832
.L827:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$63, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,8), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L839:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L838:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L826
	.cfi_endproc
.LFE12567:
	.size	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	.type	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb, @function
_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb:
.LFB12591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	88(%rdi), %rax
	movq	56(%rdi), %rsi
	movq	24(%rdi), %rbx
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	1(%r14,%rcx), %r8
	leaq	(%r8,%r8), %rcx
	cmpq	%rcx, %rbx
	jbe	.L841
	subq	%r8, %rbx
	shrq	%rbx
	salq	$3, %rbx
	testb	%dl, %dl
	leaq	(%rbx,%r14,8), %rcx
	cmovne	%rcx, %rbx
	addq	$8, %rax
	addq	16(%rdi), %rbx
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	%rbx, %rsi
	jbe	.L843
	cmpq	%rax, %rsi
	je	.L844
	movq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L841:
	cmpq	%r14, %rbx
	movq	%r14, %rax
	movq	(%rdi), %rdi
	cmovnb	%rbx, %rax
	movq	16(%rdi), %r15
	leaq	2(%rbx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L851
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L846:
	movq	%rcx, %rax
	subq	%r8, %rax
	shrq	%rax
	salq	$3, %rax
	testb	%dl, %dl
	leaq	(%rax,%r14,8), %rsi
	cmovne	%rsi, %rax
	movq	56(%r12), %rsi
	leaq	(%r15,%rax), %rbx
	movq	88(%r12), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L848
	subq	%rsi, %rdx
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L848:
	movq	24(%r12), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L849
	movq	16(%r12), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L849:
	movq	%r15, 16(%r12)
	movq	%rcx, 24(%r12)
.L844:
	movq	%rbx, 56(%r12)
	movq	(%rbx), %rax
	movq	(%rbx), %xmm0
	addq	%r13, %rbx
	addq	$512, %rax
	movq	%rbx, 88(%r12)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%r12)
	movq	(%rbx), %rax
	movq	%rax, 72(%r12)
	addq	$512, %rax
	movq	%rax, 80(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L843:
	.cfi_restore_state
	cmpq	%rax, %rsi
	je	.L844
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L851:
	movl	%edx, -68(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	movl	-68(%rbp), %edx
	movq	%rax, %r15
	jmp	.L846
	.cfi_endproc
.LFE12591:
	.size	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb, .-_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	.section	.rodata._ZN2v88internal8compiler21LoopVariableOptimizer3RunEv.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZN2v88internal8compiler21LoopVariableOptimizer3RunEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21LoopVariableOptimizer3RunEv
	.type	_ZN2v88internal8compiler21LoopVariableOptimizer3RunEv, @function
_ZN2v88internal8compiler21LoopVariableOptimizer3RunEv:
.LFB10533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-256(%rbp), %r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	%r12, %rdi
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	%rax, -256(%rbp)
	movq	$0, -232(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	je	.L948
	movdqa	-256(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	leaq	-160(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-112(%rbp), %xmm5
	movq	-200(%rbp), %r10
	movq	-208(%rbp), %xmm0
	movq	-192(%rbp), %rcx
	movq	-184(%rbp), %r9
	movdqa	-96(%rbp), %xmm6
	movaps	%xmm5, -208(%rbp)
	movq	%r10, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movq	-216(%rbp), %r11
	movq	-176(%rbp), %rdx
	movq	-168(%rbp), %r8
	movdqa	-128(%rbp), %xmm4
	movaps	%xmm6, -192(%rbp)
	movq	%r9, %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	-248(%rbp), %rax
	movaps	%xmm0, -304(%rbp)
	movq	%rcx, %xmm0
	movq	-224(%rbp), %xmm1
	punpcklqdq	%xmm6, %xmm0
	movq	-256(%rbp), %xmm2
	movaps	%xmm4, -224(%rbp)
	movq	-240(%rbp), %r13
	movq	-232(%rbp), %rbx
	movaps	%xmm7, -176(%rbp)
	movq	%rax, %xmm3
	movq	-144(%rbp), %rsi
	movq	%r11, %xmm4
	punpcklqdq	%xmm3, %xmm2
	movq	-136(%rbp), %rdi
	movaps	%xmm0, -288(%rbp)
	movq	%r8, %xmm7
	movq	%rdx, %xmm0
	punpcklqdq	%xmm4, %xmm1
	punpcklqdq	%xmm7, %xmm0
	movq	%rsi, -240(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%r13, -336(%rbp)
	movq	%rbx, -328(%rbp)
	movaps	%xmm2, -352(%rbp)
	movaps	%xmm1, -320(%rbp)
	movaps	%xmm0, -272(%rbp)
	testq	%rsi, %rsi
	je	.L854
	movq	-168(%rbp), %rbx
	movq	-200(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L855
	.p2align 4,,10
	.p2align 3
.L858:
	testq	%rax, %rax
	je	.L856
	cmpq	$64, 8(%rax)
	ja	.L857
.L856:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -248(%rbp)
.L857:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L858
	movq	-232(%rbp), %rdi
	movq	-240(%rbp), %rsi
.L855:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L859
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L859:
	movq	-288(%rbp), %rcx
	movq	-272(%rbp), %rdx
.L854:
	movq	8(%r14), %rax
	subq	$8, %rdx
	movq	8(%rax), %rbx
	cmpq	%rdx, %rcx
	je	.L860
	movq	%rbx, (%rcx)
	addq	$8, -288(%rbp)
.L861:
	movq	8(%r14), %rsi
	movq	%r12, %rdi
	movl	$2, %edx
	movl	$1, %r12d
	call	_ZN2v88internal8compiler14NodeMarkerBaseC2EPNS1_5GraphEj@PLT
	leaq	-352(%rbp), %rax
	movq	%rax, -360(%rbp)
	.p2align 4,,10
	.p2align 3
.L877:
	movq	-320(%rbp), %rax
	cmpq	%rax, -288(%rbp)
	je	.L869
	movq	-304(%rbp), %rdi
	movq	(%rax), %rbx
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L870
	addq	$8, %rax
	movq	%rax, -320(%rbp)
.L871:
	movl	-256(%rbp), %eax
	movl	%eax, 16(%rbx)
	movq	(%rbx), %rax
	cmpw	$1, 16(%rax)
	je	.L949
	movl	28(%rax), %r13d
.L875:
	testl	%r13d, %r13d
	jle	.L876
	xorl	%r15d, %r15d
.L878:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	72(%r14), %rsi
	movl	96(%r14), %edi
	movl	20(%rax), %ecx
	movq	88(%r14), %rax
	movl	%ecx, %edx
	subq	%rsi, %rax
	andl	$16777215, %edx
	leaq	(%rdi,%rax,8), %rax
	cmpq	%rax, %rdx
	jnb	.L877
	movq	%r12, %rax
	shrq	$6, %rdx
	salq	%cl, %rax
	testq	%rax, (%rsi,%rdx,8)
	je	.L877
	addl	$1, %r15d
	cmpl	%r15d, %r13d
	jne	.L878
.L876:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21LoopVariableOptimizer9VisitNodeEPNS1_4NodeE
	movq	88(%r14), %r9
	movl	20(%rbx), %r13d
	movq	72(%r14), %rdx
	movl	96(%r14), %ecx
	movq	%r9, %rsi
	movl	%r13d, %eax
	subq	%rdx, %rsi
	andl	$16777215, %eax
	movq	%rcx, %rdi
	leaq	(%rcx,%rsi,8), %rsi
	cmpq	%rsi, %rax
	jnb	.L950
.L880:
	shrq	$6, %rax
	movl	%r13d, %ecx
	leaq	(%rdx,%rax,8), %rdx
	movq	%r12, %rax
	salq	%cl, %rax
	movq	(%rdx), %rcx
	testq	%rcx, %rax
	jne	.L881
	orq	%rcx, %rax
	movq	%rax, (%rdx)
.L881:
	movq	24(%rbx), %r13
	testq	%r13, %r13
	je	.L877
	movq	0(%r13), %r15
	.p2align 4,,10
	.p2align 3
.L898:
	movl	16(%r13), %ecx
	movq	%r13, %rdi
	movl	%ecx, %edx
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rax
	leaq	0(%r13,%rax,8), %rax
	leaq	32(%rax), %rsi
	addq	$16, %rax
	andl	$1, %ecx
	cmovne	%rsi, %rax
	leaq	(%rax,%rdx,8), %rsi
	call	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L885
	movl	16(%r13), %edx
	movl	%edx, %ecx
	shrl	%ecx
	andl	$1, %edx
	movl	%ecx, %eax
	leaq	3(%rax,%rax,2), %rax
	leaq	0(%r13,%rax,8), %r13
	movq	0(%r13), %rax
	jne	.L886
	movq	%rax, %r13
	movq	(%rax), %rax
.L886:
	movl	40(%rax), %edx
	testl	%edx, %edx
	jle	.L885
	cmpw	$1, 16(%rax)
	je	.L951
.L887:
	movl	16(%r13), %eax
	cmpl	%eax, -256(%rbp)
	jb	.L885
	movq	-272(%rbp), %rax
	movq	-288(%rbp), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L890
	movq	%r13, (%rdx)
	addq	$8, -288(%rbp)
.L891:
	movl	-256(%rbp), %eax
	addl	$1, %eax
	movl	%eax, 16(%r13)
	.p2align 4,,10
	.p2align 3
.L885:
	testq	%r15, %r15
	je	.L877
	movq	%r15, %r13
	movq	(%r15), %r15
	jmp	.L898
.L950:
	leaq	1(%rax), %rcx
	cmpq	%rcx, %rsi
	jbe	.L879
	movq	%rcx, %rsi
	andl	$63, %ecx
	sarq	$6, %rsi
	movl	%ecx, 96(%r14)
	leaq	(%rdx,%rsi,8), %rsi
	movq	%rsi, 88(%r14)
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L949:
	movl	4(%r14), %r13d
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L951:
	cmpl	%ecx, (%r14)
	je	.L887
	cmpl	$2, 28(%rax)
	jne	.L885
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21LoopVariableOptimizer13VisitBackedgeEPNS1_4NodeES4_.part.0
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L870:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L872
	cmpq	$64, 8(%rax)
	ja	.L873
.L872:
	movq	-312(%rbp), %rax
	movq	$64, 8(%rax)
	movq	-344(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -344(%rbp)
.L873:
	movq	-296(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -296(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -312(%rbp)
	movq	%rdx, -304(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L869:
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L852
	movq	-264(%rbp), %rbx
	movq	-296(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L901
	movq	-344(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L904:
	testq	%rax, %rax
	je	.L902
	cmpq	$64, 8(%rax)
	ja	.L903
.L902:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-344(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -344(%rbp)
.L903:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L904
	movq	-336(%rbp), %rax
.L901:
	movq	-328(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L852
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L852:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L952
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L879:
	.cfi_restore_state
	movl	%edi, -152(%rbp)
	subq	%rsi, %rcx
	leaq	64(%r14), %rdi
	movq	-152(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r9, %rsi
	movq	%rax, -368(%rbp)
	movq	%r9, -160(%rbp)
	call	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb
	movq	72(%r14), %rdx
	movq	-368(%rbp), %rax
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L890:
	movq	-264(%rbp), %rcx
	subq	-280(%rbp), %rdx
	sarq	$3, %rdx
	movq	%rcx, %rax
	subq	-296(%rbp), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$6, %rax
	addq	%rdx, %rax
	movq	-304(%rbp), %rdx
	subq	-320(%rbp), %rdx
	sarq	$3, %rdx
	addq	%rax, %rdx
	cmpq	$268435455, %rdx
	je	.L892
	movq	-328(%rbp), %rdi
	movq	%rcx, %rax
	subq	-336(%rbp), %rax
	sarq	$3, %rax
	subq	%rax, %rdi
	cmpq	$1, %rdi
	jbe	.L953
.L893:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L894
	cmpq	$63, 8(%rax)
	ja	.L954
.L894:
	movq	-352(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L955
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L895:
	movq	%rax, 8(%rcx)
	movq	-288(%rbp), %rax
	movq	%r13, (%rax)
	movq	-264(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -264(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -280(%rbp)
	movq	%rdx, -272(%rbp)
	movq	%rax, -288(%rbp)
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L948:
	movdqa	-256(%rbp), %xmm1
	movq	-232(%rbp), %rax
	movq	$0, -336(%rbp)
	movdqa	-224(%rbp), %xmm3
	movdqa	-208(%rbp), %xmm4
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm6
	movq	%rax, -328(%rbp)
	movq	-192(%rbp), %rcx
	movq	-176(%rbp), %rdx
	movaps	%xmm1, -352(%rbp)
	movaps	%xmm3, -320(%rbp)
	movaps	%xmm4, -304(%rbp)
	movaps	%xmm5, -288(%rbp)
	movaps	%xmm6, -272(%rbp)
	jmp	.L854
.L860:
	movq	-264(%rbp), %r13
	subq	-280(%rbp), %rcx
	sarq	$3, %rcx
	movq	%r13, %rax
	subq	-296(%rbp), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$6, %rax
	addq	%rax, %rcx
	movq	-304(%rbp), %rax
	subq	-320(%rbp), %rax
	sarq	$3, %rax
	addq	%rcx, %rax
	cmpq	$268435455, %rax
	je	.L892
	movq	-328(%rbp), %rdi
	movq	%r13, %rax
	subq	-336(%rbp), %rax
	sarq	$3, %rax
	subq	%rax, %rdi
	cmpq	$1, %rdi
	jbe	.L956
.L863:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L864
	cmpq	$63, 8(%rax)
	ja	.L957
.L864:
	movq	-352(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L958
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L865:
	movq	%rax, 8(%r13)
	movq	-288(%rbp), %rax
	movq	%rbx, (%rax)
	movq	-264(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -264(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -280(%rbp)
	movq	%rdx, -272(%rbp)
	movq	%rax, -288(%rbp)
	jmp	.L861
.L953:
	movq	-360(%rbp), %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	movq	-264(%rbp), %rcx
	jmp	.L893
.L954:
	movq	(%rax), %rdx
	movq	%rdx, -344(%rbp)
	jmp	.L895
.L956:
	leaq	-352(%rbp), %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	movq	-264(%rbp), %r13
	jmp	.L863
.L957:
	movq	(%rax), %rdx
	movq	%rdx, -344(%rbp)
	jmp	.L865
.L955:
	movl	$512, %esi
	movq	%rcx, -368(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-368(%rbp), %rcx
	jmp	.L895
.L958:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L865
.L952:
	call	__stack_chk_fail@PLT
.L892:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10533:
	.size	_ZN2v88internal8compiler21LoopVariableOptimizer3RunEv, .-_ZN2v88internal8compiler21LoopVariableOptimizer3RunEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler21LoopVariableOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler21LoopVariableOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler21LoopVariableOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE:
.LFB12845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12845:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler21LoopVariableOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler21LoopVariableOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler21LoopVariableOptimizerC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
