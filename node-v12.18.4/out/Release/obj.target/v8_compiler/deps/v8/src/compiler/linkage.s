	.file	"linkage.cc"
	.text
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptor4KindE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Code"
.LC1:
	.string	"JS"
.LC2:
	.string	"Addr"
.LC3:
	.string	"WasmExit"
.LC4:
	.string	"WasmFunction"
.LC5:
	.string	"WasmImportWrapper"
.LC6:
	.string	"BuiltinPointer"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptor4KindE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptor4KindE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptor4KindE, @function
_ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptor4KindE:
.LFB22094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$6, (%rsi)
	ja	.L2
	movl	(%rsi), %eax
	leaq	.L4(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptor4KindE,"a",@progbits
	.align 4
	.align 4
.L4:
	.long	.L10-.L4
	.long	.L9-.L4
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptor4KindE
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$14, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	$17, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movl	$2, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	$8, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$12, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22094:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptor4KindE, .-_ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptor4KindE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptorE.str1.1,"aMS",@progbits,1
.LC7:
	.string	":"
.LC8:
	.string	":r"
.LC9:
	.string	"s"
.LC10:
	.string	"i"
.LC11:
	.string	"f"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptorE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptorE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptorE, @function
_ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptorE:
.LFB22095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	8(%rsi), %eax
	leaq	-44(%rbp), %rsi
	movl	%eax, -44(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptor4KindE
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	72(%rbx), %r13
	testq	%r13, %r13
	je	.L18
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L15:
	movl	$2, %edx
	movq	%r12, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	24(%rbx), %rax
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	24(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	addq	$1, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	64(%rbx), %esi
	movq	%r12, %rdi
	andl	$1, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L19
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L15
.L19:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22095:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptorE, .-_ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptorE
	.section	.text._ZNK2v88internal8compiler14CallDescriptor19GetMachineSignatureEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler14CallDescriptor19GetMachineSignatureEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler14CallDescriptor19GetMachineSignatureEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler14CallDescriptor19GetMachineSignatureEPNS0_4ZoneE:
.LFB22096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rax
	movq	16(%r15), %r14
	movq	8(%rax), %r12
	movq	(%rax), %r13
	leaq	0(%r13,%r12), %rax
	leaq	7(%rax,%rax), %rsi
	movq	24(%r15), %rax
	andq	$-8, %rsi
	subq	%r14, %rax
	cmpq	%rax, %rsi
	ja	.L38
	addq	%r14, %rsi
	movq	%rsi, 16(%r15)
.L22:
	testq	%r13, %r13
	je	.L39
	leaq	(%r14,%r13,2), %rsi
	movq	%r14, %rax
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L26:
	movq	24(%rbx), %rcx
	addq	$2, %rax
	movq	16(%rcx), %rcx
	movzwl	4(%rcx,%rdx), %ecx
	addq	$8, %rdx
	movw	%cx, -2(%rax)
	cmpq	%rax, %rsi
	jne	.L26
	movslq	%r13d, %rax
.L27:
	leaq	(%r14,%rax,2), %rdx
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L24
	.p2align 4,,10
	.p2align 3
.L28:
	movq	24(%rbx), %rcx
	addq	$2, %rdx
	movq	(%rcx), %rsi
	movq	16(%rcx), %rcx
	addq	%rax, %rsi
	addq	$1, %rax
	movzwl	4(%rcx,%rsi,8), %ecx
	movw	%cx, -2(%rdx)
	cmpq	%r12, %rax
	jne	.L28
.L24:
	movq	16(%r15), %rax
	movq	24(%r15), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L40
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%r15)
.L30:
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	movq	%r14, 16(%rax)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L22
	.cfi_endproc
.LFE22096:
	.size	_ZNK2v88internal8compiler14CallDescriptor19GetMachineSignatureEPNS0_4ZoneE, .-_ZNK2v88internal8compiler14CallDescriptor19GetMachineSignatureEPNS0_4ZoneE
	.section	.rodata._ZNK2v88internal8compiler14CallDescriptor23GetFirstUnusedStackSlotEv.str1.1,"aMS",@progbits,1
.LC12:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal8compiler14CallDescriptor23GetFirstUnusedStackSlotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler14CallDescriptor23GetFirstUnusedStackSlotEv
	.type	_ZNK2v88internal8compiler14CallDescriptor23GetFirstUnusedStackSlotEv, @function
_ZNK2v88internal8compiler14CallDescriptor23GetFirstUnusedStackSlotEv:
.LFB22097:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rsi
	xorl	%r8d, %r8d
	movq	8(%rsi), %r9
	cmpq	$-1, %r9
	je	.L41
	movl	16(%rdi), %r11d
	movzbl	20(%rdi), %edx
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	leaq	CSWTCH.53(%rip), %r10
	movl	$1, %edi
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L46:
	movq	16(%rsi), %rdx
	addq	(%rsi), %rax
	leaq	(%rdx,%rax,8), %rax
	movl	(%rax), %r11d
	movzbl	4(%rax), %edx
	movq	%rcx, %rax
.L47:
	testb	$1, %r11b
	je	.L44
	subl	$1, %edx
	cmpb	$13, %dl
	ja	.L45
	movzbl	%dl, %edx
	movl	(%r10,%rdx,4), %ecx
	movl	%edi, %edx
	sall	%cl, %edx
	movl	%edx, %ecx
	leal	14(%rdx), %edx
	addl	$7, %ecx
	cmovns	%ecx, %edx
	sarl	%r11d
	sarl	$3, %edx
	subl	%r11d, %edx
	subl	$1, %edx
	cmpl	%edx, %r8d
	cmovl	%edx, %r8d
.L44:
	leaq	1(%rax), %rcx
	cmpq	%rax, %r9
	jne	.L46
.L41:
	movl	%r8d, %eax
	ret
.L45:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22097:
	.size	_ZNK2v88internal8compiler14CallDescriptor23GetFirstUnusedStackSlotEv, .-_ZNK2v88internal8compiler14CallDescriptor23GetFirstUnusedStackSlotEv
	.section	.text._ZNK2v88internal8compiler14CallDescriptor22GetStackParameterDeltaEPKS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler14CallDescriptor22GetStackParameterDeltaEPKS2_
	.type	_ZNK2v88internal8compiler14CallDescriptor22GetStackParameterDeltaEPKS2_, @function
_ZNK2v88internal8compiler14CallDescriptor22GetStackParameterDeltaEPKS2_:
.LFB22098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	call	_ZNK2v88internal8compiler14CallDescriptor23GetFirstUnusedStackSlotEv
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal8compiler14CallDescriptor23GetFirstUnusedStackSlotEv
	movl	%ebx, %r12d
	subl	%eax, %r12d
	movl	%r12d, %edi
	call	_ZN2v88internal18ShouldPadArgumentsEi@PLT
	testb	%al, %al
	je	.L50
	leal	1(%r12), %eax
	subl	$1, %r12d
	andl	$1, %ebx
	cmovne	%eax, %r12d
.L50:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22098:
	.size	_ZNK2v88internal8compiler14CallDescriptor22GetStackParameterDeltaEPKS2_, .-_ZNK2v88internal8compiler14CallDescriptor22GetStackParameterDeltaEPKS2_
	.section	.text._ZNK2v88internal8compiler14CallDescriptor23GetTaggedParameterSlotsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler14CallDescriptor23GetTaggedParameterSlotsEv
	.type	_ZNK2v88internal8compiler14CallDescriptor23GetTaggedParameterSlotsEv, @function
_ZNK2v88internal8compiler14CallDescriptor23GetTaggedParameterSlotsEv:
.LFB22099:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rcx
	xorl	%r9d, %r9d
	movq	8(%rcx), %r8
	cmpq	$-1, %r8
	je	.L57
	movl	16(%rdi), %esi
	movzbl	20(%rdi), %edx
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L61:
	movq	16(%rcx), %rdx
	addq	(%rcx), %rax
	leaq	(%rdx,%rax,8), %rax
	movl	(%rax), %esi
	movzbl	4(%rax), %edx
	movq	%rdi, %rax
.L62:
	andl	$1, %esi
	je	.L60
	subl	$6, %edx
	cmpb	$3, %dl
	adcl	$0, %r9d
.L60:
	leaq	1(%rax), %rdi
	cmpq	%rax, %r8
	jne	.L61
.L57:
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE22099:
	.size	_ZNK2v88internal8compiler14CallDescriptor23GetTaggedParameterSlotsEv, .-_ZNK2v88internal8compiler14CallDescriptor23GetTaggedParameterSlotsEv
	.section	.text._ZNK2v88internal8compiler14CallDescriptor11CanTailCallEPKS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler14CallDescriptor11CanTailCallEPKS2_
	.type	_ZNK2v88internal8compiler14CallDescriptor11CanTailCallEPKS2_, @function
_ZNK2v88internal8compiler14CallDescriptor11CanTailCallEPKS2_:
.LFB22100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rdx
	movq	24(%r13), %rcx
	movq	(%rdx), %rsi
	cmpq	%rsi, (%rcx)
	jne	.L63
	movq	%rdi, %r12
	xorl	%ebx, %ebx
	testq	%rsi, %rsi
	je	.L69
	.p2align 4,,10
	.p2align 3
.L65:
	movq	16(%rcx), %rdi
	leaq	0(,%rbx,8), %rax
	addq	%rax, %rdi
	addq	16(%rdx), %rax
	movl	(%rdi), %esi
	cmpl	%esi, (%rax)
	je	.L66
.L68:
	xorl	%eax, %eax
.L63:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movzbl	4(%rdi), %r14d
	movzbl	4(%rax), %r15d
	movl	%r14d, %esi
	movl	%r15d, %edi
	call	_ZN2v88internal9IsSubtypeENS0_21MachineRepresentationES1_@PLT
	testb	%al, %al
	je	.L77
.L67:
	movq	24(%r12), %rdx
	addq	$1, %rbx
	cmpq	(%rdx), %rbx
	jnb	.L69
	movq	24(%r13), %rcx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L69:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movl	%r15d, %esi
	movl	%r14d, %edi
	call	_ZN2v88internal9IsSubtypeENS0_21MachineRepresentationES1_@PLT
	testb	%al, %al
	je	.L68
	jmp	.L67
	.cfi_endproc
.LFE22100:
	.size	_ZNK2v88internal8compiler14CallDescriptor11CanTailCallEPKS2_, .-_ZNK2v88internal8compiler14CallDescriptor11CanTailCallEPKS2_
	.section	.text._ZNK2v88internal8compiler14CallDescriptor23CalculateFixedFrameSizeENS0_4Code4KindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler14CallDescriptor23CalculateFixedFrameSizeENS0_4Code4KindE
	.type	_ZNK2v88internal8compiler14CallDescriptor23CalculateFixedFrameSizeENS0_4Code4KindE, @function
_ZNK2v88internal8compiler14CallDescriptor23CalculateFixedFrameSizeENS0_4Code4KindE:
.LFB22101:
	.cfi_startproc
	endbr64
	cmpl	$6, 8(%rdi)
	ja	.L79
	movl	8(%rdi), %eax
	leaq	.L81(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler14CallDescriptor23CalculateFixedFrameSizeENS0_4Code4KindE,"a",@progbits
	.align 4
	.align 4
.L81:
	.long	.L80-.L81
	.long	.L85-.L81
	.long	.L84-.L81
	.long	.L86-.L81
	.long	.L82-.L81
	.long	.L82-.L81
	.long	.L80-.L81
	.section	.text._ZNK2v88internal8compiler14CallDescriptor23CalculateFixedFrameSizeENS0_4Code4KindE
	.p2align 4,,10
	.p2align 3
.L82:
	movl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	xorl	%eax, %eax
	cmpl	$11, %esi
	sete	%al
	leal	2(%rax,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	xorl	%eax, %eax
	testb	$32, 64(%rdi)
	setne	%al
	addl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$5, %eax
	ret
.L79:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22101:
	.size	_ZNK2v88internal8compiler14CallDescriptor23CalculateFixedFrameSizeENS0_4Code4KindE, .-_ZNK2v88internal8compiler14CallDescriptor23CalculateFixedFrameSizeENS0_4Code4KindE
	.section	.text._ZN2v88internal8compiler7Linkage20NeedsFrameStateInputENS0_7Runtime10FunctionIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7Linkage20NeedsFrameStateInputENS0_7Runtime10FunctionIdE
	.type	_ZN2v88internal8compiler7Linkage20NeedsFrameStateInputENS0_7Runtime10FunctionIdE, @function
_ZN2v88internal8compiler7Linkage20NeedsFrameStateInputENS0_7Runtime10FunctionIdE:
.LFB22103:
	.cfi_startproc
	endbr64
	cmpl	$348, %edi
	jg	.L92
	cmpl	$305, %edi
	jg	.L93
	cmpl	$156, %edi
	jg	.L94
	cmpl	$98, %edi
	jg	.L95
	cmpl	$90, %edi
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	subl	$430, %edi
	movl	$1, %eax
	cmpl	$65, %edi
	ja	.L110
	leaq	.L99(%rip), %rdx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler7Linkage20NeedsFrameStateInputENS0_7Runtime10FunctionIdE,"a",@progbits
	.align 4
	.align 4
.L99:
	.long	.L106-.L99
	.long	.L106-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L106-.L99
	.long	.L106-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L106-.L99
	.long	.L106-.L99
	.long	.L106-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L106-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L106-.L99
	.long	.L104-.L99
	.long	.L106-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L104-.L99
	.long	.L106-.L99
	.section	.text._ZN2v88internal8compiler7Linkage20NeedsFrameStateInputENS0_7Runtime10FunctionIdE
.L106:
	xorl	%eax, %eax
	ret
.L104:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	leal	-99(%rdi), %ecx
	movl	$1, %eax
	movabsq	$144115737831669761, %rdx
	salq	%cl, %rax
	testq	%rdx, %rax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	cmpl	$213, %edi
	je	.L106
	cmpl	$255, %edi
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	subl	$306, %edi
	cmpl	$42, %edi
	ja	.L104
	leaq	.L101(%rip), %rdx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler7Linkage20NeedsFrameStateInputENS0_7Runtime10FunctionIdE
	.align 4
	.align 4
.L101:
	.long	.L106-.L101
	.long	.L106-.L101
	.long	.L106-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L106-.L101
	.long	.L106-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L106-.L101
	.long	.L104-.L101
	.long	.L106-.L101
	.long	.L106-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L106-.L101
	.long	.L106-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L104-.L101
	.long	.L106-.L101
	.section	.text._ZN2v88internal8compiler7Linkage20NeedsFrameStateInputENS0_7Runtime10FunctionIdE
	.p2align 4,,10
	.p2align 3
.L110:
	ret
	.cfi_endproc
.LFE22103:
	.size	_ZN2v88internal8compiler7Linkage20NeedsFrameStateInputENS0_7Runtime10FunctionIdE, .-_ZN2v88internal8compiler7Linkage20NeedsFrameStateInputENS0_7Runtime10FunctionIdE
	.section	.text._ZNK2v88internal8compiler14CallDescriptor17UsesOnlyRegistersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler14CallDescriptor17UsesOnlyRegistersEv
	.type	_ZNK2v88internal8compiler14CallDescriptor17UsesOnlyRegistersEv, @function
_ZNK2v88internal8compiler14CallDescriptor17UsesOnlyRegistersEv:
.LFB22104:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rsi
	movq	8(%rsi), %r8
	cmpq	$-1, %r8
	je	.L126
	movl	16(%rdi), %eax
	xorl	%edx, %edx
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L127:
	movq	(%rsi), %rcx
	leaq	1(%rdx), %rdi
	cmpq	%r8, %rdx
	je	.L116
	movq	16(%rsi), %rax
	addq	%rcx, %rdx
	movl	(%rax,%rdx,8), %eax
	movq	%rdi, %rdx
.L118:
	notl	%eax
	andl	$1, %eax
	jne	.L127
.L111:
	ret
.L126:
	movq	(%rsi), %rcx
	.p2align 4,,10
	.p2align 3
.L116:
	testq	%rcx, %rcx
	je	.L128
	movq	16(%rsi), %rsi
	xorl	%edx, %edx
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L130:
	addq	$1, %rdx
	cmpq	%rcx, %rdx
	je	.L129
.L117:
	movl	(%rsi,%rdx,8), %eax
	notl	%eax
	andl	$1, %eax
	jne	.L130
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L129:
	ret
.L128:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE22104:
	.size	_ZNK2v88internal8compiler14CallDescriptor17UsesOnlyRegistersEv, .-_ZNK2v88internal8compiler14CallDescriptor17UsesOnlyRegistersEv
	.section	.text._ZN2v88internal8compiler7Linkage27GetCEntryStubCallDescriptorEPNS0_4ZoneEiiPKcNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7Linkage27GetCEntryStubCallDescriptorEPNS0_4ZoneEiiPKcNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE
	.type	_ZN2v88internal8compiler7Linkage27GetCEntryStubCallDescriptorEPNS0_4ZoneEiiPKcNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE, @function
_ZN2v88internal8compiler7Linkage27GetCEntryStubCallDescriptorEPNS0_4ZoneEiiPKcNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE:
.LFB22106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	leaq	3(%rdx), %rcx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movslq	%esi, %r13
	addl	%ecx, %esi
	pushq	%r12
	movslq	%esi, %rsi
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	salq	$3, %rsi
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	%rax, %rsi
	ja	.L149
	addq	%rbx, %rsi
	movq	%rsi, 16(%rdi)
.L133:
	testq	%r13, %r13
	je	.L135
	movl	$1800, %eax
	movl	$0, (%rbx)
	movw	%ax, 4(%rbx)
	cmpq	$1, %r13
	jbe	.L135
	movl	$1800, %r11d
	movl	$4, 8(%rbx)
	movw	%r11w, 12(%rbx)
	cmpq	$2, %r13
	je	.L135
	movl	$1800, %r10d
	movl	$16, 16(%rbx)
	movw	%r10w, 20(%rbx)
.L135:
	testl	%r14d, %r14d
	jle	.L143
	movl	%r14d, %eax
	leaq	(%rbx,%r13,8), %rsi
	negl	%eax
	addl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L138:
	movl	%eax, %edi
	addq	$8, %rsi
	orl	$1, %edi
	movl	%edi, -8(%rsi)
	movl	$1800, %edi
	movw	%di, -4(%rsi)
	addl	$2, %eax
	jne	.L138
	leal	-1(%r14), %eax
	addq	$1, %rax
	leaq	0(%r13,%rax), %rsi
.L137:
	leaq	(%rbx,%rsi,8), %rsi
	movl	$5, %edi
	movl	$516, %r10d
	movl	$1800, %r11d
	movl	$6, (%rsi)
	movw	%di, 4(%rsi)
	leaq	1(%r13,%rax), %rsi
	leaq	2(%r13,%rax), %rax
	leaq	(%rbx,%rax,8), %rax
	leaq	(%rbx,%rsi,8), %rsi
	movl	$0, (%rsi)
	movw	%r10w, 4(%rsi)
	movl	$12, (%rax)
	movw	%r11w, 4(%rax)
	movq	16(%r12), %r14
	movq	24(%r12), %rax
	subq	%r14, %rax
	cmpq	$23, %rax
	jbe	.L150
	leaq	24(%r14), %rax
	movq	%rax, 16(%r12)
.L140:
	movq	%rcx, %xmm1
	movq	%r13, %xmm0
	movq	%rbx, 16(%r14)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r14)
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	subq	%rax, %rcx
	cmpq	$79, %rcx
	jbe	.L151
	leaq	80(%rax), %rcx
	movq	%rcx, 16(%r12)
.L142:
	movl	$1800, %ecx
	movl	$1800, %esi
	movb	$1, 4(%rax)
	movl	$1, (%rax)
	movl	$0, 8(%rax)
	movw	%cx, 12(%rax)
	movl	$-2, 16(%rax)
	movw	%si, 20(%rax)
	movq	%r14, 24(%rax)
	movq	%rdx, 32(%rax)
	movq	$0, 40(%rax)
	movb	%r8b, 48(%rax)
	movl	$0, 52(%rax)
	movq	$0, 56(%rax)
	movl	%r9d, 64(%rax)
	movq	%r15, 72(%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	%r13, %rsi
	xorl	%eax, %eax
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L150:
	movl	$24, %esi
	movq	%r12, %rdi
	movl	%r9d, -72(%rbp)
	movb	%r8b, -68(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movzbl	-68(%rbp), %r8d
	movl	-72(%rbp), %r9d
	movq	%rax, %r14
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L151:
	movl	$80, %esi
	movq	%r12, %rdi
	movl	%r9d, -68(%rbp)
	movb	%r8b, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movzbl	-64(%rbp), %r8d
	movl	-68(%rbp), %r9d
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L149:
	movl	%r9d, -72(%rbp)
	movb	%r8b, -68(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movzbl	-68(%rbp), %r8d
	movl	-72(%rbp), %r9d
	movq	%rax, %rbx
	jmp	.L133
	.cfi_endproc
.LFE22106:
	.size	_ZN2v88internal8compiler7Linkage27GetCEntryStubCallDescriptorEPNS0_4ZoneEiiPKcNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE, .-_ZN2v88internal8compiler7Linkage27GetCEntryStubCallDescriptorEPNS0_4ZoneEiiPKcNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE
	.section	.text._ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE
	.type	_ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE, @function
_ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE:
.LFB22105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	%esi, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	movsbl	25(%rax), %esi
	movq	8(%rax), %rcx
	cmpl	$348, %ebx
	jg	.L153
	cmpl	$305, %ebx
	jg	.L154
	cmpl	$156, %ebx
	jg	.L155
	cmpl	$98, %ebx
	jg	.L156
	cmpl	$90, %ebx
	je	.L157
.L158:
	addq	$8, %rsp
	movl	%r12d, %r9d
	movl	%r15d, %r8d
	movl	%r14d, %edx
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler7Linkage27GetCEntryStubCallDescriptorEPNS0_4ZoneEiiPKcNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	subl	$430, %ebx
	cmpl	$65, %ebx
	ja	.L158
	leaq	.L159(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE,"a",@progbits
	.align 4
	.align 4
.L159:
	.long	.L157-.L159
	.long	.L157-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L157-.L159
	.long	.L157-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L157-.L159
	.long	.L157-.L159
	.long	.L157-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L157-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L157-.L159
	.long	.L158-.L159
	.long	.L157-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L158-.L159
	.long	.L157-.L159
	.section	.text._ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE
	.p2align 4,,10
	.p2align 3
.L155:
	cmpl	$213, %ebx
	je	.L157
	cmpl	$255, %ebx
	jne	.L158
.L157:
	andl	$-2, %r12d
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L156:
	movabsq	$144115737831669761, %rax
	subl	$99, %ebx
	btq	%rbx, %rax
	jnc	.L158
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L154:
	subl	$306, %ebx
	cmpl	$42, %ebx
	ja	.L158
	leaq	.L160(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE
	.align 4
	.align 4
.L160:
	.long	.L157-.L160
	.long	.L157-.L160
	.long	.L157-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L157-.L160
	.long	.L157-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L157-.L160
	.long	.L158-.L160
	.long	.L157-.L160
	.long	.L157-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L157-.L160
	.long	.L157-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L158-.L160
	.long	.L157-.L160
	.section	.text._ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE
	.cfi_endproc
.LFE22105:
	.size	_ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE, .-_ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE
	.section	.rodata._ZN2v88internal8compiler7Linkage19GetJSCallDescriptorEPNS0_4ZoneEbiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"js-call"
	.section	.text._ZN2v88internal8compiler7Linkage19GetJSCallDescriptorEPNS0_4ZoneEbiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7Linkage19GetJSCallDescriptorEPNS0_4ZoneEbiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	.type	_ZN2v88internal8compiler7Linkage19GetJSCallDescriptorEPNS0_4ZoneEbiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEE, @function
_ZN2v88internal8compiler7Linkage19GetJSCallDescriptorEPNS0_4ZoneEbiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEE:
.LFB22107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movslq	%edx, %r14
	pushq	%r13
	leal	4(%r14), %esi
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	leaq	3(%r14), %rcx
	pushq	%r12
	movslq	%esi, %rsi
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	salq	$3, %rsi
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	%rax, %rsi
	ja	.L187
	addq	%rbx, %rsi
	movq	%rsi, 16(%rdi)
.L175:
	movl	$1800, %r11d
	movl	$0, (%rbx)
	movw	%r11w, 4(%rbx)
	testl	%edx, %edx
	jle	.L183
	movl	%edx, %eax
	leaq	8(%rbx), %rsi
	negl	%eax
	addl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L177:
	movl	%eax, %r8d
	movl	$1800, %r10d
	addq	$8, %rsi
	orl	$1, %r8d
	movw	%r10w, -4(%rsi)
	movl	%r8d, -8(%rsi)
	addl	$2, %eax
	jne	.L177
	leal	-1(%rdx), %eax
	leaq	16(,%rax,8), %rax
	leaq	16(%rax), %rdx
	leaq	8(%rax), %rsi
.L176:
	addq	%rbx, %rax
	movl	$1800, %edi
	movl	$516, %r8d
	movl	$1800, %r9d
	movl	$4, (%rax)
	cmpb	$1, %r15b
	movw	%di, 4(%rax)
	leaq	(%rbx,%rsi), %rax
	movl	$0, (%rax)
	movw	%r8w, 4(%rax)
	leaq	(%rbx,%rdx), %rax
	sbbl	%edx, %edx
	movl	$12, (%rax)
	andl	$7, %edx
	movw	%r9w, 4(%rax)
	movq	16(%r12), %r15
	addl	$7, %edx
	movq	24(%r12), %rax
	subq	%r15, %rax
	cmpq	$23, %rax
	jbe	.L188
	leaq	24(%r15), %rax
	movq	%rax, 16(%r12)
.L180:
	movq	$1, (%r15)
	movq	%rcx, 8(%r15)
	movq	%rbx, 16(%r15)
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	subq	%rax, %rcx
	cmpq	$79, %rcx
	jbe	.L189
	leaq	80(%rax), %rcx
	movq	%rcx, 16(%r12)
.L182:
	movl	$1800, %ecx
	movl	$1800, %esi
	movl	$1, (%rax)
	leaq	.LC13(%rip), %rdi
	movb	$1, 4(%rax)
	movl	$1, 8(%rax)
	movw	%cx, 12(%rax)
	movl	%edx, 16(%rax)
	movw	%si, 20(%rax)
	movq	%r15, 24(%rax)
	movq	%r14, 32(%rax)
	movq	$0, 40(%rax)
	movb	$0, 48(%rax)
	movl	$0, 52(%rax)
	movq	$0, 56(%rax)
	movl	%r13d, 64(%rax)
	movq	%rdi, 72(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movl	$8, %eax
	movl	$16, %esi
	movl	$24, %edx
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$24, %esi
	movq	%r12, %rdi
	movq	%rcx, -64(%rbp)
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %edx
	movq	-64(%rbp), %rcx
	movq	%rax, %r15
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$80, %esi
	movq	%r12, %rdi
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %edx
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L187:
	movl	%edx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movl	-64(%rbp), %edx
	movq	%rax, %rbx
	jmp	.L175
	.cfi_endproc
.LFE22107:
	.size	_ZN2v88internal8compiler7Linkage19GetJSCallDescriptorEPNS0_4ZoneEbiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEE, .-_ZN2v88internal8compiler7Linkage19GetJSCallDescriptorEPNS0_4ZoneEbiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	.section	.text._ZN2v88internal8compiler7Linkage15ComputeIncomingEPNS0_4ZoneEPNS0_24OptimizedCompilationInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7Linkage15ComputeIncomingEPNS0_4ZoneEPNS0_24OptimizedCompilationInfoE
	.type	_ZN2v88internal8compiler7Linkage15ComputeIncomingEPNS0_4ZoneEPNS0_24OptimizedCompilationInfoE, @function
_ZN2v88internal8compiler7Linkage15ComputeIncomingEPNS0_4ZoneEPNS0_24OptimizedCompilationInfoE:
.LFB22102:
	.cfi_startproc
	endbr64
	movq	32(%rsi), %rax
	testq	%rax, %rax
	je	.L190
	movq	(%rax), %rax
	movl	$4, %ecx
	movq	23(%rax), %rax
	movzwl	41(%rax), %edx
	addl	$1, %edx
	cmpl	$-1, 56(%rsi)
	setne	%sil
	movzbl	%sil, %esi
	jmp	_ZN2v88internal8compiler7Linkage19GetJSCallDescriptorEPNS0_4ZoneEbiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	.p2align 4,,10
	.p2align 3
.L190:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22102:
	.size	_ZN2v88internal8compiler7Linkage15ComputeIncomingEPNS0_4ZoneEPNS0_24OptimizedCompilationInfoE, .-_ZN2v88internal8compiler7Linkage15ComputeIncomingEPNS0_4ZoneEPNS0_24OptimizedCompilationInfoE
	.section	.text._ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE
	.type	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE, @function
_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE:
.LFB22108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	8(%rsi), %rax
	movq	16(%rdi), %r15
	movb	%r8b, -61(%rbp)
	movl	%ecx, -60(%rbp)
	movl	12(%rax), %edx
	movl	(%rax), %r8d
	movl	%r9d, -68(%rbp)
	notl	%edx
	leal	(%r14,%r8), %r12d
	andl	$1, %edx
	leal	(%rdx,%r12), %ecx
	movl	%edx, %r10d
	movslq	%ecx, %rdx
	movq	%rdx, -56(%rbp)
	movslq	4(%rax), %rdx
	movq	24(%rdi), %rax
	movq	%rdx, %rsi
	addl	%ecx, %esi
	subq	%r15, %rax
	movslq	%esi, %rsi
	salq	$3, %rsi
	cmpq	%rax, %rsi
	ja	.L223
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L194:
	testq	%rdx, %rdx
	je	.L196
	movq	8(%rbx), %rax
	movq	32(%rax), %rax
	movzwl	(%rax), %eax
	movl	$0, (%r15)
	movw	%ax, 4(%r15)
	cmpq	$1, %rdx
	jbe	.L196
	movq	8(%rbx), %rax
	movq	32(%rax), %rax
	movzwl	2(%rax), %eax
	movl	$4, 8(%r15)
	movw	%ax, 12(%r15)
	cmpq	$2, %rdx
	je	.L196
	movq	8(%rbx), %rax
	movq	32(%rax), %rax
	movzwl	4(%rax), %eax
	movl	$16, 16(%r15)
	movw	%ax, 20(%r15)
.L196:
	testl	%r12d, %r12d
	jle	.L210
	testl	%r8d, %r8d
	jle	.L211
	movq	%rdx, -80(%rbp)
	cmpl	%r8d, %r12d
	movl	%r8d, %r11d
	leaq	(%r15,%rdx,8), %rsi
	cmovle	%r12d, %r11d
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L200:
	movq	8(%rbx), %rdi
	movl	%ecx, %eax
	addq	$1, %rcx
	addq	$8, %rsi
	movq	32(%rdi), %rdx
	addl	4(%rdi), %eax
	movq	24(%rdi), %rdi
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	(%rdi,%r9), %edi
	addq	$4, %r9
	movw	%ax, -4(%rsi)
	movl	%ecx, %eax
	addl	%edi, %edi
	movl	%edi, -8(%rsi)
	cmpl	%ecx, %r11d
	jg	.L200
	movq	-80(%rbp), %rdx
	cmpl	%ecx, %r12d
	jle	.L202
.L199:
	movl	%eax, %esi
	addq	%rdx, %rcx
	subl	%r8d, %esi
	leaq	(%r15,%rcx,8), %rcx
	subl	%r14d, %esi
	addl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L203:
	movl	%esi, %edi
	addl	$1, %eax
	addl	$2, %esi
	addq	$8, %rcx
	orl	$1, %edi
	movl	%edi, -8(%rcx)
	movl	$1800, %edi
	movw	%di, -4(%rcx)
	cmpl	%eax, %r12d
	jg	.L203
.L202:
	leal	-1(%r12), %eax
	addq	$1, %rax
.L198:
	testl	%r10d, %r10d
	je	.L204
	addq	%rdx, %rax
	movl	$1800, %ecx
	leaq	(%r15,%rax,8), %rax
	movl	$12, (%rax)
	movw	%cx, 4(%rax)
.L204:
	movl	-68(%rbp), %eax
	cmpl	$2, %eax
	ja	.L212
	movl	%eax, %r9d
	leaq	CSWTCH.795(%rip), %rax
	movl	(%rax,%r9,4), %eax
	movl	%eax, -72(%rbp)
	leaq	CSWTCH.796(%rip), %rax
	movzbl	(%rax,%r9), %ecx
	leaq	CSWTCH.797(%rip), %rax
	movzbl	(%rax,%r9), %r8d
.L205:
	movq	16(%r13), %r12
	movq	24(%r13), %rax
	subq	%r12, %rax
	cmpq	$23, %rax
	jbe	.L224
	leaq	24(%r12), %rax
	movq	%rax, 16(%r13)
.L207:
	movq	%rdx, %xmm0
	movq	%r15, 16(%r12)
	movl	-60(%rbp), %eax
	movq	%rbx, %rdi
	movhps	-56(%rbp), %xmm0
	movb	%r8b, -80(%rbp)
	orl	$4, %eax
	movups	%xmm0, (%r12)
	movb	%cl, -68(%rbp)
	movl	%eax, -56(%rbp)
	call	_ZNK2v88internal23CallInterfaceDescriptor9DebugNameEv@PLT
	movq	24(%r13), %rdx
	movzbl	-68(%rbp), %ecx
	movq	%rax, %r15
	movq	8(%rbx), %rax
	movzbl	-80(%rbp), %r8d
	movl	16(%rax), %ebx
	movq	16(%r13), %rax
	subq	%rax, %rdx
	cmpq	$79, %rdx
	jbe	.L225
	leaq	80(%rax), %rdx
	movq	%rdx, 16(%r13)
.L209:
	movl	-72(%rbp), %edx
	movl	%ebx, 60(%rax)
	movl	-56(%rbp), %ebx
	movl	$1, (%rax)
	movl	%edx, 8(%rax)
	movzbl	-61(%rbp), %edx
	movb	$1, 4(%rax)
	movb	%cl, 12(%rax)
	movb	%r8b, 13(%rax)
	movl	$-2, 16(%rax)
	movb	%cl, 20(%rax)
	movb	%r8b, 21(%rax)
	movq	%r12, 24(%rax)
	movq	%r14, 32(%rax)
	movq	$0, 40(%rax)
	movb	%dl, 48(%rax)
	movq	$0, 52(%rax)
	movl	%ebx, 64(%rax)
	movq	%r15, 72(%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L212:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L210:
	xorl	%eax, %eax
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L224:
	movl	$24, %esi
	movq	%r13, %rdi
	movq	%rdx, -88(%rbp)
	movb	%r8b, -80(%rbp)
	movb	%cl, -68(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-68(%rbp), %ecx
	movzbl	-80(%rbp), %r8d
	movq	-88(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L225:
	movl	$80, %esi
	movq	%r13, %rdi
	movb	%r8b, -68(%rbp)
	movb	%cl, -60(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-60(%rbp), %ecx
	movzbl	-68(%rbp), %r8d
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L223:
	movl	%r8d, -92(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%r10d, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-80(%rbp), %r10d
	movq	-88(%rbp), %rdx
	movl	-92(%rbp), %r8d
	movq	%rax, %r15
	jmp	.L194
	.cfi_endproc
.LFE22108:
	.size	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE, .-_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE
	.section	.text._ZN2v88internal8compiler7Linkage33GetBytecodeDispatchCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7Linkage33GetBytecodeDispatchCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEi
	.type	_ZN2v88internal8compiler7Linkage33GetBytecodeDispatchCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEi, @function
_ZN2v88internal8compiler7Linkage33GetBytecodeDispatchCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEi:
.LFB22109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movslq	%edx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rax
	movq	16(%rdi), %r14
	movl	(%rax), %edx
	movq	24(%rdi), %rax
	leal	(%r15,%rdx), %ebx
	subq	%r14, %rax
	leal	1(%rbx), %esi
	movslq	%ebx, %r8
	movslq	%esi, %rsi
	salq	$3, %rsi
	cmpq	%rax, %rsi
	ja	.L244
	addq	%r14, %rsi
	movq	%rsi, 16(%rdi)
.L228:
	movq	8(%r12), %rax
	movq	32(%rax), %rax
	movzwl	(%rax), %eax
	movl	$0, (%r14)
	movw	%ax, 4(%r14)
	testl	%ebx, %ebx
	jle	.L234
	testl	%edx, %edx
	jle	.L240
	cmpl	%edx, %ebx
	movl	%edx, %r10d
	leaq	8(%r14), %rsi
	cmovle	%ebx, %r10d
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L233:
	movq	8(%r12), %rdi
	movl	%ecx, %eax
	addq	$1, %rcx
	addq	$8, %rsi
	movq	32(%rdi), %r11
	addl	4(%rdi), %eax
	cltq
	movzwl	(%r11,%rax,2), %r11d
	movq	24(%rdi), %rax
	movl	(%rax,%r9), %eax
	movw	%r11w, -4(%rsi)
	addq	$4, %r9
	addl	%eax, %eax
	movl	%eax, -8(%rsi)
	movl	%ecx, %eax
	cmpl	%ecx, %r10d
	jg	.L233
	cmpl	%ecx, %ebx
	jle	.L234
.L232:
	movl	%eax, %esi
	subl	%edx, %esi
	leaq	8(%r14,%rcx,8), %rdx
	subl	%r15d, %esi
	addl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L236:
	movl	%esi, %ecx
	movl	$1800, %edi
	addl	$1, %eax
	addl	$2, %esi
	orl	$1, %ecx
	movw	%di, 4(%rdx)
	addq	$8, %rdx
	movl	%ecx, -8(%rdx)
	cmpl	%eax, %ebx
	jg	.L236
.L234:
	movq	16(%r13), %rbx
	movq	24(%r13), %rax
	subq	%rbx, %rax
	cmpq	$23, %rax
	jbe	.L245
	leaq	24(%rbx), %rax
	movq	%rax, 16(%r13)
.L237:
	movq	$1, (%rbx)
	movq	%r12, %rdi
	movq	%r8, 8(%rbx)
	movq	%r14, 16(%rbx)
	call	_ZNK2v88internal23CallInterfaceDescriptor9DebugNameEv@PLT
	movq	24(%r13), %rdx
	movq	%rax, %r12
	movq	16(%r13), %rax
	subq	%rax, %rdx
	cmpq	$79, %rdx
	jbe	.L246
	leaq	80(%rax), %rdx
	movq	%rdx, 16(%r13)
.L239:
	movq	%rbx, 24(%rax)
	movl	$5, %edx
	movl	$5, %ecx
	movabsq	$566935683072, %rbx
	movl	$1, (%rax)
	movb	$1, 4(%rax)
	movl	$2, 8(%rax)
	movw	%dx, 12(%rax)
	movl	$-2, 16(%rax)
	movw	%cx, 20(%rax)
	movq	%r15, 32(%rax)
	movq	$0, 40(%rax)
	movb	$0, 48(%rax)
	movq	$0, 52(%rax)
	movq	%rbx, 60(%rax)
	movq	%r12, 72(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L244:
	movl	%edx, -60(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movl	-60(%rbp), %edx
	movq	%rax, %r14
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L245:
	movl	$24, %esi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rbx
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L246:
	movl	$80, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L239
	.cfi_endproc
.LFE22109:
	.size	_ZN2v88internal8compiler7Linkage33GetBytecodeDispatchCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEi, .-_ZN2v88internal8compiler7Linkage33GetBytecodeDispatchCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEi
	.section	.rodata._ZNK2v88internal8compiler7Linkage19GetOsrValueLocationEi.str1.1,"aMS",@progbits,1
.LC14:
	.string	"incoming_->IsJSFunctionCall()"
.LC15:
	.string	"Check failed: %s."
	.section	.text._ZNK2v88internal8compiler7Linkage19GetOsrValueLocationEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler7Linkage19GetOsrValueLocationEi
	.type	_ZNK2v88internal8compiler7Linkage19GetOsrValueLocationEi, @function
_ZNK2v88internal8compiler7Linkage19GetOsrValueLocationEi:
.LFB22110:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rcx
	cmpl	$1, 8(%rcx)
	jne	.L265
	movq	32(%rcx), %rax
	cmpl	$-1, %esi
	je	.L266
	cmpl	%esi, %eax
	jle	.L267
	addl	$1, %esi
	movslq	%esi, %rsi
.L264:
	movl	16(%rcx), %edx
	movzbl	20(%rcx), %eax
	movzbl	21(%rcx), %edi
	testq	%rsi, %rsi
	je	.L253
	movq	24(%rcx), %rax
	movq	(%rax), %rdx
	movq	16(%rax), %rax
	leaq	-1(%rsi,%rdx), %rdx
	leaq	(%rax,%rdx,8), %rcx
	movl	(%rcx), %edx
	movzbl	4(%rcx), %eax
	movzbl	5(%rcx), %edi
.L253:
	salq	$32, %rax
	salq	$40, %rdi
	movq	%rax, %rcx
	movl	%edx, %eax
	orq	%rcx, %rax
	orq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	movabsq	$7730941132800, %rdx
	subl	%eax, %esi
	leal	8(%rsi,%rsi), %eax
	orl	$1, %eax
	orq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	addl	$3, %eax
	movslq	%eax, %rsi
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L265:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC14(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22110:
	.size	_ZNK2v88internal8compiler7Linkage19GetOsrValueLocationEi, .-_ZNK2v88internal8compiler7Linkage19GetOsrValueLocationEi
	.section	.text._ZNK2v88internal8compiler7Linkage29ParameterHasSecondaryLocationEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler7Linkage29ParameterHasSecondaryLocationEi
	.type	_ZNK2v88internal8compiler7Linkage29ParameterHasSecondaryLocationEi, @function
_ZNK2v88internal8compiler7Linkage29ParameterHasSecondaryLocationEi:
.LFB22112:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	8(%rdx), %ecx
	cmpl	$1, %ecx
	je	.L288
	xorl	%eax, %eax
	cmpl	$4, %ecx
	je	.L289
.L268:
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	addl	$1, %esi
	movl	16(%rdx), %ecx
	movzbl	20(%rdx), %edi
	movslq	%esi, %rsi
	testq	%rsi, %rsi
	jne	.L290
	movl	%ecx, %eax
	notl	%eax
	andl	$1, %eax
	je	.L268
.L291:
	sarl	%ecx
	cmpb	$7, %dil
	sete	%dl
	cmpl	$7, %ecx
	sete	%al
	andb	%dl, %al
	jne	.L268
	cmpl	$6, %ecx
	sete	%al
	andl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	addl	$1, %esi
	movl	16(%rdx), %ecx
	movzbl	20(%rdx), %edi
	movslq	%esi, %rsi
	testq	%rsi, %rsi
	je	.L273
	movq	24(%rdx), %rax
	movq	(%rax), %rdx
	movq	16(%rax), %rax
	leaq	-1(%rsi,%rdx), %rdx
	leaq	(%rax,%rdx,8), %rax
	movl	(%rax), %ecx
	movzbl	4(%rax), %edi
.L273:
	movl	%ecx, %eax
	notl	%eax
	andl	$1, %eax
	je	.L268
	sarl	%ecx
	cmpl	$6, %ecx
	sete	%al
	cmpb	$7, %dil
	sete	%dl
	andl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	movq	24(%rdx), %rax
	movq	(%rax), %rdx
	movq	16(%rax), %rax
	leaq	-1(%rsi,%rdx), %rdx
	leaq	(%rax,%rdx,8), %rax
	movl	(%rax), %ecx
	movzbl	4(%rax), %edi
	movl	%ecx, %eax
	notl	%eax
	andl	$1, %eax
	je	.L268
	jmp	.L291
	.cfi_endproc
.LFE22112:
	.size	_ZNK2v88internal8compiler7Linkage29ParameterHasSecondaryLocationEi, .-_ZNK2v88internal8compiler7Linkage29ParameterHasSecondaryLocationEi
	.section	.text._ZNK2v88internal8compiler7Linkage29GetParameterSecondaryLocationEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler7Linkage29GetParameterSecondaryLocationEi
	.type	_ZNK2v88internal8compiler7Linkage29GetParameterSecondaryLocationEi, @function
_ZNK2v88internal8compiler7Linkage29GetParameterSecondaryLocationEi:
.LFB22113:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addl	$1, %esi
	movslq	%esi, %rsi
	movl	16(%rax), %edx
	movzbl	20(%rax), %ecx
	testq	%rsi, %rsi
	je	.L293
	movq	24(%rax), %rdx
	movq	(%rdx), %rcx
	movq	16(%rdx), %rdx
	leaq	-1(%rsi,%rcx), %rcx
	leaq	(%rdx,%rcx,8), %rcx
	movl	(%rcx), %edx
	movzbl	4(%rcx), %ecx
.L293:
	movl	8(%rax), %eax
	cmpl	$1, %eax
	je	.L316
	cmpl	$4, %eax
	jne	.L317
.L297:
	movabsq	$7730941132807, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	testb	$1, %dl
	jne	.L295
	sarl	%edx
	cmpl	$7, %edx
	jne	.L295
	cmpb	$7, %cl
	je	.L297
.L295:
	movabsq	$7730941132805, %rax
	ret
.L317:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22113:
	.size	_ZNK2v88internal8compiler7Linkage29GetParameterSecondaryLocationEi, .-_ZNK2v88internal8compiler7Linkage29GetParameterSecondaryLocationEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptor4KindE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptor4KindE, @function
_GLOBAL__sub_I__ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptor4KindE:
.LFB26851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26851:
	.size	_GLOBAL__sub_I__ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptor4KindE, .-_GLOBAL__sub_I__ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptor4KindE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compilerlsERSoRKNS1_14CallDescriptor4KindE
	.section	.rodata.CSWTCH.797,"a"
	.type	CSWTCH.797, @object
	.size	CSWTCH.797, 3
CSWTCH.797:
	.byte	7
	.byte	0
	.byte	7
	.section	.rodata.CSWTCH.796,"a"
	.type	CSWTCH.796, @object
	.size	CSWTCH.796, 3
CSWTCH.796:
	.byte	8
	.byte	5
	.byte	8
	.section	.rodata.CSWTCH.795,"a"
	.align 8
	.type	CSWTCH.795, @object
	.size	CSWTCH.795, 12
CSWTCH.795:
	.long	0
	.long	4
	.long	6
	.section	.rodata.CSWTCH.53,"a"
	.align 32
	.type	CSWTCH.53, @object
	.size	CSWTCH.53, 56
CSWTCH.53:
	.long	0
	.long	0
	.long	1
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	2
	.long	3
	.long	4
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
