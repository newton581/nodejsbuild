	.file	"move-optimizer.cc"
	.text
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler12_GLOBAL__N_17MoveKeyES4_St9_IdentityIS4_ENS3_14MoveKeyCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler12_GLOBAL__N_17MoveKeyES4_St9_IdentityIS4_ENS3_14MoveKeyCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIN2v88internal8compiler12_GLOBAL__N_17MoveKeyES4_St9_IdentityIS4_ENS3_14MoveKeyCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB12257:
	.cfi_startproc
	testq	%rsi, %rsi
	je	.L9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L3:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler12_GLOBAL__N_17MoveKeyES4_St9_IdentityIS4_ENS3_14MoveKeyCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L3
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE12257:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler12_GLOBAL__N_17MoveKeyES4_St9_IdentityIS4_ENS3_14MoveKeyCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIN2v88internal8compiler12_GLOBAL__N_17MoveKeyES4_St9_IdentityIS4_ENS3_14MoveKeyCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler12_GLOBAL__N_17MoveKeyESt4pairIKS4_jESt10_Select1stIS7_ENS3_14MoveKeyCompareENS1_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler12_GLOBAL__N_17MoveKeyESt4pairIKS4_jESt10_Select1stIS7_ENS3_14MoveKeyCompareENS1_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E, @function
_ZNSt8_Rb_treeIN2v88internal8compiler12_GLOBAL__N_17MoveKeyESt4pairIKS4_jESt10_Select1stIS7_ENS3_14MoveKeyCompareENS1_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E:
.LFB12289:
	.cfi_startproc
	testq	%rsi, %rsi
	je	.L21
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L15:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler12_GLOBAL__N_17MoveKeyESt4pairIKS4_jESt10_Select1stIS7_ENS3_14MoveKeyCompareENS1_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L15
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE12289:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler12_GLOBAL__N_17MoveKeyESt4pairIKS4_jESt10_Select1stIS7_ENS3_14MoveKeyCompareENS1_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E, .-_ZNSt8_Rb_treeIN2v88internal8compiler12_GLOBAL__N_17MoveKeyESt4pairIKS4_jESt10_Select1stIS7_ENS3_14MoveKeyCompareENS1_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_111LoadCompareEPKNS1_12MoveOperandsES5_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_111LoadCompareEPKNS1_12MoveOperandsES5_, @function
_ZN2v88internal8compiler12_GLOBAL__N_111LoadCompareEPKNS1_12MoveOperandsES5_:
.LFB10798:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	%eax, %r10d
	movq	%rax, %rcx
	andl	$4, %r10d
	je	.L25
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L26
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L26:
	movq	%rax, %rcx
	andq	$-8161, %rcx
	orq	%rdx, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L25:
	movq	(%rsi), %rdx
	movl	%edx, %r11d
	movq	%rdx, %r8
	andl	$4, %r11d
	je	.L27
	xorl	%r9d, %r9d
	testb	$24, %dl
	jne	.L28
	shrq	$5, %r8
	cmpb	$12, %r8b
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
.L28:
	movq	%rdx, %r8
	andq	$-8161, %r8
	orq	%r9, %r8
	andq	$-8, %r8
	orq	$4, %r8
.L27:
	cmpq	%rcx, %r8
	je	.L29
	testl	%r10d, %r10d
	jne	.L85
.L30:
	testl	%r11d, %r11d
	je	.L32
	xorl	%ecx, %ecx
	testb	$24, %dl
	je	.L86
.L33:
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L32:
	cmpq	%rax, %rdx
	seta	%r8b
.L24:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	xorl	%ecx, %ecx
	testb	$24, %al
	jne	.L31
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L31:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L29:
	movq	8(%rdi), %rcx
	testb	$4, %cl
	jne	.L87
	movq	8(%rsi), %rax
	testb	$4, %al
	je	.L38
	movq	%rax, %rdx
	movl	$1, %r8d
	shrq	$3, %rdx
	andl	$3, %edx
	cmpl	$1, %edx
	je	.L24
.L51:
	xorl	%esi, %esi
	testl	%edx, %edx
	jne	.L39
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L39:
	andq	$-8161, %rax
	orq	%rsi, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L38:
	cmpq	%rax, %rcx
	setb	%r8b
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%rcx, %rdx
	movq	8(%rsi), %rax
	shrq	$3, %rdx
	andl	$3, %edx
	cmpl	$1, %edx
	je	.L88
	movl	%eax, %esi
	andl	$4, %esi
	je	.L89
	movq	%rax, %rdi
	shrq	$3, %rdi
	andl	$3, %edi
	subl	$1, %edi
	je	.L90
	testl	%edx, %edx
	jne	.L81
	movq	%rcx, %rdx
	shrq	$5, %rdx
	cmpb	$11, %dl
	jbe	.L49
	andq	$-8168, %rcx
	orq	$420, %rcx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L88:
	xorl	%r8d, %r8d
	testb	$4, %al
	je	.L24
	movq	%rax, %rdx
	shrq	$3, %rdx
	andl	$3, %edx
	cmpl	$1, %edx
	jne	.L24
.L81:
	andq	$-8168, %rcx
	orq	$4, %rcx
.L44:
	movq	%rax, %rdx
	shrq	$3, %rdx
	andl	$3, %edx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%rdx, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
	jmp	.L33
.L49:
	andq	$-8168, %rcx
	orq	$4, %rcx
	testl	%esi, %esi
	je	.L38
	jmp	.L44
.L89:
	movl	%edx, %esi
	testl	%edx, %edx
	jne	.L91
	movq	%rcx, %rdx
	shrq	$5, %rdx
	cmpb	$11, %dl
	jbe	.L49
	andq	$-8168, %rcx
	orq	$420, %rcx
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L91:
	andq	$-8168, %rcx
	orq	$4, %rcx
	jmp	.L38
.L90:
	movl	$1, %r8d
	jmp	.L24
	.cfi_endproc
.LFE10798:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_111LoadCompareEPKNS1_12MoveOperandsES5_, .-_ZN2v88internal8compiler12_GLOBAL__N_111LoadCompareEPKNS1_12MoveOperandsES5_
	.section	.text._ZN2v88internal8compiler13MoveOptimizerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13MoveOptimizerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE
	.type	_ZN2v88internal8compiler13MoveOptimizerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE, @function
_ZN2v88internal8compiler13MoveOptimizerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE:
.LFB10750:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm1
	movq	%rdx, %xmm2
	movq	%rsi, 16(%rdi)
	punpcklqdq	%xmm2, %xmm1
	movq	$0, 24(%rdi)
	movups	%xmm1, (%rdi)
	pxor	%xmm1, %xmm1
	movq	%rsi, 48(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	%rsi, 80(%rdi)
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	movq	$0, 104(%rdi)
	movups	%xmm1, 32(%rdi)
	ret
	.cfi_endproc
.LFE10750:
	.size	_ZN2v88internal8compiler13MoveOptimizerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE, .-_ZN2v88internal8compiler13MoveOptimizerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE
	.globl	_ZN2v88internal8compiler13MoveOptimizerC1EPNS0_4ZoneEPNS1_19InstructionSequenceE
	.set	_ZN2v88internal8compiler13MoveOptimizerC1EPNS0_4ZoneEPNS1_19InstructionSequenceE,_ZN2v88internal8compiler13MoveOptimizerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE
	.section	.text._ZNK2v88internal8compiler13MoveOptimizer15LastInstructionEPKNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler13MoveOptimizer15LastInstructionEPKNS1_16InstructionBlockE
	.type	_ZNK2v88internal8compiler13MoveOptimizer15LastInstructionEPKNS1_16InstructionBlockE, @function
_ZNK2v88internal8compiler13MoveOptimizer15LastInstructionEPKNS1_16InstructionBlockE:
.LFB10767:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	116(%rsi), %edx
	movq	208(%rcx), %rsi
	subl	$1, %edx
	movslq	%edx, %rdx
	movq	%rsi, %rax
	subq	216(%rcx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	js	.L94
	cmpq	$63, %rax
	jle	.L98
	movq	%rax, %rdx
	sarq	$6, %rdx
.L97:
	movq	232(%rcx), %rcx
	movq	%rdx, %rsi
	salq	$6, %rsi
	movq	(%rcx,%rdx,8), %rdx
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %rax
	movq	(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	leaq	(%rsi,%rdx,8), %rax
	movq	(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L97
	.cfi_endproc
.LFE10767:
	.size	_ZNK2v88internal8compiler13MoveOptimizer15LastInstructionEPKNS1_16InstructionBlockE, .-_ZNK2v88internal8compiler13MoveOptimizer15LastInstructionEPKNS1_16InstructionBlockE
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB12131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L137
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L115
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L138
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L101:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L139
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L104:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L138:
	testq	%rdx, %rdx
	jne	.L140
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L102:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L105
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L118
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L118
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L107:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L107
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L109
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L109:
	leaq	16(%rax,%r8), %rcx
.L105:
	cmpq	%r14, %r12
	je	.L110
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L119
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L119
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L112:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L112
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L114
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L114:
	leaq	8(%rcx,%r9), %rcx
.L110:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L119:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L111:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L111
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L118:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L106:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L106
	jmp	.L109
.L139:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L104
.L137:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L140:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L101
	.cfi_endproc
.LFE12131:
	.size	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal8compiler13MoveOptimizer13CompressMovesEPNS1_12ParallelMoveEPNS0_10ZoneVectorIPNS1_12MoveOperandsEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13MoveOptimizer13CompressMovesEPNS1_12ParallelMoveEPNS0_10ZoneVectorIPNS1_12MoveOperandsEEE
	.type	_ZN2v88internal8compiler13MoveOptimizer13CompressMovesEPNS1_12ParallelMoveEPNS0_10ZoneVectorIPNS1_12MoveOperandsEEE, @function
_ZN2v88internal8compiler13MoveOptimizer13CompressMovesEPNS1_12ParallelMoveEPNS0_10ZoneVectorIPNS1_12MoveOperandsEEE:
.LFB10764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L141
	movq	16(%rdx), %r12
	movq	8(%rdx), %rbx
	movq	%rsi, %r14
	movq	%rdx, %r13
	movq	16(%rsi), %rax
	cmpq	%rax, 8(%rsi)
	je	.L144
	movq	%rdi, %r15
	leaq	16(%rdi), %rcx
	cmpq	%rbx, %r12
	je	.L154
	.p2align 4,,10
	.p2align 3
.L153:
	movq	(%rbx), %rsi
	movq	(%rsi), %rax
	testb	$7, %al
	je	.L148
	testb	$4, %al
	je	.L149
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L150
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L150:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L149:
	movq	8(%rsi), %rdx
	testb	$4, %dl
	je	.L151
	xorl	%edi, %edi
	testb	$24, %dl
	jne	.L152
	movq	%rdx, %rdi
	shrq	$5, %rdi
	cmpb	$12, %dil
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
.L152:
	andq	$-8161, %rdx
	orq	%rdi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L151:
	cmpq	%rax, %rdx
	je	.L148
	movq	%rcx, %rdx
	movq	%r14, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZNK2v88internal8compiler12ParallelMove18PrepareInsertAfterEPNS1_12MoveOperandsEPNS0_10ZoneVectorIS4_EE@PLT
	movq	-72(%rbp), %rcx
.L148:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L153
.L154:
	movq	32(%r15), %rcx
	movq	24(%r15), %rax
	cmpq	%rax, %rcx
	je	.L199
	.p2align 4,,10
	.p2align 3
.L155:
	movq	(%rax), %rdx
	addq	$8, %rax
	movq	$0, 8(%rdx)
	movq	$0, (%rdx)
	cmpq	%rax, %rcx
	jne	.L155
	movq	24(%r15), %rax
	cmpq	32(%r15), %rax
	je	.L199
	movq	%rax, 32(%r15)
.L199:
	movq	16(%r13), %r12
	movq	8(%r13), %rbx
.L144:
	cmpq	%rbx, %r12
	je	.L141
	leaq	-64(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L164:
	movq	(%rbx), %rcx
	movq	%rcx, -64(%rbp)
	movq	(%rcx), %rax
	testb	$7, %al
	je	.L158
	testb	$4, %al
	je	.L159
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L160
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L160:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L159:
	movq	8(%rcx), %rdx
	testb	$4, %dl
	je	.L161
	xorl	%esi, %esi
	testb	$24, %dl
	jne	.L162
	movq	%rdx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L162:
	andq	$-8161, %rdx
	orq	%rsi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L161:
	cmpq	%rax, %rdx
	je	.L158
	movq	16(%r14), %rsi
	cmpq	24(%r14), %rsi
	je	.L163
	movq	%rcx, (%rsi)
	addq	$8, 16(%r14)
.L158:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L164
	movq	8(%r13), %rax
	cmpq	16(%r13), %rax
	je	.L141
	movq	%rax, 16(%r13)
.L141:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L158
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10764:
	.size	_ZN2v88internal8compiler13MoveOptimizer13CompressMovesEPNS1_12ParallelMoveEPNS0_10ZoneVectorIPNS1_12MoveOperandsEEE, .-_ZN2v88internal8compiler13MoveOptimizer13CompressMovesEPNS1_12ParallelMoveEPNS0_10ZoneVectorIPNS1_12MoveOperandsEEE
	.section	.text._ZN2v88internal8compiler13MoveOptimizer12CompressGapsEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13MoveOptimizer12CompressGapsEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler13MoveOptimizer12CompressGapsEPNS1_11InstructionE, @function
_ZN2v88internal8compiler13MoveOptimizer12CompressGapsEPNS1_11InstructionE:
.LFB10765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
.L213:
	movq	8(%rsi,%r10,8), %r9
	movl	%r10d, %ebx
	testq	%r9, %r9
	je	.L203
	movq	8(%r9), %rcx
	movq	16(%r9), %r11
	cmpq	%r11, %rcx
	je	.L203
.L211:
	movq	(%rcx), %r8
	movq	(%r8), %rax
	testb	$7, %al
	je	.L205
	testb	$4, %al
	je	.L206
	xorl	%edx, %edx
	testb	$24, %al
	je	.L233
.L207:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L206:
	movq	8(%r8), %rdx
	testb	$4, %dl
	je	.L208
	xorl	%r12d, %r12d
	testb	$24, %dl
	je	.L234
.L209:
	andq	$-8161, %rdx
	orq	%r12, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L208:
	cmpq	%rax, %rdx
	je	.L205
	cmpl	$1, %ebx
	jne	.L214
	movdqu	8(%rsi), %xmm0
	shufpd	$1, %xmm0, %xmm0
	movups	%xmm0, 8(%rsi)
.L201:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	addq	$8, %rcx
	movq	$0, 8(%r8)
	movq	$0, (%r8)
	cmpq	%rcx, %r11
	jne	.L211
	movq	8(%r9), %rax
	cmpq	16(%r9), %rax
	je	.L203
	movq	%rax, 16(%r9)
	.p2align 4,,10
	.p2align 3
.L203:
	cmpq	$1, %r10
	je	.L201
	movl	$1, %r10d
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L234:
	movq	%rdx, %r12
	shrq	$5, %r12
	cmpb	$12, %r12b
	sbbq	%r12, %r12
	notq	%r12
	andl	$416, %r12d
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L214:
	movq	16(%rsi), %rdx
	movq	8(%rsi), %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13MoveOptimizer13CompressMovesEPNS1_12ParallelMoveEPNS0_10ZoneVectorIPNS1_12MoveOperandsEEE
	.cfi_endproc
.LFE10765:
	.size	_ZN2v88internal8compiler13MoveOptimizer12CompressGapsEPNS1_11InstructionE, .-_ZN2v88internal8compiler13MoveOptimizer12CompressGapsEPNS1_11InstructionE
	.section	.text._ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_:
.LFB12228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L273
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L251
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L274
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L237:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L275
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L240:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L274:
	testq	%rdx, %rdx
	jne	.L276
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L238:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L241
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L254
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L254
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L243:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L243
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L245
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L245:
	leaq	16(%rax,%r8), %rcx
.L241:
	cmpq	%r14, %r12
	je	.L246
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L255
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L255
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L248:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L248
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L250
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L250:
	leaq	8(%rcx,%r9), %rcx
.L246:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L255:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L247:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L247
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L254:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L242:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L242
	jmp	.L245
.L275:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L240
.L273:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L276:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L237
	.cfi_endproc
.LFE12228:
	.size	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.section	.text._ZN2v88internal8compiler13MoveOptimizer27RemoveClobberedDestinationsEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13MoveOptimizer27RemoveClobberedDestinationsEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler13MoveOptimizer27RemoveClobberedDestinationsEPNS1_11InstructionE, @function
_ZN2v88internal8compiler13MoveOptimizer27RemoveClobberedDestinationsEPNS1_11InstructionE:
.LFB10753:
	.cfi_startproc
	endbr64
	testb	$64, 7(%rsi)
	jne	.L443
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L277
	movq	56(%rdi), %rsi
	movq	%rdi, %r12
	leaq	48(%rdi), %r14
	cmpq	%rsi, 64(%rdi)
	je	.L279
	movq	%rsi, 64(%rdi)
.L279:
	leaq	80(%r12), %rax
	movq	%rax, -64(%rbp)
	movq	88(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L280
	movq	%rax, 96(%r12)
.L280:
	movl	4(%r13), %eax
	movl	%eax, %edx
	andl	$255, %edx
	je	.L281
	leaq	40(%r13), %r8
	xorl	%r15d, %r15d
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L451:
	movq	(%r8), %rax
	addq	$1, %r15
	addq	$8, %r8
	movq	%rax, (%rsi)
	addq	$8, 64(%r12)
	movl	4(%r13), %eax
	movzbl	%al, %esi
	movq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L281
.L452:
	movq	64(%r12), %rsi
.L282:
	cmpq	%rsi, 72(%r12)
	jne	.L451
	movq	%r8, %rdx
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	addq	$1, %r15
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movl	4(%r13), %eax
	movq	-56(%rbp), %r8
	movzbl	%al, %esi
	addq	$8, %r8
	movq	%rsi, %rdx
	cmpq	%r15, %rsi
	ja	.L452
	.p2align 4,,10
	.p2align 3
.L281:
	xorl	%r15d, %r15d
	testl	$1056964608, %eax
	jne	.L283
	.p2align 4,,10
	.p2align 3
.L284:
	xorl	%r15d, %r15d
	testl	$16776960, %eax
	jne	.L287
	.p2align 4,,10
	.p2align 3
.L294:
	movq	16(%rbx), %r9
	movq	8(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L289
	.p2align 4,,10
	.p2align 3
.L335:
	movq	56(%r12), %rax
	movq	64(%r12), %rsi
	movq	(%rdi), %r8
	cmpq	%rsi, %rax
	je	.L296
	movq	8(%r8), %rcx
	movq	%rcx, %rdx
	movq	%rcx, %r10
	shrq	$3, %rdx
	shrq	$5, %r10
	andl	$3, %edx
	testb	$4, %cl
	je	.L302
	testl	%edx, %edx
	je	.L303
	movq	%rcx, %r10
	andq	$-8168, %r10
	orq	$4, %r10
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L453:
	xorl	%r11d, %r11d
.L306:
	andq	$-8161, %rdx
	orq	%r11, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L304:
	cmpq	%rdx, %r10
	je	.L307
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L296
.L308:
	movq	(%rax), %rdx
	testb	$4, %dl
	je	.L304
	testb	$24, %dl
	jne	.L453
	movq	%rdx, %r11
	shrq	$5, %r11
	cmpb	$12, %r11b
	sbbq	%r11, %r11
	notq	%r11
	andl	$416, %r11d
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L454:
	movq	(%rdx), %rax
	addq	$1, %r15
	movq	%rax, (%rsi)
	addq	$8, 64(%r12)
	movl	4(%r13), %eax
	movl	%eax, %edx
	shrl	$24, %edx
	andl	$63, %edx
	cmpq	%rdx, %r15
	jnb	.L284
.L291:
	movzbl	%al, %edx
.L283:
	movl	%edx, %edx
	shrl	$8, %eax
	movq	64(%r12), %rsi
	leaq	5(%r15,%rdx), %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	leaq	0(%r13,%rax,8), %rdx
	cmpq	72(%r12), %rsi
	jne	.L454
	movq	%r14, %rdi
	addq	$1, %r15
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movl	4(%r13), %eax
	movl	%eax, %edx
	shrl	$24, %edx
	andl	$63, %edx
	cmpq	%r15, %rdx
	ja	.L291
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L455:
	movq	(%rdx), %rax
	movq	%rax, (%rsi)
	addq	$8, 96(%r12)
.L449:
	movl	4(%r13), %eax
	addq	$1, %r15
	movl	%eax, %edx
	shrl	$8, %edx
	movzwl	%dx, %edx
	cmpq	%rdx, %r15
	jnb	.L294
.L287:
	movzbl	%al, %eax
	movq	96(%r12), %rsi
	leaq	5(%r15,%rax), %rax
	leaq	0(%r13,%rax,8), %rdx
	cmpq	104(%r12), %rsi
	jne	.L455
	movq	-64(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L456:
	xorl	%r10d, %r10d
.L300:
	andq	$-8161, %rdx
	orq	%r10, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L298:
	cmpq	%rdx, %rcx
	je	.L301
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L296
.L302:
	movq	(%rax), %rdx
	testb	$4, %dl
	je	.L298
	testb	$24, %dl
	jne	.L456
	movq	%rdx, %r10
	shrq	$5, %r10
	cmpb	$12, %r10b
	sbbq	%r10, %r10
	notq	%r10
	andl	$416, %r10d
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L323:
	movq	$0, 8(%r8)
	movq	$0, (%r8)
.L296:
	addq	$8, %rdi
	cmpq	%rdi, %r9
	jne	.L335
.L289:
	movl	0(%r13), %eax
	andl	$511, %eax
	cmpl	$23, %eax
	ja	.L277
	movl	$8390678, %edx
	btq	%rax, %rdx
	jnc	.L277
	movq	8(%rbx), %rdi
	movq	16(%rbx), %r9
	cmpq	%rdi, %r9
	je	.L277
	.p2align 4,,10
	.p2align 3
.L356:
	movq	88(%r12), %rax
	movq	96(%r12), %rsi
	movq	(%rdi), %r8
	cmpq	%rsi, %rax
	je	.L336
	movq	8(%r8), %rcx
	movq	%rcx, %rdx
	movq	%rcx, %r10
	shrq	$3, %rdx
	shrq	$5, %r10
	andl	$3, %edx
	testb	$4, %cl
	je	.L342
	andq	$-8168, %rcx
	testl	%edx, %edx
	je	.L343
	orq	$4, %rcx
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L457:
	xorl	%r10d, %r10d
.L346:
	andq	$-8161, %rdx
	orq	%r10, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L344:
	cmpq	%rdx, %rcx
	je	.L341
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L336
.L347:
	movq	(%rax), %rdx
	testb	$4, %dl
	je	.L344
	testb	$24, %dl
	jne	.L457
	movq	%rdx, %r10
	shrq	$5, %r10
	cmpb	$12, %r10b
	sbbq	%r10, %r10
	notq	%r10
	andl	$416, %r10d
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L336:
	movq	$0, 8(%r8)
	movq	$0, (%r8)
.L341:
	addq	$8, %rdi
	cmpq	%rdi, %r9
	jne	.L356
.L277:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	movq	96(%r12), %r10
	movq	88(%r12), %rdx
	cmpq	%r10, %rdx
	jne	.L322
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L458:
	xorl	%esi, %esi
.L321:
	andq	$-8161, %rax
	orq	%rsi, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L319:
	cmpq	%rax, %rcx
	je	.L296
	addq	$8, %rdx
	cmpq	%rdx, %r10
	je	.L323
.L322:
	movq	(%rdx), %rax
	testb	$4, %al
	je	.L319
	testb	$24, %al
	jne	.L458
	movq	%rax, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L303:
	cmpb	$11, %r10b
	movq	%rcx, %r10
	ja	.L309
	andq	$-8168, %r10
	orq	$4, %r10
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L459:
	xorl	%r11d, %r11d
.L312:
	andq	$-8161, %rdx
	orq	%r11, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L310:
	cmpq	%rdx, %r10
	je	.L313
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L296
.L314:
	movq	(%rax), %rdx
	testb	$4, %dl
	je	.L310
	testb	$24, %dl
	jne	.L459
	movq	%rdx, %r11
	shrq	$5, %r11
	cmpb	$12, %r11b
	sbbq	%r11, %r11
	notq	%r11
	andl	$416, %r11d
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L309:
	andq	$-8168, %r10
	orq	$420, %r10
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L460:
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L296
.L318:
	movq	(%rax), %rdx
	testb	$4, %dl
	je	.L315
	xorl	%r11d, %r11d
	testb	$24, %dl
	jne	.L316
	movq	%rdx, %r11
	shrq	$5, %r11
	cmpb	$12, %r11b
	sbbq	%r11, %r11
	notq	%r11
	andl	$416, %r11d
.L316:
	andq	$-8161, %rdx
	orq	%r11, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L315:
	cmpq	%rdx, %r10
	jne	.L460
	andq	$-8168, %rcx
	movq	96(%r12), %rsi
	movq	88(%r12), %rdx
	orq	$420, %rcx
	movq	%rcx, %r10
	cmpq	%rsi, %rdx
	jne	.L334
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L461:
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	je	.L323
.L334:
	movq	(%rdx), %rax
	testb	$4, %al
	je	.L332
	xorl	%ecx, %ecx
	testb	$24, %al
	jne	.L333
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L333:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L332:
	cmpq	%rax, %r10
	jne	.L461
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L462:
	xorl	%r10d, %r10d
.L340:
	andq	$-8161, %rdx
	orq	%r10, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L338:
	cmpq	%rdx, %rcx
	je	.L341
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L336
.L342:
	movq	(%rax), %rdx
	testb	$4, %dl
	je	.L338
	testb	$24, %dl
	jne	.L462
	movq	%rdx, %r10
	shrq	$5, %r10
	cmpb	$12, %r10b
	sbbq	%r10, %r10
	notq	%r10
	andl	$416, %r10d
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L343:
	cmpb	$11, %r10b
	ja	.L348
	orq	$4, %rcx
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L463:
	xorl	%r10d, %r10d
.L351:
	andq	$-8161, %rdx
	orq	%r10, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L349:
	cmpq	%rdx, %rcx
	je	.L341
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L336
.L352:
	movq	(%rax), %rdx
	testb	$4, %dl
	je	.L349
	testb	$24, %dl
	jne	.L463
	movq	%rdx, %r10
	shrq	$5, %r10
	cmpb	$12, %r10b
	sbbq	%r10, %r10
	notq	%r10
	andl	$416, %r10d
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L348:
	orq	$420, %rcx
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L464:
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L336
.L355:
	movq	(%rax), %rdx
	testb	$4, %dl
	je	.L353
	xorl	%r10d, %r10d
	testb	$24, %dl
	jne	.L354
	movq	%rdx, %r10
	shrq	$5, %r10
	cmpb	$12, %r10b
	sbbq	%r10, %r10
	notq	%r10
	andl	$416, %r10d
.L354:
	andq	$-8161, %rdx
	orq	%r10, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L353:
	cmpq	%rdx, %rcx
	jne	.L464
	addq	$8, %rdi
	cmpq	%rdi, %r9
	jne	.L356
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L307:
	movq	96(%r12), %r10
	movq	88(%r12), %rdx
	andq	$-8168, %rcx
	orq	$4, %rcx
	cmpq	%r10, %rdx
	jne	.L327
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L465:
	xorl	%esi, %esi
.L326:
	andq	$-8161, %rax
	orq	%rsi, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L324:
	cmpq	%rax, %rcx
	je	.L296
	addq	$8, %rdx
	cmpq	%rdx, %r10
	je	.L323
.L327:
	movq	(%rdx), %rax
	testb	$4, %al
	je	.L324
	testb	$24, %al
	jne	.L465
	movq	%rax, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L313:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rcx, %rax
	movq	96(%r12), %r10
	movq	88(%r12), %rsi
	andq	$-8168, %rax
	orq	$4, %rax
	cmpq	%r10, %rsi
	jne	.L331
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L466:
	xorl	%ecx, %ecx
.L330:
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L328:
	cmpq	%rdx, %rax
	je	.L296
	addq	$8, %rsi
	cmpq	%rsi, %r10
	je	.L323
.L331:
	movq	(%rsi), %rdx
	testb	$4, %dl
	je	.L328
	testb	$24, %dl
	jne	.L466
	movq	%rdx, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
	jmp	.L330
	.cfi_endproc
.LFE10753:
	.size	_ZN2v88internal8compiler13MoveOptimizer27RemoveClobberedDestinationsEPNS1_11InstructionE, .-_ZN2v88internal8compiler13MoveOptimizer27RemoveClobberedDestinationsEPNS1_11InstructionE
	.section	.text._ZN2v88internal8compiler13MoveOptimizer12MigrateMovesEPNS1_11InstructionES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13MoveOptimizer12MigrateMovesEPNS1_11InstructionES4_
	.type	_ZN2v88internal8compiler13MoveOptimizer12MigrateMovesEPNS1_11InstructionES4_, @function
_ZN2v88internal8compiler13MoveOptimizer12MigrateMovesEPNS1_11InstructionES4_:
.LFB10754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$64, 7(%rdx)
	jne	.L467
	movq	8(%rdx), %rax
	movq	%rdx, %rbx
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L467
	movq	16(%rax), %rcx
	cmpq	%rcx, 8(%rax)
	je	.L467
	movq	56(%rdi), %rax
	movq	%rdi, %r15
	leaq	48(%rdi), %r13
	cmpq	%rax, 64(%rdi)
	je	.L470
	movq	%rax, 64(%rdi)
.L470:
	leaq	80(%r15), %rax
	movq	%rax, -168(%rbp)
	movq	88(%r15), %rax
	cmpq	%rax, 96(%r15)
	je	.L471
	movq	%rax, 96(%r15)
.L471:
	movl	4(%rbx), %eax
	xorl	%r12d, %r12d
	testl	$16776960, %eax
	jne	.L472
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L983:
	movq	(%rdx), %rax
	movq	%rax, (%rsi)
	addq	$8, 64(%r15)
.L974:
	movl	4(%rbx), %eax
	addq	$1, %r12
	movl	%eax, %edx
	shrl	$8, %edx
	movzwl	%dx, %edx
	cmpq	%r12, %rdx
	jbe	.L477
.L472:
	movzbl	%al, %eax
	movq	64(%r15), %rsi
	leaq	5(%r12,%rax), %rax
	leaq	(%rbx,%rax,8), %rdx
	cmpq	72(%r15), %rsi
	jne	.L983
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L974
.L665:
	movq	-88(%rbp), %rdx
	leaq	-112(%rbp), %rcx
	testq	%rdx, %rdx
	je	.L467
.L661:
	movq	24(%rdx), %rax
	testq	%rax, %rax
	je	.L666
.L667:
	movq	24(%rax), %rsi
	movq	%rcx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler12_GLOBAL__N_17MoveKeyES4_St9_IdentityIS4_ENS3_14MoveKeyCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L667
.L666:
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L661
	.p2align 4,,10
	.p2align 3
.L467:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L984
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	.cfi_restore_state
	movl	%eax, %edx
	leaq	40(%rbx), %r13
	xorl	%r12d, %r12d
	andl	$255, %edx
	je	.L474
	.p2align 4,,10
	.p2align 3
.L483:
	movq	96(%r15), %rsi
	cmpq	104(%r15), %rsi
	je	.L481
	movq	0(%r13), %rax
	addq	$1, %r12
	addq	$8, %r13
	movq	%rax, (%rsi)
	addq	$8, 96(%r15)
	movl	4(%rbx), %eax
	movzbl	%al, %ecx
	movq	%rcx, %rdx
	cmpq	%rcx, %r12
	jb	.L483
.L474:
	movq	-168(%rbp), %r13
	xorl	%r12d, %r12d
	testl	$1056964608, %eax
	jne	.L478
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L985:
	movq	(%rdx), %rax
	addq	$1, %r12
	movq	%rax, (%rsi)
	addq	$8, 96(%r15)
	movl	4(%rbx), %eax
	movl	%eax, %edx
	shrl	$24, %edx
	andl	$63, %edx
	cmpq	%rdx, %r12
	jnb	.L486
.L485:
	movzbl	%al, %edx
.L478:
	movl	%edx, %edx
	shrl	$8, %eax
	movq	96(%r15), %rsi
	leaq	5(%r12,%rdx), %rdx
	movzwl	%ax, %eax
	addq	%rdx, %rax
	leaq	(%rbx,%rax,8), %rdx
	cmpq	104(%r15), %rsi
	jne	.L985
	movq	%r13, %rdi
	addq	$1, %r12
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movl	4(%rbx), %eax
	movl	%eax, %edx
	shrl	$24, %edx
	andl	$63, %edx
	cmpq	%r12, %rdx
	ja	.L485
	.p2align 4,,10
	.p2align 3
.L486:
	movq	-184(%rbp), %rax
	movq	16(%rax), %r12
	movq	8(%rax), %rbx
	cmpq	%rbx, %r12
	je	.L480
	.p2align 4,,10
	.p2align 3
.L494:
	movq	(%rbx), %rcx
	movq	(%rcx), %rax
	testb	$7, %al
	je	.L488
	testb	$4, %al
	je	.L489
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L490
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L490:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L489:
	movq	8(%rcx), %rsi
	movq	%rsi, %rdx
	testb	$4, %sil
	je	.L491
	xorl	%edi, %edi
	testb	$24, %sil
	jne	.L492
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
.L492:
	movq	%rsi, %rdx
	andq	$-8161, %rdx
	orq	%rdi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L491:
	cmpq	%rax, %rdx
	je	.L488
	movq	96(%r15), %r8
	cmpq	104(%r15), %r8
	je	.L493
	movq	%rsi, (%r8)
	addq	$8, 96(%r15)
.L488:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L494
.L480:
	movq	(%r15), %rax
	leaq	-96(%rbp), %r14
	movl	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -112(%rbp)
	movq	-184(%rbp), %rax
	movq	%r14, -80(%rbp)
	movq	%r14, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	16(%rax), %rbx
	movq	8(%rax), %r9
	cmpq	%r9, %rbx
	je	.L467
	movq	%r14, -200(%rbp)
	movq	%r15, %r8
	movq	%r9, %r13
	movq	%rbx, %r15
	.p2align 4,,10
	.p2align 3
.L574:
	movq	0(%r13), %rdx
	movq	(%rdx), %rbx
	testb	$7, %bl
	je	.L495
	movl	%ebx, %esi
	movq	%rbx, %rax
	andl	$4, %esi
	je	.L496
	xorl	%ecx, %ecx
	testb	$24, %bl
	jne	.L497
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L497:
	movq	%rbx, %rax
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L496:
	movq	8(%rdx), %r12
	movl	%r12d, %r9d
	movq	%r12, %rdx
	andl	$4, %r9d
	je	.L498
	xorl	%ecx, %ecx
	testb	$24, %r12b
	jne	.L499
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L499:
	movq	%r12, %rdx
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L498:
	cmpq	%rax, %rdx
	je	.L495
	movq	56(%r8), %rax
	movq	64(%r8), %rcx
	cmpq	%rcx, %rax
	je	.L500
	movq	%r12, %rdx
	movq	%r12, %rdi
	shrq	$3, %rdx
	shrq	$5, %rdi
	andl	$3, %edx
	testl	%r9d, %r9d
	je	.L505
	cmpb	$11, %dil
	movq	%r12, %rdi
	ja	.L506
	andq	$-8168, %rdi
	orq	$4, %rdi
	testl	%edx, %edx
	je	.L514
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L986:
	xorl	%edi, %edi
.L504:
	andq	$-8161, %rdx
	orq	%rdi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L502:
	cmpq	%rdx, %r12
	je	.L495
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L500
.L505:
	movq	(%rax), %rdx
	testb	$4, %dl
	je	.L502
	testb	$24, %dl
	jne	.L986
	movq	%rdx, %rdi
	shrq	$5, %rdi
	cmpb	$12, %dil
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L987:
	xorl	%r10d, %r10d
.L513:
	andq	$-8161, %rdx
	orq	%r10, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L511:
	cmpq	%rdx, %rdi
	je	.L495
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L500
.L514:
	movq	(%rax), %rdx
	testb	$4, %dl
	je	.L511
	testb	$24, %dl
	jne	.L987
	movq	%rdx, %r10
	shrq	$5, %r10
	cmpb	$12, %r10b
	sbbq	%r10, %r10
	notq	%r10
	andl	$416, %r10d
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L988:
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L500
.L510:
	movq	(%rax), %rdx
	testb	$4, %dl
	je	.L508
	xorl	%r10d, %r10d
	testb	$24, %dl
	jne	.L509
	movq	%rdx, %r10
	shrq	$5, %r10
	cmpb	$12, %r10b
	sbbq	%r10, %r10
	notq	%r10
	andl	$416, %r10d
.L509:
	andq	$-8161, %rdx
	orq	%r10, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L508:
	cmpq	%rdx, %rdi
	jne	.L988
	.p2align 4,,10
	.p2align 3
.L495:
	addq	$8, %r13
	cmpq	%r13, %r15
	jne	.L574
.L1009:
	cmpq	$0, -64(%rbp)
	movq	-200(%rbp), %r14
	movq	%r8, %r12
	je	.L579
	.p2align 4,,10
	.p2align 3
.L575:
	movq	-80(%rbp), %r13
	cmpq	%r14, %r13
	je	.L975
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L577:
	movq	%r13, %rdi
	movq	%r13, %r15
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	96(%r12), %rsi
	movq	32(%r15), %rdx
	movq	%rax, %r13
	movq	88(%r12), %rax
	cmpq	%rsi, %rax
	je	.L580
	movq	%rdx, %rdi
	movq	%rdx, %r8
	shrq	$3, %rdi
	shrq	$5, %r8
	andl	$3, %edi
	testb	$4, %dl
	je	.L586
	andq	$-8168, %rdx
	cmpb	$11, %r8b
	ja	.L587
	orq	$4, %rdx
	testl	%edi, %edi
	je	.L595
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L989:
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L580
.L591:
	movq	(%rax), %rdi
	testb	$4, %dil
	je	.L589
	xorl	%r8d, %r8d
	testb	$24, %dil
	jne	.L590
	movq	%rdi, %r8
	shrq	$5, %r8
	cmpb	$12, %r8b
	sbbq	%r8, %r8
	notq	%r8
	andl	$416, %r8d
.L590:
	andq	$-8161, %rdi
	orq	%r8, %rdi
	andq	$-8, %rdi
	orq	$4, %rdi
.L589:
	cmpq	%rdx, %rdi
	jne	.L989
	.p2align 4,,10
	.p2align 3
.L585:
	cmpq	104(%r12), %rsi
	je	.L990
.L672:
	movq	40(%r15), %rax
	movq	%rax, (%rsi)
	addq	$8, 96(%r12)
.L604:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	$1, %ebx
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	subq	$1, -64(%rbp)
.L580:
	cmpq	%r14, %r13
	jne	.L577
	testb	%bl, %bl
	jne	.L575
	.p2align 4,,10
	.p2align 3
.L975:
	movq	(%r12), %rax
	movq	%r12, %r15
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%rax, -144(%rbp)
	movq	-184(%rbp), %rax
	movq	$0, -136(%rbp)
	movq	16(%rax), %rbx
	movq	8(%rax), %r12
	movq	%rbx, -168(%rbp)
	cmpq	%r12, %rbx
	je	.L579
	movq	%r15, -224(%rbp)
	movq	%r14, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L644:
	movq	(%r12), %rbx
	movq	(%rbx), %rdi
	testb	$7, %dil
	je	.L606
	movl	%edi, %r9d
	movq	%rdi, %rax
	andl	$4, %r9d
	je	.L607
	xorl	%edx, %edx
	testb	$24, %dil
	jne	.L608
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L608:
	movq	%rdi, %rax
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L607:
	movq	8(%rbx), %r8
	movl	%r8d, %r14d
	movq	%r8, %rdx
	andl	$4, %r14d
	je	.L609
	xorl	%ecx, %ecx
	testb	$24, %r8b
	jne	.L610
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L610:
	movq	%r8, %rdx
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L609:
	cmpq	%rax, %rdx
	je	.L606
	movq	-88(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L606
	movq	%rdi, %rax
	movq	%rdi, %r11
	movq	%rdi, %r13
	movq	-200(%rbp), %r15
	shrq	$5, %rax
	shrq	$3, %r11
	andq	$-8161, %r13
	movq	%rax, -232(%rbp)
	andl	$3, %r11d
	movb	%al, -176(%rbp)
	movq	%r8, %rax
	shrq	$3, %rax
	andl	$3, %eax
	movl	%eax, -184(%rbp)
	movq	%r8, %rax
	shrq	$5, %rax
	movq	%rax, -240(%rbp)
	movb	%al, -216(%rbp)
	movq	%r8, %rax
	andq	$-8161, %rax
	movq	%rax, -192(%rbp)
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L991:
	movq	40(%rsi), %rax
	testb	$4, %al
	je	.L618
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L619
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L619:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L618:
	movq	%r8, %rdx
	testl	%r14d, %r14d
	je	.L625
	movl	-184(%rbp), %r10d
	xorl	%edx, %edx
	testl	%r10d, %r10d
	jne	.L621
	cmpb	$12, -216(%rbp)
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L621:
	orq	-192(%rbp), %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L625:
	cmpq	%rax, %rdx
	seta	%al
	testb	%al, %al
	jne	.L627
.L992:
	movq	%rsi, %r15
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L612
.L611:
	movq	32(%rsi), %rax
	movl	%eax, %r10d
	movq	%rax, %rcx
	andl	$4, %r10d
	je	.L613
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L614
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L614:
	movq	%rax, %rcx
	andq	$-8161, %rcx
	orq	%rdx, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L613:
	movq	%rdi, %rdx
	testl	%r9d, %r9d
	je	.L615
	xorl	%edx, %edx
	testl	%r11d, %r11d
	jne	.L616
	cmpb	$12, -176(%rbp)
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L616:
	orq	%r13, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L615:
	cmpq	%rcx, %rdx
	je	.L991
	testl	%r10d, %r10d
	je	.L623
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L624
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L624:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L623:
	movq	%rdi, %rdx
	testl	%r9d, %r9d
	je	.L625
	xorl	%edx, %edx
	testl	%r11d, %r11d
	jne	.L626
	cmpb	$12, -176(%rbp)
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L626:
	orq	%r13, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
	cmpq	%rax, %rdx
	seta	%al
	testb	%al, %al
	je	.L992
.L627:
	movq	24(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.L611
.L612:
	cmpq	-200(%rbp), %r15
	je	.L606
	movq	%rdi, %rdx
	testl	%r9d, %r9d
	je	.L629
	xorl	%eax, %eax
	testl	%r11d, %r11d
	je	.L993
.L630:
	movq	%rdi, %rdx
	andq	$-8161, %rdx
	orq	%rax, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L629:
	movq	32(%r15), %rax
	movl	%eax, %r10d
	movq	%rax, %rcx
	andl	$4, %r10d
	je	.L631
	xorl	%esi, %esi
	testb	$24, %al
	je	.L994
.L632:
	movq	%rax, %rcx
	andq	$-8161, %rcx
	orq	%rsi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L631:
	cmpq	%rdx, %rcx
	jne	.L633
	movq	%r8, %rdx
	testl	%r14d, %r14d
	je	.L634
	movl	-184(%rbp), %ecx
	xorl	%edx, %edx
	testl	%ecx, %ecx
	jne	.L635
	cmpb	$12, -240(%rbp)
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L635:
	movq	%r8, %rax
	andq	$-8161, %rax
	orq	%rax, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L634:
	movq	40(%r15), %rax
	testb	$4, %al
	jne	.L981
.L641:
	cmpq	%rdx, %rax
	seta	%al
	testb	%al, %al
	jne	.L606
	testl	%r9d, %r9d
	je	.L675
	xorl	%eax, %eax
	testl	%r11d, %r11d
	jne	.L646
	cmpb	$12, -232(%rbp)
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L646:
	andq	$-8161, %rdi
	orq	%rax, %rdi
	andq	$-8, %rdi
	orq	$4, %rdi
.L675:
	testl	%r14d, %r14d
	je	.L647
	movl	-184(%rbp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L648
	cmpb	$12, -240(%rbp)
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L648:
	andq	$-8161, %r8
	orq	%rax, %r8
	andq	$-8, %r8
	orq	$4, %r8
.L647:
	cmpq	%rdi, %r8
	je	.L649
	movq	-224(%rbp), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L995
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L651:
	movq	(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rbx), %rdx
	movq	%rdx, 8(%rax)
	movq	-128(%rbp), %rsi
	movq	%rax, -152(%rbp)
	movq	-120(%rbp), %rax
	cmpq	-136(%rbp), %rsi
	je	.L652
.L653:
	cmpq	%rax, %rsi
	je	.L656
	movq	-152(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -128(%rbp)
.L649:
	movq	$0, 8(%rbx)
	movq	$0, (%rbx)
	.p2align 4,,10
	.p2align 3
.L606:
	addq	$8, %r12
	cmpq	%r12, -168(%rbp)
	jne	.L644
	movq	-224(%rbp), %r15
	movq	-136(%rbp), %rax
	cmpq	%rax, -128(%rbp)
	je	.L579
	movq	-208(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L996
.L657:
	leaq	-144(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13MoveOptimizer13CompressMovesEPNS1_12ParallelMoveEPNS0_10ZoneVectorIPNS1_12MoveOperandsEEE
	movq	-128(%rbp), %r13
	movq	-136(%rbp), %r12
	leaq	-152(%rbp), %r14
	cmpq	%r12, %r13
	jne	.L664
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L997:
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L663:
	addq	$8, %r12
	cmpq	%r12, %r13
	je	.L665
.L664:
	movq	(%r12), %rax
	movq	%rax, -152(%rbp)
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	jne	.L997
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L998:
	xorl	%r8d, %r8d
.L584:
	andq	$-8161, %rdi
	orq	%r8, %rdi
	andq	$-8, %rdi
	orq	$4, %rdi
.L582:
	cmpq	%rdi, %rdx
	je	.L585
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L580
.L586:
	movq	(%rax), %rdi
	testb	$4, %dil
	je	.L582
	testb	$24, %dil
	jne	.L998
	movq	%rdi, %r8
	shrq	$5, %r8
	cmpb	$12, %r8b
	sbbq	%r8, %r8
	notq	%r8
	andl	$416, %r8d
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L999:
	xorl	%r8d, %r8d
.L594:
	andq	$-8161, %rdi
	orq	%r8, %rdi
	andq	$-8, %rdi
	orq	$4, %rdi
.L592:
	cmpq	%rdi, %rdx
	je	.L585
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L580
.L595:
	movq	(%rax), %rdi
	testb	$4, %dil
	je	.L592
	testb	$24, %dil
	jne	.L999
	movq	%rdi, %r8
	shrq	$5, %r8
	cmpb	$12, %r8b
	sbbq	%r8, %r8
	notq	%r8
	andl	$416, %r8d
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L587:
	testl	%edi, %edi
	je	.L596
	orq	$4, %rdx
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L1000:
	xorl	%r8d, %r8d
.L599:
	andq	$-8161, %rdi
	orq	%r8, %rdi
	andq	$-8, %rdi
	orq	$4, %rdi
.L597:
	cmpq	%rdi, %rdx
	je	.L585
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L580
.L600:
	movq	(%rax), %rdi
	testb	$4, %dil
	je	.L597
	testb	$24, %dil
	jne	.L1000
	movq	%rdi, %r8
	shrq	$5, %r8
	cmpb	$12, %r8b
	sbbq	%r8, %r8
	notq	%r8
	andl	$416, %r8d
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L596:
	orq	$420, %rdx
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L1001:
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L580
.L603:
	movq	(%rax), %rdi
	testb	$4, %dil
	je	.L601
	xorl	%r8d, %r8d
	testb	$24, %dil
	jne	.L602
	movq	%rdi, %r8
	shrq	$5, %r8
	cmpb	$12, %r8b
	sbbq	%r8, %r8
	notq	%r8
	andl	$416, %r8d
.L602:
	andq	$-8161, %rdi
	orq	%r8, %rdi
	andq	$-8, %rdi
	orq	$4, %rdi
.L601:
	cmpq	%rdi, %rdx
	jne	.L1001
	cmpq	104(%r12), %rsi
	jne	.L672
	.p2align 4,,10
	.p2align 3
.L990:
	movq	-168(%rbp), %rdi
	leaq	40(%r15), %rdx
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L481:
	movq	-168(%rbp), %rdi
	movq	%r13, %rdx
	addq	$1, %r12
	addq	$8, %r13
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movl	4(%rbx), %eax
	movzbl	%al, %ecx
	movq	%rcx, %rdx
	cmpq	%r12, %rcx
	ja	.L483
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L500:
	movq	-88(%rbp), %r14
	testq	%r14, %r14
	je	.L799
	movq	%rbx, %rax
	movq	%rbx, %r10
	shrq	$5, %rax
	shrq	$3, %r10
	movb	%al, -192(%rbp)
	movq	%r12, %rax
	andl	$3, %r10d
	shrq	$3, %rax
	andl	$3, %eax
	movl	%eax, -216(%rbp)
	movq	%r12, %rax
	shrq	$5, %rax
	movb	%al, -232(%rbp)
	movq	%rbx, %rax
	andq	$-8161, %rax
	movq	%rax, -176(%rbp)
	movq	%r12, %rax
	andq	$-8161, %rax
	movq	%rax, -224(%rbp)
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	%r12, %rcx
	testl	%r9d, %r9d
	je	.L529
	movl	-216(%rbp), %r11d
	xorl	%ecx, %ecx
	testl	%r11d, %r11d
	jne	.L530
	cmpb	$12, -232(%rbp)
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L530:
	orq	-224(%rbp), %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L529:
	movq	40(%r14), %rdx
	testb	$4, %dl
	je	.L531
	xorl	%r11d, %r11d
	testb	$24, %dl
	jne	.L532
	movq	%rdx, %r11
	shrq	$5, %r11
	cmpb	$12, %r11b
	sbbq	%r11, %r11
	notq	%r11
	andl	$416, %r11d
.L532:
	andq	$-8161, %rdx
	orq	%r11, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L531:
	cmpq	%rcx, %rdx
	seta	%cl
	testb	%cl, %cl
	je	.L538
.L1003:
	movq	16(%r14), %rdx
	testq	%rdx, %rdx
	je	.L523
.L1004:
	movq	%rdx, %r14
.L671:
	movq	%rbx, %rdx
	testl	%esi, %esi
	je	.L524
	xorl	%edx, %edx
	testl	%r10d, %r10d
	jne	.L525
	cmpb	$12, -192(%rbp)
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L525:
	orq	-176(%rbp), %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L524:
	movq	32(%r14), %rax
	movl	%eax, %edi
	movq	%rax, %rcx
	andl	$4, %edi
	je	.L526
	xorl	%r11d, %r11d
	testb	$24, %al
	jne	.L527
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%r11, %r11
	notq	%r11
	andl	$416, %r11d
.L527:
	movq	%rax, %rcx
	andq	$-8161, %rcx
	orq	%r11, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L526:
	cmpq	%rdx, %rcx
	je	.L1002
	movq	%rbx, %rdx
	testl	%esi, %esi
	je	.L534
	xorl	%edx, %edx
	testl	%r10d, %r10d
	jne	.L535
	cmpb	$12, -192(%rbp)
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L535:
	orq	-176(%rbp), %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L534:
	movq	%rax, %rcx
	testl	%edi, %edi
	je	.L536
	xorl	%r11d, %r11d
	testb	$24, %al
	jne	.L537
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%r11, %r11
	notq	%r11
	andl	$416, %r11d
.L537:
	movq	%rax, %rcx
	andq	$-8161, %rcx
	orq	%r11, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L536:
	cmpq	%rdx, %rcx
	seta	%cl
	testb	%cl, %cl
	jne	.L1003
.L538:
	movq	24(%r14), %rdx
	testq	%rdx, %rdx
	jne	.L1004
.L523:
	movq	%r14, %rdx
	testb	%cl, %cl
	jne	.L540
	movq	%rax, %rcx
	testl	%edi, %edi
	je	.L542
.L1013:
	xorl	%r10d, %r10d
	testb	$24, %al
	jne	.L543
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%r10, %r10
	notq	%r10
	andl	$416, %r10d
.L543:
	movq	%rax, %rcx
	andq	$-8161, %rcx
	orq	%r10, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L542:
	movq	%rbx, %r10
	testl	%esi, %esi
	je	.L544
	xorl	%r11d, %r11d
	testb	$24, %bl
	jne	.L545
	shrq	$5, %r10
	cmpb	$12, %r10b
	sbbq	%r11, %r11
	notq	%r11
	andl	$416, %r11d
.L545:
	movq	%rbx, %r10
	andq	$-8161, %r10
	orq	%r11, %r10
	andq	$-8, %r10
	orq	$4, %r10
.L544:
	cmpq	%rcx, %r10
	jne	.L546
	movq	40(%r14), %rax
	testb	$4, %al
	je	.L547
	xorl	%ecx, %ecx
	testb	$24, %al
	je	.L1005
.L548:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L547:
	movq	%r12, %rcx
	testl	%r9d, %r9d
	je	.L554
	xorl	%edi, %edi
	testb	$24, %r12b
	je	.L1006
.L550:
	movq	%r12, %rcx
	andq	$-8161, %rcx
	orq	%rdi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L554:
	cmpq	%rax, %rcx
	seta	%al
	testb	%al, %al
	je	.L495
	testq	%rdx, %rdx
	je	.L495
.L676:
	movl	$1, %r14d
	cmpq	-200(%rbp), %rdx
	jne	.L1007
.L557:
	movq	-112(%rbp), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$47, %rax
	jbe	.L1008
	leaq	48(%rsi), %rax
	movq	%rax, 16(%rdi)
.L573:
	movq	%r12, %xmm1
	movq	%rbx, %xmm0
	movl	%r14d, %edi
	addq	$8, %r13
	punpcklqdq	%xmm1, %xmm0
	movq	-200(%rbp), %rcx
	movq	%r8, -176(%rbp)
	movups	%xmm0, 32(%rsi)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -64(%rbp)
	movq	-176(%rbp), %r8
	cmpq	%r13, %r15
	jne	.L574
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L633:
	movq	%rdi, %rdx
	testl	%r9d, %r9d
	je	.L639
	xorl	%ecx, %ecx
	testl	%r11d, %r11d
	jne	.L640
	cmpb	$12, -232(%rbp)
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L640:
	movq	%rdi, %rdx
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L639:
	testl	%r10d, %r10d
	je	.L641
.L981:
	xorl	%ecx, %ecx
	testb	$24, %al
	jne	.L642
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L642:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
	jmp	.L641
.L506:
	andq	$-8168, %rdi
	testl	%edx, %edx
	je	.L515
	orq	$4, %rdi
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L1010:
	xorl	%r10d, %r10d
.L518:
	andq	$-8161, %rdx
	orq	%r10, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L516:
	cmpq	%rdx, %rdi
	je	.L495
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L500
.L519:
	movq	(%rax), %rdx
	testb	$4, %dl
	je	.L516
	testb	$24, %dl
	jne	.L1010
	movq	%rdx, %r10
	shrq	$5, %r10
	cmpb	$12, %r10b
	sbbq	%r10, %r10
	notq	%r10
	andl	$416, %r10d
	jmp	.L518
.L546:
	testl	%edi, %edi
	je	.L552
	xorl	%ecx, %ecx
	testb	$24, %al
	je	.L1011
.L553:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L552:
	movq	%rbx, %rcx
	testl	%esi, %esi
	je	.L554
	xorl	%edi, %edi
	testb	$24, %bl
	jne	.L555
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
.L555:
	movq	%rbx, %rcx
	andq	$-8161, %rcx
	orq	%rdi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
	jmp	.L554
.L515:
	orq	$420, %rdi
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L1012:
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L500
.L522:
	movq	(%rax), %rdx
	testb	$4, %dl
	je	.L520
	xorl	%r10d, %r10d
	testb	$24, %dl
	jne	.L521
	movq	%rdx, %r10
	shrq	$5, %r10
	cmpb	$12, %r10b
	sbbq	%r10, %r10
	notq	%r10
	andl	$416, %r10d
.L521:
	andq	$-8161, %rdx
	orq	%r10, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L520:
	cmpq	%rdx, %rdi
	jne	.L1012
	jmp	.L495
.L994:
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
	jmp	.L632
.L993:
	cmpb	$12, -232(%rbp)
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
	jmp	.L630
.L799:
	movq	-200(%rbp), %r14
.L540:
	movq	-80(%rbp), %rdx
	cmpq	%r14, %rdx
	je	.L676
	movq	%r14, %rdi
	movq	%r8, -216(%rbp)
	movl	%esi, -192(%rbp)
	movl	%r9d, -176(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r14, %rdx
	movq	-216(%rbp), %r8
	movl	-192(%rbp), %esi
	movq	%rax, %rcx
	movq	32(%rax), %rax
	movl	-176(%rbp), %r9d
	movq	%rcx, %r14
	movl	%eax, %edi
	movq	%rax, %rcx
	andl	$4, %edi
	testl	%edi, %edi
	je	.L542
	jmp	.L1013
.L493:
	movq	-168(%rbp), %rdi
	leaq	8(%rcx), %rdx
	movq	%r8, %rsi
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L488
.L579:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L467
	leaq	-112(%rbp), %rcx
.L670:
	movq	24(%rdx), %rax
	testq	%rax, %rax
	je	.L668
.L669:
	movq	24(%rax), %rsi
	movq	%rcx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler12_GLOBAL__N_17MoveKeyES4_St9_IdentityIS4_ENS3_14MoveKeyCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L669
.L668:
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L670
	jmp	.L467
.L996:
	movq	8(%r15), %rax
	movq	8(%rax), %r12
	movq	16(%r12), %rbx
	movq	24(%r12), %rax
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L1014
	leaq	32(%rbx), %rax
	movq	%rax, 16(%r12)
.L659:
	movq	-208(%rbp), %rax
	movq	%r12, (%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 8(%rax)
	jmp	.L657
.L1005:
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
	jmp	.L548
.L1011:
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
	jmp	.L553
.L1006:
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
	jmp	.L550
.L995:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L651
.L656:
	leaq	-152(%rbp), %rdx
	leaq	-144(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L649
.L652:
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	$31, %rdx
	ja	.L653
	movq	-144(%rbp), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$31, %rax
	jbe	.L1015
	leaq	32(%rsi), %rax
	movq	%rax, 16(%rdi)
.L655:
	movq	%rsi, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rax, -120(%rbp)
	jmp	.L653
.L1007:
	movq	%rbx, %rcx
	testl	%esi, %esi
	je	.L558
	xorl	%eax, %eax
	testb	$24, %bl
	jne	.L559
	movq	%rbx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L559:
	movq	%rbx, %rcx
	andq	$-8161, %rcx
	orq	%rax, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L558:
	movq	32(%rdx), %rax
	movl	%eax, %r10d
	movq	%rax, %rdi
	andl	$4, %r10d
	je	.L560
	xorl	%r11d, %r11d
	testb	$24, %al
	jne	.L561
	shrq	$5, %rdi
	cmpb	$12, %dil
	sbbq	%r11, %r11
	notq	%r11
	andl	$416, %r11d
.L561:
	movq	%rax, %rdi
	andq	$-8161, %rdi
	orq	%r11, %rdi
	andq	$-8, %rdi
	orq	$4, %rdi
.L560:
	cmpq	%rcx, %rdi
	jne	.L562
	movq	%r12, %rcx
	testl	%r9d, %r9d
	je	.L563
	xorl	%eax, %eax
	testb	$24, %r12b
	jne	.L564
	movq	%r12, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L564:
	movq	%r12, %rcx
	andq	$-8161, %rcx
	orq	%rax, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L563:
	movq	40(%rdx), %rax
	testb	$4, %al
	je	.L570
.L979:
	xorl	%esi, %esi
	testb	$24, %al
	jne	.L571
	movq	%rax, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L571:
	andq	$-8161, %rax
	orq	%rsi, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L570:
	xorl	%r14d, %r14d
	cmpq	%rcx, %rax
	seta	%r14b
	jmp	.L557
.L562:
	movq	%rbx, %rcx
	testl	%esi, %esi
	je	.L568
	xorl	%ecx, %ecx
	testb	$24, %bl
	jne	.L569
	movq	%rbx, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L569:
	movq	%rbx, %rsi
	andq	$-8161, %rsi
	orq	%rsi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L568:
	testl	%r10d, %r10d
	je	.L570
	jmp	.L979
.L1008:
	movl	$48, %esi
	movq	%r8, -192(%rbp)
	movq	%rdx, -176(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-176(%rbp), %rdx
	movq	-192(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L573
.L1014:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L659
.L1015:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	leaq	32(%rax), %rax
	jmp	.L655
.L984:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10754:
	.size	_ZN2v88internal8compiler13MoveOptimizer12MigrateMovesEPNS1_11InstructionES4_, .-_ZN2v88internal8compiler13MoveOptimizer12MigrateMovesEPNS1_11InstructionES4_
	.section	.text._ZN2v88internal8compiler13MoveOptimizer13CompressBlockEPNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13MoveOptimizer13CompressBlockEPNS1_16InstructionBlockE
	.type	_ZN2v88internal8compiler13MoveOptimizer13CompressBlockEPNS1_16InstructionBlockE, @function
_ZN2v88internal8compiler13MoveOptimizer13CompressBlockEPNS1_16InstructionBlockE:
.LFB10766:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movslq	112(%rsi), %r13
	movl	116(%rsi), %ebx
	movq	208(%rcx), %rsi
	movq	%r13, %rdx
	movq	%rsi, %rax
	subq	216(%rcx), %rax
	sarq	$3, %rax
	addq	%r13, %rax
	js	.L1017
	cmpq	$63, %rax
	jle	.L1029
	movq	%rax, %rsi
	sarq	$6, %rsi
.L1020:
	movq	232(%rcx), %rcx
	movq	%rsi, %rdi
	salq	$6, %rdi
	movq	(%rcx,%rsi,8), %rcx
	subq	%rdi, %rax
	leaq	(%rcx,%rax,8), %rax
.L1019:
	movq	(%rax), %r15
	movq	%r14, %rdi
	movl	%edx, -52(%rbp)
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13MoveOptimizer27RemoveClobberedDestinationsEPNS1_11InstructionE
	movl	-52(%rbp), %edx
	leal	1(%rdx), %r12d
	cmpl	%ebx, %r12d
	jge	.L1016
	subl	$2, %ebx
	movslq	%r12d, %r12
	subl	%edx, %ebx
	leaq	2(%rbx,%r13), %rbx
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1030:
	cmpq	$63, %rax
	jg	.L1023
	leaq	(%rcx,%r12,8), %rax
.L1024:
	movq	(%rax), %r13
	movq	%r15, %rdx
	movq	%r14, %rdi
	addq	$1, %r12
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13MoveOptimizer12MigrateMovesEPNS1_11InstructionES4_
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13MoveOptimizer27RemoveClobberedDestinationsEPNS1_11InstructionE
	cmpq	%r12, %rbx
	je	.L1016
	movq	%r13, %r15
.L1026:
	movq	8(%r14), %rdx
	movq	208(%rdx), %rcx
	movq	%rcx, %rax
	subq	216(%rdx), %rax
	sarq	$3, %rax
	addq	%r12, %rax
	jns	.L1030
	movq	%rax, %rcx
	notq	%rcx
	shrq	$6, %rcx
	notq	%rcx
.L1025:
	movq	232(%rdx), %rdx
	movq	%rcx, %rsi
	salq	$6, %rsi
	movq	(%rdx,%rcx,8), %rdx
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %rax
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L1029:
	leaq	(%rsi,%r13,8), %rax
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1023:
	movq	%rax, %rcx
	sarq	$6, %rcx
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1016:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1017:
	.cfi_restore_state
	movq	%rax, %rsi
	notq	%rsi
	shrq	$6, %rsi
	notq	%rsi
	jmp	.L1020
	.cfi_endproc
.LFE10766:
	.size	_ZN2v88internal8compiler13MoveOptimizer13CompressBlockEPNS1_16InstructionBlockE, .-_ZN2v88internal8compiler13MoveOptimizer13CompressBlockEPNS1_16InstructionBlockE
	.section	.rodata._ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text.unlikely._ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE,"ax",@progbits
	.align 2
.LCOLDB2:
	.section	.text._ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE,"ax",@progbits
.LHOTB2:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE
	.type	_ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE, @function
_ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE:
.LFB10768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -176(%rbp)
	movq	48(%rsi), %r11
	movq	%rsi, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rsi), %rax
	movq	%r11, -200(%rbp)
	cmpq	%r11, %rax
	je	.L1031
	movq	8(%rdi), %rdi
	movq	%rax, -152(%rbp)
	movq	%rax, %r9
	movq	16(%rdi), %rdx
	movq	8(%rdx), %r10
	movq	16(%rdx), %r8
	subq	%r10, %r8
	sarq	$3, %r8
.L1041:
	movslq	(%r9), %rsi
	cmpq	%r8, %rsi
	jnb	.L1472
	movq	(%r10,%rsi,8), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	cmpq	$7, %rax
	ja	.L1031
	movl	116(%rdx), %edx
	movq	208(%rdi), %rcx
	subl	$1, %edx
	movq	%rcx, %rax
	subq	216(%rdi), %rax
	sarq	$3, %rax
	movslq	%edx, %rdx
	addq	%rdx, %rax
	js	.L1035
	cmpq	$63, %rax
	jg	.L1036
	leaq	(%rcx,%rdx,8), %rax
.L1037:
	movq	(%rax), %rcx
	movl	4(%rcx), %eax
	movl	%eax, %edx
	movl	%eax, %esi
	shrl	$24, %edx
	andl	$1073742079, %esi
	andl	$63, %edx
	orl	%esi, %edx
	jne	.L1031
	shrl	$8, %eax
	movzwl	%ax, %esi
	testl	%esi, %esi
	je	.L1039
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1040:
	movl	40(%rcx,%rdx,8), %eax
	andl	$7, %eax
	subl	$2, %eax
	cmpl	$1, %eax
	ja	.L1031
	addq	$1, %rdx
	cmpq	%rsi, %rdx
	jne	.L1040
.L1039:
	addq	$4, %r9
	cmpq	%r9, %r11
	jne	.L1041
	movq	-176(%rbp), %rax
	movl	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	(%rax), %rax
	movq	$0, -64(%rbp)
	movq	$0, -216(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L1227:
	movq	16(%rdi), %rdx
	movq	-152(%rbp), %rax
	movslq	(%rax), %rsi
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1468
	movq	(%rax,%rsi,8), %rax
	movq	208(%rdi), %rcx
	movl	116(%rax), %edx
	movq	%rcx, %rax
	subq	216(%rdi), %rax
	sarq	$3, %rax
	subl	$1, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rax
	js	.L1044
	cmpq	$63, %rax
	jg	.L1045
	leaq	(%rcx,%rdx,8), %rax
.L1046:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1148
	movq	16(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rbx, -136(%rbp)
	cmpq	%rbx, %rax
	je	.L1148
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L1110:
	movq	(%r15), %rdx
	movq	(%rdx), %rbx
	testb	$7, %bl
	je	.L1051
	movl	%ebx, %r14d
	movq	%rbx, %rax
	andl	$4, %r14d
	je	.L1052
	xorl	%ecx, %ecx
	testb	$24, %bl
	jne	.L1053
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L1053:
	movq	%rbx, %rax
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1052:
	movq	8(%rdx), %r13
	movl	%r13d, %r8d
	movq	%r13, %rdx
	andl	$4, %r8d
	je	.L1054
	xorl	%ecx, %ecx
	testb	$24, %r13b
	jne	.L1055
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L1055:
	movq	%r13, %rdx
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1054:
	cmpq	%rax, %rdx
	je	.L1051
	movq	-112(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	%rax, %rsi
	movq	%rax, -144(%rbp)
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$55, %rax
	jbe	.L1473
	movq	%rsi, %rax
	addq	$56, %rax
	movq	%rax, 16(%rdi)
.L1057:
	movq	-144(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r13, %xmm1
	movq	-88(%rbp), %r12
	punpcklqdq	%xmm1, %xmm0
	movl	$1, 48(%rax)
	movups	%xmm0, 32(%rax)
	testq	%r12, %r12
	je	.L1058
	movq	%r13, %rax
	movq	%rbx, %rdi
	movq	%rbx, %r10
	movq	%rbx, %r9
	shrq	$3, %rax
	shrq	$3, %rdi
	andq	$-8161, %r9
	andl	$3, %eax
	andl	$3, %edi
	shrq	$5, %r10
	movl	%eax, -160(%rbp)
	movq	%r13, %rax
	shrq	$5, %rax
	movb	%al, -185(%rbp)
	movq	%r13, %rax
	andq	$-8161, %rax
	movq	%rax, -168(%rbp)
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1474:
	movq	%r13, %rcx
	testl	%r8d, %r8d
	je	.L1066
	movl	-160(%rbp), %r11d
	xorl	%ecx, %ecx
	testl	%r11d, %r11d
	jne	.L1067
	cmpb	$12, -185(%rbp)
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L1067:
	orq	-168(%rbp), %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1066:
	movq	40(%r12), %rax
	testb	$4, %al
	je	.L1068
	xorl	%r11d, %r11d
	testb	$24, %al
	jne	.L1069
	movq	%rax, %r11
	shrq	$5, %r11
	cmpb	$12, %r11b
	sbbq	%r11, %r11
	notq	%r11
	andl	$416, %r11d
.L1069:
	andq	$-8161, %rax
	orq	%r11, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1068:
	cmpq	%rcx, %rax
	seta	%cl
	testb	%cl, %cl
	je	.L1075
.L1475:
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L1060
.L1476:
	movq	%rax, %r12
.L1059:
	movq	%rbx, %rax
	testl	%r14d, %r14d
	je	.L1061
	xorl	%eax, %eax
	testl	%edi, %edi
	jne	.L1062
	cmpb	$12, %r10b
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L1062:
	orq	%r9, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1061:
	movq	32(%r12), %rdx
	movl	%edx, %esi
	movq	%rdx, %rcx
	andl	$4, %esi
	je	.L1063
	xorl	%r11d, %r11d
	testb	$24, %dl
	jne	.L1064
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%r11, %r11
	notq	%r11
	andl	$416, %r11d
.L1064:
	movq	%rdx, %rcx
	andq	$-8161, %rcx
	orq	%r11, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1063:
	cmpq	%rax, %rcx
	je	.L1474
	movq	%rbx, %rax
	testl	%r14d, %r14d
	je	.L1071
	xorl	%eax, %eax
	testl	%edi, %edi
	jne	.L1072
	cmpb	$12, %r10b
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L1072:
	orq	%r9, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1071:
	movq	%rdx, %rcx
	testl	%esi, %esi
	je	.L1073
	xorl	%r11d, %r11d
	testb	$24, %dl
	jne	.L1074
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%r11, %r11
	notq	%r11
	andl	$416, %r11d
.L1074:
	movq	%rdx, %rcx
	andq	$-8161, %rcx
	orq	%r11, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1073:
	cmpq	%rax, %rcx
	seta	%cl
	testb	%cl, %cl
	jne	.L1475
.L1075:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L1476
.L1060:
	movq	%r12, %r9
	testb	%cl, %cl
	jne	.L1477
.L1078:
	movq	%rdx, %rax
	testl	%esi, %esi
	je	.L1079
	xorl	%ecx, %ecx
	testb	$24, %dl
	jne	.L1080
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L1080:
	movq	%rdx, %rax
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1079:
	movq	%rbx, %rcx
	testl	%r14d, %r14d
	je	.L1081
	xorl	%edi, %edi
	testb	$24, %bl
	jne	.L1082
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
.L1082:
	movq	%rbx, %rcx
	andq	$-8161, %rcx
	orq	%rdi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1081:
	cmpq	%rax, %rcx
	jne	.L1083
	movq	40(%r12), %rax
	testb	$4, %al
	je	.L1084
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L1085
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1085:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1084:
	movq	%r13, %rdx
	testl	%r8d, %r8d
	je	.L1086
	xorl	%ecx, %ecx
	testb	$24, %r13b
	jne	.L1087
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L1087:
	movq	%r13, %rdx
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1086:
	cmpq	%rax, %rdx
	seta	%al
.L1088:
	testb	%al, %al
	je	.L1093
	testq	%r9, %r9
	je	.L1478
.L1228:
	movl	$1, %edi
	cmpq	-208(%rbp), %r9
	jne	.L1479
.L1095:
	movq	-208(%rbp), %rcx
	movq	-144(%rbp), %rsi
	movq	%r9, %rdx
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L1051:
	addq	$8, %r15
	cmpq	%r15, -136(%rbp)
	jne	.L1110
	addq	$4, -152(%rbp)
	movq	-152(%rbp), %rax
	cmpq	%rax, -200(%rbp)
	je	.L1480
	movq	-176(%rbp), %rax
	movq	8(%rax), %rdi
	jmp	.L1227
.L1291:
	movb	$1, -185(%rbp)
.L1150:
	movq	-176(%rbp), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rbx
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1481
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rbx)
.L1155:
	movq	%rbx, (%rax)
	movq	%rax, %r15
	movq	-184(%rbp), %rbx
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	-168(%rbp), %rax
	movq	%r15, 8(%rax)
	movq	40(%rbx), %rax
	movq	48(%rbx), %rbx
	movq	%rbx, -224(%rbp)
	cmpq	%rax, %rbx
	jne	.L1152
.L1164:
	cmpb	$0, -185(%rbp)
	je	.L1482
.L1156:
	movq	-184(%rbp), %rsi
	movq	-176(%rbp), %rdi
	call	_ZN2v88internal8compiler13MoveOptimizer13CompressBlockEPNS1_16InstructionBlockE
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L1031
	leaq	-112(%rbp), %rcx
.L1219:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1217
.L1218:
	movq	24(%rdx), %rsi
	movq	%rcx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler12_GLOBAL__N_17MoveKeyESt4pairIKS4_jESt10_Select1stIS7_ENS3_14MoveKeyCompareENS1_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1218
.L1217:
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1219
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1483
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1036:
	.cfi_restore_state
	movq	%rax, %rdx
	sarq	$6, %rdx
.L1038:
	movq	232(%rdi), %rcx
	movq	%rdx, %rsi
	salq	$6, %rsi
	movq	(%rcx,%rdx,8), %rdx
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %rax
	jmp	.L1037
	.p2align 4,,10
	.p2align 3
.L1035:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1093:
	movl	48(%r12), %eax
	movq	-184(%rbp), %rdi
	addl	$1, %eax
	movl	%eax, 48(%r12)
	movq	48(%rdi), %rdx
	subq	40(%rdi), %rdx
	sarq	$2, %rdx
	cmpq	%rdx, %rax
	sete	%al
	movzbl	%al, %eax
	addq	%rax, -216(%rbp)
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1083:
	testl	%esi, %esi
	je	.L1089
	xorl	%eax, %eax
	testb	$24, %dl
	jne	.L1090
	movq	%rdx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L1090:
	andq	$-8161, %rdx
	orq	%rax, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1089:
	movq	%rbx, %rax
	testl	%r14d, %r14d
	je	.L1091
	xorl	%ecx, %ecx
	testb	$24, %bl
	jne	.L1092
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L1092:
	movq	%rbx, %rax
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1091:
	cmpq	%rdx, %rax
	seta	%al
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1477:
	cmpq	%r12, -80(%rbp)
	je	.L1484
.L1229:
	movq	%r12, %rdi
	movl	%r8d, -160(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %r9
	movl	-160(%rbp), %r8d
	movq	32(%rax), %rdx
	movq	%rax, %r12
	movl	%edx, %esi
	andl	$4, %esi
	jmp	.L1078
.L1045:
	movq	%rax, %rdx
	sarq	$6, %rdx
.L1047:
	movq	232(%rdi), %rcx
	movq	%rdx, %rsi
	salq	$6, %rsi
	movq	(%rcx,%rdx,8), %rdx
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %rax
	jmp	.L1046
.L1044:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L1047
.L1484:
	movq	%r12, %r9
	jmp	.L1228
.L1479:
	movq	%rbx, %rdx
	testl	%r14d, %r14d
	je	.L1096
	xorl	%eax, %eax
	testb	$24, %bl
	jne	.L1097
	movq	%rbx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L1097:
	movq	%rbx, %rdx
	andq	$-8161, %rdx
	orq	%rax, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1096:
	movq	32(%r9), %rax
	movl	%eax, %edi
	movq	%rax, %rcx
	andl	$4, %edi
	je	.L1098
	xorl	%esi, %esi
	testb	$24, %al
	jne	.L1099
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L1099:
	movq	%rax, %rcx
	andq	$-8161, %rcx
	orq	%rsi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1098:
	cmpq	%rdx, %rcx
	jne	.L1100
	testl	%r8d, %r8d
	je	.L1101
	xorl	%eax, %eax
	testb	$24, %r13b
	jne	.L1102
	movq	%r13, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L1102:
	andq	$-8161, %r13
	orq	%rax, %r13
	andq	$-8, %r13
	orq	$4, %r13
.L1101:
	movq	40(%r9), %rax
	testb	$4, %al
	je	.L1103
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L1104
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1104:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1103:
	cmpq	%r13, %rax
	seta	%dil
.L1105:
	movzbl	%dil, %edi
	jmp	.L1095
.L1058:
	movq	-208(%rbp), %rax
	movq	%rax, %r12
	cmpq	%rax, -80(%rbp)
	jne	.L1229
	movq	%rax, %r9
	movl	$1, %edi
	jmp	.L1095
.L1473:
	movl	$56, %esi
	movl	%r8d, -160(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-160(%rbp), %r8d
	movq	%rax, -144(%rbp)
	jmp	.L1057
.L1100:
	testl	%r14d, %r14d
	je	.L1106
	xorl	%edx, %edx
	testb	$24, %bl
	jne	.L1107
	movq	%rbx, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1107:
	andq	$-8161, %rbx
	orq	%rdx, %rbx
	andq	$-8, %rbx
	orq	$4, %rbx
.L1106:
	testl	%edi, %edi
	je	.L1108
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L1109
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1109:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1108:
	cmpq	%rbx, %rax
	seta	%dil
	jmp	.L1105
.L1148:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1031
	leaq	-112(%rbp), %rcx
.L1222:
	movq	24(%rdx), %rax
	testq	%rax, %rax
	je	.L1220
.L1221:
	movq	24(%rax), %rsi
	movq	%rcx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler12_GLOBAL__N_17MoveKeyESt4pairIKS4_jESt10_Select1stIS7_ENS3_14MoveKeyCompareENS1_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1221
.L1220:
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1222
	jmp	.L1031
.L1480:
	movq	-64(%rbp), %rsi
	cmpq	$0, -216(%rbp)
	sete	%dl
	testq	%rsi, %rsi
	sete	%al
	orb	%al, %dl
	jne	.L1148
	movq	-176(%rbp), %rax
	movq	-184(%rbp), %rbx
	movq	8(%rax), %rcx
	movslq	112(%rbx), %rdx
	movq	208(%rcx), %rdi
	movq	%rdi, %rax
	subq	216(%rcx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	js	.L1113
	cmpq	$63, %rax
	jg	.L1114
	leaq	(%rdi,%rdx,8), %rax
.L1115:
	movq	(%rax), %rax
	movq	%rax, -168(%rbp)
	cmpq	%rsi, -216(%rbp)
	je	.L1117
	movq	-176(%rbp), %rbx
	movq	56(%rbx), %rax
	leaq	48(%rbx), %r12
	cmpq	64(%rbx), %rax
	je	.L1118
	movq	%rax, 64(%rbx)
.L1118:
	movq	-80(%rbp), %r13
	movq	-176(%rbp), %r14
	leaq	-120(%rbp), %rbx
	cmpq	-208(%rbp), %r13
	je	.L1149
.L1119:
	movq	%r13, %rdi
	movq	%r13, %r15
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	-184(%rbp), %rdi
	movl	48(%r15), %edx
	movq	%rax, %r13
	movq	48(%rdi), %rax
	subq	40(%rdi), %rax
	sarq	$2, %rax
	cmpq	%rax, %rdx
	je	.L1122
	movq	40(%r15), %rax
	movq	64(%r14), %rsi
	movq	%rax, -120(%rbp)
	cmpq	72(%r14), %rsi
	je	.L1123
	movq	%rax, (%rsi)
	addq	$8, 64(%r14)
.L1124:
	movq	-208(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	subq	$1, -64(%rbp)
.L1122:
	cmpq	-208(%rbp), %r13
	jne	.L1119
	movq	%r12, -136(%rbp)
	movq	-208(%rbp), %r13
	movq	%r14, %rbx
.L1121:
	movq	-80(%rbp), %r15
	cmpq	%r13, %r15
	je	.L1467
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L1125:
	movq	%r15, %rdi
	movq	%r15, %r12
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	56(%rbx), %rdx
	movq	64(%rbx), %rsi
	movq	%rax, %r15
	cmpq	%rsi, %rdx
	je	.L1127
	movq	32(%r12), %rcx
	movq	%rcx, %rax
	movq	%rcx, %rdi
	shrq	$3, %rax
	shrq	$5, %rdi
	andl	$3, %eax
	testb	$4, %cl
	je	.L1133
	testl	%eax, %eax
	je	.L1134
	andq	$-8168, %rcx
	orq	$4, %rcx
	jmp	.L1138
.L1485:
	xorl	%edi, %edi
.L1137:
	andq	$-8161, %rax
	orq	%rdi, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1135:
	cmpq	%rcx, %rax
	je	.L1132
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	je	.L1127
.L1138:
	movq	(%rdx), %rax
	testb	$4, %al
	je	.L1135
	testb	$24, %al
	jne	.L1485
	movq	%rax, %rdi
	shrq	$5, %rdi
	cmpb	$12, %dil
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1486:
	xorl	%edi, %edi
.L1131:
	andq	$-8161, %rax
	orq	%rdi, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1129:
	cmpq	%rax, %rcx
	je	.L1132
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	je	.L1127
.L1133:
	movq	(%rdx), %rax
	testb	$4, %al
	je	.L1129
	testb	$24, %al
	jne	.L1486
	movq	%rax, %rdi
	shrq	$5, %rdi
	cmpb	$12, %dil
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
	jmp	.L1131
.L1139:
	movq	%rcx, %rdx
	orq	$420, %rdx
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1487:
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L1127
.L1146:
	movq	(%rax), %rcx
	testb	$4, %cl
	je	.L1144
	xorl	%edi, %edi
	testb	$24, %cl
	jne	.L1145
	movq	%rcx, %rdi
	shrq	$5, %rdi
	cmpb	$12, %dil
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
.L1145:
	andq	$-8161, %rcx
	orq	%rdi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1144:
	cmpq	%rcx, %rdx
	jne	.L1487
.L1132:
	cmpq	72(%rbx), %rsi
	je	.L1488
	movq	40(%r12), %rax
	movq	%rax, (%rsi)
	addq	$8, 64(%rbx)
.L1147:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %r14d
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	subq	$1, -64(%rbp)
.L1127:
	cmpq	%r13, %r15
	jne	.L1125
	testb	%r14b, %r14b
	jne	.L1121
.L1467:
	movq	%rbx, -176(%rbp)
	movq	%r13, -208(%rbp)
.L1149:
	cmpq	$0, -64(%rbp)
	je	.L1148
.L1117:
	movq	-168(%rbp), %rax
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1291
	movq	8(%rdx), %rbx
	cmpq	%rbx, 16(%rdx)
	je	.L1151
	movq	16(%rax), %r8
	movq	%rdx, 16(%rax)
	movq	%r8, %r15
	movq	%r8, 8(%rax)
	movq	%rdx, %r8
	testq	%r15, %r15
	je	.L1292
	movq	-184(%rbp), %rbx
	movb	$0, -185(%rbp)
	movq	40(%rbx), %rax
	movq	48(%rbx), %rbx
	movq	%rbx, -224(%rbp)
	cmpq	%rax, %rbx
	je	.L1489
.L1152:
	movq	%rax, -200(%rbp)
	movb	$1, -187(%rbp)
.L1163:
	movq	-176(%rbp), %rax
	movq	8(%rax), %rcx
	movq	-200(%rbp), %rax
	movq	16(%rcx), %rdx
	movslq	(%rax), %rsi
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1468
	movq	(%rax,%rsi,8), %rax
	movq	208(%rcx), %rsi
	movl	116(%rax), %edx
	movq	%rsi, %rax
	subq	216(%rcx), %rax
	sarq	$3, %rax
	subl	$1, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rax
	js	.L1158
	cmpq	$63, %rax
	jg	.L1159
	leaq	(%rsi,%rdx,8), %rax
.L1160:
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rbx
	movq	8(%rax), %r14
	movq	%rbx, -136(%rbp)
	cmpq	%r14, %rbx
	je	.L1204
.L1203:
	movq	(%r14), %rbx
	movq	(%rbx), %rdi
	testb	$7, %dil
	je	.L1165
	movl	%edi, %r9d
	movq	%rdi, %rax
	andl	$4, %r9d
	je	.L1166
	xorl	%edx, %edx
	testb	$24, %dil
	jne	.L1167
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1167:
	movq	%rdi, %rax
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1166:
	movq	8(%rbx), %r8
	movl	%r8d, %r12d
	movq	%r8, %rdx
	andl	$4, %r12d
	je	.L1168
	xorl	%ecx, %ecx
	testb	$24, %r8b
	jne	.L1169
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L1169:
	movq	%r8, %rdx
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1168:
	cmpq	%rax, %rdx
	je	.L1165
	movq	-88(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L1165
	movq	%rdi, %rax
	movq	%rdi, %r11
	movq	%r8, %rcx
	movq	%rbx, -240(%rbp)
	shrq	$5, %rax
	shrq	$3, %r11
	andq	$-8161, %rcx
	movq	-208(%rbp), %r13
	movq	%rax, -216(%rbp)
	andl	$3, %r11d
	movb	%al, -144(%rbp)
	movq	%r8, %rax
	shrq	$3, %rax
	movq	%rcx, -160(%rbp)
	andl	$3, %eax
	movl	%eax, -152(%rbp)
	movq	%r8, %rax
	shrq	$5, %rax
	movq	%rax, -232(%rbp)
	movb	%al, -186(%rbp)
	movq	%rdi, %rax
	andq	$-8161, %rax
	movq	%rax, %rbx
	jmp	.L1170
.L1490:
	movq	40(%rsi), %rax
	testb	$4, %al
	je	.L1177
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L1178
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1178:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1177:
	movq	%r8, %rdx
	testl	%r12d, %r12d
	je	.L1184
	movl	-152(%rbp), %r10d
	xorl	%edx, %edx
	testl	%r10d, %r10d
	jne	.L1180
	cmpb	$12, -186(%rbp)
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1180:
	orq	-160(%rbp), %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1184:
	cmpq	%rax, %rdx
	seta	%al
	testb	%al, %al
	jne	.L1186
.L1491:
	movq	%rsi, %r13
	movq	16(%rsi), %rsi
.L1187:
	testq	%rsi, %rsi
	je	.L1171
.L1170:
	movq	32(%rsi), %rax
	movl	%eax, %r10d
	movq	%rax, %rcx
	andl	$4, %r10d
	je	.L1172
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L1173
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1173:
	movq	%rax, %rcx
	andq	$-8161, %rcx
	orq	%rdx, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1172:
	movq	%rdi, %rdx
	testl	%r9d, %r9d
	je	.L1174
	xorl	%edx, %edx
	testl	%r11d, %r11d
	jne	.L1175
	cmpb	$12, -144(%rbp)
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1175:
	orq	%rbx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1174:
	cmpq	%rcx, %rdx
	je	.L1490
	testl	%r10d, %r10d
	je	.L1182
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L1183
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1183:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1182:
	movq	%rdi, %rdx
	testl	%r9d, %r9d
	je	.L1184
	xorl	%edx, %edx
	testl	%r11d, %r11d
	jne	.L1185
	cmpb	$12, -144(%rbp)
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1185:
	orq	%rbx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
	cmpq	%rax, %rdx
	seta	%al
	testb	%al, %al
	je	.L1491
.L1186:
	movq	24(%rsi), %rsi
	jmp	.L1187
.L1171:
	movq	-240(%rbp), %rbx
	cmpq	-208(%rbp), %r13
	je	.L1165
	movq	%rdi, %rsi
	testl	%r9d, %r9d
	je	.L1188
	xorl	%eax, %eax
	testl	%r11d, %r11d
	jne	.L1189
	cmpb	$12, -216(%rbp)
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L1189:
	movq	%rdi, %rdx
	andq	$-8161, %rdx
	movq	%rdx, %rsi
	orq	%rax, %rsi
	andq	$-8, %rsi
	orq	$4, %rsi
.L1188:
	movq	32(%r13), %rax
	movl	%eax, %r10d
	movq	%rax, %rdx
	andl	$4, %r10d
	je	.L1190
	xorl	%ecx, %ecx
	testb	$24, %al
	jne	.L1191
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L1191:
	movq	%rax, %rdx
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1190:
	cmpq	%rsi, %rdx
	jne	.L1192
	movq	%r8, %rcx
	testl	%r12d, %r12d
	je	.L1193
	movl	-152(%rbp), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.L1194
	cmpb	$12, -232(%rbp)
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L1194:
	movq	%r8, %rdx
	andq	$-8161, %rdx
	movq	%rdx, %rcx
	orq	%rax, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1193:
	movq	40(%r13), %rax
	testb	$4, %al
	je	.L1200
.L1471:
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L1201
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1201:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1200:
	cmpq	%rcx, %rax
	seta	%al
	testb	%al, %al
	jne	.L1165
	cmpb	$0, -187(%rbp)
	jne	.L1492
.L1226:
	movq	$0, 8(%rbx)
	movq	$0, (%rbx)
	.p2align 4,,10
	.p2align 3
.L1165:
	addq	$8, %r14
	cmpq	%r14, -136(%rbp)
	jne	.L1203
.L1204:
	addq	$4, -200(%rbp)
	movq	-200(%rbp), %rax
	cmpq	%rax, -224(%rbp)
	je	.L1164
	movb	$0, -187(%rbp)
	jmp	.L1163
.L1134:
	movq	%rdx, %rax
	andq	$-8168, %rcx
	cmpb	$11, %dil
	ja	.L1139
	movq	%rcx, %rdi
	orq	$4, %rdi
	jmp	.L1143
.L1493:
	xorl	%ecx, %ecx
.L1142:
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1140:
	cmpq	%rdx, %rdi
	je	.L1132
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L1127
.L1143:
	movq	(%rax), %rdx
	testb	$4, %dl
	je	.L1140
	testb	$24, %dl
	jne	.L1493
	movq	%rdx, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
	jmp	.L1142
.L1192:
	movq	%rdi, %rcx
	testl	%r9d, %r9d
	je	.L1198
	xorl	%edx, %edx
	testl	%r11d, %r11d
	jne	.L1199
	cmpb	$12, -216(%rbp)
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1199:
	movq	%rdi, %rcx
	andq	$-8161, %rcx
	orq	%rdx, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1198:
	testl	%r10d, %r10d
	je	.L1200
	jmp	.L1471
.L1488:
	movq	-136(%rbp), %rdi
	leaq	40(%r12), %rdx
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1147
.L1159:
	movq	%rax, %rdx
	sarq	$6, %rdx
.L1161:
	movq	232(%rcx), %rcx
	movq	%rdx, %rsi
	salq	$6, %rsi
	movq	(%rcx,%rdx,8), %rdx
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %rax
	jmp	.L1160
.L1492:
	testl	%r9d, %r9d
	je	.L1205
	xorl	%eax, %eax
	testl	%r11d, %r11d
	jne	.L1206
	cmpb	$12, -216(%rbp)
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L1206:
	andq	$-8161, %rdi
	orq	%rax, %rdi
	andq	$-8, %rdi
	orq	$4, %rdi
.L1205:
	testl	%r12d, %r12d
	je	.L1207
	movl	-152(%rbp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L1208
	cmpb	$12, -232(%rbp)
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L1208:
	andq	$-8161, %r8
	orq	%rax, %r8
	andq	$-8, %r8
	orq	$4, %r8
.L1207:
	cmpq	%rdi, %r8
	je	.L1226
	movq	(%r15), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L1494
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1211:
	movq	(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rbx), %rdx
	movq	%rdx, 8(%rax)
	movq	%rax, -120(%rbp)
	movq	16(%r15), %rsi
	cmpq	8(%r15), %rsi
	movq	24(%r15), %rax
	je	.L1212
.L1213:
	cmpq	%rax, %rsi
	je	.L1216
	movq	-120(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r15)
	jmp	.L1226
.L1158:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L1161
.L1123:
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1124
.L1114:
	movq	%rax, %rdx
	sarq	$6, %rdx
.L1116:
	movq	232(%rcx), %rcx
	movq	%rdx, %rdi
	salq	$6, %rdi
	movq	(%rcx,%rdx,8), %rdx
	subq	%rdi, %rax
	leaq	(%rdx,%rax,8), %rax
	jmp	.L1115
.L1113:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L1116
.L1216:
	leaq	-120(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1226
.L1212:
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	$31, %rdx
	ja	.L1213
	movq	(%r15), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$31, %rax
	jbe	.L1495
	leaq	32(%rsi), %rax
	movq	%rax, 16(%rdi)
.L1215:
	movq	%rsi, 8(%r15)
	movq	%rsi, 16(%r15)
	movq	%rax, 24(%r15)
	jmp	.L1213
.L1489:
	movq	-168(%rbp), %rax
.L1153:
	movq	8(%rax), %rsi
	movq	-176(%rbp), %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13MoveOptimizer13CompressMovesEPNS1_12ParallelMoveEPNS0_10ZoneVectorIPNS1_12MoveOperandsEEE
	jmp	.L1156
.L1494:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1211
.L1495:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	leaq	32(%rax), %rax
	jmp	.L1215
.L1482:
	movq	-168(%rbp), %rax
	movq	16(%rax), %r8
	jmp	.L1153
.L1292:
	movb	$0, -185(%rbp)
	jmp	.L1150
.L1151:
	movq	-184(%rbp), %rbx
	movb	$1, -185(%rbp)
	movq	%rdx, %r15
	movq	40(%rbx), %rax
	movq	48(%rbx), %rbx
	movq	%rbx, -224(%rbp)
	cmpq	%rbx, %rax
	jne	.L1152
	jmp	.L1156
.L1481:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1155
.L1468:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1472:
	movq	%r8, %rdx
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1483:
	call	__stack_chk_fail@PLT
.L1478:
	jmp	.L1094
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE
	.cfi_startproc
	.type	_ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE.cold, @function
_ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE.cold:
.LFSB10768:
.L1094:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	48, %eax
	ud2
	.cfi_endproc
.LFE10768:
	.section	.text._ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE
	.size	_ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE, .-_ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE
	.section	.text.unlikely._ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE
	.size	_ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE.cold, .-_ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE.cold
.LCOLDE2:
	.section	.text._ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE
.LHOTE2:
	.section	.text._ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElS6_NS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_T0_SL_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElS6_NS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_T0_SL_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElS6_NS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_T0_SL_T1_T2_
	.type	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElS6_NS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_T0_SL_T1_T2_, @function
_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElS6_NS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_T0_SL_T1_T2_:
.LFB13299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rdx, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rax, %r13
	andl	$1, %edi
	shrq	$63, %r13
	pushq	%r12
	addq	%rax, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	%rax
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -80(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r8, -56(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rdi, -96(%rbp)
	cmpq	%rax, %rsi
	jge	.L1497
	movq	%rsi, %r10
	.p2align 4,,10
	.p2align 3
.L1501:
	leaq	1(%r10), %rcx
	movq	%r10, -64(%rbp)
	movq	-56(%rbp), %rax
	leaq	(%rcx,%rcx), %rbx
	salq	$4, %rcx
	leaq	-1(%rbx), %r15
	leaq	(%r14,%rcx), %r12
	leaq	(%r14,%r15,8), %r13
	movq	(%r12), %rdi
	movq	0(%r13), %rsi
	call	*%rax
	movq	-64(%rbp), %r10
	testb	%al, %al
	jne	.L1498
	movq	(%r12), %rax
	movq	%rax, (%r14,%r10,8)
	cmpq	-88(%rbp), %rbx
	jge	.L1499
	movq	%rbx, %r10
	jmp	.L1501
	.p2align 4,,10
	.p2align 3
.L1498:
	movq	0(%r13), %rax
	movq	%rax, (%r14,%r10,8)
	cmpq	-88(%rbp), %r15
	jge	.L1506
	movq	%r15, %r10
	jmp	.L1501
	.p2align 4,,10
	.p2align 3
.L1506:
	movq	%r13, %r12
	movq	%r15, %rbx
.L1499:
	cmpq	$0, -96(%rbp)
	je	.L1505
.L1502:
	leaq	-1(%rbx), %rax
	movq	%rax, %r13
	shrq	$63, %r13
	addq	%rax, %r13
	sarq	%r13
	cmpq	-80(%rbp), %rbx
	jg	.L1504
	jmp	.L1503
	.p2align 4,,10
	.p2align 3
.L1515:
	movq	(%r15), %rax
	leaq	-1(%r13), %rdx
	movq	%r13, %rbx
	movq	%rax, (%r12)
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	cmpq	%r13, -80(%rbp)
	jge	.L1514
	movq	%rax, %r13
.L1504:
	leaq	(%r14,%r13,8), %r15
	movq	-72(%rbp), %rsi
	movq	-56(%rbp), %rax
	leaq	(%r14,%rbx,8), %r12
	movq	(%r15), %rdi
	call	*%rax
	testb	%al, %al
	jne	.L1515
.L1503:
	movq	-72(%rbp), %rax
	movq	%rax, (%r12)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1497:
	.cfi_restore_state
	cmpq	$0, -96(%rbp)
	movq	%rsi, %rax
	leaq	(%r14,%rsi,8), %r12
	jne	.L1503
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1505:
	movq	-104(%rbp), %rdx
	subq	$2, %rdx
	movq	%rdx, %r15
	shrq	$63, %r15
	addq	%rdx, %r15
	sarq	%r15
	cmpq	%rbx, %r15
	jne	.L1502
	leaq	1(%rbx,%rbx), %rbx
	leaq	(%r14,%rbx,8), %rax
	movq	(%rax), %rdx
	movq	%rdx, (%r12)
	movq	%rax, %r12
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1514:
	movq	%r15, %r12
	jmp	.L1503
	.cfi_endproc
.LFE13299:
	.size	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElS6_NS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_T0_SL_T1_T2_, .-_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElS6_NS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_T0_SL_T1_T2_
	.section	.text._ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElNS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_SK_T0_T1_,"axG",@progbits,_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElNS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_SK_T0_T1_,comdat
	.p2align 4
	.weak	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElNS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_SK_T0_T1_
	.type	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElNS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_SK_T0_T1_, @function
_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElNS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_SK_T0_T1_:
.LFB12752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	subq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	cmpq	$128, %rax
	jle	.L1516
	movq	%rdi, %r15
	movq	%rcx, %rbx
	testq	%rdx, %rdx
	je	.L1544
	leaq	8(%rdi), %rax
	movq	%rsi, %r13
	movq	%rax, -80(%rbp)
.L1520:
	movq	%r13, %rax
	subq	$1, -56(%rbp)
	movq	8(%r15), %rdi
	subq	%r15, %rax
	movq	%rax, %rdx
	shrq	$63, %rax
	sarq	$3, %rdx
	addq	%rdx, %rax
	sarq	%rax
	leaq	(%r15,%rax,8), %r12
	movq	(%r12), %rsi
	call	*%rbx
	movq	-8(%r13), %rsi
	testb	%al, %al
	je	.L1525
	movq	(%r12), %rdi
	call	*%rbx
	testb	%al, %al
	je	.L1526
	movq	(%r15), %rax
	movq	(%r12), %rdx
	movq	%rdx, (%r15)
	movq	%rax, (%r12)
	movq	(%r15), %rsi
.L1527:
	movq	-80(%rbp), %r12
	movq	%r13, %r14
	.p2align 4,,10
	.p2align 3
.L1531:
	movq	%r12, -72(%rbp)
	movq	(%r12), %rdi
	call	*%rbx
	testb	%al, %al
	jne	.L1532
	leaq	-8(%r14), %rcx
	.p2align 4,,10
	.p2align 3
.L1533:
	movq	(%rcx), %rsi
	movq	%rcx, %r14
	movq	(%r15), %rdi
	movq	%rcx, -64(%rbp)
	call	*%rbx
	movq	-64(%rbp), %rcx
	subq	$8, %rcx
	testb	%al, %al
	jne	.L1533
	cmpq	%r14, %r12
	jnb	.L1545
	movq	(%r12), %rax
	movq	(%r14), %rcx
	movq	%rcx, (%r12)
	movq	%rax, (%r14)
.L1532:
	movq	(%r15), %rsi
	addq	$8, %r12
	jmp	.L1531
	.p2align 4,,10
	.p2align 3
.L1545:
	movq	-56(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElNS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_SK_T0_T1_
	movq	%r12, %rax
	subq	%r15, %rax
	cmpq	$128, %rax
	jle	.L1516
	cmpq	$0, -56(%rbp)
	je	.L1518
	movq	%r12, %r13
	jmp	.L1520
	.p2align 4,,10
	.p2align 3
.L1525:
	movq	8(%r15), %rdi
	call	*%rbx
	testb	%al, %al
	je	.L1529
	movdqu	(%r15), %xmm0
	movq	8(%r15), %rsi
	shufpd	$1, %xmm0, %xmm0
	movups	%xmm0, (%r15)
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1544:
	movq	%rsi, -72(%rbp)
.L1518:
	sarq	$3, %rax
	leaq	-2(%rax), %r14
	movq	%rax, %r13
	sarq	%r14
	jmp	.L1522
	.p2align 4,,10
	.p2align 3
.L1546:
	subq	$1, %r14
.L1522:
	movq	(%r15,%r14,8), %rcx
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElS6_NS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_T0_SL_T1_T2_
	testq	%r14, %r14
	jne	.L1546
	movq	-72(%rbp), %r12
	subq	$8, %r12
	.p2align 4,,10
	.p2align 3
.L1523:
	movq	(%r15), %rax
	movq	%r12, %r13
	movq	(%r12), %rcx
	movq	%rbx, %r8
	subq	%r15, %r13
	xorl	%esi, %esi
	movq	%r15, %rdi
	subq	$8, %r12
	movq	%rax, 8(%r12)
	movq	%r13, %rdx
	sarq	$3, %rdx
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElS6_NS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_T0_SL_T1_T2_
	cmpq	$8, %r13
	jg	.L1523
.L1516:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1526:
	.cfi_restore_state
	movq	-8(%r13), %rsi
	movq	8(%r15), %rdi
	call	*%rbx
	testb	%al, %al
	je	.L1528
	movq	(%r15), %rax
	movq	-8(%r13), %rdx
	movq	%rdx, (%r15)
	movq	%rax, -8(%r13)
	movq	(%r15), %rsi
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1529:
	movq	-8(%r13), %rsi
	movq	(%r12), %rdi
	call	*%rbx
	testb	%al, %al
	movq	(%r15), %rax
	je	.L1530
	movq	-8(%r13), %rdx
	movq	%rdx, (%r15)
	movq	%rax, -8(%r13)
	movq	(%r15), %rsi
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1530:
	movq	(%r12), %rdx
	movq	%rdx, (%r15)
	movq	%rax, (%r12)
	movq	(%r15), %rsi
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1528:
	movdqu	(%r15), %xmm0
	movq	8(%r15), %rsi
	shufpd	$1, %xmm0, %xmm0
	movups	%xmm0, (%r15)
	jmp	.L1527
	.cfi_endproc
.LFE12752:
	.size	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElNS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_SK_T0_T1_, .-_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElNS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_SK_T0_T1_
	.section	.text._ZN2v88internal8compiler13MoveOptimizer13FinalizeMovesEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13MoveOptimizer13FinalizeMovesEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler13MoveOptimizer13FinalizeMovesEPNS1_11InstructionE, @function
_ZN2v88internal8compiler13MoveOptimizer13FinalizeMovesEPNS1_11InstructionE:
.LFB10799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	8(%rsi), %rax
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testq	%rax, %rax
	je	.L1547
	movq	8(%rax), %rbx
	movq	16(%rax), %r13
	movq	%rdi, %r12
	movq	32(%rdi), %r14
	cmpq	%r13, %rbx
	je	.L1550
	leaq	-64(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L1558:
	movq	(%rbx), %rsi
	movq	%rsi, -64(%rbp)
	movq	(%rsi), %rax
	movl	%eax, %r8d
	andl	$7, %r8d
	je	.L1551
	movq	%rax, %rdi
	testb	$4, %al
	je	.L1552
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L1553
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1553:
	movq	%rax, %rdi
	andq	$-8161, %rdi
	orq	%rdx, %rdi
	andq	$-8, %rdi
	orq	$4, %rdi
.L1552:
	movq	8(%rsi), %rdx
	testb	$4, %dl
	je	.L1554
	xorl	%r9d, %r9d
	testb	$24, %dl
	jne	.L1555
	movq	%rdx, %r9
	shrq	$5, %r9
	cmpb	$12, %r9b
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
.L1555:
	andq	$-8161, %rdx
	orq	%r9, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1554:
	cmpq	%rdi, %rdx
	je	.L1551
	cmpl	$2, %r8d
	je	.L1556
	testb	$4, %al
	je	.L1551
	movq	%rax, %rdx
	shrq	$3, %rdx
	andl	$3, %edx
	cmpl	$1, %edx
	je	.L1556
	.p2align 4,,10
	.p2align 3
.L1551:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L1558
.L1550:
	movq	24(%r12), %r13
	cmpq	%r14, %r13
	je	.L1547
	movq	%r14, %rbx
	movl	$63, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	subq	%r13, %rbx
	movq	%rbx, %rcx
	sarq	$3, %rcx
	bsrq	%rcx, %rcx
	xorq	$63, %rcx
	subl	%ecx, %edx
	leaq	_ZN2v88internal8compiler12_GLOBAL__N_111LoadCompareEPKNS1_12MoveOperandsES5_(%rip), %rcx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPPN2v88internal8compiler12MoveOperandsESt6vectorIS6_NS3_13ZoneAllocatorIS6_EEEEElNS0_5__ops15_Iter_comp_iterIPFbPKS5_SG_EEEEvT_SK_T0_T1_
	leaq	8(%r13), %rax
	cmpq	$128, %rbx
	jle	.L1560
	leaq	128(%r13), %rbx
	movq	%rax, -80(%rbp)
	movq	%rbx, -96(%rbp)
	jmp	.L1565
	.p2align 4,,10
	.p2align 3
.L1741:
	cmpq	%r15, %r13
	je	.L1562
	movq	%r15, %rdx
	movl	$8, %eax
	movq	%r13, %rsi
	subq	%r13, %rdx
	leaq	0(%r13,%rax), %rdi
	call	memmove@PLT
.L1562:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rbx, 0(%r13)
	cmpq	%rax, -96(%rbp)
	je	.L1740
.L1565:
	movq	-80(%rbp), %r15
	movq	0(%r13), %rsi
	movq	(%r15), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_111LoadCompareEPKNS1_12MoveOperandsES5_
	testb	%al, %al
	jne	.L1741
	jmp	.L1561
	.p2align 4,,10
	.p2align 3
.L1556:
	cmpq	%r14, 40(%r12)
	je	.L1557
	movq	%rsi, (%r14)
	movq	32(%r12), %rax
	addq	$8, %rbx
	leaq	8(%rax), %r14
	movq	%r14, 32(%r12)
	cmpq	%rbx, %r13
	jne	.L1558
	jmp	.L1550
	.p2align 4,,10
	.p2align 3
.L1626:
	movq	24(%r12), %rax
	cmpq	32(%r12), %rax
	je	.L1547
	movq	%rax, 32(%r12)
.L1547:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1742
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1560:
	.cfi_restore_state
	movq	%rax, -80(%rbp)
	cmpq	%rax, %r14
	jne	.L1592
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1743:
	cmpq	%r15, %r13
	je	.L1589
	movq	%r15, %rdx
	movl	$8, %eax
	movq	%r13, %rsi
	subq	%r13, %rdx
	leaq	0(%r13,%rax), %rdi
	call	memmove@PLT
.L1589:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rbx, 0(%r13)
	cmpq	%rax, %r14
	je	.L1567
.L1592:
	movq	-80(%rbp), %r15
	movq	0(%r13), %rsi
	movq	(%r15), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_111LoadCompareEPKNS1_12MoveOperandsES5_
	testb	%al, %al
	jne	.L1743
	jmp	.L1588
	.p2align 4,,10
	.p2align 3
.L1744:
	movq	-72(%rbp), %rcx
	subq	$8, %r15
	movq	%rcx, 8(%r15)
.L1588:
	movq	-8(%r15), %rcx
	movq	%rbx, %rdi
	movq	%rcx, %rsi
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_111LoadCompareEPKNS1_12MoveOperandsES5_
	testb	%al, %al
	jne	.L1744
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rbx, (%r15)
	cmpq	%rax, %r14
	jne	.L1592
	.p2align 4,,10
	.p2align 3
.L1567:
	movq	24(%r12), %rbx
	movq	32(%r12), %rcx
	cmpq	%rbx, %rcx
	je	.L1547
	movq	(%rbx), %r14
	addq	$8, %rbx
	leaq	-64(%rbp), %r8
	cmpq	%rbx, %rcx
	jne	.L1616
	jmp	.L1626
	.p2align 4,,10
	.p2align 3
.L1746:
	movq	%rdx, %rax
	shrq	$3, %rax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L1595
	movq	8(%r12), %rsi
	movq	8(%rsi), %rdi
	movq	-88(%rbp), %rsi
	movq	16(%rsi), %r9
	testq	%r9, %r9
	je	.L1618
	movq	(%r9), %rdi
.L1617:
	xorl	%esi, %esi
	testl	%eax, %eax
	jne	.L1605
	movq	%rdx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L1605:
	andq	$-8161, %rdx
	orq	%rsi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1604:
	movq	8(%r13), %rax
	testb	$4, %al
	je	.L1606
	xorl	%esi, %esi
	testb	$24, %al
	jne	.L1607
	movq	%rax, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L1607:
	andq	$-8161, %rax
	orq	%rsi, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1606:
	cmpq	%rdx, %rax
	je	.L1608
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L1745
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1610:
	movq	8(%r14), %rdx
	movq	%rdx, (%rax)
	movq	8(%r13), %rdx
	movq	%rdx, 8(%rax)
	movq	%rax, -64(%rbp)
	movq	16(%r9), %rsi
	cmpq	8(%r9), %rsi
	movq	24(%r9), %rax
	je	.L1611
.L1612:
	cmpq	%rax, %rsi
	je	.L1615
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r9)
.L1608:
	movq	$0, 8(%r13)
	movq	$0, 0(%r13)
.L1595:
	addq	$8, %rbx
	cmpq	%rbx, %rcx
	je	.L1626
.L1616:
	movq	(%rbx), %r13
	testq	%r14, %r14
	je	.L1662
	movq	0(%r13), %rax
	testb	$4, %al
	je	.L1596
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L1597
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1597:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1596:
	movq	(%r14), %rdx
	testb	$4, %dl
	je	.L1598
	xorl	%esi, %esi
	testb	$24, %dl
	jne	.L1599
	movq	%rdx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L1599:
	andq	$-8161, %rdx
	orq	%rsi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1598:
	cmpq	%rax, %rdx
	jne	.L1662
	movq	8(%r14), %rdx
	testb	$4, %dl
	jne	.L1746
	movq	8(%r12), %rax
	movq	8(%rax), %rdi
	movq	-88(%rbp), %rax
	movq	16(%rax), %r9
	testq	%r9, %r9
	je	.L1618
	movq	(%r9), %rdi
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1747:
	movq	-72(%rbp), %rcx
	subq	$8, %r15
	movq	%rcx, 8(%r15)
.L1561:
	movq	-8(%r15), %rcx
	movq	%rbx, %rdi
	movq	%rcx, %rsi
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_111LoadCompareEPKNS1_12MoveOperandsES5_
	testb	%al, %al
	jne	.L1747
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rbx, (%r15)
	cmpq	%rax, -96(%rbp)
	jne	.L1565
.L1740:
	movq	-96(%rbp), %rbx
	cmpq	%r14, %rbx
	je	.L1567
	.p2align 4,,10
	.p2align 3
.L1585:
	movq	(%rbx), %r9
	movq	%rbx, %rdi
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1748:
	testl	%r11d, %r11d
	je	.L1573
	xorl	%ecx, %ecx
	testb	$24, %al
	jne	.L1574
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L1574:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1573:
	testl	%r10d, %r10d
	je	.L1575
	xorl	%ecx, %ecx
	testb	$24, %dl
	jne	.L1576
	movq	%rdx, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L1576:
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1575:
	cmpq	%rax, %rdx
	seta	%al
.L1577:
	testb	%al, %al
	je	.L1580
.L1634:
	movq	%r8, (%rdi)
	subq	$8, %rdi
.L1584:
	movq	(%r9), %rax
	movq	-8(%rdi), %r8
	movl	%eax, %r11d
	movq	%rax, %rcx
	andl	$4, %r11d
	je	.L1568
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L1569
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1569:
	movq	%rax, %rcx
	andq	$-8161, %rcx
	orq	%rdx, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1568:
	movq	(%r8), %rdx
	movl	%edx, %r10d
	movq	%rdx, %rsi
	andl	$4, %r10d
	je	.L1570
	xorl	%r13d, %r13d
	testb	$24, %dl
	jne	.L1571
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%r13, %r13
	notq	%r13
	andl	$416, %r13d
.L1571:
	movq	%rdx, %rsi
	andq	$-8161, %rsi
	orq	%r13, %rsi
	andq	$-8, %rsi
	orq	$4, %rsi
.L1570:
	cmpq	%rcx, %rsi
	jne	.L1748
	movq	8(%r9), %rcx
	testb	$4, %cl
	jne	.L1749
	movq	8(%r8), %rax
	testb	$4, %al
	jne	.L1750
	.p2align 4,,10
	.p2align 3
.L1582:
	cmpq	%rax, %rcx
	setb	%al
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1662:
	movq	%r13, %r14
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L1618:
	movq	16(%rdi), %r9
	movq	24(%rdi), %rax
	subq	%r9, %rax
	cmpq	$31, %rax
	jbe	.L1751
	leaq	32(%r9), %rax
	movq	%rax, 16(%rdi)
.L1603:
	movq	-88(%rbp), %rax
	movq	%rdi, (%r9)
	movq	$0, 8(%r9)
	movq	$0, 16(%r9)
	movq	$0, 24(%r9)
	movq	%r9, 16(%rax)
	movq	8(%r14), %rdx
	movq	(%r9), %rdi
	testb	$4, %dl
	je	.L1604
	movq	%rdx, %rax
	shrq	$3, %rax
	andl	$3, %eax
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1615:
	movq	%r8, %rdx
	movq	%r9, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r8
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1611:
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	$31, %rdx
	ja	.L1612
	movq	(%r9), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$31, %rax
	jbe	.L1752
	leaq	32(%rsi), %rax
	movq	%rax, 16(%rdi)
.L1614:
	movq	%rsi, 8(%r9)
	movq	%rsi, 16(%r9)
	movq	%rax, 24(%r9)
	jmp	.L1612
	.p2align 4,,10
	.p2align 3
.L1557:
	movq	%rcx, %rdx
	movq	%r14, %rsi
	leaq	16(%r12), %rdi
	addq	$8, %rbx
	movq	%rcx, -72(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	32(%r12), %r14
	movq	-72(%rbp), %rcx
	cmpq	%rbx, %r13
	jne	.L1558
	jmp	.L1550
	.p2align 4,,10
	.p2align 3
.L1749:
	movq	%rcx, %rdx
	movq	8(%r8), %rax
	shrq	$3, %rdx
	andl	$3, %edx
	cmpl	$1, %edx
	je	.L1753
	movl	%eax, %r10d
	andl	$4, %r10d
	je	.L1754
	movq	%rax, %rsi
	shrq	$3, %rsi
	andl	$3, %esi
	cmpl	$1, %esi
	je	.L1634
	testl	%edx, %edx
	jne	.L1736
	movq	%rcx, %rdx
	shrq	$5, %rdx
	cmpb	$11, %dl
	jbe	.L1632
	andq	$-8168, %rcx
	orq	$420, %rcx
	.p2align 4,,10
	.p2align 3
.L1627:
	movq	%rax, %rsi
	shrq	$3, %rsi
	andl	$3, %esi
.L1636:
	xorl	%edx, %edx
	testl	%esi, %esi
	jne	.L1583
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L1583:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1753:
	testb	$4, %al
	jne	.L1755
.L1580:
	addq	$8, %rbx
	movq	%r9, (%rdi)
	cmpq	%rbx, %r14
	jne	.L1585
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1632:
	andq	$-8168, %rcx
	orq	$4, %rcx
	testl	%r10d, %r10d
	je	.L1582
	jmp	.L1627
	.p2align 4,,10
	.p2align 3
.L1755:
	movq	%rax, %rdx
	shrq	$3, %rdx
	andl	$3, %edx
	cmpl	$1, %edx
	jne	.L1580
.L1736:
	andq	$-8168, %rcx
	orq	$4, %rcx
	jmp	.L1627
	.p2align 4,,10
	.p2align 3
.L1754:
	movl	%edx, %r10d
	testl	%edx, %edx
	jne	.L1756
	movq	%rcx, %rdx
	shrq	$5, %rdx
	cmpb	$11, %dl
	jbe	.L1632
	andq	$-8168, %rcx
	orq	$420, %rcx
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1756:
	andq	$-8168, %rcx
	orq	$4, %rcx
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1745:
	movl	$16, %esi
	movq	%r8, -96(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	-96(%rbp), %r8
	jmp	.L1610
.L1751:
	movl	$32, %esi
	movq	%r8, -96(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdi
	movq	-80(%rbp), %rcx
	movq	-96(%rbp), %r8
	movq	%rax, %r9
	jmp	.L1603
.L1752:
	movl	$32, %esi
	movq	%r8, -96(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movq	-96(%rbp), %r8
	leaq	32(%rax), %rax
	jmp	.L1614
.L1742:
	call	__stack_chk_fail@PLT
.L1750:
	movq	%rax, %rsi
	shrq	$3, %rsi
	andl	$3, %esi
	cmpl	$1, %esi
	jne	.L1636
	jmp	.L1634
	.cfi_endproc
.LFE10799:
	.size	_ZN2v88internal8compiler13MoveOptimizer13FinalizeMovesEPNS1_11InstructionE, .-_ZN2v88internal8compiler13MoveOptimizer13FinalizeMovesEPNS1_11InstructionE
	.section	.text._ZN2v88internal8compiler13MoveOptimizer3RunEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13MoveOptimizer3RunEv
	.type	_ZN2v88internal8compiler13MoveOptimizer3RunEv, @function
_ZN2v88internal8compiler13MoveOptimizer3RunEv:
.LFB10752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	208(%rax), %rbx
	movq	224(%rax), %r13
	movq	232(%rax), %r15
	movq	240(%rax), %r14
	.p2align 4,,10
	.p2align 3
.L1759:
	cmpq	%rbx, %r14
	je	.L1758
.L1777:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler13MoveOptimizer12CompressGapsEPNS1_11InstructionE
	cmpq	%rbx, %r13
	jne	.L1759
	movq	8(%r15), %rbx
	addq	$8, %r15
	leaq	512(%rbx), %r13
	cmpq	%rbx, %r14
	jne	.L1777
.L1758:
	movq	8(%r12), %rax
	movq	16(%rax), %rdx
	movq	8(%rdx), %rbx
	movq	16(%rdx), %r13
	cmpq	%r13, %rbx
	je	.L1761
	.p2align 4,,10
	.p2align 3
.L1762:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler13MoveOptimizer13CompressBlockEPNS1_16InstructionBlockE
	cmpq	%rbx, %r13
	jne	.L1762
	movq	8(%r12), %rax
	movq	16(%rax), %rdx
	movq	8(%rdx), %r13
	movq	16(%rdx), %rbx
	cmpq	%rbx, %r13
	jne	.L1767
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L1764:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13MoveOptimizer13OptimizeMergeEPNS1_16InstructionBlockE
.L1763:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L1778
.L1767:
	movq	0(%r13), %r8
	movq	48(%r8), %rdi
	movq	40(%r8), %rax
	movq	%rdi, %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L1763
	cmpb	$0, 120(%r8)
	jne	.L1764
	cmpq	%rdi, %rax
	je	.L1763
	movq	8(%r12), %rdx
	movq	16(%rdx), %rdx
	movq	8(%rdx), %r9
	movq	16(%rdx), %rdx
	subq	%r9, %rdx
	sarq	$3, %rdx
	jmp	.L1766
	.p2align 4,,10
	.p2align 3
.L1765:
	movq	(%r9,%rsi,8), %rcx
	cmpb	$0, 120(%rcx)
	je	.L1764
	addq	$4, %rax
	cmpq	%rax, %rdi
	je	.L1763
.L1766:
	movslq	(%rax), %rsi
	cmpq	%rdx, %rsi
	jb	.L1765
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1778:
	movq	8(%r12), %rax
.L1761:
	movq	208(%rax), %rbx
	movq	224(%rax), %r13
	movq	232(%rax), %r15
	movq	240(%rax), %r14
	.p2align 4,,10
	.p2align 3
.L1769:
	cmpq	%rbx, %r14
	je	.L1757
.L1779:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler13MoveOptimizer13FinalizeMovesEPNS1_11InstructionE
	cmpq	%rbx, %r13
	jne	.L1769
	movq	8(%r15), %rbx
	addq	$8, %r15
	leaq	512(%rbx), %r13
	cmpq	%rbx, %r14
	jne	.L1779
.L1757:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10752:
	.size	_ZN2v88internal8compiler13MoveOptimizer3RunEv, .-_ZN2v88internal8compiler13MoveOptimizer3RunEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler13MoveOptimizerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler13MoveOptimizerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler13MoveOptimizerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE:
.LFB13356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE13356:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler13MoveOptimizerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE, .-_GLOBAL__sub_I__ZN2v88internal8compiler13MoveOptimizerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler13MoveOptimizerC2EPNS0_4ZoneEPNS1_19InstructionSequenceE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
