	.file	"frame-states.cc"
	.text
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_141CreateBuiltinContinuationFrameStateCommonEPNS1_7JSGraphENS1_14FrameStateTypeENS0_8Builtins4NameEPNS1_4NodeES9_PS9_iS9_NS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_141CreateBuiltinContinuationFrameStateCommonEPNS1_7JSGraphENS1_14FrameStateTypeENS0_8Builtins4NameEPNS1_4NodeES9_PS9_iS9_NS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal8compiler12_GLOBAL__N_141CreateBuiltinContinuationFrameStateCommonEPNS1_7JSGraphENS1_14FrameStateTypeENS0_8Builtins4NameEPNS1_4NodeES9_PS9_iS9_NS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB21566:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	xorl	%edx, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movl	16(%rbp), %ebx
	movq	8(%rdi), %r13
	movq	%r9, -120(%rbp)
	movl	%esi, -140(%rbp)
	movhps	24(%rbp), %xmm0
	movq	(%rdi), %r15
	movq	%r8, -136(%rbp)
	movl	%ebx, %esi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11StateValuesEiNS1_15SparseInputMaskE@PLT
	movq	-120(%rbp), %r9
	xorl	%r8d, %r8d
	movl	%ebx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r9, %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	%r14d, %edi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8Builtins24GetContinuationBailoutIdENS1_4NameE@PLT
	movl	-140(%rbp), %r10d
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	movq	32(%rbp), %r8
	movq	%r13, %rdi
	movl	%eax, %r14d
	movl	%r10d, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder28CreateFrameStateFunctionInfoENS1_14FrameStateTypeEiiNS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	$-1, %rdx
	movl	%r14d, %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10FrameStateENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_22FrameStateFunctionInfoE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler7JSGraph16EmptyStateValuesEv@PLT
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler7JSGraph16EmptyStateValuesEv@PLT
	xorl	%r8d, %r8d
	leaq	-112(%rbp), %rcx
	movq	%r13, %rsi
	movq	-128(%rbp), %xmm1
	movq	%rax, %xmm2
	movl	$6, %edx
	movq	%r15, %rdi
	movdqa	-160(%rbp), %xmm0
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -112(%rbp)
	movq	-120(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	movhps	-136(%rbp), %xmm1
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L5
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21566:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_141CreateBuiltinContinuationFrameStateCommonEPNS1_7JSGraphENS1_14FrameStateTypeENS0_8Builtins4NameEPNS1_4NodeES9_PS9_iS9_NS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal8compiler12_GLOBAL__N_141CreateBuiltinContinuationFrameStateCommonEPNS1_7JSGraphENS1_14FrameStateTypeENS0_8Builtins4NameEPNS1_4NodeES9_PS9_iS9_NS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal8compiler10hash_valueERKNS1_23OutputFrameStateCombineE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler10hash_valueERKNS1_23OutputFrameStateCombineE
	.type	_ZN2v88internal8compiler10hash_valueERKNS1_23OutputFrameStateCombineE, @function
_ZN2v88internal8compiler10hash_valueERKNS1_23OutputFrameStateCombineE:
.LFB21558:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	_ZN2v84base10hash_valueEm@PLT
	.cfi_endproc
.LFE21558:
	.size	_ZN2v88internal8compiler10hash_valueERKNS1_23OutputFrameStateCombineE, .-_ZN2v88internal8compiler10hash_valueERKNS1_23OutputFrameStateCombineE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_23OutputFrameStateCombineE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Ignore"
.LC1:
	.string	"PokeAt("
.LC2:
	.string	")"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_23OutputFrameStateCombineE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_23OutputFrameStateCombineE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_23OutputFrameStateCombineE, @function
_ZN2v88internal8compilerlsERSoRKNS1_23OutputFrameStateCombineE:
.LFB21559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	$-1, (%rsi)
	je	.L11
	movq	%rsi, %rbx
	movl	$7, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	$6, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21559:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_23OutputFrameStateCombineE, .-_ZN2v88internal8compilerlsERSoRKNS1_23OutputFrameStateCombineE
	.section	.text._ZN2v88internal8compilereqERKNS1_14FrameStateInfoES4_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilereqERKNS1_14FrameStateInfoES4_
	.type	_ZN2v88internal8compilereqERKNS1_14FrameStateInfoES4_, @function
_ZN2v88internal8compilereqERKNS1_14FrameStateInfoES4_:
.LFB21560:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	movq	16(%rsi), %rcx
	testq	%rdx, %rdx
	je	.L13
	movl	(%rdx), %r9d
	xorl	%r8d, %r8d
	testq	%rcx, %rcx
	je	.L14
.L16:
	movl	(%rcx), %r8d
.L14:
	xorl	%eax, %eax
	cmpl	%r9d, %r8d
	je	.L17
.L12:
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%r9d, %r9d
	testq	%rcx, %rcx
	jne	.L16
.L17:
	movl	(%rdi), %r10d
	xorl	%eax, %eax
	cmpl	%r10d, (%rsi)
	jne	.L12
	movq	8(%rsi), %rax
	cmpq	%rax, 8(%rdi)
	sete	%al
	cmpq	%rcx, %rdx
	sete	%dl
	andl	%edx, %eax
	ret
	.cfi_endproc
.LFE21560:
	.size	_ZN2v88internal8compilereqERKNS1_14FrameStateInfoES4_, .-_ZN2v88internal8compilereqERKNS1_14FrameStateInfoES4_
	.section	.text._ZN2v88internal8compilerneERKNS1_14FrameStateInfoES4_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerneERKNS1_14FrameStateInfoES4_
	.type	_ZN2v88internal8compilerneERKNS1_14FrameStateInfoES4_, @function
_ZN2v88internal8compilerneERKNS1_14FrameStateInfoES4_:
.LFB21561:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	movq	16(%rsi), %rcx
	testq	%rdx, %rdx
	je	.L24
	movl	(%rdx), %r9d
	xorl	%r8d, %r8d
	testq	%rcx, %rcx
	je	.L25
.L27:
	movl	(%rcx), %r8d
.L25:
	movl	$1, %eax
	cmpl	%r9d, %r8d
	je	.L28
.L23:
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	xorl	%r9d, %r9d
	testq	%rcx, %rcx
	jne	.L27
.L28:
	movl	(%rdi), %r10d
	movl	$1, %eax
	cmpl	%r10d, (%rsi)
	jne	.L23
	movq	8(%rdi), %rax
	cmpq	%rax, 8(%rsi)
	sete	%al
	cmpq	%rcx, %rdx
	sete	%dl
	andl	%edx, %eax
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE21561:
	.size	_ZN2v88internal8compilerneERKNS1_14FrameStateInfoES4_, .-_ZN2v88internal8compilerneERKNS1_14FrameStateInfoES4_
	.section	.text._ZN2v88internal8compiler10hash_valueERKNS1_14FrameStateInfoE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler10hash_valueERKNS1_14FrameStateInfoE
	.type	_ZN2v88internal8compiler10hash_valueERKNS1_14FrameStateInfoE, @function
_ZN2v88internal8compiler10hash_valueERKNS1_14FrameStateInfoE:
.LFB21562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	16(%rdi), %rax
	movq	8(%rdi), %r14
	movl	(%rdi), %r13d
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L35
	movl	(%rax), %edi
.L35:
	call	_ZN2v84base10hash_valueEj@PLT
	movl	%r13d, %edi
	movq	%rax, %r12
	call	_ZN2v88internal10hash_valueENS0_9BailoutIdE@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v84base10hash_valueEm@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base12hash_combineEmm@PLT
	.cfi_endproc
.LFE21562:
	.size	_ZN2v88internal8compiler10hash_valueERKNS1_14FrameStateInfoE, .-_ZN2v88internal8compiler10hash_valueERKNS1_14FrameStateInfoE
	.section	.rodata._ZN2v88internal8compilerlsERSoNS1_14FrameStateTypeE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"INTERPRETED_FRAME"
.LC4:
	.string	"ARGUMENTS_ADAPTOR"
.LC5:
	.string	"CONSTRUCT_STUB"
.LC6:
	.string	"BUILTIN_CONTINUATION_FRAME"
	.section	.rodata._ZN2v88internal8compilerlsERSoNS1_14FrameStateTypeE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"JAVA_SCRIPT_BUILTIN_CONTINUATION_FRAME"
	.align 8
.LC8:
	.string	"JAVA_SCRIPT_BUILTIN_CONTINUATION_WITH_CATCH_FRAME"
	.section	.text._ZN2v88internal8compilerlsERSoNS1_14FrameStateTypeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoNS1_14FrameStateTypeE
	.type	_ZN2v88internal8compilerlsERSoNS1_14FrameStateTypeE, @function
_ZN2v88internal8compilerlsERSoNS1_14FrameStateTypeE:
.LFB21563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$5, %esi
	ja	.L41
	leaq	.L43(%rip), %rdx
	movl	%esi, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compilerlsERSoNS1_14FrameStateTypeE,"a",@progbits
	.align 4
	.align 4
.L43:
	.long	.L48-.L43
	.long	.L47-.L43
	.long	.L46-.L43
	.long	.L45-.L43
	.long	.L44-.L43
	.long	.L42-.L43
	.section	.text._ZN2v88internal8compilerlsERSoNS1_14FrameStateTypeE
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$49, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L41:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movl	$38, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movl	$17, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movl	$17, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movl	$14, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movl	$26, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21563:
	.size	_ZN2v88internal8compilerlsERSoNS1_14FrameStateTypeE, .-_ZN2v88internal8compilerlsERSoNS1_14FrameStateTypeE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_14FrameStateInfoE.str1.1,"aMS",@progbits,1
.LC9:
	.string	", "
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_14FrameStateInfoE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_14FrameStateInfoE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_14FrameStateInfoE, @function
_ZN2v88internal8compilerlsERSoRKNS1_14FrameStateInfoE:
.LFB21564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	je	.L51
	movl	(%rax), %esi
.L51:
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoNS1_14FrameStateTypeE
	movl	$2, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %esi
	movq	%r13, %rdi
	call	_ZN2v88internallsERSoNS0_9BailoutIdE@PLT
	movl	$2, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r14
	cmpq	$-1, %r14
	je	.L66
	movl	$7, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L53:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L54
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.L54
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	movl	$2, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
.L54:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	$6, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L53
.L67:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21564:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_14FrameStateInfoE, .-_ZN2v88internal8compilerlsERSoRKNS1_14FrameStateInfoE
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_.str1.1,"aMS",@progbits,1
.LC10:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB24609:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L82
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L78
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L83
.L70:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L77:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L84
	testq	%r13, %r13
	jg	.L73
	testq	%r9, %r9
	jne	.L76
.L74:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L73
.L76:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L83:
	testq	%rsi, %rsi
	jne	.L71
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L74
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$8, %r14d
	jmp	.L70
.L82:
	leaq	.LC10(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L71:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L70
	.cfi_endproc
.LFE24609:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.rodata._ZN2v88internal8compiler39CreateStubBuiltinContinuationFrameStateEPNS1_7JSGraphENS0_8Builtins4NameEPNS1_4NodeEPKS7_iS7_NS1_26ContinuationFrameStateModeE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"unreachable code"
.LC12:
	.string	"vector::reserve"
	.section	.text._ZN2v88internal8compiler39CreateStubBuiltinContinuationFrameStateEPNS1_7JSGraphENS0_8Builtins4NameEPNS1_4NodeEPKS7_iS7_NS1_26ContinuationFrameStateModeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler39CreateStubBuiltinContinuationFrameStateEPNS1_7JSGraphENS0_8Builtins4NameEPNS1_4NodeEPKS7_iS7_NS1_26ContinuationFrameStateModeE
	.type	_ZN2v88internal8compiler39CreateStubBuiltinContinuationFrameStateEPNS1_7JSGraphENS0_8Builtins4NameEPNS1_4NodeEPKS7_iS7_NS1_26ContinuationFrameStateModeE, @function
_ZN2v88internal8compiler39CreateStubBuiltinContinuationFrameStateEPNS1_7JSGraphENS0_8Builtins4NameEPNS1_4NodeEPKS7_iS7_NS1_26ContinuationFrameStateModeE:
.LFB21567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-112(%rbp), %rdi
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -136(%rbp)
	movl	%eax, %edx
	movl	16(%rbp), %ebx
	movl	%esi, -128(%rbp)
	movq	%r9, -144(%rbp)
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movq	360(%r12), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %r14
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	8(%r14), %r10d
	cmpl	$2, %ebx
	ja	.L86
	movl	(%r14), %eax
	subl	%ebx, %r10d
	js	.L116
	movabsq	$1152921504606846975, %rdx
	leal	(%r10,%rax), %ebx
	movslq	%ebx, %rbx
	cmpq	%rdx, %rbx
	ja	.L117
	testq	%rbx, %rbx
	je	.L106
	salq	$3, %rbx
	movl	%r10d, -120(%rbp)
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	-80(%rbp), %r11
	movq	-72(%rbp), %rdx
	movl	-120(%rbp), %r10d
	movq	%rax, %r15
	subq	%r11, %rdx
	testq	%rdx, %rdx
	jg	.L118
	testq	%r11, %r11
	jne	.L92
.L93:
	movq	%r15, %xmm0
	leaq	(%r15,%rbx), %rdx
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	(%r14), %eax
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L106:
	xorl	%edx, %edx
	xorl	%r15d, %r15d
.L90:
	testl	%r10d, %r10d
	je	.L88
	xorl	%ebx, %ebx
	leaq	-80(%rbp), %rdi
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L119:
	movq	(%r11), %rax
	addl	$1, %ebx
	movq	%rax, (%r15)
	movq	-72(%rbp), %rax
	leaq	8(%rax), %r15
	movq	%r15, -72(%rbp)
	movl	(%r14), %eax
	cmpl	%ebx, %r10d
	je	.L88
.L99:
	movq	-64(%rbp), %rdx
.L94:
	addl	%ebx, %eax
	cltq
	leaq	0(%r13,%rax,8), %r11
	cmpq	%rdx, %r15
	jne	.L119
	movq	%r15, %rsi
	movq	%r11, %rdx
	movl	%r10d, -124(%rbp)
	addl	$1, %ebx
	movq	%rdi, -120(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movl	-124(%rbp), %r10d
	movq	-72(%rbp), %r15
	movl	(%r14), %eax
	movq	-120(%rbp), %rdi
	cmpl	%ebx, %r10d
	jne	.L99
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%r13, %rcx
	xorl	%ebx, %ebx
	leaq	-80(%rbp), %r13
	testl	%eax, %eax
	jle	.L96
	.p2align 4,,10
	.p2align 3
.L103:
	cmpq	%r15, -64(%rbp)
	je	.L100
	movq	(%rcx), %rax
	addl	$1, %ebx
	addq	$8, %rcx
	movq	%rax, (%r15)
	movq	-72(%rbp), %rax
	leaq	8(%rax), %r15
	movq	%r15, -72(%rbp)
	cmpl	%ebx, (%r14)
	jg	.L103
.L96:
	movq	-80(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%r15, %r8
	subq	$8, %rsp
	movl	-128(%rbp), %edx
	subq	%rbx, %r8
	pushq	$0
	movq	%r12, %rdi
	movq	%rax, %rcx
	sarq	$3, %r8
	pushq	-144(%rbp)
	movq	%rbx, %r9
	movl	$3, %esi
	pushq	%r8
	movq	-136(%rbp), %r8
	call	_ZN2v88internal8compiler12_GLOBAL__N_141CreateBuiltinContinuationFrameStateCommonEPNS1_7JSGraphENS1_14FrameStateTypeENS0_8Builtins4NameEPNS1_4NodeES9_PS9_iS9_NS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	-80(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L85
	call	_ZdlPv@PLT
.L85:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L120
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	%rcx, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rcx, -120(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-120(%rbp), %rcx
	movq	-72(%rbp), %r15
	addl	$1, %ebx
	addq	$8, %rcx
	cmpl	(%r14), %ebx
	jl	.L103
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L116:
	xorl	%r15d, %r15d
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r11, %rsi
	movq	%rax, %rdi
	movl	%r10d, -124(%rbp)
	movq	%r11, -120(%rbp)
	call	memmove@PLT
	movq	-120(%rbp), %r11
	movl	-124(%rbp), %r10d
.L92:
	movq	%r11, %rdi
	movl	%r10d, -120(%rbp)
	call	_ZdlPv@PLT
	movl	-120(%rbp), %r10d
	jmp	.L93
.L117:
	leaq	.LC12(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L120:
	call	__stack_chk_fail@PLT
.L86:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21567:
	.size	_ZN2v88internal8compiler39CreateStubBuiltinContinuationFrameStateEPNS1_7JSGraphENS0_8Builtins4NameEPNS1_4NodeEPKS7_iS7_NS1_26ContinuationFrameStateModeE, .-_ZN2v88internal8compiler39CreateStubBuiltinContinuationFrameStateEPNS1_7JSGraphENS0_8Builtins4NameEPNS1_4NodeEPKS7_iS7_NS1_26ContinuationFrameStateModeE
	.section	.text._ZN2v88internal8compiler44CreateGenericLazyDeoptContinuationFrameStateEPNS1_7JSGraphERKNS1_21SharedFunctionInfoRefEPNS1_4NodeES8_S8_S8_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler44CreateGenericLazyDeoptContinuationFrameStateEPNS1_7JSGraphERKNS1_21SharedFunctionInfoRefEPNS1_4NodeES8_S8_S8_
	.type	_ZN2v88internal8compiler44CreateGenericLazyDeoptContinuationFrameStateEPNS1_7JSGraphERKNS1_21SharedFunctionInfoRefEPNS1_4NodeES8_S8_S8_, @function
_ZN2v88internal8compiler44CreateGenericLazyDeoptContinuationFrameStateEPNS1_7JSGraphERKNS1_21SharedFunctionInfoRefEPNS1_4NodeES8_S8_S8_:
.LFB21584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$612, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r8, -64(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal8Builtins22GetStackParameterCountENS1_4NameE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	-64(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	$0, -80(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, -104(%rbp)
	cmpq	-80(%rbp), %rsi
	je	.L122
	movq	-120(%rbp), %rax
	movq	%rax, (%rsi)
	movq	-88(%rbp), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, -88(%rbp)
	cmpq	%rsi, -80(%rbp)
	je	.L124
.L135:
	movq	-104(%rbp), %rax
	movq	%rax, (%rsi)
	movq	-88(%rbp), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, -88(%rbp)
	cmpq	%rsi, -80(%rbp)
	je	.L126
.L136:
	movq	-112(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -88(%rbp)
.L127:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef6objectEv@PLT
	subq	$8, %rsp
	movq	-96(%rbp), %r9
	movq	%r12, %rdi
	pushq	%rax
	movq	-88(%rbp), %rax
	movq	%rbx, %r8
	movl	$612, %edx
	pushq	%r13
	movq	-120(%rbp), %rcx
	movl	$4, %esi
	subq	%r9, %rax
	sarq	$3, %rax
	pushq	%rax
	call	_ZN2v88internal8compiler12_GLOBAL__N_141CreateBuiltinContinuationFrameStateCommonEPNS1_7JSGraphENS1_14FrameStateTypeENS0_8Builtins4NameEPNS1_4NodeES9_PS9_iS9_NS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L121
	call	_ZdlPv@PLT
.L121:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L134
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	leaq	-120(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-88(%rbp), %rsi
	cmpq	%rsi, -80(%rbp)
	jne	.L135
	.p2align 4,,10
	.p2align 3
.L124:
	leaq	-104(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-88(%rbp), %rsi
	cmpq	%rsi, -80(%rbp)
	jne	.L136
	.p2align 4,,10
	.p2align 3
.L126:
	leaq	-112(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L127
.L134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21584:
	.size	_ZN2v88internal8compiler44CreateGenericLazyDeoptContinuationFrameStateEPNS1_7JSGraphERKNS1_21SharedFunctionInfoRefEPNS1_4NodeES8_S8_S8_, .-_ZN2v88internal8compiler44CreateGenericLazyDeoptContinuationFrameStateEPNS1_7JSGraphERKNS1_21SharedFunctionInfoRefEPNS1_4NodeES8_S8_S8_
	.section	.text._ZN2v88internal8compiler45CreateJavaScriptBuiltinContinuationFrameStateEPNS1_7JSGraphERKNS1_21SharedFunctionInfoRefENS0_8Builtins4NameEPNS1_4NodeESA_PKSA_iSA_NS1_26ContinuationFrameStateModeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler45CreateJavaScriptBuiltinContinuationFrameStateEPNS1_7JSGraphERKNS1_21SharedFunctionInfoRefENS0_8Builtins4NameEPNS1_4NodeESA_PKSA_iSA_NS1_26ContinuationFrameStateModeE
	.type	_ZN2v88internal8compiler45CreateJavaScriptBuiltinContinuationFrameStateEPNS1_7JSGraphERKNS1_21SharedFunctionInfoRefENS0_8Builtins4NameEPNS1_4NodeESA_PKSA_iSA_NS1_26ContinuationFrameStateModeE, @function
_ZN2v88internal8compiler45CreateJavaScriptBuiltinContinuationFrameStateEPNS1_7JSGraphERKNS1_21SharedFunctionInfoRefENS0_8Builtins4NameEPNS1_4NodeESA_PKSA_iSA_NS1_26ContinuationFrameStateModeE:
.LFB21583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	%edx, %edi
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movl	16(%rbp), %ebx
	movq	%rsi, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%r8, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins22GetStackParameterCountENS1_4NameE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	testl	%ebx, %ebx
	jle	.L138
	leal	-1(%rbx), %eax
	xorl	%esi, %esi
	leaq	-80(%rbp), %r15
	leaq	8(%r14,%rax,8), %rbx
	xorl	%eax, %eax
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L156:
	movq	(%r14), %rax
	addq	$8, %r14
	movq	%rax, (%rsi)
	addq	$8, -72(%rbp)
	cmpq	%rbx, %r14
	je	.L138
.L157:
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rax
.L141:
	cmpq	%rax, %rsi
	jne	.L156
	movq	%r14, %rdx
	movq	%r15, %rdi
	addq	$8, %r14
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	cmpq	%rbx, %r14
	jne	.L157
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, -88(%rbp)
	cmpq	-64(%rbp), %rsi
	je	.L142
	movq	-104(%rbp), %rax
	movq	%rax, (%rsi)
	movq	-72(%rbp), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L144
.L159:
	movq	-88(%rbp), %rax
	movq	%rax, (%rsi)
	movq	-72(%rbp), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%rsi, -64(%rbp)
	je	.L146
.L160:
	movq	-96(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -72(%rbp)
.L147:
	movq	-112(%rbp), %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef6objectEv@PLT
	movq	-80(%rbp), %r9
	xorl	%esi, %esi
	movq	-104(%rbp), %rcx
	movq	%rax, %r8
	movq	-72(%rbp), %rax
	movq	%r12, %rdi
	movl	%r13d, %edx
	subq	%r9, %rax
	sarq	$3, %rax
	cmpl	$2, 32(%rbp)
	sete	%sil
	subq	$8, %rsp
	pushq	%r8
	movq	-120(%rbp), %r8
	addl	$4, %esi
	pushq	-128(%rbp)
	pushq	%rax
	call	_ZN2v88internal8compiler12_GLOBAL__N_141CreateBuiltinContinuationFrameStateCommonEPNS1_7JSGraphENS1_14FrameStateTypeENS0_8Builtins4NameEPNS1_4NodeES9_PS9_iS9_NS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	-80(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L137
	call	_ZdlPv@PLT
.L137:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L158
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	leaq	-104(%rbp), %rdx
	leaq	-80(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-72(%rbp), %rsi
	cmpq	%rsi, -64(%rbp)
	jne	.L159
	.p2align 4,,10
	.p2align 3
.L144:
	leaq	-88(%rbp), %rdx
	leaq	-80(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-72(%rbp), %rsi
	cmpq	%rsi, -64(%rbp)
	jne	.L160
	.p2align 4,,10
	.p2align 3
.L146:
	leaq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L147
.L158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21583:
	.size	_ZN2v88internal8compiler45CreateJavaScriptBuiltinContinuationFrameStateEPNS1_7JSGraphERKNS1_21SharedFunctionInfoRefENS0_8Builtins4NameEPNS1_4NodeESA_PKSA_iSA_NS1_26ContinuationFrameStateModeE, .-_ZN2v88internal8compiler45CreateJavaScriptBuiltinContinuationFrameStateEPNS1_7JSGraphERKNS1_21SharedFunctionInfoRefENS0_8Builtins4NameEPNS1_4NodeESA_PKSA_iSA_NS1_26ContinuationFrameStateModeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler10hash_valueERKNS1_23OutputFrameStateCombineE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler10hash_valueERKNS1_23OutputFrameStateCombineE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler10hash_valueERKNS1_23OutputFrameStateCombineE:
.LFB26295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26295:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler10hash_valueERKNS1_23OutputFrameStateCombineE, .-_GLOBAL__sub_I__ZN2v88internal8compiler10hash_valueERKNS1_23OutputFrameStateCombineE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler10hash_valueERKNS1_23OutputFrameStateCombineE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
