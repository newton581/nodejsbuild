	.file	"js-graph.cc"
	.text
	.section	.text._ZN2v88internal8compiler7JSGraph18CEntryStubConstantEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph18CEntryStubConstantEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb
	.type	_ZN2v88internal8compiler7JSGraph18CEntryStubConstantEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb, @function
_ZN2v88internal8compiler7JSGraph18CEntryStubConstantEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb:
.LFB21587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%edx, %eax
	orl	%ecx, %eax
	jne	.L2
	testb	%r8b, %r8b
	je	.L17
	movq	624(%rdi), %rax
	testq	%rax, %rax
	je	.L18
.L1:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L19
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	cmpl	$1, %esi
	je	.L20
	cmpl	$2, %esi
	je	.L21
	movq	616(%rdi), %rax
	leaq	616(%rdi), %r12
.L5:
	testq	%rax, %rax
	jne	.L1
	movq	360(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal11CodeFactory6CEntryEPNS0_7IsolateEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	leaq	24(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L15
	movq	8(%rbx), %rdi
	movq	(%rbx), %r14
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 0(%r13)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L2:
	movq	360(%rdi), %rdi
	movzbl	%r8b, %r8d
	call	_ZN2v88internal11CodeFactory6CEntryEPNS0_7IsolateEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	leaq	24(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L1
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L15:
	movq	%rax, (%r12)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	movq	608(%rdi), %rax
	leaq	608(%rdi), %r12
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L20:
	movq	600(%rdi), %rax
	leaq	600(%rdi), %r12
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L18:
	movq	360(%rdi), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal11CodeFactory6CEntryEPNS0_7IsolateEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	leaq	24(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L22
.L11:
	movq	%rax, 624(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L22:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L11
.L19:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21587:
	.size	_ZN2v88internal8compiler7JSGraph18CEntryStubConstantEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb, .-_ZN2v88internal8compiler7JSGraph18CEntryStubConstantEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb
	.section	.text._ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE
	.type	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE, @function
_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE:
.LFB21588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler9ObjectRef5IsSmiEv@PLT
	movq	%r12, %rdi
	testb	%al, %al
	je	.L24
	call	_ZNK2v88internal8compiler9ObjectRef5AsSmiEv@PLT
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movq	%xmm0, %rdx
	testq	%rdx, %rdx
	jne	.L32
.L61:
	movq	552(%rbx), %rax
	testq	%rax, %rax
	je	.L65
.L23:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L66
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapObjectEv@PLT
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef17GetHeapObjectTypeEv@PLT
	movq	%r12, %rdi
	shrq	$16, %rax
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapNumberEv@PLT
	testb	%al, %al
	jne	.L67
	cmpb	$2, %r13b
	je	.L68
	cmpb	$3, %r13b
	je	.L69
	cmpb	$4, %r13b
	je	.L70
	movq	%r12, %rdi
	cmpb	$1, %r13b
	je	.L71
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapObjectEv@PLT
	movq	%r14, %rdi
	movq	%rdx, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef6objectEv@PLT
	leaq	24(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L23
	movq	8(%rbx), %rdi
	leaq	-72(%rbp), %rsi
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapNumberEv@PLT
	movq	%r14, %rdi
	movq	%rdx, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal8compiler13HeapNumberRef5valueEv@PLT
	movq	%xmm0, %rdx
	testq	%rdx, %rdx
	je	.L61
.L32:
	movabsq	$4607182418800017408, %rax
	cmpq	%rax, %rdx
	jne	.L35
	movq	560(%rbx), %rax
	testq	%rax, %rax
	jne	.L23
	movq	344(%rbx), %rsi
	leaq	216(%rbx), %rdi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L72
.L36:
	movq	%rax, 560(%rbx)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L35:
	movq	344(%rbx), %rsi
	leaq	216(%rbx), %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	movsd	-88(%rbp), %xmm0
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L23
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder14NumberConstantEd@PLT
.L51:
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L69:
	movq	544(%rbx), %rax
	testq	%rax, %rax
	jne	.L23
	movq	360(%rbx), %rax
	leaq	24(%rbx), %rdi
	leaq	104(%rax), %rsi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L73
.L41:
	movq	%rax, 544(%rbx)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L68:
	movq	512(%rbx), %rax
	testq	%rax, %rax
	jne	.L23
	movq	360(%rbx), %rax
	leaq	24(%rbx), %rdi
	leaq	88(%rax), %rsi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L74
.L39:
	movq	%rax, 512(%rbx)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L65:
	movq	344(%rbx), %rsi
	leaq	216(%rbx), %rdi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L75
.L34:
	movq	%rax, 552(%rbx)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L75:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	pxor	%xmm0, %xmm0
	call	_ZN2v88internal8compiler21CommonOperatorBuilder14NumberConstantEd@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L70:
	movq	520(%rbx), %rax
	testq	%rax, %rax
	jne	.L23
	movq	360(%rbx), %rax
	leaq	24(%rbx), %rdi
	leaq	96(%rax), %rsi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L76
.L43:
	movq	%rax, 520(%rbx)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L71:
	call	_ZNK2v88internal8compiler9ObjectRef6objectEv@PLT
	movq	360(%rbx), %rdx
	leaq	112(%rdx), %rsi
	cmpq	%rax, %rsi
	je	.L77
	movq	536(%rbx), %rax
	testq	%rax, %rax
	jne	.L23
	leaq	120(%rdx), %rsi
	leaq	24(%rbx), %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L78
.L47:
	movq	%rax, 536(%rbx)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L77:
	movq	528(%rbx), %rax
	testq	%rax, %rax
	jne	.L23
	leaq	24(%rbx), %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L79
.L46:
	movq	%rax, 528(%rbx)
	jmp	.L23
.L72:
	movq	8(%rbx), %rdi
	movsd	.LC1(%rip), %xmm0
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder14NumberConstantEd@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L36
.L74:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L39
.L73:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L41
.L76:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L43
.L78:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L47
.L66:
	call	__stack_chk_fail@PLT
.L79:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L46
	.cfi_endproc
.LFE21588:
	.size	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE, .-_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE
	.section	.text._ZN2v88internal8compiler7JSGraph8ConstantEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph8ConstantEd
	.type	_ZN2v88internal8compiler7JSGraph8ConstantEd, @function
_ZN2v88internal8compiler7JSGraph8ConstantEd:
.LFB21589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%xmm0, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rdx, %rdx
	jne	.L81
	movq	552(%rdi), %rax
	testq	%rax, %rax
	je	.L88
.L80:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movabsq	$4607182418800017408, %rax
	cmpq	%rax, %rdx
	jne	.L84
	movq	560(%rdi), %rax
	testq	%rax, %rax
	jne	.L80
	movq	344(%rdi), %rsi
	leaq	216(%rdi), %rdi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L89
.L85:
	movq	%rax, 560(%rbx)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L84:
	movq	344(%rdi), %rsi
	leaq	216(%rdi), %rdi
	movsd	%xmm0, -40(%rbp)
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	movsd	-40(%rbp), %xmm0
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L80
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder14NumberConstantEd@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L88:
	movq	344(%rdi), %rsi
	leaq	216(%rdi), %rdi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L90
.L83:
	movq	%rax, 552(%rbx)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L90:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	pxor	%xmm0, %xmm0
	call	_ZN2v88internal8compiler21CommonOperatorBuilder14NumberConstantEd@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L83
.L89:
	movq	8(%rbx), %rdi
	movsd	.LC1(%rip), %xmm0
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder14NumberConstantEd@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L85
	.cfi_endproc
.LFE21589:
	.size	_ZN2v88internal8compiler7JSGraph8ConstantEd, .-_ZN2v88internal8compiler7JSGraph8ConstantEd
	.section	.text._ZN2v88internal8compiler7JSGraph14NumberConstantEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph14NumberConstantEd
	.type	_ZN2v88internal8compiler7JSGraph14NumberConstantEd, @function
_ZN2v88internal8compiler7JSGraph14NumberConstantEd:
.LFB21590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%xmm0, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%xmm0, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	344(%rdi), %rsi
	movq	%rdi, %rbx
	addq	$216, %rdi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L94
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%rbx), %r14
	movq	%r13, %xmm0
	call	_ZN2v88internal8compiler21CommonOperatorBuilder14NumberConstantEd@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21590:
	.size	_ZN2v88internal8compiler7JSGraph14NumberConstantEd, .-_ZN2v88internal8compiler7JSGraph14NumberConstantEd
	.section	.text._ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE
	.type	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE, @function
_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE:
.LFB21591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$24, %rdi
	subq	$24, %rsp
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L98
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-40(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21591:
	.size	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE, .-_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE
	.section	.text._ZN2v88internal8compiler7JSGraph37AllocateInYoungGenerationStubConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph37AllocateInYoungGenerationStubConstantEv
	.type	_ZN2v88internal8compiler7JSGraph37AllocateInYoungGenerationStubConstantEv, @function
_ZN2v88internal8compiler7JSGraph37AllocateInYoungGenerationStubConstantEv:
.LFB21593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	384(%rdi), %rax
	testq	%rax, %rax
	je	.L104
.L99:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L105
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	movl	$80, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	leaq	24(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L106
.L101:
	movq	%rax, 384(%rbx)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L106:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L101
.L105:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21593:
	.size	_ZN2v88internal8compiler7JSGraph37AllocateInYoungGenerationStubConstantEv, .-_ZN2v88internal8compiler7JSGraph37AllocateInYoungGenerationStubConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph44AllocateRegularInYoungGenerationStubConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph44AllocateRegularInYoungGenerationStubConstantEv
	.type	_ZN2v88internal8compiler7JSGraph44AllocateRegularInYoungGenerationStubConstantEv, @function
_ZN2v88internal8compiler7JSGraph44AllocateRegularInYoungGenerationStubConstantEv:
.LFB21594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	392(%rdi), %rax
	testq	%rax, %rax
	je	.L112
.L107:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L113
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	movl	$81, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	leaq	24(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L114
.L109:
	movq	%rax, 392(%rbx)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L114:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L109
.L113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21594:
	.size	_ZN2v88internal8compiler7JSGraph44AllocateRegularInYoungGenerationStubConstantEv, .-_ZN2v88internal8compiler7JSGraph44AllocateRegularInYoungGenerationStubConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph35AllocateInOldGenerationStubConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph35AllocateInOldGenerationStubConstantEv
	.type	_ZN2v88internal8compiler7JSGraph35AllocateInOldGenerationStubConstantEv, @function
_ZN2v88internal8compiler7JSGraph35AllocateInOldGenerationStubConstantEv:
.LFB21595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	400(%rdi), %rax
	testq	%rax, %rax
	je	.L120
.L115:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L121
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	movl	$82, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	leaq	24(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L122
.L117:
	movq	%rax, 400(%rbx)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L122:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L117
.L121:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21595:
	.size	_ZN2v88internal8compiler7JSGraph35AllocateInOldGenerationStubConstantEv, .-_ZN2v88internal8compiler7JSGraph35AllocateInOldGenerationStubConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph42AllocateRegularInOldGenerationStubConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph42AllocateRegularInOldGenerationStubConstantEv
	.type	_ZN2v88internal8compiler7JSGraph42AllocateRegularInOldGenerationStubConstantEv, @function
_ZN2v88internal8compiler7JSGraph42AllocateRegularInOldGenerationStubConstantEv:
.LFB21596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	408(%rdi), %rax
	testq	%rax, %rax
	je	.L128
.L123:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L129
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	movl	$83, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	leaq	24(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L130
.L125:
	movq	%rax, 408(%rbx)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L130:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L125
.L129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21596:
	.size	_ZN2v88internal8compiler7JSGraph42AllocateRegularInOldGenerationStubConstantEv, .-_ZN2v88internal8compiler7JSGraph42AllocateRegularInOldGenerationStubConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph28ArrayConstructorStubConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph28ArrayConstructorStubConstantEv
	.type	_ZN2v88internal8compiler7JSGraph28ArrayConstructorStubConstantEv, @function
_ZN2v88internal8compiler7JSGraph28ArrayConstructorStubConstantEv:
.LFB21597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	416(%rdi), %rax
	testq	%rax, %rax
	je	.L136
.L131:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L137
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	movl	$171, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	leaq	24(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L138
.L133:
	movq	%rax, 416(%rbx)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L138:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L133
.L137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21597:
	.size	_ZN2v88internal8compiler7JSGraph28ArrayConstructorStubConstantEv, .-_ZN2v88internal8compiler7JSGraph28ArrayConstructorStubConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph17BigIntMapConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph17BigIntMapConstantEv
	.type	_ZN2v88internal8compiler7JSGraph17BigIntMapConstantEv, @function
_ZN2v88internal8compiler7JSGraph17BigIntMapConstantEv:
.LFB21598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	424(%rdi), %rax
	testq	%rax, %rax
	je	.L144
.L139:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L145
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	448(%rax), %rsi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L146
.L141:
	movq	%rax, 424(%rbx)
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L146:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L141
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21598:
	.size	_ZN2v88internal8compiler7JSGraph17BigIntMapConstantEv, .-_ZN2v88internal8compiler7JSGraph17BigIntMapConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph18BooleanMapConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph18BooleanMapConstantEv
	.type	_ZN2v88internal8compiler7JSGraph18BooleanMapConstantEv, @function
_ZN2v88internal8compiler7JSGraph18BooleanMapConstantEv:
.LFB21599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	432(%rdi), %rax
	testq	%rax, %rax
	je	.L152
.L147:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L153
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	896(%rax), %rsi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L154
.L149:
	movq	%rax, 432(%rbx)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L154:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L149
.L153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21599:
	.size	_ZN2v88internal8compiler7JSGraph18BooleanMapConstantEv, .-_ZN2v88internal8compiler7JSGraph18BooleanMapConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph23ToNumberBuiltinConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph23ToNumberBuiltinConstantEv
	.type	_ZN2v88internal8compiler7JSGraph23ToNumberBuiltinConstantEv, @function
_ZN2v88internal8compiler7JSGraph23ToNumberBuiltinConstantEv:
.LFB21600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	440(%rdi), %rax
	testq	%rax, %rax
	je	.L160
.L155:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L161
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	movl	$102, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	leaq	24(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L162
.L157:
	movq	%rax, 440(%rbx)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L162:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L157
.L161:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21600:
	.size	_ZN2v88internal8compiler7JSGraph23ToNumberBuiltinConstantEv, .-_ZN2v88internal8compiler7JSGraph23ToNumberBuiltinConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv
	.type	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv, @function
_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv:
.LFB21601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	448(%rdi), %rax
	testq	%rax, %rax
	je	.L168
.L163:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L169
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	288(%rax), %rsi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L170
.L165:
	movq	%rax, 448(%rbx)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L170:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L165
.L169:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21601:
	.size	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv, .-_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph19EmptyStringConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph19EmptyStringConstantEv
	.type	_ZN2v88internal8compiler7JSGraph19EmptyStringConstantEv, @function
_ZN2v88internal8compiler7JSGraph19EmptyStringConstantEv:
.LFB21602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	456(%rdi), %rax
	testq	%rax, %rax
	je	.L176
.L171:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L177
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	128(%rax), %rsi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L178
.L173:
	movq	%rax, 456(%rbx)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L178:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L173
.L177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21602:
	.size	_ZN2v88internal8compiler7JSGraph19EmptyStringConstantEv, .-_ZN2v88internal8compiler7JSGraph19EmptyStringConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph21FixedArrayMapConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph21FixedArrayMapConstantEv
	.type	_ZN2v88internal8compiler7JSGraph21FixedArrayMapConstantEv, @function
_ZN2v88internal8compiler7JSGraph21FixedArrayMapConstantEv:
.LFB21603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	464(%rdi), %rax
	testq	%rax, %rax
	je	.L184
.L179:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L185
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	152(%rax), %rsi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L186
.L181:
	movq	%rax, 464(%rbx)
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L186:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L181
.L185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21603:
	.size	_ZN2v88internal8compiler7JSGraph21FixedArrayMapConstantEv, .-_ZN2v88internal8compiler7JSGraph21FixedArrayMapConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph24PropertyArrayMapConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph24PropertyArrayMapConstantEv
	.type	_ZN2v88internal8compiler7JSGraph24PropertyArrayMapConstantEv, @function
_ZN2v88internal8compiler7JSGraph24PropertyArrayMapConstantEv:
.LFB21604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	472(%rdi), %rax
	testq	%rax, %rax
	je	.L192
.L187:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L193
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	584(%rax), %rsi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L194
.L189:
	movq	%rax, 472(%rbx)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L194:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L189
.L193:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21604:
	.size	_ZN2v88internal8compiler7JSGraph24PropertyArrayMapConstantEv, .-_ZN2v88internal8compiler7JSGraph24PropertyArrayMapConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph27FixedDoubleArrayMapConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph27FixedDoubleArrayMapConstantEv
	.type	_ZN2v88internal8compiler7JSGraph27FixedDoubleArrayMapConstantEv, @function
_ZN2v88internal8compiler7JSGraph27FixedDoubleArrayMapConstantEv:
.LFB21605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	480(%rdi), %rax
	testq	%rax, %rax
	je	.L200
.L195:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L201
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	488(%rax), %rsi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L202
.L197:
	movq	%rax, 480(%rbx)
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L202:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L197
.L201:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21605:
	.size	_ZN2v88internal8compiler7JSGraph27FixedDoubleArrayMapConstantEv, .-_ZN2v88internal8compiler7JSGraph27FixedDoubleArrayMapConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph21HeapNumberMapConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph21HeapNumberMapConstantEv
	.type	_ZN2v88internal8compiler7JSGraph21HeapNumberMapConstantEv, @function
_ZN2v88internal8compiler7JSGraph21HeapNumberMapConstantEv:
.LFB21606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	488(%rdi), %rax
	testq	%rax, %rax
	je	.L208
.L203:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L209
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	256(%rax), %rsi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L210
.L205:
	movq	%rax, 488(%rbx)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L210:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L205
.L209:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21606:
	.size	_ZN2v88internal8compiler7JSGraph21HeapNumberMapConstantEv, .-_ZN2v88internal8compiler7JSGraph21HeapNumberMapConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph20OptimizedOutConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph20OptimizedOutConstantEv
	.type	_ZN2v88internal8compiler7JSGraph20OptimizedOutConstantEv, @function
_ZN2v88internal8compiler7JSGraph20OptimizedOutConstantEv:
.LFB21607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	496(%rdi), %rax
	testq	%rax, %rax
	je	.L216
.L211:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L217
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	328(%rax), %rsi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L218
.L213:
	movq	%rax, 496(%rbx)
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L218:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L213
.L217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21607:
	.size	_ZN2v88internal8compiler7JSGraph20OptimizedOutConstantEv, .-_ZN2v88internal8compiler7JSGraph20OptimizedOutConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph21StaleRegisterConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph21StaleRegisterConstantEv
	.type	_ZN2v88internal8compiler7JSGraph21StaleRegisterConstantEv, @function
_ZN2v88internal8compiler7JSGraph21StaleRegisterConstantEv:
.LFB21608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	504(%rdi), %rax
	testq	%rax, %rax
	je	.L224
.L219:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L225
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	336(%rax), %rsi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L226
.L221:
	movq	%rax, 504(%rbx)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L226:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L221
.L225:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21608:
	.size	_ZN2v88internal8compiler7JSGraph21StaleRegisterConstantEv, .-_ZN2v88internal8compiler7JSGraph21StaleRegisterConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph17UndefinedConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv
	.type	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv, @function
_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv:
.LFB21609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	512(%rdi), %rax
	testq	%rax, %rax
	je	.L232
.L227:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L233
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	88(%rax), %rsi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L234
.L229:
	movq	%rax, 512(%rbx)
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L234:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L229
.L233:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21609:
	.size	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv, .-_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph15TheHoleConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv
	.type	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv, @function
_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv:
.LFB21610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	520(%rdi), %rax
	testq	%rax, %rax
	je	.L240
.L235:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L241
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	96(%rax), %rsi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L242
.L237:
	movq	%rax, 520(%rbx)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L242:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L237
.L241:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21610:
	.size	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv, .-_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph12TrueConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph12TrueConstantEv
	.type	_ZN2v88internal8compiler7JSGraph12TrueConstantEv, @function
_ZN2v88internal8compiler7JSGraph12TrueConstantEv:
.LFB21611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	528(%rdi), %rax
	testq	%rax, %rax
	je	.L248
.L243:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L249
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	112(%rax), %rsi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L250
.L245:
	movq	%rax, 528(%rbx)
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L250:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L245
.L249:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21611:
	.size	_ZN2v88internal8compiler7JSGraph12TrueConstantEv, .-_ZN2v88internal8compiler7JSGraph12TrueConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph13FalseConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph13FalseConstantEv
	.type	_ZN2v88internal8compiler7JSGraph13FalseConstantEv, @function
_ZN2v88internal8compiler7JSGraph13FalseConstantEv:
.LFB21612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	536(%rdi), %rax
	testq	%rax, %rax
	je	.L256
.L251:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L257
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	120(%rax), %rsi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L258
.L253:
	movq	%rax, 536(%rbx)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L258:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L253
.L257:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21612:
	.size	_ZN2v88internal8compiler7JSGraph13FalseConstantEv, .-_ZN2v88internal8compiler7JSGraph13FalseConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph12NullConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph12NullConstantEv
	.type	_ZN2v88internal8compiler7JSGraph12NullConstantEv, @function
_ZN2v88internal8compiler7JSGraph12NullConstantEv:
.LFB21613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	544(%rdi), %rax
	testq	%rax, %rax
	je	.L264
.L259:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L265
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	leaq	104(%rax), %rsi
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler15CommonNodeCache16FindHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L266
.L261:
	movq	%rax, 544(%rbx)
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L266:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12HeapConstantERKNS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L261
.L265:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21613:
	.size	_ZN2v88internal8compiler7JSGraph12NullConstantEv, .-_ZN2v88internal8compiler7JSGraph12NullConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph12ZeroConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv
	.type	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv, @function
_ZN2v88internal8compiler7JSGraph12ZeroConstantEv:
.LFB21614:
	.cfi_startproc
	endbr64
	movq	552(%rdi), %rax
	testq	%rax, %rax
	je	.L274
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	216(%rdi), %rdi
	subq	$8, %rsp
	movq	128(%rdi), %rsi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L275
.L269:
	movq	%rax, 552(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	pxor	%xmm0, %xmm0
	call	_ZN2v88internal8compiler21CommonOperatorBuilder14NumberConstantEd@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L269
	.cfi_endproc
.LFE21614:
	.size	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv, .-_ZN2v88internal8compiler7JSGraph12ZeroConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph11OneConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph11OneConstantEv
	.type	_ZN2v88internal8compiler7JSGraph11OneConstantEv, @function
_ZN2v88internal8compiler7JSGraph11OneConstantEv:
.LFB21615:
	.cfi_startproc
	endbr64
	movq	560(%rdi), %rax
	testq	%rax, %rax
	je	.L283
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	movabsq	$4607182418800017408, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	216(%rdi), %rdi
	subq	$8, %rsp
	movq	128(%rdi), %rsi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L284
.L278:
	movq	%rax, 560(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movsd	.LC1(%rip), %xmm0
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder14NumberConstantEd@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L278
	.cfi_endproc
.LFE21615:
	.size	_ZN2v88internal8compiler7JSGraph11OneConstantEv, .-_ZN2v88internal8compiler7JSGraph11OneConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph16MinusOneConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph16MinusOneConstantEv
	.type	_ZN2v88internal8compiler7JSGraph16MinusOneConstantEv, @function
_ZN2v88internal8compiler7JSGraph16MinusOneConstantEv:
.LFB21616:
	.cfi_startproc
	endbr64
	movq	576(%rdi), %rax
	testq	%rax, %rax
	je	.L292
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	movabsq	$-4616189618054758400, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	216(%rdi), %rdi
	subq	$8, %rsp
	movq	128(%rdi), %rsi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L293
.L287:
	movq	%rax, 576(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movsd	.LC2(%rip), %xmm0
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder14NumberConstantEd@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L287
	.cfi_endproc
.LFE21616:
	.size	_ZN2v88internal8compiler7JSGraph16MinusOneConstantEv, .-_ZN2v88internal8compiler7JSGraph16MinusOneConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph11NaNConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph11NaNConstantEv
	.type	_ZN2v88internal8compiler7JSGraph11NaNConstantEv, @function
_ZN2v88internal8compiler7JSGraph11NaNConstantEv:
.LFB21617:
	.cfi_startproc
	endbr64
	movq	568(%rdi), %rax
	testq	%rax, %rax
	je	.L301
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	movabsq	$9221120237041090560, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	216(%rdi), %rdi
	subq	$8, %rsp
	movq	128(%rdi), %rsi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L302
.L296:
	movq	%rax, 568(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movsd	.LC3(%rip), %xmm0
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder14NumberConstantEd@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	jmp	.L296
	.cfi_endproc
.LFE21617:
	.size	_ZN2v88internal8compiler7JSGraph11NaNConstantEv, .-_ZN2v88internal8compiler7JSGraph11NaNConstantEv
	.section	.text._ZN2v88internal8compiler7JSGraph16EmptyStateValuesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph16EmptyStateValuesEv
	.type	_ZN2v88internal8compiler7JSGraph16EmptyStateValuesEv, @function
_ZN2v88internal8compiler7JSGraph16EmptyStateValuesEv:
.LFB21618:
	.cfi_startproc
	endbr64
	movq	584(%rdi), %rax
	testq	%rax, %rax
	je	.L309
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %r12
	movq	8(%rdi), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11StateValuesEiNS1_15SparseInputMaskE@PLT
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 584(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21618:
	.size	_ZN2v88internal8compiler7JSGraph16EmptyStateValuesEv, .-_ZN2v88internal8compiler7JSGraph16EmptyStateValuesEv
	.section	.text._ZN2v88internal8compiler7JSGraph26SingleDeadTypedStateValuesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph26SingleDeadTypedStateValuesEv
	.type	_ZN2v88internal8compiler7JSGraph26SingleDeadTypedStateValuesEv, @function
_ZN2v88internal8compiler7JSGraph26SingleDeadTypedStateValuesEv:
.LFB21619:
	.cfi_startproc
	endbr64
	movq	592(%rdi), %rax
	testq	%rax, %rax
	je	.L318
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r13
	movq	%rdi, %rbx
	movq	8(%rdi), %r14
	movq	0(%r13), %r12
	movq	16(%r12), %rsi
	movq	24(%r12), %rax
	subq	%rsi, %rax
	cmpq	$31, %rax
	jbe	.L319
	leaq	32(%rsi), %rax
	movq	%rax, 16(%r12)
.L313:
	movq	%r12, (%rsi)
	movq	%r14, %rdi
	movl	$2, %edx
	movq	$0, 8(%rsi)
	movq	$0, 16(%rsi)
	movq	$0, 24(%rsi)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder16TypedStateValuesEPKNS0_10ZoneVectorINS0_11MachineTypeEEENS1_15SparseInputMaskE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 592(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L313
	.cfi_endproc
.LFE21619:
	.size	_ZN2v88internal8compiler7JSGraph26SingleDeadTypedStateValuesEv, .-_ZN2v88internal8compiler7JSGraph26SingleDeadTypedStateValuesEv
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB24610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L358
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L336
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L359
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L322:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L360
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L325:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L359:
	testq	%rdx, %rdx
	jne	.L361
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L323:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L326
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L339
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L339
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L328:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L328
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L330
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L330:
	leaq	16(%rax,%r8), %rcx
.L326:
	cmpq	%r14, %r12
	je	.L331
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L340
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L340
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L333:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L333
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L335
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L335:
	leaq	8(%rcx,%r9), %rcx
.L331:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L340:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L332:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L332
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L339:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L327:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L327
	jmp	.L330
.L360:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L325
.L358:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L361:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L322
	.cfi_endproc
.LFE24610:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal8compiler7JSGraph14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7JSGraph14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE
	.type	_ZN2v88internal8compiler7JSGraph14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE, @function
_ZN2v88internal8compiler7JSGraph14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE:
.LFB21592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$24, %rdi
	call	_ZN2v88internal8compiler15CommonNodeCache14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	movq	384(%rbx), %rax
	testq	%rax, %rax
	je	.L363
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L364
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L363:
	movq	392(%rbx), %rax
	testq	%rax, %rax
	je	.L365
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L366
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L365:
	movq	400(%rbx), %rax
	testq	%rax, %rax
	je	.L367
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L368
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L367:
	movq	408(%rbx), %rax
	testq	%rax, %rax
	je	.L369
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L370
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L369:
	movq	416(%rbx), %rax
	testq	%rax, %rax
	je	.L371
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L372
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L371:
	movq	424(%rbx), %rax
	testq	%rax, %rax
	je	.L373
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L374
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L373:
	movq	432(%rbx), %rax
	testq	%rax, %rax
	je	.L375
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L376
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L375:
	movq	440(%rbx), %rax
	testq	%rax, %rax
	je	.L377
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L378
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L377:
	movq	448(%rbx), %rax
	testq	%rax, %rax
	je	.L379
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L380
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L379:
	movq	456(%rbx), %rax
	testq	%rax, %rax
	je	.L381
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L382
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L381:
	movq	464(%rbx), %rax
	testq	%rax, %rax
	je	.L383
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L384
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L383:
	movq	472(%rbx), %rax
	testq	%rax, %rax
	je	.L385
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L386
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L385:
	movq	480(%rbx), %rax
	testq	%rax, %rax
	je	.L387
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L388
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L387:
	movq	488(%rbx), %rax
	testq	%rax, %rax
	je	.L389
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L390
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L389:
	movq	496(%rbx), %rax
	testq	%rax, %rax
	je	.L391
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L392
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L391:
	movq	504(%rbx), %rax
	testq	%rax, %rax
	je	.L393
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L394
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L393:
	movq	512(%rbx), %rax
	testq	%rax, %rax
	je	.L395
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L396
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L395:
	movq	520(%rbx), %rax
	testq	%rax, %rax
	je	.L397
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L398
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L397:
	movq	528(%rbx), %rax
	testq	%rax, %rax
	je	.L399
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L400
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L399:
	movq	536(%rbx), %rax
	testq	%rax, %rax
	je	.L401
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L402
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L401:
	movq	544(%rbx), %rax
	testq	%rax, %rax
	je	.L403
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L404
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L403:
	movq	552(%rbx), %rax
	testq	%rax, %rax
	je	.L405
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L406
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L405:
	movq	560(%rbx), %rax
	testq	%rax, %rax
	je	.L407
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L408
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L407:
	movq	568(%rbx), %rax
	testq	%rax, %rax
	je	.L409
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L410
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L409:
	movq	576(%rbx), %rax
	testq	%rax, %rax
	je	.L411
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L412
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L411:
	movq	584(%rbx), %rax
	testq	%rax, %rax
	je	.L413
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L414
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L413:
	movq	592(%rbx), %rax
	testq	%rax, %rax
	je	.L415
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L416
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L415:
	movq	600(%rbx), %rax
	testq	%rax, %rax
	je	.L417
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L418
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L417:
	movq	608(%rbx), %rax
	testq	%rax, %rax
	je	.L419
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L420
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L419:
	movq	616(%rbx), %rax
	testq	%rax, %rax
	je	.L421
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L422
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L421:
	movq	624(%rbx), %rax
	testq	%rax, %rax
	je	.L362
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L424
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L362:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	leaq	624(%rbx), %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	leaq	496(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L394:
	leaq	504(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L396:
	leaq	512(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L398:
	leaq	520(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L400:
	leaq	528(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L402:
	leaq	536(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L404:
	leaq	544(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L406:
	leaq	552(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L408:
	leaq	560(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L410:
	leaq	568(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L412:
	leaq	576(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L414:
	leaq	584(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L416:
	leaq	592(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L418:
	leaq	600(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L420:
	leaq	608(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L422:
	leaq	616(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L376:
	leaq	432(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L378:
	leaq	440(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L380:
	leaq	448(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L382:
	leaq	456(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L384:
	leaq	464(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L386:
	leaq	472(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L388:
	leaq	480(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L390:
	leaq	488(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L368:
	leaq	400(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L370:
	leaq	408(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L372:
	leaq	416(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	424(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L364:
	leaq	384(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L366:
	leaq	392(%rbx), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L365
	.cfi_endproc
.LFE21592:
	.size	_ZN2v88internal8compiler7JSGraph14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE, .-_ZN2v88internal8compiler7JSGraph14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler7JSGraph18CEntryStubConstantEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler7JSGraph18CEntryStubConstantEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb, @function
_GLOBAL__sub_I__ZN2v88internal8compiler7JSGraph18CEntryStubConstantEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb:
.LFB26343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26343:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler7JSGraph18CEntryStubConstantEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb, .-_GLOBAL__sub_I__ZN2v88internal8compiler7JSGraph18CEntryStubConstantEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler7JSGraph18CEntryStubConstantEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1072693248
	.align 8
.LC2:
	.long	0
	.long	-1074790400
	.align 8
.LC3:
	.long	0
	.long	2146959360
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
