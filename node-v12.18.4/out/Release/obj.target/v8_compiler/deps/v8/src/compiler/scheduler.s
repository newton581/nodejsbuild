	.file	"scheduler.cc"
	.text
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB14202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE14202:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB14692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14692:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB14203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14203:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB14693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE14693:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal8compiler9Scheduler19InitializePlacementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler19InitializePlacementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler9Scheduler19InitializePlacementEPNS1_4NodeE, @function
_ZN2v88internal8compiler9Scheduler19InitializePlacementEPNS1_4NodeE:
.LFB10884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	20(%rsi), %ebx
	andl	$16777215, %ebx
	salq	$4, %rbx
	addq	200(%rdi), %rbx
	movl	12(%rbx), %eax
	cmpl	$2, %eax
	je	.L10
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	movzwl	16(%rax), %eax
	cmpw	$36, %ax
	ja	.L12
	cmpw	$34, %ax
	ja	.L13
.L14:
	movl	$1, 12(%rbx)
	movl	$1, %eax
.L10:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	subl	$50, %eax
	cmpw	$1, %ax
	ja	.L14
	movl	$2, 12(%rbx)
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	$3, %edx
	movl	20(%rax), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	200(%r12), %rax
	movl	12(%rax), %eax
	cmpl	$2, %eax
	cmovne	%edx, %eax
	movl	%eax, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10884:
	.size	_ZN2v88internal8compiler9Scheduler19InitializePlacementEPNS1_4NodeE, .-_ZN2v88internal8compiler9Scheduler19InitializePlacementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler9Scheduler12GetPlacementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler12GetPlacementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler9Scheduler12GetPlacementEPNS1_4NodeE, @function
_ZN2v88internal8compiler9Scheduler12GetPlacementEPNS1_4NodeE:
.LFB10885:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	200(%rdi), %rax
	movl	12(%rax), %eax
	ret
	.cfi_endproc
.LFE10885:
	.size	_ZN2v88internal8compiler9Scheduler12GetPlacementEPNS1_4NodeE, .-_ZN2v88internal8compiler9Scheduler12GetPlacementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler9Scheduler6IsLiveEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler6IsLiveEPNS1_4NodeE
	.type	_ZN2v88internal8compiler9Scheduler6IsLiveEPNS1_4NodeE, @function
_ZN2v88internal8compiler9Scheduler6IsLiveEPNS1_4NodeE:
.LFB10886:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	200(%rdi), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE10886:
	.size	_ZN2v88internal8compiler9Scheduler6IsLiveEPNS1_4NodeE, .-_ZN2v88internal8compiler9Scheduler6IsLiveEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler9Scheduler28IncrementUnscheduledUseCountEPNS1_4NodeEiS4_.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"  Use count of #%d:%s (used by #%d:%s)++ = %d\n"
	.section	.text._ZN2v88internal8compiler9Scheduler28IncrementUnscheduledUseCountEPNS1_4NodeEiS4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler28IncrementUnscheduledUseCountEPNS1_4NodeEiS4_
	.type	_ZN2v88internal8compiler9Scheduler28IncrementUnscheduledUseCountEPNS1_4NodeEiS4_, @function
_ZN2v88internal8compiler9Scheduler28IncrementUnscheduledUseCountEPNS1_4NodeEiS4_:
.LFB10889:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L28:
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rdx, %rax
	movl	12(%rax), %edx
	cmpl	$2, %edx
	je	.L27
	cmpl	$3, %edx
	jne	.L32
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L33:
	movl	20(%r14), %eax
	movq	200(%rbx), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	cmpl	$3, 12(%rdx,%rax)
	jne	.L28
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE@PLT
	cmpl	%eax, %r12d
	je	.L27
	movq	200(%rbx), %rdx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L32:
	addl	$1, 8(%rax)
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L38
.L27:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movl	20(%r13), %esi
	movq	(%r14), %rdi
	movq	0(%r13), %rdx
	movl	20(%r14), %ecx
	andl	$16777215, %esi
	movq	8(%rdi), %r8
	leaq	.LC0(%rip), %rdi
	movl	%esi, %eax
	movq	8(%rdx), %rdx
	andl	$16777215, %ecx
	salq	$4, %rax
	addq	200(%rbx), %rax
	popq	%rbx
	movl	8(%rax), %r9d
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
	.cfi_endproc
.LFE10889:
	.size	_ZN2v88internal8compiler9Scheduler28IncrementUnscheduledUseCountEPNS1_4NodeEiS4_, .-_ZN2v88internal8compiler9Scheduler28IncrementUnscheduledUseCountEPNS1_4NodeEiS4_
	.section	.rodata._ZN2v88internal8compiler9Scheduler28PropagateImmediateDominatorsEPNS1_10BasicBlockE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Block id:%d's idom is id:%d, depth = %d\n"
	.section	.text._ZN2v88internal8compiler9Scheduler28PropagateImmediateDominatorsEPNS1_10BasicBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler28PropagateImmediateDominatorsEPNS1_10BasicBlockE
	.type	_ZN2v88internal8compiler9Scheduler28PropagateImmediateDominatorsEPNS1_10BasicBlockE, @function
_ZN2v88internal8compiler9Scheduler28PropagateImmediateDominatorsEPNS1_10BasicBlockE:
.LFB10959:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L58
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	.LC1(%rip), %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	.p2align 4,,10
	.p2align 3
.L46:
	movq	136(%r14), %rbx
	movq	144(%r14), %r12
	movq	(%rbx), %rdi
	movzbl	8(%rdi), %r15d
.L61:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	je	.L41
.L62:
	movq	(%rbx), %rsi
	movl	12(%rsi), %eax
	testl	%eax, %eax
	js	.L61
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	addq	$8, %rbx
	movq	%rax, %rdi
	movq	-8(%rbx), %rax
	andb	8(%rax), %r15b
	cmpq	%rbx, %r12
	jne	.L62
.L41:
	movq	%rdi, 16(%r14)
	movl	12(%rdi), %eax
	orb	%r15b, 8(%r14)
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	leal	1(%rax), %ecx
	movl	%ecx, 12(%r14)
	jne	.L63
	movq	24(%r14), %r14
	testq	%r14, %r14
	jne	.L46
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movq	160(%rdi), %rdx
	movl	160(%r14), %esi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	24(%r14), %r14
	testq	%r14, %r14
	jne	.L46
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L58:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE10959:
	.size	_ZN2v88internal8compiler9Scheduler28PropagateImmediateDominatorsEPNS1_10BasicBlockE, .-_ZN2v88internal8compiler9Scheduler28PropagateImmediateDominatorsEPNS1_10BasicBlockE
	.section	.rodata._ZN2v88internal8compiler9Scheduler30GenerateImmediateDominatorTreeEv.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"--- IMMEDIATE BLOCK DOMINATORS -----------------------------\n"
	.section	.text._ZN2v88internal8compiler9Scheduler30GenerateImmediateDominatorTreeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler30GenerateImmediateDominatorTreeEv
	.type	_ZN2v88internal8compiler9Scheduler30GenerateImmediateDominatorTreeEv, @function
_ZN2v88internal8compiler9Scheduler30GenerateImmediateDominatorTreeEv:
.LFB10960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L67
.L65:
	movq	16(%r12), %rax
	movq	%r12, %rdi
	movq	104(%rax), %rax
	movl	$0, 12(%rax)
	movq	16(%r12), %rax
	movq	104(%rax), %rax
	movq	24(%rax), %rsi
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler9Scheduler28PropagateImmediateDominatorsEPNS1_10BasicBlockE
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L65
	.cfi_endproc
.LFE10960:
	.size	_ZN2v88internal8compiler9Scheduler30GenerateImmediateDominatorTreeEv, .-_ZN2v88internal8compiler9Scheduler30GenerateImmediateDominatorTreeEv
	.section	.rodata._ZN2v88internal8compiler23ScheduleLateNodeVisitor14GetBlockForUseENS1_4EdgeE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"  inspecting uses of coupled #%d:%s\n"
	.align 8
.LC4:
	.string	"  input@%d into a fixed phi #%d:%s\n"
	.align 8
.LC5:
	.string	"  input@%d into a fixed merge #%d:%s\n"
	.align 8
.LC6:
	.string	"  must dominate use #%d:%s in id:%d\n"
	.section	.text._ZN2v88internal8compiler23ScheduleLateNodeVisitor14GetBlockForUseENS1_4EdgeE,"axG",@progbits,_ZN2v88internal8compiler23ScheduleLateNodeVisitor14GetBlockForUseENS1_4EdgeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler23ScheduleLateNodeVisitor14GetBlockForUseENS1_4EdgeE
	.type	_ZN2v88internal8compiler23ScheduleLateNodeVisitor14GetBlockForUseENS1_4EdgeE, @function
_ZN2v88internal8compiler23ScheduleLateNodeVisitor14GetBlockForUseENS1_4EdgeE:
.LFB11011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	16(%rsi), %edx
	movq	%rdi, %rbx
	movl	%edx, %esi
	shrl	%esi
	andl	$1, %edx
	movl	%esi, %eax
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r12,%rax,8), %r13
	movq	0(%r13), %rax
	jne	.L69
	movq	%rax, %r13
	movq	(%rax), %rax
.L69:
	movzwl	16(%rax), %edx
	leal	-35(%rdx), %ecx
	cmpl	$1, %ecx
	jbe	.L103
	cmpl	$10, %edx
	je	.L87
	cmpl	$1, %edx
	je	.L87
.L79:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L68
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L104
.L68:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movl	20(%r13), %r8d
	movq	8(%rbx), %rcx
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	addq	200(%rcx), %rdx
	cmpl	$2, 12(%rdx)
	jne	.L79
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L105
.L83:
	movq	(%r14), %r13
	movq	224(%rcx), %rbx
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L84:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L106
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movl	20(%r13), %r8d
	movq	8(%rbx), %rcx
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	addq	200(%rcx), %rdx
	movl	12(%rdx), %edx
	cmpl	$3, %edx
	je	.L107
	cmpl	$2, %edx
	jne	.L79
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L108
.L80:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%r12), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	movq	8(%rbx), %rax
	movq	224(%rax), %rbx
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L81:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L109
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L104:
	movq	0(%r13), %rax
	movl	20(%r13), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r12), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L110
.L72:
	movq	24(%r13), %rsi
	xorl	%r12d, %r12d
	testq	%rsi, %rsi
	je	.L68
	movq	(%rsi), %r13
	.p2align 4,,10
	.p2align 3
.L77:
	movl	16(%rsi), %ecx
	movq	8(%rbx), %rdi
	movl	%ecx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rdx
	salq	$3, %rax
	andl	$1, %ecx
	leaq	(%rsi,%rdx,8), %rdx
	leaq	32(%rdx,%rax), %r8
	jne	.L75
	leaq	16(%rdx,%rax), %r8
	movq	(%rdx), %rdx
.L75:
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	200(%rdi), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	je	.L76
	movq	%r8, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler23ScheduleLateNodeVisitor14GetBlockForUseENS1_4EdgeE
	testq	%r12, %r12
	je	.L86
	testq	%rax, %rax
	je	.L76
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L76:
	testq	%r13, %r13
	je	.L68
	movq	%r13, %rsi
	movq	0(%r13), %r13
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%rax, %r12
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L110:
	movq	8(%rax), %rdx
	movl	%r8d, %esi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L105:
	movq	8(%rax), %rcx
	movl	%r8d, %edx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%rbx), %rcx
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L108:
	movq	8(%rax), %rcx
	movl	%r8d, %edx
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L80
	.cfi_endproc
.LFE11011:
	.size	_ZN2v88internal8compiler23ScheduleLateNodeVisitor14GetBlockForUseENS1_4EdgeE, .-_ZN2v88internal8compiler23ScheduleLateNodeVisitor14GetBlockForUseENS1_4EdgeE
	.section	.text._ZN2v88internal8compiler23ScheduleLateNodeVisitor24GetCommonDominatorOfUsesEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler23ScheduleLateNodeVisitor24GetCommonDominatorOfUsesEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler23ScheduleLateNodeVisitor24GetCommonDominatorOfUsesEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23ScheduleLateNodeVisitor24GetCommonDominatorOfUsesEPNS1_4NodeE, @function
_ZN2v88internal8compiler23ScheduleLateNodeVisitor24GetCommonDominatorOfUsesEPNS1_4NodeE:
.LFB11009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rsi), %rbx
	movq	$0, -72(%rbp)
	testq	%rbx, %rbx
	je	.L111
	movq	(%rbx), %rax
	movq	%rdi, %r15
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L212:
	movl	16(%rbx), %edx
	movl	%edx, %r8d
	shrl	%r8d
	movl	%r8d, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%rbx,%rcx,8), %r12
	movq	8(%r15), %rcx
	movq	%r12, %rdi
	movq	200(%rcx), %r9
	je	.L113
	movl	20(%r12), %esi
	leaq	32(%r12,%rax), %r13
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%r9,%rdx), %edx
	testl	%edx, %edx
	je	.L114
.L115:
	movq	(%r12), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r9d
	cmpl	$1, %r9d
	jbe	.L364
	cmpl	$10, %eax
	sete	%r9b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r9b
	je	.L205
	cmpl	$2, %edx
	je	.L365
.L205:
	movq	16(%r15), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L114
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L366
.L207:
	cmpq	$0, -72(%rbp)
	jne	.L218
.L360:
	movq	-64(%rbp), %rax
	movq	%rax, -72(%rbp)
.L114:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L111
.L370:
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L113:
	movq	(%r12), %r12
	movl	20(%r12), %esi
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%r9,%rdx), %edx
	testl	%edx, %edx
	je	.L114
	leaq	16(%rdi,%rax), %r13
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L364:
	cmpl	$3, %edx
	je	.L367
	cmpl	$2, %edx
	jne	.L205
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L368
.L206:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	movq	8(%r15), %rax
	movq	224(%rax), %rbx
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L369:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L208:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L369
	movq	%rax, -64(%rbp)
.L381:
	cmpq	$0, -72(%rbp)
	je	.L360
.L218:
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %rdi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	jne	.L370
.L111:
	movq	-72(%rbp), %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	movq	160(%rax), %rax
	movl	20(%r12), %esi
	leaq	.LC6(%rip), %rdi
	movq	%rax, -80(%rbp)
	movq	(%r12), %rax
	andl	$16777215, %esi
	movl	-80(%rbp), %ecx
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L367:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L371
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L114
.L393:
	movq	(%rbx), %rax
	movq	$0, -64(%rbp)
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L204:
	movl	16(%rbx), %edx
	movl	%edx, %r8d
	shrl	%r8d
	movl	%r8d, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%rbx,%rcx,8), %r12
	movq	8(%r15), %rcx
	movq	%r12, %rdi
	movq	200(%rcx), %r9
	je	.L120
	movl	20(%r12), %esi
	leaq	32(%r12,%rax), %r13
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%r9,%rdx), %edx
	testl	%edx, %edx
	je	.L121
.L122:
	movq	(%r12), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r9d
	cmpl	$1, %r9d
	jbe	.L372
	cmpl	$10, %eax
	sete	%r9b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r9b
	je	.L196
	cmpl	$2, %edx
	je	.L373
.L196:
	movq	16(%r15), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L121
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L374
.L198:
	cmpq	$0, -64(%rbp)
	jne	.L217
.L359:
	movq	%r14, -64(%rbp)
.L121:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L203
.L378:
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L120:
	movq	(%r12), %r12
	movl	20(%r12), %esi
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%r9,%rdx), %edx
	testl	%edx, %edx
	je	.L121
	leaq	16(%rdi,%rax), %r13
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L372:
	cmpl	$3, %edx
	je	.L375
	cmpl	$2, %edx
	jne	.L196
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L376
.L197:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	movq	8(%r15), %rax
	movq	224(%rax), %rbx
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L377:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L199:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L377
	movq	%rax, %r14
.L392:
	cmpq	$0, -64(%rbp)
	je	.L359
.L217:
	movq	-64(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, -64(%rbp)
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	jne	.L378
	.p2align 4,,10
	.p2align 3
.L203:
	cmpq	$0, -72(%rbp)
	je	.L360
	cmpq	$0, -64(%rbp)
	je	.L114
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L374:
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r14), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L365:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L379
.L209:
	movq	0(%r13), %r12
	movq	224(%rcx), %rbx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L380:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L210:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L380
	movq	%rax, -64(%rbp)
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L375:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L382
.L125:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L121
	movq	(%rbx), %rax
	xorl	%r14d, %r14d
	movq	%r14, -104(%rbp)
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L195:
	movl	16(%rbx), %edx
	movl	%edx, %r8d
	shrl	%r8d
	movl	%r8d, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%rbx,%rcx,8), %r12
	movq	8(%r15), %rcx
	movq	%r12, %r9
	movq	200(%rcx), %rdi
	je	.L127
	movl	20(%r12), %esi
	leaq	32(%r12,%rax), %r13
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%rdi,%rdx), %edx
	testl	%edx, %edx
	je	.L128
.L129:
	movq	(%r12), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r9d
	cmpl	$1, %r9d
	jbe	.L383
	cmpl	$10, %eax
	sete	%r9b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r9b
	je	.L187
	cmpl	$2, %edx
	je	.L384
.L187:
	movq	16(%r15), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L128
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L385
.L189:
	cmpq	$0, -104(%rbp)
	jne	.L216
.L358:
	movq	-96(%rbp), %rax
	movq	%rax, -104(%rbp)
.L128:
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L194
.L389:
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L127:
	movq	(%r12), %r12
	movl	20(%r12), %esi
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%rdi,%rdx), %edx
	testl	%edx, %edx
	je	.L128
	leaq	16(%r9,%rax), %r13
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L383:
	cmpl	$3, %edx
	je	.L386
	cmpl	$2, %edx
	jne	.L187
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L387
.L188:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	movq	8(%r15), %rax
	movq	224(%rax), %rbx
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L388:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L190:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L388
	movq	%rax, -96(%rbp)
.L404:
	cmpq	$0, -104(%rbp)
	je	.L358
.L216:
	movq	-96(%rbp), %rsi
	movq	-104(%rbp), %rdi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, -104(%rbp)
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	jne	.L389
	.p2align 4,,10
	.p2align 3
.L194:
	cmpq	$0, -64(%rbp)
	movq	-104(%rbp), %r14
	je	.L359
	testq	%r14, %r14
	je	.L121
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L385:
	movq	160(%rax), %rax
	movl	20(%r12), %esi
	leaq	.LC6(%rip), %rdi
	movq	%rax, -112(%rbp)
	movq	(%r12), %rax
	andl	$16777215, %esi
	movl	-112(%rbp), %ecx
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L373:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L390
.L200:
	movq	0(%r13), %r12
	movq	224(%rcx), %rbx
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L391:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L201:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L391
	movq	%rax, %r14
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L371:
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	jne	.L393
	jmp	.L114
.L390:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC5(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r15), %rcx
	jmp	.L200
.L376:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC4(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L386:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L394
.L132:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L128
	movq	(%rbx), %rax
	movq	$0, -96(%rbp)
	movq	%rax, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L186:
	movl	16(%rbx), %edx
	movl	%edx, %r8d
	shrl	%r8d
	movl	%r8d, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%rbx,%rcx,8), %r13
	movq	8(%r15), %rcx
	movq	%r13, %r9
	movq	200(%rcx), %rdi
	je	.L134
	movl	20(%r13), %esi
	leaq	32(%r13,%rax), %r12
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%rdi,%rdx), %edx
	testl	%edx, %edx
	je	.L135
.L136:
	movq	0(%r13), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r9d
	cmpl	$1, %r9d
	jbe	.L395
	cmpl	$10, %eax
	sete	%r9b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r9b
	je	.L178
	cmpl	$2, %edx
	je	.L396
.L178:
	movq	16(%r15), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L135
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L397
.L180:
	cmpq	$0, -96(%rbp)
	jne	.L215
.L357:
	movq	%r12, -96(%rbp)
.L135:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L185
.L401:
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%rax, -112(%rbp)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L134:
	movq	0(%r13), %r13
	movl	20(%r13), %esi
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%rdi,%rdx), %edx
	testl	%edx, %edx
	je	.L135
	leaq	16(%r9,%rax), %r12
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L395:
	cmpl	$3, %edx
	je	.L398
	cmpl	$2, %edx
	jne	.L178
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L399
.L179:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	movq	8(%r15), %rax
	movq	224(%rax), %rbx
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L400:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L181:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L400
.L347:
	cmpq	$0, -96(%rbp)
	movq	%rax, %r12
	je	.L357
.L215:
	movq	-96(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, -96(%rbp)
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	jne	.L401
	.p2align 4,,10
	.p2align 3
.L185:
	cmpq	$0, -104(%rbp)
	je	.L358
	cmpq	$0, -96(%rbp)
	je	.L128
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L397:
	movq	0(%r13), %rax
	movl	20(%r13), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r12), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L379:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC5(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r15), %rcx
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L384:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L402
.L191:
	movq	0(%r13), %r12
	movq	224(%rcx), %rbx
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L403:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L192:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L403
	movq	%rax, -96(%rbp)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L368:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC4(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L382:
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L398:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L405
.L139:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L135
	xorl	%r12d, %r12d
	movq	(%rbx), %r13
	movq	%r12, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L177:
	movl	16(%rbx), %edx
	movl	%edx, %r8d
	shrl	%r8d
	movl	%r8d, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%rbx,%rcx,8), %r14
	movq	8(%r15), %rcx
	movq	%r14, %rdi
	movq	200(%rcx), %r9
	je	.L141
	movl	20(%r14), %esi
	leaq	32(%r14,%rax), %r12
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%r9,%rdx), %edx
	testl	%edx, %edx
	je	.L142
.L143:
	movq	(%r14), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r9d
	cmpl	$1, %r9d
	jbe	.L406
	cmpl	$10, %eax
	sete	%r9b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r9b
	je	.L169
	cmpl	$2, %edx
	je	.L407
.L169:
	movq	16(%r15), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L142
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L408
.L171:
	cmpq	$0, -120(%rbp)
	jne	.L214
.L356:
	movq	%r12, -120(%rbp)
.L142:
	testq	%r13, %r13
	je	.L176
.L412:
	movq	%r13, %rbx
	movq	0(%r13), %r13
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L141:
	movq	(%r14), %r14
	movl	20(%r14), %esi
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%r9,%rdx), %edx
	testl	%edx, %edx
	je	.L142
	leaq	16(%rdi,%rax), %r12
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L406:
	cmpl	$3, %edx
	je	.L409
	cmpl	$2, %edx
	jne	.L169
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L410
.L170:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
	movq	8(%r15), %rax
	movq	224(%rax), %rbx
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L411:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
.L172:
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L411
.L345:
	movq	%rax, %r12
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L167:
	cmpq	$0, -120(%rbp)
	movq	-128(%rbp), %r13
	je	.L356
	testq	%r12, %r12
	je	.L142
	.p2align 4,,10
	.p2align 3
.L214:
	movq	-120(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, -120(%rbp)
	testq	%r13, %r13
	jne	.L412
	.p2align 4,,10
	.p2align 3
.L176:
	cmpq	$0, -96(%rbp)
	movq	-120(%rbp), %r12
	je	.L357
	testq	%r12, %r12
	je	.L135
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L408:
	movq	(%r14), %rax
	movl	20(%r14), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r12), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L171
.L396:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L413
.L182:
	movq	(%r12), %r13
	movq	224(%rcx), %rbx
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L414:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L183:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L414
	jmp	.L347
.L394:
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L132
.L409:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L415
.L146:
	movq	24(%r14), %r9
	testq	%r9, %r9
	je	.L142
	movq	%r13, -128(%rbp)
	movq	(%r9), %rbx
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L168:
	movl	16(%r9), %edx
	movl	%edx, %esi
	shrl	%esi
	movl	%esi, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%r9,%rcx,8), %r14
	movq	8(%r15), %rcx
	movq	%r14, %rdi
	movq	200(%rcx), %r10
	je	.L148
	movl	20(%r14), %r8d
	leaq	32(%r14,%rax), %r13
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	movl	12(%r10,%rdx), %edx
	testl	%edx, %edx
	je	.L149
.L150:
	movq	(%r14), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r11d
	cmpl	$1, %r11d
	jbe	.L416
	cmpl	$10, %eax
	sete	%r9b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r9b
	je	.L160
	cmpl	$2, %edx
	je	.L417
.L160:
	movq	16(%r15), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L149
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L418
.L162:
	testq	%r12, %r12
	jne	.L213
.L355:
	movq	%r13, %r12
.L149:
	testq	%rbx, %rbx
	je	.L167
	movq	%rbx, %r9
	movq	(%rbx), %rbx
	jmp	.L168
.L413:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC5(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r15), %rcx
	jmp	.L182
.L399:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC4(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L179
.L415:
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L148:
	movq	(%r14), %r14
	movl	20(%r14), %r8d
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	movl	12(%r10,%rdx), %edx
	testl	%edx, %edx
	je	.L149
	leaq	16(%rdi,%rax), %r13
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L416:
	cmpl	$3, %edx
	je	.L419
	cmpl	$2, %edx
	jne	.L160
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L420
.L161:
	movq	%r14, %rdi
	xorl	%esi, %esi
	movq	%r9, -136(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-136(%rbp), %r9
	movq	%rax, %rdi
	movl	16(%r9), %esi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r9
	movq	8(%r15), %rax
	movq	%r9, %r13
	movq	224(%rax), %r14
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L421:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L163:
	movq	16(%r14), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L421
.L343:
	movq	%rax, %r13
	jmp	.L162
.L158:
	testq	%r12, %r12
	je	.L355
	testq	%r13, %r13
	je	.L149
.L213:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, %r12
	jmp	.L149
.L418:
	movq	(%r14), %rax
	movl	20(%r14), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r13), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L162
.L402:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC5(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r15), %rcx
	jmp	.L191
.L407:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L422
.L173:
	movq	(%r12), %r14
	movq	224(%rcx), %rbx
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L423:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
.L174:
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L423
	jmp	.L345
.L387:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC4(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L188
.L405:
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L139
.L419:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L424
.L153:
	movq	24(%r14), %rsi
	testq	%rsi, %rsi
	je	.L149
	movq	(%rsi), %r14
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L159:
	movl	16(%rsi), %ecx
	movq	8(%r15), %rdi
	movl	%ecx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rdx
	salq	$3, %rax
	andl	$1, %ecx
	leaq	(%rsi,%rdx,8), %rdx
	leaq	32(%rdx,%rax), %r8
	jne	.L156
	leaq	16(%rdx,%rax), %r8
	movq	(%rdx), %rdx
.L156:
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	200(%rdi), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	je	.L157
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler23ScheduleLateNodeVisitor14GetBlockForUseENS1_4EdgeE
	testq	%r13, %r13
	je	.L226
	testq	%rax, %rax
	je	.L157
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L157:
	testq	%r14, %r14
	je	.L158
	movq	%r14, %rsi
	movq	(%r14), %r14
	jmp	.L159
.L226:
	movq	%rax, %r13
	jmp	.L157
.L417:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L425
.L164:
	movq	0(%r13), %r9
	movq	224(%rcx), %r14
	movq	%r9, %r13
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L426:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L165:
	movq	16(%r14), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L426
	jmp	.L343
.L422:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC5(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r15), %rcx
	jmp	.L173
.L410:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC4(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L170
.L424:
	movq	8(%rdi), %rdx
	movl	%r8d, %esi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L153
.L420:
	movq	8(%rdi), %rcx
	movl	%r8d, %edx
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%r9, -136(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-136(%rbp), %r9
	jmp	.L161
.L425:
	movq	8(%rdi), %rcx
	movl	%r8d, %edx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r15), %rcx
	jmp	.L164
	.cfi_endproc
.LFE11009:
	.size	_ZN2v88internal8compiler23ScheduleLateNodeVisitor24GetCommonDominatorOfUsesEPNS1_4NodeE, .-_ZN2v88internal8compiler23ScheduleLateNodeVisitor24GetCommonDominatorOfUsesEPNS1_4NodeE
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC7:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	.type	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm, @function
_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm:
.LFB12414:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L455
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$268435455, %esi
	subq	$24, %rsp
	movq	16(%rdi), %rcx
	movq	24(%r13), %rax
	movq	%rcx, %rdx
	subq	8(%rdi), %rdx
	subq	%rcx, %rax
	movq	%rsi, %rdi
	movq	%rdx, %r14
	sarq	$3, %rax
	sarq	$3, %r14
	subq	%r14, %rdi
	cmpq	%rbx, %rax
	jb	.L429
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 16(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L458
	cmpq	%r14, %rbx
	movq	%r14, %r15
	movq	0(%r13), %rdi
	cmovnb	%rbx, %r15
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	addq	%r14, %r15
	cmpq	$268435455, %r15
	cmova	%rsi, %r15
	subq	%r12, %rax
	salq	$3, %r15
	movq	%r15, %rsi
	cmpq	%r15, %rax
	jb	.L459
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L433:
	leaq	0(,%rbx,8), %r8
	leaq	(%r12,%rdx), %rdi
	xorl	%esi, %esi
	movq	%r8, %rdx
	call	memset@PLT
	movq	16(%r13), %rax
	movq	8(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L438
	subq	$8, %rax
	leaq	15(%rdx), %rcx
	subq	%rdx, %rax
	subq	%r12, %rcx
	movq	%rax, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rcx
	jbe	.L435
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rsi
	je	.L435
	addq	$1, %rsi
	movq	%rdx, %rdi
	movq	%r12, %rax
	movq	%rsi, %rcx
	subq	%r12, %rdi
	shrq	%rcx
	salq	$4, %rcx
	addq	%r12, %rcx
	.p2align 4,,10
	.p2align 3
.L436:
	movdqu	(%rax,%rdi), %xmm0
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rcx
	jne	.L436
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	addq	%rcx, %rdx
	addq	%r12, %rcx
	cmpq	%rax, %rsi
	je	.L438
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
.L438:
	addq	%r14, %rbx
	movq	%r12, 8(%r13)
	leaq	(%r12,%rbx,8), %rax
	addq	%r15, %r12
	movq	%rax, 16(%r13)
	movq	%r12, 24(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L435:
	.cfi_restore_state
	leaq	8(%rax,%r12), %rsi
	subq	%r12, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L440:
	movq	(%rax,%rdx), %rcx
	addq	$8, %rax
	movq	%rcx, -8(%rax)
	cmpq	%rax, %rsi
	jne	.L440
	jmp	.L438
.L459:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L433
.L458:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12414:
	.size	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm, .-_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	.section	.rodata._ZNSt6vectorIN2v88internal8compiler9Scheduler13SchedulerDataENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_.str1.1,"aMS",@progbits,1
.LC8:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIN2v88internal8compiler9Scheduler13SchedulerDataENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler9Scheduler13SchedulerDataENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler9Scheduler13SchedulerDataENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	.type	_ZNSt6vectorIN2v88internal8compiler9Scheduler13SchedulerDataENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_, @function
_ZNSt6vectorIN2v88internal8compiler9Scheduler13SchedulerDataENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_:
.LFB12490:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L501
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rdi
	movq	24(%r12), %rax
	subq	%rdi, %rax
	sarq	$4, %rax
	cmpq	%rdx, %rax
	jb	.L463
	movq	%rdi, %r8
	movq	(%rcx), %r15
	movl	8(%rcx), %r14d
	subq	%rsi, %r8
	movl	12(%rcx), %ecx
	movq	%r8, %rax
	sarq	$4, %rax
	cmpq	%rax, %rdx
	jnb	.L464
	salq	$4, %rdx
	movq	%rdx, %r8
	movq	%rdi, %rdx
	subq	%r8, %rdx
	cmpq	%rdx, %rdi
	je	.L488
	movq	%rdx, %rax
	movq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L466:
	movq	(%rax), %r11
	movl	8(%rax), %r10d
	addq	$16, %rax
	addq	$16, %rsi
	movl	-4(%rax), %r9d
	movq	%r11, -16(%rsi)
	movl	%r10d, -8(%rsi)
	movl	%r9d, -4(%rsi)
	cmpq	%rax, %rdi
	jne	.L466
	movq	16(%r12), %rax
.L465:
	addq	%r8, %rax
	movq	%rax, 16(%r12)
	cmpq	%rdx, %r13
	je	.L467
	subq	%r13, %rdx
	movq	%r13, %rsi
	movl	%ecx, -64(%rbp)
	subq	%rdx, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movl	-64(%rbp), %ecx
	movq	-56(%rbp), %r8
.L467:
	leaq	0(%r13,%r8), %rdx
	cmpq	%rdx, %r13
	je	.L460
	.p2align 4,,10
	.p2align 3
.L469:
	movq	%r15, (%rbx)
	addq	$16, %rbx
	movl	%r14d, -8(%rbx)
	movl	%ecx, -4(%rbx)
	cmpq	%rbx, %rdx
	jne	.L469
.L460:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	subq	%rax, %rdx
	movq	%rdi, %rax
	je	.L470
	movq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L471:
	movq	%r15, (%rax)
	addq	$16, %rax
	movl	%r14d, -8(%rax)
	movl	%ecx, -4(%rax)
	subq	$1, %rsi
	jne	.L471
	salq	$4, %rdx
	leaq	(%rdi,%rdx), %rax
.L470:
	movq	%rax, 16(%r12)
	cmpq	%r13, %rdi
	je	.L472
	movq	%r13, %rsi
	.p2align 4,,10
	.p2align 3
.L473:
	movq	(%rsi), %r10
	movl	8(%rsi), %r9d
	addq	$16, %rsi
	addq	$16, %rax
	movl	-4(%rsi), %edx
	movq	%r10, -16(%rax)
	movl	%r9d, -8(%rax)
	movl	%edx, -4(%rax)
	cmpq	%rsi, %rdi
	jne	.L473
	addq	%r8, 16(%r12)
	.p2align 4,,10
	.p2align 3
.L475:
	movq	%r15, (%rbx)
	addq	$16, %rbx
	movl	%r14d, -8(%rbx)
	movl	%ecx, -4(%rbx)
	cmpq	%rbx, %rdi
	jne	.L475
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	movq	8(%r12), %rsi
	movl	$134217727, %r14d
	movq	%r14, %rax
	subq	%rsi, %rdi
	sarq	$4, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L504
	cmpq	%rdi, %rdx
	movq	%rdi, %rax
	cmovnb	%rdx, %rax
	addq	%rax, %rdi
	setc	%al
	subq	%rsi, %rbx
	movzbl	%al, %eax
	testq	%rax, %rax
	jne	.L490
	testq	%rdi, %rdi
	jne	.L505
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L480:
	leaq	(%rax,%rbx), %rsi
	movq	%rdx, %rdi
	.p2align 4,,10
	.p2align 3
.L483:
	movq	(%rcx), %r11
	movl	8(%rcx), %r10d
	addq	$16, %rsi
	movl	12(%rcx), %r9d
	movq	%r11, -16(%rsi)
	movl	%r10d, -8(%rsi)
	movl	%r9d, -4(%rsi)
	subq	$1, %rdi
	jne	.L483
	movq	8(%r12), %r11
	cmpq	%r11, %r13
	je	.L492
	movq	%r11, %rcx
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L485:
	movq	(%rcx), %r10
	movl	8(%rcx), %r9d
	addq	$16, %rcx
	addq	$16, %rsi
	movl	-4(%rcx), %edi
	movq	%r10, -16(%rsi)
	movl	%r9d, -8(%rsi)
	movl	%edi, -4(%rsi)
	cmpq	%rcx, %r13
	jne	.L485
	movq	%r13, %rbx
	subq	%r11, %rbx
	movq	%rbx, %r11
	addq	%rax, %r11
.L484:
	movq	16(%r12), %r10
	salq	$4, %rdx
	addq	%rdx, %r11
	cmpq	%r10, %r13
	je	.L486
	movq	%r13, %rdx
	movq	%r11, %rcx
	.p2align 4,,10
	.p2align 3
.L487:
	movq	(%rdx), %r9
	movl	8(%rdx), %edi
	addq	$16, %rdx
	addq	$16, %rcx
	movl	-4(%rdx), %esi
	movq	%r9, -16(%rcx)
	movl	%edi, -8(%rcx)
	movl	%esi, -4(%rcx)
	cmpq	%rdx, %r10
	jne	.L487
	subq	%r13, %r10
	addq	%r10, %r11
.L486:
	movq	%rax, %xmm0
	movq	%r11, %xmm1
	movq	%r8, 24(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	movl	$2147483632, %esi
	movl	$2147483632, %r14d
.L479:
	movq	(%r12), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L506
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L482:
	leaq	(%rax,%r14), %r8
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L488:
	movq	%rdi, %rax
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L472:
	addq	%r8, %rax
	movq	%rax, 16(%r12)
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L492:
	movq	%rax, %r11
	jmp	.L484
.L506:
	movq	%r8, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	jmp	.L482
.L505:
	cmpq	$134217727, %rdi
	cmova	%r14, %rdi
	salq	$4, %rdi
	movq	%rdi, %r14
	movq	%rdi, %rsi
	jmp	.L479
.L504:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12490:
	.size	_ZNSt6vectorIN2v88internal8compiler9Scheduler13SchedulerDataENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_, .-_ZNSt6vectorIN2v88internal8compiler9Scheduler13SchedulerDataENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC9:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB12499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L545
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L523
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L546
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L509:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L547
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L512:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L546:
	testq	%rdx, %rdx
	jne	.L548
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L510:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L513
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L526
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L526
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L515:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L515
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L517
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L517:
	leaq	16(%rax,%r8), %rcx
.L513:
	cmpq	%r14, %r12
	je	.L518
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L527
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L527
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L520:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L520
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L522
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L522:
	leaq	8(%rcx,%r9), %rcx
.L518:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L527:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L519:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L519
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L526:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L514:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L514
	jmp	.L517
.L547:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L512
.L545:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L548:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L509
	.cfi_endproc
.LFE12499:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZNSt6vectorIPN2v88internal10ZoneVectorIPNS1_8compiler4NodeEEENS1_13ZoneAllocatorIS7_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPN2v88internal10ZoneVectorIPNS1_8compiler4NodeEEENS1_13ZoneAllocatorIS7_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal10ZoneVectorIPNS1_8compiler4NodeEEENS1_13ZoneAllocatorIS7_EEE17_M_default_appendEm
	.type	_ZNSt6vectorIPN2v88internal10ZoneVectorIPNS1_8compiler4NodeEEENS1_13ZoneAllocatorIS7_EEE17_M_default_appendEm, @function
_ZNSt6vectorIPN2v88internal10ZoneVectorIPNS1_8compiler4NodeEEENS1_13ZoneAllocatorIS7_EEE17_M_default_appendEm:
.LFB12511:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L577
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$268435455, %esi
	subq	$24, %rsp
	movq	16(%rdi), %rcx
	movq	24(%r13), %rax
	movq	%rcx, %rdx
	subq	8(%rdi), %rdx
	subq	%rcx, %rax
	movq	%rsi, %rdi
	movq	%rdx, %r14
	sarq	$3, %rax
	sarq	$3, %r14
	subq	%r14, %rdi
	cmpq	%rbx, %rax
	jb	.L551
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 16(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L577:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L551:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L580
	cmpq	%r14, %rbx
	movq	%r14, %r15
	movq	0(%r13), %rdi
	cmovnb	%rbx, %r15
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	addq	%r14, %r15
	cmpq	$268435455, %r15
	cmova	%rsi, %r15
	subq	%r12, %rax
	salq	$3, %r15
	movq	%r15, %rsi
	cmpq	%r15, %rax
	jb	.L581
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L555:
	leaq	0(,%rbx,8), %r8
	leaq	(%r12,%rdx), %rdi
	xorl	%esi, %esi
	movq	%r8, %rdx
	call	memset@PLT
	movq	16(%r13), %rax
	movq	8(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L560
	subq	$8, %rax
	leaq	15(%rdx), %rcx
	subq	%rdx, %rax
	subq	%r12, %rcx
	movq	%rax, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rcx
	jbe	.L557
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rsi
	je	.L557
	addq	$1, %rsi
	movq	%rdx, %rdi
	movq	%r12, %rax
	movq	%rsi, %rcx
	subq	%r12, %rdi
	shrq	%rcx
	salq	$4, %rcx
	addq	%r12, %rcx
	.p2align 4,,10
	.p2align 3
.L558:
	movdqu	(%rax,%rdi), %xmm0
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rcx
	jne	.L558
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	addq	%rcx, %rdx
	addq	%r12, %rcx
	cmpq	%rax, %rsi
	je	.L560
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
.L560:
	addq	%r14, %rbx
	movq	%r12, 8(%r13)
	leaq	(%r12,%rbx,8), %rax
	addq	%r15, %r12
	movq	%rax, 16(%r13)
	movq	%r12, 24(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L557:
	.cfi_restore_state
	leaq	8(%rax,%r12), %rsi
	subq	%r12, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L562:
	movq	(%rax,%rdx), %rcx
	addq	$8, %rax
	movq	%rcx, -8(%rax)
	cmpq	%rax, %rsi
	jne	.L562
	jmp	.L560
.L581:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L555
.L580:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12511:
	.size	_ZNSt6vectorIPN2v88internal10ZoneVectorIPNS1_8compiler4NodeEEENS1_13ZoneAllocatorIS7_EEE17_M_default_appendEm, .-_ZNSt6vectorIPN2v88internal10ZoneVectorIPNS1_8compiler4NodeEEENS1_13ZoneAllocatorIS7_EEE17_M_default_appendEm
	.section	.text._ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB12563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L620
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L598
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L621
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L584:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L622
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L587:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L621:
	testq	%rdx, %rdx
	jne	.L623
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L585:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L588
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L601
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L601
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L590:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L590
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L592
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L592:
	leaq	16(%rax,%r8), %rcx
.L588:
	cmpq	%r14, %r12
	je	.L593
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L602
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L602
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L595:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L595
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L597
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L597:
	leaq	8(%rcx,%r9), %rcx
.L593:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L598:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L602:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L594:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L594
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L601:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L589:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L589
	jmp	.L592
.L622:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L587
.L620:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L623:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L584
	.cfi_endproc
.LFE12563:
	.size	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.rodata._ZN2v88internal8compiler9Scheduler17SealFinalScheduleEv.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"--- SEAL FINAL SCHEDULE ------------------------------------\n"
	.section	.text._ZN2v88internal8compiler9Scheduler17SealFinalScheduleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler17SealFinalScheduleEv
	.type	_ZN2v88internal8compiler9Scheduler17SealFinalScheduleEv, @function
_ZN2v88internal8compiler9Scheduler17SealFinalScheduleEv:
.LFB11020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L652
.L625:
	movq	232(%r12), %rbx
	movq	16(%rbx), %rdi
	movq	%rdi, -64(%rbp)
	testq	%rdi, %rdi
	je	.L641
	xorl	%esi, %esi
	leaq	-64(%rbp), %r14
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L653:
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 88(%rdi)
.L651:
	movq	-64(%rbp), %rax
	movq	24(%rax), %rdi
	movq	%rdi, -64(%rbp)
	testq	%rdi, %rdi
	je	.L626
	movl	%r13d, %esi
.L630:
	leal	1(%rsi), %r13d
	call	_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi@PLT
	movq	8(%rbx), %rdi
	movq	88(%rdi), %rsi
	cmpq	96(%rdi), %rsi
	jne	.L653
	addq	$72, %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L651
.L641:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L626:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L654
.L631:
	movl	%r13d, %esi
	call	_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi@PLT
	movq	40(%r12), %rcx
	movq	48(%r12), %rax
	movq	%rcx, -72(%rbp)
	cmpq	%rax, %rcx
	je	.L624
	subq	$8, %rax
	xorl	%r13d, %r13d
	subq	%rcx, %rax
	shrq	$3, %rax
	movq	%rax, -80(%rbp)
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L638:
	leaq	1(%r13), %rax
	cmpq	%r13, -80(%rbp)
	je	.L624
.L642:
	movq	%rax, %r13
.L636:
	movq	-72(%rbp), %rax
	movq	16(%r12), %rdi
	movq	%r13, %rsi
	movq	(%rax,%r13,8), %r14
	call	_ZN2v88internal8compiler8Schedule12GetBlockByIdENS1_10BasicBlock2IdE@PLT
	movq	%rax, %rbx
	testq	%r14, %r14
	je	.L638
	movq	16(%r14), %r15
	movq	8(%r14), %r14
	cmpq	%r15, %r14
	je	.L638
	.p2align 4,,10
	.p2align 3
.L639:
	movq	-8(%r15), %rdx
	movq	16(%r12), %rdi
	movq	%rbx, %rsi
	subq	$8, %r15
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	cmpq	%r15, %r14
	jne	.L639
	leaq	1(%r13), %rax
	cmpq	%r13, -80(%rbp)
	jne	.L642
.L624:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L655
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L652:
	.cfi_restore_state
	leaq	.LC10(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L625
.L654:
	movq	8(%rbx), %rax
	movq	(%rax), %r14
	movq	16(%r14), %rdi
	movq	24(%r14), %rax
	subq	%rdi, %rax
	cmpq	$167, %rax
	jbe	.L656
	leaq	168(%rdi), %rax
	movq	%rax, 16(%r14)
.L633:
	movq	$-1, %rdx
	movq	%r14, %rsi
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal8compiler10BasicBlockC1EPNS0_4ZoneENS2_2IdE@PLT
	movq	-72(%rbp), %rdi
	movq	%rdi, 24(%rbx)
	jmp	.L631
.L656:
	movq	%r14, %rdi
	movl	$168, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L633
.L655:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11020:
	.size	_ZN2v88internal8compiler9Scheduler17SealFinalScheduleEv, .-_ZN2v88internal8compiler9Scheduler17SealFinalScheduleEv
	.section	.text._ZNSt6vectorIN2v88internal8compiler18SpecialRPONumberer20SpecialRPOStackFrameENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler18SpecialRPONumberer20SpecialRPOStackFrameENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler18SpecialRPONumberer20SpecialRPOStackFrameENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	.type	_ZNSt6vectorIN2v88internal8compiler18SpecialRPONumberer20SpecialRPOStackFrameENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm, @function
_ZNSt6vectorIN2v88internal8compiler18SpecialRPONumberer20SpecialRPOStackFrameENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm:
.LFB12570:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L678
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$134217727, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	movq	%rcx, %rbx
	subq	8(%rdi), %rbx
	subq	%rcx, %rax
	movq	%rbx, %r14
	sarq	$4, %rax
	sarq	$4, %r14
	subq	%r14, %rsi
	cmpq	%r12, %rax
	jb	.L659
	movq	%rcx, %rax
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L660:
	movq	$0, (%rax)
	addq	$16, %rax
	movq	$0, -8(%rax)
	subq	$1, %rdx
	jne	.L660
	salq	$4, %r12
	addq	%r12, %rcx
	movq	%rcx, 16(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L678:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L659:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%r12, %rsi
	jb	.L681
	cmpq	%r14, %r12
	movq	%r14, %r15
	movq	(%rdi), %rdi
	cmovnb	%r12, %r15
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	addq	%r14, %r15
	cmpq	$134217727, %r15
	cmova	%rdx, %r15
	subq	%r8, %rax
	salq	$4, %r15
	movq	%r15, %rsi
	cmpq	%r15, %rax
	jb	.L682
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L664:
	leaq	(%r8,%rbx), %rax
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L665:
	movq	$0, (%rax)
	addq	$16, %rax
	movq	$0, -8(%rax)
	subq	$1, %rdx
	jne	.L665
	movq	8(%r13), %rax
	movq	16(%r13), %rdi
	movq	%r8, %r9
	subq	%rax, %r9
	cmpq	%rax, %rdi
	je	.L667
	.p2align 4,,10
	.p2align 3
.L666:
	movq	(%rax), %rsi
	movq	8(%rax), %rcx
	leaq	(%rax,%r9), %rdx
	addq	$16, %rax
	movq	%rsi, (%rdx)
	movq	%rcx, 8(%rdx)
	cmpq	%rax, %rdi
	jne	.L666
.L667:
	addq	%r14, %r12
	addq	%r8, %r15
	movq	%r8, 8(%r13)
	salq	$4, %r12
	movq	%r15, 24(%r13)
	leaq	(%r8,%r12), %rax
	movq	%rax, 16(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L682:
	.cfi_restore_state
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r8
	jmp	.L664
.L681:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12570:
	.size	_ZNSt6vectorIN2v88internal8compiler18SpecialRPONumberer20SpecialRPOStackFrameENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm, .-_ZNSt6vectorIN2v88internal8compiler18SpecialRPONumberer20SpecialRPOStackFrameENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	.section	.text._ZNSt6vectorIN2v88internal8compiler18SpecialRPONumberer8LoopInfoENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler18SpecialRPONumberer8LoopInfoENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler18SpecialRPONumberer8LoopInfoENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	.type	_ZNSt6vectorIN2v88internal8compiler18SpecialRPONumberer8LoopInfoENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_, @function
_ZNSt6vectorIN2v88internal8compiler18SpecialRPONumberer8LoopInfoENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_:
.LFB12581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L683
	movq	%rdi, %r13
	movq	16(%rdi), %rdi
	movq	%rsi, %r12
	movq	%rsi, %rbx
	movabsq	$-6148914691236517205, %rsi
	movq	24(%r13), %rax
	subq	%rdi, %rax
	sarq	$4, %rax
	imulq	%rsi, %rax
	cmpq	%rdx, %rax
	jb	.L685
	movq	%rdi, %r8
	movdqu	(%rcx), %xmm2
	movdqu	16(%rcx), %xmm1
	subq	%r12, %r8
	movdqu	32(%rcx), %xmm0
	movq	%r8, %rax
	movups	%xmm2, -88(%rbp)
	sarq	$4, %rax
	movups	%xmm1, -72(%rbp)
	imulq	%rsi, %rax
	movups	%xmm0, -56(%rbp)
	cmpq	%rax, %rdx
	jnb	.L686
	leaq	(%rdx,%rdx,2), %rax
	movq	%rdi, %rdx
	salq	$4, %rax
	subq	%rax, %rdx
	movq	%rax, %r14
	cmpq	%rdx, %rdi
	je	.L712
	movq	%rdx, %rax
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L688:
	movdqu	(%rax), %xmm3
	addq	$48, %rax
	addq	$48, %rcx
	movups	%xmm3, -48(%rcx)
	movdqu	-32(%rax), %xmm4
	movups	%xmm4, -32(%rcx)
	movdqu	-16(%rax), %xmm5
	movups	%xmm5, -16(%rcx)
	cmpq	%rax, %rdi
	jne	.L688
	movq	16(%r13), %rax
.L687:
	addq	%r14, %rax
	movq	%rax, 16(%r13)
	cmpq	%rdx, %r12
	je	.L689
	subq	%r12, %rdx
	movq	%r12, %rsi
	subq	%rdx, %rdi
	call	memmove@PLT
.L689:
	leaq	(%r12,%r14), %rax
	cmpq	%rax, %r12
	je	.L683
	movdqu	-88(%rbp), %xmm2
	movdqu	-72(%rbp), %xmm1
	movdqu	-56(%rbp), %xmm0
	.p2align 4,,10
	.p2align 3
.L690:
	movups	%xmm2, (%rbx)
	addq	$48, %rbx
	movups	%xmm1, -32(%rbx)
	movups	%xmm0, -16(%rbx)
	cmpq	%rbx, %rax
	jne	.L690
	.p2align 4,,10
	.p2align 3
.L683:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L729
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L686:
	.cfi_restore_state
	subq	%rax, %rdx
	movq	%rdi, %rax
	je	.L693
	movq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L694:
	movups	%xmm2, (%rax)
	addq	$48, %rax
	movups	%xmm1, -32(%rax)
	movups	%xmm0, -16(%rax)
	subq	$1, %rcx
	jne	.L694
	leaq	(%rdx,%rdx,2), %rax
	salq	$4, %rax
	addq	%rdi, %rax
.L693:
	movq	%rax, 16(%r13)
	cmpq	%r12, %rdi
	je	.L695
	movq	%r12, %rsi
	.p2align 4,,10
	.p2align 3
.L696:
	movdqu	(%rsi), %xmm6
	addq	$48, %rsi
	addq	$48, %rax
	movups	%xmm6, -48(%rax)
	movdqu	-32(%rsi), %xmm7
	movups	%xmm7, -32(%rax)
	movdqu	-16(%rsi), %xmm3
	movups	%xmm3, -16(%rax)
	cmpq	%rsi, %rdi
	jne	.L696
	addq	%r8, 16(%r13)
	movdqu	-88(%rbp), %xmm2
	movdqu	-72(%rbp), %xmm1
	movdqu	-56(%rbp), %xmm0
	.p2align 4,,10
	.p2align 3
.L698:
	movups	%xmm2, (%rbx)
	addq	$48, %rbx
	movups	%xmm1, -32(%rbx)
	movups	%xmm0, -16(%rbx)
	cmpq	%rbx, %rdi
	jne	.L698
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L685:
	movq	8(%r13), %rax
	movl	$44739242, %r8d
	subq	%rax, %rdi
	sarq	$4, %rdi
	imulq	%rsi, %rdi
	movq	%r8, %rsi
	subq	%rdi, %rsi
	cmpq	%rsi, %rdx
	ja	.L730
	cmpq	%rdi, %rdx
	movq	%rdi, %rsi
	cmovnb	%rdx, %rsi
	addq	%rsi, %rdi
	setc	%sil
	subq	%rax, %rbx
	movzbl	%sil, %esi
	testq	%rsi, %rsi
	jne	.L714
	testq	%rdi, %rdi
	jne	.L731
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L703:
	leaq	(%rax,%rbx), %rsi
	movq	%rdx, %rdi
	.p2align 4,,10
	.p2align 3
.L706:
	movdqu	(%rcx), %xmm4
	addq	$48, %rsi
	movups	%xmm4, -48(%rsi)
	movdqu	16(%rcx), %xmm5
	movups	%xmm5, -32(%rsi)
	movdqu	32(%rcx), %xmm6
	movups	%xmm6, -16(%rsi)
	subq	$1, %rdi
	jne	.L706
	movq	8(%r13), %rdi
	cmpq	%rdi, %r12
	je	.L716
	movq	%rdi, %rcx
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L708:
	movdqu	(%rcx), %xmm7
	addq	$48, %rcx
	addq	$48, %rsi
	movups	%xmm7, -48(%rsi)
	movdqu	-32(%rcx), %xmm1
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rcx), %xmm2
	movups	%xmm2, -16(%rsi)
	cmpq	%rcx, %r12
	jne	.L708
	movabsq	$768614336404564651, %rsi
	leaq	-48(%r12), %rcx
	subq	%rdi, %rcx
	shrq	$4, %rcx
	imulq	%rsi, %rcx
	movabsq	$1152921504606846975, %rsi
	andq	%rsi, %rcx
	leaq	3(%rcx,%rcx,2), %rdi
	salq	$4, %rdi
	leaq	(%rax,%rdi), %rcx
.L707:
	leaq	(%rdx,%rdx,2), %rdi
	movq	16(%r13), %rsi
	salq	$4, %rdi
	addq	%rcx, %rdi
	cmpq	%rsi, %r12
	je	.L709
	movq	%r12, %rdx
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L710:
	movdqu	(%rdx), %xmm4
	addq	$48, %rdx
	addq	$48, %rcx
	movups	%xmm4, -48(%rcx)
	movdqu	-32(%rdx), %xmm5
	movups	%xmm5, -32(%rcx)
	movdqu	-16(%rdx), %xmm6
	movups	%xmm6, -16(%rcx)
	cmpq	%rdx, %rsi
	jne	.L710
	movabsq	$768614336404564651, %rcx
	subq	%r12, %rsi
	leaq	-48(%rsi), %rdx
	shrq	$4, %rdx
	imulq	%rcx, %rdx
	movabsq	$1152921504606846975, %rcx
	andq	%rcx, %rdx
	leaq	3(%rdx,%rdx,2), %rdx
	salq	$4, %rdx
	addq	%rdx, %rdi
.L709:
	movq	%rax, %xmm0
	movq	%rdi, %xmm7
	movq	%r8, 24(%r13)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 8(%r13)
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L714:
	movl	$2147483616, %esi
	movl	$2147483616, %r14d
.L702:
	movq	0(%r13), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L732
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L705:
	leaq	(%rax,%r14), %r8
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L712:
	movq	%rdi, %rax
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L695:
	addq	%r8, %rax
	movq	%rax, 16(%r13)
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L716:
	movq	%rax, %rcx
	jmp	.L707
.L732:
	movq	%r8, %rdi
	movq	%rcx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rcx
	jmp	.L705
.L731:
	cmpq	$44739242, %rdi
	cmova	%r8, %rdi
	leaq	(%rdi,%rdi,2), %r8
	movq	%r8, %r14
	salq	$4, %r14
	movq	%r14, %rsi
	jmp	.L702
.L729:
	call	__stack_chk_fail@PLT
.L730:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12581:
	.size	_ZNSt6vectorIN2v88internal8compiler18SpecialRPONumberer8LoopInfoENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_, .-_ZNSt6vectorIN2v88internal8compiler18SpecialRPONumberer8LoopInfoENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	.section	.text._ZN2v88internal8compiler18SpecialRPONumberer15ComputeLoopInfoEPNS0_10ZoneVectorINS2_20SpecialRPOStackFrameEEEmPNS3_ISt4pairIPNS1_10BasicBlockEmEEE,"axG",@progbits,_ZN2v88internal8compiler18SpecialRPONumberer15ComputeLoopInfoEPNS0_10ZoneVectorINS2_20SpecialRPOStackFrameEEEmPNS3_ISt4pairIPNS1_10BasicBlockEmEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler18SpecialRPONumberer15ComputeLoopInfoEPNS0_10ZoneVectorINS2_20SpecialRPOStackFrameEEEmPNS3_ISt4pairIPNS1_10BasicBlockEmEEE
	.type	_ZN2v88internal8compiler18SpecialRPONumberer15ComputeLoopInfoEPNS0_10ZoneVectorINS2_20SpecialRPOStackFrameEEEmPNS3_ISt4pairIPNS1_10BasicBlockEmEEE, @function
_ZN2v88internal8compiler18SpecialRPONumberer15ComputeLoopInfoEPNS0_10ZoneVectorINS2_20SpecialRPOStackFrameEEEmPNS3_ISt4pairIPNS1_10BasicBlockEmEEE:
.LFB10953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	40(%rdi), %rdx
	movq	48(%rdi), %r8
	movq	%rsi, -120(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r8, %rdx
	je	.L734
	movq	%rdx, %rbx
	movq	%rdi, %rdx
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L736:
	addq	$48, %rbx
	movl	%r14d, (%r12)
	cmpq	%r8, %rbx
	je	.L807
.L751:
	movq	8(%rdx), %rax
	movq	16(%rbx), %r12
	movl	$1, %r15d
	movq	24(%rax), %r14
	subq	16(%rax), %r14
	sarq	$3, %r14
	cmpl	$64, %r14d
	jle	.L735
	leal	-1(%r14), %r15d
	sarl	$6, %r15d
	addl	$1, %r15d
.L735:
	movl	4(%r12), %r13d
	cmpl	%r15d, %r13d
	jge	.L736
	movq	(%rdx), %rdi
	movslq	%r15d, %rsi
	movq	8(%r12), %rcx
	salq	$3, %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r10
	subq	%rax, %r10
	cmpq	%r10, %rsi
	ja	.L808
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L738:
	movq	%rax, 8(%r12)
	movq	%rax, %rsi
	movl	%r15d, 4(%r12)
	cmpl	$1, %r15d
	je	.L809
	cmpl	$1, %r13d
	je	.L810
	testl	%r13d, %r13d
	jle	.L744
	movq	(%rcx), %rsi
	movq	%rsi, (%rax)
	cmpl	$1, %r13d
	je	.L746
	leal	-2(%r13), %eax
	leaq	16(,%rax,8), %r10
	movl	$8, %eax
	.p2align 4,,10
	.p2align 3
.L747:
	movq	(%rcx,%rax), %rdi
	movq	8(%r12), %rsi
	movq	%rdi, (%rsi,%rax)
	addq	$8, %rax
	cmpq	%r10, %rax
	jne	.L747
.L746:
	cmpl	4(%r12), %r13d
	jge	.L736
	movq	8(%r12), %rsi
.L744:
	movslq	%r13d, %rax
	addl	$1, %r13d
	movq	$0, (%rsi,%rax,8)
	leaq	8(,%rax,8), %rax
	cmpl	%r13d, 4(%r12)
	jle	.L736
	.p2align 4,,10
	.p2align 3
.L750:
	movq	8(%r12), %rcx
	addl	$1, %r13d
	movq	$0, (%rcx,%rax)
	addq	$8, %rax
	cmpl	%r13d, 4(%r12)
	jg	.L750
	addq	$48, %rbx
	movl	%r14d, (%r12)
	cmpq	%r8, %rbx
	jne	.L751
.L807:
	movq	%rdx, %rbx
	movq	40(%rdx), %rdx
	pxor	%xmm0, %xmm0
	movabsq	$-6148914691236517205, %rcx
	movq	48(%rbx), %r8
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%r8, %rax
	movaps	%xmm0, -80(%rbp)
	subq	%rdx, %rax
	sarq	$4, %rax
	imulq	%rcx, %rax
	cmpq	%rax, %r9
	ja	.L780
	jnb	.L753
	leaq	(%r9,%r9,2), %rax
	salq	$4, %rax
	addq	%rdx, %rax
	cmpq	%r8, %rax
	je	.L753
	movq	%rax, 48(%rbx)
.L753:
	movq	-144(%rbp), %rdi
	movq	$0, -128(%rbp)
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	je	.L733
	.p2align 4,,10
	.p2align 3
.L754:
	movq	-128(%rbp), %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	(%rax), %r14
	movq	8(%rax), %rdx
	movq	104(%r14), %rax
	movq	(%rax,%rdx,8), %r12
	movslq	(%r12), %rax
	leaq	(%rax,%rax,2), %r13
	movq	40(%rbx), %rax
	salq	$4, %r13
	addq	%r13, %rax
	cmpq	$0, (%rax)
	je	.L811
.L755:
	cmpq	%r12, %r14
	je	.L766
	movq	160(%r14), %rsi
	movq	40(%rbx), %rax
	movl	%esi, %edx
	movq	16(%rax,%r13), %rdi
	sarl	$31, %edx
	shrl	$26, %edx
	movq	8(%rdi), %rcx
	leal	(%rdx,%rsi), %eax
	andl	$63, %eax
	subl	%edx, %eax
	cmpl	$1, 4(%rdi)
	je	.L812
	testl	%esi, %esi
	leal	63(%rsi), %edx
	cmovns	%esi, %edx
	sarl	$6, %edx
	movslq	%edx, %rdx
	leaq	(%rcx,%rdx,8), %rcx
	movq	(%rcx), %rdx
	btq	%rax, %rdx
	jc	.L768
	btsq	%rax, %rdx
	movq	%rdx, (%rcx)
.L768:
	movq	-120(%rbp), %rax
	movq	8(%rax), %rax
	movq	%r14, (%rax)
	movl	$1, %r14d
	.p2align 4,,10
	.p2align 3
.L772:
	movq	-120(%rbp), %rdi
	subl	$1, %r14d
	xorl	%edx, %edx
	movslq	%r14d, %rax
	salq	$4, %rax
	addq	8(%rdi), %rax
	movq	(%rax), %r15
	movq	136(%r15), %r8
	movq	144(%r15), %r11
	cmpq	%r8, %r11
	jne	.L771
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L774:
	testl	%ecx, %ecx
	leal	63(%rcx), %esi
	cmovs	%esi, %ecx
	sarl	$6, %ecx
	movslq	%ecx, %rcx
	leaq	(%r10,%rcx,8), %rsi
	movq	(%rsi), %rcx
	btq	%rax, %rcx
	jc	.L773
	btsq	%rax, %rcx
	movq	%rcx, (%rsi)
.L777:
	movq	-120(%rbp), %rsi
	movslq	%r14d, %rax
	addl	$1, %r14d
	salq	$4, %rax
	addq	8(%rsi), %rax
	movq	%rdi, (%rax)
	movq	136(%r15), %r8
	movq	144(%r15), %r11
.L773:
	movq	%r11, %rax
	addq	$1, %rdx
	subq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L778
.L771:
	movq	(%r8,%rdx,8), %rdi
	cmpq	%rdi, %r12
	je	.L773
	movq	160(%rdi), %rcx
	movq	40(%rbx), %rax
	movl	%ecx, %esi
	movq	16(%rax,%r13), %r9
	sarl	$31, %esi
	shrl	$26, %esi
	movq	8(%r9), %r10
	leal	(%rsi,%rcx), %eax
	andl	$63, %eax
	subl	%esi, %eax
	cmpl	$1, 4(%r9)
	jne	.L774
	btq	%rax, %r10
	jc	.L773
	btsq	%rcx, %r10
	movq	%r10, 8(%r9)
	jmp	.L777
.L809:
	movq	%rcx, 8(%r12)
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L778:
	testl	%r14d, %r14d
	jne	.L772
.L766:
	movq	-144(%rbp), %rdx
	addq	$1, -128(%rbp)
	movq	-128(%rbp), %rdi
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	%rdx, -136(%rbp)
	subq	%rax, %rdx
	sarq	$4, %rdx
	cmpq	%rdi, %rdx
	ja	.L754
.L733:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L813
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L812:
	.cfi_restore_state
	btq	%rax, %rcx
	jc	.L768
	btsq	%rsi, %rcx
	movq	%rcx, 8(%rdi)
	jmp	.L768
.L811:
	movq	%r12, (%rax)
	movq	8(%rbx), %rax
	movq	(%rbx), %rdi
	movq	24(%rax), %rdx
	subq	16(%rax), %rdx
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	sarq	$3, %rdx
	subq	%r15, %rax
	cmpq	$15, %rax
	jbe	.L814
	leaq	16(%r15), %rax
	movq	%rax, 16(%rdi)
.L757:
	movq	(%rbx), %rdi
	movl	%edx, (%r15)
	cmpl	$64, %edx
	jle	.L815
	leal	-1(%rdx), %eax
	movq	$0, 8(%r15)
	sarl	$6, %eax
	addl	$1, %eax
	movl	%eax, 4(%r15)
	cltq
	movq	24(%rdi), %rdx
	leaq	0(,%rax,8), %rsi
	movq	16(%rdi), %rax
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L816
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L761:
	movl	4(%r15), %edx
	movq	%rax, 8(%r15)
	cmpl	$1, %edx
	je	.L817
	testl	%edx, %edx
	jle	.L759
	movq	$0, (%rax)
	cmpl	$1, 4(%r15)
	movl	$8, %edx
	movl	$1, %eax
	jle	.L759
.L764:
	movq	8(%r15), %rcx
	addl	$1, %eax
	movq	$0, (%rcx,%rdx)
	addq	$8, %rdx
	cmpl	%eax, 4(%r15)
	jg	.L764
.L759:
	movq	40(%rbx), %rax
	movq	%r15, 16(%rax,%r13)
	jmp	.L755
.L810:
	movq	%rcx, (%rax)
	cmpl	$1, 4(%r12)
	jle	.L736
	movl	$8, %eax
.L743:
	movq	8(%r12), %rcx
	addl	$1, %r13d
	movq	$0, (%rcx,%rax)
	addq	$8, %rax
	cmpl	%r13d, 4(%r12)
	jg	.L743
	jmp	.L736
.L817:
	movq	$0, 8(%r15)
	movq	40(%rbx), %rax
	movq	%r15, 16(%rax,%r13)
	jmp	.L755
.L815:
	movl	$1, 4(%r15)
	movq	$0, 8(%r15)
	movq	40(%rbx), %rax
	movq	%r15, 16(%rax,%r13)
	jmp	.L755
.L734:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%r9, %r9
	je	.L753
	xorl	%eax, %eax
.L780:
	movq	%r9, %rdx
	leaq	-112(%rbp), %rcx
	leaq	32(%rbx), %rdi
	movq	%r8, %rsi
	subq	%rax, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler18SpecialRPONumberer8LoopInfoENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	jmp	.L753
.L808:
	movq	%r9, -160(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %r8
	movq	-152(%rbp), %rdx
	movq	-160(%rbp), %r9
	jmp	.L738
.L814:
	movl	$16, %esi
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-136(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L757
.L816:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L761
.L813:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10953:
	.size	_ZN2v88internal8compiler18SpecialRPONumberer15ComputeLoopInfoEPNS0_10ZoneVectorINS2_20SpecialRPOStackFrameEEEmPNS3_ISt4pairIPNS1_10BasicBlockEmEEE, .-_ZN2v88internal8compiler18SpecialRPONumberer15ComputeLoopInfoEPNS0_10ZoneVectorINS2_20SpecialRPOStackFrameEEEmPNS3_ISt4pairIPNS1_10BasicBlockEmEEE
	.section	.rodata._ZN2v88internal8compiler18SpecialRPONumberer26ComputeAndInsertSpecialRPOEPNS1_10BasicBlockES4_.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"kBlockUnvisited1 == schedule_->start()->loop_number()"
	.section	.rodata._ZN2v88internal8compiler18SpecialRPONumberer26ComputeAndInsertSpecialRPOEPNS1_10BasicBlockES4_.str1.1,"aMS",@progbits,1
.LC12:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal8compiler18SpecialRPONumberer26ComputeAndInsertSpecialRPOEPNS1_10BasicBlockES4_.str1.8
	.align 8
.LC13:
	.string	"kBlockUnvisited1 == schedule_->start()->rpo_number()"
	.align 8
.LC14:
	.string	"0 == static_cast<int>(schedule_->rpo_order()->size())"
	.align 8
.LC15:
	.string	"id:%d is a loop header, increment loop depth to %d\n"
	.align 8
.LC16:
	.string	"id:%d is not in a loop (depth == %d)\n"
	.align 8
.LC17:
	.string	"id:%d has loop header id:%d, (depth == %d)\n"
	.section	.text._ZN2v88internal8compiler18SpecialRPONumberer26ComputeAndInsertSpecialRPOEPNS1_10BasicBlockES4_,"axG",@progbits,_ZN2v88internal8compiler18SpecialRPONumberer26ComputeAndInsertSpecialRPOEPNS1_10BasicBlockES4_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler18SpecialRPONumberer26ComputeAndInsertSpecialRPOEPNS1_10BasicBlockES4_
	.type	_ZN2v88internal8compiler18SpecialRPONumberer26ComputeAndInsertSpecialRPOEPNS1_10BasicBlockES4_, @function
_ZN2v88internal8compiler18SpecialRPONumberer26ComputeAndInsertSpecialRPOEPNS1_10BasicBlockES4_:
.LFB10944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -72(%rbp)
	movq	8(%rdi), %rdx
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	104(%rdx), %rax
	cmpl	$-1, (%rax)
	jne	.L920
	cmpl	$-1, 4(%rax)
	jne	.L921
	movq	88(%rdx), %rax
	subq	80(%rdx), %rax
	sarq	$3, %rax
	testl	%eax, %eax
	jne	.L922
	movq	24(%rsi), %rax
	movq	%rdi, %r12
	movq	112(%r12), %r8
	movq	%rax, -80(%rbp)
	leaq	96(%rdi), %rax
	movq	%rax, -96(%rbp)
	movq	24(%rdx), %rax
	movq	%r8, %rcx
	subq	16(%rdx), %rax
	sarq	$3, %rax
	movq	%rax, %rsi
	subq	128(%rdi), %rsi
	movq	104(%rdi), %rdi
	subq	%rdi, %rcx
	sarq	$4, %rcx
	cmpq	%rcx, %rsi
	ja	.L923
	jnb	.L823
	salq	$4, %rsi
	addq	%rdi, %rsi
	cmpq	%rsi, %r8
	je	.L823
	movq	%rsi, 112(%r12)
	movq	24(%rdx), %rax
	subq	16(%rdx), %rax
	sarq	$3, %rax
	jmp	.L823
.L923:
	subq	%rcx, %rsi
	leaq	96(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler18SpecialRPONumberer20SpecialRPOStackFrameENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	8(%r12), %rdx
	movq	24(%rdx), %rax
	subq	16(%rdx), %rax
	sarq	$3, %rax
.L823:
	movq	%rax, 128(%r12)
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rbx
	cmpl	$-1, 4(%rax)
	je	.L924
.L824:
	cmpq	$0, 16(%r12)
	je	.L925
.L867:
	movq	-88(%rbp), %rax
	movl	48(%rax), %r15d
	cmpq	$1, 40(%rax)
	movq	32(%rax), %r14
	adcl	$-1, %r15d
	xorl	%r13d, %r13d
	cmpq	-80(%rbp), %rbx
	je	.L818
	.p2align 4,,10
	.p2align 3
.L869:
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi@PLT
	testq	%r14, %r14
	je	.L870
	.p2align 4,,10
	.p2align 3
.L874:
	cmpq	%rbx, 40(%r14)
	jne	.L870
	movq	24(%r13), %r13
	subl	$1, %r15d
	testq	%r13, %r13
	je	.L894
	movq	0(%r13), %r14
	testq	%r14, %r14
	jne	.L874
	.p2align 4,,10
	.p2align 3
.L870:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler10BasicBlock15set_loop_headerEPS2_@PLT
	movslq	(%rbx), %rax
	testl	%eax, %eax
	js	.L873
	leaq	(%rax,%rax,2), %r13
	addl	$1, %r15d
	salq	$4, %r13
	addq	40(%r12), %r13
	movq	32(%r13), %r14
	testq	%r14, %r14
	je	.L926
.L875:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler10BasicBlock12set_loop_endEPS2_@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	0(%r13), %r14
	jne	.L927
.L873:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler10BasicBlock14set_loop_depthEi@PLT
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L928
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L929
.L880:
	movq	24(%rbx), %rbx
	cmpq	%rbx, -80(%rbp)
	jne	.L869
.L818:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L930
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L894:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L928:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L880
	movl	48(%rbx), %edx
	movl	160(%rbx), %esi
	leaq	.LC16(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L929:
	movl	48(%rbx), %ecx
	movq	160(%rax), %rdx
	xorl	%eax, %eax
	leaq	.LC17(%rip), %rdi
	movl	160(%rbx), %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L927:
	movl	160(%rbx), %esi
	movl	%r15d, %edx
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L926:
	movq	24(%r12), %r14
	testq	%r14, %r14
	jne	.L875
	movq	8(%r12), %rax
	movq	(%rax), %r9
	movq	16(%r9), %r14
	movq	24(%r9), %rax
	subq	%r14, %rax
	cmpq	$167, %rax
	jbe	.L931
	leaq	168(%r14), %rax
	movq	%rax, 16(%r9)
.L878:
	movq	$-1, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler10BasicBlockC1EPNS0_4ZoneENS2_2IdE@PLT
	movq	%r14, 24(%r12)
	jmp	.L875
.L925:
	movq	%rbx, 16(%r12)
	jmp	.L867
.L920:
	leaq	.LC11(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L921:
	leaq	.LC13(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L922:
	leaq	.LC14(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L924:
	movq	%rax, %rdi
	movq	104(%r12), %rax
	movl	$-2, %esi
	movl	$1, %r14d
	movq	%rdi, (%rax)
	movq	104(%r12), %rax
	movq	$0, 8(%rax)
	call	_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi@PLT
	movq	48(%r12), %rax
	subq	40(%r12), %rax
	sarq	$4, %rax
	movq	-80(%rbp), %r15
	imull	$-1431655765, %eax, %eax
	movl	%eax, -100(%rbp)
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L934:
	leaq	1(%r8), %rax
	movq	%rax, 8(%rcx)
	movq	104(%rbx), %rax
	movq	(%rax,%r8,8), %r13
	movl	4(%r13), %eax
	cmpl	$-3, %eax
	je	.L827
	cmpl	$-2, %eax
	je	.L932
	cmpl	$-1, %eax
	je	.L933
.L827:
	movq	%r15, %rbx
	movl	%r14d, %r13d
.L830:
	movq	%rbx, %r15
	movl	%r13d, %r14d
.L825:
	leal	-1(%r14), %r13d
	movq	104(%r12), %rcx
	movslq	%r13d, %rdx
	salq	$4, %rdx
	addq	%rdx, %rcx
	movq	(%rcx), %rbx
	cmpq	-72(%rbp), %rbx
	je	.L826
	movq	112(%rbx), %rax
	movq	8(%rcx), %r8
	subq	104(%rbx), %rax
	sarq	$3, %rax
	cmpq	%rax, %r8
	jb	.L934
.L826:
	movq	%r15, 24(%rbx)
	movq	(%rcx), %rdi
	movl	$-3, %esi
	call	_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi@PLT
	testl	%r13d, %r13d
	jne	.L830
	movq	48(%r12), %rax
	subq	40(%r12), %rax
	sarq	$4, %rax
	movl	-100(%rbp), %edi
	imull	$-1431655765, %eax, %eax
	cmpl	%edi, %eax
	jge	.L824
	movq	-96(%rbp), %rsi
	leaq	64(%r12), %rcx
	movslq	%edi, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18SpecialRPONumberer15ComputeLoopInfoEPNS0_10ZoneVectorINS2_20SpecialRPOStackFrameEEEmPNS3_ISt4pairIPNS1_10BasicBlockEmEEE
	movq	-88(%rbp), %rax
	xorl	%ecx, %ecx
	movslq	(%rax), %rax
	testl	%eax, %eax
	js	.L842
	leaq	(%rax,%rax,2), %rcx
	salq	$4, %rcx
	addq	40(%r12), %rcx
.L842:
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rbx
	cmpl	$-3, 4(%rax)
	jne	.L824
	movq	%rax, %rdi
	movq	104(%r12), %rax
	movl	$-2, %esi
	movq	%rcx, -96(%rbp)
	movq	%rdi, (%rax)
	movq	104(%r12), %rax
	movq	$0, 8(%rax)
	call	_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi@PLT
	leaq	-64(%rbp), %rax
	movq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, -120(%rbp)
	movq	-80(%rbp), %rbx
	movl	$1, %r10d
	movq	%rcx, %r15
	.p2align 4,,10
	.p2align 3
.L843:
	movq	104(%r12), %rdx
	movslq	%r8d, %r14
	salq	$4, %r14
	addq	%r14, %rdx
	movq	(%rdx), %r13
	cmpq	%r13, -72(%rbp)
	je	.L844
	movq	112(%r13), %rax
	movq	8(%rdx), %rcx
	subq	104(%r13), %rax
	sarq	$3, %rax
	cmpq	%rax, %rcx
	jnb	.L844
	leaq	1(%rcx), %rax
	movq	%rax, 8(%rdx)
	movq	104(%r13), %rax
	movq	(%rax,%rcx,8), %rcx
.L848:
	testq	%rcx, %rcx
	je	.L935
	movl	4(%rcx), %edx
	movl	%edx, %eax
	andl	$-3, %eax
	cmpl	$-4, %eax
	je	.L843
	testq	%r15, %r15
	je	.L853
	movq	16(%r15), %rsi
	movq	160(%rcx), %rax
	cmpl	$1, 4(%rsi)
	movq	8(%rsi), %rdi
	je	.L855
	testl	%eax, %eax
	leal	63(%rax), %esi
	cmovns	%eax, %esi
	sarl	$6, %esi
	movslq	%esi, %rsi
	movq	(%rdi,%rsi,8), %rdi
.L855:
	movl	%eax, %esi
	sarl	$31, %esi
	shrl	$26, %esi
	addl	%esi, %eax
	andl	$63, %eax
	subl	%esi, %eax
	btq	%rax, %rdi
	jnc	.L856
.L853:
	cmpl	$-3, %edx
	je	.L936
.L858:
	movslq	(%rcx), %rax
	testl	%eax, %eax
	js	.L843
	movq	%r15, %xmm0
	movq	%rbx, %xmm1
	leaq	(%rax,%rax,2), %r15
	punpcklqdq	%xmm1, %xmm0
	salq	$4, %r15
	addq	40(%r12), %r15
	movups	%xmm0, 24(%r15)
	jmp	.L843
.L932:
	movq	80(%r12), %rcx
	cmpq	88(%r12), %rcx
	je	.L832
	movq	%rbx, (%rcx)
	movq	%r8, 8(%rcx)
	addq	$16, 80(%r12)
.L833:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jns	.L827
	movl	-100(%rbp), %eax
	movl	%eax, 0(%r13)
	addl	$1, %eax
	movl	%eax, -100(%rbp)
	jmp	.L827
.L933:
	movq	104(%r12), %rax
	movl	$-2, %esi
	movq	%r13, %rdi
	addl	$1, %r14d
	movq	%r13, 16(%rax,%rdx)
	movq	104(%r12), %rax
	movq	$0, 24(%rax,%rdx)
	call	_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi@PLT
	jmp	.L825
.L832:
	movq	72(%r12), %r9
	movq	%rcx, %rdx
	subq	%r9, %rdx
	movq	%rdx, %rsi
	sarq	$4, %rsi
	cmpq	$134217727, %rsi
	je	.L937
	testq	%rsi, %rsi
	je	.L888
	leaq	(%rsi,%rsi), %rax
	cmpq	%rax, %rsi
	jbe	.L938
	movl	$2147483632, %esi
	movl	$2147483632, %r10d
.L835:
	movq	64(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r11
	subq	%rax, %r11
	cmpq	%rsi, %r11
	jb	.L939
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L838:
	movq	%rax, %rdi
	addq	%rax, %r10
	leaq	16(%rax), %rax
.L836:
	addq	%rdi, %rdx
	movq	%rbx, (%rdx)
	movq	%r8, 8(%rdx)
	cmpq	%r9, %rcx
	je	.L839
	movq	%r9, %rax
	movq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L840:
	movq	(%rax), %r8
	movq	8(%rax), %rsi
	addq	$16, %rax
	addq	$16, %rdx
	movq	%r8, -16(%rdx)
	movq	%rsi, -8(%rdx)
	cmpq	%rax, %rcx
	jne	.L840
	subq	%r9, %rcx
	leaq	16(%rdi,%rcx), %rax
.L839:
	movq	%rdi, %xmm0
	movq	%rax, %xmm2
	movq	%r10, 88(%r12)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 72(%r12)
	jmp	.L833
.L888:
	movl	$16, %esi
	movl	$16, %r10d
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L844:
	movslq	0(%r13), %rax
	testl	%eax, %eax
	js	.L847
	cmpl	$-2, 4(%r13)
	je	.L940
	cmpq	%r13, -88(%rbp)
	je	.L884
.L883:
	movslq	%eax, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	salq	$4, %rcx
	addq	40(%r12), %rcx
	movq	8(%rcx), %r11
	testq	%r11, %r11
	je	.L850
	movq	8(%rdx), %rsi
	movq	112(%r13), %rcx
	subq	104(%r13), %rcx
	movq	8(%r11), %rdi
	sarq	$3, %rcx
	movq	%rsi, %r9
	subq	%rcx, %r9
	movq	16(%r11), %rcx
	subq	%rdi, %rcx
	sarq	$3, %rcx
	cmpq	%rcx, %r9
	jb	.L941
.L850:
	testl	%eax, %eax
	js	.L847
.L884:
	leaq	(%rax,%rax,2), %rsi
	salq	$4, %rsi
	addq	40(%r12), %rsi
	movq	40(%rsi), %rax
	movq	32(%rsi), %rcx
	.p2align 4,,10
	.p2align 3
.L865:
	movq	%rax, %rdx
	movq	24(%rax), %rax
	cmpq	%rax, %rcx
	jne	.L865
	movq	%rbx, 24(%rdx)
	movq	%rbx, 32(%rsi)
	movq	40(%rsi), %rbx
.L866:
	testl	%r8d, %r8d
	je	.L824
	movl	%r8d, %r10d
	subl	$1, %r8d
	jmp	.L843
.L847:
	movq	%rbx, 24(%r13)
	movl	$-4, %esi
	movq	%r13, %rdi
	movq	%r13, %rbx
	movl	%r8d, -96(%rbp)
	call	_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi@PLT
	movl	-96(%rbp), %r8d
	jmp	.L866
.L856:
	movq	8(%r15), %rdi
	movq	(%r12), %r13
	movq	%rcx, -64(%rbp)
	testq	%rdi, %rdi
	je	.L942
.L859:
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L862
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%rdi)
	jmp	.L843
.L941:
	addq	$1, %rsi
	movq	(%rdi,%r9,8), %rcx
	movq	%rsi, 8(%rdx)
	jmp	.L848
.L940:
	movq	%rbx, 24(%r13)
	movl	$-4, %esi
	movq	%r13, %rdi
	movq	32(%r15), %rbx
	movq	%r13, 40(%r15)
	movl	%r8d, -112(%rbp)
	movl	%r10d, -100(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi@PLT
	cmpq	%r13, -88(%rbp)
	movq	24(%r15), %r15
	movslq	0(%r13), %rax
	movq	-96(%rbp), %rdx
	movl	-100(%rbp), %r10d
	movl	-112(%rbp), %r8d
	jne	.L883
	jmp	.L850
.L936:
	movq	104(%r12), %rax
	movq	%rcx, %rdi
	movl	$-2, %esi
	movl	%r10d, -100(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rcx, 16(%rax,%r14)
	movq	104(%r12), %rax
	movq	$0, 24(%rax,%r14)
	call	_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi@PLT
	movl	-100(%rbp), %r10d
	movq	-96(%rbp), %rcx
	addl	$1, %r10d
	leal	-1(%r10), %r8d
	jmp	.L858
.L942:
	movq	16(%r13), %rdi
	movq	24(%r13), %rax
	subq	%rdi, %rax
	cmpq	$31, %rax
	jbe	.L943
	leaq	32(%rdi), %rax
	movq	%rax, 16(%r13)
.L861:
	movq	%r13, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	%rdi, 8(%r15)
	jmp	.L859
.L938:
	testq	%rax, %rax
	jne	.L944
	movl	$16, %eax
	xorl	%r10d, %r10d
	xorl	%edi, %edi
	jmp	.L836
.L862:
	movq	-120(%rbp), %rdx
	movl	%r8d, -100(%rbp)
	movl	%r10d, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movl	-96(%rbp), %r10d
	movl	-100(%rbp), %r8d
	jmp	.L843
.L931:
	movq	%r9, %rdi
	movl	$168, %esi
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r9
	movq	%rax, %r14
	jmp	.L878
.L939:
	movq	%rdx, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%r10, -120(%rbp)
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %r8
	movq	-120(%rbp), %r10
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %r9
	movq	-144(%rbp), %rdx
	jmp	.L838
.L935:
	movslq	0(%r13), %rax
	jmp	.L850
.L943:
	movq	%r13, %rdi
	movl	$32, %esi
	movl	%r8d, -100(%rbp)
	movl	%r10d, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-96(%rbp), %r10d
	movl	-100(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L861
.L937:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L944:
	cmpq	$134217727, %rax
	movl	$134217727, %r10d
	cmovbe	%rax, %r10
	salq	$4, %r10
	movq	%r10, %rsi
	jmp	.L835
.L930:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10944:
	.size	_ZN2v88internal8compiler18SpecialRPONumberer26ComputeAndInsertSpecialRPOEPNS1_10BasicBlockES4_, .-_ZN2v88internal8compiler18SpecialRPONumberer26ComputeAndInsertSpecialRPOEPNS1_10BasicBlockES4_
	.section	.text._ZN2v88internal8compiler9Scheduler17ComputeSpecialRPOEPNS0_4ZoneEPNS1_8ScheduleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler17ComputeSpecialRPOEPNS0_4ZoneEPNS1_8ScheduleE
	.type	_ZN2v88internal8compiler9Scheduler17ComputeSpecialRPOEPNS0_4ZoneEPNS1_8ScheduleE, @function
_ZN2v88internal8compiler9Scheduler17ComputeSpecialRPOEPNS0_4ZoneEPNS1_8ScheduleE:
.LFB10954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm2
	movq	%rdi, %xmm1
	punpcklqdq	%xmm2, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$216, %rsp
	movq	112(%rsi), %rdx
	movq	104(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -176(%rbp)
	movq	%rdi, -144(%rbp)
	movq	%rdi, -112(%rbp)
	movq	%rdi, -72(%rbp)
	leaq	-208(%rbp), %rdi
	movaps	%xmm1, -208(%rbp)
	pxor	%xmm1, %xmm1
	movq	$0, -168(%rbp)
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movaps	%xmm1, -192(%rbp)
	call	_ZN2v88internal8compiler18SpecialRPONumberer26ComputeAndInsertSpecialRPOEPNS1_10BasicBlockES4_
	movq	-192(%rbp), %rdi
	movq	%rdi, -216(%rbp)
	testq	%rdi, %rdi
	je	.L955
	xorl	%esi, %esi
	leaq	-216(%rbp), %r13
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L964:
	movq	-216(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 88(%rdi)
.L963:
	movq	-216(%rbp), %rax
	movq	24(%rax), %rdi
	movq	%rdi, -216(%rbp)
	testq	%rdi, %rdi
	je	.L946
	movl	%r12d, %esi
.L950:
	leal	1(%rsi), %r12d
	call	_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi@PLT
	movq	-200(%rbp), %rdi
	movq	88(%rdi), %rsi
	cmpq	96(%rdi), %rsi
	jne	.L964
	addq	$72, %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler10BasicBlockENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L955:
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L946:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L965
.L951:
	movl	%r12d, %esi
	call	_ZN2v88internal8compiler10BasicBlock14set_rpo_numberEi@PLT
	leaq	72(%rbx), %rax
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L966
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L965:
	.cfi_restore_state
	movq	-200(%rbp), %rax
	movq	(%rax), %r13
	movq	16(%r13), %rdi
	movq	24(%r13), %rax
	subq	%rdi, %rax
	cmpq	$167, %rax
	jbe	.L967
	leaq	168(%rdi), %rax
	movq	%rax, 16(%r13)
.L953:
	movq	$-1, %rdx
	movq	%r13, %rsi
	movq	%rdi, -232(%rbp)
	call	_ZN2v88internal8compiler10BasicBlockC1EPNS0_4ZoneENS2_2IdE@PLT
	movq	-232(%rbp), %rdi
	movq	%rdi, -184(%rbp)
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L967:
	movq	%r13, %rdi
	movl	$168, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L953
.L966:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10954:
	.size	_ZN2v88internal8compiler9Scheduler17ComputeSpecialRPOEPNS0_4ZoneEPNS1_8ScheduleE, .-_ZN2v88internal8compiler9Scheduler17ComputeSpecialRPOEPNS0_4ZoneEPNS1_8ScheduleE
	.section	.rodata._ZN2v88internal8compiler9Scheduler26ComputeSpecialRPONumberingEv.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"--- COMPUTING SPECIAL RPO ----------------------------------\n"
	.section	.text._ZN2v88internal8compiler9Scheduler26ComputeSpecialRPONumberingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler26ComputeSpecialRPONumberingEv
	.type	_ZN2v88internal8compiler9Scheduler26ComputeSpecialRPONumberingEv, @function
_ZN2v88internal8compiler9Scheduler26ComputeSpecialRPONumberingEv:
.LFB10958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L973
.L969:
	movq	(%rbx), %r8
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	$167, %rax
	jbe	.L974
	leaq	168(%rdi), %rax
	movq	%rax, 16(%r8)
.L971:
	movq	16(%rbx), %rdx
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	%rax, (%rdi)
	movq	%rdx, 8(%rdi)
	movq	%rax, 32(%rdi)
	movq	%rax, 64(%rdi)
	movq	%rax, 96(%rdi)
	movq	%rax, 136(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 88(%rdi)
	movq	$0, 104(%rdi)
	movq	$0, 112(%rdi)
	movq	$0, 120(%rdi)
	movq	$0, 128(%rdi)
	movq	$0, 144(%rdi)
	movq	$0, 152(%rdi)
	movq	$0, 160(%rdi)
	movups	%xmm0, 16(%rdi)
	movq	%rdi, 232(%rbx)
	movq	8(%rdi), %rax
	movq	112(%rax), %rdx
	movq	104(%rax), %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18SpecialRPONumberer26ComputeAndInsertSpecialRPOEPNS1_10BasicBlockES4_
	.p2align 4,,10
	.p2align 3
.L973:
	.cfi_restore_state
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L974:
	movq	%r8, %rdi
	movl	$168, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L971
	.cfi_endproc
.LFE10958:
	.size	_ZN2v88internal8compiler9Scheduler26ComputeSpecialRPONumberingEv, .-_ZN2v88internal8compiler9Scheduler26ComputeSpecialRPONumberingEv
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E:
.LFB12657:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L983
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L977:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L977
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L983:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE12657:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, .-_ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.section	.rodata._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.type	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, @function
_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_:
.LFB13052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$3, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L1004
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L1005
.L988:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L996
	cmpq	$63, 8(%rax)
	ja	.L1006
.L996:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1007
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L997:
	movq	%rax, 8(%r13)
	movq	(%r12), %rdx
	movq	64(%rbx), %rax
	movq	%rdx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1005:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L1008
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L1009
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L993:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L994
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L994:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L995
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L995:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L991:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1006:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L1008:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L990
	cmpq	%r13, %rsi
	je	.L991
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L990:
	cmpq	%r13, %rsi
	je	.L991
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L1007:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L993
.L1004:
	leaq	.LC19(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13052:
	.size	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, .-_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.section	.rodata._ZN2v88internal8compiler24ScheduleEarlyNodeVisitor30PropagateMinimumPositionToNodeEPNS1_10BasicBlockEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"Propagating #%d:%s minimum_block = id:%d, dominator_depth = %d\n"
	.section	.text._ZN2v88internal8compiler24ScheduleEarlyNodeVisitor30PropagateMinimumPositionToNodeEPNS1_10BasicBlockEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler24ScheduleEarlyNodeVisitor30PropagateMinimumPositionToNodeEPNS1_10BasicBlockEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler24ScheduleEarlyNodeVisitor30PropagateMinimumPositionToNodeEPNS1_10BasicBlockEPNS1_4NodeE
	.type	_ZN2v88internal8compiler24ScheduleEarlyNodeVisitor30PropagateMinimumPositionToNodeEPNS1_10BasicBlockEPNS1_4NodeE, @function
_ZN2v88internal8compiler24ScheduleEarlyNodeVisitor30PropagateMinimumPositionToNodeEPNS1_10BasicBlockEPNS1_4NodeE:
.LFB10984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movl	20(%rdx), %ebx
	movq	(%rdi), %rax
	movq	%rdx, -40(%rbp)
	andl	$16777215, %ebx
	salq	$4, %rbx
	addq	200(%rax), %rbx
	movl	12(%rbx), %eax
	cmpl	$2, %eax
	je	.L1010
	movq	%rsi, %r13
	cmpl	$3, %eax
	je	.L1018
.L1013:
	movq	(%rbx), %rax
	movl	12(%rax), %eax
	cmpl	%eax, 12(%r13)
	jg	.L1019
.L1010:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1019:
	.cfi_restore_state
	movq	%r13, (%rbx)
	movq	96(%r12), %rax
	movq	80(%r12), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1015
	movq	-40(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 80(%r12)
.L1016:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L1010
	movq	-40(%rbp), %rdx
	movq	(%rbx), %rax
	leaq	.LC20(%rip), %rdi
	movq	(%rdx), %rsi
	movq	160(%rax), %rcx
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	8(%rsi), %r9
	movl	20(%rdx), %esi
	movq	%r9, %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	%rdx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler24ScheduleEarlyNodeVisitor30PropagateMinimumPositionToNodeEPNS1_10BasicBlockEPNS1_4NodeE
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1015:
	leaq	-40(%rbp), %rsi
	leaq	16(%r12), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1016
	.cfi_endproc
.LFE10984:
	.size	_ZN2v88internal8compiler24ScheduleEarlyNodeVisitor30PropagateMinimumPositionToNodeEPNS1_10BasicBlockEPNS1_4NodeE, .-_ZN2v88internal8compiler24ScheduleEarlyNodeVisitor30PropagateMinimumPositionToNodeEPNS1_10BasicBlockEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler9Scheduler28DecrementUnscheduledUseCountEPNS1_4NodeEiS4_.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"  Use count of #%d:%s (used by #%d:%s)-- = %d\n"
	.section	.rodata._ZN2v88internal8compiler9Scheduler28DecrementUnscheduledUseCountEPNS1_4NodeEiS4_.str1.1,"aMS",@progbits,1
.LC22:
	.string	"    newly eligible #%d:%s\n"
	.section	.text._ZN2v88internal8compiler9Scheduler28DecrementUnscheduledUseCountEPNS1_4NodeEiS4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler28DecrementUnscheduledUseCountEPNS1_4NodeEiS4_
	.type	_ZN2v88internal8compiler9Scheduler28DecrementUnscheduledUseCountEPNS1_4NodeEiS4_, @function
_ZN2v88internal8compiler9Scheduler28DecrementUnscheduledUseCountEPNS1_4NodeEiS4_:
.LFB10890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	20(%rcx), %eax
	movq	%rsi, -40(%rbp)
	movq	200(%rdi), %rsi
	andl	$16777215, %eax
	salq	$4, %rax
	cmpl	$3, 12(%rsi,%rax)
	je	.L1035
.L1021:
	movq	-40(%rbp), %rdi
	movl	20(%rdi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rsi, %rax
	movl	12(%rax), %edx
	cmpl	$2, %edx
	je	.L1020
	cmpl	$3, %edx
	je	.L1036
	subl	$1, 8(%rax)
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L1037
	movl	20(%rdi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	200(%r12), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1020
.L1028:
	movq	176(%r12), %rax
	movq	160(%r12), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1029
	movq	-40(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 160(%r12)
.L1020:
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1037:
	.cfi_restore_state
	movl	20(%rdi), %esi
	movq	(%rdi), %rdx
	leaq	.LC21(%rip), %rdi
	movq	(%r14), %r8
	movl	20(%r14), %ecx
	andl	$16777215, %esi
	movq	8(%rdx), %rdx
	movl	%esi, %eax
	movq	8(%r8), %r8
	andl	$16777215, %ecx
	salq	$4, %rax
	addq	200(%r12), %rax
	movl	8(%rax), %r9d
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-40(%rbp), %rdx
	movl	20(%rdx), %esi
	andl	$16777215, %esi
	movl	%esi, %eax
	salq	$4, %rax
	addq	200(%r12), %rax
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L1020
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L1028
	movq	(%rdx), %rax
	leaq	.LC22(%rip), %rdi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1035:
	movq	%rcx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE@PLT
	cmpl	%eax, %r13d
	je	.L1020
	movq	200(%r12), %rsi
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1036:
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler9Scheduler28DecrementUnscheduledUseCountEPNS1_4NodeEiS4_
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1029:
	leaq	-40(%rbp), %rsi
	leaq	96(%r12), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1020
	.cfi_endproc
.LFE10890:
	.size	_ZN2v88internal8compiler9Scheduler28DecrementUnscheduledUseCountEPNS1_4NodeEiS4_, .-_ZN2v88internal8compiler9Scheduler28DecrementUnscheduledUseCountEPNS1_4NodeEiS4_
	.section	.rodata._ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE.str1.1,"aMS",@progbits,1
.LC23:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	.type	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE, @function
_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE:
.LFB10887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movl	%edx, -68(%rbp)
	movq	200(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	20(%rsi), %eax
	movl	%eax, %r12d
	andl	$16777215, %r12d
	salq	$4, %r12
	leaq	(%rdx,%r12), %rsi
	movl	12(%rsi), %r8d
	movq	%rsi, -80(%rbp)
	testl	%r8d, %r8d
	je	.L1077
	movq	(%rbx), %rcx
	movq	%rdi, %r14
	movzwl	16(%rcx), %ecx
	cmpw	$36, %cx
	ja	.L1041
	cmpw	$34, %cx
	ja	.L1042
	cmpw	$22, %cx
	ja	.L1043
	movq	24(%rbx), %r13
	testq	%r13, %r13
	je	.L1043
.L1049:
	movl	16(%r13), %ecx
	movl	%ecx, %eax
	shrl	%eax
	andl	$1, %ecx
	leaq	3(%rax,%rax,2), %rax
	leaq	0(%r13,%rax,8), %rsi
	jne	.L1045
	movq	(%rsi), %rsi
.L1045:
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	cmpl	$3, 12(%rdx,%rax)
	je	.L1078
	movq	0(%r13), %r13
	testq	%r13, %r13
	jne	.L1049
.L1048:
	movl	20(%rbx), %eax
	.p2align 4,,10
	.p2align 3
.L1043:
	shrl	$24, %eax
	leaq	32(%rbx), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1050
.L1081:
	subq	$24, %rbx
.L1051:
	cltq
	leaq	(%r15,%rax,8), %r13
	leaq	-64(%rbp), %rax
	movq	%rax, -96(%rbp)
	cmpq	%r13, %r15
	jne	.L1052
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1059:
	movl	20(%rdi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	200(%r14), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1056
.L1061:
	movq	176(%r14), %rax
	movq	160(%r14), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1062
	movq	-64(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 160(%r14)
.L1056:
	addq	$8, %r15
	subq	$24, %rbx
	cmpq	%r15, %r13
	je	.L1063
.L1052:
	movl	16(%rbx), %eax
	movl	%eax, %r12d
	shrl	%r12d
	movl	%r12d, %edx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rbx,%rdx,8), %rcx
	testb	$1, %al
	jne	.L1053
	movq	(%rcx), %rcx
.L1053:
	movl	20(%rcx), %eax
	movq	200(%r14), %rdx
	movq	(%r15), %rdi
	andl	$16777215, %eax
	salq	$4, %rax
	movq	%rdi, -64(%rbp)
	cmpl	$3, 12(%rdx,%rax)
	je	.L1079
.L1054:
	movl	20(%rdi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rdx, %rax
	movl	12(%rax), %edx
	cmpl	$2, %edx
	je	.L1056
	cmpl	$3, %edx
	je	.L1080
	subl	$1, 8(%rax)
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L1059
	movl	20(%rdi), %esi
	movq	(%rcx), %rax
	movq	(%rdi), %rdx
	movl	20(%rcx), %ecx
	leaq	.LC21(%rip), %rdi
	andl	$16777215, %esi
	movl	%esi, %r8d
	movq	8(%rdx), %rdx
	andl	$16777215, %ecx
	salq	$4, %r8
	addq	200(%r14), %r8
	movl	8(%r8), %r9d
	movq	8(%rax), %r8
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-64(%rbp), %rdx
	movl	20(%rdx), %esi
	andl	$16777215, %esi
	movl	%esi, %eax
	salq	$4, %rax
	addq	200(%r14), %rax
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L1056
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L1061
	movq	(%rdx), %rax
	leaq	.LC22(%rip), %rdi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1041:
	cmpw	$50, %cx
	jne	.L1043
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1042:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	32(%rbx), %r15
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	16(%r14), %rdi
	movq	%rax, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	16(%r14), %rdi
	movq	%rbx, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movl	20(%rbx), %eax
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1081
.L1050:
	movq	32(%rbx), %r10
	movl	8(%r10), %eax
	leaq	-24(%r10), %rbx
	leaq	16(%r10), %r15
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1079:
	movq	%rcx, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE@PLT
	cmpl	%eax, %r12d
	je	.L1056
	movq	-64(%rbp), %rdi
	movq	200(%r14), %rdx
	movq	-88(%rbp), %rcx
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1063:
	movq	-80(%rbp), %rax
	movl	-68(%rbp), %ebx
	movl	%ebx, 12(%rax)
.L1038:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1082
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1080:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-88(%rbp), %rcx
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler9Scheduler28DecrementUnscheduledUseCountEPNS1_4NodeEiS4_
	jmp	.L1056
	.p2align 4,,10
	.p2align 3
.L1077:
	movl	-68(%rbp), %ebx
	movl	%ebx, 12(%rsi)
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1062:
	movq	-96(%rbp), %rsi
	leaq	96(%r14), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1056
	.p2align 4,,10
	.p2align 3
.L1078:
	movl	-68(%rbp), %edx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L1048
	movq	200(%r14), %rdx
	jmp	.L1049
.L1082:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10887:
	.size	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE, .-_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	.section	.rodata._ZN2v88internal8compiler10CFGBuilder13ConnectBlocksEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"Connect #%d:%s, id:%d -> end\n"
	.section	.rodata._ZN2v88internal8compiler10CFGBuilder13ConnectBlocksEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"Connect #%d:%s, id:%d -> id:%d\n"
	.section	.text._ZN2v88internal8compiler10CFGBuilder13ConnectBlocksEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler10CFGBuilder13ConnectBlocksEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler10CFGBuilder13ConnectBlocksEPNS1_4NodeE
	.type	_ZN2v88internal8compiler10CFGBuilder13ConnectBlocksEPNS1_4NodeE, @function
_ZN2v88internal8compiler10CFGBuilder13ConnectBlocksEPNS1_4NodeE:
.LFB10899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movzwl	16(%rax), %eax
	cmpw	$49, %ax
	ja	.L1084
	testw	%ax, %ax
	je	.L1083
	cmpw	$49, %ax
	ja	.L1083
	leaq	.L1087(%rip), %rsi
	movzwl	%ax, %ecx
	movslq	(%rsi,%rcx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal8compiler10CFGBuilder13ConnectBlocksEPNS1_4NodeE,"aG",@progbits,_ZN2v88internal8compiler10CFGBuilder13ConnectBlocksEPNS1_4NodeE,comdat
	.align 4
	.align 4
.L1087:
	.long	.L1083-.L1087
	.long	.L1092-.L1087
	.long	.L1094-.L1087
	.long	.L1093-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1092-.L1087
	.long	.L1091-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1090-.L1087
	.long	.L1089-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1088-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1083-.L1087
	.long	.L1086-.L1087
	.section	.text._ZN2v88internal8compiler10CFGBuilder13ConnectBlocksEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler10CFGBuilder13ConnectBlocksEPNS1_4NodeE,comdat
	.p2align 4,,10
	.p2align 3
.L1084:
	subw	$682, %ax
	cmpw	$106, %ax
	ja	.L1083
.L1086:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_@PLT
	testb	%al, %al
	je	.L1083
	movq	8(%rbx), %rdi
	movl	$2, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	movl	$2, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m@PLT
	movq	16(%rbx), %rdi
	movq	-80(%rbp), %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-72(%rbp), %rsi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	movb	$1, 8(%rax)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1232:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L1159:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L1232
	movq	%rax, %r14
	movq	-80(%rbp), %rax
	movzbl	_ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip), %edx
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1233
	testb	%dl, %dl
	jne	.L1234
.L1161:
	movq	-72(%rbp), %r8
	testq	%r8, %r8
	je	.L1165
.L1173:
	movq	%rax, %rcx
.L1165:
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler8Schedule7AddCallEPNS1_10BasicBlockEPNS1_4NodeES4_S4_@PLT
.L1083:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1235
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1088:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movl	$2, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1236:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L1156:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L1236
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, %r14
	jne	.L1237
.L1157:
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler8Schedule8AddThrowEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	jmp	.L1083
.L1089:
	movq	8(%rdi), %rdi
	movl	$2, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1238:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L1150:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L1238
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, %r14
	jne	.L1239
.L1151:
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler8Schedule11AddTailCallEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	jmp	.L1083
.L1090:
	movq	8(%rdi), %rdi
	movl	$2, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1240:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L1153:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L1240
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, %r14
	jne	.L1241
.L1154:
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler8Schedule9AddReturnEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	jmp	.L1083
.L1091:
	movq	8(%rdi), %rdi
	movl	$2, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	jmp	.L1147
	.p2align 4,,10
	.p2align 3
.L1242:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L1147:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L1242
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, %r14
	jne	.L1243
.L1148:
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler8Schedule13AddDeoptimizeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	jmp	.L1083
.L1094:
	movq	8(%rdi), %rdi
	movl	$2, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	movl	$2, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m@PLT
	movq	16(%rbx), %rdi
	movq	-80(%rbp), %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	16(%rbx), %rdi
	movq	-72(%rbp), %rsi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	(%r12), %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler12BranchHintOfEPKNS1_8OperatorE@PLT
	cmpb	$1, %al
	je	.L1105
	cmpb	$2, %al
	jne	.L1107
	movq	-80(%rbp), %r8
	movb	$1, 8(%r8)
	cmpq	160(%rbx), %r12
	je	.L1244
.L1109:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	jmp	.L1119
	.p2align 4,,10
	.p2align 3
.L1245:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L1119:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L1245
	movq	%rax, %r14
	movq	-80(%rbp), %rax
	movzbl	_ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip), %edx
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1246
	testb	%dl, %dl
	jne	.L1247
	movq	-72(%rbp), %r8
	testq	%r8, %r8
	je	.L1126
.L1169:
	movq	%rax, %rcx
.L1126:
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler8Schedule9AddBranchEPNS1_10BasicBlockEPNS1_4NodeES4_S4_@PLT
	jmp	.L1083
.L1092:
	cmpw	$10, %ax
	je	.L1248
.L1095:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	leaq	32(%r12), %r13
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r14
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1098
	movq	32(%r12), %r13
	movl	8(%r13), %eax
	addq	$16, %r13
.L1098:
	cltq
	leaq	0(%r13,%rax,8), %rax
	movq	%rax, -88(%rbp)
	cmpq	%rax, %r13
	je	.L1083
	.p2align 4,,10
	.p2align 3
.L1104:
	movq	0(%r13), %r15
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1249:
	movq	%r15, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r15
.L1101:
	movq	16(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L1249
	movzbl	_ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip), %eax
	testq	%r14, %r14
	je	.L1250
	testb	%al, %al
	jne	.L1251
.L1103:
	movq	16(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r9, %rsi
	addq	$8, %r13
	call	_ZN2v88internal8compiler8Schedule7AddGotoEPNS1_10BasicBlockES4_@PLT
	cmpq	%r13, -88(%rbp)
	jne	.L1104
	jmp	.L1083
.L1093:
	movq	8(%rdi), %rdi
	movq	%r12, %rsi
	movl	$2, %edx
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	movq	(%r12), %rax
	movq	(%rbx), %rdi
	movslq	40(%rax), %rax
	movq	16(%rdi), %r13
	movq	%rax, -88(%rbp)
	salq	$3, %rax
	movq	%rax, -96(%rbp)
	movq	%rax, %rsi
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L1252
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1129:
	movq	-88(%rbp), %r14
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m@PLT
	testq	%r14, %r14
	je	.L1130
	movq	-96(%rbp), %rax
	movq	%r13, %r15
	leaq	(%rax,%r13), %r14
	.p2align 4,,10
	.p2align 3
.L1132:
	movq	(%r15), %rsi
	movq	16(%rbx), %rdi
	addq	$8, %r15
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, -8(%r15)
	cmpq	%r15, %r14
	jne	.L1132
	cmpq	160(%rbx), %r12
	je	.L1253
.L1170:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1254:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
.L1138:
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L1254
	movq	%rax, %r9
	movq	-96(%rbp), %rax
	cmpq	$0, -88(%rbp)
	movq	%r13, %r14
	leaq	.LC25(%rip), %r15
	leaq	(%rax,%r13), %r11
	je	.L1142
	movq	%r13, -104(%rbp)
	movq	%r12, %r13
	movq	%r11, %r12
	movq	%rbx, -112(%rbp)
	movq	%r9, %rbx
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1140:
	testb	%dl, %dl
	jne	.L1255
.L1141:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L1256
.L1143:
	movq	(%r14), %rax
	movzbl	_ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip), %edx
	testq	%rax, %rax
	jne	.L1140
	testb	%dl, %dl
	je	.L1141
	movq	0(%r13), %rax
	movl	20(%r13), %esi
	leaq	.LC24(%rip), %rdi
	movl	160(%rbx), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1250:
	testb	%al, %al
	je	.L1103
	movq	(%r12), %rax
	movl	20(%r12), %esi
	movq	%r9, -96(%rbp)
	leaq	.LC24(%rip), %rdi
	movl	160(%r9), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-96(%rbp), %r9
	jmp	.L1103
	.p2align 4,,10
	.p2align 3
.L1251:
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC25(%rip), %rdi
	movq	%r9, -96(%rbp)
	movl	160(%r9), %ecx
	movl	160(%r14), %r8d
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-96(%rbp), %r9
	jmp	.L1103
	.p2align 4,,10
	.p2align 3
.L1255:
	movq	0(%r13), %rdx
	movl	20(%r13), %esi
	movq	%r15, %rdi
	movl	160(%rax), %r8d
	movl	160(%rbx), %ecx
	xorl	%eax, %eax
	movq	8(%rdx), %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1105:
	movq	-72(%rbp), %rax
	movb	$1, 8(%rax)
.L1107:
	cmpq	160(%rbx), %r12
	jne	.L1109
	movq	-80(%rbp), %r8
	movq	168(%rbx), %rsi
	movzbl	_ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip), %eax
	testq	%r8, %r8
	je	.L1257
.L1110:
	testb	%al, %al
	jne	.L1258
.L1111:
	movq	-72(%rbp), %r9
	testq	%r9, %r9
	je	.L1167
.L1115:
	movq	176(%rbx), %rdx
	movq	16(%rbx), %rdi
	movq	%r12, %rcx
	call	_ZN2v88internal8compiler8Schedule12InsertBranchEPNS1_10BasicBlockES4_PNS1_4NodeES4_S4_@PLT
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1256:
	movq	%rbx, %r9
	movq	%r13, %r12
	movq	-112(%rbp), %rbx
	movq	-104(%rbp), %r13
.L1142:
	movq	16(%rbx), %rdi
	movq	-88(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler8Schedule9AddSwitchEPNS1_10BasicBlockEPNS1_4NodeEPS4_m@PLT
.L1136:
	cmpq	$0, -88(%rbp)
	je	.L1083
	movq	-96(%rbp), %rbx
	addq	%r13, %rbx
	.p2align 4,,10
	.p2align 3
.L1145:
	movq	0(%r13), %rax
	movq	72(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler12BranchHintOfEPKNS1_8OperatorE@PLT
	cmpb	$2, %al
	jne	.L1144
	movq	0(%r13), %rax
	movb	$1, 8(%rax)
.L1144:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1145
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1247:
	movq	(%r12), %rdx
	movl	20(%r12), %esi
	leaq	.LC25(%rip), %rdi
	movl	160(%rax), %r8d
	movl	160(%r14), %ecx
	xorl	%eax, %eax
	movq	8(%rdx), %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L1122:
	movq	-72(%rbp), %r8
	movzbl	_ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip), %eax
	testq	%r8, %r8
	je	.L1259
	testb	%al, %al
	je	.L1260
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC25(%rip), %rdi
	movl	160(%r8), %r8d
	movl	160(%r14), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rcx
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	168(%rbx), %rsi
	movzbl	_ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip), %eax
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1253:
	movq	168(%rbx), %rsi
	movq	%r13, %r15
	jmp	.L1135
	.p2align 4,,10
	.p2align 3
.L1133:
	testb	%dl, %dl
	jne	.L1261
.L1134:
	addq	$8, %r15
	cmpq	%r15, %r14
	je	.L1171
.L1135:
	movq	(%r15), %rax
	movzbl	_ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip), %edx
	testq	%rax, %rax
	jne	.L1133
	testb	%dl, %dl
	je	.L1134
	movq	(%r12), %rax
	movq	160(%rsi), %rcx
	leaq	.LC24(%rip), %rdi
	movl	20(%r12), %esi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	168(%rbx), %rsi
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1261:
	movq	(%r12), %rdx
	movq	160(%rsi), %rcx
	leaq	.LC25(%rip), %rdi
	movl	20(%r12), %esi
	movl	160(%rax), %r8d
	xorl	%eax, %eax
	movq	8(%rdx), %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	168(%rbx), %rsi
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1130:
	cmpq	160(%rbx), %r12
	jne	.L1170
	movq	168(%rbx), %rsi
	.p2align 4,,10
	.p2align 3
.L1171:
	movq	16(%rbx), %rdi
	movq	-88(%rbp), %r9
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	176(%rbx), %rdx
	call	_ZN2v88internal8compiler8Schedule12InsertSwitchEPNS1_10BasicBlockES4_PNS1_4NodeEPS4_m@PLT
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1248:
	movq	8(%rdi), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	movq	32(%rax), %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1096
	movq	16(%rdx), %rdx
.L1096:
	cmpq	%rdx, %r12
	jne	.L1095
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1241:
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC24(%rip), %rdi
	movl	160(%r14), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1237:
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC24(%rip), %rdi
	movl	160(%r14), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1243:
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC24(%rip), %rdi
	movl	160(%r14), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1239:
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC24(%rip), %rdi
	movl	160(%r14), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1246:
	testb	%dl, %dl
	jne	.L1262
	movq	-72(%rbp), %r8
	testq	%r8, %r8
	jne	.L1126
.L1168:
	movq	%rax, %rcx
	xorl	%r8d, %r8d
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1252:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1233:
	testb	%dl, %dl
	je	.L1161
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC24(%rip), %rdi
	movl	160(%r14), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L1162:
	movq	-72(%rbp), %r8
	movzbl	_ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip), %eax
	testq	%r8, %r8
	je	.L1263
	testb	%al, %al
	je	.L1264
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC25(%rip), %rdi
	movl	160(%r8), %r8d
	movl	160(%r14), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-72(%rbp), %r8
.L1231:
	movq	-80(%rbp), %rcx
	jmp	.L1165
.L1257:
	testb	%al, %al
	je	.L1111
	movq	(%r12), %rax
	movq	160(%rsi), %rcx
	leaq	.LC24(%rip), %rdi
	movl	20(%r12), %esi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L1112:
	movq	-72(%rbp), %r9
	movq	168(%rbx), %rsi
	movzbl	_ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip), %eax
	testq	%r9, %r9
	je	.L1265
	testb	%al, %al
	je	.L1266
	movq	(%r12), %rax
	movq	160(%rsi), %rcx
	leaq	.LC25(%rip), %rdi
	movl	20(%r12), %esi
	movl	160(%r9), %r8d
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r8
	movq	168(%rbx), %rsi
	jmp	.L1115
.L1234:
	movq	(%r12), %rdx
	movl	20(%r12), %esi
	leaq	.LC25(%rip), %rdi
	movl	160(%rax), %r8d
	movl	160(%r14), %ecx
	xorl	%eax, %eax
	movq	8(%rdx), %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1162
.L1258:
	movq	(%r12), %rax
	movq	160(%rsi), %rcx
	leaq	.LC25(%rip), %rdi
	movl	20(%r12), %esi
	movl	160(%r8), %r8d
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1112
.L1262:
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC24(%rip), %rdi
	movl	160(%r14), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1122
.L1259:
	testb	%al, %al
	je	.L1267
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC24(%rip), %rdi
	movl	160(%r14), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rcx
	jmp	.L1126
.L1167:
	movq	-80(%rbp), %r8
	xorl	%r9d, %r9d
	jmp	.L1115
.L1263:
	testb	%al, %al
	je	.L1231
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC24(%rip), %rdi
	movl	160(%r14), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rcx
	jmp	.L1165
.L1265:
	testb	%al, %al
	je	.L1167
	movq	(%r12), %rax
	movq	160(%rsi), %rcx
	leaq	.LC24(%rip), %rdi
	movl	20(%r12), %esi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r8
	movq	168(%rbx), %rsi
	jmp	.L1115
.L1235:
	call	__stack_chk_fail@PLT
.L1266:
	movq	-80(%rbp), %r8
	jmp	.L1115
.L1267:
	movq	-80(%rbp), %rax
	jmp	.L1168
.L1260:
	movq	-80(%rbp), %rax
	jmp	.L1169
.L1264:
	movq	-80(%rbp), %rax
	jmp	.L1173
	.cfi_endproc
.LFE10899:
	.size	_ZN2v88internal8compiler10CFGBuilder13ConnectBlocksEPNS1_4NodeE, .-_ZN2v88internal8compiler10CFGBuilder13ConnectBlocksEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler10CFGBuilder5QueueEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"Create block id:%d for #%d:%s\n"
	.section	.text._ZN2v88internal8compiler10CFGBuilder5QueueEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler10CFGBuilder5QueueEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler10CFGBuilder5QueueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler10CFGBuilder5QueueEPNS1_4NodeE, @function
_ZN2v88internal8compiler10CFGBuilder5QueueEPNS1_4NodeE:
.LFB10897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movl	24(%rdi), %eax
	cmpl	%eax, 16(%rsi)
	ja	.L1268
	movq	(%rsi), %rcx
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movzwl	16(%rcx), %eax
	cmpw	$49, %ax
	ja	.L1270
	leaq	.L1273(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler10CFGBuilder5QueueEPNS1_4NodeE,"aG",@progbits,_ZN2v88internal8compiler10CFGBuilder5QueueEPNS1_4NodeE,comdat
	.align 4
	.align 4
.L1273:
	.long	.L1278-.L1273
	.long	.L1276-.L1273
	.long	.L1277-.L1273
	.long	.L1277-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1276-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1275-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1274-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1271-.L1273
	.long	.L1272-.L1273
	.section	.text._ZN2v88internal8compiler10CFGBuilder5QueueEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler10CFGBuilder5QueueEPNS1_4NodeE,comdat
.L1278:
	movq	16(%rdi), %rdi
	movq	104(%rdi), %rsi
.L1307:
	movq	%r12, %rdx
.L1308:
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	8(%rbx), %rdi
	movl	$2, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
.L1271:
	movq	112(%rbx), %rax
	movq	96(%rbx), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1294
	movq	-56(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 96(%rbx)
.L1295:
	movl	24(%rbx), %eax
	movq	-56(%rbp), %rdx
	addl	$1, %eax
	movl	%eax, 16(%rdx)
	movq	144(%rbx), %rsi
	cmpq	152(%rbx), %rsi
	je	.L1296
	movq	%rdx, (%rsi)
	addq	$8, 144(%rbx)
.L1268:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1270:
	.cfi_restore_state
	subw	$682, %ax
	cmpw	$106, %ax
	ja	.L1271
.L1272:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_@PLT
	testb	%al, %al
	je	.L1271
	movq	(%r12), %rax
	movq	(%rbx), %rdi
	movslq	40(%rax), %r15
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	leaq	0(,%r15,8), %r14
	subq	%r13, %rax
	movq	%r14, %rsi
	cmpq	%rax, %r14
	ja	.L1309
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1290:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m@PLT
	testq	%r15, %r15
	je	.L1271
	addq	%r13, %r14
	leaq	.LC26(%rip), %r15
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1291:
	addq	$8, %r13
	cmpq	%r13, %r14
	je	.L1271
.L1293:
	movq	0(%r13), %r12
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	jne	.L1291
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, %r8
	jne	.L1310
.L1292:
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	8(%rbx), %rdi
	movl	$2, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	jmp	.L1291
.L1276:
	movq	16(%rdi), %rdi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	jne	.L1271
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, %r13
	je	.L1281
	movq	(%r12), %rax
	movl	20(%r12), %edx
	leaq	.LC26(%rip), %rdi
	movl	160(%r13), %esi
	movq	8(%rax), %rcx
	andl	$16777215, %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1281
.L1277:
	movq	(%rdi), %rdi
	movslq	40(%rcx), %r15
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	leaq	0(,%r15,8), %r14
	movq	%r14, %rsi
	subq	%r13, %rax
	cmpq	%rax, %r14
	ja	.L1311
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1284:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m@PLT
	testq	%r15, %r15
	je	.L1271
	addq	%r13, %r14
	leaq	.LC26(%rip), %r15
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1286:
	addq	$8, %r13
	cmpq	%r13, %r14
	je	.L1271
.L1288:
	movq	0(%r13), %r12
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	jne	.L1286
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, %r8
	jne	.L1312
.L1287:
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	8(%rbx), %rdi
	movl	$2, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	jmp	.L1286
.L1275:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1313
.L1281:
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	jmp	.L1308
.L1274:
	movq	16(%rdi), %rdi
	movq	112(%rdi), %rsi
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1294:
	leaq	-56(%rbp), %rsi
	leaq	32(%rbx), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1296:
	leaq	-56(%rbp), %rdx
	leaq	128(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1312:
	.cfi_restore_state
	movq	(%r12), %rax
	movl	20(%r12), %edx
	movq	%r15, %rdi
	movq	%r8, -64(%rbp)
	movl	160(%r8), %esi
	movq	8(%rax), %rcx
	andl	$16777215, %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-64(%rbp), %r8
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1313:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, %r13
	jne	.L1314
.L1282:
	movq	16(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	8(%rbx), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1310:
	movq	(%r12), %rax
	movl	20(%r12), %edx
	movq	%r15, %rdi
	movq	%r8, -64(%rbp)
	movl	160(%r8), %esi
	movq	8(%rax), %rcx
	andl	$16777215, %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-64(%rbp), %r8
	jmp	.L1292
.L1311:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1284
.L1314:
	movq	(%r14), %rax
	movl	20(%r14), %edx
	leaq	.LC26(%rip), %rdi
	movl	160(%r13), %esi
	movq	8(%rax), %rcx
	andl	$16777215, %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1282
.L1309:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1290
	.cfi_endproc
.LFE10897:
	.size	_ZN2v88internal8compiler10CFGBuilder5QueueEPNS1_4NodeE, .-_ZN2v88internal8compiler10CFGBuilder5QueueEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler24ScheduleEarlyNodeVisitor3RunEPNS0_10ZoneVectorIPNS1_4NodeEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"Fixing #%d:%s minimum_block = id:%d, dominator_depth = %d\n"
	.section	.text._ZN2v88internal8compiler24ScheduleEarlyNodeVisitor3RunEPNS0_10ZoneVectorIPNS1_4NodeEEE,"axG",@progbits,_ZN2v88internal8compiler24ScheduleEarlyNodeVisitor3RunEPNS0_10ZoneVectorIPNS1_4NodeEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler24ScheduleEarlyNodeVisitor3RunEPNS0_10ZoneVectorIPNS1_4NodeEEE
	.type	_ZN2v88internal8compiler24ScheduleEarlyNodeVisitor3RunEPNS0_10ZoneVectorIPNS1_4NodeEEE, @function
_ZN2v88internal8compiler24ScheduleEarlyNodeVisitor3RunEPNS0_10ZoneVectorIPNS1_4NodeEEE:
.LFB10982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movq	%rax, -152(%rbp)
	cmpq	%rax, %rdx
	je	.L1315
	leaq	-120(%rbp), %rcx
	leaq	16(%rdi), %rax
	movq	%rdx, -136(%rbp)
	movq	%rdi, %rbx
	movq	%rcx, -176(%rbp)
	leaq	-112(%rbp), %rcx
	movq	%rcx, -160(%rbp)
	leaq	-104(%rbp), %rcx
	movq	%rcx, -184(%rbp)
	movq	%rax, -144(%rbp)
	movq	80(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L1381:
	movq	-136(%rbp), %rcx
	movq	96(%rbx), %rsi
	movq	(%rcx), %rcx
	leaq	-8(%rsi), %rdx
	movq	%rcx, -120(%rbp)
	cmpq	%rdx, %rax
	je	.L1317
	movq	%rcx, (%rax)
	movq	80(%rbx), %rax
	addq	$8, %rax
	movq	%rax, 80(%rbx)
.L1320:
	cmpq	%rax, 48(%rbx)
	je	.L1319
	.p2align 4,,10
	.p2align 3
.L1318:
	movq	(%rbx), %rax
	movq	248(%rax), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	48(%rbx), %rax
	movq	(%rax), %r12
	movq	(%rbx), %rax
	movl	20(%r12), %r13d
	andl	$16777215, %r13d
	salq	$4, %r13
	addq	200(%rax), %r13
	cmpl	$2, 12(%r13)
	je	.L1321
.L1400:
	movq	0(%r13), %rax
.L1322:
	movq	8(%rbx), %rdx
	cmpq	%rax, 104(%rdx)
	je	.L1326
	movq	24(%r12), %r14
	testq	%r14, %r14
	jne	.L1377
	jmp	.L1326
	.p2align 4,,10
	.p2align 3
.L1328:
	movq	(%r14), %r14
	testq	%r14, %r14
	je	.L1326
.L1377:
	movl	16(%r14), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r14,%rax,8), %rdi
	jne	.L1327
	movq	(%rdi), %rdi
.L1327:
	movl	20(%rdi), %r12d
	movq	(%rbx), %rax
	andl	$16777215, %r12d
	salq	$4, %r12
	addq	200(%rax), %r12
	movl	12(%r12), %eax
	testl	%eax, %eax
	je	.L1328
	movq	0(%r13), %r15
	movq	%rdi, -112(%rbp)
	cmpl	$2, %eax
	je	.L1328
	cmpl	$3, %eax
	je	.L1331
.L1406:
	movl	12(%r15), %eax
.L1332:
	movq	(%r12), %rdx
	cmpl	%eax, 12(%rdx)
	jge	.L1328
	movq	%r15, (%r12)
	movq	96(%rbx), %rax
	movq	80(%rbx), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1375
	movq	-112(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 80(%rbx)
.L1376:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L1328
	movq	-112(%rbp), %rdx
	movq	(%r12), %rax
	leaq	.LC20(%rip), %rdi
	movq	(%rdx), %rsi
	movq	160(%rax), %rcx
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	8(%rsi), %r9
	movl	20(%rdx), %esi
	movq	%r9, %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.L1377
	.p2align 4,,10
	.p2align 3
.L1326:
	movq	64(%rbx), %rcx
	movq	48(%rbx), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L1407
	addq	$8, %rax
	movq	%rax, 48(%rbx)
.L1378:
	cmpq	%rax, 80(%rbx)
	jne	.L1318
.L1319:
	addq	$8, -136(%rbp)
	movq	-136(%rbp), %rsi
	cmpq	%rsi, -152(%rbp)
	jne	.L1381
.L1315:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1408
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1331:
	.cfi_restore_state
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	movq	(%rbx), %rax
	movl	20(%rdi), %r9d
	andl	$16777215, %r9d
	salq	$4, %r9
	addq	200(%rax), %r9
	movl	12(%r9), %eax
	cmpl	$2, %eax
	je	.L1406
	cmpl	$3, %eax
	je	.L1335
.L1405:
	movl	12(%r15), %eax
.L1336:
	movq	(%r9), %rdx
	cmpl	%eax, 12(%rdx)
	jge	.L1332
	movq	%r15, (%r9)
	movq	96(%rbx), %rax
	movq	80(%rbx), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1371
	movq	-104(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 80(%rbx)
.L1372:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L1406
	movq	-104(%rbp), %rdx
	movq	(%r9), %rax
	leaq	.LC20(%rip), %rdi
	movq	(%rdx), %rsi
	movq	160(%rax), %rcx
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	8(%rsi), %r9
	movl	20(%rdx), %esi
	movq	%r9, %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1406
	.p2align 4,,10
	.p2align 3
.L1321:
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, 0(%r13)
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L1322
	movq	(%r12), %rdx
	movl	20(%r12), %esi
	leaq	.LC27(%rip), %rdi
	movq	160(%rax), %rcx
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	8(%rdx), %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1400
	.p2align 4,,10
	.p2align 3
.L1407:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L1379
	cmpq	$64, 8(%rax)
	ja	.L1380
.L1379:
	movq	56(%rbx), %rax
	movq	$64, 8(%rax)
	movq	24(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%rbx)
.L1380:
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	jmp	.L1378
	.p2align 4,,10
	.p2align 3
.L1375:
	movq	-160(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L1376
	.p2align 4,,10
	.p2align 3
.L1335:
	xorl	%esi, %esi
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-168(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movl	20(%rdi), %r10d
	andl	$16777215, %r10d
	salq	$4, %r10
	addq	200(%rax), %r10
	movl	12(%r10), %eax
	cmpl	$2, %eax
	je	.L1405
	cmpl	$3, %eax
	je	.L1339
.L1404:
	movl	12(%r15), %eax
.L1340:
	movq	(%r10), %rdx
	cmpl	%eax, 12(%rdx)
	jge	.L1336
	movq	%r15, (%r10)
	movq	96(%rbx), %rax
	movq	80(%rbx), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1368
	movq	-96(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 80(%rbx)
.L1369:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L1405
	movq	-96(%rbp), %rdx
	movq	(%r10), %rax
	leaq	.LC20(%rip), %rdi
	movq	%r9, -168(%rbp)
	movq	(%rdx), %rsi
	movq	160(%rax), %rcx
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	8(%rsi), %r10
	movl	20(%rdx), %esi
	movq	%r10, %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	12(%r15), %eax
	movq	-168(%rbp), %r9
	jmp	.L1336
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	-176(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	80(%rbx), %rax
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1371:
	movq	-184(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	%r9, -168(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	-168(%rbp), %r9
	jmp	.L1372
.L1339:
	xorl	%esi, %esi
	movq	%r10, -192(%rbp)
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-168(%rbp), %r9
	movq	-192(%rbp), %r10
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	movq	(%rbx), %rax
	movl	20(%rdi), %r11d
	andl	$16777215, %r11d
	salq	$4, %r11
	addq	200(%rax), %r11
	movl	12(%r11), %eax
	cmpl	$2, %eax
	je	.L1404
	cmpl	$3, %eax
	je	.L1343
.L1403:
	movl	12(%r15), %eax
.L1344:
	movq	(%r11), %rdx
	cmpl	%eax, 12(%rdx)
	jge	.L1340
	movq	%r15, (%r11)
	movq	96(%rbx), %rax
	movq	80(%rbx), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1365
	movq	-88(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 80(%rbx)
.L1366:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L1404
	movq	-88(%rbp), %rdx
	movq	(%r11), %rax
	leaq	.LC20(%rip), %rdi
	movq	%r10, -192(%rbp)
	movq	%r9, -168(%rbp)
	movq	(%rdx), %rsi
	movq	160(%rax), %rcx
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	8(%rsi), %r11
	movl	20(%rdx), %esi
	movq	%r11, %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	12(%r15), %eax
	movq	-192(%rbp), %r10
	movq	-168(%rbp), %r9
	jmp	.L1340
.L1368:
	movq	-144(%rbp), %rdi
	leaq	-96(%rbp), %rsi
	movq	%r10, -192(%rbp)
	movq	%r9, -168(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	-192(%rbp), %r10
	movq	-168(%rbp), %r9
	jmp	.L1369
.L1343:
	xorl	%esi, %esi
	movq	%r11, -208(%rbp)
	movq	%r10, -200(%rbp)
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rbx), %rdx
	movq	-192(%rbp), %r9
	movq	%rax, -80(%rbp)
	movq	%rax, %rdi
	movl	20(%rax), %eax
	movq	-200(%rbp), %r10
	movq	-208(%rbp), %r11
	andl	$16777215, %eax
	salq	$4, %rax
	addq	200(%rdx), %rax
	movq	%rax, -168(%rbp)
	movl	12(%rax), %eax
	cmpl	$2, %eax
	je	.L1403
	cmpl	$3, %eax
	je	.L1347
.L1402:
	movl	12(%r15), %eax
.L1348:
	movq	-168(%rbp), %rcx
	movq	(%rcx), %rdx
	cmpl	%eax, 12(%rdx)
	jge	.L1344
	movq	%r15, (%rcx)
	movq	96(%rbx), %rax
	movq	80(%rbx), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1362
	movq	-80(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 80(%rbx)
.L1363:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L1403
	movq	-80(%rbp), %rdx
	movq	-168(%rbp), %rax
	movq	%r11, -208(%rbp)
	movq	%r10, -200(%rbp)
	movq	(%rdx), %rsi
	movq	(%rax), %rax
	movq	%r9, -192(%rbp)
	movq	8(%rsi), %rdi
	movl	20(%rdx), %esi
	movq	160(%rax), %rcx
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	%rdi, %rdx
	andl	$16777215, %esi
	leaq	.LC20(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	12(%r15), %eax
	movq	-208(%rbp), %r11
	movq	-200(%rbp), %r10
	movq	-192(%rbp), %r9
	jmp	.L1344
.L1365:
	movq	-144(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	movq	%r11, -200(%rbp)
	movq	%r10, -192(%rbp)
	movq	%r9, -168(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	-200(%rbp), %r11
	movq	-192(%rbp), %r10
	movq	-168(%rbp), %r9
	jmp	.L1366
.L1347:
	xorl	%esi, %esi
	movq	%r11, -216(%rbp)
	movq	%r10, -208(%rbp)
	movq	%r9, -200(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rbx), %rdx
	movq	-200(%rbp), %r9
	movq	%rax, -72(%rbp)
	movq	%rax, %rdi
	movl	20(%rax), %eax
	movq	-208(%rbp), %r10
	movq	-216(%rbp), %r11
	andl	$16777215, %eax
	salq	$4, %rax
	addq	200(%rdx), %rax
	movq	%rax, -192(%rbp)
	movl	12(%rax), %eax
	cmpl	$2, %eax
	je	.L1402
	cmpl	$3, %eax
	je	.L1351
.L1401:
	movl	12(%r15), %eax
.L1352:
	movq	-192(%rbp), %rcx
	movq	(%rcx), %rdx
	cmpl	%eax, 12(%rdx)
	jge	.L1348
	movq	%r15, (%rcx)
	movq	96(%rbx), %rax
	movq	80(%rbx), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1359
	movq	-72(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 80(%rbx)
.L1360:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L1402
	movq	-72(%rbp), %rdx
	movq	-192(%rbp), %rax
	movq	%r11, -216(%rbp)
	movq	%r10, -208(%rbp)
	movq	(%rdx), %rsi
	movq	(%rax), %rax
	movq	%r9, -200(%rbp)
	movq	8(%rsi), %rdi
	movl	20(%rdx), %esi
	movq	160(%rax), %rcx
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	%rdi, %rdx
	andl	$16777215, %esi
	leaq	.LC20(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	12(%r15), %eax
	movq	-216(%rbp), %r11
	movq	-208(%rbp), %r10
	movq	-200(%rbp), %r9
	jmp	.L1348
.L1362:
	movq	-144(%rbp), %rdi
	leaq	-80(%rbp), %rsi
	movq	%r11, -208(%rbp)
	movq	%r10, -200(%rbp)
	movq	%r9, -192(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	-208(%rbp), %r11
	movq	-200(%rbp), %r10
	movq	-192(%rbp), %r9
	jmp	.L1363
.L1408:
	call	__stack_chk_fail@PLT
.L1359:
	movq	-144(%rbp), %rdi
	leaq	-72(%rbp), %rsi
	movq	%r11, -216(%rbp)
	movq	%r10, -208(%rbp)
	movq	%r9, -200(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	-216(%rbp), %r11
	movq	-208(%rbp), %r10
	movq	-200(%rbp), %r9
	jmp	.L1360
.L1351:
	xorl	%esi, %esi
	movq	%r11, -216(%rbp)
	movq	%r10, -208(%rbp)
	movq	%r9, -200(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-200(%rbp), %r9
	movq	-208(%rbp), %r10
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	movq	(%rbx), %rax
	movl	20(%rdi), %ecx
	movq	-216(%rbp), %r11
	andl	$16777215, %ecx
	salq	$4, %rcx
	addq	200(%rax), %rcx
	movl	12(%rcx), %eax
	cmpl	$2, %eax
	je	.L1401
	cmpl	$3, %eax
	je	.L1409
.L1355:
	movq	(%rcx), %rdx
	movl	12(%r15), %eax
	cmpl	12(%rdx), %eax
	jle	.L1352
	movq	%r15, (%rcx)
	movq	96(%rbx), %rax
	movq	80(%rbx), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1356
	movq	-64(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 80(%rbx)
.L1357:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L1401
	movq	-64(%rbp), %rdx
	movq	(%rcx), %rax
	movq	%r11, -216(%rbp)
	movq	%r10, -208(%rbp)
	movq	(%rdx), %rsi
	movq	160(%rax), %rcx
	movq	%r9, -200(%rbp)
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	8(%rsi), %rdi
	movl	20(%rdx), %esi
	movq	%rdi, %rdx
	andl	$16777215, %esi
	leaq	.LC20(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	12(%r15), %eax
	movq	-216(%rbp), %r11
	movq	-208(%rbp), %r10
	movq	-200(%rbp), %r9
	jmp	.L1352
.L1409:
	xorl	%esi, %esi
	movq	%rcx, -224(%rbp)
	movq	%r11, -216(%rbp)
	movq	%r10, -208(%rbp)
	movq	%r9, -200(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler24ScheduleEarlyNodeVisitor30PropagateMinimumPositionToNodeEPNS1_10BasicBlockEPNS1_4NodeE
	movq	-224(%rbp), %rcx
	movq	-216(%rbp), %r11
	movq	-208(%rbp), %r10
	movq	-200(%rbp), %r9
	jmp	.L1355
.L1356:
	movq	-144(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	movq	%rcx, -224(%rbp)
	movq	%r11, -216(%rbp)
	movq	%r10, -208(%rbp)
	movq	%r9, -200(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	-224(%rbp), %rcx
	movq	-216(%rbp), %r11
	movq	-208(%rbp), %r10
	movq	-200(%rbp), %r9
	jmp	.L1357
	.cfi_endproc
.LFE10982:
	.size	_ZN2v88internal8compiler24ScheduleEarlyNodeVisitor3RunEPNS0_10ZoneVectorIPNS1_4NodeEEE, .-_ZN2v88internal8compiler24ScheduleEarlyNodeVisitor3RunEPNS0_10ZoneVectorIPNS1_4NodeEEE
	.section	.text._ZNSt5dequeIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_:
.LFB13152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	80(%rdi), %rcx
	movq	64(%rdi), %rax
	leaq	-16(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L1411
	movdqu	(%rsi), %xmm1
	movups	%xmm1, (%rax)
	addq	$16, 64(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1411:
	.cfi_restore_state
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	subq	72(%rdi), %rax
	movq	%r13, %r14
	sarq	$4, %rax
	subq	%rsi, %r14
	movq	%r14, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$5, %rdx
	addq	%rax, %rdx
	movq	48(%rdi), %rax
	subq	32(%rdi), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$134217727, %rax
	je	.L1430
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rdi
	movq	%r13, %rax
	subq	%rdi, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L1431
.L1414:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L1422
	cmpq	$31, 8(%rax)
	ja	.L1432
.L1422:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1433
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1423:
	movq	%rax, 8(%r13)
	movdqu	(%r12), %xmm0
	movq	64(%rbx), %rax
	movups	%xmm0, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1431:
	.cfi_restore_state
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L1434
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L1435
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1419:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1420
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L1420:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L1421
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L1421:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L1417:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1434:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rdi,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L1416
	cmpq	%r13, %rsi
	je	.L1417
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1432:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L1423
	.p2align 4,,10
	.p2align 3
.L1416:
	cmpq	%r13, %rsi
	je	.L1417
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1433:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1423
.L1435:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L1419
.L1430:
	leaq	.LC19(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13152:
	.size	_ZNSt5dequeIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	.section	.rodata._ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb.str1.1,"aMS",@progbits,1
.LC28:
	.string	"vector<bool>::_M_fill_insert"
	.section	.text._ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb,"axG",@progbits,_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb
	.type	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb, @function
_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb:
.LFB13175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	testq	%rcx, %rcx
	je	.L1436
	movq	%rdi, %rbx
	movq	8(%rdi), %rax
	movq	%rsi, -80(%rbp)
	movq	%rsi, %r12
	movq	24(%rbx), %r9
	movq	40(%rbx), %rsi
	movq	%rcx, %r13
	movl	32(%rdi), %edx
	movl	-56(%rbp), %r14d
	movq	%r9, %rcx
	subq	%rax, %rsi
	subq	%rax, %rcx
	movq	%rsi, %rax
	movq	%rdx, %rdi
	leaq	(%rdx,%rcx,8), %rcx
	salq	$3, %rax
	subq	%rcx, %rax
	cmpq	%r13, %rax
	jb	.L1438
	leaq	0(%r13,%rdx), %rax
	testq	%rax, %rax
	leaq	63(%rax), %rcx
	cmovns	%rax, %rcx
	sarq	$6, %rcx
	leaq	(%r9,%rcx,8), %r8
	movq	%rax, %rcx
	sarq	$63, %rcx
	shrq	$58, %rcx
	addq	%rcx, %rax
	andl	$63, %eax
	subq	%rcx, %rax
	jns	.L1439
	addq	$64, %rax
	subq	$8, %r8
.L1439:
	movabsq	$-9223372036854775808, %r15
	movq	%r9, %rcx
	movl	%r14d, %r11d
	subq	%r12, %rcx
	movq	%r11, -88(%rbp)
	leaq	(%rdx,%rcx,8), %rsi
	subq	%r11, %rsi
	movl	$1, %r11d
	testq	%rsi, %rsi
	jle	.L1448
	.p2align 4,,10
	.p2align 3
.L1440:
	testl	%edi, %edi
	je	.L1443
.L1520:
	subl	$1, %edi
	movq	%r11, %r10
	movl	%edi, %ecx
	salq	%cl, %r10
	testl	%eax, %eax
	je	.L1445
.L1521:
	subl	$1, %eax
	movq	%r11, %rdx
	movl	%eax, %ecx
	salq	%cl, %rdx
.L1446:
	movq	(%r8), %rcx
	testq	%r10, (%r9)
	je	.L1447
	orq	%rcx, %rdx
	movq	%rdx, (%r8)
	subq	$1, %rsi
	jne	.L1440
.L1448:
	movq	-88(%rbp), %rax
	addq	%r13, %rax
	leaq	63(%rax), %rdx
	cmovns	%rax, %rdx
	sarq	$6, %rdx
	leaq	(%r12,%rdx,8), %r8
	cqto
	shrq	$58, %rdx
	leaq	(%rax,%rdx), %r15
	andl	$63, %r15d
	subq	%rdx, %r15
	jns	.L1442
	addq	$64, %r15
	subq	$8, %r8
.L1442:
	movl	%r15d, %r9d
	cmpq	%r12, %r8
	je	.L1450
	testl	%r14d, %r14d
	jne	.L1514
	movq	%r8, %r10
	subq	%r12, %r10
	cmpb	$0, -72(%rbp)
	je	.L1454
.L1453:
	movq	-80(%rbp), %rdi
	movq	%r10, %rdx
	movl	$-1, %esi
	movl	%r9d, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	memset@PLT
	testq	%r15, %r15
	movq	-72(%rbp), %r8
	movl	-88(%rbp), %r9d
	je	.L1456
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r9d, %ecx
	shrq	%cl, %rax
	orq	%rax, (%r8)
	.p2align 4,,10
	.p2align 3
.L1456:
	movl	32(%rbx), %ecx
	movq	24(%rbx), %rdx
	addq	%rcx, %r13
	leaq	63(%r13), %rax
	cmovns	%r13, %rax
	sarq	$6, %rax
	leaq	(%rdx,%rax,8), %rdx
	movq	%r13, %rax
	sarq	$63, %rax
	shrq	$58, %rax
	addq	%rax, %r13
	andl	$63, %r13d
	subq	%rax, %r13
	jns	.L1511
	addq	$64, %r13
	subq	$8, %rdx
.L1511:
	movq	%rdx, 24(%rbx)
.L1512:
	movl	%r13d, 32(%rbx)
.L1436:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1438:
	.cfi_restore_state
	movabsq	$17179869120, %rax
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %r13
	ja	.L1515
	cmpq	%rcx, %r13
	movq	%rcx, %rdx
	cmovnb	%r13, %rdx
	addq	%rdx, %rcx
	jc	.L1464
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	addq	$63, %rcx
	shrq	$6, %rcx
	leaq	0(,%rcx,8), %rax
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
.L1465:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L1516
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L1467:
	movq	8(%rbx), %rsi
	movq	%r12, %rdx
	subq	%rsi, %rdx
	cmpq	%r12, %rsi
	je	.L1468
	movq	%r15, %rdi
	movq	%rdx, -96(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %rdx
.L1468:
	movl	%r14d, %esi
	leaq	(%r15,%rdx), %rdi
	movq	%rsi, -96(%rbp)
	testq	%rsi, %rsi
	je	.L1496
	xorl	%edx, %edx
	movq	%r12, %r9
	movl	$1, %r10d
	movl	%edx, %ecx
	jmp	.L1474
	.p2align 4,,10
	.p2align 3
.L1472:
	addl	$1, %ecx
	subq	$1, %rsi
	je	.L1517
.L1474:
	movq	%r10, %rdx
	movq	(%rdi), %r11
	salq	%cl, %rdx
	movq	%rdx, %rax
	movq	%r11, %r8
	notq	%rax
	orq	%rdx, %r8
	andq	%r11, %rax
	testq	%rdx, (%r9)
	cmovne	%r8, %rax
	movq	%rax, (%rdi)
	cmpl	$63, %ecx
	jne	.L1472
	addq	$8, %r9
	addq	$8, %rdi
	xorl	%ecx, %ecx
	subq	$1, %rsi
	jne	.L1474
.L1517:
	movl	%ecx, %ecx
	movq	%rcx, %rdx
.L1469:
	addq	%rcx, %r13
	leaq	63(%r13), %rax
	cmovns	%r13, %rax
	sarq	$6, %rax
	leaq	(%rdi,%rax,8), %r8
	movq	%r13, %rax
	sarq	$63, %rax
	shrq	$58, %rax
	leaq	0(%r13,%rax), %r9
	andl	$63, %r9d
	subq	%rax, %r9
	jns	.L1475
	addq	$64, %r9
	subq	$8, %r8
.L1475:
	movl	%r9d, %r13d
	cmpq	%rdi, %r8
	je	.L1476
	testl	%edx, %edx
	je	.L1477
	movl	%edx, %ecx
	leaq	8(%rdi), %rsi
	movq	$-1, %rax
	movq	%r8, %rdx
	salq	%cl, %rax
	subq	%rsi, %rdx
	cmpb	$0, -72(%rbp)
	movq	(%rdi), %rcx
	je	.L1478
	orq	%rcx, %rax
	movq	%rax, (%rdi)
	movq	%rsi, %rdi
.L1479:
	movl	$-1, %esi
	movq	%r8, -104(%rbp)
	movq	%r9, -72(%rbp)
	call	memset@PLT
	movq	-72(%rbp), %r9
	movq	-104(%rbp), %r8
	testq	%r9, %r9
	jne	.L1518
	.p2align 4,,10
	.p2align 3
.L1482:
	movq	24(%rbx), %rax
	movl	32(%rbx), %edx
	subq	%r12, %rax
	leaq	(%rdx,%rax,8), %rdx
	subq	-96(%rbp), %rdx
	testq	%rdx, %rdx
	jle	.L1486
	movq	-80(%rbp), %r9
	movl	$1, %edi
	jmp	.L1493
	.p2align 4,,10
	.p2align 3
.L1489:
	addl	$1, %r14d
	cmpl	$63, %r13d
	je	.L1519
.L1491:
	addl	$1, %r13d
	subq	$1, %rdx
	je	.L1486
.L1493:
	movq	(%r8), %rsi
	movl	%r13d, %ecx
	movq	%rdi, %rax
	movq	%rdi, %r10
	salq	%cl, %rax
	movl	%r14d, %ecx
	salq	%cl, %r10
	movq	%rsi, %rcx
	orq	%rax, %rcx
	notq	%rax
	andq	%rsi, %rax
	testq	%r10, (%r9)
	cmovne	%rcx, %rax
	movq	%rax, (%r8)
	cmpl	$63, %r14d
	jne	.L1489
	addq	$8, %r9
	xorl	%r14d, %r14d
	cmpl	$63, %r13d
	jne	.L1491
.L1519:
	addq	$8, %r8
	xorl	%r13d, %r13d
	subq	$1, %rdx
	jne	.L1493
.L1486:
	cmpq	$0, 8(%rbx)
	je	.L1494
	movq	$0, 24(%rbx)
	movl	$0, 32(%rbx)
	movq	$0, 8(%rbx)
	movl	$0, 16(%rbx)
.L1494:
	movq	-88(%rbp), %rax
	movq	%r15, 8(%rbx)
	movl	$0, 16(%rbx)
	addq	%r15, %rax
	movq	%r8, 24(%rbx)
	movq	%rax, 40(%rbx)
	jmp	.L1512
	.p2align 4,,10
	.p2align 3
.L1447:
	notq	%rdx
	andq	%rcx, %rdx
	movq	%rdx, (%r8)
	subq	$1, %rsi
	je	.L1448
	testl	%edi, %edi
	jne	.L1520
.L1443:
	subq	$8, %r9
	movq	%r15, %r10
	movl	$63, %edi
	testl	%eax, %eax
	jne	.L1521
.L1445:
	subq	$8, %r8
	movq	%r15, %rdx
	movl	$63, %eax
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1514:
	movl	%r14d, %ecx
	leaq	8(%r12), %rdx
	movq	$-1, %rax
	movq	%r8, %r10
	salq	%cl, %rax
	subq	%rdx, %r10
	cmpb	$0, -72(%rbp)
	movq	(%r12), %rcx
	je	.L1452
	orq	%rcx, %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, (%r12)
	jmp	.L1453
	.p2align 4,,10
	.p2align 3
.L1452:
	notq	%rax
	movq	%rdx, -80(%rbp)
	andq	%rcx, %rax
	movq	%rax, (%r12)
.L1454:
	movq	-80(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r10, %rdx
	movl	%r9d, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	memset@PLT
	testq	%r15, %r15
	movq	-72(%rbp), %r8
	movl	-88(%rbp), %r9d
	je	.L1456
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r9d, %ecx
	shrq	%cl, %rax
	notq	%rax
	andq	%rax, (%r8)
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1450:
	cmpl	%r14d, %r15d
	je	.L1456
	movq	$-1, %rdx
	movl	$64, %ecx
	subl	%r15d, %ecx
	movq	%rdx, %rax
	shrq	%cl, %rax
	movl	%r14d, %ecx
	salq	%cl, %rdx
	movq	(%r8), %rcx
	andq	%rdx, %rax
	movq	%rax, %rdx
	orq	%rcx, %rax
	notq	%rdx
	andq	%rcx, %rdx
	cmpb	$0, -72(%rbp)
	cmove	%rdx, %rax
	movq	%rax, (%r8)
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1477:
	movq	%r8, %rdx
	subq	%rdi, %rdx
	cmpb	$0, -72(%rbp)
	jne	.L1479
.L1480:
	xorl	%esi, %esi
	movq	%r8, -104(%rbp)
	movq	%r9, -72(%rbp)
	call	memset@PLT
	movq	-72(%rbp), %r9
	movq	-104(%rbp), %r8
	testq	%r9, %r9
	je	.L1482
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r13d, %ecx
	shrq	%cl, %rax
	notq	%rax
	andq	%rax, (%r8)
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1476:
	cmpl	%edx, %r9d
	je	.L1482
	movq	$-1, %rsi
	movl	$64, %ecx
	subl	%r9d, %ecx
	movq	%rsi, %rax
	shrq	%cl, %rax
	movl	%edx, %ecx
	salq	%cl, %rsi
	movq	(%r8), %rcx
	andq	%rsi, %rax
	movq	%rax, %rdx
	orq	%rcx, %rax
	notq	%rdx
	andq	%rcx, %rdx
	cmpb	$0, -72(%rbp)
	cmove	%rdx, %rax
	movq	%rax, (%r8)
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1518:
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r13d, %ecx
	shrq	%cl, %rax
	orq	%rax, (%r8)
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1478:
	notq	%rax
	andq	%rcx, %rax
	movq	%rax, (%rdi)
	movq	%rsi, %rdi
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1516:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1496:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.L1469
.L1515:
	leaq	.LC28(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1464:
	movq	$2147483640, -88(%rbp)
	movl	$2147483640, %esi
	jmp	.L1465
	.cfi_endproc
.LFE13175:
	.size	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb, .-_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS9_ERS6_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS9_ERS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS9_ERS6_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS9_ERS6_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS9_ERS6_:
.LFB13188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r15
	je	.L1572
	movq	(%rdx), %r14
	cmpq	%r14, 32(%rsi)
	jbe	.L1533
	movq	32(%rdi), %rbx
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rsi, %rbx
	je	.L1525
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpq	%r14, 32(%rax)
	jnb	.L1535
	movl	$0, %ebx
	cmpq	$0, 24(%rax)
	movq	%rbx, %rax
	cmovne	%r12, %rdx
	cmovne	%r12, %rax
.L1525:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1533:
	.cfi_restore_state
	jnb	.L1544
	movq	40(%rdi), %rdx
	cmpq	%rsi, %rdx
	je	.L1571
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpq	%r14, 32(%rax)
	jbe	.L1546
	movl	$0, %ebx
	cmpq	$0, 24(%r12)
	movq	%rbx, %rax
	cmovne	%rdx, %rax
	cmove	%r12, %rdx
	jmp	.L1525
	.p2align 4,,10
	.p2align 3
.L1572:
	cmpq	$0, 48(%rdi)
	je	.L1524
	movq	40(%rdi), %rdx
	movq	32(%rdx), %rax
	cmpq	%rax, (%r14)
	ja	.L1571
.L1524:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L1555
	movq	(%r14), %rsi
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1573:
	movq	16(%rbx), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L1528
.L1574:
	movq	%rax, %rbx
.L1527:
	movq	32(%rbx), %rdx
	cmpq	%rdx, %rsi
	jb	.L1573
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L1574
.L1528:
	movq	%rbx, %r12
	testb	%cl, %cl
	jne	.L1526
.L1531:
	xorl	%eax, %eax
	cmpq	%rdx, %rsi
	cmova	%rax, %rbx
	cmovbe	%rax, %r12
.L1532:
	addq	$8, %rsp
	movq	%rbx, %rax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1544:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1571:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1535:
	.cfi_restore_state
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L1538
	jmp	.L1575
	.p2align 4,,10
	.p2align 3
.L1576:
	movq	16(%r12), %rax
	movl	$1, %esi
.L1541:
	testq	%rax, %rax
	je	.L1539
	movq	%rax, %r12
.L1538:
	movq	32(%r12), %rcx
	cmpq	%rcx, %r14
	jb	.L1576
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L1541
	.p2align 4,,10
	.p2align 3
.L1555:
	movq	%r12, %rbx
.L1526:
	cmpq	%rbx, 32(%r13)
	je	.L1557
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r14), %rsi
	movq	32(%rax), %rdx
	movq	%rax, %rbx
	jmp	.L1531
	.p2align 4,,10
	.p2align 3
.L1539:
	movq	%r12, %rdx
	testb	%sil, %sil
	jne	.L1537
.L1542:
	xorl	%eax, %eax
	cmpq	%rcx, %r14
	cmova	%rax, %r12
	cmovbe	%rax, %rdx
.L1543:
	movq	%r12, %rax
	jmp	.L1525
.L1575:
	movq	%r15, %r12
.L1537:
	cmpq	%r12, %rbx
	je	.L1561
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdx
	movq	32(%rax), %rcx
	movq	%rax, %r12
	jmp	.L1542
	.p2align 4,,10
	.p2align 3
.L1546:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L1549
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1578:
	movq	16(%rbx), %rax
	movl	$1, %esi
.L1552:
	testq	%rax, %rax
	je	.L1550
	movq	%rax, %rbx
.L1549:
	movq	32(%rbx), %rcx
	cmpq	%rcx, %r14
	jb	.L1578
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	jmp	.L1552
	.p2align 4,,10
	.p2align 3
.L1550:
	movq	%rbx, %rdx
	testb	%sil, %sil
	jne	.L1548
.L1553:
	xorl	%eax, %eax
	cmpq	%rcx, %r14
	cmova	%rax, %rbx
	cmovbe	%rax, %rdx
.L1554:
	movq	%rbx, %rax
	jmp	.L1525
	.p2align 4,,10
	.p2align 3
.L1557:
	movq	%rbx, %r12
	xorl	%ebx, %ebx
	jmp	.L1532
.L1577:
	movq	%r15, %rbx
.L1548:
	cmpq	%rbx, 32(%r13)
	je	.L1565
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rdx
	movq	32(%rax), %rcx
	movq	%rax, %rbx
	jmp	.L1553
	.p2align 4,,10
	.p2align 3
.L1561:
	movq	%r12, %rdx
	xorl	%r12d, %r12d
	jmp	.L1543
.L1565:
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	jmp	.L1554
	.cfi_endproc
.LFE13188:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS9_ERS6_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS9_ERS6_
	.section	.rodata._ZN2v88internal8compiler23ScheduleLateNodeVisitor9SplitNodeEPNS1_10BasicBlockEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"  not splitting #%d:%s, it is used in id:%d\n"
	.align 8
.LC30:
	.string	"  not splitting #%d:%s, its common dominator id:%d is perfect\n"
	.align 8
.LC31:
	.string	"  pushing #%d:%s down to id:%d\n"
	.section	.rodata._ZN2v88internal8compiler23ScheduleLateNodeVisitor9SplitNodeEPNS1_10BasicBlockEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC32:
	.string	"clone #%d:%s -> #%d\n"
.LC33:
	.string	"  cloning #%d:%s for id:%d\n"
	.section	.text._ZN2v88internal8compiler23ScheduleLateNodeVisitor9SplitNodeEPNS1_10BasicBlockEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler23ScheduleLateNodeVisitor9SplitNodeEPNS1_10BasicBlockEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler23ScheduleLateNodeVisitor9SplitNodeEPNS1_10BasicBlockEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23ScheduleLateNodeVisitor9SplitNodeEPNS1_10BasicBlockEPNS1_4NodeE, @function
_ZN2v88internal8compiler23ScheduleLateNodeVisitor9SplitNodeEPNS1_10BasicBlockEPNS1_4NodeE:
.LFB11001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -200(%rbp)
	movq	(%rdx), %rdx
	movq	%rsi, -160(%rbp)
	movzbl	18(%rdx), %eax
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	andl	$124, %eax
	cmpb	$124, %al
	jne	.L1579
	cmpw	$55, 16(%rdx)
	je	.L1579
	movq	112(%rsi), %rax
	subq	104(%rsi), %rax
	cmpq	$15, %rax
	jbe	.L1579
	movq	48(%rdi), %r12
	movl	56(%rdi), %ebx
	movq	%rdi, %r15
	movq	32(%rdi), %rdi
	cmpq	%rdi, %r12
	je	.L1583
	movq	%r12, %rdx
	xorl	%esi, %esi
	subq	%rdi, %rdx
	call	memset@PLT
	testl	%ebx, %ebx
	je	.L2170
.L2171:
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%ebx, %ecx
	shrq	%cl, %rax
	notq	%rax
	andq	%rax, (%r12)
.L2170:
	movq	32(%r15), %rdi
	movl	56(%r15), %ebx
	movq	48(%r15), %r12
.L1585:
	movq	16(%r15), %rdx
	movq	%r12, %rsi
	subq	%rdi, %rsi
	movq	24(%rdx), %rax
	subq	16(%rdx), %rax
	movl	%ebx, %edx
	sarq	$3, %rax
	leaq	(%rdx,%rsi,8), %rdx
	leaq	1(%rax), %rcx
	cmpq	%rdx, %rcx
	jnb	.L1586
	addq	$64, %rax
	testq	%rcx, %rcx
	cmovns	%rcx, %rax
	sarq	$6, %rax
	leaq	(%rdi,%rax,8), %rdx
	movq	%rcx, %rax
	sarq	$63, %rax
	shrq	$58, %rax
	addq	%rax, %rcx
	andl	$63, %ecx
	subq	%rax, %rcx
	jns	.L1587
	addq	$64, %rcx
	subq	$8, %rdx
.L1587:
	movq	%rdx, 48(%r15)
	movl	%ecx, 56(%r15)
.L1588:
	movq	-200(%rbp), %rax
	movq	24(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1589
	movq	(%rbx), %rax
	movq	%rax, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L1715:
	movl	16(%rbx), %edx
	movl	%edx, %r8d
	shrl	%r8d
	movl	%r8d, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%rbx,%rcx,8), %r12
	movq	8(%r15), %rcx
	movq	%r12, %rdi
	movq	200(%rcx), %r9
	je	.L1590
	movl	20(%r12), %esi
	leaq	32(%r12,%rax), %r13
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%r9,%rdx), %edx
	testl	%edx, %edx
	je	.L1593
.L1592:
	movq	(%r12), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r9d
	cmpl	$1, %r9d
	jbe	.L2185
	cmpl	$10, %eax
	sete	%r9b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r9b
	je	.L1683
	cmpl	$2, %edx
	je	.L2186
.L1683:
	movq	16(%r15), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1593
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	160(%rax), %rcx
	jne	.L2187
.L1868:
	movq	32(%r15), %rax
	movq	%rcx, %rdx
	shrq	$6, %rdx
	leaq	(%rax,%rdx,8), %rdx
	movl	$1, %eax
	salq	%cl, %rax
	movq	(%rdx), %rcx
	testq	%rcx, %rax
	jne	.L1593
	cmpq	%r14, -160(%rbp)
	je	.L2188
	orq	%rcx, %rax
	movq	%rax, (%rdx)
	movq	136(%r14), %rbx
	movq	144(%r14), %r13
	cmpq	%r13, %rbx
	je	.L1593
	.p2align 4,,10
	.p2align 3
.L1714:
	movq	(%rbx), %r12
	movq	32(%r15), %rax
	movl	$1, %esi
	movq	160(%r12), %rcx
	movq	%rcx, %rdx
	salq	%cl, %rsi
	shrq	$6, %rdx
	testq	%rsi, (%rax,%rdx,8)
	jne	.L1699
	movq	152(%r15), %rdi
	movq	136(%r15), %rax
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L1700
	movq	%r12, (%rax)
	addq	$8, 136(%r15)
.L1699:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L1714
.L1593:
	movq	-152(%rbp), %rax
	testq	%rax, %rax
	je	.L1589
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%rax, -152(%rbp)
	jmp	.L1715
.L2192:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L1579
	movq	-200(%rbp), %rbx
	leaq	.LC30(%rip), %rdi
	movq	(%rbx), %rax
	movl	20(%rbx), %esi
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1579:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2189
	movq	-160(%rbp), %rax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1583:
	.cfi_restore_state
	testl	%ebx, %ebx
	jne	.L2171
	jmp	.L1585
	.p2align 4,,10
	.p2align 3
.L1590:
	movq	(%r12), %r12
	movl	20(%r12), %esi
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%r9,%rdx), %edx
	testl	%edx, %edx
	je	.L1593
	leaq	16(%rdi,%rax), %r13
	jmp	.L1592
	.p2align 4,,10
	.p2align 3
.L1589:
	movq	104(%r15), %rsi
	movl	$1, %ebx
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1720:
	movq	136(%r15), %rax
.L1723:
	cmpq	%rax, %rsi
	je	.L2190
.L1741:
	movq	120(%r15), %rax
	movq	(%rsi), %r9
	subq	$8, %rax
	cmpq	%rax, %rsi
	je	.L1716
	addq	$8, %rsi
	movq	%rsi, 104(%r15)
.L1717:
	movq	160(%r9), %rcx
	movq	32(%r15), %rdi
	movq	%rbx, %r10
	movq	%rcx, %rax
	salq	%cl, %r10
	shrq	$6, %rax
	leaq	(%rdi,%rax,8), %r11
	movq	(%r11), %r12
	testq	%r12, %r10
	jne	.L1720
	movq	104(%r9), %rax
	movq	112(%r9), %r8
	cmpq	%rax, %r8
	je	.L1721
	.p2align 4,,10
	.p2align 3
.L1724:
	movq	(%rax), %rdx
	movq	%rbx, %r14
	movq	160(%rdx), %rcx
	movq	%rcx, %rdx
	salq	%cl, %r14
	shrq	$6, %rdx
	testq	%r14, (%rdi,%rdx,8)
	je	.L1720
	addq	$8, %rax
	cmpq	%rax, %r8
	jne	.L1724
.L1721:
	orq	%r12, %r10
	movq	%r10, (%r11)
	movq	136(%r9), %r12
	movq	144(%r9), %r14
	cmpq	%r14, %r12
	je	.L2191
	movq	136(%r15), %rax
	.p2align 4,,10
	.p2align 3
.L1740:
	movq	(%r12), %r13
	movq	32(%r15), %rdx
	movq	%rbx, %rdi
	movq	160(%r13), %rcx
	movq	%rcx, %rsi
	salq	%cl, %rdi
	shrq	$6, %rsi
	testq	%rdi, (%rdx,%rsi,8)
	jne	.L1725
	movq	152(%r15), %rdi
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L1726
	movq	%r13, (%rax)
	movq	136(%r15), %rax
	addq	$8, %rax
	movq	%rax, 136(%r15)
.L1725:
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L1740
	movq	104(%r15), %rsi
	cmpq	%rax, %rsi
	jne	.L1741
	.p2align 4,,10
	.p2align 3
.L2190:
	movq	-160(%rbp), %rax
	movq	32(%r15), %rdx
	movq	160(%rax), %rcx
	movl	$1, %eax
	movq	%rcx, %rsi
	salq	%cl, %rax
	shrq	$6, %rsi
	testq	%rax, (%rdx,%rsi,8)
	jne	.L2192
	movq	8(%r15), %rdx
	movq	(%rdx), %rax
	movl	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	-200(%rbp), %rax
	movq	$0, -64(%rbp)
	movq	24(%rax), %rax
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L1579
	movq	(%rax), %rax
	movq	%r15, %r14
	movq	%rax, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L1860:
	movq	-168(%rbp), %rbx
	movq	200(%rdx), %r9
	movl	16(%rbx), %ecx
	movl	%ecx, %r8d
	shrl	%r8d
	movl	%r8d, %eax
	leaq	3(%rax,%rax,2), %rsi
	salq	$3, %rax
	andl	$1, %ecx
	leaq	(%rbx,%rsi,8), %r12
	movq	%r12, %rdi
	je	.L1744
	movl	20(%r12), %esi
	andl	$16777215, %esi
	movl	%esi, %ecx
	salq	$4, %rcx
	movl	12(%r9,%rcx), %ecx
	testl	%ecx, %ecx
	je	.L1857
	leaq	32(%r12,%rax), %rax
	movq	%rax, -176(%rbp)
.L1746:
	movq	(%r12), %r9
	movzwl	16(%r9), %eax
	leal	-35(%rax), %edi
	cmpl	$1, %edi
	jbe	.L2193
	cmpl	$10, %eax
	sete	%dil
	cmpl	$1, %eax
	sete	%al
	orb	%al, %dil
	je	.L1821
	cmpl	$2, %ecx
	je	.L2194
.L1821:
	movq	16(%r14), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1857
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2195
.L1873:
	movq	16(%r15), %rax
	movq	32(%r14), %rsi
	movq	160(%rax), %rcx
	jmp	.L2181
	.p2align 4,,10
	.p2align 3
.L2196:
	movq	%rax, %r15
	movq	16(%rax), %rax
	movq	160(%rax), %rcx
.L2181:
	movq	%rcx, %rdx
	movl	$1, %ebx
	shrq	$6, %rdx
	salq	%cl, %rbx
	testq	%rbx, (%rsi,%rdx,8)
	jne	.L2196
	movq	-88(%rbp), %rax
	movq	-208(%rbp), %r12
	testq	%rax, %rax
	jne	.L1831
	jmp	.L1830
	.p2align 4,,10
	.p2align 3
.L2197:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1832
.L1831:
	cmpq	%r15, 32(%rax)
	jnb	.L2197
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1831
.L1832:
	cmpq	-208(%rbp), %r12
	je	.L1830
	cmpq	%r15, 32(%r12)
	jbe	.L1835
.L1830:
	movq	-112(%rbp), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L2198
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1837:
	movq	%r15, 32(%rbx)
	movq	%r12, %rsi
	leaq	32(%rbx), %rdx
	leaq	-112(%rbp), %rdi
	movq	$0, 40(%rbx)
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS9_ERS6_
	movq	%rax, %r12
	testq	%rdx, %rdx
	je	.L1835
	testq	%rax, %rax
	jne	.L1890
	cmpq	%rdx, -208(%rbp)
	jne	.L2199
.L1890:
	movl	$1, %edi
.L1839:
	movq	-208(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%rbx, %r12
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -64(%rbp)
.L1835:
	movq	40(%r12), %r13
	testq	%r13, %r13
	je	.L2200
.L1840:
	movq	-176(%rbp), %rax
	movq	(%rax), %rdi
	cmpq	%rdi, %r13
	je	.L1857
	testq	%rdi, %rdi
	je	.L1855
	movq	-168(%rbp), %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1855:
	movq	-176(%rbp), %rax
	movq	%r13, (%rax)
	testq	%r13, %r13
	je	.L1857
	movq	-168(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L1857:
	cmpq	$0, -152(%rbp)
	je	.L2201
	movq	-152(%rbp), %rax
	movq	8(%r14), %rdx
	movq	%rax, -168(%rbp)
	movq	(%rax), %rax
	movq	%rax, -152(%rbp)
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L2185:
	cmpl	$3, %edx
	je	.L2202
	cmpl	$2, %edx
	jne	.L1683
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2203
.L1684:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	movq	8(%r15), %rax
	movq	224(%rax), %rbx
	jmp	.L1686
	.p2align 4,,10
	.p2align 3
.L2204:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L1686:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2204
	movq	160(%rax), %rcx
	movq	%rax, %r14
	jmp	.L1868
	.p2align 4,,10
	.p2align 3
.L1700:
	movq	160(%r15), %rcx
	movq	128(%r15), %rsi
	subq	144(%r15), %rax
	movq	%rcx, %r14
	sarq	$3, %rax
	subq	%rsi, %r14
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	120(%r15), %rax
	subq	104(%r15), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L1727
	movq	88(%r15), %r8
	movq	96(%r15), %rdx
	movq	%rcx, %rax
	subq	%r8, %rax
	movq	%rdx, %r11
	sarq	$3, %rax
	subq	%rax, %r11
	cmpq	$1, %r11
	jbe	.L2205
.L1702:
	movq	80(%r15), %rax
	testq	%rax, %rax
	je	.L1710
	cmpq	$63, 8(%rax)
	ja	.L2206
.L1710:
	movq	72(%r15), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L2207
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1711:
	movq	%rax, 8(%rcx)
	movq	136(%r15), %rax
	movq	%r12, (%rax)
	movq	160(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 160(%r15)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 144(%r15)
	movq	%rdx, 152(%r15)
	movq	%rax, 136(%r15)
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L2187:
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC6(%rip), %rdi
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	160(%r14), %rcx
	jmp	.L1868
	.p2align 4,,10
	.p2align 3
.L1716:
	movq	80(%r15), %rax
	testq	%rax, %rax
	je	.L1718
	cmpq	$64, 8(%rax)
	ja	.L1719
.L1718:
	movq	112(%r15), %rax
	movq	$64, 8(%rax)
	movq	80(%r15), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 80(%r15)
.L1719:
	movq	128(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 128(%r15)
	movq	8(%rax), %rsi
	leaq	512(%rsi), %rax
	movq	%rsi, 112(%r15)
	movq	%rax, 120(%r15)
	movq	%rsi, 104(%r15)
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L2202:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2208
.L1596:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1593
	movq	(%rbx), %rax
	xorl	%r14d, %r14d
	movq	%r14, -176(%rbp)
	movq	%rax, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L1682:
	movl	16(%rbx), %edx
	movl	%edx, %r8d
	shrl	%r8d
	movl	%r8d, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%rbx,%rcx,8), %r12
	movq	8(%r15), %rcx
	movq	%r12, %r9
	movq	200(%rcx), %rdi
	je	.L1598
	movl	20(%r12), %esi
	leaq	32(%r12,%rax), %r13
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%rdi,%rdx), %edx
	testl	%edx, %edx
	je	.L1599
.L1600:
	movq	(%r12), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r9d
	cmpl	$1, %r9d
	jbe	.L2209
	cmpl	$10, %eax
	sete	%r9b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r9b
	je	.L1674
	cmpl	$2, %edx
	je	.L2210
.L1674:
	movq	16(%r15), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1599
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2211
.L1676:
	cmpq	$0, -176(%rbp)
	jne	.L1867
.L2176:
	movq	%r14, -176(%rbp)
.L1599:
	movq	-168(%rbp), %rax
	testq	%rax, %rax
	je	.L1681
.L2215:
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%rax, -168(%rbp)
	jmp	.L1682
	.p2align 4,,10
	.p2align 3
.L1598:
	movq	(%r12), %r12
	movl	20(%r12), %esi
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%rdi,%rdx), %edx
	testl	%edx, %edx
	je	.L1599
	leaq	16(%r9,%rax), %r13
	jmp	.L1600
	.p2align 4,,10
	.p2align 3
.L2209:
	cmpl	$3, %edx
	je	.L2212
	cmpl	$2, %edx
	jne	.L1674
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2213
.L1675:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	movq	8(%r15), %rax
	movq	224(%rax), %rbx
	jmp	.L1677
	.p2align 4,,10
	.p2align 3
.L2214:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L1677:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2214
.L2156:
	cmpq	$0, -176(%rbp)
	movq	%rax, %r14
	je	.L2176
.L1867:
	movq	-176(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, -176(%rbp)
	movq	-168(%rbp), %rax
	testq	%rax, %rax
	jne	.L2215
	.p2align 4,,10
	.p2align 3
.L1681:
	movq	-176(%rbp), %r14
	testq	%r14, %r14
	je	.L1593
	movq	160(%r14), %rcx
	jmp	.L1868
	.p2align 4,,10
	.p2align 3
.L2205:
	leaq	2(%rdi), %r9
	leaq	(%r9,%r9), %rax
	cmpq	%rax, %rdx
	jbe	.L1703
	subq	%r9, %rdx
	addq	$8, %rcx
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r8
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	cmpq	%r8, %rsi
	jbe	.L1704
	cmpq	%rcx, %rsi
	je	.L1705
	movq	%r8, %rdi
	call	memmove@PLT
	movq	%rax, %r8
	jmp	.L1705
	.p2align 4,,10
	.p2align 3
.L1744:
	movq	(%r12), %r12
	movl	20(%r12), %esi
	andl	$16777215, %esi
	movl	%esi, %ecx
	salq	$4, %rcx
	movl	12(%r9,%rcx), %ecx
	testl	%ecx, %ecx
	je	.L1857
	leaq	16(%rdi,%rax), %rax
	movq	%rax, -176(%rbp)
	jmp	.L1746
	.p2align 4,,10
	.p2align 3
.L2201:
	movq	-88(%rbp), %r12
	testq	%r12, %r12
	je	.L1579
	leaq	-112(%rbp), %r13
.L1859:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1861
.L1862:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler10BasicBlockESt4pairIKS4_PNS2_4NodeEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1862
.L1861:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1859
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L2193:
	cmpl	$3, %ecx
	je	.L2216
	cmpl	$2, %ecx
	jne	.L1821
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2217
.L1822:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %rdi
	movq	-168(%rbp), %rax
	movl	16(%rax), %eax
	movl	%eax, %esi
	movl	%eax, -184(%rbp)
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	movq	8(%r14), %rax
	movq	224(%rax), %rbx
	jmp	.L1824
	.p2align 4,,10
	.p2align 3
.L2218:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L1824:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2218
.L2167:
	movq	%rax, %r15
	jmp	.L1873
	.p2align 4,,10
	.p2align 3
.L2211:
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r14), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1676
	.p2align 4,,10
	.p2align 3
.L1726:
	movq	160(%r15), %r8
	movq	128(%r15), %rsi
	subq	144(%r15), %rax
	movq	%r8, %rcx
	sarq	$3, %rax
	subq	%rsi, %rcx
	movq	%rcx, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	120(%r15), %rax
	subq	104(%r15), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L1727
	movq	88(%r15), %r9
	movq	96(%r15), %rdx
	movq	%r8, %rax
	subq	%r9, %rax
	movq	%rdx, %r11
	sarq	$3, %rax
	subq	%rax, %r11
	cmpq	$1, %r11
	jbe	.L2219
.L1728:
	movq	80(%r15), %rax
	testq	%rax, %rax
	je	.L1736
	cmpq	$63, 8(%rax)
	ja	.L2220
.L1736:
	movq	72(%r15), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L2221
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1737:
	movq	%rax, 8(%r8)
	movq	136(%r15), %rax
	movq	%r13, (%rax)
	movq	160(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 160(%r15)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 144(%r15)
	movq	%rdx, 152(%r15)
	movq	%rax, 136(%r15)
	jmp	.L1725
	.p2align 4,,10
	.p2align 3
.L2206:
	movq	(%rax), %rdx
	movq	%rdx, 80(%r15)
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L2186:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2222
.L1687:
	movq	0(%r13), %r12
	movq	224(%rcx), %rbx
	jmp	.L1690
	.p2align 4,,10
	.p2align 3
.L1688:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L1690:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L1688
	movq	160(%rax), %rcx
	movq	%rax, %r14
	jmp	.L1868
	.p2align 4,,10
	.p2align 3
.L2212:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2223
.L1603:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1599
	movq	(%rbx), %rax
	xorl	%r14d, %r14d
	movq	%r14, -208(%rbp)
	movq	%rax, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L1673:
	movl	16(%rbx), %edx
	movl	%edx, %r8d
	shrl	%r8d
	movl	%r8d, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%rbx,%rcx,8), %r12
	movq	8(%r15), %rcx
	movq	%r12, %r9
	movq	200(%rcx), %rdi
	je	.L1605
	movl	20(%r12), %esi
	leaq	32(%r12,%rax), %r13
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%rdi,%rdx), %edx
	testl	%edx, %edx
	je	.L1606
.L1607:
	movq	(%r12), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r9d
	cmpl	$1, %r9d
	jbe	.L2224
	cmpl	$10, %eax
	sete	%r9b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r9b
	je	.L1665
	cmpl	$2, %edx
	je	.L2225
.L1665:
	movq	16(%r15), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L1606
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2226
.L1667:
	cmpq	$0, -208(%rbp)
	jne	.L1866
.L2175:
	movq	-192(%rbp), %rax
	movq	%rax, -208(%rbp)
.L1606:
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	je	.L1672
.L2230:
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%rax, -184(%rbp)
	jmp	.L1673
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	(%r12), %r12
	movl	20(%r12), %esi
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%rdi,%rdx), %edx
	testl	%edx, %edx
	je	.L1606
	leaq	16(%r9,%rax), %r13
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L2224:
	cmpl	$3, %edx
	je	.L2227
	cmpl	$2, %edx
	jne	.L1665
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2228
.L1666:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	movq	8(%r15), %rax
	movq	224(%rax), %rbx
	jmp	.L1668
	.p2align 4,,10
	.p2align 3
.L2229:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L1668:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2229
.L2154:
	movq	%rax, -192(%rbp)
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1663:
	cmpq	$0, -208(%rbp)
	je	.L2175
	cmpq	$0, -192(%rbp)
	je	.L1606
	.p2align 4,,10
	.p2align 3
.L1866:
	movq	-192(%rbp), %rsi
	movq	-208(%rbp), %rdi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, -208(%rbp)
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	jne	.L2230
	.p2align 4,,10
	.p2align 3
.L1672:
	cmpq	$0, -176(%rbp)
	movq	-208(%rbp), %r14
	je	.L2176
	testq	%r14, %r14
	je	.L1599
	jmp	.L1867
	.p2align 4,,10
	.p2align 3
.L2226:
	movq	160(%rax), %rax
	movl	20(%r12), %esi
	leaq	.LC6(%rip), %rdi
	movq	%rax, -216(%rbp)
	movq	(%r12), %rax
	andl	$16777215, %esi
	movl	-216(%rbp), %ecx
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L2195:
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r15), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1873
	.p2align 4,,10
	.p2align 3
.L1703:
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	72(%r15), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %rcx
	leaq	2(%rdx,%rax), %r10
	movq	24(%rdi), %rax
	leaq	0(,%r10,8), %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L2231
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L1707:
	movq	%r10, %rax
	movq	128(%r15), %rsi
	subq	%r9, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r8
	movq	160(%r15), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1708
	movq	%r8, %rdi
	subq	%rsi, %rdx
	movq	%rcx, -176(%rbp)
	movq	%r10, -168(%rbp)
	call	memmove@PLT
	movq	-176(%rbp), %rcx
	movq	-168(%rbp), %r10
	movq	%rax, %r8
.L1708:
	movq	96(%r15), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L1709
	movq	88(%r15), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L1709:
	movq	%rcx, 88(%r15)
	movq	%r10, 96(%r15)
.L1705:
	movq	%r8, 128(%r15)
	movq	(%r8), %rax
	leaq	(%r8,%r14), %rcx
	movq	(%r8), %xmm0
	movq	%rcx, 160(%r15)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 112(%r15)
	movq	(%rcx), %rax
	movq	%rax, 144(%r15)
	addq	$512, %rax
	movq	%rax, 152(%r15)
	jmp	.L1702
	.p2align 4,,10
	.p2align 3
.L2200:
	cmpq	$1, -64(%rbp)
	movq	-200(%rbp), %rax
	jne	.L1841
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, 40(%r12)
	movq	%rax, %r13
	movq	%r15, -160(%rbp)
	je	.L1840
	movq	(%rax), %rax
	movq	160(%r15), %rcx
	leaq	.LC31(%rip), %rdi
	movq	8(%rax), %rdx
	movl	20(%r13), %eax
	movl	%eax, %esi
	movl	%eax, -184(%rbp)
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	40(%r12), %r13
	jmp	.L1840
	.p2align 4,,10
	.p2align 3
.L2210:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2232
.L1678:
	movq	0(%r13), %r12
	movq	224(%rcx), %rbx
	jmp	.L1679
	.p2align 4,,10
	.p2align 3
.L2233:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L1679:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2233
	jmp	.L2156
	.p2align 4,,10
	.p2align 3
.L1586:
	movl	%ebx, -136(%rbp)
	subq	%rdx, %rcx
	leaq	24(%r15), %rdi
	xorl	%r8d, %r8d
	movq	-136(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r12, -144(%rbp)
	call	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb
	jmp	.L1588
	.p2align 4,,10
	.p2align 3
.L2208:
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L2227:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2234
.L1610:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1606
	movq	$0, -192(%rbp)
	movq	(%rbx), %rax
	movq	%rax, -216(%rbp)
	.p2align 4,,10
	.p2align 3
.L1664:
	movl	16(%rbx), %edx
	movl	%edx, %esi
	shrl	%esi
	movl	%esi, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%rbx,%rcx,8), %r13
	movq	8(%r15), %rcx
	movq	%r13, %rdi
	movq	200(%rcx), %r9
	je	.L1612
	movl	20(%r13), %r8d
	leaq	32(%r13,%rax), %r12
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	movl	12(%r9,%rdx), %edx
	testl	%edx, %edx
	je	.L1613
.L1614:
	movq	0(%r13), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r9d
	cmpl	$1, %r9d
	jbe	.L2235
	cmpl	$10, %eax
	sete	%r9b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r9b
	je	.L1656
	cmpl	$2, %edx
	je	.L2236
.L1656:
	movq	16(%r15), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1613
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2237
.L1658:
	cmpq	$0, -192(%rbp)
	jne	.L1865
.L2174:
	movq	%r12, -192(%rbp)
.L1613:
	movq	-216(%rbp), %rax
	testq	%rax, %rax
	je	.L1663
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%rax, -216(%rbp)
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L2216:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2238
.L1749:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1857
	movq	(%rbx), %rax
	xorl	%r15d, %r15d
	movq	%r15, -216(%rbp)
	movq	%r14, %r15
	movq	%rax, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L1819:
	movl	16(%rbx), %edx
	movl	%edx, %esi
	shrl	%esi
	movl	%esi, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%rbx,%rcx,8), %r12
	movq	8(%r15), %rcx
	movq	%r12, %rdi
	movq	200(%rcx), %r9
	je	.L1751
	movl	20(%r12), %r8d
	leaq	32(%r12,%rax), %r13
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	movl	12(%r9,%rdx), %edx
	testl	%edx, %edx
	je	.L1752
.L1753:
	movq	(%r12), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r9d
	cmpl	$1, %r9d
	jbe	.L2239
	cmpl	$10, %eax
	sete	%r9b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r9b
	je	.L1811
	cmpl	$2, %edx
	je	.L2240
.L1811:
	movq	16(%r15), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L1752
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2241
.L1813:
	cmpq	$0, -216(%rbp)
	jne	.L1872
.L2180:
	movq	-192(%rbp), %rax
	movq	%rax, -216(%rbp)
.L1752:
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	je	.L1818
.L2251:
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%rax, -184(%rbp)
	jmp	.L1819
.L2219:
	leaq	2(%rdi), %r10
	leaq	(%r10,%r10), %rax
	cmpq	%rax, %rdx
	ja	.L2242
	testq	%rdx, %rdx
	movq	%rbx, %rax
	movq	72(%r15), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r8
	leaq	2(%rdx,%rax), %r11
	movq	24(%rdi), %rax
	leaq	0(,%r11,8), %rsi
	subq	%r8, %rax
	cmpq	%rax, %rsi
	ja	.L2243
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L1733:
	movq	%r11, %rax
	movq	128(%r15), %rsi
	subq	%r10, %rax
	shrq	%rax
	leaq	(%r8,%rax,8), %r9
	movq	160(%r15), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1734
	movq	%r9, %rdi
	subq	%rsi, %rdx
	movq	%r8, -176(%rbp)
	movq	%r11, -168(%rbp)
	movq	%rcx, -152(%rbp)
	call	memmove@PLT
	movq	-176(%rbp), %r8
	movq	-168(%rbp), %r11
	movq	-152(%rbp), %rcx
	movq	%rax, %r9
.L1734:
	movq	96(%r15), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L1735
	movq	88(%r15), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L1735:
	movq	%r8, 88(%r15)
	movq	%r11, 96(%r15)
	jmp	.L1731
.L2222:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC5(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r15), %rcx
	jmp	.L1687
.L1704:
	cmpq	%rcx, %rsi
	je	.L1705
	leaq	8(%r14), %rdi
	movq	%r8, -168(%rbp)
	subq	%rdx, %rdi
	addq	%r8, %rdi
	call	memmove@PLT
	movq	-168(%rbp), %r8
	jmp	.L1705
.L1841:
	movl	20(%rax), %eax
	movl	%eax, %edx
	shrl	$24, %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L1843
	movq	-200(%rbp), %rbx
	movq	32(%rbx), %rdx
	movl	8(%rdx), %edx
.L1843:
	testl	%edx, %edx
	jle	.L1844
	movq	-200(%rbp), %rdi
	leal	-1(%rdx), %esi
	xorl	%r13d, %r13d
	movq	%r12, -184(%rbp)
	movq	%r15, -192(%rbp)
	movq	%r14, %r15
	leaq	32(%rdi), %rbx
	movq	%rdi, %r14
	movq	%rbx, %r12
	movq	%rsi, %rbx
	jmp	.L1847
	.p2align 4,,10
	.p2align 3
.L2244:
	movq	(%r12,%r13,8), %rsi
.L2184:
	movq	8(%r15), %rdi
	movl	%r13d, %edx
	movq	%r14, %rcx
	call	_ZN2v88internal8compiler9Scheduler28IncrementUnscheduledUseCountEPNS1_4NodeEiS4_
	leaq	1(%r13), %rdx
	cmpq	%r13, %rbx
	je	.L2168
	movl	20(%r14), %eax
	movq	%rdx, %r13
.L1847:
	xorl	$251658240, %eax
	leaq	0(,%r13,8), %rdx
	testl	$251658240, %eax
	jne	.L2244
	movq	(%r12), %rax
	movq	16(%rax,%rdx), %rsi
	jmp	.L2184
.L2242:
	subq	%r10, %rdx
	addq	$8, %r8
	shrq	%rdx
	leaq	(%r9,%rdx,8), %r9
	movq	%r8, %rdx
	subq	%rsi, %rdx
	cmpq	%r9, %rsi
	jbe	.L1730
	cmpq	%r8, %rsi
	je	.L1731
	movq	%r9, %rdi
	movq	%rcx, -152(%rbp)
	call	memmove@PLT
	movq	-152(%rbp), %rcx
	movq	%rax, %r9
.L1731:
	movq	%r9, 128(%r15)
	movq	(%r9), %rax
	leaq	(%r9,%rcx), %r8
	movq	(%r9), %xmm0
	movq	%r8, 160(%r15)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 112(%r15)
	movq	(%r8), %rax
	movq	%rax, 144(%r15)
	addq	$512, %rax
	movq	%rax, 152(%r15)
	jmp	.L1728
.L2238:
	movq	8(%r9), %rdx
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1749
.L2234:
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1610
.L2199:
	xorl	%edi, %edi
	movq	32(%rdx), %rax
	cmpq	%rax, 32(%rbx)
	setb	%dil
	jmp	.L1839
	.p2align 4,,10
	.p2align 3
.L1612:
	movq	0(%r13), %r13
	movl	20(%r13), %r8d
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	movl	12(%r9,%rdx), %edx
	testl	%edx, %edx
	je	.L1613
	leaq	16(%rdi,%rax), %r12
	jmp	.L1614
	.p2align 4,,10
	.p2align 3
.L1751:
	movq	(%r12), %r12
	movl	20(%r12), %r8d
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	movl	12(%r9,%rdx), %edx
	testl	%edx, %edx
	je	.L1752
	leaq	16(%rdi,%rax), %r13
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L2235:
	cmpl	$3, %edx
	je	.L2245
	cmpl	$2, %edx
	jne	.L1656
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2246
.L1657:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	movq	8(%r15), %rax
	movq	224(%rax), %rbx
	jmp	.L1659
	.p2align 4,,10
	.p2align 3
.L2247:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L1659:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2247
.L2152:
	movq	%rax, %r12
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L2239:
	cmpl	$3, %edx
	je	.L2248
	cmpl	$2, %edx
	jne	.L1811
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2249
.L1812:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	movq	8(%r15), %rax
	movq	224(%rax), %rbx
	jmp	.L1814
	.p2align 4,,10
	.p2align 3
.L2250:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L1814:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2250
.L2165:
	movq	%rax, -192(%rbp)
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1654:
	cmpq	$0, -192(%rbp)
	movq	-224(%rbp), %r12
	je	.L2174
	testq	%r12, %r12
	je	.L1613
	.p2align 4,,10
	.p2align 3
.L1865:
	movq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, -192(%rbp)
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L1809:
	cmpq	$0, -216(%rbp)
	je	.L2180
	cmpq	$0, -192(%rbp)
	je	.L1752
	.p2align 4,,10
	.p2align 3
.L1872:
	movq	-192(%rbp), %rsi
	movq	-216(%rbp), %rdi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, -216(%rbp)
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	jne	.L2251
	.p2align 4,,10
	.p2align 3
.L1818:
	movq	%r15, %r14
	movq	-216(%rbp), %r15
	testq	%r15, %r15
	je	.L1857
	jmp	.L1873
	.p2align 4,,10
	.p2align 3
.L2237:
	movq	0(%r13), %rax
	movl	20(%r13), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r12), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1658
.L2241:
	movq	160(%rax), %rax
	movl	20(%r12), %esi
	leaq	.LC6(%rip), %rdi
	movq	%rax, -224(%rbp)
	movq	(%r12), %rax
	andl	$16777215, %esi
	movl	-224(%rbp), %ecx
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1813
.L2225:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2252
.L1669:
	movq	0(%r13), %r12
	movq	224(%rcx), %rbx
	jmp	.L1670
	.p2align 4,,10
	.p2align 3
.L2253:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L1670:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2253
	jmp	.L2154
.L2194:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2254
.L1825:
	movq	-176(%rbp), %rax
	movq	224(%rdx), %rbx
	movq	(%rax), %r12
	jmp	.L1826
	.p2align 4,,10
	.p2align 3
.L2255:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L1826:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2255
	jmp	.L2167
.L2188:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2256
.L1693:
	movq	128(%r15), %r8
	movq	160(%r15), %rax
	movq	104(%r15), %xmm0
	movq	112(%r15), %r9
	leaq	8(%rax), %rcx
	leaq	8(%r8), %rdx
	movq	120(%r15), %rdi
	cmpq	%rdx, %rcx
	jbe	.L1697
	movq	80(%r15), %rax
	.p2align 4,,10
	.p2align 3
.L1698:
	testq	%rax, %rax
	je	.L1695
	cmpq	$64, 8(%rax)
	ja	.L1696
.L1695:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	80(%r15), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 80(%r15)
.L1696:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L1698
.L1697:
	movq	%r9, %xmm3
	movq	%r8, %xmm4
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 136(%r15)
	movq	%rdi, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 152(%r15)
	jmp	.L1579
.L2203:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC4(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1684
.L2220:
	movq	(%rax), %rdx
	movq	%rdx, 80(%r15)
	jmp	.L1737
.L2168:
	movq	%r15, %r14
	movq	-184(%rbp), %r12
	movq	-192(%rbp), %r15
.L1844:
	movq	8(%r14), %rax
	movq	-200(%rbp), %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph9CloneNodeEPKNS1_4NodeE@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, %rbx
	jne	.L2257
.L1848:
	movq	8(%r14), %rdi
	movq	$0, -120(%rbp)
	movq	16(%rdi), %rax
	movq	208(%rdi), %rsi
	movq	200(%rdi), %rcx
	movq	104(%rax), %rax
	movq	%rsi, %r8
	subq	%rcx, %r8
	movq	%rax, -128(%rbp)
	movl	20(%rbx), %eax
	sarq	$4, %r8
	andl	$16777215, %eax
	leal	1(%rax), %edx
	cmpq	%r8, %rdx
	ja	.L2258
	jnb	.L1850
	salq	$4, %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %rsi
	je	.L1850
	movq	%rdx, 208(%rdi)
	movq	8(%r14), %rax
	movq	200(%rax), %rcx
	movl	20(%rbx), %eax
	andl	$16777215, %eax
.L1850:
	movq	-200(%rbp), %rdi
	salq	$4, %rax
	movl	20(%rdi), %edx
	andl	$16777215, %edx
	salq	$4, %rdx
	movdqu	(%rcx,%rdx), %xmm5
	movups	%xmm5, (%rcx,%rax)
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rbx, 40(%r12)
	jne	.L2259
.L1851:
	movq	8(%r14), %rax
	movq	176(%rax), %rbx
	movq	160(%rax), %rcx
	leaq	-8(%rbx), %rdx
	cmpq	%rdx, %rcx
	je	.L1852
	movq	40(%r12), %rdx
	movq	%rdx, (%rcx)
	addq	$8, 160(%rax)
	movq	40(%r12), %r13
	jmp	.L1840
.L2207:
	movl	$512, %esi
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-168(%rbp), %rcx
	jmp	.L1711
.L2223:
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1603
.L2245:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2260
.L1617:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L1613
	xorl	%r12d, %r12d
	movq	(%rbx), %r13
	movq	%r12, -224(%rbp)
	.p2align 4,,10
	.p2align 3
.L1655:
	movl	16(%rbx), %edx
	movl	%edx, %esi
	shrl	%esi
	movl	%esi, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%rbx,%rcx,8), %r14
	movq	8(%r15), %rcx
	movq	%r14, %r9
	movq	200(%rcx), %rdi
	je	.L1619
	movl	20(%r14), %r8d
	leaq	32(%r14,%rax), %r12
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	movl	12(%rdi,%rdx), %edx
	testl	%edx, %edx
	je	.L1620
.L1621:
	movq	(%r14), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r9d
	cmpl	$1, %r9d
	jbe	.L2261
	cmpl	$10, %eax
	sete	%r9b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r9b
	je	.L1647
	cmpl	$2, %edx
	je	.L2262
.L1647:
	movq	16(%r15), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1620
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2263
.L1649:
	cmpq	$0, -224(%rbp)
	jne	.L1864
.L2173:
	movq	%r12, -224(%rbp)
.L1620:
	testq	%r13, %r13
	je	.L1654
	movq	%r13, %rbx
	movq	0(%r13), %r13
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1619:
	movq	(%r14), %r14
	movl	20(%r14), %r8d
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	movl	12(%rdi,%rdx), %edx
	testl	%edx, %edx
	je	.L1620
	leaq	16(%r9,%rax), %r12
	jmp	.L1621
.L2261:
	cmpl	$3, %edx
	je	.L2264
	cmpl	$2, %edx
	jne	.L1647
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2265
.L1648:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
	movq	8(%r15), %rax
	movq	224(%rax), %rbx
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L2266:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
.L1650:
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2266
.L2150:
	movq	%rax, %r12
	jmp	.L1649
.L1645:
	cmpq	$0, -224(%rbp)
	movq	-232(%rbp), %r13
	je	.L2173
	testq	%r12, %r12
	je	.L1620
.L1864:
	movq	-224(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, -224(%rbp)
	jmp	.L1620
.L2248:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2267
.L1756:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1752
	movq	$0, -192(%rbp)
	movq	(%rbx), %rax
	movq	%rax, -224(%rbp)
	.p2align 4,,10
	.p2align 3
.L1810:
	movl	16(%rbx), %edx
	movl	%edx, %esi
	shrl	%esi
	movl	%esi, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%rbx,%rcx,8), %r13
	movq	8(%r15), %rcx
	movq	%r13, %rdi
	movq	200(%rcx), %r9
	je	.L1758
	movl	20(%r13), %r8d
	leaq	32(%r13,%rax), %r12
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	movl	12(%r9,%rdx), %edx
	testl	%edx, %edx
	je	.L1759
.L1760:
	movq	0(%r13), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r9d
	cmpl	$1, %r9d
	jbe	.L2268
	cmpl	$10, %eax
	sete	%r9b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r9b
	je	.L1802
	cmpl	$2, %edx
	je	.L2269
.L1802:
	movq	16(%r15), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1759
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2270
.L1804:
	cmpq	$0, -192(%rbp)
	jne	.L1871
.L2179:
	movq	%r12, -192(%rbp)
.L1759:
	movq	-224(%rbp), %rax
	testq	%rax, %rax
	je	.L1809
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%rax, -224(%rbp)
	jmp	.L1810
	.p2align 4,,10
	.p2align 3
.L1758:
	movq	0(%r13), %r13
	movl	20(%r13), %r8d
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	movl	12(%r9,%rdx), %edx
	testl	%edx, %edx
	je	.L1759
	leaq	16(%rdi,%rax), %r12
	jmp	.L1760
.L2268:
	cmpl	$3, %edx
	je	.L2271
	cmpl	$2, %edx
	jne	.L1802
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2272
.L1803:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	movq	8(%r15), %rax
	movq	224(%rax), %rbx
	jmp	.L1805
	.p2align 4,,10
	.p2align 3
.L2273:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L1805:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2273
.L2163:
	movq	%rax, %r12
	jmp	.L1804
.L1800:
	cmpq	$0, -192(%rbp)
	movq	-232(%rbp), %r12
	je	.L2179
	testq	%r12, %r12
	je	.L1759
.L1871:
	movq	-192(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, -192(%rbp)
	jmp	.L1759
.L2263:
	movq	(%r14), %rax
	movl	20(%r14), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r12), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1649
.L2270:
	movq	0(%r13), %rax
	movl	20(%r13), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r12), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1804
.L2232:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC5(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r15), %rcx
	jmp	.L1678
.L2236:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2274
.L1660:
	movq	(%r12), %r13
	movq	224(%rcx), %rbx
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L2275:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L1661:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2275
	jmp	.L2152
.L2240:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2276
.L1815:
	movq	0(%r13), %r12
	movq	224(%rcx), %rbx
	jmp	.L1816
	.p2align 4,,10
	.p2align 3
.L2277:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L1816:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2277
	jmp	.L2165
.L2213:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC4(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1675
.L2191:
	movq	104(%r15), %rsi
	movq	136(%r15), %rax
	jmp	.L1723
.L2264:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2278
.L1624:
	movq	24(%r14), %r9
	testq	%r9, %r9
	je	.L1620
	movq	%r13, -232(%rbp)
	movq	(%r9), %rbx
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L1646:
	movl	16(%r9), %edx
	movl	%edx, %esi
	shrl	%esi
	movl	%esi, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%r9,%rcx,8), %r14
	movq	8(%r15), %rcx
	movq	%r14, %rdi
	movq	200(%rcx), %r10
	je	.L1626
	movl	20(%r14), %r8d
	leaq	32(%r14,%rax), %r13
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	movl	12(%r10,%rdx), %edx
	testl	%edx, %edx
	je	.L1627
.L1628:
	movq	(%r14), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r11d
	cmpl	$1, %r11d
	jbe	.L2279
	cmpl	$10, %eax
	sete	%r9b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r9b
	je	.L1638
	cmpl	$2, %edx
	je	.L2280
.L1638:
	movq	16(%r15), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1627
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2281
.L1640:
	testq	%r12, %r12
	jne	.L1863
.L2172:
	movq	%r13, %r12
.L1627:
	testq	%rbx, %rbx
	je	.L1645
	movq	%rbx, %r9
	movq	(%rbx), %rbx
	jmp	.L1646
	.p2align 4,,10
	.p2align 3
.L1626:
	movq	(%r14), %r14
	movl	20(%r14), %r8d
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	movl	12(%r10,%rdx), %edx
	testl	%edx, %edx
	je	.L1627
	leaq	16(%rdi,%rax), %r13
	jmp	.L1628
.L2279:
	cmpl	$3, %edx
	je	.L2282
	cmpl	$2, %edx
	jne	.L1638
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2283
.L1639:
	movq	%r14, %rdi
	xorl	%esi, %esi
	movq	%r9, -240(%rbp)
	movq	%rbx, %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-240(%rbp), %r9
	movq	%r12, %rbx
	movq	%rax, %rdi
	movl	16(%r9), %esi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r9
	movq	8(%r15), %rax
	movq	%r9, %r12
	movq	224(%rax), %r14
	jmp	.L1641
.L2284:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L1641:
	movq	16(%r14), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2284
.L2148:
	movq	%rbx, %r12
	movq	%r13, %rbx
	movq	%rax, %r13
	jmp	.L1640
.L1636:
	testq	%r12, %r12
	je	.L2172
	testq	%r13, %r13
	je	.L1627
.L1863:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, %r12
	jmp	.L1627
.L2271:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2285
.L1763:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L1759
	xorl	%r12d, %r12d
	movq	(%rbx), %r14
	movq	%rbx, %r13
	movq	%r12, -232(%rbp)
	.p2align 4,,10
	.p2align 3
.L1801:
	movl	16(%r13), %edx
	movl	%edx, %esi
	shrl	%esi
	movl	%esi, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	0(%r13,%rcx,8), %rbx
	movq	8(%r15), %rcx
	movq	%rbx, %rdi
	movq	200(%rcx), %r9
	je	.L1765
	movl	20(%rbx), %r8d
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	movl	12(%r9,%rdx), %edx
	leaq	32(%rbx,%rax), %r9
	testl	%edx, %edx
	je	.L1766
.L1767:
	movq	(%rbx), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r10d
	cmpl	$1, %r10d
	jbe	.L2286
	cmpl	$10, %eax
	sete	%r10b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r10b
	je	.L1793
	cmpl	$2, %edx
	je	.L2287
.L1793:
	movq	16(%r15), %rdi
	movq	%rbx, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1766
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2288
.L1795:
	cmpq	$0, -232(%rbp)
	jne	.L1870
.L2178:
	movq	%r13, -232(%rbp)
.L1766:
	testq	%r14, %r14
	je	.L1800
	movq	%r14, %r13
	movq	(%r14), %r14
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1765:
	movq	(%rbx), %rbx
	movl	20(%rbx), %r8d
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	movl	12(%r9,%rdx), %edx
	testl	%edx, %edx
	je	.L1766
	leaq	16(%rdi,%rax), %r9
	jmp	.L1767
.L2286:
	cmpl	$3, %edx
	je	.L2289
	cmpl	$2, %edx
	jne	.L1793
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2290
.L1794:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%r13), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r8
	movq	8(%r15), %rax
	movq	%r8, %r12
	movq	224(%rax), %rbx
	jmp	.L1796
.L2291:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L1796:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2291
.L2161:
	movq	%rax, %r13
	jmp	.L1795
.L1791:
	cmpq	$0, -232(%rbp)
	movq	-240(%rbp), %r14
	je	.L2178
	testq	%r13, %r13
	je	.L1766
.L1870:
	movq	-232(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, -232(%rbp)
	jmp	.L1766
.L2288:
	movq	(%rbx), %rax
	movl	20(%rbx), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r13), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1795
.L2281:
	movq	(%r14), %rax
	movl	20(%r14), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r13), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1640
.L2198:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1837
.L2252:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC5(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r15), %rcx
	jmp	.L1669
.L2254:
	movq	8(%r9), %rcx
	movl	%esi, %edx
	leaq	.LC5(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r14), %rdx
	jmp	.L1825
.L2256:
	movq	-160(%rbp), %rax
	movq	-200(%rbp), %rbx
	leaq	.LC29(%rip), %rdi
	movq	160(%rax), %rax
	movl	20(%rbx), %esi
	movq	%rax, -152(%rbp)
	movq	(%rbx), %rax
	andl	$16777215, %esi
	movl	-152(%rbp), %ecx
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1693
.L2262:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2292
.L1651:
	movq	(%r12), %r14
	movq	224(%rcx), %rbx
	jmp	.L1652
	.p2align 4,,10
	.p2align 3
.L2293:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
.L1652:
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2293
	jmp	.L2150
.L2259:
	movq	(%rbx), %rax
	movl	20(%rbx), %esi
	leaq	.LC33(%rip), %rdi
	movq	160(%r15), %rcx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1851
.L2269:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2294
.L1806:
	movq	(%r12), %r13
	movq	224(%rcx), %rbx
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L2295:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L1807:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2295
	jmp	.L2163
.L2228:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC4(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1666
.L2258:
	leaq	-128(%rbp), %rcx
	subq	%r8, %rdx
	addq	$192, %rdi
	call	_ZNSt6vectorIN2v88internal8compiler9Scheduler13SchedulerDataENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	movq	8(%r14), %rax
	movq	200(%rax), %rcx
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	jmp	.L1850
.L2257:
	movq	-200(%rbp), %rdi
	movl	20(%rax), %ecx
	movq	(%rdi), %rax
	andl	$16777215, %ecx
	movq	8(%rax), %rdx
	movl	20(%rdi), %eax
	leaq	.LC32(%rip), %rdi
	movl	%eax, %esi
	movl	%eax, -184(%rbp)
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1848
.L2217:
	movq	8(%r9), %rcx
	movl	%esi, %edx
	leaq	.LC4(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1822
.L1730:
	cmpq	%r8, %rsi
	je	.L1731
	leaq	8(%rcx), %rdi
	movq	%rcx, -168(%rbp)
	subq	%rdx, %rdi
	movq	%r9, -152(%rbp)
	addq	%r9, %rdi
	call	memmove@PLT
	movq	-152(%rbp), %r9
	movq	-168(%rbp), %rcx
	jmp	.L1731
.L2282:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2296
.L1631:
	movq	24(%r14), %rsi
	testq	%rsi, %rsi
	je	.L1627
	movq	(%rsi), %r14
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1637:
	movl	16(%rsi), %ecx
	movq	8(%r15), %rdi
	movl	%ecx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rdx
	salq	$3, %rax
	andl	$1, %ecx
	leaq	(%rsi,%rdx,8), %rdx
	leaq	32(%rdx,%rax), %r8
	jne	.L1634
	leaq	16(%rdx,%rax), %r8
	movq	(%rdx), %rdx
.L1634:
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	200(%rdi), %rax
	movl	12(%rax), %edx
	testl	%edx, %edx
	je	.L1635
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler23ScheduleLateNodeVisitor14GetBlockForUseENS1_4EdgeE
	testq	%r13, %r13
	je	.L1886
	testq	%rax, %rax
	je	.L1635
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L1635:
	testq	%r14, %r14
	je	.L1636
	movq	%r14, %rsi
	movq	(%r14), %r14
	jmp	.L1637
.L1886:
	movq	%rax, %r13
	jmp	.L1635
.L2260:
	movq	8(%rdi), %rdx
	movl	%r8d, %esi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1617
.L2267:
	movq	8(%rdi), %rdx
	movl	%r8d, %esi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1756
.L2289:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2297
.L1770:
	movq	24(%rbx), %r9
	testq	%r9, %r9
	je	.L1766
	movq	%r14, -240(%rbp)
	movq	(%r9), %r12
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1792:
	movl	16(%r9), %edx
	movl	%edx, %esi
	shrl	%esi
	movl	%esi, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%r9,%rcx,8), %rbx
	movq	8(%r15), %rcx
	movq	%rbx, %rdi
	movq	200(%rcx), %r10
	je	.L1772
	movl	20(%rbx), %r8d
	leaq	32(%rbx,%rax), %r14
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	movl	12(%r10,%rdx), %edx
	testl	%edx, %edx
	je	.L1773
.L1774:
	movq	(%rbx), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r11d
	cmpl	$1, %r11d
	jbe	.L2298
	cmpl	$10, %eax
	sete	%r9b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r9b
	je	.L1784
	cmpl	$2, %edx
	je	.L2299
.L1784:
	movq	16(%r15), %rdi
	movq	%rbx, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1773
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2300
.L1786:
	testq	%r13, %r13
	jne	.L1869
.L2177:
	movq	%r14, %r13
.L1773:
	testq	%r12, %r12
	je	.L1791
	movq	%r12, %r9
	movq	(%r12), %r12
	jmp	.L1792
.L1772:
	movq	(%rbx), %rbx
	movl	20(%rbx), %r8d
	andl	$16777215, %r8d
	movl	%r8d, %edx
	salq	$4, %rdx
	movl	12(%r10,%rdx), %edx
	testl	%edx, %edx
	je	.L1773
	leaq	16(%rdi,%rax), %r14
	jmp	.L1774
.L2298:
	cmpl	$3, %edx
	je	.L2301
	cmpl	$2, %edx
	jne	.L1784
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2302
.L1785:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	movq	%r9, -248(%rbp)
	movq	%r12, %r14
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-248(%rbp), %r9
	movq	%rax, %rdi
	movl	16(%r9), %esi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r9
	movq	8(%r15), %rax
	movq	%r9, %r12
	movq	224(%rax), %rbx
	jmp	.L1787
.L2303:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L1787:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2303
.L2159:
	movq	%r14, %r12
	movq	%rax, %r14
	jmp	.L1786
.L1782:
	testq	%r13, %r13
	je	.L2177
	testq	%r14, %r14
	je	.L1773
.L1869:
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, %r13
	jmp	.L1773
.L2221:
	movl	$512, %esi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-152(%rbp), %r8
	jmp	.L1737
.L2300:
	movq	(%rbx), %rax
	movl	20(%rbx), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r14), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1786
.L2274:
	movq	8(%rdi), %rcx
	movl	%r8d, %edx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r15), %rcx
	jmp	.L1660
.L2276:
	movq	8(%rdi), %rcx
	movl	%r8d, %edx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r15), %rcx
	jmp	.L1815
.L2249:
	movq	8(%rdi), %rcx
	movl	%r8d, %edx
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1812
.L2246:
	movq	8(%rdi), %rcx
	movl	%r8d, %edx
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1657
.L2280:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2304
.L1642:
	movq	0(%r13), %r9
	movq	224(%rcx), %r14
	movq	%rbx, %r13
	movq	%r12, %rbx
	movq	%r9, %r12
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L2305:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L1643:
	movq	16(%r14), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2305
	jmp	.L2148
.L2287:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2306
.L1797:
	movq	(%r9), %r8
	movq	224(%rcx), %rbx
	movq	%r8, %r12
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L2307:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L1798:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2307
	jmp	.L2161
.L1852:
	leaq	40(%r12), %rsi
	leaq	96(%rax), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	40(%r12), %r13
	jmp	.L1840
.L2285:
	movq	8(%rdi), %rdx
	movl	%r8d, %esi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1763
.L2278:
	movq	8(%rdi), %rdx
	movl	%r8d, %esi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1624
.L2231:
	movq	%r10, -176(%rbp)
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-168(%rbp), %r9
	movq	-176(%rbp), %r10
	movq	%rax, %rcx
	jmp	.L1707
.L2301:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2308
.L1777:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1773
	movq	(%rsi), %rbx
	xorl	%r14d, %r14d
.L1783:
	movl	16(%rsi), %ecx
	movq	8(%r15), %rdi
	movl	%ecx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rdx
	salq	$3, %rax
	andl	$1, %ecx
	leaq	(%rsi,%rdx,8), %rdx
	leaq	32(%rdx,%rax), %r8
	jne	.L1780
	leaq	16(%rdx,%rax), %r8
	movq	(%rdx), %rdx
.L1780:
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	200(%rdi), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	je	.L1781
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler23ScheduleLateNodeVisitor14GetBlockForUseENS1_4EdgeE
	testq	%r14, %r14
	je	.L1887
	testq	%rax, %rax
	je	.L1781
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, %r14
.L1781:
	testq	%rbx, %rbx
	je	.L1782
	movq	%rbx, %rsi
	movq	(%rbx), %rbx
	jmp	.L1783
.L1887:
	movq	%rax, %r14
	jmp	.L1781
.L2292:
	movq	8(%rdi), %rcx
	movl	%r8d, %edx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r15), %rcx
	jmp	.L1651
.L2294:
	movq	8(%rdi), %rcx
	movl	%r8d, %edx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r15), %rcx
	jmp	.L1806
.L2299:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2309
.L1788:
	movq	(%r14), %r9
	movq	224(%rcx), %rbx
	movq	%r12, %r14
	movq	%r9, %r12
	jmp	.L1789
.L2310:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L1789:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L2310
	jmp	.L2159
.L2272:
	movq	8(%rdi), %rcx
	movl	%r8d, %edx
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1803
.L2265:
	movq	8(%rdi), %rcx
	movl	%r8d, %edx
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1648
.L2296:
	movq	8(%rdi), %rdx
	movl	%r8d, %esi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1631
.L2297:
	movq	8(%rdi), %rdx
	movl	%r8d, %esi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1770
.L2290:
	movq	8(%rdi), %rcx
	movl	%r8d, %edx
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1794
.L2283:
	movq	8(%rdi), %rcx
	movl	%r8d, %edx
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%r9, -240(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-240(%rbp), %r9
	jmp	.L1639
.L2306:
	movq	8(%rdi), %rcx
	movl	%r8d, %edx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movq	%r9, -240(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r15), %rcx
	movq	-240(%rbp), %r9
	jmp	.L1797
.L2304:
	movq	8(%rdi), %rcx
	movl	%r8d, %edx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r15), %rcx
	jmp	.L1642
.L2243:
	movq	%r11, -176(%rbp)
	movq	%r10, -168(%rbp)
	movq	%rcx, -152(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-152(%rbp), %rcx
	movq	-168(%rbp), %r10
	movq	-176(%rbp), %r11
	movq	%rax, %r8
	jmp	.L1733
.L2308:
	movq	8(%rdi), %rdx
	movl	%r8d, %esi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1777
.L2309:
	movq	8(%rdi), %rcx
	movl	%r8d, %edx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r15), %rcx
	jmp	.L1788
.L2302:
	movq	8(%rdi), %rcx
	movl	%r8d, %edx
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-248(%rbp), %r9
	jmp	.L1785
.L2189:
	call	__stack_chk_fail@PLT
.L1727:
	leaq	.LC19(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11001:
	.size	_ZN2v88internal8compiler23ScheduleLateNodeVisitor9SplitNodeEPNS1_10BasicBlockEPNS1_4NodeE, .-_ZN2v88internal8compiler23ScheduleLateNodeVisitor9SplitNodeEPNS1_10BasicBlockEPNS1_4NodeE
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag.str1.1,"aMS",@progbits,1
.LC34:
	.string	"vector::_M_range_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag:
.LFB13220:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rcx
	je	.L2413
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	subq	%rdx, %r14
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	movq	%r14, %rax
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	sarq	$3, %rax
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	24(%rbx), %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%rax, %rdx
	jb	.L2314
	movq	%rdi, %r8
	subq	%rsi, %r8
	movq	%r8, %r9
	sarq	$3, %r9
	cmpq	%r9, %rax
	jnb	.L2315
	movl	$16, %ecx
	movq	%rdi, %rdx
	leaq	-8(%r14), %rax
	subq	%r14, %rcx
	subq	%r14, %rdx
	shrq	$3, %rax
	testq	%rcx, %rcx
	leaq	16(%rdi), %rcx
	setle	%sil
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L2353
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L2353
	leaq	1(%rax), %r8
	xorl	%eax, %eax
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L2317:
	movdqu	(%rdx,%rax), %xmm1
	movups	%xmm1, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L2317
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	leaq	(%rdx,%rcx), %rsi
	addq	%rdi, %rcx
	cmpq	%rax, %r8
	je	.L2319
	movq	(%rsi), %rax
	movq	%rax, (%rcx)
.L2319:
	addq	%r14, 16(%rbx)
	cmpq	%r12, %rdx
	je	.L2320
	subq	%r12, %rdx
	movq	%r12, %rsi
	subq	%rdx, %rdi
	call	memmove@PLT
.L2320:
	movq	%r14, %rdx
.L2416:
	addq	$16, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L2314:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	movl	$268435455, %r14d
	movq	%r14, %rdx
	subq	%rsi, %rdi
	sarq	$3, %rdi
	subq	%rdi, %rdx
	cmpq	%rdx, %rax
	ja	.L2417
	cmpq	%rdi, %rax
	cmovb	%rdi, %rax
	addq	%rdi, %rax
	jc	.L2334
	testq	%rax, %rax
	jne	.L2418
	xorl	%edi, %edi
	xorl	%eax, %eax
.L2336:
	cmpq	%rsi, %r12
	je	.L2359
	leaq	-8(%r12), %r10
	leaq	15(%rax), %rdx
	subq	%rsi, %r10
	subq	%rsi, %rdx
	movq	%r10, %r9
	shrq	$3, %r9
	cmpq	$30, %rdx
	jbe	.L2360
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r9
	je	.L2360
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %r8
	shrq	%r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L2341:
	movdqu	(%rsi,%rdx), %xmm5
	movups	%xmm5, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%r8, %rdx
	jne	.L2341
	movq	%r9, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %r8
	addq	%r8, %rsi
	addq	%rax, %r8
	cmpq	%rdx, %r9
	je	.L2343
	movq	(%rsi), %rdx
	movq	%rdx, (%r8)
.L2343:
	leaq	8(%rax,%r10), %rsi
.L2339:
	subq	$8, %rcx
	leaq	15(%r13), %rdx
	subq	%r13, %rcx
	subq	%rsi, %rdx
	movq	%rcx, %r8
	shrq	$3, %r8
	cmpq	$30, %rdx
	jbe	.L2361
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r8
	je	.L2361
	leaq	1(%r8), %r10
	xorl	%edx, %edx
	movq	%r10, %r9
	shrq	%r9
	salq	$4, %r9
	.p2align 4,,10
	.p2align 3
.L2345:
	movdqu	0(%r13,%rdx), %xmm2
	movups	%xmm2, (%rsi,%rdx)
	addq	$16, %rdx
	cmpq	%r9, %rdx
	jne	.L2345
	movq	%r10, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %r8
	addq	%r8, %r13
	addq	%rsi, %r8
	cmpq	%rdx, %r10
	je	.L2347
	movq	0(%r13), %rdx
	movq	%rdx, (%r8)
.L2347:
	movq	16(%rbx), %rdx
	leaq	8(%rsi,%rcx), %r8
	cmpq	%rdx, %r12
	je	.L2348
	subq	%r12, %rdx
	leaq	-8(%rdx), %r10
	leaq	24(%rsi,%rcx), %rdx
	movq	%r10, %r9
	shrq	$3, %r9
	cmpq	%rdx, %r12
	leaq	16(%r12), %rdx
	setnb	%cl
	cmpq	%rdx, %r8
	setnb	%dl
	orb	%dl, %cl
	je	.L2362
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r9
	je	.L2362
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L2350:
	movdqu	(%r12,%rdx), %xmm6
	movups	%xmm6, (%r8,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L2350
	movq	%r9, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %r12
	addq	%r8, %rcx
	cmpq	%rdx, %r9
	je	.L2352
	movq	(%r12), %rdx
	movq	%rdx, (%rcx)
.L2352:
	leaq	8(%r8,%r10), %r8
.L2348:
	movq	%rax, %xmm0
	movq	%r8, %xmm7
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 8(%rbx)
.L2311:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2315:
	.cfi_restore_state
	leaq	0(%r13,%r8), %rsi
	cmpq	%rsi, %rcx
	je	.L2354
	subq	$8, %rcx
	leaq	16(%r8,%r13), %rdx
	subq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	%rdx, %rdi
	leaq	16(%rdi), %rdx
	setnb	%r10b
	cmpq	%rdx, %rsi
	setnb	%dl
	orb	%dl, %r10b
	je	.L2355
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L2355
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %r10
	shrq	%r10
	salq	$4, %r10
	.p2align 4,,10
	.p2align 3
.L2323:
	movdqu	(%rsi,%rdx), %xmm3
	movups	%xmm3, (%rdi,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %r10
	jne	.L2323
	movq	%rcx, %r11
	andq	$-2, %r11
	leaq	0(,%r11,8), %r10
	leaq	(%rsi,%r10), %rdx
	addq	%rdi, %r10
	cmpq	%r11, %rcx
	je	.L2325
	movq	(%rdx), %rdx
	movq	%rdx, (%r10)
.L2325:
	movq	16(%rbx), %r10
.L2321:
	movq	%rax, %rdx
	subq	%r9, %rdx
	leaq	(%r10,%rdx,8), %rdx
	movq	%rdx, 16(%rbx)
	cmpq	%r12, %rdi
	je	.L2326
	subq	%r12, %rdi
	subq	%r9, %rax
	leaq	-8(%rdi), %rcx
	leaq	16(%r10,%rax,8), %rax
	shrq	$3, %rcx
	cmpq	%rax, %r12
	leaq	16(%r12), %rax
	setnb	%dil
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %dil
	je	.L2356
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L2356
	addq	$1, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L2328:
	movdqu	(%r12,%rax), %xmm4
	movups	%xmm4, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L2328
	movq	%rcx, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rax
	leaq	(%r12,%rax), %rdi
	addq	%rax, %rdx
	cmpq	%r9, %rcx
	je	.L2330
	movq	(%rdi), %rax
	movq	%rax, (%rdx)
.L2330:
	movq	16(%rbx), %rdx
.L2326:
	addq	%r8, %rdx
	movq	%rdx, 16(%rbx)
	cmpq	%rsi, %r13
	je	.L2311
	movq	%r8, %rdx
	jmp	.L2416
	.p2align 4,,10
	.p2align 3
.L2413:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L2353:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L2316:
	movq	(%rdx,%rcx,8), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rsi, %rax
	jne	.L2316
	jmp	.L2319
	.p2align 4,,10
	.p2align 3
.L2355:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2322:
	movq	(%rsi,%rdx,8), %r10
	movq	%r10, (%rdi,%rdx,8)
	movq	%rdx, %r10
	addq	$1, %rdx
	cmpq	%r10, %rcx
	jne	.L2322
	jmp	.L2325
	.p2align 4,,10
	.p2align 3
.L2360:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2340:
	movq	(%rsi,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %r9
	jne	.L2340
	jmp	.L2343
	.p2align 4,,10
	.p2align 3
.L2361:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2344:
	movq	0(%r13,%rdx,8), %r9
	movq	%r9, (%rsi,%rdx,8)
	movq	%rdx, %r9
	addq	$1, %rdx
	cmpq	%r9, %r8
	jne	.L2344
	jmp	.L2347
	.p2align 4,,10
	.p2align 3
.L2356:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2327:
	movq	(%r12,%rax,8), %rdi
	movq	%rdi, (%rdx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rcx, %rdi
	jne	.L2327
	jmp	.L2330
	.p2align 4,,10
	.p2align 3
.L2362:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2349:
	movq	(%r12,%rdx,8), %rcx
	movq	%rcx, (%r8,%rdx,8)
	movq	%rdx, %rcx
	addq	$1, %rdx
	cmpq	%r9, %rcx
	jne	.L2349
	jmp	.L2352
	.p2align 4,,10
	.p2align 3
.L2354:
	movq	%rdi, %r10
	jmp	.L2321
	.p2align 4,,10
	.p2align 3
.L2359:
	movq	%rax, %rsi
	jmp	.L2339
.L2418:
	cmpq	$268435455, %rax
	cmova	%r14, %rax
	leaq	0(,%rax,8), %r14
	movq	%r14, %rsi
.L2335:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L2419
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2338:
	movq	8(%rbx), %rsi
	leaq	(%rax,%r14), %rdi
	jmp	.L2336
.L2419:
	movq	%rcx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rcx
	jmp	.L2338
.L2334:
	movl	$2147483640, %esi
	movl	$2147483640, %r14d
	jmp	.L2335
.L2417:
	leaq	.LC34(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13220:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag
	.section	.rodata._ZN2v88internal8compiler9Scheduler16MovePlannedNodesEPNS1_10BasicBlockES4_.str1.8,"aMS",@progbits,1
	.align 8
.LC35:
	.string	"Move planned nodes from id:%d to id:%d\n"
	.section	.text._ZN2v88internal8compiler9Scheduler16MovePlannedNodesEPNS1_10BasicBlockES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler16MovePlannedNodesEPNS1_10BasicBlockES4_
	.type	_ZN2v88internal8compiler9Scheduler16MovePlannedNodesEPNS1_10BasicBlockES4_, @function
_ZN2v88internal8compiler9Scheduler16MovePlannedNodesEPNS1_10BasicBlockES4_:
.LFB11033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rsi, -64(%rbp)
	movq	160(%rdx), %rdx
	movq	160(%rsi), %rsi
	jne	.L2434
.L2421:
	movq	40(%r14), %rax
	movq	(%rax,%rsi,8), %r13
	leaq	(%rax,%rdx,8), %rax
	testq	%r13, %r13
	je	.L2420
	movq	(%rax), %rax
	movq	16(%r13), %r12
	movq	8(%r13), %r15
	movq	%rax, -56(%rbp)
	cmpq	%r15, %r12
	je	.L2428
	.p2align 4,,10
	.p2align 3
.L2427:
	movq	(%r15), %rdx
	movq	16(%r14), %rdi
	movq	%rbx, %rsi
	addq	$8, %r15
	call	_ZN2v88internal8compiler8Schedule15SetBlockForNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	cmpq	%r15, %r12
	jne	.L2427
.L2428:
	cmpq	$0, -56(%rbp)
	je	.L2435
	movq	-56(%rbp), %rdi
	movq	16(%r13), %rcx
	movq	8(%r13), %rdx
	movq	16(%rdi), %rsi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag
	movq	8(%r13), %rax
	cmpq	16(%r13), %rax
	je	.L2420
	movq	%rax, 16(%r13)
.L2420:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2435:
	.cfi_restore_state
	movq	-64(%rbp), %rcx
	movq	40(%r14), %rdx
	movq	160(%rbx), %rax
	movq	160(%rcx), %rcx
	leaq	(%rdx,%rax,8), %rax
	leaq	(%rdx,%rcx,8), %rdx
	movq	(%rax), %rsi
	movq	(%rdx), %rcx
	movq	%rsi, (%rdx)
	movq	%rcx, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2434:
	.cfi_restore_state
	xorl	%eax, %eax
	leaq	.LC35(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-64(%rbp), %rax
	movq	160(%rbx), %rdx
	movq	160(%rax), %rsi
	jmp	.L2421
	.cfi_endproc
.LFE11033:
	.size	_ZN2v88internal8compiler9Scheduler16MovePlannedNodesEPNS1_10BasicBlockES4_, .-_ZN2v88internal8compiler9Scheduler16MovePlannedNodesEPNS1_10BasicBlockES4_
	.section	.text._ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB13696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$6, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L2450
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L2438:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L2439
	movq	%r15, %rbx
	jmp	.L2444
	.p2align 4,,10
	.p2align 3
.L2440:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L2451
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2441:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L2439
.L2444:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L2440
	cmpq	$63, 8(%rax)
	jbe	.L2440
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L2444
.L2439:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$63, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,8), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2451:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2441
	.p2align 4,,10
	.p2align 3
.L2450:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2438
	.cfi_endproc
.LFE13696:
	.size	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZN2v88internal9ZoneQueueIPNS0_8compiler4NodeEEC2EPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal9ZoneQueueIPNS0_8compiler4NodeEEC5EPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9ZoneQueueIPNS0_8compiler4NodeEEC2EPNS0_4ZoneE
	.type	_ZN2v88internal9ZoneQueueIPNS0_8compiler4NodeEEC2EPNS0_4ZoneE, @function
_ZN2v88internal9ZoneQueueIPNS0_8compiler4NodeEEC2EPNS0_4ZoneE:
.LFB11878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-224(%rbp), %rdi
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -224(%rbp)
	xorl	%esi, %esi
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -208(%rbp)
	je	.L2469
	movdqa	-224(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	movaps	%xmm0, -48(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movq	-184(%rbp), %r9
	movq	-112(%rbp), %rsi
	movdqa	-64(%rbp), %xmm5
	movq	-168(%rbp), %r8
	movq	-192(%rbp), %xmm3
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %xmm1
	movaps	%xmm5, -160(%rbp)
	movq	%r9, %xmm5
	movq	-176(%rbp), %xmm2
	punpcklqdq	%xmm5, %xmm3
	movq	-136(%rbp), %rdx
	movq	-104(%rbp), %rdi
	movups	%xmm3, 32(%rbx)
	movq	%r8, %xmm3
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	punpcklqdq	%xmm3, %xmm2
	movq	-216(%rbp), %rax
	movq	-144(%rbp), %xmm0
	movups	%xmm2, 48(%rbx)
	movq	%rcx, %xmm2
	movq	-224(%rbp), %xmm4
	movq	-208(%rbp), %r11
	punpcklqdq	%xmm2, %xmm1
	movq	-200(%rbp), %r10
	movaps	%xmm6, -192(%rbp)
	movdqa	-48(%rbp), %xmm6
	movaps	%xmm7, -176(%rbp)
	movq	%rax, %xmm7
	movups	%xmm1, 64(%rbx)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm7, %xmm4
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -208(%rbp)
	movq	%rdi, -200(%rbp)
	movq	%r11, 16(%rbx)
	movq	%r10, 24(%rbx)
	movaps	%xmm6, -144(%rbp)
	movups	%xmm4, (%rbx)
	movups	%xmm0, 80(%rbx)
	testq	%rsi, %rsi
	je	.L2452
	movq	-136(%rbp), %rcx
	movq	-168(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L2455
	.p2align 4,,10
	.p2align 3
.L2458:
	testq	%rax, %rax
	je	.L2456
	cmpq	$64, 8(%rax)
	ja	.L2457
.L2456:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-216(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -216(%rbp)
.L2457:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L2458
	movq	-200(%rbp), %rdi
	movq	-208(%rbp), %rsi
.L2455:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L2452
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L2452:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2470
	addq	$216, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2469:
	.cfi_restore_state
	movdqa	-224(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm7
	movq	$0, 16(%rbx)
	movq	-200(%rbp), %rax
	movdqa	-144(%rbp), %xmm1
	movups	%xmm6, (%rbx)
	movdqa	-176(%rbp), %xmm6
	movups	%xmm7, 32(%rbx)
	movdqa	-160(%rbp), %xmm7
	movq	%rax, 24(%rbx)
	movups	%xmm6, 48(%rbx)
	movups	%xmm7, 64(%rbx)
	movups	%xmm1, 80(%rbx)
	jmp	.L2452
.L2470:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11878:
	.size	_ZN2v88internal9ZoneQueueIPNS0_8compiler4NodeEEC2EPNS0_4ZoneE, .-_ZN2v88internal9ZoneQueueIPNS0_8compiler4NodeEEC2EPNS0_4ZoneE
	.weak	_ZN2v88internal9ZoneQueueIPNS0_8compiler4NodeEEC1EPNS0_4ZoneE
	.set	_ZN2v88internal9ZoneQueueIPNS0_8compiler4NodeEEC1EPNS0_4ZoneE,_ZN2v88internal9ZoneQueueIPNS0_8compiler4NodeEEC2EPNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler9SchedulerC2EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE.str1.1,"aMS",@progbits,1
.LC36:
	.string	"vector::reserve"
	.section	.text._ZN2v88internal8compiler9SchedulerC2EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9SchedulerC2EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE
	.type	_ZN2v88internal8compiler9SchedulerC2EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE, @function
_ZN2v88internal8compiler9SchedulerC2EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE:
.LFB10872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$96, %rdi
	subq	$40, %rsp
	movq	16(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -80(%rdi)
	movl	%r8d, -72(%rdi)
	movq	%rsi, -64(%rdi)
	movq	$0, -56(%rdi)
	movq	$0, -48(%rdi)
	movq	$0, -40(%rdi)
	movq	%rsi, -32(%rdi)
	movq	$0, -24(%rdi)
	movq	$0, -16(%rdi)
	movq	$0, -8(%rdi)
	movups	%xmm0, -96(%rdi)
	call	_ZN2v88internal9ZoneQueueIPNS0_8compiler4NodeEEC1EPNS0_4ZoneE
	movq	%r12, 192(%rbx)
	movq	$0, 200(%rbx)
	movq	$0, 208(%rbx)
	movq	$0, 216(%rbx)
	movq	%r15, 248(%rbx)
	cmpq	$134217727, %r13
	ja	.L2484
	xorl	%esi, %esi
	testq	%r13, %r13
	jne	.L2485
.L2473:
	movq	16(%rbx), %rax
	movl	28(%r14), %edx
	movq	$0, -72(%rbp)
	movq	104(%rax), %rax
	movq	%rax, -80(%rbp)
	testq	%rdx, %rdx
	jne	.L2486
.L2471:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2487
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2485:
	.cfi_restore_state
	movq	16(%r12), %rsi
	movq	24(%r12), %rax
	salq	$4, %r13
	movq	%r13, %r8
	subq	%rsi, %rax
	cmpq	%rax, %r13
	ja	.L2488
	addq	%rsi, %r8
	movq	%r8, 16(%r12)
.L2475:
	addq	%rsi, %r13
	movq	%rsi, 200(%rbx)
	movq	%rsi, 208(%rbx)
	movq	%r13, 216(%rbx)
	jmp	.L2473
	.p2align 4,,10
	.p2align 3
.L2486:
	leaq	-80(%rbp), %rcx
	leaq	192(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler9Scheduler13SchedulerDataENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	jmp	.L2471
	.p2align 4,,10
	.p2align 3
.L2488:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L2475
.L2484:
	leaq	.LC36(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2487:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10872:
	.size	_ZN2v88internal8compiler9SchedulerC2EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE, .-_ZN2v88internal8compiler9SchedulerC2EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE
	.globl	_ZN2v88internal8compiler9SchedulerC1EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE
	.set	_ZN2v88internal8compiler9SchedulerC1EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE,_ZN2v88internal8compiler9SchedulerC2EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE
	.section	.rodata._ZN2v88internal8compiler9Scheduler8BuildCFGEv.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"--- CREATING CFG -------------------------------------------\n"
	.align 8
.LC38:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal8compiler9Scheduler8BuildCFGEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler8BuildCFGEv
	.type	_ZN2v88internal8compiler9Scheduler8BuildCFGEv, @function
_ZN2v88internal8compiler9Scheduler8BuildCFGEv:
.LFB10916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2595
.L2490:
	movq	(%r15), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L2596
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdi)
.L2492:
	movq	8(%r15), %rax
	movdqu	(%r15), %xmm2
	movabsq	$4294967296, %rcx
	movq	(%r15), %r8
	movq	%rcx, 16(%rbx)
	movl	28(%rax), %eax
	movups	%xmm2, (%rbx)
	cmpq	$268435455, %rax
	ja	.L2597
	movq	%r8, 24(%rbx)
	leaq	0(,%rax,8), %rdx
	xorl	%r12d, %r12d
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	testq	%rax, %rax
	je	.L2561
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	movq	%rdx, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L2598
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L2496:
	leaq	(%rdi,%rdx), %r12
	movq	%rdi, 32(%rbx)
	xorl	%esi, %esi
	movq	%r12, 48(%rbx)
	call	memset@PLT
.L2561:
	movq	%r12, 40(%rbx)
	movq	(%r15), %rdi
	movq	%rbx, 240(%r15)
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$183, %rax
	jbe	.L2599
	leaq	184(%r13), %rax
	movq	%rax, 16(%rdi)
.L2498:
	movq	(%r15), %rbx
	movq	16(%r15), %rax
	movq	%r15, 8(%r13)
	leaq	24(%r13), %rdi
	movq	8(%r15), %rsi
	movl	$2, %edx
	movq	%rax, 16(%r13)
	movq	%rbx, 0(%r13)
	call	_ZN2v88internal8compiler14NodeMarkerBaseC2EPNS1_5GraphEj@PLT
	leaq	32(%r13), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal9ZoneQueueIPNS0_8compiler4NodeEEC1EPNS0_4ZoneE
	movq	%rbx, 128(%r13)
	movq	$0, 136(%r13)
	movq	$0, 144(%r13)
	movq	$0, 152(%r13)
	movq	$0, 160(%r13)
	movq	$0, 168(%r13)
	movq	$0, 176(%r13)
	movq	%r13, 224(%r15)
	movq	136(%r13), %rax
	cmpq	144(%r13), %rax
	je	.L2499
	movq	%rax, 144(%r13)
.L2499:
	movq	8(%r13), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rax
	movq	16(%rax), %rsi
	call	_ZN2v88internal8compiler10CFGBuilder5QueueEPNS1_4NodeE
	movq	64(%r13), %rax
	cmpq	%rax, 96(%r13)
	je	.L2500
	leaq	-64(%rbp), %rax
	movq	%r15, -128(%rbp)
	movq	%r13, %r15
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L2506:
	movq	8(%r15), %rax
	movq	248(%rax), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	80(%r15), %rcx
	movq	64(%r15), %rax
	leaq	-8(%rcx), %rdx
	movq	(%rax), %r12
	cmpq	%rdx, %rax
	je	.L2501
	addq	$8, %rax
	movq	%rax, 64(%r15)
.L2502:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties16PastControlIndexEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE@PLT
	cmpl	%eax, %ebx
	jle	.L2537
	subl	$1, %ebx
	movslq	%eax, %rdx
	leaq	32(%r12), %rcx
	movq	%r12, -112(%rbp)
	subl	%eax, %ebx
	movq	%rcx, -96(%rbp)
	leaq	(%r12,%rdx,8), %r13
	addq	%rdx, %rbx
	leaq	8(%r12,%rbx,8), %rax
	movq	%rax, -88(%rbp)
	leaq	128(%r15), %rax
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L2538:
	movq	-112(%rbp), %rax
	movq	%r13, %rdx
	subq	%rax, %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2507
	leaq	32(%r13), %rax
.L2508:
	movq	(%rax), %r12
	movl	24(%r15), %eax
	movq	%r12, -64(%rbp)
	cmpl	%eax, 16(%r12)
	ja	.L2509
	movq	(%r12), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$49, %ax
	ja	.L2510
	leaq	.L2513(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler9Scheduler8BuildCFGEv,"a",@progbits
	.align 4
	.align 4
.L2513:
	.long	.L2518-.L2513
	.long	.L2516-.L2513
	.long	.L2517-.L2513
	.long	.L2517-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2516-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2515-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2514-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2511-.L2513
	.long	.L2512-.L2513
	.section	.text._ZN2v88internal8compiler9Scheduler8BuildCFGEv
	.p2align 4,,10
	.p2align 3
.L2510:
	subw	$682, %ax
	cmpw	$106, %ax
	ja	.L2511
.L2512:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_@PLT
	testb	%al, %al
	jne	.L2600
.L2511:
	movq	112(%r15), %rax
	movq	96(%r15), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2534
	movq	-64(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 96(%r15)
.L2535:
	movl	24(%r15), %eax
	movq	-64(%rbp), %rdx
	addl	$1, %eax
	movl	%eax, 16(%rdx)
	movq	144(%r15), %rsi
	cmpq	152(%r15), %rsi
	je	.L2536
	movq	%rdx, (%rsi)
	addq	$8, 144(%r15)
.L2509:
	addq	$8, %r13
	cmpq	%r13, -88(%rbp)
	jne	.L2538
.L2537:
	movq	64(%r15), %rax
	cmpq	%rax, 96(%r15)
	jne	.L2506
	movq	%r15, %r13
	movq	-128(%rbp), %r15
.L2500:
	movq	136(%r13), %rbx
	cmpq	144(%r13), %rbx
	je	.L2547
	.p2align 4,,10
	.p2align 3
.L2546:
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler10CFGBuilder13ConnectBlocksEPNS1_4NodeE
	cmpq	%rbx, 144(%r13)
	jne	.L2546
.L2547:
	movq	16(%r15), %rax
	movq	24(%rax), %rbx
	movq	%rbx, %rcx
	subq	16(%rax), %rcx
	movq	%rcx, %rsi
	sarq	$3, %rsi
	js	.L2540
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L2541:
	mulsd	.LC39(%rip), %xmm0
	movsd	.LC40(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L2542
	cvttsd2siq	%xmm0, %rdx
.L2543:
	cmpq	$268435455, %rdx
	ja	.L2601
	movq	40(%r15), %rbx
	movq	56(%r15), %rax
	movq	48(%r15), %r12
	subq	%rbx, %rax
	movq	%r12, %r13
	sarq	$3, %rax
	subq	%rbx, %r13
	cmpq	%rax, %rdx
	ja	.L2602
.L2548:
	sarq	$3, %r13
	cmpq	%rsi, %r13
	jb	.L2603
	jbe	.L2489
	addq	%rcx, %rbx
	cmpq	%r12, %rbx
	je	.L2489
	movq	%rbx, 48(%r15)
.L2489:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2604
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2518:
	.cfi_restore_state
	movq	16(%r15), %rdi
	movq	104(%rdi), %rsi
.L2593:
	movq	%r12, %rdx
.L2594:
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	8(%r15), %rdi
	movl	$2, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	jmp	.L2511
.L2514:
	movq	16(%r15), %rdi
	movq	112(%rdi), %rsi
	jmp	.L2593
.L2515:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	16(%r15), %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2605
.L2521:
	movq	16(%r15), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	jmp	.L2594
.L2517:
	movslq	40(%rdx), %r14
	movq	(%r15), %rdi
	leaq	0(,%r14,8), %rax
	movq	16(%rdi), %rbx
	movq	%rax, -80(%rbp)
	movq	%rax, %rsi
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	%rax, %rsi
	ja	.L2606
	addq	%rbx, %rsi
	movq	%rsi, 16(%rdi)
.L2524:
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m@PLT
	testq	%r14, %r14
	je	.L2511
	movq	-80(%rbp), %rax
	addq	%rbx, %rax
	movq	%rax, -80(%rbp)
	jmp	.L2528
	.p2align 4,,10
	.p2align 3
.L2526:
	addq	$8, %rbx
	cmpq	%rbx, -80(%rbp)
	je	.L2511
.L2528:
	movq	(%rbx), %r12
	movq	16(%r15), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	jne	.L2526
	movq	16(%r15), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, %r14
	jne	.L2607
.L2527:
	movq	16(%r15), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	8(%r15), %rdi
	movl	$2, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	jmp	.L2526
.L2516:
	movq	16(%r15), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	jne	.L2511
	movq	16(%r15), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, %r14
	je	.L2521
	movq	(%r12), %rax
	movl	20(%r12), %edx
	leaq	.LC26(%rip), %rdi
	movl	160(%r14), %esi
	movq	8(%rax), %rcx
	andl	$16777215, %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2521
	.p2align 4,,10
	.p2align 3
.L2536:
	movq	-72(%rbp), %rdx
	movq	-104(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2509
	.p2align 4,,10
	.p2align 3
.L2507:
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	leaq	16(%rax,%rdx), %rax
	jmp	.L2508
	.p2align 4,,10
	.p2align 3
.L2501:
	movq	40(%r15), %rax
	testq	%rax, %rax
	je	.L2503
	cmpq	$64, 8(%rax)
	ja	.L2504
.L2503:
	movq	72(%r15), %rax
	movq	$64, 8(%rax)
	movq	40(%r15), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 40(%r15)
.L2504:
	movq	88(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%r15)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%r15)
	movq	%rdx, 80(%r15)
	movq	%rax, 64(%r15)
	jmp	.L2502
	.p2align 4,,10
	.p2align 3
.L2534:
	movq	-72(%rbp), %rsi
	movq	-120(%rbp), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2535
	.p2align 4,,10
	.p2align 3
.L2600:
	movq	(%r12), %rax
	movq	(%r15), %rdi
	movslq	40(%rax), %rdx
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	leaq	0(,%rdx,8), %r14
	subq	%rbx, %rax
	movq	%r14, %rsi
	cmpq	%rax, %r14
	ja	.L2608
	addq	%rbx, %rsi
	movq	%rsi, 16(%rdi)
.L2530:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m@PLT
	movq	-80(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2511
	leaq	(%r14,%rbx), %rax
	movq	%rax, -80(%rbp)
	jmp	.L2533
	.p2align 4,,10
	.p2align 3
.L2531:
	addq	$8, %rbx
	cmpq	%rbx, -80(%rbp)
	je	.L2511
.L2533:
	movq	(%rbx), %r12
	movq	16(%r15), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	jne	.L2531
	movq	16(%r15), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, %r14
	jne	.L2609
.L2532:
	movq	16(%r15), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	8(%r15), %rdi
	movl	$2, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	jmp	.L2531
	.p2align 4,,10
	.p2align 3
.L2605:
	movq	16(%r15), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, %r14
	jne	.L2610
.L2522:
	movq	16(%r15), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	8(%r15), %rdi
	movl	$2, %edx
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	jmp	.L2521
	.p2align 4,,10
	.p2align 3
.L2607:
	movq	(%r12), %rax
	movl	20(%r12), %edx
	leaq	.LC26(%rip), %rdi
	movl	160(%r14), %esi
	movq	8(%rax), %rcx
	andl	$16777215, %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2527
	.p2align 4,,10
	.p2align 3
.L2609:
	movq	(%r12), %rax
	movl	20(%r12), %edx
	leaq	.LC26(%rip), %rdi
	movl	160(%r14), %esi
	movq	8(%rax), %rcx
	andl	$16777215, %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2532
	.p2align 4,,10
	.p2align 3
.L2606:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L2524
	.p2align 4,,10
	.p2align 3
.L2542:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rdx
	btcq	$63, %rdx
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2540:
	movq	%rsi, %rax
	movq	%rsi, %rdx
	pxor	%xmm0, %xmm0
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L2541
	.p2align 4,,10
	.p2align 3
.L2610:
	movq	(%rbx), %rax
	movl	20(%rbx), %edx
	leaq	.LC26(%rip), %rdi
	movl	160(%r14), %esi
	movq	8(%rax), %rcx
	andl	$16777215, %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2522
	.p2align 4,,10
	.p2align 3
.L2603:
	subq	%r13, %rsi
	leaq	32(%r15), %rdi
	call	_ZNSt6vectorIPN2v88internal10ZoneVectorIPNS1_8compiler4NodeEEENS1_13ZoneAllocatorIS7_EEE17_M_default_appendEm
	jmp	.L2489
	.p2align 4,,10
	.p2align 3
.L2602:
	leaq	0(,%rdx,8), %r14
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L2549
	movq	32(%r15), %rdi
	movq	%r14, %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %r14
	ja	.L2611
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2549:
	cmpq	%r12, %rbx
	je	.L2556
	subq	$8, %r12
	leaq	15(%rbx), %rdx
	subq	%rbx, %r12
	subq	%rax, %rdx
	shrq	$3, %r12
	cmpq	$30, %rdx
	jbe	.L2564
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r12
	je	.L2564
	addq	$1, %r12
	xorl	%edx, %edx
	movq	%r12, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L2554:
	movdqu	(%rbx,%rdx), %xmm3
	movups	%xmm3, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L2554
	movq	%r12, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%r12, %rdx
	je	.L2556
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L2556:
	movq	16(%r15), %rdx
	leaq	(%rax,%r13), %r12
	addq	%rax, %r14
	movq	%rax, 40(%r15)
	movq	%r12, 48(%r15)
	movq	%r14, 56(%r15)
	movq	24(%rdx), %rbx
	movq	%rbx, %rcx
	subq	16(%rdx), %rcx
	movq	%rax, %rbx
	movq	%rcx, %rsi
	sarq	$3, %rsi
	jmp	.L2548
	.p2align 4,,10
	.p2align 3
.L2595:
	leaq	.LC37(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2490
	.p2align 4,,10
	.p2align 3
.L2608:
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L2530
	.p2align 4,,10
	.p2align 3
.L2598:
	movq	%r8, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L2496
	.p2align 4,,10
	.p2align 3
.L2599:
	movl	$184, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L2498
	.p2align 4,,10
	.p2align 3
.L2596:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L2492
	.p2align 4,,10
	.p2align 3
.L2564:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2553:
	movq	(%rbx,%rdx,8), %rcx
	movq	%rcx, (%rax,%rdx,8)
	movq	%rdx, %rcx
	addq	$1, %rdx
	cmpq	%rcx, %r12
	jne	.L2553
	jmp	.L2556
	.p2align 4,,10
	.p2align 3
.L2611:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2549
.L2597:
	leaq	.LC38(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2601:
	leaq	.LC36(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2604:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10916:
	.size	_ZN2v88internal8compiler9Scheduler8BuildCFGEv, .-_ZN2v88internal8compiler9Scheduler8BuildCFGEv
	.section	.rodata._ZN2v88internal8compiler9Scheduler13ScheduleEarlyEv.str1.8,"aMS",@progbits,1
	.align 8
.LC41:
	.string	"--- SCHEDULE EARLY -----------------------------------------\n"
	.section	.rodata._ZN2v88internal8compiler9Scheduler13ScheduleEarlyEv.str1.1,"aMS",@progbits,1
.LC42:
	.string	"roots: "
.LC43:
	.string	"#%d:%s "
.LC44:
	.string	"\n"
	.section	.text._ZN2v88internal8compiler9Scheduler13ScheduleEarlyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler13ScheduleEarlyEv
	.type	_ZN2v88internal8compiler9Scheduler13ScheduleEarlyEv, @function
_ZN2v88internal8compiler9Scheduler13ScheduleEarlyEv:
.LFB10985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$472, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2744
.L2614:
	movq	(%rbx), %rax
	movq	16(%rbx), %rdx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movaps	%xmm0, -336(%rbp)
	movq	%rax, -368(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm0, -320(%rbp)
	movaps	%xmm0, -304(%rbp)
	movaps	%xmm0, -288(%rbp)
	movq	%rbx, -176(%rbp)
	movq	%rdx, -168(%rbp)
	movq	$0, -360(%rbp)
	movq	$0, -352(%rbp)
	movq	$0, -344(%rbp)
	movq	%rax, -472(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -352(%rbp)
	pxor	%xmm0, %xmm0
	je	.L2745
	leaq	-272(%rbp), %rdi
	xorl	%esi, %esi
	movdqa	-368(%rbp), %xmm5
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm5, -272(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movq	$0, -256(%rbp)
	movq	$0, -248(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movq	-328(%rbp), %r9
	movdqa	-208(%rbp), %xmm5
	movq	-336(%rbp), %xmm3
	movq	-312(%rbp), %r8
	movq	-304(%rbp), %xmm1
	movq	-296(%rbp), %rsi
	movaps	%xmm5, -304(%rbp)
	movq	%r9, %xmm5
	movq	-320(%rbp), %xmm2
	punpcklqdq	%xmm5, %xmm3
	movq	-280(%rbp), %rdx
	movaps	%xmm3, -128(%rbp)
	movq	%r8, %xmm3
	movdqa	-240(%rbp), %xmm6
	movdqa	-224(%rbp), %xmm7
	punpcklqdq	%xmm3, %xmm2
	movq	-360(%rbp), %rax
	movq	-288(%rbp), %xmm0
	movaps	%xmm2, -112(%rbp)
	movq	%rsi, %xmm2
	movq	-368(%rbp), %xmm4
	movq	-352(%rbp), %r11
	punpcklqdq	%xmm2, %xmm1
	movq	-344(%rbp), %r10
	movq	-256(%rbp), %rcx
	movaps	%xmm6, -336(%rbp)
	movq	-248(%rbp), %rdi
	movdqa	-192(%rbp), %xmm6
	movaps	%xmm7, -320(%rbp)
	movq	%rax, %xmm7
	movaps	%xmm1, -96(%rbp)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm7, %xmm4
	punpcklqdq	%xmm1, %xmm0
	movq	%rcx, -352(%rbp)
	movq	%rdi, -344(%rbp)
	movq	%r11, -144(%rbp)
	movq	%r10, -136(%rbp)
	movaps	%xmm6, -288(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%rcx, %rcx
	je	.L2620
	movq	-280(%rbp), %rsi
	movq	-312(%rbp), %rdx
	addq	$8, %rsi
	cmpq	%rdx, %rsi
	jbe	.L2621
	.p2align 4,,10
	.p2align 3
.L2624:
	testq	%rax, %rax
	je	.L2622
	cmpq	$64, 8(%rax)
	ja	.L2623
.L2622:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-360(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	%rax, -360(%rbp)
.L2623:
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	ja	.L2624
	movq	-344(%rbp), %rdi
	movq	-352(%rbp), %rcx
.L2621:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L2620
	movq	%rdi, 8(%rcx)
	movq	$0, (%rcx)
.L2620:
	leaq	-416(%rbp), %rcx
	movq	80(%rbx), %rdi
	movq	72(%rbx), %rbx
	movq	%rcx, -464(%rbp)
	leaq	-160(%rbp), %rcx
	movq	-96(%rbp), %rax
	movq	%rcx, -440(%rbp)
	leaq	-408(%rbp), %rcx
	movq	%rdi, -448(%rbp)
	movq	%rbx, -424(%rbp)
	movq	%rcx, -456(%rbp)
	cmpq	%rdi, %rbx
	je	.L2694
	.p2align 4,,10
	.p2align 3
.L2693:
	movq	-424(%rbp), %rbx
	movq	(%rbx), %rcx
	movq	-80(%rbp), %rbx
	leaq	-8(%rbx), %rdx
	movq	%rcx, -416(%rbp)
	cmpq	%rdx, %rax
	je	.L2629
	movq	%rcx, (%rax)
	movq	-96(%rbp), %rax
	addq	$8, %rax
	movq	%rax, -96(%rbp)
.L2632:
	cmpq	%rax, -128(%rbp)
	je	.L2631
	.p2align 4,,10
	.p2align 3
.L2630:
	movq	-176(%rbp), %rax
	movq	248(%rax), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	-128(%rbp), %rax
	movq	-176(%rbp), %rdx
	movq	(%rax), %rbx
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	200(%rdx), %rax
	cmpl	$2, 12(%rax)
	movq	%rax, -432(%rbp)
	je	.L2633
.L2737:
	movq	(%rax), %rax
.L2634:
	movq	-168(%rbp), %rdx
	cmpq	%rax, 104(%rdx)
	je	.L2638
	movq	24(%rbx), %r15
	testq	%r15, %r15
	jne	.L2689
	jmp	.L2638
	.p2align 4,,10
	.p2align 3
.L2640:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L2638
.L2689:
	movl	16(%r15), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r15,%rax,8), %rdi
	jne	.L2639
	movq	(%rdi), %rdi
.L2639:
	movl	20(%rdi), %ebx
	movq	-176(%rbp), %rax
	andl	$16777215, %ebx
	salq	$4, %rbx
	addq	200(%rax), %rbx
	movl	12(%rbx), %eax
	testl	%eax, %eax
	je	.L2640
	movq	-432(%rbp), %rcx
	movq	(%rcx), %r13
	movq	%rdi, -408(%rbp)
	cmpl	$2, %eax
	je	.L2640
	cmpl	$3, %eax
	je	.L2643
.L2743:
	movl	12(%r13), %eax
.L2644:
	movq	(%rbx), %rdx
	cmpl	%eax, 12(%rdx)
	jge	.L2640
	movq	%r13, (%rbx)
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2687
	movq	-408(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, -96(%rbp)
.L2688:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L2640
	movq	-408(%rbp), %rdx
	movq	(%rbx), %rax
	leaq	.LC20(%rip), %rdi
	movq	(%rdx), %rsi
	movq	160(%rax), %rcx
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	8(%rsi), %r9
	movl	20(%rdx), %esi
	movq	%r9, %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L2689
	.p2align 4,,10
	.p2align 3
.L2638:
	movq	-112(%rbp), %rbx
	movq	-128(%rbp), %rax
	leaq	-8(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L2746
	addq	$8, %rax
	movq	%rax, -128(%rbp)
.L2690:
	cmpq	%rax, -96(%rbp)
	jne	.L2630
.L2631:
	addq	$8, -424(%rbp)
	movq	-424(%rbp), %rcx
	cmpq	%rcx, -448(%rbp)
	jne	.L2693
.L2694:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L2612
	movq	-72(%rbp), %rdi
	movq	-104(%rbp), %rdx
	leaq	8(%rdi), %rsi
	cmpq	%rdx, %rsi
	jbe	.L2695
	movq	-152(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L2698:
	testq	%rax, %rax
	je	.L2696
	cmpq	$64, 8(%rax)
	ja	.L2697
.L2696:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-152(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	%rax, -152(%rbp)
.L2697:
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	ja	.L2698
	movq	-144(%rbp), %rax
.L2695:
	movq	-136(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L2612
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L2612:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2747
	addq	$472, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2687:
	.cfi_restore_state
	movq	-456(%rbp), %rsi
	movq	-440(%rbp), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L2643:
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	20(%rax), %r12d
	movq	%rax, -400(%rbp)
	movq	%rax, %rdi
	movq	-176(%rbp), %rax
	andl	$16777215, %r12d
	salq	$4, %r12
	addq	200(%rax), %r12
	movl	12(%r12), %eax
	cmpl	$2, %eax
	je	.L2743
	cmpl	$3, %eax
	je	.L2647
.L2742:
	movl	12(%r13), %eax
.L2648:
	movq	(%r12), %rdx
	cmpl	%eax, 12(%rdx)
	jge	.L2644
	movq	%r13, (%r12)
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2683
	movq	-400(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, -96(%rbp)
.L2684:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L2743
	movq	-400(%rbp), %rdx
	movq	(%r12), %rax
	leaq	.LC20(%rip), %rdi
	movq	(%rdx), %rsi
	movq	160(%rax), %rcx
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	8(%rsi), %r9
	movl	20(%rdx), %esi
	movq	%r9, %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2743
	.p2align 4,,10
	.p2align 3
.L2647:
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	20(%rax), %r14d
	movq	%rax, -392(%rbp)
	movq	%rax, %rdi
	movq	-176(%rbp), %rax
	andl	$16777215, %r14d
	salq	$4, %r14
	addq	200(%rax), %r14
	movl	12(%r14), %eax
	cmpl	$2, %eax
	je	.L2742
	cmpl	$3, %eax
	je	.L2651
.L2741:
	movl	12(%r13), %eax
.L2652:
	movq	(%r14), %rdx
	cmpl	%eax, 12(%rdx)
	jge	.L2648
	movq	%r13, (%r14)
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2680
	movq	-392(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, -96(%rbp)
.L2681:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L2742
	movq	-392(%rbp), %rdx
	movq	(%r14), %rax
	leaq	.LC20(%rip), %rdi
	movq	(%rdx), %rsi
	movq	160(%rax), %rcx
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	8(%rsi), %r9
	movl	20(%rdx), %esi
	movq	%r9, %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2742
	.p2align 4,,10
	.p2align 3
.L2746:
	movq	-152(%rbp), %rax
	testq	%rax, %rax
	je	.L2691
	cmpq	$64, 8(%rax)
	ja	.L2692
.L2691:
	movq	-120(%rbp), %rax
	movq	$64, 8(%rax)
	movq	-152(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -152(%rbp)
.L2692:
	movq	-104(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -104(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rax, -128(%rbp)
	jmp	.L2690
	.p2align 4,,10
	.p2align 3
.L2633:
	movq	-168(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	-432(%rbp), %rcx
	movq	%rax, (%rcx)
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L2634
	movq	(%rbx), %rdx
	movl	20(%rbx), %esi
	leaq	.LC27(%rip), %rdi
	movq	160(%rax), %rcx
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	8(%rdx), %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-432(%rbp), %rax
	jmp	.L2737
	.p2align 4,,10
	.p2align 3
.L2629:
	movq	-464(%rbp), %rsi
	movq	-440(%rbp), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	-96(%rbp), %rax
	jmp	.L2632
	.p2align 4,,10
	.p2align 3
.L2683:
	movq	-440(%rbp), %rdi
	leaq	-400(%rbp), %rsi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2684
	.p2align 4,,10
	.p2align 3
.L2651:
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	20(%rax), %r9d
	movq	%rax, -384(%rbp)
	movq	%rax, %rdi
	movq	-176(%rbp), %rax
	andl	$16777215, %r9d
	salq	$4, %r9
	addq	200(%rax), %r9
	movl	12(%r9), %eax
	cmpl	$2, %eax
	je	.L2741
	cmpl	$3, %eax
	je	.L2655
.L2740:
	movl	12(%r13), %eax
.L2656:
	movq	(%r9), %rdx
	cmpl	%eax, 12(%rdx)
	jge	.L2652
	movq	%r13, (%r9)
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2677
	movq	-384(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, -96(%rbp)
.L2678:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L2741
	movq	-384(%rbp), %rdx
	movq	(%r9), %rax
	leaq	.LC20(%rip), %rdi
	movq	(%rdx), %rsi
	movq	160(%rax), %rcx
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	8(%rsi), %r9
	movl	20(%rdx), %esi
	movq	%r9, %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2741
	.p2align 4,,10
	.p2align 3
.L2745:
	movdqa	-368(%rbp), %xmm6
	movdqa	-336(%rbp), %xmm7
	movq	$0, -144(%rbp)
	movq	-344(%rbp), %rax
	movdqa	-288(%rbp), %xmm1
	movaps	%xmm6, -160(%rbp)
	movdqa	-320(%rbp), %xmm6
	movaps	%xmm7, -128(%rbp)
	movdqa	-304(%rbp), %xmm7
	movq	%rax, -136(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	jmp	.L2620
	.p2align 4,,10
	.p2align 3
.L2744:
	leaq	.LC41(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L2614
	xorl	%eax, %eax
	leaq	.LC42(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	72(%rbx), %r12
	movq	80(%rbx), %r13
	movzbl	_ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip), %eax
	cmpq	%r13, %r12
	je	.L2618
	leaq	.LC43(%rip), %r14
.L2617:
	testb	%al, %al
	je	.L2614
	movq	(%r12), %rax
	movq	%r14, %rdi
	addq	$8, %r12
	movq	(%rax), %rdx
	movl	20(%rax), %esi
	xorl	%eax, %eax
	movq	8(%rdx), %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	_ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip), %eax
	cmpq	%r12, %r13
	jne	.L2617
.L2618:
	testb	%al, %al
	je	.L2614
	leaq	.LC44(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2614
	.p2align 4,,10
	.p2align 3
.L2680:
	movq	-440(%rbp), %rdi
	leaq	-392(%rbp), %rsi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2681
	.p2align 4,,10
	.p2align 3
.L2655:
	xorl	%esi, %esi
	movq	%r9, -480(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-480(%rbp), %r9
	movl	20(%rax), %r10d
	movq	%rax, -376(%rbp)
	movq	%rax, %rdi
	movq	-176(%rbp), %rax
	andl	$16777215, %r10d
	salq	$4, %r10
	addq	200(%rax), %r10
	movl	12(%r10), %eax
	cmpl	$2, %eax
	je	.L2740
	cmpl	$3, %eax
	je	.L2659
.L2739:
	movl	12(%r13), %eax
.L2660:
	movq	(%r10), %rdx
	cmpl	%eax, 12(%rdx)
	jge	.L2656
	movq	%r13, (%r10)
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2674
	movq	-376(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, -96(%rbp)
.L2675:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L2740
	movq	-376(%rbp), %rdx
	movq	(%r10), %rax
	leaq	.LC20(%rip), %rdi
	movq	%r9, -480(%rbp)
	movq	(%rdx), %rsi
	movq	160(%rax), %rcx
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	8(%rsi), %r10
	movl	20(%rdx), %esi
	movq	%r10, %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	12(%r13), %eax
	movq	-480(%rbp), %r9
	jmp	.L2656
.L2677:
	movq	-440(%rbp), %rdi
	leaq	-384(%rbp), %rsi
	movq	%r9, -480(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	-480(%rbp), %r9
	jmp	.L2678
.L2659:
	xorl	%esi, %esi
	movq	%r10, -488(%rbp)
	movq	%r9, -480(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-480(%rbp), %r9
	movq	-488(%rbp), %r10
	movq	%rax, -368(%rbp)
	movl	20(%rax), %r11d
	movq	%rax, %rdi
	movq	-176(%rbp), %rax
	andl	$16777215, %r11d
	salq	$4, %r11
	addq	200(%rax), %r11
	movl	12(%r11), %eax
	cmpl	$2, %eax
	je	.L2739
	cmpl	$3, %eax
	je	.L2663
.L2738:
	movl	12(%r13), %eax
.L2664:
	movq	(%r11), %rdx
	cmpl	%eax, 12(%rdx)
	jge	.L2660
	movq	%r13, (%r11)
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2671
	movq	-368(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, -96(%rbp)
.L2672:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L2739
	movq	-368(%rbp), %rdx
	movq	(%r11), %rax
	leaq	.LC20(%rip), %rdi
	movq	%r10, -488(%rbp)
	movq	%r9, -480(%rbp)
	movq	(%rdx), %rsi
	movq	160(%rax), %rcx
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	8(%rsi), %r11
	movl	20(%rdx), %esi
	movq	%r11, %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	12(%r13), %eax
	movq	-488(%rbp), %r10
	movq	-480(%rbp), %r9
	jmp	.L2660
.L2674:
	movq	-440(%rbp), %rdi
	leaq	-376(%rbp), %rsi
	movq	%r10, -488(%rbp)
	movq	%r9, -480(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	-488(%rbp), %r10
	movq	-480(%rbp), %r9
	jmp	.L2675
.L2663:
	xorl	%esi, %esi
	movq	%r11, -496(%rbp)
	movq	%r10, -488(%rbp)
	movq	%r9, -480(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-480(%rbp), %r9
	movq	-488(%rbp), %r10
	movq	%rax, -272(%rbp)
	movl	20(%rax), %ecx
	movq	%rax, %rdi
	movq	-176(%rbp), %rax
	movq	-496(%rbp), %r11
	andl	$16777215, %ecx
	salq	$4, %rcx
	addq	200(%rax), %rcx
	movl	12(%rcx), %eax
	cmpl	$2, %eax
	je	.L2738
	cmpl	$3, %eax
	je	.L2748
.L2667:
	movq	(%rcx), %rdx
	movl	12(%r13), %eax
	cmpl	12(%rdx), %eax
	jle	.L2664
	movq	%r13, (%rcx)
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2668
	movq	-272(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, -96(%rbp)
.L2669:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L2738
	movq	-272(%rbp), %rdx
	movq	(%rcx), %rax
	movq	%r11, -496(%rbp)
	movq	%r10, -488(%rbp)
	movq	(%rdx), %rsi
	movq	160(%rax), %rcx
	movq	%r9, -480(%rbp)
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	movq	8(%rsi), %rdi
	movl	20(%rdx), %esi
	movq	%rdi, %rdx
	andl	$16777215, %esi
	leaq	.LC20(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	12(%r13), %eax
	movq	-496(%rbp), %r11
	movq	-488(%rbp), %r10
	movq	-480(%rbp), %r9
	jmp	.L2664
.L2671:
	movq	-472(%rbp), %rsi
	movq	-440(%rbp), %rdi
	movq	%r11, -496(%rbp)
	movq	%r10, -488(%rbp)
	movq	%r9, -480(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	-496(%rbp), %r11
	movq	-488(%rbp), %r10
	movq	-480(%rbp), %r9
	jmp	.L2672
.L2747:
	call	__stack_chk_fail@PLT
.L2748:
	xorl	%esi, %esi
	movq	%rcx, -504(%rbp)
	movq	%r11, -496(%rbp)
	movq	%r10, -488(%rbp)
	movq	%r9, -480(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	leaq	-176(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler24ScheduleEarlyNodeVisitor30PropagateMinimumPositionToNodeEPNS1_10BasicBlockEPNS1_4NodeE
	movq	-504(%rbp), %rcx
	movq	-496(%rbp), %r11
	movq	-488(%rbp), %r10
	movq	-480(%rbp), %r9
	jmp	.L2667
.L2668:
	movq	-440(%rbp), %rdi
	leaq	-272(%rbp), %rsi
	movq	%rcx, -504(%rbp)
	movq	%r11, -496(%rbp)
	movq	%r10, -488(%rbp)
	movq	%r9, -480(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	-504(%rbp), %rcx
	movq	-496(%rbp), %r11
	movq	-488(%rbp), %r10
	movq	-480(%rbp), %r9
	jmp	.L2669
	.cfi_endproc
.LFE10985:
	.size	_ZN2v88internal8compiler9Scheduler13ScheduleEarlyEv, .-_ZN2v88internal8compiler9Scheduler13ScheduleEarlyEv
	.section	.rodata._ZN2v88internal8compiler9Scheduler19FuseFloatingControlEPNS1_10BasicBlockEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC45:
	.string	"--- FUSE FLOATING CONTROL ----------------------------------\n"
	.align 8
.LC46:
	.string	"Schedule before control flow fusion:\n"
	.section	.rodata._ZN2v88internal8compiler9Scheduler19FuseFloatingControlEPNS1_10BasicBlockEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC47:
	.string	"Found SESE at #%d:%s\n"
.LC48:
	.string	"propagation roots: "
	.section	.rodata._ZN2v88internal8compiler9Scheduler19FuseFloatingControlEPNS1_10BasicBlockEPNS1_4NodeE.str1.8
	.align 8
.LC49:
	.string	"Schedule after control flow fusion:\n"
	.section	.text._ZN2v88internal8compiler9Scheduler19FuseFloatingControlEPNS1_10BasicBlockEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler19FuseFloatingControlEPNS1_10BasicBlockEPNS1_4NodeE
	.type	_ZN2v88internal8compiler9Scheduler19FuseFloatingControlEPNS1_10BasicBlockEPNS1_4NodeE, @function
_ZN2v88internal8compiler9Scheduler19FuseFloatingControlEPNS1_10BasicBlockEPNS1_4NodeE:
.LFB11021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$792, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -768(%rbp)
	movq	%rsi, -792(%rbp)
	movq	%rdx, -760(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2918
.L2751:
	movq	-768(%rbp), %rax
	movq	224(%rax), %r14
	movq	136(%r14), %rax
	cmpq	144(%r14), %rax
	je	.L2753
	movq	%rax, 144(%r14)
.L2753:
	movq	-760(%rbp), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler10CFGBuilder5QueueEPNS1_4NodeE
	movq	-792(%rbp), %rax
	movq	16(%r14), %rdi
	movq	%rbx, %rsi
	movq	$0, 160(%r14)
	movq	%rax, 168(%r14)
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rbx, %rsi
	movq	%rax, 176(%r14)
	movq	8(%r14), %rax
	movq	240(%rax), %rdi
	call	_ZN2v88internal8compiler18ControlEquivalence3RunEPNS1_4NodeE@PLT
	movq	64(%r14), %rax
	cmpq	%rax, 96(%r14)
	je	.L2919
	leaq	32(%r14), %rax
	leaq	.L2774(%rip), %r15
	movq	%rax, -816(%rbp)
	leaq	-512(%rbp), %rax
	movq	%rax, -784(%rbp)
	leaq	128(%r14), %rax
	movq	%rax, -824(%rbp)
	jmp	.L2755
	.p2align 4,,10
	.p2align 3
.L2760:
	movq	-760(%rbp), %rsi
	movq	(%rdx,%rbx,8), %rdx
	movl	20(%rsi), %ebx
	movq	(%rdx), %rdx
	andl	$16777215, %ebx
	cmpq	%rax, %rbx
	jnb	.L2920
.L2762:
	movq	(%rcx,%rbx,8), %rax
	cmpq	%rdx, (%rax)
	jne	.L2764
.L2924:
	cmpq	%r12, -760(%rbp)
	je	.L2764
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2921
.L2765:
	movq	%r12, 160(%r14)
.L2766:
	movq	64(%r14), %rax
	cmpq	%rax, 96(%r14)
	je	.L2754
.L2755:
	movq	8(%r14), %rax
	movq	248(%rax), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	80(%r14), %rcx
	movq	64(%r14), %rax
	leaq	-8(%rcx), %rdx
	movq	(%rax), %r12
	cmpq	%rdx, %rax
	je	.L2756
	addq	$8, %rax
	movq	%rax, 64(%r14)
.L2757:
	movq	8(%r14), %rax
	movl	20(%r12), %ebx
	movq	240(%rax), %r13
	andl	$16777215, %ebx
	movq	40(%r13), %rdi
	movq	32(%r13), %rdx
	movq	%rdi, %rax
	movq	%rdx, %rcx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L2760
	leaq	1(%rbx), %rsi
	cmpq	%rsi, %rax
	jb	.L2922
	jbe	.L2760
	leaq	(%rdx,%rsi,8), %rsi
	cmpq	%rsi, %rdi
	je	.L2760
	movq	%rsi, 40(%r13)
	movq	8(%r14), %rax
	movq	240(%rax), %r13
	movq	40(%r13), %rdi
	movq	32(%r13), %rcx
	movq	%rdi, %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	jmp	.L2760
	.p2align 4,,10
	.p2align 3
.L2920:
	leaq	1(%rbx), %rsi
	cmpq	%rax, %rsi
	ja	.L2923
	jnb	.L2762
	leaq	(%rcx,%rsi,8), %rax
	cmpq	%rax, %rdi
	je	.L2762
	movq	%rax, 40(%r13)
	movq	(%rcx,%rbx,8), %rax
	cmpq	%rdx, (%rax)
	je	.L2924
	.p2align 4,,10
	.p2align 3
.L2764:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties16PastControlIndexEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE@PLT
	cmpl	%eax, %r13d
	jle	.L2766
	subl	$1, %r13d
	movslq	%eax, %rdx
	leaq	32(%r12), %rcx
	movq	%r12, -752(%rbp)
	subl	%eax, %r13d
	movq	%rcx, -776(%rbp)
	leaq	(%r12,%rdx,8), %rbx
	leaq	0(%r13,%rdx), %rax
	leaq	8(%r12,%rax,8), %rax
	movq	%rax, -744(%rbp)
	.p2align 4,,10
	.p2align 3
.L2798:
	movq	-752(%rbp), %rax
	movq	%rbx, %rdx
	subq	%rax, %rdx
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L2768
	leaq	32(%rbx), %rax
.L2769:
	movq	(%rax), %r13
	movq	%r13, -512(%rbp)
	movl	24(%r14), %eax
	cmpl	%eax, 16(%r13)
	ja	.L2770
	movq	0(%r13), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$49, %ax
	ja	.L2771
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler9Scheduler19FuseFloatingControlEPNS1_10BasicBlockEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L2774:
	.long	.L2779-.L2774
	.long	.L2777-.L2774
	.long	.L2778-.L2774
	.long	.L2778-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2777-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2776-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2775-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2772-.L2774
	.long	.L2773-.L2774
	.section	.text._ZN2v88internal8compiler9Scheduler19FuseFloatingControlEPNS1_10BasicBlockEPNS1_4NodeE
.L2775:
	movq	16(%r14), %rdi
	movq	112(%rdi), %rsi
.L2915:
	movq	%r13, %rdx
.L2917:
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	8(%r14), %rdi
	movl	$2, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
.L2772:
	movq	112(%r14), %rax
	movq	96(%r14), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2795
	movq	-512(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 96(%r14)
.L2796:
	movl	24(%r14), %eax
	movq	-512(%rbp), %rdx
	addl	$1, %eax
	movl	%eax, 16(%rdx)
	movq	144(%r14), %rsi
	cmpq	152(%r14), %rsi
	je	.L2797
	movq	%rdx, (%rsi)
	addq	$8, 144(%r14)
.L2770:
	addq	$8, %rbx
	cmpq	%rbx, -744(%rbp)
	jne	.L2798
	jmp	.L2766
	.p2align 4,,10
	.p2align 3
.L2771:
	subw	$682, %ax
	cmpw	$106, %ax
	ja	.L2772
.L2773:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_@PLT
	testb	%al, %al
	je	.L2772
	movq	0(%r13), %rax
	movq	(%r14), %rdi
	movslq	40(%rax), %rdx
	movq	16(%rdi), %r9
	movq	24(%rdi), %rax
	leaq	0(,%rdx,8), %rcx
	subq	%r9, %rax
	movq	%rcx, %rsi
	cmpq	%rax, %rcx
	ja	.L2925
	addq	%r9, %rsi
	movq	%rsi, 16(%rdi)
.L2791:
	movq	%r9, %rsi
	movq	%r13, %rdi
	movq	%rdx, -800(%rbp)
	movq	%rcx, -832(%rbp)
	movq	%r9, -808(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m@PLT
	movq	-800(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2772
	movq	-808(%rbp), %r9
	movq	-832(%rbp), %rcx
	movq	%rbx, -800(%rbp)
	leaq	(%rcx,%r9), %r13
	movq	%r9, %r12
	movq	%r13, %rbx
	jmp	.L2794
	.p2align 4,,10
	.p2align 3
.L2792:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L2916
.L2794:
	movq	(%r12), %r13
	movq	16(%r14), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	jne	.L2792
	movq	16(%r14), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, %r9
	jne	.L2926
.L2793:
	movq	16(%r14), %rdi
	movq	%r9, %rsi
	movq	%r13, %rdx
	addq	$8, %r12
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	8(%r14), %rdi
	movl	$2, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	cmpq	%r12, %rbx
	jne	.L2794
	.p2align 4,,10
	.p2align 3
.L2916:
	movq	-800(%rbp), %rbx
	jmp	.L2772
.L2776:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	16(%r14), %rdi
	movq	%rax, %rsi
	movq	%rax, -800(%rbp)
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2927
.L2782:
	movq	16(%r14), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	jmp	.L2917
.L2778:
	movq	(%r14), %rdi
	movslq	40(%rdx), %rdx
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	leaq	0(,%rdx,8), %rcx
	movq	%rcx, %rsi
	subq	%r12, %rax
	cmpq	%rax, %rcx
	ja	.L2928
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L2785:
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rdx, -800(%rbp)
	movq	%rcx, -808(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties25CollectControlProjectionsEPNS1_4NodeEPS4_m@PLT
	movq	-800(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2772
	movq	-808(%rbp), %rcx
	movq	%rbx, -800(%rbp)
	leaq	(%rcx,%r12), %rax
	movq	%rax, %rbx
	jmp	.L2789
	.p2align 4,,10
	.p2align 3
.L2787:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L2916
.L2789:
	movq	(%r12), %r13
	movq	16(%r14), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	jne	.L2787
	movq	16(%r14), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, %r9
	jne	.L2929
.L2788:
	movq	16(%r14), %rdi
	movq	%r13, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	8(%r14), %rdi
	movl	$2, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	jmp	.L2787
.L2777:
	movq	16(%r14), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	jne	.L2772
	movq	16(%r14), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	%rax, %r12
	je	.L2782
	movq	0(%r13), %rax
	movl	20(%r13), %edx
	leaq	.LC26(%rip), %rdi
	movl	160(%r12), %esi
	movq	8(%rax), %rcx
	andl	$16777215, %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2782
.L2779:
	movq	16(%r14), %rdi
	movq	104(%rdi), %rsi
	jmp	.L2915
	.p2align 4,,10
	.p2align 3
.L2768:
	movq	-776(%rbp), %rax
	movq	(%rax), %rax
	leaq	16(%rax,%rdx), %rax
	jmp	.L2769
	.p2align 4,,10
	.p2align 3
.L2797:
	movq	-784(%rbp), %rdx
	movq	-824(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2770
	.p2align 4,,10
	.p2align 3
.L2795:
	movq	-784(%rbp), %rsi
	movq	-816(%rbp), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2921:
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC47(%rip), %rdi
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2765
	.p2align 4,,10
	.p2align 3
.L2756:
	movq	40(%r14), %rax
	testq	%rax, %rax
	je	.L2758
	cmpq	$64, 8(%rax)
	ja	.L2759
.L2758:
	movq	72(%r14), %rax
	movq	$64, 8(%rax)
	movq	40(%r14), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 40(%r14)
.L2759:
	movq	88(%r14), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%r14)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%r14)
	movq	%rdx, 80(%r14)
	movq	%rax, 64(%r14)
	jmp	.L2757
.L2919:
	leaq	-512(%rbp), %rax
	movq	%rax, -784(%rbp)
	.p2align 4,,10
	.p2align 3
.L2754:
	movq	136(%r14), %rbx
	cmpq	144(%r14), %rbx
	je	.L2803
	.p2align 4,,10
	.p2align 3
.L2802:
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler10CFGBuilder13ConnectBlocksEPNS1_4NodeE
	cmpq	%rbx, 144(%r14)
	jne	.L2802
.L2803:
	movq	-768(%rbp), %rax
	movq	-760(%rbp), %rsi
	movq	16(%rax), %rdi
	movq	232(%rax), %r12
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	-792(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler18SpecialRPONumberer26ComputeAndInsertSpecialRPOEPNS1_10BasicBlockES4_
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2800
	.p2align 4,,10
	.p2align 3
.L2801:
	movl	$-1, 12(%rsi)
	movq	$0, 16(%rsi)
	movq	24(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.L2801
	movq	-792(%rbp), %rax
	movq	24(%rax), %rsi
.L2800:
	movq	-768(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler9Scheduler28PropagateImmediateDominatorsEPNS1_10BasicBlockE
	movq	224(%rbx), %r12
	movq	$0, -728(%rbp)
	movq	$0, -720(%rbp)
	movq	144(%r12), %rax
	movq	128(%r12), %rdi
	movq	$0, -712(%rbp)
	movq	136(%r12), %rdx
	movq	%rax, %rbx
	movq	%rdi, -736(%rbp)
	subq	%rdx, %rbx
	je	.L2847
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	7(%rbx), %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L2930
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L2806:
	movq	144(%r12), %rax
	movq	136(%r12), %rdx
.L2804:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -712(%rbp)
	movups	%xmm0, -728(%rbp)
	cmpq	%rdx, %rax
	je	.L2807
	leaq	-8(%rax), %rsi
	leaq	15(%rcx), %rax
	subq	%rdx, %rsi
	subq	%rdx, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L2848
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L2848
	leaq	1(%rdi), %r8
	xorl	%eax, %eax
	movq	%r8, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L2809:
	movdqu	(%rdx,%rax), %xmm5
	movups	%xmm5, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L2809
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	addq	%rdi, %rdx
	addq	%rcx, %rdi
	cmpq	%r8, %rax
	je	.L2812
	movq	(%rdx), %rax
	movq	%rax, (%rdi)
.L2812:
	leaq	8(%rcx,%rsi), %rcx
.L2807:
	movq	-768(%rbp), %rax
	movq	%rcx, -720(%rbp)
	leaq	-736(%rbp), %r12
	movq	224(%rax), %rax
	movq	136(%rax), %r13
	movq	144(%rax), %r14
	cmpq	%r14, %r13
	je	.L2818
	movq	%r12, %r15
	movq	%r14, %r12
	movq	-768(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L2817:
	movq	0(%r13), %rax
	movq	24(%rax), %rbx
	testq	%rbx, %rbx
	jne	.L2816
	jmp	.L2822
	.p2align 4,,10
	.p2align 3
.L2820:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2822
.L2816:
	movl	16(%rbx), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rax
	jne	.L2819
	movq	(%rax), %rax
.L2819:
	movq	%rax, -512(%rbp)
	movq	(%rax), %rdx
	movzwl	16(%rdx), %edx
	subl	$35, %edx
	cmpl	$1, %edx
	ja	.L2820
	movl	20(%rax), %edx
	andl	$16777215, %edx
	salq	$4, %rdx
	addq	200(%r14), %rdx
	movl	12(%rdx), %edx
	testl	%edx, %edx
	je	.L2820
	movq	-720(%rbp), %rsi
	cmpq	-712(%rbp), %rsi
	je	.L2821
	movq	%rax, (%rsi)
	addq	$8, -720(%rbp)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2816
	.p2align 4,,10
	.p2align 3
.L2822:
	addq	$8, %r13
	cmpq	%r13, %r12
	jne	.L2817
	movq	%r15, %r12
.L2818:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2931
.L2823:
	movq	-768(%rbp), %rcx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	-704(%rbp), %rdi
	movaps	%xmm0, -672(%rbp)
	movq	(%rcx), %rax
	movq	16(%rcx), %rdx
	movaps	%xmm0, -656(%rbp)
	movaps	%xmm0, -640(%rbp)
	movaps	%xmm0, -624(%rbp)
	movq	%rcx, -512(%rbp)
	movq	%rdx, -504(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	$0, -680(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -688(%rbp)
	pxor	%xmm0, %xmm0
	je	.L2932
	leaq	-608(%rbp), %rdi
	xorl	%esi, %esi
	movdqa	-704(%rbp), %xmm6
	movaps	%xmm0, -576(%rbp)
	movaps	%xmm0, -560(%rbp)
	movaps	%xmm6, -608(%rbp)
	movaps	%xmm0, -544(%rbp)
	movaps	%xmm0, -528(%rbp)
	movq	$0, -592(%rbp)
	movq	$0, -584(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-560(%rbp), %xmm6
	movq	-664(%rbp), %r9
	movq	-656(%rbp), %xmm2
	movq	-648(%rbp), %r8
	movaps	%xmm6, -656(%rbp)
	movdqa	-528(%rbp), %xmm6
	movq	-672(%rbp), %xmm3
	movq	-624(%rbp), %xmm0
	movq	-616(%rbp), %rdx
	movaps	%xmm6, -624(%rbp)
	movq	%r9, %xmm6
	movq	-632(%rbp), %rcx
	movdqa	-576(%rbp), %xmm7
	punpcklqdq	%xmm6, %xmm3
	movq	-696(%rbp), %rax
	movq	-640(%rbp), %xmm1
	movaps	%xmm3, -464(%rbp)
	movq	%r8, %xmm3
	movq	-704(%rbp), %xmm4
	movq	-688(%rbp), %r11
	punpcklqdq	%xmm3, %xmm2
	movaps	%xmm7, -672(%rbp)
	movdqa	-544(%rbp), %xmm7
	movq	-680(%rbp), %r10
	movaps	%xmm2, -448(%rbp)
	movq	%rcx, %xmm2
	movq	-592(%rbp), %rsi
	movq	-584(%rbp), %rdi
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm7, -640(%rbp)
	movq	%rax, %xmm7
	movaps	%xmm1, -432(%rbp)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm7, %xmm4
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -688(%rbp)
	movq	%rdi, -680(%rbp)
	movq	%r11, -480(%rbp)
	movq	%r10, -472(%rbp)
	movaps	%xmm4, -496(%rbp)
	movaps	%xmm0, -416(%rbp)
	testq	%rsi, %rsi
	je	.L2828
	movq	-616(%rbp), %rcx
	movq	-648(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L2829
	.p2align 4,,10
	.p2align 3
.L2832:
	testq	%rax, %rax
	je	.L2830
	cmpq	$64, 8(%rax)
	ja	.L2831
.L2830:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-696(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -696(%rbp)
.L2831:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L2832
	movq	-680(%rbp), %rdi
	movq	-688(%rbp), %rsi
.L2829:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L2828
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L2828:
	movq	-784(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler24ScheduleEarlyNodeVisitor3RunEPNS0_10ZoneVectorIPNS1_4NodeEEE
	movq	-768(%rbp), %rbx
	movq	16(%rbx), %rdi
	movq	48(%rbx), %r8
	movq	40(%rbx), %rcx
	movq	24(%rdi), %rax
	movq	%r8, %rdx
	subq	16(%rdi), %rax
	movq	%rax, %rsi
	subq	%rcx, %rdx
	sarq	$3, %rsi
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	ja	.L2933
	jnb	.L2835
	addq	%rcx, %rax
	cmpq	%rax, %r8
	je	.L2835
	movq	%rax, 48(%rbx)
.L2835:
	movq	-760(%rbp), %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	-792(%rbp), %rsi
	movq	-768(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9Scheduler16MovePlannedNodesEPNS1_10BasicBlockES4_
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L2934
.L2836:
	movq	-480(%rbp), %rax
	testq	%rax, %rax
	je	.L2749
	movq	-408(%rbp), %rcx
	movq	-440(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L2838
	movq	-488(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L2841:
	testq	%rax, %rax
	je	.L2839
	cmpq	$64, 8(%rax)
	ja	.L2840
.L2839:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-488(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -488(%rbp)
.L2840:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L2841
	movq	-480(%rbp), %rax
.L2838:
	movq	-472(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L2749
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L2749:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2935
	addq	$792, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2922:
	.cfi_restore_state
	subq	%rax, %rsi
	leaq	24(%r13), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	8(%r14), %rax
	movq	32(%r13), %rdx
	movq	240(%rax), %rsi
	movq	40(%rsi), %rdi
	movq	32(%rsi), %rcx
	movq	%rsi, %r13
	movq	%rdi, %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	jmp	.L2760
	.p2align 4,,10
	.p2align 3
.L2923:
	subq	%rax, %rsi
	leaq	24(%r13), %rdi
	movq	%rdx, -744(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler18ControlEquivalence8NodeDataENS1_13ZoneAllocatorIS5_EEE17_M_default_appendEm
	movq	32(%r13), %rcx
	movq	-744(%rbp), %rdx
	jmp	.L2762
	.p2align 4,,10
	.p2align 3
.L2929:
	movq	0(%r13), %rax
	movl	20(%r13), %edx
	leaq	.LC26(%rip), %rdi
	movq	%r9, -808(%rbp)
	movl	160(%r9), %esi
	movq	8(%rax), %rcx
	andl	$16777215, %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-808(%rbp), %r9
	jmp	.L2788
	.p2align 4,,10
	.p2align 3
.L2821:
	movq	-784(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2820
.L2932:
	movdqa	-704(%rbp), %xmm1
	movq	-680(%rbp), %rax
	movq	$0, -480(%rbp)
	movdqa	-672(%rbp), %xmm2
	movdqa	-656(%rbp), %xmm3
	movdqa	-640(%rbp), %xmm7
	movdqa	-624(%rbp), %xmm4
	movq	%rax, -472(%rbp)
	movaps	%xmm1, -496(%rbp)
	movaps	%xmm2, -464(%rbp)
	movaps	%xmm3, -448(%rbp)
	movaps	%xmm7, -432(%rbp)
	movaps	%xmm4, -416(%rbp)
	jmp	.L2828
.L2847:
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	jmp	.L2804
.L2918:
	leaq	.LC45(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L2751
	leaq	-320(%rbp), %r13
	leaq	-400(%rbp), %r12
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%cx, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rbx, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r12, %rdi
	movl	$37, %edx
	movq	%rax, -400(%rbp)
	leaq	.LC46(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-768(%rbp), %rax
	movq	%r12, %rdi
	movq	16(%rax), %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -320(%rbp)
	movq	%rax, -400(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2751
.L2931:
	xorl	%eax, %eax
	leaq	.LC48(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-728(%rbp), %r13
	movq	-720(%rbp), %rbx
	movzbl	_ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip), %eax
	cmpq	%r13, %rbx
	je	.L2826
	leaq	.LC43(%rip), %r14
.L2825:
	testb	%al, %al
	je	.L2823
	movq	0(%r13), %rax
	movq	%r14, %rdi
	addq	$8, %r13
	movq	(%rax), %rdx
	movl	20(%rax), %esi
	xorl	%eax, %eax
	movq	8(%rdx), %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	_ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip), %eax
	cmpq	%r13, %rbx
	jne	.L2825
.L2826:
	testb	%al, %al
	je	.L2823
	leaq	.LC44(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2823
.L2933:
	leaq	32(%rbx), %rdi
	subq	%rdx, %rsi
	call	_ZNSt6vectorIPN2v88internal10ZoneVectorIPNS1_8compiler4NodeEEENS1_13ZoneAllocatorIS7_EEE17_M_default_appendEm
	movq	16(%rbx), %rdi
	jmp	.L2835
.L2934:
	leaq	-320(%rbp), %r13
	leaq	-400(%rbp), %r12
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	%rbx, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r12, %rdi
	movl	$36, %edx
	movq	%rax, -400(%rbp)
	leaq	.LC49(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-768(%rbp), %rax
	movq	%r12, %rdi
	movq	16(%rax), %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -320(%rbp)
	movq	%rax, -400(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2836
.L2927:
	movq	16(%r14), %rdi
	call	_ZN2v88internal8compiler8Schedule13NewBasicBlockEv@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	-800(%rbp), %r10
	movq	%rax, %r12
	jne	.L2936
.L2783:
	movq	16(%r14), %rdi
	movq	%r10, %rdx
	movq	%r12, %rsi
	movq	%r10, -800(%rbp)
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	-800(%rbp), %r10
	movq	8(%r14), %rdi
	movl	$2, %edx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	jmp	.L2782
.L2926:
	movq	0(%r13), %rax
	movl	20(%r13), %edx
	leaq	.LC26(%rip), %rdi
	movq	%r9, -808(%rbp)
	movl	160(%r9), %esi
	movq	8(%rax), %rcx
	andl	$16777215, %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-808(%rbp), %r9
	jmp	.L2793
.L2936:
	movq	(%r10), %rax
	movl	20(%r10), %edx
	leaq	.LC26(%rip), %rdi
	movl	160(%r12), %esi
	movq	8(%rax), %rcx
	andl	$16777215, %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-800(%rbp), %r10
	jmp	.L2783
.L2930:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L2806
.L2848:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2808:
	movq	(%rdx,%rax,8), %r8
	movq	%r8, (%rcx,%rax,8)
	movq	%rax, %r8
	addq	$1, %rax
	cmpq	%rdi, %r8
	jne	.L2808
	jmp	.L2812
.L2928:
	movq	%rcx, -808(%rbp)
	movq	%rdx, -800(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-800(%rbp), %rdx
	movq	-808(%rbp), %rcx
	movq	%rax, %r12
	jmp	.L2785
.L2925:
	movq	%rcx, -808(%rbp)
	movq	%rdx, -800(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-800(%rbp), %rdx
	movq	-808(%rbp), %rcx
	movq	%rax, %r9
	jmp	.L2791
.L2935:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11021:
	.size	_ZN2v88internal8compiler9Scheduler19FuseFloatingControlEPNS1_10BasicBlockEPNS1_4NodeE, .-_ZN2v88internal8compiler9Scheduler19FuseFloatingControlEPNS1_10BasicBlockEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler9Scheduler12ScheduleLateEv.str1.8,"aMS",@progbits,1
	.align 8
.LC50:
	.string	"--- SCHEDULE LATE ------------------------------------------\n"
	.section	.rodata._ZN2v88internal8compiler9Scheduler12ScheduleLateEv.str1.1,"aMS",@progbits,1
.LC51:
	.string	"Scheduling #%d:%s\n"
	.section	.rodata._ZN2v88internal8compiler9Scheduler12ScheduleLateEv.str1.8
	.align 8
.LC52:
	.string	"Schedule late of #%d:%s is id:%d at loop depth %d, minimum = id:%d\n"
	.align 8
.LC53:
	.string	"  hoisting #%d:%s to block id:%d\n"
	.section	.text._ZN2v88internal8compiler9Scheduler12ScheduleLateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler12ScheduleLateEv
	.type	_ZN2v88internal8compiler9Scheduler12ScheduleLateEv, @function
_ZN2v88internal8compiler9Scheduler12ScheduleLateEv:
.LFB11016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3321
.L2939:
	movq	(%r12), %rdi
	movq	16(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -192(%rbp)
	movl	$0, -184(%rbp)
	movq	%rdi, -200(%rbp)
	movq	$0, -176(%rbp)
	movl	$0, -168(%rbp)
	movq	$0, -160(%rbp)
	movq	%rdi, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$8, -128(%rbp)
	movups	%xmm0, -120(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	24(%rdi), %rdx
	movq	%rax, -208(%rbp)
	movq	16(%rdi), %rax
	movq	%rdi, -224(%rbp)
	subq	%rax, %rdx
	movq	%r12, -216(%rbp)
	cmpq	$63, %rdx
	jbe	.L3322
	leaq	64(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2945:
	movq	-128(%rbp), %rdx
	movq	%rax, -136(%rbp)
	leaq	-4(,%rdx,4), %rbx
	andq	$-8, %rbx
	addq	%rax, %rbx
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L2951
	cmpq	$63, 8(%rax)
	ja	.L3323
.L2951:
	movq	-152(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L2947
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2949:
	movq	%rax, (%rbx)
	movq	%rbx, -96(%rbp)
	movq	(%rbx), %rdx
	movq	%rbx, -64(%rbp)
	leaq	512(%rdx), %rax
	movq	%rdx, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	(%rbx), %rax
	movq	72(%r12), %rbx
	movq	%rdx, -120(%rbp)
	leaq	512(%rax), %rcx
	movq	%rax, -80(%rbp)
	movq	%rax, -88(%rbp)
	movq	80(%r12), %rax
	movq	%rcx, -72(%rbp)
	movq	%rax, -368(%rbp)
	movq	%rbx, -344(%rbp)
	cmpq	%rax, %rbx
	je	.L3119
.L3118:
	movq	-344(%rbp), %rax
	movq	(%rax), %rcx
	movq	-216(%rbp), %rax
	movq	%rax, -272(%rbp)
	movzbl	23(%rcx), %eax
	leaq	32(%rcx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2956
	movq	32(%rcx), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
.L2956:
	cltq
	leaq	(%rdx,%rax,8), %rax
	movq	%rax, -336(%rbp)
	cmpq	%rax, %rdx
	je	.L2957
	movq	%rdx, -280(%rbp)
	movq	-272(%rbp), %rdx
	movq	-280(%rbp), %rax
	jmp	.L3117
	.p2align 4,,10
	.p2align 3
.L3116:
	addq	$8, -280(%rbp)
	movq	-280(%rbp), %rax
	cmpq	%rax, -336(%rbp)
	je	.L2957
	movq	-216(%rbp), %rdx
.L3117:
	movq	(%rax), %rdi
	movl	20(%rdi), %ecx
	movq	%rdi, -240(%rbp)
	andl	$16777215, %ecx
	salq	$4, %rcx
	addq	200(%rdx), %rcx
	cmpl	$3, 12(%rcx)
	movq	%rcx, %rdx
	je	.L3324
.L2958:
	movl	8(%rdx), %eax
	testl	%eax, %eax
	jne	.L3116
	movq	-272(%rbp), %rbx
	movq	176(%rbx), %rax
	movq	160(%rbx), %rdx
	movq	%rax, -248(%rbp)
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2961
	movq	%rdi, (%rdx)
	addq	$8, 160(%rbx)
.L2962:
	leaq	-232(%rbp), %rax
	movq	%rax, -328(%rbp)
	.p2align 4,,10
	.p2align 3
.L3115:
	movq	-216(%rbp), %rax
	movq	248(%rax), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	-272(%rbp), %rcx
	movq	144(%rcx), %rbx
	movq	128(%rcx), %rax
	leaq	-8(%rbx), %rdx
	movq	%rbx, -248(%rbp)
	movq	(%rax), %r15
	cmpq	%rdx, %rax
	je	.L2963
	addq	$8, %rax
	movq	%rax, 128(%rcx)
.L2964:
	movq	-208(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler8Schedule11IsScheduledEPNS1_4NodeE@PLT
	testb	%al, %al
	jne	.L2967
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3325
.L2968:
	movq	24(%r15), %rbx
	movq	-216(%rbp), %rax
	movq	%rbx, -248(%rbp)
	movq	200(%rax), %rdx
	testq	%rbx, %rbx
	je	.L2969
	movq	(%rbx), %rcx
	movq	%r15, -320(%rbp)
	movq	$0, -248(%rbp)
	movq	%rcx, -288(%rbp)
	leaq	-224(%rbp), %rcx
	movq	%rcx, -352(%rbp)
	.p2align 4,,10
	.p2align 3
.L3065:
	movl	16(%rbx), %ecx
	movl	%ecx, %r8d
	shrl	%r8d
	movl	%r8d, %edi
	leaq	3(%rdi,%rdi,2), %rsi
	salq	$3, %rdi
	andl	$1, %ecx
	leaq	(%rbx,%rsi,8), %r12
	movq	%r12, %r9
	je	.L2970
	movl	20(%r12), %esi
	leaq	32(%r12,%rdi), %r13
	andl	$16777215, %esi
	movl	%esi, %ecx
	salq	$4, %rcx
	movl	12(%rdx,%rcx), %ecx
	testl	%ecx, %ecx
	je	.L2971
.L2972:
	movq	(%r12), %r9
	movzwl	16(%r9), %edi
	leal	-35(%rdi), %r10d
	cmpl	$1, %r10d
	jbe	.L3326
	cmpl	$10, %edi
	sete	%r10b
	cmpl	$1, %edi
	sete	%dl
	orb	%dl, %r10b
	je	.L3057
	cmpl	$2, %ecx
	je	.L3327
.L3057:
	movq	-208(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3304
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3328
.L3059:
	cmpq	$0, -248(%rbp)
	jne	.L3064
.L3305:
	movq	-216(%rbp), %rax
	movq	200(%rax), %rdx
	movq	%r14, -248(%rbp)
.L2971:
	movq	-288(%rbp), %rcx
	movq	%rcx, %rbx
	testq	%rcx, %rcx
	je	.L3291
.L3340:
	movq	(%rcx), %rcx
	movq	%rcx, -288(%rbp)
	jmp	.L3065
	.p2align 4,,10
	.p2align 3
.L3326:
	cmpl	$3, %ecx
	je	.L3329
	cmpl	$2, %ecx
	jne	.L3057
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3330
.L3058:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	movq	-216(%rbp), %rax
	movq	224(%rax), %rbx
	jmp	.L3060
	.p2align 4,,10
	.p2align 3
.L3331:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L3060:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3331
	cmpq	$0, -248(%rbp)
	je	.L3305
	.p2align 4,,10
	.p2align 3
.L3064:
	movq	-248(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, -248(%rbp)
	jmp	.L3304
	.p2align 4,,10
	.p2align 3
.L3329:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3332
.L2975:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L2971
	movq	(%rbx), %rcx
	xorl	%r14d, %r14d
	movq	%r14, -264(%rbp)
	movq	%rcx, -296(%rbp)
	.p2align 4,,10
	.p2align 3
.L3056:
	movl	16(%rbx), %esi
	movl	%esi, %r8d
	shrl	%r8d
	andl	$1, %esi
	movl	%r8d, %ecx
	leaq	3(%rcx,%rcx,2), %rdi
	leaq	(%rbx,%rdi,8), %r12
	leaq	0(,%rcx,8), %rdi
	movq	%r12, %r9
	je	.L2977
	movl	20(%r12), %esi
	leaq	32(%r12,%rdi), %r13
	andl	$16777215, %esi
	movl	%esi, %ecx
	salq	$4, %rcx
	movl	12(%rdx,%rcx), %edx
	testl	%edx, %edx
	je	.L2978
.L2979:
	movq	(%r12), %rdi
	movzwl	16(%rdi), %ecx
	leal	-35(%rcx), %r9d
	cmpl	$1, %r9d
	jbe	.L3333
	cmpl	$10, %ecx
	sete	%r9b
	cmpl	$1, %ecx
	sete	%cl
	orb	%cl, %r9b
	je	.L3048
	cmpl	$2, %edx
	je	.L3334
.L3048:
	movq	-208(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, -256(%rbp)
	testq	%rax, %rax
	je	.L2978
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3335
.L3050:
	cmpq	$0, -264(%rbp)
	jne	.L3129
.L3303:
	movq	-256(%rbp), %rax
	movq	%rax, -264(%rbp)
.L2978:
	movq	-296(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L3055
.L3339:
	movq	-216(%rbp), %rax
	movq	%rcx, %rbx
	movq	(%rcx), %rcx
	movq	200(%rax), %rdx
	movq	%rcx, -296(%rbp)
	jmp	.L3056
	.p2align 4,,10
	.p2align 3
.L3333:
	cmpl	$3, %edx
	je	.L3336
	cmpl	$2, %edx
	jne	.L3048
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3337
.L3049:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	movq	-216(%rbp), %rax
	movq	224(%rax), %rbx
	jmp	.L3051
	.p2align 4,,10
	.p2align 3
.L3338:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L3051:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L3338
	movq	%rax, -256(%rbp)
.L3359:
	cmpq	$0, -264(%rbp)
	je	.L3303
.L3129:
	movq	-256(%rbp), %rsi
	movq	-264(%rbp), %rdi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	-296(%rbp), %rcx
	movq	%rax, -264(%rbp)
	testq	%rcx, %rcx
	jne	.L3339
	.p2align 4,,10
	.p2align 3
.L3055:
	cmpq	$0, -248(%rbp)
	movq	-264(%rbp), %r14
	je	.L3305
	testq	%r14, %r14
	jne	.L3064
.L3304:
	movq	-216(%rbp), %rax
	movq	-288(%rbp), %rcx
	movq	200(%rax), %rdx
	movq	%rcx, %rbx
	testq	%rcx, %rcx
	jne	.L3340
.L3291:
	movq	-320(%rbp), %r15
.L2969:
	movl	20(%r15), %esi
	andl	$16777215, %esi
	movl	%esi, %eax
	salq	$4, %rax
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	(%rdx,%rax), %rbx
	jne	.L3341
	movq	-248(%rbp), %rax
	cmpq	$0, 40(%rax)
	je	.L3067
.L3346:
	movq	16(%rax), %r12
.L3068:
	testq	%r12, %r12
	je	.L3078
	movl	12(%rbx), %eax
	cmpl	%eax, 12(%r12)
	jge	.L3077
.L3078:
	movq	-216(%rbp), %rax
.L3069:
	testb	$2, 24(%rax)
	jne	.L3079
	movq	-248(%rbp), %r12
.L3080:
	movq	(%r15), %rax
	movzwl	16(%rax), %eax
	cmpl	$10, %eax
	je	.L3137
	cmpl	$1, %eax
	je	.L3137
	movq	%r15, -232(%rbp)
	movq	-208(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	cmpl	$40, %eax
	je	.L3342
.L3091:
	call	_ZN2v88internal8compiler8Schedule8PlanNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	-216(%rbp), %rax
	movq	160(%r12), %rbx
	movq	40(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L3343
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L3113
.L3364:
	movq	-232(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%rdi)
.L3114:
	movq	-232(%rbp), %rsi
	movq	-216(%rbp), %rdi
	movl	$4, %edx
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
.L2967:
	movq	-272(%rbp), %rax
	movq	128(%rax), %rbx
	cmpq	%rbx, 160(%rax)
	jne	.L3115
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3335:
	movq	160(%rax), %rax
	movl	20(%r12), %esi
	leaq	.LC6(%rip), %rdi
	movq	%rax, -304(%rbp)
	movq	(%r12), %rax
	andl	$16777215, %esi
	movl	-304(%rbp), %ecx
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3050
	.p2align 4,,10
	.p2align 3
.L2977:
	movq	(%r12), %r12
	movl	20(%r12), %esi
	andl	$16777215, %esi
	movl	%esi, %ecx
	salq	$4, %rcx
	movl	12(%rdx,%rcx), %edx
	testl	%edx, %edx
	je	.L2978
	leaq	16(%r9,%rdi), %r13
	jmp	.L2979
	.p2align 4,,10
	.p2align 3
.L3344:
	movq	(%r15), %rax
	movl	20(%r15), %esi
	leaq	.LC53(%rip), %rdi
	movl	160(%r12), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpq	$0, 40(%r12)
	je	.L3082
.L3345:
	movq	16(%r12), %rax
.L3083:
	testq	%rax, %rax
	je	.L3080
	movl	12(%rbx), %ecx
	cmpl	%ecx, 12(%rax)
	jl	.L3080
	movq	%rax, %r12
.L3077:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3344
	cmpq	$0, 40(%r12)
	jne	.L3345
.L3082:
	movq	32(%r12), %r14
	testq	%r14, %r14
	je	.L3080
	movq	-216(%rbp), %rax
	movq	232(%rax), %rdx
	movslq	(%r14), %rax
	testl	%eax, %eax
	js	.L3084
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	40(%rdx), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L3084
.L3085:
	movq	16(%rax), %rdx
	movq	8(%rax), %r13
	cmpq	%rdx, %r13
	je	.L3088
	movq	%rdx, -248(%rbp)
	movq	%r14, %rax
	movq	%rbx, %r14
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3087:
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	cmpq	%rax, %r12
	jne	.L3080
	addq	$8, %r13
	cmpq	%r13, -248(%rbp)
	jne	.L3087
	movq	%rbx, %rax
	movq	%r14, %rbx
	movq	%rax, %r14
.L3088:
	movq	16(%r14), %rax
	jmp	.L3083
	.p2align 4,,10
	.p2align 3
.L3084:
	leaq	136(%rdx), %rax
	jmp	.L3085
	.p2align 4,,10
	.p2align 3
.L3341:
	movq	-248(%rbp), %rcx
	movl	160(%rbx), %r9d
	leaq	.LC52(%rip), %rdi
	movq	160(%rcx), %rax
	movl	48(%rcx), %r8d
	movq	%rax, -256(%rbp)
	movq	(%r15), %rax
	movl	-256(%rbp), %ecx
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-248(%rbp), %rax
	cmpq	$0, 40(%rax)
	jne	.L3346
.L3067:
	movq	32(%rax), %r13
	movq	-216(%rbp), %rax
	testq	%r13, %r13
	je	.L3069
	movq	232(%rax), %rdx
	movslq	0(%r13), %rax
	testl	%eax, %eax
	js	.L3070
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	40(%rdx), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L3070
.L3071:
	movq	16(%rax), %r14
	movq	8(%rax), %r12
	cmpq	%r14, %r12
	je	.L3075
	.p2align 4,,10
	.p2align 3
.L3074:
	movq	(%r12), %rsi
	movq	-248(%rbp), %rdi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	cmpq	%rax, -248(%rbp)
	jne	.L3078
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L3074
.L3075:
	movq	16(%r13), %r12
	jmp	.L3068
	.p2align 4,,10
	.p2align 3
.L3137:
	movq	-216(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9Scheduler19FuseFloatingControlEPNS1_10BasicBlockEPNS1_4NodeE
	jmp	.L2967
	.p2align 4,,10
	.p2align 3
.L2970:
	movq	(%r12), %r12
	movl	20(%r12), %esi
	andl	$16777215, %esi
	movl	%esi, %ecx
	salq	$4, %rcx
	movl	12(%rdx,%rcx), %ecx
	testl	%ecx, %ecx
	je	.L2971
	leaq	16(%r9,%rdi), %r13
	jmp	.L2972
	.p2align 4,,10
	.p2align 3
.L3328:
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r14), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3059
	.p2align 4,,10
	.p2align 3
.L3325:
	movq	(%r15), %rax
	movl	20(%r15), %esi
	leaq	.LC51(%rip), %rdi
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2968
	.p2align 4,,10
	.p2align 3
.L3327:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3347
.L3061:
	movq	0(%r13), %r12
	movq	224(%rax), %rbx
	jmp	.L3062
	.p2align 4,,10
	.p2align 3
.L3348:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L3062:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3348
	jmp	.L3059
	.p2align 4,,10
	.p2align 3
.L3336:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3349
.L2982:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L2978
	movq	$0, -256(%rbp)
	movq	(%rbx), %rax
	movq	%rax, -304(%rbp)
	.p2align 4,,10
	.p2align 3
.L3047:
	movl	16(%rbx), %esi
	movq	-216(%rbp), %rcx
	movl	%esi, %r8d
	movq	200(%rcx), %rdi
	shrl	%r8d
	andl	$1, %esi
	movl	%r8d, %eax
	leaq	3(%rax,%rax,2), %rdx
	leaq	(%rbx,%rdx,8), %r12
	leaq	0(,%rax,8), %rdx
	movq	%r12, %r9
	je	.L2984
	movl	20(%r12), %esi
	leaq	32(%r12,%rdx), %r13
	andl	$16777215, %esi
	movl	%esi, %eax
	salq	$4, %rax
	movl	12(%rdi,%rax), %eax
	testl	%eax, %eax
	je	.L2985
.L2986:
	movq	(%r12), %rdi
	movzwl	16(%rdi), %edx
	leal	-35(%rdx), %r9d
	cmpl	$1, %r9d
	jbe	.L3350
	cmpl	$10, %edx
	sete	%r9b
	cmpl	$1, %edx
	sete	%dl
	orb	%dl, %r9b
	je	.L3039
	cmpl	$2, %eax
	je	.L3351
.L3039:
	movq	-208(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2985
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3352
.L3041:
	cmpq	$0, -256(%rbp)
	jne	.L3128
.L3302:
	movq	%r15, -256(%rbp)
.L2985:
	movq	-304(%rbp), %rax
	testq	%rax, %rax
	je	.L3046
.L3356:
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%rax, -304(%rbp)
	jmp	.L3047
	.p2align 4,,10
	.p2align 3
.L3350:
	cmpl	$3, %eax
	je	.L3353
	cmpl	$2, %eax
	jne	.L3039
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3354
.L3040:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	movq	-216(%rbp), %rax
	movq	224(%rax), %rbx
	jmp	.L3042
	.p2align 4,,10
	.p2align 3
.L3355:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L3042:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3355
	cmpq	$0, -256(%rbp)
	je	.L3302
	.p2align 4,,10
	.p2align 3
.L3128:
	movq	-256(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, -256(%rbp)
	movq	-304(%rbp), %rax
	testq	%rax, %rax
	jne	.L3356
	.p2align 4,,10
	.p2align 3
.L3046:
	cmpq	$0, -264(%rbp)
	je	.L3303
	cmpq	$0, -256(%rbp)
	je	.L2978
	jmp	.L3129
	.p2align 4,,10
	.p2align 3
.L2984:
	movq	(%r12), %r12
	movl	20(%r12), %esi
	andl	$16777215, %esi
	movl	%esi, %eax
	salq	$4, %rax
	movl	12(%rdi,%rax), %eax
	testl	%eax, %eax
	je	.L2985
	leaq	16(%r9,%rdx), %r13
	jmp	.L2986
	.p2align 4,,10
	.p2align 3
.L2963:
	movq	104(%rcx), %rax
	testq	%rax, %rax
	je	.L2965
	cmpq	$64, 8(%rax)
	ja	.L2966
.L2965:
	movq	-272(%rbp), %rbx
	movq	136(%rbx), %rax
	movq	$64, 8(%rax)
	movq	104(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 104(%rbx)
.L2966:
	movq	-272(%rbp), %rcx
	movq	152(%rcx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 152(%rcx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 136(%rcx)
	movq	%rdx, 144(%rcx)
	movq	%rax, 128(%rcx)
	jmp	.L2964
	.p2align 4,,10
	.p2align 3
.L3352:
	movq	(%r12), %rax
	movl	20(%r12), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r15), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3041
	.p2align 4,,10
	.p2align 3
.L3334:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3357
.L3052:
	movq	0(%r13), %r12
	movq	224(%rax), %rbx
	jmp	.L3053
	.p2align 4,,10
	.p2align 3
.L3358:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L3053:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L3358
	movq	%rax, -256(%rbp)
	jmp	.L3359
	.p2align 4,,10
	.p2align 3
.L3079:
	movq	-248(%rbp), %rsi
	leaq	-224(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal8compiler23ScheduleLateNodeVisitor9SplitNodeEPNS1_10BasicBlockEPNS1_4NodeE
	movq	%rax, %r12
	jmp	.L3080
	.p2align 4,,10
	.p2align 3
.L3342:
	call	_ZN2v88internal8compiler8Schedule8PlanNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	-216(%rbp), %rdx
	movq	160(%r12), %rax
	movq	40(%rdx), %rdx
	leaq	0(,%rax,8), %rbx
	movq	(%rdx,%rax,8), %rdi
	testq	%rdi, %rdi
	je	.L3360
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L3095
.L3388:
	movq	-232(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%rdi)
.L3096:
	movq	-232(%rbp), %rsi
	movq	-216(%rbp), %rdi
	movl	$4, %edx
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	xorl	%esi, %esi
	movq	%r15, %rdi
	jmp	.L3306
	.p2align 4,,10
	.p2align 3
.L3098:
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L3101
.L3362:
	movq	-232(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%rdi)
.L3307:
	movq	-232(%rbp), %rsi
	movq	-216(%rbp), %rdi
	movl	$4, %edx
	call	_ZN2v88internal8compiler9Scheduler15UpdatePlacementEPNS1_4NodeENS2_9PlacementE
	xorl	%esi, %esi
	movq	%r13, %rdi
.L3306:
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	-208(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	%r13, %rdx
	cmpw	$39, 16(%rax)
	movq	%r13, -232(%rbp)
	je	.L3091
	call	_ZN2v88internal8compiler8Schedule8PlanNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	movq	-216(%rbp), %rdx
	movq	160(%r12), %rax
	movq	40(%rdx), %rdx
	leaq	0(,%rax,8), %rbx
	movq	(%rdx,%rax,8), %rdi
	testq	%rdi, %rdi
	jne	.L3098
	movq	-224(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L3361
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3100:
	movq	%rdi, (%rax)
	movq	$0, 8(%rax)
	movq	-216(%rbp), %rdx
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	40(%rdx), %rdx
	movq	%rax, (%rdx,%rbx)
	movq	-216(%rbp), %rax
	movq	40(%rax), %rax
	movq	(%rax,%rbx), %rdi
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	jne	.L3362
.L3101:
	movq	-328(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L3307
	.p2align 4,,10
	.p2align 3
.L3343:
	movq	-224(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L3363
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3112:
	movq	%rdi, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	-216(%rbp), %rdx
	movq	40(%rdx), %rdx
	movq	%rax, (%rdx,%rbx,8)
	movq	-216(%rbp), %rax
	movq	40(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	jne	.L3364
.L3113:
	movq	-328(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L3114
	.p2align 4,,10
	.p2align 3
.L3332:
	movq	8(%r9), %rdx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-216(%rbp), %rax
	movq	200(%rax), %rdx
	jmp	.L2975
	.p2align 4,,10
	.p2align 3
.L3353:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3365
.L2989:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L2985
	movq	(%rbx), %rax
	xorl	%r15d, %r15d
	movq	%rax, -312(%rbp)
	.p2align 4,,10
	.p2align 3
.L3038:
	movl	16(%rbx), %esi
	movq	-216(%rbp), %rcx
	movl	%esi, %r8d
	movq	200(%rcx), %rdi
	shrl	%r8d
	andl	$1, %esi
	movl	%r8d, %eax
	leaq	3(%rax,%rax,2), %rdx
	leaq	(%rbx,%rdx,8), %r13
	leaq	0(,%rax,8), %rdx
	movq	%r13, %r9
	je	.L2991
	movl	20(%r13), %esi
	leaq	32(%r13,%rdx), %r12
	andl	$16777215, %esi
	movl	%esi, %eax
	salq	$4, %rax
	movl	12(%rdi,%rax), %eax
	testl	%eax, %eax
	je	.L2992
.L2993:
	movq	0(%r13), %rdi
	movzwl	16(%rdi), %edx
	leal	-35(%rdx), %r9d
	cmpl	$1, %r9d
	jbe	.L3366
	cmpl	$10, %edx
	sete	%r9b
	cmpl	$1, %edx
	sete	%dl
	orb	%dl, %r9b
	je	.L3030
	cmpl	$2, %eax
	je	.L3367
.L3030:
	movq	-208(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2992
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3368
.L3032:
	testq	%r15, %r15
	jne	.L3127
.L3301:
	movq	%r12, %r15
.L2992:
	movq	-312(%rbp), %rax
	testq	%rax, %rax
	je	.L3037
.L3372:
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%rax, -312(%rbp)
	jmp	.L3038
	.p2align 4,,10
	.p2align 3
.L2991:
	movq	0(%r13), %r13
	movl	20(%r13), %esi
	andl	$16777215, %esi
	movl	%esi, %eax
	salq	$4, %rax
	movl	12(%rdi,%rax), %eax
	testl	%eax, %eax
	je	.L2992
	leaq	16(%r9,%rdx), %r12
	jmp	.L2993
	.p2align 4,,10
	.p2align 3
.L3366:
	cmpl	$3, %eax
	je	.L3369
	cmpl	$2, %eax
	jne	.L3030
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3370
.L3031:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	movq	-216(%rbp), %rax
	movq	224(%rax), %rbx
	jmp	.L3033
	.p2align 4,,10
	.p2align 3
.L3371:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L3033:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L3371
	movq	%rax, %r12
.L3386:
	testq	%r15, %r15
	je	.L3301
.L3127:
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, %r15
	movq	-312(%rbp), %rax
	testq	%rax, %rax
	jne	.L3372
	.p2align 4,,10
	.p2align 3
.L3037:
	cmpq	$0, -256(%rbp)
	je	.L3302
	testq	%r15, %r15
	je	.L2985
	jmp	.L3128
	.p2align 4,,10
	.p2align 3
.L3070:
	leaq	136(%rdx), %rax
	jmp	.L3071
	.p2align 4,,10
	.p2align 3
.L3368:
	movq	0(%r13), %rax
	movl	20(%r13), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r12), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3032
	.p2align 4,,10
	.p2align 3
.L3324:
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	20(%rax), %edx
	movq	%rax, -240(%rbp)
	movq	%rax, %rdi
	movq	-216(%rbp), %rax
	andl	$16777215, %edx
	salq	$4, %rdx
	addq	200(%rax), %rdx
	jmp	.L2958
	.p2align 4,,10
	.p2align 3
.L3347:
	movq	8(%r9), %rcx
	movl	%esi, %edx
	xorl	%eax, %eax
	movl	%r8d, %esi
	leaq	.LC5(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-216(%rbp), %rax
	jmp	.L3061
	.p2align 4,,10
	.p2align 3
.L3351:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3373
.L3043:
	movq	0(%r13), %r12
	movq	224(%rcx), %rbx
	jmp	.L3044
	.p2align 4,,10
	.p2align 3
.L3374:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L3044:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3374
	jmp	.L3041
	.p2align 4,,10
	.p2align 3
.L3330:
	movq	8(%r9), %rcx
	movl	%esi, %edx
	leaq	.LC4(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3058
	.p2align 4,,10
	.p2align 3
.L3349:
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2982
	.p2align 4,,10
	.p2align 3
.L3369:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3375
.L2996:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L2992
	movq	(%rbx), %rax
	movq	%r15, -360(%rbp)
	xorl	%r12d, %r12d
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L3029:
	movl	16(%rbx), %edx
	movl	%edx, %r8d
	shrl	%r8d
	movl	%r8d, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%rbx,%rcx,8), %r14
	movq	-216(%rbp), %rcx
	movq	%r14, %r9
	movq	200(%rcx), %rdi
	je	.L2998
	movl	20(%r14), %esi
	leaq	32(%r14,%rax), %r13
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%rdi,%rdx), %edx
	testl	%edx, %edx
	je	.L2999
.L3000:
	movq	(%r14), %rdi
	movzwl	16(%rdi), %eax
	leal	-35(%rax), %r9d
	cmpl	$1, %r9d
	jbe	.L3376
	cmpl	$10, %eax
	sete	%r9b
	cmpl	$1, %eax
	sete	%al
	orb	%al, %r9b
	je	.L3021
	cmpl	$2, %edx
	je	.L3377
.L3021:
	movq	-208(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2999
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3378
.L3023:
	testq	%r12, %r12
	jne	.L3126
.L3300:
	movq	%r13, %r12
.L2999:
	testq	%r15, %r15
	je	.L3028
.L3382:
	movq	%r15, %rbx
	movq	(%r15), %r15
	jmp	.L3029
	.p2align 4,,10
	.p2align 3
.L2998:
	movq	(%r14), %r14
	movl	20(%r14), %esi
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%rdi,%rdx), %edx
	testl	%edx, %edx
	je	.L2999
	leaq	16(%r9,%rax), %r13
	jmp	.L3000
	.p2align 4,,10
	.p2align 3
.L3376:
	cmpl	$3, %edx
	je	.L3379
	cmpl	$2, %edx
	jne	.L3021
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3380
.L3022:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%rbx), %esi
	movq	%rax, %rdi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
	movq	-216(%rbp), %rax
	movq	224(%rax), %rbx
	jmp	.L3024
	.p2align 4,,10
	.p2align 3
.L3381:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
.L3024:
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L3381
	movq	%rax, %r13
.L3399:
	testq	%r12, %r12
	je	.L3300
.L3126:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, %r12
	testq	%r15, %r15
	jne	.L3382
	.p2align 4,,10
	.p2align 3
.L3028:
	movq	-360(%rbp), %r15
	testq	%r15, %r15
	je	.L3301
	testq	%r12, %r12
	je	.L2992
	jmp	.L3127
	.p2align 4,,10
	.p2align 3
.L2957:
	addq	$8, -344(%rbp)
	movq	-344(%rbp), %rax
	cmpq	%rax, -368(%rbp)
	jne	.L3118
.L3119:
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L2937
	movq	-64(%rbp), %rbx
	movq	-96(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L3120
	movq	-144(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L3123:
	testq	%rax, %rax
	je	.L3121
	cmpq	$64, 8(%rax)
	ja	.L3122
.L3121:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-144(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -144(%rbp)
.L3122:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L3123
	movq	-136(%rbp), %rax
.L3120:
	movq	-128(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L2937
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L2937:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3383
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3378:
	.cfi_restore_state
	movq	(%r14), %rax
	movl	20(%r14), %esi
	leaq	.LC6(%rip), %rdi
	movl	160(%r13), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3023
	.p2align 4,,10
	.p2align 3
.L3357:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	xorl	%eax, %eax
	movl	%r8d, %esi
	leaq	.LC5(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-216(%rbp), %rax
	jmp	.L3052
	.p2align 4,,10
	.p2align 3
.L3367:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3384
.L3034:
	movq	(%r12), %r13
	movq	224(%rcx), %rbx
	jmp	.L3035
	.p2align 4,,10
	.p2align 3
.L3385:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
.L3035:
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L3385
	movq	%rax, %r12
	jmp	.L3386
	.p2align 4,,10
	.p2align 3
.L3337:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC4(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3049
	.p2align 4,,10
	.p2align 3
.L3360:
	movq	-224(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L3387
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3094:
	movq	%rdi, (%rax)
	movq	$0, 8(%rax)
	movq	-216(%rbp), %rdx
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	40(%rdx), %rdx
	movq	%rax, (%rdx,%rbx)
	movq	-216(%rbp), %rax
	movq	40(%rax), %rax
	movq	(%rax,%rbx), %rdi
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	jne	.L3388
.L3095:
	movq	-328(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L3096
	.p2align 4,,10
	.p2align 3
.L3365:
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2989
	.p2align 4,,10
	.p2align 3
.L3379:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3389
.L3003:
	movq	24(%r14), %r8
	testq	%r8, %r8
	je	.L2999
	movq	%r12, -384(%rbp)
	movq	(%r8), %rbx
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L3020:
	movl	16(%r8), %edx
	movl	%edx, %r9d
	shrl	%r9d
	movl	%r9d, %eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%r8,%rcx,8), %r14
	movq	-216(%rbp), %rcx
	movq	%r14, %rdi
	movq	200(%rcx), %r10
	je	.L3005
	movl	20(%r14), %esi
	leaq	32(%r14,%rax), %r12
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%r10,%rdx), %edx
	testl	%edx, %edx
	je	.L3006
	movq	(%r14), %r11
	movzwl	16(%r11), %edi
	leal	-35(%rdi), %eax
	cmpl	$1, %eax
	jbe	.L3390
.L3008:
	cmpl	$10, %edi
	sete	%r8b
	cmpl	$1, %edi
	sete	%al
	orb	%al, %r8b
	je	.L3012
	cmpl	$2, %edx
	je	.L3391
.L3012:
	movq	-208(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L3006
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3392
.L3014:
	testq	%r13, %r13
	jne	.L3125
.L3299:
	movq	%r8, %r13
.L3006:
	testq	%rbx, %rbx
	je	.L3019
.L3396:
	movq	%rbx, %r8
	movq	(%rbx), %rbx
	jmp	.L3020
	.p2align 4,,10
	.p2align 3
.L3005:
	movq	(%r14), %r14
	movl	20(%r14), %esi
	andl	$16777215, %esi
	movl	%esi, %edx
	salq	$4, %rdx
	movl	12(%r10,%rdx), %edx
	testl	%edx, %edx
	je	.L3006
	movq	(%r14), %r11
	leaq	16(%rdi,%rax), %r12
	movzwl	16(%r11), %edi
	leal	-35(%rdi), %eax
	cmpl	$1, %eax
	ja	.L3008
.L3390:
	cmpl	$3, %edx
	je	.L3393
	cmpl	$2, %edx
	jne	.L3012
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3394
.L3013:
	movq	%r14, %rdi
	xorl	%esi, %esi
	movq	%r8, -376(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-376(%rbp), %r8
	movq	%rax, %rdi
	movl	16(%r8), %esi
	shrl	%esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
	movq	-216(%rbp), %rax
	movq	224(%rax), %r14
	jmp	.L3015
	.p2align 4,,10
	.p2align 3
.L3395:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L3015:
	movq	16(%r14), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L3395
	movq	%rax, %r8
.L3403:
	testq	%r13, %r13
	je	.L3299
.L3125:
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler10BasicBlock18GetCommonDominatorEPS2_S3_@PLT
	movq	%rax, %r13
	testq	%rbx, %rbx
	jne	.L3396
	.p2align 4,,10
	.p2align 3
.L3019:
	movq	-384(%rbp), %r12
	testq	%r12, %r12
	je	.L3300
	testq	%r13, %r13
	je	.L2999
	jmp	.L3126
	.p2align 4,,10
	.p2align 3
.L3361:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-224(%rbp), %rdi
	jmp	.L3100
	.p2align 4,,10
	.p2align 3
.L3392:
	movq	(%r14), %rax
	movl	20(%r14), %esi
	leaq	.LC6(%rip), %rdi
	movq	%r8, -376(%rbp)
	movl	160(%r8), %ecx
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-376(%rbp), %r8
	jmp	.L3014
	.p2align 4,,10
	.p2align 3
.L3373:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC5(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-216(%rbp), %rcx
	jmp	.L3043
	.p2align 4,,10
	.p2align 3
.L3377:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3397
.L3025:
	movq	0(%r13), %r14
	movq	224(%rcx), %rbx
	jmp	.L3026
	.p2align 4,,10
	.p2align 3
.L3398:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
.L3026:
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L3398
	movq	%rax, %r13
	jmp	.L3399
	.p2align 4,,10
	.p2align 3
.L3354:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC4(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3040
	.p2align 4,,10
	.p2align 3
.L3375:
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2996
	.p2align 4,,10
	.p2align 3
.L2961:
	leaq	-240(%rbp), %rsi
	leaq	96(%rbx), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L2962
	.p2align 4,,10
	.p2align 3
.L3393:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3400
.L3010:
	movq	-352(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler23ScheduleLateNodeVisitor24GetCommonDominatorOfUsesEPNS1_4NodeE
	movq	%rax, %r8
	testq	%r13, %r13
	je	.L3299
	testq	%rax, %rax
	je	.L3006
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3384:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC5(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-216(%rbp), %rcx
	jmp	.L3034
	.p2align 4,,10
	.p2align 3
.L3391:
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3401
.L3016:
	movq	(%r12), %r12
	movq	224(%rcx), %r14
	jmp	.L3017
	.p2align 4,,10
	.p2align 3
.L3402:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r12
.L3017:
	movq	16(%r14), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L3402
	movq	%rax, %r8
	jmp	.L3403
	.p2align 4,,10
	.p2align 3
.L3370:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC4(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3031
	.p2align 4,,10
	.p2align 3
.L3363:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-224(%rbp), %rdi
	jmp	.L3112
	.p2align 4,,10
	.p2align 3
.L3389:
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3003
	.p2align 4,,10
	.p2align 3
.L3401:
	movq	8(%r11), %rcx
	movl	%esi, %edx
	leaq	.LC5(%rip), %rdi
	movl	%r9d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-216(%rbp), %rcx
	jmp	.L3016
	.p2align 4,,10
	.p2align 3
.L3397:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC5(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-216(%rbp), %rcx
	jmp	.L3025
	.p2align 4,,10
	.p2align 3
.L3380:
	movq	8(%rdi), %rcx
	movl	%esi, %edx
	leaq	.LC4(%rip), %rdi
	movl	%r8d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3022
	.p2align 4,,10
	.p2align 3
.L3321:
	leaq	.LC50(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L2939
	xorl	%eax, %eax
	leaq	.LC42(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	72(%r12), %rbx
	movq	80(%r12), %r13
	movzbl	_ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip), %eax
	cmpq	%r13, %rbx
	je	.L2943
	leaq	.LC43(%rip), %r14
.L2942:
	testb	%al, %al
	je	.L2939
	movq	(%rbx), %rax
	movq	%r14, %rdi
	addq	$8, %rbx
	movq	(%rax), %rdx
	movl	20(%rax), %esi
	xorl	%eax, %eax
	movq	8(%rdx), %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	_ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip), %eax
	cmpq	%rbx, %r13
	jne	.L2942
.L2943:
	testb	%al, %al
	je	.L2939
	leaq	.LC44(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2939
	.p2align 4,,10
	.p2align 3
.L3400:
	movq	8(%r11), %rdx
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3010
	.p2align 4,,10
	.p2align 3
.L3323:
	movq	(%rax), %rdx
	movq	%rdx, -144(%rbp)
	jmp	.L2949
	.p2align 4,,10
	.p2align 3
.L3394:
	movq	8(%r11), %rcx
	movl	%esi, %edx
	leaq	.LC4(%rip), %rdi
	movl	%r9d, %esi
	xorl	%eax, %eax
	movq	%r8, -376(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-376(%rbp), %r8
	jmp	.L3013
	.p2align 4,,10
	.p2align 3
.L3387:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-224(%rbp), %rdi
	jmp	.L3094
.L2947:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2949
	.p2align 4,,10
	.p2align 3
.L3322:
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2945
.L3383:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11016:
	.size	_ZN2v88internal8compiler9Scheduler12ScheduleLateEv, .-_ZN2v88internal8compiler9Scheduler12ScheduleLateEv
	.section	.text._ZNSt11_Deque_baseIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm:
.LFB13812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$5, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L3418
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L3406:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L3407
	movq	%r15, %rbx
	jmp	.L3412
	.p2align 4,,10
	.p2align 3
.L3408:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L3419
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3409:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L3407
.L3412:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L3408
	cmpq	$31, 8(%rax)
	jbe	.L3408
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L3412
.L3407:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$31, %r13d
	salq	$4, %r13
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	addq	%rax, %r13
	leaq	512(%rax), %rcx
	movq	%rax, 72(%r12)
	movq	%r13, 64(%r12)
	movq	%rcx, 80(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3419:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3409
	.p2align 4,,10
	.p2align 3
.L3418:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L3406
	.cfi_endproc
.LFE13812:
	.size	_ZNSt11_Deque_baseIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm
	.section	.rodata._ZN2v88internal8compiler9Scheduler11PrepareUsesEv.str1.8,"aMS",@progbits,1
	.align 8
.LC54:
	.string	"--- PREPARE USES -------------------------------------------\n"
	.align 8
.LC55:
	.string	"Scheduling fixed position node #%d:%s\n"
	.section	.text._ZN2v88internal8compiler9Scheduler11PrepareUsesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler11PrepareUsesEv
	.type	_ZN2v88internal8compiler9Scheduler11PrepareUsesEv, @function
_ZN2v88internal8compiler9Scheduler11PrepareUsesEv:
.LFB10966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$344, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	jne	.L3517
.L3421:
	movq	8(%rbx), %rax
	movq	16(%rbx), %r13
	xorl	%r12d, %r12d
	movq	(%rbx), %rdi
	movl	28(%rax), %eax
	testq	%rax, %rax
	jne	.L3518
.L3426:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rdi, -256(%rbp)
	leaq	-256(%rbp), %rdi
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	pxor	%xmm0, %xmm0
	je	.L3519
	leaq	-160(%rbp), %rax
	movdqa	-256(%rbp), %xmm7
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movq	%rax, %rdi
	movq	%rax, -376(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm
	movdqa	-96(%rbp), %xmm5
	movq	-216(%rbp), %r9
	movq	-224(%rbp), %xmm3
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %xmm1
	movq	-184(%rbp), %rcx
	movaps	%xmm5, -192(%rbp)
	movq	%r9, %xmm5
	punpcklqdq	%xmm5, %xmm3
	movdqa	-128(%rbp), %xmm7
	movq	-208(%rbp), %xmm2
	movaps	%xmm3, -320(%rbp)
	movq	%r8, %xmm3
	movq	-168(%rbp), %rdx
	movdqa	-112(%rbp), %xmm6
	punpcklqdq	%xmm3, %xmm2
	movq	-248(%rbp), %rax
	movq	-176(%rbp), %xmm0
	movaps	%xmm7, -224(%rbp)
	movaps	%xmm2, -304(%rbp)
	movq	%rcx, %xmm2
	movq	-256(%rbp), %xmm4
	movq	-240(%rbp), %r11
	punpcklqdq	%xmm2, %xmm1
	movq	-232(%rbp), %r10
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm6, -208(%rbp)
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	%rax, %xmm6
	movaps	%xmm1, -288(%rbp)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm6, %xmm4
	movq	%r11, -336(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -240(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%r10, -328(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm4, -352(%rbp)
	movaps	%xmm0, -272(%rbp)
	testq	%rsi, %rsi
	je	.L3428
	movq	-168(%rbp), %rcx
	movq	-200(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L3429
	.p2align 4,,10
	.p2align 3
.L3432:
	testq	%rax, %rax
	je	.L3430
	cmpq	$32, 8(%rax)
	ja	.L3431
.L3430:
	movq	(%rdx), %rax
	movq	$32, 8(%rax)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -248(%rbp)
.L3431:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L3432
	movq	-232(%rbp), %rdi
	movq	-240(%rbp), %rsi
.L3429:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L3428
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L3428:
	movq	8(%rbx), %rax
	movq	16(%rax), %r15
	movq	%r15, -160(%rbp)
	movl	20(%r15), %edx
	andl	$16777215, %edx
	salq	$4, %rdx
	addq	200(%rbx), %rdx
	cmpl	$2, 12(%rdx)
	je	.L3439
	movq	(%r15), %rax
	movzwl	16(%rax), %eax
	cmpw	$36, %ax
	ja	.L3435
	cmpw	$34, %ax
	ja	.L3436
.L3437:
	movl	$1, 12(%rdx)
.L3489:
	movl	20(%r15), %ecx
	movl	$1, %edx
	movq	%rcx, %rax
	salq	%cl, %rdx
	shrq	$3, %rax
	andl	$2097144, %eax
	orq	%rdx, (%r12,%rax)
	movzbl	23(%r15), %eax
	leaq	32(%r15), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3447
	leaq	-24(%r15), %rax
	movq	%rax, %xmm0
.L3448:
	leaq	-352(%rbp), %rax
	movq	%rdx, %xmm6
	movq	-376(%rbp), %rsi
	punpcklqdq	%xmm6, %xmm0
	movq	%rax, %rdi
	movq	%rax, -384(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	movq	-320(%rbp), %rax
	cmpq	%rax, -288(%rbp)
	jne	.L3449
	jmp	.L3450
	.p2align 4,,10
	.p2align 3
.L3524:
	movq	-16(%rax), %r14
	movl	16(%r14), %eax
	movl	%eax, %edx
	shrl	%edx
	movl	%edx, %ecx
	leaq	3(%rcx,%rcx,2), %rcx
	leaq	(%r14,%rcx,8), %rcx
	testb	$1, %al
	jne	.L3453
	movq	(%rcx), %rcx
.L3453:
	movq	%rcx, %rsi
	movq	%r13, %rdi
	movl	%edx, -364(%rbp)
	movq	%rcx, -360(%rbp)
	call	_ZN2v88internal8compiler8Schedule11IsScheduledEPNS1_4NodeE@PLT
	movq	-360(%rbp), %rcx
	movl	-364(%rbp), %edx
	testb	%al, %al
	je	.L3520
.L3454:
	movl	16(%r14), %ecx
	movl	%ecx, %eax
	shrl	%eax
	andl	$1, %ecx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r14,%rax,8), %rdx
	jne	.L3455
	movq	(%rdx), %rdx
.L3455:
	movzbl	23(%rdx), %eax
	leaq	32(%rdx), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3457
	movq	32(%rdx), %rcx
	movl	8(%rcx), %eax
	addq	$16, %rcx
.L3457:
	cltq
	leaq	(%rcx,%rax,8), %rcx
	movq	-288(%rbp), %rax
	cmpq	-280(%rbp), %rax
	je	.L3521
.L3458:
	movq	-8(%rax), %rdi
	movdqu	-16(%rax), %xmm0
	paddq	.LC56(%rip), %xmm0
	leaq	8(%rdi), %rdx
	movups	%xmm0, -16(%rax)
	movq	-288(%rbp), %rax
	cmpq	%rcx, %rdx
	je	.L3522
.L3461:
	cmpq	%rax, -320(%rbp)
	je	.L3450
.L3449:
	movq	248(%rbx), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	-288(%rbp), %rax
	cmpq	-280(%rbp), %rax
	je	.L3523
.L3451:
	movq	-8(%rax), %rdx
	movq	(%rdx), %r15
	movl	$1, %edx
	movl	20(%r15), %ecx
	movl	%ecx, %esi
	salq	%cl, %rdx
	andl	$16777215, %esi
	movq	%rsi, %rdi
	shrq	$6, %rdi
	testq	%rdx, (%r12,%rdi,8)
	jne	.L3524
	salq	$4, %rsi
	addq	200(%rbx), %rsi
	movq	%r15, -160(%rbp)
	cmpl	$2, 12(%rsi)
	movq	%rsi, %r14
	je	.L3469
	movq	(%r15), %rax
	movzwl	16(%rax), %eax
	cmpw	$36, %ax
	ja	.L3465
	cmpw	$34, %ax
	ja	.L3466
.L3467:
	movl	$1, 12(%r14)
.L3488:
	movl	20(%r15), %ecx
	movl	$1, %edx
	movq	%rcx, %rax
	salq	%cl, %rdx
	shrq	$3, %rax
	andl	$2097144, %eax
	orq	%rdx, (%r12,%rax)
	movzbl	23(%r15), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L3477
	testl	%eax, %eax
	jne	.L3478
.L3515:
	movq	-288(%rbp), %rax
	cmpq	%rax, -320(%rbp)
	jne	.L3449
.L3450:
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L3420
	movq	-264(%rbp), %rbx
	movq	-296(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L3483
	movq	-344(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L3486:
	testq	%rax, %rax
	je	.L3484
	cmpq	$32, 8(%rax)
	ja	.L3485
.L3484:
	movq	(%rdx), %rax
	movq	$32, 8(%rax)
	movq	-344(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -344(%rbp)
.L3485:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L3486
	movq	-336(%rbp), %rax
.L3483:
	movq	-328(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L3420
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L3420:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3525
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3523:
	.cfi_restore_state
	movq	-264(%rbp), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3451
	.p2align 4,,10
	.p2align 3
.L3465:
	subl	$50, %eax
	cmpw	$1, %ax
	ja	.L3467
.L3516:
	movl	$2, 12(%r14)
.L3469:
	movq	80(%rbx), %rsi
	cmpq	88(%rbx), %rsi
	je	.L3472
	movq	-160(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 80(%rbx)
.L3473:
	movq	-160(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8Schedule11IsScheduledEPNS1_4NodeE@PLT
	testb	%al, %al
	jne	.L3488
	movq	-160(%rbp), %rdx
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	(%rdx), %rax
	jne	.L3526
.L3474:
	cmpw	$50, 16(%rax)
	jne	.L3475
	movq	104(%r13), %rsi
.L3476:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	jmp	.L3488
	.p2align 4,,10
	.p2align 3
.L3520:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler9Scheduler28IncrementUnscheduledUseCountEPNS1_4NodeEiS4_
	jmp	.L3454
	.p2align 4,,10
	.p2align 3
.L3522:
	movq	-280(%rbp), %rdx
	cmpq	%rdx, %rax
	je	.L3460
	subq	$16, %rax
	movq	%rax, -288(%rbp)
	jmp	.L3461
	.p2align 4,,10
	.p2align 3
.L3521:
	movq	-264(%rbp), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3458
	.p2align 4,,10
	.p2align 3
.L3477:
	movq	32(%r15), %r8
	movl	8(%r8), %eax
	testl	%eax, %eax
	jle	.L3515
	leaq	-24(%r8), %rax
	addq	$16, %r8
	movq	%rax, %xmm0
	jmp	.L3481
	.p2align 4,,10
	.p2align 3
.L3435:
	subl	$50, %eax
	cmpw	$1, %ax
	ja	.L3437
.L3440:
	movl	$2, 12(%rdx)
.L3439:
	movq	80(%rbx), %rsi
	cmpq	88(%rbx), %rsi
	je	.L3442
	movq	-160(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 80(%rbx)
.L3443:
	movq	-160(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8Schedule11IsScheduledEPNS1_4NodeE@PLT
	testb	%al, %al
	jne	.L3489
	movq	-160(%rbp), %rdx
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	movq	(%rdx), %rax
	jne	.L3527
.L3444:
	cmpw	$50, 16(%rax)
	jne	.L3445
	movq	104(%r13), %rsi
.L3446:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8Schedule7AddNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	jmp	.L3489
	.p2align 4,,10
	.p2align 3
.L3478:
	leaq	-24(%r15), %rax
	leaq	32(%r15), %r8
	movq	%rax, %xmm0
.L3481:
	movq	%r8, %xmm5
	movq	-376(%rbp), %rsi
	movq	-384(%rbp), %rdi
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler4Node10InputEdges8iteratorENS1_22RecyclingZoneAllocatorIS5_EEE12emplace_backIJS5_EEEvDpOT_
	jmp	.L3515
	.p2align 4,,10
	.p2align 3
.L3466:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	20(%rax), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	200(%rbx), %rax
	cmpl	$2, 12(%rax)
	je	.L3516
	movl	$3, 12(%r14)
	jmp	.L3488
	.p2align 4,,10
	.p2align 3
.L3519:
	movq	-232(%rbp), %rax
	movdqa	-256(%rbp), %xmm1
	movq	$0, -336(%rbp)
	movdqa	-224(%rbp), %xmm2
	movdqa	-208(%rbp), %xmm3
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm6
	movq	%rax, -328(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, -376(%rbp)
	movaps	%xmm1, -352(%rbp)
	movaps	%xmm2, -320(%rbp)
	movaps	%xmm3, -304(%rbp)
	movaps	%xmm7, -288(%rbp)
	movaps	%xmm6, -272(%rbp)
	jmp	.L3428
	.p2align 4,,10
	.p2align 3
.L3447:
	movq	32(%r15), %rdx
	leaq	-24(%rdx), %rax
	addq	$16, %rdx
	movq	%rax, %xmm0
	jmp	.L3448
	.p2align 4,,10
	.p2align 3
.L3517:
	leaq	.LC54(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3421
	.p2align 4,,10
	.p2align 3
.L3518:
	leaq	63(%rax), %rdx
	movq	16(%rdi), %r12
	movq	%rdx, %rax
	shrq	$6, %rax
	leaq	0(,%rax,8), %rdx
	movq	24(%rdi), %rax
	movq	%rdx, %rsi
	subq	%r12, %rax
	cmpq	%rax, %rdx
	ja	.L3528
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L3424:
	testq	%r12, %r12
	je	.L3514
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	memset@PLT
.L3514:
	movq	(%rbx), %rdi
	jmp	.L3426
	.p2align 4,,10
	.p2align 3
.L3460:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L3462
	cmpq	$32, 8(%rax)
	ja	.L3463
.L3462:
	movq	$32, 8(%rdx)
	movq	-344(%rbp), %rax
	movq	%rax, (%rdx)
	movq	%rdx, -344(%rbp)
.L3463:
	movq	-264(%rbp), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, -264(%rbp)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -280(%rbp)
	addq	$496, %rax
	movq	%rdx, -272(%rbp)
	movq	%rax, -288(%rbp)
	jmp	.L3461
	.p2align 4,,10
	.p2align 3
.L3436:
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-360(%rbp), %rdx
	movl	20(%rax), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	200(%rbx), %rax
	cmpl	$2, 12(%rax)
	je	.L3440
	movl	$3, 12(%rdx)
	jmp	.L3489
	.p2align 4,,10
	.p2align 3
.L3475:
	movq	%rdx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	-160(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L3476
	.p2align 4,,10
	.p2align 3
.L3472:
	movq	-376(%rbp), %rdx
	leaq	64(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L3473
.L3526:
	movq	8(%rax), %r9
	movl	20(%rdx), %esi
	xorl	%eax, %eax
	leaq	.LC55(%rip), %rdi
	movq	%r9, %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-160(%rbp), %rdx
	movq	(%rdx), %rax
	jmp	.L3474
.L3442:
	movq	-376(%rbp), %rdx
	leaq	64(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L3443
.L3445:
	movq	%rdx, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	-160(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L3446
.L3527:
	movq	8(%rax), %r8
	movl	20(%rdx), %esi
	xorl	%eax, %eax
	leaq	.LC55(%rip), %rdi
	movq	%r8, %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-160(%rbp), %rdx
	movq	(%rdx), %rax
	jmp	.L3444
.L3528:
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-360(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L3424
.L3525:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10966:
	.size	_ZN2v88internal8compiler9Scheduler11PrepareUsesEv, .-_ZN2v88internal8compiler9Scheduler11PrepareUsesEv
	.section	.text._ZN2v88internal8compiler9Scheduler15ComputeScheduleEPNS0_4ZoneEPNS1_5GraphENS_4base5FlagsINS2_4FlagEiEEPNS0_11TickCounterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9Scheduler15ComputeScheduleEPNS0_4ZoneEPNS1_5GraphENS_4base5FlagsINS2_4FlagEiEEPNS0_11TickCounterE
	.type	_ZN2v88internal8compiler9Scheduler15ComputeScheduleEPNS0_4ZoneEPNS1_5GraphENS_4base5FlagsINS2_4FlagEiEEPNS0_11TickCounterE, @function
_ZN2v88internal8compiler9Scheduler15ComputeScheduleEPNS0_4ZoneEPNS1_5GraphENS_4base5FlagsINS2_4FlagEiEEPNS0_11TickCounterE:
.LFB10874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -328(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$4, %edx
	jne	.L3530
	movq	(%rsi), %r12
.L3530:
	movss	.LC57(%rip), %xmm1
	testb	$2, %r8b
	jne	.L3531
	movss	.LC58(%rip), %xmm1
.L3531:
	movl	28(%r15), %eax
	pxor	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	mulss	%xmm1, %xmm0
	comiss	.LC59(%rip), %xmm0
	jnb	.L3534
	cvttss2siq	%xmm0, %rbx
.L3535:
	movq	16(%r12), %r13
	movq	24(%r12), %rax
	subq	%r13, %rax
	cmpq	$119, %rax
	jbe	.L3557
	leaq	120(%r13), %rax
	movq	%rax, 16(%r12)
.L3537:
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movl	%r8d, -332(%rbp)
	call	_ZN2v88internal8compiler8ScheduleC1EPNS0_4ZoneEm@PLT
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	%rbx, %r9
	pushq	-328(%rbp)
	movl	-332(%rbp), %r8d
	movq	%r13, %rcx
	movq	%r14, %rsi
	leaq	-320(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9SchedulerC1EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9Scheduler8BuildCFGEv
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9Scheduler26ComputeSpecialRPONumberingEv
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	popq	%rax
	popq	%rdx
	jne	.L3558
.L3538:
	movq	-304(%rbp), %rax
	movq	%r12, %rdi
	movq	104(%rax), %rax
	movl	$0, 12(%rax)
	movq	-304(%rbp), %rax
	movq	104(%rax), %rax
	movq	24(%rax), %rsi
	call	_ZN2v88internal8compiler9Scheduler28PropagateImmediateDominatorsEPNS1_10BasicBlockE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9Scheduler11PrepareUsesEv
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9Scheduler13ScheduleEarlyEv
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9Scheduler12ScheduleLateEv
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9Scheduler17SealFinalScheduleEv
	movq	-208(%rbp), %rax
	testq	%rax, %rax
	je	.L3529
	movq	-136(%rbp), %rbx
	movq	-168(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L3540
	movq	-216(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L3543:
	testq	%rax, %rax
	je	.L3541
	cmpq	$64, 8(%rax)
	ja	.L3542
.L3541:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-216(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -216(%rbp)
.L3542:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L3543
	movq	-208(%rbp), %rax
.L3540:
	movq	-200(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L3529
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L3529:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3559
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3534:
	.cfi_restore_state
	subss	.LC59(%rip), %xmm0
	cvttss2siq	%xmm0, %rbx
	btcq	$63, %rbx
	jmp	.L3535
	.p2align 4,,10
	.p2align 3
.L3558:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3538
	.p2align 4,,10
	.p2align 3
.L3557:
	movl	$120, %esi
	movq	%r12, %rdi
	movl	%r8d, -332(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-332(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L3537
.L3559:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10874:
	.size	_ZN2v88internal8compiler9Scheduler15ComputeScheduleEPNS0_4ZoneEPNS1_5GraphENS_4base5FlagsINS2_4FlagEiEEPNS0_11TickCounterE, .-_ZN2v88internal8compiler9Scheduler15ComputeScheduleEPNS0_4ZoneEPNS1_5GraphENS_4base5FlagsINS2_4FlagEiEEPNS0_11TickCounterE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler9SchedulerC2EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler9SchedulerC2EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler9SchedulerC2EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE:
.LFB14237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE14237:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler9SchedulerC2EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE, .-_GLOBAL__sub_I__ZN2v88internal8compiler9SchedulerC2EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler9SchedulerC2EPNS0_4ZoneEPNS1_5GraphEPNS1_8ScheduleENS_4base5FlagsINS2_4FlagEiEEmPNS0_11TickCounterE
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC39:
	.long	2576980378
	.long	1072798105
	.align 8
.LC40:
	.long	0
	.long	1138753536
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC56:
	.quad	-24
	.quad	8
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC57:
	.long	1066192077
	.align 4
.LC58:
	.long	1065353216
	.align 4
.LC59:
	.long	1593835520
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
