	.file	"loop-analysis.cc"
	.text
	.section	.rodata._ZN2v88internal8compiler14LoopFinderImpl9PrintLoopEPNS1_8LoopTree4LoopE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"  "
.LC1:
	.string	"Loop depth = %d "
.LC2:
	.string	" H#%d"
.LC3:
	.string	" B#%d"
.LC4:
	.string	" E#%d"
.LC5:
	.string	"\n"
	.section	.text._ZN2v88internal8compiler14LoopFinderImpl9PrintLoopEPNS1_8LoopTree4LoopE,"axG",@progbits,_ZN2v88internal8compiler14LoopFinderImpl9PrintLoopEPNS1_8LoopTree4LoopE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler14LoopFinderImpl9PrintLoopEPNS1_8LoopTree4LoopE
	.type	_ZN2v88internal8compiler14LoopFinderImpl9PrintLoopEPNS1_8LoopTree4LoopE, @function
_ZN2v88internal8compiler14LoopFinderImpl9PrintLoopEPNS1_8LoopTree4LoopE:
.LFB10278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rsi), %esi
	testl	%esi, %esi
	jle	.L2
	xorl	%ebx, %ebx
	leaq	.LC0(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%r14, %rdi
	xorl	%eax, %eax
	addl	$1, %ebx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	8(%r12), %esi
	cmpl	%ebx, %esi
	jg	.L3
.L2:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	leaq	.LC2(%rip), %r14
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movslq	48(%r12), %r15
	movq	%r15, %rbx
	salq	$3, %r15
	cmpl	52(%r12), %ebx
	jge	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	movq	216(%r13), %rax
	movq	%r14, %rdi
	addl	$1, %ebx
	movq	112(%rax), %rax
	movq	(%rax,%r15), %rax
	addq	$8, %r15
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpl	%ebx, 52(%r12)
	jg	.L8
.L7:
	movslq	%ebx, %r15
	leaq	.LC3(%rip), %r14
	salq	$3, %r15
	cmpl	%ebx, 56(%r12)
	jle	.L5
	.p2align 4,,10
	.p2align 3
.L11:
	movq	216(%r13), %rax
	movq	%r14, %rdi
	addl	$1, %ebx
	movq	112(%rax), %rax
	movq	(%rax,%r15), %rax
	addq	$8, %r15
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpl	%ebx, 56(%r12)
	jg	.L11
.L5:
	movslq	%ebx, %r15
	leaq	.LC4(%rip), %r14
	salq	$3, %r15
	cmpl	%ebx, 60(%r12)
	jle	.L9
	.p2align 4,,10
	.p2align 3
.L12:
	movq	216(%r13), %rax
	movq	%r14, %rdi
	addl	$1, %ebx
	movq	112(%rax), %rax
	movq	(%rax,%r15), %rax
	addq	$8, %r15
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpl	%ebx, 60(%r12)
	jg	.L12
.L9:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	24(%r12), %rbx
	movq	32(%r12), %r12
	cmpq	%rbx, %r12
	je	.L1
.L14:
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler14LoopFinderImpl9PrintLoopEPNS1_8LoopTree4LoopE
	cmpq	%rbx, %r12
	jne	.L14
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10278:
	.size	_ZN2v88internal8compiler14LoopFinderImpl9PrintLoopEPNS1_8LoopTree4LoopE, .-_ZN2v88internal8compiler14LoopFinderImpl9PrintLoopEPNS1_8LoopTree4LoopE
	.section	.text._ZN2v88internal8compiler8LoopTree10HeaderNodeEPNS2_4LoopE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8LoopTree10HeaderNodeEPNS2_4LoopE
	.type	_ZN2v88internal8compiler8LoopTree10HeaderNodeEPNS2_4LoopE, @function
_ZN2v88internal8compiler8LoopTree10HeaderNodeEPNS2_4LoopE:
.LFB10283:
	.cfi_startproc
	endbr64
	movq	112(%rdi), %rax
	movslq	48(%rsi), %rdx
	movq	(%rax,%rdx,8), %rdi
	movq	(%rdi), %rax
	cmpw	$1, 16(%rax)
	je	.L23
	xorl	%esi, %esi
	jmp	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE10283:
	.size	_ZN2v88internal8compiler8LoopTree10HeaderNodeEPNS2_4LoopE, .-_ZN2v88internal8compiler8LoopTree10HeaderNodeEPNS2_4LoopE
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE7reserveEm.str1.1,"aMS",@progbits,1
.LC6:
	.string	"vector::reserve"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE7reserveEm,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE7reserveEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE7reserveEm
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE7reserveEm, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE7reserveEm:
.LFB11142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$268435455, %rsi
	ja	.L50
	movq	8(%rdi), %r12
	movq	24(%rdi), %rax
	movq	%rdi, %rbx
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L51
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	16(%rdi), %r13
	leaq	0(,%rsi,8), %r15
	xorl	%eax, %eax
	movq	%r13, %r14
	subq	%r12, %r14
	testq	%rsi, %rsi
	je	.L28
	movq	(%rdi), %rdi
	movq	%r15, %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %r15
	ja	.L52
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L28:
	cmpq	%r12, %r13
	je	.L35
	leaq	-8(%r13), %rdx
	leaq	15(%r12), %rcx
	subq	%r12, %rdx
	subq	%rax, %rcx
	shrq	$3, %rdx
	cmpq	$30, %rcx
	jbe	.L39
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdx
	je	.L39
	leaq	1(%rdx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L33:
	movdqu	(%r12,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L33
	movq	%rcx, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r12
	addq	%rax, %rsi
	cmpq	%rdx, %rcx
	je	.L35
	movq	(%r12), %rdx
	movq	%rdx, (%rsi)
.L35:
	movq	%rax, 8(%rbx)
	addq	%rax, %r14
	addq	%r15, %rax
	movq	%r14, 16(%rbx)
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L32:
	movq	(%r12,%rcx,8), %rsi
	movq	%rsi, (%rax,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rsi, %rdx
	jne	.L32
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L52:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L28
.L50:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11142:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE7reserveEm, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE7reserveEm
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_.str1.1,"aMS",@progbits,1
.LC7:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_:
.LFB11531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L91
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L69
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L92
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L55:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L93
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L58:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L92:
	testq	%rdx, %rdx
	jne	.L94
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L56:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L59
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L72
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L72
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L61:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L61
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L63
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L63:
	leaq	16(%rax,%r8), %rcx
.L59:
	cmpq	%r14, %r12
	je	.L64
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L73
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L73
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L66:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L66
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L68
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L68:
	leaq	8(%rcx,%r9), %rcx
.L64:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L65:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L65
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L72:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L60:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L60
	jmp	.L63
.L93:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L58
.L91:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L94:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L55
	.cfi_endproc
.LFE11531:
	.size	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	.section	.text._ZN2v88internal8compiler14LoopFinderImpl15ConnectLoopTreeEi,"axG",@progbits,_ZN2v88internal8compiler14LoopFinderImpl15ConnectLoopTreeEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler14LoopFinderImpl15ConnectLoopTreeEi
	.type	_ZN2v88internal8compiler14LoopFinderImpl15ConnectLoopTreeEi, @function
_ZN2v88internal8compiler14LoopFinderImpl15ConnectLoopTreeEi:
.LFB10277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leal	-1(%rsi), %ebx
	movslq	%ebx, %rbx
	leaq	(%rbx,%rbx,4), %rdx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	160(%rdi), %rax
	leaq	(%rax,%rdx,8), %r15
	movq	32(%r15), %r13
	testq	%r13, %r13
	je	.L117
.L95:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	movq	(%r15), %rax
	movq	%rdi, %r12
	movl	%esi, %r14d
	movl	20(%rax), %edx
	andl	$16777215, %edx
	salq	$4, %rdx
	addq	128(%rdi), %rdx
	cmpq	$0, (%rdx)
	je	.L119
.L97:
	movl	224(%r12), %esi
	salq	$6, %rbx
	testl	%esi, %esi
	jle	.L98
	movl	$1, %ecx
.L102:
	cmpl	%ecx, %r14d
	je	.L99
	movq	(%rdx), %rax
	movl	%ecx, %edi
	movq	240(%r12), %r8
	sarl	$5, %edi
	movl	20(%rax), %eax
	andl	$16777215, %eax
	imull	228(%r12), %eax
	addl	%edi, %eax
	movq	232(%r12), %rdi
	cltq
	movl	(%rdi,%rax,4), %edi
	andl	(%r8,%rax,4), %edi
	btl	%ecx, %edi
	jc	.L120
.L99:
	addl	$1, %ecx
	cmpl	%esi, %ecx
	jle	.L102
	movq	216(%r12), %rax
	addq	48(%rax), %rbx
	movq	%rbx, 32(%r15)
	movq	216(%r12), %rax
	movq	%rbx, -64(%rbp)
	testq	%r13, %r13
	je	.L103
	movq	32(%r13), %rsi
	cmpq	40(%r13), %rsi
	je	.L104
	movq	%rbx, (%rsi)
	addq	$8, 32(%r13)
.L105:
	movq	-64(%rbp), %rax
	movq	%r13, (%rax)
	movl	8(%r13), %eax
	movq	-64(%rbp), %rdx
	addl	$1, %eax
	movl	%eax, 8(%rdx)
.L106:
	movq	32(%r15), %r13
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L120:
	movl	%ecx, %esi
	movq	%r12, %rdi
	movq	%rdx, -80(%rbp)
	movl	%ecx, -68(%rbp)
	call	_ZN2v88internal8compiler14LoopFinderImpl15ConnectLoopTreeEi
	testq	%r13, %r13
	movl	-68(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	224(%r12), %esi
	je	.L101
	movl	8(%r13), %edi
	cmpl	%edi, 8(%rax)
	jle	.L99
.L101:
	movq	%rax, %r13
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L119:
	movq	%rax, (%rdx)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	movq	216(%r12), %rax
	addq	48(%rax), %rbx
	movq	%rbx, -64(%rbp)
	movq	%rbx, 32(%r15)
	movq	216(%r12), %rax
.L103:
	movq	24(%rax), %rsi
	cmpq	32(%rax), %rsi
	je	.L107
	movq	%rbx, (%rsi)
	addq	$8, 24(%rax)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L104:
	leaq	-64(%rbp), %rdx
	leaq	16(%r13), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L107:
	leaq	-64(%rbp), %rdx
	leaq	8(%rax), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	jmp	.L106
.L118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10277:
	.size	_ZN2v88internal8compiler14LoopFinderImpl15ConnectLoopTreeEi, .-_ZN2v88internal8compiler14LoopFinderImpl15ConnectLoopTreeEi
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB11604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L159
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L137
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L160
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L123:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L161
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L126:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L160:
	testq	%rdx, %rdx
	jne	.L162
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L124:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L127
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L140
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L140
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L129:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L129
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L131
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L131:
	leaq	16(%rax,%r8), %rcx
.L127:
	cmpq	%r14, %r12
	je	.L132
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L141
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L141
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L134:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L134
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L136
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L136:
	leaq	8(%rcx,%r9), %rcx
.L132:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L141:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L133:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L133
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L140:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L128:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L128
	jmp	.L131
.L161:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L126
.L159:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L162:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L123
	.cfi_endproc
.LFE11604:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal8compiler14LoopFinderImpl13SerializeLoopEPNS1_8LoopTree4LoopE,"axG",@progbits,_ZN2v88internal8compiler14LoopFinderImpl13SerializeLoopEPNS1_8LoopTree4LoopE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler14LoopFinderImpl13SerializeLoopEPNS1_8LoopTree4LoopE
	.type	_ZN2v88internal8compiler14LoopFinderImpl13SerializeLoopEPNS1_8LoopTree4LoopE, @function
_ZN2v88internal8compiler14LoopFinderImpl13SerializeLoopEPNS1_8LoopTree4LoopE:
.LFB10276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	216(%rdi), %rdx
	subq	48(%rdx), %rax
	sarq	$6, %rax
	leal	1(%rax), %r15d
	cltq
	leaq	(%rax,%rax,4), %rcx
	movq	160(%rdi), %rax
	leaq	(%rax,%rcx,8), %rcx
	movq	120(%rdx), %rax
	subq	112(%rdx), %rax
	sarq	$3, %rax
	movq	%rcx, -56(%rbp)
	movl	%eax, 48(%rsi)
	movq	8(%rcx), %r12
	testq	%r12, %r12
	jne	.L167
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L213:
	movq	(%r12), %rax
	movq	%rax, (%rsi)
	addq	$8, 120(%rdi)
.L208:
	movq	(%r12), %rax
	movq	216(%rbx), %rdx
	movl	20(%rax), %eax
	movq	80(%rdx), %rdx
	andl	$16777215, %eax
	movl	%r15d, (%rdx,%rax,4)
	movq	8(%r12), %r12
	testq	%r12, %r12
	je	.L164
.L167:
	movq	216(%rbx), %rdi
	movq	120(%rdi), %rsi
	cmpq	128(%rdi), %rsi
	jne	.L213
	addq	$104, %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L164:
	movq	216(%rbx), %rdx
	movq	120(%rdx), %rax
	subq	112(%rdx), %rax
	sarq	$3, %rax
	movl	%eax, 52(%r13)
	movq	-56(%rbp), %rax
	movq	24(%rax), %r12
	testq	%r12, %r12
	jne	.L171
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L214:
	movq	(%r12), %rax
	movq	%rax, (%rsi)
	addq	$8, 120(%rdi)
.L210:
	movq	(%r12), %rax
	movq	216(%rbx), %rdx
	movl	20(%rax), %eax
	movq	80(%rdx), %rdx
	andl	$16777215, %eax
	movl	%r15d, (%rdx,%rax,4)
	movq	8(%r12), %r12
	testq	%r12, %r12
	je	.L168
.L171:
	movq	216(%rbx), %rdi
	movq	120(%rdi), %rsi
	cmpq	128(%rdi), %rsi
	jne	.L214
	addq	$104, %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L168:
	movq	24(%r13), %rax
	movq	32(%r13), %r12
	cmpq	%rax, %r12
	je	.L172
	movq	%rax, %r14
.L173:
	movq	(%r14), %rsi
	movq	%rbx, %rdi
	addq	$8, %r14
	call	_ZN2v88internal8compiler14LoopFinderImpl13SerializeLoopEPNS1_8LoopTree4LoopE
	cmpq	%r14, %r12
	jne	.L173
.L172:
	movq	216(%rbx), %rdx
	movq	120(%rdx), %rax
	subq	112(%rdx), %rax
	sarq	$3, %rax
	movl	%eax, 56(%r13)
	movq	-56(%rbp), %rax
	movq	16(%rax), %r12
	testq	%r12, %r12
	jne	.L177
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L215:
	movq	(%r12), %rax
	movq	%rax, (%rsi)
	addq	$8, 120(%rdi)
.L212:
	movq	(%r12), %rax
	movq	216(%rbx), %rdx
	movl	20(%rax), %eax
	movq	80(%rdx), %rdx
	andl	$16777215, %eax
	movl	%r15d, (%rdx,%rax,4)
	movq	8(%r12), %r12
	testq	%r12, %r12
	je	.L174
.L177:
	movq	216(%rbx), %rdi
	movq	120(%rdi), %rsi
	cmpq	128(%rdi), %rsi
	jne	.L215
	addq	$104, %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L174:
	movq	216(%rbx), %rdx
	movq	120(%rdx), %rax
	subq	112(%rdx), %rax
	sarq	$3, %rax
	movl	%eax, 60(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10276:
	.size	_ZN2v88internal8compiler14LoopFinderImpl13SerializeLoopEPNS1_8LoopTree4LoopE, .-_ZN2v88internal8compiler14LoopFinderImpl13SerializeLoopEPNS1_8LoopTree4LoopE
	.section	.text._ZNSt6vectorIN2v88internal8compiler12TempLoopInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler12TempLoopInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler12TempLoopInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal8compiler12TempLoopInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal8compiler12TempLoopInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_:
.LFB11902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$-3689348814741910323, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	8(%rdi), %r14
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	$53687091, %rax
	je	.L232
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L226
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L233
	movl	$2147483640, %esi
	movl	$2147483640, %ecx
.L218:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L234
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L221:
	leaq	(%rax,%rcx), %rdi
	leaq	40(%rax), %r8
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L233:
	testq	%rcx, %rcx
	jne	.L235
	movl	$40, %r8d
	xorl	%edi, %edi
	xorl	%eax, %eax
.L219:
	movdqu	(%r15), %xmm5
	movups	%xmm5, (%rax,%rdx)
	movdqu	16(%r15), %xmm6
	movups	%xmm6, 16(%rax,%rdx)
	movq	32(%r15), %rcx
	movq	%rcx, 32(%rax,%rdx)
	cmpq	%r14, %rbx
	je	.L222
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L223:
	movdqu	(%rdx), %xmm1
	addq	$40, %rdx
	addq	$40, %rcx
	movups	%xmm1, -40(%rcx)
	movdqu	-24(%rdx), %xmm2
	movups	%xmm2, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L223
	leaq	-40(%rbx), %rdx
	subq	%r14, %rdx
	shrq	$3, %rdx
	leaq	80(%rax,%rdx,8), %r8
.L222:
	cmpq	%r12, %rbx
	je	.L224
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L225:
	movdqu	(%rdx), %xmm3
	addq	$40, %rdx
	addq	$40, %rcx
	movups	%xmm3, -40(%rcx)
	movdqu	-24(%rdx), %xmm4
	movups	%xmm4, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L225
	subq	%rbx, %r12
	leaq	-40(%r12), %rdx
	shrq	$3, %rdx
	leaq	40(%r8,%rdx,8), %r8
.L224:
	movq	%rax, %xmm0
	movq	%r8, %xmm7
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	movl	$40, %esi
	movl	$40, %ecx
	jmp	.L218
.L234:
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	jmp	.L221
.L235:
	cmpq	$53687091, %rcx
	movl	$53687091, %eax
	cmova	%rax, %rcx
	imulq	$40, %rcx, %rcx
	movq	%rcx, %rsi
	jmp	.L218
.L232:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11902:
	.size	_ZNSt6vectorIN2v88internal8compiler12TempLoopInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal8compiler12TempLoopInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.section	.rodata._ZN2v88internal8compiler14LoopFinderImpl17PropagateBackwardEv.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZN2v88internal8compiler14LoopFinderImpl17PropagateBackwardEv,"axG",@progbits,_ZN2v88internal8compiler14LoopFinderImpl17PropagateBackwardEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler14LoopFinderImpl17PropagateBackwardEv
	.type	_ZN2v88internal8compiler14LoopFinderImpl17PropagateBackwardEv, @function
_ZN2v88internal8compiler14LoopFinderImpl17PropagateBackwardEv:
.LFB10260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	228(%rdi), %eax
	leal	1(%rax), %ebx
	movq	216(%rdi), %rax
	movq	(%rdi), %rdi
	movl	%ebx, %edx
	movq	88(%rax), %r13
	subq	80(%rax), %r13
	sarq	$2, %r13
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	imull	%r13d, %edx
	movl	%r13d, %r12d
	subq	%r8, %rax
	movslq	%edx, %rdx
	salq	$2, %rdx
	leaq	7(%rdx), %rsi
	andq	$-8, %rsi
	cmpq	%rax, %rsi
	ja	.L570
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L238:
	movq	%r8, %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movl	228(%r15), %ecx
	movq	%rax, %r8
	testl	%ecx, %ecx
	jle	.L242
	testl	%r13d, %r13d
	jle	.L242
	movslq	%ebx, %r13
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movq	%r13, %r11
	negq	%r11
	salq	$2, %r11
	.p2align 4,,10
	.p2align 3
.L245:
	movl	%edi, %eax
	movq	232(%r15), %rsi
	imull	%ecx, %eax
	movslq	%eax, %rdx
	testl	%ecx, %ecx
	jle	.L243
	movq	%r10, %rcx
	leaq	(%r8,%r9,4), %rax
	subq	%r8, %rcx
	leaq	(%rcx,%rdx,4), %rdx
	addq	%rdx, %rsi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L244:
	movl	(%rsi,%rax), %ecx
	addl	$1, %edx
	addq	$4, %rax
	movl	%ecx, -4(%rax)
	movl	228(%r15), %ecx
	cmpl	%edx, %ecx
	jg	.L244
.L243:
	addl	$1, %edi
	addq	%r13, %r9
	addq	%r11, %r10
	cmpl	%edi, %r12d
	jne	.L245
.L242:
	movq	8(%r15), %rax
	movl	%ebx, 228(%r15)
	movq	%r8, 232(%r15)
	movl	20(%rax), %eax
	andl	$16777215, %eax
	imull	%eax, %ebx
	orl	$1, (%r8,%rbx,4)
	movq	8(%r15), %rbx
	movl	112(%r15), %eax
	cmpl	%eax, 16(%rbx)
	ja	.L418
	movq	96(%r15), %rax
	movq	80(%r15), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L246
	movq	%rbx, (%rdx)
	addq	$8, 80(%r15)
.L247:
	movl	112(%r15), %eax
	addl	$1, %eax
	movl	%eax, 16(%rbx)
	.p2align 4,,10
	.p2align 3
.L418:
	movq	48(%r15), %rax
	cmpq	%rax, 80(%r15)
	je	.L236
	movq	248(%r15), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	48(%r15), %rdx
	movq	(%rdx), %r12
	movl	20(%r12), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	128(%r15), %rax
	cmpq	$0, (%rax)
	je	.L571
	movq	64(%r15), %rax
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L264
.L582:
	addq	$8, %rdx
	movq	%rdx, 48(%r15)
.L265:
	movl	112(%r15), %eax
	movl	%eax, 16(%r12)
	movq	(%r12), %rax
	movzwl	16(%rax), %eax
	cmpl	$1, %eax
	je	.L572
	leal	-35(%rax), %edx
	cmpl	$1, %edx
	jbe	.L573
	cmpl	$52, %eax
	je	.L574
	subl	$53, %eax
	cmpl	$1, %eax
	jbe	.L379
.L552:
	movl	$-1, -104(%rbp)
	movl	20(%r12), %edx
	movl	$2147483647, %r8d
	movl	$-2147483648, %r11d
	movl	$-1, %r14d
.L291:
	leaq	32(%r12), %rax
	xorl	%ebx, %ebx
	movq	%rax, -112(%rbp)
	movl	%edx, %eax
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L416
	.p2align 4,,10
	.p2align 3
.L577:
	cmpl	%ebx, %eax
	jle	.L418
	movq	-112(%rbp), %rax
	leaq	(%rax,%rbx,8), %rax
.L420:
	movq	(%rax), %r13
	movq	216(%r15), %rax
	movl	%edx, %ecx
	andl	$16777215, %ecx
	movq	80(%rax), %rax
	movl	(%rax,%rcx,4), %eax
	testl	%eax, %eax
	jle	.L421
	movq	(%r12), %rax
	movzwl	16(%rax), %eax
	leal	-35(%rax), %ecx
	cmpl	$1, %ecx
	jbe	.L575
	cmpl	$1, %eax
	je	.L576
.L421:
	cmpq	%r13, %r12
	je	.L427
	movl	20(%r13), %eax
	movl	228(%r15), %edi
	movl	%edx, %ecx
	andl	$16777215, %ecx
	movq	232(%r15), %rsi
	andl	$16777215, %eax
	imull	%edi, %ecx
	imull	%edi, %eax
	testl	%edi, %edi
	jle	.L427
	leaq	(%rsi,%rax,4), %rdi
	leaq	(%rsi,%rcx,4), %r10
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	movl	$-1, %r9d
	.p2align 4,,10
	.p2align 3
.L446:
	movl	(%rdi,%rax,4), %esi
	cmpl	%eax, %r14d
	movl	%r9d, %edx
	cmove	%r8d, %edx
	andl	(%r10,%rax,4), %edx
	orl	%esi, %edx
	cmpl	%esi, %edx
	movl	%ecx, %esi
	movl	%edx, (%rdi,%rax,4)
	setne	%dl
	xorl	$1, %esi
	andb	%sil, %dl
	cmovne	%edx, %ecx
	addq	$1, %rax
	cmpl	%eax, 228(%r15)
	jg	.L446
	testb	%cl, %cl
	je	.L561
	movl	112(%r15), %eax
	cmpl	%eax, 16(%r13)
	ja	.L561
	movq	96(%r15), %rdi
	movq	80(%r15), %rax
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L449
.L562:
	movq	%r13, (%rax)
	addq	$8, 80(%r15)
.L450:
	movl	112(%r15), %eax
	addl	$1, %eax
	movl	%eax, 16(%r13)
.L561:
	movl	20(%r12), %edx
.L427:
	movl	%edx, %eax
	addq	$1, %rbx
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L577
.L416:
	movq	32(%r12), %rax
	cmpl	%ebx, 8(%rax)
	jle	.L418
	leaq	16(%rax,%rbx,8), %rax
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L575:
	movq	%r12, %rdi
	movl	%r8d, -128(%rbp)
	movl	%r11d, -120(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE@PLT
	movl	-120(%rbp), %r11d
	movl	-128(%rbp), %r8d
	cmpl	%ebx, %eax
	je	.L423
	testl	%ebx, %ebx
	jne	.L426
.L423:
	movl	20(%r12), %edx
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L576:
	testq	%rbx, %rbx
	je	.L421
.L426:
	movl	20(%r13), %eax
	movl	-104(%rbp), %edi
	andl	$16777215, %eax
	imull	228(%r15), %eax
	leal	(%rax,%rdi), %edx
	movq	232(%r15), %rax
	leaq	(%rax,%rdx,4), %rcx
	movl	(%rcx), %edx
	movl	%edx, %eax
	orl	%r11d, %eax
	movl	%eax, (%rcx)
	cmpl	%eax, %edx
	je	.L561
	movl	112(%r15), %eax
	cmpl	%eax, 16(%r13)
	ja	.L561
	movq	96(%r15), %rdi
	movq	80(%r15), %rax
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rax
	jne	.L562
	movq	104(%r15), %rdx
	movq	72(%r15), %r9
	subq	88(%r15), %rax
	movq	%rdx, %r10
	sarq	$3, %rax
	subq	%r9, %r10
	movq	%rax, %rcx
	movq	%r10, %rsi
	sarq	$3, %rsi
	leaq	-1(%rsi), %rax
	salq	$6, %rax
	addq	%rcx, %rax
	movq	64(%r15), %rcx
	subq	48(%r15), %rcx
	sarq	$3, %rcx
	addq	%rcx, %rax
	cmpq	$268435455, %rax
	je	.L431
	movq	32(%r15), %rdi
	movq	40(%r15), %rcx
	movq	%rdx, %rax
	subq	%rdi, %rax
	movq	%rdi, -120(%rbp)
	movq	%rcx, %rdi
	sarq	$3, %rax
	subq	%rax, %rdi
	cmpq	$1, %rdi
	jbe	.L578
.L451:
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L459
	cmpq	$63, 8(%rax)
	ja	.L579
.L459:
	movq	16(%r15), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	$511, %rcx
	jbe	.L580
	leaq	512(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L460:
	movq	%rax, 8(%rdx)
	movq	80(%r15), %rax
	movq	%r13, (%rax)
	movq	104(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 104(%r15)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 88(%r15)
	movq	%rdx, 96(%r15)
	movq	%rax, 80(%r15)
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L573:
	movl	20(%r12), %edx
	leaq	32(%r12), %rcx
	movl	%edx, %eax
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L306
	subl	$1, %eax
	cltq
	leaq	(%rcx,%rax,8), %rax
	movq	(%rax), %rbx
	movq	(%rbx), %rax
	cmpw	$1, 16(%rax)
	je	.L581
.L472:
	movl	$-1, -104(%rbp)
	movl	$2147483647, %r8d
	movl	$-2147483648, %r11d
	movl	$-1, %r14d
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L571:
	movq	%r12, (%rax)
	movq	64(%r15), %rax
	movq	48(%r15), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	jne	.L582
.L264:
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L266
	cmpq	$64, 8(%rax)
	ja	.L267
.L266:
	movq	56(%r15), %rax
	movq	$64, 8(%rax)
	movq	24(%r15), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 24(%r15)
.L267:
	movq	72(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%r15)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 56(%r15)
	movq	%rdx, 64(%r15)
	movq	%rax, 48(%r15)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L572:
	movq	216(%r15), %rsi
	movl	20(%r12), %edx
	movq	80(%rsi), %rcx
	movl	%edx, %edi
	andl	$16777215, %edi
	movl	(%rcx,%rdi,4), %ecx
	testl	%ecx, %ecx
	jg	.L269
	movl	224(%r15), %eax
	leal	1(%rax), %ebx
	movl	228(%r15), %eax
	movl	%ebx, %r14d
	movl	%ebx, 224(%r15)
	sarl	$5, %r14d
	cmpl	%eax, %r14d
	jge	.L583
.L270:
	movq	%r12, -96(%rbp)
	movq	168(%r15), %rsi
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	cmpq	176(%r15), %rsi
	je	.L278
	movdqa	-96(%rbp), %xmm7
	movups	%xmm7, (%rsi)
	movdqa	-80(%rbp), %xmm5
	movups	%xmm5, 16(%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 32(%rsi)
	addq	$40, 168(%r15)
.L279:
	movq	216(%r15), %rcx
	movq	(%rcx), %rdx
	movq	56(%rcx), %r13
	cmpq	64(%rcx), %r13
	je	.L280
	pcmpeqd	%xmm6, %xmm6
	movq	$0, 0(%r13)
	movl	$0, 8(%r13)
	movq	%rdx, 16(%r13)
	movq	$0, 24(%r13)
	movq	$0, 32(%r13)
	movq	$0, 40(%r13)
	movups	%xmm6, 48(%r13)
	addq	$64, 56(%rcx)
.L281:
	movl	20(%r12), %eax
	andl	$16777215, %eax
	movl	%eax, %edx
	salq	$4, %rdx
	addq	128(%r15), %rdx
	cmpq	$0, (%rdx)
	je	.L584
.L289:
	imull	228(%r15), %eax
	movl	$1, %esi
	movl	%ebx, %ecx
	movl	%r14d, -104(%rbp)
	sall	%cl, %esi
	movl	%esi, %r8d
	movl	%esi, %r11d
	leal	(%rax,%r14), %edx
	movq	232(%r15), %rax
	notl	%r8d
	orl	%esi, (%rax,%rdx,4)
	movq	216(%r15), %rdx
	movl	20(%r12), %eax
	movq	80(%rdx), %rdx
	andl	$16777215, %eax
	movl	%ebx, (%rdx,%rax,4)
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	jne	.L290
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L293:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L296
	movq	32(%r12), %rax
	movl	8(%rax), %eax
.L296:
	cmpl	$1, %eax
	jle	.L299
	movq	(%rcx), %rax
	cmpw	$52, 16(%rax)
	je	.L585
.L299:
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L550
.L290:
	movl	16(%rdx), %esi
	movl	%esi, %eax
	shrl	%eax
	andl	$1, %esi
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,8), %rcx
	movq	(%rcx), %rax
	jne	.L292
	movq	%rax, %rcx
	movq	(%rax), %rax
.L292:
	movzwl	16(%rax), %eax
	subl	$35, %eax
	cmpl	$1, %eax
	ja	.L293
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	movl	%eax, %esi
	salq	$4, %rsi
	addq	128(%r15), %rsi
	cmpq	$0, (%rsi)
	je	.L586
.L294:
	imull	228(%r15), %eax
	leal	(%rax,%r14), %esi
	movq	232(%r15), %rax
	orl	%r11d, (%rax,%rsi,4)
	movq	216(%r15), %rsi
	movl	20(%rcx), %eax
	movq	80(%rsi), %rsi
	andl	$16777215, %eax
	movl	%ebx, (%rsi,%rax,4)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L574:
	movl	20(%r12), %edx
	movl	%edx, %eax
	xorl	$251658240, %eax
	testl	$251658240, %eax
	je	.L344
	leaq	40(%r12), %rax
.L345:
	movq	(%rax), %rbx
	movq	216(%r15), %rcx
	movl	20(%rbx), %eax
	movq	80(%rcx), %rsi
	andl	$16777215, %eax
	movl	(%rsi,%rax,4), %esi
	testl	%esi, %esi
	jg	.L472
	movl	224(%r15), %eax
	leal	1(%rax), %r13d
	movl	228(%r15), %eax
	movl	%r13d, %r8d
	movl	%r13d, 224(%r15)
	sarl	$5, %r8d
	cmpl	%eax, %r8d
	jge	.L587
.L346:
	movq	%rbx, -96(%rbp)
	movq	168(%r15), %rsi
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	cmpq	176(%r15), %rsi
	je	.L354
	movdqa	-96(%rbp), %xmm3
	movups	%xmm3, (%rsi)
	movdqa	-80(%rbp), %xmm4
	movups	%xmm4, 16(%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 32(%rsi)
	addq	$40, 168(%r15)
.L355:
	movq	216(%r15), %rdx
	movq	(%rdx), %rcx
	movq	56(%rdx), %r14
	cmpq	64(%rdx), %r14
	je	.L356
	pcmpeqd	%xmm1, %xmm1
	movq	$0, (%r14)
	movl	$0, 8(%r14)
	movq	%rcx, 16(%r14)
	movq	$0, 24(%r14)
	movq	$0, 32(%r14)
	movq	$0, 40(%r14)
	movups	%xmm1, 48(%r14)
	addq	$64, 56(%rdx)
.L357:
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	movl	%eax, %edx
	salq	$4, %rdx
	addq	128(%r15), %rdx
	cmpq	$0, (%rdx)
	je	.L588
.L364:
	imull	228(%r15), %eax
	movl	$1, %esi
	movl	%r13d, %ecx
	sall	%cl, %esi
	leal	(%rax,%r8), %edx
	movq	232(%r15), %rax
	orl	%esi, (%rax,%rdx,4)
	movq	216(%r15), %rdx
	movl	20(%rbx), %eax
	movq	80(%rdx), %rdx
	andl	$16777215, %eax
	movl	%r13d, (%rdx,%rax,4)
	movq	24(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L365
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L367:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L370
	movq	32(%rbx), %rax
	movl	8(%rax), %eax
.L370:
	cmpl	$1, %eax
	jle	.L373
	movq	(%rcx), %rax
	cmpw	$52, 16(%rax)
	je	.L589
.L373:
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L552
.L365:
	movl	16(%rdx), %edi
	movl	%edi, %eax
	shrl	%eax
	andl	$1, %edi
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,8), %rcx
	movq	(%rcx), %rax
	jne	.L366
	movq	%rax, %rcx
	movq	(%rax), %rax
.L366:
	movzwl	16(%rax), %eax
	subl	$35, %eax
	cmpl	$1, %eax
	ja	.L367
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	movl	%eax, %edi
	salq	$4, %rdi
	addq	128(%r15), %rdi
	cmpq	$0, (%rdi)
	je	.L590
.L368:
	imull	228(%r15), %eax
	leal	(%rax,%r8), %edi
	movq	232(%r15), %rax
	orl	%esi, (%rax,%rdi,4)
	movq	216(%r15), %rdi
	movl	20(%rcx), %eax
	movq	80(%rdi), %rdi
	andl	$16777215, %eax
	movl	%r13d, (%rdi,%rax,4)
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L550:
	movl	20(%r12), %edx
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L379:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L380
	addq	$40, %rax
.L381:
	movq	(%rax), %rbx
	movq	216(%r15), %rdx
	movl	20(%rbx), %eax
	movq	80(%rdx), %rcx
	andl	$16777215, %eax
	movl	(%rcx,%rax,4), %ecx
	testl	%ecx, %ecx
	jg	.L552
	movl	224(%r15), %eax
	leal	1(%rax), %r13d
	movl	228(%r15), %eax
	movl	%r13d, %r8d
	movl	%r13d, 224(%r15)
	sarl	$5, %r8d
	cmpl	%eax, %r8d
	jge	.L591
.L383:
	movq	%rbx, -96(%rbp)
	movq	168(%r15), %rsi
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	cmpq	176(%r15), %rsi
	je	.L391
	movdqa	-96(%rbp), %xmm2
	movups	%xmm2, (%rsi)
	movdqa	-80(%rbp), %xmm7
	movups	%xmm7, 16(%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 32(%rsi)
	addq	$40, 168(%r15)
.L392:
	movq	216(%r15), %rdx
	movq	(%rdx), %rcx
	movq	56(%rdx), %r14
	cmpq	64(%rdx), %r14
	je	.L393
	pcmpeqd	%xmm3, %xmm3
	movq	$0, (%r14)
	movl	$0, 8(%r14)
	movq	%rcx, 16(%r14)
	movq	$0, 24(%r14)
	movq	$0, 32(%r14)
	movq	$0, 40(%r14)
	movups	%xmm3, 48(%r14)
	addq	$64, 56(%rdx)
.L394:
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	movl	%eax, %edx
	salq	$4, %rdx
	addq	128(%r15), %rdx
	cmpq	$0, (%rdx)
	je	.L592
.L401:
	imull	228(%r15), %eax
	movl	$1, %esi
	movl	%r13d, %ecx
	sall	%cl, %esi
	leal	(%rax,%r8), %edx
	movq	232(%r15), %rax
	orl	%esi, (%rax,%rdx,4)
	movq	216(%r15), %rdx
	movl	20(%rbx), %eax
	movq	80(%rdx), %rdx
	andl	$16777215, %eax
	movl	%r13d, (%rdx,%rax,4)
	movq	24(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L402
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L404:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L407
	movq	32(%rbx), %rax
	movl	8(%rax), %eax
.L407:
	cmpl	$1, %eax
	jle	.L410
	movq	(%rcx), %rax
	cmpw	$52, 16(%rax)
	je	.L593
.L410:
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L552
.L402:
	movl	16(%rdx), %edi
	movl	%edi, %eax
	shrl	%eax
	andl	$1, %edi
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,8), %rcx
	movq	(%rcx), %rax
	jne	.L403
	movq	%rax, %rcx
	movq	(%rax), %rax
.L403:
	movzwl	16(%rax), %eax
	subl	$35, %eax
	cmpl	$1, %eax
	ja	.L404
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	movl	%eax, %edi
	salq	$4, %rdi
	addq	128(%r15), %rdi
	cmpq	$0, (%rdi)
	je	.L594
.L405:
	imull	228(%r15), %eax
	leal	(%rax,%r8), %edi
	movq	232(%r15), %rax
	orl	%esi, (%rax,%rdi,4)
	movq	216(%r15), %rdi
	movl	20(%rcx), %eax
	movq	80(%rdi), %rdi
	andl	$16777215, %eax
	movl	%r13d, (%rdi,%rax,4)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L236:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L595
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L579:
	.cfi_restore_state
	movq	(%rax), %rcx
	movq	%rcx, 24(%r15)
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L306:
	movq	32(%r12), %rcx
	movl	8(%rcx), %eax
	subl	$1, %eax
	cltq
	leaq	16(%rcx,%rax,8), %rax
	movq	(%rax), %rbx
	movq	(%rbx), %rax
	cmpw	$1, 16(%rax)
	jne	.L472
.L581:
	movq	216(%r15), %rsi
	movl	20(%rbx), %eax
	movq	80(%rsi), %rcx
	andl	$16777215, %eax
	movl	(%rcx,%rax,4), %ecx
	testl	%ecx, %ecx
	jg	.L596
	movl	224(%r15), %eax
	leal	1(%rax), %ecx
	movl	228(%r15), %eax
	movl	%ecx, %r14d
	movl	%ecx, 224(%r15)
	sarl	$5, %r14d
	cmpl	%eax, %r14d
	jge	.L597
.L309:
	movq	%rbx, -96(%rbp)
	movq	168(%r15), %rsi
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	cmpq	176(%r15), %rsi
	je	.L317
	movdqa	-96(%rbp), %xmm6
	movups	%xmm6, (%rsi)
	movdqa	-80(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 32(%rsi)
	addq	$40, 168(%r15)
.L318:
	movq	216(%r15), %rdx
	movq	(%rdx), %r10
	movq	56(%rdx), %r13
	cmpq	64(%rdx), %r13
	je	.L319
	pcmpeqd	%xmm1, %xmm1
	movq	$0, 0(%r13)
	movl	$0, 8(%r13)
	movq	%r10, 16(%r13)
	movq	$0, 24(%r13)
	movq	$0, 32(%r13)
	movq	$0, 40(%r13)
	movups	%xmm1, 48(%r13)
	addq	$64, 56(%rdx)
.L320:
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	movl	%eax, %edx
	salq	$4, %rdx
	addq	128(%r15), %rdx
	cmpq	$0, (%rdx)
	je	.L598
.L328:
	imull	228(%r15), %eax
	movl	$1, %esi
	movl	%r14d, -104(%rbp)
	sall	%cl, %esi
	movl	%esi, %r8d
	movl	%esi, %r11d
	leal	(%rax,%r14), %edx
	movq	232(%r15), %rax
	notl	%r8d
	orl	%esi, (%rax,%rdx,4)
	movq	216(%r15), %rdx
	movl	20(%rbx), %eax
	movq	80(%rdx), %rdx
	andl	$16777215, %eax
	movl	%ecx, (%rdx,%rax,4)
	movq	24(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L329
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L331:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L334
	movq	32(%rbx), %rax
	movl	8(%rax), %eax
.L334:
	cmpl	$1, %eax
	jle	.L337
	movq	(%rsi), %rax
	cmpw	$52, 16(%rax)
	je	.L599
.L337:
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L550
.L329:
	movl	16(%rdx), %edi
	movl	%edi, %eax
	shrl	%eax
	andl	$1, %edi
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,8), %rsi
	movq	(%rsi), %rax
	jne	.L330
	movq	%rax, %rsi
	movq	(%rax), %rax
.L330:
	movzwl	16(%rax), %eax
	subl	$35, %eax
	cmpl	$1, %eax
	ja	.L331
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	movl	%eax, %edi
	salq	$4, %rdi
	addq	128(%r15), %rdi
	cmpq	$0, (%rdi)
	je	.L600
.L332:
	imull	228(%r15), %eax
	leal	(%rax,%r14), %edi
	movq	232(%r15), %rax
	orl	%r11d, (%rax,%rdi,4)
	movq	216(%r15), %rdi
	movl	20(%rsi), %eax
	movq	80(%rdi), %rdi
	andl	$16777215, %eax
	movl	%ecx, (%rdi,%rax,4)
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L449:
	movq	104(%r15), %rdx
	movq	72(%r15), %rsi
	subq	88(%r15), %rax
	movq	%rdx, %r10
	sarq	$3, %rax
	subq	%rsi, %r10
	movq	%rax, %rcx
	movq	%r10, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	addq	%rcx, %rax
	movq	64(%r15), %rcx
	subq	48(%r15), %rcx
	sarq	$3, %rcx
	addq	%rcx, %rax
	cmpq	$268435455, %rax
	je	.L431
	movq	32(%r15), %r9
	movq	40(%r15), %rcx
	movq	%rdx, %rax
	subq	%r9, %rax
	movq	%r9, -120(%rbp)
	movq	%rcx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	ja	.L451
	addq	$2, %rdi
	leaq	(%rdi,%rdi), %rax
	movq	%rdi, -128(%rbp)
	cmpq	%rax, %rcx
	ja	.L601
	testq	%rcx, %rcx
	movl	$1, %eax
	movq	16(%r15), %rdi
	cmovne	%rcx, %rax
	movq	16(%rdi), %r9
	leaq	2(%rcx,%rax), %rax
	movq	%rax, -120(%rbp)
	leaq	0(,%rax,8), %rsi
	movq	24(%rdi), %rax
	subq	%r9, %rax
	cmpq	%rax, %rsi
	ja	.L602
	addq	%r9, %rsi
	movq	%rsi, 16(%rdi)
.L456:
	movq	-120(%rbp), %rax
	subq	-128(%rbp), %rax
	shrq	%rax
	movq	72(%r15), %rsi
	leaq	(%r9,%rax,8), %rcx
	movq	104(%r15), %rax
	addq	$8, %rax
	cmpq	%rsi, %rax
	je	.L457
	subq	%rsi, %rax
	movq	%rcx, %rdi
	movl	%r8d, -148(%rbp)
	movq	%rax, %rdx
	movl	%r11d, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r10, -128(%rbp)
	call	memmove@PLT
	movl	-148(%rbp), %r8d
	movl	-144(%rbp), %r11d
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %r10
	movq	%rax, %rcx
.L457:
	movq	40(%r15), %rdx
	leaq	0(,%rdx,8), %rax
	cmpq	$15, %rax
	jbe	.L458
	movq	32(%r15), %rax
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L458:
	movq	-120(%rbp), %rax
	movq	%r9, 32(%r15)
	movq	%rax, 40(%r15)
.L454:
	movq	%rcx, 72(%r15)
	movq	(%rcx), %rax
	movq	(%rcx), %xmm0
	addq	$512, %rax
	movq	%rax, %xmm6
	punpcklqdq	%xmm6, %xmm0
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L578:
	leaq	2(%rsi), %rdi
	leaq	(%rdi,%rdi), %rax
	movq	%rdi, -128(%rbp)
	cmpq	%rax, %rcx
	ja	.L603
	testq	%rcx, %rcx
	movl	$1, %eax
	movq	16(%r15), %rdi
	cmovne	%rcx, %rax
	movq	16(%rdi), %r9
	leaq	2(%rcx,%rax), %rax
	movq	%rax, -120(%rbp)
	leaq	0(,%rax,8), %rsi
	movq	24(%rdi), %rax
	subq	%r9, %rax
	cmpq	%rax, %rsi
	ja	.L604
	addq	%r9, %rsi
	movq	%rsi, 16(%rdi)
.L437:
	movq	-120(%rbp), %rax
	subq	-128(%rbp), %rax
	shrq	%rax
	movq	72(%r15), %rsi
	leaq	(%r9,%rax,8), %rcx
	movq	104(%r15), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L438
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	movl	%r8d, -148(%rbp)
	movl	%r11d, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r10, -128(%rbp)
	call	memmove@PLT
	movl	-148(%rbp), %r8d
	movl	-144(%rbp), %r11d
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %r10
	movq	%rax, %rcx
.L438:
	movq	40(%r15), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L439
	movq	32(%r15), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L439:
	movq	-120(%rbp), %rax
	movq	%r9, 32(%r15)
	movq	%rax, 40(%r15)
.L435:
	movq	%rcx, 72(%r15)
	movq	(%rcx), %rax
	movq	(%rcx), %xmm0
	addq	$512, %rax
	movq	%rax, %xmm7
	punpcklqdq	%xmm7, %xmm0
.L569:
	leaq	(%rcx,%r10), %rdx
	movups	%xmm0, 56(%r15)
	movq	%rdx, 104(%r15)
	movq	(%rdx), %rax
	movq	%rax, 88(%r15)
	addq	$512, %rax
	movq	%rax, 96(%r15)
	jmp	.L451
.L269:
	sall	%cl, %eax
	movl	%ecx, %r14d
	sarl	$5, %r14d
	movl	%eax, %r8d
	movl	%eax, %r11d
	movl	%r14d, -104(%rbp)
	notl	%r8d
	jmp	.L291
.L586:
	movq	%rcx, (%rsi)
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	jmp	.L294
.L603:
	movq	-120(%rbp), %rax
	subq	%rdi, %rcx
	addq	$8, %rdx
	shrq	%rcx
	leaq	(%rax,%rcx,8), %rcx
	movq	%rdx, %rax
	subq	%r9, %rax
	cmpq	%rcx, %r9
	jbe	.L434
	cmpq	%rdx, %r9
	je	.L435
	movq	%rcx, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	movl	%r8d, -136(%rbp)
	movl	%r11d, -128(%rbp)
	movq	%r10, -120(%rbp)
	call	memmove@PLT
	movq	-120(%rbp), %r10
	movl	-128(%rbp), %r11d
	movl	-136(%rbp), %r8d
	movq	%rax, %rcx
	jmp	.L435
.L587:
	movq	88(%rcx), %r9
	subq	80(%rcx), %r9
	addl	$1, %eax
	sarq	$2, %r9
	movl	%eax, -104(%rbp)
	movq	(%r15), %r10
	imull	%r9d, %eax
	movl	%r9d, %r14d
	movq	16(%r10), %rdi
	movslq	%eax, %rdx
	movq	24(%r10), %rax
	salq	$2, %rdx
	leaq	7(%rdx), %rsi
	subq	%rdi, %rax
	andq	$-8, %rsi
	cmpq	%rax, %rsi
	ja	.L605
	addq	%rdi, %rsi
	movq	%rsi, 16(%r10)
.L348:
	xorl	%esi, %esi
	movq	%r9, -120(%rbp)
	movl	%r8d, -112(%rbp)
	call	memset@PLT
	movl	228(%r15), %ecx
	movl	-112(%rbp), %r8d
	movq	-120(%rbp), %r9
	movq	%rax, %rdi
	testl	%ecx, %ecx
	jle	.L350
	testl	%r9d, %r9d
	jle	.L350
	movslq	-104(%rbp), %rsi
	movq	%r12, -112(%rbp)
	xorl	%r11d, %r11d
	xorl	%r10d, %r10d
	movq	%rbx, -120(%rbp)
	xorl	%r9d, %r9d
	movq	%rsi, %rax
	movq	%rsi, %rbx
	negq	%rax
	salq	$2, %rax
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L353:
	movl	%r9d, %edx
	movq	232(%r15), %rsi
	imull	%ecx, %edx
	movslq	%edx, %rdx
	testl	%ecx, %ecx
	jle	.L351
	movq	%r11, %rcx
	leaq	(%rdi,%r10,4), %rax
	subq	%rdi, %rcx
	leaq	(%rcx,%rdx,4), %rdx
	addq	%rdx, %rsi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L352:
	movl	(%rsi,%rax), %ecx
	addl	$1, %edx
	addq	$4, %rax
	movl	%ecx, -4(%rax)
	movl	228(%r15), %ecx
	cmpl	%edx, %ecx
	jg	.L352
.L351:
	addl	$1, %r9d
	addq	%rbx, %r10
	addq	%r12, %r11
	cmpl	%r9d, %r14d
	jne	.L353
	movq	-112(%rbp), %r12
	movq	-120(%rbp), %rbx
.L350:
	movl	-104(%rbp), %eax
	movq	%rdi, 232(%r15)
	movl	%eax, 228(%r15)
	jmp	.L346
.L591:
	movq	88(%rdx), %r9
	subq	80(%rdx), %r9
	addl	$1, %eax
	sarq	$2, %r9
	movl	%eax, -104(%rbp)
	movq	(%r15), %r10
	imull	%r9d, %eax
	movl	%r9d, %r14d
	movq	16(%r10), %rdi
	movslq	%eax, %rdx
	movq	24(%r10), %rax
	salq	$2, %rdx
	leaq	7(%rdx), %rsi
	subq	%rdi, %rax
	andq	$-8, %rsi
	cmpq	%rax, %rsi
	ja	.L606
	addq	%rdi, %rsi
	movq	%rsi, 16(%r10)
.L385:
	xorl	%esi, %esi
	movq	%r9, -120(%rbp)
	movl	%r8d, -112(%rbp)
	call	memset@PLT
	movl	228(%r15), %ecx
	movl	-112(%rbp), %r8d
	movq	-120(%rbp), %r9
	movq	%rax, %rdi
	testl	%ecx, %ecx
	jle	.L387
	testl	%r9d, %r9d
	jle	.L387
	movslq	-104(%rbp), %rsi
	movq	%r12, -112(%rbp)
	xorl	%r11d, %r11d
	xorl	%r10d, %r10d
	movq	%rbx, -120(%rbp)
	xorl	%r9d, %r9d
	movq	%rsi, %rax
	movq	%rsi, %rbx
	negq	%rax
	salq	$2, %rax
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L390:
	movl	%r9d, %edx
	movq	232(%r15), %rsi
	imull	%ecx, %edx
	movslq	%edx, %rdx
	testl	%ecx, %ecx
	jle	.L388
	movq	%r11, %rcx
	leaq	(%rdi,%r10,4), %rax
	subq	%rdi, %rcx
	leaq	(%rcx,%rdx,4), %rdx
	addq	%rdx, %rsi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L389:
	movl	(%rsi,%rax), %ecx
	addl	$1, %edx
	addq	$4, %rax
	movl	%ecx, -4(%rax)
	movl	228(%r15), %ecx
	cmpl	%edx, %ecx
	jg	.L389
.L388:
	addl	$1, %r9d
	addq	%rbx, %r10
	addq	%r12, %r11
	cmpl	%r9d, %r14d
	jne	.L390
	movq	-112(%rbp), %r12
	movq	-120(%rbp), %rbx
.L387:
	movl	-104(%rbp), %eax
	movq	%rdi, 232(%r15)
	movl	%eax, 228(%r15)
	jmp	.L383
.L596:
	movl	$1, %eax
	movl	%ecx, %r14d
	sall	%cl, %eax
	sarl	$5, %r14d
	movl	%eax, %r8d
	movl	%r14d, -104(%rbp)
	movl	%eax, %r11d
	notl	%r8d
	jmp	.L291
.L592:
	movq	%rbx, (%rdx)
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	jmp	.L401
.L585:
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	movl	%eax, %esi
	salq	$4, %rsi
	addq	128(%r15), %rsi
	cmpq	$0, (%rsi)
	je	.L607
.L300:
	imull	228(%r15), %eax
	leal	(%rax,%r14), %esi
	movq	232(%r15), %rax
	orl	%r11d, (%rax,%rsi,4)
	movq	216(%r15), %rsi
	movl	20(%rcx), %eax
	movq	80(%rsi), %rsi
	andl	$16777215, %eax
	movl	%ebx, (%rsi,%rax,4)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	jne	.L304
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L302:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L299
.L304:
	movl	16(%rax), %edi
	movl	%edi, %ecx
	shrl	%ecx
	andl	$1, %edi
	leaq	3(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rsi
	movq	(%rsi), %rcx
	jne	.L301
	movq	%rcx, %rsi
	movq	(%rcx), %rcx
.L301:
	movzwl	16(%rcx), %ecx
	subl	$53, %ecx
	cmpl	$1, %ecx
	ja	.L302
	movl	20(%rsi), %ecx
	andl	$16777215, %ecx
	movl	%ecx, %edi
	salq	$4, %rdi
	addq	128(%r15), %rdi
	cmpq	$0, (%rdi)
	je	.L608
.L303:
	imull	228(%r15), %ecx
	leal	(%rcx,%r14), %edi
	movq	232(%r15), %rcx
	orl	%r11d, (%rcx,%rdi,4)
	movq	216(%r15), %rdi
	movl	20(%rsi), %ecx
	movq	80(%rdi), %rsi
	andl	$16777215, %ecx
	movl	%ebx, (%rsi,%rcx,4)
	jmp	.L302
.L608:
	movq	%rsi, (%rdi)
	movl	20(%rsi), %ecx
	andl	$16777215, %ecx
	jmp	.L303
.L590:
	movq	%rcx, (%rdi)
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	jmp	.L368
.L344:
	movq	32(%r12), %rax
	addq	$24, %rax
	jmp	.L345
.L594:
	movq	%rcx, (%rdi)
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	jmp	.L405
.L380:
	movq	32(%rax), %rax
	addq	$24, %rax
	jmp	.L381
.L589:
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	movl	%eax, %edi
	salq	$4, %rdi
	addq	128(%r15), %rdi
	cmpq	$0, (%rdi)
	je	.L609
.L374:
	imull	228(%r15), %eax
	leal	(%rax,%r8), %edi
	movq	232(%r15), %rax
	orl	%esi, (%rax,%rdi,4)
	movq	216(%r15), %rdi
	movl	20(%rcx), %eax
	movq	80(%rdi), %rdi
	andl	$16777215, %eax
	movl	%r13d, (%rdi,%rax,4)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	jne	.L378
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L376:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L373
.L378:
	movl	16(%rax), %r9d
	movl	%r9d, %ecx
	shrl	%ecx
	andl	$1, %r9d
	leaq	3(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rdi
	movq	(%rdi), %rcx
	jne	.L375
	movq	%rcx, %rdi
	movq	(%rcx), %rcx
.L375:
	movzwl	16(%rcx), %ecx
	subl	$53, %ecx
	cmpl	$1, %ecx
	ja	.L376
	movl	20(%rdi), %ecx
	andl	$16777215, %ecx
	movl	%ecx, %r9d
	salq	$4, %r9
	addq	128(%r15), %r9
	cmpq	$0, (%r9)
	je	.L610
.L377:
	imull	228(%r15), %ecx
	leal	(%rcx,%r8), %r9d
	movq	232(%r15), %rcx
	orl	%esi, (%rcx,%r9,4)
	movq	216(%r15), %r9
	movl	20(%rdi), %ecx
	movq	80(%r9), %rdi
	andl	$16777215, %ecx
	movl	%r13d, (%rdi,%rcx,4)
	jmp	.L376
.L610:
	movq	%rdi, (%r9)
	movl	20(%rdi), %ecx
	andl	$16777215, %ecx
	jmp	.L377
.L593:
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	movl	%eax, %edi
	salq	$4, %rdi
	addq	128(%r15), %rdi
	cmpq	$0, (%rdi)
	je	.L611
.L411:
	imull	228(%r15), %eax
	leal	(%rax,%r8), %edi
	movq	232(%r15), %rax
	orl	%esi, (%rax,%rdi,4)
	movq	216(%r15), %rdi
	movl	20(%rcx), %eax
	movq	80(%rdi), %rdi
	andl	$16777215, %eax
	movl	%r13d, (%rdi,%rax,4)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	jne	.L415
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L413:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L410
.L415:
	movl	16(%rax), %r9d
	movl	%r9d, %ecx
	shrl	%ecx
	andl	$1, %r9d
	leaq	3(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rdi
	movq	(%rdi), %rcx
	jne	.L412
	movq	%rcx, %rdi
	movq	(%rcx), %rcx
.L412:
	movzwl	16(%rcx), %ecx
	subl	$53, %ecx
	cmpl	$1, %ecx
	ja	.L413
	movl	20(%rdi), %ecx
	andl	$16777215, %ecx
	movl	%ecx, %r9d
	salq	$4, %r9
	addq	128(%r15), %r9
	cmpq	$0, (%r9)
	je	.L612
.L414:
	imull	228(%r15), %ecx
	leal	(%rcx,%r8), %r9d
	movq	232(%r15), %rcx
	orl	%esi, (%rcx,%r9,4)
	movq	216(%r15), %r9
	movl	20(%rdi), %ecx
	movq	80(%r9), %rdi
	andl	$16777215, %ecx
	movl	%r13d, (%rdi,%rcx,4)
	jmp	.L413
.L612:
	movq	%rdi, (%r9)
	movl	20(%rdi), %ecx
	andl	$16777215, %ecx
	jmp	.L414
.L583:
	movq	88(%rsi), %rcx
	subq	80(%rsi), %rcx
	addl	$1, %eax
	sarq	$2, %rcx
	movl	%eax, -104(%rbp)
	movq	(%r15), %rdi
	imull	%ecx, %eax
	movl	%ecx, %r13d
	movq	16(%rdi), %r8
	movslq	%eax, %rdx
	movq	24(%rdi), %rax
	salq	$2, %rdx
	leaq	7(%rdx), %rsi
	subq	%r8, %rax
	andq	$-8, %rsi
	cmpq	%rax, %rsi
	ja	.L613
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L272:
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%rcx, -112(%rbp)
	call	memset@PLT
	movl	228(%r15), %esi
	movq	-112(%rbp), %rcx
	movq	%rax, %r8
	testl	%esi, %esi
	jle	.L274
	testl	%ecx, %ecx
	jle	.L274
	movslq	-104(%rbp), %r11
	movq	%r12, -112(%rbp)
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movq	%r11, %rax
	negq	%rax
	salq	$2, %rax
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L277:
	movl	%edi, %edx
	movq	232(%r15), %rcx
	imull	%esi, %edx
	movslq	%edx, %rdx
	testl	%esi, %esi
	jle	.L275
	movq	%r10, %rsi
	leaq	(%r8,%r9,4), %rax
	subq	%r8, %rsi
	leaq	(%rsi,%rdx,4), %rdx
	addq	%rdx, %rcx
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L276:
	movl	(%rcx,%rax), %esi
	addl	$1, %edx
	addq	$4, %rax
	movl	%esi, -4(%rax)
	movl	228(%r15), %esi
	cmpl	%edx, %esi
	jg	.L276
.L275:
	addl	$1, %edi
	addq	%r11, %r9
	addq	%r12, %r10
	cmpl	%edi, %r13d
	jne	.L277
	movq	-112(%rbp), %r12
.L274:
	movl	-104(%rbp), %eax
	movq	%r8, 232(%r15)
	movl	%eax, 228(%r15)
	jmp	.L270
.L584:
	movq	%r12, (%rdx)
	movl	20(%r12), %eax
	andl	$16777215, %eax
	jmp	.L289
.L580:
	movl	$512, %esi
	movl	%r8d, -136(%rbp)
	movl	%r11d, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %rdx
	movl	-128(%rbp), %r11d
	movl	-136(%rbp), %r8d
	jmp	.L460
.L588:
	movq	%rbx, (%rdx)
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	jmp	.L364
.L434:
	cmpq	%rdx, %r9
	je	.L435
	leaq	8(%r10), %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	movq	%r10, -128(%rbp)
	subq	%rax, %rdi
	movl	%r8d, -144(%rbp)
	addq	%rcx, %rdi
	movl	%r11d, -136(%rbp)
	movq	%rcx, -120(%rbp)
	call	memmove@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r10
	movl	-136(%rbp), %r11d
	movl	-144(%rbp), %r8d
	jmp	.L435
.L601:
	movq	%rcx, %rax
	subq	%rdi, %rax
	movq	-120(%rbp), %rdi
	shrq	%rax
	leaq	(%rdi,%rax,8), %rcx
	leaq	8(%rdx), %rax
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	%rcx, %rsi
	jbe	.L453
	cmpq	%rax, %rsi
	je	.L454
	movq	%rcx, %rdi
	movl	%r8d, -136(%rbp)
	movl	%r11d, -128(%rbp)
	movq	%r10, -120(%rbp)
	call	memmove@PLT
	movq	-120(%rbp), %r10
	movl	-128(%rbp), %r11d
	movl	-136(%rbp), %r8d
	movq	%rax, %rcx
	jmp	.L454
.L600:
	movq	%rsi, (%rdi)
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	jmp	.L332
.L599:
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	movl	%eax, %edi
	salq	$4, %rdi
	addq	128(%r15), %rdi
	cmpq	$0, (%rdi)
	je	.L614
.L338:
	imull	228(%r15), %eax
	leal	(%rax,%r14), %edi
	movq	232(%r15), %rax
	orl	%r11d, (%rax,%rdi,4)
	movq	216(%r15), %rdi
	movl	20(%rsi), %eax
	movq	80(%rdi), %rdi
	andl	$16777215, %eax
	movl	%ecx, (%rdi,%rax,4)
	movq	24(%rsi), %rax
	testq	%rax, %rax
	jne	.L342
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L340:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L337
.L342:
	movl	16(%rax), %r9d
	movl	%r9d, %esi
	shrl	%esi
	andl	$1, %r9d
	leaq	3(%rsi,%rsi,2), %rsi
	leaq	(%rax,%rsi,8), %rdi
	movq	(%rdi), %rsi
	jne	.L339
	movq	%rsi, %rdi
	movq	(%rsi), %rsi
.L339:
	movzwl	16(%rsi), %esi
	subl	$53, %esi
	cmpl	$1, %esi
	ja	.L340
	movl	20(%rdi), %esi
	andl	$16777215, %esi
	movl	%esi, %r9d
	salq	$4, %r9
	addq	128(%r15), %r9
	cmpq	$0, (%r9)
	je	.L615
.L341:
	imull	228(%r15), %esi
	leal	(%rsi,%r14), %r9d
	movq	232(%r15), %rsi
	orl	%r11d, (%rsi,%r9,4)
	movq	216(%r15), %r9
	movl	20(%rdi), %esi
	movq	80(%r9), %rdi
	andl	$16777215, %esi
	movl	%ecx, (%rdi,%rsi,4)
	jmp	.L340
.L615:
	movq	%rdi, (%r9)
	movl	20(%rdi), %esi
	andl	$16777215, %esi
	jmp	.L341
.L280:
	movq	48(%rcx), %r8
	movq	%r13, %r10
	subq	%r8, %r10
	movq	%r10, %rax
	sarq	$6, %rax
	cmpq	$33554431, %rax
	je	.L321
	testq	%rax, %rax
	je	.L465
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L616
	movl	$2147483584, %esi
	movl	$2147483584, %r9d
.L283:
	movq	40(%rcx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r11
	subq	%rax, %r11
	cmpq	%rsi, %r11
	jb	.L617
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L286:
	movq	%rax, %rdi
	addq	%rax, %r9
	leaq	64(%rax), %rsi
.L284:
	leaq	(%rdi,%r10), %rax
	pcmpeqd	%xmm4, %xmm4
	movq	$0, (%rax)
	movl	$0, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movups	%xmm4, 48(%rax)
	cmpq	%r8, %r13
	je	.L287
	movq	%r8, %rax
	movq	%rdi, %rdx
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L288:
	movq	(%rax), %rsi
	addq	$64, %rax
	addq	$64, %rdx
	movq	%rsi, -64(%rdx)
	movl	-56(%rax), %esi
	movl	%esi, -56(%rdx)
	movq	-48(%rax), %rsi
	movq	%rsi, -48(%rdx)
	movdqu	-40(%rax), %xmm1
	movups	%xmm1, -40(%rdx)
	movq	-24(%rax), %rsi
	movq	%rsi, -24(%rdx)
	movdqu	-16(%rax), %xmm2
	movq	$0, -24(%rax)
	movups	%xmm0, -40(%rax)
	movups	%xmm2, -16(%rdx)
	cmpq	%rax, %r13
	jne	.L288
	subq	%r8, %r13
	leaq	64(%rdi,%r13), %rsi
.L287:
	movq	%rdi, %xmm0
	movq	%rsi, %xmm2
	movq	%r9, 64(%rcx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 48(%rcx)
	jmp	.L281
.L278:
	leaq	-96(%rbp), %rdx
	leaq	152(%r15), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler12TempLoopInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L279
.L607:
	movq	%rcx, (%rsi)
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	jmp	.L300
.L354:
	leaq	-96(%rbp), %rdx
	leaq	152(%r15), %rdi
	movl	%r8d, -104(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler12TempLoopInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movl	-104(%rbp), %r8d
	jmp	.L355
.L356:
	movq	48(%rdx), %r11
	movq	%r14, %rax
	subq	%r11, %rax
	movq	%rax, -104(%rbp)
	sarq	$6, %rax
	movq	%rax, %rsi
	cmpq	$33554431, %rax
	je	.L321
	testq	%rax, %rax
	je	.L473
	leaq	(%rax,%rax), %rax
	cmpq	%rax, %rsi
	jbe	.L618
	movl	$2147483584, %esi
	movl	$2147483584, %r9d
.L358:
	movq	40(%rdx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r10
	subq	%rax, %r10
	cmpq	%rsi, %r10
	jb	.L619
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L361:
	addq	%rax, %r9
	leaq	64(%rax), %rsi
	jmp	.L359
.L618:
	testq	%rax, %rax
	jne	.L620
	movl	$64, %esi
	xorl	%r9d, %r9d
	xorl	%eax, %eax
.L359:
	movq	-104(%rbp), %r10
	pcmpeqd	%xmm3, %xmm3
	addq	%rax, %r10
	movq	$0, (%r10)
	movl	$0, 8(%r10)
	movq	%rcx, 16(%r10)
	movq	$0, 24(%r10)
	movq	$0, 32(%r10)
	movq	$0, 40(%r10)
	movups	%xmm3, 48(%r10)
	cmpq	%r11, %r14
	je	.L362
	movq	%r11, %rcx
	movq	%rax, %rsi
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L363:
	movq	(%rcx), %rdi
	addq	$64, %rcx
	addq	$64, %rsi
	movq	%rdi, -64(%rsi)
	movl	-56(%rcx), %edi
	movl	%edi, -56(%rsi)
	movq	-48(%rcx), %rdi
	movq	%rdi, -48(%rsi)
	movdqu	-40(%rcx), %xmm3
	movups	%xmm3, -40(%rsi)
	movq	-24(%rcx), %rdi
	movq	%rdi, -24(%rsi)
	movdqu	-16(%rcx), %xmm4
	movq	$0, -24(%rcx)
	movups	%xmm0, -40(%rcx)
	movups	%xmm4, -16(%rsi)
	cmpq	%rcx, %r14
	jne	.L363
	subq	%r11, %r14
	leaq	64(%rax,%r14), %rsi
.L362:
	movq	%rax, %xmm0
	movq	%rsi, %xmm4
	movq	%r9, 64(%rdx)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 48(%rdx)
	jmp	.L357
.L393:
	movq	48(%rdx), %r11
	movq	%r14, %rax
	subq	%r11, %rax
	movq	%rax, -104(%rbp)
	sarq	$6, %rax
	movq	%rax, %rsi
	cmpq	$33554431, %rax
	je	.L321
	testq	%rax, %rax
	je	.L476
	leaq	(%rax,%rax), %rax
	cmpq	%rax, %rsi
	jbe	.L621
	movl	$2147483584, %esi
	movl	$2147483584, %r9d
.L395:
	movq	40(%rdx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r10
	subq	%rax, %r10
	cmpq	%rsi, %r10
	jb	.L622
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L398:
	addq	%rax, %r9
	leaq	64(%rax), %rsi
	jmp	.L396
.L621:
	testq	%rax, %rax
	jne	.L623
	movl	$64, %esi
	xorl	%r9d, %r9d
	xorl	%eax, %eax
.L396:
	movq	-104(%rbp), %r10
	pcmpeqd	%xmm5, %xmm5
	addq	%rax, %r10
	movq	$0, (%r10)
	movl	$0, 8(%r10)
	movq	%rcx, 16(%r10)
	movq	$0, 24(%r10)
	movq	$0, 32(%r10)
	movq	$0, 40(%r10)
	movups	%xmm5, 48(%r10)
	cmpq	%r11, %r14
	je	.L399
	movq	%r11, %rcx
	movq	%rax, %rsi
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L400:
	movq	(%rcx), %rdi
	addq	$64, %rcx
	addq	$64, %rsi
	movq	%rdi, -64(%rsi)
	movl	-56(%rcx), %edi
	movl	%edi, -56(%rsi)
	movq	-48(%rcx), %rdi
	movq	%rdi, -48(%rsi)
	movdqu	-40(%rcx), %xmm5
	movups	%xmm5, -40(%rsi)
	movq	-24(%rcx), %rdi
	movq	%rdi, -24(%rsi)
	movdqu	-16(%rcx), %xmm6
	movq	$0, -24(%rcx)
	movups	%xmm0, -40(%rcx)
	movups	%xmm6, -16(%rsi)
	cmpq	%rcx, %r14
	jne	.L400
	subq	%r11, %r14
	leaq	64(%rax,%r14), %rsi
.L399:
	movq	%rax, %xmm0
	movq	%rsi, %xmm6
	movq	%r9, 64(%rdx)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 48(%rdx)
	jmp	.L394
.L246:
	movq	104(%r15), %r13
	movq	72(%r15), %rsi
	movq	%rdx, %rax
	subq	88(%r15), %rax
	movq	%r13, %r12
	sarq	$3, %rax
	subq	%rsi, %r12
	movq	%r12, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	64(%r15), %rdx
	subq	48(%r15), %rdx
	sarq	$3, %rdx
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L431
	movq	32(%r15), %rdi
	movq	40(%r15), %rax
	movq	%r13, %rdx
	subq	%rdi, %rdx
	movq	%rax, %r10
	sarq	$3, %rdx
	subq	%rdx, %r10
	cmpq	$1, %r10
	jbe	.L624
.L249:
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L257
	cmpq	$63, 8(%rax)
	ja	.L625
.L257:
	movq	16(%r15), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L626
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L258:
	movq	%rax, 8(%r13)
	movq	80(%r15), %rax
	movq	%rbx, (%rax)
	movq	104(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 104(%r15)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 88(%r15)
	movq	%rdx, 96(%r15)
	movq	%rax, 80(%r15)
	jmp	.L247
.L391:
	leaq	-96(%rbp), %rdx
	leaq	152(%r15), %rdi
	movl	%r8d, -104(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler12TempLoopInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movl	-104(%rbp), %r8d
	jmp	.L392
.L609:
	movq	%rcx, (%rdi)
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	jmp	.L374
.L611:
	movq	%rcx, (%rdi)
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	jmp	.L411
.L597:
	movq	88(%rsi), %r13
	subq	80(%rsi), %r13
	addl	$1, %eax
	sarq	$2, %r13
	movl	%eax, -104(%rbp)
	movq	(%r15), %rdi
	imull	%r13d, %eax
	movl	%r13d, -112(%rbp)
	movq	16(%rdi), %r8
	movslq	%eax, %rdx
	movq	24(%rdi), %rax
	salq	$2, %rdx
	leaq	7(%rdx), %rsi
	subq	%r8, %rax
	andq	$-8, %rsi
	cmpq	%rax, %rsi
	ja	.L627
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L311:
	xorl	%esi, %esi
	movq	%r8, %rdi
	movl	%ecx, -120(%rbp)
	call	memset@PLT
	movl	228(%r15), %esi
	movl	-120(%rbp), %ecx
	movq	%rax, %r8
	testl	%esi, %esi
	jle	.L313
	testl	%r13d, %r13d
	jle	.L313
	movslq	-104(%rbp), %r13
	movl	%ecx, -128(%rbp)
	xorl	%r11d, %r11d
	xorl	%r10d, %r10d
	movl	-112(%rbp), %ecx
	movq	%rbx, -120(%rbp)
	xorl	%r9d, %r9d
	movq	%r13, %rax
	negq	%rax
	salq	$2, %rax
	movq	%rax, %rbx
.L316:
	movl	%r9d, %edx
	movq	232(%r15), %rdi
	imull	%esi, %edx
	movslq	%edx, %rdx
	testl	%esi, %esi
	jle	.L314
	movq	%r11, %rsi
	leaq	(%r8,%r10,4), %rax
	subq	%r8, %rsi
	leaq	(%rsi,%rdx,4), %rdx
	addq	%rdx, %rdi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L315:
	movl	(%rdi,%rax), %esi
	addl	$1, %edx
	addq	$4, %rax
	movl	%esi, -4(%rax)
	movl	228(%r15), %esi
	cmpl	%edx, %esi
	jg	.L315
.L314:
	addl	$1, %r9d
	addq	%r13, %r10
	addq	%rbx, %r11
	cmpl	%r9d, %ecx
	jne	.L316
	movq	-120(%rbp), %rbx
	movl	-128(%rbp), %ecx
.L313:
	movl	-104(%rbp), %eax
	movq	%r8, 232(%r15)
	movl	%eax, 228(%r15)
	jmp	.L309
.L616:
	testq	%rsi, %rsi
	jne	.L628
	movl	$64, %esi
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	jmp	.L284
.L570:
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L238
.L598:
	movq	%rbx, (%rdx)
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	jmp	.L328
.L453:
	cmpq	%rax, %rsi
	je	.L454
	leaq	8(%r10), %rdi
	movl	%r8d, -144(%rbp)
	subq	%rdx, %rdi
	movl	%r11d, -136(%rbp)
	addq	%rcx, %rdi
	movq	%r10, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	memmove@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r10
	movl	-136(%rbp), %r11d
	movl	-144(%rbp), %r8d
	jmp	.L454
.L465:
	movl	$64, %esi
	movl	$64, %r9d
	jmp	.L283
.L319:
	movq	48(%rdx), %r11
	movq	%r13, %rax
	subq	%r11, %rax
	movq	%rax, -104(%rbp)
	sarq	$6, %rax
	movq	%rax, %rsi
	cmpq	$33554431, %rax
	je	.L321
	testq	%rax, %rax
	je	.L469
	leaq	(%rax,%rax), %rax
	cmpq	%rax, %rsi
	jbe	.L629
	movl	$2147483584, %esi
	movl	$2147483584, %r8d
.L322:
	movq	40(%rdx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r9
	subq	%rax, %r9
	cmpq	%rsi, %r9
	jb	.L630
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L325:
	addq	%rax, %r8
	leaq	64(%rax), %rsi
.L323:
	movq	-104(%rbp), %r9
	pcmpeqd	%xmm7, %xmm7
	addq	%rax, %r9
	movq	$0, (%r9)
	movl	$0, 8(%r9)
	movq	%r10, 16(%r9)
	movq	$0, 24(%r9)
	movq	$0, 32(%r9)
	movq	$0, 40(%r9)
	movups	%xmm7, 48(%r9)
	cmpq	%r11, %r13
	je	.L326
	movq	%r11, %rsi
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
.L327:
	movq	(%rsi), %r9
	addq	$64, %rsi
	addq	$64, %rdi
	movq	%r9, -64(%rdi)
	movl	-56(%rsi), %r9d
	movl	%r9d, -56(%rdi)
	movq	-48(%rsi), %r9
	movq	%r9, -48(%rdi)
	movdqu	-40(%rsi), %xmm4
	movups	%xmm4, -40(%rdi)
	movq	-24(%rsi), %r9
	movq	%r9, -24(%rdi)
	movdqu	-16(%rsi), %xmm5
	movq	$0, -24(%rsi)
	movups	%xmm0, -40(%rsi)
	movups	%xmm5, -16(%rdi)
	cmpq	%rsi, %r13
	jne	.L327
	subq	%r11, %r13
	leaq	64(%rax,%r13), %rsi
.L326:
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	movq	%r8, 64(%rdx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 48(%rdx)
	jmp	.L320
.L317:
	leaq	-96(%rbp), %rdx
	leaq	152(%r15), %rdi
	movl	%ecx, -104(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler12TempLoopInfoENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movl	-104(%rbp), %ecx
	jmp	.L318
.L469:
	movl	$64, %esi
	movl	$64, %r8d
	jmp	.L322
.L473:
	movl	$64, %esi
	movl	$64, %r9d
	jmp	.L358
.L614:
	movq	%rsi, (%rdi)
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	jmp	.L338
.L624:
	leaq	2(%rcx), %r14
	leaq	(%r14,%r14), %rdx
	cmpq	%rdx, %rax
	jbe	.L250
	subq	%r14, %rax
	addq	$8, %r13
	shrq	%rax
	movq	%r13, %rdx
	leaq	(%rdi,%rax,8), %r14
	subq	%rsi, %rdx
	cmpq	%r14, %rsi
	jbe	.L251
	cmpq	%r13, %rsi
	je	.L252
	movq	%r14, %rdi
	call	memmove@PLT
.L252:
	movq	%r14, 72(%r15)
	movq	(%r14), %rax
	leaq	(%r14,%r12), %r13
	movq	(%r14), %xmm0
	movq	%r13, 104(%r15)
	addq	$512, %rax
	movq	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 56(%r15)
	movq	0(%r13), %rax
	movq	%rax, 88(%r15)
	addq	$512, %rax
	movq	%rax, 96(%r15)
	jmp	.L249
.L476:
	movl	$64, %esi
	movl	$64, %r9d
	jmp	.L395
.L629:
	testq	%rax, %rax
	jne	.L631
	movl	$64, %esi
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	jmp	.L323
.L625:
	movq	(%rax), %rdx
	movq	%rdx, 24(%r15)
	jmp	.L258
.L613:
	movq	%rcx, -120(%rbp)
	movq	%rdx, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %rcx
	movq	%rax, %r8
	jmp	.L272
.L604:
	movl	%r8d, -148(%rbp)
	movl	%r11d, -144(%rbp)
	movq	%r10, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-136(%rbp), %r10
	movl	-144(%rbp), %r11d
	movl	-148(%rbp), %r8d
	movq	%rax, %r9
	jmp	.L437
.L606:
	movq	%r10, %rdi
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movl	%r8d, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-112(%rbp), %r8d
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %r9
	movq	%rax, %rdi
	jmp	.L385
.L605:
	movq	%r10, %rdi
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movl	%r8d, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-112(%rbp), %r8d
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %r9
	movq	%rax, %rdi
	jmp	.L348
.L250:
	testq	%rax, %rax
	movl	$1, %edx
	movq	16(%r15), %rdi
	cmovne	%rax, %rdx
	movq	16(%rdi), %rcx
	leaq	2(%rax,%rdx), %r13
	movq	24(%rdi), %rax
	leaq	0(,%r13,8), %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L632
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L254:
	movq	%r13, %rax
	movq	72(%r15), %rsi
	subq	%r14, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r14
	movq	104(%r15), %rax
	addq	$8, %rax
	cmpq	%rsi, %rax
	je	.L255
	subq	%rsi, %rax
	movq	%r14, %rdi
	movq	%rcx, -104(%rbp)
	movq	%rax, %rdx
	call	memmove@PLT
	movq	-104(%rbp), %rcx
.L255:
	movq	40(%r15), %rdx
	leaq	0(,%rdx,8), %rax
	cmpq	$15, %rax
	jbe	.L256
	movq	32(%r15), %rax
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L256:
	movq	%rcx, 32(%r15)
	movq	%r13, 40(%r15)
	jmp	.L252
.L251:
	cmpq	%r13, %rsi
	je	.L252
	leaq	8(%r12), %rdi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L252
.L622:
	movq	%r9, -144(%rbp)
	movq	%r11, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movl	%r8d, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-112(%rbp), %r8d
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %r11
	movq	-144(%rbp), %r9
	jmp	.L398
.L627:
	movq	%rdx, -128(%rbp)
	movl	%ecx, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-120(%rbp), %ecx
	movq	-128(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L311
.L617:
	movq	%r9, -136(%rbp)
	movq	%r10, -128(%rbp)
	movq	%r8, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %r8
	movq	-128(%rbp), %r10
	movq	-136(%rbp), %r9
	jmp	.L286
.L602:
	movl	%r8d, -148(%rbp)
	movl	%r11d, -144(%rbp)
	movq	%r10, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-136(%rbp), %r10
	movl	-144(%rbp), %r11d
	movl	-148(%rbp), %r8d
	movq	%rax, %r9
	jmp	.L456
.L619:
	movq	%r9, -144(%rbp)
	movq	%r11, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movl	%r8d, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-112(%rbp), %r8d
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %r11
	movq	-144(%rbp), %r9
	jmp	.L361
.L626:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L258
.L631:
	cmpq	$33554431, %rax
	movl	$33554431, %r8d
	cmova	%r8, %rax
	salq	$6, %rax
	movq	%rax, %r8
	movq	%rax, %rsi
	jmp	.L322
.L620:
	cmpq	$33554431, %rax
	movl	$33554431, %r9d
	cmova	%r9, %rax
	salq	$6, %rax
	movq	%rax, %r9
	movq	%rax, %rsi
	jmp	.L358
.L595:
	call	__stack_chk_fail@PLT
.L431:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L623:
	cmpq	$33554431, %rax
	movl	$33554431, %r9d
	cmova	%r9, %rax
	salq	$6, %rax
	movq	%rax, %r9
	movq	%rax, %rsi
	jmp	.L395
.L628:
	cmpq	$33554431, %rsi
	movl	$33554431, %r9d
	cmovbe	%rsi, %r9
	salq	$6, %r9
	movq	%r9, %rsi
	jmp	.L283
.L321:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L632:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L254
.L630:
	movq	%r8, -144(%rbp)
	movq	%r11, -136(%rbp)
	movq	%r10, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movl	%ecx, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-112(%rbp), %ecx
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %r10
	movq	-136(%rbp), %r11
	movq	-144(%rbp), %r8
	jmp	.L325
	.cfi_endproc
.LFE10260:
	.size	_ZN2v88internal8compiler14LoopFinderImpl17PropagateBackwardEv, .-_ZN2v88internal8compiler14LoopFinderImpl17PropagateBackwardEv
	.section	.rodata._ZN2v88internal8compiler10LoopFinder13BuildLoopTreeEPNS1_5GraphEPNS0_11TickCounterEPNS0_4ZoneE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"cannot create std::vector larger than max_size()"
	.align 8
.LC10:
	.string	"ni.node->opcode() != IrOpcode::kReturn"
	.section	.rodata._ZN2v88internal8compiler10LoopFinder13BuildLoopTreeEPNS1_5GraphEPNS0_11TickCounterEPNS0_4ZoneE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"Check failed: %s."
.LC12:
	.string	"X"
.LC13:
	.string	">"
.LC14:
	.string	"<"
.LC15:
	.string	" "
.LC16:
	.string	" #%d:%s\n"
.LC17:
	.string	"Loop %d headed at #%d\n"
	.section	.text._ZN2v88internal8compiler10LoopFinder13BuildLoopTreeEPNS1_5GraphEPNS0_11TickCounterEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10LoopFinder13BuildLoopTreeEPNS1_5GraphEPNS0_11TickCounterEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler10LoopFinder13BuildLoopTreeEPNS1_5GraphEPNS0_11TickCounterEPNS0_4ZoneE, @function
_ZN2v88internal8compiler10LoopFinder13BuildLoopTreeEPNS1_5GraphEPNS0_11TickCounterEPNS0_4ZoneE:
.LFB10279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$616, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movl	28(%rdi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%r15), %rax
	movq	%rax, %rsi
	movq	%rax, -344(%rbp)
	movq	24(%r15), %rax
	subq	%rsi, %rax
	cmpq	$135, %rax
	jbe	.L1103
	movq	%rsi, %rax
	addq	$136, %rax
	movq	%rax, 16(%r15)
.L635:
	movq	-344(%rbp), %rax
	movq	%r15, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rax)
	movq	%r15, 40(%rax)
	movq	$0, 64(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 48(%rax)
	movslq	%ebx, %rax
	cmpq	$536870911, %rax
	ja	.L647
	movq	-344(%rbp), %rbx
	leaq	0(,%rax,4), %rdx
	movq	%r15, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	xorl	%ebx, %ebx
	testq	%rax, %rax
	je	.L875
	movq	16(%r15), %rdi
	movq	24(%r15), %rax
	leaq	7(%rdx), %rsi
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1104
	addq	%rdi, %rsi
	movq	%rsi, 16(%r15)
.L639:
	movq	-344(%rbp), %rax
	leaq	(%rdi,%rdx), %rbx
	movl	$255, %esi
	movq	%rdi, 80(%rax)
	movq	%rbx, 96(%rax)
	call	memset@PLT
.L875:
	movq	-344(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, -320(%rbp)
	movq	%r12, -304(%rbp)
	movq	%rbx, 88(%rax)
	movq	%r15, 104(%rax)
	movq	$0, 112(%rax)
	movq	$0, 120(%rax)
	movq	$0, 128(%rax)
	movq	16(%r12), %rbx
	movq	24(%r12), %rdx
	movq	16(%r13), %rax
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	subq	%rbx, %rdx
	movq	%rax, -312(%rbp)
	movq	$8, -280(%rbp)
	movaps	%xmm0, -272(%rbp)
	movaps	%xmm0, -256(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	cmpq	$63, %rdx
	jbe	.L1105
	leaq	64(%rbx), %rax
	movq	%rbx, -288(%rbp)
	addq	$24, %rbx
	movq	%rax, 16(%r12)
.L642:
	movq	-304(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L643
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L645:
	movq	%rax, (%rbx)
	leaq	-208(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, -248(%rbp)
	movq	(%rbx), %rdx
	movq	%rbx, -216(%rbp)
	leaq	512(%rdx), %rax
	movq	%rdx, -264(%rbp)
	movq	%rax, -256(%rbp)
	movq	(%rbx), %rax
	movq	%rdx, -272(%rbp)
	movl	$2, %edx
	leaq	512(%rax), %rcx
	movq	%rax, -232(%rbp)
	movq	%rax, -240(%rbp)
	movq	%rcx, -224(%rbp)
	call	_ZN2v88internal8compiler14NodeMarkerBaseC2EPNS1_5GraphEj@PLT
	movl	28(%r13), %eax
	cmpq	$134217727, %rax
	ja	.L647
	movq	%rax, %rbx
	movq	%r12, -200(%rbp)
	movq	$0, -192(%rbp)
	salq	$4, %rbx
	movq	$0, -184(%rbp)
	movq	$0, -176(%rbp)
	testq	%rax, %rax
	je	.L648
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	movq	%rbx, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rbx
	ja	.L1106
	addq	%rax, %rsi
	movq	%rsi, 16(%r12)
.L650:
	leaq	(%rax,%rbx), %rdx
	movq	%rax, -192(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L651:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L651
	movl	28(%r13), %eax
	movq	%rdx, -184(%rbp)
	movq	%r12, -168(%rbp)
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	cmpq	$536870911, %rax
	ja	.L647
	movq	%r12, -136(%rbp)
	leaq	0(,%rax,4), %rdx
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	testq	%rax, %rax
	je	.L652
	movq	16(%r12), %rdi
	movq	24(%r12), %rax
	leaq	7(%rdx), %rsi
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1107
	addq	%rdi, %rsi
	movq	%rsi, 16(%r12)
.L654:
	leaq	(%rdi,%rdx), %rbx
	movl	$255, %esi
	movq	%rdi, -128(%rbp)
	movq	%rbx, -112(%rbp)
	call	memset@PLT
.L874:
	movq	-344(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rbx, -120(%rbp)
	movq	$0, -96(%rbp)
	movq	%rax, -104(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -352(%rbp)
	movq	%r14, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	call	_ZN2v88internal8compiler14LoopFinderImpl17PropagateBackwardEv
	movq	-104(%rbp), %rax
	movq	-320(%rbp), %r8
	movq	88(%rax), %rbx
	subq	80(%rax), %rbx
	movl	-92(%rbp), %eax
	sarq	$2, %rbx
	movq	16(%r8), %rdi
	imull	%ebx, %eax
	cltq
	leaq	7(,%rax,4), %rsi
	movq	24(%r8), %rax
	andq	$-8, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L1108
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L656:
	imull	-92(%rbp), %ebx
	xorl	%esi, %esi
	movq	%rdi, -80(%rbp)
	movl	$1, %r15d
	movslq	%ebx, %rdx
	salq	$2, %rdx
	call	memset@PLT
	movq	-152(%rbp), %r14
	movq	-160(%rbp), %rbx
	cmpq	%r14, %rbx
	jne	.L675
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L1109:
	movq	%r12, (%rax)
	addq	$8, -240(%rbp)
.L661:
	movl	-208(%rbp), %eax
	addl	$1, %eax
	movl	%eax, 16(%r12)
.L659:
	addq	$40, %rbx
	cmpq	%rbx, %r14
	je	.L676
.L675:
	movq	(%rbx), %rax
	movq	-104(%rbp), %rdx
	movl	%r15d, %edi
	movl	20(%rax), %eax
	movq	80(%rdx), %rdx
	andl	$16777215, %eax
	movl	%eax, %ecx
	imull	-92(%rbp), %eax
	movl	(%rdx,%rcx,4), %ecx
	movl	%ecx, %edx
	sall	%cl, %edi
	sarl	$5, %edx
	addl	%eax, %edx
	movq	-80(%rbp), %rax
	orl	%edi, (%rax,%rdx,4)
	movq	(%rbx), %r12
	movl	-208(%rbp), %eax
	cmpl	%eax, 16(%r12)
	ja	.L659
	movq	-224(%rbp), %rcx
	movq	-240(%rbp), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	jne	.L1109
	movq	-216(%rbp), %rcx
	movq	-248(%rbp), %rsi
	subq	-232(%rbp), %rax
	movq	%rcx, %r13
	sarq	$3, %rax
	subq	%rsi, %r13
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	-256(%rbp), %rax
	subq	-272(%rbp), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L694
	movq	-288(%rbp), %r8
	movq	-280(%rbp), %rdx
	movq	%rcx, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L1110
.L663:
	movq	-296(%rbp), %rax
	testq	%rax, %rax
	je	.L671
	cmpq	$63, 8(%rax)
	ja	.L1111
.L671:
	movq	-304(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1112
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L672:
	movq	%rax, 8(%rcx)
	movq	-240(%rbp), %rax
	movq	%r12, (%rax)
	movq	-216(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -216(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -232(%rbp)
	movq	%rdx, -224(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L676:
	movl	$1, %r12d
	.p2align 4,,10
	.p2align 3
.L682:
	movq	-272(%rbp), %rax
	cmpq	%rax, -240(%rbp)
	je	.L677
	movq	-72(%rbp), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	-256(%rbp), %rbx
	movq	-272(%rbp), %rax
	leaq	-8(%rbx), %rdx
	movq	(%rax), %r14
	cmpq	%rdx, %rax
	je	.L678
	addq	$8, %rax
	movq	%rax, -272(%rbp)
.L679:
	movl	-208(%rbp), %eax
	movl	%eax, 16(%r14)
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L682
	movq	(%rax), %r13
	.p2align 4,,10
	.p2align 3
.L707:
	movl	16(%rax), %edx
	movl	%edx, %r15d
	shrl	%r15d
	andl	$1, %edx
	movl	%r15d, %ecx
	leaq	3(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rbx
	jne	.L683
	movq	(%rbx), %rbx
.L683:
	movq	-104(%rbp), %rdx
	movl	20(%rbx), %eax
	movq	80(%rdx), %rdx
	andl	$16777215, %eax
	movl	(%rdx,%rax,4), %ecx
	testl	%ecx, %ecx
	jle	.L688
	movq	(%rbx), %rax
	movzwl	16(%rax), %eax
	leal	-35(%rax), %edx
	cmpl	$1, %edx
	jbe	.L1113
	cmpl	$1, %eax
	je	.L1090
.L688:
	cmpq	%rbx, %r14
	je	.L689
	movl	20(%r14), %eax
	movl	20(%rbx), %edx
	movl	-92(%rbp), %ecx
	andl	$16777215, %eax
	andl	$16777215, %edx
	imull	%ecx, %eax
	imull	%ecx, %edx
	testl	%ecx, %ecx
	jle	.L689
	movslq	%edx, %rdx
	cltq
	xorl	%esi, %esi
	xorl	%edi, %edi
	subq	%rdx, %rax
	leaq	0(,%rdx,4), %rcx
	salq	$2, %rax
	.p2align 4,,10
	.p2align 3
.L691:
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %r10
	leaq	(%rdx,%rcx), %r9
	addq	%rax, %rdx
	movl	(%r9), %r8d
	movl	(%rdx,%rcx), %edx
	andl	(%r10,%rcx), %edx
	orl	%r8d, %edx
	cmpl	%r8d, %edx
	movl	%esi, %r8d
	movl	%edx, (%r9)
	setne	%dl
	xorl	$1, %r8d
	andb	%r8b, %dl
	cmovne	%edx, %esi
	addl	$1, %edi
	addq	$4, %rcx
	cmpl	%edi, -92(%rbp)
	jg	.L691
	testb	%sil, %sil
	je	.L689
	movl	-208(%rbp), %eax
	cmpl	%eax, 16(%rbx)
	ja	.L689
	movq	-224(%rbp), %rcx
	movq	-240(%rbp), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L692
	movq	%rbx, (%rax)
	addq	$8, -240(%rbp)
.L693:
	movl	-208(%rbp), %eax
	addl	$1, %eax
	movl	%eax, 16(%rbx)
	.p2align 4,,10
	.p2align 3
.L689:
	testq	%r13, %r13
	je	.L682
	movq	%r13, %rax
	movq	0(%r13), %r13
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L678:
	movq	-296(%rbp), %rax
	testq	%rax, %rax
	je	.L680
	cmpq	$64, 8(%rax)
	ja	.L681
.L680:
	movq	-264(%rbp), %rax
	movq	$64, 8(%rax)
	movq	-296(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -296(%rbp)
.L681:
	movq	-248(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -248(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -264(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rax, -272(%rbp)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE@PLT
	cmpl	%eax, %r15d
	je	.L688
.L1090:
	testl	%r15d, %r15d
	jne	.L689
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L677:
	movl	-96(%rbp), %edi
	testl	%edi, %edi
	je	.L732
	cmpl	$1, %edi
	je	.L1114
	movq	-192(%rbp), %rdx
	testl	%edi, %edi
	jle	.L735
	leaq	-328(%rbp), %rax
	xorl	%r11d, %r11d
	movq	%rax, -448(%rbp)
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L736:
	addq	$1, %r11
	leal	1(%r11), %eax
	cmpl	%eax, %edi
	jl	.L735
.L808:
	movq	-160(%rbp), %rcx
	leaq	(%r11,%r11,4), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	32(%rax), %r10
	movq	%rax, -368(%rbp)
	testq	%r10, %r10
	jne	.L736
	movq	(%rax), %rcx
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rdx, %rax
	cmpq	$0, (%rax)
	movq	%rax, -360(%rbp)
	je	.L1115
.L737:
	movq	%r11, %rax
	salq	$6, %rax
	movq	%rax, -376(%rbp)
	testl	%edi, %edi
	jle	.L738
	leal	1(%r11), %eax
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r11, -408(%rbp)
	movl	%eax, -396(%rbp)
	movl	%ecx, %r11d
	movq	%r8, %r9
.L802:
	cmpl	-396(%rbp), %r11d
	je	.L739
	movq	-360(%rbp), %rax
	movl	%r11d, %edx
	movq	-80(%rbp), %rcx
	sarl	$5, %edx
	movq	(%rax), %rax
	movl	20(%rax), %eax
	andl	$16777215, %eax
	imull	-92(%rbp), %eax
	addl	%edx, %eax
	movq	-88(%rbp), %rdx
	cltq
	movl	(%rdx,%rax,4), %edx
	andl	(%rcx,%rax,4), %edx
	btl	%r11d, %edx
	jc	.L1116
.L739:
	addl	$1, %r11d
	addq	$1, %r9
	cmpl	%edi, %r11d
	jle	.L802
	movq	-104(%rbp), %rax
	movq	-376(%rbp), %rbx
	movq	-408(%rbp), %r11
	addq	48(%rax), %rbx
	movq	%rbx, %rax
	movq	-368(%rbp), %rbx
	movq	%rax, 32(%rbx)
	movq	-104(%rbp), %rdi
	movq	%rax, -328(%rbp)
	testq	%r10, %r10
	je	.L803
	movq	32(%r10), %rsi
	cmpq	40(%r10), %rsi
	je	.L804
	movq	%rax, (%rsi)
	addq	$8, 32(%r10)
.L805:
	movq	-328(%rbp), %rax
	movq	%r10, (%rax)
	movl	8(%r10), %eax
	movq	-328(%rbp), %rdx
	addl	$1, %eax
	movl	%eax, 8(%rdx)
.L806:
	addq	$1, %r11
	movl	-96(%rbp), %edi
	movq	-192(%rbp), %rdx
	leal	1(%r11), %eax
	cmpl	%eax, %edi
	jge	.L808
.L735:
	movq	-184(%rbp), %rax
	movq	%rax, -384(%rbp)
	cmpq	%rdx, %rax
	je	.L885
	movq	%rdx, %r14
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L818:
	movq	(%r14), %r9
	testq	%r9, %r9
	je	.L810
	movl	20(%r9), %edx
	movl	-92(%rbp), %ecx
	andl	$16777215, %edx
	movl	%edx, %eax
	imull	%ecx, %eax
	testl	%ecx, %ecx
	jle	.L810
	movq	-160(%rbp), %rbx
	movq	-88(%rbp), %r15
	cltq
	xorl	%edi, %edi
	salq	$2, %rax
	movq	%rbx, -360(%rbp)
	addq	%rax, %r15
	leal	-1(%rcx), %ebx
	xorl	%ecx, %ecx
	addq	-80(%rbp), %rax
	movq	%rbx, -368(%rbp)
	movq	%rax, %rbx
	xorl	%eax, %eax
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L1118:
	movq	32(%rax), %rdx
	movq	32(%r10), %r12
	movl	8(%r12), %r12d
	cmpl	8(%rdx), %r12d
	cmovg	%esi, %edi
	cmovg	%r10, %rax
.L811:
	addl	$1, %r8d
	addl	$1, %esi
	cmpl	$32, %r8d
	jne	.L812
	movl	-376(%rbp), %edx
	leaq	1(%rcx), %rsi
	cmpq	%rcx, -368(%rbp)
	je	.L1117
	movq	%rsi, %rcx
.L813:
	movl	(%r15,%rcx,4), %esi
	andl	(%rbx,%rcx,4), %esi
	movl	%edx, -376(%rbp)
	xorl	%r8d, %r8d
	movl	%esi, %r11d
	movl	%ecx, %esi
	sall	$5, %esi
.L812:
	btl	%r8d, %r11d
	jnc	.L811
	testl	%esi, %esi
	je	.L811
	leal	-1(%rsi), %r10d
	movq	-360(%rbp), %r12
	movslq	%r10d, %r10
	leaq	(%r10,%r10,4), %r10
	leaq	(%r12,%r10,8), %r10
	testq	%rax, %rax
	jne	.L1118
	movl	%esi, %edi
	movq	%r10, %rax
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	-160(%rbp), %rdx
	leaq	(%r9,%r9,4), %rax
	leaq	(%rdx,%rax,8), %r15
	movq	32(%r15), %rcx
	testq	%rcx, %rcx
	je	.L1119
	testq	%r10, %r10
	je	.L884
.L1122:
	movl	8(%r10), %eax
	cmpl	%eax, 8(%rcx)
	cmovg	%rcx, %r10
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L1119:
	movq	(%r15), %rdx
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	-192(%rbp), %rax
	cmpq	$0, (%rax)
	movq	%rax, -392(%rbp)
	je	.L1120
.L741:
	movq	%r9, %rax
	salq	$6, %rax
	movq	%rax, -416(%rbp)
	testl	%edi, %edi
	jle	.L742
	movl	%r11d, -384(%rbp)
	movl	$1, %r8d
	xorl	%r14d, %r14d
	movq	%r10, -424(%rbp)
	movq	%r9, -440(%rbp)
	movq	%r15, -432(%rbp)
	movq	%rcx, %r15
	movl	%r8d, %ecx
	movq	%r14, %r8
.L796:
	cmpl	-384(%rbp), %ecx
	je	.L743
	movq	-392(%rbp), %rax
	movl	%ecx, %edx
	movq	-80(%rbp), %rsi
	sarl	$5, %edx
	movq	(%rax), %rax
	movl	20(%rax), %eax
	andl	$16777215, %eax
	imull	-92(%rbp), %eax
	addl	%edx, %eax
	movq	-88(%rbp), %rdx
	cltq
	movl	(%rdx,%rax,4), %edx
	andl	(%rsi,%rax,4), %edx
	btl	%ecx, %edx
	jc	.L1121
.L743:
	addl	$1, %ecx
	addq	$1, %r8
	cmpl	%edi, %ecx
	jle	.L796
	movq	-104(%rbp), %rax
	movq	%r15, %rcx
	movq	-432(%rbp), %r15
	movq	-416(%rbp), %rbx
	movl	-384(%rbp), %r11d
	addq	48(%rax), %rbx
	movq	-424(%rbp), %r10
	movq	%rbx, 32(%r15)
	movq	-440(%rbp), %r9
	movq	%rbx, %rax
	movq	%rbx, -328(%rbp)
	movq	-104(%rbp), %rdi
	testq	%rcx, %rcx
	je	.L797
	movq	32(%rcx), %rsi
	cmpq	40(%rcx), %rsi
	je	.L798
	movq	%rbx, (%rsi)
	addq	$8, 32(%rcx)
.L799:
	movq	-328(%rbp), %rax
	movq	%rcx, (%rax)
	movl	8(%rcx), %eax
	movq	-328(%rbp), %rdx
	addl	$1, %eax
	movl	%eax, 8(%rdx)
.L800:
	movq	32(%r15), %rcx
	movl	-96(%rbp), %edi
	testq	%r10, %r10
	jne	.L1122
.L884:
	movq	%rcx, %r10
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L1121:
	movq	-160(%rbp), %rdx
	leaq	(%r8,%r8,4), %rax
	leaq	(%rdx,%rax,8), %r9
	movq	32(%r9), %r10
	testq	%r10, %r10
	je	.L1123
	testq	%r15, %r15
	je	.L883
.L1134:
	movl	8(%r15), %eax
	cmpl	%eax, 8(%r10)
	cmovg	%r10, %r15
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L692:
	movq	-216(%rbp), %rcx
	movq	-248(%rbp), %rsi
	subq	-232(%rbp), %rax
	movq	%rcx, %r15
	sarq	$3, %rax
	subq	%rsi, %r15
	movq	%r15, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	-256(%rbp), %rax
	subq	-272(%rbp), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L694
	movq	-288(%rbp), %r8
	movq	-280(%rbp), %rdx
	movq	%rcx, %rax
	subq	%r8, %rax
	movq	%rdx, %r11
	sarq	$3, %rax
	subq	%rax, %r11
	cmpq	$1, %r11
	jbe	.L1124
.L695:
	movq	-296(%rbp), %rax
	testq	%rax, %rax
	je	.L703
	cmpq	$63, 8(%rax)
	ja	.L1125
.L703:
	movq	-304(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1126
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L704:
	movq	%rax, 8(%rcx)
	movq	-240(%rbp), %rax
	movq	%rbx, (%rax)
	movq	-216(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -216(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -232(%rbp)
	movq	%rdx, -224(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L729:
	movq	-104(%rbp), %rdx
	movq	120(%rdx), %rax
	subq	112(%rdx), %rax
	sarq	$3, %rax
	movl	%eax, 60(%r12)
	.p2align 4,,10
	.p2align 3
.L732:
	cmpb	$0, _ZN2v88internal21FLAG_trace_turbo_loopE(%rip)
	jne	.L836
.L852:
	movq	-288(%rbp), %rax
	testq	%rax, %rax
	je	.L633
	movq	-216(%rbp), %rbx
	movq	-248(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L869
	movq	-296(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L872:
	testq	%rax, %rax
	je	.L870
	cmpq	$64, 8(%rax)
	ja	.L871
.L870:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-296(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -296(%rbp)
.L871:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L872
	movq	-288(%rbp), %rax
.L869:
	movq	-280(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L633
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L633:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1127
	movq	-344(%rbp), %rax
	addq	$616, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1115:
	.cfi_restore_state
	movq	%rcx, (%rax)
	movl	-96(%rbp), %edi
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L738:
	movq	%rax, %rbx
	movq	-104(%rbp), %rax
	addq	48(%rax), %rbx
	movq	%rbx, %rax
	movq	-368(%rbp), %rbx
	movq	%rax, 32(%rbx)
	movq	-104(%rbp), %rdi
	movq	%rax, -328(%rbp)
.L803:
	movq	24(%rdi), %rsi
	cmpq	32(%rdi), %rsi
	je	.L807
	movq	%rax, (%rsi)
	addq	$8, 24(%rdi)
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L1117:
	testq	%rax, %rax
	je	.L810
	movq	(%r9), %rcx
	movzwl	16(%rcx), %ecx
	cmpw	$16, %cx
	je	.L814
	movq	-104(%rbp), %rsi
	movq	80(%rsi), %rsi
	cmpl	(%rsi,%rdx,4), %edi
	je	.L1128
	movq	24(%rax), %rdx
	movq	%rdx, 8(%r14)
	movq	%r14, 24(%rax)
.L817:
	addq	$1, %r13
.L810:
	addq	$16, %r14
	cmpq	%r14, -384(%rbp)
	jne	.L818
.L809:
	movq	-104(%rbp), %rax
	movq	%r13, %rsi
	leaq	104(%rax), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE7reserveEm
	movq	-104(%rbp), %rcx
	movq	24(%rcx), %rax
	movq	16(%rcx), %r12
	movq	%rax, -368(%rbp)
	cmpq	%rax, %r12
	je	.L732
	.p2align 4,,10
	.p2align 3
.L835:
	movq	(%r12), %r14
	movq	%r14, %rax
	subq	48(%rcx), %rax
	sarq	$6, %rax
	leal	1(%rax), %r13d
	cltq
	leaq	(%rax,%rax,4), %rdx
	movq	-160(%rbp), %rax
	leaq	(%rax,%rdx,8), %rbx
	movq	120(%rcx), %rax
	subq	112(%rcx), %rax
	sarq	$3, %rax
	movl	%eax, 48(%r14)
	movq	8(%rbx), %r15
	testq	%r15, %r15
	jne	.L819
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L1129:
	movq	(%r15), %rdx
	movq	%rdx, (%rsi)
	addq	$8, 120(%rdi)
.L1098:
	movq	(%r15), %rdx
	movq	-104(%rbp), %rcx
	movl	20(%rdx), %edx
	movq	80(%rcx), %rcx
	andl	$16777215, %edx
	movl	%r13d, (%rcx,%rdx,4)
	movq	8(%r15), %r15
	testq	%r15, %r15
	je	.L823
.L819:
	movq	-104(%rbp), %rdi
	movq	120(%rdi), %rsi
	cmpq	128(%rdi), %rsi
	jne	.L1129
	addq	$104, %rdi
	movq	%r15, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L823:
	movq	-104(%rbp), %rdx
	movq	120(%rdx), %rax
	subq	112(%rdx), %rax
	sarq	$3, %rax
	movl	%eax, 52(%r14)
	movq	24(%rbx), %r15
	testq	%r15, %r15
	jne	.L821
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L1130:
	movq	(%r15), %rdx
	movq	%rdx, (%rsi)
	addq	$8, 120(%rdi)
.L1100:
	movq	(%r15), %rdx
	movq	-104(%rbp), %rcx
	movl	20(%rdx), %edx
	movq	80(%rcx), %rcx
	andl	$16777215, %edx
	movl	%r13d, (%rcx,%rdx,4)
	movq	8(%r15), %r15
	testq	%r15, %r15
	je	.L820
.L821:
	movq	-104(%rbp), %rdi
	movq	120(%rdi), %rsi
	cmpq	128(%rdi), %rsi
	jne	.L1130
	addq	$104, %rdi
	movq	%r15, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L820:
	movq	32(%r14), %r15
	movq	24(%r14), %rax
	cmpq	%r15, %rax
	je	.L826
.L831:
	movq	(%rax), %rsi
	movq	-352(%rbp), %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler14LoopFinderImpl13SerializeLoopEPNS1_8LoopTree4LoopE
	movq	-360(%rbp), %rax
	addq	$8, %rax
	cmpq	%rax, %r15
	jne	.L831
.L826:
	movq	-104(%rbp), %rdx
	movq	120(%rdx), %rax
	subq	112(%rdx), %rax
	sarq	$3, %rax
	movl	%eax, 56(%r14)
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L830
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L1131:
	movq	(%rbx), %rax
	movq	%rax, (%rsi)
	addq	$8, 120(%rdi)
.L1102:
	movq	(%rbx), %rax
	movq	-104(%rbp), %rdx
	movl	20(%rax), %eax
	movq	80(%rdx), %rdx
	andl	$16777215, %eax
	movl	%r13d, (%rdx,%rax,4)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L829
.L830:
	movq	-104(%rbp), %rdi
	movq	120(%rdi), %rsi
	cmpq	128(%rdi), %rsi
	jne	.L1131
	addq	$104, %rdi
	movq	%rbx, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L829:
	movq	-104(%rbp), %rdx
	addq	$8, %r12
	movq	120(%rdx), %rax
	subq	112(%rdx), %rax
	sarq	$3, %rax
	movl	%eax, 60(%r14)
	cmpq	%r12, -368(%rbp)
	je	.L732
	movq	-104(%rbp), %rcx
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L1128:
	movzwl	%cx, %edx
	cmpw	$1, %cx
	je	.L816
	subl	$35, %edx
	cmpl	$1, %edx
	jbe	.L816
	movq	16(%rax), %rdx
	movq	%rdx, 8(%r14)
	movq	%r14, 16(%rax)
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L816:
	movq	8(%rax), %rdx
	movq	%rdx, 8(%r14)
	movq	%r14, 8(%rax)
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L1123:
	movq	(%r9), %rdx
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	-192(%rbp), %rax
	cmpq	$0, (%rax)
	movq	%rax, %r13
	je	.L1132
.L745:
	movq	%r8, %rax
	salq	$6, %rax
	movq	%rax, -456(%rbp)
	testl	%edi, %edi
	jle	.L746
	xorl	%r11d, %r11d
	movl	$1, %r14d
	movq	%r15, -472(%rbp)
	movq	%r10, %r15
	movq	%r9, -480(%rbp)
	movl	%ecx, %r9d
	movq	%r13, %rcx
	movq	%r8, -488(%rbp)
	movq	%r11, %r8
	movl	%r14d, %r11d
.L790:
	cmpl	%r9d, %r11d
	je	.L747
	movq	(%rcx), %rax
	movl	%r11d, %edx
	movq	-80(%rbp), %rsi
	sarl	$5, %edx
	movl	20(%rax), %eax
	andl	$16777215, %eax
	imull	-92(%rbp), %eax
	addl	%edx, %eax
	movq	-88(%rbp), %rdx
	cltq
	movl	(%rdx,%rax,4), %edx
	andl	(%rsi,%rax,4), %edx
	btl	%r11d, %edx
	jc	.L1133
.L747:
	addl	$1, %r11d
	addq	$1, %r8
	cmpl	%edi, %r11d
	jle	.L790
	movq	-104(%rbp), %rax
	movl	%r9d, %ecx
	movq	-480(%rbp), %r9
	movq	%r15, %r10
	movq	-456(%rbp), %rbx
	movq	-472(%rbp), %r15
	addq	48(%rax), %rbx
	movq	-488(%rbp), %r8
	movq	%rbx, 32(%r9)
	movq	%rbx, %rax
	movq	-104(%rbp), %rdi
	movq	%rbx, -328(%rbp)
	testq	%r10, %r10
	je	.L791
	movq	32(%r10), %rsi
	cmpq	40(%r10), %rsi
	je	.L792
	movq	%rbx, (%rsi)
	addq	$8, 32(%r10)
.L793:
	movq	-328(%rbp), %rax
	movq	%r10, (%rax)
	movl	8(%r10), %eax
	movq	-328(%rbp), %rdx
	addl	$1, %eax
	movl	%eax, 8(%rdx)
.L794:
	movq	32(%r9), %r10
	movl	-96(%rbp), %edi
	testq	%r15, %r15
	jne	.L1134
.L883:
	movq	%r10, %r15
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L1133:
	movq	-160(%rbp), %rdx
	leaq	(%r8,%r8,4), %rax
	leaq	(%rdx,%rax,8), %rax
	movq	32(%rax), %r14
	movq	%rax, -464(%rbp)
	testq	%r14, %r14
	je	.L1135
	testq	%r15, %r15
	je	.L882
.L1143:
	movl	8(%r15), %eax
	cmpl	%eax, 8(%r14)
	cmovg	%r14, %r15
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L1105:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	movq	-280(%rbp), %rax
	movq	%rdx, -288(%rbp)
	leaq	-4(,%rax,4), %rbx
	movq	-296(%rbp), %rax
	andq	$-8, %rbx
	addq	%rdx, %rbx
	testq	%rax, %rax
	je	.L642
	cmpq	$63, 8(%rax)
	jbe	.L642
	movq	(%rax), %rdx
	movq	%rdx, -296(%rbp)
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L814:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1110:
	leaq	2(%rdi), %r9
	leaq	(%r9,%r9), %rax
	cmpq	%rax, %rdx
	ja	.L1136
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	-304(%rbp), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %rcx
	leaq	2(%rdx,%rax), %r10
	movq	24(%rdi), %rax
	leaq	0(,%r10,8), %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1137
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L668:
	movq	%r10, %rax
	movq	-248(%rbp), %rsi
	subq	%r9, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r8
	movq	-216(%rbp), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L669
	movq	%r8, %rdi
	subq	%rsi, %rdx
	movq	%rcx, -368(%rbp)
	movq	%r10, -360(%rbp)
	call	memmove@PLT
	movq	-368(%rbp), %rcx
	movq	-360(%rbp), %r10
	movq	%rax, %r8
.L669:
	movq	-280(%rbp), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L670
	movq	-288(%rbp), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L670:
	movq	%rcx, -288(%rbp)
	movq	%r10, -280(%rbp)
.L666:
	movq	%r8, -248(%rbp)
	movq	(%r8), %rax
	leaq	(%r8,%r13), %rcx
	movq	(%r8), %xmm0
	movq	%rcx, -216(%rbp)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -264(%rbp)
	movq	(%rcx), %rax
	movq	%rax, -232(%rbp)
	addq	$512, %rax
	movq	%rax, -224(%rbp)
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L1124:
	leaq	2(%rdi), %r9
	leaq	(%r9,%r9), %rax
	cmpq	%rax, %rdx
	ja	.L1138
	testq	%rdx, %rdx
	movq	%r12, %rax
	movq	-304(%rbp), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %rcx
	leaq	2(%rdx,%rax), %r10
	movq	24(%rdi), %rax
	leaq	0(,%r10,8), %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1139
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L700:
	movq	%r10, %rax
	movq	-248(%rbp), %rsi
	subq	%r9, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r8
	movq	-216(%rbp), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L701
	movq	%r8, %rdi
	subq	%rsi, %rdx
	movq	%rcx, -368(%rbp)
	movq	%r10, -360(%rbp)
	call	memmove@PLT
	movq	-368(%rbp), %rcx
	movq	-360(%rbp), %r10
	movq	%rax, %r8
.L701:
	movq	-280(%rbp), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L702
	movq	-288(%rbp), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L702:
	movq	%rcx, -288(%rbp)
	movq	%r10, -280(%rbp)
.L698:
	movq	%r8, -248(%rbp)
	movq	(%r8), %rax
	leaq	(%r8,%r15), %rcx
	movq	(%r8), %xmm0
	movq	%rcx, -216(%rbp)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -264(%rbp)
	movq	(%rcx), %rax
	movq	%rax, -232(%rbp)
	addq	$512, %rax
	movq	%rax, -224(%rbp)
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L1120:
	movq	%rdx, (%rax)
	movl	-96(%rbp), %edi
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L742:
	movq	%rax, %rbx
	movq	-104(%rbp), %rax
	addq	48(%rax), %rbx
	movq	%rbx, 32(%r15)
	movq	-104(%rbp), %rdi
	movq	%rbx, %rax
	movq	%rbx, -328(%rbp)
.L797:
	movq	24(%rdi), %rsi
	cmpq	32(%rdi), %rsi
	je	.L801
	movq	%rax, (%rsi)
	addq	$8, 24(%rdi)
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L1111:
	movq	(%rax), %rdx
	movq	%rdx, -296(%rbp)
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L804:
	movq	-448(%rbp), %rdx
	leaq	16(%r10), %rdi
	movq	%r11, -368(%rbp)
	movq	%r10, -360(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	movq	-368(%rbp), %r11
	movq	-360(%rbp), %r10
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L1125:
	movq	(%rax), %rdx
	movq	%rdx, -296(%rbp)
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L836:
	movq	-184(%rbp), %r13
	movq	-192(%rbp), %r12
	leaq	.LC15(%rip), %r14
	cmpq	%r13, %r12
	je	.L850
	.p2align 4,,10
	.p2align 3
.L849:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L842
	movl	-96(%rbp), %edx
	testl	%edx, %edx
	jle	.L843
	movl	$1, %ebx
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L844:
	testl	%esi, %esi
	jne	.L1140
	testl	%eax, %eax
	je	.L847
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L845:
	movq	(%r12), %rax
	addl	$1, %ebx
	cmpl	%ebx, -96(%rbp)
	jl	.L843
.L848:
	movl	20(%rax), %eax
	movl	%ebx, %edx
	movl	%ebx, %ecx
	sarl	$5, %edx
	andl	$16777215, %eax
	imull	-92(%rbp), %eax
	addl	%edx, %eax
	movq	-80(%rbp), %rdx
	cltq
	movl	(%rdx,%rax,4), %edx
	shrl	%cl, %edx
	movq	-88(%rbp), %rcx
	movl	%edx, %esi
	movl	(%rcx,%rax,4), %eax
	movl	%ebx, %ecx
	andl	$1, %esi
	shrl	%cl, %eax
	andl	$1, %eax
	testl	%eax, %edx
	je	.L844
	xorl	%eax, %eax
	leaq	.LC12(%rip), %rdi
	addl	$1, %ebx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rax
	cmpl	%ebx, -96(%rbp)
	jge	.L848
.L843:
	movq	(%rax), %rdx
	movl	20(%rax), %esi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	movq	8(%rdx), %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L842:
	addq	$16, %r12
	cmpq	%r12, %r13
	jne	.L849
.L850:
	movq	-152(%rbp), %r12
	movq	-160(%rbp), %rbx
	xorl	%r14d, %r14d
	leaq	.LC17(%rip), %r13
	cmpq	%r12, %rbx
	je	.L841
	.p2align 4,,10
	.p2align 3
.L853:
	movq	(%rbx), %rax
	movl	%r14d, %esi
	movq	%r13, %rdi
	addq	$40, %rbx
	addl	$1, %r14d
	movl	20(%rax), %edx
	xorl	%eax, %eax
	andl	$16777215, %edx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpq	%rbx, %r12
	jne	.L853
.L841:
	movq	-104(%rbp), %rax
	leaq	.LC0(%rip), %r15
	movq	24(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rcx, -360(%rbp)
	cmpq	%rcx, %rbx
	je	.L852
	.p2align 4,,10
	.p2align 3
.L867:
	movq	(%rbx), %r13
	movl	8(%r13), %esi
	testl	%esi, %esi
	jle	.L854
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L855:
	movq	%r15, %rdi
	xorl	%eax, %eax
	addl	$1, %r12d
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	8(%r13), %esi
	cmpl	%r12d, %esi
	jg	.L855
.L854:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movslq	48(%r13), %rcx
	movq	%rcx, %r12
	leaq	0(,%rcx,8), %r14
	cmpl	52(%r13), %ecx
	jge	.L859
	.p2align 4,,10
	.p2align 3
.L860:
	movq	-104(%rbp), %rax
	leaq	.LC2(%rip), %rdi
	addl	$1, %r12d
	movq	112(%rax), %rax
	movq	(%rax,%r14), %rax
	addq	$8, %r14
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpl	%r12d, 52(%r13)
	jg	.L860
.L859:
	cmpl	%r12d, 56(%r13)
	jle	.L857
	movslq	%r12d, %rcx
	leaq	0(,%rcx,8), %r14
	.p2align 4,,10
	.p2align 3
.L864:
	movq	-104(%rbp), %rax
	leaq	.LC3(%rip), %rdi
	addl	$1, %r12d
	movq	112(%rax), %rax
	movq	(%rax,%r14), %rax
	addq	$8, %r14
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpl	%r12d, 56(%r13)
	jg	.L864
.L857:
	movslq	%r12d, %rcx
	leaq	0(,%rcx,8), %r14
	cmpl	60(%r13), %r12d
	jge	.L865
	.p2align 4,,10
	.p2align 3
.L866:
	movq	-104(%rbp), %rax
	leaq	.LC4(%rip), %rdi
	addl	$1, %r12d
	movq	112(%rax), %rax
	movq	(%rax,%r14), %rax
	addq	$8, %r14
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	cmpl	%r12d, 60(%r13)
	jg	.L866
.L865:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	32(%r13), %r14
	movq	24(%r13), %r12
	cmpq	%r14, %r12
	je	.L863
.L868:
	movq	(%r12), %rsi
	movq	-352(%rbp), %rdi
	addq	$8, %r12
	call	_ZN2v88internal8compiler14LoopFinderImpl9PrintLoopEPNS1_8LoopTree4LoopE
	cmpq	%r12, %r14
	jne	.L868
.L863:
	addq	$8, %rbx
	cmpq	%rbx, -360(%rbp)
	jne	.L867
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L847:
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L1140:
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L1135:
	movq	(%rax), %rdx
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	-192(%rbp), %rax
	cmpq	$0, (%rax)
	movq	%rax, %r10
	je	.L1141
.L749:
	movq	%r8, %rax
	salq	$6, %rax
	movq	%rax, -496(%rbp)
	testl	%edi, %edi
	jle	.L750
	movq	%r15, -520(%rbp)
	xorl	%r12d, %r12d
	movl	$1, %r13d
	movl	%r9d, -512(%rbp)
	movq	%r12, %r9
	movq	%rcx, -528(%rbp)
	movq	%r10, %rcx
	movl	%r11d, %r10d
	movl	%r13d, %r11d
	movq	%r8, -536(%rbp)
	movl	%edi, %r8d
.L784:
	cmpl	%r10d, %r11d
	je	.L751
	movq	(%rcx), %rax
	movl	%r11d, %edx
	movq	-80(%rbp), %rsi
	sarl	$5, %edx
	movl	20(%rax), %eax
	andl	$16777215, %eax
	imull	-92(%rbp), %eax
	addl	%edx, %eax
	movq	-88(%rbp), %rdx
	cltq
	movl	(%rdx,%rax,4), %edx
	andl	(%rsi,%rax,4), %edx
	btl	%r11d, %edx
	jc	.L1142
.L751:
	addl	$1, %r11d
	addq	$1, %r9
	cmpl	%r8d, %r11d
	jle	.L784
	movq	-104(%rbp), %rax
	movq	-496(%rbp), %rbx
	movl	%r10d, %r11d
	movl	-512(%rbp), %r9d
	movq	-520(%rbp), %r15
	addq	48(%rax), %rbx
	movq	-528(%rbp), %rcx
	movq	%rbx, %rax
	movq	-464(%rbp), %rbx
	movq	-536(%rbp), %r8
	movq	%rax, 32(%rbx)
	movq	-104(%rbp), %rdx
	movq	%rax, -328(%rbp)
	testq	%r14, %r14
	je	.L785
	movq	32(%r14), %rsi
	cmpq	40(%r14), %rsi
	je	.L786
	movq	%rax, (%rsi)
	addq	$8, 32(%r14)
.L787:
	movq	-328(%rbp), %rax
	movq	%r14, (%rax)
	movl	8(%r14), %eax
	movq	-328(%rbp), %rdx
	addl	$1, %eax
	movl	%eax, 8(%rdx)
.L788:
	movq	-464(%rbp), %rax
	movl	-96(%rbp), %edi
	movq	32(%rax), %r14
	testq	%r15, %r15
	jne	.L1143
.L882:
	movq	%r14, %r15
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L1136:
	subq	%r9, %rdx
	addq	$8, %rcx
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r8
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	cmpq	%r8, %rsi
	jbe	.L665
	cmpq	%rcx, %rsi
	je	.L666
	movq	%r8, %rdi
	call	memmove@PLT
	movq	%rax, %r8
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L1138:
	subq	%r9, %rdx
	addq	$8, %rcx
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r8
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	cmpq	%r8, %rsi
	jbe	.L697
	cmpq	%rcx, %rsi
	je	.L698
	movq	%r8, %rdi
	call	memmove@PLT
	movq	%rax, %r8
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L1104:
	movq	%r15, %rdi
	movq	%rdx, -352(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-352(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L1142:
	movq	-160(%rbp), %rdx
	leaq	(%r9,%r9,4), %rax
	leaq	(%rdx,%rax,8), %rax
	movq	32(%rax), %r13
	movq	%rax, -504(%rbp)
	testq	%r13, %r13
	je	.L1144
.L752:
	testq	%r14, %r14
	je	.L881
	movl	8(%r14), %eax
	cmpl	%eax, 8(%r13)
	cmovg	%r13, %r14
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L746:
	movq	%rax, %rbx
	movq	-104(%rbp), %rax
	addq	48(%rax), %rbx
	movq	%rbx, 32(%r9)
	movq	-104(%rbp), %rdi
	movq	%rbx, %rax
	movq	%rbx, -328(%rbp)
.L791:
	movq	24(%rdi), %rsi
	cmpq	32(%rdi), %rsi
	je	.L795
	movq	%rax, (%rsi)
	addq	$8, 24(%rdi)
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L1132:
	movq	%rdx, (%rax)
	movl	-96(%rbp), %edi
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L807:
	movq	-448(%rbp), %rdx
	addq	$8, %rdi
	movq	%r11, -360(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	movq	-360(%rbp), %r11
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L798:
	movq	-448(%rbp), %rdx
	leaq	16(%rcx), %rdi
	movq	%r9, -424(%rbp)
	movq	%r10, -416(%rbp)
	movl	%r11d, -392(%rbp)
	movq	%rcx, -384(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	movq	-424(%rbp), %r9
	movq	-416(%rbp), %r10
	movl	-392(%rbp), %r11d
	movq	-384(%rbp), %rcx
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L648:
	movq	%r12, -168(%rbp)
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	%r12, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -112(%rbp)
.L652:
	xorl	%ebx, %ebx
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L1103:
	movl	$136, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, -344(%rbp)
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L1108:
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L665:
	cmpq	%rcx, %rsi
	je	.L666
	leaq	8(%r13), %rdi
	movq	%r8, -360(%rbp)
	subq	%rdx, %rdi
	addq	%r8, %rdi
	call	memmove@PLT
	movq	-360(%rbp), %r8
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L1114:
	movq	-104(%rbp), %rax
	movq	-160(%rbp), %rbx
	movq	48(%rax), %rdx
	movq	%rdx, 32(%rbx)
	movq	-104(%rbp), %rax
	movq	%rdx, -328(%rbp)
	movq	24(%rax), %rsi
	cmpq	32(%rax), %rsi
	je	.L710
	movq	%rdx, (%rsi)
	addq	$8, 24(%rax)
.L711:
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r9
	xorl	%r10d, %r10d
	cmpq	%r9, %rax
	jne	.L718
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L715:
	movq	24(%rbx), %rdx
	movq	%rdx, 8(%rax)
	movq	%rax, 24(%rbx)
.L717:
	addq	$1, %r10
.L713:
	addq	$16, %rax
	cmpq	%rax, %r9
	je	.L712
.L718:
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L713
	movl	20(%rsi), %edx
	movl	-92(%rbp), %ecx
	movq	-88(%rbp), %rdi
	movq	-80(%rbp), %r8
	andl	$16777215, %edx
	imull	%edx, %ecx
	movslq	%ecx, %rcx
	movl	(%rdi,%rcx,4), %edi
	andl	(%r8,%rcx,4), %edi
	movl	%edi, %ecx
	andl	$2, %ecx
	je	.L713
	movq	(%rsi), %rcx
	movzwl	16(%rcx), %ecx
	cmpw	$16, %cx
	je	.L814
	movq	-104(%rbp), %rsi
	movq	80(%rsi), %rsi
	cmpl	$1, (%rsi,%rdx,4)
	jne	.L715
	movzwl	%cx, %edx
	cmpw	$1, %cx
	je	.L716
	subl	$35, %edx
	cmpl	$1, %edx
	jbe	.L716
	movq	16(%rbx), %rdx
	movq	%rdx, 8(%rax)
	movq	%rax, 16(%rbx)
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L716:
	movq	8(%rbx), %rdx
	movq	%rdx, 8(%rax)
	movq	%rax, 8(%rbx)
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L643:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L1144:
	movq	(%rax), %rdx
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	-192(%rbp), %rax
	cmpq	$0, (%rax)
	movq	%rax, %rbx
	je	.L1145
.L753:
	movq	%r9, %rax
	salq	$6, %rax
	movq	%rax, -544(%rbp)
	testl	%r8d, %r8d
	jle	.L754
	movq	%r14, -560(%rbp)
	xorl	%r15d, %r15d
	movl	$1, %r12d
	movq	%rcx, -576(%rbp)
	movl	%r10d, -568(%rbp)
	movl	%r11d, %r10d
	movq	%r9, -584(%rbp)
	movq	%rbx, %r9
.L778:
	cmpl	%r10d, %r12d
	je	.L755
	movq	(%r9), %rax
	movl	%r12d, %edx
	movq	-80(%rbp), %rcx
	sarl	$5, %edx
	movl	20(%rax), %eax
	andl	$16777215, %eax
	imull	-92(%rbp), %eax
	addl	%edx, %eax
	movq	-88(%rbp), %rdx
	cltq
	movl	(%rdx,%rax,4), %edx
	andl	(%rcx,%rax,4), %edx
	btl	%r12d, %edx
	jc	.L1146
.L755:
	addl	$1, %r12d
	addq	$1, %r15
	cmpl	%r8d, %r12d
	jle	.L778
	movq	-104(%rbp), %rax
	movq	-544(%rbp), %rbx
	movl	%r10d, %r11d
	movq	-560(%rbp), %r14
	movl	-568(%rbp), %r10d
	addq	48(%rax), %rbx
	movq	-576(%rbp), %rcx
	movq	%rbx, %rax
	movq	-504(%rbp), %rbx
	movq	-584(%rbp), %r9
	movq	%rax, 32(%rbx)
	movq	-104(%rbp), %rdx
	movq	%rax, -328(%rbp)
	testq	%r13, %r13
	je	.L779
	movq	32(%r13), %rsi
	cmpq	40(%r13), %rsi
	je	.L780
	movq	%rax, (%rsi)
	addq	$8, 32(%r13)
.L781:
	movq	-328(%rbp), %rax
	movq	%r13, (%rax)
	movl	8(%r13), %eax
	movq	-328(%rbp), %rdx
	addl	$1, %eax
	movl	%eax, 8(%rdx)
.L782:
	movq	-504(%rbp), %rax
	movl	-96(%rbp), %r8d
	movq	32(%rax), %r13
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L697:
	cmpq	%rcx, %rsi
	je	.L698
	leaq	8(%r15), %rdi
	movq	%r8, -360(%rbp)
	subq	%rdx, %rdi
	addq	%r8, %rdi
	call	memmove@PLT
	movq	-360(%rbp), %r8
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L1112:
	movl	$512, %esi
	movq	%rcx, -360(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-360(%rbp), %rcx
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L1146:
	movq	-160(%rbp), %rdx
	leaq	(%r15,%r15,4), %rax
	leaq	(%rdx,%rax,8), %rax
	movq	32(%rax), %r14
	movq	%rax, -552(%rbp)
	testq	%r14, %r14
	je	.L1147
.L756:
	testq	%r13, %r13
	je	.L880
	movl	8(%r13), %eax
	cmpl	%eax, 8(%r14)
	cmovg	%r14, %r13
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L1126:
	movl	$512, %esi
	movq	%rcx, -360(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-360(%rbp), %rcx
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L1107:
	movq	%r12, %rdi
	movq	%rdx, -352(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-352(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L1147:
	movq	(%rax), %rdx
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	-192(%rbp), %rax
	cmpq	$0, (%rax)
	movq	%rax, %rcx
	je	.L1148
.L757:
	movq	%r15, %rax
	salq	$6, %rax
	movq	%rax, -592(%rbp)
	testl	%r8d, %r8d
	jle	.L758
	movl	%r10d, -400(%rbp)
	xorl	%r11d, %r11d
	movl	$1, %ebx
	movq	%r13, -600(%rbp)
	movq	%r11, %r13
	movq	%r15, %r11
	movl	%r12d, %r15d
	movq	%r9, -608(%rbp)
	movq	%r14, %r9
.L772:
	cmpl	%ebx, %r15d
	je	.L759
	movq	(%rcx), %rax
	movl	%ebx, %edx
	movq	-80(%rbp), %rsi
	sarl	$5, %edx
	movl	20(%rax), %eax
	andl	$16777215, %eax
	imull	-92(%rbp), %eax
	addl	%edx, %eax
	movq	-88(%rbp), %rdx
	cltq
	movl	(%rdx,%rax,4), %edx
	andl	(%rsi,%rax,4), %edx
	btl	%ebx, %edx
	jc	.L1149
.L759:
	addl	$1, %ebx
	addq	$1, %r13
	cmpl	%r8d, %ebx
	jle	.L772
	movq	-104(%rbp), %rax
	movq	-592(%rbp), %rbx
	movq	%r9, %r14
	movl	%r15d, %r12d
	movq	-600(%rbp), %r13
	movl	-400(%rbp), %r10d
	movq	%r11, %r15
	addq	48(%rax), %rbx
	movq	-608(%rbp), %r9
	movq	%rbx, %rax
	movq	-552(%rbp), %rbx
	movq	%rax, 32(%rbx)
	movq	-104(%rbp), %rdx
	movq	%rax, -328(%rbp)
	testq	%r14, %r14
	je	.L773
	movq	32(%r14), %rsi
	cmpq	40(%r14), %rsi
	je	.L774
	movq	%rax, (%rsi)
	addq	$8, 32(%r14)
.L775:
	movq	-328(%rbp), %rax
	movq	%r14, (%rax)
	movl	8(%r14), %eax
	movq	-328(%rbp), %rdx
	addl	$1, %eax
	movl	%eax, 8(%rdx)
.L776:
	movq	-552(%rbp), %rax
	movl	-96(%rbp), %r8d
	movq	32(%rax), %r14
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L1145:
	movq	%rdx, (%rax)
	movl	-96(%rbp), %r8d
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L880:
	movq	%r14, %r13
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L754:
	movq	%rax, %rbx
	movq	-104(%rbp), %rax
	addq	48(%rax), %rbx
	movq	%rbx, %rax
	movq	-504(%rbp), %rbx
	movq	%rax, 32(%rbx)
	movq	-104(%rbp), %rdx
	movq	%rax, -328(%rbp)
.L779:
	movq	24(%rdx), %rsi
	cmpq	32(%rdx), %rsi
	je	.L783
	movq	%rax, (%rsi)
	addq	$8, 24(%rdx)
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L1141:
	movq	%rdx, (%rax)
	movl	-96(%rbp), %edi
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L750:
	movq	%rax, %rbx
	movq	-104(%rbp), %rax
	addq	48(%rax), %rbx
	movq	%rbx, %rax
	movq	-464(%rbp), %rbx
	movq	%rax, 32(%rbx)
	movq	-104(%rbp), %rdx
	movq	%rax, -328(%rbp)
.L785:
	movq	24(%rdx), %rsi
	cmpq	32(%rdx), %rsi
	je	.L789
	movq	%rax, (%rsi)
	addq	$8, 24(%rdx)
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L801:
	movq	-448(%rbp), %rdx
	addq	$8, %rdi
	movq	%r9, -416(%rbp)
	movq	%r10, -392(%rbp)
	movl	%r11d, -384(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	movq	-416(%rbp), %r9
	movq	-392(%rbp), %r10
	movl	-384(%rbp), %r11d
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L792:
	movq	-448(%rbp), %rdx
	leaq	16(%r10), %rdi
	movq	%r8, -480(%rbp)
	movl	%ecx, -472(%rbp)
	movq	%r9, -464(%rbp)
	movq	%r10, -456(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	movq	-480(%rbp), %r8
	movl	-472(%rbp), %ecx
	movq	-464(%rbp), %r9
	movq	-456(%rbp), %r10
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L881:
	movq	%r13, %r14
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L712:
	movq	-104(%rbp), %rax
	movq	%r10, %rsi
	leaq	104(%rax), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE7reserveEm
	movq	-104(%rbp), %rdx
	movq	32(%rbx), %r12
	movq	%r12, %rax
	subq	48(%rdx), %rax
	sarq	$6, %rax
	leal	1(%rax), %r14d
	cltq
	leaq	(%rax,%rax,4), %rcx
	movq	-160(%rbp), %rax
	leaq	(%rax,%rcx,8), %r13
	movq	120(%rdx), %rax
	subq	112(%rdx), %rax
	sarq	$3, %rax
	movl	%eax, 48(%r12)
	movq	8(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L719
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L1150:
	movq	(%rbx), %rax
	movq	%rax, (%rsi)
	addq	$8, 120(%rdi)
.L1092:
	movq	(%rbx), %rax
	movq	-104(%rbp), %rdx
	movl	20(%rax), %eax
	movq	80(%rdx), %rdx
	andl	$16777215, %eax
	movl	%r14d, (%rdx,%rax,4)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L723
.L719:
	movq	-104(%rbp), %rdi
	movq	120(%rdi), %rsi
	cmpq	128(%rdi), %rsi
	jne	.L1150
	addq	$104, %rdi
	movq	%rbx, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1092
	.p2align 4,,10
	.p2align 3
.L723:
	movq	-104(%rbp), %rdx
	movq	120(%rdx), %rax
	subq	112(%rdx), %rax
	sarq	$3, %rax
	movl	%eax, 52(%r12)
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L721
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L1151:
	movq	(%rbx), %rax
	movq	%rax, (%rsi)
	addq	$8, 120(%rdi)
.L1094:
	movq	(%rbx), %rax
	movq	-104(%rbp), %rdx
	movl	20(%rax), %eax
	movq	80(%rdx), %rdx
	andl	$16777215, %eax
	movl	%r14d, (%rdx,%rax,4)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L720
.L721:
	movq	-104(%rbp), %rdi
	movq	120(%rdi), %rsi
	cmpq	128(%rdi), %rsi
	jne	.L1151
	addq	$104, %rdi
	movq	%rbx, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L720:
	movq	32(%r12), %rbx
	movq	24(%r12), %r15
	cmpq	%rbx, %r15
	je	.L726
	movq	%r12, -360(%rbp)
	movq	%r15, %r12
	movq	-352(%rbp), %r15
.L731:
	movq	(%r12), %rsi
	movq	%r15, %rdi
	addq	$8, %r12
	call	_ZN2v88internal8compiler14LoopFinderImpl13SerializeLoopEPNS1_8LoopTree4LoopE
	cmpq	%r12, %rbx
	jne	.L731
	movq	-360(%rbp), %r12
.L726:
	movq	-104(%rbp), %rdx
	movq	120(%rdx), %rax
	subq	112(%rdx), %rax
	sarq	$3, %rax
	movl	%eax, 56(%r12)
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L730
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L1152:
	movq	(%rbx), %rax
	movq	%rax, (%rsi)
	addq	$8, 120(%rdi)
.L1096:
	movq	(%rbx), %rax
	movq	-104(%rbp), %rdx
	movl	20(%rax), %eax
	movq	80(%rdx), %rdx
	andl	$16777215, %eax
	movl	%r14d, (%rdx,%rax,4)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L729
.L730:
	movq	-104(%rbp), %rdi
	movq	120(%rdi), %rsi
	cmpq	128(%rdi), %rsi
	jne	.L1152
	addq	$104, %rdi
	movq	%rbx, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L885:
	xorl	%r13d, %r13d
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L1149:
	movq	-160(%rbp), %rdx
	leaq	0(%r13,%r13,4), %rax
	leaq	(%rdx,%rax,8), %r10
	movq	32(%r10), %r14
	testq	%r14, %r14
	je	.L1153
.L760:
	testq	%r9, %r9
	je	.L879
	movl	8(%r9), %eax
	cmpl	%eax, 8(%r14)
	cmovg	%r14, %r9
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L795:
	movq	-448(%rbp), %rdx
	addq	$8, %rdi
	movq	%r8, -472(%rbp)
	movl	%ecx, -464(%rbp)
	movq	%r9, -456(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	movq	-472(%rbp), %r8
	movl	-464(%rbp), %ecx
	movq	-456(%rbp), %r9
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L786:
	movq	-448(%rbp), %rdx
	leaq	16(%r14), %rdi
	movq	%r8, -520(%rbp)
	movq	%rcx, -512(%rbp)
	movl	%r9d, -504(%rbp)
	movl	%r10d, -496(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	movq	-520(%rbp), %r8
	movq	-512(%rbp), %rcx
	movl	-504(%rbp), %r9d
	movl	-496(%rbp), %r11d
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L710:
	leaq	-328(%rbp), %rdx
	leaq	8(%rax), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L1153:
	movq	(%r10), %rax
	movl	20(%rax), %edx
	andl	$16777215, %edx
	salq	$4, %rdx
	addq	-192(%rbp), %rdx
	cmpq	$0, (%rdx)
	je	.L1154
.L761:
	movq	%r13, %rax
	salq	$6, %rax
	movq	%rax, -616(%rbp)
	testl	%r8d, %r8d
	jle	.L762
	movq	%r9, -632(%rbp)
	movl	$1, %r12d
	movq	%rcx, -640(%rbp)
	movq	%r10, -648(%rbp)
	movq	%r11, -656(%rbp)
	movl	%ebx, %r11d
	movq	%r14, %rbx
	movq	%r13, %r14
	movl	%r12d, %r13d
	movq	%rdx, %r12
.L766:
	cmpl	%r13d, %r11d
	je	.L763
	movq	(%r12), %rax
	movl	%r13d, %esi
	movq	-80(%rbp), %rdi
	sarl	$5, %esi
	movl	20(%rax), %eax
	andl	$16777215, %eax
	imull	-92(%rbp), %eax
	addl	%esi, %eax
	movq	-88(%rbp), %rsi
	cltq
	movl	(%rsi,%rax,4), %esi
	andl	(%rdi,%rax,4), %esi
	btl	%r13d, %esi
	jc	.L1155
.L763:
	addl	$1, %r13d
	cmpl	%r8d, %r13d
	jle	.L766
	movq	-104(%rbp), %rax
	movq	-648(%rbp), %r10
	movq	%r14, %r13
	movq	%rbx, %r14
	movq	-616(%rbp), %rsi
	movl	%r11d, %ebx
	movq	-632(%rbp), %r9
	addq	48(%rax), %rsi
	movq	-640(%rbp), %rcx
	movq	%rsi, 32(%r10)
	movq	-656(%rbp), %r11
	movq	%rsi, %rax
	movq	%rsi, -328(%rbp)
	movq	-104(%rbp), %rdx
	testq	%r14, %r14
	je	.L767
	movq	32(%r14), %rsi
	cmpq	40(%r14), %rsi
	je	.L768
	movq	%rax, (%rsi)
	addq	$8, 32(%r14)
.L769:
	movq	-328(%rbp), %rax
	movq	%r14, (%rax)
	movl	8(%r14), %eax
	movq	-328(%rbp), %rdx
	addl	$1, %eax
	movl	%eax, 8(%rdx)
.L770:
	movq	32(%r10), %r14
	movl	-96(%rbp), %r8d
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L1155:
	movq	-352(%rbp), %rdi
	movl	%r13d, %esi
	movl	%r11d, -624(%rbp)
	call	_ZN2v88internal8compiler14LoopFinderImpl15ConnectLoopTreeEi
	testq	%rbx, %rbx
	movl	-96(%rbp), %r8d
	movl	-624(%rbp), %r11d
	je	.L765
	movl	8(%rbx), %ecx
	cmpl	%ecx, 8(%rax)
	jle	.L763
.L765:
	movq	%rax, %rbx
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L1137:
	movq	%r10, -368(%rbp)
	movq	%r9, -360(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-360(%rbp), %r9
	movq	-368(%rbp), %r10
	movq	%rax, %rcx
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L1139:
	movq	%r10, -368(%rbp)
	movq	%r9, -360(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-360(%rbp), %r9
	movq	-368(%rbp), %r10
	movq	%rax, %rcx
	jmp	.L700
.L1148:
	movq	%rdx, (%rax)
	movl	-96(%rbp), %r8d
	jmp	.L757
.L758:
	movq	%rax, %rbx
	movq	-104(%rbp), %rax
	addq	48(%rax), %rbx
	movq	%rbx, %rax
	movq	-552(%rbp), %rbx
	movq	%rax, 32(%rbx)
	movq	-104(%rbp), %rdx
	movq	%rax, -328(%rbp)
.L773:
	movq	24(%rdx), %rsi
	cmpq	32(%rdx), %rsi
	je	.L777
	movq	%rax, (%rsi)
	addq	$8, 24(%rdx)
	jmp	.L776
.L789:
	leaq	8(%rdx), %rdi
	movq	-448(%rbp), %rdx
	movq	%r8, -520(%rbp)
	movq	%rcx, -512(%rbp)
	movl	%r9d, -504(%rbp)
	movl	%r11d, -496(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	movq	-520(%rbp), %r8
	movq	-512(%rbp), %rcx
	movl	-504(%rbp), %r9d
	movl	-496(%rbp), %r11d
	jmp	.L788
.L780:
	movq	-448(%rbp), %rdx
	leaq	16(%r13), %rdi
	movq	%r9, -568(%rbp)
	movq	%rcx, -560(%rbp)
	movl	%r10d, -552(%rbp)
	movl	%r11d, -544(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	movq	-568(%rbp), %r9
	movq	-560(%rbp), %rcx
	movl	-552(%rbp), %r10d
	movl	-544(%rbp), %r11d
	jmp	.L781
.L879:
	movq	%r14, %r9
	jmp	.L759
.L1154:
	movq	%rax, (%rdx)
	movl	-96(%rbp), %r8d
	jmp	.L761
.L762:
	movq	%rax, %rsi
	movq	-104(%rbp), %rax
	addq	48(%rax), %rsi
	movq	%rsi, 32(%r10)
	movq	-104(%rbp), %rdx
	movq	%rsi, %rax
	movq	%rsi, -328(%rbp)
.L767:
	movq	24(%rdx), %rsi
	cmpq	32(%rdx), %rsi
	je	.L771
	movq	%rax, (%rsi)
	addq	$8, 24(%rdx)
	jmp	.L770
.L774:
	movq	-448(%rbp), %rdx
	leaq	16(%r14), %rdi
	movq	%r9, -600(%rbp)
	movl	%r10d, -592(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	movq	-600(%rbp), %r9
	movl	-592(%rbp), %r10d
	jmp	.L775
.L783:
	leaq	8(%rdx), %rdi
	movq	-448(%rbp), %rdx
	movq	%r9, -568(%rbp)
	movq	%rcx, -560(%rbp)
	movl	%r10d, -552(%rbp)
	movl	%r11d, -544(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	movq	-568(%rbp), %r9
	movq	-560(%rbp), %rcx
	movl	-552(%rbp), %r10d
	movl	-544(%rbp), %r11d
	jmp	.L782
.L768:
	movq	-448(%rbp), %rdx
	leaq	16(%r14), %rdi
	movq	%r11, -640(%rbp)
	movq	%rcx, -632(%rbp)
	movq	%r9, -624(%rbp)
	movq	%r10, -616(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	movq	-640(%rbp), %r11
	movq	-632(%rbp), %rcx
	movq	-624(%rbp), %r9
	movq	-616(%rbp), %r10
	jmp	.L769
.L777:
	leaq	8(%rdx), %rdi
	movq	-448(%rbp), %rdx
	movq	%r9, -600(%rbp)
	movl	%r10d, -592(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	movq	-600(%rbp), %r9
	movl	-592(%rbp), %r10d
	jmp	.L776
.L647:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L694:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L771:
	leaq	8(%rdx), %rdi
	movq	-448(%rbp), %rdx
	movq	%r11, -640(%rbp)
	movq	%rcx, -632(%rbp)
	movq	%r9, -624(%rbp)
	movq	%r10, -616(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler8LoopTree4LoopENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	movq	-640(%rbp), %r11
	movq	-632(%rbp), %rcx
	movq	-624(%rbp), %r9
	movq	-616(%rbp), %r10
	jmp	.L770
.L1127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10279:
	.size	_ZN2v88internal8compiler10LoopFinder13BuildLoopTreeEPNS1_5GraphEPNS0_11TickCounterEPNS0_4ZoneE, .-_ZN2v88internal8compiler10LoopFinder13BuildLoopTreeEPNS1_5GraphEPNS0_11TickCounterEPNS0_4ZoneE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler10LoopFinder13BuildLoopTreeEPNS1_5GraphEPNS0_11TickCounterEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler10LoopFinder13BuildLoopTreeEPNS1_5GraphEPNS0_11TickCounterEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler10LoopFinder13BuildLoopTreeEPNS1_5GraphEPNS0_11TickCounterEPNS0_4ZoneE:
.LFB12445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12445:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler10LoopFinder13BuildLoopTreeEPNS1_5GraphEPNS0_11TickCounterEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler10LoopFinder13BuildLoopTreeEPNS1_5GraphEPNS0_11TickCounterEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler10LoopFinder13BuildLoopTreeEPNS1_5GraphEPNS0_11TickCounterEPNS0_4ZoneE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
