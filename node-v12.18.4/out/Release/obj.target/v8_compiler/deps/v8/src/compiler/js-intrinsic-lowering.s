	.file	"js-intrinsic-lowering.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler19JSIntrinsicLowering12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"JSIntrinsicLowering"
	.section	.text._ZNK2v88internal8compiler19JSIntrinsicLowering12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler19JSIntrinsicLowering12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler19JSIntrinsicLowering12reducer_nameEv
	.type	_ZNK2v88internal8compiler19JSIntrinsicLowering12reducer_nameEv, @function
_ZNK2v88internal8compiler19JSIntrinsicLowering12reducer_nameEv:
.LFB10466:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10466:
	.size	_ZNK2v88internal8compiler19JSIntrinsicLowering12reducer_nameEv, .-_ZNK2v88internal8compiler19JSIntrinsicLowering12reducer_nameEv
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB10610:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10610:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLoweringD2Ev,"axG",@progbits,_ZN2v88internal8compiler19JSIntrinsicLoweringD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19JSIntrinsicLoweringD2Ev
	.type	_ZN2v88internal8compiler19JSIntrinsicLoweringD2Ev, @function
_ZN2v88internal8compiler19JSIntrinsicLoweringD2Ev:
.LFB27284:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE27284:
	.size	_ZN2v88internal8compiler19JSIntrinsicLoweringD2Ev, .-_ZN2v88internal8compiler19JSIntrinsicLoweringD2Ev
	.weak	_ZN2v88internal8compiler19JSIntrinsicLoweringD1Ev
	.set	_ZN2v88internal8compiler19JSIntrinsicLoweringD1Ev,_ZN2v88internal8compiler19JSIntrinsicLoweringD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB10612:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10612:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLoweringD0Ev,"axG",@progbits,_ZN2v88internal8compiler19JSIntrinsicLoweringD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19JSIntrinsicLoweringD0Ev
	.type	_ZN2v88internal8compiler19JSIntrinsicLoweringD0Ev, @function
_ZN2v88internal8compiler19JSIntrinsicLoweringD0Ev:
.LFB27286:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27286:
	.size	_ZN2v88internal8compiler19JSIntrinsicLoweringD0Ev, .-_ZN2v88internal8compiler19JSIntrinsicLoweringD0Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB10604:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE10604:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB10603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10603:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.type	_ZN2v88internal8compiler19JSIntrinsicLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, @function
_ZN2v88internal8compiler19JSIntrinsicLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE:
.LFB23342:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler19JSIntrinsicLoweringE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rdx, 16(%rdi)
	movq	%rcx, 24(%rdi)
	ret
	.cfi_endproc
.LFE23342:
	.size	_ZN2v88internal8compiler19JSIntrinsicLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, .-_ZN2v88internal8compiler19JSIntrinsicLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.globl	_ZN2v88internal8compiler19JSIntrinsicLoweringC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.set	_ZN2v88internal8compiler19JSIntrinsicLoweringC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE,_ZN2v88internal8compiler19JSIntrinsicLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering24ReduceCopyDataPropertiesEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering24ReduceCopyDataPropertiesEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering24ReduceCopyDataPropertiesEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering24ReduceCopyDataPropertiesEPNS1_4NodeE:
.LFB23345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$161, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r12), %rdx
	xorl	%r9d, %r9d
	leaq	-80(%rbp), %rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	movzbl	18(%rdx), %r8d
	xorl	%edx, %edx
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L14
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23345:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering24ReduceCopyDataPropertiesEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering24ReduceCopyDataPropertiesEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering28ReduceCreateIterResultObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering28ReduceCreateIterResultObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering28ReduceCreateIterResultObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering28ReduceCreateIterResultObjectEPNS1_4NodeE:
.LFB23349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$40, %rsp
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder22CreateIterResultObjectEv@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, -56(%rbp)
	movq	%r12, %rdx
	movq	%r12, %rsi
	leaq	32(%r12), %rbx
	movq	(%rdi), %rax
	call	*32(%rax)
	movzbl	23(%r12), %eax
	movq	-64(%rbp), %r9
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L16
	movq	32(%r12), %rdi
	movq	%rbx, %rax
	movq	%r12, %rsi
	cmpq	%rdi, %r9
	je	.L18
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L20
.L63:
	movq	%rax, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rsi
.L20:
	movq	%r9, (%rax)
	testq	%r9, %r9
	je	.L21
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L21:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L62
.L18:
	movq	8(%rbx), %rdi
	cmpq	%rdi, %r15
	je	.L22
	leaq	8(%rbx), %rax
	movq	%r12, %rsi
.L23:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L25
	movq	%rax, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %rsi
.L25:
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L26
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L26:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L22
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L24:
	movq	16(%rax), %rdi
	cmpq	%rdi, %r14
	je	.L29
	addq	$16, %rax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L22:
	movq	16(%rbx), %rdi
	cmpq	%rdi, %r14
	je	.L27
	leaq	16(%rbx), %rax
	movq	%r12, %rsi
.L28:
	leaq	-72(%rsi), %r15
	testq	%rdi, %rdi
	je	.L30
	movq	%r15, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %rax
.L30:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L31
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L31:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L27
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L29:
	movq	24(%rax), %rdi
	cmpq	%rdi, %r13
	je	.L34
	leaq	24(%rax), %r15
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L27:
	movq	24(%rbx), %rdi
	cmpq	%rdi, %r13
	je	.L34
	leaq	24(%rbx), %r15
	movq	%r12, %rsi
.L33:
	leaq	-96(%rsi), %r14
	testq	%rdi, %rdi
	je	.L35
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L35:
	movq	%r13, (%r15)
	testq	%r13, %r13
	je	.L34
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L34:
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L19:
	movq	8(%rax), %rdi
	cmpq	%rdi, %r15
	je	.L24
	addq	$8, %rax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L16:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rax
	cmpq	%rdi, %r9
	je	.L19
	subq	$24, %rsi
	testq	%rdi, %rdi
	jne	.L63
	jmp	.L20
	.cfi_endproc
.LFE23349:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering28ReduceCreateIterResultObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering28ReduceCreateIterResultObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering19ReduceDeoptimizeNowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering19ReduceDeoptimizeNowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering19ReduceDeoptimizeNowEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering19ReduceDeoptimizeNowEPNS1_4NodeE:
.LFB23350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	leaq	-80(%rbp), %rcx
	movl	$4, %edx
	movq	%rax, %r13
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10DeoptimizeENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %rcx
	movq	%r14, %rdi
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r13, -48(%rbp)
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rdx
	movq	16(%rbx), %rax
	movq	8(%rax), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$64, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L67:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23350:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering19ReduceDeoptimizeNowEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering19ReduceDeoptimizeNowEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering29ReduceCreateJSGeneratorObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering29ReduceCreateJSGeneratorObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering29ReduceCreateJSGeneratorObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering29ReduceCreateJSGeneratorObjectEPNS1_4NodeE:
.LFB23351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder21CreateGeneratorObjectEv@PLT
	movq	-88(%rbp), %xmm0
	xorl	%r8d, %r8d
	leaq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movl	$5, %edx
	movhps	-96(%rbp), %xmm0
	movq	(%rax), %rdi
	movaps	%xmm0, -80(%rbp)
	movq	-104(%rbp), %xmm0
	movq	%r13, -48(%rbp)
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%rax, %r13
	movq	(%rdi), %rax
	movq	%r13, %rcx
	movq	%r13, %rdx
	call	*32(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L71:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23351:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering29ReduceCreateJSGeneratorObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering29ReduceCreateJSGeneratorObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering20ReduceGeneratorCloseEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering20ReduceGeneratorCloseEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering20ReduceGeneratorCloseEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering20ReduceGeneratorCloseEPNS1_4NodeE:
.LFB23352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movsd	.LC2(%rip), %xmm0
	movq	16(%rbx), %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	leaq	-112(%rbp), %rsi
	movq	%rax, -136(%rbp)
	movq	16(%rbx), %rax
	movq	%rsi, %rdi
	movq	%rsi, -120(%rbp)
	movq	376(%rax), %r8
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder32ForJSGeneratorObjectContinuationEv@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, -120(%rbp)
	movq	-136(%rbp), %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r12, %rdx
	movq	$0, 8(%r12)
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	leaq	32(%r12), %rbx
	movq	(%rdi), %rax
	call	*32(%rax)
	movzbl	23(%r12), %eax
	movq	-144(%rbp), %r9
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L73
	movq	32(%r12), %rdi
	movq	%rbx, %rax
	movq	%r12, %rsi
	cmpq	%rdi, %r9
	je	.L75
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L77
.L122:
	movq	%rax, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-144(%rbp), %rax
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %rsi
.L77:
	movq	%r9, (%rax)
	testq	%r9, %r9
	je	.L78
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L78:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L120
.L75:
	movq	8(%rbx), %rdi
	cmpq	%rdi, %r15
	je	.L79
	leaq	8(%rbx), %rax
	movq	%r12, %rsi
.L80:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L82
	movq	%rax, -136(%rbp)
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rsi
.L82:
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L83
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L83:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L79
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L81:
	movq	16(%rax), %rdi
	cmpq	%rdi, %r14
	je	.L86
	addq	$16, %rax
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L79:
	movq	16(%rbx), %rdi
	cmpq	%rdi, %r14
	je	.L84
	leaq	16(%rbx), %rax
	movq	%r12, %rsi
.L85:
	leaq	-72(%rsi), %r15
	testq	%rdi, %rdi
	je	.L87
	movq	%r15, %rsi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-128(%rbp), %rax
.L87:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L88
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L88:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L84
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L86:
	movq	24(%rax), %rdi
	cmpq	%rdi, %r13
	je	.L91
	leaq	24(%rax), %r15
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L84:
	movq	24(%rbx), %rdi
	cmpq	%rdi, %r13
	je	.L91
	leaq	24(%rbx), %r15
	movq	%r12, %rsi
.L90:
	leaq	-96(%rsi), %r14
	testq	%rdi, %rdi
	je	.L92
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L92:
	movq	%r13, (%r15)
	testq	%r13, %r13
	je	.L91
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L91:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	-120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L76:
	movq	8(%rax), %rdi
	cmpq	%rdi, %r15
	je	.L81
	addq	$8, %rax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L73:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rax
	cmpq	%rdi, %r9
	je	.L76
	subq	$24, %rsi
	testq	%rdi, %rdi
	jne	.L122
	jmp	.L77
.L121:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23352:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering20ReduceGeneratorCloseEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering20ReduceGeneratorCloseEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering30ReduceAsyncFunctionAwaitCaughtEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering30ReduceAsyncFunctionAwaitCaughtEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering30ReduceAsyncFunctionAwaitCaughtEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering30ReduceAsyncFunctionAwaitCaughtEPNS1_4NodeE:
.LFB23353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$230, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r12), %rdx
	xorl	%r9d, %r9d
	leaq	-80(%rbp), %rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	movzbl	18(%rdx), %r8d
	xorl	%edx, %edx
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L126:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23353:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering30ReduceAsyncFunctionAwaitCaughtEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering30ReduceAsyncFunctionAwaitCaughtEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering32ReduceAsyncFunctionAwaitUncaughtEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering32ReduceAsyncFunctionAwaitUncaughtEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering32ReduceAsyncFunctionAwaitUncaughtEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering32ReduceAsyncFunctionAwaitUncaughtEPNS1_4NodeE:
.LFB23354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$231, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r12), %rdx
	xorl	%r9d, %r9d
	leaq	-80(%rbp), %rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	movzbl	18(%rdx), %r8d
	xorl	%edx, %edx
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L130
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L130:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23354:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering32ReduceAsyncFunctionAwaitUncaughtEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering32ReduceAsyncFunctionAwaitUncaughtEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering24ReduceAsyncFunctionEnterEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering24ReduceAsyncFunctionEnterEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering24ReduceAsyncFunctionEnterEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering24ReduceAsyncFunctionEnterEPNS1_4NodeE:
.LFB23355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder18AsyncFunctionEnterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23355:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering24ReduceAsyncFunctionEnterEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering24ReduceAsyncFunctionEnterEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering25ReduceAsyncFunctionRejectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering25ReduceAsyncFunctionRejectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering25ReduceAsyncFunctionRejectEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering25ReduceAsyncFunctionRejectEPNS1_4NodeE:
.LFB23356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsi, %rdx
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder19AsyncFunctionRejectEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23356:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering25ReduceAsyncFunctionRejectEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering25ReduceAsyncFunctionRejectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering26ReduceAsyncFunctionResolveEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceAsyncFunctionResolveEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceAsyncFunctionResolveEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceAsyncFunctionResolveEPNS1_4NodeE:
.LFB23357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsi, %rdx
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder20AsyncFunctionResolveEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23357:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceAsyncFunctionResolveEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceAsyncFunctionResolveEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering31ReduceAsyncGeneratorAwaitCaughtEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering31ReduceAsyncGeneratorAwaitCaughtEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering31ReduceAsyncGeneratorAwaitCaughtEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering31ReduceAsyncGeneratorAwaitCaughtEPNS1_4NodeE:
.LFB23358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$683, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r12), %rdx
	xorl	%r9d, %r9d
	leaq	-80(%rbp), %rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	movzbl	18(%rdx), %r8d
	xorl	%edx, %edx
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L140
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L140:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23358:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering31ReduceAsyncGeneratorAwaitCaughtEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering31ReduceAsyncGeneratorAwaitCaughtEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering33ReduceAsyncGeneratorAwaitUncaughtEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering33ReduceAsyncGeneratorAwaitUncaughtEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering33ReduceAsyncGeneratorAwaitUncaughtEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering33ReduceAsyncGeneratorAwaitUncaughtEPNS1_4NodeE:
.LFB23359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$684, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r12), %rdx
	xorl	%r9d, %r9d
	leaq	-80(%rbp), %rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	movzbl	18(%rdx), %r8d
	xorl	%edx, %edx
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L144
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L144:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23359:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering33ReduceAsyncGeneratorAwaitUncaughtEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering33ReduceAsyncGeneratorAwaitUncaughtEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering26ReduceAsyncGeneratorRejectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceAsyncGeneratorRejectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceAsyncGeneratorRejectEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceAsyncGeneratorRejectEPNS1_4NodeE:
.LFB23360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$675, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r12), %rdx
	xorl	%r9d, %r9d
	leaq	-80(%rbp), %rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	movzbl	18(%rdx), %r8d
	xorl	%edx, %edx
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L148
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L148:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23360:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceAsyncGeneratorRejectEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceAsyncGeneratorRejectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering27ReduceAsyncGeneratorResolveEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering27ReduceAsyncGeneratorResolveEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering27ReduceAsyncGeneratorResolveEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering27ReduceAsyncGeneratorResolveEPNS1_4NodeE:
.LFB23361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$674, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r12), %rdx
	xorl	%r9d, %r9d
	leaq	-80(%rbp), %rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	movzbl	18(%rdx), %r8d
	xorl	%edx, %edx
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L152
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L152:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23361:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering27ReduceAsyncGeneratorResolveEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering27ReduceAsyncGeneratorResolveEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering25ReduceAsyncGeneratorYieldEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering25ReduceAsyncGeneratorYieldEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering25ReduceAsyncGeneratorYieldEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering25ReduceAsyncGeneratorYieldEPNS1_4NodeE:
.LFB23362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$676, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r12), %rdx
	xorl	%r9d, %r9d
	leaq	-80(%rbp), %rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	movzbl	18(%rdx), %r8d
	xorl	%edx, %edx
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L156
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L156:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23362:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering25ReduceAsyncGeneratorYieldEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering25ReduceAsyncGeneratorYieldEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering28ReduceGeneratorGetResumeModeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering28ReduceGeneratorGetResumeModeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering28ReduceGeneratorGetResumeModeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering28ReduceGeneratorGetResumeModeEPNS1_4NodeE:
.LFB23363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	376(%rax), %r8
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder30ForJSGeneratorObjectResumeModeEv@PLT
	movq	-120(%rbp), %r8
	movq	%r15, %rsi
	leaq	32(%r12), %r15
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, -120(%rbp)
	movq	%r12, %rcx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movzbl	23(%r12), %edx
	movq	-128(%rbp), %r9
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L158
	movq	32(%r12), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	cmpq	%rdi, %r9
	je	.L160
	leaq	-24(%rsi), %rbx
	testq	%rdi, %rdi
	je	.L162
.L196:
	movq	%rbx, %rsi
	movq	%rdx, -136(%rbp)
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %r9
.L162:
	movq	%r9, (%rdx)
	testq	%r9, %r9
	je	.L163
	movq	%rbx, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L163:
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L194
.L160:
	movq	8(%r15), %rdi
	cmpq	%rdi, %r13
	je	.L164
	leaq	8(%r15), %rdx
	movq	%r12, %rsi
.L165:
	leaq	-48(%rsi), %rbx
	testq	%rdi, %rdi
	je	.L167
	movq	%rbx, %rsi
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-128(%rbp), %rdx
.L167:
	movq	%r13, (%rdx)
	testq	%r13, %r13
	je	.L168
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L168:
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L164
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rdx
.L166:
	movq	16(%rdx), %rdi
	cmpq	%rdi, %r14
	je	.L171
	leaq	16(%rdx), %rbx
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L164:
	movq	16(%r15), %rdi
	cmpq	%rdi, %r14
	je	.L171
	leaq	16(%r15), %rbx
	movq	%r12, %rsi
.L170:
	leaq	-72(%rsi), %r13
	testq	%rdi, %rdi
	je	.L172
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L172:
	movq	%r14, (%rbx)
	testq	%r14, %r14
	je	.L171
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L171:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	-120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L195
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rdx
.L161:
	movq	8(%rdx), %rdi
	cmpq	%rdi, %r13
	je	.L166
	addq	$8, %rdx
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L158:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rdx
	cmpq	%rdi, %r9
	je	.L161
	leaq	-24(%rsi), %rbx
	testq	%rdi, %rdi
	jne	.L196
	jmp	.L162
.L195:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23363:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering28ReduceGeneratorGetResumeModeEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering28ReduceGeneratorGetResumeModeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering18ReduceIsJSReceiverEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering18ReduceIsJSReceiverEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering18ReduceIsJSReceiverEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering18ReduceIsJSReceiverEPNS1_4NodeE:
.LFB23365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16ObjectIsReceiverEv@PLT
	movq	8(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%rax, %r13
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23365:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering18ReduceIsJSReceiverEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering18ReduceIsJSReceiverEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering11ReduceIsSmiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering11ReduceIsSmiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering11ReduceIsSmiEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering11ReduceIsSmiEPNS1_4NodeE:
.LFB23366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11ObjectIsSmiEv@PLT
	movq	8(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%rax, %r13
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23366:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering11ReduceIsSmiEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering11ReduceIsSmiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering26ReduceTurbofanStaticAssertEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceTurbofanStaticAssertEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceTurbofanStaticAssertEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceTurbofanStaticAssertEPNS1_4NodeE:
.LFB23367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal15FLAG_always_optE(%rip)
	je	.L202
	movq	8(%rdi), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rsi, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
.L203:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L206
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, -80(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12StaticAssertEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	-72(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-80(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rax, %rcx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L203
.L206:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23367:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceTurbofanStaticAssertEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceTurbofanStaticAssertEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorE:
.LFB23368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	movq	%rsi, %rdx
	pushq	%r12
	.cfi_offset 12, -32
	movq	8(%rdi), %rdi
	movq	%rsi, %r12
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23368:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorE, .-_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToLengthEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToLengthEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToLengthEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToLengthEPNS1_4NodeE:
.LFB23369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8ToLengthEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23369:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToLengthEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToLengthEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToObjectEPNS1_4NodeE:
.LFB23370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8ToObjectEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23370:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToObjectEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToStringEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"IsHeapObject()"
.LC4:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToStringEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToStringEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToStringEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToStringEPNS1_4NodeE:
.LFB23371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	cmpw	$30, 16(%rax)
	jne	.L214
	leaq	-64(%rbp), %r14
	movq	24(%rbx), %rsi
	movq	48(%rax), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L222
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	testb	%al, %al
	je	.L214
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L214:
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8ToStringEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
.L216:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L223
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L223:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23371:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToStringEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToStringEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering10ReduceCallEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering10ReduceCallEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering10ReduceCallEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering10ReduceCallEPNS1_4NodeE:
.LFB23372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler23CallRuntimeParametersOfEPKNS1_8OperatorE@PLT
	leaq	-48(%rbp), %rcx
	leaq	-52(%rbp), %rdx
	movl	$1, %r9d
	movq	8(%rax), %rsi
	movq	16(%rbx), %rax
	movl	$2, %r8d
	movq	$0, -48(%rbp)
	movl	$-1, -40(%rbp)
	movq	368(%rax), %rdi
	movl	$0x7fc00000, -52(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder4CallEmRKNS1_13CallFrequencyERKNS1_14FeedbackSourceENS0_19ConvertReceiverModeENS0_15SpeculationModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L227
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L227:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23372:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering10ReduceCallEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering10ReduceCallEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering21ReduceIncBlockCounterEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering21ReduceIncBlockCounterEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering21ReduceIncBlockCounterEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering21ReduceIncBlockCounterEPNS1_4NodeE:
.LFB23373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$818, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r12), %rdx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-80(%rbp), %rsi
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	movzbl	18(%rdx), %r8d
	xorl	%edx, %edx
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L231
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L231:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23373:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering21ReduceIncBlockCounterEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering21ReduceIncBlockCounterEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_, @function
_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_:
.LFB23374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movq	%rsi, %rdx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	xorl	%r8d, %r8d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	movq	%rsi, %rcx
	subq	$24, %rsp
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movzbl	23(%r12), %edx
	leaq	32(%r12), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L233
	movq	32(%r12), %rdi
	movq	%rax, %rdx
	movq	%r12, %rsi
	cmpq	%rdi, %rbx
	je	.L235
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	je	.L237
.L258:
	movq	%r13, %rsi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rdx
.L237:
	movq	%rbx, (%rdx)
	testq	%rbx, %rbx
	je	.L238
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rax
.L238:
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L257
.L235:
	movq	8(%rax), %rdi
	cmpq	%rdi, %r14
	je	.L241
	leaq	8(%rax), %rbx
	movq	%r12, %rsi
.L240:
	leaq	-48(%rsi), %r13
	testq	%rdi, %rdi
	je	.L242
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L242:
	movq	%r14, (%rbx)
	testq	%r14, %r14
	je	.L241
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L241:
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rdx
.L236:
	movq	8(%rdx), %rdi
	cmpq	%rdi, %r14
	je	.L241
	leaq	8(%rdx), %rbx
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L233:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rdx
	cmpq	%rdi, %rbx
	je	.L236
	leaq	-24(%rsi), %r13
	testq	%rdi, %rdi
	jne	.L258
	jmp	.L237
	.cfi_endproc
.LFE23374:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_, .-_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_S4_
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_S4_, @function
_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_S4_:
.LFB23375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	xorl	%r8d, %r8d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	movq	%rsi, %rcx
	leaq	32(%r12), %r15
	subq	$40, %rsp
	movq	8(%rdi), %rdi
	movq	%rdx, -56(%rbp)
	movq	%rsi, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L260
	movq	32(%r12), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	cmpq	%rdi, %rbx
	je	.L262
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L264
.L296:
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rsi
.L264:
	movq	%rbx, (%rdx)
	testq	%rbx, %rbx
	je	.L265
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L265:
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L295
.L262:
	movq	8(%r15), %rdi
	cmpq	%rdi, %r13
	je	.L266
	leaq	8(%r15), %rdx
	movq	%r12, %rsi
.L267:
	leaq	-48(%rsi), %rbx
	testq	%rdi, %rdi
	je	.L269
	movq	%rbx, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %rdx
.L269:
	movq	%r13, (%rdx)
	testq	%r13, %r13
	je	.L270
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L270:
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L266
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rdx
.L268:
	movq	16(%rdx), %rdi
	cmpq	%rdi, %r14
	je	.L273
	leaq	16(%rdx), %rbx
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L266:
	movq	16(%r15), %rdi
	cmpq	%r14, %rdi
	je	.L273
	leaq	16(%r15), %rbx
	movq	%r12, %rsi
.L272:
	leaq	-72(%rsi), %r13
	testq	%rdi, %rdi
	je	.L274
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L274:
	movq	%r14, (%rbx)
	testq	%r14, %r14
	je	.L273
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L273:
	movq	%r12, %rdi
	movl	$3, %esi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rdx
.L263:
	movq	8(%rdx), %rdi
	cmpq	%rdi, %r13
	je	.L268
	addq	$8, %rdx
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L260:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rdx
	cmpq	%rdi, %rbx
	je	.L263
	subq	$24, %rsi
	testq	%rdi, %rdi
	jne	.L296
	jmp	.L264
	.cfi_endproc
.LFE23375:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_S4_, .-_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_S4_
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering20ReduceIsInstanceTypeEPNS1_4NodeENS0_12InstanceTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering20ReduceIsInstanceTypeEPNS1_4NodeENS0_12InstanceTypeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering20ReduceIsInstanceTypeEPNS1_4NodeENS0_12InstanceTypeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering20ReduceIsInstanceTypeEPNS1_4NodeENS0_12InstanceTypeE:
.LFB23364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	movzwl	%bx, %ebx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, -152(%rbp)
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11ObjectIsSmiEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	-160(%rbp), %rax
	movl	$1, %edx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -176(%rbp)
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-176(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-152(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	movq	16(%r12), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-152(%rbp), %r9
	movq	%rax, %rsi
	movq	%r15, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	%rax, -200(%rbp)
	movq	16(%r12), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	%r13, %rcx
	movl	$1, %edx
	xorl	%r8d, %r8d
	movq	-152(%rbp), %r9
	movq	%rax, %rsi
	movq	%r15, -80(%rbp)
	leaq	-144(%rbp), %r15
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -152(%rbp)
	movq	16(%r12), %rax
	movq	(%rax), %r11
	movq	376(%rax), %r8
	movq	%r11, -192(%rbp)
	movq	%r8, -176(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-176(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	movl	$3, %edx
	xorl	%r8d, %r8d
	movq	-192(%rbp), %r11
	movq	%rax, %rsi
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %xmm0
	movq	%r11, %rdi
	movq	%rax, -64(%rbp)
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -160(%rbp)
	movq	16(%r12), %rax
	movq	(%rax), %r11
	movq	376(%rax), %r8
	movq	%r11, -192(%rbp)
	movq	%r8, -176(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	-176(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-192(%rbp), %r11
	movq	%rax, %rsi
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %xmm0
	movq	%r11, %rdi
	movq	%rax, -64(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	16(%r12), %rdi
	cvtsi2sdl	%ebx, %xmm0
	movq	%rax, -160(%rbp)
	movq	(%rdi), %r9
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -176(%rbp)
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11NumberEqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-192(%rbp), %r9
	movq	%rax, %rsi
	movq	-160(%rbp), %xmm0
	movq	%r9, %rdi
	movhps	-176(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, -176(%rbp)
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-184(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movhps	-152(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, %rbx
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-168(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%rbx, -64(%rbp)
	movhps	-160(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %rdi
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%rax, %rcx
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	16(%r12), %rax
	movl	$2, %edx
	movl	$8, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%rbx, %r9
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	-176(%rbp), %r11
	movq	-200(%rbp), %r10
	movq	%rax, %rdx
	movq	%r11, %r8
	movq	%r10, %rcx
	call	_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_S4_
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L300
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L300:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23364:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering20ReduceIsInstanceTypeEPNS1_4NodeENS0_12InstanceTypeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering20ReduceIsInstanceTypeEPNS1_4NodeENS0_12InstanceTypeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering6ReduceEPNS1_4NodeE:
.LFB23344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -40
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpw	$766, 16(%rdi)
	je	.L302
.L306:
	xorl	%eax, %eax
.L303:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L335
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler23CallRuntimeParametersOfEPKNS1_8OperatorE@PLT
	movl	(%rax), %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	movl	(%rax), %edx
	cmpl	$432, %edx
	je	.L336
	cmpl	$1, 4(%rax)
	jne	.L306
	subl	$467, %edx
	cmpl	$29, %edx
	ja	.L306
	leaq	.L308(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19JSIntrinsicLowering6ReduceEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L308:
	.long	.L331-.L308
	.long	.L330-.L308
	.long	.L329-.L308
	.long	.L328-.L308
	.long	.L327-.L308
	.long	.L326-.L308
	.long	.L325-.L308
	.long	.L324-.L308
	.long	.L323-.L308
	.long	.L322-.L308
	.long	.L321-.L308
	.long	.L320-.L308
	.long	.L319-.L308
	.long	.L318-.L308
	.long	.L317-.L308
	.long	.L316-.L308
	.long	.L306-.L308
	.long	.L306-.L308
	.long	.L315-.L308
	.long	.L314-.L308
	.long	.L306-.L308
	.long	.L313-.L308
	.long	.L306-.L308
	.long	.L312-.L308
	.long	.L311-.L308
	.long	.L306-.L308
	.long	.L310-.L308
	.long	.L309-.L308
	.long	.L306-.L308
	.long	.L307-.L308
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering6ReduceEPNS1_4NodeE
.L307:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering19ReduceDeoptimizeNowEPNS1_4NodeE
	jmp	.L303
.L309:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering14ReduceToStringEPNS1_4NodeE
	jmp	.L303
.L310:
	movq	16(%r13), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8ToObjectEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L303
.L311:
	movq	16(%r13), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8ToLengthEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L303
.L312:
	movq	16(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16ObjectIsReceiverEv@PLT
.L334:
	movq	8(%r13), %rdi
	movq	%rax, %r14
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties20RemoveNonValueInputsEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L303
.L313:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering28ReduceCreateIterResultObjectEPNS1_4NodeE
	jmp	.L303
.L314:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering24ReduceCopyDataPropertiesEPNS1_4NodeE
	jmp	.L303
.L315:
	movq	16(%r13), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11ObjectIsSmiEv@PLT
	jmp	.L334
.L316:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering28ReduceGeneratorGetResumeModeEPNS1_4NodeE
	jmp	.L303
.L317:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering20ReduceGeneratorCloseEPNS1_4NodeE
	jmp	.L303
.L318:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering29ReduceCreateJSGeneratorObjectEPNS1_4NodeE
	jmp	.L303
.L319:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering25ReduceAsyncGeneratorYieldEPNS1_4NodeE
	jmp	.L303
.L320:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering27ReduceAsyncGeneratorResolveEPNS1_4NodeE
	jmp	.L303
.L321:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceAsyncGeneratorRejectEPNS1_4NodeE
	jmp	.L303
.L322:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering33ReduceAsyncGeneratorAwaitUncaughtEPNS1_4NodeE
	jmp	.L303
.L323:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering31ReduceAsyncGeneratorAwaitCaughtEPNS1_4NodeE
	jmp	.L303
.L324:
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	16(%r13), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder20AsyncFunctionResolveEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L303
.L325:
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	16(%r13), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder19AsyncFunctionRejectEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L303
.L326:
	movq	16(%r13), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder18AsyncFunctionEnterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L303
.L327:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering32ReduceAsyncFunctionAwaitUncaughtEPNS1_4NodeE
	jmp	.L303
.L328:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering30ReduceAsyncFunctionAwaitCaughtEPNS1_4NodeE
	jmp	.L303
.L329:
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler23CallRuntimeParametersOfEPKNS1_8OperatorE@PLT
	leaq	-64(%rbp), %rcx
	leaq	-68(%rbp), %rdx
	movl	$1, %r9d
	movq	8(%rax), %rsi
	movq	16(%r13), %rax
	movl	$2, %r8d
	movq	$0, -64(%rbp)
	movl	$-1, -56(%rbp)
	movq	368(%rax), %rdi
	movl	$0x7fc00000, -68(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder4CallEmRKNS1_13CallFrequencyERKNS1_14FeedbackSourceENS0_19ConvertReceiverModeENS0_15SpeculationModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%r12, %rax
	jmp	.L303
.L330:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering21ReduceIncBlockCounterEPNS1_4NodeE
	jmp	.L303
.L331:
	movl	$1061, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering20ReduceIsInstanceTypeEPNS1_4NodeENS0_12InstanceTypeE
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L336:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19JSIntrinsicLowering26ReduceTurbofanStaticAssertEPNS1_4NodeE
	jmp	.L303
.L335:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23344:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSIntrinsicLowering6ReduceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_S4_S4_
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_S4_S4_, @function
_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_S4_S4_:
.LFB23376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	movq	%rsi, %rcx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	xorl	%r8d, %r8d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%r12), %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rdi
	movq	%rdx, -56(%rbp)
	movq	%rsi, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L338
	movq	32(%r12), %rdi
	movq	%rbx, %rax
	movq	%r12, %rsi
	cmpq	%rdi, %r15
	je	.L340
	subq	$24, %rsi
	testq	%rdi, %rdi
	je	.L342
.L385:
	movq	%rax, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %rsi
.L342:
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L343
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L343:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L384
.L340:
	movq	8(%rbx), %rdi
	cmpq	%rdi, %r14
	je	.L344
	leaq	8(%rbx), %rax
	movq	%r12, %rsi
.L345:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L347
	movq	%rax, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %rsi
.L347:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L348
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L348:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L344
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L346:
	movq	16(%rax), %rdi
	cmpq	%rdi, %r13
	je	.L351
	addq	$16, %rax
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L344:
	movq	16(%rbx), %rdi
	cmpq	%rdi, %r13
	je	.L349
	leaq	16(%rbx), %rax
	movq	%r12, %rsi
.L350:
	leaq	-72(%rsi), %r14
	testq	%rdi, %rdi
	je	.L352
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %rax
.L352:
	movq	%r13, (%rax)
	testq	%r13, %r13
	je	.L353
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L353:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L349
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L351:
	movq	24(%rax), %rdi
	cmpq	16(%rbp), %rdi
	je	.L356
	leaq	24(%rax), %r14
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L349:
	movq	24(%rbx), %rdi
	cmpq	%rdi, 16(%rbp)
	je	.L356
	leaq	24(%rbx), %r14
	movq	%r12, %rsi
.L355:
	leaq	-96(%rsi), %r13
	testq	%rdi, %rdi
	je	.L357
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L357:
	movq	16(%rbp), %rax
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L356
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L356:
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L341:
	movq	8(%rax), %rdi
	cmpq	%rdi, %r14
	je	.L346
	addq	$8, %rax
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L338:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rax
	cmpq	%rdi, %r15
	je	.L341
	subq	$24, %rsi
	testq	%rdi, %rdi
	jne	.L385
	jmp	.L342
	.cfi_endproc
.LFE23376:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_S4_S4_, .-_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeEPKNS1_8OperatorES4_S4_S4_S4_
	.section	.text._ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeERKNS0_8CallableEiNS2_14FrameStateFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeERKNS0_8CallableEiNS2_14FrameStateFlagE
	.type	_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeERKNS0_8CallableEiNS2_14FrameStateFlagE, @function
_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeERKNS0_8CallableEiNS2_14FrameStateFlagE:
.LFB23377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	movl	%ecx, %edx
	xorl	%ecx, %ecx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	(%rsi), %rdi
	leaq	-64(%rbp), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	testl	%r8d, %r8d
	movq	%rax, -64(%rbp)
	movq	16(%r13), %rax
	sete	%cl
	xorl	%r9d, %r9d
	movzbl	18(%rdi), %r8d
	movq	%rax, -56(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	0(%r13), %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r14, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L389
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L389:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23377:
	.size	_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeERKNS0_8CallableEiNS2_14FrameStateFlagE, .-_ZN2v88internal8compiler19JSIntrinsicLowering6ChangeEPNS1_4NodeERKNS0_8CallableEiNS2_14FrameStateFlagE
	.section	.text._ZNK2v88internal8compiler19JSIntrinsicLowering5graphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19JSIntrinsicLowering5graphEv
	.type	_ZNK2v88internal8compiler19JSIntrinsicLowering5graphEv, @function
_ZNK2v88internal8compiler19JSIntrinsicLowering5graphEv:
.LFB23378:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE23378:
	.size	_ZNK2v88internal8compiler19JSIntrinsicLowering5graphEv, .-_ZNK2v88internal8compiler19JSIntrinsicLowering5graphEv
	.section	.text._ZNK2v88internal8compiler19JSIntrinsicLowering7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19JSIntrinsicLowering7isolateEv
	.type	_ZNK2v88internal8compiler19JSIntrinsicLowering7isolateEv, @function
_ZNK2v88internal8compiler19JSIntrinsicLowering7isolateEv:
.LFB23379:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE23379:
	.size	_ZNK2v88internal8compiler19JSIntrinsicLowering7isolateEv, .-_ZNK2v88internal8compiler19JSIntrinsicLowering7isolateEv
	.section	.text._ZNK2v88internal8compiler19JSIntrinsicLowering6commonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19JSIntrinsicLowering6commonEv
	.type	_ZNK2v88internal8compiler19JSIntrinsicLowering6commonEv, @function
_ZNK2v88internal8compiler19JSIntrinsicLowering6commonEv:
.LFB23380:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE23380:
	.size	_ZNK2v88internal8compiler19JSIntrinsicLowering6commonEv, .-_ZNK2v88internal8compiler19JSIntrinsicLowering6commonEv
	.section	.text._ZNK2v88internal8compiler19JSIntrinsicLowering10javascriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19JSIntrinsicLowering10javascriptEv
	.type	_ZNK2v88internal8compiler19JSIntrinsicLowering10javascriptEv, @function
_ZNK2v88internal8compiler19JSIntrinsicLowering10javascriptEv:
.LFB23381:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	368(%rax), %rax
	ret
	.cfi_endproc
.LFE23381:
	.size	_ZNK2v88internal8compiler19JSIntrinsicLowering10javascriptEv, .-_ZNK2v88internal8compiler19JSIntrinsicLowering10javascriptEv
	.section	.text._ZNK2v88internal8compiler19JSIntrinsicLowering10simplifiedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19JSIntrinsicLowering10simplifiedEv
	.type	_ZNK2v88internal8compiler19JSIntrinsicLowering10simplifiedEv, @function
_ZNK2v88internal8compiler19JSIntrinsicLowering10simplifiedEv:
.LFB23382:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	376(%rax), %rax
	ret
	.cfi_endproc
.LFE23382:
	.size	_ZNK2v88internal8compiler19JSIntrinsicLowering10simplifiedEv, .-_ZNK2v88internal8compiler19JSIntrinsicLowering10simplifiedEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler19JSIntrinsicLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler19JSIntrinsicLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler19JSIntrinsicLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE:
.LFB28104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28104:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler19JSIntrinsicLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, .-_GLOBAL__sub_I__ZN2v88internal8compiler19JSIntrinsicLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler19JSIntrinsicLoweringC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal8compiler19JSIntrinsicLoweringE
	.section	.data.rel.ro._ZTVN2v88internal8compiler19JSIntrinsicLoweringE,"awG",@progbits,_ZTVN2v88internal8compiler19JSIntrinsicLoweringE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler19JSIntrinsicLoweringE, @object
	.size	_ZTVN2v88internal8compiler19JSIntrinsicLoweringE, 56
_ZTVN2v88internal8compiler19JSIntrinsicLoweringE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler19JSIntrinsicLoweringD1Ev
	.quad	_ZN2v88internal8compiler19JSIntrinsicLoweringD0Ev
	.quad	_ZNK2v88internal8compiler19JSIntrinsicLowering12reducer_nameEv
	.quad	_ZN2v88internal8compiler19JSIntrinsicLowering6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	-1074790400
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
