	.file	"graph-visualizer.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1338:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1338:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v88internal8compiler13TurboJsonFileD1Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13TurboJsonFileD1Ev
	.type	_ZN2v88internal8compiler13TurboJsonFileD1Ev, @function
_ZN2v88internal8compiler13TurboJsonFileD1Ev:
.LFB20256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8compiler13TurboJsonFileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$40, %rax
	movq	%rax, 248(%rdi)
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	leaq	24+_ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rdx
	movq	%rax, 248(%rbx)
	movq	%rdx, %xmm0
	leaq	8(%rbx), %rdi
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	leaq	112(%rbx), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, 8(%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8compiler13TurboJsonFileE0_So(%rip), %rax
	leaq	248(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 248(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE20256:
	.size	_ZN2v88internal8compiler13TurboJsonFileD1Ev, .-_ZN2v88internal8compiler13TurboJsonFileD1Ev
	.section	.text._ZN2v88internal8compiler12TurboCfgFileD1Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12TurboCfgFileD1Ev
	.type	_ZN2v88internal8compiler12TurboCfgFileD1Ev, @function
_ZN2v88internal8compiler12TurboCfgFileD1Ev:
.LFB20263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8compiler12TurboCfgFileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$40, %rax
	movq	%rax, 248(%rdi)
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	leaq	24+_ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rdx
	movq	%rax, 248(%rbx)
	movq	%rdx, %xmm0
	leaq	8(%rbx), %rdi
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	leaq	112(%rbx), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, 8(%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8compiler12TurboCfgFileE0_So(%rip), %rax
	leaq	248(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 248(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE20263:
	.size	_ZN2v88internal8compiler12TurboCfgFileD1Ev, .-_ZN2v88internal8compiler12TurboCfgFileD1Ev
	.section	.text._ZTv0_n24_N2v88internal8compiler12TurboCfgFileD0Ev,"ax",@progbits
	.p2align 4
	.globl	_ZTv0_n24_N2v88internal8compiler12TurboCfgFileD0Ev
	.type	_ZTv0_n24_N2v88internal8compiler12TurboCfgFileD0Ev, @function
_ZTv0_n24_N2v88internal8compiler12TurboCfgFileD0Ev:
.LFB26049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	24+_ZTVN2v88internal8compiler12TurboCfgFileE(%rip), %rax
	movq	%rax, (%rdi)
	addq	$40, %rax
	movq	%rdi, %r12
	movq	%rax, 248(%rdi)
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	leaq	8(%r12), %rdi
	leaq	24+_ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rdx
	movq	%rax, 248(%r12)
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdx, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	leaq	112(%r12), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%r12), %rdi
	movq	%rax, 8(%r12)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8compiler12TurboCfgFileE0_So(%rip), %rax
	leaq	248(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 248(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$512, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26049:
	.size	_ZTv0_n24_N2v88internal8compiler12TurboCfgFileD0Ev, .-_ZTv0_n24_N2v88internal8compiler12TurboCfgFileD0Ev
	.section	.text._ZTv0_n24_N2v88internal8compiler13TurboJsonFileD0Ev,"ax",@progbits
	.p2align 4
	.globl	_ZTv0_n24_N2v88internal8compiler13TurboJsonFileD0Ev
	.type	_ZTv0_n24_N2v88internal8compiler13TurboJsonFileD0Ev, @function
_ZTv0_n24_N2v88internal8compiler13TurboJsonFileD0Ev:
.LFB26048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	24+_ZTVN2v88internal8compiler13TurboJsonFileE(%rip), %rax
	movq	%rax, (%rdi)
	addq	$40, %rax
	movq	%rdi, %r12
	movq	%rax, 248(%rdi)
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	leaq	8(%r12), %rdi
	leaq	24+_ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rdx
	movq	%rax, 248(%r12)
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdx, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	leaq	112(%r12), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%r12), %rdi
	movq	%rax, 8(%r12)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8compiler13TurboJsonFileE0_So(%rip), %rax
	leaq	248(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 248(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$512, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26048:
	.size	_ZTv0_n24_N2v88internal8compiler13TurboJsonFileD0Ev, .-_ZTv0_n24_N2v88internal8compiler13TurboJsonFileD0Ev
	.section	.text._ZN2v88internal8compiler13TurboJsonFileD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13TurboJsonFileD0Ev
	.type	_ZN2v88internal8compiler13TurboJsonFileD0Ev, @function
_ZN2v88internal8compiler13TurboJsonFileD0Ev:
.LFB20257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8compiler13TurboJsonFileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$40, %rax
	movq	%rax, 248(%rdi)
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	leaq	8(%r12), %rdi
	leaq	24+_ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rdx
	movq	%rax, 248(%r12)
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdx, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	leaq	112(%r12), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%r12), %rdi
	movq	%rax, 8(%r12)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8compiler13TurboJsonFileE0_So(%rip), %rax
	leaq	248(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 248(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$512, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20257:
	.size	_ZN2v88internal8compiler13TurboJsonFileD0Ev, .-_ZN2v88internal8compiler13TurboJsonFileD0Ev
	.section	.text._ZN2v88internal8compiler12TurboCfgFileD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12TurboCfgFileD0Ev
	.type	_ZN2v88internal8compiler12TurboCfgFileD0Ev, @function
_ZN2v88internal8compiler12TurboCfgFileD0Ev:
.LFB20264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8compiler12TurboCfgFileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$40, %rax
	movq	%rax, 248(%rdi)
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	leaq	8(%r12), %rdi
	leaq	24+_ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rdx
	movq	%rax, 248(%r12)
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdx, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	leaq	112(%r12), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%r12), %rdi
	movq	%rax, 8(%r12)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8compiler12TurboCfgFileE0_So(%rip), %rax
	leaq	248(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 248(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$512, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20264:
	.size	_ZN2v88internal8compiler12TurboCfgFileD0Ev, .-_ZN2v88internal8compiler12TurboCfgFileD0Ev
	.section	.text._ZTv0_n24_N2v88internal8compiler12TurboCfgFileD1Ev,"ax",@progbits
	.p2align 4
	.globl	_ZTv0_n24_N2v88internal8compiler12TurboCfgFileD1Ev
	.type	_ZTv0_n24_N2v88internal8compiler12TurboCfgFileD1Ev, @function
_ZTv0_n24_N2v88internal8compiler12TurboCfgFileD1Ev:
.LFB26050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	24+_ZTVN2v88internal8compiler12TurboCfgFileE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, (%rbx)
	addq	$40, %rax
	movq	%rbx, %rdi
	movq	%rax, 248(%rbx)
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	leaq	24+_ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rdx
	movq	%rax, 248(%rbx)
	movq	%rdx, %xmm0
	leaq	8(%rbx), %rdi
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	leaq	112(%rbx), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, 8(%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8compiler12TurboCfgFileE0_So(%rip), %rax
	leaq	248(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 248(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE26050:
	.size	_ZTv0_n24_N2v88internal8compiler12TurboCfgFileD1Ev, .-_ZTv0_n24_N2v88internal8compiler12TurboCfgFileD1Ev
	.section	.text._ZTv0_n24_N2v88internal8compiler13TurboJsonFileD1Ev,"ax",@progbits
	.p2align 4
	.globl	_ZTv0_n24_N2v88internal8compiler13TurboJsonFileD1Ev
	.type	_ZTv0_n24_N2v88internal8compiler13TurboJsonFileD1Ev, @function
_ZTv0_n24_N2v88internal8compiler13TurboJsonFileD1Ev:
.LFB26051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	24+_ZTVN2v88internal8compiler13TurboJsonFileE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, (%rbx)
	addq	$40, %rax
	movq	%rbx, %rdi
	movq	%rax, 248(%rbx)
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	leaq	24+_ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rdx
	movq	%rax, 248(%rbx)
	movq	%rdx, %xmm0
	leaq	8(%rbx), %rdi
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	leaq	112(%rbx), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, 8(%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8compiler13TurboJsonFileE0_So(%rip), %rax
	leaq	248(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 248(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE26051:
	.size	_ZTv0_n24_N2v88internal8compiler13TurboJsonFileD1Ev, .-_ZTv0_n24_N2v88internal8compiler13TurboJsonFileD1Ev
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_118PrintScheduledNodeERSoiPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"null"
.LC1:
	.string	"     "
.LC2:
	.string	"#"
.LC3:
	.string	":"
.LC4:
	.string	"("
.LC5:
	.string	". "
.LC6:
	.string	")"
.LC7:
	.string	", "
.LC8:
	.string	"  [Type: "
.LC9:
	.string	"]"
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_118PrintScheduledNodeERSoiPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_118PrintScheduledNodeERSoiPNS1_4NodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_118PrintScheduledNodeERSoiPNS1_4NodeE:
.LFB20363:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	.LC5(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	leaq	.LC1(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movl	$5, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testl	%r13d, %r13d
	jle	.L23
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, %r13d
	jne	.L20
.L23:
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rbx
	movq	%r12, %rdi
	movl	20(%rbx), %esi
	addq	$32, %rbx
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-32(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	-9(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L22
	movq	-56(%rbp), %rax
	movq	32(%rax), %rbx
	movl	8(%rbx), %eax
	addq	$16, %rbx
.L22:
	cltq
	leaq	(%rbx,%rax,8), %r14
	cmpq	%rbx, %r14
	je	.L31
	movq	(%rbx), %r13
	addq	$8, %rbx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
.L32:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%rbx, %r14
	je	.L31
.L30:
	movl	$2, %edx
	movq	%r12, %rdi
	movq	(%rbx), %r13
	addq	$8, %rbx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L33:
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r13, %r13
	je	.L27
	movl	20(%r13), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	8(%rax), %r13
	testq	%r13, %r13
	jne	.L28
	movq	(%r15), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	cmpq	%rbx, %r14
	jne	.L30
.L31:
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	cmpq	$0, 8(%rax)
	jne	.L39
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L27:
	.cfi_restore_state
	movl	$-1, %esi
	movq	%r12, %rdi
	leaq	.LC0(%rip), %r13
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$4, %edx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$9, %edx
	movq	%r12, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v88internal8compilerlsERSoNS1_4TypeE@PLT
	addq	$24, %rsp
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	popq	%rbx
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.cfi_endproc
.LFE20363:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_118PrintScheduledNodeERSoiPNS1_4NodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_118PrintScheduledNodeERSoiPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci.part.0.str1.1,"aMS",@progbits,1
.LC10:
	.string	"invalid"
.LC11:
	.string	" "
.LC12:
	.string	"  "
.LC13:
	.string	" \""
.LC14:
	.string	"\""
.LC15:
	.string	" \"const(nostack):"
.LC16:
	.string	" \"fp_stack:"
.LC17:
	.string	" \"stack:"
.LC18:
	.string	" B"
.LC19:
	.string	" unknown"
.LC20:
	.string	" ["
.LC21:
	.string	"["
.LC22:
	.string	" \"\"\n"
.LC23:
	.string	" M"
	.section	.text._ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci.part.0, @function
_ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci.part.0:
.LFB25884:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.LC12(%rip), %rbx
	subq	$24, %rsp
	movl	8(%rdi), %eax
	movl	%ecx, -52(%rbp)
	testl	%eax, %eax
	jle	.L44
	.p2align 4,,10
	.p2align 3
.L41:
	movq	0(%r13), %rdi
	movl	$2, %edx
	movq	%rbx, %rsi
	addl	$1, %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 8(%r13)
	jg	.L41
.L44:
	movl	-52(%rbp), %esi
	movq	0(%r13), %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%r14), %esi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r12, %r12
	je	.L100
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L45:
	movl	4(%r14), %edx
	movl	%edx, %eax
	shrl	$7, %eax
	andl	$63, %eax
	cmpl	$32, %eax
	jne	.L101
	andl	$1, %edx
	movq	32(%r14), %r12
	movq	0(%r13), %r15
	jne	.L102
.L53:
	movq	96(%r12), %rbx
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r15, %rdi
	testq	%rbx, %rbx
	cmove	%r12, %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movl	88(%rbx), %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	cmpq	$0, 80(%r14)
	je	.L63
	movq	0(%r13), %r12
	leaq	.LC18(%rip), %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	80(%r14), %rax
	movq	%r12, %rdi
	movl	112(%rax), %esi
	call	_ZNSolsEi@PLT
.L64:
	movq	16(%r14), %rbx
	leaq	.LC20(%rip), %r15
	testq	%rbx, %rbx
	je	.L68
	.p2align 4,,10
	.p2align 3
.L65:
	movq	0(%r13), %r12
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%rbx), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC21(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L65
.L68:
	movq	24(%r14), %rbx
	leaq	.LC11(%rip), %r12
	testq	%rbx, %rbx
	je	.L66
	.p2align 4,,10
	.p2align 3
.L67:
	testb	$32, 28(%rbx)
	jne	.L69
	cmpb	$0, _ZN2v88internal19FLAG_trace_all_usesE(%rip)
	je	.L70
.L69:
	movq	0(%r13), %r14
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	24(%rbx), %esi
	movq	%r14, %rdi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC23(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L70:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L67
.L66:
	movq	0(%r13), %rdi
	addq	$24, %rsp
	movl	$4, %edx
	leaq	.LC22(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movl	4(%r12), %eax
	testb	$64, %al
	jne	.L53
	movq	104(%r12), %rdx
	movq	(%rdx), %rbx
	movl	%ebx, %edx
	andl	$7, %edx
	cmpl	$2, %edx
	je	.L103
	shrl	$13, %eax
	sarq	$35, %rbx
	movl	$11, %edx
	leaq	.LC16(%rip), %rsi
	cmpb	$11, %al
	jbe	.L104
.L94:
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%ebx, %esi
.L95:
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r14), %r12
	movq	0(%r13), %r15
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9LiveRange18GetAssignedOperandEv@PLT
	movq	0(%r13), %r12
	testb	$4, %al
	je	.L47
	movq	%rax, %r15
	sarq	$35, %r15
	movslq	%r15d, %rbx
	testb	$24, %al
	jne	.L48
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L48
	movl	$2, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	$-1, %r15
	je	.L75
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rax
	movq	(%rax,%rbx,8), %r15
	testq	%r15, %r15
	je	.L58
	.p2align 4,,10
	.p2align 3
.L96:
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
.L57:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L59:
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r14), %r12
	movq	0(%r13), %r15
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L47:
	sarq	$35, %rax
	movq	%rax, %r15
	movslq	%eax, %rbx
.L48:
	movl	$2, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	$-1, %r15
	je	.L75
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rax
	movq	(%rax,%rbx,8), %r15
	testq	%r15, %r15
	jne	.L96
.L58:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L100:
	movq	(%r15), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L63:
	movq	0(%r13), %rdi
	movl	$8, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$7, %edx
	leaq	.LC10(%rip), %r15
	jmp	.L57
.L104:
	movl	$8, %edx
	leaq	.LC17(%rip), %rsi
	jmp	.L94
.L103:
	leaq	.LC15(%rip), %rsi
	movl	$17, %edx
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	104(%r12), %rax
	movq	(%rax), %rsi
	shrq	$3, %rsi
	jmp	.L95
	.cfi_endproc
.LFE25884:
	.size	_ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci.part.0, .-_ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci.part.0
	.section	.text._ZN2v88internal8compiler13TurboJsonFileD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13TurboJsonFileD2Ev
	.type	_ZN2v88internal8compiler13TurboJsonFileD2Ev, @function
_ZN2v88internal8compiler13TurboJsonFileD2Ev:
.LFB20255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	40(%rsi), %rdx
	movq	-24(%rax), %rax
	movq	%rdx, (%rdi,%rax)
	call	_ZNSo5flushEv@PLT
	movq	8(%r12), %rax
	leaq	8(%rbx), %rdi
	movq	%rax, (%rbx)
	movq	32(%r12), %rdx
	movq	-24(%rax), %rax
	movq	%rdx, (%rbx,%rax)
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 8(%rbx)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	leaq	112(%rbx), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, 8(%rbx)
	call	_ZNSt6localeD1Ev@PLT
	movq	16(%r12), %rax
	movq	%rax, (%rbx)
	movq	24(%r12), %rdx
	movq	-24(%rax), %rax
	movq	%rdx, (%rbx,%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20255:
	.size	_ZN2v88internal8compiler13TurboJsonFileD2Ev, .-_ZN2v88internal8compiler13TurboJsonFileD2Ev
	.section	.text._ZN2v88internal8compiler12TurboCfgFileC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12TurboCfgFileC2EPNS0_7IsolateE
	.type	_ZN2v88internal8compiler12TurboCfgFileC2EPNS0_7IsolateE, @function
_ZN2v88internal8compiler12TurboCfgFileC2EPNS0_7IsolateE:
.LFB20259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	-80(%rbp), %rdi
	leaq	8(%rbx), %r13
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate19GetTurboCfgFileNameB5cxx11EPS1_@PLT
	movq	16(%r12), %rax
	xorl	%esi, %esi
	movq	-80(%rbp), %r14
	movq	24(%r12), %rdx
	movq	%rax, (%rbx)
	movq	-24(%rax), %rax
	movq	%rdx, (%rbx,%rax)
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8(%r12), %rax
	movq	32(%r12), %rdx
	movq	%r13, %rdi
	movq	%rax, (%rbx)
	movq	-24(%rax), %rax
	movq	%rdx, (%rbx,%rax)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEEC1Ev@PLT
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	%r13, %rdi
	movl	$17, %edx
	movq	%r14, %rsi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	movq	%rbx, %rdi
	testq	%rax, %rax
	movq	(%rbx), %rax
	je	.L113
	addq	-24(%rax), %rdi
	xorl	%esi, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.L109:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L110
	call	_ZdlPv@PLT
.L110:
	movq	(%r12), %rax
	movq	40(%r12), %rdx
	movq	%rax, (%rbx)
	movq	-24(%rax), %rax
	movq	%rdx, (%rbx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	addq	-24(%rax), %rdi
	movl	32(%rdi), %esi
	orl	$4, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L109
.L114:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20259:
	.size	_ZN2v88internal8compiler12TurboCfgFileC2EPNS0_7IsolateE, .-_ZN2v88internal8compiler12TurboCfgFileC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal8compiler12TurboCfgFileC1EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12TurboCfgFileC1EPNS0_7IsolateE
	.type	_ZN2v88internal8compiler12TurboCfgFileC1EPNS0_7IsolateE, @function
_ZN2v88internal8compiler12TurboCfgFileC1EPNS0_7IsolateE:
.LFB20260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	248(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	leaq	-80(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movups	%xmm0, 480(%rbx)
	leaq	8(%rbx), %r12
	movq	%rax, 248(%rbx)
	xorl	%eax, %eax
	movw	%ax, 472(%rbx)
	movq	$0, 464(%rbx)
	movups	%xmm0, 496(%rbx)
	call	_ZN2v88internal7Isolate19GetTurboCfgFileNameB5cxx11EPS1_@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	-80(%rbp), %r14
	leaq	24+_ZTCN2v88internal8compiler12TurboCfgFileE0_So(%rip), %rax
	movq	%rax, (%rbx)
	addq	$40, %rax
	movq	%rax, 248(%rbx)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	24+_ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%rbx)
	addq	$40, %rax
	movq	%rax, 248(%rbx)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEEC1Ev@PLT
	movq	(%rbx), %rax
	movq	%r12, %rsi
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	%r12, %rdi
	movl	$17, %edx
	movq	%r14, %rsi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	movq	%rbx, %rdi
	testq	%rax, %rax
	movq	(%rbx), %rax
	je	.L121
	addq	-24(%rax), %rdi
	xorl	%esi, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.L117:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L118
	call	_ZdlPv@PLT
.L118:
	leaq	24+_ZTVN2v88internal8compiler12TurboCfgFileE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$40, %rax
	movq	%rax, 248(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L122
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	addq	-24(%rax), %rdi
	movl	32(%rdi), %esi
	orl	$4, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L117
.L122:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20260:
	.size	_ZN2v88internal8compiler12TurboCfgFileC1EPNS0_7IsolateE, .-_ZN2v88internal8compiler12TurboCfgFileC1EPNS0_7IsolateE
	.section	.text._ZN2v88internal8compiler12TurboCfgFileD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12TurboCfgFileD2Ev
	.type	_ZN2v88internal8compiler12TurboCfgFileD2Ev, @function
_ZN2v88internal8compiler12TurboCfgFileD2Ev:
.LFB20262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	40(%rsi), %rdx
	movq	-24(%rax), %rax
	movq	%rdx, (%rdi,%rax)
	call	_ZNSo5flushEv@PLT
	movq	8(%r12), %rax
	leaq	8(%rbx), %rdi
	movq	%rax, (%rbx)
	movq	32(%r12), %rdx
	movq	-24(%rax), %rax
	movq	%rdx, (%rbx,%rax)
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 8(%rbx)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	leaq	112(%rbx), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, 8(%rbx)
	call	_ZNSt6localeD1Ev@PLT
	movq	16(%r12), %rax
	movq	%rax, (%rbx)
	movq	24(%r12), %rdx
	movq	-24(%rax), %rax
	movq	%rdx, (%rbx,%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20262:
	.size	_ZN2v88internal8compiler12TurboCfgFileD2Ev, .-_ZN2v88internal8compiler12TurboCfgFileD2Ev
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_20SourcePositionAsJSONE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_20SourcePositionAsJSONE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_20SourcePositionAsJSONE, @function
_ZN2v88internal8compilerlsERSoRKNS1_20SourcePositionAsJSONE:
.LFB20265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%r8), %rdi
	call	_ZNK2v88internal14SourcePosition9PrintJsonERSo@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20265:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_20SourcePositionAsJSONE, .-_ZN2v88internal8compilerlsERSoRKNS1_20SourcePositionAsJSONE
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_16NodeOriginAsJSONE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_16NodeOriginAsJSONE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_16NodeOriginAsJSONE, @function
_ZN2v88internal8compilerlsERSoRKNS1_16NodeOriginAsJSONE:
.LFB20266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%r8), %rdi
	call	_ZNK2v88internal8compiler10NodeOrigin9PrintJsonERSo@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20266:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_16NodeOriginAsJSONE, .-_ZN2v88internal8compilerlsERSoRKNS1_16NodeOriginAsJSONE
	.section	.rodata._ZN2v88internal8compiler23JsonPrintFunctionSourceERSoiSt10unique_ptrIA_cSt14default_deleteIS4_EENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateENS8_INS0_18SharedFunctionInfoEEEb.str1.1,"aMS",@progbits,1
.LC24:
	.string	"\" : "
.LC25:
	.string	"{ "
.LC26:
	.string	"\"sourceId\": "
.LC27:
	.string	", \"functionName\": \""
.LC28:
	.string	"\" "
.LC29:
	.string	", \"sourceName\": \""
.LC30:
	.string	", \"sourceName\": \"\""
.LC31:
	.string	", \"sourceText\": \"\""
.LC32:
	.string	"\\\""
.LC33:
	.string	"\\\\"
.LC34:
	.string	"\\b"
.LC35:
	.string	"\\f"
.LC36:
	.string	"\\n"
.LC37:
	.string	"\\r"
.LC38:
	.string	"\\t"
.LC39:
	.string	", \"sourceText\": \""
.LC40:
	.string	", \"startPosition\": "
.LC41:
	.string	", \"endPosition\": "
.LC42:
	.string	"}"
	.section	.text._ZN2v88internal8compiler23JsonPrintFunctionSourceERSoiSt10unique_ptrIA_cSt14default_deleteIS4_EENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateENS8_INS0_18SharedFunctionInfoEEEb,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler23JsonPrintFunctionSourceERSoiSt10unique_ptrIA_cSt14default_deleteIS4_EENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateENS8_INS0_18SharedFunctionInfoEEEb
	.type	_ZN2v88internal8compiler23JsonPrintFunctionSourceERSoiSt10unique_ptrIA_cSt14default_deleteIS4_EENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateENS8_INS0_18SharedFunctionInfoEEEb, @function
_ZN2v88internal8compiler23JsonPrintFunctionSourceERSoiSt10unique_ptrIA_cSt14default_deleteIS4_EENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateENS8_INS0_18SharedFunctionInfoEEEb:
.LFB20280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$552, %rsp
	movq	%r8, -552(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 16(%rbp)
	jne	.L181
.L130:
	movl	$2, %edx
	leaq	.LC25(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$12, %edx
	leaq	.LC26(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	movl	$19, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L182
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L132:
	movl	$2, %edx
	leaq	.LC28(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r14, %r14
	je	.L133
	movq	-552(%rbp), %rcx
	movq	(%r14), %rax
	cmpq	%rax, 88(%rcx)
	je	.L133
	testq	%rbx, %rbx
	je	.L133
	movq	15(%rax), %r13
	movl	$17, %edx
	movq	%r15, %rdi
	leaq	.LC29(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-496(%rbp), %r12
	testb	$1, %r13b
	jne	.L183
.L135:
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, -496(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	movq	(%rbx), %rax
	movq	%rax, -496(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	movl	$17, %edx
	leaq	.LC39(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, -560(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, -496(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	movl	%eax, %ecx
	movl	%eax, -552(%rbp)
	movq	(%r14), %rax
	movq	7(%rax), %r14
	movl	%ecx, %eax
	subl	%r13d, %eax
	cmpl	$-1, %eax
	je	.L184
.L156:
	leaq	-537(%rbp), %rbx
	movq	%r12, %rdi
	movq	%r14, -496(%rbp)
	movq	%rbx, %rsi
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%r14, -536(%rbp)
	movq	%rbx, %rsi
	leaq	-536(%rbp), %r14
	movq	%rdx, %rdi
	movq	%rax, %r12
	movq	%rax, -528(%rbp)
	movq	%rdi, -520(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	cmpq	%rax, %r12
	movq	%rax, -496(%rbp)
	movq	%rdx, -488(%rbp)
	setne	%al
	cmpl	-552(%rbp), %r13d
	jne	.L157
	testb	%al, %al
	je	.L160
.L157:
	cmpl	$1, -516(%rbp)
	leal	1(%r13), %edx
	je	.L158
	movl	%r13d, -576(%rbp)
	movslq	%edx, %rbx
	movq	%r12, %r13
	movl	%eax, %r12d
	.p2align 4,,10
	.p2align 3
.L159:
	movzwl	-2(%r13,%rbx,2), %eax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movw	%ax, -536(%rbp)
	call	_ZN2v88internallsERSoRKNS0_20AsEscapedUC16ForJSONE@PLT
	cmpl	%ebx, -552(%rbp)
	setne	%al
	addq	$1, %rbx
	orb	%r12b, %al
	jne	.L159
.L180:
	movl	-576(%rbp), %r13d
.L160:
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L133:
	movl	$18, %edx
	leaq	.LC30(%rip), %rsi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$18, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$0, -560(%rbp)
.L136:
	movl	$19, %edx
	leaq	.LC40(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	movl	$17, %edx
	leaq	.LC41(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-560(%rbp), %esi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC42(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L185
	addq	$552, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	movl	$4, %edx
	leaq	.LC24(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L182:
	movq	(%r15), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L184:
	movl	11(%r14), %eax
	addl	%r13d, %eax
	movl	%eax, -552(%rbp)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L183:
	movq	-1(%r13), %rax
	leaq	-496(%rbp), %r12
	cmpw	$63, 11(%rax)
	ja	.L135
	movq	.LC43(%rip), %xmm1
	leaq	-320(%rbp), %rdx
	leaq	-432(%rbp), %rax
	movq	%rdx, %rdi
	movq	%rdx, -560(%rbp)
	movhps	.LC44(%rip), %xmm1
	movq	%rax, -552(%rbp)
	movaps	%xmm1, -576(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rsi
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movq	%rsi, -320(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movq	-552(%rbp), %rcx
	movq	%rsi, -432(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rsi), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rsi, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	xorl	%esi, %esi
	addq	-24(%rax), %rcx
	movq	%rcx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movdqa	-576(%rbp), %xmm1
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -576(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-560(%rbp), %rdi
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -584(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	leaq	-528(%rbp), %rsi
	movl	$1, %edx
	movq	%r13, -528(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-496(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L186
	movq	%rsi, %rdi
	movq	%rsi, -592(%rbp)
	call	strlen@PLT
	movq	-592(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L139:
	movq	-496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L140
	call	_ZdaPv@PLT
.L140:
	leaq	-448(%rbp), %rax
	movb	$0, -448(%rbp)
	leaq	-464(%rbp), %rdi
	movq	%rax, -552(%rbp)
	movq	%rax, -464(%rbp)
	movq	-384(%rbp), %rax
	movq	$0, -456(%rbp)
	testq	%rax, %rax
	je	.L141
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L142
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L143:
	movq	-464(%rbp), %rcx
	movq	-456(%rbp), %rax
	addq	%rcx, %rax
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L144
	movq	%rbx, -592(%rbp)
	movq	%rax, %r13
	movq	%rcx, %rbx
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L145:
	cmpb	$92, %al
	je	.L187
	cmpb	$8, %al
	je	.L188
	cmpb	$12, %al
	je	.L189
	cmpb	$10, %al
	je	.L190
	cmpb	$13, %al
	je	.L191
	cmpb	$9, %al
	je	.L192
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movb	%al, -496(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L146:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L193
.L153:
	movzbl	(%rbx), %eax
	cmpb	$34, %al
	jne	.L145
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L158:
	movslq	%edx, %rdx
	movl	%r13d, -576(%rbp)
	movl	%eax, %r13d
	movq	%rdx, %rbx
	.p2align 4,,10
	.p2align 3
.L161:
	movzbl	-1(%r12,%rbx), %eax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movw	%ax, -536(%rbp)
	call	_ZN2v88internallsERSoRKNS0_20AsEscapedUC16ForJSONE@PLT
	cmpl	%ebx, -552(%rbp)
	setne	%al
	addq	$1, %rbx
	orb	%r13b, %al
	jne	.L161
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$2, %edx
	leaq	.LC33(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$2, %edx
	leaq	.LC34(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L193:
	movq	-464(%rbp), %rax
	movq	-592(%rbp), %rbx
	movq	%rax, %rdi
.L144:
	movq	-552(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L154
	call	_ZdlPv@PLT
.L154:
	movq	.LC43(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC45(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-584(%rbp), %rdi
	je	.L155
	call	_ZdlPv@PLT
.L155:
	movq	-576(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-560(%rbp), %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$2, %edx
	leaq	.LC35(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L192:
	movl	$2, %edx
	leaq	.LC38(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L190:
	movl	$2, %edx
	leaq	.LC36(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L146
.L191:
	movl	$2, %edx
	leaq	.LC37(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L146
.L142:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L143
.L186:
	movq	-432(%rbp), %rax
	movq	-552(%rbp), %rdi
	addq	-24(%rax), %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L139
.L141:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L143
.L185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20280:
	.size	_ZN2v88internal8compiler23JsonPrintFunctionSourceERSoiSt10unique_ptrIA_cSt14default_deleteIS4_EENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateENS8_INS0_18SharedFunctionInfoEEEb, .-_ZN2v88internal8compiler23JsonPrintFunctionSourceERSoiSt10unique_ptrIA_cSt14default_deleteIS4_EENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateENS8_INS0_18SharedFunctionInfoEEEb
	.section	.rodata._ZN2v88internal8compiler24GetVisualizerLogFileNameEPNS0_24OptimizedCompilationInfoEPKcS5_S5_.str1.1,"aMS",@progbits,1
.LC46:
	.string	"turbo-%s-%i"
.LC47:
	.string	"turbo-%p-%i"
.LC48:
	.string	"turbo-none-%i"
.LC49:
	.string	"%s"
.LC50:
	.string	"%s%c"
.LC51:
	.string	"%s%s.%s"
.LC52:
	.string	"%s%s-%s.%s"
.LC53:
	.string	"%s%s_%s.%s"
.LC54:
	.string	"%s%s_%s-%s.%s"
	.section	.text._ZN2v88internal8compiler24GetVisualizerLogFileNameEPNS0_24OptimizedCompilationInfoEPKcS5_S5_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler24GetVisualizerLogFileNameEPNS0_24OptimizedCompilationInfoEPKcS5_S5_
	.type	_ZN2v88internal8compiler24GetVisualizerLogFileNameEPNS0_24OptimizedCompilationInfoEPKcS5_S5_, @function
_ZN2v88internal8compiler24GetVisualizerLogFileNameEPNS0_24OptimizedCompilationInfoEPKcS5_S5_:
.LFB20290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	leaq	-1136(%rbp), %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	movl	$32, %ecx
	subq	$1160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -1152(%rbp)
	movq	$256, -1144(%rbp)
	rep stosq
	leaq	-1176(%rbp), %rdi
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movl	8(%r15), %r8d
	testl	%r8d, %r8d
	jne	.L253
	movq	-1176(%rbp), %rcx
	movl	120(%r15), %r8d
	cmpb	$0, (%rcx)
	jne	.L256
.L196:
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L198
	movq	(%rax), %rcx
	movq	-1152(%rbp), %rdi
	leaq	.LC47(%rip), %rdx
	xorl	%eax, %eax
	movq	-1144(%rbp), %rsi
	subq	$1, %rcx
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
.L197:
	leaq	-864(%rbp), %rdx
	movzbl	_ZN2v88internal21FLAG_trace_file_namesE(%rip), %r9d
	movl	$32, %ecx
	xorl	%eax, %eax
	movq	%rdx, %rdi
	movq	%rdx, -880(%rbp)
	rep stosq
	movq	$256, -872(%rbp)
	testb	%r9b, %r9b
	je	.L199
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L199
	movq	(%rax), %rdx
	movq	31(%rdx), %rdx
	movq	%rdx, %r10
	notq	%r10
	andl	$1, %r10d
	je	.L257
	.p2align 4,,10
	.p2align 3
.L199:
	testq	%rbx, %rbx
	movl	$1, %r11d
	sete	%r15b
	xorl	%r10d, %r10d
	movb	%r15b, -1185(%rbp)
.L202:
	movq	-1152(%rbp), %rax
	movslq	-1144(%rbp), %rdx
	addq	%rax, %rdx
	cmpq	%rax, %rdx
	je	.L213
	.p2align 4,,10
	.p2align 3
.L209:
	cmpb	$32, (%rax)
	jne	.L212
	movb	$95, (%rax)
.L212:
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L209
.L213:
	leaq	-592(%rbp), %rcx
	movq	$256, -600(%rbp)
	movq	%rcx, -608(%rbp)
	testq	%r13, %r13
	je	.L258
	movb	%r11b, -1187(%rbp)
	movb	%r10b, -1186(%rbp)
	call	_ZN2v84base2OS18DirectorySeparatorEv@PLT
	movq	-608(%rbp), %rdi
	movq	%r13, %rcx
	movq	-600(%rbp), %rsi
	movsbl	%al, %r8d
	leaq	.LC50(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-608(%rbp), %rcx
	movzbl	-1186(%rbp), %r10d
	movzbl	-1187(%rbp), %r11d
.L214:
	leaq	-320(%rbp), %rdi
	movq	-1152(%rbp), %r8
	movq	$256, -328(%rbp)
	movq	%rdi, -336(%rbp)
	testb	%r15b, %r15b
	jne	.L259
	testq	%rbx, %rbx
	je	.L217
	testb	%r11b, %r11b
	je	.L217
	subq	$8, %rsp
	movl	$256, %esi
	movq	%rbx, %r9
	xorl	%eax, %eax
	pushq	%r14
	leaq	.LC52(%rip), %rdx
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	popq	%rcx
	popq	%rsi
.L216:
	movl	-328(%rbp), %eax
	leal	1(%rax), %edi
	movslq	%edi, %rdi
	call	_Znam@PLT
	movslq	-328(%rbp), %rbx
	movq	-336(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rbx, %rdx
	call	memcpy@PLT
	movq	-1176(%rbp), %rdi
	movb	$0, (%rax,%rbx)
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L194
	call	_ZdaPv@PLT
.L194:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L260
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movq	-1176(%rbp), %rcx
	xorl	%r8d, %r8d
	cmpb	$0, (%rcx)
	je	.L196
.L256:
	movq	-1152(%rbp), %rdi
	movq	-1144(%rbp), %rsi
	leaq	.LC46(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L198:
	movq	-1152(%rbp), %rdi
	movq	-1144(%rbp), %rsi
	movl	%r8d, %ecx
	xorl	%eax, %eax
	leaq	.LC48(%rip), %rdx
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L258:
	movb	$0, -592(%rbp)
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L217:
	movq	-880(%rbp), %r9
	testb	%r10b, %r10b
	je	.L218
	cmpb	$0, -1185(%rbp)
	je	.L218
	subq	$8, %rsp
	leaq	.LC53(%rip), %rdx
	pushq	%r14
.L255:
	movq	-336(%rbp), %rdi
	movq	-328(%rbp), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	popq	%rax
	popq	%rdx
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L259:
	movq	%r14, %r9
	leaq	.LC51(%rip), %rdx
	movl	$256, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L218:
	pushq	%r14
	leaq	.LC54(%rip), %rdx
	pushq	%rbx
	jmp	.L255
.L257:
	movq	-1(%rdx), %rsi
	leaq	-1(%rdx), %rcx
	cmpw	$86, 11(%rsi)
	je	.L261
.L200:
	movq	(%rcx), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L199
	movq	(%rax), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L262
.L201:
	movq	15(%rax), %rax
	testq	%rbx, %rbx
	sete	-1185(%rbp)
	movzbl	-1185(%rbp), %esi
	movq	%rax, %r11
	notq	%r11
	movl	%esi, %r15d
	andl	$1, %r11d
	jne	.L202
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L223
	movq	%rax, -1168(%rbp)
	movl	11(%rax), %edi
	testl	%edi, %edi
	jg	.L263
.L204:
	movl	%r11d, %r10d
	movl	%r9d, %r11d
	jmp	.L202
.L261:
	movq	23(%rdx), %rcx
	testb	$1, %cl
	je	.L199
	subq	$1, %rcx
	jmp	.L200
.L262:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L201
	movq	23(%rax), %rax
	jmp	.L201
.L223:
	xorl	%r10d, %r10d
	movl	%r9d, %r11d
	jmp	.L202
.L263:
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movb	%r9b, -1186(%rbp)
	leaq	-1160(%rbp), %rdi
	leaq	-1168(%rbp), %rsi
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-880(%rbp), %rdi
	movq	-1160(%rbp), %rcx
	xorl	%eax, %eax
	movq	-872(%rbp), %rsi
	leaq	.LC49(%rip), %rdx
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-1160(%rbp), %rdi
	movzbl	-1186(%rbp), %r9d
	testq	%rdi, %rdi
	je	.L205
	call	_ZdaPv@PLT
	movzbl	-1186(%rbp), %r9d
.L205:
	movq	-880(%rbp), %rax
	movslq	-872(%rbp), %rdx
	addq	%rax, %rdx
	cmpq	%rax, %rdx
	je	.L208
.L206:
	cmpb	$47, (%rax)
	jne	.L207
	movb	$95, (%rax)
.L207:
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L206
.L208:
	movl	%r9d, %r11d
	xorl	%r15d, %r15d
	xorl	%r9d, %r9d
	jmp	.L204
.L260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20290:
	.size	_ZN2v88internal8compiler24GetVisualizerLogFileNameEPNS0_24OptimizedCompilationInfoEPKcS5_S5_, .-_ZN2v88internal8compiler24GetVisualizerLogFileNameEPNS0_24OptimizedCompilationInfoEPKcS5_S5_
	.section	.rodata._ZN2v88internal8compiler31get_cached_trace_turbo_filenameEPNS0_24OptimizedCompilationInfoE.str1.1,"aMS",@progbits,1
.LC55:
	.string	"json"
	.section	.text._ZN2v88internal8compiler31get_cached_trace_turbo_filenameEPNS0_24OptimizedCompilationInfoE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler31get_cached_trace_turbo_filenameEPNS0_24OptimizedCompilationInfoE
	.type	_ZN2v88internal8compiler31get_cached_trace_turbo_filenameEPNS0_24OptimizedCompilationInfoE, @function
_ZN2v88internal8compiler31get_cached_trace_turbo_filenameEPNS0_24OptimizedCompilationInfoE:
.LFB20250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	152(%rdi), %rax
	testq	%rax, %rax
	je	.L274
.L264:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L275
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	_ZN2v88internal21FLAG_trace_turbo_pathE(%rip), %rdx
	leaq	-32(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	.LC55(%rip), %r8
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler24GetVisualizerLogFileNameEPNS0_24OptimizedCompilationInfoEPKcS5_S5_
	movq	-32(%rbp), %rax
	movq	152(%rbx), %rdi
	movq	$0, -32(%rbp)
	movq	%rax, 152(%rbx)
	testq	%rdi, %rdi
	je	.L264
	call	_ZdaPv@PLT
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L273
	call	_ZdaPv@PLT
.L273:
	movq	152(%rbx), %rax
	jmp	.L264
.L275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20250:
	.size	_ZN2v88internal8compiler31get_cached_trace_turbo_filenameEPNS0_24OptimizedCompilationInfoE, .-_ZN2v88internal8compiler31get_cached_trace_turbo_filenameEPNS0_24OptimizedCompilationInfoE
	.section	.text._ZN2v88internal8compiler13TurboJsonFileC2EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13TurboJsonFileC2EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode
	.type	_ZN2v88internal8compiler13TurboJsonFileC2EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode, @function
_ZN2v88internal8compiler13TurboJsonFileC2EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode:
.LFB20252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	152(%rdx), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L288
.L277:
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	xorl	%esi, %esi
	leaq	8(%rbx), %r14
	movq	%rax, (%rbx)
	movq	-24(%rax), %rax
	movq	%rdx, (%rbx,%rax)
	movq	(%rbx), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8(%r12), %rax
	movq	32(%r12), %rdx
	movq	%r14, %rdi
	movq	%rax, (%rbx)
	movq	-24(%rax), %rax
	movq	%rdx, (%rbx,%rax)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEEC1Ev@PLT
	movq	(%rbx), %rax
	movq	%r14, %rsi
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	orl	$16, %edx
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	movq	%rbx, %rdi
	testq	%rax, %rax
	movq	(%rbx), %rax
	je	.L289
	addq	-24(%rax), %rdi
	xorl	%esi, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.L281:
	movq	(%r12), %rax
	movq	40(%r12), %rdx
	movq	%rax, (%rbx)
	movq	-24(%rax), %rax
	movq	%rdx, (%rbx,%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L290
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	addq	-24(%rax), %rdi
	movl	32(%rdi), %esi
	orl	$4, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L288:
	movq	%rdx, %r14
	movq	_ZN2v88internal21FLAG_trace_turbo_pathE(%rip), %rdx
	leaq	-64(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	.LC55(%rip), %r8
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler24GetVisualizerLogFileNameEPNS0_24OptimizedCompilationInfoEPKcS5_S5_
	movq	-64(%rbp), %r15
	movq	152(%r14), %rdi
	movq	$0, -64(%rbp)
	movq	%r15, 152(%r14)
	testq	%rdi, %rdi
	je	.L277
	call	_ZdaPv@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L287
	call	_ZdaPv@PLT
.L287:
	movq	152(%r14), %r15
	jmp	.L277
.L290:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20252:
	.size	_ZN2v88internal8compiler13TurboJsonFileC2EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode, .-_ZN2v88internal8compiler13TurboJsonFileC2EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode
	.section	.text._ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode
	.type	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode, @function
_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode:
.LFB20253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	248(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 464(%rbx)
	movq	%rax, 248(%rbx)
	xorl	%eax, %eax
	movw	%ax, 472(%rbx)
	movups	%xmm0, 480(%rbx)
	movups	%xmm0, 496(%rbx)
	movq	152(%r13), %r14
	testq	%r14, %r14
	je	.L303
.L292:
	leaq	24+_ZTCN2v88internal8compiler13TurboJsonFileE0_So(%rip), %rax
	xorl	%esi, %esi
	leaq	8(%rbx), %r13
	movq	%r15, %rdi
	movq	%rax, (%rbx)
	addq	$40, %rax
	movq	%rax, 248(%rbx)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	24+_ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, (%rbx)
	addq	$40, %rax
	movq	%rax, 248(%rbx)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEEC1Ev@PLT
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	%r12d, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	orl	$16, %edx
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	movq	%rbx, %rdi
	testq	%rax, %rax
	movq	(%rbx), %rax
	je	.L304
	addq	-24(%rax), %rdi
	xorl	%esi, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.L296:
	leaq	24+_ZTVN2v88internal8compiler13TurboJsonFileE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$40, %rax
	movq	%rax, 248(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L305
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	addq	-24(%rax), %rdi
	movl	32(%rdi), %esi
	orl	$4, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L303:
	movq	_ZN2v88internal21FLAG_trace_turbo_pathE(%rip), %rdx
	leaq	-64(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	.LC55(%rip), %r8
	call	_ZN2v88internal8compiler24GetVisualizerLogFileNameEPNS0_24OptimizedCompilationInfoEPKcS5_S5_
	movq	-64(%rbp), %r14
	movq	152(%r13), %rdi
	movq	$0, -64(%rbp)
	movq	%r14, 152(%r13)
	testq	%rdi, %rdi
	je	.L292
	call	_ZdaPv@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L302
	call	_ZdaPv@PLT
.L302:
	movq	152(%r13), %r14
	jmp	.L292
.L305:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20253:
	.size	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode, .-_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode
	.section	.rodata._ZN2v88internal8compiler19JSONGraphNodeWriter9PrintNodeEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC56:
	.string	"true"
.LC57:
	.string	"false"
.LC58:
	.string	",\n"
.LC59:
	.string	"{\"id\":"
.LC60:
	.string	",\"label\":\""
.LC61:
	.string	",\"title\":\""
.LC62:
	.string	",\"live\": "
.LC63:
	.string	"only_inputs_"
.LC64:
	.string	"Check failed: %s."
.LC65:
	.string	",\"rankInputs\":[0,"
.LC66:
	.string	",\"rankWithInput\":["
.LC67:
	.string	",\"rankInputs\":["
.LC68:
	.string	",\"rankInputs\":[0]"
.LC69:
	.string	", \"sourcePosition\" : "
.LC70:
	.string	", \"origin\" : "
.LC71:
	.string	",\"opcode\":\""
.LC72:
	.string	",\"control\":"
.LC73:
	.string	",\"type\":\""
.LC74:
	.string	",\"opinfo\":\""
.LC75:
	.string	" v "
.LC76:
	.string	" eff "
.LC77:
	.string	" ctrl in, "
.LC78:
	.string	" ctrl out\""
.LC79:
	.string	",\"properties\":\""
	.section	.text._ZN2v88internal8compiler19JSONGraphNodeWriter9PrintNodeEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler19JSONGraphNodeWriter9PrintNodeEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19JSONGraphNodeWriter9PrintNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19JSONGraphNodeWriter9PrintNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19JSONGraphNodeWriter9PrintNodeEPNS1_4NodeE:
.LFB20307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1800, %rsp
	movq	%rdi, -1704(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpb	$0, 200(%rdi)
	je	.L307
	movb	$0, 200(%rdi)
.L308:
	movq	.LC43(%rip), %xmm1
	leaq	-1472(%rbp), %r13
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r12
	movq	%r13, %rdi
	movq	%r13, -1816(%rbp)
	leaq	-1584(%rbp), %r15
	movhps	.LC44(%rip), %xmm1
	movaps	%xmm1, -1728(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, -1472(%rbp)
	movw	%dx, -1248(%rbp)
	movq	%r14, -1584(%rbp)
	movups	%xmm0, -1240(%rbp)
	movups	%xmm0, -1224(%rbp)
	movq	$0, -1256(%rbp)
	movq	-24(%r14), %rax
	movq	%rcx, -1584(%rbp,%rax)
	movq	-1584(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	-1520(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-1728(%rbp), %xmm1
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rdx, %rdi
	movaps	%xmm0, -1568(%rbp)
	movaps	%xmm1, -1584(%rbp)
	movq	%rax, -1472(%rbp)
	movaps	%xmm0, -1552(%rbp)
	movaps	%xmm0, -1536(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	-1488(%rbp), %rsi
	movq	%r13, %rdi
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rsi, -1800(%rbp)
	leaq	-1200(%rbp), %r13
	movq	%rsi, -1504(%rbp)
	leaq	-1576(%rbp), %rsi
	movq	%rdx, -1576(%rbp)
	movl	$16, -1512(%rbp)
	movq	$0, -1496(%rbp)
	movb	$0, -1488(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	-1088(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -1712(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r12, -1088(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r10
	leaq	-816(%rbp), %r12
	movw	%cx, -864(%rbp)
	movq	%r14, -1200(%rbp)
	movups	%xmm0, -856(%rbp)
	movups	%xmm0, -840(%rbp)
	movq	$0, -872(%rbp)
	movq	-24(%r14), %rax
	movq	%r10, -1200(%rbp,%rax)
	movq	-1200(%rbp), %rax
	movq	-24(%rax), %r9
	addq	%r13, %r9
	movq	%r9, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	-1136(%rbp), %r10
	pxor	%xmm0, %xmm0
	movdqa	-1728(%rbp), %xmm1
	movq	%r10, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r10, -1792(%rbp)
	movq	%rcx, -1088(%rbp)
	movaps	%xmm1, -1200(%rbp)
	movaps	%xmm0, -1184(%rbp)
	movaps	%xmm0, -1168(%rbp)
	movaps	%xmm0, -1152(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-1712(%rbp), %rdi
	leaq	-1104(%rbp), %r11
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-1192(%rbp), %rsi
	movq	%r11, -1824(%rbp)
	movq	%r11, -1120(%rbp)
	movq	%rdx, -1192(%rbp)
	movl	$16, -1128(%rbp)
	movq	$0, -1112(%rbp)
	movb	$0, -1104(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	-704(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -1736(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%si, -480(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r9
	xorl	%esi, %esi
	movq	%r14, -816(%rbp)
	movups	%xmm0, -472(%rbp)
	movups	%xmm0, -456(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -488(%rbp)
	movq	-24(%r14), %rax
	movq	%r9, -816(%rbp,%rax)
	movq	-816(%rbp), %rax
	movq	-24(%rax), %r9
	addq	%r12, %r9
	movq	%r9, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	-752(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1728(%rbp), %xmm1
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, %rdi
	movaps	%xmm0, -800(%rbp)
	movq	%rcx, -704(%rbp)
	movaps	%xmm1, -816(%rbp)
	movaps	%xmm0, -784(%rbp)
	movaps	%xmm0, -768(%rbp)
	movq	%rax, -1808(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-1736(%rbp), %rdi
	leaq	-720(%rbp), %rax
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-808(%rbp), %rsi
	movq	%rdx, -808(%rbp)
	movq	%rax, -1832(%rbp)
	movq	%rax, -736(%rbp)
	movl	$16, -744(%rbp)
	movq	$0, -728(%rbp)
	movb	$0, -720(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler8Operator12PrintPropsToERSo@PLT
	movq	-1704(%rbp), %rax
	movl	$6, %edx
	leaq	.LC59(%rip), %rsi
	movq	(%rax), %r12
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%rbx), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSolsEi@PLT
	movl	$10, %edx
	leaq	.LC60(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-1632(%rbp), %rax
	movq	$0, -1640(%rbp)
	leaq	-1648(%rbp), %rdi
	movq	%rax, -1776(%rbp)
	movq	%rax, -1648(%rbp)
	movq	-1536(%rbp), %rax
	movb	$0, -1632(%rbp)
	testq	%rax, %rax
	je	.L309
	movq	-1552(%rbp), %r8
	movq	-1544(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L428
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L311:
	movq	-1648(%rbp), %r15
	movq	-1640(%rbp), %r13
	leaq	-1680(%rbp), %rax
	movq	%rax, -1744(%rbp)
	addq	%r15, %r13
	cmpq	%r15, %r13
	jne	.L323
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L315:
	cmpb	$92, %al
	je	.L429
	cmpb	$8, %al
	je	.L430
	cmpb	$12, %al
	je	.L431
	cmpb	$10, %al
	je	.L432
	cmpb	$13, %al
	je	.L433
	cmpb	$9, %al
	je	.L434
	movq	-1744(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	movb	%al, -1680(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L316:
	addq	$1, %r15
	cmpq	%r15, %r13
	je	.L324
.L323:
	movzbl	(%r15), %eax
	cmpb	$34, %al
	jne	.L315
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	addq	$1, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%r15, %r13
	jne	.L323
	.p2align 4,,10
	.p2align 3
.L324:
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$10, %edx
	leaq	.LC61(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-1616(%rbp), %rax
	movq	$0, -1608(%rbp)
	movq	%rax, -1760(%rbp)
	leaq	-1600(%rbp), %rax
	movq	%rax, -1744(%rbp)
	movq	%rax, -1616(%rbp)
	movq	-1152(%rbp), %rax
	movb	$0, -1600(%rbp)
	testq	%rax, %rax
	je	.L435
	movq	-1168(%rbp), %r8
	movq	-1160(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L325
	subq	%rcx, %rax
	movq	-1760(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L326:
	movq	-1616(%rbp), %r15
	movq	-1608(%rbp), %r13
	leaq	-1680(%rbp), %rax
	movq	%rax, -1752(%rbp)
	addq	%r15, %r13
	cmpq	%r15, %r13
	jne	.L338
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L330:
	cmpb	$92, %al
	je	.L436
	cmpb	$8, %al
	je	.L437
	cmpb	$12, %al
	je	.L438
	cmpb	$10, %al
	je	.L439
	cmpb	$13, %al
	je	.L440
	cmpb	$9, %al
	je	.L441
	movq	-1752(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	movb	%al, -1680(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L331:
	addq	$1, %r15
	cmpq	%r15, %r13
	je	.L339
.L338:
	movzbl	(%r15), %eax
	cmpb	$34, %al
	jne	.L330
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	addq	$1, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%r15, %r13
	jne	.L338
	.p2align 4,,10
	.p2align 3
.L339:
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$9, %edx
	leaq	.LC62(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1704(%rbp), %rax
	cmpb	$0, 176(%rax)
	je	.L442
	movq	136(%rax), %rdi
	movq	%rax, %rdx
	movl	20(%rbx), %ecx
	leaq	.LC57(%rip), %r8
	movq	152(%rax), %rax
	movl	160(%rdx), %edx
	movl	%ecx, %esi
	movq	%rax, -1752(%rbp)
	subq	%rdi, %rax
	andl	$16777215, %esi
	leaq	(%rdx,%rax,8), %rax
	movl	$5, %edx
	cmpq	%rax, %rsi
	jnb	.L340
	shrq	$6, %rsi
	movl	$1, %eax
	salq	%cl, %rax
	andq	(%rdi,%rsi,8), %rax
	cmpq	$1, %rax
	sbbq	%rdx, %rdx
	notq	%rdx
	addq	$5, %rdx
	testq	%rax, %rax
	leaq	.LC56(%rip), %rax
	cmovne	%rax, %r8
.L340:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$15, %edx
	leaq	.LC79(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-432(%rbp), %rax
	movq	$0, -424(%rbp)
	movq	%rax, -1768(%rbp)
	leaq	-416(%rbp), %rax
	movq	%rax, -1840(%rbp)
	movq	%rax, -432(%rbp)
	movq	-768(%rbp), %rax
	movb	$0, -416(%rbp)
	testq	%rax, %rax
	je	.L443
	movq	-784(%rbp), %r8
	movq	-776(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L341
	subq	%rcx, %rax
	movq	-1768(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L342:
	movq	-432(%rbp), %rcx
	movq	-424(%rbp), %r13
	leaq	-1680(%rbp), %rax
	movq	%rax, -1752(%rbp)
	addq	%rcx, %r13
	movq	%rcx, %r15
	cmpq	%r13, %rcx
	jne	.L354
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L346:
	cmpb	$92, %al
	je	.L444
	cmpb	$8, %al
	je	.L445
	cmpb	$12, %al
	je	.L446
	cmpb	$10, %al
	je	.L447
	cmpb	$13, %al
	je	.L448
	cmpb	$9, %al
	je	.L449
	movq	-1752(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	movb	%al, -1680(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L347:
	addq	$1, %r15
	cmpq	%r15, %r13
	je	.L355
.L354:
	movzbl	(%r15), %eax
	cmpb	$34, %al
	jne	.L346
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	addq	$1, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%r15, %r13
	jne	.L354
	.p2align 4,,10
	.p2align 3
.L355:
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-432(%rbp), %rdi
	cmpq	-1840(%rbp), %rdi
	je	.L345
	call	_ZdlPv@PLT
.L345:
	movq	-1616(%rbp), %rdi
	cmpq	-1744(%rbp), %rdi
	je	.L356
	call	_ZdlPv@PLT
.L356:
	movq	-1648(%rbp), %rdi
	cmpq	-1776(%rbp), %rdi
	je	.L357
	call	_ZdlPv@PLT
.L357:
	movq	(%rbx), %rax
	movzwl	16(%rax), %r12d
	leal	-35(%r12), %eax
	cmpl	$1, %eax
	jbe	.L450
	leal	-4(%r12), %eax
	cmpl	$1, %eax
	jbe	.L398
	cmpl	$1, %r12d
	je	.L398
	cmpl	$2, %r12d
	je	.L451
.L359:
	movq	-1704(%rbp), %rax
	movq	184(%rax), %rdi
	testq	%rdi, %rdi
	je	.L362
	movq	%rbx, %rsi
	call	_ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE@PLT
	movq	%rax, -1680(%rbp)
	testb	$1, %al
	jne	.L363
	movq	%rax, %rdx
	shrq	$31, %rax
	shrq	%rdx
	movzwl	%ax, %eax
	andl	$1073741823, %edx
	orl	%eax, %edx
	jne	.L363
	.p2align 4,,10
	.p2align 3
.L362:
	movq	-1704(%rbp), %r15
	movq	192(%r15), %rsi
	testq	%rsi, %rsi
	je	.L365
	leaq	-1680(%rbp), %r12
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15NodeOriginTable13GetNodeOriginEPNS1_4NodeE@PLT
	cmpq	$0, -1656(%rbp)
	js	.L365
	movq	(%r15), %r13
	leaq	.LC70(%rip), %rsi
	movl	$13, %edx
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler10NodeOrigin9PrintJsonERSo@PLT
.L365:
	movq	-1704(%rbp), %rax
	movl	$11, %edx
	leaq	.LC71(%rip), %rsi
	movq	(%rax), %r13
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movzwl	16(%rax), %edi
	call	_ZN2v88internal8compiler8IrOpcode8MnemonicENS2_5ValueE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L452
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L368:
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1704(%rbp), %r15
	movl	$11, %edx
	leaq	.LC72(%rip), %rsi
	movq	(%r15), %r12
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	leaq	.LC56(%rip), %rsi
	movq	%r12, %rdi
	cmpw	$23, 16(%rax)
	sbbq	%rdx, %rdx
	addq	$5, %rdx
	cmpw	$23, 16(%rax)
	leaq	.LC57(%rip), %rax
	cmovnb	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %r12
	movl	$11, %edx
	leaq	.LC74(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	20(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$3, %edx
	leaq	.LC75(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	24(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$5, %edx
	leaq	.LC76(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	28(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$10, %edx
	leaq	.LC77(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	32(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$3, %edx
	leaq	.LC75(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movzbl	36(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$5, %edx
	leaq	.LC76(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	40(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$10, %edx
	leaq	.LC78(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rax
	testq	%rax, %rax
	jne	.L453
.L391:
	movq	-1704(%rbp), %rax
	movl	$1, %edx
	movq	.LC43(%rip), %xmm2
	leaq	.LC42(%rip), %rsi
	movq	(%rax), %rdi
	movhps	.LC45(%rip), %xmm2
	movaps	%xmm2, -1728(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movdqa	-1728(%rbp), %xmm2
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-736(%rbp), %rdi
	movq	%rax, -704(%rbp)
	movaps	%xmm2, -816(%rbp)
	cmpq	-1832(%rbp), %rdi
	je	.L387
	call	_ZdlPv@PLT
.L387:
	movq	-1808(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -808(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, -816(%rbp)
	movq	-24(%r14), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-1736(%rbp), %rdi
	movq	%rcx, -816(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -704(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movdqa	-1728(%rbp), %xmm3
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-1120(%rbp), %rdi
	movq	%rax, -1088(%rbp)
	movaps	%xmm3, -1200(%rbp)
	cmpq	-1824(%rbp), %rdi
	je	.L388
	call	_ZdlPv@PLT
.L388:
	movq	-1792(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1192(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, -1200(%rbp)
	movq	-24(%r14), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-1712(%rbp), %rdi
	movq	%rcx, -1200(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1088(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movdqa	-1728(%rbp), %xmm4
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-1504(%rbp), %rdi
	movq	%rax, -1472(%rbp)
	movaps	%xmm4, -1584(%rbp)
	cmpq	-1800(%rbp), %rdi
	je	.L389
	call	_ZdlPv@PLT
.L389:
	movq	-1784(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1576(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, -1584(%rbp)
	movq	-24(%r14), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-1816(%rbp), %rdi
	movq	%rcx, -1584(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1472(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L454
	addq	$1800, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L429:
	movl	$2, %edx
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L430:
	movl	$2, %edx
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L436:
	movl	$2, %edx
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L437:
	movl	$2, %edx
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L444:
	movl	$2, %edx
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L445:
	movl	$2, %edx
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L363:
	movq	-1704(%rbp), %rax
	movl	$21, %edx
	leaq	.LC69(%rip), %rsi
	movq	(%rax), %r12
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-1680(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal14SourcePosition9PrintJsonERSo@PLT
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L438:
	movl	$2, %edx
	leaq	.LC35(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L431:
	movl	$2, %edx
	leaq	.LC35(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L446:
	movl	$2, %edx
	leaq	.LC35(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L441:
	movl	$2, %edx
	leaq	.LC38(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L434:
	movl	$2, %edx
	leaq	.LC38(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L439:
	movl	$2, %edx
	leaq	.LC36(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L432:
	movl	$2, %edx
	leaq	.LC36(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L449:
	movl	$2, %edx
	leaq	.LC38(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L447:
	movl	$2, %edx
	leaq	.LC36(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L440:
	movl	$2, %edx
	leaq	.LC37(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L433:
	movl	$2, %edx
	leaq	.LC37(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L448:
	movl	$2, %edx
	leaq	.LC37(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L325:
	movq	-1760(%rbp), %rdi
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L341:
	movq	-1768(%rbp), %rdi
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L307:
	movq	(%rdi), %rdi
	movl	$2, %edx
	leaq	.LC58(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L453:
	leaq	-320(%rbp), %r12
	movq	%rax, -1680(%rbp)
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, -432(%rbp)
	movq	-1768(%rbp), %rbx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%r14), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1728(%rbp), %xmm5
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm5, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -1752(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -1768(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	-1680(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZNK2v88internal8compiler4Type7PrintToERSo@PLT
	movq	-1704(%rbp), %rax
	movl	$9, %edx
	leaq	.LC73(%rip), %rsi
	movq	(%rax), %r13
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1744(%rbp), %rax
	movq	$0, -1608(%rbp)
	movb	$0, -1600(%rbp)
	movq	%rax, -1616(%rbp)
	movq	-384(%rbp), %rax
	testq	%rax, %rax
	je	.L370
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L455
	movq	-1760(%rbp), %rdi
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L372:
	movq	-1616(%rbp), %rcx
	movq	-1608(%rbp), %rbx
	leaq	-1681(%rbp), %rax
	movq	%rax, -1728(%rbp)
	addq	%rcx, %rbx
	movq	%rcx, %r15
	cmpq	%rbx, %rcx
	jne	.L384
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L376:
	cmpb	$92, %al
	je	.L456
	cmpb	$8, %al
	je	.L457
	cmpb	$12, %al
	je	.L458
	cmpb	$10, %al
	je	.L459
	cmpb	$13, %al
	je	.L460
	cmpb	$9, %al
	je	.L461
	movq	-1728(%rbp), %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	movb	%al, -1681(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L377:
	addq	$1, %r15
	cmpq	%r15, %rbx
	je	.L385
.L384:
	movzbl	(%r15), %eax
	cmpb	$34, %al
	jne	.L376
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%r13, %rdi
	addq	$1, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%r15, %rbx
	jne	.L384
	.p2align 4,,10
	.p2align 3
.L385:
	movq	%r13, %rdi
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1616(%rbp), %rdi
	cmpq	-1744(%rbp), %rdi
	je	.L375
	call	_ZdlPv@PLT
.L375:
	movq	.LC43(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC45(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-1768(%rbp), %rdi
	je	.L386
	call	_ZdlPv@PLT
.L386:
	movq	-1752(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, -432(%rbp)
	movq	-24(%r14), %rax
	movq	%r12, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L452:
	movq	0(%r13), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L450:
	movq	-1704(%rbp), %r15
	movl	$17, %edx
	leaq	.LC65(%rip), %rsi
	movq	(%r15), %r12
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %r12
	movl	$18, %edx
	leaq	.LC66(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L398:
	movq	-1704(%rbp), %rax
	movl	$15, %edx
	leaq	.LC67(%rip), %rsi
	movq	(%rax), %r13
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	$2, %r12d
	jne	.L359
.L451:
	movq	-1704(%rbp), %rax
	movl	$17, %edx
	leaq	.LC68(%rip), %rsi
	movq	(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L455:
	subq	%rcx, %rax
	movq	-1760(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L456:
	movl	$2, %edx
	leaq	.LC33(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L457:
	movl	$2, %edx
	leaq	.LC34(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L458:
	movl	$2, %edx
	leaq	.LC35(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L461:
	movl	$2, %edx
	leaq	.LC38(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L459:
	movl	$2, %edx
	leaq	.LC36(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L435:
	movq	-1760(%rbp), %rdi
	leaq	-1120(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L309:
	leaq	-1504(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L443:
	movq	-1768(%rbp), %rdi
	leaq	-736(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L460:
	movl	$2, %edx
	leaq	.LC37(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L442:
	leaq	.LC63(%rip), %rsi
	leaq	.LC64(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L370:
	movq	-1760(%rbp), %rdi
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L372
.L454:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20307:
	.size	_ZN2v88internal8compiler19JSONGraphNodeWriter9PrintNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler19JSONGraphNodeWriter9PrintNodeEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_11GraphAsJSONE.str1.1,"aMS",@progbits,1
.LC80:
	.string	"effect"
.LC81:
	.string	"frame-state"
.LC82:
	.string	"control"
.LC83:
	.string	"context"
.LC84:
	.string	"value"
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_11GraphAsJSONE.str1.8,"aMS",@progbits,1
	.align 8
.LC85:
	.string	"../deps/v8/src/compiler/graph-visualizer.cc:409"
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_11GraphAsJSONE.str1.1
.LC86:
	.string	"{\n\"nodes\":["
.LC87:
	.string	"\n"
.LC88:
	.string	"],\n\"edges\":["
.LC89:
	.string	"]}"
.LC90:
	.string	"{\"source\":"
.LC91:
	.string	",\"target\":"
.LC92:
	.string	",\"index\":"
.LC93:
	.string	"\"}"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_11GraphAsJSONE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_11GraphAsJSONE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_11GraphAsJSONE, @function
_ZN2v88internal8compilerlsERSoRKNS1_11GraphAsJSONE:
.LFB20314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	leaq	.LC85(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-336(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$392, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal19AccountingAllocatorE(%rip), %rax
	movups	%xmm0, -360(%rbp)
	movq	%rax, -368(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -416(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movl	$11, %edx
	leaq	.LC86(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%rbx), %rax
	movq	(%rbx), %r15
	xorl	%ecx, %ecx
	movq	8(%rbx), %r12
	movq	%r13, %rsi
	movq	%r14, -272(%rbp)
	movq	%rax, -376(%rbp)
	leaq	-264(%rbp), %rax
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal8compiler8AllNodesC1EPNS0_4ZoneEPKNS1_5GraphEb@PLT
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal8compiler8AllNodesC1EPNS0_4ZoneEPKNS1_5GraphEb@PLT
	movq	%r12, %xmm0
	movq	-248(%rbp), %rax
	movq	-256(%rbp), %r12
	movhps	-376(%rbp), %xmm0
	movb	$1, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	cmpq	%rax, %r12
	je	.L467
	movq	%rax, -376(%rbp)
	leaq	-272(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L466:
	movq	(%r12), %rsi
	movq	%r15, %rdi
	addq	$8, %r12
	call	_ZN2v88internal8compiler19JSONGraphNodeWriter9PrintNodeEPNS1_4NodeE
	cmpq	%r12, -376(%rbp)
	jne	.L466
.L467:
	movq	-272(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$12, %edx
	leaq	.LC88(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	-384(%rbp), %rdi
	movq	%r14, -272(%rbp)
	call	_ZN2v88internal8compiler8AllNodesC1EPNS0_4ZoneEPKNS1_5GraphEb@PLT
	movq	-248(%rbp), %rax
	movq	-256(%rbp), %rcx
	movb	$1, -176(%rbp)
	movq	%rax, -392(%rbp)
	movq	%rcx, -376(%rbp)
	cmpq	%rax, %rcx
	je	.L465
	movq	%r14, -424(%rbp)
	movq	%r13, -432(%rbp)
	.p2align 4,,10
	.p2align 3
.L478:
	movq	-376(%rbp), %rax
	xorl	%ebx, %ebx
	movq	(%rax), %r12
	leaq	32(%r12), %rax
	movl	20(%r12), %edx
	movq	%rax, -408(%rbp)
	movq	%rax, -384(%rbp)
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L498:
	cmpl	%ebx, %eax
	jle	.L470
	movq	-384(%rbp), %rax
.L472:
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L473
	cmpb	$0, -176(%rbp)
	je	.L474
	movb	$0, -176(%rbp)
.L475:
	movq	%r12, %rdi
	movl	%ebx, %r14d
	leaq	.LC84(%rip), %r15
	call	_ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE@PLT
	cmpl	%ebx, %eax
	jle	.L497
.L476:
	movq	-272(%rbp), %r14
	movl	$10, %edx
	leaq	.LC90(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r14, %rdi
	andl	$16777215, %esi
	call	_ZNSolsEi@PLT
	movl	$10, %edx
	leaq	.LC91(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r12), %esi
	movq	%r13, %rdi
	andl	$16777215, %esi
	call	_ZNSolsEi@PLT
	movl	$9, %edx
	leaq	.LC92(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-396(%rbp), %esi
	movq	%r13, %rdi
	call	_ZNSolsEi@PLT
	movl	$9, %edx
	leaq	.LC73(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC93(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r12), %edx
.L473:
	addq	$8, -384(%rbp)
	addq	$1, %rbx
.L477:
	movl	%edx, %eax
	movl	%ebx, -396(%rbp)
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L498
	movq	-408(%rbp), %rax
	movq	(%rax), %rax
	cmpl	%ebx, 8(%rax)
	jle	.L470
	leaq	16(%rax,%rbx,8), %rax
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L470:
	addq	$8, -376(%rbp)
	movq	-376(%rbp), %rax
	cmpq	%rax, -392(%rbp)
	jne	.L478
	movq	-424(%rbp), %r14
	movq	-432(%rbp), %r13
.L465:
	movq	-272(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC89(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-416(%rbp), %rdi
	call	_ZN2v88internal19AccountingAllocatorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L499
	addq	$392, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	.LC83(%rip), %r15
	call	_ZN2v88internal8compiler14NodeProperties16PastContextIndexEPNS1_4NodeE@PLT
	cmpl	%ebx, %eax
	jg	.L476
	movq	%r12, %rdi
	leaq	.LC81(%rip), %r15
	call	_ZN2v88internal8compiler14NodeProperties19PastFrameStateIndexEPNS1_4NodeE@PLT
	cmpl	%r14d, %eax
	jg	.L476
	movq	%r12, %rdi
	leaq	.LC82(%rip), %r15
	call	_ZN2v88internal8compiler14NodeProperties15PastEffectIndexEPNS1_4NodeE@PLT
	leaq	.LC80(%rip), %r8
	cmpl	%r14d, %eax
	cmovg	%r8, %r15
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L474:
	movq	-272(%rbp), %rdi
	movl	$2, %edx
	leaq	.LC58(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L475
.L499:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20314:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_11GraphAsJSONE, .-_ZN2v88internal8compilerlsERSoRKNS1_11GraphAsJSONE
	.section	.text._ZN2v88internal8compiler17GraphC1Visualizer11PrintIndentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17GraphC1Visualizer11PrintIndentEv
	.type	_ZN2v88internal8compiler17GraphC1Visualizer11PrintIndentEv, @function
_ZN2v88internal8compiler17GraphC1Visualizer11PrintIndentEv:
.LFB20331:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L505
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	.LC12(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L502:
	movq	(%r12), %rdi
	movl	$2, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r12)
	jg	.L502
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L505:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE20331:
	.size	_ZN2v88internal8compiler17GraphC1Visualizer11PrintIndentEv, .-_ZN2v88internal8compiler17GraphC1Visualizer11PrintIndentEv
	.section	.text._ZN2v88internal8compiler17GraphC1VisualizerC2ERSoPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17GraphC1VisualizerC2ERSoPNS0_4ZoneE
	.type	_ZN2v88internal8compiler17GraphC1VisualizerC2ERSoPNS0_4ZoneE, @function
_ZN2v88internal8compiler17GraphC1VisualizerC2ERSoPNS0_4ZoneE:
.LFB20333:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movl	$0, 8(%rdi)
	movq	%rdx, 16(%rdi)
	ret
	.cfi_endproc
.LFE20333:
	.size	_ZN2v88internal8compiler17GraphC1VisualizerC2ERSoPNS0_4ZoneE, .-_ZN2v88internal8compiler17GraphC1VisualizerC2ERSoPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler17GraphC1VisualizerC1ERSoPNS0_4ZoneE
	.set	_ZN2v88internal8compiler17GraphC1VisualizerC1ERSoPNS0_4ZoneE,_ZN2v88internal8compiler17GraphC1VisualizerC2ERSoPNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler17GraphC1Visualizer19PrintStringPropertyEPKcS4_.str1.1,"aMS",@progbits,1
.LC94:
	.string	"\"\n"
	.section	.text._ZN2v88internal8compiler17GraphC1Visualizer19PrintStringPropertyEPKcS4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17GraphC1Visualizer19PrintStringPropertyEPKcS4_
	.type	_ZN2v88internal8compiler17GraphC1Visualizer19PrintStringPropertyEPKcS4_, @function
_ZN2v88internal8compiler17GraphC1Visualizer19PrintStringPropertyEPKcS4_:
.LFB20335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L510
	xorl	%ebx, %ebx
	leaq	.LC12(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L511:
	movq	(%r12), %rdi
	movl	$2, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r12)
	jg	.L511
.L510:
	movq	(%r12), %r12
	testq	%r15, %r15
	je	.L518
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L513:
	movl	$2, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r14, %r14
	je	.L519
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L515:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$2, %edx
	popq	%rbx
	leaq	.LC94(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L519:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L515
	.cfi_endproc
.LFE20335:
	.size	_ZN2v88internal8compiler17GraphC1Visualizer19PrintStringPropertyEPKcS4_, .-_ZN2v88internal8compiler17GraphC1Visualizer19PrintStringPropertyEPKcS4_
	.section	.text._ZN2v88internal8compiler17GraphC1Visualizer17PrintLongPropertyEPKcl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17GraphC1Visualizer17PrintLongPropertyEPKcl
	.type	_ZN2v88internal8compiler17GraphC1Visualizer17PrintLongPropertyEPKcl, @function
_ZN2v88internal8compiler17GraphC1Visualizer17PrintLongPropertyEPKcl:
.LFB20336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L521
	xorl	%ebx, %ebx
	leaq	.LC12(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L522:
	movq	(%r12), %rdi
	movl	$2, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r12)
	jg	.L522
.L521:
	movq	(%r12), %r12
	testq	%r15, %r15
	je	.L527
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L524:
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rax
	sarq	$63, %r14
	movq	%r12, %rdi
	movabsq	$2361183241434822607, %rdx
	imulq	%rdx
	sarq	$7, %rdx
	movq	%rdx, %rsi
	subq	%r14, %rsi
	call	_ZNSolsEi@PLT
	addq	$8, %rsp
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	popq	%rbx
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L527:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L524
	.cfi_endproc
.LFE20336:
	.size	_ZN2v88internal8compiler17GraphC1Visualizer17PrintLongPropertyEPKcl, .-_ZN2v88internal8compiler17GraphC1Visualizer17PrintLongPropertyEPKcl
	.section	.rodata._ZN2v88internal8compiler17GraphC1Visualizer18PrintBlockPropertyEPKci.str1.1,"aMS",@progbits,1
.LC95:
	.string	" \"B"
	.section	.text._ZN2v88internal8compiler17GraphC1Visualizer18PrintBlockPropertyEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17GraphC1Visualizer18PrintBlockPropertyEPKci
	.type	_ZN2v88internal8compiler17GraphC1Visualizer18PrintBlockPropertyEPKci, @function
_ZN2v88internal8compiler17GraphC1Visualizer18PrintBlockPropertyEPKci:
.LFB20337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L529
	xorl	%ebx, %ebx
	leaq	.LC12(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L530:
	movq	(%r12), %rdi
	movl	$2, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r12)
	jg	.L530
.L529:
	movq	(%r12), %r12
	testq	%r14, %r14
	je	.L535
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L532:
	movl	$3, %edx
	movq	%r12, %rdi
	leaq	.LC95(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	addq	$8, %rsp
	movl	$2, %edx
	leaq	.LC94(%rip), %rsi
	popq	%rbx
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L532
	.cfi_endproc
.LFE20337:
	.size	_ZN2v88internal8compiler17GraphC1Visualizer18PrintBlockPropertyEPKci, .-_ZN2v88internal8compiler17GraphC1Visualizer18PrintBlockPropertyEPKci
	.section	.text._ZN2v88internal8compiler17GraphC1Visualizer16PrintIntPropertyEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17GraphC1Visualizer16PrintIntPropertyEPKci
	.type	_ZN2v88internal8compiler17GraphC1Visualizer16PrintIntPropertyEPKci, @function
_ZN2v88internal8compiler17GraphC1Visualizer16PrintIntPropertyEPKci:
.LFB20338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L537
	xorl	%ebx, %ebx
	leaq	.LC12(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L538:
	movq	(%r12), %rdi
	movl	$2, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r12)
	jg	.L538
.L537:
	movq	(%r12), %r12
	testq	%r14, %r14
	je	.L543
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L540:
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	addq	$8, %rsp
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	popq	%rbx
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L540
	.cfi_endproc
.LFE20338:
	.size	_ZN2v88internal8compiler17GraphC1Visualizer16PrintIntPropertyEPKci, .-_ZN2v88internal8compiler17GraphC1Visualizer16PrintIntPropertyEPKci
	.section	.rodata._ZN2v88internal8compiler17GraphC1Visualizer16PrintCompilationEPKNS0_24OptimizedCompilationInfoE.str1.1,"aMS",@progbits,1
.LC96:
	.string	"begin_"
.LC97:
	.string	"compilation"
.LC98:
	.string	"name"
.LC99:
	.string	"method \""
.LC100:
	.string	"stub"
.LC101:
	.string	"method"
.LC102:
	.string	"date"
.LC103:
	.string	"end_"
	.section	.text._ZN2v88internal8compiler17GraphC1Visualizer16PrintCompilationEPKNS0_24OptimizedCompilationInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17GraphC1Visualizer16PrintCompilationEPKNS0_24OptimizedCompilationInfoE
	.type	_ZN2v88internal8compiler17GraphC1Visualizer16PrintCompilationEPKNS0_24OptimizedCompilationInfoE, @function
_ZN2v88internal8compiler17GraphC1Visualizer16PrintCompilationEPKNS0_24OptimizedCompilationInfoE:
.LFB20339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	8(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.L545
	xorl	%r12d, %r12d
	leaq	.LC12(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L546:
	movq	(%rbx), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %r12d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, 8(%rbx)
	jg	.L546
.L545:
	movq	(%rbx), %r12
	movl	$6, %edx
	leaq	.LC96(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$11, %edx
	leaq	.LC97(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$1, 8(%rbx)
	leaq	-48(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movl	8(%r13), %ecx
	movq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	leaq	.LC98(%rip), %rsi
	testl	%ecx, %ecx
	jne	.L547
	call	_ZN2v88internal8compiler17GraphC1Visualizer19PrintStringPropertyEPKcS4_
	movl	8(%rbx), %edx
	testl	%edx, %edx
	jle	.L548
	xorl	%r12d, %r12d
	leaq	.LC12(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L549:
	movq	(%rbx), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %r12d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, 8(%rbx)
	jg	.L549
.L548:
	movq	(%rbx), %r12
	movl	$8, %edx
	leaq	.LC99(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-48(%rbp), %r14
	testq	%r14, %r14
	je	.L567
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L551:
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	120(%r13), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC94(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L547:
	call	_ZN2v88internal8compiler17GraphC1Visualizer19PrintStringPropertyEPKcS4_
	leaq	.LC100(%rip), %rdx
	leaq	.LC101(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler17GraphC1Visualizer19PrintStringPropertyEPKcS4_
.L552:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*128(%rax)
	movl	8(%rbx), %eax
	cvttsd2siq	%xmm0, %r13
	testl	%eax, %eax
	jle	.L553
	xorl	%r12d, %r12d
	leaq	.LC12(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L554:
	movq	(%rbx), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %r12d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, 8(%rbx)
	jg	.L554
.L553:
	movq	(%rbx), %r12
	movl	$4, %edx
	leaq	.LC102(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rax
	sarq	$63, %r13
	movq	%r12, %rdi
	movabsq	$2361183241434822607, %rdx
	imulq	%rdx
	sarq	$7, %rdx
	subq	%r13, %rdx
	movq	%rdx, %rsi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L555
	call	_ZdaPv@PLT
.L555:
	movl	8(%rbx), %eax
	subl	$1, %eax
	movl	%eax, 8(%rbx)
	testl	%eax, %eax
	jle	.L556
	xorl	%r12d, %r12d
	leaq	.LC12(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L557:
	movq	(%rbx), %rdi
	movl	$2, %edx
	movq	%r13, %rsi
	addl	$1, %r12d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, 8(%rbx)
	jg	.L557
.L556:
	movq	(%rbx), %r12
	movl	$4, %edx
	leaq	.LC103(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$11, %edx
	leaq	.LC97(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L568
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L551
.L568:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20339:
	.size	_ZN2v88internal8compiler17GraphC1Visualizer16PrintCompilationEPKNS0_24OptimizedCompilationInfoE, .-_ZN2v88internal8compiler17GraphC1Visualizer16PrintCompilationEPKNS0_24OptimizedCompilationInfoE
	.section	.rodata._ZN2v88internal8compiler17GraphC1Visualizer11PrintNodeIdEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC104:
	.string	"n"
	.section	.text._ZN2v88internal8compiler17GraphC1Visualizer11PrintNodeIdEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17GraphC1Visualizer11PrintNodeIdEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17GraphC1Visualizer11PrintNodeIdEPNS1_4NodeE, @function
_ZN2v88internal8compiler17GraphC1Visualizer11PrintNodeIdEPNS1_4NodeE:
.LFB20340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rsi, %rbx
	leaq	.LC104(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%rbx, %rbx
	je	.L571
	movl	20(%rbx), %esi
	andl	$16777215, %esi
.L570:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSolsEi@PLT
.L571:
	.cfi_restore_state
	movl	$-1, %esi
	jmp	.L570
	.cfi_endproc
.LFE20340:
	.size	_ZN2v88internal8compiler17GraphC1Visualizer11PrintNodeIdEPNS1_4NodeE, .-_ZN2v88internal8compiler17GraphC1Visualizer11PrintNodeIdEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler17GraphC1Visualizer11PrintInputsEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC105:
	.string	" Ctx:"
.LC106:
	.string	" FS:"
.LC107:
	.string	" Eff:"
.LC108:
	.string	" Ctrl:"
	.section	.text._ZN2v88internal8compiler17GraphC1Visualizer11PrintInputsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17GraphC1Visualizer11PrintInputsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17GraphC1Visualizer11PrintInputsEPNS1_4NodeE, @function
_ZN2v88internal8compiler17GraphC1Visualizer11PrintInputsEPNS1_4NodeE:
.LFB20343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$40, %rsp
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L574
	movq	32(%rsi), %rax
	leaq	16(%rax), %rbx
.L574:
	movq	0(%r13), %rdi
	movl	20(%rdi), %r15d
	testl	%r15d, %r15d
	jg	.L606
.L575:
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	je	.L580
	movq	(%r12), %rdi
	movl	$5, %edx
	leaq	.LC105(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rdi
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %r14
	movq	(%rbx), %r15
	movl	$1, %edx
	leaq	.LC104(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r15, %r15
	je	.L594
	movl	20(%r15), %esi
	andl	$16777215, %esi
.L581:
	movq	%r14, %rdi
	addq	$8, %rbx
	call	_ZNSolsEi@PLT
.L580:
	movq	0(%r13), %rdi
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	je	.L582
	movq	(%r12), %rdi
	movl	$4, %edx
	leaq	.LC106(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rdi
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %r14
	movq	(%rbx), %r15
	movl	$1, %edx
	leaq	.LC104(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r15, %r15
	je	.L595
	movl	20(%r15), %esi
	andl	$16777215, %esi
.L583:
	movq	%r14, %rdi
	addq	$8, %rbx
	call	_ZNSolsEi@PLT
.L582:
	movq	0(%r13), %rax
	movl	24(%rax), %r15d
	testl	%r15d, %r15d
	jg	.L607
.L584:
	movl	28(%rax), %r13d
	testl	%r13d, %r13d
	jg	.L608
.L573:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L606:
	.cfi_restore_state
	movq	(%r12), %rdi
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rbx, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leal	-1(%r15), %eax
	leaq	.LC11(%rip), %r15
	movl	%eax, -76(%rbp)
	leaq	8(%rbx,%rax,8), %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L579:
	movq	(%r12), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rcx
	movq	(%r12), %rdi
	movl	$1, %edx
	leaq	.LC104(%rip), %rsi
	movq	%rcx, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdi
	testq	%rcx, %rcx
	je	.L576
	movl	20(%rcx), %esi
	addq	$8, %r14
	andl	$16777215, %esi
	call	_ZNSolsEi@PLT
	cmpq	-72(%rbp), %r14
	jne	.L579
.L577:
	movslq	-76(%rbp), %rax
	movq	0(%r13), %rdi
	leaq	8(%rbx,%rax,8), %rbx
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L608:
	movq	(%r12), %rdi
	movl	$6, %edx
	leaq	.LC108(%rip), %rsi
	leaq	.LC11(%rip), %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leal	-1(%r13), %eax
	leaq	8(%rbx,%rax,8), %rax
	movq	%rax, -56(%rbp)
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L609:
	movl	20(%r13), %esi
	andl	$16777215, %esi
.L605:
	movq	%r15, %rdi
	addq	$8, %rbx
	call	_ZNSolsEi@PLT
	cmpq	%rbx, -56(%rbp)
	je	.L573
.L593:
	movq	(%r12), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %r15
	movq	(%rbx), %r13
	movl	$1, %edx
	leaq	.LC104(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r13, %r13
	jne	.L609
	movl	$-1, %esi
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L607:
	movq	(%r12), %rdi
	movl	$5, %edx
	leaq	.LC107(%rip), %rsi
	movq	%rbx, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leal	-1(%r15), %eax
	leaq	.LC11(%rip), %r15
	movl	%eax, -76(%rbp)
	leaq	8(%rbx,%rax,8), %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L588:
	movq	(%r12), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rcx
	movq	(%r12), %rdi
	movl	$1, %edx
	leaq	.LC104(%rip), %rsi
	movq	%rcx, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdi
	testq	%rcx, %rcx
	je	.L585
	movl	20(%rcx), %esi
	addq	$8, %r14
	andl	$16777215, %esi
	call	_ZNSolsEi@PLT
	cmpq	-72(%rbp), %r14
	jne	.L588
.L586:
	movslq	-76(%rbp), %rax
	leaq	8(%rbx,%rax,8), %rbx
	movq	0(%r13), %rax
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L576:
	movl	$-1, %esi
	addq	$8, %r14
	call	_ZNSolsEi@PLT
	cmpq	%r14, -72(%rbp)
	jne	.L579
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L595:
	movl	$-1, %esi
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L594:
	movl	$-1, %esi
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L585:
	movl	$-1, %esi
	addq	$8, %r14
	call	_ZNSolsEi@PLT
	cmpq	%r14, -72(%rbp)
	jne	.L588
	jmp	.L586
	.cfi_endproc
.LFE20343:
	.size	_ZN2v88internal8compiler17GraphC1Visualizer11PrintInputsEPNS1_4NodeE, .-_ZN2v88internal8compiler17GraphC1Visualizer11PrintInputsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17GraphC1Visualizer9PrintNodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17GraphC1Visualizer9PrintNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17GraphC1Visualizer9PrintNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler17GraphC1Visualizer9PrintNodeEPNS1_4NodeE:
.LFB20341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	leaq	.LC104(%rip), %rsi
	subq	$8, %rsp
	movq	(%rdi), %r14
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r12, %r12
	je	.L612
	movl	20(%r12), %esi
	andl	$16777215, %esi
.L611:
	movq	%r14, %rdi
	call	_ZNSolsEi@PLT
	movq	0(%r13), %r14
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC11(%rip), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler17GraphC1Visualizer11PrintInputsEPNS1_4NodeE
.L612:
	.cfi_restore_state
	movl	$-1, %esi
	jmp	.L611
	.cfi_endproc
.LFE20341:
	.size	_ZN2v88internal8compiler17GraphC1Visualizer9PrintNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler17GraphC1Visualizer9PrintNodeEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler17GraphC1Visualizer9PrintTypeEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC109:
	.string	" type:"
	.section	.text._ZN2v88internal8compiler17GraphC1Visualizer9PrintTypeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17GraphC1Visualizer9PrintTypeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17GraphC1Visualizer9PrintTypeEPNS1_4NodeE, @function
_ZN2v88internal8compiler17GraphC1Visualizer9PrintTypeEPNS1_4NodeE:
.LFB20344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rsi), %r12
	testq	%r12, %r12
	jne	.L617
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L617:
	.cfi_restore_state
	movq	(%rdi), %r13
	leaq	.LC109(%rip), %rsi
	movl	$6, %edx
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compilerlsERSoNS1_4TypeE@PLT
	.cfi_endproc
.LFE20344:
	.size	_ZN2v88internal8compiler17GraphC1Visualizer9PrintTypeEPNS1_4NodeE, .-_ZN2v88internal8compiler17GraphC1Visualizer9PrintTypeEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler17GraphC1Visualizer13PrintScheduleEPKcPKNS1_8ScheduleEPKNS1_19SourcePositionTableEPKNS1_19InstructionSequenceE.str1.1,"aMS",@progbits,1
.LC110:
	.string	"cfg"
.LC111:
	.string	"block"
.LC112:
	.string	"from_bci"
.LC113:
	.string	"to_bci"
.LC114:
	.string	"predecessors"
.LC115:
	.string	"successors"
.LC116:
	.string	"xhandlers\n"
.LC117:
	.string	"flags\n"
.LC118:
	.string	"dominator"
.LC119:
	.string	"loop_depth"
	.section	.rodata._ZN2v88internal8compiler17GraphC1Visualizer13PrintScheduleEPKcPKNS1_8ScheduleEPKNS1_19SourcePositionTableEPKNS1_19InstructionSequenceE.str1.8,"aMS",@progbits,1
	.align 8
.LC120:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.rodata._ZN2v88internal8compiler17GraphC1Visualizer13PrintScheduleEPKcPKNS1_8ScheduleEPKNS1_19SourcePositionTableEPKNS1_19InstructionSequenceE.str1.1
.LC121:
	.string	"first_lir_id"
.LC122:
	.string	"last_lir_id"
.LC123:
	.string	"states"
.LC124:
	.string	"locals"
.LC125:
	.string	"size"
.LC126:
	.string	"None"
.LC127:
	.string	"]\n"
.LC128:
	.string	"HIR"
.LC129:
	.string	"0 "
.LC130:
	.string	"inlining("
.LC131:
	.string	"),"
.LC132:
	.string	" <|@\n"
.LC133:
	.string	"0 0 "
.LC134:
	.string	" Goto"
.LC135:
	.string	" ->"
.LC136:
	.string	"LIR"
.LC137:
	.string	" pos:"
	.section	.text._ZN2v88internal8compiler17GraphC1Visualizer13PrintScheduleEPKcPKNS1_8ScheduleEPKNS1_19SourcePositionTableEPKNS1_19InstructionSequenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17GraphC1Visualizer13PrintScheduleEPKcPKNS1_8ScheduleEPKNS1_19SourcePositionTableEPKNS1_19InstructionSequenceE
	.type	_ZN2v88internal8compiler17GraphC1Visualizer13PrintScheduleEPKcPKNS1_8ScheduleEPKNS1_19SourcePositionTableEPKNS1_19InstructionSequenceE, @function
_ZN2v88internal8compiler17GraphC1Visualizer13PrintScheduleEPKcPKNS1_8ScheduleEPKNS1_19SourcePositionTableEPKNS1_19InstructionSequenceE:
.LFB20345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.LC12(%rip), %rbx
	subq	$72, %rsp
	movl	8(%rdi), %r14d
	movq	%rdx, -112(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%r8, -104(%rbp)
	testl	%r14d, %r14d
	jle	.L622
	.p2align 4,,10
	.p2align 3
.L619:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%rbx, %rsi
	addl	$1, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, 8(%r15)
	jg	.L619
.L622:
	movq	(%r15), %r13
	movl	$6, %edx
	leaq	.LC96(%rip), %rsi
	leaq	.LC12(%rip), %r14
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC110(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$1, 8(%r15)
	movq	%r12, %rdx
	movq	%r15, %rdi
	leaq	.LC98(%rip), %rsi
	call	_ZN2v88internal8compiler17GraphC1Visualizer19PrintStringPropertyEPKcS4_
	movq	-112(%rbp), %rax
	movq	$0, -96(%rbp)
	movq	80(%rax), %rdx
	cmpq	88(%rax), %rdx
	je	.L621
	.p2align 4,,10
	.p2align 3
.L620:
	movq	-96(%rbp), %rax
	movl	8(%r15), %r13d
	xorl	%ebx, %ebx
	movq	(%rdx,%rax,8), %rax
	movq	%rax, -56(%rbp)
	testl	%r13d, %r13d
	jle	.L628
	.p2align 4,,10
	.p2align 3
.L625:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L625
.L628:
	movq	(%r15), %r12
	movl	$6, %edx
	leaq	.LC96(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$5, %edx
	leaq	.LC111(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%r15), %eax
	movq	-56(%rbp), %rcx
	addl	$1, %eax
	movl	%eax, 8(%r15)
	movl	4(%rcx), %r12d
	testl	%eax, %eax
	jle	.L626
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L627:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L627
.L626:
	movq	(%r15), %r13
	movl	$4, %edx
	leaq	.LC98(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC95(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC94(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%r15), %r12d
	testl	%r12d, %r12d
	jle	.L629
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L630:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L630
.L629:
	movq	(%r15), %r12
	movl	$8, %edx
	leaq	.LC112(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%r15), %ebx
	testl	%ebx, %ebx
	jle	.L631
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L632:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L632
.L631:
	movq	(%r15), %r12
	movl	$6, %edx
	leaq	.LC113(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%r15), %r11d
	testl	%r11d, %r11d
	jle	.L633
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L634:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L634
.L633:
	movq	(%r15), %rdi
	movl	$12, %edx
	leaq	.LC114(%rip), %rsi
	leaq	.LC95(%rip), %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	movq	144(%rax), %rcx
	movq	136(%rax), %r13
	movq	%rcx, -72(%rbp)
	cmpq	%rcx, %r13
	je	.L636
	movq	%r15, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L639:
	movq	-64(%rbp), %rax
	movq	0(%r13), %rbx
	movl	$3, %edx
	movq	%r12, %rsi
	addq	$8, %r13
	movq	(%rax), %r15
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%rbx), %esi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%r13, -72(%rbp)
	jne	.L639
	movq	-64(%rbp), %r15
.L636:
	movq	(%r15), %rdi
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%r15), %r10d
	testl	%r10d, %r10d
	jle	.L637
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L638:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	8(%r15), %ebx
	jl	.L638
.L637:
	movq	(%r15), %rdi
	movl	$10, %edx
	leaq	.LC115(%rip), %rsi
	leaq	.LC95(%rip), %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	movq	112(%rax), %rcx
	movq	104(%rax), %r13
	movq	%rcx, -72(%rbp)
	cmpq	%rcx, %r13
	je	.L641
	movq	%r15, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L644:
	movq	-64(%rbp), %rax
	movq	0(%r13), %rbx
	movl	$3, %edx
	movq	%r12, %rsi
	addq	$8, %r13
	movq	(%rax), %r15
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%rbx), %esi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%r13, -72(%rbp)
	jne	.L644
	movq	-64(%rbp), %r15
.L641:
	movq	(%r15), %rdi
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%r15), %r9d
	testl	%r9d, %r9d
	jle	.L642
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L643:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L643
.L642:
	movq	(%r15), %rdi
	movl	$10, %edx
	leaq	.LC116(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%r15), %r8d
	testl	%r8d, %r8d
	jle	.L645
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L646:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	8(%r15), %ebx
	jl	.L646
.L645:
	movq	(%r15), %rdi
	movl	$6, %edx
	leaq	.LC117(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L648
	movl	4(%rax), %edx
	leaq	.LC118(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17GraphC1Visualizer18PrintBlockPropertyEPKci
.L648:
	movq	-56(%rbp), %rbx
	leaq	.LC119(%rip), %rsi
	movq	%r15, %rdi
	movl	48(%rbx), %edx
	call	_ZN2v88internal8compiler17GraphC1Visualizer16PrintIntPropertyEPKci
	movq	-104(%rbp), %rax
	movslq	4(%rbx), %rsi
	movq	16(%rax), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L778
	movq	(%rax,%rsi,8), %rcx
	movl	112(%rcx), %eax
	movq	%rcx, -72(%rbp)
	testl	%eax, %eax
	js	.L650
	movl	116(%rcx), %ecx
	leal	0(,%rax,4), %edx
	leaq	.LC121(%rip), %rsi
	movq	%r15, %rdi
	leal	-1(%rcx), %ebx
	movl	%ecx, -64(%rbp)
	call	_ZN2v88internal8compiler17GraphC1Visualizer16PrintIntPropertyEPKci
	leal	2(,%rbx,4), %edx
	leaq	.LC122(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17GraphC1Visualizer16PrintIntPropertyEPKci
.L650:
	movl	8(%r15), %edi
	xorl	%ebx, %ebx
	testl	%edi, %edi
	jle	.L654
	.p2align 4,,10
	.p2align 3
.L651:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L651
.L654:
	movq	(%r15), %r12
	movl	$6, %edx
	leaq	.LC96(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$6, %edx
	leaq	.LC123(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%r15), %eax
	addl	$1, %eax
	movl	%eax, 8(%r15)
	testl	%eax, %eax
	jle	.L652
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L653:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L653
.L652:
	movq	(%r15), %r12
	movl	$6, %edx
	leaq	.LC96(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$6, %edx
	leaq	.LC124(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	addl	$1, 8(%r15)
	xorl	%edx, %edx
	movq	72(%rax), %rcx
	movq	80(%rax), %rsi
	movq	%rcx, -64(%rbp)
	movq	%rcx, %rax
	cmpq	%rsi, %rcx
	je	.L656
	.p2align 4,,10
	.p2align 3
.L658:
	movq	(%rax), %rcx
	movq	(%rcx), %rcx
	cmpw	$35, 16(%rcx)
	sete	%cl
	addq	$8, %rax
	movzbl	%cl, %ecx
	addl	%ecx, %edx
	cmpq	%rax, %rsi
	jne	.L658
.L656:
	leaq	.LC125(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17GraphC1Visualizer16PrintIntPropertyEPKci
	leaq	.LC126(%rip), %rdx
	leaq	.LC101(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17GraphC1Visualizer19PrintStringPropertyEPKcS4_
	movq	-56(%rbp), %rax
	movl	8(%r15), %ecx
	movq	72(%rax), %rbx
	movq	80(%rax), %rdx
	cmpq	%rdx, %rbx
	je	.L666
	xorl	%r12d, %r12d
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L660:
	addq	$8, %rbx
	cmpq	%rbx, %rdx
	je	.L666
.L665:
	movq	(%rbx), %rax
	movq	(%rax), %rax
	cmpw	$35, 16(%rax)
	jne	.L660
	xorl	%r13d, %r13d
	testl	%ecx, %ecx
	jle	.L664
	.p2align 4,,10
	.p2align 3
.L661:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, 8(%r15)
	jg	.L661
.L664:
	movq	(%r15), %rdi
	movl	%r12d, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %r13
	movq	(%rbx), %rcx
	movl	$1, %edx
	leaq	.LC104(%rip), %rsi
	movq	%r13, %rdi
	movq	%rcx, -64(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-64(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L779
	movl	20(%rcx), %esi
	andl	$16777215, %esi
.L663:
	movq	%r13, %rdi
	addq	$8, %rbx
	addl	$1, %r12d
	call	_ZNSolsEi@PLT
	movq	(%r15), %rdi
	movl	$2, %edx
	leaq	.LC20(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-8(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17GraphC1Visualizer11PrintInputsEPNS1_4NodeE
	movq	(%r15), %rdi
	movl	$2, %edx
	leaq	.LC127(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	movl	8(%r15), %ecx
	movq	80(%rax), %rdx
	cmpq	%rbx, %rdx
	jne	.L665
.L666:
	subl	$1, %ecx
	xorl	%ebx, %ebx
	movl	%ecx, 8(%r15)
	testl	%ecx, %ecx
	jle	.L670
	.p2align 4,,10
	.p2align 3
.L667:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L667
.L670:
	movq	(%r15), %r12
	movl	$4, %edx
	leaq	.LC103(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$6, %edx
	leaq	.LC124(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%r15), %eax
	subl	$1, %eax
	movl	%eax, 8(%r15)
	testl	%eax, %eax
	jle	.L668
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L669:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L669
.L668:
	movq	(%r15), %r12
	movl	$4, %edx
	leaq	.LC103(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$6, %edx
	leaq	.LC123(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC87(%rip), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%r15), %esi
	testl	%esi, %esi
	jle	.L671
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L672:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L672
.L671:
	movq	(%r15), %r12
	movl	$6, %edx
	leaq	.LC96(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC128(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%r15), %eax
	movq	-56(%rbp), %rcx
	addl	$1, %eax
	movl	%eax, 8(%r15)
	movq	80(%rcx), %rdx
	movq	72(%rcx), %rbx
	cmpq	%rbx, %rdx
	jne	.L686
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L675:
	addq	$8, %rbx
	cmpq	%rbx, %rdx
	je	.L780
.L686:
	movq	(%rbx), %r12
	movq	(%r12), %rax
	cmpw	$35, 16(%rax)
	je	.L675
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZNK2v88internal8compiler4Node8UseCountEv@PLT
	movl	8(%r15), %ecx
	movl	%eax, -64(%rbp)
	testl	%ecx, %ecx
	jle	.L679
	.p2align 4,,10
	.p2align 3
.L676:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, 8(%r15)
	jg	.L676
.L679:
	movq	(%r15), %rdi
	movl	$2, %edx
	leaq	.LC129(%rip), %rsi
	movq	%rdi, -80(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-64(%rbp), %esi
	movq	-80(%rbp), %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %r13
	movl	$1, %edx
	leaq	.LC104(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r12), %esi
	movq	%r13, %rdi
	andl	$16777215, %esi
	call	_ZNSolsEi@PLT
	movq	(%r15), %r13
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17GraphC1Visualizer11PrintInputsEPNS1_4NodeE
	cmpb	$0, _ZN2v88internal22FLAG_trace_turbo_typesE(%rip)
	jne	.L781
.L680:
	cmpq	$0, -88(%rbp)
	je	.L776
	movq	-88(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE@PLT
	movq	%rax, %r12
	testb	$1, %al
	jne	.L683
	movq	%rax, %rsi
	shrq	$31, %r12
	movq	(%r15), %rdi
	shrq	%rsi
	movzwl	%r12w, %r13d
	movl	%esi, %r12d
	andl	$1073741823, %r12d
	jne	.L684
	testl	%r13d, %r13d
	je	.L682
	movl	$5, %edx
	leaq	.LC137(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L718:
	movq	(%r15), %rdi
	movl	$9, %edx
	leaq	.LC130(%rip), %rsi
	movq	%rdi, -64(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-64(%rbp), %rdi
	leal	-1(%r13), %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC131(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L717:
	movq	(%r15), %rdi
	leal	-1(%r12), %esi
	call	_ZNSolsEi@PLT
.L776:
	movq	(%r15), %rdi
.L682:
	movl	$5, %edx
	leaq	.LC132(%rip), %rsi
	addq	$8, %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	movq	80(%rax), %rdx
	cmpq	%rbx, %rdx
	jne	.L686
.L780:
	movl	8(%r15), %eax
.L674:
	movq	-56(%rbp), %rcx
	movl	52(%rcx), %edx
	testl	%edx, %edx
	jne	.L782
.L687:
	subl	$1, %eax
	xorl	%ebx, %ebx
	movl	%eax, 8(%r15)
	testl	%eax, %eax
	jle	.L702
	.p2align 4,,10
	.p2align 3
.L699:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L699
.L702:
	movq	(%r15), %r12
	movl	$4, %edx
	leaq	.LC103(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC128(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%r15), %eax
	testl	%eax, %eax
	jle	.L700
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L701:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L701
.L700:
	movq	(%r15), %r12
	movl	$6, %edx
	leaq	.LC96(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$3, %edx
	leaq	.LC136(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC87(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%r15), %edx
	movq	-72(%rbp), %rcx
	leal	1(%rdx), %eax
	movl	%eax, 8(%r15)
	movl	112(%rcx), %r12d
	cmpl	116(%rcx), %r12d
	jge	.L703
	movslq	%r12d, %rbx
	movq	-104(%rbp), %r13
	movq	%rbx, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L704:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.L708
	.p2align 4,,10
	.p2align 3
.L705:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L705
.L708:
	movq	(%r15), %rdi
	movl	%r12d, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	208(%r13), %rdx
	movq	-64(%rbp), %rdi
	movq	%rdx, %rax
	subq	216(%r13), %rax
	sarq	$3, %rax
	addq	-56(%rbp), %rax
	js	.L783
	cmpq	$63, %rax
	jg	.L709
	movq	-56(%rbp), %rax
	leaq	(%rdx,%rax,8), %rax
.L710:
	movq	(%rax), %rsi
	addl	$1, %r12d
	call	_ZN2v88internal8compilerlsERSoRKNS1_11InstructionE@PLT
	movl	$5, %edx
	leaq	.LC132(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %rax
	addq	$1, -56(%rbp)
	cmpl	116(%rax), %r12d
	movl	8(%r15), %eax
	jl	.L704
	leal	-1(%rax), %edx
.L703:
	movl	%edx, 8(%r15)
	xorl	%ebx, %ebx
	testl	%edx, %edx
	jle	.L716
	.p2align 4,,10
	.p2align 3
.L713:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L713
.L716:
	movq	(%r15), %r12
	movl	$4, %edx
	leaq	.LC103(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC136(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%r15), %eax
	subl	$1, %eax
	movl	%eax, 8(%r15)
	testl	%eax, %eax
	jle	.L714
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L715:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L715
.L714:
	movq	(%r15), %r12
	movl	$4, %edx
	leaq	.LC103(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$5, %edx
	leaq	.LC111(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-112(%rbp), %rax
	addq	$1, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	80(%rax), %rdx
	movq	88(%rax), %rax
	movq	%rax, -56(%rbp)
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	ja	.L620
.L621:
	movl	8(%r15), %eax
	subl	$1, %eax
	movl	%eax, 8(%r15)
	testl	%eax, %eax
	jle	.L623
	xorl	%ebx, %ebx
	leaq	.LC12(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L624:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L624
.L623:
	movq	(%r15), %r12
	movl	$4, %edx
	leaq	.LC103(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$3, %edx
	leaq	.LC110(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$72, %rsp
	movq	%r12, %rdi
	movl	$1, %edx
	popq	%rbx
	leaq	.LC87(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L781:
	.cfi_restore_state
	movq	(%r15), %rdi
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L680
	movq	(%r15), %rdi
	movl	$6, %edx
	leaq	.LC109(%rip), %rsi
	movq	%rdi, -64(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-64(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compilerlsERSoNS1_4TypeE@PLT
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L709:
	movq	%rax, %rdx
	sarq	$6, %rdx
.L711:
	movq	232(%r13), %rcx
	movq	%rdx, %rsi
	salq	$6, %rsi
	movq	(%rcx,%rdx,8), %rdx
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %rax
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L783:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L711
.L684:
	movl	$5, %edx
	leaq	.LC137(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testl	%r13d, %r13d
	je	.L717
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L683:
	movq	(%r15), %rdi
	movl	$5, %edx
	leaq	.LC137(%rip), %rsi
	shrq	%r12
	andl	$1073741823, %r12d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L717
.L782:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.L691
	.p2align 4,,10
	.p2align 3
.L688:
	movq	(%r15), %rdi
	movl	$2, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r15)
	jg	.L688
.L691:
	movq	(%r15), %rdi
	movl	$4, %edx
	leaq	.LC133(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	movq	56(%rax), %r12
	testq	%r12, %r12
	je	.L784
	movq	(%r15), %r13
	movl	$1, %edx
	leaq	.LC104(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r12), %esi
	movq	%r13, %rdi
	andl	$16777215, %esi
	call	_ZNSolsEi@PLT
	movq	(%r15), %r13
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	.LC11(%rip), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17GraphC1Visualizer11PrintInputsEPNS1_4NodeE
.L692:
	movq	(%r15), %rdi
	movl	$3, %edx
	leaq	.LC135(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	movq	112(%rax), %rcx
	movq	104(%rax), %r12
	movq	%rcx, -64(%rbp)
	cmpq	%r12, %rcx
	je	.L697
	.p2align 4,,10
	.p2align 3
.L696:
	movq	(%r15), %r13
	movq	(%r12), %rbx
	movl	$2, %edx
	addq	$8, %r12
	leaq	.LC18(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%rbx), %esi
	movq	%r13, %rdi
	call	_ZNSolsEi@PLT
	cmpq	%r12, -64(%rbp)
	jne	.L696
.L697:
	cmpb	$0, _ZN2v88internal22FLAG_trace_turbo_typesE(%rip)
	movq	(%r15), %rdi
	je	.L695
	movq	-56(%rbp), %rbx
	cmpq	$0, 56(%rbx)
	je	.L695
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	56(%rbx), %rax
	movq	8(%rax), %r13
	testq	%r13, %r13
	jne	.L698
.L777:
	movq	(%r15), %rdi
.L695:
	movl	$5, %edx
	leaq	.LC132(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%r15), %eax
	jmp	.L687
.L784:
	movl	4(%rax), %eax
	movq	(%r15), %rdi
	movl	%eax, %esi
	movl	%eax, -64(%rbp)
	notl	%esi
	call	_ZNSolsEi@PLT
	movl	$5, %edx
	leaq	.LC134(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L692
.L698:
	movq	(%r15), %r12
	movl	$6, %edx
	leaq	.LC109(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoNS1_4TypeE@PLT
	jmp	.L777
.L779:
	movl	$-1, %esi
	jmp	.L663
.L778:
	leaq	.LC120(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE20345:
	.size	_ZN2v88internal8compiler17GraphC1Visualizer13PrintScheduleEPKcPKNS1_8ScheduleEPKNS1_19SourcePositionTableEPKNS1_19InstructionSequenceE, .-_ZN2v88internal8compiler17GraphC1Visualizer13PrintScheduleEPKcPKNS1_8ScheduleEPKNS1_19SourcePositionTableEPKNS1_19InstructionSequenceE
	.section	.rodata._ZN2v88internal8compiler17GraphC1Visualizer15PrintLiveRangesEPKcPKNS1_22RegisterAllocationDataE.str1.1,"aMS",@progbits,1
.LC138:
	.string	"intervals"
.LC139:
	.string	"fixed"
.LC140:
	.string	"object"
	.section	.text._ZN2v88internal8compiler17GraphC1Visualizer15PrintLiveRangesEPKcPKNS1_22RegisterAllocationDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17GraphC1Visualizer15PrintLiveRangesEPKcPKNS1_22RegisterAllocationDataE
	.type	_ZN2v88internal8compiler17GraphC1Visualizer15PrintLiveRangesEPKcPKNS1_22RegisterAllocationDataE, @function
_ZN2v88internal8compiler17GraphC1Visualizer15PrintLiveRangesEPKcPKNS1_22RegisterAllocationDataE:
.LFB20346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	.LC12(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movl	8(%rdi), %eax
	movq	%rdx, -64(%rbp)
	testl	%eax, %eax
	jle	.L789
	.p2align 4,,10
	.p2align 3
.L786:
	movq	(%r12), %rdi
	movl	$2, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r12)
	jg	.L786
.L789:
	movq	(%r12), %r13
	movl	$6, %edx
	leaq	.LC96(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$9, %edx
	leaq	.LC138(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC87(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$1, 8(%r12)
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	.LC98(%rip), %rsi
	leaq	.LC139(%rip), %r15
	call	_ZN2v88internal8compiler17GraphC1Visualizer19PrintStringPropertyEPKcS4_
	movq	-64(%rbp), %rax
	movq	272(%rax), %rcx
	movq	264(%rax), %r14
	movq	%rcx, -56(%rbp)
	cmpq	%rcx, %r14
	je	.L788
	.p2align 4,,10
	.p2align 3
.L796:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L792
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L792
	movl	88(%rbx), %r13d
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L857:
	movq	%rbx, %rsi
	movl	%r13d, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci.part.0
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L792
.L794:
	movq	16(%rbx), %rdx
.L795:
	testq	%rdx, %rdx
	jne	.L857
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L794
.L792:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	jne	.L796
.L788:
	movq	-64(%rbp), %rax
	leaq	.LC139(%rip), %r15
	movq	208(%rax), %rcx
	movq	200(%rax), %r14
	movq	%rcx, -56(%rbp)
	cmpq	%rcx, %r14
	je	.L791
	.p2align 4,,10
	.p2align 3
.L803:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L799
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L799
	movl	88(%rbx), %r13d
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L858:
	movq	%rbx, %rsi
	movl	%r13d, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci.part.0
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L799
.L801:
	movq	16(%rbx), %rdx
.L802:
	testq	%rdx, %rdx
	jne	.L858
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L801
.L799:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	jne	.L803
.L791:
	movq	-64(%rbp), %rax
	leaq	.LC140(%rip), %r14
	movq	176(%rax), %rcx
	movq	168(%rax), %r13
	movq	%rcx, -56(%rbp)
	cmpq	%rcx, %r13
	je	.L798
	.p2align 4,,10
	.p2align 3
.L810:
	movq	0(%r13), %rbx
	testq	%rbx, %rbx
	je	.L806
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L806
	movl	88(%rbx), %r15d
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L859:
	movq	%rbx, %rsi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci.part.0
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L806
.L808:
	movq	16(%rbx), %rax
.L809:
	testq	%rax, %rax
	jne	.L859
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L808
.L806:
	addq	$8, %r13
	cmpq	%r13, -56(%rbp)
	jne	.L810
.L798:
	movl	8(%r12), %eax
	subl	$1, %eax
	movl	%eax, 8(%r12)
	testl	%eax, %eax
	jle	.L804
	xorl	%ebx, %ebx
	leaq	.LC12(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L805:
	movq	(%r12), %rdi
	movl	$2, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 8(%r12)
	jg	.L805
.L804:
	movq	(%r12), %r12
	movl	$4, %edx
	leaq	.LC103(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$9, %edx
	leaq	.LC138(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	$1, %edx
	popq	%rbx
	leaq	.LC87(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.cfi_endproc
.LFE20346:
	.size	_ZN2v88internal8compiler17GraphC1Visualizer15PrintLiveRangesEPKcPKNS1_22RegisterAllocationDataE, .-_ZN2v88internal8compiler17GraphC1Visualizer15PrintLiveRangesEPKcPKNS1_22RegisterAllocationDataE
	.section	.text._ZN2v88internal8compiler17GraphC1Visualizer19PrintLiveRangeChainEPKNS1_17TopLevelLiveRangeEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17GraphC1Visualizer19PrintLiveRangeChainEPKNS1_17TopLevelLiveRangeEPKc
	.type	_ZN2v88internal8compiler17GraphC1Visualizer19PrintLiveRangeChainEPKNS1_17TopLevelLiveRangeEPKc, @function
_ZN2v88internal8compiler17GraphC1Visualizer19PrintLiveRangeChainEPKNS1_17TopLevelLiveRangeEPKc:
.LFB20347:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L878
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L860
	movl	88(%rsi), %r14d
	movq	%rdi, %r12
	movq	%rdx, %r13
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L881:
	movq	%rbx, %rsi
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci.part.0
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L860
.L863:
	movq	16(%rbx), %rax
.L864:
	testq	%rax, %rax
	jne	.L881
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L863
.L860:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L878:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE20347:
	.size	_ZN2v88internal8compiler17GraphC1Visualizer19PrintLiveRangeChainEPKNS1_17TopLevelLiveRangeEPKc, .-_ZN2v88internal8compiler17GraphC1Visualizer19PrintLiveRangeChainEPKNS1_17TopLevelLiveRangeEPKc
	.section	.text._ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci
	.type	_ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci, @function
_ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci:
.LFB20348:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L882
	cmpq	$0, 16(%rsi)
	je	.L882
	jmp	_ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci.part.0
	.p2align 4,,10
	.p2align 3
.L882:
	ret
	.cfi_endproc
.LFE20348:
	.size	_ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci, .-_ZN2v88internal8compiler17GraphC1Visualizer14PrintLiveRangeEPKNS1_9LiveRangeEPKci
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_16AsC1VCompilationE.str1.8,"aMS",@progbits,1
	.align 8
.LC141:
	.string	"../deps/v8/src/compiler/graph-visualizer.cc:809"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_16AsC1VCompilationE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_16AsC1VCompilationE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_16AsC1VCompilationE, @function
_ZN2v88internal8compilerlsERSoRKNS1_16AsC1VCompilationE:
.LFB20349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	leaq	.LC141(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-176(%rbp), %r14
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	%r14, %rsi
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal19AccountingAllocatorE(%rip), %rax
	movups	%xmm0, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	(%rbx), %rsi
	leaq	-144(%rbp), %rdi
	movq	%r12, -144(%rbp)
	movl	$0, -136(%rbp)
	movq	%r13, -128(%rbp)
	call	_ZN2v88internal8compiler17GraphC1Visualizer16PrintCompilationEPKNS0_24OptimizedCompilationInfoE
	movq	%r13, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal19AccountingAllocatorD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L890
	addq	$144, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L890:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20349:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_16AsC1VCompilationE, .-_ZN2v88internal8compilerlsERSoRKNS1_16AsC1VCompilationE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_5AsC1VE.str1.8,"aMS",@progbits,1
	.align 8
.LC142:
	.string	"../deps/v8/src/compiler/graph-visualizer.cc:817"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_5AsC1VE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_5AsC1VE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_5AsC1VE, @function
_ZN2v88internal8compilerlsERSoRKNS1_5AsC1VE:
.LFB20350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	leaq	.LC142(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-176(%rbp), %r14
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	%r14, %rsi
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal19AccountingAllocatorE(%rip), %rax
	movups	%xmm0, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	16(%rbx), %rcx
	movq	(%rbx), %rdx
	movq	%r13, -128(%rbp)
	movq	24(%rbx), %rsi
	movq	8(%rbx), %r8
	leaq	-144(%rbp), %rdi
	movq	%r12, -144(%rbp)
	movl	$0, -136(%rbp)
	call	_ZN2v88internal8compiler17GraphC1Visualizer13PrintScheduleEPKcPKNS1_8ScheduleEPKNS1_19SourcePositionTableEPKNS1_19InstructionSequenceE
	movq	%r13, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal19AccountingAllocatorD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L894
	addq	$144, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L894:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20350:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_5AsC1VE, .-_ZN2v88internal8compilerlsERSoRKNS1_5AsC1VE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_27AsC1VRegisterAllocationDataE.str1.8,"aMS",@progbits,1
	.align 8
.LC143:
	.string	"../deps/v8/src/compiler/graph-visualizer.cc:827"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_27AsC1VRegisterAllocationDataE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_27AsC1VRegisterAllocationDataE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_27AsC1VRegisterAllocationDataE, @function
_ZN2v88internal8compilerlsERSoRKNS1_27AsC1VRegisterAllocationDataE:
.LFB20351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	leaq	.LC143(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-176(%rbp), %r14
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	%r14, %rsi
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal19AccountingAllocatorE(%rip), %rax
	movups	%xmm0, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r13, -128(%rbp)
	leaq	-144(%rbp), %rdi
	movq	%r12, -144(%rbp)
	movl	$0, -136(%rbp)
	call	_ZN2v88internal8compiler17GraphC1Visualizer15PrintLiveRangesEPKcPKNS1_22RegisterAllocationDataE
	movq	%r13, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal19AccountingAllocatorD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L898
	addq	$144, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L898:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20351:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_27AsC1VRegisterAllocationDataE, .-_ZN2v88internal8compilerlsERSoRKNS1_27AsC1VRegisterAllocationDataE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_16AsScheduledGraphE.str1.1,"aMS",@progbits,1
.LC144:
	.string	"  + Block B"
.LC145:
	.string	" (pred:"
.LC146:
	.string	", loop until B"
.LC147:
	.string	", in loop B"
.LC148:
	.string	"Goto"
.LC149:
	.string	","
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_16AsScheduledGraphE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_16AsScheduledGraphE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_16AsScheduledGraphE, @function
_ZN2v88internal8compilerlsERSoRKNS1_16AsScheduledGraphE:
.LFB20365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r13
	movq	$0, -56(%rbp)
	movq	80(%r13), %rdx
	cmpq	88(%r13), %rdx
	je	.L945
	.p2align 4,,10
	.p2align 3
.L900:
	movq	-56(%rbp), %rax
	leaq	.LC144(%rip), %rsi
	movq	%r14, %rdi
	movq	(%rdx,%rax,8), %rbx
	movl	$11, %edx
	movl	48(%rbx), %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%rbx), %esi
	movq	%r14, %rdi
	call	_ZNSolsEi@PLT
	movl	$7, %edx
	leaq	.LC145(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	144(%rbx), %rcx
	movq	136(%rbx), %r12
	cmpq	%rcx, %r12
	je	.L905
	.p2align 4,,10
	.p2align 3
.L904:
	movq	(%r12), %r9
	movl	$2, %edx
	movq	%r14, %rdi
	movq	%rcx, -72(%rbp)
	leaq	.LC18(%rip), %rsi
	addq	$8, %r12
	movq	%r9, -64(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-64(%rbp), %r9
	movq	%r14, %rdi
	movl	4(%r9), %esi
	call	_ZNSolsEi@PLT
	movq	-72(%rbp), %rcx
	cmpq	%r12, %rcx
	jne	.L904
.L905:
	cmpq	$0, 40(%rbx)
	je	.L948
	leaq	.LC146(%rip), %rsi
	movq	%r14, %rdi
	movl	$14, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	40(%rbx), %rax
	movq	%r14, %rdi
	movl	4(%rax), %esi
	call	_ZNSolsEi@PLT
.L906:
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %r12
	testq	%r12, %r12
	je	.L913
	cmpb	$0, 56(%r12)
	je	.L908
	movsbl	67(%r12), %esi
.L909:
	movq	%r14, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	72(%rbx), %r12
	cmpq	80(%rbx), %r12
	jne	.L916
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L949:
	movsbl	67(%rdi), %esi
.L915:
	movq	%r14, %rdi
	addq	$8, %r12
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	cmpq	%r12, 80(%rbx)
	je	.L917
.L916:
	movq	(%r12), %rdx
	movq	%r14, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal8compiler12_GLOBAL__N_118PrintScheduledNodeERSoiPNS1_4NodeE
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %rdi
	testq	%rdi, %rdi
	je	.L913
	cmpb	$0, 56(%rdi)
	jne	.L949
	movq	%rdi, -64(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-64(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L915
	call	*%rax
	movsbl	%al, %esi
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L917:
	movq	112(%rbx), %rax
	cmpq	%rax, 104(%rbx)
	jne	.L950
.L912:
	movq	80(%r13), %rdx
	movq	88(%r13), %rax
	addq	$1, -56(%rbp)
	movq	-56(%rbp), %rcx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	ja	.L900
.L945:
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L950:
	.cfi_restore_state
	movq	56(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L918
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_118PrintScheduledNodeERSoiPNS1_4NodeE
.L919:
	movl	$3, %edx
	leaq	.LC135(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	104(%rbx), %r15
	movq	112(%rbx), %r12
	cmpq	%r12, %r15
	je	.L922
	movq	(%r15), %rbx
	addq	$8, %r15
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L951:
	movl	$1, %edx
	movq	%r14, %rdi
	movq	(%r15), %rbx
	addq	$8, %r15
	leaq	.LC149(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L923:
	leaq	.LC18(%rip), %rsi
	movq	%r14, %rdi
	movl	$2, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%rbx), %esi
	movq	%r14, %rdi
	call	_ZNSolsEi@PLT
	cmpq	%r15, %r12
	jne	.L951
.L922:
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %r12
	testq	%r12, %r12
	je	.L913
	cmpb	$0, 56(%r12)
	je	.L924
	movsbl	67(%r12), %esi
.L925:
	movq	%r14, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L918:
	movl	$5, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testl	%r15d, %r15d
	jle	.L921
	.p2align 4,,10
	.p2align 3
.L920:
	movl	$2, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r14, %rdi
	addl	$1, %r12d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, %r15d
	jne	.L920
.L921:
	movl	$4, %edx
	leaq	.LC148(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L924:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L925
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L948:
	cmpq	$0, 32(%rbx)
	je	.L906
	movq	%r14, %rdi
	movl	$11, %edx
	leaq	.LC147(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%rbx), %rax
	movq	%r14, %rdi
	movl	4(%rax), %esi
	call	_ZNSolsEi@PLT
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L908:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L909
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L913:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE20365:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_16AsScheduledGraphE, .-_ZN2v88internal8compilerlsERSoRKNS1_16AsScheduledGraphE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE.str1.1,"aMS",@progbits,1
.LC150:
	.string	"{"
.LC151:
	.string	"\"type\": \"unallocated\", "
.LC152:
	.string	"\"text\": \"v"
.LC153:
	.string	",\"tooltip\": \"FIXED_SLOT: "
.LC154:
	.string	",\"tooltip\": \"FIXED_REGISTER: "
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE.str1.8,"aMS",@progbits,1
	.align 8
.LC155:
	.string	",\"tooltip\": \"FIXED_FP_REGISTER: "
	.align 8
.LC156:
	.string	",\"tooltip\": \"MUST_HAVE_REGISTER\""
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE.str1.1
.LC157:
	.string	",\"tooltip\": \"MUST_HAVE_SLOT\""
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE.str1.8
	.align 8
.LC158:
	.string	",\"tooltip\": \"SAME_AS_FIRST_INPUT\""
	.align 8
.LC159:
	.string	",\"tooltip\": \"REGISTER_OR_SLOT\""
	.align 8
.LC160:
	.string	",\"tooltip\": \"REGISTER_OR_SLOT_OR_CONSTANT\""
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE.str1.1
.LC161:
	.string	"\"type\": \"constant\", "
.LC162:
	.string	"\","
.LC163:
	.string	"\"tooltip\": \""
.LC164:
	.string	"\"type\": \"immediate\", "
.LC165:
	.string	"\"text\": \"#"
.LC166:
	.string	"\"text\": \"imm:"
.LC167:
	.string	"\"type\": "
.LC168:
	.string	"\"explicit\", "
.LC169:
	.string	"\"allocated\", "
.LC170:
	.string	"\"text\": \""
.LC171:
	.string	"stack:"
.LC172:
	.string	"fp_stack:"
.LC173:
	.string	"UNKNOWN"
.LC174:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE, @function
_ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE:
.LFB20366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %r15
	movq	8(%rsi), %r13
	leaq	.LC150(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rbx
	movl	%ebx, %eax
	andl	$7, %eax
	cmpl	$5, %eax
	ja	.L961
	leaq	.L955(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE,"a",@progbits
	.align 4
	.align 4
.L955:
	.long	.L959-.L955
	.long	.L958-.L955
	.long	.L957-.L955
	.long	.L956-.L955
	.long	.L954-.L955
	.long	.L954-.L955
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE
	.p2align 4,,10
	.p2align 3
.L997:
	sarq	$35, %rax
	movq	%rax, %rdx
	movslq	%eax, %rcx
.L1013:
	cmpq	$-1, %rdx
	je	.L1022
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rax
	movq	(%rax,%rcx,8), %r13
	testq	%r13, %r13
	je	.L1010
.L1035:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
.L1009:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1000:
	movl	$2, %edx
	leaq	.LC162(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$12, %edx
	leaq	.LC163(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdi
	shrq	$5, %rdi
	call	_ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1031
.L970:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L971:
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L961:
	movl	$1, %edx
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1045
	addq	$536, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L954:
	.cfi_restore_state
	movl	$8, %edx
	leaq	.LC167(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%r15), %eax
	andl	$7, %eax
	cmpl	$4, %eax
	je	.L1046
	movl	$13, %edx
	leaq	.LC169(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L996:
	movl	$9, %edx
	leaq	.LC170(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	testb	$4, %al
	je	.L997
	movq	%rax, %rsi
	shrq	$3, %rsi
	andl	$3, %esi
	cmpl	$1, %esi
	je	.L1047
	movq	%rax, %rdx
	sarq	$35, %rdx
	movslq	%edx, %rcx
	testl	%esi, %esi
	jne	.L1013
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L1013
	cmpq	$15, %rdx
	jg	.L1002
	cmpq	$-1, %rdx
	je	.L1022
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rax
	movq	(%rax,%rcx,8), %r13
	testq	%r13, %r13
	jne	.L1035
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L958:
	movl	$23, %edx
	leaq	.LC151(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$10, %edx
	leaq	.LC152(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rsi
	movq	%r12, %rdi
	shrq	$3, %rsi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	btq	$35, %rax
	jnc	.L1048
	shrq	$36, %rax
	leaq	.L963(%rip), %rdx
	andl	$7, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE
	.align 4
	.align 4
.L963:
	.long	.L961-.L963
	.long	.L969-.L963
	.long	.L968-.L963
	.long	.L967-.L963
	.long	.L966-.L963
	.long	.L965-.L963
	.long	.L964-.L963
	.long	.L962-.L963
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE
	.p2align 4,,10
	.p2align 3
.L957:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movl	$20, %edx
	movq	%r12, %rdi
	movq	.LC175(%rip), %xmm1
	movq	%rax, -504(%rbp)
	leaq	.LC161(%rip), %rsi
	shrq	$3, %rbx
	leaq	-320(%rbp), %r14
	movl	%ebx, %r15d
	movhps	-504(%rbp), %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$10, %edx
	leaq	.LC152(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC162(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$12, %edx
	leaq	.LC163(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-448(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, -528(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -536(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	%rax, -448(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%dx, -96(%rbp)
	movq	-24(%rax), %rdx
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp,%rdx)
	movq	-448(%rbp), %rdx
	movq	$0, -440(%rbp)
	movq	-528(%rbp), %rax
	addq	-24(%rdx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	leaq	-432(%rbp), %r8
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%r8, %rdi
	movq	%r8, -528(%rbp)
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	addq	-24(%rax), %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-560(%rbp), %xmm1
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -512(%rbp)
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -576(%rbp)
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -560(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -568(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -544(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	112(%r13), %rax
	movq	-528(%rbp), %r8
	leaq	104(%r13), %rcx
	testq	%rax, %rax
	je	.L973
	movq	%rcx, %rdx
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L975
.L974:
	cmpl	%r15d, 32(%rax)
	jge	.L1049
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L974
.L975:
	cmpq	%rdx, %rcx
	je	.L973
	cmpl	%ebx, 32(%rdx)
	cmovle	%rdx, %rcx
.L973:
	movzbl	44(%rcx), %edx
	movq	48(%rcx), %rax
	leaq	-496(%rbp), %rbx
	movq	%r8, %rdi
	movl	40(%rcx), %ecx
	movq	%rbx, %rsi
	movb	%dl, -492(%rbp)
	movl	%ecx, -496(%rbp)
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_8ConstantE@PLT
	leaq	-464(%rbp), %rax
	movq	$0, -472(%rbp)
	leaq	-480(%rbp), %rdi
	movq	%rax, -528(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L978
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L979
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L980:
	movq	-480(%rbp), %rdi
	movq	-472(%rbp), %r13
	addq	%rdi, %r13
	cmpq	%rdi, %r13
	je	.L991
	movq	%rdi, %r15
	.p2align 4,,10
	.p2align 3
.L982:
	movsbw	(%r15), %ax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addq	$1, %r15
	movw	%ax, -496(%rbp)
	call	_ZN2v88internallsERSoRKNS0_20AsEscapedUC16ForJSONE@PLT
	cmpq	%r15, %r13
	jne	.L982
.L1044:
	movq	-480(%rbp), %rdi
.L991:
	cmpq	-528(%rbp), %rdi
	je	.L993
	call	_ZdlPv@PLT
.L993:
	movq	%r12, %rdi
	movl	$1, %edx
	movq	.LC175(%rip), %xmm0
	leaq	.LC14(%rip), %rsi
	movhps	-568(%rbp), %xmm0
	movaps	%xmm0, -528(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-512(%rbp), %rax
	movdqa	-528(%rbp), %xmm0
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-576(%rbp), %rax
	movaps	%xmm0, -432(%rbp)
	movq	%rax, -320(%rbp)
	cmpq	-544(%rbp), %rdi
	je	.L994
	call	_ZdlPv@PLT
.L994:
	movq	-504(%rbp), %rax
	movq	-560(%rbp), %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-536(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L956:
	movl	$21, %edx
	leaq	.LC164(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rbx
	testb	$8, %bl
	jne	.L985
	leaq	.LC165(%rip), %rsi
	movl	$10, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rsi
	sarq	$32, %rsi
.L1033:
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L979:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L966:
	movl	$32, %edx
	leaq	.LC155(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rdx
	shrq	$41, %rax
	andl	$63, %eax
	movq	(%rdx,%rax,8), %r13
	testq	%r13, %r13
	jne	.L970
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L985:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movl	$13, %edx
	sarq	$32, %rbx
	movq	%r12, %rdi
	movq	%rax, -504(%rbp)
	leaq	.LC166(%rip), %rsi
	movq	.LC175(%rip), %xmm1
	leaq	-320(%rbp), %r14
	movhps	-504(%rbp), %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-448(%rbp), %rbx
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC162(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$12, %edx
	leaq	.LC163(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -536(%rbp)
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	leaq	-432(%rbp), %r8
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%r8, %rdi
	movq	%r8, -528(%rbp)
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	addq	-24(%rax), %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-560(%rbp), %xmm1
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -512(%rbp)
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -576(%rbp)
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -560(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -568(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -544(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	(%r15), %rsi
	movq	-528(%rbp), %r8
	testb	$8, %sil
	je	.L1050
	movq	152(%r13), %rax
	sarq	$32, %rsi
	leaq	-496(%rbp), %rbx
	salq	$4, %rsi
	addq	%rsi, %rax
	movl	(%rax), %ecx
	movzbl	4(%rax), %edx
	movq	8(%rax), %rax
.L987:
	movq	%r8, %rdi
	movq	%rbx, %rsi
	movl	%ecx, -496(%rbp)
	movb	%dl, -492(%rbp)
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_8ConstantE@PLT
	leaq	-464(%rbp), %rax
	movq	$0, -472(%rbp)
	leaq	-480(%rbp), %rdi
	movq	%rax, -528(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L988
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L989
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L990:
	movq	-480(%rbp), %rdi
	movq	-472(%rbp), %r15
	addq	%rdi, %r15
	cmpq	%rdi, %r15
	je	.L991
	movq	%rdi, %r13
	.p2align 4,,10
	.p2align 3
.L992:
	movsbw	0(%r13), %ax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addq	$1, %r13
	movw	%ax, -496(%rbp)
	call	_ZN2v88internallsERSoRKNS0_20AsEscapedUC16ForJSONE@PLT
	cmpq	%r13, %r15
	jne	.L992
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1046:
	movl	$12, %edx
	leaq	.LC168(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1048:
	leaq	.LC153(%rip), %rsi
	movl	$25, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rsi
	sarq	$36, %rsi
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1022:
	movl	$7, %edx
	leaq	.LC10(%rip), %r13
	jmp	.L1009
.L959:
	leaq	.LC174(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1050:
	leaq	-496(%rbp), %rbx
	sarq	$32, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-496(%rbp), %ecx
	movzbl	-492(%rbp), %edx
	movq	-488(%rbp), %rax
	movq	-528(%rbp), %r8
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L969:
	movl	$30, %edx
	leaq	.LC159(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L965:
	movl	$32, %edx
	leaq	.LC156(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L967:
	movl	$29, %edx
	leaq	.LC154(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rdx
	shrq	$41, %rax
	andl	$63, %eax
	movq	(%rdx,%rax,8), %r13
	testq	%r13, %r13
	jne	.L970
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L968:
	movl	$42, %edx
	leaq	.LC160(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L962:
	movl	$33, %edx
	leaq	.LC158(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L964:
	movl	$28, %edx
	leaq	.LC157(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L978:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L1047:
	shrq	$5, %rax
	movl	$6, %edx
	leaq	.LC171(%rip), %rsi
	cmpb	$11, %al
	jbe	.L1032
	movl	$9, %edx
	leaq	.LC172(%rip), %rsi
.L1032:
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rsi
	movq	%r12, %rdi
	sarq	$35, %rsi
	call	_ZNSolsEi@PLT
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L989:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L990
.L988:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L990
.L1002:
	movl	$7, %edx
	leaq	.LC173(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1000
.L1045:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20366:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE, .-_ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_17InstructionAsJSONE.str1.1,"aMS",@progbits,1
.LC176:
	.string	"\"id\": "
.LC177:
	.string	"\"opcode\": \""
.LC178:
	.string	"\"flags\": \""
.LC179:
	.string	" : "
.LC180:
	.string	" && "
.LC181:
	.string	" if "
.LC182:
	.string	"\"gaps\": ["
.LC183:
	.string	"],"
.LC184:
	.string	"\"outputs\": ["
.LC185:
	.string	"\"inputs\": ["
.LC186:
	.string	"\"temps\": ["
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_17InstructionAsJSONE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_17InstructionAsJSONE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_17InstructionAsJSONE, @function
_ZN2v88internal8compilerlsERSoRKNS1_17InstructionAsJSONE:
.LFB20367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rbx
	leaq	.LC150(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$6, %edx
	leaq	.LC176(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%r14), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC149(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$11, %edx
	leaq	.LC177(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %eax
	movq	%r15, %rsi
	movq	%r12, %rdi
	andl	$511, %eax
	movl	%eax, -80(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_10ArchOpcodeE@PLT
	movl	$2, %edx
	leaq	.LC162(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$10, %edx
	leaq	.LC178(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %eax
	shrl	$14, %eax
	andl	$7, %eax
	movl	%eax, -100(%rbp)
	testl	$15872, (%rbx)
	jne	.L1093
	testl	%eax, %eax
	jne	.L1094
.L1053:
	movl	$2, %edx
	leaq	.LC162(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$9, %edx
	leaq	.LC182(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-96(%rbp), %rax
	movq	$1, -152(%rbp)
	movq	%rax, -144(%rbp)
.L1054:
	movl	$1, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-152(%rbp), %rax
	movq	(%rbx,%rax,8), %rax
	testq	%rax, %rax
	je	.L1059
	movq	16(%rax), %rsi
	movq	8(%rax), %rcx
	movl	$1, %eax
	movq	%rsi, -136(%rbp)
	cmpq	%rsi, %rcx
	jne	.L1062
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1061:
	movl	$1, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	movq	%rcx, -128(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	8(%r13), %rax
	movq	-144(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %xmm0
	movhps	16(%r14), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE
	movl	$1, %edx
	leaq	.LC149(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-120(%rbp), %rdi
	movq	%r13, %xmm0
	movq	%r15, %rsi
	movhps	16(%r14), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-128(%rbp), %rcx
	xorl	%eax, %eax
.L1060:
	addq	$8, %rcx
	cmpq	%rcx, -136(%rbp)
	je	.L1059
.L1062:
	movq	(%rcx), %r13
	testb	$7, 0(%r13)
	je	.L1060
	testb	%al, %al
	jne	.L1061
	movl	$1, %edx
	leaq	.LC149(%rip), %rsi
	movq	%r12, %rdi
	movq	%rcx, -120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-120(%rbp), %rcx
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1059:
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	$2, -152(%rbp)
	jne	.L1091
	movl	$2, %edx
	leaq	.LC183(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$12, %edx
	leaq	.LC184(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 4(%rbx)
	je	.L1064
	leaq	40(%rbx), %rax
	xorl	%r13d, %r13d
	movq	%rax, -120(%rbp)
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1095:
	movl	$1, %edx
	leaq	.LC149(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1065:
	movq	-120(%rbp), %xmm0
	movq	%r15, %rsi
	movq	%r12, %rdi
	addq	$1, %r13
	movhps	16(%r14), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE
	movzbl	4(%rbx), %eax
	addq	$8, -120(%rbp)
	cmpq	%rax, %r13
	jb	.L1095
.L1064:
	movl	$2, %edx
	leaq	.LC183(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$11, %edx
	leaq	.LC185(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%rbx), %eax
	testl	$16776960, %eax
	je	.L1066
	xorl	%r13d, %r13d
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1096:
	movl	$1, %edx
	leaq	.LC149(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%rbx), %eax
.L1067:
	movzbl	%al, %eax
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	5(%r13,%rax), %rax
	addq	$1, %r13
	leaq	(%rbx,%rax,8), %rax
	movq	%rax, %xmm0
	movhps	16(%r14), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE
	movzwl	5(%rbx), %eax
	cmpq	%rax, %r13
	jb	.L1096
.L1066:
	movl	$2, %edx
	leaq	.LC183(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$10, %edx
	leaq	.LC186(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%rbx), %eax
	testl	$1056964608, %eax
	je	.L1068
	xorl	%r13d, %r13d
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1097:
	movl	$1, %edx
	leaq	.LC149(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%rbx), %eax
.L1069:
	movzbl	%al, %edx
	shrl	$8, %eax
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	5(%r13,%rdx), %rdx
	movzwl	%ax, %eax
	addq	$1, %r13
	addq	%rdx, %rax
	leaq	(%rbx,%rax,8), %rax
	movq	%rax, %xmm0
	movhps	16(%r14), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE
	movzbl	7(%rbx), %eax
	andl	$63, %eax
	cmpq	%rax, %r13
	jb	.L1097
.L1068:
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1098
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1091:
	.cfi_restore_state
	movl	$1, %edx
	leaq	.LC149(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	$2, -152(%rbp)
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1093:
	movl	$3, %edx
	leaq	.LC179(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %eax
	movq	%r15, %rsi
	movq	%r12, %rdi
	shrl	$9, %eax
	andl	$31, %eax
	movl	%eax, -80(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_14AddressingModeE@PLT
	movl	-100(%rbp), %eax
	testl	%eax, %eax
	je	.L1053
.L1094:
	movl	$4, %edx
	movq	%r12, %rdi
	leaq	.LC180(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-100(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9FlagsModeE@PLT
	movl	$4, %edx
	leaq	.LC181(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %eax
	movq	%r15, %rsi
	movq	%r13, %rdi
	shrl	$17, %eax
	andl	$31, %eax
	movl	%eax, -80(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_14FlagsConditionE@PLT
	jmp	.L1053
.L1098:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20367:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_17InstructionAsJSONE, .-_ZN2v88internal8compilerlsERSoRKNS1_17InstructionAsJSONE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_22InstructionBlockAsJSONE.str1.1,"aMS",@progbits,1
.LC187:
	.string	"\"deferred\": "
.LC188:
	.string	"\"loop_end\": "
.LC189:
	.string	"\"predecessors\": ["
.LC190:
	.string	"\"successors\": ["
.LC191:
	.string	"\"phis\": ["
.LC192:
	.string	"{\"output\" : "
.LC193:
	.string	"\"operands\": ["
.LC194:
	.string	"\"v"
.LC195:
	.string	"\"instructions\": ["
.LC196:
	.string	"\"loop_header\": "
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_22InstructionBlockAsJSONE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_22InstructionBlockAsJSONE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_22InstructionBlockAsJSONE, @function
_ZN2v88internal8compilerlsERSoRKNS1_22InstructionBlockAsJSONE:
.LFB20368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	leaq	.LC150(%rip), %rsi
	movq	%rbx, -120(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$6, %edx
	leaq	.LC176(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	100(%rbx), %eax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r15, -136(%rbp)
	movl	%eax, -80(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_9RpoNumberE@PLT
	movl	$1, %edx
	leaq	.LC149(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$12, %edx
	leaq	.LC187(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$1, 120(%rbx)
	leaq	.LC57(%rip), %rax
	movq	%r12, %rdi
	sbbq	%rdx, %rdx
	leaq	.LC56(%rip), %rsi
	notq	%rdx
	addq	$5, %rdx
	cmpb	$0, 120(%rbx)
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC149(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$15, %edx
	leaq	.LC196(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	108(%rbx), %eax
	movq	%r12, %rdi
	movl	%eax, %esi
	movl	%eax, -112(%rbp)
	notl	%esi
	shrl	$31, %esi
	call	_ZNSo9_M_insertIbEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC149(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	108(%rbx), %eax
	testl	%eax, %eax
	js	.L1117
	movl	$12, %edx
	leaq	.LC188(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	108(%rbx), %eax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, -80(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_9RpoNumberE@PLT
	movl	$1, %edx
	leaq	.LC149(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1117:
	movl	$17, %edx
	leaq	.LC189(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-120(%rbp), %rax
	movq	40(%rax), %rbx
	movq	48(%rax), %r14
	cmpq	%r14, %rbx
	je	.L1101
	movl	(%rbx), %r15d
	leaq	.LC149(%rip), %r13
	addq	$4, %rbx
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1122:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	(%rbx), %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$4, %rbx
.L1102:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	cmpq	%rbx, %r14
	jne	.L1122
.L1101:
	movl	$2, %edx
	leaq	.LC183(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$15, %edx
	leaq	.LC190(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-120(%rbp), %rax
	movq	8(%rax), %rbx
	movq	16(%rax), %r14
	cmpq	%r14, %rbx
	je	.L1103
	movl	(%rbx), %r15d
	leaq	.LC149(%rip), %r13
	addq	$4, %rbx
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1123:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	(%rbx), %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$4, %rbx
.L1104:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	cmpq	%rbx, %r14
	jne	.L1123
.L1103:
	movl	$2, %edx
	leaq	.LC183(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$9, %edx
	leaq	.LC191(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-104(%rbp), %rax
	movq	-120(%rbp), %rcx
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	72(%rcx), %rax
	movq	80(%rcx), %rcx
	movq	%rcx, -128(%rbp)
	cmpq	%rcx, %rax
	je	.L1105
	movq	(%rax), %r13
	addq	$8, %rax
	leaq	.LC194(%rip), %r15
	movq	%rax, -112(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L1108:
	movl	$12, %edx
	leaq	8(%r13), %rax
	leaq	.LC192(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_24InstructionOperandAsJSONE
	movl	$1, %edx
	leaq	.LC149(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$13, %edx
	leaq	.LC193(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	24(%r13), %rbx
	movq	32(%r13), %r14
	cmpq	%r14, %rbx
	je	.L1106
	movl	(%rbx), %r13d
	addq	$4, %rbx
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1124:
	movl	$1, %edx
	movq	%r12, %rdi
	movl	(%rbx), %r13d
	addq	$4, %rbx
	leaq	.LC149(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1107:
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%rbx, %r14
	jne	.L1124
.L1106:
	movl	$2, %edx
	leaq	.LC89(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-112(%rbp), %rbx
	cmpq	%rbx, -128(%rbp)
	je	.L1105
	movq	(%rbx), %r13
	movl	$1, %edx
	movq	%r12, %rdi
	addq	$8, %rbx
	leaq	.LC149(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, -112(%rbp)
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1105:
	movl	$2, %edx
	leaq	.LC183(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$17, %edx
	leaq	.LC195(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-104(%rbp), %rax
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -64(%rbp)
	movq	-120(%rbp), %rax
	movl	$-1, -80(%rbp)
	movl	112(%rax), %r14d
	cmpl	116(%rax), %r14d
	jge	.L1109
	movslq	%r14d, %r13
	leaq	.LC149(%rip), %rbx
.L1110:
	movq	-104(%rbp), %rax
	movl	%r14d, -80(%rbp)
	movq	208(%rax), %rdx
	movq	%rdx, %rcx
	subq	216(%rax), %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	addq	%r13, %rax
	js	.L1111
	cmpq	$63, %rax
	jg	.L1112
	leaq	(%rdx,%r13,8), %rax
.L1113:
	movq	(%rax), %rax
	movq	%r12, %rdi
	addl	$1, %r14d
	addq	$1, %r13
	movq	-136(%rbp), %rsi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_17InstructionAsJSONE
	movq	-120(%rbp), %rax
	cmpl	116(%rax), %r14d
	jge	.L1109
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1112:
	movq	%rax, %rdx
	sarq	$6, %rdx
.L1114:
	movq	-104(%rbp), %rcx
	movq	%rdx, %rsi
	salq	$6, %rsi
	movq	232(%rcx), %rcx
	subq	%rsi, %rax
	movq	(%rcx,%rdx,8), %rdx
	leaq	(%rdx,%rax,8), %rax
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1111:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1109:
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1125
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1125:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20368:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_22InstructionBlockAsJSONE, .-_ZN2v88internal8compilerlsERSoRKNS1_22InstructionBlockAsJSONE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_25InstructionSequenceAsJSONE.str1.1,"aMS",@progbits,1
.LC197:
	.string	"\"blocks\": ["
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_25InstructionSequenceAsJSONE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_25InstructionSequenceAsJSONE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_25InstructionSequenceAsJSONE, @function
_ZN2v88internal8compilerlsERSoRKNS1_25InstructionSequenceAsJSONE:
.LFB20369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$11, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %r13
	leaq	.LC197(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%r13), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	%rdx, %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	testl	%ecx, %ecx
	jle	.L1127
	xorl	%r12d, %r12d
	leaq	-64(%rbp), %rbx
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1133:
	movl	$1, %edx
	leaq	.LC149(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%r13), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
.L1129:
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%r12, %rdx
	jbe	.L1132
	movq	(%rax,%r12,8), %rax
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%r13, -56(%rbp)
	addq	$1, %r12
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_22InstructionBlockAsJSONE
	movq	16(%r13), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	$3, %rax
	cmpl	%r12d, %eax
	jg	.L1133
.L1127:
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1134
	addq	$32, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1132:
	.cfi_restore_state
	movq	%r12, %rsi
	leaq	.LC120(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20369:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_25InstructionSequenceAsJSONE, .-_ZN2v88internal8compilerlsERSoRKNS1_25InstructionSequenceAsJSONE
	.section	.rodata._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC198:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB23998:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L1149
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1145
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1150
.L1137:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1144:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1151
	testq	%r13, %r13
	jg	.L1140
	testq	%r9, %r9
	jne	.L1143
.L1141:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1151:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1140
.L1143:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1150:
	testq	%rsi, %rsi
	jne	.L1138
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1140:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1141
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1145:
	movl	$4, %r14d
	jmp	.L1137
.L1149:
	leaq	.LC198(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1138:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L1137
	.cfi_endproc
.LFE23998:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB24858:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L1166
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1162
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1167
.L1154:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1161:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1168
	testq	%r13, %r13
	jg	.L1157
	testq	%r9, %r9
	jne	.L1160
.L1158:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1168:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1157
.L1160:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1167:
	testq	%rsi, %rsi
	jne	.L1155
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1158
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1162:
	movl	$4, %r14d
	jmp	.L1154
.L1166:
	leaq	.LC198(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1155:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L1154
	.cfi_endproc
.LFE24858:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZN2v88internal8compiler16SourceIdAssigner8GetIdForENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16SourceIdAssigner8GetIdForENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal8compiler16SourceIdAssigner8GetIdForENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal8compiler16SourceIdAssigner8GetIdForENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB20284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, %rbx
	subq	%r13, %rbx
	movq	%rbx, %rcx
	sarq	$3, %rcx
	je	.L1210
	xorl	%edx, %edx
.L1170:
	movq	0(%r13,%rdx,8), %rdx
	cmpq	%rsi, %rdx
	je	.L1173
	testq	%rsi, %rsi
	je	.L1174
	testq	%rdx, %rdx
	je	.L1174
	movq	(%rdx), %rdi
	cmpq	%rdi, (%rsi)
	je	.L1173
.L1174:
	leal	1(%rax), %edx
	movq	%rdx, %rax
	cmpq	%rcx, %rdx
	jb	.L1170
	movl	%ecx, -60(%rbp)
	cmpq	%r14, 16(%r12)
	je	.L1178
.L1171:
	movq	%rsi, (%r14)
	addq	$8, 8(%r12)
.L1179:
	movq	32(%r12), %rsi
	cmpq	40(%r12), %rsi
	je	.L1185
	movl	-60(%rbp), %eax
	movl	%eax, (%rsi)
	addq	$4, 32(%r12)
.L1186:
	movl	-60(%rbp), %eax
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1173:
	movl	%eax, -60(%rbp)
	movq	32(%r12), %rsi
	cmpq	40(%r12), %rsi
	je	.L1175
	movl	%eax, (%rsi)
	addq	$4, 32(%r12)
.L1169:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1211
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1175:
	.cfi_restore_state
	leaq	-60(%rbp), %rdx
	leaq	24(%r12), %rdi
	movl	%eax, -72(%rbp)
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movl	-72(%rbp), %eax
	jmp	.L1169
.L1210:
	movl	$0, -60(%rbp)
	movl	$8, %ecx
	cmpq	16(%rdi), %r14
	jne	.L1171
.L1172:
	movq	%rcx, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rsi
	cmpq	%r13, %r14
	movq	-72(%rbp), %rcx
	movq	%rax, %r15
	movq	%rsi, (%rax,%rbx)
	je	.L1212
	leaq	-8(%r14), %rsi
	leaq	15(%r15), %rax
	subq	%r13, %rsi
	subq	%r13, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L1192
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L1192
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1181:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%r15,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1181
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	0(%r13,%rdx), %rax
	addq	%r15, %rdx
	cmpq	%r8, %rdi
	je	.L1183
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1183:
	leaq	8(%r15,%rsi), %rbx
.L1187:
	addq	$8, %rbx
	testq	%r13, %r13
	je	.L1184
	movq	%r13, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L1184:
	movq	%r15, %xmm0
	movq	%rbx, %xmm2
	addq	%r15, %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%r12)
	movups	%xmm0, (%r12)
	jmp	.L1179
.L1185:
	leaq	-60(%rbp), %rdx
	leaq	24(%r12), %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L1186
.L1178:
	leaq	(%rcx,%rcx), %rax
	movq	%rcx, %rdx
	salq	$4, %rdx
	cmpq	%rax, %rcx
	movabsq	$9223372036854775800, %rax
	cmovbe	%rdx, %rax
	movq	%rax, %rcx
	jmp	.L1172
.L1192:
	movq	%r15, %rdx
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L1180:
	movq	(%rax), %rdi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rdi, -8(%rdx)
	cmpq	%r14, %rax
	jne	.L1180
	jmp	.L1183
.L1212:
	movq	%r15, %rbx
	jmp	.L1187
.L1211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20284:
	.size	_ZN2v88internal8compiler16SourceIdAssigner8GetIdForENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal8compiler16SourceIdAssigner8GetIdForENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.rodata._ZN2v88internal8compiler31JsonPrintAllSourceWithPositionsERSoPNS0_24OptimizedCompilationInfoEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC199:
	.string	"\"sources\" : {"
.LC200:
	.string	"vector::reserve"
.LC201:
	.string	"}, "
.LC202:
	.string	"\"inlinings\" : {"
.LC203:
	.string	"{ \"inliningId\" : "
.LC204:
	.string	", \"sourceId\" : "
.LC205:
	.string	", \"inliningPosition\" : "
	.section	.text._ZN2v88internal8compiler31JsonPrintAllSourceWithPositionsERSoPNS0_24OptimizedCompilationInfoEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler31JsonPrintAllSourceWithPositionsERSoPNS0_24OptimizedCompilationInfoEPNS0_7IsolateE
	.type	_ZN2v88internal8compiler31JsonPrintAllSourceWithPositionsERSoPNS0_24OptimizedCompilationInfoEPNS0_7IsolateE, @function
_ZN2v88internal8compiler31JsonPrintAllSourceWithPositionsERSoPNS0_24OptimizedCompilationInfoEPNS0_7IsolateE:
.LFB20286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$13, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC199(%rip), %rsi
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L1295
	movq	(%rax), %rdx
	movq	31(%rdx), %rsi
	testb	$1, %sil
	jne	.L1296
	movq	%rax, %r14
	testq	%rsi, %rsi
	jne	.L1220
.L1256:
	xorl	%r10d, %r10d
.L1219:
	testq	%r14, %r14
	je	.L1215
	movq	(%r14), %rax
	movq	%r10, -152(%rbp)
	leaq	-120(%rbp), %r15
	movq	%rax, -128(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%rax, -120(%rbp)
	leaq	-112(%rbp), %rax
	movl	$1, %edx
	movq	%rax, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-152(%rbp), %r10
.L1225:
	subq	$8, %rsp
	movq	%r10, %rcx
	movl	$-1, %esi
	movq	%r12, %rdi
	pushq	$1
	movq	-176(%rbp), %rdx
	movq	%r14, %r9
	movq	%r13, %r8
	call	_ZN2v88internal8compiler23JsonPrintFunctionSourceERSoiSt10unique_ptrIA_cSt14default_deleteIS4_EENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateENS8_INS0_18SharedFunctionInfoEEEb
	movq	-112(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L1226
	call	_ZdaPv@PLT
.L1226:
	movq	104(%rbx), %rdx
	movq	96(%rbx), %rax
	pxor	%xmm0, %xmm0
	movabsq	$1152921504606846975, %rcx
	movaps	%xmm0, -112(%rbp)
	movq	%rdx, %r14
	movaps	%xmm0, -96(%rbp)
	subq	%rax, %r14
	movaps	%xmm0, -80(%rbp)
	sarq	$5, %r14
	cmpq	%rcx, %r14
	ja	.L1297
	testq	%r14, %r14
	jne	.L1298
.L1228:
	cmpq	%rax, %rdx
	je	.L1249
	leaq	-128(%rbp), %rax
	xorl	%r14d, %r14d
	leaq	-120(%rbp), %r15
	movl	$0, -152(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-136(%rbp), %rax
	movq	%rax, -184(%rbp)
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1300:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
.L1245:
	movq	(%r14), %rax
	movq	-184(%rbp), %rdi
	movq	%r10, -168(%rbp)
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	-192(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	%r12, %rdi
	pushq	$1
	movq	-168(%rbp), %r10
	movq	%r14, %r9
	movq	%r13, %r8
	movl	-156(%rbp), %esi
	movq	%r10, %rcx
	call	_ZN2v88internal8compiler23JsonPrintFunctionSourceERSoiSt10unique_ptrIA_cSt14default_deleteIS4_EENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateENS8_INS0_18SharedFunctionInfoEEEb
	movq	-120(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1247
	call	_ZdaPv@PLT
.L1247:
	addl	$1, -152(%rbp)
	movq	104(%rbx), %rax
	movl	-152(%rbp), %r14d
	subq	96(%rbx), %rax
	sarq	$5, %rax
	cmpq	%rax, %r14
	jnb	.L1249
.L1240:
	movl	$2, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	salq	$5, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	96(%rbx), %r14
	movq	-176(%rbp), %rdi
	movq	(%r14), %r14
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler16SourceIdAssigner8GetIdForENS0_6HandleINS0_18SharedFunctionInfoEEE
	movl	%eax, -156(%rbp)
	movq	(%r14), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L1299
.L1243:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L1300
	movq	41088(%r13), %r10
	cmpq	41096(%r13), %r10
	je	.L1301
.L1246:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r10)
	jmp	.L1245
	.p2align 4,,10
	.p2align 3
.L1299:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L1243
	movq	23(%rsi), %rsi
	jmp	.L1243
	.p2align 4,,10
	.p2align 3
.L1249:
	movl	$3, %edx
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	leaq	.LC201(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$15, %edx
	leaq	.LC202(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-120(%rbp), %rcx
	movq	96(%rbx), %rax
	movq	%rcx, -152(%rbp)
	cmpq	%rax, 104(%rbx)
	jne	.L1241
	jmp	.L1242
	.p2align 4,,10
	.p2align 3
.L1302:
	movl	$2, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	96(%rbx), %rax
.L1241:
	movq	-88(%rbp), %rdx
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	movl	(%rdx,%r15,4), %r14d
	movl	$1, %edx
	salq	$5, %r15
	addq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$4, %edx
	leaq	.LC24(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$17, %edx
	leaq	.LC203(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$15, %edx
	leaq	.LC204(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movq	16(%r15), %rax
	movq	%rax, -120(%rbp)
	testb	$1, %al
	jne	.L1250
	movq	%rax, %rdx
	shrq	$31, %rax
	shrq	%rdx
	movzwl	%ax, %eax
	andl	$1073741823, %edx
	orl	%eax, %edx
	je	.L1251
.L1250:
	leaq	.LC205(%rip), %rsi
	movq	%r12, %rdi
	movl	$23, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-152(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal14SourcePosition9PrintJsonERSo@PLT
.L1251:
	movl	$1, %edx
	leaq	.LC42(%rip), %rsi
	leal	1(%r13), %r15d
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	104(%rbx), %rax
	subq	96(%rbx), %rax
	movq	%r15, %r13
	sarq	$5, %rax
	cmpq	%rax, %r15
	jb	.L1302
.L1242:
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC42(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1252
	call	_ZdlPv@PLT
.L1252:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1213
	call	_ZdlPv@PLT
.L1213:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1303
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1301:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1295:
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
.L1215:
	movl	$1, %edi
	movq	%r10, -152(%rbp)
	call	_Znam@PLT
	movq	-152(%rbp), %r10
	movb	$0, (%rax)
	movq	%rax, -112(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -176(%rbp)
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1298:
	leaq	0(,%r14,8), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -152(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	-152(%rbp), %rcx
	movq	%rax, %r15
	cmpq	%rdi, %rsi
	je	.L1236
	leaq	-8(%rsi), %rax
	leaq	15(%rdi), %rdx
	subq	%rdi, %rax
	subq	%r15, %rdx
	shrq	$3, %rax
	cmpq	$30, %rdx
	jbe	.L1258
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rax
	je	.L1258
	addq	$1, %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1234:
	movdqu	(%rdi,%rdx), %xmm1
	movups	%xmm1, (%r15,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L1234
	movq	%rax, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%rdi,%rdx), %rsi
	addq	%r15, %rdx
	cmpq	%rax, %r8
	je	.L1230
	movq	(%rsi), %rax
	movq	%rax, (%rdx)
.L1230:
	movq	%rcx, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %rcx
.L1231:
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %rax
	movq	%r15, %xmm0
	addq	%rcx, %r15
	punpcklqdq	%xmm0, %xmm0
	movq	%r15, -96(%rbp)
	subq	%rdx, %rax
	movaps	%xmm0, -112(%rbp)
	sarq	$2, %rax
	cmpq	%rax, %r14
	jbe	.L1293
	movq	-80(%rbp), %r15
	salq	$2, %r14
	movq	%r14, %rdi
	subq	%rdx, %r15
	call	_Znwm@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rdx
	movq	%rax, %rcx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L1304
	testq	%r8, %r8
	jne	.L1238
.L1239:
	addq	%rcx, %r15
	addq	%rcx, %r14
	movq	%rcx, -88(%rbp)
	movq	%r15, -80(%rbp)
	movq	%r14, -72(%rbp)
.L1293:
	movq	104(%rbx), %rdx
	movq	96(%rbx), %rax
	jmp	.L1228
	.p2align 4,,10
	.p2align 3
.L1296:
	movq	-1(%rsi), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L1217
	movq	23(%rsi), %rsi
.L1217:
	movq	%rax, %r14
	testq	%rsi, %rsi
	je	.L1256
	movq	(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	je	.L1220
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	je	.L1305
.L1220:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1221
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
.L1222:
	movq	24(%rbx), %r14
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1304:
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%r8, -152(%rbp)
	call	memmove@PLT
	movq	-152(%rbp), %r8
	movq	%rax, %rcx
.L1238:
	movq	%r8, %rdi
	movq	%rcx, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %rcx
	jmp	.L1239
.L1258:
	movq	%r15, %rdx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L1232:
	movq	(%rax), %r8
	addq	$8, %rax
	addq	$8, %rdx
	movq	%r8, -8(%rdx)
	cmpq	%rax, %rsi
	jne	.L1232
.L1236:
	testq	%rdi, %rdi
	je	.L1231
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1221:
	movq	41088(%r13), %r10
	cmpq	%r10, 41096(%r13)
	je	.L1306
.L1223:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r10)
	jmp	.L1222
.L1305:
	movq	23(%rsi), %rsi
	jmp	.L1220
.L1306:
	movq	%r13, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %r10
	jmp	.L1223
.L1297:
	leaq	.LC200(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1303:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20286:
	.size	_ZN2v88internal8compiler31JsonPrintAllSourceWithPositionsERSoPNS0_24OptimizedCompilationInfoEPNS0_7IsolateE, .-_ZN2v88internal8compiler31JsonPrintAllSourceWithPositionsERSoPNS0_24OptimizedCompilationInfoEPNS0_7IsolateE
	.section	.text._ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB25493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$6, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L1321
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L1309:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L1310
	movq	%r15, %rbx
	jmp	.L1315
	.p2align 4,,10
	.p2align 3
.L1311:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1322
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1312:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L1310
.L1315:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L1311
	cmpq	$63, 8(%rax)
	jbe	.L1311
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L1315
.L1310:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$63, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,8), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1322:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1321:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1309
	.cfi_endproc
.LFE25493:
	.size	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	.type	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb, @function
_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb:
.LFB25511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	88(%rdi), %rax
	movq	56(%rdi), %rsi
	movq	24(%rdi), %rbx
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	1(%r14,%rcx), %r8
	leaq	(%r8,%r8), %rcx
	cmpq	%rcx, %rbx
	jbe	.L1324
	subq	%r8, %rbx
	shrq	%rbx
	salq	$3, %rbx
	testb	%dl, %dl
	leaq	(%rbx,%r14,8), %rcx
	cmovne	%rcx, %rbx
	addq	$8, %rax
	addq	16(%rdi), %rbx
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	%rbx, %rsi
	jbe	.L1326
	cmpq	%rax, %rsi
	je	.L1327
	movq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L1327
	.p2align 4,,10
	.p2align 3
.L1324:
	cmpq	%r14, %rbx
	movq	%r14, %rax
	movq	(%rdi), %rdi
	cmovnb	%rbx, %rax
	movq	16(%rdi), %r15
	leaq	2(%rbx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L1334
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L1329:
	movq	%rcx, %rax
	subq	%r8, %rax
	shrq	%rax
	salq	$3, %rax
	testb	%dl, %dl
	leaq	(%rax,%r14,8), %rsi
	cmovne	%rsi, %rax
	movq	56(%r12), %rsi
	leaq	(%r15,%rax), %rbx
	movq	88(%r12), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1331
	subq	%rsi, %rdx
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L1331:
	movq	24(%r12), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L1332
	movq	16(%r12), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L1332:
	movq	%r15, 16(%r12)
	movq	%rcx, 24(%r12)
.L1327:
	movq	%rbx, 56(%r12)
	movq	(%rbx), %rax
	movq	(%rbx), %xmm0
	addq	%r13, %rbx
	addq	$512, %rax
	movq	%rbx, 88(%r12)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%r12)
	movq	(%rbx), %rax
	movq	%rax, 72(%r12)
	addq	$512, %rax
	movq	%rax, 80(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1326:
	.cfi_restore_state
	cmpq	%rax, %rsi
	je	.L1327
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L1327
	.p2align 4,,10
	.p2align 3
.L1334:
	movl	%edx, -68(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	movl	-68(%rbp), %edx
	movq	%rax, %r15
	jmp	.L1329
	.cfi_endproc
.LFE25511:
	.size	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb, .-_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_5AsRPOE.str1.8,"aMS",@progbits,1
	.align 8
.LC206:
	.string	"../deps/v8/src/compiler/graph-visualizer.cc:838"
	.align 8
.LC207:
	.string	"cannot create std::vector larger than max_size()"
	.align 8
.LC208:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_5AsRPOE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_5AsRPOE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_5AsRPOE, @function
_ZN2v88internal8compilerlsERSoRKNS1_5AsRPOE:
.LFB20352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	leaq	.LC206(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	-448(%rbp), %rsi
	subq	$440, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal19AccountingAllocatorE(%rip), %rax
	movq	%rsi, -480(%rbp)
	movq	%rax, -448(%rbp)
	leaq	-416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -472(%rbp)
	movups	%xmm0, -440(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	(%rbx), %rax
	movl	28(%rax), %edx
	movl	28(%rax), %eax
	testl	%eax, %eax
	js	.L1432
	xorl	%r14d, %r14d
	testq	%rdx, %rdx
	je	.L1337
	movq	-400(%rbp), %rax
	movq	-392(%rbp), %rcx
	leaq	7(%rdx), %rsi
	andq	$-8, %rsi
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L1433
	addq	%rax, %rsi
	movq	%rsi, -400(%rbp)
.L1339:
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	memset@PLT
.L1337:
	movq	-472(%rbp), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	-256(%rbp), %rdi
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	%rax, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	pxor	%xmm0, %xmm0
	je	.L1434
	movdqa	-256(%rbp), %xmm3
	leaq	-160(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-112(%rbp), %xmm5
	movq	-200(%rbp), %r8
	movq	-208(%rbp), %xmm0
	movq	-192(%rbp), %rcx
	movq	-184(%rbp), %r12
	movdqa	-96(%rbp), %xmm6
	movaps	%xmm5, -208(%rbp)
	movq	%r8, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movq	-216(%rbp), %r9
	movq	-176(%rbp), %rdx
	movq	-168(%rbp), %r13
	movdqa	-128(%rbp), %xmm4
	movaps	%xmm6, -192(%rbp)
	movq	%r12, %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	-248(%rbp), %rax
	movaps	%xmm0, -304(%rbp)
	movq	%rcx, %xmm0
	movq	-224(%rbp), %xmm1
	punpcklqdq	%xmm6, %xmm0
	movq	-256(%rbp), %xmm2
	movaps	%xmm4, -224(%rbp)
	movq	-240(%rbp), %r11
	movq	-232(%rbp), %r10
	movaps	%xmm7, -176(%rbp)
	movq	%rax, %xmm3
	movq	-144(%rbp), %rsi
	movq	%r9, %xmm4
	punpcklqdq	%xmm3, %xmm2
	movq	-136(%rbp), %rdi
	movaps	%xmm0, -288(%rbp)
	movq	%r13, %xmm7
	movq	%rdx, %xmm0
	punpcklqdq	%xmm4, %xmm1
	punpcklqdq	%xmm7, %xmm0
	movq	%rsi, -240(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%r11, -336(%rbp)
	movq	%r10, -328(%rbp)
	movaps	%xmm2, -352(%rbp)
	movaps	%xmm1, -320(%rbp)
	movaps	%xmm0, -272(%rbp)
	testq	%rsi, %rsi
	je	.L1341
	movq	-168(%rbp), %rcx
	movq	-200(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L1342
	.p2align 4,,10
	.p2align 3
.L1345:
	testq	%rax, %rax
	je	.L1343
	cmpq	$64, 8(%rax)
	ja	.L1344
.L1343:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -248(%rbp)
.L1344:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L1345
	movq	-232(%rbp), %rdi
	movq	-240(%rbp), %rsi
.L1342:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L1346
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L1346:
	movq	-288(%rbp), %rcx
	movq	-272(%rbp), %rdx
.L1341:
	movq	(%rbx), %rax
	subq	$8, %rdx
	movq	16(%rax), %r12
	cmpq	%rdx, %rcx
	je	.L1347
.L1442:
	movq	%r12, (%rcx)
	addq	$8, -288(%rbp)
.L1348:
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movl	20(%rax), %eax
	andl	$16777215, %eax
	movb	$1, (%r14,%rax)
	movq	-288(%rbp), %rax
	cmpq	-320(%rbp), %rax
	je	.L1356
	.p2align 4,,10
	.p2align 3
.L1355:
	cmpq	%rax, -280(%rbp)
	je	.L1435
.L1357:
	movq	-8(%rax), %rcx
	movl	20(%rcx), %esi
	leaq	32(%rcx), %rbx
	movq	%rcx, -464(%rbp)
	movq	%rbx, %rdx
	movl	%esi, %eax
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1359
	movq	32(%rcx), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
.L1359:
	cltq
	leaq	(%rdx,%rax,8), %rcx
	cmpq	%rdx, %rcx
	jne	.L1370
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1361:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	je	.L1360
.L1370:
	movq	(%rdx), %r12
	movl	20(%r12), %eax
	andl	$16777215, %eax
	addq	%r14, %rax
	cmpb	$0, (%rax)
	jne	.L1361
	movb	$1, (%rax)
	movq	-272(%rbp), %rax
	movq	-288(%rbp), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1362
	movq	%r12, (%rdx)
	movq	-288(%rbp), %rax
	addq	$8, %rax
	movq	%rax, -288(%rbp)
.L1363:
	cmpq	%rax, -320(%rbp)
	jne	.L1355
.L1356:
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L1388
	movq	-264(%rbp), %rbx
	movq	-296(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L1389
	movq	-344(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L1392:
	testq	%rax, %rax
	je	.L1390
	cmpq	$64, 8(%rax)
	ja	.L1391
.L1390:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-344(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -344(%rbp)
.L1391:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L1392
	movq	-336(%rbp), %rax
.L1389:
	movq	-328(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L1388
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L1388:
	movq	-472(%rbp), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-480(%rbp), %rdi
	call	_ZN2v88internal19AccountingAllocatorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1436
	addq	$440, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1360:
	.cfi_restore_state
	andl	$16777215, %esi
	movb	$2, (%r14,%rsi)
	movq	-288(%rbp), %rax
	movq	-280(%rbp), %rdx
	cmpq	%rdx, %rax
	je	.L1371
	subq	$8, %rax
	movq	%rax, -288(%rbp)
.L1372:
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-464(%rbp), %r13
	movq	%r15, %rdi
	movl	20(%r13), %esi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1376
	movq	-464(%rbp), %rax
	movq	32(%rax), %rbx
	movl	8(%rbx), %eax
	addq	$16, %rbx
.L1376:
	cltq
	leaq	(%rbx,%rax,8), %rax
	movq	%rax, -456(%rbp)
	cmpq	%rax, %rbx
	je	.L1384
	movq	(%rbx), %r12
	addq	$8, %rbx
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1381:
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
.L1394:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%rbx, -456(%rbp)
	je	.L1384
.L1383:
	movl	$2, %edx
	movq	%r15, %rdi
	movq	(%rbx), %r12
	addq	$8, %rbx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1395:
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r12, %r12
	je	.L1380
	movl	20(%r12), %esi
	movq	%r15, %rdi
	andl	$16777215, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	8(%rax), %r12
	testq	%r12, %r12
	jne	.L1381
	movq	0(%r13), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	cmpq	%rbx, -456(%rbp)
	jne	.L1383
.L1384:
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-464(%rbp), %rax
	cmpq	$0, 8(%rax)
	jne	.L1437
.L1379:
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %r12
	testq	%r12, %r12
	je	.L1438
	cmpb	$0, 56(%r12)
	je	.L1386
	movsbl	67(%r12), %esi
.L1387:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-288(%rbp), %rax
	jmp	.L1363
.L1380:
	movl	$-1, %esi
	movq	%r15, %rdi
	leaq	.LC0(%rip), %r12
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$4, %edx
	jmp	.L1394
	.p2align 4,,10
	.p2align 3
.L1435:
	movq	-264(%rbp), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1386:
	movq	%r12, %rdi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rbx
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	movq	48(%rax), %rax
	cmpq	%rbx, %rax
	je	.L1387
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1387
	.p2align 4,,10
	.p2align 3
.L1437:
	movl	$9, %edx
	movq	%r15, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-464(%rbp), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v88internal8compilerlsERSoNS1_4TypeE@PLT
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1379
.L1371:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L1373
	cmpq	$64, 8(%rax)
	ja	.L1374
.L1373:
	movq	$64, 8(%rdx)
	movq	-344(%rbp), %rax
	movq	%rax, (%rdx)
	movq	%rdx, -344(%rbp)
.L1374:
	movq	-264(%rbp), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, -264(%rbp)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -280(%rbp)
	addq	$504, %rax
	movq	%rdx, -272(%rbp)
	movq	%rax, -288(%rbp)
	jmp	.L1372
.L1362:
	movq	-264(%rbp), %rbx
	subq	-280(%rbp), %rdx
	sarq	$3, %rdx
	movq	%rbx, %rax
	subq	-296(%rbp), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$6, %rax
	addq	%rax, %rdx
	movq	-304(%rbp), %rax
	subq	-320(%rbp), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L1364
	movq	-328(%rbp), %rcx
	movq	%rbx, %rax
	subq	-336(%rbp), %rax
	sarq	$3, %rax
	subq	%rax, %rcx
	cmpq	$1, %rcx
	jbe	.L1439
.L1365:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L1366
	cmpq	$63, 8(%rax)
	ja	.L1440
.L1366:
	movq	-352(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1441
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1367:
	movq	%rax, 8(%rbx)
	movq	-288(%rbp), %rax
	movq	%r12, (%rax)
	movq	-264(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -264(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -280(%rbp)
	movq	%rdx, -272(%rbp)
	movq	%rax, -288(%rbp)
	jmp	.L1363
.L1439:
	leaq	-352(%rbp), %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	movq	-264(%rbp), %rbx
	jmp	.L1365
.L1434:
	movq	-232(%rbp), %rax
	movq	-176(%rbp), %rdx
	movq	$0, -336(%rbp)
	movdqa	-256(%rbp), %xmm1
	movdqa	-224(%rbp), %xmm3
	movq	%rax, -328(%rbp)
	movdqa	-208(%rbp), %xmm4
	subq	$8, %rdx
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm6
	movaps	%xmm3, -320(%rbp)
	movq	(%rbx), %rax
	movq	-192(%rbp), %rcx
	movaps	%xmm4, -304(%rbp)
	movaps	%xmm5, -288(%rbp)
	movaps	%xmm6, -272(%rbp)
	movq	16(%rax), %r12
	movaps	%xmm1, -352(%rbp)
	cmpq	%rdx, %rcx
	jne	.L1442
.L1347:
	movq	-264(%rbp), %r13
	subq	-280(%rbp), %rcx
	sarq	$3, %rcx
	movq	%r13, %rax
	subq	-296(%rbp), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$6, %rax
	addq	%rax, %rcx
	movq	-304(%rbp), %rax
	subq	-320(%rbp), %rax
	sarq	$3, %rax
	addq	%rcx, %rax
	cmpq	$268435455, %rax
	je	.L1364
	movq	-328(%rbp), %rcx
	movq	%r13, %rax
	subq	-336(%rbp), %rax
	sarq	$3, %rax
	subq	%rax, %rcx
	cmpq	$1, %rcx
	jbe	.L1443
.L1350:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L1351
	cmpq	$63, 8(%rax)
	ja	.L1444
.L1351:
	movq	-352(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1445
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1352:
	movq	%rax, 8(%r13)
	movq	-288(%rbp), %rax
	movq	%r12, (%rax)
	movq	-264(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -264(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -280(%rbp)
	movq	%rdx, -272(%rbp)
	movq	%rax, -288(%rbp)
	jmp	.L1348
.L1440:
	movq	(%rax), %rdx
	movq	%rdx, -344(%rbp)
	jmp	.L1367
.L1433:
	movq	-472(%rbp), %rdi
	movq	%rdx, -456(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-456(%rbp), %rdx
	jmp	.L1339
.L1443:
	leaq	-352(%rbp), %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	movq	-264(%rbp), %r13
	jmp	.L1350
.L1444:
	movq	(%rax), %rdx
	movq	%rdx, -344(%rbp)
	jmp	.L1352
.L1441:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1367
.L1445:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1352
.L1438:
	call	_ZSt16__throw_bad_castv@PLT
.L1436:
	call	__stack_chk_fail@PLT
.L1364:
	leaq	.LC208(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1432:
	leaq	.LC207(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE20352:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_5AsRPOE, .-_ZN2v88internal8compilerlsERSoRKNS1_5AsRPOE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler31get_cached_trace_turbo_filenameEPNS0_24OptimizedCompilationInfoE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler31get_cached_trace_turbo_filenameEPNS0_24OptimizedCompilationInfoE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler31get_cached_trace_turbo_filenameEPNS0_24OptimizedCompilationInfoE:
.LFB25752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25752:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler31get_cached_trace_turbo_filenameEPNS0_24OptimizedCompilationInfoE, .-_GLOBAL__sub_I__ZN2v88internal8compiler31get_cached_trace_turbo_filenameEPNS0_24OptimizedCompilationInfoE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler31get_cached_trace_turbo_filenameEPNS0_24OptimizedCompilationInfoE
	.hidden	_ZTCN2v88internal8compiler13TurboJsonFileE0_So
	.weak	_ZTCN2v88internal8compiler13TurboJsonFileE0_So
	.section	.rodata._ZTCN2v88internal8compiler13TurboJsonFileE0_So,"aG",@progbits,_ZTVN2v88internal8compiler13TurboJsonFileE,comdat
	.align 8
	.type	_ZTCN2v88internal8compiler13TurboJsonFileE0_So, @object
	.size	_ZTCN2v88internal8compiler13TurboJsonFileE0_So, 80
_ZTCN2v88internal8compiler13TurboJsonFileE0_So:
	.quad	248
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-248
	.quad	-248
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE
	.weak	_ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE
	.section	.rodata._ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE,"aG",@progbits,_ZTVN2v88internal8compiler13TurboJsonFileE,comdat
	.align 8
	.type	_ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE, @object
	.size	_ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE, 80
_ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE:
	.quad	248
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-248
	.quad	-248
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal8compiler13TurboJsonFileE
	.section	.data.rel.ro.local._ZTTN2v88internal8compiler13TurboJsonFileE,"awG",@progbits,_ZTVN2v88internal8compiler13TurboJsonFileE,comdat
	.align 8
	.type	_ZTTN2v88internal8compiler13TurboJsonFileE, @object
	.size	_ZTTN2v88internal8compiler13TurboJsonFileE, 48
_ZTTN2v88internal8compiler13TurboJsonFileE:
	.quad	_ZTVN2v88internal8compiler13TurboJsonFileE+24
	.quad	_ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE+24
	.quad	_ZTCN2v88internal8compiler13TurboJsonFileE0_So+24
	.quad	_ZTCN2v88internal8compiler13TurboJsonFileE0_So+64
	.quad	_ZTCN2v88internal8compiler13TurboJsonFileE0_St14basic_ofstreamIcSt11char_traitsIcEE+64
	.quad	_ZTVN2v88internal8compiler13TurboJsonFileE+64
	.weak	_ZTVN2v88internal8compiler13TurboJsonFileE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler13TurboJsonFileE,"awG",@progbits,_ZTVN2v88internal8compiler13TurboJsonFileE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler13TurboJsonFileE, @object
	.size	_ZTVN2v88internal8compiler13TurboJsonFileE, 80
_ZTVN2v88internal8compiler13TurboJsonFileE:
	.quad	248
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler13TurboJsonFileD1Ev
	.quad	_ZN2v88internal8compiler13TurboJsonFileD0Ev
	.quad	-248
	.quad	-248
	.quad	0
	.quad	_ZTv0_n24_N2v88internal8compiler13TurboJsonFileD1Ev
	.quad	_ZTv0_n24_N2v88internal8compiler13TurboJsonFileD0Ev
	.hidden	_ZTCN2v88internal8compiler12TurboCfgFileE0_So
	.weak	_ZTCN2v88internal8compiler12TurboCfgFileE0_So
	.section	.rodata._ZTCN2v88internal8compiler12TurboCfgFileE0_So,"aG",@progbits,_ZTVN2v88internal8compiler12TurboCfgFileE,comdat
	.align 8
	.type	_ZTCN2v88internal8compiler12TurboCfgFileE0_So, @object
	.size	_ZTCN2v88internal8compiler12TurboCfgFileE0_So, 80
_ZTCN2v88internal8compiler12TurboCfgFileE0_So:
	.quad	248
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-248
	.quad	-248
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE
	.weak	_ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE
	.section	.rodata._ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE,"aG",@progbits,_ZTVN2v88internal8compiler12TurboCfgFileE,comdat
	.align 8
	.type	_ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE, @object
	.size	_ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE, 80
_ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE:
	.quad	248
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-248
	.quad	-248
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal8compiler12TurboCfgFileE
	.section	.data.rel.ro.local._ZTTN2v88internal8compiler12TurboCfgFileE,"awG",@progbits,_ZTVN2v88internal8compiler12TurboCfgFileE,comdat
	.align 8
	.type	_ZTTN2v88internal8compiler12TurboCfgFileE, @object
	.size	_ZTTN2v88internal8compiler12TurboCfgFileE, 48
_ZTTN2v88internal8compiler12TurboCfgFileE:
	.quad	_ZTVN2v88internal8compiler12TurboCfgFileE+24
	.quad	_ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE+24
	.quad	_ZTCN2v88internal8compiler12TurboCfgFileE0_So+24
	.quad	_ZTCN2v88internal8compiler12TurboCfgFileE0_So+64
	.quad	_ZTCN2v88internal8compiler12TurboCfgFileE0_St14basic_ofstreamIcSt11char_traitsIcEE+64
	.quad	_ZTVN2v88internal8compiler12TurboCfgFileE+64
	.weak	_ZTVN2v88internal8compiler12TurboCfgFileE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler12TurboCfgFileE,"awG",@progbits,_ZTVN2v88internal8compiler12TurboCfgFileE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler12TurboCfgFileE, @object
	.size	_ZTVN2v88internal8compiler12TurboCfgFileE, 80
_ZTVN2v88internal8compiler12TurboCfgFileE:
	.quad	248
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler12TurboCfgFileD1Ev
	.quad	_ZN2v88internal8compiler12TurboCfgFileD0Ev
	.quad	-248
	.quad	-248
	.quad	0
	.quad	_ZTv0_n24_N2v88internal8compiler12TurboCfgFileD1Ev
	.quad	_ZTv0_n24_N2v88internal8compiler12TurboCfgFileD0Ev
	.weak	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC209:
	.string	"xmm0"
.LC210:
	.string	"xmm1"
.LC211:
	.string	"xmm2"
.LC212:
	.string	"xmm3"
.LC213:
	.string	"xmm4"
.LC214:
	.string	"xmm5"
.LC215:
	.string	"xmm6"
.LC216:
	.string	"xmm7"
.LC217:
	.string	"xmm8"
.LC218:
	.string	"xmm9"
.LC219:
	.string	"xmm10"
.LC220:
	.string	"xmm11"
.LC221:
	.string	"xmm12"
.LC222:
	.string	"xmm13"
.LC223:
	.string	"xmm14"
.LC224:
	.string	"xmm15"
	.section	.data.rel.ro.local._ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names,"awG",@progbits,_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names,comdat
	.align 32
	.type	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names, @gnu_unique_object
	.size	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names, 128
_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names:
	.quad	.LC209
	.quad	.LC210
	.quad	.LC211
	.quad	.LC212
	.quad	.LC213
	.quad	.LC214
	.quad	.LC215
	.quad	.LC216
	.quad	.LC217
	.quad	.LC218
	.quad	.LC219
	.quad	.LC220
	.quad	.LC221
	.quad	.LC222
	.quad	.LC223
	.quad	.LC224
	.weak	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names
	.section	.rodata.str1.1
.LC225:
	.string	"rax"
.LC226:
	.string	"rcx"
.LC227:
	.string	"rdx"
.LC228:
	.string	"rbx"
.LC229:
	.string	"rsp"
.LC230:
	.string	"rbp"
.LC231:
	.string	"rsi"
.LC232:
	.string	"rdi"
.LC233:
	.string	"r8"
.LC234:
	.string	"r9"
.LC235:
	.string	"r10"
.LC236:
	.string	"r11"
.LC237:
	.string	"r12"
.LC238:
	.string	"r13"
.LC239:
	.string	"r14"
.LC240:
	.string	"r15"
	.section	.data.rel.ro.local._ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names,"awG",@progbits,_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names,comdat
	.align 32
	.type	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names, @gnu_unique_object
	.size	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names, 128
_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names:
	.quad	.LC225
	.quad	.LC226
	.quad	.LC227
	.quad	.LC228
	.quad	.LC229
	.quad	.LC230
	.quad	.LC231
	.quad	.LC232
	.quad	.LC233
	.quad	.LC234
	.quad	.LC235
	.quad	.LC236
	.quad	.LC237
	.quad	.LC238
	.quad	.LC239
	.quad	.LC240
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC43:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC44:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC45:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.align 8
.LC175:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
