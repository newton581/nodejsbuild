	.file	"js-heap-copy-reducer.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler17JSHeapCopyReducer12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"JSHeapCopyReducer"
	.section	.text._ZNK2v88internal8compiler17JSHeapCopyReducer12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler17JSHeapCopyReducer12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler17JSHeapCopyReducer12reducer_nameEv
	.type	_ZNK2v88internal8compiler17JSHeapCopyReducer12reducer_nameEv, @function
_ZNK2v88internal8compiler17JSHeapCopyReducer12reducer_nameEv:
.LFB10163:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10163:
	.size	_ZNK2v88internal8compiler17JSHeapCopyReducer12reducer_nameEv, .-_ZNK2v88internal8compiler17JSHeapCopyReducer12reducer_nameEv
	.section	.text._ZN2v88internal8compiler17JSHeapCopyReducerD2Ev,"axG",@progbits,_ZN2v88internal8compiler17JSHeapCopyReducerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler17JSHeapCopyReducerD2Ev
	.type	_ZN2v88internal8compiler17JSHeapCopyReducerD2Ev, @function
_ZN2v88internal8compiler17JSHeapCopyReducerD2Ev:
.LFB23032:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23032:
	.size	_ZN2v88internal8compiler17JSHeapCopyReducerD2Ev, .-_ZN2v88internal8compiler17JSHeapCopyReducerD2Ev
	.weak	_ZN2v88internal8compiler17JSHeapCopyReducerD1Ev
	.set	_ZN2v88internal8compiler17JSHeapCopyReducerD1Ev,_ZN2v88internal8compiler17JSHeapCopyReducerD2Ev
	.section	.text._ZN2v88internal8compiler17JSHeapCopyReducerD0Ev,"axG",@progbits,_ZN2v88internal8compiler17JSHeapCopyReducerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler17JSHeapCopyReducerD0Ev
	.type	_ZN2v88internal8compiler17JSHeapCopyReducerD0Ev, @function
_ZN2v88internal8compiler17JSHeapCopyReducerD0Ev:
.LFB23034:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23034:
	.size	_ZN2v88internal8compiler17JSHeapCopyReducerD0Ev, .-_ZN2v88internal8compiler17JSHeapCopyReducerD0Ev
	.section	.rodata._ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC1:
	.string	"IsName()"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler7NameRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L8
.L5:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6IsNameEv@PLT
	testb	%al, %al
	jne	.L5
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9654:
	.size	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC3:
	.string	"IsFeedbackCell()"
	.section	.text._ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler15FeedbackCellRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L12
.L9:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef14IsFeedbackCellEv@PLT
	testb	%al, %al
	jne	.L9
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9672:
	.size	_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler15FeedbackCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler15FeedbackCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler15FeedbackCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC4:
	.string	"IsFeedbackVector()"
	.section	.text._ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler17FeedbackVectorRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L16
.L13:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef16IsFeedbackVectorEv@PLT
	testb	%al, %al
	jne	.L13
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9678:
	.size	_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler17FeedbackVectorRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler17FeedbackVectorRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler17FeedbackVectorRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler17AllocationSiteRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC5:
	.string	"IsAllocationSite()"
	.section	.text._ZN2v88internal8compiler17AllocationSiteRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler17AllocationSiteRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler17AllocationSiteRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler17AllocationSiteRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler17AllocationSiteRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L20
.L17:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef16IsAllocationSiteEv@PLT
	testb	%al, %al
	jne	.L17
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9696:
	.size	_ZN2v88internal8compiler17AllocationSiteRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler17AllocationSiteRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler17AllocationSiteRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler17AllocationSiteRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler17AllocationSiteRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC6:
	.string	"IsMap()"
	.section	.text._ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler6MapRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L24
.L21:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef5IsMapEv@PLT
	testb	%al, %al
	jne	.L21
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9708:
	.size	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC7:
	.string	"IsScopeInfo()"
	.section	.text._ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler12ScopeInfoRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L28
.L25:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef11IsScopeInfoEv@PLT
	testb	%al, %al
	jne	.L25
	leaq	.LC7(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9753:
	.size	_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC8:
	.string	"IsSharedFunctionInfo()"
	.section	.text._ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler21SharedFunctionInfoRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L32
.L29:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef20IsSharedFunctionInfoEv@PLT
	testb	%al, %al
	jne	.L29
	leaq	.LC8(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9759:
	.size	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler17JSHeapCopyReducer6ReduceEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"(location_) != nullptr"
.LC10:
	.string	"IsHeapObject()"
	.section	.rodata._ZN2v88internal8compiler17JSHeapCopyReducer6ReduceEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal8compiler17JSHeapCopyReducer6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSHeapCopyReducer6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSHeapCopyReducer6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSHeapCopyReducer6ReduceEPNS1_4NodeE:
.LFB19121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdi), %eax
	cmpw	$754, %ax
	ja	.L34
	cmpw	$716, %ax
	jbe	.L141
	subw	$717, %ax
	cmpw	$37, %ax
	ja	.L34
	leaq	.L41(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler17JSHeapCopyReducer6ReduceEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L41:
	.long	.L54-.L41
	.long	.L53-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L52-.L41
	.long	.L51-.L41
	.long	.L34-.L41
	.long	.L50-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L49-.L41
	.long	.L49-.L41
	.long	.L49-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L47-.L41
	.long	.L46-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L45-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L34-.L41
	.long	.L44-.L41
	.long	.L42-.L41
	.long	.L42-.L41
	.long	.L42-.L41
	.section	.text._ZN2v88internal8compiler17JSHeapCopyReducer6ReduceEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L38:
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L34
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %r12
	movq	8(%rbx), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	testb	%al, %al
	jne	.L142
.L58:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10IsJSObjectEv@PLT
	testb	%al, %al
	jne	.L143
.L59:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef18IsSourceTextModuleEv@PLT
	testb	%al, %al
	je	.L34
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef18AsSourceTextModuleEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler19SourceTextModuleRef9SerializeEv@PLT
.L34:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L144
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	cmpw	$220, %ax
	je	.L36
	jbe	.L145
	cmpw	$240, %ax
	je	.L55
	cmpw	$245, %ax
	jne	.L146
.L55:
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L34
	call	_ZN2v88internal8compiler13FieldAccessOfEPKNS1_8OperatorE@PLT
	movq	16(%rax), %rdx
	movq	8(%rax), %r12
	testq	%rdx, %rdx
	je	.L66
	movq	8(%rbx), %rsi
	leaq	-80(%rbp), %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
.L66:
	testq	%r12, %r12
	je	.L34
	movq	8(%rbx), %rsi
	leaq	-80(%rbp), %rdi
	movl	$1, %ecx
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L145:
	cmpw	$30, %ax
	je	.L38
	cmpw	$57, %ax
	jne	.L34
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L34
	call	_ZN2v88internal8compiler14MapGuardMapsOfEPKNS1_8OperatorE@PLT
	movq	(%rax), %rdx
	movq	%rax, %r12
	movq	%rdx, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L34
	testq	%rax, %rax
	je	.L83
	movq	14(%rdx), %r13
	subq	6(%rdx), %r13
	sarq	$3, %r13
	je	.L34
.L67:
	xorl	%r15d, %r15d
	leaq	-80(%rbp), %r14
	testb	$3, %dl
	je	.L69
.L147:
	movq	6(%rdx), %rax
	subq	$2, %rdx
	movslq	%r15d, %rsi
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L137
	movq	(%rax,%rsi,8), %rdx
.L69:
	movq	8(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	addq	$1, %r15
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	cmpq	%r15, %r13
	je	.L34
	movq	(%r12), %rdx
	testb	$3, %dl
	je	.L69
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L146:
	cmpw	$233, %ax
	jne	.L34
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L34
	call	_ZN2v88internal8compiler23CompareMapsParametersOfEPKNS1_8OperatorE@PLT
	movq	(%rax), %rdx
	movq	%rax, %r12
	movq	%rdx, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L34
	testq	%rax, %rax
	je	.L85
	movq	14(%rdx), %r13
	subq	6(%rdx), %r13
	sarq	$3, %r13
	je	.L34
.L77:
	xorl	%r15d, %r15d
	leaq	-80(%rbp), %r14
	testb	$3, %dl
	je	.L79
.L148:
	movq	6(%rdx), %rax
	subq	$2, %rdx
	movslq	%r15d, %rsi
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L137
	movq	(%rax,%rsi,8), %rdx
.L79:
	movq	8(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	addq	$1, %r15
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	cmpq	%r13, %r15
	je	.L34
	movq	(%r12), %rdx
	testb	$3, %dl
	je	.L79
	jmp	.L148
.L42:
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L34
	call	_ZN2v88internal8compiler11ScopeInfoOfEPKNS1_8OperatorE@PLT
	movq	8(%rbx), %rsi
	leaq	-80(%rbp), %rdi
	movl	$1, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	jmp	.L34
.L49:
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L34
	call	_ZN2v88internal8compiler25CreateLiteralParametersOfEPKNS1_8OperatorE@PLT
	movq	8(%rax), %rdx
.L134:
	leaq	-80(%rbp), %r12
	movq	8(%rbx), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17FeedbackVectorRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17FeedbackVectorRef9SerializeEv@PLT
	jmp	.L34
.L50:
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L34
	call	_ZN2v88internal8compiler19FeedbackParameterOfEPKNS1_8OperatorE@PLT
	movq	(%rax), %rdx
	jmp	.L134
.L44:
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L34
	call	_ZN2v88internal8compiler33CreateFunctionContextParametersOfEPKNS1_8OperatorE@PLT
	movq	8(%rbx), %rsi
	leaq	-80(%rbp), %rdi
	movl	$1, %ecx
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	jmp	.L34
.L47:
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L34
	call	_ZN2v88internal8compiler16PropertyAccessOfEPKNS1_8OperatorE@PLT
	cmpq	$0, (%rax)
	movq	%rax, %rsi
	je	.L34
	cmpl	$-1, 8(%rax)
	je	.L34
	subq	$8, %rsp
	movb	$0, -80(%rbp)
	movq	8(%rbx), %rdi
	xorl	%edx, %edx
	pushq	-64(%rbp)
	movb	$0, -72(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8compiler12JSHeapBroker32ProcessFeedbackForPropertyAccessERKNS1_14FeedbackSourceENS1_10AccessModeENS_4base8OptionalINS1_7NameRefEEE@PLT
	addq	$32, %rsp
	jmp	.L34
.L52:
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L34
	call	_ZN2v88internal8compiler31CreateBoundFunctionParametersOfEPKNS1_8OperatorE@PLT
	movq	8(%rbx), %rsi
	leaq	-80(%rbp), %rdi
	movl	$1, %ecx
	movq	8(%rax), %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	jmp	.L34
.L51:
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L34
	call	_ZN2v88internal8compiler25CreateClosureParametersOfEPKNS1_8OperatorE@PLT
	leaq	-80(%rbp), %r13
	movq	8(%rbx), %rsi
	movl	$1, %ecx
	movq	(%rax), %rdx
	movq	%rax, %r12
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	8(%r12), %rdx
	movq	8(%rbx), %rsi
	movq	%r13, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler15FeedbackCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	8(%rbx), %rsi
	movq	16(%r12), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	jne	.L34
	leaq	.LC10(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L46:
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L34
	call	_ZN2v88internal8compiler13NamedAccessOfEPKNS1_8OperatorE@PLT
	movq	8(%rbx), %rsi
	leaq	-96(%rbp), %rdi
	movl	$1, %ecx
	movq	(%rax), %rdx
	movq	%rax, %r12
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	cmpq	$0, 8(%r12)
	je	.L34
	cmpl	$-1, 16(%r12)
	je	.L34
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	subq	$8, %rsp
	movb	$1, -80(%rbp)
	movq	8(%rbx), %rdi
	leaq	8(%r12), %rsi
	pushq	%rdx
	pushq	%rax
	pushq	-80(%rbp)
	movq	%rdx, -64(%rbp)
	xorl	%edx, %edx
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler12JSHeapBroker32ProcessFeedbackForPropertyAccessERKNS1_14FeedbackSourceENS1_10AccessModeENS_4base8OptionalINS1_7NameRefEEE@PLT
	addq	$32, %rsp
	jmp	.L34
.L45:
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L34
	call	_ZN2v88internal8compiler13NamedAccessOfEPKNS1_8OperatorE@PLT
	movq	8(%rbx), %rsi
	leaq	-80(%rbp), %rdi
	movl	$1, %ecx
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	jmp	.L34
.L54:
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L34
	movq	%rsi, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler16FrameStateInfoOfEPKNS1_8OperatorE@PLT
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L62
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L62
	movq	8(%rbx), %rsi
	leaq	-80(%rbp), %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	jmp	.L34
.L53:
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L34
	call	_ZN2v88internal8compiler23CreateArrayParametersOfEPKNS1_8OperatorE@PLT
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L34
	movq	8(%rbx), %rsi
	leaq	-80(%rbp), %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler17AllocationSiteRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L36:
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L34
	call	_ZN2v88internal8compiler21CheckMapsParametersOfEPKNS1_8OperatorE@PLT
	movq	8(%rax), %rdx
	movq	%rax, %r12
	movq	%rdx, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L34
	testq	%rax, %rax
	je	.L84
	movq	14(%rdx), %r13
	subq	6(%rdx), %r13
	sarq	$3, %r13
	je	.L34
.L72:
	xorl	%r15d, %r15d
	leaq	-80(%rbp), %r14
	testb	$3, %dl
	je	.L74
.L149:
	movq	6(%rdx), %rax
	subq	$2, %rdx
	movslq	%r15d, %rsi
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L137
	movq	(%rax,%rsi,8), %rdx
.L74:
	movq	8(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	addq	$1, %r15
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	cmpq	%r15, %r13
	je	.L34
	movq	8(%r12), %rdx
	testb	$3, %dl
	je	.L74
	jmp	.L149
.L143:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10AsJSObjectEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler11JSObjectRef24SerializeObjectCreateMapEv@PLT
	jmp	.L59
.L142:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler13JSFunctionRef9SerializeEv@PLT
	jmp	.L58
.L85:
	movl	$1, %r13d
	jmp	.L77
.L84:
	movl	$1, %r13d
	jmp	.L72
.L83:
	movl	$1, %r13d
	jmp	.L67
.L62:
	leaq	.LC9(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L144:
	call	__stack_chk_fail@PLT
.L137:
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE19121:
	.size	_ZN2v88internal8compiler17JSHeapCopyReducer6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSHeapCopyReducer6ReduceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSHeapCopyReducerC2EPNS1_12JSHeapBrokerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSHeapCopyReducerC2EPNS1_12JSHeapBrokerE
	.type	_ZN2v88internal8compiler17JSHeapCopyReducerC2EPNS1_12JSHeapBrokerE, @function
_ZN2v88internal8compiler17JSHeapCopyReducerC2EPNS1_12JSHeapBrokerE:
.LFB19118:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler17JSHeapCopyReducerE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE19118:
	.size	_ZN2v88internal8compiler17JSHeapCopyReducerC2EPNS1_12JSHeapBrokerE, .-_ZN2v88internal8compiler17JSHeapCopyReducerC2EPNS1_12JSHeapBrokerE
	.globl	_ZN2v88internal8compiler17JSHeapCopyReducerC1EPNS1_12JSHeapBrokerE
	.set	_ZN2v88internal8compiler17JSHeapCopyReducerC1EPNS1_12JSHeapBrokerE,_ZN2v88internal8compiler17JSHeapCopyReducerC2EPNS1_12JSHeapBrokerE
	.section	.text._ZN2v88internal8compiler17JSHeapCopyReducer6brokerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSHeapCopyReducer6brokerEv
	.type	_ZN2v88internal8compiler17JSHeapCopyReducer6brokerEv, @function
_ZN2v88internal8compiler17JSHeapCopyReducer6brokerEv:
.LFB19120:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE19120:
	.size	_ZN2v88internal8compiler17JSHeapCopyReducer6brokerEv, .-_ZN2v88internal8compiler17JSHeapCopyReducer6brokerEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler17JSHeapCopyReducerC2EPNS1_12JSHeapBrokerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler17JSHeapCopyReducerC2EPNS1_12JSHeapBrokerE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler17JSHeapCopyReducerC2EPNS1_12JSHeapBrokerE:
.LFB23072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23072:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler17JSHeapCopyReducerC2EPNS1_12JSHeapBrokerE, .-_GLOBAL__sub_I__ZN2v88internal8compiler17JSHeapCopyReducerC2EPNS1_12JSHeapBrokerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler17JSHeapCopyReducerC2EPNS1_12JSHeapBrokerE
	.weak	_ZTVN2v88internal8compiler17JSHeapCopyReducerE
	.section	.data.rel.ro._ZTVN2v88internal8compiler17JSHeapCopyReducerE,"awG",@progbits,_ZTVN2v88internal8compiler17JSHeapCopyReducerE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler17JSHeapCopyReducerE, @object
	.size	_ZTVN2v88internal8compiler17JSHeapCopyReducerE, 56
_ZTVN2v88internal8compiler17JSHeapCopyReducerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler17JSHeapCopyReducerD1Ev
	.quad	_ZN2v88internal8compiler17JSHeapCopyReducerD0Ev
	.quad	_ZNK2v88internal8compiler17JSHeapCopyReducer12reducer_nameEv
	.quad	_ZN2v88internal8compiler17JSHeapCopyReducer6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
