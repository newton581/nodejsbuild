	.file	"csa-load-elimination.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler18CsaLoadElimination12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"CsaLoadElimination"
	.section	.text._ZNK2v88internal8compiler18CsaLoadElimination12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler18CsaLoadElimination12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler18CsaLoadElimination12reducer_nameEv
	.type	_ZNK2v88internal8compiler18CsaLoadElimination12reducer_nameEv, @function
_ZNK2v88internal8compiler18CsaLoadElimination12reducer_nameEv:
.LFB10860:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10860:
	.size	_ZNK2v88internal8compiler18CsaLoadElimination12reducer_nameEv, .-_ZNK2v88internal8compiler18CsaLoadElimination12reducer_nameEv
	.section	.text._ZN2v88internal8compiler18CsaLoadEliminationD2Ev,"axG",@progbits,_ZN2v88internal8compiler18CsaLoadEliminationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler18CsaLoadEliminationD2Ev
	.type	_ZN2v88internal8compiler18CsaLoadEliminationD2Ev, @function
_ZN2v88internal8compiler18CsaLoadEliminationD2Ev:
.LFB14441:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE14441:
	.size	_ZN2v88internal8compiler18CsaLoadEliminationD2Ev, .-_ZN2v88internal8compiler18CsaLoadEliminationD2Ev
	.weak	_ZN2v88internal8compiler18CsaLoadEliminationD1Ev
	.set	_ZN2v88internal8compiler18CsaLoadEliminationD1Ev,_ZN2v88internal8compiler18CsaLoadEliminationD2Ev
	.section	.text._ZN2v88internal8compiler18CsaLoadEliminationD0Ev,"axG",@progbits,_ZN2v88internal8compiler18CsaLoadEliminationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler18CsaLoadEliminationD0Ev
	.type	_ZN2v88internal8compiler18CsaLoadEliminationD0Ev, @function
_ZN2v88internal8compiler18CsaLoadEliminationD0Ev:
.LFB14443:
	.cfi_startproc
	endbr64
	movl	$96, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14443:
	.size	_ZN2v88internal8compiler18CsaLoadEliminationD0Ev, .-_ZN2v88internal8compiler18CsaLoadEliminationD0Ev
	.section	.text._ZN2v88internal8compiler25CsaLoadEliminationHelpers12IsCompatibleENS0_21MachineRepresentationES3_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler25CsaLoadEliminationHelpers12IsCompatibleENS0_21MachineRepresentationES3_
	.type	_ZN2v88internal8compiler25CsaLoadEliminationHelpers12IsCompatibleENS0_21MachineRepresentationES3_, @function
_ZN2v88internal8compiler25CsaLoadEliminationHelpers12IsCompatibleENS0_21MachineRepresentationES3_:
.LFB11937:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpb	%sil, %dil
	je	.L5
	subl	$6, %edi
	xorl	%eax, %eax
	cmpb	$2, %dil
	ja	.L5
	subl	$6, %esi
	cmpb	$2, %sil
	setbe	%al
.L5:
	ret
	.cfi_endproc
.LFE11937:
	.size	_ZN2v88internal8compiler25CsaLoadEliminationHelpers12IsCompatibleENS0_21MachineRepresentationES3_, .-_ZN2v88internal8compiler25CsaLoadEliminationHelpers12IsCompatibleENS0_21MachineRepresentationES3_
	.section	.text._ZN2v88internal8compiler25CsaLoadEliminationHelpers14ObjectMayAliasEPNS1_4NodeES4_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler25CsaLoadEliminationHelpers14ObjectMayAliasEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler25CsaLoadEliminationHelpers14ObjectMayAliasEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler25CsaLoadEliminationHelpers14ObjectMayAliasEPNS1_4NodeES4_:
.LFB11938:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L9
	movq	(%rsi), %rdx
	movzwl	16(%rdx), %ecx
	movq	(%rdi), %rdx
	movzwl	16(%rdx), %edx
	cmpw	$237, %cx
	je	.L11
	cmpw	$237, %dx
	je	.L18
.L9:
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	cmpw	$50, %dx
	je	.L17
	cmpw	$237, %dx
	je	.L17
.L13:
	cmpw	$30, %dx
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%eax, %eax
	ret
.L18:
	cmpw	$50, %cx
	je	.L17
	movl	%ecx, %edx
	jmp	.L13
	.cfi_endproc
.LFE11938:
	.size	_ZN2v88internal8compiler25CsaLoadEliminationHelpers14ObjectMayAliasEPNS1_4NodeES4_, .-_ZN2v88internal8compiler25CsaLoadEliminationHelpers14ObjectMayAliasEPNS1_4NodeES4_
	.section	.rodata._ZN2v88internal8compiler25CsaLoadEliminationHelpers14OffsetMayAliasEPNS1_4NodeENS0_21MachineRepresentationES4_S5_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler25CsaLoadEliminationHelpers14OffsetMayAliasEPNS1_4NodeENS0_21MachineRepresentationES4_S5_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler25CsaLoadEliminationHelpers14OffsetMayAliasEPNS1_4NodeENS0_21MachineRepresentationES4_S5_
	.type	_ZN2v88internal8compiler25CsaLoadEliminationHelpers14OffsetMayAliasEPNS1_4NodeENS0_21MachineRepresentationES4_S5_, @function
_ZN2v88internal8compiler25CsaLoadEliminationHelpers14OffsetMayAliasEPNS1_4NodeENS0_21MachineRepresentationES4_S5_:
.LFB11939:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movl	%ecx, %edi
	movq	(%r8), %rcx
	movzwl	16(%rcx), %eax
	cmpl	$23, %eax
	je	.L33
	xorl	%r10d, %r10d
	xorl	%r8d, %r8d
	cmpl	$24, %eax
	je	.L34
.L21:
	movq	(%rdx), %rcx
	movzwl	16(%rcx), %edx
	cmpl	$23, %edx
	je	.L35
.L22:
	movl	$1, %eax
	cmpl	$24, %edx
	je	.L36
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movq	48(%rcx), %r9
	testb	%r10b, %r10b
	je	.L37
.L28:
	subl	$1, %esi
	cmpb	$13, %sil
	ja	.L27
	leaq	CSWTCH.175(%rip), %r10
	movzbl	%sil, %esi
	movl	$1, %eax
	subl	$1, %edi
	movl	(%r10,%rsi,4), %ecx
	movl	%eax, %edx
	sall	%cl, %edx
	movslq	%edx, %rdx
	addq	%r8, %rdx
	cmpb	$13, %dil
	ja	.L27
	movzbl	%dil, %edi
	movl	(%r10,%rdi,4), %ecx
	sall	%cl, %eax
	cltq
	addq	%r9, %rax
	cmpq	%r8, %rax
	setg	%al
	cmpq	%r9, %rdx
	setg	%dl
	andl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	movq	48(%rcx), %r8
	movq	(%rdx), %rcx
	movl	$1, %r10d
	movzwl	16(%rcx), %edx
	cmpl	$23, %edx
	jne	.L22
.L35:
	movslq	44(%rcx), %r9
	testb	%r10b, %r10b
	jne	.L28
.L37:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movslq	44(%rcx), %r8
	movl	$1, %r10d
	jmp	.L21
.L27:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE11939:
	.size	_ZN2v88internal8compiler25CsaLoadEliminationHelpers14OffsetMayAliasEPNS1_4NodeENS0_21MachineRepresentationES4_S5_, .-_ZN2v88internal8compiler25CsaLoadEliminationHelpers14OffsetMayAliasEPNS1_4NodeENS0_21MachineRepresentationES4_S5_
	.section	.text._ZNK2v88internal8compiler18CsaLoadElimination6commonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18CsaLoadElimination6commonEv
	.type	_ZNK2v88internal8compiler18CsaLoadElimination6commonEv, @function
_ZNK2v88internal8compiler18CsaLoadElimination6commonEv:
.LFB11983:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE11983:
	.size	_ZNK2v88internal8compiler18CsaLoadElimination6commonEv, .-_ZNK2v88internal8compiler18CsaLoadElimination6commonEv
	.section	.text._ZNK2v88internal8compiler18CsaLoadElimination5graphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18CsaLoadElimination5graphEv
	.type	_ZNK2v88internal8compiler18CsaLoadElimination5graphEv, @function
_ZNK2v88internal8compiler18CsaLoadElimination5graphEv:
.LFB11984:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE11984:
	.size	_ZNK2v88internal8compiler18CsaLoadElimination5graphEv, .-_ZNK2v88internal8compiler18CsaLoadElimination5graphEv
	.section	.text._ZNK2v88internal8compiler18CsaLoadElimination7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18CsaLoadElimination7isolateEv
	.type	_ZNK2v88internal8compiler18CsaLoadElimination7isolateEv, @function
_ZNK2v88internal8compiler18CsaLoadElimination7isolateEv:
.LFB11985:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE11985:
	.size	_ZNK2v88internal8compiler18CsaLoadElimination7isolateEv, .-_ZNK2v88internal8compiler18CsaLoadElimination7isolateEv
	.section	.text._ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE,"axG",@progbits,_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	.type	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE, @function
_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE:
.LFB13326:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movl	(%rsi), %ecx
	movsbl	32(%rdi), %edi
	cmpl	%ecx, %edi
	jle	.L48
	movl	$-2147483648, %r9d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L52:
	testq	%r8, %r8
	jne	.L51
	movq	$0, (%rdx,%rdi,8)
	movl	(%rsi), %edi
	leal	1(%rdi), %ecx
	movl	%ecx, (%rsi)
	movsbl	32(%rax), %edi
	cmpl	%ecx, %edi
	jle	.L48
.L43:
	movslq	%ecx, %rdi
	movl	%r9d, %r10d
	movq	48(%rax,%rdi,8), %r8
	shrl	%cl, %r10d
	testl	%r10d, 36(%rax)
	jne	.L52
	movq	%r8, (%rdx,%rdi,8)
	movl	(%rsi), %edi
	leal	1(%rdi), %ecx
	movl	%ecx, (%rsi)
.L53:
	movsbl	32(%rax), %edi
	cmpl	%ecx, %edi
	jg	.L43
.L48:
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%r8, %rcx
	movq	%rax, %r8
	movq	%r8, (%rdx,%rdi,8)
	movl	(%rsi), %edi
	movq	%rcx, %rax
	leal	1(%rdi), %ecx
	movl	%ecx, (%rsi)
	jmp	.L53
	.cfi_endproc
.LFE13326:
	.size	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE, .-_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	.section	.rodata._ZNK2v88internal8compiler18CsaLoadElimination13AbstractState5PrintEv.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"    #%d+#%d:%s -> #%d:%s [repr=%s]\n"
	.section	.text._ZNK2v88internal8compiler18CsaLoadElimination13AbstractState5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState5PrintEv
	.type	_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState5PrintEv, @function
_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState5PrintEv:
.LFB11958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movzbl	16(%rdi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %r12
	movups	%xmm0, -344(%rbp)
	movl	$0, -352(%rbp)
	movq	%r13, -72(%rbp)
	movb	%r14b, -64(%rbp)
	testq	%r12, %r12
	je	.L56
	leaq	-352(%rbp), %r15
	leaq	-328(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	movq	-360(%rbp), %rdx
	movq	%rax, -336(%rbp)
	movq	%rax, %r12
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L57
	movq	32(%rax), %rcx
	movq	%rcx, -344(%rbp)
.L57:
	movl	$-2147483648, %r8d
.L89:
	testq	%rax, %rax
	je	.L58
	movq	-344(%rbp), %rax
	movq	48(%rax), %rcx
	movzbl	56(%rax), %eax
.L59:
	cmpq	%rcx, %r13
	sete	%bl
	cmpb	%r14b, %al
	sete	%al
	andb	%al, %bl
	jne	.L71
.L56:
	movl	$-2147483648, %ebx
.L72:
	testq	%r12, %r12
	je	.L54
	cmpq	$0, 40(%r12)
	je	.L74
	movq	-344(%rbp), %rax
	movq	32(%rax), %r13
	movq	40(%rax), %r15
	movq	48(%rax), %r14
	movzbl	56(%rax), %edi
.L75:
	call	_ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE@PLT
	movq	(%r14), %rdx
	subq	$8, %rsp
	movl	20(%r14), %r8d
	movl	20(%r13), %esi
	leaq	.LC2(%rip), %rdi
	movq	8(%rdx), %r9
	movq	0(%r13), %rdx
	andl	$16777215, %r8d
	andl	$16777215, %esi
	movq	8(%rdx), %rcx
	movl	20(%r15), %edx
	pushq	%rax
	xorl	%eax, %eax
	andl	$16777215, %edx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-336(%rbp), %r12
	popq	%rax
	popq	%rdx
.L86:
	testq	%r12, %r12
	je	.L54
	movq	40(%r12), %r13
	testq	%r13, %r13
	je	.L81
	movq	-344(%rbp), %rdi
	addq	$16, %r13
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, -344(%rbp)
	cmpq	%r13, %rax
	jne	.L72
.L81:
	movl	-352(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L54
	subl	$1, %ecx
	movl	36(%r12), %esi
	movl	%ecx, -352(%rbp)
	movslq	%ecx, %rcx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L123:
	subq	$1, %rcx
.L85:
	movl	%ebx, %eax
	movl	%ecx, %edx
	shrl	%cl, %eax
	testl	%esi, %eax
	jne	.L82
	movq	-328(%rbp,%rcx,8), %rdi
	leaq	-352(%rbp), %r8
	testq	%rdi, %rdi
	jne	.L122
.L82:
	testl	%ecx, %ecx
	jne	.L123
.L54:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L124
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	addl	$1, %edx
	movq	%r8, %rsi
	movl	%edx, -352(%rbp)
	leaq	-328(%rbp), %rdx
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	movq	%rax, -336(%rbp)
	movq	%rax, %r12
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L125
	movq	32(%rax), %rax
	movq	%rax, -344(%rbp)
	movq	48(%rax), %rdx
	movzbl	56(%rax), %eax
.L87:
	cmpq	%rdx, -72(%rbp)
	jne	.L72
	cmpb	%al, -64(%rbp)
	jne	.L72
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L74:
	movq	(%r12), %r13
	movq	8(%r12), %r15
	movq	16(%r12), %r14
	movzbl	24(%r12), %edi
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L125:
	movq	16(%r12), %rdx
	movzbl	24(%r12), %eax
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L58:
	movq	16(%r12), %rcx
	movzbl	24(%r12), %eax
	jmp	.L59
.L127:
	addl	$1, %esi
	movq	%r10, %rdi
	movq	%rdx, -360(%rbp)
	movl	%esi, -352(%rbp)
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	movq	-360(%rbp), %rdx
	movl	$-2147483648, %r8d
	movq	%rax, -336(%rbp)
	movq	%rax, %r12
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L126
	movq	32(%rax), %rcx
	movq	%rcx, -344(%rbp)
	movq	48(%rcx), %rax
	movzbl	56(%rcx), %ecx
.L88:
	cmpq	%rax, -72(%rbp)
	jne	.L64
	cmpb	%cl, -64(%rbp)
	jne	.L64
.L71:
	movq	40(%r12), %rcx
	testq	%rcx, %rcx
	je	.L65
	movq	-344(%rbp), %rdi
	movq	%rdx, -368(%rbp)
	movq	%rcx, -360(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rdx
	movl	$-2147483648, %r8d
	movq	%rax, -344(%rbp)
	addq	$16, %rcx
	cmpq	%rcx, %rax
	je	.L65
.L64:
	movq	40(%r12), %rax
	jmp	.L89
.L65:
	movl	-352(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L62
	subl	$1, %ecx
	movl	36(%r12), %edi
	xorl	%r9d, %r9d
	movl	%ecx, -352(%rbp)
	movslq	%ecx, %rcx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L69:
	subq	$1, %rcx
	movl	%ebx, %r9d
.L70:
	movl	%r8d, %eax
	movl	%ecx, %esi
	shrl	%cl, %eax
	testl	%edi, %eax
	jne	.L66
	movq	24(%r15,%rcx,8), %r10
	testq	%r10, %r10
	jne	.L127
.L66:
	testl	%ecx, %ecx
	jne	.L69
	testb	%r9b, %r9b
	je	.L62
	movl	%esi, -352(%rbp)
.L62:
	pxor	%xmm0, %xmm0
	xorl	%r12d, %r12d
	movups	%xmm0, -344(%rbp)
	jmp	.L56
.L126:
	movq	16(%r12), %rax
	movzbl	24(%r12), %ecx
	jmp	.L88
.L124:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11958:
	.size	_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState5PrintEv, .-_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState5PrintEv
	.section	.text._ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEEeqERKSC_,"axG",@progbits,_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEEeqERKSC_,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEEeqERKSC_
	.type	_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEEeqERKSC_, @function
_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEEeqERKSC_:
.LFB12800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2520, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r8, %r12
	je	.L235
	movq	8(%rdi), %rbx
	movq	8(%rsi), %r13
	cmpq	%rbx, %r13
	je	.L330
.L128:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L331
	addq	$2520, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore_state
	movzbl	16(%rdi), %r14d
	movzbl	16(%rsi), %r15d
	cmpb	%r14b, %r15b
	jne	.L128
	movl	$0, -2176(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -2168(%rbp)
	testq	%r8, %r8
	je	.L332
	leaq	-2176(%rbp), %r9
	leaq	-2152(%rbp), %rdx
	movq	%r8, %rdi
	movq	%r13, -1896(%rbp)
	movq	%r9, %rsi
	movq	%rdx, -2528(%rbp)
	movq	%r9, -2520(%rbp)
	movb	%r15b, -1888(%rbp)
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	movq	-2520(%rbp), %r9
	movq	-2528(%rbp), %rdx
	movq	%rax, -2160(%rbp)
	movq	%rax, %rsi
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L132
	movq	32(%rax), %rcx
	movq	%rcx, -2168(%rbp)
.L132:
	movl	$-2147483648, %r10d
.L233:
	testq	%rax, %rax
	je	.L133
	movq	-2168(%rbp), %rax
	movq	48(%rax), %rcx
	movzbl	56(%rax), %eax
.L134:
	cmpq	%rcx, %r13
	sete	%r8b
	cmpb	%al, %r15b
	sete	%al
	andb	%al, %r8b
	jne	.L146
.L131:
	testq	%r12, %r12
	je	.L333
	leaq	-2480(%rbp), %r13
	leaq	-2456(%rbp), %r15
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r9, -2520(%rbp)
	movl	$0, -2480(%rbp)
	movq	%rbx, -2200(%rbp)
	movb	%r14b, -2192(%rbp)
	movups	%xmm0, -2472(%rbp)
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	movq	-2520(%rbp), %r9
	movq	%rax, -2464(%rbp)
	movq	%rax, %rsi
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L149
	movq	32(%rax), %rcx
	movq	%rcx, -2472(%rbp)
.L149:
	movl	$-2147483648, %r11d
.L232:
	testq	%rax, %rax
	je	.L150
	movq	-2472(%rbp), %rax
	movq	48(%rax), %rcx
	movzbl	56(%rax), %eax
.L151:
	cmpq	%rcx, %rbx
	sete	%r12b
	cmpb	%al, %r14b
	sete	%al
	andb	%al, %r12b
	jne	.L163
.L148:
	movq	%r13, %rsi
	leaq	-1568(%rbp), %rdi
	movl	$37, %ecx
	rep movsq
	movl	$37, %ecx
	movq	%r9, %rsi
	leaq	-1264(%rbp), %r12
	leaq	-1568(%rbp), %r13
	leaq	-656(%rbp), %rdi
	rep movsq
	movl	$37, %ecx
	movq	%r12, %rdi
	movq	%r13, %rsi
	rep movsq
	leaq	-968(%rbp), %r15
	leaq	-656(%rbp), %r14
	movl	$37, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	rep movsq
	movq	-1248(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L334
	movq	-952(%rbp), %rax
	testq	%rax, %rax
	je	.L167
	movl	36(%rax), %edx
	cmpl	%edx, 36(%rbx)
	je	.L335
	jnb	.L166
.L167:
	movl	$1, %edx
	movw	%dx, -672(%rbp)
	.p2align 4,,10
	.p2align 3
.L174:
	movl	$-2147483648, %r9d
.L228:
	testq	%rbx, %rbx
	je	.L336
.L180:
	movzbl	-672(%rbp), %r8d
	testb	%r8b, %r8b
	je	.L181
	cmpq	$0, 40(%rbx)
	je	.L182
	movq	-1256(%rbp), %rax
	movdqu	48(%rax), %xmm1
	movaps	%xmm1, -2496(%rbp)
.L183:
	cmpb	$0, -671(%rbp)
	je	.L184
	movq	-952(%rbp), %rax
	cmpq	$0, 40(%rax)
	je	.L185
	movq	-960(%rbp), %rax
	movq	48(%rax), %rdx
	movzbl	56(%rax), %esi
.L186:
	movq	-2496(%rbp), %rax
	movzbl	-2488(%rbp), %ecx
.L187:
	cmpq	%rdx, %rax
	je	.L337
.L190:
	xorl	%eax, %eax
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L336:
	cmpq	$0, -952(%rbp)
	jne	.L180
	.p2align 4,,10
	.p2align 3
.L235:
	movl	$1, %eax
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L332:
	movq	%rbx, -1896(%rbp)
	leaq	-2176(%rbp), %r9
	movb	%r14b, -1888(%rbp)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L337:
	cmpb	%cl, %sil
	jne	.L190
	testb	%r8b, %r8b
	jne	.L192
.L193:
	cmpb	$0, -671(%rbp)
	jne	.L204
.L205:
	leaq	-1872(%rbp), %rdi
	movl	$37, %ecx
	movq	%r12, %rsi
	rep movsq
	movq	%r13, %rdi
	movl	$37, %ecx
	movq	%r15, %rsi
	rep movsq
	leaq	-1568(%rbp), %r13
	leaq	-1872(%rbp), %rsi
	movq	%r14, %rdi
	movq	-1856(%rbp), %r8
	movl	$37, %ecx
	movq	-1864(%rbp), %rdx
	rep movsq
	movq	-1560(%rbp), %rax
	leaq	-360(%rbp), %rdi
	movq	%r13, %rsi
	movl	$37, %ecx
	rep movsq
	movq	-1552(%rbp), %rcx
	testq	%r8, %r8
	je	.L338
	testq	%rcx, %rcx
	je	.L240
	movl	36(%r8), %r10d
	movl	36(%rcx), %edi
	cmpl	%edi, %r10d
	je	.L339
	sbbl	%esi, %esi
	addl	$1, %esi
	cmpl	%edi, %r10d
	setb	%dil
.L217:
	movq	%rdx, %xmm0
	movq	%r8, %xmm4
	movq	%rcx, %xmm5
	movb	%dil, -64(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movb	%sil, -63(%rbp)
	movl	$74, %ecx
	movq	%r12, %rdi
	movups	%xmm0, -648(%rbp)
	movq	%rax, %xmm0
	movq	%r14, %rsi
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -352(%rbp)
	rep movsq
	movq	-1248(%rbp), %rbx
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	jmp	.L228
.L341:
	addl	$1, %eax
	movq	%r9, %rsi
	movb	%r8b, -2536(%rbp)
	movq	%rdx, -2528(%rbp)
	movq	%r9, -2520(%rbp)
	movl	%eax, -2176(%rbp)
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	movq	-2520(%rbp), %r9
	movq	-2528(%rbp), %rdx
	movl	$-2147483648, %r10d
	movq	%rax, -2160(%rbp)
	movq	%rax, %rsi
	movq	40(%rax), %rax
	movzbl	-2536(%rbp), %r8d
	testq	%rax, %rax
	je	.L340
	movq	32(%rax), %rax
	movq	%rax, -2168(%rbp)
	movq	48(%rax), %rcx
	movzbl	56(%rax), %eax
.L231:
	cmpq	%rcx, -1896(%rbp)
	jne	.L139
	cmpb	%al, -1888(%rbp)
	jne	.L139
.L146:
	movq	40(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L140
	movq	-2168(%rbp), %rdi
	movq	%rdx, -2552(%rbp)
	movq	%r9, -2544(%rbp)
	movb	%r8b, -2536(%rbp)
	movq	%rcx, -2528(%rbp)
	movq	%rsi, -2520(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-2528(%rbp), %rcx
	movq	-2520(%rbp), %rsi
	movl	$-2147483648, %r10d
	movq	%rax, -2168(%rbp)
	movzbl	-2536(%rbp), %r8d
	addq	$16, %rcx
	movq	-2544(%rbp), %r9
	movq	-2552(%rbp), %rdx
	cmpq	%rcx, %rax
	jne	.L139
.L140:
	movl	-2176(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L137
	subl	$1, %ecx
	movl	36(%rsi), %esi
	xorl	%r11d, %r11d
	movl	%ecx, -2176(%rbp)
	movslq	%ecx, %rcx
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L144:
	subq	$1, %rcx
	movl	%r8d, %r11d
.L145:
	movl	%r10d, %edi
	movl	%ecx, %eax
	shrl	%cl, %edi
	testl	%esi, %edi
	jne	.L141
	movq	24(%r9,%rcx,8), %rdi
	testq	%rdi, %rdi
	jne	.L341
.L141:
	testl	%ecx, %ecx
	jne	.L144
	testb	%r11b, %r11b
	je	.L137
	movl	%eax, -2176(%rbp)
.L137:
	pxor	%xmm0, %xmm0
	movups	%xmm0, -2168(%rbp)
	jmp	.L131
.L343:
	addl	$1, %esi
	movq	%r15, %rdx
	movq	%r8, %rdi
	movq	%r9, -2520(%rbp)
	movl	%esi, -2480(%rbp)
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	movq	-2520(%rbp), %r9
	movl	$-2147483648, %r11d
	movq	%rax, -2464(%rbp)
	movq	%rax, %rsi
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L342
	movq	32(%rax), %rax
	movq	%rax, -2472(%rbp)
	movq	48(%rax), %rcx
	movzbl	56(%rax), %eax
.L230:
	cmpq	%rcx, -2200(%rbp)
	jne	.L156
	cmpb	%al, -2192(%rbp)
	jne	.L156
.L163:
	movq	40(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L157
	movq	-2472(%rbp), %rdi
	movq	%r9, -2536(%rbp)
	movq	%rcx, -2528(%rbp)
	movq	%rsi, -2520(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-2528(%rbp), %rcx
	movq	-2520(%rbp), %rsi
	movl	$-2147483648, %r11d
	movq	%rax, -2472(%rbp)
	movq	-2536(%rbp), %r9
	addq	$16, %rcx
	cmpq	%rcx, %rax
	jne	.L156
.L157:
	movl	-2480(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L154
	subl	$1, %ecx
	movl	36(%rsi), %edi
	xorl	%r10d, %r10d
	movl	%ecx, -2480(%rbp)
	movslq	%ecx, %rcx
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L161:
	subq	$1, %rcx
	movl	%r12d, %r10d
.L162:
	movl	%r11d, %eax
	movl	%ecx, %esi
	shrl	%cl, %eax
	testl	%edi, %eax
	jne	.L158
	movq	24(%r13,%rcx,8), %r8
	testq	%r8, %r8
	jne	.L343
.L158:
	testl	%ecx, %ecx
	jne	.L161
	testb	%r10b, %r10b
	je	.L154
	movl	%esi, -2480(%rbp)
.L154:
	pxor	%xmm0, %xmm0
	movups	%xmm0, -2472(%rbp)
	jmp	.L148
.L344:
	addl	$1, %esi
	leaq	-944(%rbp), %rdx
	movl	%esi, -968(%rbp)
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	movl	$-2147483648, %r9d
	movq	%rax, -952(%rbp)
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L213
	movq	32(%rdx), %rdx
	movq	%rdx, -960(%rbp)
	cmpq	$0, 40(%rax)
	je	.L213
	movq	48(%rdx), %rcx
	movzbl	56(%rdx), %eax
.L215:
	cmpq	%rcx, -688(%rbp)
	jne	.L205
	cmpb	%al, -680(%rbp)
	jne	.L205
.L204:
	movq	-952(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L205
	cmpq	$0, 40(%rbx)
	je	.L210
	movq	-960(%rbp), %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movl	$-2147483648, %r9d
	movq	%rax, -960(%rbp)
	movq	40(%rbx), %rdx
	addq	$16, %rdx
	cmpq	%rdx, %rax
	jne	.L205
.L210:
	movl	-968(%rbp), %eax
	testl	%eax, %eax
	je	.L208
	leal	-1(%rax), %esi
	movslq	%eax, %rdx
	movl	%esi, -968(%rbp)
	movslq	%esi, %rcx
	subq	$2, %rdx
	movl	%esi, %esi
	subq	%rsi, %rdx
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L345:
	movl	%ecx, -968(%rbp)
.L214:
	movl	%r9d, %eax
	movl	%ecx, %esi
	shrl	%cl, %eax
	testl	%eax, 36(%rbx)
	jne	.L211
	movq	320(%r12,%rcx,8), %rdi
	testq	%rdi, %rdi
	jne	.L344
.L211:
	subq	$1, %rcx
	cmpq	%rdx, %rcx
	jne	.L345
.L208:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -960(%rbp)
	jmp	.L205
.L347:
	addl	$1, %edx
	movq	%r12, %rsi
	movb	%r8b, -2520(%rbp)
	movl	%edx, -1264(%rbp)
	leaq	-1240(%rbp), %rdx
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	movzbl	-2520(%rbp), %r8d
	movl	$-2147483648, %r9d
	movq	%rax, -1248(%rbp)
	movq	%rax, %rbx
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L346
	movq	32(%rax), %rax
	movq	%rax, -1256(%rbp)
	movq	48(%rax), %rdx
	movzbl	56(%rax), %eax
.L229:
	cmpq	%rdx, -984(%rbp)
	jne	.L193
	cmpb	%al, -976(%rbp)
	jne	.L193
.L192:
	testq	%rbx, %rbx
	je	.L193
	movq	40(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L198
	movq	-1256(%rbp), %rdi
	movq	%rdx, -2528(%rbp)
	movb	%r8b, -2520(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-2528(%rbp), %rdx
	movzbl	-2520(%rbp), %r8d
	movl	$-2147483648, %r9d
	movq	%rax, -1256(%rbp)
	addq	$16, %rdx
	cmpq	%rdx, %rax
	jne	.L193
.L198:
	movl	-1264(%rbp), %eax
	testl	%eax, %eax
	je	.L196
	subl	$1, %eax
	movl	36(%rbx), %esi
	xorl	%r10d, %r10d
	movl	%eax, -1264(%rbp)
	movslq	%eax, %rcx
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L202:
	subq	$1, %rcx
	movl	%r8d, %r10d
.L203:
	movl	%r9d, %eax
	movl	%ecx, %edx
	shrl	%cl, %eax
	testl	%esi, %eax
	jne	.L199
	movq	24(%r12,%rcx,8), %rdi
	testq	%rdi, %rdi
	jne	.L347
.L199:
	testl	%ecx, %ecx
	jne	.L202
	testb	%r10b, %r10b
	je	.L196
	movl	%edx, -1264(%rbp)
.L196:
	pxor	%xmm0, %xmm0
	movups	%xmm0, -1256(%rbp)
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L181:
	movq	-952(%rbp), %rax
	cmpq	$0, 40(%rax)
	je	.L188
	movq	-960(%rbp), %rax
	movq	48(%rax), %rdx
	movzbl	56(%rax), %esi
.L189:
	movq	-984(%rbp), %rax
	movzbl	-976(%rbp), %ecx
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L333:
	pxor	%xmm0, %xmm0
	movq	%rbx, -2200(%rbp)
	leaq	-2480(%rbp), %r13
	movl	$0, -2480(%rbp)
	movb	%r14b, -2192(%rbp)
	movups	%xmm0, -2472(%rbp)
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L334:
	cmpq	$0, -952(%rbp)
	jne	.L166
.L165:
	movl	$257, %ecx
	movw	%cx, -672(%rbp)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L335:
	cmpq	$0, 40(%rax)
	je	.L169
	movq	-960(%rbp), %rdx
	movq	32(%rdx), %rsi
	movq	40(%rdx), %rdi
.L170:
	cmpq	$0, 40(%rbx)
	je	.L171
	movq	-1256(%rbp), %rdx
	movq	32(%rdx), %rcx
	movq	40(%rdx), %rdx
.L172:
	cmpq	%rcx, %rsi
	jne	.L173
	cmpq	%rdi, %rdx
	je	.L165
.L173:
	cmpq	$0, 40(%rax)
	je	.L175
	movq	-960(%rbp), %rax
	movq	32(%rax), %rdx
	movq	40(%rax), %rsi
.L176:
	cmpq	$0, 40(%rbx)
	je	.L177
	movq	-1256(%rbp), %rcx
	movq	32(%rcx), %rax
	movq	40(%rcx), %rcx
.L178:
	cmpq	%rax, %rdx
	ja	.L167
	cmpq	%rcx, %rsi
	jbe	.L166
	cmpq	%rax, %rdx
	je	.L167
.L166:
	movl	$256, %eax
	movw	%ax, -672(%rbp)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L184:
	movq	-688(%rbp), %rdx
	movzbl	-680(%rbp), %esi
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L133:
	movq	16(%rsi), %rcx
	movzbl	24(%rsi), %eax
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L150:
	movq	16(%rsi), %rcx
	movzbl	24(%rsi), %eax
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L188:
	movq	16(%rax), %rdx
	movzbl	24(%rax), %esi
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L182:
	movdqu	(%rbx), %xmm2
	movdqu	16(%rbx), %xmm3
	movaps	%xmm2, -2512(%rbp)
	movaps	%xmm3, -2496(%rbp)
	jmp	.L183
.L171:
	movq	(%rbx), %rcx
	movq	8(%rbx), %rdx
	jmp	.L172
.L169:
	movq	(%rax), %rsi
	movq	8(%rax), %rdi
	jmp	.L170
.L185:
	movq	16(%rax), %rdx
	movzbl	24(%rax), %esi
	jmp	.L186
.L139:
	movq	40(%rsi), %rax
	jmp	.L233
.L156:
	movq	40(%rsi), %rax
	jmp	.L232
.L338:
	testq	%rcx, %rcx
	movl	$1, %esi
	sete	%dil
	jmp	.L217
.L339:
	cmpq	$0, 40(%rcx)
	je	.L219
	movq	32(%rax), %rdi
	movq	40(%rax), %r10
.L220:
	cmpq	$0, 40(%r8)
	je	.L221
	movq	32(%rdx), %rsi
	movq	40(%rdx), %r11
.L222:
	cmpq	%rsi, %rdi
	jne	.L243
	cmpq	%r10, %r11
	je	.L239
.L243:
	cmpq	$0, 40(%rcx)
	je	.L224
	movq	32(%rax), %r11
	movq	40(%rax), %rdi
.L225:
	cmpq	$0, 40(%r8)
	je	.L226
	movq	32(%rdx), %r10
	movq	40(%rdx), %rsi
.L227:
	cmpq	%r10, %r11
	ja	.L240
	jne	.L241
	cmpq	%rsi, %rdi
	ja	.L240
.L241:
	movl	$1, %esi
	xorl	%edi, %edi
	jmp	.L217
.L177:
	movq	(%rbx), %rax
	movq	8(%rbx), %rcx
	jmp	.L178
.L175:
	movq	(%rax), %rdx
	movq	8(%rax), %rsi
	jmp	.L176
.L240:
	xorl	%esi, %esi
	movl	$1, %edi
	jmp	.L217
.L221:
	movq	(%r8), %rsi
	movq	8(%r8), %r11
	jmp	.L222
.L219:
	movq	(%rcx), %rdi
	movq	8(%rcx), %r10
	jmp	.L220
.L340:
	movq	16(%rsi), %rcx
	movzbl	24(%rsi), %eax
	jmp	.L231
.L342:
	movq	16(%rsi), %rcx
	movzbl	24(%rsi), %eax
	jmp	.L230
.L331:
	call	__stack_chk_fail@PLT
.L213:
	movq	16(%rax), %rcx
	movzbl	24(%rax), %eax
	jmp	.L215
.L346:
	movq	16(%rbx), %rdx
	movzbl	24(%rbx), %eax
	jmp	.L229
.L226:
	movq	(%r8), %r10
	movq	8(%r8), %rsi
	jmp	.L227
.L239:
	movl	$1, %esi
	movl	$1, %edi
	jmp	.L217
.L224:
	movq	(%rcx), %r11
	movq	8(%rcx), %rdi
	jmp	.L225
	.cfi_endproc
.LFE12800:
	.size	_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEEeqERKSC_, .-_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEEeqERKSC_
	.section	.text._ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE15GetFocusedValueEPKNSC_11FocusedTreeERKS6_,"axG",@progbits,_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE15GetFocusedValueEPKNSC_11FocusedTreeERKS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE15GetFocusedValueEPKNSC_11FocusedTreeERKS6_
	.type	_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE15GetFocusedValueEPKNSC_11FocusedTreeERKS6_, @function
_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE15GetFocusedValueEPKNSC_11FocusedTreeERKS6_:
.LFB13338:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L352
	movq	40(%rsi), %rax
	testq	%rax, %rax
	je	.L351
	movq	24(%rax), %rcx
	leaq	16(%rax), %r8
	testq	%rcx, %rcx
	je	.L352
	movq	(%rdx), %rsi
	movq	%r8, %rax
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L362:
	jne	.L356
	movq	8(%rdx), %r9
	cmpq	%r9, 40(%rcx)
	jb	.L355
.L356:
	movq	%rcx, %rax
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L354
.L353:
	cmpq	%rsi, 32(%rcx)
	jnb	.L362
.L355:
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L353
.L354:
	cmpq	%rax, %r8
	je	.L352
	cmpq	32(%rax), %rsi
	jb	.L352
	jne	.L358
	movq	40(%rax), %rcx
	cmpq	%rcx, 8(%rdx)
	jnb	.L358
.L352:
	leaq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	movq	(%rsi), %rax
	cmpq	%rax, (%rdx)
	jne	.L352
	movq	8(%rsi), %rax
	cmpq	%rax, 8(%rdx)
	jne	.L352
	leaq	16(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	addq	$48, %rax
	ret
	.cfi_endproc
.LFE13338:
	.size	_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE15GetFocusedValueEPKNSC_11FocusedTreeERKS6_, .-_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE15GetFocusedValueEPKNSC_11FocusedTreeERKS6_
	.section	.text._ZNK2v88internal8compiler18CsaLoadElimination13AbstractState6LookupEPNS1_4NodeES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState6LookupEPNS1_4NodeES5_
	.type	_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState6LookupEPNS1_4NodeES5_, @function
_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState6LookupEPNS1_4NodeES5_:
.LFB11957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	leaq	32(%rsi), %rdx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L365
	movq	32(%rsi), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
.L365:
	testl	%eax, %eax
	jle	.L366
	cmpq	$0, (%rdx)
	je	.L383
.L366:
	movq	%r13, %xmm1
	movq	%rdi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v84base10hash_valueEm@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v84base10hash_valueEm@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L368
	xorl	%ecx, %ecx
	movl	$-2147483648, %edi
	.p2align 4,,10
	.p2align 3
.L372:
	movl	36(%rsi), %edx
	cmpl	%edx, %eax
	je	.L368
	movl	%edi, %r8d
	xorl	%eax, %edx
	shrl	%cl, %r8d
	testl	%edx, %r8d
	jne	.L370
.L371:
	addl	$1, %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	testl	%edx, %r8d
	je	.L371
.L370:
	movsbl	32(%rsi), %edx
	cmpl	%edx, %ecx
	jge	.L374
	movslq	%ecx, %rdx
	addl	$1, %ecx
	movq	48(%rsi,%rdx,8), %rsi
	testq	%rsi, %rsi
	jne	.L372
	.p2align 4,,10
	.p2align 3
.L368:
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE15GetFocusedValueEPKNSC_11FocusedTreeERKS6_
	movq	(%rax), %r8
	movq	8(%rax), %rdx
.L367:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L384
	addq	$40, %rsp
	movq	%r8, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	xorl	%esi, %esi
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L383:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	jmp	.L367
.L384:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11957:
	.size	_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState6LookupEPNS1_4NodeES5_, .-_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState6LookupEPNS1_4NodeES5_
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB13389:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L393
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L387:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L387
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE13389:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_:
.LFB13391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L410
	movq	(%rsi), %rsi
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L417:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L399
.L418:
	movq	%rax, %r12
.L398:
	movq	32(%r12), %rcx
	cmpq	%rcx, %rsi
	jb	.L417
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L418
.L399:
	testb	%dl, %dl
	jne	.L397
	cmpq	%rcx, %rsi
	jbe	.L403
.L408:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L419
.L404:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$39, %rax
	jbe	.L420
	leaq	40(%r13), %rax
	movq	%rax, 16(%rdi)
.L406:
	movq	(%r14), %rax
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	movq	%r15, %r12
.L397:
	cmpq	%r12, 32(%rbx)
	je	.L408
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r14), %rcx
	cmpq	%rcx, 32(%rax)
	jb	.L408
	movq	%rax, %r12
.L403:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movq	32(%r12), %rax
	cmpq	%rax, (%r14)
	setb	%r8b
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L420:
	movl	$40, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L406
	.cfi_endproc
.LFE13391:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.section	.rodata._ZNSt6vectorIPKN2v88internal8compiler18CsaLoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_.str1.1,"aMS",@progbits,1
.LC3:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIPKN2v88internal8compiler18CsaLoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_,"axG",@progbits,_ZNSt6vectorIPKN2v88internal8compiler18CsaLoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal8compiler18CsaLoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	.type	_ZNSt6vectorIPKN2v88internal8compiler18CsaLoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_, @function
_ZNSt6vectorIPKN2v88internal8compiler18CsaLoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_:
.LFB13754:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L531
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	24(%r12), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jb	.L424
	movq	%rdi, %r8
	movq	(%rcx), %xmm0
	subq	%rsi, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L425
	leaq	0(,%rdx,8), %r14
	movq	%rdi, %rdx
	subq	%r14, %rdx
	cmpq	%rdx, %rdi
	je	.L472
	leaq	-8(%rdi), %rax
	movl	$16, %ecx
	subq	%rdx, %rax
	subq	%r14, %rcx
	shrq	$3, %rax
	testq	%rcx, %rcx
	leaq	16(%rdi), %rcx
	setle	%sil
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L473
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L473
	leaq	1(%rax), %r8
	xorl	%eax, %eax
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L428:
	movdqu	(%rdx,%rax), %xmm2
	movups	%xmm2, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L428
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	leaq	(%rdx,%rcx), %rsi
	addq	%rdi, %rcx
	cmpq	%rax, %r8
	je	.L430
	movq	(%rsi), %rax
	movq	%rax, (%rcx)
.L430:
	movq	16(%r12), %rax
.L426:
	addq	%r14, %rax
	movq	%rax, 16(%r12)
	cmpq	%rdx, %rbx
	je	.L431
	subq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%xmm0, -40(%rbp)
	subq	%rdx, %rdi
	call	memmove@PLT
	movq	-40(%rbp), %xmm0
.L431:
	leaq	(%rbx,%r14), %rdx
	cmpq	%rdx, %rbx
	je	.L421
	subq	%rbx, %rdx
	movq	%rbx, %rax
	subq	$8, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rdx, %rdx
	je	.L471
	movq	%rcx, %rdx
	movdqa	%xmm0, %xmm1
	shrq	%rdx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rdx
	addq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L435:
	movups	%xmm1, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L435
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	(%rbx,%rax,8), %r13
	cmpq	%rax, %rcx
	je	.L421
.L471:
	movq	%xmm0, 0(%r13)
.L421:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L531:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	subq	%rax, %rdx
	je	.L474
	cmpq	$1, %rdx
	je	.L475
	movq	%rdx, %rsi
	movdqa	%xmm0, %xmm1
	xorl	%eax, %eax
	shrq	%rsi
	punpcklqdq	%xmm1, %xmm1
	.p2align 4,,10
	.p2align 3
.L439:
	movq	%rax, %rcx
	addq	$1, %rax
	salq	$4, %rcx
	movups	%xmm1, (%rdi,%rcx)
	cmpq	%rax, %rsi
	jne	.L439
	movq	%rdx, %rcx
	andq	$-2, %rcx
	leaq	(%rdi,%rcx,8), %rax
	cmpq	%rcx, %rdx
	je	.L440
.L438:
	movq	%xmm0, (%rax)
.L440:
	leaq	(%rdi,%rdx,8), %rsi
.L437:
	movq	%rsi, 16(%r12)
	cmpq	%rbx, %rdi
	je	.L441
	subq	%rbx, %rdi
	leaq	15(%rbx), %rcx
	movq	%rbx, %rdx
	leaq	-8(%rdi), %r9
	subq	%rsi, %rcx
	movq	%r9, %rax
	shrq	$3, %rax
	cmpq	$30, %rcx
	jbe	.L476
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L476
	leaq	1(%rax), %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L443:
	movdqu	(%rbx,%rax), %xmm3
	movups	%xmm3, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L443
	movq	%rcx, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rax
	leaq	(%rbx,%rax), %rdi
	addq	%rax, %rsi
	cmpq	%r9, %rcx
	je	.L444
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
.L444:
	addq	%r8, 16(%r12)
	movq	%rcx, %rax
.L470:
	movq	%rax, %rcx
	movdqa	%xmm0, %xmm1
	shrq	%rcx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rcx
	addq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L448:
	movups	%xmm1, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L448
	movq	%rax, %rdx
	andq	$-2, %rdx
	leaq	(%rbx,%rdx,8), %r13
	cmpq	%rax, %rdx
	jne	.L471
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L424:
	movq	8(%r12), %rsi
	movl	$268435455, %r8d
	movq	%r8, %rax
	subq	%rsi, %rdi
	sarq	$3, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L534
	cmpq	%rdi, %rdx
	movq	%rdi, %rax
	cmovnb	%rdx, %rax
	addq	%rax, %rdi
	setc	%al
	subq	%rsi, %r13
	movzbl	%al, %eax
	testq	%rax, %rax
	jne	.L477
	testq	%rdi, %rdi
	jne	.L535
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L453:
	leaq	0(,%rdx,8), %r9
	leaq	(%rax,%r13), %r11
	leaq	0(%r13,%r9), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	leaq	8(%rcx), %rsi
	setnb	%dil
	cmpq	%rsi, %r11
	setnb	%sil
	orb	%sil, %dil
	je	.L479
	leaq	-1(%rdx), %rsi
	cmpq	$3, %rsi
	jbe	.L479
	movq	(%rcx), %xmm0
	leaq	-2(%rdx), %rdi
	xorl	%esi, %esi
	shrq	%rdi
	addq	$1, %rdi
	punpcklqdq	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L457:
	movq	%rsi, %r10
	addq	$1, %rsi
	salq	$4, %r10
	movups	%xmm0, (%r11,%r10)
	cmpq	%rsi, %rdi
	ja	.L457
	leaq	(%rdi,%rdi), %rsi
	salq	$4, %rdi
	addq	%r11, %rdi
	cmpq	%rsi, %rdx
	je	.L459
	movq	(%rcx), %rdx
	movq	%rdx, (%rdi)
.L459:
	movq	8(%r12), %rcx
	cmpq	%rcx, %rbx
	je	.L480
	leaq	-8(%rbx), %r10
	leaq	15(%rcx), %rdx
	subq	%rcx, %r10
	subq	%rax, %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L481
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L481
	leaq	1(%rsi), %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L462:
	movdqu	(%rcx,%rdx), %xmm4
	movups	%xmm4, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L462
	movq	%r11, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %rcx
	addq	%rax, %rsi
	cmpq	%rdx, %r11
	je	.L464
	movq	(%rcx), %rdx
	movq	%rdx, (%rsi)
.L464:
	leaq	8(%rax,%r10), %rdi
.L460:
	movq	16(%r12), %rdx
	leaq	(%rdi,%r9), %rcx
	cmpq	%rdx, %rbx
	je	.L465
	subq	%rbx, %rdx
	leaq	-8(%rdx), %r10
	leaq	16(%rdi,%r9), %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %rbx
	leaq	16(%rbx), %rdx
	setnb	%dil
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %dil
	je	.L482
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L482
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L467:
	movdqu	(%rbx,%rdx), %xmm5
	movups	%xmm5, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L467
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rdi
	addq	%rdi, %rbx
	addq	%rcx, %rdi
	cmpq	%rdx, %rsi
	je	.L469
	movq	(%rbx), %rdx
	movq	%rdx, (%rdi)
.L469:
	leaq	8(%rcx,%r10), %rcx
.L465:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%r8, 24(%r12)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	.cfi_restore_state
	movl	$2147483640, %esi
	movl	$2147483640, %r14d
.L452:
	movq	(%r12), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L536
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L455:
	leaq	(%rax,%r14), %r8
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L479:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L456:
	movq	(%rcx), %rdi
	movq	%rdi, (%r11,%rsi,8)
	addq	$1, %rsi
	cmpq	%rsi, %rdx
	jne	.L456
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L473:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L427:
	movq	(%rdx,%rcx,8), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rax, %rsi
	jne	.L427
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L476:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L442:
	movq	(%rbx,%rcx,8), %rdi
	movq	%rdi, (%rsi,%rcx,8)
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	%rax, %rdi
	jne	.L442
	addq	%r8, 16(%r12)
	addq	$1, %rax
	testq	%r9, %r9
	je	.L471
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L481:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L461:
	movq	(%rcx,%rdx,8), %rdi
	movq	%rdi, (%rax,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L461
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L482:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L466:
	movq	(%rbx,%rdx,8), %rdi
	movq	%rdi, (%rcx,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L466
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L474:
	movq	%rdi, %rsi
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L472:
	movq	%rdi, %rax
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L441:
	addq	%r8, %rsi
	movq	%rsi, 16(%r12)
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L480:
	movq	%rax, %rdi
	jmp	.L460
.L475:
	movq	%rdi, %rax
	jmp	.L438
.L536:
	movq	%r8, %rdi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	jmp	.L455
.L535:
	cmpq	$268435455, %rdi
	cmova	%r8, %rdi
	leaq	0(,%rdi,8), %r14
	movq	%r14, %rsi
	jmp	.L452
.L534:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13754:
	.size	_ZNSt6vectorIPKN2v88internal8compiler18CsaLoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_, .-_ZNSt6vectorIPKN2v88internal8compiler18CsaLoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	.section	.text._ZN2v88internal8compiler18CsaLoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18CsaLoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	.type	_ZN2v88internal8compiler18CsaLoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE, @function
_ZN2v88internal8compiler18CsaLoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE:
.LFB11965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	20(%rsi), %r12d
	movq	64(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	56(%rdi), %rax
	movq	%rsi, %rcx
	andl	$16777215, %r12d
	subq	%rax, %rcx
	sarq	$3, %rcx
	cmpq	%rcx, %r12
	jnb	.L551
	leaq	(%rax,%r12,8), %rax
	movq	(%rax), %rsi
	cmpq	%rdx, %rsi
	je	.L540
	testq	%rsi, %rsi
	je	.L541
	movq	%rdx, %rdi
	call	_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEEeqERKSC_
	testb	%al, %al
	jne	.L540
	movq	64(%r13), %rsi
	movq	56(%r13), %rax
	movl	20(%rbx), %r12d
	movq	%rsi, %rcx
	subq	%rax, %rcx
	andl	$16777215, %r12d
	sarq	$3, %rcx
	leaq	0(,%r12,8), %rdx
	cmpq	%rcx, %r12
	jnb	.L539
	addq	%rdx, %rax
	movq	(%rax), %rsi
.L541:
	cmpq	%rsi, %r14
	je	.L544
	movq	%r14, (%rax)
.L544:
	movq	%rbx, %rax
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L540:
	xorl	%eax, %eax
.L545:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L552
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L551:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L540
.L539:
	leaq	1(%r12), %rdx
	movq	$0, -48(%rbp)
	cmpq	%rcx, %rdx
	ja	.L553
	jnb	.L543
	leaq	(%rax,%rdx,8), %rdx
	cmpq	%rsi, %rdx
	je	.L543
	movq	%rdx, 64(%r13)
.L543:
	leaq	(%rax,%r12,8), %rax
	movq	(%rax), %rsi
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L553:
	leaq	-48(%rbp), %r8
	subq	%rcx, %rdx
	leaq	48(%r13), %rdi
	movq	%r8, %rcx
	call	_ZNSt6vectorIPKN2v88internal8compiler18CsaLoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	56(%r13), %rax
	jmp	.L543
.L552:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11965:
	.size	_ZN2v88internal8compiler18CsaLoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE, .-_ZN2v88internal8compiler18CsaLoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	.section	.text._ZN2v88internal8compiler18CsaLoadElimination15ReduceOtherNodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18CsaLoadElimination15ReduceOtherNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18CsaLoadElimination15ReduceOtherNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler18CsaLoadElimination15ReduceOtherNodeEPNS1_4NodeE:
.LFB11964:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	cmpl	$1, 24(%rax)
	je	.L567
.L555:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	cmpb	$1, 36(%rax)
	jne	.L555
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	56(%r13), %rsi
	xorl	%edx, %edx
	movl	20(%rax), %ecx
	movq	64(%r13), %rax
	subq	%rsi, %rax
	andl	$16777215, %ecx
	sarq	$3, %rax
	cmpq	%rax, %rcx
	jnb	.L556
	movq	(%rsi,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.L556
	movq	(%r12), %rcx
	leaq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	testb	$16, 18(%rcx)
	cmove	%rax, %rdx
	call	_ZN2v88internal8compiler18CsaLoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	movq	%rax, %rdx
.L556:
	popq	%r12
	movq	%rdx, %rax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11964:
	.size	_ZN2v88internal8compiler18CsaLoadElimination15ReduceOtherNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler18CsaLoadElimination15ReduceOtherNodeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18CsaLoadElimination10ReduceCallEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18CsaLoadElimination10ReduceCallEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18CsaLoadElimination10ReduceCallEPNS1_4NodeE, @function
_ZN2v88internal8compiler18CsaLoadElimination10ReduceCallEPNS1_4NodeE:
.LFB11963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	subq	$40, %rsp
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpw	$27, 16(%rax)
	jne	.L569
	movq	48(%rax), %rax
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal17ExternalReference17check_object_typeEv@PLT
	movq	-56(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internaleqENS0_17ExternalReferenceES1_@PLT
	testb	%al, %al
	je	.L571
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	56(%r13), %rsi
	xorl	%edx, %edx
	movl	20(%rax), %ecx
	movq	64(%r13), %rax
	subq	%rsi, %rax
	andl	$16777215, %ecx
	sarq	$3, %rax
	cmpq	%rax, %rcx
	jnb	.L572
	movq	(%rsi,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.L572
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CsaLoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	movq	%rax, %rdx
.L572:
	addq	$40, %rsp
	movq	%rdx, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	.cfi_restore_state
	call	_ZN2v88internal17ExternalReference17check_object_typeEv@PLT
.L571:
	movq	(%r12), %rax
	xorl	%r14d, %r14d
	cmpl	$1, 24(%rax)
	je	.L589
.L574:
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L589:
	.cfi_restore_state
	cmpb	$1, 36(%rax)
	jne	.L574
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	56(%r13), %rsi
	movl	20(%rax), %ecx
	movq	64(%r13), %rax
	subq	%rsi, %rax
	andl	$16777215, %ecx
	sarq	$3, %rax
	cmpq	%rax, %rcx
	jnb	.L574
	movq	(%rsi,%rcx,8), %r14
	testq	%r14, %r14
	je	.L574
	movq	(%r12), %rcx
	leaq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	testb	$16, 18(%rcx)
	cmove	%rax, %r14
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler18CsaLoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	movq	%rax, %r14
	jmp	.L574
	.cfi_endproc
.LFE11963:
	.size	_ZN2v88internal8compiler18CsaLoadElimination10ReduceCallEPNS1_4NodeE, .-_ZN2v88internal8compiler18CsaLoadElimination10ReduceCallEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18CsaLoadElimination11ReduceStartEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18CsaLoadElimination11ReduceStartEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18CsaLoadElimination11ReduceStartEPNS1_4NodeE, @function
_ZN2v88internal8compiler18CsaLoadElimination11ReduceStartEPNS1_4NodeE:
.LFB11962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	16(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	20(%rsi), %edx
	movq	64(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	56(%rdi), %rcx
	movq	%rsi, %rax
	andl	$16777215, %edx
	subq	%rcx, %rax
	leaq	0(,%rdx,8), %r14
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L591
	addq	%r14, %rcx
	movq	(%rcx), %rsi
	cmpq	%rsi, %r13
	je	.L592
	testq	%rsi, %rsi
	je	.L593
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEEeqERKSC_
	testb	%al, %al
	jne	.L592
	movq	64(%rbx), %rsi
	movq	56(%rbx), %rcx
	movl	20(%r12), %edx
	movq	%rsi, %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	leaq	0(,%rdx,8), %r14
	cmpq	%rax, %rdx
	jnb	.L591
.L596:
	addq	%r14, %rcx
	movq	(%rcx), %rax
	cmpq	%rax, %r13
	je	.L598
.L593:
	movq	%r13, (%rcx)
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L592:
	xorl	%r12d, %r12d
.L598:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L604
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L591:
	.cfi_restore_state
	addq	$1, %rdx
	movq	$0, -48(%rbp)
	cmpq	%rax, %rdx
	ja	.L605
	jnb	.L596
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rsi, %rax
	je	.L596
	movq	%rax, 64(%rbx)
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L605:
	leaq	-48(%rbp), %rcx
	subq	%rax, %rdx
	leaq	48(%rbx), %rdi
	call	_ZNSt6vectorIPKN2v88internal8compiler18CsaLoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	56(%rbx), %rcx
	jmp	.L596
.L604:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11962:
	.size	_ZN2v88internal8compiler18CsaLoadElimination11ReduceStartEPNS1_4NodeE, .-_ZN2v88internal8compiler18CsaLoadElimination11ReduceStartEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18CsaLoadElimination19PropagateInputStateEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18CsaLoadElimination19PropagateInputStateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18CsaLoadElimination19PropagateInputStateEPNS1_4NodeE, @function
_ZN2v88internal8compiler18CsaLoadElimination19PropagateInputStateEPNS1_4NodeE:
.LFB11966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	64(%r12), %rsi
	movq	56(%r12), %rcx
	movl	20(%rax), %edx
	movq	%rsi, %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L607
	movq	(%rcx,%rdx,8), %r13
	testq	%r13, %r13
	je	.L607
	movl	20(%rbx), %edx
	movq	%rax, %rdi
	andl	$16777215, %edx
	leaq	0(,%rdx,8), %r14
	cmpq	%rdx, %rax
	jbe	.L610
	addq	%r14, %rcx
	movq	(%rcx), %rsi
	cmpq	%rsi, %r13
	je	.L611
	testq	%rsi, %rsi
	je	.L612
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEEeqERKSC_
	testb	%al, %al
	jne	.L611
	movq	64(%r12), %rsi
	movq	56(%r12), %rcx
	movl	20(%rbx), %edx
	movq	%rsi, %rdi
	subq	%rcx, %rdi
	andl	$16777215, %edx
	sarq	$3, %rdi
	leaq	0(,%rdx,8), %r14
	cmpq	%rdi, %rdx
	jnb	.L610
.L615:
	addq	%r14, %rcx
	movq	(%rcx), %rax
	cmpq	%rax, %r13
	je	.L617
.L612:
	movq	%r13, (%rcx)
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L607:
	xorl	%eax, %eax
.L609:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L623
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L611:
	.cfi_restore_state
	xorl	%ebx, %ebx
.L617:
	movq	%rbx, %rax
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L610:
	addq	$1, %rdx
	movq	$0, -48(%rbp)
	cmpq	%rdi, %rdx
	ja	.L624
	jnb	.L615
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L615
	movq	%rax, 64(%r12)
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L624:
	leaq	-48(%rbp), %rcx
	subq	%rdi, %rdx
	leaq	48(%r12), %rdi
	call	_ZNSt6vectorIPKN2v88internal8compiler18CsaLoadElimination13AbstractStateENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	56(%r12), %rcx
	jmp	.L615
.L623:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11966:
	.size	_ZN2v88internal8compiler18CsaLoadElimination19PropagateInputStateEPNS1_4NodeE, .-_ZN2v88internal8compiler18CsaLoadElimination19PropagateInputStateEPNS1_4NodeE
	.section	.text._ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E,"axG",@progbits,_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.type	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E, @function
_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E:
.LFB13968:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L633
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L627:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L627
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L633:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE13968:
	.size	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E, .-_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.section	.text._ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_:
.LFB13974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rcx), %r15
	testq	%r15, %r15
	je	.L637
	movq	8(%r15), %rax
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.L638
	cmpq	24(%rax), %r15
	je	.L699
	movq	$0, 16(%rax)
.L643:
	movdqu	32(%rbx), %xmm4
	movups	%xmm4, 32(%r15)
	movdqu	48(%rbx), %xmm5
	movups	%xmm5, 48(%r15)
.L661:
	movl	(%rbx), %eax
	movq	$0, 16(%r15)
	movq	$0, 24(%r15)
	movl	%eax, (%r15)
	movq	%rdx, 8(%r15)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L645
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r15)
.L645:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L636
	movq	8(%r14), %rbx
	movq	%r15, %rdx
	testq	%rbx, %rbx
	je	.L647
.L701:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.L648
	cmpq	24(%rax), %rbx
	je	.L700
	movq	$0, 16(%rax)
.L653:
	movdqu	32(%r12), %xmm0
	movups	%xmm0, 32(%rbx)
	movdqu	48(%r12), %xmm1
	movups	%xmm1, 48(%rbx)
.L658:
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movl	%eax, (%rbx)
	movq	%rbx, 16(%rdx)
	movq	%rdx, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L655
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L636
.L656:
	movq	%rbx, %rdx
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	jne	.L701
.L647:
	movq	16(%r14), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$63, %rax
	jbe	.L702
	leaq	64(%rbx), %rax
	movq	%rax, 16(%rdi)
.L654:
	movdqu	32(%r12), %xmm2
	movups	%xmm2, 32(%rbx)
	movdqu	48(%r12), %xmm3
	movups	%xmm3, 48(%rbx)
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L655:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L656
.L636:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L648:
	.cfi_restore_state
	movq	$0, (%r14)
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L700:
	movq	$0, 24(%rax)
	movq	8(%r14), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L653
	movq	%rcx, 8(%r14)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L651
	.p2align 4,,10
	.p2align 3
.L652:
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L652
.L651:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L653
	movq	%rax, 8(%r14)
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L637:
	movq	16(%rcx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$63, %rax
	jbe	.L703
	leaq	64(%r15), %rax
	movq	%rax, 16(%rdi)
.L644:
	movdqu	32(%rbx), %xmm6
	movups	%xmm6, 32(%r15)
	movdqu	48(%rbx), %xmm7
	movups	%xmm7, 48(%r15)
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L638:
	movq	$0, (%rcx)
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L699:
	movq	$0, 24(%rax)
	movq	8(%rcx), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L643
	movq	%rcx, 8(%r14)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L641
	.p2align 4,,10
	.p2align 3
.L642:
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L642
.L641:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L643
	movq	%rax, 8(%r14)
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L702:
	movl	$64, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L654
.L703:
	movl	$64, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L644
	.cfi_endproc
.LFE13974:
	.size	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_
	.section	.rodata._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_
	.type	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_, @function
_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_:
.LFB14057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$3, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L722
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L723
.L706:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L714
	cmpq	$63, 8(%rax)
	ja	.L724
.L714:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L725
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L715:
	movq	%rax, 8(%r13)
	movq	(%r12), %rdx
	movq	64(%rbx), %rax
	movq	%rdx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L723:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L726
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L727
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L711:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L712
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L712:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L713
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L713:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L709:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L724:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L726:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L708
	cmpq	%r13, %rsi
	je	.L709
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L708:
	cmpq	%r13, %rsi
	je	.L709
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L725:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L727:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L711
.L722:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE14057:
	.size	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_, .-_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_
	.section	.text._ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE24_M_get_insert_unique_posERS7_,"axG",@progbits,_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE24_M_get_insert_unique_posERS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE24_M_get_insert_unique_posERS7_
	.type	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE24_M_get_insert_unique_posERS7_, @function
_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE24_M_get_insert_unique_posERS7_:
.LFB14174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	24(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L744
	movq	(%rsi), %r8
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L745:
	jne	.L734
	movq	8(%r12), %rax
	cmpq	%rax, 40(%rbx)
	ja	.L733
.L734:
	movq	24(%rbx), %rax
	xorl	%r9d, %r9d
	testq	%rax, %rax
	je	.L732
.L746:
	movq	%rax, %rbx
.L731:
	movq	32(%rbx), %rcx
	cmpq	%r8, %rcx
	jbe	.L745
.L733:
	movq	16(%rbx), %rax
	movl	$1, %r9d
	testq	%rax, %rax
	jne	.L746
.L732:
	movq	%rbx, %rdx
	testb	%r9b, %r9b
	jne	.L730
	cmpq	%r8, %rcx
	jnb	.L747
.L739:
	xorl	%eax, %eax
.L738:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L744:
	.cfi_restore_state
	leaq	16(%rdi), %rbx
.L730:
	xorl	%eax, %eax
	movq	%rbx, %rdx
	cmpq	32(%rdi), %rbx
	je	.L738
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r12), %r8
	movq	%rbx, %rdx
	movq	32(%rax), %rcx
	movq	%rax, %rbx
	cmpq	%r8, %rcx
	jb	.L739
.L747:
	jne	.L740
	movq	8(%r12), %rax
	cmpq	%rax, 40(%rbx)
	jb	.L739
.L740:
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE14174:
	.size	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE24_M_get_insert_unique_posERS7_, .-_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE24_M_get_insert_unique_posERS7_
	.section	.text._ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESM_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_,"axG",@progbits,_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESM_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESM_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_
	.type	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESM_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_, @function
_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESM_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_:
.LFB13750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$63, %rax
	jbe	.L782
	leaq	64(%rbx), %rax
	movq	%rax, 16(%rdi)
.L750:
	movq	(%rcx), %rax
	leaq	16(%r13), %r15
	movb	$0, 56(%rbx)
	leaq	32(%rbx), %rsi
	movq	$0, 48(%rbx)
	movq	%r12, %r14
	movdqu	(%rax), %xmm0
	movups	%xmm0, 32(%rbx)
	cmpq	%r12, %r15
	je	.L783
	movq	32(%rbx), %rdx
	movq	32(%r12), %rcx
	cmpq	%rcx, %rdx
	jnb	.L784
.L755:
	movq	%rdx, -56(%rbp)
	movq	%rsi, -64(%rbp)
	cmpq	%r12, 32(%r13)
	je	.L769
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rdx
	cmpq	32(%rax), %rdx
	jbe	.L785
.L760:
	cmpq	$0, 24(%rax)
	jne	.L769
	movq	%rax, %r14
.L753:
	xorl	%eax, %eax
.L791:
	cmpq	%r14, %r15
	jne	.L786
	.p2align 4,,10
	.p2align 3
.L773:
	movl	$1, %edi
.L766:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
	addq	$40, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L784:
	.cfi_restore_state
	je	.L787
	jbe	.L788
.L758:
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	movq	%rsi, -72(%rbp)
	cmpq	%r12, 40(%r13)
	je	.L772
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rcx
	cmpq	32(%rax), %rdx
	jb	.L763
	movq	-72(%rbp), %rsi
	je	.L789
	.p2align 4,,10
	.p2align 3
.L764:
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE24_M_get_insert_unique_posERS7_
	movq	%rax, %r14
	testq	%rdx, %rdx
	jne	.L790
.L762:
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L783:
	.cfi_restore_state
	cmpq	$0, 48(%r13)
	je	.L764
	movq	40(%r13), %r14
	movq	32(%rbx), %rax
	cmpq	%rax, 32(%r14)
	jb	.L753
	jne	.L764
	movq	40(%rbx), %rax
	cmpq	%rax, 40(%r14)
	jnb	.L764
	xorl	%eax, %eax
	jmp	.L791
.L790:
	movq	%rax, %r12
	movq	%rdx, %r14
	.p2align 4,,10
	.p2align 3
.L769:
	testq	%r12, %r12
	setne	%al
	cmpq	%r14, %r15
	je	.L773
.L786:
	testb	%al, %al
	jne	.L773
	movq	32(%rbx), %rdx
	movq	32(%r14), %rcx
	movl	$1, %edi
	cmpq	%rdx, %rcx
	ja	.L766
.L765:
	xorl	%edi, %edi
	cmpq	%rdx, %rcx
	jne	.L766
	xorl	%edi, %edi
	movq	40(%r14), %rax
	cmpq	%rax, 40(%rbx)
	setb	%dil
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L787:
	movq	40(%rbx), %rdi
	movq	40(%r12), %rax
	cmpq	%rax, %rdi
	jb	.L755
.L757:
	cmpq	%rax, %rdi
	jbe	.L762
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L785:
	movq	-64(%rbp), %rsi
	jne	.L764
	movq	40(%rbx), %rdx
	cmpq	%rdx, 40(%rax)
	jb	.L760
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L782:
	movl	$64, %esi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rbx
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L789:
	movq	40(%rax), %rdi
	cmpq	%rdi, 40(%rbx)
	jnb	.L764
.L763:
	cmpq	$0, 24(%r12)
	je	.L765
	movq	%rax, %r14
	movl	$1, %edi
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L772:
	xorl	%r12d, %r12d
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L788:
	movq	40(%rbx), %rdi
	movq	40(%r12), %rax
	jmp	.L757
	.cfi_endproc
.LFE13750:
	.size	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESM_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_, .-_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESM_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_
	.section	.text._ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE3SetES6_S8_,"axG",@progbits,_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE3SetES6_S8_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE3SetES6_S8_
	.type	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE3SetES6_S8_, @function
_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE3SetES6_S8_:
.LFB12854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$440, %rsp
	movq	%rdx, -376(%rbp)
	movq	%rcx, -416(%rbp)
	movq	%r8, -424(%rbp)
	movq	%rsi, -384(%rbp)
	movq	%rdi, -408(%rbp)
	movq	%rsi, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base10hash_valueEm@PLT
	movq	-376(%rbp), %rdi
	movq	%rax, %r13
	call	_ZN2v84base10hash_valueEm@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	(%rbx), %r13
	movq	%rax, -448(%rbp)
	testq	%r13, %r13
	je	.L834
	movl	36(%r13), %edx
	xorl	%r12d, %r12d
	movl	%eax, %edi
	movsbl	32(%r13), %r15d
	movl	$-2147483648, %esi
	movl	%r12d, %ecx
	cmpl	%edx, %edi
	je	.L794
	.p2align 4,,10
	.p2align 3
.L858:
	movl	%esi, %eax
	xorl	%edi, %edx
	shrl	%cl, %eax
	testl	%edx, %eax
	jne	.L795
	addl	$1, %ecx
	leaq	-320(%rbp), %r8
	movslq	%ecx, %rax
.L801:
	leal	-1(%rax), %ecx
	cmpl	%ecx, %r15d
	jle	.L857
	movq	40(%r13,%rax,8), %rcx
	movq	%rcx, -8(%r8,%rax,8)
.L855:
	movl	%eax, %ecx
	movl	%esi, %r9d
	addq	$1, %rax
	shrl	%cl, %r9d
	testl	%edx, %r9d
	je	.L801
.L795:
	movslq	%ecx, %rax
	leal	1(%rcx), %r14d
	movq	%r13, -320(%rbp,%rax,8)
	cmpl	%ecx, %r15d
	jle	.L856
	movq	48(%r13,%rax,8), %r13
	testq	%r13, %r13
	je	.L856
	movl	36(%r13), %edx
	movsbl	32(%r13), %r15d
	movl	%r14d, %ecx
	cmpl	%edx, %edi
	jne	.L858
.L794:
	cmpl	%r15d, %ecx
	jge	.L836
	movslq	%ecx, %rax
	leal	-1(%r15), %edx
	salq	$3, %rax
	subl	%ecx, %edx
	leaq	-320(%rbp,%rax), %rdi
	leaq	8(,%rdx,8), %rdx
	leaq	48(%r13,%rax), %rsi
	call	memcpy@PLT
.L803:
	leaq	-384(%rbp), %rax
	movq	-408(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%rax, -456(%rbp)
	call	_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE15GetFocusedValueEPKNSC_11FocusedTreeERKS6_
	movq	-416(%rbp), %rbx
	cmpq	(%rax), %rbx
	je	.L859
.L831:
	movq	40(%r13), %rax
	leal	-1(%r15), %r12d
	movq	%rax, -432(%rbp)
	testq	%rax, %rax
	je	.L860
.L807:
	movq	-408(%rbp), %rax
	movq	24(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L861
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdi)
.L809:
	movq	-408(%rbp), %rax
	leaq	16(%rbx), %rdx
	movq	%rbx, -432(%rbp)
	movq	%rdx, -440(%rbp)
	movq	24(%rax), %rax
	movl	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	%rax, (%rbx)
	movq	%rdx, 32(%rbx)
	movq	%rdx, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	40(%r13), %r14
	testq	%r14, %r14
	je	.L810
	cmpq	%rbx, %r14
	je	.L838
	pxor	%xmm0, %xmm0
	movq	%rbx, -336(%rbp)
	movaps	%xmm0, -352(%rbp)
	movq	24(%r14), %rsi
	testq	%rsi, %rsi
	je	.L838
	leaq	-352(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	%rax, -464(%rbp)
	call	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE7_M_copyINSH_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSL_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %r13
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L813:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L813
	movq	%rcx, 32(%rbx)
	movq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L814:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L814
	movq	%rcx, 40(%rbx)
	movq	48(%r14), %rdx
	movq	%r13, 24(%rbx)
	movq	%rdx, 48(%rbx)
	movq	-352(%rbp), %rax
	movq	-336(%rbp), %r14
	testq	%rax, %rax
	je	.L819
	movq	%rbx, -480(%rbp)
	movq	%r13, -472(%rbp)
	movq	%rax, %r13
.L818:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L816
.L817:
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L817
.L816:
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L818
	movq	-472(%rbp), %r13
	movq	-480(%rbp), %rbx
.L819:
	testq	%r13, %r13
	je	.L839
	movq	-384(%rbp), %rdx
	movq	-376(%rbp), %rcx
	movq	-440(%rbp), %rsi
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L862:
	jne	.L823
	cmpq	%rcx, 40(%r13)
	jb	.L822
.L823:
	movq	%r13, %rsi
	movq	16(%r13), %r13
	testq	%r13, %r13
	je	.L821
.L820:
	cmpq	%rdx, 32(%r13)
	jnb	.L862
.L822:
	movq	24(%r13), %r13
	testq	%r13, %r13
	jne	.L820
.L821:
	cmpq	%rsi, -440(%rbp)
	je	.L811
	cmpq	32(%rsi), %rdx
	jb	.L811
	jne	.L825
	cmpq	40(%rsi), %rcx
	jnb	.L825
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L856:
	movl	%ecx, %r12d
.L793:
	movq	-408(%rbp), %rdi
	xorl	%esi, %esi
	leaq	-384(%rbp), %rdx
	movl	%r14d, %r15d
	call	_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE15GetFocusedValueEPKNSC_11FocusedTreeERKS6_
	movq	-416(%rbp), %rbx
	movq	$0, -432(%rbp)
	cmpq	(%rax), %rbx
	je	.L832
.L804:
	movq	-408(%rbp), %rax
	testl	%r12d, %r12d
	movq	24(%rax), %rdi
	movl	$0, %eax
	cmovns	%r12d, %eax
	movq	16(%rdi), %r13
	movslq	%eax, %r9
	movq	24(%rdi), %rax
	leaq	56(,%r9,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L863
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L827:
	movq	-416(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 48(%r13)
	movups	%xmm0, 32(%r13)
	movq	%rax, 16(%r13)
	movq	-424(%rbp), %rax
	movups	%xmm0, 0(%r13)
	movdqu	-384(%rbp), %xmm1
	movq	%rax, 24(%r13)
	movl	-448(%rbp), %eax
	movb	%r15b, 32(%r13)
	movl	%eax, 36(%r13)
	movq	-432(%rbp), %rax
	movups	%xmm1, 0(%r13)
	movq	%rax, 40(%r13)
	testl	%r15d, %r15d
	je	.L829
	leal	-1(%r15), %eax
	leaq	48(%r13), %rdi
	leaq	8(,%rax,8), %rdx
	leaq	-320(%rbp), %rsi
	call	memcpy@PLT
.L829:
	movq	-408(%rbp), %rax
	movq	%r13, (%rax)
.L792:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L864
	addq	$440, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L857:
	.cfi_restore_state
	movq	$0, -8(%r8,%rax,8)
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L838:
	leaq	-352(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, -464(%rbp)
.L811:
	movq	-456(%rbp), %rax
	movq	-464(%rbp), %rcx
	movq	%rbx, %rdi
	leaq	-353(%rbp), %r8
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	movq	%rax, -352(%rbp)
	call	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESM_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_
	movq	%rax, %rsi
.L825:
	movq	-416(%rbp), %rax
	movq	-424(%rbp), %rbx
	movq	%rax, -400(%rbp)
	movq	%rbx, -392(%rbp)
	movq	%rax, 48(%rsi)
	movzbl	-392(%rbp), %eax
	movb	%al, 56(%rsi)
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L859:
	movzbl	-424(%rbp), %ebx
	cmpb	%bl, 8(%rax)
	jne	.L831
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L832:
	movzbl	-424(%rbp), %ebx
	cmpb	%bl, 8(%rax)
	jne	.L804
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L860:
	movq	-384(%rbp), %rax
	cmpq	%rax, 0(%r13)
	jne	.L807
	movq	-376(%rbp), %rax
	cmpq	%rax, 8(%r13)
	jne	.L807
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L810:
	leaq	-352(%rbp), %rax
	movq	%rdx, %rsi
	leaq	-353(%rbp), %r8
	movq	%rbx, %rdi
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	movq	%rax, %rcx
	movq	%r13, -352(%rbp)
	movq	%rax, -464(%rbp)
	call	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler4NodeES5_ES0_IKS6_NS3_18CsaLoadElimination9FieldInfoEESt10_Select1stISA_ESt4lessIS6_ENS2_13ZoneAllocatorISA_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESM_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_
	movq	16(%r13), %rdx
	movq	%rdx, 48(%rax)
	movzbl	24(%r13), %edx
	movb	%dl, 56(%rax)
	movq	24(%rbx), %r13
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L863:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L827
.L861:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L809
.L836:
	movl	%ecx, %r15d
	jmp	.L803
.L834:
	movl	$-1, %r12d
	xorl	%r14d, %r14d
	jmp	.L793
.L839:
	movq	-440(%rbp), %rsi
	jmp	.L811
.L864:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12854:
	.size	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE3SetES6_S8_, .-_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE3SetES6_S8_
	.section	.text._ZN2v88internal8compiler18CsaLoadElimination13AbstractState5MergeEPKS3_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18CsaLoadElimination13AbstractState5MergeEPKS3_PNS0_4ZoneE
	.type	_ZN2v88internal8compiler18CsaLoadElimination13AbstractState5MergeEPKS3_PNS0_4ZoneE, @function
_ZN2v88internal8compiler18CsaLoadElimination13AbstractState5MergeEPKS3_PNS0_4ZoneE:
.LFB11940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	16(%rdi), %r8d
	movups	%xmm0, -344(%rbp)
	movl	$0, -352(%rbp)
	movq	%r9, -72(%rbp)
	testq	%r15, %r15
	je	.L949
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	movb	%r8b, -64(%rbp)
	leaq	-328(%rbp), %rdx
	movq	%r9, -416(%rbp)
	movb	%r8b, -408(%rbp)
	movq	%rdx, -400(%rbp)
	movq	%rsi, -392(%rbp)
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	movq	-392(%rbp), %rsi
	movq	-400(%rbp), %rdx
	movq	%rax, -336(%rbp)
	movq	%rax, %r15
	movq	40(%rax), %rax
	movzbl	-408(%rbp), %r8d
	movq	-416(%rbp), %r9
	testq	%rax, %rax
	je	.L868
	movq	32(%rax), %rcx
	movq	%rcx, -344(%rbp)
.L868:
	movl	$-2147483648, %r10d
.L908:
	testq	%rax, %rax
	je	.L869
	movq	-344(%rbp), %rax
	movq	48(%rax), %rcx
	movzbl	56(%rax), %eax
.L870:
	cmpq	%r9, %rcx
	sete	%bl
	cmpb	%r8b, %al
	sete	%al
	andb	%al, %bl
	jne	.L882
.L867:
	movl	$-2147483648, %ebx
.L883:
	testq	%r15, %r15
	je	.L895
	cmpq	$0, 40(%r15)
	je	.L885
	movq	-344(%rbp), %rax
	movdqu	32(%rax), %xmm1
	movdqu	48(%rax), %xmm2
	movaps	%xmm1, -384(%rbp)
	movaps	%xmm2, -368(%rbp)
.L886:
	movq	-384(%rbp), %rdi
	call	_ZN2v84base10hash_valueEm@PLT
	movq	-376(%rbp), %rdi
	movq	%rax, %r15
	call	_ZN2v84base10hash_valueEm@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L887
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L893:
	movl	36(%rsi), %edx
	cmpl	%edx, %eax
	je	.L887
	movl	%ebx, %edi
	xorl	%eax, %edx
	shrl	%cl, %edi
	testl	%edx, %edi
	jne	.L891
.L892:
	addl	$1, %ecx
	movl	%ebx, %edi
	shrl	%cl, %edi
	testl	%edx, %edi
	je	.L892
.L891:
	movsbl	32(%rsi), %edx
	cmpl	%ecx, %edx
	jle	.L910
	movslq	%ecx, %rdx
	addl	$1, %ecx
	movq	48(%rsi,%rdx,8), %rsi
	testq	%rsi, %rsi
	jne	.L893
	.p2align 4,,10
	.p2align 3
.L887:
	leaq	-384(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE15GetFocusedValueEPKNSC_11FocusedTreeERKS6_
	movq	-368(%rbp), %rsi
	cmpq	%rsi, (%rax)
	je	.L950
.L890:
	movq	-384(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %r8
	movq	%r13, %rdi
	movq	-376(%rbp), %rdx
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE3SetES6_S8_
.L894:
	movq	-336(%rbp), %r15
.L905:
	testq	%r15, %r15
	je	.L895
	movq	40(%r15), %rdx
	testq	%rdx, %rdx
	je	.L900
	movq	-344(%rbp), %rdi
	movq	%rdx, -392(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-392(%rbp), %rdx
	movq	%rax, -344(%rbp)
	addq	$16, %rdx
	cmpq	%rdx, %rax
	jne	.L883
.L900:
	movl	-352(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L895
	subl	$1, %ecx
	movl	36(%r15), %esi
	movl	%ecx, -352(%rbp)
	movslq	%ecx, %rcx
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L952:
	subq	$1, %rcx
.L904:
	movl	%ebx, %eax
	movl	%ecx, %edx
	shrl	%cl, %eax
	testl	%esi, %eax
	jne	.L901
	movq	-328(%rbp,%rcx,8), %rdi
	leaq	-352(%rbp), %r8
	testq	%rdi, %rdi
	jne	.L951
.L901:
	testl	%ecx, %ecx
	jne	.L952
.L895:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L953
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L910:
	.cfi_restore_state
	xorl	%esi, %esi
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L951:
	addl	$1, %edx
	movq	%r8, %rsi
	movl	%edx, -352(%rbp)
	leaq	-328(%rbp), %rdx
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	movq	%rax, -336(%rbp)
	movq	%rax, %r15
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L954
	movq	32(%rax), %rax
	movq	%rax, -344(%rbp)
	movq	48(%rax), %rdx
	movzbl	56(%rax), %eax
.L906:
	cmpq	%rdx, -72(%rbp)
	jne	.L883
	cmpb	%al, -64(%rbp)
	jne	.L883
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L950:
	movzbl	-360(%rbp), %esi
	cmpb	%sil, 8(%rax)
	jne	.L890
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L885:
	movdqu	(%r15), %xmm3
	movdqu	16(%r15), %xmm4
	movaps	%xmm3, -384(%rbp)
	movaps	%xmm4, -368(%rbp)
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L954:
	movq	16(%r15), %rdx
	movzbl	24(%r15), %eax
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L949:
	movb	%r8b, -64(%rbp)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L869:
	movq	16(%r15), %rcx
	movzbl	24(%r15), %eax
	jmp	.L870
.L956:
	addl	$1, %eax
	movq	%r15, %rdi
	movq	%r9, -416(%rbp)
	movb	%r8b, -408(%rbp)
	movq	%rdx, -400(%rbp)
	movq	%rsi, -392(%rbp)
	movl	%eax, -352(%rbp)
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	movq	-392(%rbp), %rsi
	movq	-400(%rbp), %rdx
	movl	$-2147483648, %r10d
	movq	%rax, -336(%rbp)
	movq	%rax, %r15
	movq	40(%rax), %rax
	movzbl	-408(%rbp), %r8d
	movq	-416(%rbp), %r9
	testq	%rax, %rax
	je	.L955
	movq	32(%rax), %rcx
	movq	%rcx, -344(%rbp)
	movq	48(%rcx), %rax
	movzbl	56(%rcx), %ecx
.L907:
	cmpq	%rax, -72(%rbp)
	jne	.L875
	cmpb	%cl, -64(%rbp)
	jne	.L875
.L882:
	movq	40(%r15), %rcx
	testq	%rcx, %rcx
	je	.L876
	movq	-344(%rbp), %rdi
	movq	%rdx, -424(%rbp)
	movq	%rsi, -416(%rbp)
	movq	%r9, -408(%rbp)
	movb	%r8b, -400(%rbp)
	movq	%rcx, -392(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-392(%rbp), %rcx
	movzbl	-400(%rbp), %r8d
	movl	$-2147483648, %r10d
	movq	%rax, -344(%rbp)
	movq	-408(%rbp), %r9
	addq	$16, %rcx
	movq	-416(%rbp), %rsi
	movq	-424(%rbp), %rdx
	cmpq	%rcx, %rax
	je	.L876
.L875:
	movq	40(%r15), %rax
	jmp	.L908
.L876:
	movl	-352(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L873
	subl	$1, %ecx
	movl	36(%r15), %edi
	xorl	%r11d, %r11d
	movl	%ecx, -352(%rbp)
	movslq	%ecx, %rcx
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L880:
	subq	$1, %rcx
	movl	%ebx, %r11d
.L881:
	movl	%r10d, %r15d
	movl	%ecx, %eax
	shrl	%cl, %r15d
	testl	%edi, %r15d
	jne	.L877
	movq	24(%rsi,%rcx,8), %r15
	testq	%r15, %r15
	jne	.L956
.L877:
	testl	%ecx, %ecx
	jne	.L880
	testb	%r11b, %r11b
	je	.L873
	movl	%eax, -352(%rbp)
.L873:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movups	%xmm0, -344(%rbp)
	jmp	.L867
.L955:
	movq	16(%r15), %rax
	movzbl	24(%r15), %ecx
	jmp	.L907
.L953:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11940:
	.size	_ZN2v88internal8compiler18CsaLoadElimination13AbstractState5MergeEPKS3_PNS0_4ZoneE, .-_ZN2v88internal8compiler18CsaLoadElimination13AbstractState5MergeEPKS3_PNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler18CsaLoadElimination13AbstractState9KillFieldEPNS1_4NodeES5_NS0_21MachineRepresentationEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState9KillFieldEPNS1_4NodeES5_NS0_21MachineRepresentationEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState9KillFieldEPNS1_4NodeES5_NS0_21MachineRepresentationEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState9KillFieldEPNS1_4NodeES5_NS0_21MachineRepresentationEPNS0_4ZoneE:
.LFB11947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -360(%rbp)
	movq	16(%r8), %r12
	movl	%ecx, -364(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%r8), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	jbe	.L1074
	leaq	32(%r12), %rax
	movq	%rax, 16(%r8)
.L959:
	movdqu	0(%r13), %xmm1
	movups	%xmm1, (%r12)
	movdqu	16(%r13), %xmm2
	movq	(%r12), %rbx
	movups	%xmm2, 16(%r12)
	testq	%rbx, %rbx
	je	.L1075
	movzbl	16(%r12), %r9d
	movq	8(%r12), %r15
	pxor	%xmm0, %xmm0
	leaq	-352(%rbp), %rsi
	leaq	-328(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rsi, -376(%rbp)
	movb	%r9b, -64(%rbp)
	movb	%r9b, -392(%rbp)
	movq	%rdx, -384(%rbp)
	movl	$0, -352(%rbp)
	movq	%r15, -72(%rbp)
	movups	%xmm0, -344(%rbp)
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	movq	-376(%rbp), %rsi
	movq	-384(%rbp), %rdx
	movq	%rax, -336(%rbp)
	movq	%rax, %rbx
	movq	40(%rax), %rax
	movzbl	-392(%rbp), %r9d
	testq	%rax, %rax
	je	.L962
	movq	32(%rax), %rcx
	movq	%rcx, -344(%rbp)
.L962:
	movl	$-2147483648, %r10d
.L1008:
	testq	%rax, %rax
	je	.L963
	movq	-344(%rbp), %rax
	movq	48(%rax), %rcx
	movzbl	56(%rax), %eax
.L964:
	cmpb	%r9b, %al
	sete	%r13b
	cmpq	%rcx, %r15
	sete	%al
	andb	%al, %r13b
	jne	.L1076
.L961:
	movzbl	-364(%rbp), %eax
	movl	$-2147483648, %r13d
	subl	$1, %eax
	movb	%al, -364(%rbp)
	movq	%r14, %rax
	movq	%rbx, %r14
	movq	%rax, %rbx
.L977:
	testq	%r14, %r14
	je	.L991
	cmpq	$0, 40(%r14)
	je	.L979
	movq	-344(%rbp), %rax
	movq	32(%rax), %r10
	movq	40(%rax), %rdx
	movzbl	56(%rax), %r11d
.L980:
	movq	-360(%rbp), %rax
	movq	(%rax), %rsi
	movzwl	16(%rsi), %eax
	cmpl	$23, %eax
	je	.L1077
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	cmpl	$24, %eax
	jne	.L982
	movq	48(%rsi), %rdi
	movl	$1, %ecx
.L982:
	movq	(%rdx), %rsi
	movzwl	16(%rsi), %eax
	cmpl	$23, %eax
	je	.L1078
	cmpl	$24, %eax
	je	.L1079
.L985:
	cmpq	%r10, %rbx
	je	.L988
	movq	(%r10), %rax
	movzwl	16(%rax), %ecx
	movq	(%rbx), %rax
	movzwl	16(%rax), %eax
	cmpw	$237, %cx
	je	.L989
	cmpw	$237, %ax
	je	.L1080
.L988:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE3SetES6_S8_
	movq	-336(%rbp), %r14
.L1001:
	testq	%r14, %r14
	je	.L991
	movq	40(%r14), %r15
	testq	%r15, %r15
	je	.L996
	movq	-344(%rbp), %rdi
	addq	$16, %r15
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, -344(%rbp)
	cmpq	%r15, %rax
	jne	.L977
.L996:
	movl	-352(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L991
	subl	$1, %ecx
	movl	36(%r14), %esi
	movl	%ecx, -352(%rbp)
	movslq	%ecx, %rcx
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1082:
	subq	$1, %rcx
.L1000:
	movl	%r13d, %eax
	movl	%ecx, %edx
	shrl	%cl, %eax
	testl	%esi, %eax
	jne	.L997
	movq	-328(%rbp,%rcx,8), %rdi
	leaq	-352(%rbp), %r8
	testq	%rdi, %rdi
	jne	.L1081
.L997:
	testl	%ecx, %ecx
	jne	.L1082
.L991:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1083
	addq	$360, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1078:
	.cfi_restore_state
	movslq	44(%rsi), %r8
.L984:
	testb	%cl, %cl
	je	.L985
	cmpb	$13, -364(%rbp)
	ja	.L1004
	movzbl	-364(%rbp), %eax
	leaq	CSWTCH.175(%rip), %rsi
	subl	$1, %r11d
	movl	(%rsi,%rax,4), %ecx
	movl	$1, %eax
	movl	%eax, %esi
	sall	%cl, %esi
	movslq	%esi, %rsi
	addq	%rdi, %rsi
	cmpb	$13, %r11b
	ja	.L1004
	movzbl	%r11b, %r11d
	leaq	CSWTCH.175(%rip), %rcx
	movl	(%rcx,%r11,4), %ecx
	sall	%cl, %eax
	cltq
	addq	%r8, %rax
	cmpq	%rax, %rdi
	jge	.L1001
	cmpq	%r8, %rsi
	jle	.L1001
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L1077:
	movslq	44(%rsi), %rdi
	movl	$1, %ecx
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1081:
	addl	$1, %edx
	movq	%r8, %rsi
	movl	%edx, -352(%rbp)
	leaq	-328(%rbp), %rdx
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	movq	%rax, -336(%rbp)
	movq	%rax, %r14
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L1084
	movq	32(%rax), %rax
	movq	%rax, -344(%rbp)
	movq	48(%rax), %rdx
	movzbl	56(%rax), %eax
.L1002:
	cmpq	%rdx, -72(%rbp)
	jne	.L977
	cmpb	%al, -64(%rbp)
	jne	.L977
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L979:
	movq	(%r14), %r10
	movq	8(%r14), %rdx
	movzbl	24(%r14), %r11d
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L1079:
	movq	48(%rsi), %r8
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L989:
	cmpw	$50, %ax
	je	.L1001
	cmpw	$237, %ax
	je	.L1001
.L1007:
	cmpw	$30, %ax
	jne	.L988
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1084:
	movq	16(%r14), %rdx
	movzbl	24(%r14), %eax
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1075:
	movq	8(%r12), %rdx
	movzbl	16(%r12), %eax
	pxor	%xmm0, %xmm0
	movl	$0, -352(%rbp)
	movups	%xmm0, -344(%rbp)
	movq	%rdx, -72(%rbp)
	movb	%al, -64(%rbp)
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L963:
	movq	16(%rbx), %rcx
	movzbl	24(%rbx), %eax
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L1074:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L959
.L1076:
	movb	%r9b, -384(%rbp)
	movq	%rdx, %rax
	movq	%rbx, %rdx
	movq	%r14, %rbx
	movq	%r15, -376(%rbp)
	movq	%r12, %r14
	movq	%rsi, %r15
	movq	%rax, %r12
.L976:
	movq	40(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L970
	movq	-344(%rbp), %rdi
	movq	%rdx, -400(%rbp)
	movq	%rcx, -392(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-392(%rbp), %rcx
	movq	-400(%rbp), %rdx
	movl	$-2147483648, %r10d
	movq	%rax, -344(%rbp)
	addq	$16, %rcx
	cmpq	%rcx, %rax
	je	.L970
.L1062:
	movq	%r12, %rax
	movq	%r14, %r12
	movq	%rbx, %r14
	movq	%rdx, %rbx
	movq	%r15, %rsi
	movq	%rax, %rdx
	movq	40(%rbx), %rax
	movzbl	-384(%rbp), %r9d
	movq	-376(%rbp), %r15
	jmp	.L1008
.L970:
	movl	-352(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1085
	subl	$1, %ecx
	movl	36(%rdx), %eax
	xorl	%r8d, %r8d
	movl	%ecx, -352(%rbp)
	movslq	%ecx, %rcx
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L974:
	subq	$1, %rcx
	movl	%r13d, %r8d
.L975:
	movl	%r10d, %edx
	movl	%ecx, %r11d
	shrl	%cl, %edx
	testl	%eax, %edx
	jne	.L971
	movq	24(%r15,%rcx,8), %rdi
	testq	%rdi, %rdi
	jne	.L1086
.L971:
	testl	%ecx, %ecx
	jne	.L974
	movq	%r14, %r12
	movq	%rbx, %r14
	testb	%r8b, %r8b
	je	.L967
	movl	%r11d, -352(%rbp)
.L967:
	pxor	%xmm0, %xmm0
	xorl	%ebx, %ebx
	movups	%xmm0, -344(%rbp)
	jmp	.L961
.L1086:
	addl	$1, %r11d
	movq	%r12, %rdx
	movq	%r15, %rsi
	movl	%r11d, -352(%rbp)
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE12FindLeftmostEPKNSC_11FocusedTreeEPiPSt5arrayISF_Lm32EE
	movl	$-2147483648, %r10d
	movq	%rax, -336(%rbp)
	movq	%rax, %rdx
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L1087
	movq	32(%rax), %rcx
	movq	%rcx, -344(%rbp)
	movq	48(%rcx), %rax
	movzbl	56(%rcx), %ecx
.L1006:
	cmpq	%rax, -72(%rbp)
	jne	.L1062
	cmpb	%cl, -64(%rbp)
	jne	.L1062
	jmp	.L976
.L1087:
	movq	16(%rdx), %rax
	movzbl	24(%rdx), %ecx
	jmp	.L1006
.L1004:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1085:
	movq	%r14, %r12
	movq	%rbx, %r14
	jmp	.L967
.L1083:
	call	__stack_chk_fail@PLT
.L1080:
	cmpw	$50, %cx
	je	.L1001
	movl	%ecx, %eax
	jmp	.L1007
	.cfi_endproc
.LFE11947:
	.size	_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState9KillFieldEPNS1_4NodeES5_NS0_21MachineRepresentationEPNS0_4ZoneE, .-_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState9KillFieldEPNS1_4NodeES5_NS0_21MachineRepresentationEPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler18CsaLoadElimination13AbstractState8AddFieldEPNS1_4NodeES5_NS2_9FieldInfoEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState8AddFieldEPNS1_4NodeES5_NS2_9FieldInfoEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState8AddFieldEPNS1_4NodeES5_NS2_9FieldInfoEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState8AddFieldEPNS1_4NodeES5_NS2_9FieldInfoEPNS0_4ZoneE:
.LFB11948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%r9), %r12
	movq	24(%r9), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	jbe	.L1092
	leaq	32(%r12), %rax
	movq	%rax, 16(%r9)
.L1090:
	movdqu	(%rbx), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movups	%xmm0, (%r12)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%r12)
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE3SetES6_S8_
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1092:
	.cfi_restore_state
	movl	$32, %esi
	movq	%r9, %rdi
	movq	%r8, -56(%rbp)
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	movq	-56(%rbp), %r8
	movq	%rax, %r12
	jmp	.L1090
	.cfi_endproc
.LFE11948:
	.size	_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState8AddFieldEPNS1_4NodeES5_NS2_9FieldInfoEPNS0_4ZoneE, .-_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState8AddFieldEPNS1_4NodeES5_NS2_9FieldInfoEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler18CsaLoadElimination19ReduceStoreToObjectEPNS1_4NodeERKNS1_12ObjectAccessE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18CsaLoadElimination19ReduceStoreToObjectEPNS1_4NodeERKNS1_12ObjectAccessE
	.type	_ZN2v88internal8compiler18CsaLoadElimination19ReduceStoreToObjectEPNS1_4NodeERKNS1_12ObjectAccessE, @function
_ZN2v88internal8compiler18CsaLoadElimination19ReduceStoreToObjectEPNS1_4NodeERKNS1_12ObjectAccessE:
.LFB11960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	56(%r15), %rcx
	movl	20(%rax), %edx
	movq	64(%r15), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L1099
	movq	(%rcx,%rdx,8), %rdi
	testq	%rdi, %rdi
	je	.L1099
	movzbl	(%rbx), %ebx
	movq	88(%r15), %r8
	movq	%r13, %rdx
	movq	%r14, %rsi
	movl	%ebx, %ecx
	call	_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState9KillFieldEPNS1_4NodeES5_NS0_21MachineRepresentationEPNS0_4ZoneE
	movq	88(%r15), %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rdx
	movb	%bl, %r8b
	movq	24(%rdi), %rax
	movq	16(%rdi), %rbx
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L1101
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1098:
	movdqu	(%rdx), %xmm0
	movq	-56(%rbp), %rcx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movups	%xmm0, (%rbx)
	movdqu	16(%rdx), %xmm1
	movq	%r13, %rdx
	movups	%xmm1, 16(%rbx)
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE3SetES6_S8_
	addq	$40, %rsp
	movq	%rbx, %rdx
	movq	%r12, %rsi
	popq	%rbx
	movq	%r15, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CsaLoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	.p2align 4,,10
	.p2align 3
.L1099:
	.cfi_restore_state
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1101:
	.cfi_restore_state
	movl	$32, %esi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	jmp	.L1098
	.cfi_endproc
.LFE11960:
	.size	_ZN2v88internal8compiler18CsaLoadElimination19ReduceStoreToObjectEPNS1_4NodeERKNS1_12ObjectAccessE, .-_ZN2v88internal8compiler18CsaLoadElimination19ReduceStoreToObjectEPNS1_4NodeERKNS1_12ObjectAccessE
	.section	.text._ZN2v88internal8compiler18CsaLoadElimination20ReduceLoadFromObjectEPNS1_4NodeERKNS1_12ObjectAccessE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18CsaLoadElimination20ReduceLoadFromObjectEPNS1_4NodeERKNS1_12ObjectAccessE
	.type	_ZN2v88internal8compiler18CsaLoadElimination20ReduceLoadFromObjectEPNS1_4NodeERKNS1_12ObjectAccessE, @function
_ZN2v88internal8compiler18CsaLoadElimination20ReduceLoadFromObjectEPNS1_4NodeERKNS1_12ObjectAccessE:
.LFB11959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	56(%r15), %rsi
	movl	20(%rax), %edx
	movq	%rax, %rcx
	movq	64(%r15), %rax
	subq	%rsi, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L1103
	movq	(%rsi,%rdx,8), %r9
	testq	%r9, %r9
	je	.L1103
	movzbl	(%rbx), %r10d
	movq	%r9, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r9, -56(%rbp)
	movb	%r10b, -64(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState6LookupEPNS1_4NodeES5_
	movq	-56(%rbp), %r9
	movzbl	-64(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L1106
	cmpb	%r10b, %dl
	movq	-72(%rbp), %rcx
	je	.L1107
	leal	-6(%r10), %eax
	cmpb	$2, %al
	ja	.L1106
	subl	$6, %edx
	cmpb	$2, %dl
	ja	.L1106
.L1107:
	movzbl	23(%rbx), %eax
	leaq	32(%rbx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1109
	movq	32(%rbx), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
.L1109:
	testl	%eax, %eax
	jle	.L1110
	cmpq	$0, (%rdx)
	je	.L1106
.L1110:
	movq	8(%r15), %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	xorl	%r8d, %r8d
	movq	(%rdi), %rax
	call	*32(%rax)
	addq	$40, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1103:
	.cfi_restore_state
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1106:
	.cfi_restore_state
	movq	88(%r15), %rdi
	xorl	%r8d, %r8d
	movb	%r10b, %r8b
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L1117
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1112:
	movdqu	(%r9), %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%r12, %rcx
	movups	%xmm0, (%rbx)
	movdqu	16(%r9), %xmm1
	movups	%xmm1, 16(%rbx)
	call	_ZN2v88internal8compiler13PersistentMapISt4pairIPNS1_4NodeES5_ENS1_18CsaLoadElimination9FieldInfoENS_4base4hashIS6_EEE3SetES6_S8_
	addq	$40, %rsp
	movq	%rbx, %rdx
	movq	%r12, %rsi
	popq	%rbx
	movq	%r15, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CsaLoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	.p2align 4,,10
	.p2align 3
.L1117:
	.cfi_restore_state
	movl	$32, %esi
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	movq	%rax, %rbx
	jmp	.L1112
	.cfi_endproc
.LFE11959:
	.size	_ZN2v88internal8compiler18CsaLoadElimination20ReduceLoadFromObjectEPNS1_4NodeERKNS1_12ObjectAccessE, .-_ZN2v88internal8compiler18CsaLoadElimination20ReduceLoadFromObjectEPNS1_4NodeERKNS1_12ObjectAccessE
	.section	.text._ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB14201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$6, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L1132
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L1120:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L1121
	movq	%r15, %rbx
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1122:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1133
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1123:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L1121
.L1126:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L1122
	cmpq	$63, 8(%rax)
	jbe	.L1122
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L1126
.L1121:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$63, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,8), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1133:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1132:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1120
	.cfi_endproc
.LFE14201:
	.size	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZNK2v88internal8compiler18CsaLoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18CsaLoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE
	.type	_ZNK2v88internal8compiler18CsaLoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE, @function
_ZNK2v88internal8compiler18CsaLoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE:
.LFB11967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-256(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$360, %rsp
	movq	%rdi, -384(%rbp)
	movq	%rsi, %rdi
	movq	%rsi, -376(%rbp)
	xorl	%esi, %esi
	movq	%rdx, -400(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	88(%rbx), %rax
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	%rax, -256(%rbp)
	movq	$0, -232(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	je	.L1204
	movdqa	-256(%rbp), %xmm5
	leaq	-160(%rbp), %rbx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-96(%rbp), %xmm5
	movq	-216(%rbp), %r9
	movq	-224(%rbp), %xmm3
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %xmm1
	movq	-184(%rbp), %rcx
	movaps	%xmm5, -192(%rbp)
	movq	%r9, %xmm5
	punpcklqdq	%xmm5, %xmm3
	movdqa	-128(%rbp), %xmm6
	movq	-208(%rbp), %xmm2
	movaps	%xmm3, -320(%rbp)
	movq	%r8, %xmm3
	movq	-168(%rbp), %rdx
	movdqa	-112(%rbp), %xmm7
	punpcklqdq	%xmm3, %xmm2
	movq	-248(%rbp), %rax
	movq	-176(%rbp), %xmm0
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm2, -304(%rbp)
	movq	%rcx, %xmm2
	movq	-256(%rbp), %xmm4
	movq	-240(%rbp), %r11
	punpcklqdq	%xmm2, %xmm1
	movq	-232(%rbp), %r10
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm7, -208(%rbp)
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	%rax, %xmm7
	movaps	%xmm1, -288(%rbp)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm7, %xmm4
	movq	%r11, -336(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -240(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%r10, -328(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm4, -352(%rbp)
	movaps	%xmm0, -272(%rbp)
	testq	%rsi, %rsi
	je	.L1136
	movq	-168(%rbp), %rcx
	movq	-200(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L1137
	.p2align 4,,10
	.p2align 3
.L1140:
	testq	%rax, %rax
	je	.L1138
	cmpq	$64, 8(%rax)
	ja	.L1139
.L1138:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -248(%rbp)
.L1139:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L1140
	movq	-232(%rbp), %rdi
	movq	-240(%rbp), %rsi
.L1137:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L1136
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L1136:
	movq	-384(%rbp), %rax
	movq	%rbx, %rdi
	leaq	-376(%rbp), %rsi
	movl	$0, -144(%rbp)
	movq	$0, -112(%rbp)
	movl	$1, %r15d
	movq	$0, -136(%rbp)
	movq	88(%rax), %rax
	movq	%rax, -160(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movl	$8, %ecx
	leaq	-352(%rbp), %rdi
	movq	%rbx, -392(%rbp)
	movq	-288(%rbp), %rdx
	movq	%rcx, %r14
	movq	%rdi, %rbx
	jmp	.L1149
	.p2align 4,,10
	.p2align 3
.L1205:
	leaq	(%rcx,%r14), %rax
.L1146:
	movq	(%rax), %rcx
	movq	-272(%rbp), %rax
	subq	$8, %rax
	movq	%rcx, -256(%rbp)
	cmpq	%rax, %rdx
	je	.L1147
	movq	%rcx, (%rdx)
	movq	-288(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -288(%rbp)
.L1148:
	addl	$1, %r15d
	addq	$8, %r14
.L1149:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1143
	movq	32(%r12), %rax
	movl	8(%rax), %eax
.L1143:
	cmpl	%eax, %r15d
	jge	.L1144
	movq	-376(%rbp), %rsi
	movzbl	23(%rsi), %eax
	leaq	32(%rsi), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1205
	movq	32(%rsi), %rax
	leaq	16(%rax,%r14), %rax
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1147:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_
	movq	-288(%rbp), %rdx
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1144:
	movq	-320(%rbp), %rax
	movq	-392(%rbp), %rbx
	leaq	-360(%rbp), %r12
	leaq	-352(%rbp), %r15
	cmpq	%rdx, %rax
	jne	.L1150
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1159:
	movq	-320(%rbp), %rax
	cmpq	%rax, -288(%rbp)
	je	.L1157
.L1150:
	movq	(%rax), %rdx
	movq	-304(%rbp), %rcx
	movq	%rdx, -360(%rbp)
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L1152
	addq	$8, %rax
	movq	%rax, -320(%rbp)
.L1153:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	testb	%dl, %dl
	je	.L1159
	movq	-360(%rbp), %rdi
	movq	(%rdi), %rax
	testb	$16, 18(%rax)
	je	.L1206
	movl	24(%rax), %eax
	testl	%eax, %eax
	jle	.L1159
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L1162:
	movl	%r14d, %esi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	-272(%rbp), %rsi
	movq	-288(%rbp), %rcx
	movq	%rax, -256(%rbp)
	leaq	-8(%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L1160
	movq	%rax, (%rcx)
	movq	-360(%rbp), %rdi
	addl	$1, %r14d
	addq	$8, -288(%rbp)
	movq	(%rdi), %rax
	cmpl	%r14d, 24(%rax)
	jg	.L1162
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	%r15, %rdi
	movq	%r13, %rsi
	addl	$1, %r14d
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJS4_EEEvDpOT_
	movq	-360(%rbp), %rdi
	movq	(%rdi), %rax
	cmpl	24(%rax), %r14d
	jl	.L1162
	movq	-320(%rbp), %rax
	cmpq	%rax, -288(%rbp)
	jne	.L1150
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	-400(%rbp), %r14
.L1151:
	movq	-136(%rbp), %r13
	testq	%r13, %r13
	je	.L1163
.L1166:
	movq	24(%r13), %r12
	testq	%r12, %r12
	je	.L1164
.L1165:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1165
.L1164:
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L1166
.L1163:
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L1134
	movq	-264(%rbp), %rbx
	movq	-296(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L1168
	movq	-344(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L1171:
	testq	%rax, %rax
	je	.L1169
	cmpq	$64, 8(%rax)
	ja	.L1170
.L1169:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-344(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -344(%rbp)
.L1170:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L1171
	movq	-336(%rbp), %rax
.L1168:
	movq	-328(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L1134
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L1134:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1207
	addq	$360, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1152:
	.cfi_restore_state
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L1154
	cmpq	$64, 8(%rax)
	ja	.L1155
.L1154:
	movq	-312(%rbp), %rax
	movq	$64, 8(%rax)
	movq	-344(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -344(%rbp)
.L1155:
	movq	-296(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -296(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -312(%rbp)
	movq	%rdx, -304(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1204:
	movdqa	-256(%rbp), %xmm6
	movdqa	-224(%rbp), %xmm7
	movq	$0, -336(%rbp)
	leaq	-160(%rbp), %rbx
	movq	-232(%rbp), %rax
	movdqa	-176(%rbp), %xmm1
	movaps	%xmm6, -352(%rbp)
	movdqa	-208(%rbp), %xmm6
	movaps	%xmm7, -320(%rbp)
	movdqa	-192(%rbp), %xmm7
	movq	%rax, -328(%rbp)
	movaps	%xmm6, -304(%rbp)
	movaps	%xmm7, -288(%rbp)
	movaps	%xmm1, -272(%rbp)
	jmp	.L1136
.L1206:
	movq	-384(%rbp), %r14
	addq	$16, %r14
	jmp	.L1151
.L1207:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11967:
	.size	_ZNK2v88internal8compiler18CsaLoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE, .-_ZNK2v88internal8compiler18CsaLoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE
	.section	.text._ZN2v88internal8compiler18CsaLoadElimination15ReduceEffectPhiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18CsaLoadElimination15ReduceEffectPhiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18CsaLoadElimination15ReduceEffectPhiEPNS1_4NodeE, @function
_ZN2v88internal8compiler18CsaLoadElimination15ReduceEffectPhiEPNS1_4NodeE:
.LFB11961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	56(%r12), %rsi
	movq	64(%r12), %rdx
	movl	20(%rbx), %ecx
	subq	%rsi, %rdx
	andl	$16777215, %ecx
	sarq	$3, %rdx
	cmpq	%rcx, %rdx
	jbe	.L1222
	movq	(%rsi,%rcx,8), %r14
	testq	%r14, %r14
	je	.L1222
	movq	(%rax), %rax
	cmpw	$1, 16(%rax)
	je	.L1230
	movq	0(%r13), %rax
	movl	24(%rax), %ebx
	cmpl	$1, %ebx
	jle	.L1213
	movl	$1, %r15d
.L1214:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	56(%r12), %rcx
	movl	20(%rax), %edx
	movq	64(%r12), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L1222
	cmpq	$0, (%rcx,%rdx,8)
	je	.L1222
	addl	$1, %r15d
	cmpl	%ebx, %r15d
	jne	.L1214
.L1213:
	movq	88(%r12), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$31, %rax
	jbe	.L1231
	leaq	32(%r15), %rax
	movq	%rax, 16(%rdi)
.L1216:
	movdqu	(%r14), %xmm0
	movups	%xmm0, (%r15)
	movdqu	16(%r14), %xmm1
	movups	%xmm1, 16(%r15)
	cmpl	$1, %ebx
	jle	.L1217
	movl	$1, %r14d
	jmp	.L1220
	.p2align 4,,10
	.p2align 3
.L1232:
	movq	(%rcx,%rdx,8), %rsi
	movq	%r8, %rdx
.L1228:
	movq	%r15, %rdi
	addl	$1, %r14d
	call	_ZN2v88internal8compiler18CsaLoadElimination13AbstractState5MergeEPKS3_PNS0_4ZoneE
	cmpl	%ebx, %r14d
	je	.L1217
.L1220:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	56(%r12), %rcx
	movq	88(%r12), %r8
	movl	20(%rax), %edx
	movq	64(%r12), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jb	.L1232
	movq	%r8, %rdx
	xorl	%esi, %esi
	jmp	.L1228
	.p2align 4,,10
	.p2align 3
.L1222:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1230:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler18CsaLoadElimination16ComputeLoopStateEPNS1_4NodeEPKNS2_13AbstractStateE
	movq	%rax, %rdx
.L1229:
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CsaLoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	.p2align 4,,10
	.p2align 3
.L1217:
	.cfi_restore_state
	movq	%r15, %rdx
	jmp	.L1229
.L1231:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L1216
	.cfi_endproc
.LFE11961:
	.size	_ZN2v88internal8compiler18CsaLoadElimination15ReduceEffectPhiEPNS1_4NodeE, .-_ZN2v88internal8compiler18CsaLoadElimination15ReduceEffectPhiEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler18CsaLoadElimination6ReduceEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC5:
	.string	" visit #%d:%s"
.LC6:
	.string	"("
.LC7:
	.string	")"
.LC8:
	.string	", "
.LC9:
	.string	"#%d:%s"
.LC10:
	.string	"\n"
.LC11:
	.string	"  state[%i]: #%d:%s\n"
.LC12:
	.string	"  no state[%i]: #%d:%s\n"
	.section	.text._ZN2v88internal8compiler18CsaLoadElimination6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18CsaLoadElimination6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18CsaLoadElimination6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler18CsaLoadElimination6ReduceEPNS1_4NodeE:
.LFB11936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpb	$0, _ZN2v88internal33FLAG_trace_turbo_load_eliminationE(%rip)
	movq	(%rsi), %rdi
	je	.L1234
	movl	24(%rdi), %esi
	testl	%esi, %esi
	jg	.L1267
.L1234:
	movzwl	16(%rdi), %eax
	cmpw	$61, %ax
	je	.L1243
	ja	.L1244
	cmpw	$36, %ax
	je	.L1245
	cmpw	$49, %ax
	jne	.L1268
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CsaLoadElimination10ReduceCallEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1244:
	.cfi_restore_state
	cmpw	$248, %ax
	je	.L1249
	jbe	.L1269
	subw	$426, %ax
	cmpw	$1, %ax
	ja	.L1248
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	56(%r13), %rsi
	xorl	%edx, %edx
	movl	20(%rax), %ecx
	movq	64(%r13), %rax
	subq	%rsi, %rax
	andl	$16777215, %ecx
	sarq	$3, %rax
	cmpq	%rax, %rcx
	jnb	.L1253
	movq	(%rsi,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.L1253
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CsaLoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
	movq	%rax, %rdx
.L1253:
	addq	$8, %rsp
	movq	%rdx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1268:
	.cfi_restore_state
	testw	%ax, %ax
	jne	.L1248
	addq	$8, %rsp
	leaq	16(%r13), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CsaLoadElimination11UpdateStateEPNS1_4NodeEPKNS2_13AbstractStateE
.L1269:
	.cfi_restore_state
	cmpw	$243, %ax
	jne	.L1248
	call	_ZN2v88internal8compiler14ObjectAccessOfEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CsaLoadElimination20ReduceLoadFromObjectEPNS1_4NodeERKNS1_12ObjectAccessE
	.p2align 4,,10
	.p2align 3
.L1267:
	.cfi_restore_state
	movl	20(%r12), %esi
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rdi
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rax
	movl	20(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L1270
.L1235:
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rdi
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jle	.L1234
	xorl	%r14d, %r14d
	leaq	.LC12(%rip), %rbx
	jmp	.L1242
	.p2align 4,,10
	.p2align 3
.L1271:
	leaq	.LC11(%rip), %rdi
	movl	%r8d, %edx
	movl	%r14d, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler18CsaLoadElimination13AbstractState5PrintEv
.L1241:
	movq	(%r12), %rdi
	addl	$1, %r14d
	cmpl	%r14d, 24(%rdi)
	jle	.L1234
.L1242:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	56(%r13), %rcx
	movq	64(%r13), %rdx
	movl	20(%rax), %r8d
	subq	%rcx, %rdx
	andl	$16777215, %r8d
	sarq	$3, %rdx
	movl	%r8d, %esi
	cmpq	%rdx, %rsi
	jnb	.L1239
	movq	(%rcx,%rsi,8), %r15
	movq	(%rax), %rax
	movq	8(%rax), %rcx
	testq	%r15, %r15
	jne	.L1271
	movl	%r8d, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1270:
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rax
	leaq	.LC9(%rip), %rbx
	movl	20(%rax), %edx
	testl	%edx, %edx
	jg	.L1236
	jmp	.L1238
	.p2align 4,,10
	.p2align 3
.L1237:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L1236:
	movl	%r14d, %esi
	movq	%r12, %rdi
	addl	$1, %r14d
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	(%rax), %rdx
	movl	20(%rax), %esi
	xorl	%eax, %eax
	movq	8(%rdx), %rdx
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rax
	cmpl	20(%rax), %r14d
	jl	.L1237
.L1238:
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1235
	.p2align 4,,10
	.p2align 3
.L1239:
	movq	(%rax), %rax
	movl	%r8d, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movq	8(%rax), %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1249:
	call	_ZN2v88internal8compiler14ObjectAccessOfEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CsaLoadElimination19ReduceStoreToObjectEPNS1_4NodeERKNS1_12ObjectAccessE
	.p2align 4,,10
	.p2align 3
.L1248:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CsaLoadElimination15ReduceOtherNodeEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1243:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1245:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CsaLoadElimination15ReduceEffectPhiEPNS1_4NodeE
	.cfi_endproc
.LFE11936:
	.size	_ZN2v88internal8compiler18CsaLoadElimination6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler18CsaLoadElimination6ReduceEPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler18CsaLoadElimination6ReduceEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler18CsaLoadElimination6ReduceEPNS1_4NodeE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler18CsaLoadElimination6ReduceEPNS1_4NodeE:
.LFB14481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE14481:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler18CsaLoadElimination6ReduceEPNS1_4NodeE, .-_GLOBAL__sub_I__ZN2v88internal8compiler18CsaLoadElimination6ReduceEPNS1_4NodeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler18CsaLoadElimination6ReduceEPNS1_4NodeE
	.section	.rodata.CSWTCH.175,"a"
	.align 32
	.type	CSWTCH.175, @object
	.size	CSWTCH.175, 56
CSWTCH.175:
	.long	0
	.long	0
	.long	1
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	2
	.long	3
	.long	4
	.weak	_ZTVN2v88internal8compiler18CsaLoadEliminationE
	.section	.data.rel.ro._ZTVN2v88internal8compiler18CsaLoadEliminationE,"awG",@progbits,_ZTVN2v88internal8compiler18CsaLoadEliminationE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler18CsaLoadEliminationE, @object
	.size	_ZTVN2v88internal8compiler18CsaLoadEliminationE, 56
_ZTVN2v88internal8compiler18CsaLoadEliminationE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler18CsaLoadEliminationD1Ev
	.quad	_ZN2v88internal8compiler18CsaLoadEliminationD0Ev
	.quad	_ZNK2v88internal8compiler18CsaLoadElimination12reducer_nameEv
	.quad	_ZN2v88internal8compiler18CsaLoadElimination6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata._ZStL19piecewise_construct,"a"
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
