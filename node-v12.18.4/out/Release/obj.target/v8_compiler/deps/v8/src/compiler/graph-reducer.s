	.file	"graph-reducer.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1886:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1886:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v88internal8compiler7Reducer8FinalizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.type	_ZN2v88internal8compiler7Reducer8FinalizeEv, @function
_ZN2v88internal8compiler7Reducer8FinalizeEv:
.LFB10216:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10216:
	.size	_ZN2v88internal8compiler7Reducer8FinalizeEv, .-_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB12223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE12223:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB12365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12365:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB12224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12224:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB12366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE12366:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal8compiler12GraphReducerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12GraphReducerD2Ev
	.type	_ZN2v88internal8compiler12GraphReducerD2Ev, @function
_ZN2v88internal8compiler12GraphReducerD2Ev:
.LFB10249:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler12GraphReducerE(%rip), %rax
	movq	%rax, (%rdi)
	movq	176(%rdi), %rax
	testq	%rax, %rax
	je	.L13
	movq	248(%rdi), %rsi
	movq	216(%rdi), %rdx
	leaq	8(%rsi), %rcx
	cmpq	%rdx, %rcx
	jbe	.L14
	movq	168(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L17:
	testq	%rax, %rax
	je	.L15
	cmpq	$32, 8(%rax)
	ja	.L16
.L15:
	movq	(%rdx), %rax
	movq	$32, 8(%rax)
	movq	168(%rdi), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 168(%rdi)
.L16:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L17
	movq	176(%rdi), %rax
.L14:
	movq	184(%rdi), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L13
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L13:
	movq	80(%rdi), %rax
	testq	%rax, %rax
	je	.L12
	movq	152(%rdi), %rsi
	movq	120(%rdi), %rdx
	leaq	8(%rsi), %rcx
	cmpq	%rdx, %rcx
	jbe	.L20
	movq	72(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L23:
	testq	%rax, %rax
	je	.L21
	cmpq	$64, 8(%rax)
	ja	.L22
.L21:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	72(%rdi), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 72(%rdi)
.L22:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L23
	movq	80(%rdi), %rax
.L20:
	movq	88(%rdi), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L12
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L12:
	ret
	.cfi_endproc
.LFE10249:
	.size	_ZN2v88internal8compiler12GraphReducerD2Ev, .-_ZN2v88internal8compiler12GraphReducerD2Ev
	.globl	_ZN2v88internal8compiler12GraphReducerD1Ev
	.set	_ZN2v88internal8compiler12GraphReducerD1Ev,_ZN2v88internal8compiler12GraphReducerD2Ev
	.section	.text._ZN2v88internal8compiler12GraphReducerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12GraphReducerD0Ev
	.type	_ZN2v88internal8compiler12GraphReducerD0Ev, @function
_ZN2v88internal8compiler12GraphReducerD0Ev:
.LFB10251:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler12GraphReducerE(%rip), %rax
	movq	%rax, (%rdi)
	movq	176(%rdi), %rax
	testq	%rax, %rax
	je	.L40
	movq	248(%rdi), %rsi
	movq	216(%rdi), %rdx
	leaq	8(%rsi), %rcx
	cmpq	%rdx, %rcx
	jbe	.L41
	movq	168(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L44:
	testq	%rax, %rax
	je	.L42
	cmpq	$32, 8(%rax)
	ja	.L43
.L42:
	movq	(%rdx), %rax
	movq	$32, 8(%rax)
	movq	168(%rdi), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 168(%rdi)
.L43:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L44
	movq	176(%rdi), %rax
.L41:
	movq	184(%rdi), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L40
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L40:
	movq	80(%rdi), %rax
	testq	%rax, %rax
	je	.L46
	movq	152(%rdi), %rsi
	movq	120(%rdi), %rdx
	leaq	8(%rsi), %rcx
	cmpq	%rdx, %rcx
	jbe	.L47
	movq	72(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L50:
	testq	%rax, %rax
	je	.L48
	cmpq	$64, 8(%rax)
	ja	.L49
.L48:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	72(%rdi), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 72(%rdi)
.L49:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L50
	movq	80(%rdi), %rax
.L47:
	movq	88(%rdi), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L46
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L46:
	movl	$264, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10251:
	.size	_ZN2v88internal8compiler12GraphReducerD0Ev, .-_ZN2v88internal8compiler12GraphReducerD0Ev
	.section	.rodata._ZN2v88internal8compiler12GraphReducer10AddReducerEPNS1_7ReducerE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal8compiler12GraphReducer10AddReducerEPNS1_7ReducerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12GraphReducer10AddReducerEPNS1_7ReducerE
	.type	_ZN2v88internal8compiler12GraphReducer10AddReducerEPNS1_7ReducerE, @function
_ZN2v88internal8compiler12GraphReducer10AddReducerEPNS1_7ReducerE:
.LFB10252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	48(%rdi), %r12
	cmpq	56(%rdi), %r12
	je	.L67
	movq	%rsi, (%r12)
	addq	$8, 48(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	40(%rdi), %r14
	movq	%r12, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L92
	testq	%rax, %rax
	je	.L79
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L93
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L70:
	movq	32(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L94
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L73:
	leaq	(%rax,%r15), %rsi
	leaq	8(%rax), %rdx
.L71:
	movq	%r13, (%rax,%rcx)
	cmpq	%r14, %r12
	je	.L74
	subq	$8, %r12
	leaq	15(%rax), %rdx
	subq	%r14, %r12
	subq	%r14, %rdx
	movq	%r12, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L82
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L82
	leaq	1(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L76:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L76
	movq	%rdi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %r14
	addq	%rax, %rcx
	cmpq	%rdx, %rdi
	je	.L78
	movq	(%r14), %rdx
	movq	%rdx, (%rcx)
.L78:
	leaq	16(%rax,%r12), %rdx
.L74:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rsi, 56(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 40(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L95
	movl	$8, %edx
	xorl	%esi, %esi
	xorl	%eax, %eax
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L82:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L75:
	movq	(%r14,%rdx,8), %rdi
	movq	%rdi, (%rax,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rdi, %rcx
	jne	.L75
	jmp	.L78
.L94:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L73
.L92:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L95:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L70
	.cfi_endproc
.LFE10252:
	.size	_ZN2v88internal8compiler12GraphReducer10AddReducerEPNS1_7ReducerE, .-_ZN2v88internal8compiler12GraphReducer10AddReducerEPNS1_7ReducerE
	.section	.rodata._ZN2v88internal8compiler12GraphReducer6ReduceEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"- In-place update of "
.LC2:
	.string	" by reducer "
.LC3:
	.string	"- Replacement of "
.LC4:
	.string	" with "
	.section	.text._ZN2v88internal8compiler12GraphReducer6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12GraphReducer6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler12GraphReducer6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler12GraphReducer6ReduceEPNS1_4NodeE:
.LFB10255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rcx
	movq	%rcx, %xmm2
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rsi
	pushq	%r12
	movq	%rsi, %xmm1
	pushq	%rbx
	punpcklqdq	%xmm2, %xmm1
	subq	$744, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	movaps	%xmm1, -768(%rbp)
	movq	%rax, %r14
	cmpq	%rax, %rbx
	je	.L98
.L97:
	cmpq	%rbx, %r14
	je	.L100
	movq	256(%r15), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L134
	movzbl	_ZN2v88internal26FLAG_trace_turbo_reductionE(%rip), %eax
	cmpq	%r13, %r12
	je	.L135
	testb	%al, %al
	jne	.L136
.L110:
	movq	%r12, %rax
.L116:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L137
	addq	$744, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	48(%r15), %rax
.L100:
	addq	$8, %rbx
.L109:
	cmpq	%rax, %rbx
	jne	.L97
	cmpq	%rbx, %r14
	je	.L98
	movq	%r13, %rax
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L136:
	leaq	-320(%rbp), %r14
	leaq	-400(%rbp), %r15
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rax, -320(%rbp)
	movq	%r15, %rdi
	xorl	%eax, %eax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$17, %edx
	movq	%r15, %rdi
	movq	%rax, -400(%rbp)
	leaq	.LC3(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_4NodeE@PLT
	movl	$6, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_4NodeE@PLT
	movl	$12, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L138
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L112:
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %rbx
	testq	%rbx, %rbx
	je	.L113
	cmpb	$0, 56(%rbx)
	je	.L114
	movsbl	67(%rbx), %esi
.L115:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L138:
	movq	0(%r13), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L135:
	testb	%al, %al
	jne	.L139
.L103:
	movq	%rbx, %r14
	movq	48(%r15), %rax
	movq	40(%r15), %rbx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L114:
	movq	%rbx, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%rbx), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L115
	movq	%rbx, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L139:
	leaq	-672(%rbp), %r12
	leaq	-752(%rbp), %r14
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movw	%dx, -448(%rbp)
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -440(%rbp)
	movups	%xmm0, -424(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -456(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r14, %rdi
	movl	$21, %edx
	movq	%rax, -752(%rbp)
	leaq	.LC1(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -672(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_4NodeE@PLT
	movl	$12, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	testq	%rax, %rax
	je	.L140
	movq	%rax, %rdi
	movq	%rax, -776(%rbp)
	call	strlen@PLT
	movq	-776(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L105:
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %rdi
	testq	%rdi, %rdi
	je	.L113
	cmpb	$0, 56(%rdi)
	je	.L107
	movsbl	67(%rdi), %esi
.L108:
	movq	%r14, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-768(%rbp), %xmm4
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	-688(%rbp), %rdi
	movq	%rax, -672(%rbp)
	movaps	%xmm4, -752(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -752(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -672(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L103
.L140:
	movq	(%r14), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L98:
	xorl	%eax, %eax
	jmp	.L116
.L107:
	movq	%rdi, -776(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-776(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L108
	call	*%rax
	movsbl	%al, %esi
	jmp	.L108
.L137:
	call	__stack_chk_fail@PLT
.L113:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE10255:
	.size	_ZN2v88internal8compiler12GraphReducer6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler12GraphReducer6ReduceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12GraphReducer3PopEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12GraphReducer3PopEv
	.type	_ZN2v88internal8compiler12GraphReducer3PopEv, @function
_ZN2v88internal8compiler12GraphReducer3PopEv:
.LFB10260:
	.cfi_startproc
	endbr64
	movq	224(%rdi), %rax
	cmpq	232(%rdi), %rax
	je	.L150
.L142:
	movq	-16(%rax), %rdx
	movl	24(%rdi), %eax
	addl	$3, %eax
	movl	%eax, 16(%rdx)
	movq	224(%rdi), %rax
	movq	232(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L143
	subq	$16, %rax
	movq	%rax, 224(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	movq	248(%rdi), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L143:
	movq	168(%rdi), %rax
	testq	%rax, %rax
	je	.L145
	cmpq	$32, 8(%rax)
	ja	.L146
.L145:
	movq	$32, 8(%rdx)
	movq	168(%rdi), %rax
	movq	%rax, (%rdx)
	movq	%rdx, 168(%rdi)
.L146:
	movq	248(%rdi), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 248(%rdi)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 232(%rdi)
	addq	$496, %rax
	movq	%rdx, 240(%rdi)
	movq	%rax, 224(%rdi)
	ret
	.cfi_endproc
.LFE10260:
	.size	_ZN2v88internal8compiler12GraphReducer3PopEv, .-_ZN2v88internal8compiler12GraphReducer3PopEv
	.section	.rodata._ZN2v88internal8compiler12GraphReducer4PushEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZN2v88internal8compiler12GraphReducer4PushEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12GraphReducer4PushEPNS1_4NodeE
	.type	_ZN2v88internal8compiler12GraphReducer4PushEPNS1_4NodeE, @function
_ZN2v88internal8compiler12GraphReducer4PushEPNS1_4NodeE:
.LFB10261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	24(%rdi), %eax
	addl	$2, %eax
	movl	%eax, 16(%rsi)
	movq	240(%rdi), %rcx
	movq	224(%rdi), %rax
	leaq	-16(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L152
	movq	%rsi, (%rax)
	movl	$0, 8(%rax)
	addq	$16, 224(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movq	248(%rdi), %r13
	movq	216(%rdi), %rsi
	subq	232(%rdi), %rax
	movq	%r13, %r14
	sarq	$4, %rax
	subq	%rsi, %r14
	movq	%r14, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$5, %rdx
	addq	%rax, %rdx
	movq	208(%rdi), %rax
	subq	192(%rdi), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$134217727, %rax
	je	.L171
	movq	184(%rdi), %rdx
	movq	176(%rdi), %rdi
	movq	%r13, %rax
	subq	%rdi, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L172
.L155:
	movq	168(%rbx), %rax
	testq	%rax, %rax
	je	.L163
	cmpq	$31, 8(%rax)
	ja	.L173
.L163:
	movq	160(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L174
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L164:
	movq	%rax, 8(%r13)
	movq	224(%rbx), %rax
	movl	$0, 8(%rax)
	movq	%r12, (%rax)
	movq	248(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 248(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 232(%rbx)
	movq	%rdx, 240(%rbx)
	movq	%rax, 224(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L175
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	160(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L176
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L160:
	movq	%rcx, %rax
	movq	216(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	248(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L161
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L161:
	movq	184(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L162
	movq	176(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L162:
	movq	%r13, 176(%rbx)
	movq	%rcx, 184(%rbx)
.L158:
	movq	%r15, 216(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 248(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 200(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 232(%rbx)
	addq	$512, %rax
	movq	%rax, 240(%rbx)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L175:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rdi,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L157
	cmpq	%r13, %rsi
	je	.L158
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L173:
	movq	(%rax), %rdx
	movq	%rdx, 168(%rbx)
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L157:
	cmpq	%r13, %rsi
	je	.L158
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L174:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L164
.L176:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L160
.L171:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10261:
	.size	_ZN2v88internal8compiler12GraphReducer4PushEPNS1_4NodeE, .-_ZN2v88internal8compiler12GraphReducer4PushEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12GraphReducer7RecurseEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12GraphReducer7RecurseEPNS1_4NodeE
	.type	_ZN2v88internal8compiler12GraphReducer7RecurseEPNS1_4NodeE, @function
_ZN2v88internal8compiler12GraphReducer7RecurseEPNS1_4NodeE:
.LFB10262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	16(%rsi), %eax
	movl	24(%rdi), %edx
	cmpl	%edx, %eax
	jb	.L178
	subl	%edx, %eax
	xorl	%r8d, %r8d
	cmpb	$1, %al
	jbe	.L178
.L177:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	addl	$2, %edx
	movl	%edx, 16(%r12)
	movq	240(%rbx), %rcx
	movq	224(%rbx), %rax
	leaq	-16(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L180
	movq	%r12, (%rax)
	movl	$0, 8(%rax)
	addq	$16, 224(%rbx)
.L181:
	movl	$1, %r8d
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L180:
	movq	248(%rbx), %r13
	movq	216(%rbx), %rsi
	subq	232(%rbx), %rax
	movq	%r13, %r14
	sarq	$4, %rax
	subq	%rsi, %r14
	movq	%r14, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$5, %rdx
	addq	%rax, %rdx
	movq	208(%rbx), %rax
	subq	192(%rbx), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$134217727, %rax
	je	.L200
	movq	176(%rbx), %rdi
	movq	184(%rbx), %rdx
	movq	%r13, %rax
	subq	%rdi, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L201
.L183:
	movq	168(%rbx), %rax
	testq	%rax, %rax
	je	.L191
	cmpq	$31, 8(%rax)
	ja	.L202
.L191:
	movq	160(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L203
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L192:
	movq	%rax, 8(%r13)
	movq	224(%rbx), %rax
	movl	$0, 8(%rax)
	movq	%r12, (%rax)
	movq	248(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 248(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 232(%rbx)
	movq	%rdx, 240(%rbx)
	movq	%rax, 224(%rbx)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L201:
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L184
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rdi,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L185
	cmpq	%r13, %rsi
	je	.L186
	movq	%r15, %rdi
	call	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L186:
	movq	%r15, 216(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 248(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 200(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 232(%rbx)
	addq	$512, %rax
	movq	%rax, 240(%rbx)
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L202:
	movq	(%rax), %rdx
	movq	%rdx, 168(%rbx)
	jmp	.L192
.L203:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L184:
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	160(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L204
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L188:
	movq	%rcx, %rax
	movq	216(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	248(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L189
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L189:
	movq	184(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L190
	movq	176(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L190:
	movq	%r13, 176(%rbx)
	movq	%rcx, 184(%rbx)
	jmp	.L186
.L185:
	cmpq	%r13, %rsi
	je	.L186
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L186
.L204:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L188
.L200:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10262:
	.size	_ZN2v88internal8compiler12GraphReducer7RecurseEPNS1_4NodeE, .-_ZN2v88internal8compiler12GraphReducer7RecurseEPNS1_4NodeE
	.section	.text._ZNSt5dequeIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_:
.LFB11764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	80(%rdi), %rcx
	movq	64(%rdi), %rax
	leaq	-16(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L206
	movq	(%rsi), %rcx
	movl	8(%rsi), %edx
	movq	%rcx, (%rax)
	movl	%edx, 8(%rax)
	addq	$16, 64(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	subq	72(%rdi), %rax
	movq	%r13, %r14
	sarq	$4, %rax
	subq	%rsi, %r14
	movq	%r14, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$5, %rdx
	addq	%rax, %rdx
	movq	48(%rdi), %rax
	subq	32(%rdi), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$134217727, %rax
	je	.L225
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rdi
	movq	%r13, %rax
	subq	%rdi, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L226
.L209:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L217
	cmpq	$31, 8(%rax)
	ja	.L227
.L217:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L228
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L218:
	movq	%rax, 8(%r13)
	movl	8(%r12), %edx
	movq	(%r12), %rcx
	movq	64(%rbx), %rax
	movl	%edx, 8(%rax)
	movq	%rcx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L229
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L230
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L214:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L215
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L215:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L216
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L216:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L212:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L229:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rdi,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L211
	cmpq	%r13, %rsi
	je	.L212
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L227:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L211:
	cmpq	%r13, %rsi
	je	.L212
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L228:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L218
.L230:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L214
.L225:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11764:
	.size	_ZNSt5dequeIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.section	.text._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.type	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, @function
_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_:
.LFB11768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$3, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L249
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L250
.L233:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L241
	cmpq	$63, 8(%rax)
	ja	.L251
.L241:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L252
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L242:
	movq	%rax, 8(%r13)
	movq	(%r12), %rdx
	movq	64(%rbx), %rax
	movq	%rdx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L253
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L254
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L238:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L239
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L239:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L240
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L240:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L236:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L251:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L253:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L235
	cmpq	%r13, %rsi
	je	.L236
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L235:
	cmpq	%r13, %rsi
	je	.L236
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L252:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L254:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L238
.L249:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11768:
	.size	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, .-_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.section	.text._ZN2v88internal8compiler12GraphReducer7RevisitEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12GraphReducer7RevisitEPNS1_4NodeE
	.type	_ZN2v88internal8compiler12GraphReducer7RevisitEPNS1_4NodeE, @function
_ZN2v88internal8compiler12GraphReducer7RevisitEPNS1_4NodeE:
.LFB10263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	16(%rsi), %edx
	movl	24(%rdi), %ecx
	movq	%rsi, -8(%rbp)
	cmpl	%ecx, %edx
	jb	.L255
	subl	%ecx, %edx
	cmpb	$3, %dl
	je	.L259
.L255:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	addl	$1, %ecx
	movq	%rsi, %rax
	movl	%ecx, 16(%rsi)
	movq	144(%rdi), %rsi
	movq	128(%rdi), %rcx
	leaq	-8(%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L257
	movq	%rax, (%rcx)
	addq	$8, 128(%rdi)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	leaq	-8(%rbp), %rsi
	addq	$64, %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10263:
	.size	_ZN2v88internal8compiler12GraphReducer7RevisitEPNS1_4NodeE, .-_ZN2v88internal8compiler12GraphReducer7RevisitEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12GraphReducer7ReplaceEPNS1_4NodeES4_j,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12GraphReducer7ReplaceEPNS1_4NodeES4_j
	.type	_ZN2v88internal8compiler12GraphReducer7ReplaceEPNS1_4NodeES4_j, @function
_ZN2v88internal8compiler12GraphReducer7ReplaceEPNS1_4NodeES4_j:
.LFB10258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	cmpq	8(%rax), %rsi
	je	.L310
.L261:
	cmpq	16(%rax), %r15
	je	.L311
.L262:
	movl	20(%r14), %eax
	movq	24(%r15), %rsi
	andl	$16777215, %eax
	cmpl	%ecx, %eax
	ja	.L263
	testq	%rsi, %rsi
	je	.L264
	leaq	-80(%rbp), %rax
	movq	(%rsi), %r12
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L272:
	movl	16(%rsi), %ecx
	movl	%ecx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rdi
	salq	$3, %rax
	andl	$1, %ecx
	leaq	(%rsi,%rdi,8), %r13
	je	.L265
	leaq	32(%r13,%rax), %rax
.L266:
	movq	(%rax), %rdi
	cmpq	%rdi, %r14
	je	.L267
	testq	%rdi, %rdi
	je	.L268
	movq	%rax, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rax
.L268:
	movq	%r14, (%rax)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L267:
	cmpq	%r13, %r15
	je	.L269
	movq	%r13, -80(%rbp)
	movl	24(%rbx), %ecx
	movl	16(%r13), %eax
	cmpl	%ecx, %eax
	jb	.L269
	subl	%ecx, %eax
	cmpb	$3, %al
	je	.L312
.L269:
	testq	%r12, %r12
	je	.L264
	movq	%r12, %rsi
	movq	(%r12), %r12
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L264:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
.L260:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L313
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	leaq	-80(%rbp), %r12
	testq	%rsi, %rsi
	je	.L274
	leaq	64(%rbx), %rax
	movq	(%rsi), %r13
	movq	%rax, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L284:
	movl	16(%rsi), %edi
	movl	%edi, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rdx
	salq	$3, %rax
	andl	$1, %edi
	leaq	(%rsi,%rdx,8), %rdx
	leaq	32(%rdx,%rax), %r9
	jne	.L276
	leaq	16(%rdx,%rax), %r9
	movq	(%rdx), %rdx
.L276:
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	cmpl	%eax, %ecx
	jnb	.L314
.L278:
	testq	%r13, %r13
	je	.L274
	movq	%r13, %rsi
	movq	0(%r13), %r13
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%r12, %rdi
	movq	%r15, -80(%rbp)
	call	_ZNK2v88internal8compiler4Node4Uses5emptyEv@PLT
	testb	%al, %al
	jne	.L315
.L285:
	movl	16(%r14), %eax
	movl	24(%rbx), %edx
	cmpl	%edx, %eax
	jb	.L286
	subl	%edx, %eax
	cmpb	$1, %al
	ja	.L260
	.p2align 4,,10
	.p2align 3
.L286:
	addl	$2, %edx
	leaq	160(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r14, -80(%rbp)
	movl	%edx, 16(%r14)
	movl	$0, -72(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L265:
	leaq	16(%r13,%rax), %rax
	movq	0(%r13), %r13
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L314:
	movq	(%r9), %rdi
	cmpq	%rdi, %r14
	je	.L279
	testq	%rdi, %rdi
	je	.L280
	movl	%ecx, -108(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %r9
	movl	-108(%rbp), %ecx
.L280:
	movq	%r14, (%r9)
	movq	%r14, %rdi
	movl	%ecx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %rdx
	movl	-96(%rbp), %ecx
.L279:
	cmpq	%rdx, %r15
	je	.L278
	movq	%rdx, -80(%rbp)
	movl	24(%rbx), %esi
	movl	16(%rdx), %eax
	cmpl	%esi, %eax
	jb	.L278
	subl	%esi, %eax
	cmpb	$3, %al
	jne	.L278
	addl	$1, %esi
	movl	%esi, 16(%rdx)
	movq	144(%rbx), %rax
	movq	128(%rbx), %rsi
	subq	$8, %rax
	cmpq	%rax, %rsi
	je	.L283
	movq	%rdx, (%rsi)
	addq	$8, 128(%rbx)
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L312:
	addl	$1, %ecx
	movl	%ecx, 16(%r13)
	movq	144(%rbx), %rax
	movq	128(%rbx), %rcx
	subq	$8, %rax
	cmpq	%rax, %rcx
	je	.L271
	movq	%r13, (%rcx)
	addq	$8, 128(%rbx)
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L311:
	movq	%r14, 16(%rax)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L310:
	movq	%rdx, 8(%rax)
	movq	8(%rdi), %rax
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L271:
	movq	-104(%rbp), %rsi
	leaq	64(%rbx), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L269
.L283:
	movq	-120(%rbp), %rdi
	movq	%r12, %rsi
	movl	%ecx, -88(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movl	-88(%rbp), %ecx
	jmp	.L278
.L313:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10258:
	.size	_ZN2v88internal8compiler12GraphReducer7ReplaceEPNS1_4NodeES4_j, .-_ZN2v88internal8compiler12GraphReducer7ReplaceEPNS1_4NodeES4_j
	.section	.text._ZN2v88internal8compiler12GraphReducer7ReplaceEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12GraphReducer7ReplaceEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler12GraphReducer7ReplaceEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler12GraphReducer7ReplaceEPNS1_4NodeES4_:
.LFB10257:
	.cfi_startproc
	endbr64
	movl	$-1, %ecx
	jmp	_ZN2v88internal8compiler12GraphReducer7ReplaceEPNS1_4NodeES4_j
	.cfi_endproc
.LFE10257:
	.size	_ZN2v88internal8compiler12GraphReducer7ReplaceEPNS1_4NodeES4_, .-_ZN2v88internal8compiler12GraphReducer7ReplaceEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler12GraphReducer16ReplaceWithValueEPNS1_4NodeES4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12GraphReducer16ReplaceWithValueEPNS1_4NodeES4_S4_S4_
	.type	_ZN2v88internal8compiler12GraphReducer16ReplaceWithValueEPNS1_4NodeES4_S4_S4_, @function
_ZN2v88internal8compiler12GraphReducer16ReplaceWithValueEPNS1_4NodeES4_S4_S4_:
.LFB10259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%rdx, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L399
.L318:
	cmpq	$0, -72(%rbp)
	je	.L400
.L319:
	movq	24(%rbx), %r15
	testq	%r15, %r15
	je	.L320
	leaq	-64(%rbp), %rax
	movq	(%r15), %rbx
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L352:
	movl	16(%r15), %edx
	movl	%edx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%r15,%rcx,8), %r14
	leaq	32(%r14,%rax), %r13
	jne	.L322
	leaq	16(%r14,%rax), %r13
	movq	(%r14), %r14
.L322:
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L323
	movq	(%r14), %rax
	movzwl	16(%rax), %eax
	cmpl	$6, %eax
	je	.L401
	movq	0(%r13), %rdi
	cmpl	$7, %eax
	je	.L402
	cmpq	%rdi, -72(%rbp)
	je	.L347
	testq	%rdi, %rdi
	je	.L335
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L335:
	movq	-72(%rbp), %rax
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L347
.L398:
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L347:
	movq	%r14, -64(%rbp)
	movl	24(%r12), %edx
	movl	16(%r14), %eax
	cmpl	%edx, %eax
	jb	.L325
	subl	%edx, %eax
	cmpb	$3, %al
	je	.L403
.L325:
	testq	%rbx, %rbx
	je	.L320
	movq	%rbx, %r15
	movq	(%rbx), %rbx
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L320:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L404
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE@PLT
	movq	0(%r13), %rdi
	testb	%al, %al
	je	.L339
	cmpq	%rdi, -80(%rbp)
	je	.L347
	testq	%rdi, %rdi
	je	.L342
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L342:
	movq	-80(%rbp), %rax
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	jne	.L398
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L402:
	movq	16(%r12), %r10
	cmpq	%rdi, %r10
	je	.L347
	testq	%rdi, %rdi
	je	.L329
	movq	%r15, %rsi
	movq	%r10, -96(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %r10
.L329:
	movq	%r10, 0(%r13)
	testq	%r10, %r10
	je	.L347
	movq	%r15, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L403:
	addl	$1, %edx
	movl	%edx, 16(%r14)
	movq	144(%r12), %rax
	movq	128(%r12), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L351
	movq	%r14, (%rdx)
	addq	$8, 128(%r12)
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L339:
	cmpq	%rdi, -88(%rbp)
	je	.L347
	testq	%rdi, %rdi
	je	.L348
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L348:
	movq	-88(%rbp), %rax
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	jne	.L398
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L401:
	movq	-72(%rbp), %rdx
	movl	$-1, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducer7ReplaceEPNS1_4NodeES4_j
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L400:
	movq	(%rbx), %rax
	movl	28(%rax), %eax
	testl	%eax, %eax
	jle	.L319
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, -72(%rbp)
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L399:
	movq	(%rsi), %rax
	movl	24(%rax), %edx
	testl	%edx, %edx
	jle	.L318
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, -80(%rbp)
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L351:
	movq	-104(%rbp), %rsi
	leaq	64(%r12), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L325
.L404:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10259:
	.size	_ZN2v88internal8compiler12GraphReducer16ReplaceWithValueEPNS1_4NodeES4_S4_S4_, .-_ZN2v88internal8compiler12GraphReducer16ReplaceWithValueEPNS1_4NodeES4_S4_S4_
	.section	.text._ZN2v88internal8compiler12GraphReducer9ReduceTopEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12GraphReducer9ReduceTopEv
	.type	_ZN2v88internal8compiler12GraphReducer9ReduceTopEv, @function
_ZN2v88internal8compiler12GraphReducer9ReduceTopEv:
.LFB10256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	224(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	232(%rdi), %rbx
	je	.L470
.L406:
	movq	-16(%rbx), %r13
	movzbl	23(%r13), %r8d
	leaq	32(%r13), %r14
	movq	%r14, %rdi
	andl	$15, %r8d
	cmpl	$15, %r8d
	jne	.L408
	movq	32(%r13), %rdi
	movl	8(%rdi), %r8d
	addq	$16, %rdi
.L408:
	testl	%r8d, %r8d
	jle	.L409
	cmpq	$0, (%rdi)
	je	.L471
	movl	-8(%rbx), %r9d
	movl	$0, %eax
	cmpl	%r9d, %r8d
	cmovle	%eax, %r9d
.L415:
	movslq	%r9d, %rax
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L473:
	subl	%esi, %ecx
	cmpb	$1, %cl
	jbe	.L423
.L418:
	addq	$1, %rax
	cmpl	%eax, %r8d
	jle	.L472
.L420:
	movq	(%rdi,%rax,8), %rdx
	movl	%eax, %r15d
	cmpq	%rdx, %r13
	je	.L418
	movl	16(%rdx), %ecx
	movl	24(%r12), %esi
	cmpl	%esi, %ecx
	jnb	.L473
.L423:
	addl	$2, %esi
	leaq	160(%r12), %rdi
	addl	$1, %r15d
	movl	%esi, 16(%rdx)
	leaq	-80(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movl	$0, -72(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movl	%r15d, -8(%rbx)
.L405:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L474
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	testl	%r9d, %r9d
	jle	.L416
	leal	-1(%r9), %r8d
	xorl	%eax, %eax
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L475:
	subl	%esi, %ecx
	cmpb	$1, %cl
	jbe	.L423
.L422:
	leaq	1(%rax), %rdx
	cmpq	%rax, %r8
	je	.L416
	movq	%rdx, %rax
.L424:
	movq	(%rdi,%rax,8), %rdx
	movl	%eax, %r15d
	cmpq	%rdx, %r13
	je	.L422
	movl	16(%rdx), %ecx
	movl	24(%r12), %esi
	cmpl	%esi, %ecx
	jnb	.L475
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L409:
	movl	-8(%rbx), %r9d
	cmpl	%r8d, %r9d
	jl	.L415
	.p2align 4,,10
	.p2align 3
.L416:
	movq	8(%r12), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	28(%rax), %eax
	movl	%eax, -84(%rbp)
	call	_ZN2v88internal8compiler12GraphReducer6ReduceEPNS1_4NodeE
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L476
	movl	24(%r12), %esi
	cmpq	%rax, %r13
	je	.L477
.L430:
	movq	224(%r12), %rax
	cmpq	232(%r12), %rax
	je	.L478
.L436:
	movq	-16(%rax), %rax
	addl	$3, %esi
	movl	%esi, 16(%rax)
	movq	224(%r12), %rax
	movq	232(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L437
	subq	$16, %rax
	movq	%rax, 224(%r12)
.L438:
	cmpq	%r8, %r13
	je	.L441
	movl	-84(%rbp), %ecx
	movq	%r8, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	subl	$1, %ecx
	call	_ZN2v88internal8compiler12GraphReducer7ReplaceEPNS1_4NodeES4_j
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L470:
	movq	248(%rdi), %rax
	movq	-8(%rax), %rbx
	addq	$512, %rbx
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L471:
	movl	24(%r12), %eax
	addl	$3, %eax
	movl	%eax, 16(%r13)
	movq	224(%r12), %rax
	movq	232(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L427
.L467:
	subq	$16, %rax
	movq	%rax, 224(%r12)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L477:
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L432
	movq	32(%r13), %r14
	movl	8(%r14), %eax
	addq	$16, %r14
.L432:
	testl	%eax, %eax
	jle	.L430
	leal	-1(%rax), %r9d
	xorl	%edx, %edx
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L479:
	subl	%esi, %edi
	cmpb	$1, %dil
	jbe	.L434
.L433:
	leaq	1(%rdx), %rcx
	cmpq	%rdx, %r9
	je	.L430
	movq	%rcx, %rdx
.L435:
	movq	(%r14,%rdx,8), %rcx
	movl	%edx, %r15d
	cmpq	%rcx, %r13
	je	.L433
	movl	16(%rcx), %edi
	cmpl	%edi, %esi
	jbe	.L479
.L434:
	addl	$2, %esi
	leaq	160(%r12), %rdi
	movl	%esi, 16(%rcx)
	leaq	-80(%rbp), %rsi
	movq	%rcx, -80(%rbp)
	movl	$0, -72(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	leal	1(%r15), %eax
	movl	%eax, -8(%rbx)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L441:
	movq	24(%r13), %rbx
	leaq	-80(%rbp), %r14
	testq	%rbx, %rbx
	jne	.L442
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L444:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L405
.L442:
	movl	16(%rbx), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rax
	jne	.L443
	movq	(%rax), %rax
.L443:
	cmpq	%rax, %r13
	je	.L444
	movq	%rax, -80(%rbp)
	movl	24(%r12), %ecx
	movl	16(%rax), %edx
	cmpl	%ecx, %edx
	jb	.L444
	subl	%ecx, %edx
	cmpb	$3, %dl
	jne	.L444
	addl	$1, %ecx
	movl	%ecx, 16(%rax)
	movq	144(%r12), %rdi
	movq	128(%r12), %rcx
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rcx
	je	.L446
	movq	%rax, (%rcx)
	addq	$8, 128(%r12)
	jmp	.L444
.L478:
	movq	248(%r12), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L436
.L437:
	movq	168(%r12), %rax
	testq	%rax, %rax
	je	.L439
	cmpq	$32, 8(%rax)
	ja	.L440
.L439:
	movq	$32, 8(%rdx)
	movq	168(%r12), %rax
	movq	%rax, (%rdx)
	movq	%rdx, 168(%r12)
.L440:
	movq	248(%r12), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 248(%r12)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 232(%r12)
	addq	$496, %rax
	movq	%rdx, 240(%r12)
	movq	%rax, 224(%r12)
	jmp	.L438
.L476:
	movq	224(%r12), %rax
	cmpq	232(%r12), %rax
	je	.L480
.L426:
	movq	-16(%rax), %rdx
	movl	24(%r12), %eax
	addl	$3, %eax
	movl	%eax, 16(%rdx)
	movq	224(%r12), %rax
	movq	232(%r12), %rdx
	cmpq	%rdx, %rax
	jne	.L467
.L427:
	movq	168(%r12), %rax
	testq	%rax, %rax
	je	.L428
	cmpq	$32, 8(%rax)
	ja	.L429
.L428:
	movq	$32, 8(%rdx)
	movq	168(%r12), %rax
	movq	%rax, (%rdx)
	movq	%rdx, 168(%r12)
.L429:
	movq	248(%r12), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 248(%r12)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 232(%r12)
	addq	$496, %rax
	movq	%rdx, 240(%r12)
	movq	%rax, 224(%r12)
	jmp	.L405
.L446:
	leaq	64(%r12), %rdi
	movq	%r14, %rsi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L444
.L480:
	movq	248(%r12), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L426
.L474:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10256:
	.size	_ZN2v88internal8compiler12GraphReducer9ReduceTopEv, .-_ZN2v88internal8compiler12GraphReducer9ReduceTopEv
	.section	.text._ZN2v88internal8compiler12GraphReducer10ReduceNodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12GraphReducer10ReduceNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler12GraphReducer10ReduceNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler12GraphReducer10ReduceNodeEPNS1_4NodeE:
.LFB10253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN2v88internal8compiler7Reducer8FinalizeEv(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	24(%rdi), %eax
	movq	%rsi, -80(%rbp)
	movl	$0, -72(%rbp)
	addl	$2, %eax
	movl	%eax, 16(%rsi)
	leaq	160(%rdi), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.p2align 4,,10
	.p2align 3
.L482:
	movq	192(%rbx), %rax
	cmpq	%rax, 224(%rbx)
	je	.L483
.L501:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12GraphReducer9ReduceTopEv
	movq	192(%rbx), %rax
	cmpq	%rax, 224(%rbx)
	jne	.L501
.L483:
	movq	96(%rbx), %rax
	cmpq	%rax, 128(%rbx)
	je	.L485
	movq	112(%rbx), %rsi
	movq	(%rax), %rcx
	leaq	-8(%rsi), %rdx
	cmpq	%rdx, %rax
	je	.L486
	addq	$8, %rax
	movq	%rax, 96(%rbx)
.L487:
	movl	16(%rcx), %eax
	movl	24(%rbx), %edx
	cmpl	%eax, %edx
	ja	.L482
	subl	%edx, %eax
	cmpb	$1, %al
	jne	.L482
	addl	$2, %edx
	movq	-88(%rbp), %rdi
	movq	%r15, %rsi
	movl	%edx, 16(%rcx)
	movq	%rcx, -80(%rbp)
	movl	$0, -72(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L485:
	movq	40(%rbx), %r12
	movq	48(%rbx), %r14
	cmpq	%r12, %r14
	jne	.L493
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L492:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L502
.L493:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%r13, %rax
	je	.L492
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L493
.L502:
	movq	96(%rbx), %rax
	cmpq	%rax, 128(%rbx)
	jne	.L482
	.p2align 4,,10
	.p2align 3
.L481:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L503
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L486:
	.cfi_restore_state
	movq	72(%rbx), %rax
	testq	%rax, %rax
	je	.L488
	cmpq	$64, 8(%rax)
	ja	.L489
.L488:
	movq	104(%rbx), %rax
	movq	$64, 8(%rax)
	movq	72(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 72(%rbx)
.L489:
	movq	120(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 120(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 104(%rbx)
	movq	%rdx, 112(%rbx)
	movq	%rax, 96(%rbx)
	jmp	.L487
.L503:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10253:
	.size	_ZN2v88internal8compiler12GraphReducer10ReduceNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler12GraphReducer10ReduceNodeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12GraphReducer11ReduceGraphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv
	.type	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv, @function
_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv:
.LFB10254:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rax), %rsi
	jmp	_ZN2v88internal8compiler12GraphReducer10ReduceNodeEPNS1_4NodeE
	.cfi_endproc
.LFE10254:
	.size	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv, .-_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv
	.section	.text._ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB12007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$6, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L519
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L507:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L508
	movq	%r15, %rbx
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L509:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L520
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L510:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L508
.L513:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L509
	cmpq	$63, 8(%rax)
	jbe	.L509
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L513
.L508:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$63, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,8), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L519:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L507
	.cfi_endproc
.LFE12007:
	.size	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZNSt11_Deque_baseIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB12023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$5, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L535
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L523:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L524
	movq	%r15, %rbx
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L525:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L536
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L526:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L524
.L529:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L525
	cmpq	$31, 8(%rax)
	jbe	.L525
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L529
.L524:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$31, %r13d
	salq	$4, %r13
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	addq	%rax, %r13
	leaq	512(%rax), %rcx
	movq	%rax, 72(%r12)
	movq	%r13, 64(%r12)
	movq	%rcx, 80(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L535:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L523
	.cfi_endproc
.LFE12023:
	.size	_ZNSt11_Deque_baseIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZN2v88internal8compiler12GraphReducerC2EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12GraphReducerC2EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE
	.type	_ZN2v88internal8compiler12GraphReducerC2EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE, @function
_ZN2v88internal8compiler12GraphReducerC2EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE:
.LFB10246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	leaq	-256(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$24, %rdi
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal8compiler12GraphReducerE(%rip), %rax
	movq	%rdx, -16(%rdi)
	movl	$4, %edx
	movq	%rax, -24(%rdi)
	movq	%r8, -8(%rdi)
	call	_ZN2v88internal8compiler14NodeMarkerBaseC2EPNS1_5GraphEj@PLT
	movq	%r14, 32(%rbx)
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	$0, 40(%rbx)
	movq	%r15, %rdi
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	%r14, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	je	.L572
	movdqa	-256(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	leaq	-160(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	-216(%rbp), %r9
	movdqa	-96(%rbp), %xmm5
	movq	-248(%rbp), %rax
	movq	-200(%rbp), %r8
	movq	-224(%rbp), %xmm3
	movq	-184(%rbp), %rcx
	movaps	%xmm6, -224(%rbp)
	movq	-168(%rbp), %rdx
	movdqa	-80(%rbp), %xmm6
	movq	-256(%rbp), %xmm4
	movq	-240(%rbp), %r11
	movq	-208(%rbp), %xmm2
	movaps	%xmm7, -208(%rbp)
	movq	-192(%rbp), %xmm1
	movq	%rax, %xmm7
	movq	-232(%rbp), %r10
	movaps	%xmm5, -192(%rbp)
	movq	%r9, %xmm5
	movq	-176(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm4
	punpcklqdq	%xmm5, %xmm3
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	%rcx, %xmm7
	movaps	%xmm6, -176(%rbp)
	movq	%rdx, %xmm5
	movq	%r8, %xmm6
	punpcklqdq	%xmm6, %xmm2
	punpcklqdq	%xmm7, %xmm1
	punpcklqdq	%xmm5, %xmm0
	movq	%r11, 80(%rbx)
	movq	%rsi, -240(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%r10, 88(%rbx)
	movups	%xmm4, 64(%rbx)
	movups	%xmm3, 96(%rbx)
	movups	%xmm2, 112(%rbx)
	movups	%xmm1, 128(%rbx)
	movups	%xmm0, 144(%rbx)
	testq	%rsi, %rsi
	je	.L539
	movq	-168(%rbp), %rcx
	movq	-200(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L540
	.p2align 4,,10
	.p2align 3
.L543:
	testq	%rax, %rax
	je	.L541
	cmpq	$64, 8(%rax)
	ja	.L542
.L541:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -248(%rbp)
.L542:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L543
	movq	-232(%rbp), %rdi
	movq	-240(%rbp), %rsi
.L540:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L539
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L539:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r14, -256(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	pxor	%xmm0, %xmm0
	je	.L573
	movdqa	-256(%rbp), %xmm6
	leaq	-160(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler12GraphReducer9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-96(%rbp), %xmm6
	movq	-216(%rbp), %r9
	movq	-224(%rbp), %xmm3
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %xmm1
	movq	-184(%rbp), %rcx
	movaps	%xmm6, -192(%rbp)
	movq	%r9, %xmm6
	punpcklqdq	%xmm6, %xmm3
	movdqa	-128(%rbp), %xmm7
	movq	-208(%rbp), %xmm2
	movups	%xmm3, 192(%rbx)
	movq	%r8, %xmm3
	movq	-168(%rbp), %rdx
	movdqa	-112(%rbp), %xmm5
	punpcklqdq	%xmm3, %xmm2
	movq	-248(%rbp), %rax
	movq	-176(%rbp), %xmm0
	movaps	%xmm7, -224(%rbp)
	movups	%xmm2, 208(%rbx)
	movq	%rcx, %xmm2
	movq	-256(%rbp), %xmm4
	movq	-240(%rbp), %r11
	punpcklqdq	%xmm2, %xmm1
	movq	-232(%rbp), %r10
	movdqa	-80(%rbp), %xmm7
	movaps	%xmm5, -208(%rbp)
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	%rax, %xmm5
	movups	%xmm1, 224(%rbx)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm5, %xmm4
	movq	%r11, 176(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -240(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%r10, 184(%rbx)
	movaps	%xmm7, -176(%rbp)
	movups	%xmm4, 160(%rbx)
	movups	%xmm0, 240(%rbx)
	testq	%rsi, %rsi
	je	.L546
	movq	-168(%rbp), %rcx
	movq	-200(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L547
	.p2align 4,,10
	.p2align 3
.L550:
	testq	%rax, %rax
	je	.L548
	cmpq	$32, 8(%rax)
	ja	.L549
.L548:
	movq	(%rdx), %rax
	movq	$32, 8(%rax)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -248(%rbp)
.L549:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L550
	movq	-232(%rbp), %rdi
	movq	-240(%rbp), %rsi
.L547:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L546
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L546:
	movq	%r13, 256(%rbx)
	testq	%r12, %r12
	je	.L537
	movq	16(%rbx), %rax
	movq	$1, 8(%rax)
.L537:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L574
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	.cfi_restore_state
	movdqa	-256(%rbp), %xmm2
	movq	-232(%rbp), %rax
	movq	$0, 176(%rbx)
	movdqa	-224(%rbp), %xmm3
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm6
	movq	%rax, 184(%rbx)
	movups	%xmm2, 160(%rbx)
	movups	%xmm3, 192(%rbx)
	movups	%xmm7, 208(%rbx)
	movups	%xmm5, 224(%rbx)
	movups	%xmm6, 240(%rbx)
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L572:
	movdqa	-256(%rbp), %xmm1
	movq	-232(%rbp), %rax
	movq	$0, 80(%rbx)
	movdqa	-224(%rbp), %xmm2
	movdqa	-208(%rbp), %xmm3
	movdqa	-192(%rbp), %xmm7
	movups	%xmm1, 64(%rbx)
	movdqa	-176(%rbp), %xmm1
	movq	%rax, 88(%rbx)
	movups	%xmm2, 96(%rbx)
	movups	%xmm3, 112(%rbx)
	movups	%xmm7, 128(%rbx)
	movups	%xmm1, 144(%rbx)
	jmp	.L539
.L574:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10246:
	.size	_ZN2v88internal8compiler12GraphReducerC2EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE, .-_ZN2v88internal8compiler12GraphReducerC2EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE
	.globl	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE
	.set	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE,_ZN2v88internal8compiler12GraphReducerC2EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler7Reducer8FinalizeEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler7Reducer8FinalizeEv, @function
_GLOBAL__sub_I__ZN2v88internal8compiler7Reducer8FinalizeEv:
.LFB12258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12258:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler7Reducer8FinalizeEv, .-_GLOBAL__sub_I__ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler7Reducer8FinalizeEv
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.weak	_ZTVN2v88internal8compiler7ReducerE
	.section	.data.rel.ro._ZTVN2v88internal8compiler7ReducerE,"awG",@progbits,_ZTVN2v88internal8compiler7ReducerE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler7ReducerE, @object
	.size	_ZTVN2v88internal8compiler7ReducerE, 56
_ZTVN2v88internal8compiler7ReducerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.weak	_ZTVN2v88internal8compiler12GraphReducerE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler12GraphReducerE,"awG",@progbits,_ZTVN2v88internal8compiler12GraphReducerE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler12GraphReducerE, @object
	.size	_ZTVN2v88internal8compiler12GraphReducerE, 56
_ZTVN2v88internal8compiler12GraphReducerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler12GraphReducerD1Ev
	.quad	_ZN2v88internal8compiler12GraphReducerD0Ev
	.quad	_ZN2v88internal8compiler12GraphReducer7ReplaceEPNS1_4NodeES4_
	.quad	_ZN2v88internal8compiler12GraphReducer7RevisitEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler12GraphReducer16ReplaceWithValueEPNS1_4NodeES4_S4_S4_
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
