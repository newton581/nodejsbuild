	.file	"js-native-context-specialization.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1549:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1549:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.rodata._ZNK2v88internal8compiler29JSNativeContextSpecialization12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"JSNativeContextSpecialization"
	.section	.text._ZNK2v88internal8compiler29JSNativeContextSpecialization12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler29JSNativeContextSpecialization12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler29JSNativeContextSpecialization12reducer_nameEv
	.type	_ZNK2v88internal8compiler29JSNativeContextSpecialization12reducer_nameEv, @function
_ZNK2v88internal8compiler29JSNativeContextSpecialization12reducer_nameEv:
.LFB10412:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10412:
	.size	_ZNK2v88internal8compiler29JSNativeContextSpecialization12reducer_nameEv, .-_ZNK2v88internal8compiler29JSNativeContextSpecialization12reducer_nameEv
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB18942:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE18942:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecializationD2Ev,"axG",@progbits,_ZN2v88internal8compiler29JSNativeContextSpecializationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler29JSNativeContextSpecializationD2Ev
	.type	_ZN2v88internal8compiler29JSNativeContextSpecializationD2Ev, @function
_ZN2v88internal8compiler29JSNativeContextSpecializationD2Ev:
.LFB28372:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28372:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecializationD2Ev, .-_ZN2v88internal8compiler29JSNativeContextSpecializationD2Ev
	.weak	_ZN2v88internal8compiler29JSNativeContextSpecializationD1Ev
	.set	_ZN2v88internal8compiler29JSNativeContextSpecializationD1Ev,_ZN2v88internal8compiler29JSNativeContextSpecializationD2Ev
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB18935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18935:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB18936:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE18936:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB18944:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE18944:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecializationD0Ev,"axG",@progbits,_ZN2v88internal8compiler29JSNativeContextSpecializationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler29JSNativeContextSpecializationD0Ev
	.type	_ZN2v88internal8compiler29JSNativeContextSpecializationD0Ev, @function
_ZN2v88internal8compiler29JSNativeContextSpecializationD0Ev:
.LFB28374:
	.cfi_startproc
	endbr64
	movl	$88, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28374:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecializationD0Ev, .-_ZN2v88internal8compiler29JSNativeContextSpecializationD0Ev
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceSoftDeoptimizeEPNS1_4NodeENS0_16DeoptimizeReasonE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceSoftDeoptimizeEPNS1_4NodeENS0_16DeoptimizeReasonE.part.0, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceSoftDeoptimizeEPNS1_4NodeENS0_16DeoptimizeReasonE.part.0:
.LFB29317:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	16(%rbx), %r15
	movq	%rax, %r13
	movq	352(%r15), %rsi
	testq	%rsi, %rsi
	je	.L15
.L12:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties20FindFrameStateBeforeEPNS1_4NodeES4_@PLT
	leaq	-96(%rbp), %rcx
	movl	%r14d, %edx
	movl	$1, %esi
	movq	%rax, -112(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10DeoptimizeENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	leaq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r13, -64(%rbp)
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rdx
	movq	16(%rbx), %rax
	movq	8(%rax), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L16
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	(%r15), %r9
	movq	8(%r15), %rdi
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	-112(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r15)
	movq	%rax, %rsi
	jmp	.L12
.L16:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29317:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceSoftDeoptimizeEPNS1_4NodeENS0_16DeoptimizeReasonE.part.0, .-_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceSoftDeoptimizeEPNS1_4NodeENS0_16DeoptimizeReasonE.part.0
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_116IsStringConstantEPNS1_12JSHeapBrokerEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"IsHeapObject()"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_116IsStringConstantEPNS1_12JSHeapBrokerEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_116IsStringConstantEPNS1_12JSHeapBrokerEPNS1_4NodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_116IsStringConstantEPNS1_12JSHeapBrokerEPNS1_4NodeE:
.LFB23103:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdx), %eax
	cmpw	$284, %ax
	je	.L22
	cmpw	$30, %ax
	je	.L25
	xorl	%eax, %eax
.L17:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L26
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	leaq	-48(%rbp), %r12
	movq	48(%rdx), %rdx
	xorl	%ecx, %ecx
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L27
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$1, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L26:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23103:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_116IsStringConstantEPNS1_12JSHeapBrokerEPNS1_4NodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_116IsStringConstantEPNS1_12JSHeapBrokerEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_121GetTypedArrayConstantEPNS1_12JSHeapBrokerEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_121GetTypedArrayConstantEPNS1_12JSHeapBrokerEPNS1_4NodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_121GetTypedArrayConstantEPNS1_12JSHeapBrokerEPNS1_4NodeE:
.LFB23211:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	cmpw	$30, 16(%rax)
	jne	.L29
	leaq	-64(%rbp), %r13
	movq	48(%rax), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L36
	movdqa	-64(%rbp), %xmm0
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef14IsJSTypedArrayEv@PLT
	testb	%al, %al
	jne	.L31
.L29:
	movb	$0, (%r12)
	movb	$0, 8(%r12)
.L28:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef14AsJSTypedArrayEv@PLT
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal8compiler15JSTypedArrayRef10is_on_heapEv@PLT
	testb	%al, %al
	jne	.L29
	movdqa	-64(%rbp), %xmm1
	movb	$1, (%r12)
	movups	%xmm1, 8(%r12)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23211:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_121GetTypedArrayConstantEPNS1_12JSHeapBrokerEPNS1_4NodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_121GetTypedArrayConstantEPNS1_12JSHeapBrokerEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler15PropertyCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC4:
	.string	"IsPropertyCell()"
	.section	.text._ZN2v88internal8compiler15PropertyCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler15PropertyCellRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler15PropertyCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler15PropertyCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler15PropertyCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L41
.L38:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef14IsPropertyCellEv@PLT
	testb	%al, %al
	jne	.L38
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9594:
	.size	_ZN2v88internal8compiler15PropertyCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler15PropertyCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler15PropertyCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler15PropertyCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler15PropertyCellRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC5:
	.string	"IsJSReceiver()"
	.section	.text._ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler13JSReceiverRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L45
.L42:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSReceiverEv@PLT
	testb	%al, %al
	jne	.L42
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9600:
	.size	_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler13JSReceiverRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler13JSReceiverRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC6:
	.string	"IsJSObject()"
	.section	.text._ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler11JSObjectRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	testb	%bl, %bl
	jne	.L49
.L46:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10IsJSObjectEv@PLT
	testb	%al, %al
	jne	.L46
	leaq	.LC6(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9606:
	.size	_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC7:
	.string	"IsName()"
	.section	.text._ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler7NameRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L53
.L50:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6IsNameEv@PLT
	testb	%al, %al
	jne	.L50
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9654:
	.size	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC8:
	.string	"IsMap()"
	.section	.text._ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler6MapRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L57
.L54:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef5IsMapEv@PLT
	testb	%al, %al
	jne	.L54
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9708:
	.size	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC9:
	.string	"IsSharedFunctionInfo()"
	.section	.text._ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler21SharedFunctionInfoRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L61
.L58:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef20IsSharedFunctionInfoEv@PLT
	testb	%al, %al
	jne	.L58
	leaq	.LC9(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9759:
	.size	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.text._ZN2v88internal8compiler4Node12ReplaceInputEiPS2_,"axG",@progbits,_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_
	.type	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_, @function
_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_:
.LFB10029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rax
	salq	$3, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movzbl	23(%rdi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L63
	leaq	32(%rdi), %rbx
	addq	%rax, %rbx
	movq	(%rbx), %r8
	cmpq	%r8, %r12
	je	.L62
.L64:
	notl	%esi
	movslq	%esi, %rsi
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%rdi,%rax,8), %r13
	testq	%r8, %r8
	je	.L67
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L67:
	movq	%r12, (%rbx)
	testq	%r12, %r12
	je	.L62
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movq	32(%rdi), %rdi
	leaq	16(%rdi,%rax), %rbx
	movq	(%rbx), %r8
	cmpq	%r8, %r12
	jne	.L64
.L62:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10029:
	.size	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_, .-_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_
	.section	.rodata._ZN2v88internal8compiler29JSNativeContextSpecializationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_.str1.1,"aMS",@progbits,1
.LC10:
	.string	"storage_.is_populated_"
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecializationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecializationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_
	.type	_ZN2v88internal8compiler29JSNativeContextSpecializationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_, @function
_ZN2v88internal8compiler29JSNativeContextSpecializationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_:
.LFB23097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	24(%rbp), %r14
	movq	%r9, -112(%rbp)
	movq	%rax, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, 8(%rdi)
	leaq	16+_ZTVN2v88internal8compiler29JSNativeContextSpecializationE(%rip), %rax
	cmpb	$0, 24(%rcx)
	movq	%rax, (%rdi)
	movq	%rdx, 16(%rdi)
	movq	%rcx, 24(%rdi)
	movl	%r8d, 32(%rdi)
	je	.L77
	movdqu	32(%rcx), %xmm1
	leaq	-80(%rbp), %r15
	leaq	-96(%rbp), %r13
	movq	%rcx, %r12
	movq	%r15, %rdi
	movaps	%xmm1, -80(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef13global_objectEv@PLT
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler17JSGlobalObjectRef6objectEv@PLT
	cmpb	$0, 24(%r12)
	movq	%rax, 40(%rbx)
	je	.L77
	movq	-112(%rbp), %xmm0
	movdqu	32(%r12), %xmm2
	movq	%r15, %rdi
	movhps	-120(%rbp), %xmm0
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef19global_proxy_objectEv@PLT
	movq	%r13, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal8compiler16JSGlobalProxyRef6objectEv@PLT
	movdqa	-112(%rbp), %xmm0
	movq	%r14, 72(%rbx)
	movq	%rax, 48(%rbx)
	movups	%xmm0, 56(%rbx)
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	movq	%rax, 80(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	leaq	.LC10(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23097:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecializationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_, .-_ZN2v88internal8compiler29JSNativeContextSpecializationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecializationC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_
	.set	_ZN2v88internal8compiler29JSNativeContextSpecializationC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_,_ZN2v88internal8compiler29JSNativeContextSpecializationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization18GetMaxStringLengthEPNS1_12JSHeapBrokerEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization18GetMaxStringLengthEPNS1_12JSHeapBrokerEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization18GetMaxStringLengthEPNS1_12JSHeapBrokerEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization18GetMaxStringLengthEPNS1_12JSHeapBrokerEPNS1_4NodeE:
.LFB23100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdi), %edx
	cmpw	$284, %dx
	je	.L94
	cmpw	$30, %dx
	je	.L95
.L84:
	xorl	%eax, %eax
	movl	$18, %ecx
	cmpw	$28, %dx
	movl	$0, %edx
	cmove	%rcx, %rdx
	sete	%al
.L83:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L96
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movq	48(%rdi), %r14
	leaq	-96(%rbp), %r13
	xorl	%ecx, %ecx
	movq	%rsi, %rbx
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L87
	movdqa	-96(%rbp), %xmm0
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	testb	%al, %al
	jne	.L97
	movq	(%rbx), %rax
	movzwl	16(%rax), %edx
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L94:
	call	_ZN2v88internal8compiler20StringConstantBaseOfEPKNS1_8OperatorE@PLT
	movq	%rax, %rdi
	call	_ZNK2v88internal18StringConstantBase26GetMaxStringConstantLengthEv@PLT
	movq	%rax, %rdx
	movl	$1, %eax
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L97:
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L87
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8AsStringEv@PLT
	movq	%r13, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal8compiler9StringRef6lengthEv@PLT
	movslq	%eax, %rdx
	movl	$1, %eax
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23100:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization18GetMaxStringLengthEPNS1_12JSHeapBrokerEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization18GetMaxStringLengthEPNS1_12JSHeapBrokerEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization16ReduceJSToStringEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization16ReduceJSToStringEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization16ReduceJSToStringEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization16ReduceJSToStringEPNS1_4NodeE:
.LFB23101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	32(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L99
	movq	16(%rbx), %rbx
.L99:
	movq	(%rbx), %rax
	cmpw	$30, 16(%rax)
	jne	.L100
	leaq	-64(%rbp), %r14
	movq	24(%r13), %rsi
	movq	48(%rax), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L112
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	testb	%al, %al
	jne	.L111
.L100:
	movq	(%rbx), %rdx
	xorl	%eax, %eax
	cmpw	$28, 16(%rdx)
	jne	.L102
	movq	72(%r13), %rdi
	movsd	48(%rdx), %xmm0
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$23, %rax
	jbe	.L113
	leaq	24(%rsi), %rax
	movq	%rax, 16(%rdi)
.L105:
	movsd	%xmm0, 16(%rsi)
	movl	$1, (%rsi)
	movq	$0, 8(%rsi)
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder21DelayedStringConstantEPKNS0_18StringConstantBaseE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rbx
.L111:
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rax
.L102:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L114
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L113:
	movl	$24, %esi
	movsd	%xmm0, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movsd	-72(%rbp), %xmm0
	movq	%rax, %rsi
	jmp	.L105
.L114:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23101:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization16ReduceJSToStringEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization16ReduceJSToStringEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization27CreateDelayedStringConstantEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization27CreateDelayedStringConstantEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization27CreateDelayedStringConstantEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization27CreateDelayedStringConstantEPNS1_4NodeE:
.LFB23102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdi), %eax
	cmpw	$284, %ax
	je	.L135
	cmpw	$28, %ax
	jne	.L118
	movsd	48(%rdi), %xmm0
	movq	72(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L136
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L120:
	movl	$1, (%rax)
	movq	$0, 8(%rax)
	movsd	%xmm0, 16(%rax)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L118:
	cmpw	$30, %ax
	jne	.L122
	movq	48(%rdi), %r14
	leaq	-80(%rbp), %r13
	movq	24(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L123
	movdqa	-80(%rbp), %xmm1
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	movaps	%xmm1, -64(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	testb	%al, %al
	je	.L122
	movq	24(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L123
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8AsStringEv@PLT
	movq	%r13, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal8compiler9StringRef6objectEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZNK2v88internal8compiler9StringRef6lengthEv@PLT
	movq	72(%rbx), %rdi
	movslq	%eax, %r13
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L137
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L125:
	movl	$0, (%rax)
	movq	$0, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%r13, 24(%rax)
.L115:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L138
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20StringConstantBaseOfEPKNS1_8OperatorE@PLT
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L123:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L122:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L137:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L136:
	movl	$24, %esi
	movsd	%xmm0, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movsd	-88(%rbp), %xmm0
	jmp	.L120
.L138:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23102:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization27CreateDelayedStringConstantEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization27CreateDelayedStringConstantEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceJSAsyncFunctionEnterEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceJSAsyncFunctionEnterEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceJSAsyncFunctionEnterEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceJSAsyncFunctionEnterEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceJSAsyncFunctionEnterEPNS1_4NodeE:
.LFB23104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	56(%rbx), %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies28DependOnPromiseHookProtectorEv@PLT
	testb	%al, %al
	jne	.L140
	xorl	%eax, %eax
.L141:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L149
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movq	16(%rbx), %rax
	leaq	-112(%rbp), %r14
	movq	-160(%rbp), %xmm0
	movq	(%rax), %r9
	movq	368(%rax), %rdi
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%r9, -192(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder13CreatePromiseEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-192(%rbp), %r9
	movdqa	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r9, %rdi
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%r15), %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler16FrameStateInfoOfEPKNS1_8OperatorE@PLT
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L142
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L142
	movq	24(%rbx), %rsi
	movq	%r13, %xmm0
	movl	$1, %ecx
	movq	-192(%rbp), %xmm3
	leaq	-144(%rbp), %r15
	movhps	-208(%rbp), %xmm0
	movdqa	%xmm3, %xmm2
	movdqa	%xmm3, %xmm1
	movq	%r15, %rdi
	movaps	%xmm0, -208(%rbp)
	movhps	-160(%rbp), %xmm2
	movhps	-168(%rbp), %xmm1
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm1, -160(%rbp)
	call	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef31internal_formal_parameter_countEv@PLT
	movq	%r15, %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	leaq	-128(%rbp), %rdi
	movq	%rdx, -120(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeArrayRef14register_countEv@PLT
	movq	16(%rbx), %rdx
	leal	0(%r13,%rax), %esi
	movq	368(%rdx), %rdi
	movq	(%rdx), %r15
	call	_ZN2v88internal8compiler17JSOperatorBuilder25CreateAsyncFunctionObjectEi@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$6, %edx
	movdqa	-208(%rbp), %xmm0
	movdqa	-192(%rbp), %xmm2
	movq	%rax, %rsi
	movq	%r15, %rdi
	movdqa	-160(%rbp), %xmm1
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	-168(%rbp), %r8
	movq	%r12, %rsi
	movq	%rax, %r13
	movq	(%rdi), %rax
	movq	%r13, %rcx
	movq	%r13, %rdx
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	.LC11(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23104:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceJSAsyncFunctionEnterEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceJSAsyncFunctionEnterEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSAsyncFunctionRejectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSAsyncFunctionRejectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSAsyncFunctionRejectEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSAsyncFunctionRejectEPNS1_4NodeE:
.LFB23105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	56(%rbx), %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler23CompilationDependencies28DependOnPromiseHookProtectorEv@PLT
	testb	%al, %al
	jne	.L151
	xorl	%eax, %eax
.L152:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L155
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movq	16(%rbx), %rax
	leaq	-176(%rbp), %rsi
	movq	-184(%rbp), %xmm0
	movq	%rsi, %rdi
	movq	%rsi, -184(%rbp)
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%r9, -208(%rbp)
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder31ForJSAsyncFunctionObjectPromiseEv@PLT
	movq	-200(%rbp), %r8
	movq	-184(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	leaq	-112(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r13, -96(%rbp)
	movq	-208(%rbp), %r9
	movq	%r10, %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	movdqa	-224(%rbp), %xmm0
	movq	%r10, -224(%rbp)
	movq	%r9, %rdi
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	subq	$8, %rsp
	movq	16(%rbx), %rdi
	movq	%r15, %r9
	pushq	$1
	leaq	-120(%rbp), %rcx
	movl	$1, %r8d
	movq	%r14, %rdx
	movl	$229, %esi
	movq	%rax, -184(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler39CreateStubBuiltinContinuationFrameStateEPNS1_7JSGraphENS0_8Builtins4NameEPNS1_4NodeEPKS7_iS7_NS1_26ContinuationFrameStateModeE@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	%rax, -208(%rbp)
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler17JSOperatorBuilder13RejectPromiseEv@PLT
	movq	%r14, %xmm3
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	-184(%rbp), %xmm1
	movq	-224(%rbp), %r10
	movq	%rax, %rsi
	movq	%r13, -64(%rbp)
	movl	$7, %edx
	movdqa	%xmm1, %xmm0
	movq	%r10, %rcx
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-208(%rbp), %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-200(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%r13, %r8
	movq	%r12, %rsi
	movq	%rax, %rcx
	movq	-184(%rbp), %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	popq	%rdx
	movq	-184(%rbp), %rax
	popq	%rcx
	jmp	.L152
.L155:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23105:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSAsyncFunctionRejectEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSAsyncFunctionRejectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization28ReduceJSAsyncFunctionResolveEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization28ReduceJSAsyncFunctionResolveEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization28ReduceJSAsyncFunctionResolveEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization28ReduceJSAsyncFunctionResolveEPNS1_4NodeE:
.LFB23106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	56(%rbx), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler23CompilationDependencies28DependOnPromiseHookProtectorEv@PLT
	testb	%al, %al
	jne	.L157
	xorl	%eax, %eax
.L158:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L161
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movq	16(%rbx), %rax
	leaq	-176(%rbp), %rsi
	movq	-184(%rbp), %xmm0
	movq	%rsi, %rdi
	movq	%rsi, -184(%rbp)
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%r9, -208(%rbp)
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder31ForJSAsyncFunctionObjectPromiseEv@PLT
	movq	-200(%rbp), %r8
	movq	-184(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	leaq	-112(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r14, -96(%rbp)
	movq	-208(%rbp), %r9
	movq	%r10, %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	movdqa	-224(%rbp), %xmm0
	movq	%r10, -224(%rbp)
	movq	%r9, %rdi
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	subq	$8, %rsp
	movq	16(%rbx), %rdi
	movq	%r15, %r9
	pushq	$1
	leaq	-120(%rbp), %rcx
	movl	$1, %r8d
	movq	%r13, %rdx
	movl	$229, %esi
	movq	%rax, -184(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler39CreateStubBuiltinContinuationFrameStateEPNS1_7JSGraphENS0_8Builtins4NameEPNS1_4NodeEPKS7_iS7_NS1_26ContinuationFrameStateModeE@PLT
	movq	%rax, -200(%rbp)
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler17JSOperatorBuilder14ResolvePromiseEv@PLT
	movq	%r14, %xmm3
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	-224(%rbp), %r10
	movq	%rax, %rsi
	movl	$6, %edx
	movq	-184(%rbp), %xmm1
	movdqa	%xmm1, %xmm0
	movq	%r10, %rcx
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%r13, %xmm0
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm1, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%r14, %r8
	movq	%r12, %rsi
	movq	%rax, %rcx
	movq	-184(%rbp), %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	popq	%rdx
	movq	-184(%rbp), %rax
	popq	%rcx
	jmp	.L158
.L161:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23106:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization28ReduceJSAsyncFunctionResolveEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization28ReduceJSAsyncFunctionResolveEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization11ReduceJSAddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization11ReduceJSAddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization11ReduceJSAddEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization11ReduceJSAddEPNS1_4NodeE:
.LFB23107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L163
	leaq	40(%rsi), %rax
.L164:
	movq	(%rax), %rax
	movq	0(%r13), %rdi
	movq	%rax, -104(%rbp)
	movzwl	16(%rdi), %eax
	cmpw	$284, %ax
	je	.L188
	movq	24(%rbx), %r15
	cmpw	$30, %ax
	jne	.L167
	movq	48(%rdi), %rax
	leaq	-96(%rbp), %r14
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L170
	movdqa	-96(%rbp), %xmm0
	leaq	-80(%rbp), %rdi
	movq	%rdi, -120(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	movq	-120(%rbp), %rdi
	testb	%al, %al
	jne	.L169
	movq	24(%rbx), %r15
.L167:
	movq	0(%r13), %rax
	cmpw	$28, 16(%rax)
	jne	.L189
	movl	$18, %r14d
.L166:
	movq	-104(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization18GetMaxStringLengthEPNS1_12JSHeapBrokerEPNS1_4NodeE
	testb	%al, %al
	je	.L178
	addq	%r14, %rdx
	cmpq	$1073741799, %rdx
	jbe	.L190
.L178:
	xorl	%eax, %eax
.L171:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L191
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movq	-104(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization18GetMaxStringLengthEPNS1_12JSHeapBrokerEPNS1_4NodeE
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L163:
	leaq	16(%r13), %rax
	movq	16(%r13), %r13
	addq	$8, %rax
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L188:
	call	_ZN2v88internal8compiler20StringConstantBaseOfEPKNS1_8OperatorE@PLT
	movq	%rax, %rdi
	call	_ZNK2v88internal18StringConstantBase26GetMaxStringConstantLengthEv@PLT
	movq	24(%rbx), %r15
	movq	%rax, %r14
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L190:
	movq	24(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116IsStringConstantEPNS1_12JSHeapBrokerEPNS1_4NodeE
	testb	%al, %al
	je	.L173
.L176:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization27CreateDelayedStringConstantEPNS1_4NodeE
	movq	-104(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization27CreateDelayedStringConstantEPNS1_4NodeE
	movq	72(%rbx), %rdi
	movq	%rax, %r13
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$31, %rax
	jbe	.L192
	leaq	32(%rsi), %rax
	movq	%rax, 16(%rdi)
.L177:
	movl	$2, (%rsi)
	movq	$0, 8(%rsi)
	movq	%r14, 16(%rsi)
	movq	%r13, 24(%rsi)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder21DelayedStringConstantEPKNS0_18StringConstantBaseE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %r13
	movq	%r12, %rsi
	movq	(%rdi), %rax
	movq	%r13, %rdx
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L169:
	movq	-112(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	-112(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L170
	movq	-112(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8AsStringEv@PLT
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler9StringRef6lengthEv@PLT
	movq	24(%rbx), %r15
	movslq	%eax, %r14
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L173:
	movq	24(%rbx), %rdi
	movq	-104(%rbp), %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116IsStringConstantEPNS1_12JSHeapBrokerEPNS1_4NodeE
	testb	%al, %al
	jne	.L176
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L170:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L192:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L177
.L191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23107:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization11ReduceJSAddEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization11ReduceJSAddEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSGetSuperConstructorEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"Missing "
.LC13:
	.string	"data for map "
.LC14:
	.string	" ("
	.section	.rodata._ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSGetSuperConstructorEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"../deps/v8/src/compiler/js-native-context-specialization.cc"
	.section	.rodata._ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSGetSuperConstructorEPNS1_4NodeE.str1.1
.LC16:
	.string	":"
.LC17:
	.string	")"
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSGetSuperConstructorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSGetSuperConstructorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSGetSuperConstructorEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSGetSuperConstructorEPNS1_4NodeE:
.LFB23109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpw	$30, 16(%rax)
	jne	.L194
	leaq	-80(%rbp), %r13
	movq	24(%rbx), %rsi
	movq	48(%rax), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L215
	movq	%r13, %rdi
	leaq	-128(%rbp), %r14
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	leaq	-144(%rbp), %rdi
	movq	%rax, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	movq	%rax, -128(%rbp)
	movq	%rdx, -120(%rbp)
	je	.L196
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef20serialized_prototypeEv@PLT
	testb	%al, %al
	jne	.L196
	movq	24(%rbx), %rdi
	cmpb	$0, 124(%rdi)
	jne	.L216
	.p2align 4,,10
	.p2align 3
.L194:
	xorl	%eax, %eax
.L199:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L217
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef9prototypeEv@PLT
	movq	%r14, %rdi
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	je	.L194
	leaq	-112(%rbp), %r15
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L194
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapObjectEv@PLT
	leaq	-96(%rbp), %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef14is_constructorEv@PLT
	testb	%al, %al
	je	.L194
	movq	56(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler23CompilationDependencies17DependOnStableMapERKNS1_6MapRefE@PLT
	movq	16(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %r13
	movq	%r12, %rsi
	movq	(%rdi), %rax
	movq	%r13, %rdx
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L215:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L216:
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$8, %edx
	leaq	.LC12(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$13, %edx
	movq	%r12, %rdi
	leaq	.LC13(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$2, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$59, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$357, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r13
	testq	%r13, %r13
	je	.L218
	cmpb	$0, 56(%r13)
	je	.L201
	movsbl	67(%r13), %esi
.L202:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L201:
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L202
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L202
.L217:
	call	__stack_chk_fail@PLT
.L218:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE23109:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSGetSuperConstructorEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSGetSuperConstructorEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler29JSNativeContextSpecialization24InferHasInPrototypeChainEPNS1_4NodeES4_RKNS1_13HeapObjectRefE.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.rodata._ZN2v88internal8compiler29JSNativeContextSpecialization24InferHasInPrototypeChainEPNS1_4NodeES4_RKNS1_13HeapObjectRefE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"prototype data for map "
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization24InferHasInPrototypeChainEPNS1_4NodeES4_RKNS1_13HeapObjectRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization24InferHasInPrototypeChainEPNS1_4NodeES4_RKNS1_13HeapObjectRefE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization24InferHasInPrototypeChainEPNS1_4NodeES4_RKNS1_13HeapObjectRefE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization24InferHasInPrototypeChainEPNS1_4NodeES4_RKNS1_13HeapObjectRefE:
.LFB23172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-104(%rbp), %rax
	movq	$1, -104(%rbp)
	movq	%rax, %rcx
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties23InferReceiverMapsUnsafeEPNS1_12JSHeapBrokerEPNS1_4NodeES6_PNS0_13ZoneHandleSetINS0_3MapEEE@PLT
	movl	%eax, -116(%rbp)
	testl	%eax, %eax
	je	.L264
	movq	-104(%rbp), %rdx
	movq	%rdx, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L222
	movb	$1, -117(%rbp)
	xorl	%r15d, %r15d
	leaq	-96(%rbp), %r14
	leaq	-80(%rbp), %rbx
	movb	$1, -144(%rbp)
.L241:
	testq	%rax, %rax
	je	.L223
	movq	6(%rdx), %rax
	subq	$2, %rdx
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%r15, %rdx
	jbe	.L261
	movslq	%r15d, %rsi
	cmpq	%rsi, %rdx
	jbe	.L265
	movq	(%rax,%rsi,8), %rdx
.L226:
	movq	24(%r13), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	cmpl	$2, -116(%rbp)
	jne	.L229
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L269:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef13IsJSObjectMapEv@PLT
	testb	%al, %al
	je	.L248
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	je	.L235
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef20serialized_prototypeEv@PLT
	testb	%al, %al
	je	.L267
.L235:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef9prototypeEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	jne	.L268
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef9prototypeEv@PLT
	movq	%rbx, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	je	.L264
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef12oddball_typeEv@PLT
	cmpb	$3, %al
	je	.L248
.L229:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	cmpw	$1040, %ax
	ja	.L269
.L264:
	movl	$2, %eax
.L219:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L270
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	movb	$0, -144(%rbp)
	movzbl	-117(%rbp), %ecx
.L232:
	movq	-104(%rbp), %rdx
	addq	$1, %r15
	movq	%rdx, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	jne	.L241
.L240:
	testb	%cl, %cl
	je	.L264
	xorl	%eax, %eax
	leaq	-80(%rbp), %rdi
	movl	$6, %ecx
	cmpb	$0, -144(%rbp)
	rep stosl
	jne	.L242
	xorl	%edx, %edx
	cmpl	$2, -116(%rbp)
	movq	56(%r13), %rdi
	movb	$0, -80(%rbp)
	setne	%dl
	subq	$8, %rsp
	pushq	-64(%rbp)
	movq	-128(%rbp), %rsi
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_13ZoneHandleSetINS0_3MapEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE@PLT
	addq	$32, %rsp
	movl	$1, %eax
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L223:
	testq	%r15, %r15
	je	.L226
.L261:
	movzbl	-117(%rbp), %ecx
	orl	-144(%rbp), %ecx
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L266:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	jne	.L229
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L268:
	movb	$0, -117(%rbp)
	movzbl	-144(%rbp), %ecx
	jmp	.L232
.L267:
	movq	24(%r13), %rdi
	cmpb	$0, 124(%rdi)
	je	.L264
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$8, %edx
	leaq	.LC12(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$23, %edx
	movq	%r12, %rdi
	leaq	.LC19(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$2, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$59, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$535, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r13
	testq	%r13, %r13
	je	.L271
	cmpb	$0, 56(%r13)
	je	.L237
	movsbl	67(%r13), %esi
.L238:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L222:
	xorl	%eax, %eax
	movl	$0, -79(%rbp)
	movw	%ax, -75(%rbp)
	movb	$0, -73(%rbp)
.L242:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	leaq	-96(%rbp), %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	je	.L264
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10AsJSObjectEv@PLT
	movq	56(%r13), %rdi
	movb	$1, -80(%rbp)
	movq	%rdx, -136(%rbp)
	xorl	%edx, %edx
	cmpl	$2, -116(%rbp)
	movq	%rax, -144(%rbp)
	setne	%dl
	subq	$8, %rsp
	movdqa	-144(%rbp), %xmm0
	pushq	-136(%rbp)
	movq	-128(%rbp), %rsi
	pushq	-144(%rbp)
	pushq	-80(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_13ZoneHandleSetINS0_3MapEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE@PLT
	addq	$32, %rsp
	xorl	%eax, %eax
	jmp	.L219
.L237:
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L238
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L238
.L270:
	call	__stack_chk_fail@PLT
.L271:
	call	_ZSt16__throw_bad_castv@PLT
.L265:
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE23172:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization24InferHasInPrototypeChainEPNS1_4NodeES4_RKNS1_13HeapObjectRefE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization24InferHasInPrototypeChainEPNS1_4NodeES4_RKNS1_13HeapObjectRefE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSHasInPrototypeChainEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSHasInPrototypeChainEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSHasInPrototypeChainEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSHasInPrototypeChainEPNS1_4NodeE:
.LFB23173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, %r15
	movq	0(%r13), %rax
	cmpw	$30, 16(%rax)
	jne	.L273
	leaq	-80(%rbp), %r13
	movq	24(%rbx), %rsi
	movq	48(%rax), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L283
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization24InferHasInPrototypeChainEPNS1_4NodeES4_RKNS1_13HeapObjectRefE
	cmpl	$2, %eax
	je	.L273
	movq	16(%rbx), %rdi
	testl	%eax, %eax
	jne	.L275
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	%rax, %r13
.L276:
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L273:
	xorl	%eax, %eax
.L277:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L284
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	%rax, %r13
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L283:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L284:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23173:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSHasInPrototypeChainEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSHasInPrototypeChainEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSOrdinaryHasInstanceEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"data for JSBoundFunction "
.LC21:
	.string	"data for JSFunction "
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSOrdinaryHasInstanceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSOrdinaryHasInstanceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSOrdinaryHasInstanceEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSOrdinaryHasInstanceEPNS1_4NodeE:
.LFB23174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	movq	(%r14), %rax
	cmpw	$30, 16(%rax)
	jne	.L286
	movq	48(%rax), %r14
	leaq	-128(%rbp), %r15
	movq	24(%r12), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L289
	movdqa	-128(%rbp), %xmm0
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef17IsJSBoundFunctionEv@PLT
	movq	24(%r12), %rsi
	testb	%al, %al
	je	.L288
	leaq	-112(%rbp), %rax
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rax, %rdi
	movq	%rax, %r14
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L289
	movdqa	-112(%rbp), %xmm1
	movq	%r15, %rdi
	movaps	%xmm1, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef17AsJSBoundFunctionEv@PLT
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	je	.L290
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler18JSBoundFunctionRef10serializedEv@PLT
	testb	%al, %al
	jne	.L290
	movq	24(%r12), %rdi
	cmpb	$0, 124(%rdi)
	je	.L286
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$8, %edx
	leaq	.LC12(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$25, %edx
	movq	%r12, %rdi
	leaq	.LC20(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$2, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$59, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC16(%rip), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$614, %esi
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L288:
	leaq	-96(%rbp), %rax
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	-136(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L289
	movdqa	-96(%rbp), %xmm2
	movq	%r15, %rdi
	movaps	%xmm2, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	testb	%al, %al
	je	.L286
	movq	24(%r12), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L289
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	je	.L303
	movq	-136(%rbp), %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef10serializedEv@PLT
	testb	%al, %al
	jne	.L303
	movq	24(%r12), %rdi
	cmpb	$0, 124(%rdi)
	je	.L286
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$8, %edx
	leaq	.LC12(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$20, %edx
	movq	%r12, %rdi
	leaq	.LC21(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$2, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$59, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC16(%rip), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$633, %esi
.L337:
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r13
	testq	%r13, %r13
	je	.L338
	cmpb	$0, 56(%r13)
	je	.L306
	movsbl	67(%r13), %esi
.L307:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	.p2align 4,,10
	.p2align 3
.L286:
	xorl	%eax, %eax
.L299:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L339
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L290:
	movq	-136(%rbp), %rdi
	call	_ZNK2v88internal8compiler18JSBoundFunctionRef21bound_target_functionEv@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -88(%rbp)
	xorl	%edx, %edx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	movq	16(%r12), %rdi
	leaq	-96(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	movq	16(%r12), %rax
	movq	%r15, %rsi
	movq	368(%rax), %rdi
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder10InstanceOfERKNS1_14FeedbackSourceE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSInstanceOfEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L303:
	movq	-136(%rbp), %rdi
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef18has_prototype_slotEv@PLT
	testb	%al, %al
	je	.L286
	movq	-136(%rbp), %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef13has_prototypeEv@PLT
	testb	%al, %al
	je	.L286
	movq	-136(%rbp), %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef30PrototypeRequiresRuntimeLookupEv@PLT
	testb	%al, %al
	jne	.L286
	movq	56(%r12), %rdi
	movq	-136(%rbp), %rsi
	call	_ZN2v88internal8compiler23CompilationDependencies25DependOnPrototypePropertyERKNS1_13JSFunctionRefE@PLT
	movq	16(%r12), %rdi
	movq	%r15, %rsi
	movq	%rdx, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder19HasInPrototypeChainEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSHasInPrototypeChainEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
	jmp	.L299
.L306:
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L307
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L307
.L339:
	call	__stack_chk_fail@PLT
.L338:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE23174:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSOrdinaryHasInstanceEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSOrdinaryHasInstanceEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSInstanceOfEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSInstanceOfEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSInstanceOfEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSInstanceOfEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSInstanceOfEPNS1_4NodeE:
.LFB23118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$648, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19FeedbackParameterOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, -624(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	movq	%rax, -632(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, -640(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, -616(%rbp)
	movq	(%r15), %rax
	cmpw	$30, 16(%rax)
	jne	.L341
	movq	48(%rax), %rax
	leaq	-352(%rbp), %r14
	movq	24(%r13), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%rax, -656(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L343
	movdqa	-352(%rbp), %xmm2
	leaq	-208(%rbp), %r15
	movq	%r15, %rdi
	movaps	%xmm2, -208(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef10IsJSObjectEv@PLT
	testb	%al, %al
	jne	.L580
.L341:
	cmpq	$0, (%r12)
	je	.L447
	cmpl	$-1, 8(%r12)
	je	.L447
	movdqu	(%r12), %xmm1
	movq	24(%r13), %rdi
	leaq	-208(%rbp), %r15
	movq	%r15, %rsi
	movaps	%xmm1, -208(%rbp)
	call	_ZN2v88internal8compiler12JSHeapBroker24GetFeedbackForInstanceOfERKNS1_14FeedbackSourceE@PLT
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jne	.L346
.L447:
	xorl	%eax, %eax
.L347:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L581
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	movq	%rax, %rdi
	call	_ZNK2v88internal8compiler17ProcessedFeedback12AsInstanceOfEv@PLT
	movdqu	8(%rax), %xmm3
	movaps	%xmm3, -208(%rbp)
	movq	24(%rax), %rax
	cmpb	$0, -208(%rbp)
	movq	%rax, -192(%rbp)
	je	.L447
	leaq	-200(%rbp), %rdi
	call	_ZNK2v88internal8compiler11JSObjectRef6objectEv@PLT
	movq	%rax, -656(%rbp)
	movq	%rax, %rdx
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L580:
	movq	24(%r13), %rsi
	movq	-656(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L343
	movdqa	-208(%rbp), %xmm4
	movq	%r14, %rdi
	movaps	%xmm4, -352(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef10AsJSObjectEv@PLT
	movq	%r15, %rdi
	movq	%rdx, -200(%rbp)
	movq	%rax, -208(%rbp)
	call	_ZNK2v88internal8compiler11JSObjectRef6objectEv@PLT
	movq	%rax, -656(%rbp)
	movq	%rax, %rdx
.L344:
	movq	24(%r13), %rsi
	leaq	-592(%rbp), %r12
	movl	$1, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	leaq	-496(%rbp), %r12
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r12, %rdi
	movq	%rax, -576(%rbp)
	movq	16(%r13), %rax
	movq	%rdx, -568(%rbp)
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler18PropertyAccessInfo7InvalidEPNS0_4ZoneE@PLT
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	je	.L349
	movq	16(%r13), %rax
	movq	24(%r13), %r14
	movl	$1, %ecx
	leaq	-544(%rbp), %rdi
	movq	360(%rax), %rdx
	movq	%r14, %rsi
	addq	$3936, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	subq	$8, %rsp
	movq	%r14, %rsi
	movq	-536(%rbp), %r9
	pushq	$0
	movq	-544(%rbp), %r8
	leaq	-352(%rbp), %rdi
	movq	-576(%rbp), %rdx
	movq	-568(%rbp), %rcx
	pushq	$0
	pushq	$0
	call	_ZN2v88internal8compiler12JSHeapBroker21GetPropertyAccessInfoENS1_6MapRefENS1_7NameRefENS1_10AccessModeEPNS1_23CompilationDependenciesENS1_19SerializationPolicyE@PLT
	movl	-352(%rbp), %eax
	movq	-488(%rbp), %r9
	addq	$32, %rsp
	movl	%eax, -496(%rbp)
	cmpq	%r9, -344(%rbp)
	je	.L582
	movq	-328(%rbp), %rcx
	movq	-336(%rbp), %r14
	movq	-480(%rbp), %rdi
	movq	-464(%rbp), %rax
	movq	%rcx, %r8
	subq	%r14, %r8
	subq	%rdi, %rax
	movq	%r8, %rsi
	sarq	$3, %rax
	sarq	$3, %rsi
	cmpq	%rax, %rsi
	jbe	.L352
	cmpq	$268435455, %rsi
	ja	.L376
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.L354
	movq	16(%r9), %rdx
	movq	24(%r9), %rax
	leaq	7(%r8), %rsi
	andq	$-8, %rsi
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L583
	addq	%rdx, %rsi
	movq	%rsi, 16(%r9)
.L354:
	cmpq	%r14, %rcx
	je	.L362
	subq	$8, %rcx
	leaq	15(%r14), %rax
	subq	%r14, %rcx
	subq	%rdx, %rax
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L468
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L468
	addq	$1, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L360:
	movdqu	(%r14,%rax), %xmm5
	movups	%xmm5, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L360
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rsi
	addq	%rsi, %r14
	addq	%rdx, %rsi
	cmpq	%rcx, %rax
	je	.L362
	movq	(%r14), %rax
	movq	%rax, (%rsi)
.L362:
	movq	%rdx, -480(%rbp)
	addq	%r8, %rdx
	movq	-336(%rbp), %r14
	movq	%rdx, -472(%rbp)
	movq	-328(%rbp), %rax
	movq	%rdx, -464(%rbp)
.L358:
	cmpq	%rax, %r14
	je	.L351
	movq	%r14, -328(%rbp)
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L349:
	movq	16(%r13), %rax
	movq	56(%r13), %rdx
	leaq	-528(%rbp), %r14
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rcx
	call	_ZN2v88internal8compiler17AccessInfoFactoryC1EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneE@PLT
	movq	16(%r13), %rax
	leaq	-576(%rbp), %rdi
	movq	360(%rax), %rcx
	addq	$3936, %rcx
	movq	%rcx, -664(%rbp)
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-664(%rbp), %rcx
	movq	%rax, %rdx
	call	_ZNK2v88internal8compiler17AccessInfoFactory25ComputePropertyAccessInfoENS0_6HandleINS0_3MapEEENS3_INS0_4NameEEENS1_10AccessModeE@PLT
	movl	-208(%rbp), %eax
	movq	-488(%rbp), %r9
	movl	%eax, -496(%rbp)
	cmpq	%r9, -200(%rbp)
	je	.L584
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %r14
	movq	-480(%rbp), %rdi
	movq	-464(%rbp), %rax
	movq	%rcx, %r8
	subq	%r14, %r8
	subq	%rdi, %rax
	movq	%r8, %rsi
	sarq	$3, %rax
	sarq	$3, %rsi
	cmpq	%rax, %rsi
	jbe	.L399
	cmpq	$268435455, %rsi
	ja	.L376
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.L400
	movq	16(%r9), %rdx
	movq	24(%r9), %rax
	leaq	7(%r8), %rsi
	andq	$-8, %rsi
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L585
	addq	%rdx, %rsi
	movq	%rsi, 16(%r9)
.L400:
	cmpq	%r14, %rcx
	je	.L408
	subq	$8, %rcx
	leaq	15(%r14), %rax
	subq	%r14, %rcx
	subq	%rdx, %rax
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L474
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L474
	addq	$1, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L406:
	movdqu	(%r14,%rax), %xmm7
	movups	%xmm7, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L406
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rsi
	addq	%rsi, %r14
	addq	%rdx, %rsi
	cmpq	%rax, %rcx
	je	.L408
	movq	(%r14), %rax
	movq	%rax, (%rsi)
.L408:
	movq	%rdx, -480(%rbp)
	addq	%r8, %rdx
	movq	-192(%rbp), %r14
	movq	%rdx, -472(%rbp)
	movq	-184(%rbp), %rax
	movq	%rdx, -464(%rbp)
.L404:
	cmpq	%rax, %r14
	je	.L398
	movq	%r14, -184(%rbp)
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L343:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L582:
	movq	-320(%rbp), %rax
	movdqa	-336(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movq	$0, -320(%rbp)
	movaps	%xmm0, -336(%rbp)
	movq	%rax, -464(%rbp)
	movaps	%xmm4, -480(%rbp)
.L351:
	movq	-456(%rbp), %r9
	movq	-296(%rbp), %rcx
	movq	-304(%rbp), %r14
	cmpq	%r9, -312(%rbp)
	je	.L586
	movq	-448(%rbp), %rdi
	movq	-432(%rbp), %rax
	movq	%rcx, %r8
	subq	%r14, %r8
	movq	%r8, %rsi
	subq	%rdi, %rax
	sarq	$3, %rsi
	sarq	$3, %rax
	cmpq	%rax, %rsi
	jbe	.L375
	cmpq	$268435455, %rsi
	ja	.L376
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.L377
	movq	16(%r9), %rdx
	movq	24(%r9), %rax
	leaq	7(%r8), %rsi
	andq	$-8, %rsi
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L587
	addq	%rdx, %rsi
	movq	%rsi, 16(%r9)
.L377:
	cmpq	%r14, %rcx
	je	.L385
	subq	$8, %rcx
	leaq	15(%rdx), %rax
	subq	%r14, %rcx
	subq	%r14, %rax
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L471
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L471
	addq	$1, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L383:
	movdqu	(%r14,%rax), %xmm6
	movups	%xmm6, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L383
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rsi
	addq	%rsi, %r14
	addq	%rdx, %rsi
	cmpq	%rcx, %rax
	je	.L385
	movq	(%r14), %rax
	movq	%rax, (%rsi)
.L385:
	movq	%rdx, -448(%rbp)
	addq	%r8, %rdx
	movq	%rdx, -440(%rbp)
	movq	%rdx, -432(%rbp)
.L374:
	movq	-280(%rbp), %rax
	movq	%rax, -424(%rbp)
	movq	-272(%rbp), %rax
	movq	%rax, -416(%rbp)
	movq	-264(%rbp), %rax
	movq	%rax, -408(%rbp)
	movq	-256(%rbp), %rax
	movq	%rax, -400(%rbp)
	movzbl	-248(%rbp), %eax
	movb	%al, -392(%rbp)
	movq	-240(%rbp), %rax
	movq	%rax, -384(%rbp)
	movq	-232(%rbp), %rax
	movq	%rax, -376(%rbp)
	movq	-224(%rbp), %rax
	movq	%rax, -368(%rbp)
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L584:
	movq	-176(%rbp), %rax
	movdqa	-192(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movq	%rax, -464(%rbp)
	movaps	%xmm7, -480(%rbp)
.L398:
	movq	-456(%rbp), %r9
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %r14
	cmpq	%r9, -168(%rbp)
	je	.L588
	movq	-448(%rbp), %rdi
	movq	-432(%rbp), %rax
	movq	%rcx, %r8
	subq	%r14, %r8
	movq	%r8, %rsi
	subq	%rdi, %rax
	sarq	$3, %rsi
	sarq	$3, %rax
	cmpq	%rax, %rsi
	jbe	.L421
	cmpq	$268435455, %rsi
	ja	.L376
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.L422
	movq	16(%r9), %rdx
	movq	24(%r9), %rax
	leaq	7(%r8), %rsi
	andq	$-8, %rsi
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L589
	addq	%rdx, %rsi
	movq	%rsi, 16(%r9)
.L422:
	cmpq	%r14, %rcx
	je	.L430
	subq	$8, %rcx
	leaq	15(%rdx), %rax
	subq	%r14, %rcx
	subq	%r14, %rax
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L477
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L477
	addq	$1, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L428:
	movdqu	(%r14,%rax), %xmm5
	movups	%xmm5, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L428
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rsi
	addq	%rsi, %r14
	addq	%rdx, %rsi
	cmpq	%rcx, %rax
	je	.L430
	movq	(%r14), %rax
	movq	%rax, (%rsi)
.L430:
	movq	%rdx, -448(%rbp)
	addq	%r8, %rdx
	movq	%rdx, -440(%rbp)
	movq	%rdx, -432(%rbp)
.L420:
	movq	-136(%rbp), %rax
	movq	%rax, -424(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -416(%rbp)
	movq	-120(%rbp), %rax
	movq	%rax, -408(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -400(%rbp)
	movzbl	-104(%rbp), %eax
	movb	%al, -392(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -384(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -376(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -368(%rbp)
.L396:
	movl	-496(%rbp), %edi
	testl	%edi, %edi
	je	.L447
	movq	56(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18PropertyAccessInfo18RecordDependenciesEPNS1_23CompilationDependenciesE@PLT
	movq	56(%r13), %rax
	movdqu	16(%r13), %xmm6
	movq	24(%r13), %rsi
	movq	%rax, -512(%rbp)
	movl	-496(%rbp), %eax
	movaps	%xmm6, -528(%rbp)
	cmpl	$1, %eax
	je	.L590
	cmpl	$3, %eax
	jne	.L447
	movq	-408(%rbp), %r12
	testq	%r12, %r12
	je	.L591
	leaq	-560(%rbp), %r14
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movb	$1, -664(%rbp)
.L446:
	movq	-400(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movzbl	-392(%rbp), %edx
	leaq	-352(%rbp), %rdi
	call	_ZNK2v88internal8compiler11JSObjectRef18GetOwnDataPropertyENS0_14RepresentationENS0_10FieldIndexENS1_19SerializationPolicyE@PLT
	cmpb	$0, -352(%rbp)
	je	.L447
	leaq	-344(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -672(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L447
	movq	-672(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapObjectEv@PLT
	leaq	-544(%rbp), %r10
	movq	%r10, %rdi
	movq	%rax, -544(%rbp)
	movq	%rdx, -536(%rbp)
	movq	%r10, -680(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r15, %rdi
	movq	%rax, -208(%rbp)
	movq	%rdx, -200(%rbp)
	call	_ZNK2v88internal8compiler6MapRef11is_callableEv@PLT
	testb	%al, %al
	je	.L447
	cmpb	$0, -664(%rbp)
	movq	-680(%rbp), %r10
	jne	.L448
	leaq	-488(%rbp), %r14
.L449:
	movq	-656(%rbp), %r8
	movq	-632(%rbp), %rsi
	leaq	-600(%rbp), %rdx
	leaq	-528(%rbp), %rdi
	movq	-616(%rbp), %rcx
	movq	%r10, -664(%rbp)
	movq	%rdx, -656(%rbp)
	movq	%rdi, -632(%rbp)
	call	_ZN2v88internal8compiler21PropertyAccessBuilder15BuildCheckValueEPNS1_4NodeEPS4_S4_NS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-616(%rbp), %rcx
	movq	-656(%rbp), %rdx
	movq	%r14, %r8
	movq	-632(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21PropertyAccessBuilder14BuildCheckMapsEPNS1_4NodeEPS4_S4_RKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE@PLT
	movq	16(%r13), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	%rax
	movq	-648(%rbp), %r9
	movl	$115, %esi
	pushq	$1
	movq	-640(%rbp), %rdx
	call	_ZN2v88internal8compiler39CreateStubBuiltinContinuationFrameStateEPNS1_7JSGraphENS0_8Builtins4NameEPNS1_4NodeEPKS7_iS7_NS1_26ContinuationFrameStateModeE@PLT
	movq	16(%r13), %rdi
	movq	-672(%rbp), %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_
	movq	-624(%rbp), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_
	movq	%r14, %rdx
	movl	$4, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_
	movq	-600(%rbp), %rdx
	movl	$5, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node12ReplaceInputEiPS2_
	movq	16(%r13), %rax
	movq	%r15, %rcx
	movq	-664(%rbp), %r10
	movl	$1, %r9d
	movl	$1, %r8d
	movl	$3, %esi
	movq	368(%rax), %rdi
	movq	%r10, %rdx
	movq	$0, -208(%rbp)
	movl	$-1, -200(%rbp)
	movl	$0x7fc00000, -544(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder4CallEmRKNS1_13CallFrequencyERKNS1_14FeedbackSourceENS0_19ConvertReceiverModeENS0_15SpeculationModeE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%r13), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9ToBooleanEv@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%rbx), %r15
	popq	%rdx
	popq	%rcx
	testq	%r15, %r15
	je	.L451
	movq	%rbx, -616(%rbp)
	movq	(%r15), %r12
	movq	%rax, %r14
.L461:
	movl	16(%r15), %ecx
	movq	%r15, %rdi
	movl	%ecx, %edx
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rax
	leaq	(%r15,%rax,8), %rax
	leaq	32(%rax), %rsi
	addq	$16, %rax
	andl	$1, %ecx
	cmovne	%rsi, %rax
	leaq	(%rax,%rdx,8), %rbx
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler14NodeProperties11IsValueEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L454
	movl	16(%r15), %eax
	movl	%eax, %ecx
	shrl	%ecx
	leaq	3(%rcx,%rcx,2), %rcx
	leaq	(%r15,%rcx,8), %rcx
	testb	$1, %al
	jne	.L455
	movq	(%rcx), %rcx
.L455:
	cmpq	%rcx, %r14
	je	.L454
	movq	(%rbx), %rdi
	cmpq	%rdi, %r14
	je	.L457
	testq	%rdi, %rdi
	je	.L458
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L458:
	movq	%r14, (%rbx)
	testq	%r14, %r14
	je	.L579
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L579:
	movl	16(%r15), %eax
.L457:
	movl	%eax, %edx
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%r15,%rdx,8), %rsi
	testb	$1, %al
	jne	.L460
	movq	(%rsi), %rsi
.L460:
	movq	8(%r13), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
.L454:
	testq	%r12, %r12
	je	.L577
	movq	%r12, %r15
	movq	(%r12), %r12
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L588:
	movq	-144(%rbp), %rax
	movq	%r14, %xmm0
	movq	%rcx, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, -432(%rbp)
	movaps	%xmm0, -448(%rbp)
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L586:
	movq	-288(%rbp), %rax
	movq	%r14, %xmm0
	movq	%rcx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, -432(%rbp)
	movaps	%xmm0, -448(%rbp)
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L375:
	movq	-440(%rbp), %rax
	movq	%rax, %rdx
	subq	%rdi, %rdx
	movq	%rdx, %r9
	sarq	$3, %r9
	cmpq	%r9, %rsi
	ja	.L388
	cmpq	%r14, %rcx
	je	.L389
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%r8, -664(%rbp)
	call	memmove@PLT
	movq	-664(%rbp), %r8
	movq	%rax, %rdi
	movq	-440(%rbp), %rax
.L389:
	addq	%r8, %rdi
	cmpq	%rax, %rdi
	je	.L374
	movq	%rdi, -440(%rbp)
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L399:
	movq	-472(%rbp), %r9
	movq	%r9, %rdx
	subq	%rdi, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L411
	cmpq	%r14, %rcx
	je	.L412
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%r8, -664(%rbp)
	call	memmove@PLT
	movq	-664(%rbp), %r8
	movq	-192(%rbp), %r14
	movq	%rax, %rdi
	movq	-184(%rbp), %rax
	addq	%r8, %rdi
	cmpq	-472(%rbp), %rdi
	je	.L404
.L464:
	movq	%rdi, -472(%rbp)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L352:
	movq	-472(%rbp), %r9
	movq	%r9, %rdx
	subq	%rdi, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L365
	cmpq	%r14, %rcx
	je	.L366
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%r8, -664(%rbp)
	call	memmove@PLT
	movq	-664(%rbp), %r8
	movq	-336(%rbp), %r14
	movq	%rax, %rdi
	movq	-328(%rbp), %rax
	addq	%r8, %rdi
	cmpq	-472(%rbp), %rdi
	je	.L358
.L462:
	movq	%rdi, -472(%rbp)
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L421:
	movq	-440(%rbp), %rax
	movq	%rax, %rdx
	subq	%rdi, %rdx
	movq	%rdx, %r9
	sarq	$3, %r9
	cmpq	%r9, %rsi
	ja	.L433
	cmpq	%r14, %rcx
	je	.L434
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%r8, -664(%rbp)
	call	memmove@PLT
	movq	-664(%rbp), %r8
	movq	%rax, %rdi
	movq	-440(%rbp), %rax
.L434:
	addq	%r8, %rdi
	cmpq	%rax, %rdi
	je	.L420
	movq	%rdi, -440(%rbp)
	jmp	.L420
.L433:
	leaq	(%r14,%rdx), %r8
	cmpq	%r14, %r8
	je	.L435
	movq	%r14, %rsi
	movq	%rcx, -680(%rbp)
	movq	%r8, -672(%rbp)
	movq	%rdx, -664(%rbp)
	call	memmove@PLT
	movq	-440(%rbp), %rax
	movq	-680(%rbp), %rcx
	movq	-672(%rbp), %r8
	movq	-664(%rbp), %rdx
.L435:
	cmpq	%rcx, %r8
	je	.L436
	subq	$8, %rcx
	leaq	16(%r14,%rdx), %rdx
	subq	%r8, %rcx
	movq	%rcx, %rdi
	shrq	$3, %rdi
	cmpq	%rdx, %rax
	leaq	16(%rax), %rdx
	setnb	%sil
	cmpq	%rdx, %r8
	setnb	%dl
	orb	%dl, %sil
	je	.L478
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rdi
	je	.L478
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L438:
	movdqu	(%r8,%rdx), %xmm6
	movups	%xmm6, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L438
	movq	%rdi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r8
	addq	%rax, %rsi
	cmpq	%rdi, %rdx
	je	.L440
	movq	(%r8), %rdx
	movq	%rdx, (%rsi)
.L440:
	leaq	8(%rax,%rcx), %rax
.L436:
	movq	%rax, -440(%rbp)
	jmp	.L420
.L388:
	leaq	(%r14,%rdx), %r8
	cmpq	%r14, %r8
	je	.L390
	movq	%r14, %rsi
	movq	%rcx, -680(%rbp)
	movq	%r8, -672(%rbp)
	movq	%rdx, -664(%rbp)
	call	memmove@PLT
	movq	-440(%rbp), %rax
	movq	-680(%rbp), %rcx
	movq	-672(%rbp), %r8
	movq	-664(%rbp), %rdx
.L390:
	cmpq	%rcx, %r8
	je	.L391
	subq	$8, %rcx
	leaq	16(%r14,%rdx), %rdx
	subq	%r8, %rcx
	movq	%rcx, %rdi
	shrq	$3, %rdi
	cmpq	%rdx, %rax
	leaq	16(%rax), %rdx
	setnb	%sil
	cmpq	%rdx, %r8
	setnb	%dl
	orb	%dl, %sil
	je	.L472
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rdi
	je	.L472
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L393:
	movdqu	(%r8,%rdx), %xmm4
	movups	%xmm4, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L393
	movq	%rdi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r8
	addq	%rax, %rsi
	cmpq	%rdi, %rdx
	je	.L395
	movq	(%r8), %rdx
	movq	%rdx, (%rsi)
.L395:
	leaq	8(%rax,%rcx), %rax
.L391:
	movq	%rax, -440(%rbp)
	jmp	.L374
.L411:
	leaq	(%r14,%rdx), %r8
	cmpq	%r8, %r14
	je	.L413
	movq	%r14, %rsi
	movq	%r8, -680(%rbp)
	movq	%rcx, -672(%rbp)
	movq	%rdx, -664(%rbp)
	call	memmove@PLT
	movq	-472(%rbp), %r9
	movq	-680(%rbp), %r8
	movq	-672(%rbp), %rcx
	movq	-664(%rbp), %rdx
.L413:
	cmpq	%r8, %rcx
	je	.L414
	subq	$8, %rcx
	leaq	16(%r14,%rdx), %rax
	subq	%r8, %rcx
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	%rax, %r9
	leaq	16(%r9), %rax
	setnb	%dl
	cmpq	%rax, %r8
	setnb	%al
	orb	%al, %dl
	je	.L475
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L475
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L416:
	movdqu	(%r8,%rax), %xmm7
	movups	%xmm7, (%r9,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L416
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdx
	addq	%rdx, %r8
	addq	%r9, %rdx
	cmpq	%rax, %rsi
	je	.L418
	movq	(%r8), %rax
	movq	%rax, (%rdx)
.L418:
	leaq	8(%r9,%rcx), %r9
.L414:
	movq	%r9, -472(%rbp)
	movq	-192(%rbp), %r14
	movq	-184(%rbp), %rax
	jmp	.L404
.L590:
	leaq	-576(%rbp), %rdi
	call	_ZNK2v88internal8compiler6MapRef11is_callableEv@PLT
	testb	%al, %al
	je	.L447
	subq	$8, %rsp
	movb	$0, -208(%rbp)
	movq	56(%r13), %rdi
	leaq	-488(%rbp), %r12
	pushq	-192(%rbp)
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$0, -200(%rbp)
	pushq	-200(%rbp)
	pushq	-208(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE@PLT
	movq	-632(%rbp), %r14
	addq	$32, %rsp
	movq	%r12, %r8
	movq	-616(%rbp), %rcx
	leaq	-600(%rbp), %rdx
	leaq	-528(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler21PropertyAccessBuilder14BuildCheckMapsEPNS1_4NodeEPS4_S4_RKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	movq	-624(%rbp), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	movq	-600(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	16(%r13), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder19OrdinaryHasInstanceEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSOrdinaryHasInstanceEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%rbx, %rax
	jmp	.L347
.L365:
	leaq	(%r14,%rdx), %r8
	cmpq	%r8, %r14
	je	.L367
	movq	%r14, %rsi
	movq	%r8, -680(%rbp)
	movq	%rcx, -672(%rbp)
	movq	%rdx, -664(%rbp)
	call	memmove@PLT
	movq	-472(%rbp), %r9
	movq	-680(%rbp), %r8
	movq	-672(%rbp), %rcx
	movq	-664(%rbp), %rdx
.L367:
	cmpq	%r8, %rcx
	je	.L368
	subq	$8, %rcx
	leaq	16(%r14,%rdx), %rax
	subq	%r8, %rcx
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	%rax, %r9
	leaq	16(%r9), %rax
	setnb	%dl
	cmpq	%rax, %r8
	setnb	%al
	orb	%al, %dl
	je	.L469
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L469
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L370:
	movdqu	(%r8,%rax), %xmm7
	movups	%xmm7, (%r9,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L370
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdx
	addq	%rdx, %r8
	addq	%r9, %rdx
	cmpq	%rsi, %rax
	je	.L372
	movq	(%r8), %rax
	movq	%rax, (%rdx)
.L372:
	leaq	8(%r9,%rcx), %r9
.L368:
	movq	%r9, -472(%rbp)
	movq	-336(%rbp), %r14
	movq	-328(%rbp), %rax
	jmp	.L358
.L366:
	addq	%r8, %rdi
	movq	%r14, %rax
	cmpq	%rdi, %r9
	jne	.L462
	jmp	.L351
.L412:
	addq	%r8, %rdi
	movq	%r14, %rax
	cmpq	%rdi, %r9
	jne	.L464
	jmp	.L398
.L477:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L427:
	movq	(%r14,%rax,8), %rsi
	movq	%rsi, (%rdx,%rax,8)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rsi, %rcx
	jne	.L427
	jmp	.L430
.L468:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L359:
	movq	(%r14,%rax,8), %rsi
	movq	%rsi, (%rdx,%rax,8)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rcx, %rsi
	jne	.L359
	jmp	.L362
.L474:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L405:
	movq	(%r14,%rax,8), %rsi
	movq	%rsi, (%rdx,%rax,8)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rsi, %rcx
	jne	.L405
	jmp	.L408
.L471:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L382:
	movq	(%r14,%rax,8), %rsi
	movq	%rsi, (%rdx,%rax,8)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rcx, %rsi
	jne	.L382
	jmp	.L385
.L591:
	movdqa	-592(%rbp), %xmm4
	movb	$0, -664(%rbp)
	leaq	-560(%rbp), %r14
	movaps	%xmm4, -560(%rbp)
	jmp	.L446
.L577:
	movq	-616(%rbp), %rbx
.L451:
	movq	%rbx, %rax
	jmp	.L347
.L478:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L437:
	movq	(%r8,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L437
	jmp	.L440
.L475:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L415:
	movq	(%r8,%rax,8), %rdx
	movq	%rdx, (%r9,%rax,8)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%rdx, %rsi
	jne	.L415
	jmp	.L418
.L469:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L369:
	movq	(%r8,%rax,8), %rdx
	movq	%rdx, (%r9,%rax,8)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%rdx, %rsi
	jne	.L369
	jmp	.L372
.L472:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L392:
	movq	(%r8,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rdi, %rsi
	jne	.L392
	jmp	.L395
.L585:
	movq	%r9, %rdi
	movq	%r8, -672(%rbp)
	movq	%rcx, -664(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-664(%rbp), %rcx
	movq	-672(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L400
.L583:
	movq	%r9, %rdi
	movq	%r8, -672(%rbp)
	movq	%rcx, -664(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-664(%rbp), %rcx
	movq	-672(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L354
.L589:
	movq	%r9, %rdi
	movq	%rcx, -672(%rbp)
	movq	%r8, -664(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-664(%rbp), %r8
	movq	-672(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L422
.L587:
	movq	%r9, %rdi
	movq	%rcx, -672(%rbp)
	movq	%r8, -664(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-664(%rbp), %r8
	movq	-672(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L377
.L581:
	call	__stack_chk_fail@PLT
.L448:
	movq	24(%r13), %rsi
	movq	56(%r13), %r8
	movq	%r10, %rdi
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%r10, -664(%rbp)
	leaq	-488(%rbp), %r14
	movq	%r8, -680(%rbp)
	call	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-536(%rbp), %rdx
	movq	-544(%rbp), %rax
	movb	$1, -208(%rbp)
	movq	-680(%rbp), %r8
	pushq	%rsi
	movq	%r14, %rsi
	pushq	%rdx
	pushq	%rax
	movq	%r8, %rdi
	pushq	-208(%rbp)
	movq	%rdx, -192(%rbp)
	movl	$1, %edx
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE@PLT
	movq	-664(%rbp), %r10
	addq	$32, %rsp
	jmp	.L449
.L376:
	leaq	.LC22(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23118:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSInstanceOfEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSInstanceOfEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization22ReduceJSPromiseResolveEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization22ReduceJSPromiseResolveEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization22ReduceJSPromiseResolveEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization22ReduceJSPromiseResolveEPNS1_4NodeE:
.LFB23175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$200, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, -224(%rbp)
	movq	(%r15), %rax
	cmpw	$30, 16(%rax)
	jne	.L597
	leaq	-160(%rbp), %r15
	movq	24(%rbx), %rsi
	movq	48(%rax), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L610
	movq	24(%rbx), %rax
	movdqa	-160(%rbp), %xmm2
	cmpb	$0, 24(%rax)
	movaps	%xmm2, -192(%rbp)
	je	.L611
	movdqu	32(%rax), %xmm3
	leaq	-176(%rbp), %rdi
	movaps	%xmm3, -176(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef16promise_functionEv@PLT
	leaq	-192(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	jne	.L612
.L597:
	xorl	%eax, %eax
.L594:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L613
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L612:
	.cfi_restore_state
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler12MapInferenceC1EPNS1_12JSHeapBrokerEPNS1_4NodeES6_@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler12MapInference8HaveMapsEv@PLT
	testb	%al, %al
	jne	.L614
.L600:
	xorl	%eax, %eax
.L601:
	movq	%r15, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler12MapInferenceD1Ev@PLT
	movq	-208(%rbp), %rax
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L610:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L614:
	movl	$1074, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler12MapInference21AnyOfInstanceTypesAreENS0_12InstanceTypeE@PLT
	testb	%al, %al
	jne	.L600
	movq	56(%rbx), %rdi
	call	_ZN2v88internal8compiler23CompilationDependencies28DependOnPromiseHookProtectorEv@PLT
	testb	%al, %al
	je	.L600
	movq	-208(%rbp), %rax
	movq	%r14, %xmm4
	movq	%rax, %xmm1
	movq	%rax, %xmm0
	movq	16(%rbx), %rax
	punpcklqdq	%xmm4, %xmm0
	movhps	-216(%rbp), %xmm1
	movq	368(%rax), %rdi
	movq	(%rax), %r14
	movaps	%xmm1, -240(%rbp)
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder13CreatePromiseEv@PLT
	leaq	-112(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movdqa	-208(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%rcx, -216(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -208(%rbp)
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler17JSOperatorBuilder14ResolvePromiseEv@PLT
	movq	%r13, %xmm5
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	-208(%rbp), %rax
	movq	-224(%rbp), %r13
	movl	$6, %edx
	movdqa	-240(%rbp), %xmm1
	movq	-216(%rbp), %rcx
	movq	%rax, %xmm0
	movq	%r13, %xmm6
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	%rax, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%r13, %r8
	movq	%r12, %rsi
	movq	%rax, %rcx
	movq	-208(%rbp), %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-208(%rbp), %rax
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L611:
	leaq	.LC10(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L613:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23175:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization22ReduceJSPromiseResolveEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization22ReduceJSPromiseResolveEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler29JSNativeContextSpecialization22ReduceJSResolvePromiseEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization22ReduceJSResolvePromiseEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization22ReduceJSResolvePromiseEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization22ReduceJSResolvePromiseEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization22ReduceJSResolvePromiseEPNS1_4NodeE:
.LFB23176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-288(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -440(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	%rax, -448(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %rbx
	movq	%rax, -464(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	24(%r15), %rsi
	movq	%r12, %rdx
	movq	%rbx, %rcx
	movq	%r14, %rdi
	movq	%rax, -472(%rbp)
	xorl	%r12d, %r12d
	call	_ZN2v88internal8compiler12MapInferenceC1EPNS1_12JSHeapBrokerEPNS1_4NodeES6_@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler12MapInference8HaveMapsEv@PLT
	testb	%al, %al
	jne	.L688
.L617:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12MapInferenceD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L689
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L688:
	.cfi_restore_state
	movq	%r14, %rdi
	leaq	-352(%rbp), %rbx
	call	_ZN2v88internal8compiler12MapInference7GetMapsEv@PLT
	movq	24(%r15), %rsi
	movq	%rbx, %rdi
	movq	%rbx, -480(%rbp)
	movq	%rax, %r12
	movq	16(%r15), %rax
	movq	(%rax), %rdx
	movq	(%rdx), %rdx
	movq	$0, -376(%rbp)
	movq	$0, -368(%rbp)
	movq	%rdx, -384(%rbp)
	movq	56(%r15), %rdx
	movq	$0, -360(%rbp)
	movq	(%rax), %rax
	movq	(%rax), %rcx
	call	_ZN2v88internal8compiler17AccessInfoFactoryC1EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneE@PLT
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	je	.L690
	movq	8(%r12), %rax
	movq	(%r12), %r11
	movq	%rax, -432(%rbp)
	cmpq	%rax, %r11
	je	.L620
	leaq	-320(%rbp), %rax
	leaq	-400(%rbp), %rcx
	movq	%r13, -496(%rbp)
	movq	%r11, %r13
	leaq	-240(%rbp), %rdi
	movq	%r14, -504(%rbp)
	movq	%rcx, %rbx
	movq	%r15, %r14
	movq	%rax, -488(%rbp)
	movq	%rax, %r15
	movq	%rdi, -416(%rbp)
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L692:
	movl	-240(%rbp), %eax
	addq	$8, %r13
	movl	%eax, (%r12)
	movq	-232(%rbp), %rax
	movq	%rax, 8(%r12)
	movq	-224(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-216(%rbp), %rax
	movq	%rax, 24(%r12)
	movq	-208(%rbp), %rax
	movq	%rax, 32(%r12)
	movq	-200(%rbp), %rax
	movq	%rax, 40(%r12)
	movq	-192(%rbp), %rax
	movq	%rax, 48(%r12)
	movq	-184(%rbp), %rax
	movq	%rax, 56(%r12)
	movq	-176(%rbp), %rax
	movq	%rax, 64(%r12)
	movq	-168(%rbp), %rax
	movq	%rax, 72(%r12)
	movq	-160(%rbp), %rax
	movq	%rax, 80(%r12)
	movq	-152(%rbp), %rax
	movq	%rax, 88(%r12)
	movq	-144(%rbp), %rax
	movq	%rax, 96(%r12)
	movzbl	-136(%rbp), %eax
	movb	%al, 104(%r12)
	movq	-128(%rbp), %rax
	movq	%rax, 112(%r12)
	movq	-120(%rbp), %rax
	movq	%rax, 120(%r12)
	movq	-112(%rbp), %rax
	movq	%rax, 128(%r12)
	movq	-368(%rbp), %rax
	leaq	136(%rax), %rdx
	movq	%rdx, -368(%rbp)
	cmpq	%r13, -432(%rbp)
	je	.L691
.L630:
	movq	0(%r13), %rdx
	movq	24(%r14), %rsi
	movl	$1, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	16(%r14), %rax
	movq	24(%r14), %r12
	movq	%r15, %rdi
	movl	$1, %ecx
	movq	360(%rax), %rdx
	movq	%r12, %rsi
	addq	$3408, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	-320(%rbp), %r8
	movq	-312(%rbp), %r9
	movq	-400(%rbp), %rdx
	pushq	$0
	movq	-392(%rbp), %rcx
	movq	-416(%rbp), %rdi
	pushq	$0
	pushq	$0
	call	_ZN2v88internal8compiler12JSHeapBroker21GetPropertyAccessInfoENS1_6MapRefENS1_7NameRefENS1_10AccessModeEPNS1_23CompilationDependenciesENS1_19SerializationPolicyE@PLT
	movq	-368(%rbp), %r12
	addq	$32, %rsp
	cmpq	-360(%rbp), %r12
	pxor	%xmm0, %xmm0
	jne	.L692
	movq	-376(%rbp), %r8
	movq	%r12, %rcx
	movabsq	$-1085102592571150095, %rdi
	subq	%r8, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	$15790320, %rax
	je	.L693
	testq	%rax, %rax
	je	.L656
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L694
	movl	$2147483520, %esi
	movl	$2147483520, %edx
.L624:
	movq	-384(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r9
	subq	%rax, %r9
	cmpq	%rsi, %r9
	jb	.L695
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L627:
	leaq	(%rax,%rdx), %rdi
	leaq	136(%rax), %rdx
.L625:
	movl	-240(%rbp), %esi
	addq	%rax, %rcx
	movl	%esi, (%rcx)
	movq	-232(%rbp), %rsi
	movq	%rsi, 8(%rcx)
	movq	-224(%rbp), %rsi
	movq	%rsi, 16(%rcx)
	movq	-216(%rbp), %rsi
	movq	%rsi, 24(%rcx)
	movq	-208(%rbp), %rsi
	movq	%rsi, 32(%rcx)
	movq	-200(%rbp), %rsi
	movq	$0, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movq	%rsi, 40(%rcx)
	movq	-192(%rbp), %rsi
	movq	%rsi, 48(%rcx)
	movq	-184(%rbp), %rsi
	movq	%rsi, 56(%rcx)
	movq	-176(%rbp), %rsi
	movq	%rsi, 64(%rcx)
	movq	-168(%rbp), %rsi
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movq	%rsi, 72(%rcx)
	movq	-160(%rbp), %rsi
	movq	%rsi, 80(%rcx)
	movq	-152(%rbp), %rsi
	movq	%rsi, 88(%rcx)
	movq	-144(%rbp), %rsi
	movq	%rsi, 96(%rcx)
	movzbl	-136(%rbp), %esi
	movb	%sil, 104(%rcx)
	movq	-128(%rbp), %rsi
	movq	%rsi, 112(%rcx)
	movq	-120(%rbp), %rsi
	movq	%rsi, 120(%rcx)
	movq	-112(%rbp), %rsi
	movq	%rsi, 128(%rcx)
	cmpq	%r8, %r12
	je	.L628
	movq	%r8, %rcx
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L629:
	movl	(%rcx), %edx
	addq	$136, %rcx
	addq	$136, %rsi
	movl	%edx, -136(%rsi)
	movq	-128(%rcx), %rdx
	movq	%rdx, -128(%rsi)
	movdqu	-120(%rcx), %xmm2
	movups	%xmm2, -120(%rsi)
	movq	-104(%rcx), %rdx
	movq	%rdx, -104(%rsi)
	movq	-96(%rcx), %rdx
	movq	$0, -104(%rcx)
	movups	%xmm0, -120(%rcx)
	movq	%rdx, -96(%rsi)
	movdqu	-88(%rcx), %xmm3
	movups	%xmm3, -88(%rsi)
	movq	-72(%rcx), %rdx
	movq	%rdx, -72(%rsi)
	movq	-64(%rcx), %rdx
	movq	$0, -72(%rcx)
	movups	%xmm0, -88(%rcx)
	movq	%rdx, -64(%rsi)
	movq	-56(%rcx), %rdx
	movq	%rdx, -56(%rsi)
	movq	-48(%rcx), %rdx
	movq	%rdx, -48(%rsi)
	movq	-40(%rcx), %rdx
	movq	%rdx, -40(%rsi)
	movzbl	-32(%rcx), %edx
	movb	%dl, -32(%rsi)
	movq	-24(%rcx), %rdx
	movq	%rdx, -24(%rsi)
	movq	-16(%rcx), %rdx
	movq	%rdx, -16(%rsi)
	movq	-8(%rcx), %rdx
	movq	%rdx, -8(%rsi)
	cmpq	%rcx, %r12
	jne	.L629
	leaq	-136(%r12), %rdx
	movabsq	$1220740416642543857, %rcx
	subq	%r8, %rdx
	shrq	$3, %rdx
	imulq	%rcx, %rdx
	movabsq	$2305843009213693951, %rcx
	andq	%rcx, %rdx
	addq	$2, %rdx
	movq	%rdx, %rcx
	salq	$4, %rcx
	addq	%rcx, %rdx
	leaq	(%rax,%rdx,8), %rdx
.L628:
	movq	%rax, %xmm1
	movq	%rdx, %xmm6
	movq	%rdi, -360(%rbp)
	addq	$8, %r13
	punpcklqdq	%xmm6, %xmm1
	movups	%xmm1, -376(%rbp)
	cmpq	%r13, -432(%rbp)
	jne	.L630
.L691:
	movq	%r14, %r15
	movq	-496(%rbp), %r13
	movq	-504(%rbp), %r14
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L690:
	movq	16(%r15), %rax
	movq	-480(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	-384(%rbp), %r8
	movq	360(%rax), %rdx
	addq	$3408, %rdx
	call	_ZNK2v88internal8compiler17AccessInfoFactory26ComputePropertyAccessInfosERKSt6vectorINS0_6HandleINS0_3MapEEESaIS6_EENS4_INS0_4NameEEENS1_10AccessModeEPNS0_10ZoneVectorINS1_18PropertyAccessInfoEEE@PLT
.L620:
	leaq	-320(%rbp), %rax
	movq	-368(%rbp), %rdx
	movq	%rax, -488(%rbp)
	leaq	-240(%rbp), %rax
	movq	%rax, -416(%rbp)
.L619:
	movq	-384(%rbp), %rdi
	movq	%rdx, %rbx
	movq	-376(%rbp), %rax
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	xorl	%r10d, %r10d
	movq	%rax, -432(%rbp)
	movq	%rdi, -320(%rbp)
	movq	$0, -296(%rbp)
	subq	%rax, %rbx
	je	.L631
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	leaq	7(%rbx), %rsi
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L696
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L633:
	movq	-368(%rbp), %rdx
	movq	%rax, %r10
.L631:
	movq	%r10, %xmm0
	movq	-432(%rbp), %rax
	addq	%r10, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -296(%rbp)
	movups	%xmm0, -312(%rbp)
	cmpq	%rax, %rdx
	je	.L634
	movq	%r14, %r9
	movq	%rax, %r12
	movq	%rdx, %r14
	movq	%r10, %rbx
	movabsq	$2305843009213693948, %r8
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r10, %r15
	.p2align 4,,10
	.p2align 3
.L651:
	movl	(%r12), %eax
	movl	%eax, (%rbx)
	movq	8(%r12), %rdi
	movq	24(%r12), %r13
	subq	16(%r12), %r13
	movq	$0, 16(%rbx)
	movq	%rdi, 8(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	je	.L660
	movq	16(%rdi), %rax
	movq	24(%rdi), %r10
	leaq	7(%r13), %rsi
	andq	$-8, %rsi
	subq	%rax, %r10
	cmpq	%r10, %rsi
	ja	.L697
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L635:
	movq	%rax, %xmm0
	addq	%rax, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movq	24(%r12), %rsi
	movq	16(%r12), %r11
	cmpq	%r11, %rsi
	je	.L638
	subq	$8, %rsi
	leaq	15(%r11), %r10
	subq	%r11, %rsi
	subq	%rax, %r10
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %r10
	jbe	.L661
	testq	%r8, %rdi
	je	.L661
	addq	$1, %rdi
	xorl	%r10d, %r10d
	movq	%rdi, %r13
	shrq	%r13
	salq	$4, %r13
	.p2align 4,,10
	.p2align 3
.L640:
	movdqu	(%r11,%r10), %xmm4
	movups	%xmm4, (%rax,%r10)
	addq	$16, %r10
	cmpq	%r13, %r10
	jne	.L640
	movq	%rdi, %r13
	andq	$-2, %r13
	leaq	0(,%r13,8), %r10
	addq	%r10, %r11
	addq	%rax, %r10
	cmpq	%r13, %rdi
	je	.L642
	movq	(%r11), %rdi
	movq	%rdi, (%r10)
.L642:
	leaq	8(%rax,%rsi), %rax
.L638:
	movq	%rax, 24(%rbx)
	movq	40(%r12), %rdi
	movq	56(%r12), %r13
	subq	48(%r12), %r13
	movq	$0, 48(%rbx)
	movq	%rdi, 40(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	je	.L662
	movq	16(%rdi), %rax
	movq	24(%rdi), %r10
	leaq	7(%r13), %rsi
	andq	$-8, %rsi
	subq	%rax, %r10
	cmpq	%r10, %rsi
	ja	.L698
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L643:
	movq	%rax, %xmm0
	addq	%rax, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movq	56(%r12), %rsi
	movq	48(%r12), %r11
	cmpq	%r11, %rsi
	je	.L646
	subq	$8, %rsi
	leaq	15(%r11), %r10
	subq	%r11, %rsi
	subq	%rax, %r10
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %r10
	jbe	.L663
	testq	%r8, %rdi
	je	.L663
	addq	$1, %rdi
	xorl	%r10d, %r10d
	movq	%rdi, %r13
	shrq	%r13
	salq	$4, %r13
	.p2align 4,,10
	.p2align 3
.L648:
	movdqu	(%r11,%r10), %xmm5
	movups	%xmm5, (%rax,%r10)
	addq	$16, %r10
	cmpq	%r10, %r13
	jne	.L648
	movq	%rdi, %r13
	andq	$-2, %r13
	leaq	0(,%r13,8), %r10
	addq	%r10, %r11
	addq	%rax, %r10
	cmpq	%r13, %rdi
	je	.L650
	movq	(%r11), %rdi
	movq	%rdi, (%r10)
.L650:
	leaq	8(%rax,%rsi), %rax
.L646:
	movq	%rax, 56(%rbx)
	movq	72(%r12), %rax
	addq	$136, %r12
	addq	$136, %rbx
	movq	%rax, -64(%rbx)
	movq	-56(%r12), %rax
	movq	%rax, -56(%rbx)
	movq	-48(%r12), %rax
	movq	%rax, -48(%rbx)
	movq	-40(%r12), %rax
	movq	%rax, -40(%rbx)
	movzbl	-32(%r12), %eax
	movb	%al, -32(%rbx)
	movq	-24(%r12), %rax
	movq	%rax, -24(%rbx)
	movq	-16(%r12), %rax
	movq	%rax, -16(%rbx)
	movq	-8(%r12), %rax
	movq	%rax, -8(%rbx)
	cmpq	%r12, %r14
	jne	.L651
	movq	%r15, %r10
	movq	%rdx, %r15
	movq	%r14, %rdx
	movq	%rcx, %r13
	leaq	-136(%rdx), %rax
	subq	-432(%rbp), %rax
	movq	%r9, %r14
	movabsq	$1220740416642543857, %rdx
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	leaq	(%r10,%rax,8), %r10
.L634:
	movq	-488(%rbp), %rdx
	movq	-480(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r10, -304(%rbp)
	movq	-416(%rbp), %rdi
	call	_ZNK2v88internal8compiler17AccessInfoFactory32FinalizePropertyAccessInfosAsOneENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeE@PLT
	cmpl	$1, -240(%rbp)
	jne	.L654
	movq	56(%r15), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12MapInference22RelyOnMapsViaStabilityEPNS1_23CompilationDependenciesE@PLT
	testb	%al, %al
	jne	.L699
.L654:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12MapInference8NoChangeEv@PLT
	movq	%rax, %r12
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L660:
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L662:
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L661:
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L639:
	movq	(%r11,%r10,8), %r13
	movq	%r13, (%rax,%r10,8)
	movq	%r10, %r13
	addq	$1, %r10
	cmpq	%rdi, %r13
	jne	.L639
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L663:
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L647:
	movq	(%r11,%r10,8), %r13
	movq	%r13, (%rax,%r10,8)
	movq	%r10, %r13
	addq	$1, %r10
	cmpq	%rdi, %r13
	jne	.L647
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L694:
	testq	%rdx, %rdx
	jne	.L700
	movl	$136, %edx
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L656:
	movl	$136, %esi
	movl	$136, %edx
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L698:
	movq	%r9, -512(%rbp)
	movq	%rcx, -504(%rbp)
	movq	%rdx, -496(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-496(%rbp), %rdx
	movq	-504(%rbp), %rcx
	movabsq	$2305843009213693948, %r8
	movq	-512(%rbp), %r9
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L697:
	movq	%r9, -512(%rbp)
	movq	%rcx, -504(%rbp)
	movq	%rdx, -496(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-496(%rbp), %rdx
	movq	-504(%rbp), %rcx
	movabsq	$2305843009213693948, %r8
	movq	-512(%rbp), %r9
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L699:
	movq	56(%r15), %rdi
	subq	$8, %rsp
	movb	$0, -320(%rbp)
	leaq	-232(%rbp), %rsi
	pushq	-304(%rbp)
	movl	$1, %edx
	movq	-440(%rbp), %xmm1
	movq	-456(%rbp), %xmm0
	movb	$0, -312(%rbp)
	pushq	-312(%rbp)
	movhps	-448(%rbp), %xmm1
	pushq	-320(%rbp)
	movhps	-464(%rbp), %xmm0
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE@PLT
	movq	16(%r15), %rax
	addq	$32, %rsp
	movq	368(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler17JSOperatorBuilder14FulfillPromiseEv@PLT
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	-472(%rbp), %rbx
	movdqa	-432(%rbp), %xmm1
	movq	%rax, %rsi
	movl	$5, %edx
	movdqa	-416(%rbp), %xmm0
	movq	%rbx, -64(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r15), %rdi
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	%rax, %r12
	movq	(%rdi), %rax
	movq	%r12, %rcx
	movq	%r12, %rdx
	call	*32(%rax)
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L695:
	movq	%rdx, -528(%rbp)
	movq	%rcx, -520(%rbp)
	movq	%r8, -512(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-512(%rbp), %r8
	movq	-520(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	-528(%rbp), %rdx
	jmp	.L627
.L696:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-376(%rbp), %rcx
	movq	%rcx, -432(%rbp)
	jmp	.L633
.L689:
	call	__stack_chk_fail@PLT
.L693:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L700:
	cmpq	$15790320, %rdx
	movl	$15790320, %eax
	cmova	%rax, %rdx
	imulq	$136, %rdx, %rdx
	movq	%rdx, %rsi
	jmp	.L624
	.cfi_endproc
.LFE23176:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization22ReduceJSResolvePromiseEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization22ReduceJSResolvePromiseEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE:
.LFB23183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler15ContextAccessOfEPKNS1_8OperatorE@PLT
	movq	%rax, %r8
	xorl	%eax, %eax
	cmpl	$3, 4(%r8)
	je	.L708
.L704:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L709
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L708:
	.cfi_restore_state
	movq	24(%rbx), %rax
	movq	16(%rbx), %rdi
	cmpb	$0, 24(%rax)
	je	.L710
	movdqu	32(%rax), %xmm0
	leaq	-64(%rbp), %rsi
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %r13
	movq	%r12, %rsi
	movq	(%rdi), %rax
	movq	%r13, %rdx
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L710:
	leaq	.LC10(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L709:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23183:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_RKNS1_15PropertyCellRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_RKNS1_15PropertyCellRefE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_RKNS1_15PropertyCellRefE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_RKNS1_15PropertyCellRefE:
.LFB23187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$264, %rsp
	movq	16(%rbp), %rax
	movq	%rdx, -240(%rbp)
	movq	%rcx, -224(%rbp)
	movq	24(%rbp), %r15
	movq	%r8, -256(%rbp)
	movq	%rax, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r15, %rdi
	movq	%rax, -216(%rbp)
	call	_ZNK2v88internal8compiler15PropertyCellRef5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, -192(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L712
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapObjectEv@PLT
	leaq	-176(%rbp), %rdi
	movq	%rax, -176(%rbp)
	movq	%rdx, -168(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	leaq	-160(%rbp), %r9
	movq	%r9, %rdi
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZNK2v88internal8compiler6MapRef12oddball_typeEv@PLT
	cmpb	$4, %al
	je	.L771
.L712:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler15PropertyCellRef16property_detailsEv@PLT
	movl	%eax, -264(%rbp)
	shrl	$6, %eax
	movl	%eax, -280(%rbp)
	andl	$3, %eax
	movl	%eax, -268(%rbp)
	cmpl	$1, %r13d
	je	.L774
	cmpl	$3, %r13d
	je	.L775
.L717:
	cmpq	$0, -248(%rbp)
	je	.L721
	movq	-256(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8IsSymbolEv@PLT
	testb	%al, %al
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	jne	.L776
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder29CheckEqualsInternalizedStringEv@PLT
	movq	%rax, %r9
.L723:
	movq	16(%rbx), %rdi
	movq	-256(%rbp), %rsi
	movq	%r9, -288(%rbp)
	movq	-208(%rbp), %xmm1
	movq	(%rdi), %r10
	movhps	-216(%rbp), %xmm1
	movq	%r10, -208(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	-288(%rbp), %r9
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-208(%rbp), %r10
	movq	%rax, %xmm0
	movl	$4, %edx
	movdqa	-304(%rbp), %xmm1
	movhps	-248(%rbp), %xmm0
	movq	%r9, %rsi
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -208(%rbp)
.L721:
	cmpq	$0, -240(%rbp)
	je	.L724
	movq	16(%rbx), %rdi
	movq	48(%rbx), %rsi
	movq	(%rdi), %r9
	movq	%r9, -288(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, -248(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	-288(%rbp), %r9
	leaq	-96(%rbp), %rcx
	movq	-240(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%rcx, -304(%rbp)
	movhps	-248(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	leaq	-160(%rbp), %r9
	movl	$30, %esi
	movq	%rax, -240(%rbp)
	movq	16(%rbx), %rax
	movq	%r9, %rdx
	movq	(%rax), %r10
	movq	376(%rax), %rdi
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder7CheckIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	movq	-248(%rbp), %r10
	xorl	%r8d, %r8d
	movq	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-304(%rbp), %rcx
	movq	-216(%rbp), %rax
	movl	$3, %edx
	movhps	-208(%rbp), %xmm0
	movq	%r10, %rdi
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -208(%rbp)
.L724:
	testl	%r13d, %r13d
	je	.L748
	cmpl	$3, %r13d
	je	.L748
	movl	-268(%rbp), %eax
	cmpl	$2, %eax
	je	.L740
	cmpl	$3, %eax
	je	.L741
	cmpl	$1, %eax
	je	.L742
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L774:
	testb	$8, -264(%rbp)
	jne	.L771
	testl	%eax, %eax
	je	.L771
	cmpl	$2, %eax
	jne	.L717
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L717
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapObjectEv@PLT
	leaq	-176(%rbp), %rdi
	movq	%rax, -176(%rbp)
	movq	%rdx, -168(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	leaq	-160(%rbp), %r9
	movq	%r9, %rdi
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	jne	.L717
	.p2align 4,,10
	.p2align 3
.L771:
	xorl	%eax, %eax
.L713:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L777
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L748:
	.cfi_restore_state
	movl	-264(%rbp), %eax
	shrl	$3, %eax
	movl	%eax, %edx
	andl	$7, %edx
	testb	$4, %al
	je	.L727
	andl	$1, %edx
	jne	.L773
	cmpl	$3, -268(%rbp)
	jne	.L727
.L733:
	movq	16(%rbx), %rdi
	movq	%r15, %rsi
	xorl	%r14d, %r14d
	movq	(%rdi), %r13
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movl	$5, %esi
	movq	$209682431, -240(%rbp)
	movl	$7, %ecx
	movq	%rax, -224(%rbp)
	movq	16(%rbx), %rax
	movl	$8, %edx
	leaq	-160(%rbp), %r9
	movq	376(%rax), %r8
.L737:
	movq	-256(%rbp), %rdi
	movq	%r9, -304(%rbp)
	movb	%dl, -280(%rbp)
	movb	%cl, -268(%rbp)
	movq	%r8, -264(%rbp)
	movb	%sil, -248(%rbp)
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	movzbl	-248(%rbp), %esi
	movb	$0, -112(%rbp)
	movq	-304(%rbp), %r9
	movq	-264(%rbp), %r8
	movq	%rax, -152(%rbp)
	movzbl	-280(%rbp), %edx
	movzbl	-268(%rbp), %ecx
	movb	%sil, -126(%rbp)
	movq	-240(%rbp), %rax
	movq	%r9, %rsi
	movq	%r8, %rdi
	movb	$1, -160(%rbp)
	movb	%dl, -128(%rbp)
	movb	%cl, -127(%rbp)
	movl	$24, -156(%rbp)
	movq	%r14, -144(%rbp)
	movq	%rax, -136(%rbp)
	movl	$1, -124(%rbp)
	movq	$0, -120(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-216(%rbp), %rax
	movl	$3, %edx
	movhps	-208(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -208(%rbp)
	movq	%rax, -224(%rbp)
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L775:
	movl	-264(%rbp), %eax
	shrl	$3, %eax
	movl	%eax, %edx
	andl	$7, %edx
	testb	$4, %al
	je	.L720
	andl	$1, %edx
	jne	.L717
.L720:
	testb	$2, -280(%rbp)
	je	.L717
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L776:
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder17CheckEqualsSymbolEv@PLT
	movq	%rax, %r9
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L727:
	movq	56(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler23CompilationDependencies22DependOnGlobalPropertyERKNS1_15PropertyCellRefE@PLT
	testb	$2, -280(%rbp)
	jne	.L734
.L773:
	movq	16(%rbx), %rdi
	cmpl	$3, %r13d
	je	.L778
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%rax, -224(%rbp)
.L730:
	movq	8(%rbx), %rdi
	movq	-224(%rbp), %rbx
	movq	%r12, %rsi
	movq	-216(%rbp), %r8
	movq	-208(%rbp), %rcx
	movq	(%rdi), %rax
	movq	%rbx, %rdx
	call	*32(%rax)
	movq	%rbx, %rax
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L741:
	movq	-208(%rbp), %xmm1
	movq	56(%rbx), %rdi
	movq	%r15, %rsi
	movhps	-216(%rbp), %xmm1
	movaps	%xmm1, -240(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies22DependOnGlobalPropertyERKNS1_15PropertyCellRefE@PLT
	movq	16(%rbx), %rdi
	movq	%r15, %rsi
	movq	(%rdi), %r13
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	-256(%rbp), %rdi
	movq	%rax, -208(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %r14
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	leaq	-160(%rbp), %rsi
	movb	$5, -126(%rbp)
	movq	%rax, -152(%rbp)
	movq	%r14, %rdi
	movl	$1800, %eax
	movb	$1, -160(%rbp)
	movl	$24, -156(%rbp)
	movq	$0, -144(%rbp)
	movq	$209682431, -136(%rbp)
	movw	%ax, -128(%rbp)
	movl	$1, -124(%rbp)
	movq	$0, -120(%rbp)
	movb	$0, -112(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movdqa	-240(%rbp), %xmm1
	movq	%rax, %rsi
	movl	$4, %edx
	movq	-208(%rbp), %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -208(%rbp)
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L742:
	movq	56(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler23CompilationDependencies22DependOnGlobalPropertyERKNS1_15PropertyCellRefE@PLT
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	leaq	-96(%rbp), %r14
	movq	(%rdi), %r13
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%rax, -240(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r13, %rdi
	movq	-224(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	leaq	-160(%rbp), %rdx
	movl	$33, %esi
	movq	%rax, -240(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder7CheckIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$3, %edx
	movq	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	-216(%rbp), %rax
	movhps	-208(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -208(%rbp)
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L740:
	movq	56(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler23CompilationDependencies22DependOnGlobalPropertyERKNS1_15PropertyCellRefE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	movq	-224(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	testb	%al, %al
	jne	.L779
	movq	16(%rbx), %rax
	leaq	-160(%rbp), %r9
	movq	%r9, %rsi
	movq	%r9, -264(%rbp)
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8CheckSmiERKNS1_14FeedbackSourceE@PLT
	leaq	-96(%rbp), %r10
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movdqa	-208(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r10, %rcx
	movl	$3, %edx
	movq	-216(%rbp), %rax
	movq	%r10, -248(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	16(%rbx), %rdi
	movq	%r15, %rsi
	movl	%eax, %r13d
	movq	(%rdi), %r14
	orl	$1, %r13d
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	-264(%rbp), %r9
	movl	$2, %ecx
	xorl	%edx, %edx
	movq	%rax, -240(%rbp)
	movq	16(%rbx), %rax
	movl	$6, %esi
	movq	-248(%rbp), %r10
	movq	376(%rax), %r15
	movq	-208(%rbp), %rax
	movq	%rax, -224(%rbp)
.L744:
	movq	-256(%rbp), %rdi
	movq	%r10, -304(%rbp)
	movq	%r9, -280(%rbp)
	movb	%dl, -268(%rbp)
	movb	%cl, -264(%rbp)
	movb	%sil, -248(%rbp)
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	movzbl	-248(%rbp), %esi
	movq	%r15, %rdi
	movq	-280(%rbp), %r9
	movzbl	-264(%rbp), %ecx
	movb	$0, -112(%rbp)
	movzbl	-268(%rbp), %edx
	movb	%sil, -128(%rbp)
	movq	%r9, %rsi
	movb	%cl, -127(%rbp)
	movb	%dl, -126(%rbp)
	movb	$1, -160(%rbp)
	movl	$24, -156(%rbp)
	movq	%rax, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	%r13, -136(%rbp)
	movl	$1, -124(%rbp)
	movq	$0, -120(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r14, %rdi
	movq	-240(%rbp), %xmm0
	movq	-304(%rbp), %r10
	movq	%rax, %rsi
	movhps	-224(%rbp), %xmm0
	movq	%r10, %rcx
	movaps	%xmm0, -96(%rbp)
	movq	-208(%rbp), %xmm0
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -208(%rbp)
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L778:
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	%rax, -224(%rbp)
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L779:
	movq	%r14, %rdi
	leaq	-176(%rbp), %r13
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapObjectEv@PLT
	leaq	-160(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -248(%rbp)
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	56(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rdx, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies17DependOnStableMapERKNS1_6MapRefE@PLT
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15CheckHeapObjectEv@PLT
	leaq	-96(%rbp), %r10
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movdqa	-208(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r10, %rcx
	movl	$3, %edx
	movq	-216(%rbp), %rax
	movq	%r10, -240(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movl	$16777217, %r13d
	movq	%rax, -224(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %r8
	movq	(%rax), %r14
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	movq	%r8, -208(%rbp)
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	-248(%rbp), %r9
	movq	-208(%rbp), %r8
	xorl	%esi, %esi
	movq	%rax, %rdx
	movq	%r9, %rcx
	movq	%r8, %rdi
	movq	%r9, -264(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9CheckMapsENS_4base5FlagsINS1_13CheckMapsFlagEiEENS0_13ZoneHandleSetINS0_3MapEEERKNS1_14FeedbackSourceE@PLT
	movl	$3, %edx
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	-240(%rbp), %r10
	movq	%rax, %rsi
	movq	-216(%rbp), %rax
	movq	-224(%rbp), %xmm0
	movq	%r10, %rcx
	movq	%r10, -248(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rax, -208(%rbp)
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movl	$7, %ecx
	movl	$7, %esi
	movq	-248(%rbp), %r10
	movq	%rax, -240(%rbp)
	movq	16(%rbx), %rax
	movl	$3, %edx
	movq	-264(%rbp), %r9
	movq	376(%rax), %r15
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L734:
	cmpl	$2, -268(%rbp)
	jne	.L733
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef5IsSmiEv@PLT
	testb	%al, %al
	je	.L736
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	16(%rbx), %rdi
	movq	%r15, %rsi
	xorl	%r14d, %r14d
	movl	%eax, %ecx
	orl	$1, %ecx
	movq	(%rdi), %r13
	movq	%rcx, -240(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	xorl	%esi, %esi
	movl	$2, %ecx
	movl	$6, %edx
	movq	%rax, -224(%rbp)
	movq	16(%rbx), %rax
	leaq	-160(%rbp), %r9
	movq	376(%rax), %r8
	jmp	.L737
.L736:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapNumberEv@PLT
	testb	%al, %al
	je	.L780
	movq	$7263, -240(%rbp)
	xorl	%r14d, %r14d
	leaq	-160(%rbp), %r9
.L738:
	movq	16(%rbx), %rdi
	movq	%r15, %rsi
	movq	%r9, -248(%rbp)
	movq	(%rdi), %r13
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movl	$3, %esi
	movl	$7, %ecx
	movq	-248(%rbp), %r9
	movq	%rax, -224(%rbp)
	movq	16(%rbx), %rax
	movl	$7, %edx
	movq	376(%rax), %r8
	jmp	.L737
.L780:
	movq	%r14, %rdi
	leaq	-176(%rbp), %r13
	xorl	%r14d, %r14d
	call	_ZNK2v88internal8compiler9ObjectRef12AsHeapObjectEv@PLT
	leaq	-160(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -224(%rbp)
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r13, %rdi
	movq	%rdx, -168(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler10BitsetType3LubINS1_6MapRefEEEjRKT_@PLT
	movl	%eax, %edi
	call	_ZN2v88internal8compiler10BitsetType15ExpandInternalsEj@PLT
	movq	%r13, %rdi
	movl	%eax, %ecx
	orl	$1, %ecx
	movq	%rcx, -240(%rbp)
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	movq	-224(%rbp), %r9
	testb	%al, %al
	je	.L738
	movq	56(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler23CompilationDependencies17DependOnStableMapERKNS1_6MapRefE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	-224(%rbp), %r9
	movq	%rax, %r14
	jmp	.L738
.L777:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23187:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_RKNS1_15PropertyCellRefE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_RKNS1_15PropertyCellRefE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_:
.LFB23185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rsi, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	cmpb	$0, 24(%rax)
	je	.L787
	movdqu	32(%rax), %xmm0
	movq	%rdi, %r12
	leaq	-112(%rbp), %rdi
	movq	%r8, %rbx
	movq	%rcx, %r14
	movl	%r9d, %r15d
	movq	%rdx, %r13
	movaps	%xmm0, -112(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef19global_proxy_objectEv@PLT
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %rdi
	leaq	-96(%rbp), %rsi
	movq	%rdx, -88(%rbp)
	movq	%rbx, %rdx
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal8compiler16JSGlobalProxyRef15GetPropertyCellERKNS1_7NameRefENS1_19SerializationPolicyE@PLT
	xorl	%eax, %eax
	cmpb	$0, -80(%rbp)
	jne	.L788
.L784:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L789
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L788:
	.cfi_restore_state
	leaq	-72(%rbp), %rax
	movq	-120(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdx
	pushq	%rax
	movl	%r15d, %r9d
	movq	%rbx, %r8
	movq	%r12, %rdi
	pushq	-128(%rbp)
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_RKNS1_15PropertyCellRefE
	popq	%rdx
	popq	%rcx
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L787:
	leaq	.LC10(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L789:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23185:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_, .-_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSLoadGlobalEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSLoadGlobalEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSLoadGlobalEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSLoadGlobalEPNS1_4NodeE:
.LFB23188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler22LoadGlobalParametersOfEPKNS1_8OperatorE@PLT
	cmpq	$0, 8(%rax)
	je	.L791
	cmpl	$-1, 16(%rax)
	movq	%rax, %rbx
	je	.L791
	movdqu	8(%rax), %xmm1
	movq	24(%r13), %rdi
	leaq	-96(%rbp), %r14
	movq	%r14, %rsi
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler12JSHeapBroker26GetFeedbackForGlobalAccessERKNS1_14FeedbackSourceE@PLT
	movl	(%rax), %esi
	testl	%esi, %esi
	jne	.L792
.L791:
	xorl	%eax, %eax
.L793:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L800
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L792:
	.cfi_restore_state
	movq	%rax, %rdi
	call	_ZNK2v88internal8compiler17ProcessedFeedback14AsGlobalAccessEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler20GlobalAccessFeedback19IsScriptContextSlotEv@PLT
	testb	%al, %al
	je	.L794
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	16(%r13), %rbx
	movq	%r15, %rdi
	movq	%rax, -120(%rbp)
	call	_ZNK2v88internal8compiler20GlobalAccessFeedback14script_contextEv@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	movq	16(%r13), %rax
	movq	368(%rax), %r8
	movq	(%rax), %r14
	movq	%r8, -136(%rbp)
	call	_ZNK2v88internal8compiler20GlobalAccessFeedback9immutableEv@PLT
	movq	%r15, %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal8compiler20GlobalAccessFeedback10slot_indexEv@PLT
	movq	-136(%rbp), %r8
	movzbl	%bl, %ecx
	xorl	%esi, %esi
	movslq	%eax, %rdx
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11LoadContextEmmb@PLT
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-128(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%rax, %rbx
	movq	(%rdi), %rax
	movq	%rbx, %rcx
	movq	%rbx, %rdx
	call	*32(%rax)
	movq	%rbx, %rax
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L794:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler20GlobalAccessFeedback14IsPropertyCellEv@PLT
	testb	%al, %al
	je	.L791
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler20GlobalAccessFeedback13property_cellEv@PLT
	movq	24(%r13), %rsi
	movl	$1, %ecx
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	(%rbx), %rdx
	leaq	-112(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	pushq	%r14
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	$0
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_RKNS1_15PropertyCellRefE
	popq	%rdx
	popq	%rcx
	jmp	.L793
.L800:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23188:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSLoadGlobalEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSLoadGlobalEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSStoreGlobalEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSStoreGlobalEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSStoreGlobalEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSStoreGlobalEPNS1_4NodeE:
.LFB23189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	0(%r13), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler23StoreGlobalParametersOfEPKNS1_8OperatorE@PLT
	cmpq	$0, 16(%rax)
	je	.L802
	cmpl	$-1, 24(%rax)
	movq	%rax, %rbx
	je	.L802
	movdqu	16(%rax), %xmm1
	movq	24(%r14), %rdi
	leaq	-112(%rbp), %r15
	movq	%r15, %rsi
	movaps	%xmm1, -112(%rbp)
	call	_ZN2v88internal8compiler12JSHeapBroker26GetFeedbackForGlobalAccessERKNS1_14FeedbackSourceE@PLT
	movl	(%rax), %esi
	testl	%esi, %esi
	jne	.L803
.L802:
	xorl	%eax, %eax
.L804:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L811
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L803:
	.cfi_restore_state
	movq	%rax, %rdi
	call	_ZNK2v88internal8compiler17ProcessedFeedback14AsGlobalAccessEv@PLT
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNK2v88internal8compiler20GlobalAccessFeedback19IsScriptContextSlotEv@PLT
	movq	-136(%rbp), %r8
	testb	%al, %al
	movq	%r8, %rdi
	je	.L805
	call	_ZNK2v88internal8compiler20GlobalAccessFeedback9immutableEv@PLT
	movq	-136(%rbp), %r8
	testb	%al, %al
	jne	.L802
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-152(%rbp), %r8
	movq	16(%r14), %r9
	movq	%rax, %rbx
	movq	%r8, %rdi
	movq	%r8, -160(%rbp)
	movq	%r9, -144(%rbp)
	call	_ZNK2v88internal8compiler20GlobalAccessFeedback14script_contextEv@PLT
	movq	-144(%rbp), %r9
	movq	%r15, %rsi
	movq	%rdx, -104(%rbp)
	movq	%r9, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	-160(%rbp), %r8
	movq	%rax, -144(%rbp)
	movq	16(%r14), %rax
	movq	%r8, %rdi
	movq	368(%rax), %r9
	movq	(%rax), %r15
	movq	%r9, -152(%rbp)
	call	_ZNK2v88internal8compiler20GlobalAccessFeedback10slot_indexEv@PLT
	movq	-152(%rbp), %r9
	xorl	%esi, %esi
	movslq	%eax, %rdx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder12StoreContextEmm@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movhps	-144(%rbp), %xmm0
	movq	%rbx, %xmm2
	leaq	-96(%rbp), %rcx
	movq	%rax, %rsi
	movaps	%xmm0, -96(%rbp)
	movl	$4, %edx
	movq	-136(%rbp), %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r14), %rdi
	movq	%rbx, %r8
	movq	%r12, %rdx
	movq	%rax, %rcx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r12, %rax
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L805:
	movq	%r8, -136(%rbp)
	call	_ZNK2v88internal8compiler20GlobalAccessFeedback14IsPropertyCellEv@PLT
	movq	-136(%rbp), %r8
	testb	%al, %al
	je	.L802
	movq	%r8, %rdi
	call	_ZNK2v88internal8compiler20GlobalAccessFeedback13property_cellEv@PLT
	movq	24(%r14), %rsi
	movl	$1, %ecx
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	8(%rbx), %rdx
	leaq	-128(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	pushq	%r15
	movq	%r12, %rcx
	xorl	%edx, %edx
	pushq	$0
	movl	$1, %r9d
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_RKNS1_15PropertyCellRefE
	popq	%rdx
	popq	%rcx
	jmp	.L804
.L811:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23189:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSStoreGlobalEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSStoreGlobalEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceSoftDeoptimizeEPNS1_4NodeENS0_16DeoptimizeReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceSoftDeoptimizeEPNS1_4NodeENS0_16DeoptimizeReasonE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceSoftDeoptimizeEPNS1_4NodeENS0_16DeoptimizeReasonE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceSoftDeoptimizeEPNS1_4NodeENS0_16DeoptimizeReasonE:
.LFB23233:
	.cfi_startproc
	endbr64
	testb	$2, 32(%rdi)
	jne	.L816
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L816:
	movzbl	%dl, %edx
	jmp	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceSoftDeoptimizeEPNS1_4NodeENS0_16DeoptimizeReasonE.part.0
	.cfi_endproc
.LFE23233:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceSoftDeoptimizeEPNS1_4NodeENS0_16DeoptimizeReasonE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceSoftDeoptimizeEPNS1_4NodeENS0_16DeoptimizeReasonE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization37ReduceJSLoadPropertyWithEnumeratedKeyEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization37ReduceJSLoadPropertyWithEnumeratedKeyEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization37ReduceJSLoadPropertyWithEnumeratedKeyEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization37ReduceJSLoadPropertyWithEnumeratedKeyEPNS1_4NodeE:
.LFB23235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	0(%r13), %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler11ForInModeOfEPKNS1_8OperatorE@PLT
	testb	%al, %al
	je	.L818
.L821:
	xorl	%eax, %eax
.L819:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L826
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L818:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$3, %esi
	movq	%r13, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-176(%rbp), %r8
	movq	%rax, -192(%rbp)
	movq	(%r8), %rax
	cmpw	$708, 16(%rax)
	je	.L827
.L820:
	cmpq	%r14, %r8
	jne	.L821
	movq	-168(%rbp), %rdi
	movq	%r13, %rsi
	leaq	-160(%rbp), %r13
	call	_ZN2v88internal8compiler14NodeProperties29NoObservableSideEffectBetweenEPNS1_4NodeES4_@PLT
	leaq	-96(%rbp), %rdx
	movq	%rdx, -176(%rbp)
	testb	%al, %al
	je	.L828
.L822:
	movq	16(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movq	%r9, -224(%rbp)
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder17ForMapDescriptorsEv@PLT
	movq	-200(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	-224(%rbp), %r9
	movl	$3, %edx
	xorl	%r8d, %r8d
	movq	-184(%rbp), %xmm0
	movq	-176(%rbp), %rcx
	movq	%rax, %rsi
	movq	%r15, -80(%rbp)
	movq	%r9, %rdi
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -168(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movq	%r9, -200(%rbp)
	movq	%r8, -184(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder27ForDescriptorArrayEnumCacheEv@PLT
	movq	-184(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	-200(%rbp), %r9
	movl	$3, %edx
	xorl	%r8d, %r8d
	movq	-168(%rbp), %xmm0
	movq	-176(%rbp), %rcx
	movq	%rax, %rsi
	movq	%r15, -80(%rbp)
	movq	%r9, %rdi
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -168(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movq	%r9, -200(%rbp)
	movq	%r8, -184(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForEnumCacheIndicesEv@PLT
	movq	-184(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	-200(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-168(%rbp), %xmm0
	movq	-176(%rbp), %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r15, -80(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -168(%rbp)
	movq	(%rdi), %r9
	movq	%r9, -200(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23EmptyFixedArrayConstantEv@PLT
	movq	%rax, -184(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	-200(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-168(%rbp), %xmm0
	movq	-176(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-184(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -184(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10BooleanNotEv@PLT
	movq	-184(%rbp), %rdx
	movq	-200(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-176(%rbp), %rcx
	movq	%rax, %rsi
	movq	%r9, %rdi
	movq	%rdx, -96(%rbp)
	movl	$1, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdx
	movl	$35, %esi
	movq	%rax, -184(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	movq	%r9, -200(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder7CheckIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	movq	-200(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-184(%rbp), %xmm0
	movq	-176(%rbp), %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r15, -80(%rbp)
	movhps	-168(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	xorl	%esi, %esi
	movq	%rax, -184(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movq	%r9, -224(%rbp)
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementENS0_12ElementsKindENS0_15LoadSensitivityE@PLT
	movq	-200(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11LoadElementERKNS1_13ElementAccessE@PLT
	movq	-224(%rbp), %r9
	movq	%r15, %xmm2
	xorl	%r8d, %r8d
	movq	-176(%rbp), %rcx
	movq	%rax, %rsi
	movl	$4, %edx
	movq	-168(%rbp), %xmm0
	movq	%r9, %rdi
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-184(%rbp), %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -168(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16LoadFieldByIndexEv@PLT
	movq	%r14, %xmm0
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movq	%r15, %xmm3
	movq	-176(%rbp), %rcx
	movq	%rax, %rsi
	movl	$4, %edx
	movq	-168(%rbp), %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm1, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %rdi
	movq	%r15, %r8
	movq	%r12, %rsi
	movq	%rax, %r13
	movq	(%rdi), %rax
	movq	%r13, %rcx
	movq	%r13, %rdx
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L827:
	movq	%r8, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r8
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L828:
	movq	16(%rbx), %rax
	movq	%r14, %xmm0
	movq	%r13, %rdi
	movhps	-168(%rbp), %xmm0
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movaps	%xmm0, -224(%rbp)
	movq	%r9, -200(%rbp)
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-168(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	-200(%rbp), %r9
	xorl	%r8d, %r8d
	movl	$3, %edx
	movdqa	-224(%rbp), %xmm0
	movq	-176(%rbp), %rcx
	movq	%rax, %rsi
	movq	%r15, -80(%rbp)
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -168(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -200(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	-200(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-168(%rbp), %xmm0
	movq	-176(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-184(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdx
	movl	$37, %esi
	movq	%rax, -200(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder7CheckIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	movq	-224(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-200(%rbp), %xmm0
	movq	-176(%rbp), %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r15, -80(%rbp)
	movhps	-168(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -168(%rbp)
	jmp	.L822
.L826:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23235:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization37ReduceJSLoadPropertyWithEnumeratedKeyEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization37ReduceJSLoadPropertyWithEnumeratedKeyEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler29JSNativeContextSpecialization13InlineApiCallEPNS1_4NodeES4_S4_S4_PS4_S5_RKNS1_21SharedFunctionInfoRefERKNS1_23FunctionTemplateInfoRefE.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"call code for function template info "
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization13InlineApiCallEPNS1_4NodeES4_S4_S4_PS4_S5_RKNS1_21SharedFunctionInfoRefERKNS1_23FunctionTemplateInfoRefE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization13InlineApiCallEPNS1_4NodeES4_S4_S4_PS4_S5_RKNS1_21SharedFunctionInfoRefERKNS1_23FunctionTemplateInfoRefE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization13InlineApiCallEPNS1_4NodeES4_S4_S4_PS4_S5_RKNS1_21SharedFunctionInfoRefERKNS1_23FunctionTemplateInfoRefE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization13InlineApiCallEPNS1_4NodeES4_S4_S4_PS4_S5_RKNS1_21SharedFunctionInfoRefERKNS1_23FunctionTemplateInfoRefE:
.LFB23240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$280, %rsp
	movq	16(%rbp), %rax
	movq	%rsi, -256(%rbp)
	movq	%rdx, -272(%rbp)
	movq	32(%rbp), %r12
	movq	%rcx, -280(%rbp)
	movq	%rax, -248(%rbp)
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler23FunctionTemplateInfoRef13has_call_codeEv@PLT
	testb	%al, %al
	je	.L845
	leaq	-176(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler23FunctionTemplateInfoRef9call_codeEv@PLT
	cmpb	$0, -176(%rbp)
	je	.L846
	movq	%r12, %rsi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88internal8compiler23FunctionTemplateInfoRef9call_codeEv@PLT
	movq	16(%rbx), %rax
	testq	%r13, %r13
	movq	%r15, %rdi
	movdqu	-168(%rbp), %xmm3
	setne	%r12b
	leaq	-192(%rbp), %r15
	movq	360(%rax), %rsi
	movaps	%xmm3, -224(%rbp)
	call	_ZN2v88internal11CodeFactory15CallApiCallbackEPNS0_7IsolateE@PLT
	xorl	%r9d, %r9d
	movl	$1, %ecx
	xorl	%r8d, %r8d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-208(%rbp), %rsi
	movq	%rax, -208(%rbp)
	movq	-160(%rbp), %rax
	movq	%rax, -200(%rbp)
	movl	8(%rax), %edx
	addl	%r12d, %edx
	subl	(%rax), %edx
	movq	16(%rbx), %rax
	addl	$1, %edx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	leaq	-224(%rbp), %r8
	movq	16(%rbx), %r10
	movq	%r8, %rdi
	movq	%r8, -288(%rbp)
	movq	%r10, -304(%rbp)
	movq	%rax, -312(%rbp)
	call	_ZNK2v88internal8compiler18CallHandlerInfoRef4dataEv@PLT
	movq	-304(%rbp), %r10
	movq	%r15, %rsi
	movq	%rdx, -184(%rbp)
	movq	%r10, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	-288(%rbp), %r8
	movq	%rax, -320(%rbp)
	movq	%r8, %rdi
	call	_ZNK2v88internal8compiler18CallHandlerInfoRef8callbackEv@PLT
	leaq	-232(%rbp), %rdi
	movl	$6, %esi
	movq	%rax, -232(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r10
	movq	8(%rax), %r8
	movq	%r10, -304(%rbp)
	movq	%r8, -288(%rbp)
	call	_ZN2v88internal17ExternalReference6CreateEPNS_11ApiFunctionENS1_4TypeE@PLT
	movq	-288(%rbp), %r8
	movq	%r15, %rsi
	movq	%rax, -192(%rbp)
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder16ExternalConstantERKNS0_17ExternalReferenceE@PLT
	movq	-304(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	-176(%rbp), %rsi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %xmm0
	movq	24(%rbx), %rax
	cmpb	$0, 24(%rax)
	je	.L847
	movdqu	32(%rax), %xmm4
	movhps	-288(%rbp), %xmm0
	movq	%r15, %rsi
	movq	-272(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-256(%rbp), %xmm1
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	16(%rbx), %rdi
	movdqa	-272(%rbp), %xmm0
	pxor	%xmm2, %xmm2
	movq	%rax, -256(%rbp)
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movdqa	-304(%rbp), %xmm1
	leal	10(%r12), %edx
	movq	%rax, %xmm0
	leal	6(%r12), %eax
	addl	$8, %r12d
	movhps	-320(%rbp), %xmm0
	cltq
	movaps	%xmm1, -112(%rbp)
	movslq	%r12d, %r12
	movaps	%xmm0, -128(%rbp)
	movq	-256(%rbp), %xmm0
	movhps	-280(%rbp), %xmm0
	movups	%xmm0, -144(%rbp,%rax,8)
	movq	-248(%rbp), %rax
	movq	(%r14), %xmm0
	movhps	(%rax), %xmm0
	movups	%xmm0, -144(%rbp,%r12,8)
	testq	%r13, %r13
	je	.L838
	movq	%r13, -96(%rbp)
.L838:
	movq	16(%rbx), %rax
	movq	-312(%rbp), %rsi
	movl	%edx, -256(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movl	-256(%rbp), %edx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-144(%rbp), %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-248(%rbp), %rbx
	movq	%rax, (%rbx)
	movq	%rax, (%r14)
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L846:
	movq	24(%rbx), %rdi
	cmpb	$0, 124(%rdi)
	je	.L845
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$8, %edx
	leaq	.LC12(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$37, %edx
	movq	%r13, %rdi
	leaq	.LC25(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$2, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$59, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$2089, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r13
	testq	%r13, %r13
	je	.L848
	cmpb	$0, 56(%r13)
	je	.L835
	movsbl	67(%r13), %esi
.L836:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	.p2align 4,,10
	.p2align 3
.L845:
	xorl	%eax, %eax
.L829:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L849
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L835:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L836
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L847:
	leaq	.LC10(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L849:
	call	__stack_chk_fail@PLT
.L848:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE23240:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization13InlineApiCallEPNS1_4NodeES4_S4_S4_PS4_S5_RKNS1_21SharedFunctionInfoRefERKNS1_23FunctionTemplateInfoRefE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization13InlineApiCallEPNS1_4NodeES4_S4_S4_PS4_S5_RKNS1_21SharedFunctionInfoRefERKNS1_23FunctionTemplateInfoRefE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization17BuildPropertyTestEPNS1_4NodeES4_RKNS1_18PropertyAccessInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization17BuildPropertyTestEPNS1_4NodeES4_RKNS1_18PropertyAccessInfoE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization17BuildPropertyTestEPNS1_4NodeES4_RKNS1_18PropertyAccessInfoE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization17BuildPropertyTestEPNS1_4NodeES4_RKNS1_18PropertyAccessInfoE:
.LFB23248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$72, %rsp
	movq	%rdx, -104(%rbp)
	movq	88(%r8), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L851
	movq	56(%rsi), %r15
	movq	24(%rsi), %rsi
	leaq	-96(%rbp), %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	subq	$8, %rsp
	movb	$1, -80(%rbp)
	movq	%r15, %rdi
	pushq	-88(%rbp)
	leaq	8(%rbx), %rsi
	movl	$1, %edx
	pushq	-96(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE@PLT
	addq	$32, %rsp
.L851:
	cmpl	$1, (%rbx)
	movq	16(%r14), %rdi
	je	.L859
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	%rax, %xmm0
.L853:
	movq	%r13, 16(%r12)
	movhps	-104(%rbp), %xmm0
	movups	%xmm0, (%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L860
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L859:
	.cfi_restore_state
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	%rax, %xmm0
	jmp	.L853
.L860:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23248:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization17BuildPropertyTestEPNS1_4NodeES4_RKNS1_18PropertyAccessInfoE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization17BuildPropertyTestEPNS1_4NodeES4_RKNS1_18PropertyAccessInfoE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization16ReduceJSToObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization16ReduceJSToObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization16ReduceJSToObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization16ReduceJSToObjectEPNS1_4NodeE:
.LFB23253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r15), %rsi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movq	%rax, %rcx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler12MapInferenceC1EPNS1_12JSHeapBrokerEPNS1_4NodeES6_@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler12MapInference8HaveMapsEv@PLT
	testb	%al, %al
	jne	.L862
.L864:
	xorl	%r13d, %r13d
.L863:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12MapInferenceD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L870
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L862:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler12MapInference31AllOfInstanceTypesAreJSReceiverEv@PLT
	testb	%al, %al
	je	.L864
	movq	8(%r15), %rdi
	movq	%r13, %rsi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %r13
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L863
.L870:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23253:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization16ReduceJSToObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization16ReduceJSToObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization22BuildIndexedStringLoadEPNS1_4NodeES4_S4_PS4_S5_NS0_19KeyedAccessLoadModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization22BuildIndexedStringLoadEPNS1_4NodeES4_S4_PS4_S5_NS0_19KeyedAccessLoadModeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization22BuildIndexedStringLoadEPNS1_4NodeES4_S4_PS4_S5_NS0_19KeyedAccessLoadModeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization22BuildIndexedStringLoadEPNS1_4NodeES4_S4_PS4_S5_NS0_19KeyedAccessLoadModeE:
.LFB23256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$120, %rsp
	movq	%rsi, -120(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 16(%rbp)
	jne	.L872
	movq	56(%rdi), %rdi
	call	_ZN2v88internal8compiler23CompilationDependencies27DependOnNoElementsProtectorEv@PLT
	testb	%al, %al
	jne	.L879
.L872:
	movq	16(%r14), %rax
	movq	-128(%rbp), %xmm0
	leaq	-112(%rbp), %rsi
	movq	(%r12), %rcx
	movq	(%rbx), %r13
	movq	376(%rax), %rdi
	movhps	-144(%rbp), %xmm0
	movq	(%rax), %r15
	movq	$0, -112(%rbp)
	movq	%rcx, -128(%rbp)
	movaps	%xmm0, -144(%rbp)
	movl	$-1, -104(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckBoundsERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r15, %rdi
	movdqa	-144(%rbp), %xmm0
	movq	%rax, %rsi
	movaps	%xmm0, -96(%rbp)
	movq	%r13, %xmm0
	leaq	-96(%rbp), %r13
	movhps	-128(%rbp), %xmm0
	movq	%r13, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%rbx)
	movq	%rax, -128(%rbp)
	movq	16(%r14), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11PoisonIndexEv@PLT
	movq	-128(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%r13, %rcx
	movq	%rdx, -96(%rbp)
	movl	$1, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%r12), %rcx
	movq	(%rbx), %r12
	movq	%rax, -144(%rbp)
	movq	16(%r14), %rax
	movq	%rcx, -128(%rbp)
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16StringCharCodeAtEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-120(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r12, %xmm0
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r12
	movq	16(%r14), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder24StringFromSingleCharCodeEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r12, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L871:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L880
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L879:
	.cfi_restore_state
	movq	16(%r14), %rdi
	movq	(%r12), %rax
	movsd	.LC26(%rip), %xmm0
	movq	(%rbx), %r13
	movq	(%rdi), %r15
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	leaq	-112(%rbp), %rsi
	movq	%rax, -160(%rbp)
	movq	16(%r14), %rax
	movq	376(%rax), %rdi
	movq	$0, -112(%rbp)
	movl	$-1, -104(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckBoundsERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r15, %rdi
	movq	-128(%rbp), %xmm0
	movq	%rax, %rsi
	movhps	-160(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r13, %xmm0
	leaq	-96(%rbp), %r13
	movhps	-152(%rbp), %xmm0
	movq	%r13, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%rbx)
	movq	%rax, -128(%rbp)
	movq	16(%r14), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberLessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	-128(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%r12), %rcx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rax, -152(%rbp)
	movq	16(%r14), %rax
	movq	%rcx, -144(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-152(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -144(%rbp)
	movq	16(%r14), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11PoisonIndexEv@PLT
	movq	-128(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rcx, -96(%rbp)
	movq	%r13, %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -160(%rbp)
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-144(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%rbx), %rcx
	movq	%rax, -128(%rbp)
	movq	16(%r14), %rax
	movq	%rcx, -152(%rbp)
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16StringCharCodeAtEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-120(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-160(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-152(%rbp), %xmm0
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -120(%rbp)
	movq	16(%r14), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder24StringFromSingleCharCodeEv@PLT
	movq	-120(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%rdx, -96(%rbp)
	movl	$1, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -152(%rbp)
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-144(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r14), %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movl	$2, %esi
	movq	%rax, -160(%rbp)
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-128(%rbp), %xmm0
	movq	%r15, %rdi
	movq	%rax, %rsi
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, (%r12)
	movq	%rax, %r15
	movq	16(%r14), %rax
	movq	(%rbx), %rcx
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%rcx, -128(%rbp)
	movq	%r9, -144(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	-144(%rbp), %r9
	movq	-120(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, -80(%rbp)
	movhps	-128(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %edx
	movl	$8, %esi
	movq	%rax, (%rbx)
	movq	16(%r14), %rax
	movq	(%r12), %rbx
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-152(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%rbx, -80(%rbp)
	movhps	-160(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L871
.L880:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23256:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization22BuildIndexedStringLoadEPNS1_4NodeES4_S4_PS4_S5_NS0_19KeyedAccessLoadModeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization22BuildIndexedStringLoadEPNS1_4NodeES4_S4_PS4_S5_NS0_19KeyedAccessLoadModeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceElementAccessOnStringEPNS1_4NodeES4_S4_RKNS1_15KeyedAccessModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceElementAccessOnStringEPNS1_4NodeES4_S4_RKNS1_15KeyedAccessModeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceElementAccessOnStringEPNS1_4NodeES4_S4_RKNS1_15KeyedAccessModeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceElementAccessOnStringEPNS1_4NodeES4_S4_RKNS1_15KeyedAccessModeE:
.LFB23210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal8compiler15KeyedAccessMode11access_modeEv@PLT
	cmpl	$1, %eax
	jne	.L882
.L884:
	xorl	%eax, %eax
.L883:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L890
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L882:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler15KeyedAccessMode11access_modeEv@PLT
	cmpl	$3, %eax
	je	.L884
	movq	16(%rbx), %rax
	movq	-104(%rbp), %rdx
	leaq	-96(%rbp), %rsi
	movq	-112(%rbp), %rcx
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%rdx, -128(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%r9, -136(%rbp)
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckStringERKNS1_14FeedbackSourceE@PLT
	movq	-128(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-136(%rbp), %r9
	movq	%r14, %xmm0
	movq	%rax, %rsi
	movq	%rcx, -128(%rbp)
	movhps	-120(%rbp), %xmm0
	movq	%rdx, -64(%rbp)
	movl	$3, %edx
	movq	%r9, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -112(%rbp)
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StringLengthEv@PLT
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r14, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -120(%rbp)
	call	_ZNK2v88internal8compiler15KeyedAccessMode9load_modeEv@PLT
	subq	$8, %rsp
	movq	-120(%rbp), %rcx
	movq	%r15, %rdx
	pushq	%rax
	leaq	-104(%rbp), %r9
	leaq	-112(%rbp), %r8
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization22BuildIndexedStringLoadEPNS1_4NodeES4_S4_PS4_S5_NS0_19KeyedAccessLoadModeE
	movq	8(%rbx), %rdi
	movq	-112(%rbp), %rcx
	movq	%r12, %rsi
	movq	%rax, %r13
	movq	-104(%rbp), %r8
	movq	(%rdi), %rax
	movq	%r13, %rdx
	call	*32(%rax)
	popq	%rdx
	movq	%r13, %rax
	popq	%rcx
	jmp	.L883
.L890:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23210:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceElementAccessOnStringEPNS1_4NodeES4_S4_RKNS1_15KeyedAccessModeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceElementAccessOnStringEPNS1_4NodeES4_S4_RKNS1_15KeyedAccessModeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization33ReduceElementLoadFromHeapConstantEPNS1_4NodeES4_NS1_10AccessModeENS0_19KeyedAccessLoadModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization33ReduceElementLoadFromHeapConstantEPNS1_4NodeES4_NS1_10AccessModeENS0_19KeyedAccessLoadModeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization33ReduceElementLoadFromHeapConstantEPNS1_4NodeES4_NS1_10AccessModeENS0_19KeyedAccessLoadModeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization33ReduceElementLoadFromHeapConstantEPNS1_4NodeES4_NS1_10AccessModeENS0_19KeyedAccessLoadModeE:
.LFB23230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$264, %rsp
	movq	%rdx, -248(%rbp)
	movl	%ecx, -252(%rbp)
	movl	%r8d, -256(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movq	(%r14), %rax
	cmpw	$30, 16(%rax)
	jne	.L892
	movq	48(%rax), %rdx
.L892:
	leaq	-144(%rbp), %r13
	movq	24(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L937
	movdqa	-144(%rbp), %xmm2
	leaq	-224(%rbp), %r15
	leaq	-192(%rbp), %r14
	movq	%r15, %rdi
	movaps	%xmm2, -224(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r14, %rdi
	movq	%rax, -192(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZNK2v88internal8compiler6MapRef12oddball_typeEv@PLT
	cmpb	$4, %al
	jne	.L894
.L896:
	xorl	%eax, %eax
.L895:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L938
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L894:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%rax, -176(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -168(%rbp)
	movq	%rax, -272(%rbp)
	call	_ZNK2v88internal8compiler6MapRef12oddball_typeEv@PLT
	cmpb	$3, %al
	je	.L896
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r13, %rdi
	movq	%rax, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZNK2v88internal8compiler6MapRef12oddball_typeEv@PLT
	cmpb	$2, %al
	je	.L896
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	testb	%al, %al
	je	.L897
	cmpl	$3, -252(%rbp)
	je	.L896
.L897:
	movq	-248(%rbp), %rax
	movq	(%rax), %rax
	cmpw	$28, 16(%rax)
	jne	.L898
	movsd	48(%rax), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, -280(%rbp)
	call	nearbyint@PLT
	movsd	-280(%rbp), %xmm1
	ucomisd	%xmm0, %xmm1
	jp	.L898
	jne	.L898
	comisd	.LC27(%rip), %xmm1
	jb	.L898
	movsd	.LC28(%rip), %xmm0
	comisd	%xmm1, %xmm0
	jb	.L898
	cvttsd2siq	%xmm1, %rax
	movq	-272(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movl	%eax, %edx
	movq	%rax, -280(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef21GetOwnConstantElementEjNS1_19SerializationPolicyE@PLT
	cmpb	$0, -176(%rbp)
	je	.L902
.L910:
	cmpl	$3, -252(%rbp)
	movq	16(%rbx), %rdi
	je	.L939
	leaq	-168(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%rax, %r13
.L911:
	movq	8(%rbx), %rdi
	movq	-232(%rbp), %r8
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	-240(%rbp), %rcx
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L895
.L902:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef9IsJSArrayEv@PLT
	testb	%al, %al
	jne	.L940
.L905:
	cmpb	$0, -176(%rbp)
	jne	.L910
	.p2align 4,,10
	.p2align 3
.L898:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L895
	movq	%r15, %rdi
	movq	16(%rbx), %r14
	call	_ZNK2v88internal8compiler9ObjectRef8AsStringEv@PLT
	movq	%r13, %rdi
	movq	%rdx, -136(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal8compiler9StringRef6lengthEv@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	subq	$8, %rsp
	movq	%rbx, %rdi
	movq	-248(%rbp), %rdx
	movq	%rax, %rcx
	movl	-256(%rbp), %eax
	movq	-264(%rbp), %rsi
	leaq	-232(%rbp), %r9
	leaq	-240(%rbp), %r8
	pushq	%rax
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization22BuildIndexedStringLoadEPNS1_4NodeES4_S4_PS4_S5_NS0_19KeyedAccessLoadModeE
	movq	8(%rbx), %rdi
	movq	-240(%rbp), %rcx
	movq	%r12, %rsi
	movq	%rax, %r13
	movq	-232(%rbp), %r8
	movq	(%rdi), %rax
	movq	%r13, %rdx
	call	*32(%rax)
	popq	%rdx
	movq	%r13, %rax
	popq	%rcx
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L937:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L939:
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	%rax, %r13
	jmp	.L911
.L940:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef9AsJSArrayEv@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rdx, -184(%rbp)
	movl	-280(%rbp), %edx
	movq	%rax, -192(%rbp)
	call	_ZNK2v88internal8compiler10JSArrayRef16GetOwnCowElementEjNS1_19SerializationPolicyE@PLT
	cmpb	$0, -144(%rbp)
	je	.L906
	cmpb	$0, -176(%rbp)
	je	.L907
	movdqu	-136(%rbp), %xmm4
	movups	%xmm4, -168(%rbp)
.L908:
	cmpb	$0, -176(%rbp)
	je	.L898
	movq	16(%rbx), %rax
	movq	-232(%rbp), %rdx
	movq	%r13, %rdi
	movq	-240(%rbp), %rcx
	movq	(%rax), %r10
	movq	376(%rax), %r8
	movq	%rdx, -288(%rbp)
	movq	%rcx, -272(%rbp)
	movq	%r10, -296(%rbp)
	movq	%r8, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	-280(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	-296(%rbp), %r10
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-288(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rcx, -288(%rbp)
	movq	-264(%rbp), %xmm0
	movq	%r10, %rdi
	movq	%rdx, -64(%rbp)
	movl	$3, %edx
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -280(%rbp)
	movq	%rax, -240(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef9AsJSArrayEv@PLT
	movq	%r14, %rdi
	movq	%rax, -192(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZNK2v88internal8compiler11JSObjectRef8elementsEv@PLT
	movq	%r13, %rdi
	movq	%rax, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12AsFixedArrayEv@PLT
	movq	16(%rbx), %rdi
	leaq	-208(%rbp), %rsi
	movq	%rdx, -200(%rbp)
	movq	%rax, -208(%rbp)
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%rax, -272(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	-280(%rbp), %xmm0
	movq	-288(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-272(%rbp), %xmm0
	movq	%rcx, -296(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-232(%rbp), %r8
	movq	%r13, %rdx
	movq	-240(%rbp), %rcx
	movq	%rax, -280(%rbp)
	movq	16(%rbx), %rax
	movl	$2, %esi
	movq	%r8, -288(%rbp)
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	movq	%rcx, -272(%rbp)
	movq	$0, -144(%rbp)
	movl	$-1, -136(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder7CheckIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	movq	-288(%rbp), %r8
	movl	$3, %edx
	movq	%r14, %rdi
	movq	-280(%rbp), %xmm0
	movq	-296(%rbp), %rcx
	movq	%rax, %rsi
	movq	%r8, -64(%rbp)
	xorl	%r8d, %r8d
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -240(%rbp)
	jmp	.L905
.L938:
	call	__stack_chk_fail@PLT
.L906:
	cmpb	$0, -176(%rbp)
	je	.L908
	movb	$0, -176(%rbp)
	jmp	.L908
.L907:
	movdqu	-136(%rbp), %xmm5
	movb	$1, -176(%rbp)
	movups	%xmm5, -168(%rbp)
	jmp	.L908
	.cfi_endproc
.LFE23230:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization33ReduceElementLoadFromHeapConstantEPNS1_4NodeES4_NS1_10AccessModeENS0_19KeyedAccessLoadModeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization33ReduceElementLoadFromHeapConstantEPNS1_4NodeES4_NS1_10AccessModeENS0_19KeyedAccessLoadModeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization20BuildCheckEqualsNameERKNS1_7NameRefEPNS1_4NodeES7_S7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization20BuildCheckEqualsNameERKNS1_7NameRefEPNS1_4NodeES7_S7_
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization20BuildCheckEqualsNameERKNS1_7NameRefEPNS1_4NodeES7_S7_, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization20BuildCheckEqualsNameERKNS1_7NameRefEPNS1_4NodeES7_S7_:
.LFB23258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$80, %rsp
	movq	%rdx, -88(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%r8, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler9ObjectRef8IsSymbolEv@PLT
	testb	%al, %al
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	je	.L942
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder17CheckEqualsSymbolEv@PLT
	movq	%rax, %r13
.L943:
	movq	-112(%rbp), %xmm0
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	movhps	-96(%rbp), %xmm0
	movq	(%rdi), %r14
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movdqa	-112(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%rax, %xmm1
	leaq	-80(%rbp), %rcx
	movl	$4, %edx
	movq	%r14, %rdi
	movhps	-88(%rbp), %xmm1
	movaps	%xmm0, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L946
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L942:
	.cfi_restore_state
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder29CheckEqualsInternalizedStringEv@PLT
	movq	%rax, %r13
	jmp	.L943
.L946:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23258:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization20BuildCheckEqualsNameERKNS1_7NameRefEPNS1_4NodeES7_S7_, .-_ZN2v88internal8compiler29JSNativeContextSpecialization20BuildCheckEqualsNameERKNS1_7NameRefEPNS1_4NodeES7_S7_
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization23CanTreatHoleAsUndefinedERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization23CanTreatHoleAsUndefinedERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization23CanTreatHoleAsUndefinedERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization23CanTreatHoleAsUndefinedERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE:
.LFB23259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	8(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movq	%rax, -128(%rbp)
	cmpq	%r8, %rax
	je	.L948
	leaq	-80(%rbp), %rax
	movq	%r8, %r14
	leaq	-112(%rbp), %r12
	movq	%rax, -120(%rbp)
	leaq	-96(%rbp), %r13
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L949:
	movq	24(%rbx), %r15
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10AsJSObjectEv@PLT
	movq	-120(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler12JSHeapBroker24IsArrayOrObjectPrototypeERKNS1_11JSObjectRefE@PLT
	testb	%al, %al
	je	.L951
	addq	$8, %r14
	cmpq	%r14, -128(%rbp)
	je	.L948
.L952:
	movq	(%r14), %rdx
	movq	24(%rbx), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler6MapRef9prototypeEv@PLT
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef10IsJSObjectEv@PLT
	testb	%al, %al
	jne	.L949
.L951:
	xorl	%eax, %eax
.L947:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L959
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L948:
	.cfi_restore_state
	movq	56(%rbx), %rdi
	call	_ZN2v88internal8compiler23CompilationDependencies27DependOnNoElementsProtectorEv@PLT
	jmp	.L947
.L959:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23259:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization23CanTreatHoleAsUndefinedERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization23CanTreatHoleAsUndefinedERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization18BuildElementAccessEPNS1_4NodeES4_S4_S4_S4_RKNS1_17ElementAccessInfoERKNS1_15KeyedAccessModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization18BuildElementAccessEPNS1_4NodeES4_S4_S4_S4_RKNS1_17ElementAccessInfoERKNS1_15KeyedAccessModeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization18BuildElementAccessEPNS1_4NodeES4_S4_S4_S4_RKNS1_17ElementAccessInfoERKNS1_15KeyedAccessModeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization18BuildElementAccessEPNS1_4NodeES4_S4_S4_S4_RKNS1_17ElementAccessInfoERKNS1_15KeyedAccessModeE:
.LFB23255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$408, %rsp
	movq	16(%rbp), %rax
	movq	%rdi, -344(%rbp)
	movq	%rdx, -336(%rbp)
	movq	32(%rbp), %r14
	movq	%rax, -272(%rbp)
	movq	24(%rbp), %rax
	movq	%rcx, -368(%rbp)
	movq	%rax, -384(%rbp)
	movzbl	(%rax), %eax
	movq	%r8, -320(%rbp)
	movq	%r9, -304(%rbp)
	leal	-17(%rax), %r15d
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movb	%al, -288(%rbp)
	cmpb	$10, %r15b
	ja	.L961
	movq	24(%rsi), %rsi
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_121GetTypedArrayConstantEPNS1_12JSHeapBrokerEPNS1_4NodeE
	cmpb	$0, -240(%rbp)
	jne	.L1087
	movq	16(%rbx), %rax
	leaq	-176(%rbp), %r12
	movq	-336(%rbp), %xmm0
	movq	%r12, %rdi
	movq	(%rax), %r9
	movq	376(%rax), %r13
	movhps	-304(%rbp), %xmm0
	movaps	%xmm0, -304(%rbp)
	movq	%r9, -288(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder21ForJSTypedArrayLengthEv@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	leaq	-112(%rbp), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	movl	$3, %edx
	xorl	%r8d, %r8d
	movq	-288(%rbp), %r9
	movdqa	-304(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movq	%r9, %rdi
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -288(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movq	%r9, -384(%rbp)
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder26ForJSTypedArrayBasePointerEv@PLT
	movq	-304(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	movl	$3, %edx
	xorl	%r8d, %r8d
	movq	-384(%rbp), %r9
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movq	-336(%rbp), %xmm0
	movq	%r9, %rdi
	movq	%rax, -96(%rbp)
	movhps	-288(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -384(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movq	%r9, -352(%rbp)
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder30ForJSTypedArrayExternalPointerEv@PLT
	movq	-304(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-336(%rbp), %xmm0
	movq	-352(%rbp), %r9
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movhps	-384(%rbp), %xmm0
	movq	%r9, %rdi
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -304(%rbp)
	movq	%rax, -352(%rbp)
.L965:
	movq	56(%rbx), %rdi
	call	_ZN2v88internal8compiler23CompilationDependencies37DependOnArrayBufferDetachingProtectorEv@PLT
	testb	%al, %al
	jne	.L966
	cmpb	$0, -240(%rbp)
	movq	16(%rbx), %r8
	jne	.L1088
	movq	-336(%rbp), %xmm0
	movq	(%r8), %r9
	movq	%r12, %rdi
	movq	376(%r8), %r8
	movhps	-304(%rbp), %xmm0
	movq	%r9, -336(%rbp)
	movaps	%xmm0, -400(%rbp)
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder26ForJSArrayBufferViewBufferEv@PLT
	movq	-304(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-336(%rbp), %r9
	movdqa	-400(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movq	%r9, %rdi
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -304(%rbp)
	movq	%rax, -336(%rbp)
.L968:
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movq	%r9, -416(%rbp)
	movq	%r8, -400(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder24ForJSArrayBufferBitFieldEv@PLT
	movq	-400(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-416(%rbp), %r9
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movq	-336(%rbp), %xmm0
	movq	%r9, %rdi
	movq	%rax, -96(%rbp)
	movhps	-304(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -304(%rbp)
	movq	(%rdi), %r9
	movq	%r9, -432(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movq	16(%rbx), %rdi
	movsd	.LC29(%rip), %xmm0
	movq	%rax, -400(%rbp)
	movq	(%rdi), %r10
	movq	%r10, -424(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -416(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseAndEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-424(%rbp), %r10
	movq	%rax, %rsi
	movq	-304(%rbp), %xmm0
	movq	%r10, %rdi
	movhps	-416(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -416(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11NumberEqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-432(%rbp), %r9
	movq	%rax, %rsi
	movq	-416(%rbp), %xmm0
	movq	%r9, %rdi
	movhps	-400(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%rax, -400(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	movq	%r9, -416(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder7CheckIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-400(%rbp), %xmm0
	movq	-416(%rbp), %r9
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movhps	-304(%rbp), %xmm0
	movq	%r9, %rdi
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -304(%rbp)
.L966:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler15KeyedAccessMode6IsLoadEv@PLT
	testb	%al, %al
	je	.L972
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler15KeyedAccessMode9load_modeEv@PLT
	cmpl	$1, %eax
	je	.L974
.L972:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler15KeyedAccessMode7IsStoreEv@PLT
	testb	%al, %al
	je	.L971
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler15KeyedAccessMode10store_modeEv@PLT
	cmpl	$2, %eax
	je	.L974
.L971:
	movq	-368(%rbp), %xmm1
	movq	16(%rbx), %rax
	movq	%r12, %rsi
	movq	-304(%rbp), %xmm0
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movhps	-288(%rbp), %xmm1
	movq	$0, -176(%rbp)
	movhps	-272(%rbp), %xmm0
	movaps	%xmm1, -400(%rbp)
	movq	%r9, -304(%rbp)
	movaps	%xmm0, -368(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckBoundsERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-304(%rbp), %r9
	movdqa	-400(%rbp), %xmm1
	movq	%rax, %rsi
	movdqa	-368(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -304(%rbp)
	movq	%rax, -368(%rbp)
.L973:
	movzbl	%r15b, %r15d
	leaq	CSWTCH.542(%rip), %rax
	movq	%r14, %rdi
	movl	(%rax,%r15,4), %eax
	movl	%eax, -244(%rbp)
	call	_ZNK2v88internal8compiler15KeyedAccessMode11access_modeEv@PLT
	cmpl	$1, %eax
	je	.L975
	jg	.L976
	testl	%eax, %eax
	jne	.L984
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler15KeyedAccessMode9load_modeEv@PLT
	movq	-336(%rbp), %xmm2
	movq	-352(%rbp), %xmm1
	movhps	-384(%rbp), %xmm2
	movhps	-368(%rbp), %xmm1
	movaps	%xmm2, -336(%rbp)
	movaps	%xmm1, -320(%rbp)
	cmpl	$1, %eax
	je	.L1089
	movq	16(%rbx), %rax
	leaq	-244(%rbp), %rsi
	movq	-304(%rbp), %xmm0
	movq	376(%rax), %rdi
	movhps	-272(%rbp), %xmm0
	movq	(%rax), %r12
	movaps	%xmm0, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16LoadTypedElementERKNS0_17ExternalArrayTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$6, %edx
	movdqa	-336(%rbp), %xmm2
	movdqa	-320(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%r12, %rdi
	movdqa	-288(%rbp), %xmm0
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -304(%rbp)
	movq	%rax, -320(%rbp)
	.p2align 4,,10
	.p2align 3
.L984:
	movq	-344(%rbp), %rax
	movq	-272(%rbp), %rcx
	movq	-320(%rbp), %xmm0
	movq	%rcx, 16(%rax)
	movhps	-304(%rbp), %xmm0
	movups	%xmm0, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1090
	movq	-344(%rbp), %rax
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L961:
	.cfi_restore_state
	movq	16(%rsi), %rax
	movq	%rdx, %xmm0
	leaq	-176(%rbp), %r12
	movhps	-304(%rbp), %xmm0
	movq	%r12, %rdi
	movq	376(%rax), %r13
	movq	(%rax), %r15
	movaps	%xmm0, -304(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-112(%rbp), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movdqa	-304(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	-272(%rbp), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -304(%rbp)
	call	_ZNK2v88internal8compiler15KeyedAccessMode11access_modeEv@PLT
	cmpb	$3, -288(%rbp)
	ja	.L1032
	cmpl	$1, %eax
	je	.L1091
.L1032:
	movq	-304(%rbp), %rax
	movq	%rax, -400(%rbp)
.L985:
	movzbl	-288(%rbp), %eax
	movq	24(%rbx), %rsi
	movl	%eax, -424(%rbp)
	movq	-384(%rbp), %rax
	movq	24(%rax), %rcx
	movq	16(%rax), %r8
	movq	%rcx, -352(%rbp)
	cmpq	%rcx, %r8
	je	.L990
	movq	%rbx, -416(%rbp)
	movq	%r8, %r15
	movq	%rsi, %rbx
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L988:
	addq	$8, %r15
	cmpq	%r15, -352(%rbp)
	je	.L1092
.L989:
	movq	(%r15), %rdx
	movq	%r12, %rdi
	movl	$1, %ecx
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler6MapRef12IsJSArrayMapEv@PLT
	testb	%al, %al
	jne	.L988
	movq	-416(%rbp), %rbx
	movl	%eax, %r15d
	movq	%r12, %rdi
	movq	-304(%rbp), %xmm0
	movq	16(%rbx), %rax
	movhps	-400(%rbp), %xmm0
	movaps	%xmm0, -416(%rbp)
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movq	%r9, -400(%rbp)
	movq	%r8, -352(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	-352(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-400(%rbp), %r9
	movdqa	-416(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movq	%r9, %rdi
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -352(%rbp)
.L987:
	movq	-384(%rbp), %rax
	movq	%r14, %rdi
	addq	$8, %rax
	movq	%rax, -416(%rbp)
	call	_ZNK2v88internal8compiler15KeyedAccessMode7IsStoreEv@PLT
	testb	%al, %al
	je	.L995
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler15KeyedAccessMode10store_modeEv@PLT
	movq	-352(%rbp), %rdx
	movq	%rdx, -384(%rbp)
	cmpl	$1, %eax
	je	.L994
.L995:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler15KeyedAccessMode6IsLoadEv@PLT
	testb	%al, %al
	je	.L993
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler15KeyedAccessMode9load_modeEv@PLT
	cmpl	$1, %eax
	je	.L1093
.L993:
	movq	-368(%rbp), %xmm0
	movq	16(%rbx), %rax
	movq	%r12, %rsi
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movhps	-352(%rbp), %xmm0
	movq	$0, -176(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%r9, -368(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckBoundsERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movdqa	-384(%rbp), %xmm0
	movq	-368(%rbp), %r9
	movq	%rax, %rsi
	movaps	%xmm0, -112(%rbp)
	movq	%r9, %rdi
	movq	-352(%rbp), %xmm0
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -368(%rbp)
	movq	%rax, -384(%rbp)
.L994:
	movzbl	-288(%rbp), %edx
	leal	-4(%rdx), %eax
	movb	%al, -432(%rbp)
	cmpb	$1, %al
	jbe	.L1035
	cmpb	$1, %dl
	jbe	.L1094
	movq	$209682431, -400(%rbp)
	movl	$7, %eax
	movl	$8, %edx
.L996:
	movq	-400(%rbp), %rcx
	movq	%r14, %rdi
	movb	$1, -240(%rbp)
	movl	$16, -236(%rbp)
	movq	%rcx, -232(%rbp)
	movb	%dl, -224(%rbp)
	movb	%al, -223(%rbp)
	movb	$5, -222(%rbp)
	movl	$0, -220(%rbp)
	call	_ZNK2v88internal8compiler15KeyedAccessMode11access_modeEv@PLT
	testl	%eax, %eax
	jne	.L997
	movzbl	-288(%rbp), %eax
	cmpb	$5, %al
	ja	.L998
	testb	$1, %al
	jne	.L1095
.L998:
	movzbl	-288(%rbp), %r15d
	andl	$-3, %r15d
	cmpb	$1, %r15b
	jne	.L999
	movl	$1800, %edx
	movq	%r14, %rdi
	movw	%dx, -224(%rbp)
	call	_ZNK2v88internal8compiler15KeyedAccessMode9load_modeEv@PLT
	cmpl	$1, %eax
	je	.L1028
	movq	16(%rbx), %rax
	leaq	-240(%rbp), %rsi
	movq	-304(%rbp), %xmm0
	movq	-384(%rbp), %xmm1
	movq	376(%rax), %rdi
	movhps	-368(%rbp), %xmm0
	movq	(%rax), %r12
	movhps	-272(%rbp), %xmm1
	movaps	%xmm0, -304(%rbp)
	movaps	%xmm1, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11LoadElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movdqa	-304(%rbp), %xmm0
	movdqa	-288(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%r12, %rdi
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -304(%rbp)
.L1002:
	movq	-416(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization23CanTreatHoleAsUndefinedERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	testb	%al, %al
	je	.L1007
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder28ConvertTaggedHoleToUndefinedEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-304(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -320(%rbp)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1087:
	leaq	-232(%rbp), %r12
	movq	16(%rbx), %r13
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler15JSTypedArrayRef6lengthEv@PLT
	testq	%rax, %rax
	js	.L963
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L964:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movq	16(%rbx), %r13
	movq	%r12, %rdi
	leaq	-176(%rbp), %r12
	movq	%rax, -384(%rbp)
	call	_ZNK2v88internal8compiler15JSTypedArrayRef16external_pointerEv@PLT
	movq	%r13, %rdi
	leaq	-112(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler12MachineGraph15PointerConstantEl@PLT
	movq	%rax, -352(%rbp)
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L976:
	cmpl	$2, %eax
	je	.L979
	cmpl	$3, %eax
	jne	.L984
	movq	-368(%rbp), %xmm0
	movq	16(%rbx), %rax
	xorl	%esi, %esi
	movq	-304(%rbp), %xmm1
	movq	376(%rax), %rdi
	movhps	-288(%rbp), %xmm0
	movq	(%rax), %r12
	movhps	-272(%rbp), %xmm1
	movaps	%xmm0, -320(%rbp)
	movaps	%xmm1, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeNumberLessThanENS1_19NumberOperationHintE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movdqa	-320(%rbp), %xmm0
	movdqa	-288(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%r12, %rdi
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -304(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1092:
	movq	-416(%rbp), %rbx
.L990:
	movq	16(%rbx), %rax
	leaq	-240(%rbp), %r8
	movq	-336(%rbp), %xmm0
	movl	-424(%rbp), %esi
	movq	%r8, %rdi
	movq	%r8, -352(%rbp)
	movq	376(%rax), %r9
	movhps	-400(%rbp), %xmm0
	movq	(%rax), %r15
	movaps	%xmm0, -416(%rbp)
	movq	%r9, -400(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder16ForJSArrayLengthENS0_12ElementsKindE@PLT
	movq	-352(%rbp), %r8
	movq	-400(%rbp), %r9
	movq	%r8, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movdqa	-416(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movl	$3, %edx
	movl	$1, %r15d
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -352(%rbp)
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L1035:
	movq	$7263, -400(%rbp)
	movl	$6, %eax
	movl	$13, %edx
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L999:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler15KeyedAccessMode9load_modeEv@PLT
	cmpl	$1, %eax
	je	.L1028
	movq	16(%rbx), %rax
	leaq	-240(%rbp), %rsi
	movq	-304(%rbp), %xmm1
	movq	-384(%rbp), %xmm0
	movq	376(%rax), %rdi
	movhps	-368(%rbp), %xmm1
	movq	(%rax), %r14
	movhps	-272(%rbp), %xmm0
	movaps	%xmm1, -320(%rbp)
	movaps	%xmm0, -304(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11LoadElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movdqa	-320(%rbp), %xmm1
	movdqa	-304(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -320(%rbp)
.L1003:
	movq	-320(%rbp), %rax
	cmpb	$5, -288(%rbp)
	movq	%rax, -304(%rbp)
	jne	.L984
	movq	-416(%rbp), %rsi
	movq	%rax, %xmm0
	movq	%rbx, %rdi
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -288(%rbp)
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization23CanTreatHoleAsUndefinedERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	movq	%r12, %rdx
	movl	%eax, %esi
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16CheckFloat64HoleENS1_20CheckFloat64HoleModeERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movdqa	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	-272(%rbp), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -320(%rbp)
	movq	%rax, -304(%rbp)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1088:
	leaq	-232(%rbp), %rdi
	movq	%r8, -336(%rbp)
	call	_ZNK2v88internal8compiler15JSTypedArrayRef6bufferEv@PLT
	movq	-336(%rbp), %r8
	movq	%r12, %rsi
	movq	%rax, -176(%rbp)
	movq	%r8, %rdi
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%rax, -336(%rbp)
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L963:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L974:
	movq	-368(%rbp), %xmm0
	movq	16(%rbx), %rax
	movq	%r12, %rsi
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movhps	-304(%rbp), %xmm0
	movq	$0, -176(%rbp)
	movaps	%xmm0, -368(%rbp)
	movq	%r9, -304(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8CheckSmiERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-304(%rbp), %r9
	movdqa	-368(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movq	%r9, %rdi
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -304(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -368(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberToUint32Ev@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-368(%rbp), %r9
	movq	%rax, %rsi
	movq	-304(%rbp), %rax
	movq	%r9, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -368(%rbp)
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L1095:
	movq	16(%rbx), %rax
	movq	-400(%rbp), %rdi
	movl	$8388609, %esi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, -232(%rbp)
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L997:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler15KeyedAccessMode11access_modeEv@PLT
	cmpl	$3, %eax
	je	.L1096
	cmpb	$1, -288(%rbp)
	jbe	.L1097
	cmpb	$1, -432(%rbp)
	jbe	.L1098
.L1018:
	cmpb	$3, -288(%rbp)
	jbe	.L1017
.L1022:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler15KeyedAccessMode10store_modeEv@PLT
	cmpl	$1, %eax
	je	.L1099
.L1085:
	movq	16(%rbx), %rax
	movq	(%rax), %r14
	movq	376(%rax), %rdi
.L1023:
	leaq	-240(%rbp), %rsi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$5, %edx
	movq	-304(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	-272(%rbp), %rax
	movhps	-368(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	-320(%rbp), %xmm0
	movhps	-384(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -304(%rbp)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L975:
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	movl	$4, %esi
	movq	-320(%rbp), %xmm0
	movq	376(%rax), %rdi
	movhps	-304(%rbp), %xmm0
	movq	(%rax), %r15
	movq	$0, -176(%rbp)
	movaps	%xmm0, -304(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder19SpeculativeToNumberENS1_19NumberOperationHintERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movdqa	-304(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	-272(%rbp), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	cmpl	$9, -244(%rbp)
	movq	%rax, -320(%rbp)
	movq	%rax, -304(%rbp)
	je	.L1100
.L982:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler15KeyedAccessMode10store_modeEv@PLT
	cmpl	$2, %eax
	je	.L1101
	movq	16(%rbx), %rax
	leaq	-244(%rbp), %rsi
	movq	-336(%rbp), %xmm2
	movq	-352(%rbp), %xmm0
	movq	-320(%rbp), %xmm1
	movq	376(%rax), %rdi
	movhps	-384(%rbp), %xmm2
	movq	(%rax), %r12
	movhps	-368(%rbp), %xmm0
	movhps	-304(%rbp), %xmm1
	movaps	%xmm2, -384(%rbp)
	movaps	%xmm0, -336(%rbp)
	movaps	%xmm1, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder17StoreTypedElementERKNS0_17ExternalArrayTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$7, %edx
	movdqa	-384(%rbp), %xmm2
	movdqa	-336(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movdqa	-288(%rbp), %xmm1
	movq	-272(%rbp), %rax
	movaps	%xmm2, -112(%rbp)
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -304(%rbp)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1028:
	movq	-416(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization23CanTreatHoleAsUndefinedERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	movq	-304(%rbp), %xmm1
	testb	%al, %al
	jne	.L1001
	movq	16(%rbx), %rax
	movhps	-368(%rbp), %xmm1
	movq	-384(%rbp), %xmm0
	leaq	-240(%rbp), %rsi
	movaps	%xmm1, -320(%rbp)
	movq	376(%rax), %rdi
	movhps	-272(%rbp), %xmm0
	movq	(%rax), %r14
	movaps	%xmm0, -304(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11LoadElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movdqa	-320(%rbp), %xmm1
	movdqa	-304(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -320(%rbp)
	cmpb	$1, %r15b
	jne	.L1003
	movq	-320(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1094:
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movl	$6, %edx
	movl	%eax, %ecx
	movl	$2, %eax
	orl	$1, %ecx
	movq	%rcx, -400(%rbp)
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1097:
	movq	-320(%rbp), %xmm0
	movq	16(%rbx), %rax
	movq	%r12, %rsi
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movhps	-384(%rbp), %xmm0
	movq	$0, -176(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%r9, -320(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8CheckSmiERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-320(%rbp), %r9
	movdqa	-384(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movq	%r9, %rdi
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -384(%rbp)
	movq	%rax, -320(%rbp)
.L1017:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler15KeyedAccessMode10store_modeEv@PLT
	cmpl	$3, %eax
	jne	.L1022
	movq	-336(%rbp), %xmm1
	movq	16(%rbx), %rax
	movq	-384(%rbp), %xmm0
	movq	376(%rax), %rdi
	movhps	-304(%rbp), %xmm1
	movq	(%rax), %r12
	movhps	-272(%rbp), %xmm0
	movaps	%xmm1, -304(%rbp)
	movaps	%xmm0, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26EnsureWritableFastElementsEv@PLT
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movdqa	-304(%rbp), %xmm1
	movdqa	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$4, %edx
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -384(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r14
	movq	376(%rax), %rdi
	movq	-384(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1091:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler15KeyedAccessMode10store_modeEv@PLT
	movq	-304(%rbp), %rdx
	andl	$-3, %eax
	movq	%rdx, -400(%rbp)
	cmpl	$1, %eax
	je	.L985
	movq	16(%rbx), %rax
	movq	%r12, %rcx
	xorl	%esi, %esi
	movq	(%rax), %r15
	movq	376(%rax), %rdi
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	movq	360(%rax), %rdx
	addq	$152, %rdx
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9CheckMapsENS_4base5FlagsINS1_13CheckMapsFlagEiEENS0_13ZoneHandleSetINS0_3MapEEERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-304(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	-272(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -400(%rbp)
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	-304(%rbp), %xmm0
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	punpcklqdq	%xmm0, %xmm0
	movq	(%rax), %r12
	movaps	%xmm0, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder18CheckNotTaggedHoleEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movdqa	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	-272(%rbp), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -304(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1089:
	movq	-368(%rbp), %xmm0
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	movhps	-288(%rbp), %xmm0
	movaps	%xmm0, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberLessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movdqa	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rax, -288(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%r12, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	leaq	-244(%rbp), %rsi
	movq	%rax, -272(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16LoadTypedElementERKNS0_17ExternalArrayTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$6, %edx
	movq	-304(%rbp), %xmm0
	movdqa	-336(%rbp), %xmm2
	movq	%rax, %rsi
	movq	%r14, %rdi
	movdqa	-320(%rbp), %xmm1
	movhps	-272(%rbp), %xmm0
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -288(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r12, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movl	$2, %esi
	movq	%rax, -336(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-272(%rbp), %xmm0
	movq	%r12, %rdi
	movq	%rax, %rsi
	movhps	-320(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, -272(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movl	$3, %edx
	movhps	-304(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$8, %esi
	movl	$2, %edx
	movq	%rax, -304(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movhps	-336(%rbp), %xmm0
.L1086:
	movq	-272(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$3, %edx
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -320(%rbp)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1093:
	movq	-416(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization23CanTreatHoleAsUndefinedERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	testb	%al, %al
	je	.L993
	movq	16(%rbx), %rdi
	movsd	.LC30(%rip), %xmm0
	movq	(%rdi), %r9
	movq	%r9, -400(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r12, %rsi
	movq	%rax, -384(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckBoundsERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-368(%rbp), %xmm0
	movq	-400(%rbp), %r9
	movq	%rax, %rsi
	movhps	-384(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -112(%rbp)
	movq	-352(%rbp), %xmm0
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -368(%rbp)
	movq	%rax, -384(%rbp)
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1101:
	movq	-336(%rbp), %xmm3
	movq	16(%rbx), %rax
	movq	-352(%rbp), %xmm2
	movq	-320(%rbp), %xmm1
	movq	-368(%rbp), %xmm0
	movhps	-384(%rbp), %xmm3
	movq	376(%rax), %rdi
	movhps	-368(%rbp), %xmm2
	movhps	-304(%rbp), %xmm1
	movq	(%rax), %r12
	movaps	%xmm3, -400(%rbp)
	movhps	-288(%rbp), %xmm0
	movaps	%xmm2, -384(%rbp)
	movaps	%xmm1, -336(%rbp)
	movaps	%xmm0, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberLessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	movdqa	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$1, %esi
	movq	%rax, -288(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%r12, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	leaq	-244(%rbp), %rsi
	movq	%rax, -272(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder17StoreTypedElementERKNS0_17ExternalArrayTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$7, %edx
	movdqa	-400(%rbp), %xmm3
	movdqa	-384(%rbp), %xmm2
	movq	%rax, %rsi
	movq	%r14, %rdi
	movdqa	-336(%rbp), %xmm1
	movq	-272(%rbp), %rax
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -288(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r12, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, -336(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-272(%rbp), %xmm0
	movq	%r12, %rdi
	movq	%rax, %rsi
	movhps	-336(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, -272(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	-272(%rbp), %rax
	movhps	-304(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -304(%rbp)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	-368(%rbp), %xmm7
	movq	16(%rbx), %rax
	movdqa	%xmm7, %xmm0
	movq	376(%rax), %rdi
	punpcklqdq	%xmm7, %xmm1
	movq	(%rax), %r14
	movhps	-352(%rbp), %xmm0
	movaps	%xmm1, -320(%rbp)
	movaps	%xmm0, -304(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberLessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r14, %rdi
	movdqa	-304(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rax, -304(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-304(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -272(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-272(%rbp), %r9
	movq	%rax, %rsi
	movq	%r14, -112(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	leaq	-240(%rbp), %rsi
	movq	%rax, -272(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -304(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11LoadElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-384(%rbp), %xmm0
	movq	-304(%rbp), %r9
	movq	%rax, %rsi
	movdqa	-320(%rbp), %xmm1
	movhps	-272(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -304(%rbp)
	cmpb	$1, %r15b
	je	.L1102
	movq	%rax, %rdx
	movq	16(%rbx), %rax
	cmpb	$5, -288(%rbp)
	movq	%rdx, -320(%rbp)
	movq	(%rax), %r15
	je	.L1103
.L1005:
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r14, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movl	$2, %esi
	movq	%rax, -288(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-272(%rbp), %xmm0
	movq	%r12, %rdi
	movq	%rax, %rsi
	movhps	-336(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, -272(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-304(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movl	$3, %edx
	movhps	-384(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$8, %esi
	movl	$2, %edx
	movq	%rax, -304(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	-320(%rbp), %xmm0
	movq	%rax, %rsi
	movhps	-288(%rbp), %xmm0
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1096:
	movq	-368(%rbp), %xmm7
	movq	16(%rbx), %rax
	xorl	%esi, %esi
	movq	-384(%rbp), %xmm0
	movq	376(%rax), %rdi
	movhps	-352(%rbp), %xmm7
	movq	(%rax), %r14
	movhps	-272(%rbp), %xmm0
	movaps	%xmm7, -336(%rbp)
	movaps	%xmm0, -320(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeNumberLessThanENS1_19NumberOperationHintE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movdqa	-336(%rbp), %xmm7
	movdqa	-320(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -320(%rbp)
	movq	%rax, %xmm3
	movzbl	-288(%rbp), %eax
	cmpb	$5, %al
	ja	.L1040
	testb	$1, %al
	jne	.L1104
.L1040:
	movq	-320(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1100:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20NumberToUint8ClampedEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-320(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -320(%rbp)
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1098:
	movq	-320(%rbp), %xmm0
	movq	16(%rbx), %rax
	movq	%r12, %rsi
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movhps	-384(%rbp), %xmm0
	movq	$0, -176(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%r9, -320(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckNumberERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-320(%rbp), %r9
	movdqa	-384(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movq	%r9, %rdi
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -384(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -320(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberSilenceNaNEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-320(%rbp), %r9
	movq	%rax, %rsi
	movq	-384(%rbp), %rax
	movq	%r9, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -320(%rbp)
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	-304(%rbp), %xmm0
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movhps	-384(%rbp), %xmm0
	movaps	%xmm0, -416(%rbp)
	movq	%r9, -400(%rbp)
	movq	%r8, -384(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	-384(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-400(%rbp), %r9
	movdqa	-416(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movq	%r9, %rdi
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -384(%rbp)
	movzbl	-288(%rbp), %eax
	movq	(%rdi), %r9
	movq	%r9, -416(%rbp)
	cmpb	$5, %al
	ja	.L1042
	testb	$1, %al
	jne	.L1024
.L1042:
	call	_ZN2v88internal8compiler7JSGraph11OneConstantEv@PLT
	movq	%rax, -400(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberAddEv@PLT
	movq	-352(%rbp), %xmm0
	movq	%rax, %rsi
.L1084:
	movq	-416(%rbp), %r9
	xorl	%r8d, %r8d
	movhps	-400(%rbp), %xmm0
	movq	%r13, %rcx
	movl	$2, %edx
	movaps	%xmm0, -112(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rsi
	movq	-336(%rbp), %xmm0
	movq	-384(%rbp), %xmm1
	movq	%rax, -400(%rbp)
	movq	16(%rbx), %rax
	movhps	-304(%rbp), %xmm0
	movhps	-272(%rbp), %xmm1
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movaps	%xmm0, -448(%rbp)
	movaps	%xmm1, -416(%rbp)
	movq	%r9, -304(%rbp)
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckBoundsERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-304(%rbp), %r9
	movdqa	-416(%rbp), %xmm1
	movq	%rax, %rsi
	movq	-368(%rbp), %xmm2
	movq	%r9, %rdi
	movaps	%xmm1, -96(%rbp)
	movhps	-400(%rbp), %xmm2
	movaps	%xmm2, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	cmpb	$1, -432(%rbp)
	movq	%r12, %rdx
	movq	%rax, -368(%rbp)
	movq	16(%rbx), %rax
	seta	%sil
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	movq	%r9, -304(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder21MaybeGrowFastElementsENS1_20GrowFastElementsModeERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$6, %edx
	movdqa	-448(%rbp), %xmm0
	movq	-304(%rbp), %r9
	movq	%rax, %rsi
	movq	-368(%rbp), %xmm6
	movaps	%xmm0, -112(%rbp)
	movq	%r9, %rdi
	movdqa	%xmm6, %xmm0
	movhps	-384(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm6, %xmm0
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	cmpb	$3, -288(%rbp)
	movq	%rax, -304(%rbp)
	jbe	.L1105
.L1027:
	movq	16(%rbx), %rax
	movq	(%rax), %r14
	movq	376(%rax), %rdi
	movq	-304(%rbp), %rax
	movq	%rax, -384(%rbp)
	testb	%r15b, %r15b
	je	.L1023
	movq	-368(%rbp), %xmm0
	movhps	-352(%rbp), %xmm0
	movaps	%xmm0, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberLessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r14, %rdi
	movdqa	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%rax, -288(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r14, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -384(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r14, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -272(%rbp)
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler7JSGraph11OneConstantEv@PLT
	movq	%rax, -288(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberAddEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-368(%rbp), %xmm0
	movq	%r14, %rdi
	movq	%rax, %rsi
	movhps	-288(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-424(%rbp), %esi
	movq	%r12, %rdi
	movq	%rax, -288(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %r15
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler13AccessBuilder16ForJSArrayLengthENS0_12ElementsKindE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-336(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-288(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-304(%rbp), %xmm0
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, -288(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-384(%rbp), %xmm0
	movq	%r12, %rdi
	movq	%rax, %rsi
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, -272(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-304(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	-272(%rbp), %rax
	movhps	-288(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -384(%rbp)
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1102:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder28ConvertTaggedHoleToUndefinedEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-304(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -320(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r15
	jmp	.L1005
.L1104:
	movq	16(%rbx), %rax
	movdqa	%xmm3, %xmm0
	movl	$1, %edx
	xorl	%esi, %esi
	movhps	-272(%rbp), %xmm0
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	movaps	%xmm0, -272(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movdqa	-272(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r14, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	-400(%rbp), %rdi
	movl	$8388609, %esi
	movq	%rax, -368(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movzbl	-288(%rbp), %r15d
	movq	%rax, -232(%rbp)
	andl	$-3, %r15d
	cmpb	$1, %r15b
	jne	.L1009
	movl	$1800, %eax
	movw	%ax, -224(%rbp)
.L1009:
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -272(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-272(%rbp), %r9
	movq	%rax, %rsi
	movq	%r14, -112(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rsi
	movq	%rax, -272(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckBoundsERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-320(%rbp), %xmm0
	movdqa	-336(%rbp), %xmm7
	movq	%r14, %rdi
	movq	%rax, %rsi
	movhps	-272(%rbp), %xmm0
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	leaq	-240(%rbp), %rsi
	movq	%rax, -288(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11LoadElementERKNS1_13ElementAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r13, %rcx
	movq	-288(%rbp), %xmm3
	movq	%rax, %rsi
	movl	$4, %edx
	movq	-304(%rbp), %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -112(%rbp)
	movdqa	%xmm3, %xmm0
	movhps	-272(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-416(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization23CanTreatHoleAsUndefinedERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	testb	%al, %al
	je	.L1010
	movq	16(%rbx), %rdi
	movq	(%rdi), %r12
	cmpb	$1, %r15b
	jne	.L1011
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	movq	%rax, -304(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-304(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
.L1012:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10BooleanNotEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r12, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -336(%rbp)
.L1013:
	movq	16(%rbx), %rax
	movl	$2, %esi
	movq	-272(%rbp), %xmm0
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	movhps	-384(%rbp), %xmm0
	movaps	%xmm0, -272(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movdqa	-272(%rbp), %xmm0
	movq	%r12, %rdi
	movq	%rax, %rsi
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, -272(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	movl	$3, %edx
	movhps	-320(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$8, %esi
	movl	$2, %edx
	movq	%rax, -304(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	-336(%rbp), %xmm0
	movq	%rax, %rsi
	movhps	-368(%rbp), %xmm0
	jmp	.L1086
.L1105:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler15KeyedAccessMode10store_modeEv@PLT
	cmpl	$1, %eax
	jne	.L1027
	movq	-304(%rbp), %xmm7
	movq	16(%rbx), %rax
	movq	-336(%rbp), %xmm1
	movdqa	%xmm7, %xmm0
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	punpcklqdq	%xmm7, %xmm1
	movhps	-272(%rbp), %xmm0
	movaps	%xmm1, -304(%rbp)
	movaps	%xmm0, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26EnsureWritableFastElementsEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movdqa	-304(%rbp), %xmm1
	movdqa	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -304(%rbp)
	jmp	.L1027
.L1024:
	movsd	.LC31(%rip), %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -400(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9NumberAddEv@PLT
	movq	-384(%rbp), %xmm0
	movq	%rax, %rsi
	jmp	.L1084
.L1103:
	movq	376(%rax), %rdi
	movq	%rdx, %xmm0
	movl	$1, %esi
	movq	%r12, %rdx
	movq	$0, -176(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -288(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16CheckFloat64HoleENS1_20CheckFloat64HoleModeERKNS1_14FeedbackSourceE@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movdqa	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	-272(%rbp), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %xmm3
	movq	%rax, -304(%rbp)
	movq	16(%rbx), %rax
	movq	%xmm3, -320(%rbp)
	movq	(%rax), %r15
	jmp	.L1005
.L1010:
	movq	-288(%rbp), %xmm0
	movq	16(%rbx), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	(%rax), %r14
	movq	376(%rax), %rdi
	movaps	%xmm0, -288(%rbp)
	cmpb	$1, %r15b
	jne	.L1014
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder18CheckNotTaggedHoleEv@PLT
.L1083:
	movq	%rax, %rsi
	movq	-272(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movdqa	-288(%rbp), %xmm0
	movl	$3, %edx
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	%rax, -336(%rbp)
	jmp	.L1013
.L1011:
	movq	376(%rdi), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder19NumberIsFloat64HoleEv@PLT
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rsi
	movq	-288(%rbp), %rax
	movl	$1, %edx
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	jmp	.L1012
.L1014:
	movq	$0, -176(%rbp)
	movq	%r12, %rdx
	xorl	%esi, %esi
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16CheckFloat64HoleENS1_20CheckFloat64HoleModeERKNS1_14FeedbackSourceE@PLT
	jmp	.L1083
.L1090:
	call	__stack_chk_fail@PLT
.L979:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23255:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization18BuildElementAccessEPNS1_4NodeES4_S4_S4_S4_RKNS1_17ElementAccessInfoERKNS1_15KeyedAccessModeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization18BuildElementAccessEPNS1_4NodeES4_S4_S4_S4_RKNS1_17ElementAccessInfoERKNS1_15KeyedAccessModeE
	.section	.text._ZNK2v88internal8compiler29JSNativeContextSpecialization20InferReceiverRootMapEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler29JSNativeContextSpecialization20InferReceiverRootMapEPNS1_4NodeE
	.type	_ZNK2v88internal8compiler29JSNativeContextSpecialization20InferReceiverRootMapEPNS1_4NodeE, @function
_ZNK2v88internal8compiler29JSNativeContextSpecialization20InferReceiverRootMapEPNS1_4NodeE:
.LFB23261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$64, %rsp
	movq	(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rcx), %eax
	cmpw	$30, %ax
	jne	.L1107
	movq	48(%rcx), %rdx
	leaq	-48(%rbp), %r13
	movq	24(%rsi), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L1117
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1107:
	cmpw	$716, %ax
	je	.L1118
.L1113:
	movb	$0, (%r12)
	movb	$0, 8(%r12)
.L1106:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1119
	addq	$64, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1118:
	.cfi_restore_state
	movq	24(%rsi), %rsi
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetJSCreateMapEPNS1_12JSHeapBrokerEPNS1_4NodeE@PLT
	cmpb	$0, -80(%rbp)
	je	.L1113
	leaq	-48(%rbp), %rdi
	leaq	-72(%rbp), %rsi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	cmpb	$0, -48(%rbp)
	je	.L1113
	movdqu	-72(%rbp), %xmm0
	movb	$1, (%r12)
	movups	%xmm0, 8(%r12)
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1117:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23261:
	.size	_ZNK2v88internal8compiler29JSNativeContextSpecialization20InferReceiverRootMapEPNS1_4NodeE, .-_ZNK2v88internal8compiler29JSNativeContextSpecialization20InferReceiverRootMapEPNS1_4NodeE
	.section	.text._ZNK2v88internal8compiler29JSNativeContextSpecialization28RemoveImpossibleReceiverMapsEPNS1_4NodeEPNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler29JSNativeContextSpecialization28RemoveImpossibleReceiverMapsEPNS1_4NodeEPNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	.type	_ZNK2v88internal8compiler29JSNativeContextSpecialization28RemoveImpossibleReceiverMapsEPNS1_4NodeEPNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE, @function
_ZNK2v88internal8compiler29JSNativeContextSpecialization28RemoveImpossibleReceiverMapsEPNS1_4NodeEPNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE:
.LFB23212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-352(%rbp), %rdi
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -376(%rbp)
	movq	%rsi, %rdx
	movq	%r12, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler29JSNativeContextSpecialization20InferReceiverRootMapEPNS1_4NodeE
	movzbl	-352(%rbp), %eax
	testb	%al, %al
	jne	.L1170
.L1120:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1171
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1170:
	.cfi_restore_state
	movdqa	-352(%rbp), %xmm2
	movq	-336(%rbp), %rdx
	movq	%r12, -232(%rbp)
	movq	-376(%rbp), %rcx
	movaps	%xmm2, -256(%rbp)
	movq	16(%rcx), %r15
	movq	8(%rcx), %rbx
	movb	%al, -256(%rbp)
	movq	%rdx, -240(%rbp)
	movdqa	-256(%rbp), %xmm3
	movdqa	-240(%rbp), %xmm4
	movq	%r15, %r14
	movaps	%xmm3, -224(%rbp)
	subq	%rbx, %r14
	movaps	%xmm4, -208(%rbp)
	movb	%al, -224(%rbp)
	movdqa	-224(%rbp), %xmm5
	movq	%r12, -200(%rbp)
	movdqa	-208(%rbp), %xmm6
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movb	%al, -128(%rbp)
	movdqa	-128(%rbp), %xmm7
	movq	%r12, -104(%rbp)
	movdqa	-112(%rbp), %xmm2
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movb	%al, -96(%rbp)
	movdqa	-96(%rbp), %xmm3
	movq	%r12, -72(%rbp)
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movb	%al, -192(%rbp)
	movq	%r14, %rax
	sarq	$5, %r14
	movq	%r12, -168(%rbp)
	movdqa	-192(%rbp), %xmm1
	sarq	$3, %rax
	movdqa	-176(%rbp), %xmm0
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%r14, %r14
	jle	.L1122
	salq	$5, %r14
	leaq	-368(%rbp), %r13
	leaq	-288(%rbp), %r12
	addq	%rbx, %r14
	.p2align 4,,10
	.p2align 3
.L1133:
	movq	-72(%rbp), %rax
	movq	(%rbx), %rdx
	movq	%r13, %rdi
	movl	$1, %ecx
	movq	24(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef26is_abandoned_prototype_mapEv@PLT
	testb	%al, %al
	je	.L1172
.L1124:
	cmpq	%rbx, %r15
	je	.L1120
	leaq	8(%rbx), %r12
	cmpq	%r12, %r15
	je	.L1152
	leaq	-288(%rbp), %r14
	leaq	-96(%rbp), %r13
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1150:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1173
.L1151:
	movq	-136(%rbp), %rax
	movq	(%r12), %rdx
	movq	%r14, %rdi
	movl	$1, %ecx
	movq	24(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef26is_abandoned_prototype_mapEv@PLT
	testb	%al, %al
	jne	.L1150
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	cmpb	$0, -96(%rbp)
	jne	.L1174
.L1149:
	movq	(%r12), %rax
	addq	$8, %r12
	addq	$8, %rbx
	movq	%rax, -8(%rbx)
	cmpq	%r12, %r15
	jne	.L1151
.L1173:
	cmpq	%rbx, %r15
	je	.L1120
.L1152:
	movq	-376(%rbp), %r14
	movq	16(%r14), %rax
	movq	%rax, %rdx
	subq	%r15, %rdx
	cmpq	%rax, %r15
	je	.L1153
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	memmove@PLT
	movq	16(%r14), %rax
	movq	%rax, %rdx
	subq	%r15, %rdx
.L1153:
	addq	%rdx, %rbx
	cmpq	%rbx, %rax
	je	.L1120
	movq	-376(%rbp), %rax
	movq	%rbx, 16(%rax)
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1172:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	cmpb	$0, -288(%rbp)
	jne	.L1175
.L1125:
	movq	-72(%rbp), %rax
	movq	8(%rbx), %rdx
	movq	%r13, %rdi
	movl	$1, %ecx
	movq	24(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef26is_abandoned_prototype_mapEv@PLT
	testb	%al, %al
	je	.L1176
.L1127:
	addq	$8, %rbx
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1176:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	cmpb	$0, -288(%rbp)
	jne	.L1177
.L1128:
	movq	-72(%rbp), %rax
	movq	16(%rbx), %rdx
	movq	%r13, %rdi
	movl	$1, %ecx
	movq	24(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef26is_abandoned_prototype_mapEv@PLT
	testb	%al, %al
	je	.L1178
.L1129:
	addq	$16, %rbx
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1178:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	cmpb	$0, -288(%rbp)
	jne	.L1179
.L1130:
	movq	-72(%rbp), %rax
	movq	24(%rbx), %rdx
	movq	%r13, %rdi
	movl	$1, %ecx
	movq	24(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef26is_abandoned_prototype_mapEv@PLT
	testb	%al, %al
	je	.L1180
.L1131:
	addq	$24, %rbx
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1180:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	cmpb	$0, -288(%rbp)
	jne	.L1181
.L1132:
	addq	$32, %rbx
	cmpq	%rbx, %r14
	jne	.L1133
	movq	%r15, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
.L1122:
	leaq	-368(%rbp), %r13
	cmpq	$2, %rax
	je	.L1134
	cmpq	$3, %rax
	je	.L1135
	leaq	-368(%rbp), %r13
	cmpq	$1, %rax
	jne	.L1120
.L1136:
	movq	-72(%rbp), %rax
	movq	(%rbx), %rdx
	movq	%r13, %rdi
	movl	$1, %ecx
	movq	24(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef26is_abandoned_prototype_mapEv@PLT
	testb	%al, %al
	jne	.L1124
	leaq	-288(%rbp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	cmpb	$0, -288(%rbp)
	je	.L1120
	leaq	-320(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	leaq	-88(%rbp), %rsi
	leaq	-312(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	jne	.L1120
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1175:
	leaq	-320(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	leaq	-88(%rbp), %rsi
	leaq	-312(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	je	.L1124
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1177:
	leaq	-320(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	leaq	-88(%rbp), %rsi
	leaq	-312(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	jne	.L1128
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1179:
	leaq	-320(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	leaq	-88(%rbp), %rsi
	leaq	-312(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	jne	.L1130
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1181:
	leaq	-320(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	leaq	-88(%rbp), %rsi
	leaq	-312(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	jne	.L1132
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1174:
	leaq	-128(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	leaq	-152(%rbp), %rsi
	leaq	-120(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	je	.L1150
	jmp	.L1149
	.p2align 4,,10
	.p2align 3
.L1135:
	movq	-72(%rbp), %rax
	leaq	-368(%rbp), %r13
	movq	(%rbx), %rdx
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	24(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef26is_abandoned_prototype_mapEv@PLT
	testb	%al, %al
	jne	.L1124
	leaq	-288(%rbp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	cmpb	$0, -288(%rbp)
	jne	.L1182
.L1139:
	addq	$8, %rbx
.L1134:
	movq	-72(%rbp), %rax
	movq	(%rbx), %rdx
	movq	%r13, %rdi
	movl	$1, %ecx
	movq	24(%rax), %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef26is_abandoned_prototype_mapEv@PLT
	testb	%al, %al
	jne	.L1124
	leaq	-288(%rbp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	cmpb	$0, -288(%rbp)
	jne	.L1183
.L1141:
	addq	$8, %rbx
	jmp	.L1136
.L1183:
	leaq	-320(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	leaq	-88(%rbp), %rsi
	leaq	-312(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	je	.L1124
	jmp	.L1141
.L1182:
	leaq	-320(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler6MapRef11FindRootMapEv@PLT
	leaq	-88(%rbp), %rsi
	leaq	-312(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	je	.L1124
	jmp	.L1139
.L1171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23212:
	.size	_ZNK2v88internal8compiler29JSNativeContextSpecialization28RemoveImpossibleReceiverMapsEPNS1_4NodeEPNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE, .-_ZNK2v88internal8compiler29JSNativeContextSpecialization28RemoveImpossibleReceiverMapsEPNS1_4NodeEPNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	.section	.text._ZNK2v88internal8compiler29JSNativeContextSpecialization5graphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler29JSNativeContextSpecialization5graphEv
	.type	_ZNK2v88internal8compiler29JSNativeContextSpecialization5graphEv, @function
_ZNK2v88internal8compiler29JSNativeContextSpecialization5graphEv:
.LFB23262:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE23262:
	.size	_ZNK2v88internal8compiler29JSNativeContextSpecialization5graphEv, .-_ZNK2v88internal8compiler29JSNativeContextSpecialization5graphEv
	.section	.text._ZNK2v88internal8compiler29JSNativeContextSpecialization7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler29JSNativeContextSpecialization7isolateEv
	.type	_ZNK2v88internal8compiler29JSNativeContextSpecialization7isolateEv, @function
_ZNK2v88internal8compiler29JSNativeContextSpecialization7isolateEv:
.LFB23263:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE23263:
	.size	_ZNK2v88internal8compiler29JSNativeContextSpecialization7isolateEv, .-_ZNK2v88internal8compiler29JSNativeContextSpecialization7isolateEv
	.section	.text._ZNK2v88internal8compiler29JSNativeContextSpecialization7factoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler29JSNativeContextSpecialization7factoryEv
	.type	_ZNK2v88internal8compiler29JSNativeContextSpecialization7factoryEv, @function
_ZNK2v88internal8compiler29JSNativeContextSpecialization7factoryEv:
.LFB23264:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE23264:
	.size	_ZNK2v88internal8compiler29JSNativeContextSpecialization7factoryEv, .-_ZNK2v88internal8compiler29JSNativeContextSpecialization7factoryEv
	.section	.text._ZNK2v88internal8compiler29JSNativeContextSpecialization6commonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler29JSNativeContextSpecialization6commonEv
	.type	_ZNK2v88internal8compiler29JSNativeContextSpecialization6commonEv, @function
_ZNK2v88internal8compiler29JSNativeContextSpecialization6commonEv:
.LFB23265:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE23265:
	.size	_ZNK2v88internal8compiler29JSNativeContextSpecialization6commonEv, .-_ZNK2v88internal8compiler29JSNativeContextSpecialization6commonEv
	.section	.text._ZNK2v88internal8compiler29JSNativeContextSpecialization10javascriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler29JSNativeContextSpecialization10javascriptEv
	.type	_ZNK2v88internal8compiler29JSNativeContextSpecialization10javascriptEv, @function
_ZNK2v88internal8compiler29JSNativeContextSpecialization10javascriptEv:
.LFB23266:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	368(%rax), %rax
	ret
	.cfi_endproc
.LFE23266:
	.size	_ZNK2v88internal8compiler29JSNativeContextSpecialization10javascriptEv, .-_ZNK2v88internal8compiler29JSNativeContextSpecialization10javascriptEv
	.section	.text._ZNK2v88internal8compiler29JSNativeContextSpecialization10simplifiedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler29JSNativeContextSpecialization10simplifiedEv
	.type	_ZNK2v88internal8compiler29JSNativeContextSpecialization10simplifiedEv, @function
_ZNK2v88internal8compiler29JSNativeContextSpecialization10simplifiedEv:
.LFB23267:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	376(%rax), %rax
	ret
	.cfi_endproc
.LFE23267:
	.size	_ZNK2v88internal8compiler29JSNativeContextSpecialization10simplifiedEv, .-_ZNK2v88internal8compiler29JSNativeContextSpecialization10simplifiedEv
	.section	.rodata._ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE7reserveEm.str1.1,"aMS",@progbits,1
.LC32:
	.string	"vector::reserve"
	.section	.text._ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE7reserveEm,"axG",@progbits,_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE7reserveEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE7reserveEm
	.type	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE7reserveEm, @function
_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE7reserveEm:
.LFB27015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$268435455, %rsi
	ja	.L1215
	movq	8(%rdi), %r12
	movq	24(%rdi), %rax
	movq	%rdi, %rbx
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L1216
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1216:
	.cfi_restore_state
	movq	16(%rdi), %r13
	leaq	0(,%rsi,8), %r15
	xorl	%eax, %eax
	movq	%r13, %r14
	subq	%r12, %r14
	testq	%rsi, %rsi
	je	.L1193
	movq	(%rdi), %rdi
	movq	%r15, %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %r15
	ja	.L1217
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1193:
	cmpq	%r12, %r13
	je	.L1200
	leaq	-8(%r13), %rdx
	leaq	15(%r12), %rcx
	subq	%r12, %rdx
	subq	%rax, %rcx
	shrq	$3, %rdx
	cmpq	$30, %rcx
	jbe	.L1204
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdx
	je	.L1204
	leaq	1(%rdx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1198:
	movdqu	(%r12,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L1198
	movq	%rcx, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r12
	addq	%rax, %rsi
	cmpq	%rdx, %rcx
	je	.L1200
	movq	(%r12), %rdx
	movq	%rdx, (%rsi)
.L1200:
	movq	%rax, 8(%rbx)
	addq	%rax, %r14
	addq	%r15, %rax
	movq	%r14, 16(%rbx)
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1204:
	.cfi_restore_state
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	(%r12,%rcx,8), %rsi
	movq	%rsi, (%rax,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rsi, %rdx
	jne	.L1197
	jmp	.L1200
	.p2align 4,,10
	.p2align 3
.L1217:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1193
.L1215:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE27015:
	.size	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE7reserveEm, .-_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE7reserveEm
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_:
.LFB27018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r13
	cmpq	24(%rdi), %r13
	je	.L1219
	movq	(%rsi), %rax
	movq	%rax, 0(%r13)
	addq	$8, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1219:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r13, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L1244
	testq	%rax, %rax
	je	.L1231
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1245
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L1222:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1246
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1225:
	leaq	(%rax,%r15), %rdi
	leaq	8(%rax), %rdx
.L1223:
	movq	(%r12), %rsi
	movq	%rsi, (%rax,%rcx)
	cmpq	%r14, %r13
	je	.L1226
	leaq	-8(%r13), %rdx
	movq	%rdx, %rcx
	leaq	15(%rax), %rdx
	subq	%r14, %rcx
	subq	%r14, %rdx
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L1234
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L1234
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1228:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L1228
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r14
	addq	%rax, %rsi
	cmpq	%rdx, %r8
	je	.L1230
	movq	(%r14), %rdx
	movq	%rdx, (%rsi)
.L1230:
	leaq	16(%rax,%rcx), %rdx
.L1226:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1245:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L1247
	movl	$8, %edx
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1231:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L1222
	.p2align 4,,10
	.p2align 3
.L1234:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1227:
	movq	(%r14,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %rsi
	jne	.L1227
	jmp	.L1230
.L1246:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1225
.L1244:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1247:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L1222
	.cfi_endproc
.LFE27018:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB27025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L1286
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L1264
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1287
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L1250:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L1288
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1253:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1287:
	testq	%rdx, %rdx
	jne	.L1289
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L1251:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L1254
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L1267
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L1267
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1256:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L1256
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L1258
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L1258:
	leaq	16(%rax,%r8), %rcx
.L1254:
	cmpq	%r14, %r12
	je	.L1259
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L1268
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L1268
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1261:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L1261
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L1263
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L1263:
	leaq	8(%rcx,%r9), %rcx
.L1259:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1264:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1268:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1260:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L1260
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1267:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1255:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L1255
	jmp	.L1258
.L1288:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L1253
.L1286:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1289:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L1250
	.cfi_endproc
.LFE27025:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization33BuildExtendPropertiesBackingStoreERKNS1_6MapRefEPNS1_4NodeES7_S7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization33BuildExtendPropertiesBackingStoreERKNS1_6MapRefEPNS1_4NodeES7_S7_
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization33BuildExtendPropertiesBackingStoreERKNS1_6MapRefEPNS1_4NodeES7_S7_, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization33BuildExtendPropertiesBackingStoreERKNS1_6MapRefEPNS1_4NodeES7_S7_:
.LFB23257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$248, %rsp
	movq	%rdx, -224(%rbp)
	movq	%r8, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler6MapRef21NextFreePropertyIndexEv@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal8compiler6MapRef21GetInObjectPropertiesEv@PLT
	movq	64(%r14), %rdi
	movq	$0, -184(%rbp)
	movq	$0, -176(%rbp)
	subl	%eax, %r13d
	leal	3(%r13), %eax
	movl	%r13d, -260(%rbp)
	movl	%eax, -264(%rbp)
	cltq
	movq	%rdi, -192(%rbp)
	movq	$0, -168(%rbp)
	cmpq	$268435455, %rax
	ja	.L1315
	testq	%rax, %rax
	jne	.L1292
.L1313:
	leaq	-192(%rbp), %rax
	leaq	-160(%rbp), %r12
	movq	%rax, -272(%rbp)
	leaq	-96(%rbp), %r13
.L1293:
	movq	%rbx, -240(%rbp)
	movl	$3, %edx
	movq	%r14, %rbx
	movq	%r13, %r14
	movq	%r12, %r13
	movq	-272(%rbp), %r12
	movl	%edx, %r15d
.L1301:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	subl	$1, %r15d
	jne	.L1301
	movq	%r13, %r12
	movq	%r14, %r13
	movq	%rbx, %r14
	movl	-260(%rbp), %edx
	movq	16(%r14), %rdi
	movq	-240(%rbp), %rbx
	movq	(%rdi), %r15
	testl	%edx, %edx
	je	.L1316
	movq	-224(%rbp), %xmm0
	movq	%rbx, %xmm4
	movq	376(%rdi), %rbx
	movq	%r12, %rdi
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder29ForPropertyArrayLengthAndHashEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movdqa	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	-216(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r14), %rdi
	movsd	.LC34(%rip), %xmm0
	movq	%rax, -240(%rbp)
	movq	(%rdi), %r15
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -224(%rbp)
	movq	16(%r14), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16NumberBitwiseAndEv@PLT
.L1314:
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-240(%rbp), %xmm0
	movq	%r15, %rdi
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	16(%r14), %rdi
	cvtsi2sdl	-264(%rbp), %xmm0
	movq	%rax, -248(%rbp)
	movq	(%rdi), %r15
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -224(%rbp)
	movq	16(%r14), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberBitwiseOrEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-224(%rbp), %xmm0
	movq	%r15, %rdi
	movq	%rax, %rsi
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -224(%rbp)
	movq	16(%r14), %rax
	movq	(%rax), %r15
	movq	8(%rax), %rbx
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movl	%eax, %esi
	movq	%rbx, %rdi
	orl	$1, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TypeGuardENS1_4TypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	-216(%rbp), %rdx
	movq	%rax, %rsi
	movq	-224(%rbp), %xmm0
	movq	%rdx, -80(%rbp)
	movhps	-240(%rbp), %xmm0
	movl	$3, %edx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r14), %rbx
	movl	$1, %esi
	movq	%rax, -240(%rbp)
	movq	8(%rbx), %rdi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	$1, %edx
	movq	-240(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rcx, -96(%rbp)
	movq	%r13, %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	(%rbx), %r15
	movl	-260(%rbp), %edx
	movq	%rax, -224(%rbp)
	leal	40(,%rdx,8), %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$16777217, %esi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r15, %rdi
	movq	-216(%rbp), %rcx
	movq	%rax, %rsi
	movq	-248(%rbp), %xmm0
	movq	%rcx, -80(%rbp)
	movhps	-224(%rbp), %xmm0
	movq	%r13, %rcx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r14), %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler7JSGraph24PropertyArrayMapConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-248(%rbp), %xmm2
	movq	%rax, %rsi
	movq	%r14, %rdi
	movdqa	%xmm2, %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm2, %xmm0
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder29ForPropertyArrayLengthAndHashEv@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-248(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-224(%rbp), %xmm0
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -240(%rbp)
	movl	-264(%rbp), %eax
	testl	%eax, %eax
	jle	.L1304
	movl	-260(%rbp), %edx
	xorl	%r14d, %r14d
	movq	%r14, %r15
	leal	2(%rdx), %eax
	movq	%rax, -256(%rbp)
	.p2align 4,,10
	.p2align 3
.L1305:
	movq	-184(%rbp), %rax
	movl	$5, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	(%rax,%r15,8), %rax
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder17ForFixedArraySlotEmNS1_16WriteBarrierKindE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-248(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-240(%rbp), %xmm0
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -240(%rbp)
	movq	%r15, %rax
	addq	$1, %r15
	cmpq	%rax, -256(%rbp)
	jne	.L1305
.L1304:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r13, %rcx
	movq	-248(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-240(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1317
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1316:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -248(%rbp)
	movq	16(%r14), %rax
	movq	(%rax), %r10
	movq	376(%rax), %rdi
	movq	%r10, -240(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11ObjectIsSmiEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-240(%rbp), %r10
	movq	%rax, %rsi
	movq	-224(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%rax, -240(%rbp)
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-240(%rbp), %xmm0
	movq	-248(%rbp), %r9
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-224(%rbp), %xmm0
	movq	%r9, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -240(%rbp)
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	movq	%rdi, -224(%rbp)
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	-224(%rbp), %rdi
	movl	%eax, %esi
	orl	$1, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TypeGuardENS1_4TypeE@PLT
	movq	%rbx, %xmm3
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-240(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-216(%rbp), %rax
	movq	%r15, %rdi
	movl	$3, %edx
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r14), %rdi
	movsd	.LC33(%rip), %xmm0
	movq	%rax, -240(%rbp)
	movq	(%rdi), %r15
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -224(%rbp)
	movq	16(%r14), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15NumberShiftLeftEv@PLT
	jmp	.L1314
	.p2align 4,,10
	.p2align 3
.L1292:
	leaq	0(,%rax,8), %r12
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	movq	%r12, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %r12
	ja	.L1318
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1295:
	movl	-260(%rbp), %ecx
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	addq	%r12, %rax
	movq	%rax, -168(%rbp)
	testl	%ecx, %ecx
	jle	.L1313
	movl	-260(%rbp), %eax
	leaq	-96(%rbp), %r13
	xorl	%r15d, %r15d
	leaq	-160(%rbp), %r12
	movq	%r13, -248(%rbp)
	subl	$1, %eax
	movq	%rax, -256(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -272(%rbp)
	leaq	-200(%rbp), %rax
	movq	%rax, -280(%rbp)
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1319:
	movq	%rax, (%rsi)
	leaq	1(%r15), %rax
	addq	$8, -176(%rbp)
	cmpq	%r15, -256(%rbp)
	je	.L1311
.L1299:
	movq	%rax, %r15
.L1300:
	movq	16(%r14), %rax
	movl	$5, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	376(%rax), %r8
	movq	(%rax), %r13
	movq	%r8, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder17ForFixedArraySlotEmNS1_16WriteBarrierKindE@PLT
	movq	-240(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%rbx, %xmm1
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	-248(%rbp), %rcx
	movl	$3, %edx
	movq	-224(%rbp), %xmm0
	movq	-216(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, -200(%rbp)
	movq	%rax, %rbx
	cmpq	-168(%rbp), %rsi
	jne	.L1319
	movq	-280(%rbp), %rdx
	movq	-272(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	leaq	1(%r15), %rax
	cmpq	%r15, -256(%rbp)
	jne	.L1299
	.p2align 4,,10
	.p2align 3
.L1311:
	movq	-248(%rbp), %r13
	jmp	.L1293
.L1318:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1295
.L1315:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1317:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23257:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization33BuildExtendPropertiesBackingStoreERKNS1_6MapRefEPNS1_4NodeES7_S7_, .-_ZN2v88internal8compiler29JSNativeContextSpecialization33BuildExtendPropertiesBackingStoreERKNS1_6MapRefEPNS1_4NodeES7_S7_
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization24InlinePropertyGetterCallEPNS1_4NodeES4_S4_PS4_S5_PNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization24InlinePropertyGetterCallEPNS1_4NodeES4_S4_PS4_S5_PNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization24InlinePropertyGetterCallEPNS1_4NodeES4_S4_PS4_S5_PNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization24InlinePropertyGetterCallEPNS1_4NodeES4_S4_PS4_S5_PNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE:
.LFB23238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$184, %rsp
	movq	24(%rbp), %r8
	movq	16(%rbp), %r15
	movq	%rsi, -176(%rbp)
	movq	%rcx, -168(%rbp)
	movq	24(%rdi), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	%rdx, -192(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	movq	72(%r8), %rdx
	movq	%r8, -184(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	16(%r13), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%rax, -224(%rbp)
	movq	-168(%rbp), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler16FrameStateInfoOfEPKNS1_8OperatorE@PLT
	movq	%r14, %rdi
	movq	%rax, -208(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	movq	-184(%rbp), %r8
	testb	%al, %al
	jne	.L1339
	movq	88(%r8), %rdx
	movq	-176(%rbp), %r11
	testq	%rdx, %rdx
	je	.L1323
	leaq	-128(%rbp), %r8
	movq	16(%r13), %r9
	movq	24(%r13), %rsi
	movl	$1, %ecx
	movq	%r8, %rdi
	movq	%r8, -184(%rbp)
	movq	%r9, -224(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	-184(%rbp), %r8
	movq	-224(%rbp), %r9
	movq	%r8, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%rax, %r11
.L1323:
	movq	-208(%rbp), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1324
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1324
	leaq	-144(%rbp), %r8
	movq	24(%r13), %rsi
	movl	$1, %ecx
	movq	%r11, -208(%rbp)
	movq	%r8, %rdi
	movq	%r8, -184(%rbp)
	call	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	leaq	-128(%rbp), %r14
	call	_ZNK2v88internal8compiler9ObjectRef22AsFunctionTemplateInfoEv@PLT
	movq	-184(%rbp), %r8
	subq	$8, %rsp
	movq	-208(%rbp), %r11
	pushq	%r14
	movq	-168(%rbp), %rcx
	movq	%r12, %r9
	movq	%r13, %rdi
	pushq	%r8
	movq	-176(%rbp), %rsi
	xorl	%r8d, %r8d
	pushq	%rbx
	movq	%rdx, -120(%rbp)
	movq	%r11, %rdx
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization13InlineApiCallEPNS1_4NodeES4_S4_S4_PS4_S5_RKNS1_21SharedFunctionInfoRefERKNS1_23FunctionTemplateInfoRefE
	addq	$32, %rsp
	movq	%rax, %r9
.L1322:
	testq	%r15, %r15
	je	.L1320
	movq	16(%r13), %rax
	movq	(%r12), %rcx
	movq	%r9, -184(%rbp)
	movq	(%rbx), %r12
	movq	(%rax), %r11
	movq	8(%rax), %rdi
	movq	%rcx, -168(%rbp)
	movq	%r11, -176(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11IfExceptionEv@PLT
	movq	-176(%rbp), %r11
	movq	%r12, %xmm0
	leaq	-112(%rbp), %r12
	movhps	-168(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, %rsi
	movq	%r11, %rdi
	movl	$2, %edx
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%rbx), %rdx
	movq	%rax, -128(%rbp)
	movq	16(%r13), %rax
	movq	%rdx, -168(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfSuccessEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	-168(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rdx, -112(%rbp)
	movl	$1, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r15), %rsi
	cmpq	24(%r15), %rsi
	movq	-184(%rbp), %r9
	movq	%rax, %r12
	je	.L1327
	movq	-128(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r15)
.L1328:
	movq	%r12, (%rbx)
.L1320:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1340
	leaq	-40(%rbp), %rsp
	movq	%r9, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1339:
	.cfi_restore_state
	movq	(%rbx), %rcx
	movq	16(%r13), %rax
	leaq	-128(%rbp), %r14
	movl	$2, %esi
	movq	-192(%rbp), %xmm0
	movl	$1, %r9d
	movq	-224(%rbp), %xmm1
	leaq	-144(%rbp), %rdx
	movq	(%rax), %r11
	movq	368(%rax), %rdi
	movl	$1, %r8d
	movq	$0, -128(%rbp)
	movhps	-168(%rbp), %xmm0
	movq	%rcx, -168(%rbp)
	movq	(%r12), %rcx
	movhps	-176(%rbp), %xmm1
	movq	%r11, -184(%rbp)
	movq	%rcx, -176(%rbp)
	movq	%r14, %rcx
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movl	$-1, -120(%rbp)
	movl	$0x7fc00000, -144(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder4CallEmRKNS1_13CallFrequencyERKNS1_14FeedbackSourceENS0_19ConvertReceiverModeENS0_15SpeculationModeE@PLT
	movdqa	-208(%rbp), %xmm0
	leaq	-112(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-184(%rbp), %r11
	movdqa	-224(%rbp), %xmm1
	movq	%rax, %rsi
	movl	$6, %edx
	movaps	%xmm0, -96(%rbp)
	movq	-176(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm1, -112(%rbp)
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r9
	movq	%rax, (%r12)
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1324:
	leaq	.LC11(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1327:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%r9, -168(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-168(%rbp), %r9
	jmp	.L1328
.L1340:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23238:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization24InlinePropertyGetterCallEPNS1_4NodeES4_S4_PS4_S5_PNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization24InlinePropertyGetterCallEPNS1_4NodeES4_S4_PS4_S5_PNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization17BuildPropertyLoadEPNS1_4NodeES4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization17BuildPropertyLoadEPNS1_4NodeES4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization17BuildPropertyLoadEPNS1_4NodeES4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization17BuildPropertyLoadEPNS1_4NodeES4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE:
.LFB23247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	24(%rbp), %rax
	movq	16(%rbp), %rdx
	movq	%rcx, -192(%rbp)
	movq	40(%rbp), %r14
	movq	%r8, -200(%rbp)
	movq	%rax, -184(%rbp)
	movq	32(%rbp), %rax
	movq	%rdx, -176(%rbp)
	movq	88(%r14), %rdx
	movq	%r9, -168(%rbp)
	movq	%rax, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1342
	movq	56(%rsi), %r13
	movq	24(%rsi), %rsi
	leaq	-160(%rbp), %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-160(%rbp), %rsi
	subq	$8, %rsp
	movq	-152(%rbp), %rdi
	movb	$1, -144(%rbp)
	leaq	8(%r14), %r9
	movl	$1, %edx
	pushq	%rdi
	pushq	%rsi
	pushq	-144(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%r9, %rsi
	movq	%rdi, -128(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE@PLT
	addq	$32, %rsp
.L1342:
	movl	(%r14), %edx
	cmpl	$1, %edx
	je	.L1353
	cmpl	$4, %edx
	je	.L1354
	movq	16(%r12), %r13
	cmpl	$5, %edx
	je	.L1355
	cmpl	$6, %edx
	je	.L1356
	movq	56(%r12), %rdx
	movq	24(%r12), %rcx
	leaq	-144(%rbp), %rdi
	leaq	-176(%rbp), %r9
	movq	-184(%rbp), %rsi
	leaq	-168(%rbp), %r8
	movq	%r13, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%r15, %rcx
	movq	%rdx, -128(%rbp)
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler21PropertyAccessBuilder18BuildLoadDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeEPSA_SB_@PLT
	movq	%rax, %xmm0
	movq	-168(%rbp), %rax
.L1344:
	movq	%rax, %xmm1
	movq	-176(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1357
	leaq	-40(%rbp), %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1353:
	.cfi_restore_state
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rax, %xmm0
	movq	-168(%rbp), %rax
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1356:
	movq	376(%r13), %rdi
	movq	0(%r13), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StringLengthEv@PLT
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r15, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %xmm0
	movq	-168(%rbp), %rax
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1354:
	pushq	%r14
	movq	-200(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	pushq	-208(%rbp)
	movq	-192(%rbp), %rdx
	leaq	-176(%rbp), %r9
	leaq	-168(%rbp), %r8
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization24InlinePropertyGetterCallEPNS1_4NodeES4_S4_PS4_S5_PNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE
	popq	%rdx
	popq	%rcx
	movq	%rax, %xmm0
	movq	-168(%rbp), %rax
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1355:
	movq	24(%r12), %rsi
	movq	72(%r14), %rdx
	leaq	-160(%rbp), %r14
	movl	$1, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	leaq	-144(%rbp), %r14
	call	_ZNK2v88internal8compiler9ObjectRef6AsCellEv@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rdx, -136(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	-168(%rbp), %rcx
	movq	%r14, %rdi
	movq	-176(%rbp), %r13
	movq	%rax, -192(%rbp)
	movq	16(%r12), %rax
	movq	%rcx, -184(%rbp)
	movq	376(%rax), %r15
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler13AccessBuilder12ForCellValueEv@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	-192(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r13, -64(%rbp)
	movhps	-184(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %xmm0
	jmp	.L1344
.L1357:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23247:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization17BuildPropertyLoadEPNS1_4NodeES4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization17BuildPropertyLoadEPNS1_4NodeES4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization24InlinePropertySetterCallEPNS1_4NodeES4_S4_S4_PS4_S5_PNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization24InlinePropertySetterCallEPNS1_4NodeES4_S4_S4_PS4_S5_PNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization24InlinePropertySetterCallEPNS1_4NodeES4_S4_S4_PS4_S5_PNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization24InlinePropertySetterCallEPNS1_4NodeES4_S4_S4_PS4_S5_PNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE:
.LFB23239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-160(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	%rcx, -232(%rbp)
	movq	16(%rbp), %rcx
	movq	32(%rbp), %r14
	movq	24(%rdi), %rsi
	movq	%r8, -176(%rbp)
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	24(%rbp), %r15
	movq	%rdx, -192(%rbp)
	movq	72(%r14), %rdx
	movq	%rcx, -168(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movl	$1, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%rax, -224(%rbp)
	movq	-176(%rbp), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler16FrameStateInfoOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%rax, -208(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	testb	%al, %al
	jne	.L1377
	movq	88(%r14), %rdx
	movq	%r12, %r14
	testq	%rdx, %rdx
	je	.L1361
	movq	16(%rbx), %r8
	leaq	-128(%rbp), %r14
	movq	24(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	%r8, -224(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	-224(%rbp), %r8
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%rax, %r14
.L1361:
	movq	-208(%rbp), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1362
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1362
	leaq	-144(%rbp), %r8
	movq	24(%rbx), %rsi
	movl	$1, %ecx
	movq	%r8, %rdi
	movq	%r8, -208(%rbp)
	call	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	leaq	-128(%rbp), %r13
	call	_ZNK2v88internal8compiler9ObjectRef22AsFunctionTemplateInfoEv@PLT
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	-208(%rbp), %r8
	movq	-184(%rbp), %r9
	pushq	%r13
	movq	-176(%rbp), %rcx
	movq	%rdx, -120(%rbp)
	movq	%r14, %rdx
	pushq	%r8
	movq	-192(%rbp), %r8
	pushq	-168(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization13InlineApiCallEPNS1_4NodeES4_S4_S4_PS4_S5_RKNS1_21SharedFunctionInfoRefERKNS1_23FunctionTemplateInfoRefE
	addq	$32, %rsp
.L1360:
	testq	%r15, %r15
	je	.L1358
	movq	-184(%rbp), %rcx
	movq	16(%rbx), %rax
	movq	(%rcx), %rdx
	movq	-168(%rbp), %rcx
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	movq	(%rcx), %r12
	movq	%rdx, -176(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11IfExceptionEv@PLT
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r14, %rdi
	movq	%r12, %xmm0
	leaq	-112(%rbp), %r12
	movq	%rax, %rsi
	movhps	-176(%rbp), %xmm0
	movq	%r12, %rcx
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-168(%rbp), %rcx
	movq	%rax, -128(%rbp)
	movq	16(%rbx), %rax
	movq	(%rcx), %rbx
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfSuccessEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%rbx, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r15), %rsi
	movq	%rax, %rbx
	cmpq	24(%r15), %rsi
	je	.L1365
	movq	-128(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r15)
.L1366:
	movq	-168(%rbp), %rax
	movq	%rbx, (%rax)
.L1358:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1378
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1377:
	.cfi_restore_state
	movq	-184(%rbp), %rdx
	movq	16(%rbx), %rax
	movq	%r12, %xmm2
	leaq	-128(%rbp), %r13
	movq	-224(%rbp), %xmm0
	movq	-168(%rbp), %rcx
	movl	$1, %r9d
	movl	$1, %r8d
	movq	(%rdx), %rsi
	leaq	-144(%rbp), %rdx
	movq	368(%rax), %rdi
	movq	-192(%rbp), %xmm1
	punpcklqdq	%xmm2, %xmm0
	movq	(%rcx), %r14
	movq	%r13, %rcx
	movq	%rsi, -192(%rbp)
	movl	$3, %esi
	movq	(%rax), %r12
	movhps	-232(%rbp), %xmm1
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movq	$0, -128(%rbp)
	movl	$-1, -120(%rbp)
	movl	$0x7fc00000, -144(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder4CallEmRKNS1_13CallFrequencyERKNS1_14FeedbackSourceENS0_19ConvertReceiverModeENS0_15SpeculationModeE@PLT
	leaq	-112(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movdqa	-224(%rbp), %xmm0
	movl	$7, %edx
	movq	%rax, %rsi
	movq	%r14, -64(%rbp)
	movdqa	-208(%rbp), %xmm1
	movaps	%xmm0, -112(%rbp)
	movq	-176(%rbp), %xmm0
	movaps	%xmm1, -96(%rbp)
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-168(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movq	%rax, (%rcx)
	movq	%rax, (%rdx)
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1362:
	leaq	.LC11(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1365:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1366
.L1378:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23239:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization24InlinePropertySetterCallEPNS1_4NodeES4_S4_S4_PS4_S5_PNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization24InlinePropertySetterCallEPNS1_4NodeES4_S4_S4_PS4_S5_PNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization18BuildPropertyStoreEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization18BuildPropertyStoreEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization18BuildPropertyStoreEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization18BuildPropertyStoreEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE:
.LFB23250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 3, -56
	movq	32(%rbp), %rax
	movq	48(%rbp), %r15
	movq	%rdx, -320(%rbp)
	movq	56(%rsi), %r13
	movq	40(%rbp), %rbx
	movq	%rdi, -296(%rbp)
	movq	88(%r15), %rdx
	movq	24(%rsi), %rsi
	movq	%rax, -336(%rbp)
	movq	16(%rbp), %rax
	movq	%rcx, -288(%rbp)
	movq	%r8, -304(%rbp)
	movq	%rax, -264(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1380
	leaq	-224(%rbp), %r9
	movl	$1, %ecx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	subq	$8, %rsp
	leaq	8(%r15), %rsi
	movq	%r13, %rdi
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rax
	movb	$1, -160(%rbp)
	pushq	%rdx
	pushq	%rax
	pushq	-160(%rbp)
	movq	%rdx, -144(%rbp)
	movl	$1, %edx
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE@PLT
	addq	$32, %rsp
.L1380:
	cmpl	$4, (%r15)
	je	.L1438
	movq	112(%r15), %rax
	movzbl	104(%r15), %edi
	movq	96(%r15), %r14
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal8compiler21PropertyAccessBuilder21ConvertRepresentationENS0_14RepresentationE@PLT
	movl	%eax, %ebx
	movq	-320(%rbp), %rax
	movq	%rax, -304(%rbp)
	movq	%r14, %rax
	andl	$16384, %eax
	movq	%rax, -376(%rbp)
	je	.L1439
.L1383:
	cmpl	$3, (%r15)
	sete	%dl
	cmpl	$1, 56(%rbp)
	sete	%al
	andb	%al, %dl
	movb	%dl, -368(%rbp)
	jne	.L1440
.L1384:
	movq	-336(%rbp), %rdi
	movl	%r14d, %eax
	andl	$16383, %eax
	movl	%eax, -344(%rbp)
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	movq	%rax, %r13
	cmpb	$14, %bl
	ja	.L1385
	leaq	.L1387(%rip), %rdx
	movzbl	%bl, %r14d
	movslq	(%rdx,%r14,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler29JSNativeContextSpecialization18BuildPropertyStoreEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE,"a",@progbits
	.align 4
	.align 4
.L1387:
	.long	.L1418-.L1387
	.long	.L1418-.L1387
	.long	.L1418-.L1387
	.long	.L1392-.L1387
	.long	.L1392-.L1387
	.long	.L1396-.L1387
	.long	.L1392-.L1387
	.long	.L1418-.L1387
	.long	.L1391-.L1387
	.long	.L1392-.L1387
	.long	.L1391-.L1387
	.long	.L1391-.L1387
	.long	.L1388-.L1387
	.long	.L1388-.L1387
	.long	.L1386-.L1387
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization18BuildPropertyStoreEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE
	.p2align 4,,10
	.p2align 3
.L1439:
	movq	16(%r12), %rax
	movq	-264(%rbp), %rcx
	leaq	-160(%rbp), %rsi
	movq	%rsi, %rdi
	movq	-272(%rbp), %r13
	movq	%rsi, -344(%rbp)
	movq	(%rax), %r9
	movq	376(%rax), %r8
	movq	%rcx, -304(%rbp)
	movq	%r9, -368(%rbp)
	movq	%r8, -360(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	-360(%rbp), %r8
	movq	-344(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	-368(%rbp), %r9
	movq	%r13, -80(%rbp)
	leaq	-96(%rbp), %r13
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-320(%rbp), %xmm0
	movq	%r9, %rdi
	movhps	-304(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -304(%rbp)
	movq	%rax, -264(%rbp)
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1438:
	subq	$8, %rsp
	leaq	-272(%rbp), %rax
	movq	-304(%rbp), %rcx
	movq	-288(%rbp), %rdx
	pushq	%r15
	movq	-320(%rbp), %rsi
	movq	%r14, %r8
	movq	%r12, %rdi
	pushq	%rbx
	leaq	-264(%rbp), %r9
	pushq	%rax
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization24InlinePropertySetterCallEPNS1_4NodeES4_S4_S4_PS4_S5_PNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE
	movq	-264(%rbp), %rax
	addq	$32, %rsp
	movq	%rax, -320(%rbp)
.L1382:
	movq	-296(%rbp), %rcx
	movq	-272(%rbp), %rax
	movq	-288(%rbp), %xmm0
	movq	%rax, 16(%rcx)
	movhps	-320(%rbp), %xmm0
	movups	%xmm0, (%rcx)
.L1379:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1441
	movq	-296(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1418:
	.cfi_restore_state
	movl	%ebx, %edx
.L1394:
	movq	%r15, %rdi
	movb	%dl, -360(%rbp)
	call	_ZNK2v88internal8compiler18PropertyAccessInfo17GetConstFieldInfoEv@PLT
	movl	-344(%rbp), %ecx
	cmpl	$2, 56(%rbp)
	movb	$1, -224(%rbp)
	movzbl	-360(%rbp), %edx
	sete	%r9b
	movq	%r13, -216(%rbp)
	movl	%ecx, -220(%rbp)
	movq	-352(%rbp), %rcx
	movq	$0, -208(%rbp)
	movq	%rcx, -200(%rbp)
	movb	%bl, -192(%rbp)
	movb	%dl, -191(%rbp)
	movb	$5, -190(%rbp)
	movl	$1, -188(%rbp)
	movq	%rax, -184(%rbp)
	movb	%r9b, -176(%rbp)
	cmpb	$14, %bl
	ja	.L1442
	leaq	.L1400(%rip), %rdx
	movslq	(%rdx,%r14,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler29JSNativeContextSpecialization18BuildPropertyStoreEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE
	.align 4
	.align 4
.L1400:
	.long	.L1385-.L1400
	.long	.L1385-.L1400
	.long	.L1385-.L1400
	.long	.L1385-.L1400
	.long	.L1385-.L1400
	.long	.L1385-.L1400
	.long	.L1402-.L1400
	.long	.L1402-.L1400
	.long	.L1402-.L1400
	.long	.L1402-.L1400
	.long	.L1402-.L1400
	.long	.L1402-.L1400
	.long	.L1385-.L1400
	.long	.L1401-.L1400
	.long	.L1385-.L1400
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization18BuildPropertyStoreEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE
	.p2align 4,,10
	.p2align 3
.L1402:
	cmpb	$0, -368(%rbp)
	jne	.L1443
	cmpb	$6, %bl
	je	.L1420
	cmpb	$9, %bl
	je	.L1420
	cmpb	$7, %bl
	je	.L1421
	leaq	-96(%rbp), %r13
	cmpb	$10, %bl
	je	.L1421
	.p2align 4,,10
	.p2align 3
.L1399:
	movq	80(%r15), %rdx
	testq	%rdx, %rdx
	je	.L1444
	movq	24(%r12), %rsi
	leaq	-256(%rbp), %r15
	movl	$1, %ecx
	leaq	-160(%rbp), %r14
	movq	%r15, %rdi
	leaq	-240(%rbp), %rbx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler6MapRef14GetBackPointerEv@PLT
	movq	%r14, %rdi
	movq	%rax, -160(%rbp)
	movq	%rdx, -152(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef5AsMapEv@PLT
	movq	%rbx, %rdi
	movq	%rax, -240(%rbp)
	movq	%rdx, -232(%rbp)
	call	_ZNK2v88internal8compiler6MapRef20UnusedPropertyFieldsEv@PLT
	leaq	-224(%rbp), %r9
	testl	%eax, %eax
	je	.L1445
.L1414:
	movq	16(%r12), %rax
	movq	-264(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r9, -360(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %rbx
	movq	%rdx, -336(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	-336(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rdx, -96(%rbp)
	movl	$1, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%r15, %rsi
	movq	%rax, -264(%rbp)
	movq	%rax, -352(%rbp)
	movq	-272(%rbp), %rax
	movq	(%rdi), %rbx
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%r14, %rdi
	movq	%rax, -344(%rbp)
	movq	16(%r12), %rax
	movq	376(%rax), %r15
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-320(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movhps	-344(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-352(%rbp), %xmm0
	movhps	-336(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-360(%rbp), %r9
	movq	-272(%rbp), %rbx
	movq	%rax, -264(%rbp)
	movq	%rax, -320(%rbp)
	movq	16(%r12), %rax
	movq	%r9, %rsi
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%rbx, %xmm3
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-304(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$4, %edx
	movq	%r14, %rdi
	movhps	-288(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-320(%rbp), %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -264(%rbp)
	movq	%rax, %rbx
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rax, -320(%rbp)
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	movq	%rbx, %xmm4
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-320(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r14, %rdi
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -320(%rbp)
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1392:
	movl	$2, %edx
	jmp	.L1394
	.p2align 4,,10
	.p2align 3
.L1391:
	movl	$7, %edx
	jmp	.L1394
	.p2align 4,,10
	.p2align 3
.L1388:
	movl	$6, %edx
	jmp	.L1394
	.p2align 4,,10
	.p2align 3
.L1401:
	movq	16(%r12), %rax
	movq	-264(%rbp), %rcx
	leaq	-160(%rbp), %r14
	movb	%r9b, -360(%rbp)
	movq	-272(%rbp), %r13
	movq	%r14, %rsi
	movq	376(%rax), %rdi
	movq	(%rax), %rbx
	movq	%rcx, -352(%rbp)
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckNumberERKNS1_14FeedbackSourceE@PLT
	movq	%r13, -80(%rbp)
	leaq	-96(%rbp), %r13
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r13, %rcx
	movl	$3, %edx
	movq	%rbx, %rdi
	movq	-288(%rbp), %xmm0
	movhps	-352(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	cmpq	$0, -376(%rbp)
	movq	%rax, -288(%rbp)
	movq	%rax, -264(%rbp)
	movq	%rax, -352(%rbp)
	jne	.L1403
	cmpq	$0, 80(%r15)
	movzbl	-360(%rbp), %r9d
	je	.L1404
	movq	16(%r12), %rbx
	movq	-272(%rbp), %rax
	movl	$1, %esi
	movq	(%rbx), %r9
	movq	8(%rbx), %rdi
	movq	%rax, -344(%rbp)
	movq	%r9, -336(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11BeginRegionENS1_19RegionObservabilityE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	-336(%rbp), %r9
	movq	%rax, %rsi
	movq	-288(%rbp), %rax
	movq	%r9, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%rbx), %r9
	movsd	.LC35(%rip), %xmm0
	movq	%rbx, %rdi
	movq	%rax, -336(%rbp)
	movq	%r9, -360(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	376(%rbx), %rdi
	xorl	%edx, %edx
	movl	$16777217, %esi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8AllocateENS1_4TypeENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-360(%rbp), %r9
	movq	%rax, %rsi
	movq	-344(%rbp), %rax
	movq	-352(%rbp), %xmm0
	movq	%r9, %rdi
	movq	%rax, -80(%rbp)
	movhps	-336(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %rsi
	movl	$1, %ecx
	leaq	-240(%rbp), %r8
	movq	%rax, -336(%rbp)
	movq	16(%r12), %rax
	movq	%r8, %rdi
	movq	%r8, -352(%rbp)
	movq	360(%rax), %rdx
	addq	$256, %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-352(%rbp), %r8
	movq	%rbx, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, -352(%rbp)
	movq	%r9, -360(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-336(%rbp), %xmm6
	movq	-360(%rbp), %r9
	movq	%rax, %rsi
	movdqa	%xmm6, %xmm0
	movq	%r9, %rdi
	movhps	-352(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm6, %xmm0
	movhps	-344(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	-184(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, -120(%rbp)
	movq	(%rbx), %r9
	movq	376(%rbx), %rdi
	movq	%r9, -360(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-336(%rbp), %xmm0
	movq	-360(%rbp), %r9
	movq	%rax, %rsi
	movhps	-288(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-352(%rbp), %xmm0
	movhps	-344(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%rbx), %r9
	movq	8(%rbx), %rdi
	movq	%rax, -288(%rbp)
	movq	%r9, -344(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12FinishRegionEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-336(%rbp), %xmm0
	movq	-344(%rbp), %r9
	movq	%rax, %rsi
	movhps	-288(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1799, %esi
	movb	$3, -190(%rbp)
	movq	%rax, %xmm6
	movq	%rax, -288(%rbp)
	movq	%rax, -264(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -200(%rbp)
	movw	%si, -192(%rbp)
	movq	%xmm6, -352(%rbp)
.L1403:
	cmpb	$0, -368(%rbp)
	je	.L1399
	movq	16(%r12), %rax
	leaq	-224(%rbp), %rsi
	movq	-304(%rbp), %xmm0
	movq	-272(%rbp), %rbx
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	movhps	-352(%rbp), %xmm0
	movaps	%xmm0, -320(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movdqa	-320(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%rbx, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -264(%rbp)
	movq	%rax, -320(%rbp)
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9SameValueEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	-320(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-288(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-264(%rbp), %rbx
	movq	%r14, %rdx
	movl	$39, %esi
	movq	%rax, -320(%rbp)
	movq	16(%r12), %rax
	movq	-272(%rbp), %r15
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder7CheckIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	movq	%rbx, %xmm5
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, %rdi
	movq	%r15, -80(%rbp)
	movq	-320(%rbp), %xmm0
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-296(%rbp), %rbx
	movq	-288(%rbp), %xmm0
	movq	%rax, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, (%rbx)
	movq	-272(%rbp), %rax
	movq	%rax, 16(%rbx)
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	-272(%rbp), %r8
	movq	-304(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-264(%rbp), %rcx
	movq	%r9, -344(%rbp)
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization33BuildExtendPropertiesBackingStoreERKNS1_6MapRefEPNS1_4NodeES7_S7_
	movq	-344(%rbp), %r9
	movq	-272(%rbp), %rcx
	movq	%rax, -264(%rbp)
	movq	%rax, -336(%rbp)
	movq	16(%r12), %rax
	movq	%r9, %rsi
	movq	%rcx, -304(%rbp)
	movq	376(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$4, %edx
	movq	-336(%rbp), %xmm6
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movdqa	%xmm6, %xmm0
	movhps	-288(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm6, %xmm0
	movhps	-304(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movzbl	-112(%rbp), %eax
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm5
	movq	-344(%rbp), %r9
	movb	%al, -176(%rbp)
	movq	-320(%rbp), %rax
	movaps	%xmm7, -224(%rbp)
	movdqa	-128(%rbp), %xmm7
	movq	%rax, -304(%rbp)
	movq	-336(%rbp), %rax
	movaps	%xmm5, -208(%rbp)
	movq	%rax, -288(%rbp)
	movaps	%xmm7, -192(%rbp)
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	16(%r12), %rax
	leaq	-224(%rbp), %rsi
	movq	-304(%rbp), %xmm0
	movq	-272(%rbp), %r12
	movq	-264(%rbp), %rbx
	movq	376(%rax), %rdi
	movhps	-288(%rbp), %xmm0
	movq	(%rax), %r14
	movaps	%xmm0, -320(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movq	%r12, %xmm6
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movdqa	-320(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$4, %edx
	movq	%r14, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -320(%rbp)
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1440:
	cmpq	$0, 80(%r15)
	sete	-368(%rbp)
	jmp	.L1384
	.p2align 4,,10
	.p2align 3
.L1443:
	movq	16(%r12), %rax
	movq	-272(%rbp), %r13
	leaq	-224(%rbp), %rsi
	movq	-264(%rbp), %rbx
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%rbx, %xmm6
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-304(%rbp), %xmm0
	movq	%r13, -80(%rbp)
	leaq	-96(%rbp), %r13
	movq	%rax, %rsi
	movq	%r13, %rcx
	movl	$3, %edx
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -264(%rbp)
	movq	%rax, -320(%rbp)
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20SameValueNumbersOnlyEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-320(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-288(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-264(%rbp), %rbx
	leaq	-160(%rbp), %rdx
	movl	$39, %esi
	movq	%rax, -320(%rbp)
	movq	16(%r12), %rax
	movq	-272(%rbp), %r14
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder7CheckIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	movq	%rbx, %xmm3
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, %rdi
	movq	%r14, -80(%rbp)
	movq	-320(%rbp), %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-296(%rbp), %rbx
	movq	-288(%rbp), %xmm0
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%rbx)
	movq	-272(%rbp), %rax
	movq	%rax, 16(%rbx)
	jmp	.L1379
.L1385:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1386:
	xorl	%edx, %edx
	jmp	.L1394
	.p2align 4,,10
	.p2align 3
.L1396:
	movl	$4, %edx
	jmp	.L1394
	.p2align 4,,10
	.p2align 3
.L1421:
	movq	16(%r12), %rax
	movq	128(%r15), %rdx
	movq	-272(%rbp), %rbx
	movq	-264(%rbp), %rcx
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	movq	-288(%rbp), %xmm0
	testq	%rdx, %rdx
	je	.L1446
	movq	%rcx, %xmm6
	xorl	%esi, %esi
	leaq	-96(%rbp), %r13
	movq	$0, -160(%rbp)
	punpcklqdq	%xmm6, %xmm0
	leaq	-160(%rbp), %rcx
	movl	$-1, -152(%rbp)
	movaps	%xmm0, -336(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9CheckMapsENS_4base5FlagsINS1_13CheckMapsFlagEiEENS0_13ZoneHandleSetINS0_3MapEEERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movdqa	-336(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%rbx, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -264(%rbp)
.L1411:
	movb	$3, -190(%rbp)
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1420:
	movq	16(%r12), %rax
	movq	-272(%rbp), %r13
	leaq	-160(%rbp), %rsi
	movq	-264(%rbp), %rbx
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8CheckSmiERKNS1_14FeedbackSourceE@PLT
	movq	%r13, -80(%rbp)
	leaq	-96(%rbp), %r13
	xorl	%r8d, %r8d
	movq	%rbx, %xmm7
	movq	%rax, %rsi
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-288(%rbp), %xmm0
	movq	%r14, %rdi
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movb	$0, -190(%rbp)
	movq	%rax, -288(%rbp)
	movq	%rax, -264(%rbp)
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	-336(%rbp), %rdi
	movb	%r9b, -352(%rbp)
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler18PropertyAccessInfo17GetConstFieldInfoEv@PLT
	movl	-344(%rbp), %ecx
	movb	$3, -126(%rbp)
	movl	$1799, %edx
	movq	%rax, -120(%rbp)
	movq	16(%r12), %rax
	movq	%r14, %rsi
	movzbl	-352(%rbp), %r9d
	movl	%ecx, -156(%rbp)
	movw	%dx, -128(%rbp)
	movq	-264(%rbp), %rcx
	movb	%r9b, -112(%rbp)
	movq	-272(%rbp), %rdx
	movb	$1, -160(%rbp)
	movq	%rbx, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	$16777217, -136(%rbp)
	movl	$1, -124(%rbp)
	movq	376(%rax), %rdi
	movq	%rdx, -344(%rbp)
	movq	(%rax), %rbx
	movq	%rcx, -336(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	-344(%rbp), %rdx
	movq	%rax, %rsi
	movq	-304(%rbp), %xmm0
	movq	%rdx, -80(%rbp)
	movhps	-336(%rbp), %xmm0
	movl	$3, %edx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1549, %ecx
	movl	$8, -220(%rbp)
	movq	%rax, -304(%rbp)
	movq	%rax, -264(%rbp)
	movq	$0, -216(%rbp)
	movw	%cx, -192(%rbp)
	movq	%rax, -352(%rbp)
	jmp	.L1403
.L1446:
	movq	%rcx, %xmm4
	leaq	-96(%rbp), %r13
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -288(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder15CheckHeapObjectEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movdqa	-288(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%rbx, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -288(%rbp)
	movq	%rax, -264(%rbp)
	jmp	.L1411
.L1441:
	call	__stack_chk_fail@PLT
.L1442:
	leaq	-96(%rbp), %r13
	jmp	.L1399
	.cfi_endproc
.LFE23250:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization18BuildPropertyStoreEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization18BuildPropertyStoreEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization19BuildPropertyAccessEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization19BuildPropertyAccessEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization19BuildPropertyAccessEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization19BuildPropertyAccessEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE:
.LFB23249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	56(%rbp), %eax
	movq	16(%rbp), %r15
	movq	%fs:40, %r11
	movq	%r11, -56(%rbp)
	xorl	%r11d, %r11d
	movq	24(%rbp), %r14
	movq	32(%rbp), %rsi
	movq	40(%rbp), %rdi
	movq	48(%rbp), %rbx
	cmpl	$2, %eax
	jg	.L1448
	movq	%rcx, %r10
	movq	%r8, %rcx
	movq	%r9, %r8
	testl	%eax, %eax
	jg	.L1449
	jne	.L1451
	pushq	%rbx
	movq	%r15, %r9
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movq	%r13, %rsi
	pushq	%r14
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization17BuildPropertyLoadEPNS1_4NodeES4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoE
	addq	$32, %rsp
.L1447:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1462
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1448:
	.cfi_restore_state
	cmpl	$3, %eax
	jne	.L1451
	movq	88(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L1454
	movq	56(%r13), %r8
	movq	24(%r13), %rsi
	leaq	-96(%rbp), %rdi
	movl	$1, %ecx
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	subq	$8, %rsp
	movq	-104(%rbp), %r8
	pushq	-88(%rbp)
	movb	$1, -80(%rbp)
	leaq	8(%rbx), %rsi
	movl	$1, %edx
	pushq	-96(%rbp)
	movq	%r8, %rdi
	pushq	-80(%rbp)
	call	_ZN2v88internal8compiler23CompilationDependencies29DependOnStablePrototypeChainsINS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEEEvRKT_NS0_12WhereToStartENS_4base8OptionalINS1_11JSObjectRefEEE@PLT
	addq	$32, %rsp
.L1454:
	cmpl	$1, (%rbx)
	movq	16(%r13), %rdi
	je	.L1463
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	%rax, %xmm0
.L1456:
	movq	%r15, %xmm1
	movq	%r14, 16(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1449:
	pushq	%rax
	movq	%rcx, %r8
	movq	%r10, %rcx
	pushq	%rbx
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movq	%r13, %rsi
	pushq	%r14
	pushq	%r15
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization18BuildPropertyStoreEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE
	addq	$48, %rsp
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1463:
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	%rax, %xmm0
	jmp	.L1456
.L1451:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1462:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23249:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization19BuildPropertyAccessEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization19BuildPropertyAccessEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_3MapEEENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_:
.LFB27096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r13
	cmpq	24(%rdi), %r13
	je	.L1465
	movq	(%rsi), %rax
	movq	%rax, 0(%r13)
	addq	$8, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1465:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r13, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L1490
	testq	%rax, %rax
	je	.L1477
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1491
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L1468:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1492
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1471:
	leaq	(%rax,%r15), %rdi
	leaq	8(%rax), %rdx
.L1469:
	movq	(%r12), %rsi
	movq	%rsi, (%rax,%rcx)
	cmpq	%r14, %r13
	je	.L1472
	leaq	-8(%r13), %rdx
	movq	%rdx, %rcx
	leaq	15(%rax), %rdx
	subq	%r14, %rcx
	subq	%r14, %rdx
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L1480
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L1480
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1474:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L1474
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r14
	addq	%rax, %rsi
	cmpq	%rdx, %r8
	je	.L1476
	movq	(%r14), %rdx
	movq	%rdx, (%rsi)
.L1476:
	leaq	16(%rax,%rcx), %rdx
.L1472:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1491:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L1493
	movl	$8, %edx
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1477:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1480:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1473:
	movq	(%r14,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %rsi
	jne	.L1473
	jmp	.L1476
.L1492:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1471
.L1490:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1493:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L1468
	.cfi_endproc
.LFE27096:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.section	.text._ZNK2v88internal8compiler29JSNativeContextSpecialization17InferReceiverMapsEPNS1_4NodeES4_PNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler29JSNativeContextSpecialization17InferReceiverMapsEPNS1_4NodeES4_PNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	.type	_ZNK2v88internal8compiler29JSNativeContextSpecialization17InferReceiverMapsEPNS1_4NodeES4_PNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE, @function
_ZNK2v88internal8compiler29JSNativeContextSpecialization17InferReceiverMapsEPNS1_4NodeES4_PNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE:
.LFB23260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	leaq	-72(%rbp), %rcx
	subq	$48, %rsp
	movq	24(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$1, -72(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties23InferReceiverMapsUnsafeEPNS1_12JSHeapBrokerEPNS1_4NodeES6_PNS0_13ZoneHandleSetINS0_3MapEEE@PLT
	cmpl	$1, %eax
	je	.L1538
	movl	%eax, %edx
	xorl	%eax, %eax
	cmpl	$2, %edx
	je	.L1539
.L1494:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1540
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1539:
	.cfi_restore_state
	xorl	%r13d, %r13d
	leaq	-64(%rbp), %r12
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1542:
	movq	6(%rdx), %rsi
	movq	14(%rdx), %r8
	leaq	-2(%rdx), %rcx
	subq	%rsi, %r8
	sarq	$3, %r8
	cmpq	%r8, %r13
	jnb	.L1503
	movslq	%r13d, %r9
	cmpq	%r9, %r8
	jbe	.L1541
	movq	(%rsi,%r9,8), %rdx
.L1504:
	movq	24(%r14), %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	je	.L1494
	addq	$1, %r13
.L1507:
	movq	-72(%rbp), %rdx
	movq	%rdx, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L1500
	testq	%rax, %rax
	jne	.L1542
	testq	%r13, %r13
	je	.L1504
.L1503:
	xorl	%r13d, %r13d
	leaq	-64(%rbp), %r12
	jmp	.L1510
	.p2align 4,,10
	.p2align 3
.L1543:
	movq	6(%rdx), %rax
	subq	$2, %rdx
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%r13, %rdx
	jbe	.L1500
	movslq	%r13d, %rsi
	cmpq	%rsi, %rdx
	jbe	.L1536
	movq	(%rax,%rsi,8), %rdx
.L1515:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -64(%rbp)
	addq	$1, %r13
	call	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movq	-72(%rbp), %rdx
	movq	%rdx, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	je	.L1500
.L1510:
	testq	%rax, %rax
	jne	.L1543
	testq	%r13, %r13
	je	.L1515
	.p2align 4,,10
	.p2align 3
.L1500:
	movl	$1, %eax
	jmp	.L1494
	.p2align 4,,10
	.p2align 3
.L1538:
	movq	-72(%rbp), %rax
	xorl	%r13d, %r13d
	leaq	-64(%rbp), %r12
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L1500
	testq	%rdx, %rdx
	je	.L1498
	.p2align 4,,10
	.p2align 3
.L1544:
	movq	6(%rax), %rcx
	movq	14(%rax), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%r13, %rdx
	jbe	.L1500
	movslq	%r13d, %rsi
	cmpq	%rsi, %rdx
	jbe	.L1536
	movq	(%rcx,%rsi,8), %rax
.L1511:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -64(%rbp)
	addq	$1, %r13
	call	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movq	-72(%rbp), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L1500
	testq	%rdx, %rdx
	jne	.L1544
.L1498:
	testq	%r13, %r13
	je	.L1511
	jmp	.L1500
.L1541:
	movq	%r8, %rdx
	movq	%r9, %rsi
.L1536:
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1540:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23260:
	.size	_ZNK2v88internal8compiler29JSNativeContextSpecialization17InferReceiverMapsEPNS1_4NodeES4_PNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE, .-_ZNK2v88internal8compiler29JSNativeContextSpecialization17InferReceiverMapsEPNS1_4NodeES4_PNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	.section	.text._ZNK2v88internal8compiler29JSNativeContextSpecialization30TryRefineElementAccessFeedbackERKNS1_21ElementAccessFeedbackEPNS1_4NodeES7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler29JSNativeContextSpecialization30TryRefineElementAccessFeedbackERKNS1_21ElementAccessFeedbackEPNS1_4NodeES7_
	.type	_ZNK2v88internal8compiler29JSNativeContextSpecialization30TryRefineElementAccessFeedbackERKNS1_21ElementAccessFeedbackEPNS1_4NodeES7_, @function
_ZNK2v88internal8compiler29JSNativeContextSpecialization30TryRefineElementAccessFeedbackERKNS1_21ElementAccessFeedbackEPNS1_4NodeES7_:
.LFB23215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler21ElementAccessFeedback10keyed_modeEv@PLT
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal8compiler15KeyedAccessMode11access_modeEv@PLT
	testl	%eax, %eax
	je	.L1550
	cmpl	$3, %eax
	je	.L1550
.L1546:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1558
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1550:
	.cfi_restore_state
	movq	64(%rbx), %rax
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZNK2v88internal8compiler29JSNativeContextSpecialization17InferReceiverMapsEPNS1_4NodeES4_PNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	testb	%al, %al
	je	.L1546
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler29JSNativeContextSpecialization28RemoveImpossibleReceiverMapsEPNS1_4NodeEPNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	movq	64(%rbx), %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler21ElementAccessFeedback6RefineERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEPNS0_4ZoneE@PLT
	movq	%rax, %r12
	jmp	.L1546
.L1558:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23215:
	.size	_ZNK2v88internal8compiler29JSNativeContextSpecialization30TryRefineElementAccessFeedbackERKNS1_21ElementAccessFeedbackEPNS1_4NodeES7_, .-_ZNK2v88internal8compiler29JSNativeContextSpecialization30TryRefineElementAccessFeedbackERKNS1_21ElementAccessFeedbackEPNS1_4NodeES7_
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization35FilterMapsAndGetPropertyAccessInfosERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPNS1_4NodeES8_PNS0_10ZoneVectorINS1_18PropertyAccessInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization35FilterMapsAndGetPropertyAccessInfosERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPNS1_4NodeES8_PNS0_10ZoneVectorINS1_18PropertyAccessInfoEEE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization35FilterMapsAndGetPropertyAccessInfosERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPNS1_4NodeES8_PNS0_10ZoneVectorINS1_18PropertyAccessInfoEEE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization35FilterMapsAndGetPropertyAccessInfosERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPNS1_4NodeES8_PNS0_10ZoneVectorINS1_18PropertyAccessInfoEEE:
.LFB23190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r13, %rcx
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -264(%rbp)
	movq	%r12, %rsi
	movl	%edx, -268(%rbp)
	movq	%r8, %rdx
	movq	%r9, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	%rax, -224(%rbp)
	movq	$0, -200(%rbp)
	call	_ZNK2v88internal8compiler29JSNativeContextSpecialization17InferReceiverMapsEPNS1_4NodeES4_PNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	testb	%al, %al
	je	.L1696
.L1560:
	movq	%r13, %rdx
	movq	%r12, %rsi
	leaq	-240(%rbp), %r15
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler29JSNativeContextSpecialization28RemoveImpossibleReceiverMapsEPNS1_4NodeEPNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	movq	-208(%rbp), %rax
	movq	-216(%rbp), %r13
	movq	%rax, -256(%rbp)
	cmpq	%rax, %r13
	je	.L1559
	movq	%r14, %rax
	movq	%r13, %r14
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L1583:
	movq	(%r14), %rdx
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler6MapRef13is_deprecatedEv@PLT
	testb	%al, %al
	jne	.L1602
	movzbl	_ZN2v88internal24FLAG_concurrent_inliningE(%rip), %eax
	subq	$8, %rsp
	movq	24(%r13), %rsi
	leaq	-192(%rbp), %rdi
	movq	-240(%rbp), %rdx
	movq	-232(%rbp), %rcx
	xorl	$1, %eax
	movzbl	%al, %eax
	pushq	%rax
	movl	-268(%rbp), %eax
	pushq	56(%r13)
	pushq	%rax
	movq	-264(%rbp), %rax
	movq	8(%rax), %r8
	movq	16(%rax), %r9
	call	_ZN2v88internal8compiler12JSHeapBroker21GetPropertyAccessInfoENS1_6MapRefENS1_7NameRefENS1_10AccessModeEPNS1_23CompilationDependenciesENS1_19SerializationPolicyE@PLT
	movq	-248(%rbp), %rax
	addq	$32, %rsp
	movq	16(%rax), %rbx
	cmpq	24(%rax), %rbx
	je	.L1585
	movl	-192(%rbp), %eax
	movq	$0, 32(%rbx)
	movl	%eax, (%rbx)
	movq	-184(%rbp), %rdi
	movq	-168(%rbp), %r12
	subq	-176(%rbp), %r12
	movq	$0, 24(%rbx)
	movq	%rdi, 8(%rbx)
	movq	$0, 16(%rbx)
	je	.L1629
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	leaq	7(%r12), %rsi
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1697
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1586:
	movq	%rax, %xmm0
	addq	%rax, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movq	-168(%rbp), %rsi
	movq	-176(%rbp), %rcx
	cmpq	%rcx, %rsi
	je	.L1589
	subq	$8, %rsi
	leaq	15(%rcx), %rdx
	subq	%rcx, %rsi
	subq	%rax, %rdx
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L1630
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rdi
	je	.L1630
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %r8
	shrq	%r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L1591:
	movdqu	(%rcx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%r8, %rdx
	jne	.L1591
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	addq	%rdx, %rcx
	addq	%rax, %rdx
	cmpq	%r8, %rdi
	je	.L1593
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
.L1593:
	leaq	8(%rax,%rsi), %rax
.L1589:
	movq	%rax, 24(%rbx)
	movq	-152(%rbp), %rdi
	movq	-136(%rbp), %r12
	subq	-144(%rbp), %r12
	movq	$0, 56(%rbx)
	movq	%rdi, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 64(%rbx)
	je	.L1631
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	leaq	7(%r12), %rsi
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1698
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1594:
	movq	%rax, %xmm0
	addq	%rax, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rcx
	cmpq	%rcx, %rsi
	je	.L1597
	subq	$8, %rsi
	leaq	15(%rcx), %rdx
	subq	%rcx, %rsi
	subq	%rax, %rdx
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L1632
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rdi
	je	.L1632
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %r8
	shrq	%r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L1599:
	movdqu	(%rcx,%rdx), %xmm2
	movups	%xmm2, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %r8
	jne	.L1599
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	addq	%rdx, %rcx
	addq	%rax, %rdx
	cmpq	%rdi, %r8
	je	.L1601
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
.L1601:
	leaq	8(%rax,%rsi), %rax
.L1597:
	movq	%rax, 56(%rbx)
	movq	-120(%rbp), %rax
	movq	%rax, 72(%rbx)
	movq	-112(%rbp), %rax
	movq	%rax, 80(%rbx)
	movq	-104(%rbp), %rax
	movq	%rax, 88(%rbx)
	movq	-96(%rbp), %rax
	movq	%rax, 96(%rbx)
	movzbl	-88(%rbp), %eax
	movb	%al, 104(%rbx)
	movq	-80(%rbp), %rax
	movq	%rax, 112(%rbx)
	movq	-72(%rbp), %rax
	movq	%rax, 120(%rbx)
	movq	-64(%rbp), %rax
	movq	%rax, 128(%rbx)
	movq	-248(%rbp), %rax
	addq	$136, 16(%rax)
.L1602:
	addq	$8, %r14
	cmpq	%r14, -256(%rbp)
	jne	.L1583
.L1559:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1699
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1629:
	.cfi_restore_state
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	jmp	.L1586
	.p2align 4,,10
	.p2align 3
.L1631:
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	jmp	.L1594
	.p2align 4,,10
	.p2align 3
.L1585:
	movq	8(%rax), %r8
	movq	%rbx, %r12
	movabsq	$-1085102592571150095, %rcx
	subq	%r8, %r12
	movq	%r12, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	$15790320, %rax
	je	.L1700
	testq	%rax, %rax
	je	.L1633
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1701
	movl	$2147483520, %esi
	movl	$2147483520, %r10d
.L1604:
	movq	-248(%rbp), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rsi, %rax
	jb	.L1702
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L1607:
	leaq	136(%rcx), %rax
	addq	%rcx, %r10
	movq	%rax, -280(%rbp)
.L1605:
	movl	-192(%rbp), %eax
	addq	%rcx, %r12
	movq	$0, 32(%r12)
	movl	%eax, (%r12)
	movq	-184(%rbp), %rdi
	movq	-168(%rbp), %rdx
	subq	-176(%rbp), %rdx
	movq	$0, 24(%r12)
	movq	%rdi, 8(%r12)
	movq	$0, 16(%r12)
	je	.L1636
	movq	16(%rdi), %rax
	movq	24(%rdi), %r9
	leaq	7(%rdx), %rsi
	andq	$-8, %rsi
	subq	%rax, %r9
	cmpq	%r9, %rsi
	ja	.L1703
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1608:
	movq	%rax, %xmm0
	addq	%rax, %rdx
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, 32(%r12)
	movups	%xmm0, 16(%r12)
	movq	-168(%rbp), %rdi
	movq	-176(%rbp), %rsi
	cmpq	%rsi, %rdi
	je	.L1611
	subq	$8, %rdi
	leaq	15(%rsi), %rdx
	subq	%rsi, %rdi
	subq	%rax, %rdx
	movq	%rdi, %r9
	shrq	$3, %r9
	cmpq	$30, %rdx
	jbe	.L1637
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r9
	je	.L1637
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %r11
	shrq	%r11
	salq	$4, %r11
	.p2align 4,,10
	.p2align 3
.L1613:
	movdqu	(%rsi,%rdx), %xmm5
	movups	%xmm5, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%r11, %rdx
	jne	.L1613
	movq	%r9, %r11
	andq	$-2, %r11
	leaq	0(,%r11,8), %rdx
	addq	%rdx, %rsi
	addq	%rax, %rdx
	cmpq	%r11, %r9
	je	.L1615
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
.L1615:
	leaq	8(%rax,%rdi), %rax
.L1611:
	movq	%rax, 24(%r12)
	movq	-152(%rbp), %rdi
	movq	$0, 64(%r12)
	movq	-136(%rbp), %rdx
	movq	$0, 56(%r12)
	subq	-144(%rbp), %rdx
	movq	%rdi, 40(%r12)
	movq	$0, 48(%r12)
	je	.L1638
	movq	16(%rdi), %rax
	movq	24(%rdi), %r9
	leaq	7(%rdx), %rsi
	andq	$-8, %rsi
	subq	%rax, %r9
	cmpq	%r9, %rsi
	ja	.L1704
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1616:
	movq	%rax, %xmm0
	addq	%rax, %rdx
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, 64(%r12)
	movups	%xmm0, 48(%r12)
	movq	-136(%rbp), %rdi
	movq	-144(%rbp), %rsi
	cmpq	%rsi, %rdi
	je	.L1619
	subq	$8, %rdi
	leaq	15(%rsi), %rdx
	subq	%rsi, %rdi
	subq	%rax, %rdx
	movq	%rdi, %r9
	shrq	$3, %r9
	cmpq	$30, %rdx
	jbe	.L1639
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r9
	je	.L1639
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %r11
	shrq	%r11
	salq	$4, %r11
	.p2align 4,,10
	.p2align 3
.L1621:
	movdqu	(%rsi,%rdx), %xmm6
	movups	%xmm6, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %r11
	jne	.L1621
	movq	%r9, %r11
	andq	$-2, %r11
	leaq	0(,%r11,8), %rdx
	addq	%rdx, %rsi
	addq	%rax, %rdx
	cmpq	%r11, %r9
	je	.L1623
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
.L1623:
	leaq	8(%rax,%rdi), %rax
.L1619:
	movq	%rax, 56(%r12)
	movq	-120(%rbp), %rax
	movq	%rax, 72(%r12)
	movq	-112(%rbp), %rax
	movq	%rax, 80(%r12)
	movq	-104(%rbp), %rax
	movq	%rax, 88(%r12)
	movq	-96(%rbp), %rax
	movq	%rax, 96(%r12)
	movzbl	-88(%rbp), %eax
	movb	%al, 104(%r12)
	movq	-80(%rbp), %rax
	movq	%rax, 112(%r12)
	movq	-72(%rbp), %rax
	movq	%rax, 120(%r12)
	movq	-64(%rbp), %rax
	movq	%rax, 128(%r12)
	cmpq	%r8, %rbx
	je	.L1624
	movq	%r8, %rax
	movq	%rcx, %rdx
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1625:
	movl	(%rax), %esi
	addq	$136, %rax
	addq	$136, %rdx
	movl	%esi, -136(%rdx)
	movq	-128(%rax), %rsi
	movq	%rsi, -128(%rdx)
	movdqu	-120(%rax), %xmm3
	movups	%xmm3, -120(%rdx)
	movq	-104(%rax), %rsi
	movq	%rsi, -104(%rdx)
	movq	-96(%rax), %rsi
	movq	$0, -104(%rax)
	movups	%xmm0, -120(%rax)
	movq	%rsi, -96(%rdx)
	movdqu	-88(%rax), %xmm4
	movups	%xmm4, -88(%rdx)
	movq	-72(%rax), %rsi
	movq	%rsi, -72(%rdx)
	movq	-64(%rax), %rsi
	movq	$0, -72(%rax)
	movups	%xmm0, -88(%rax)
	movq	%rsi, -64(%rdx)
	movq	-56(%rax), %rsi
	movq	%rsi, -56(%rdx)
	movq	-48(%rax), %rsi
	movq	%rsi, -48(%rdx)
	movq	-40(%rax), %rsi
	movq	%rsi, -40(%rdx)
	movzbl	-32(%rax), %esi
	movb	%sil, -32(%rdx)
	movq	-24(%rax), %rsi
	movq	%rsi, -24(%rdx)
	movq	-16(%rax), %rsi
	movq	%rsi, -16(%rdx)
	movq	-8(%rax), %rsi
	movq	%rsi, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L1625
	leaq	-136(%rbx), %rax
	movabsq	$1220740416642543857, %rbx
	subq	%r8, %rax
	shrq	$3, %rax
	imulq	%rbx, %rax
	movabsq	$2305843009213693951, %rbx
	andq	%rbx, %rax
	addq	$2, %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, -280(%rbp)
.L1624:
	movq	-248(%rbp), %rax
	movq	%rcx, %xmm0
	movhps	-280(%rbp), %xmm0
	movq	%r10, 24(%rax)
	movups	%xmm0, 8(%rax)
	jmp	.L1602
	.p2align 4,,10
	.p2align 3
.L1701:
	testq	%rdx, %rdx
	jne	.L1705
	movq	$136, -280(%rbp)
	xorl	%r10d, %r10d
	xorl	%ecx, %ecx
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1638:
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L1616
	.p2align 4,,10
	.p2align 3
.L1636:
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1630:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1590:
	movq	(%rcx,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %rdi
	jne	.L1590
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1632:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1598:
	movq	(%rcx,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %rdi
	jne	.L1598
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1633:
	movl	$136, %esi
	movl	$136, %r10d
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1696:
	movq	-264(%rbp), %rax
	movq	-216(%rbp), %rdi
	movq	40(%rax), %rbx
	movq	32(%rax), %r15
	movq	-200(%rbp), %rax
	movq	%rbx, %rcx
	subq	%r15, %rcx
	subq	%rdi, %rax
	movq	%rcx, %r8
	sarq	$3, %rax
	sarq	$3, %r8
	cmpq	%rax, %r8
	ja	.L1706
	movq	-208(%rbp), %rsi
	movq	%rsi, %rdx
	subq	%rdi, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r8
	jbe	.L1707
	movq	%rdx, %r8
	testq	%rdx, %rdx
	je	.L1575
	movq	%r15, %rsi
	movq	%rcx, -256(%rbp)
	call	memmove@PLT
	movq	-264(%rbp), %rax
	movq	-208(%rbp), %rsi
	movq	-216(%rbp), %rdi
	movq	-256(%rbp), %rcx
	movq	%rsi, %r8
	movq	40(%rax), %rbx
	movq	32(%rax), %r15
	subq	%rdi, %r8
.L1575:
	leaq	(%r15,%r8), %rdx
	cmpq	%rbx, %rdx
	je	.L1708
	leaq	-8(%rbx), %rdi
	leaq	16(%r15,%r8), %rax
	subq	%rdx, %rdi
	shrq	$3, %rdi
	cmpq	%rax, %rsi
	leaq	16(%rsi), %rax
	setnb	%r8b
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %r8b
	je	.L1678
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L1678
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %r8
	shrq	%r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L1578:
	movdqu	(%rdx,%rax), %xmm7
	movups	%xmm7, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%r8, %rax
	jne	.L1578
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	addq	%r8, %rdx
	addq	%r8, %rsi
	cmpq	%rax, %rdi
	je	.L1580
	movq	(%rdx), %rax
	movq	%rax, (%rsi)
.L1580:
	addq	-216(%rbp), %rcx
	movq	%rcx, %rdx
	jmp	.L1566
	.p2align 4,,10
	.p2align 3
.L1639:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1620:
	movq	(%rsi,%rdx,8), %r11
	movq	%r11, (%rax,%rdx,8)
	movq	%rdx, %r11
	addq	$1, %rdx
	cmpq	%r9, %r11
	jne	.L1620
	jmp	.L1623
	.p2align 4,,10
	.p2align 3
.L1637:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1612:
	movq	(%rsi,%rdx,8), %r11
	movq	%r11, (%rax,%rdx,8)
	movq	%rdx, %r11
	addq	$1, %rdx
	cmpq	%r9, %r11
	jne	.L1612
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1698:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1594
	.p2align 4,,10
	.p2align 3
.L1697:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1586
	.p2align 4,,10
	.p2align 3
.L1678:
	movq	(%rdx), %rax
	addq	$8, %rdx
	addq	$8, %rsi
	movq	%rax, -8(%rsi)
	cmpq	%rdx, %rbx
	jne	.L1678
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1707:
	leaq	(%rdi,%rcx), %rdx
	cmpq	%rbx, %r15
	je	.L1566
	movq	%rcx, %rdx
	movq	%r15, %rsi
	movq	%rcx, -256(%rbp)
	call	memmove@PLT
	movq	-256(%rbp), %rcx
	addq	-216(%rbp), %rcx
	movq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L1566:
	movq	%rdx, -208(%rbp)
	jmp	.L1560
	.p2align 4,,10
	.p2align 3
.L1706:
	xorl	%edx, %edx
	testq	%r8, %r8
	je	.L1562
	movq	-224(%rbp), %rdi
	leaq	7(%rcx), %rsi
	andq	$-8, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L1709
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L1562:
	cmpq	%rbx, %r15
	je	.L1570
	subq	$8, %rbx
	leaq	15(%r15), %rax
	subq	%r15, %rbx
	subq	%rdx, %rax
	shrq	$3, %rbx
	cmpq	$30, %rax
	jbe	.L1628
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rbx
	je	.L1628
	addq	$1, %rbx
	xorl	%eax, %eax
	movq	%rbx, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1568:
	movdqu	(%r15,%rax), %xmm7
	movups	%xmm7, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L1568
	movq	%rbx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rsi
	addq	%rsi, %r15
	addq	%rdx, %rsi
	cmpq	%rax, %rbx
	je	.L1570
	movq	(%r15), %rax
	movq	%rax, (%rsi)
.L1570:
	movq	%rdx, -216(%rbp)
	addq	%rcx, %rdx
	movq	%rdx, -200(%rbp)
	jmp	.L1566
.L1702:
	movq	%r8, -288(%rbp)
	movq	%r10, -280(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-280(%rbp), %r10
	movq	-288(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L1607
.L1703:
	movq	%r8, -312(%rbp)
	movq	%rcx, -304(%rbp)
	movq	%r10, -296(%rbp)
	movq	%rdx, -288(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-288(%rbp), %rdx
	movq	-296(%rbp), %r10
	movq	-304(%rbp), %rcx
	movq	-312(%rbp), %r8
	jmp	.L1608
.L1704:
	movq	%rdx, -312(%rbp)
	movq	%r8, -304(%rbp)
	movq	%rcx, -296(%rbp)
	movq	%r10, -288(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-288(%rbp), %r10
	movq	-296(%rbp), %rcx
	movq	-304(%rbp), %r8
	movq	-312(%rbp), %rdx
	jmp	.L1616
.L1628:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1567:
	movq	(%r15,%rax,8), %rsi
	movq	%rsi, (%rdx,%rax,8)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rsi, %rbx
	jne	.L1567
	jmp	.L1570
.L1708:
	leaq	(%rdi,%rcx), %rdx
	jmp	.L1566
.L1709:
	movq	%rcx, -256(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-256(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L1562
.L1700:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1705:
	cmpq	$15790320, %rdx
	movl	$15790320, %eax
	cmova	%rax, %rdx
	imulq	$136, %rdx, %r10
	movq	%r10, %rsi
	jmp	.L1604
.L1699:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23190:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization35FilterMapsAndGetPropertyAccessInfosERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPNS1_4NodeES8_PNS0_10ZoneVectorINS1_18PropertyAccessInfoEEE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization35FilterMapsAndGetPropertyAccessInfosERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPNS1_4NodeES8_PNS0_10ZoneVectorINS1_18PropertyAccessInfoEEE
	.section	.text._ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	.type	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_, @function
_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_:
.LFB27728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r13
	cmpq	24(%rdi), %r13
	je	.L1711
	movq	(%rsi), %rax
	movq	%rax, 0(%r13)
	addq	$8, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1711:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r13, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L1736
	testq	%rax, %rax
	je	.L1723
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1737
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L1714:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1738
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1717:
	leaq	(%rax,%r15), %rdi
	leaq	8(%rax), %rdx
.L1715:
	movq	(%r12), %rsi
	movq	%rsi, (%rax,%rcx)
	cmpq	%r14, %r13
	je	.L1718
	leaq	-8(%r13), %rdx
	movq	%rdx, %rcx
	leaq	15(%rax), %rdx
	subq	%r14, %rcx
	subq	%r14, %rdx
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L1726
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L1726
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1720:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L1720
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r14
	addq	%rax, %rsi
	cmpq	%rdx, %r8
	je	.L1722
	movq	(%r14), %rdx
	movq	%rdx, (%rsi)
.L1722:
	leaq	16(%rax,%rcx), %rdx
.L1718:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1737:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L1739
	movl	$8, %edx
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L1715
	.p2align 4,,10
	.p2align 3
.L1723:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1726:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1719:
	movq	(%r14,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %rsi
	jne	.L1719
	jmp	.L1722
.L1738:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1717
.L1736:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1739:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L1714
	.cfi_endproc
.LFE27728:
	.size	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_, .-_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	.section	.text._ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	.type	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_, @function
_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_:
.LFB27733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L1778
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L1756
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1779
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L1742:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L1780
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1745:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L1743
	.p2align 4,,10
	.p2align 3
.L1779:
	testq	%rdx, %rdx
	jne	.L1781
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L1743:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L1746
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L1759
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L1759
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1748:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L1748
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L1750
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L1750:
	leaq	16(%rax,%r8), %rcx
.L1746:
	cmpq	%r14, %r12
	je	.L1751
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L1760
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L1760
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1753:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L1753
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L1755
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L1755:
	leaq	8(%rcx,%r9), %rcx
.L1751:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1756:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L1742
	.p2align 4,,10
	.p2align 3
.L1760:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1752:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L1752
	jmp	.L1755
	.p2align 4,,10
	.p2align 3
.L1759:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1747:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L1747
	jmp	.L1750
.L1780:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L1745
.L1778:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1781:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L1742
	.cfi_endproc
.LFE27733:
	.size	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_, .-_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	.section	.rodata._ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceElementAccessEPNS1_4NodeES4_S4_RKNS1_21ElementAccessFeedbackE.str1.1,"aMS",@progbits,1
.LC36:
	.string	"data for typed array "
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceElementAccessEPNS1_4NodeES4_S4_RKNS1_21ElementAccessFeedbackE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceElementAccessEPNS1_4NodeES4_S4_RKNS1_21ElementAccessFeedbackE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceElementAccessEPNS1_4NodeES4_S4_RKNS1_21ElementAccessFeedbackE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceElementAccessEPNS1_4NodeES4_S4_RKNS1_21ElementAccessFeedbackE:
.LFB23216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rax, %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -560(%rbp)
	movq	%rcx, -568(%rbp)
	movq	%r8, -528(%rbp)
	movq	%rsi, -544(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	xorl	%esi, %esi
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	16(%r14), %rbx
	movq	%rax, -400(%rbp)
	movq	352(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1990
.L1783:
	movq	-544(%rbp), %rdi
	call	_ZN2v88internal8compiler14NodeProperties20FindFrameStateBeforeEPNS1_4NodeES4_@PLT
	movq	-528(%rbp), %rdi
	movq	%rax, -432(%rbp)
	call	_ZNK2v88internal8compiler21ElementAccessFeedback17transition_groupsEv@PLT
	movq	16(%rax), %rcx
	cmpq	%rcx, 8(%rax)
	je	.L1815
	movq	-528(%rbp), %r12
	movq	-408(%rbp), %r13
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler21ElementAccessFeedback10keyed_modeEv@PLT
	movq	%rax, -160(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -552(%rbp)
	call	_ZNK2v88internal8compiler15KeyedAccessMode11access_modeEv@PLT
	testl	%eax, %eax
	je	.L1913
	cmpl	$3, %eax
	je	.L1913
.L1786:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler21ElementAccessFeedback10keyed_modeEv@PLT
	movq	-552(%rbp), %rdi
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal8compiler15KeyedAccessMode11access_modeEv@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L1914
	cmpl	$3, %eax
	jne	.L1789
.L1914:
	movq	-488(%rbp), %rax
	movq	(%rax), %rax
	cmpw	$30, 16(%rax)
	je	.L1991
.L1789:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler21ElementAccessFeedback17transition_groupsEv@PLT
	movq	16(%rax), %rbx
	cmpq	%rbx, 8(%rax)
	je	.L1795
	movq	24(%r14), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler21ElementAccessFeedback17HasOnlyStringMapsEPNS1_12JSHeapBrokerE@PLT
	testb	%al, %al
	jne	.L1992
.L1795:
	movq	16(%r14), %rax
	movq	56(%r14), %rdx
	leaq	-288(%rbp), %r15
	movq	24(%r14), %rsi
	movq	%r15, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rcx
	call	_ZN2v88internal8compiler17AccessInfoFactoryC1EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneE@PLT
	movq	64(%r14), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-256(%rbp), %rdx
	movq	$0, -248(%rbp)
	movq	%rax, -256(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	call	_ZNK2v88internal8compiler17AccessInfoFactory25ComputeElementAccessInfosERKNS1_21ElementAccessFeedbackEPNS0_10ZoneVectorINS1_17ElementAccessInfoEEE@PLT
	testb	%al, %al
	je	.L1815
	movq	-248(%rbp), %rax
	movq	-240(%rbp), %r12
	movq	%rax, -440(%rbp)
	cmpq	%r12, %rax
	je	.L1815
	cmpl	$1, %r13d
	je	.L1993
	cmpl	$3, %r13d
	je	.L1810
.L1808:
	movq	24(%r14), %rsi
.L1894:
	movq	-440(%rbp), %rbx
	movq	-552(%rbp), %r15
	leaq	-152(%rbp), %r13
	jmp	.L1819
	.p2align 4,,10
	.p2align 3
.L1814:
	movq	24(%r14), %rsi
.L1813:
	addq	$72, %rbx
	cmpq	%rbx, %r12
	je	.L1994
.L1819:
	movzbl	(%rbx), %eax
	subl	$17, %eax
	cmpb	$10, %al
	ja	.L1813
	movq	-488(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_121GetTypedArrayConstantEPNS1_12JSHeapBrokerEPNS1_4NodeE
	cmpb	$0, -160(%rbp)
	je	.L1814
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	je	.L1814
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler15JSTypedArrayRef10serializedEv@PLT
	testb	%al, %al
	jne	.L1814
	movq	24(%r14), %rdi
	cmpb	$0, 124(%rdi)
	je	.L1815
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$8, %edx
	leaq	.LC12(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$21, %edx
	movq	%r12, %rdi
	leaq	.LC36(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$2, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$59, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$1596, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r13
	testq	%r13, %r13
	je	.L1995
	cmpb	$0, 56(%r13)
	je	.L1817
	movzbl	67(%r13), %eax
.L1818:
	movq	%r12, %rdi
	movsbl	%al, %esi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	.p2align 4,,10
	.p2align 3
.L1815:
	xorl	%eax, %eax
.L1785:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1996
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1913:
	.cfi_restore_state
	movq	-552(%rbp), %rbx
	movq	64(%r14), %rax
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	-488(%rbp), %rsi
	movq	$0, -152(%rbp)
	movq	%rbx, %rcx
	movq	%rax, -160(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNK2v88internal8compiler29JSNativeContextSpecialization17InferReceiverMapsEPNS1_4NodeES4_PNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	movq	-528(%rbp), %r15
	movq	%r15, %r12
	testb	%al, %al
	je	.L1786
	movq	-488(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler29JSNativeContextSpecialization28RemoveImpossibleReceiverMapsEPNS1_4NodeEPNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	movq	64(%r14), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21ElementAccessFeedback6RefineERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEPNS0_4ZoneE@PLT
	movq	%rax, %r12
	jmp	.L1786
	.p2align 4,,10
	.p2align 3
.L1809:
	addq	$72, %rax
	cmpq	%rax, %r12
	je	.L1808
.L1810:
	cmpb	$5, (%rax)
	ja	.L1809
	movq	16(%r14), %rax
	movq	360(%rax), %rdi
	call	_ZN2v88internal7Isolate27IsNoElementsProtectorIntactEv@PLT
	testb	%al, %al
	je	.L1815
	movq	16(%r14), %rax
	movq	-552(%rbp), %rbx
	movl	$1, %ecx
	movq	56(%r14), %r12
	movq	24(%r14), %rsi
	movq	360(%rax), %rdx
	movq	%rbx, %rdi
	addq	$4504, %rdx
	call	_ZN2v88internal8compiler15PropertyCellRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler23CompilationDependencies17DependOnProtectorERKNS1_15PropertyCellRefE@PLT
.L1805:
	movq	-248(%rbp), %rax
	movq	-240(%rbp), %r12
	movq	24(%r14), %rsi
	movq	%rax, -440(%rbp)
	cmpq	%r12, %rax
	jne	.L1894
	movq	56(%r14), %rax
	movq	16(%r14), %r13
	movq	%rsi, -344(%rbp)
	movq	%rax, -336(%rbp)
	xorl	%eax, %eax
	movq	%r13, -352(%rbp)
	jmp	.L1820
	.p2align 4,,10
	.p2align 3
.L1990:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%rbx)
	movq	%rax, %rsi
	jmp	.L1783
	.p2align 4,,10
	.p2align 3
.L1992:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler21ElementAccessFeedback10keyed_modeEv@PLT
	movq	-552(%rbp), %r8
	movq	%r14, %rdi
	movq	-568(%rbp), %rcx
	movq	-560(%rbp), %rdx
	movq	-544(%rbp), %rsi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceElementAccessOnStringEPNS1_4NodeES4_S4_RKNS1_15KeyedAccessModeE
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1991:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler21ElementAccessFeedback10keyed_modeEv@PLT
	movq	-552(%rbp), %rdi
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal8compiler15KeyedAccessMode9load_modeEv@PLT
	movq	-560(%rbp), %rdx
	movl	%r13d, %ecx
	movq	%r14, %rdi
	movq	-544(%rbp), %rsi
	movl	%eax, %r8d
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization33ReduceElementLoadFromHeapConstantEPNS1_4NodeES4_NS1_10AccessModeENS0_19KeyedAccessLoadModeE
	testq	%rax, %rax
	jne	.L1785
	jmp	.L1789
	.p2align 4,,10
	.p2align 3
.L1994:
	movq	56(%r14), %rax
	movq	-248(%rbp), %r12
	movq	%rsi, -344(%rbp)
	movq	16(%r14), %r13
	movq	%rax, -336(%rbp)
	movq	-240(%rbp), %rax
	movq	%r13, -352(%rbp)
	subq	%r12, %rax
	cmpq	$72, %rax
	je	.L1997
.L1820:
	movq	64(%r14), %rdx
	movq	-400(%rbp), %rbx
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	%rdx, -224(%rbp)
	movq	$0, -200(%rbp)
	movq	%rdx, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	%rbx, -496(%rbp)
	testq	%rax, %rax
	je	.L1844
	leaq	-192(%rbp), %rax
	leaq	-384(%rbp), %rbx
	movq	$0, -536(%rbp)
	movq	%rax, -600(%rbp)
	leaq	-224(%rbp), %rax
	leaq	-368(%rbp), %r15
	movq	%rax, -592(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, -584(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -512(%rbp)
	movq	-536(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L1883:
	leaq	(%rax,%rax,8), %rax
	movl	$1, %ecx
	movq	%rbx, %rdi
	leaq	(%r12,%rax,8), %r12
	movq	-408(%rbp), %rax
	movq	%r12, -576(%rbp)
	movq	%rax, -392(%rbp)
	movq	16(%r12), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	56(%r12), %rcx
	movq	48(%r12), %r13
	movq	%rcx, -520(%rbp)
	cmpq	%r13, %rcx
	jne	.L1851
	jmp	.L1852
	.p2align 4,,10
	.p2align 3
.L1998:
	cmpb	$4, %al
	je	.L1907
	cmpb	$2, %al
	je	.L1908
	cmpb	$6, %al
	movl	$7, %ecx
	cmovne	%eax, %ecx
.L1849:
	cmpb	%cl, %dl
	je	.L1910
	cmpb	$1, %al
	jbe	.L1893
	movl	$1, %eax
.L1850:
	movq	-440(%rbp), %xmm0
	subq	$8, %rsp
	movq	%r8, %rdi
	addq	$8, %r13
	movq	-488(%rbp), %xmm1
	movb	%al, -320(%rbp)
	movhps	-464(%rbp), %xmm0
	movhps	-432(%rbp), %xmm1
	movaps	%xmm0, -432(%rbp)
	movq	-424(%rbp), %rdx
	movq	-432(%rbp), %rax
	movaps	%xmm1, -480(%rbp)
	pushq	%rdx
	pushq	%rax
	pushq	-320(%rbp)
	movups	%xmm0, -312(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22TransitionElementsKindENS1_18ElementsTransitionE@PLT
	addq	$32, %rsp
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movdqa	-480(%rbp), %xmm1
	movq	%rax, %rsi
	movl	$3, %edx
	movq	-496(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	%rax, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -392(%rbp)
	cmpq	%r13, -520(%rbp)
	je	.L1852
.L1851:
	movq	0(%r13), %rdx
	movq	24(%r14), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	16(%r14), %rax
	movq	-392(%rbp), %rcx
	movq	%rbx, %rdi
	movq	376(%rax), %r8
	movq	(%rax), %r12
	movq	%rcx, -432(%rbp)
	movq	%r8, -448(%rbp)
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	%r15, %rdi
	movq	%rax, -464(%rbp)
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	%rbx, %rdi
	movq	%rax, -440(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13elements_kindEv@PLT
	movq	%r15, %rdi
	movb	%al, -480(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13elements_kindEv@PLT
	movzbl	-480(%rbp), %edx
	movq	-448(%rbp), %r8
	testb	%al, %al
	jne	.L1998
	cmpb	$1, %dl
	je	.L1850
.L1893:
	subl	$2, %edx
	cmpb	$1, %dl
	seta	%al
	jmp	.L1850
	.p2align 4,,10
	.p2align 3
.L1910:
	xorl	%eax, %eax
	jmp	.L1850
	.p2align 4,,10
	.p2align 3
.L1852:
	movq	-240(%rbp), %rax
	subq	-248(%rbp), %rax
	movabsq	$-8198552921648689607, %rcx
	sarq	$3, %rax
	imulq	%rcx, %rax
	subq	$1, %rax
	cmpq	-536(%rbp), %rax
	je	.L1999
	movq	-576(%rbp), %rax
	movq	16(%r14), %r10
	movl	$1, %r13d
	movq	16(%rax), %r8
	movq	24(%rax), %r12
	cmpq	%r8, %r12
	je	.L1854
	movq	%rbx, -464(%rbp)
	movq	%r8, %rbx
	movq	%r14, -432(%rbp)
	movq	%r15, -440(%rbp)
	jmp	.L1881
	.p2align 4,,10
	.p2align 3
.L2002:
	testq	%rax, %rax
	jne	.L1856
	cmpq	%rdx, %r13
	je	.L1880
	movq	16(%r15), %r14
	movq	24(%r15), %rdx
	subq	%r14, %rdx
	cmpq	$31, %rdx
	jbe	.L2000
	leaq	32(%r14), %rdx
	movq	%rdx, 16(%r15)
.L1859:
	movq	%r15, (%r14)
	movq	$0, 8(%r14)
	movq	$0, 16(%r14)
	movq	$0, 24(%r14)
	cmpq	-368(%rbp), %r13
	jnb	.L1860
	movq	-584(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r13, -320(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	movq	16(%r14), %rsi
	cmpq	24(%r14), %rsi
	je	.L1861
	movq	-368(%rbp), %rdx
	movq	%rdx, (%rsi)
	addq	$8, 16(%r14)
.L1862:
	movq	-432(%rbp), %rax
	orq	$2, %r14
	movq	%r14, %r13
	movq	16(%rax), %r10
.L1880:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	je	.L2001
.L1881:
	movq	(%r10), %rax
	movq	(%rbx), %rdx
	movq	(%rax), %r15
	movq	%r13, %rax
	movq	%rdx, -368(%rbp)
	andl	$3, %eax
	cmpq	$1, %rax
	jne	.L2002
	addq	$8, %rbx
	movq	%rdx, %r13
	cmpq	%rbx, %r12
	jne	.L1881
.L2001:
	movq	-432(%rbp), %r14
	movq	-464(%rbp), %rbx
	movq	-440(%rbp), %r15
.L1854:
	movq	376(%r10), %rdi
	movq	-392(%rbp), %rax
	movq	%r13, %rsi
	movq	(%r10), %r12
	movq	%rax, -432(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CompareMapsENS0_13ZoneHandleSetINS0_3MapEEE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$3, %edx
	movq	-488(%rbp), %xmm0
	movq	-512(%rbp), %rcx
	movq	%rax, %rsi
	movq	-496(%rbp), %rax
	movhps	-432(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%rax, -392(%rbp)
	movq	%rax, -432(%rbp)
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r12, %rdi
	movq	-432(%rbp), %xmm0
	movq	-512(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-496(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -432(%rbp)
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	-512(%rbp), %rcx
	movq	-432(%rbp), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -496(%rbp)
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	-512(%rbp), %rcx
	movq	%rax, %rsi
	movq	-432(%rbp), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-392(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	movq	16(%r14), %rax
	movq	%rdi, -432(%rbp)
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -440(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8MapGuardENS0_13ZoneHandleSetINS0_3MapEEE@PLT
	movq	-440(%rbp), %r9
	xorl	%r8d, %r8d
	movq	-488(%rbp), %xmm0
	movq	-512(%rbp), %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -64(%rbp)
	movhps	-432(%rbp), %xmm0
	movq	%r9, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -392(%rbp)
.L1853:
	movq	-528(%rbp), %rdi
	call	_ZNK2v88internal8compiler21ElementAccessFeedback10keyed_modeEv@PLT
	subq	$8, %rsp
	movq	%r14, %rsi
	movq	-560(%rbp), %rcx
	movq	-392(%rbp), %r9
	movq	-568(%rbp), %r8
	pushq	%r15
	pushq	-576(%rbp)
	movq	-488(%rbp), %rdx
	movq	-584(%rbp), %rdi
	pushq	%r12
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization18BuildElementAccessEPNS1_4NodeES4_S4_S4_S4_RKNS1_17ElementAccessInfoERKNS1_15KeyedAccessModeE
	movq	-320(%rbp), %rax
	addq	$32, %rsp
	movq	%r15, %rsi
	movq	-592(%rbp), %rdi
	movq	%rax, -368(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movq	-312(%rbp), %rax
	movq	-600(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, -368(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movq	-304(%rbp), %rax
	movq	-552(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, -368(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movq	-248(%rbp), %r12
	movq	-240(%rbp), %rax
	movabsq	$-8198552921648689607, %rdi
	addq	$1, -536(%rbp)
	movq	-536(%rbp), %rcx
	subq	%r12, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	cmpq	%rax, %rcx
	jnb	.L1882
	movq	24(%r14), %rsi
	movq	%rcx, %rax
	jmp	.L1883
	.p2align 4,,10
	.p2align 3
.L1856:
	movq	6(%r13), %r11
	movq	14(%r13), %rsi
	leaq	-2(%r13), %r14
	subq	%r11, %rsi
	sarq	$3, %rsi
	je	.L1864
	xorl	%eax, %eax
	jmp	.L1865
	.p2align 4,,10
	.p2align 3
.L2003:
	addq	$1, %rax
	cmpq	%rsi, %rax
	je	.L1864
.L1865:
	cmpq	%rdx, (%r11,%rax,8)
	je	.L1880
	jbe	.L2003
.L1864:
	movq	16(%r15), %r13
	movq	24(%r15), %rdx
	subq	%r13, %rdx
	cmpq	$31, %rdx
	jbe	.L2004
	leaq	32(%r13), %rdx
	movq	%rdx, 16(%r15)
.L1867:
	movq	%r15, 0(%r13)
	movq	%r13, %rdi
	movq	$0, 8(%r13)
	movq	$0, 16(%r13)
	movq	$0, 24(%r13)
	movq	16(%r14), %rsi
	subq	8(%r14), %rsi
	sarq	$3, %rsi
	addq	$1, %rsi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE7reserveEm
	movq	8(%r14), %rdi
	cmpq	%rdi, 16(%r14)
	movq	16(%r13), %rsi
	je	.L1868
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L1873:
	leaq	(%rdi,%r15,8), %rdx
	movq	24(%r13), %rdi
	movq	(%rdx), %rax
	cmpq	-368(%rbp), %rax
	ja	.L1869
	cmpq	%rsi, %rdi
	je	.L1870
	movq	%rax, (%rsi)
	movq	16(%r13), %rax
	addq	$1, %r15
	leaq	8(%rax), %rsi
	movq	%rsi, 16(%r13)
	movq	8(%r14), %rdi
	movq	16(%r14), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rax, %r15
	jb	.L1873
.L1872:
	movq	24(%r13), %rdi
.L1869:
	cmpq	%rdi, %rsi
	je	.L1874
	movq	-368(%rbp), %rdx
	movq	%rdx, (%rsi)
	addq	$8, 16(%r13)
.L1875:
	movq	8(%r14), %rsi
	movq	16(%r14), %rdx
	subq	%rsi, %rdx
	sarq	$3, %rdx
	cmpq	%r15, %rdx
	jbe	.L1879
	.p2align 4,,10
	.p2align 3
.L1876:
	leaq	(%rsi,%r15,8), %rdx
	movq	16(%r13), %rsi
	cmpq	24(%r13), %rsi
	je	.L1877
	movq	(%rdx), %rax
	addq	$1, %r15
	movq	%rax, (%rsi)
	addq	$8, 16(%r13)
	movq	8(%r14), %rsi
	movq	16(%r14), %rax
	subq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rax, %r15
	jb	.L1876
.L1879:
	movq	-432(%rbp), %rax
	orq	$2, %r13
	movq	16(%rax), %r10
	jmp	.L1880
	.p2align 4,,10
	.p2align 3
.L1870:
	movq	%r13, %rdi
	addq	$1, %r15
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	8(%r14), %rdi
	movq	16(%r14), %rax
	movq	16(%r13), %rsi
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%r15, %rax
	ja	.L1873
	jmp	.L1872
	.p2align 4,,10
	.p2align 3
.L1877:
	movq	%r13, %rdi
	addq	$1, %r15
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	8(%r14), %rsi
	movq	16(%r14), %rax
	subq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%r15, %rax
	ja	.L1876
	jmp	.L1879
	.p2align 4,,10
	.p2align 3
.L1907:
	movl	$5, %ecx
	jmp	.L1849
	.p2align 4,,10
	.p2align 3
.L1860:
	movq	-440(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-584(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r13, -320(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	jmp	.L1862
	.p2align 4,,10
	.p2align 3
.L1908:
	movl	$3, %ecx
	jmp	.L1849
	.p2align 4,,10
	.p2align 3
.L1999:
	movq	-576(%rbp), %rax
	movq	-496(%rbp), %r12
	leaq	-392(%rbp), %rdx
	leaq	-352(%rbp), %rdi
	movq	-488(%rbp), %rsi
	leaq	8(%rax), %r8
	movq	%r12, %rcx
	call	_ZN2v88internal8compiler21PropertyAccessBuilder14BuildCheckMapsEPNS1_4NodeEPS4_S4_RKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE@PLT
	movq	$0, -496(%rbp)
	jmp	.L1853
	.p2align 4,,10
	.p2align 3
.L1874:
	movq	-440(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L2000:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L1859
	.p2align 4,,10
	.p2align 3
.L1861:
	movq	-440(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	jmp	.L1862
	.p2align 4,,10
	.p2align 3
.L1882:
	movq	-152(%rbp), %r12
	movq	-144(%rbp), %r13
	subq	%r12, %r13
	sarq	$3, %r13
	testl	%r13d, %r13d
	je	.L2005
	cmpl	$1, %r13d
	jne	.L1887
	movq	-216(%rbp), %rax
	movq	(%rax), %rbx
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -408(%rbp)
	movq	(%r12), %r8
	movq	%r8, -400(%rbp)
.L1843:
	movq	8(%r14), %rdi
	movq	-544(%rbp), %rsi
	movq	%rbx, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rax
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L2004:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1867
.L2005:
	movq	16(%r14), %r13
.L1844:
	movq	352(%r13), %rbx
	testq	%rbx, %rbx
	je	.L2006
.L1885:
	movq	%rbx, -400(%rbp)
	movq	%rbx, %rcx
	movq	%rbx, %r8
	movq	%rbx, -408(%rbp)
	jmp	.L1843
.L1993:
	movq	64(%r14), %rax
	movq	$0, -152(%rbp)
	leaq	-192(%rbp), %r13
	movq	$0, -144(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-224(%rbp), %rax
	movq	$0, -136(%rbp)
	movq	%rax, -464(%rbp)
	movq	%r12, -480(%rbp)
	movq	-552(%rbp), %r12
.L1803:
	movq	-440(%rbp), %rax
	movq	16(%rax), %r8
	movq	24(%rax), %rbx
	cmpq	%r8, %rbx
	je	.L1797
	movq	%r8, %r15
	jmp	.L1802
	.p2align 4,,10
	.p2align 3
.L2007:
	movq	-528(%rbp), %rdi
	call	_ZNK2v88internal8compiler21ElementAccessFeedback10keyed_modeEv@PLT
	movq	-464(%rbp), %rdi
	movq	%rax, -224(%rbp)
	call	_ZNK2v88internal8compiler15KeyedAccessMode10store_modeEv@PLT
	cmpl	$1, %eax
	je	.L1801
.L1799:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L1797
.L1802:
	movq	(%r15), %rdx
	movq	24(%r14), %rsi
	movq	%r13, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef13elements_kindEv@PLT
	cmpb	$11, %al
	ja	.L1798
	testb	$1, %al
	jne	.L1801
.L1798:
	cmpb	$12, %al
	jne	.L2007
.L1801:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler6MapRef39HasOnlyStablePrototypesWithFastElementsEPNS0_10ZoneVectorIS2_EE@PLT
	testb	%al, %al
	jne	.L1799
	jmp	.L1815
	.p2align 4,,10
	.p2align 3
.L1797:
	addq	$72, -440(%rbp)
	movq	-440(%rbp), %rax
	cmpq	-480(%rbp), %rax
	jne	.L1803
	movq	-144(%rbp), %rbx
	movq	-152(%rbp), %r12
	cmpq	%rbx, %r12
	je	.L1805
	.p2align 4,,10
	.p2align 3
.L1806:
	movq	56(%r14), %rdi
	movq	%r12, %rsi
	addq	$16, %r12
	call	_ZN2v88internal8compiler23CompilationDependencies17DependOnStableMapERKNS1_6MapRefE@PLT
	cmpq	%r12, %rbx
	jne	.L1806
	jmp	.L1805
.L1868:
	movq	24(%r13), %rdi
	xorl	%r15d, %r15d
	jmp	.L1869
.L1997:
	movzbl	(%r12), %eax
	movb	%al, -160(%rbp)
	movq	8(%r12), %rdi
	movq	24(%r12), %rax
	subq	16(%r12), %rax
	movq	$0, -144(%rbp)
	movq	%rdi, -152(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -128(%rbp)
	je	.L1898
	leaq	7(%rax), %rsi
	movq	16(%rdi), %rcx
	movq	%rax, %rbx
	movq	24(%rdi), %rax
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L2008
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L1821:
	movq	%rcx, %xmm0
	leaq	(%rcx,%rbx), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -128(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	24(%r12), %rax
	movq	16(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L1824
	leaq	-8(%rax), %rsi
	leaq	15(%rdx), %rax
	subq	%rdx, %rsi
	subq	%rcx, %rax
	movq	%rsi, %r8
	shrq	$3, %r8
	cmpq	$30, %rax
	jbe	.L1899
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r8
	je	.L1899
	addq	$1, %r8
	xorl	%eax, %eax
	movq	%r8, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1826:
	movdqu	(%rdx,%rax), %xmm3
	movups	%xmm3, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L1826
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	addq	%rdi, %rdx
	addq	%rcx, %rdi
	cmpq	%r8, %rax
	je	.L1828
	movq	(%rdx), %rax
	movq	%rax, (%rdi)
.L1828:
	leaq	8(%rcx,%rsi), %rcx
.L1824:
	movq	%rcx, -136(%rbp)
	movq	40(%r12), %rdi
	movq	56(%r12), %rax
	subq	48(%r12), %rax
	movq	$0, -112(%rbp)
	movq	%rdi, -120(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	je	.L1900
	leaq	7(%rax), %rsi
	movq	16(%rdi), %rcx
	movq	%rax, %rbx
	movq	24(%rdi), %rax
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L2009
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L1829:
	movq	%rcx, %xmm0
	leaq	(%rcx,%rbx), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	56(%r12), %rax
	movq	48(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L1832
	leaq	-8(%rax), %rsi
	leaq	15(%rdx), %rax
	subq	%rdx, %rsi
	subq	%rcx, %rax
	movq	%rsi, %r8
	shrq	$3, %r8
	cmpq	$30, %rax
	jbe	.L1901
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r8
	je	.L1901
	addq	$1, %r8
	xorl	%eax, %eax
	movq	%r8, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1834:
	movdqu	(%rdx,%rax), %xmm4
	movups	%xmm4, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L1834
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	addq	%rdi, %rdx
	addq	%rcx, %rdi
	cmpq	%r8, %rax
	je	.L1836
	movq	(%rdx), %rax
	movq	%rax, (%rdi)
.L1836:
	leaq	8(%rcx,%rsi), %rcx
.L1832:
	movq	-144(%rbp), %rax
	movq	24(%r14), %rsi
	movq	%rcx, -104(%rbp)
	leaq	-320(%rbp), %r13
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %r9
	movq	%rax, -536(%rbp)
	cmpq	%r9, %rax
	je	.L1837
	leaq	-224(%rbp), %rax
	leaq	-80(%rbp), %r12
	movq	%r13, %r15
	movq	%r9, %rbx
	movq	%rax, -592(%rbp)
	movq	%rax, %r13
	movq	%r12, -520(%rbp)
	jmp	.L1841
	.p2align 4,,10
	.p2align 3
.L2011:
	cmpb	$4, %al
	je	.L1902
	cmpb	$2, %al
	je	.L1903
	cmpb	$6, %al
	movl	$7, %ecx
	cmovne	%eax, %ecx
.L1839:
	cmpb	%cl, %dl
	je	.L1905
	cmpb	$1, %al
	jbe	.L1892
	movl	$1, %eax
.L1840:
	movq	-480(%rbp), %xmm0
	subq	$8, %rsp
	movq	%r8, %rdi
	addq	$8, %rbx
	movq	-488(%rbp), %xmm1
	movb	%al, -192(%rbp)
	movhps	-448(%rbp), %xmm0
	movhps	-464(%rbp), %xmm1
	movaps	%xmm0, -464(%rbp)
	movq	-456(%rbp), %rdx
	movq	-464(%rbp), %rax
	movaps	%xmm1, -512(%rbp)
	pushq	%rdx
	pushq	%rax
	pushq	-192(%rbp)
	movups	%xmm0, -184(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22TransitionElementsKindENS1_18ElementsTransitionE@PLT
	addq	$32, %rsp
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movdqa	-512(%rbp), %xmm1
	movq	%rax, %rsi
	movl	$3, %edx
	movq	-440(%rbp), %rax
	movq	-520(%rbp), %rcx
	movq	%rax, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -408(%rbp)
	cmpq	%rbx, -536(%rbp)
	je	.L2010
.L1841:
	movq	(%rbx), %rdx
	movq	24(%r14), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	16(%r14), %rax
	movq	-400(%rbp), %rcx
	movq	%r15, %rdi
	movq	376(%rax), %r8
	movq	(%rax), %r12
	movq	%rcx, -440(%rbp)
	movq	-408(%rbp), %rcx
	movq	%r8, -496(%rbp)
	movq	%rcx, -464(%rbp)
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	%r13, %rdi
	movq	%rax, -448(%rbp)
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	%r15, %rdi
	movq	%rax, -480(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13elements_kindEv@PLT
	movq	%r13, %rdi
	movb	%al, -512(%rbp)
	call	_ZNK2v88internal8compiler6MapRef13elements_kindEv@PLT
	movzbl	-512(%rbp), %edx
	movq	-496(%rbp), %r8
	testb	%al, %al
	jne	.L2011
	cmpb	$1, %dl
	je	.L1840
.L1892:
	subl	$2, %edx
	cmpb	$1, %dl
	seta	%al
	jmp	.L1840
.L1905:
	xorl	%eax, %eax
	jmp	.L1840
.L1887:
	movq	16(%r14), %rax
	movl	%r13d, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	%r13d, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-208(%rbp), %rsi
	movq	%rax, -400(%rbp)
	cmpq	-200(%rbp), %rsi
	je	.L1888
	movq	%rax, (%rsi)
	addq	$8, -208(%rbp)
.L1889:
	movq	16(%r14), %rax
	movl	%r13d, %edx
	movl	$8, %esi
	leal	1(%r13), %r12d
	movq	-216(%rbp), %r15
	movq	8(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%rbx, %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	%r12d, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, %rbx
	cmpq	-168(%rbp), %rsi
	je	.L1890
	movq	-400(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -176(%rbp)
.L1891:
	movq	16(%r14), %rax
	movl	%r13d, %esi
	movq	-184(%rbp), %r15
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -432(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	%r12d, %edx
	movq	-432(%rbp), %r9
	movq	%rax, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-400(%rbp), %r8
	movq	%rax, -408(%rbp)
	movq	%rax, %rcx
	jmp	.L1843
.L2010:
	movq	-520(%rbp), %r12
	movq	%rax, %rbx
.L1842:
	movq	16(%r14), %rax
	movq	-400(%rbp), %r13
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10CheckpointEv@PLT
	movq	%rbx, %xmm5
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r15, %rdi
	movq	%r13, -64(%rbp)
	movq	-432(%rbp), %xmm0
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-400(%rbp), %rcx
	movq	-488(%rbp), %rsi
	leaq	-408(%rbp), %rdx
	leaq	-152(%rbp), %r8
	leaq	-352(%rbp), %rdi
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal8compiler21PropertyAccessBuilder14BuildCheckMapsEPNS1_4NodeEPS4_S4_RKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE@PLT
	movq	-528(%rbp), %rdi
	call	_ZNK2v88internal8compiler21ElementAccessFeedback10keyed_modeEv@PLT
	subq	$8, %rsp
	movq	%r14, %rsi
	pushq	-592(%rbp)
	pushq	-552(%rbp)
	leaq	-192(%rbp), %rdi
	movq	-568(%rbp), %r8
	pushq	-400(%rbp)
	movq	-560(%rbp), %rcx
	movq	%rax, -224(%rbp)
	movq	-408(%rbp), %r9
	movq	-488(%rbp), %rdx
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization18BuildElementAccessEPNS1_4NodeES4_S4_S4_S4_RKNS1_17ElementAccessInfoERKNS1_15KeyedAccessModeE
	movq	-184(%rbp), %rcx
	movq	-176(%rbp), %r8
	addq	$32, %rsp
	movq	-192(%rbp), %rbx
	movq	%rcx, -408(%rbp)
	movq	%r8, -400(%rbp)
	jmp	.L1843
.L1902:
	movl	$5, %ecx
	jmp	.L1839
.L1903:
	movl	$3, %ecx
	jmp	.L1839
.L1900:
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	jmp	.L1829
.L1898:
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	jmp	.L1821
.L2006:
	movq	8(%r13), %rdi
	movq	0(%r13), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r13)
	movq	%rax, %rbx
	jmp	.L1885
.L1899:
	xorl	%eax, %eax
.L1825:
	movq	(%rdx,%rax,8), %rdi
	movq	%rdi, (%rcx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%r8, %rdi
	jne	.L1825
	jmp	.L1828
.L1901:
	xorl	%eax, %eax
.L1833:
	movq	(%rdx,%rax,8), %rdi
	movq	%rdi, (%rcx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%r8, %rdi
	jne	.L1833
	jmp	.L1836
.L1890:
	movq	-600(%rbp), %rdi
	leaq	-400(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1891
.L1888:
	movq	-592(%rbp), %rdi
	leaq	-400(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1889
.L1837:
	leaq	-224(%rbp), %rax
	movq	-408(%rbp), %rbx
	leaq	-80(%rbp), %r12
	movq	%rax, -592(%rbp)
	jmp	.L1842
.L2008:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1821
.L2009:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1829
.L1817:
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	movq	%r13, %rdi
	call	*48(%rax)
	jmp	.L1818
.L1996:
	call	__stack_chk_fail@PLT
.L1995:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE23216:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceElementAccessEPNS1_4NodeES4_S4_RKNS1_21ElementAccessFeedbackE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceElementAccessEPNS1_4NodeES4_S4_RKNS1_21ElementAccessFeedbackE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceNamedAccessEPNS1_4NodeES4_RKNS1_19NamedAccessFeedbackENS1_10AccessModeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceNamedAccessEPNS1_4NodeES4_RKNS1_19NamedAccessFeedbackENS1_10AccessModeES4_
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceNamedAccessEPNS1_4NodeES4_RKNS1_19NamedAccessFeedbackENS1_10AccessModeES4_, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceNamedAccessEPNS1_4NodeES4_RKNS1_19NamedAccessFeedbackENS1_10AccessModeES4_:
.LFB23192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rbx, %rdi
	subq	$696, %rsp
	movq	%rdx, -656(%rbp)
	movl	%r8d, -620(%rbp)
	movq	%r9, -552(%rbp)
	movq	%rsi, -728(%rbp)
	xorl	%esi, %esi
	movq	%rcx, -560(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	%rax, -544(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	%rbx, %rdi
	movq	%rax, -664(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movq	%rbx, %rdi
	xorl	%esi, %esi
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	xorl	%esi, %esi
	movq	%rax, -536(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-536(%rbp), %r8
	movq	%rax, -528(%rbp)
	leaq	-432(%rbp), %r9
	leaq	-368(%rbp), %r15
	movq	64(%r14), %rax
	movq	-544(%rbp), %rcx
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	%rax, -400(%rbp)
	movq	$0, -408(%rbp)
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	movq	$0, -376(%rbp)
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization35FilterMapsAndGetPropertyAccessInfosERKNS1_19NamedAccessFeedbackENS1_10AccessModeEPNS1_4NodeES8_PNS0_10ZoneVectorINS1_18PropertyAccessInfoEEE
	movq	16(%r14), %rax
	movq	56(%r14), %rdx
	movq	%r15, %rdi
	movq	24(%r14), %rsi
	movq	(%rax), %rax
	movq	(%rax), %rcx
	call	_ZN2v88internal8compiler17AccessInfoFactoryC1EPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneE@PLT
	movq	-416(%rbp), %r13
	movq	-432(%rbp), %rdi
	movq	$0, -232(%rbp)
	movq	-424(%rbp), %r8
	movq	$0, -224(%rbp)
	movq	%r13, %rbx
	movq	%rdi, -240(%rbp)
	movq	$0, -216(%rbp)
	subq	%r8, %rbx
	je	.L2161
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	leaq	7(%rbx), %rsi
	andq	$-8, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L2257
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L2015:
	movq	-416(%rbp), %r13
.L2013:
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -216(%rbp)
	movups	%xmm0, -232(%rbp)
	cmpq	%r8, %r13
	je	.L2016
	movq	%r15, %rax
	movq	%r8, %r12
	movq	%rcx, %rbx
	movq	%rcx, %r15
	movabsq	$2305843009213693948, %r11
	movq	%r14, %rcx
	movq	%r8, %r14
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L2035:
	movl	(%r12), %eax
	movl	%eax, (%rbx)
	movq	8(%r12), %rdi
	movq	24(%r12), %rdx
	subq	16(%r12), %rdx
	movq	$0, 16(%rbx)
	movq	%rdi, 8(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	je	.L2162
	movq	16(%rdi), %rax
	movq	24(%rdi), %r9
	leaq	7(%rdx), %rsi
	andq	$-8, %rsi
	subq	%rax, %r9
	cmpq	%r9, %rsi
	ja	.L2258
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2017:
	movq	%rax, %xmm0
	addq	%rax, %rdx
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movq	24(%r12), %rdx
	movq	16(%r12), %rsi
	cmpq	%rsi, %rdx
	je	.L2020
	subq	$8, %rdx
	movq	%rdx, %rdi
	leaq	15(%rsi), %rdx
	subq	%rsi, %rdi
	subq	%rax, %rdx
	movq	%rdi, %r9
	shrq	$3, %r9
	cmpq	$30, %rdx
	jbe	.L2163
	testq	%r11, %r9
	je	.L2163
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %r10
	shrq	%r10
	salq	$4, %r10
	.p2align 4,,10
	.p2align 3
.L2022:
	movdqu	(%rsi,%rdx), %xmm2
	movups	%xmm2, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %r10
	jne	.L2022
	movq	%r9, %r10
	andq	$-2, %r10
	leaq	0(,%r10,8), %rdx
	addq	%rdx, %rsi
	addq	%rax, %rdx
	cmpq	%r9, %r10
	je	.L2025
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
.L2025:
	leaq	8(%rax,%rdi), %rax
.L2020:
	movq	%rax, 24(%rbx)
	movq	40(%r12), %rdi
	movq	56(%r12), %rdx
	subq	48(%r12), %rdx
	movq	$0, 48(%rbx)
	movq	%rdi, 40(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	je	.L2164
	movq	16(%rdi), %rax
	movq	24(%rdi), %r9
	leaq	7(%rdx), %rsi
	andq	$-8, %rsi
	subq	%rax, %r9
	cmpq	%r9, %rsi
	ja	.L2259
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2026:
	movq	%rax, %xmm0
	addq	%rax, %rdx
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movq	56(%r12), %rdx
	movq	48(%r12), %rsi
	cmpq	%rsi, %rdx
	je	.L2029
	subq	$8, %rdx
	movq	%rdx, %rdi
	leaq	15(%rsi), %rdx
	subq	%rsi, %rdi
	subq	%rax, %rdx
	movq	%rdi, %r9
	shrq	$3, %r9
	cmpq	$30, %rdx
	jbe	.L2165
	testq	%r11, %r9
	je	.L2165
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %r10
	shrq	%r10
	salq	$4, %r10
	.p2align 4,,10
	.p2align 3
.L2031:
	movdqu	(%rsi,%rdx), %xmm3
	movups	%xmm3, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %r10
	jne	.L2031
	movq	%r9, %r10
	andq	$-2, %r10
	leaq	0(,%r10,8), %rdx
	addq	%rdx, %rsi
	addq	%rax, %rdx
	cmpq	%r9, %r10
	je	.L2034
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
.L2034:
	leaq	8(%rax,%rdi), %rax
.L2029:
	movq	%rax, 56(%rbx)
	movq	72(%r12), %rax
	addq	$136, %r12
	addq	$136, %rbx
	movq	%rax, -64(%rbx)
	movq	-56(%r12), %rax
	movq	%rax, -56(%rbx)
	movq	-48(%r12), %rax
	movq	%rax, -48(%rbx)
	movq	-40(%r12), %rax
	movq	%rax, -40(%rbx)
	movzbl	-32(%r12), %eax
	movb	%al, -32(%rbx)
	movq	-24(%r12), %rax
	movq	%rax, -24(%rbx)
	movq	-16(%r12), %rax
	movq	%rax, -16(%rbx)
	movq	-8(%r12), %rax
	movq	%rax, -8(%rbx)
	cmpq	%r12, %r13
	jne	.L2035
	movabsq	$2305843009213693951, %rdx
	movq	%r8, %rax
	movq	%r14, %r8
	movq	%rcx, %r14
	movq	%r15, %rcx
	movq	%rax, %r15
	leaq	-136(%r13), %rax
	movabsq	$1220740416642543857, %r13
	subq	%r8, %rax
	shrq	$3, %rax
	imulq	%r13, %rax
	andq	%rdx, %rax
	addq	$1, %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rcx
.L2016:
	leaq	-240(%rbp), %rax
	movl	-620(%rbp), %edx
	movq	%rcx, -224(%rbp)
	movq	%r15, %rdi
	leaq	-400(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rax, -648(%rbp)
	call	_ZNK2v88internal8compiler17AccessInfoFactory27FinalizePropertyAccessInfosENS0_10ZoneVectorINS1_18PropertyAccessInfoEEENS1_10AccessModeEPS5_@PLT
	movb	%al, -621(%rbp)
	testb	%al, %al
	jne	.L2036
	xorl	%eax, %eax
.L2037:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2260
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2162:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L2017
	.p2align 4,,10
	.p2align 3
.L2164:
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2165:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2030:
	movq	(%rsi,%rdx,8), %r10
	movq	%r10, (%rax,%rdx,8)
	movq	%rdx, %r10
	addq	$1, %rdx
	cmpq	%r9, %r10
	jne	.L2030
	jmp	.L2034
	.p2align 4,,10
	.p2align 3
.L2163:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2021:
	movq	(%rsi,%rdx,8), %r10
	movq	%r10, (%rax,%rdx,8)
	movq	%rdx, %r10
	addq	$1, %rdx
	cmpq	%r10, %r9
	jne	.L2021
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L2036:
	movq	-392(%rbp), %rdx
	movq	-384(%rbp), %rax
	subq	%rdx, %rax
	cmpq	$136, %rax
	je	.L2261
.L2038:
	cmpq	$0, -552(%rbp)
	je	.L2042
	movq	-560(%rbp), %rax
	movq	-528(%rbp), %r12
	movq	-536(%rbp), %rbx
	leaq	8(%rax), %r15
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8IsSymbolEv@PLT
	testb	%al, %al
	movq	16(%r14), %rax
	movq	376(%rax), %rdi
	jne	.L2262
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder29CheckEqualsInternalizedStringEv@PLT
	movq	%rax, %r13
.L2044:
	movq	%r12, %xmm1
	movq	16(%r14), %rdi
	movq	%rbx, %xmm0
	movq	%r15, %rsi
	punpcklqdq	%xmm1, %xmm0
	leaq	-96(%rbp), %r12
	movq	(%rdi), %rbx
	movaps	%xmm0, -576(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$4, %edx
	movdqa	-576(%rbp), %xmm0
	movq	%rax, %xmm1
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movhps	-552(%rbp), %xmm1
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -536(%rbp)
.L2042:
	movq	64(%r14), %rax
	movq	-728(%rbp), %rdi
	leaq	-520(%rbp), %rsi
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -336(%rbp)
	movq	$0, -312(%rbp)
	movq	$0, -520(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties17IsExceptionalCallEPNS1_4NodeEPS4_@PLT
	leaq	-336(%rbp), %rdx
	movq	-392(%rbp), %rcx
	movdqu	16(%r14), %xmm7
	testb	%al, %al
	movl	$0, %eax
	movq	24(%r14), %r12
	cmovne	%rdx, %rax
	movaps	%xmm7, -496(%rbp)
	movq	%rcx, %r15
	movq	%rax, -632(%rbp)
	movq	56(%r14), %rax
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rax, -576(%rbp)
	subq	%rcx, %rax
	cmpq	$136, %rax
	je	.L2263
	movq	64(%r14), %rdx
	movq	-576(%rbp), %rcx
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rdx, -304(%rbp)
	movq	$0, -280(%rbp)
	movq	%rdx, -272(%rbp)
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	%rdx, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	cmpq	%rcx, %r15
	je	.L2073
	movq	%r14, -552(%rbp)
	leaq	-464(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L2081:
	movq	24(%r15), %rbx
	movq	16(%r15), %r14
	cmpq	%rbx, %r14
	jne	.L2079
	jmp	.L2080
	.p2align 4,,10
	.p2align 3
.L2077:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2080
.L2079:
	movq	(%r14), %rdx
	movq	%r13, %rdi
	movl	$1, %ecx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef15IsHeapNumberMapEv@PLT
	testb	%al, %al
	je	.L2077
	movq	-552(%rbp), %r14
	leaq	-96(%rbp), %r12
	movq	-544(%rbp), %rbx
	movq	16(%r14), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11ObjectIsSmiEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-528(%rbp), %rbx
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%rax, -552(%rbp)
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%rbx, %xmm6
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	-552(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r13, %rdi
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rbx
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%rbx, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -528(%rbp)
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	%r12, %rcx
	movl	$1, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%rbx, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-536(%rbp), %rdx
	movq	%rax, -640(%rbp)
	movq	-392(%rbp), %rax
	movq	%rax, %rcx
	movq	%rax, -576(%rbp)
	movq	-384(%rbp), %rax
	subq	%rcx, %rax
.L2078:
	movq	-528(%rbp), %rcx
	sarq	$3, %rax
	movq	%rcx, -584(%rbp)
	movabsq	$-1085102592571150095, %rcx
	imulq	%rcx, %rax
	testq	%rax, %rax
	je	.L2082
	movq	-560(%rbp), %rbx
	leaq	-272(%rbp), %rdi
	movq	%rdx, -712(%rbp)
	leaq	-464(%rbp), %r13
	movq	%rdi, -680(%rbp)
	leaq	-504(%rbp), %rdi
	movq	-576(%rbp), %rsi
	addq	$8, %rbx
	movq	%rdi, -592(%rbp)
	movq	%rbx, -688(%rbp)
	leaq	-304(%rbp), %rbx
	movq	%rcx, -704(%rbp)
	movq	$0, -552(%rbp)
	movq	%rbx, -696(%rbp)
	movq	-552(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L2148:
	movq	%rbx, %rcx
	subq	$1, %rax
	movq	%rdx, -512(%rbp)
	salq	$4, %rcx
	addq	%rbx, %rcx
	leaq	(%rsi,%rcx,8), %r12
	movq	-544(%rbp), %rsi
	leaq	8(%r12), %r8
	movq	%r12, -560(%rbp)
	movq	%rsi, -600(%rbp)
	movq	%r8, -616(%rbp)
	cmpq	%rbx, %rax
	je	.L2264
	movq	16(%r12), %r8
	movq	24(%r12), %r12
	movq	16(%r14), %r9
	cmpq	%r8, %r12
	je	.L2173
	movl	$1, %r15d
	movq	%r13, -608(%rbp)
	movq	%r8, %r13
	movq	%r14, -576(%rbp)
	movq	%r15, %rbx
	jmp	.L2113
	.p2align 4,,10
	.p2align 3
.L2267:
	testq	%rax, %rax
	jne	.L2088
	cmpq	%rdx, %rbx
	je	.L2112
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$31, %rax
	jbe	.L2265
	leaq	32(%r15), %rax
	movq	%rax, 16(%rdi)
.L2091:
	movq	%rdi, (%r15)
	movq	$0, 8(%r15)
	movq	$0, 16(%r15)
	movq	$0, 24(%r15)
	cmpq	-504(%rbp), %rbx
	jnb	.L2092
	movq	-608(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rbx, -464(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	movq	16(%r15), %rsi
	cmpq	24(%r15), %rsi
	je	.L2093
	movq	-504(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r15)
.L2094:
	movq	-576(%rbp), %rax
	movq	%r15, %rbx
	orq	$2, %rbx
	movq	16(%rax), %r9
.L2112:
	addq	$8, %r13
	cmpq	%r13, %r12
	je	.L2266
.L2113:
	movq	(%r9), %rax
	movq	0(%r13), %rdx
	movq	(%rax), %rdi
	movq	%rbx, %rax
	movq	%rdx, -504(%rbp)
	andl	$3, %eax
	cmpq	$1, %rax
	jne	.L2267
	addq	$8, %r13
	movq	%rdx, %rbx
	cmpq	%r13, %r12
	jne	.L2113
.L2266:
	movq	-576(%rbp), %r14
	movq	-608(%rbp), %r13
	movq	%rbx, %r15
	movq	-544(%rbp), %xmm0
	movq	-512(%rbp), %rdx
.L2086:
	movq	376(%r9), %rdi
	movq	%r15, %rsi
	movq	(%r9), %rbx
	movq	%rdx, -608(%rbp)
	movq	%xmm0, -576(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CompareMapsENS0_13ZoneHandleSetINS0_3MapEEE@PLT
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	-608(%rbp), %rdx
	movq	%rax, %rsi
	movq	-584(%rbp), %rax
	movq	%rcx, -608(%rbp)
	movq	-576(%rbp), %xmm0
	movq	%rdx, %xmm5
	movl	$3, %edx
	movq	%rax, -80(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -512(%rbp)
	movq	%rax, -576(%rbp)
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r15, %rdi
	movq	-576(%rbp), %xmm0
	movq	-608(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-584(%rbp), %xmm0
	movq	%rcx, -576(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rbx
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r15, %rdi
	movq	-576(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rbx, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -584(%rbp)
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	$1, %edx
	movq	-576(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rbx, -96(%rbp)
	movq	%rcx, -608(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %r15
	movq	-608(%rbp), %rcx
	movq	%rax, -576(%rbp)
	movq	-560(%rbp), %rax
	movq	16(%rax), %r8
	movq	24(%rax), %rax
	cmpq	%rax, %r8
	movq	%rax, %rdi
	je	.L2114
	movzbl	-621(%rbp), %r12d
.L2084:
	movb	%r12b, -608(%rbp)
	movq	%rdi, %rbx
	movq	%r8, %r12
	jmp	.L2116
	.p2align 4,,10
	.p2align 3
.L2115:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L2268
.L2116:
	movq	(%r12), %rdx
	movq	%r13, %rdi
	movl	$1, %ecx
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef15IsHeapNumberMapEv@PLT
	testb	%al, %al
	je	.L2115
	movq	16(%r14), %rax
	movl	$2, %esi
	movq	-576(%rbp), %xmm0
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	movhps	-640(%rbp), %xmm0
	movaps	%xmm0, -576(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movdqa	-576(%rbp), %xmm0
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%rcx, -608(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-512(%rbp), %rbx
	movl	$2, %esi
	movq	%rax, -576(%rbp)
	movq	%rax, %r12
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	%rbx, %xmm0
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	-608(%rbp), %rcx
	movhps	-712(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, -80(%rbp)
	movl	$3, %edx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r14), %r15
	movq	$0, -712(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -640(%rbp)
.L2085:
	movq	-616(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17HasOnlyStringMapsEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE@PLT
	testb	%al, %al
	jne	.L2146
	movq	-512(%rbp), %rax
.L2147:
	movl	-620(%rbp), %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	-672(%rbp), %r9
	movq	-664(%rbp), %r8
	movq	-600(%rbp), %rdx
	pushq	%rcx
	movq	-656(%rbp), %rcx
	pushq	-560(%rbp)
	pushq	-632(%rbp)
	pushq	-688(%rbp)
	pushq	-576(%rbp)
	pushq	%rax
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization19BuildPropertyAccessEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE
	movq	-592(%rbp), %rbx
	movq	-464(%rbp), %rax
	addq	$48, %rsp
	movq	-696(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rax, -504(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movq	-456(%rbp), %rax
	movq	-680(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rax, -504(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movq	-448(%rbp), %rax
	movq	-648(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rax, -504(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movq	-392(%rbp), %rsi
	movq	-384(%rbp), %rax
	addq	$1, -552(%rbp)
	movq	-552(%rbp), %rcx
	subq	%rsi, %rax
	sarq	$3, %rax
	imulq	-704(%rbp), %rax
	cmpq	%rax, %rcx
	jnb	.L2082
	movq	-536(%rbp), %rdx
	movq	%rcx, %rbx
	jmp	.L2148
	.p2align 4,,10
	.p2align 3
.L2088:
	movq	6(%rbx), %r11
	movq	14(%rbx), %rsi
	leaq	-2(%rbx), %r14
	subq	%r11, %rsi
	sarq	$3, %rsi
	je	.L2096
	xorl	%eax, %eax
	jmp	.L2097
	.p2align 4,,10
	.p2align 3
.L2269:
	addq	$1, %rax
	cmpq	%rsi, %rax
	je	.L2096
.L2097:
	cmpq	%rdx, (%r11,%rax,8)
	je	.L2112
	jbe	.L2269
.L2096:
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L2270
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rdi)
.L2099:
	movq	%rdi, (%rbx)
	movq	%rbx, %rdi
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	16(%r14), %rsi
	subq	8(%r14), %rsi
	sarq	$3, %rsi
	addq	$1, %rsi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE7reserveEm
	movq	8(%r14), %rdi
	cmpq	16(%r14), %rdi
	movq	16(%rbx), %rsi
	je	.L2100
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L2105:
	leaq	(%rdi,%r15,8), %rdx
	movq	24(%rbx), %rdi
	movq	(%rdx), %rax
	cmpq	-504(%rbp), %rax
	ja	.L2101
	cmpq	%rsi, %rdi
	je	.L2102
	movq	%rax, (%rsi)
	movq	16(%rbx), %rax
	addq	$1, %r15
	leaq	8(%rax), %rsi
	movq	%rsi, 16(%rbx)
	movq	8(%r14), %rdi
	movq	16(%r14), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rax, %r15
	jb	.L2105
.L2104:
	movq	24(%rbx), %rdi
.L2101:
	cmpq	%rdi, %rsi
	je	.L2106
	movq	-504(%rbp), %rdx
	movq	%rdx, (%rsi)
	addq	$8, 16(%rbx)
.L2107:
	movq	8(%r14), %rsi
	movq	16(%r14), %rdx
	subq	%rsi, %rdx
	sarq	$3, %rdx
	cmpq	%r15, %rdx
	jbe	.L2111
	.p2align 4,,10
	.p2align 3
.L2108:
	leaq	(%rsi,%r15,8), %rdx
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L2109
	movq	(%rdx), %rax
	addq	$1, %r15
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
	movq	8(%r14), %rsi
	movq	16(%r14), %rax
	subq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rax, %r15
	jb	.L2108
.L2111:
	movq	-576(%rbp), %rax
	orq	$2, %rbx
	movq	16(%rax), %r9
	jmp	.L2112
	.p2align 4,,10
	.p2align 3
.L2102:
	movq	%rbx, %rdi
	addq	$1, %r15
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	8(%r14), %rdi
	movq	16(%r14), %rax
	movq	16(%rbx), %rsi
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%r15, %rax
	ja	.L2105
	jmp	.L2104
	.p2align 4,,10
	.p2align 3
.L2109:
	movq	%rbx, %rdi
	addq	$1, %r15
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	8(%r14), %rsi
	movq	16(%r14), %rax
	subq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%r15, %rax
	ja	.L2108
	movq	-576(%rbp), %rax
	orq	$2, %rbx
	movq	16(%rax), %r9
	jmp	.L2112
	.p2align 4,,10
	.p2align 3
.L2080:
	addq	$136, %r15
	cmpq	-576(%rbp), %r15
	je	.L2271
	movq	-552(%rbp), %rax
	movq	24(%rax), %r12
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L2092:
	movq	-592(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-608(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rbx, -464(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	jmp	.L2094
	.p2align 4,,10
	.p2align 3
.L2268:
	movzbl	-608(%rbp), %r12d
	testb	%r12b, %r12b
	je	.L2256
	movq	-560(%rbp), %rax
	movq	16(%r14), %rsi
	movq	16(%rax), %r8
	movq	24(%rax), %r12
	cmpq	%r8, %r12
	je	.L2174
	movq	%r14, -608(%rbp)
	movq	%r8, %rbx
	movl	$1, %r15d
	movq	%r13, -720(%rbp)
	jmp	.L2145
	.p2align 4,,10
	.p2align 3
.L2274:
	testq	%rax, %rax
	jne	.L2120
	cmpq	%r15, %rdx
	je	.L2144
	movq	16(%r13), %r14
	movq	24(%r13), %rdx
	subq	%r14, %rdx
	cmpq	$31, %rdx
	jbe	.L2272
	leaq	32(%r14), %rdx
	movq	%rdx, 16(%r13)
.L2123:
	movq	%r13, (%r14)
	movq	$0, 8(%r14)
	movq	$0, 16(%r14)
	movq	$0, 24(%r14)
	cmpq	-504(%rbp), %r15
	jnb	.L2124
	movq	-720(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r15, -464(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	movq	16(%r14), %rsi
	cmpq	24(%r14), %rsi
	je	.L2125
	movq	-504(%rbp), %rdx
	movq	%rdx, (%rsi)
	addq	$8, 16(%r14)
.L2126:
	movq	-608(%rbp), %rax
	orq	$2, %r14
	movq	%r14, %r15
	movq	16(%rax), %rsi
.L2144:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	je	.L2273
.L2145:
	movq	(%rsi), %rax
	movq	(%rbx), %rdx
	movq	(%rax), %r13
	movq	%r15, %rax
	movq	%rdx, -504(%rbp)
	andl	$3, %eax
	cmpq	$1, %rax
	jne	.L2274
	addq	$8, %rbx
	movq	%rdx, %r15
	cmpq	%rbx, %r12
	jne	.L2145
.L2273:
	movq	-608(%rbp), %r14
	movq	-720(%rbp), %r13
	leaq	-96(%rbp), %rcx
.L2118:
	movq	-544(%rbp), %xmm0
	movq	376(%rsi), %rdi
	movq	%rcx, -720(%rbp)
	movq	(%rsi), %rbx
	movq	-512(%rbp), %r12
	movq	%r15, %rsi
	movq	%xmm0, -608(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8MapGuardENS0_13ZoneHandleSetINS0_3MapEEE@PLT
	movq	%r12, %xmm1
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	-720(%rbp), %rcx
	movl	$3, %edx
	movq	-608(%rbp), %xmm0
	movq	-576(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -512(%rbp)
.L2256:
	movq	24(%r14), %r15
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2120:
	movq	6(%r15), %r10
	movq	14(%r15), %rdi
	leaq	-2(%r15), %r14
	subq	%r10, %rdi
	sarq	$3, %rdi
	je	.L2128
	xorl	%eax, %eax
	jmp	.L2129
	.p2align 4,,10
	.p2align 3
.L2275:
	addq	$1, %rax
	cmpq	%rax, %rdi
	je	.L2128
.L2129:
	cmpq	(%r10,%rax,8), %rdx
	je	.L2144
	jnb	.L2275
.L2128:
	movq	16(%r13), %r15
	movq	24(%r13), %rax
	subq	%r15, %rax
	cmpq	$31, %rax
	jbe	.L2276
	leaq	32(%r15), %rax
	movq	%rax, 16(%r13)
.L2131:
	movq	%r13, (%r15)
	movq	%r15, %rdi
	movq	$0, 8(%r15)
	movq	$0, 16(%r15)
	movq	$0, 24(%r15)
	movq	16(%r14), %rsi
	subq	8(%r14), %rsi
	sarq	$3, %rsi
	addq	$1, %rsi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE7reserveEm
	movq	8(%r14), %rdi
	cmpq	%rdi, 16(%r14)
	movq	16(%r15), %rsi
	je	.L2132
	xorl	%r13d, %r13d
	jmp	.L2137
	.p2align 4,,10
	.p2align 3
.L2278:
	movq	%rax, (%rsi)
	movq	16(%r15), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, 16(%r15)
.L2253:
	movq	8(%r14), %rdi
	movq	16(%r14), %rax
	addq	$1, %r13
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%r13, %rax
	jbe	.L2277
.L2137:
	leaq	(%rdi,%r13,8), %rdx
	movq	24(%r15), %rdi
	movq	(%rdx), %rax
	cmpq	-504(%rbp), %rax
	ja	.L2133
	cmpq	%rsi, %rdi
	jne	.L2278
	movq	%r15, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	16(%r15), %rsi
	jmp	.L2253
	.p2align 4,,10
	.p2align 3
.L2277:
	movq	24(%r15), %rdi
.L2133:
	cmpq	%rdi, %rsi
	je	.L2138
	movq	-504(%rbp), %rdx
	movq	%rdx, (%rsi)
	addq	$8, 16(%r15)
.L2139:
	movq	8(%r14), %rsi
	movq	16(%r14), %rdx
	subq	%rsi, %rdx
	sarq	$3, %rdx
	cmpq	%r13, %rdx
	ja	.L2140
	jmp	.L2143
	.p2align 4,,10
	.p2align 3
.L2279:
	movq	(%rdx), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r15)
.L2255:
	movq	8(%r14), %rsi
	movq	16(%r14), %rax
	addq	$1, %r13
	subq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%r13, %rax
	jbe	.L2143
.L2140:
	leaq	(%rsi,%r13,8), %rdx
	movq	16(%r15), %rsi
	cmpq	24(%r15), %rsi
	jne	.L2279
	movq	%r15, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	jmp	.L2255
	.p2align 4,,10
	.p2align 3
.L2143:
	movq	-608(%rbp), %rax
	orq	$2, %r15
	movq	16(%rax), %rsi
	jmp	.L2144
	.p2align 4,,10
	.p2align 3
.L2264:
	movq	-584(%rbp), %rcx
	leaq	-496(%rbp), %rdi
	leaq	-512(%rbp), %rdx
	call	_ZN2v88internal8compiler21PropertyAccessBuilder14BuildCheckMapsEPNS1_4NodeEPS4_S4_RKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE@PLT
	movq	24(%r12), %rax
	movq	16(%r12), %r8
	movq	24(%r14), %r15
	movq	%rax, %rdi
	cmpq	%r8, %rax
	movq	-584(%rbp), %rax
	movq	%rax, -576(%rbp)
	je	.L2280
	movq	$0, -584(%rbp)
	xorl	%r12d, %r12d
	jmp	.L2084
	.p2align 4,,10
	.p2align 3
.L2146:
	movq	16(%r14), %rax
	movq	-512(%rbp), %r12
	movl	$16417, %esi
	movq	-544(%rbp), %rbx
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TypeGuardENS1_4TypeE@PLT
	movq	%r12, %xmm7
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rbx, %xmm0
	leaq	-96(%rbp), %rcx
	movq	-576(%rbp), %rax
	punpcklqdq	%xmm7, %xmm0
	movl	$3, %edx
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -600(%rbp)
	movq	%rax, -512(%rbp)
	jmp	.L2147
	.p2align 4,,10
	.p2align 3
.L2124:
	movq	-592(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-720(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r15, -464(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	jmp	.L2126
	.p2align 4,,10
	.p2align 3
.L2106:
	movq	-592(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	jmp	.L2107
	.p2align 4,,10
	.p2align 3
.L2161:
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	jmp	.L2013
	.p2align 4,,10
	.p2align 3
.L2259:
	movq	%r8, -592(%rbp)
	movq	%rcx, -584(%rbp)
	movq	%rdx, -576(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-576(%rbp), %rdx
	movq	-584(%rbp), %rcx
	movabsq	$2305843009213693948, %r11
	movq	-592(%rbp), %r8
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2258:
	movq	%r8, -592(%rbp)
	movq	%rcx, -584(%rbp)
	movq	%rdx, -576(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-576(%rbp), %rdx
	movq	-584(%rbp), %rcx
	movabsq	$2305843009213693948, %r11
	movq	-592(%rbp), %r8
	jmp	.L2017
	.p2align 4,,10
	.p2align 3
.L2265:
	movl	$32, %esi
	movq	%rdi, -720(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-720(%rbp), %rdi
	movq	%rax, %r15
	jmp	.L2091
	.p2align 4,,10
	.p2align 3
.L2082:
	movq	-232(%rbp), %r12
	movq	-224(%rbp), %rbx
	subq	%r12, %rbx
	sarq	$3, %rbx
	testl	%ebx, %ebx
	je	.L2281
	cmpl	$1, %ebx
	jne	.L2152
	movq	-296(%rbp), %rax
	movq	(%rax), %r13
	movq	-264(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -536(%rbp)
	movq	(%r12), %rax
	movq	%rax, -528(%rbp)
.L2072:
	movq	-328(%rbp), %rax
	cmpq	%rax, -320(%rbp)
	je	.L2157
	movq	-632(%rbp), %rcx
	movq	16(%rcx), %rax
	movq	8(%rcx), %r15
	movq	%rax, -552(%rbp)
	movq	%rax, %rbx
	movq	16(%r14), %rax
	subq	%r15, %rbx
	movq	8(%rax), %rdi
	sarq	$3, %rbx
	movq	(%rax), %r12
	movl	%ebx, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	%r15, %rcx
	xorl	%r8d, %r8d
	movl	%ebx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-632(%rbp), %rcx
	movq	%rax, -240(%rbp)
	movq	16(%rcx), %rsi
	cmpq	24(%rcx), %rsi
	je	.L2158
	movq	%rax, (%rsi)
	addq	$8, 16(%rcx)
.L2159:
	movq	16(%r14), %rax
	leal	1(%rbx), %r9d
	movl	%ebx, %esi
	movq	8(%rcx), %r15
	movl	%r9d, -552(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	movl	-552(%rbp), %r9d
	movq	%rax, %rsi
	movl	%r9d, %edx
	movl	%r9d, -576(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-632(%rbp), %rcx
	movl	%ebx, %edx
	movl	$8, %esi
	movq	%rax, %r12
	movq	16(%r14), %rax
	movq	8(%rcx), %rcx
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	movq	%rcx, -552(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movl	-576(%rbp), %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	-552(%rbp), %rcx
	movq	%rax, %rsi
	movl	%r9d, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r14), %rdi
	movq	-240(%rbp), %r8
	movq	%r12, %rcx
	movq	%rax, %rdx
	movq	-520(%rbp), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
.L2157:
	movq	8(%r14), %rdi
	movq	-528(%rbp), %r8
	movq	%r13, %rdx
	movq	-536(%rbp), %rcx
	movq	-728(%rbp), %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L2037
	.p2align 4,,10
	.p2align 3
.L2093:
	movq	-592(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	jmp	.L2094
	.p2align 4,,10
	.p2align 3
.L2270:
	movl	$32, %esi
	movq	%rdi, -720(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-720(%rbp), %rdi
	movq	%rax, %rbx
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L2138:
	movq	-592(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	jmp	.L2139
	.p2align 4,,10
	.p2align 3
.L2173:
	movq	%rsi, %xmm0
	movl	$1, %r15d
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2271:
	movq	-392(%rbp), %rax
	movq	-552(%rbp), %r14
	movq	%rax, %rcx
	movq	%rax, -576(%rbp)
	movq	-384(%rbp), %rax
	subq	%rcx, %rax
.L2073:
	movq	$0, -640(%rbp)
	movq	-536(%rbp), %rdx
	jmp	.L2078
	.p2align 4,,10
	.p2align 3
.L2281:
	movq	16(%r14), %rbx
	movq	352(%rbx), %r13
	testq	%r13, %r13
	je	.L2282
.L2150:
	movq	%r13, -528(%rbp)
	movq	%r13, -536(%rbp)
	jmp	.L2072
.L2262:
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder17CheckEqualsSymbolEv@PLT
	movq	%rax, %r13
	jmp	.L2044
.L2257:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-424(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L2015
	.p2align 4,,10
	.p2align 3
.L2272:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L2123
	.p2align 4,,10
	.p2align 3
.L2100:
	movq	24(%rbx), %rdi
	xorl	%r15d, %r15d
	jmp	.L2101
.L2261:
	movq	16(%rdx), %rcx
	movq	24(%rdx), %rax
	subq	%rcx, %rax
	cmpq	$8, %rax
	jne	.L2038
	movq	(%rcx), %rdx
	leaq	-336(%rbp), %r12
	movq	24(%r14), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler6MapRef24IsMapOfTargetGlobalProxyEv@PLT
	testb	%al, %al
	je	.L2038
	movq	-560(%rbp), %rbx
	movq	24(%r14), %rax
	movq	-544(%rbp), %r12
	addq	$8, %rbx
	cmpb	$0, 24(%rax)
	je	.L2283
	movdqu	32(%rax), %xmm5
	leaq	-272(%rbp), %rdi
	movaps	%xmm5, -272(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef19global_proxy_objectEv@PLT
	movq	-648(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-304(%rbp), %rsi
	movq	%rdx, -296(%rbp)
	movq	%rbx, %rdx
	movq	%rax, -304(%rbp)
	call	_ZNK2v88internal8compiler16JSGlobalProxyRef15GetPropertyCellERKNS1_7NameRefENS1_19SerializationPolicyE@PLT
	xorl	%eax, %eax
	cmpb	$0, -240(%rbp)
	je	.L2037
	leaq	-232(%rbp), %rax
	movq	%r12, %rdx
	movq	%rbx, %r8
	movq	%r14, %rdi
	movq	-656(%rbp), %rcx
	movl	-620(%rbp), %r9d
	pushq	%rax
	pushq	-552(%rbp)
	movq	-728(%rbp), %rsi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_RKNS1_15PropertyCellRefE
	popq	%rdx
	popq	%rcx
	jmp	.L2037
	.p2align 4,,10
	.p2align 3
.L2125:
	movq	-592(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	jmp	.L2126
.L2276:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L2131
.L2114:
	movq	16(%r14), %rsi
	movl	$1, %r15d
	jmp	.L2118
.L2263:
	movl	(%rcx), %eax
	movl	%eax, -240(%rbp)
	movq	8(%rcx), %rdi
	movq	24(%rcx), %rbx
	subq	16(%rcx), %rbx
	movq	$0, -224(%rbp)
	movq	%rdi, -232(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	je	.L2168
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	leaq	7(%rbx), %rsi
	andq	$-8, %rsi
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L2284
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L2047:
	movq	%rdx, %xmm0
	addq	%rdx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movq	24(%r15), %rax
	movq	16(%r15), %rcx
	cmpq	%rcx, %rax
	je	.L2050
	leaq	-8(%rax), %rdi
	leaq	15(%rcx), %rax
	subq	%rcx, %rdi
	subq	%rdx, %rax
	movq	%rdi, %r8
	shrq	$3, %r8
	cmpq	$30, %rax
	jbe	.L2169
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r8
	je	.L2169
	addq	$1, %r8
	xorl	%eax, %eax
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L2052:
	movdqu	(%rcx,%rax), %xmm7
	movups	%xmm7, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L2052
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rsi
	addq	%rsi, %rcx
	addq	%rdx, %rsi
	cmpq	%r8, %rax
	je	.L2055
	movq	(%rcx), %rax
	movq	%rax, (%rsi)
.L2055:
	leaq	8(%rdx,%rdi), %rdx
.L2050:
	movq	%rdx, -216(%rbp)
	movq	40(%r15), %rdi
	movq	56(%r15), %rbx
	subq	48(%r15), %rbx
	movq	$0, -192(%rbp)
	movq	%rdi, -200(%rbp)
	movq	$0, -184(%rbp)
	movq	$0, -176(%rbp)
	je	.L2170
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	leaq	7(%rbx), %rsi
	andq	$-8, %rsi
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L2285
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L2056:
	movq	%rdx, %xmm0
	addq	%rdx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movq	56(%r15), %rax
	movq	48(%r15), %rcx
	cmpq	%rcx, %rax
	je	.L2059
	leaq	-8(%rax), %rdi
	leaq	15(%rcx), %rax
	subq	%rcx, %rdi
	subq	%rdx, %rax
	movq	%rdi, %r8
	shrq	$3, %r8
	cmpq	$30, %rax
	jbe	.L2171
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r8
	je	.L2171
	addq	$1, %r8
	xorl	%eax, %eax
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L2061:
	movdqu	(%rcx,%rax), %xmm6
	movups	%xmm6, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L2061
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rsi
	addq	%rsi, %rcx
	addq	%rdx, %rsi
	cmpq	%r8, %rax
	je	.L2064
	movq	(%rcx), %rax
	movq	%rax, (%rsi)
.L2064:
	leaq	8(%rdx,%rdi), %rdx
.L2059:
	movq	%rdx, -184(%rbp)
	movq	72(%r15), %rax
	leaq	-536(%rbp), %r12
	leaq	-232(%rbp), %r13
	leaq	-496(%rbp), %rbx
	movq	24(%r14), %rsi
	movq	%r12, %r8
	movq	%r13, %rdx
	movq	%rax, -168(%rbp)
	movq	80(%r15), %rax
	movq	%rbx, %rdi
	movq	-528(%rbp), %r9
	movq	%rax, -160(%rbp)
	movq	88(%r15), %rax
	movq	%rax, -152(%rbp)
	movq	96(%r15), %rax
	movq	%rax, -144(%rbp)
	movzbl	104(%r15), %eax
	movb	%al, -136(%rbp)
	movq	112(%r15), %rax
	movq	%rax, -128(%rbp)
	movq	120(%r15), %rax
	movq	%rax, -120(%rbp)
	movq	128(%r15), %rax
	leaq	-544(%rbp), %r15
	movq	%r15, %rcx
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler21PropertyAccessBuilder19TryBuildStringCheckEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEPPNS1_4NodeESE_SD_@PLT
	testb	%al, %al
	je	.L2065
.L2252:
	leaq	-272(%rbp), %rcx
	movq	-536(%rbp), %rax
	movq	%rcx, -680(%rbp)
	movq	%rcx, %rdi
.L2066:
	movq	-560(%rbp), %rdx
	movl	-620(%rbp), %ecx
	movq	%r14, %rsi
	movq	-672(%rbp), %r9
	movq	-664(%rbp), %r8
	addq	$8, %rdx
	pushq	%rcx
	movq	-656(%rbp), %rcx
	pushq	-648(%rbp)
	pushq	-632(%rbp)
	pushq	%rdx
	pushq	-528(%rbp)
	movq	-544(%rbp), %rdx
	pushq	%rax
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization19BuildPropertyAccessEPNS1_4NodeES4_S4_S4_S4_S4_RKNS1_7NameRefEPNS0_10ZoneVectorIS4_EERKNS1_18PropertyAccessInfoENS1_10AccessModeE
	movq	-264(%rbp), %rax
	movq	-272(%rbp), %r13
	addq	$48, %rsp
	movq	%rax, -536(%rbp)
	movq	-256(%rbp), %rax
	movq	%rax, -528(%rbp)
	jmp	.L2072
.L2152:
	movq	16(%r14), %rax
	movl	%ebx, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	%ebx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-288(%rbp), %rsi
	movq	%rax, -528(%rbp)
	cmpq	-280(%rbp), %rsi
	je	.L2153
	movq	%rax, (%rsi)
	addq	$8, -288(%rbp)
.L2154:
	movq	16(%r14), %rax
	movl	%ebx, %edx
	movl	$8, %esi
	leal	1(%rbx), %r12d
	movq	-296(%rbp), %r15
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	%r12d, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-256(%rbp), %rsi
	movq	%rax, %r13
	cmpq	-248(%rbp), %rsi
	je	.L2155
	movq	-528(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -256(%rbp)
.L2156:
	movq	16(%r14), %rax
	movl	%ebx, %esi
	movq	-264(%rbp), %r15
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -552(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	%r12d, %edx
	movq	-552(%rbp), %r9
	movq	%rax, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -536(%rbp)
	jmp	.L2072
.L2132:
	movq	24(%r15), %rdi
	xorl	%r13d, %r13d
	jmp	.L2133
.L2280:
	movq	$0, -584(%rbp)
	jmp	.L2085
.L2284:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2047
.L2158:
	movq	-648(%rbp), %rdx
	movq	%rcx, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-632(%rbp), %rcx
	jmp	.L2159
.L2170:
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	jmp	.L2056
.L2168:
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	jmp	.L2047
.L2282:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%rbx)
	movq	%rax, %r13
	jmp	.L2150
.L2065:
	movq	24(%r14), %rsi
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	-528(%rbp), %r9
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21PropertyAccessBuilder19TryBuildNumberCheckEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEPPNS1_4NodeESE_SD_@PLT
	testb	%al, %al
	jne	.L2252
	movq	24(%r14), %rax
	movq	-224(%rbp), %r15
	leaq	-272(%rbp), %rcx
	movq	%rcx, -680(%rbp)
	movq	%rax, %rdi
	movq	-216(%rbp), %rax
	cmpq	%rax, %r15
	je	.L2071
	movq	%r14, -552(%rbp)
	movq	-680(%rbp), %r14
	movq	%r12, -584(%rbp)
	movq	%r15, %r12
	movq	%r13, %r15
	movq	%rax, %r13
	movq	%rbx, -576(%rbp)
	movq	%rdi, %rbx
	jmp	.L2070
.L2069:
	addq	$8, %r12
	cmpq	%r12, %r13
	je	.L2286
.L2070:
	movq	(%r12), %rdx
	movq	%r14, %rdi
	movl	$1, %ecx
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef15IsHeapNumberMapEv@PLT
	testb	%al, %al
	je	.L2069
	movq	-552(%rbp), %r14
	movq	%r15, %r13
	movq	-544(%rbp), %r15
	movq	-576(%rbp), %rbx
	movq	16(%r14), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11ObjectIsSmiEv@PLT
	movq	%r15, -96(%rbp)
	leaq	-96(%rbp), %r15
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r15, %rcx
	movl	$1, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-528(%rbp), %rcx
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%rax, -576(%rbp)
	movq	16(%r14), %rax
	movq	%rcx, -552(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-576(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-552(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	16(%r14), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -552(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-552(%rbp), %r9
	movq	%rax, %rsi
	movq	%r12, -96(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -552(%rbp)
	movq	-536(%rbp), %rax
	movq	%rax, -576(%rbp)
	movq	16(%r14), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -584(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	-584(%rbp), %r9
	movq	%rax, %rsi
	movq	%r12, -96(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-680(%rbp), %rdx
	movq	%r13, %r8
	movq	%rbx, %rdi
	movq	%rax, %r12
	movq	-544(%rbp), %rsi
	movq	-536(%rbp), %rax
	movq	%r12, %rcx
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler21PropertyAccessBuilder14BuildCheckMapsEPNS1_4NodeEPS4_S4_RKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE@PLT
	movq	16(%r14), %rax
	movl	$2, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	%r12, %xmm5
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-552(%rbp), %xmm0
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-272(%rbp), %rbx
	movl	$2, %esi
	movq	%rax, -528(%rbp)
	movq	%rax, %r13
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	%rbx, %xmm5
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r15, %rcx
	movl	$3, %edx
	movq	%r13, -80(%rbp)
	movq	-576(%rbp), %xmm0
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-680(%rbp), %rdi
	movq	%rax, -536(%rbp)
	jmp	.L2066
.L2171:
	xorl	%eax, %eax
.L2060:
	movq	(%rcx,%rax,8), %rsi
	movq	%rsi, (%rdx,%rax,8)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%r8, %rsi
	jne	.L2060
	jmp	.L2064
.L2169:
	xorl	%eax, %eax
.L2051:
	movq	(%rcx,%rax,8), %rsi
	movq	%rsi, (%rdx,%rax,8)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%r8, %rsi
	jne	.L2051
	jmp	.L2055
.L2155:
	leaq	-528(%rbp), %rdx
	leaq	-272(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2156
.L2153:
	leaq	-528(%rbp), %rdx
	leaq	-304(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2154
.L2285:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2056
.L2286:
	movq	-552(%rbp), %r14
	movq	-584(%rbp), %r12
	movq	%r15, %r13
	movq	-576(%rbp), %rbx
.L2071:
	movq	-528(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %r8
	movq	%r12, %rdx
	movq	-544(%rbp), %rsi
	call	_ZN2v88internal8compiler21PropertyAccessBuilder14BuildCheckMapsEPNS1_4NodeEPS4_S4_RKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE@PLT
	movq	-536(%rbp), %rax
	movq	-680(%rbp), %rdi
	jmp	.L2066
.L2283:
	leaq	.LC10(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2260:
	call	__stack_chk_fail@PLT
.L2174:
	movl	$1, %r15d
	leaq	-96(%rbp), %rcx
	jmp	.L2118
	.cfi_endproc
.LFE23192:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceNamedAccessEPNS1_4NodeES4_RKNS1_19NamedAccessFeedbackENS1_10AccessModeES4_, .-_ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceNamedAccessEPNS1_4NodeES4_RKNS1_19NamedAccessFeedbackENS1_10AccessModeES4_
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization20ReducePropertyAccessEPNS1_4NodeES4_NS_4base8OptionalINS1_7NameRefEEES4_RKNS1_14FeedbackSourceENS1_10AccessModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReducePropertyAccessEPNS1_4NodeES4_NS_4base8OptionalINS1_7NameRefEEES4_RKNS1_14FeedbackSourceENS1_10AccessModeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReducePropertyAccessEPNS1_4NodeES4_NS_4base8OptionalINS1_7NameRefEEES4_RKNS1_14FeedbackSourceENS1_10AccessModeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization20ReducePropertyAccessEPNS1_4NodeES4_NS_4base8OptionalINS1_7NameRefEEES4_RKNS1_14FeedbackSourceENS1_10AccessModeE:
.LFB23232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	%r9d, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%r8, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$16, %rsp
	movq	24(%rdi), %rdi
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN2v88internal8compiler12JSHeapBroker28GetFeedbackForPropertyAccessERKNS1_14FeedbackSourceENS1_10AccessModeENS_4base8OptionalINS1_7NameRefEEE@PLT
	addq	$32, %rsp
	movq	%rax, %rdi
	movl	(%rax), %eax
	cmpl	$4, %eax
	je	.L2288
	cmpl	$8, %eax
	je	.L2289
	testl	%eax, %eax
	je	.L2297
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2288:
	call	_ZNK2v88internal8compiler17ProcessedFeedback15AsElementAccessEv@PLT
	leaq	-40(%rbp), %rsp
	movq	%r15, %rcx
	movq	%r14, %rdx
	popq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r8
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceElementAccessEPNS1_4NodeES4_S4_RKNS1_21ElementAccessFeedbackE
	.p2align 4,,10
	.p2align 3
.L2297:
	.cfi_restore_state
	xorl	%eax, %eax
	testb	$2, 32(%r12)
	jne	.L2298
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2289:
	.cfi_restore_state
	call	_ZNK2v88internal8compiler17ProcessedFeedback13AsNamedAccessEv@PLT
	leaq	-40(%rbp), %rsp
	movq	%r14, %r9
	movl	%ebx, %r8d
	movq	%r15, %rdx
	popq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceNamedAccessEPNS1_4NodeES4_RKNS1_19NamedAccessFeedbackENS1_10AccessModeES4_
	.p2align 4,,10
	.p2align 3
.L2298:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$13, %edx
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceSoftDeoptimizeEPNS1_4NodeENS0_16DeoptimizeReasonE.part.0
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23232:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReducePropertyAccessEPNS1_4NodeES4_NS_4base8OptionalINS1_7NameRefEEES4_RKNS1_14FeedbackSourceENS1_10AccessModeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization20ReducePropertyAccessEPNS1_4NodeES4_NS_4base8OptionalINS1_7NameRefEEES4_RKNS1_14FeedbackSourceENS1_10AccessModeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSHasPropertyEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSHasPropertyEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSHasPropertyEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSHasPropertyEPNS1_4NodeE:
.LFB23234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16PropertyAccessOfEPKNS1_8OperatorE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	16(%r13), %r14
	movq	%rax, %r15
	movq	352(%r14), %rcx
	testq	%rcx, %rcx
	je	.L2305
.L2300:
	cmpq	$0, (%rbx)
	je	.L2301
	cmpl	$-1, 8(%rbx)
	je	.L2301
	movdqu	(%rbx), %xmm0
	subq	$8, %rsp
	movb	$0, -80(%rbp)
	leaq	-96(%rbp), %r8
	pushq	-64(%rbp)
	movl	$3, %r9d
	movq	%r15, %rdx
	movq	%r12, %rsi
	movb	$0, -72(%rbp)
	movq	%r13, %rdi
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReducePropertyAccessEPNS1_4NodeES4_NS_4base8OptionalINS1_7NameRefEEES4_RKNS1_14FeedbackSourceENS1_10AccessModeE
	addq	$32, %rsp
	jmp	.L2302
	.p2align 4,,10
	.p2align 3
.L2301:
	xorl	%eax, %eax
.L2302:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2306
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2305:
	.cfi_restore_state
	movq	(%r14), %r9
	movq	8(%r14), %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	-104(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	xorl	%edx, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r14)
	movq	%rax, %rcx
	jmp	.L2300
.L2306:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23234:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSHasPropertyEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSHasPropertyEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceJSLoadPropertyEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceJSLoadPropertyEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceJSLoadPropertyEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceJSLoadPropertyEPNS1_4NodeE:
.LFB23236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16PropertyAccessOfEPKNS1_8OperatorE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	cmpw	$768, 16(%rax)
	je	.L2320
.L2308:
	cmpq	$0, (%rbx)
	je	.L2311
	cmpl	$-1, 8(%rbx)
	je	.L2311
	movq	16(%r14), %r15
	movq	352(%r15), %rcx
	testq	%rcx, %rcx
	je	.L2321
.L2312:
	movdqu	(%rbx), %xmm0
	subq	$8, %rsp
	movb	$0, -80(%rbp)
	xorl	%r9d, %r9d
	pushq	-64(%rbp)
	leaq	-96(%rbp), %r8
	movq	%r13, %rdx
	movq	%r12, %rsi
	movb	$0, -72(%rbp)
	movq	%r14, %rdi
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReducePropertyAccessEPNS1_4NodeES4_NS_4base8OptionalINS1_7NameRefEEES4_RKNS1_14FeedbackSourceENS1_10AccessModeE
	addq	$32, %rsp
.L2310:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2322
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2311:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L2310
	.p2align 4,,10
	.p2align 3
.L2320:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization37ReduceJSLoadPropertyWithEnumeratedKeyEPNS1_4NodeE
	testq	%rax, %rax
	jne	.L2310
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2321:
	movq	(%r15), %r9
	movq	8(%r15), %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	-104(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	xorl	%edx, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r15)
	movq	%rax, %rcx
	jmp	.L2312
.L2322:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23236:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceJSLoadPropertyEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceJSLoadPropertyEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization21ReduceJSStorePropertyEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization21ReduceJSStorePropertyEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization21ReduceJSStorePropertyEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization21ReduceJSStorePropertyEPNS1_4NodeE:
.LFB23237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16PropertyAccessOfEPKNS1_8OperatorE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	cmpq	$0, (%rbx)
	je	.L2324
	cmpl	$-1, 8(%rbx)
	je	.L2324
	movdqu	(%rbx), %xmm0
	subq	$8, %rsp
	movb	$0, -64(%rbp)
	movq	%rax, %rcx
	pushq	-48(%rbp)
	movl	$1, %r9d
	leaq	-80(%rbp), %r8
	movq	%r14, %rdx
	movb	$0, -56(%rbp)
	movq	%r12, %rsi
	movq	%r13, %rdi
	pushq	-56(%rbp)
	pushq	-64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReducePropertyAccessEPNS1_4NodeES4_NS_4base8OptionalINS1_7NameRefEEES4_RKNS1_14FeedbackSourceENS1_10AccessModeE
	addq	$32, %rsp
	jmp	.L2325
	.p2align 4,,10
	.p2align 3
.L2324:
	xorl	%eax, %eax
.L2325:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2328
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2328:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23237:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization21ReduceJSStorePropertyEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization21ReduceJSStorePropertyEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization34ReduceJSStoreDataPropertyInLiteralEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization34ReduceJSStoreDataPropertyInLiteralEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization34ReduceJSStoreDataPropertyInLiteralEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization34ReduceJSStoreDataPropertyInLiteralEPNS1_4NodeE:
.LFB23251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19FeedbackParameterOfEPKNS1_8OperatorE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	cmpq	$0, (%rbx)
	je	.L2330
	cmpl	$-1, 8(%rbx)
	je	.L2330
	movdqu	(%rbx), %xmm0
	subq	$8, %rsp
	movb	$0, -64(%rbp)
	movq	%rax, %rcx
	pushq	-48(%rbp)
	movl	$2, %r9d
	leaq	-80(%rbp), %r8
	movq	%r14, %rdx
	movb	$0, -56(%rbp)
	movq	%r12, %rsi
	movq	%r13, %rdi
	pushq	-56(%rbp)
	pushq	-64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReducePropertyAccessEPNS1_4NodeES4_NS_4base8OptionalINS1_7NameRefEEES4_RKNS1_14FeedbackSourceENS1_10AccessModeE
	addq	$32, %rsp
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2330:
	xorl	%eax, %eax
.L2331:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2334
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2334:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23251:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization34ReduceJSStoreDataPropertyInLiteralEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization34ReduceJSStoreDataPropertyInLiteralEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSStoreInArrayLiteralEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSStoreInArrayLiteralEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSStoreInArrayLiteralEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSStoreInArrayLiteralEPNS1_4NodeE:
.LFB29499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19FeedbackParameterOfEPKNS1_8OperatorE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rcx
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L2336
	xorl	%eax, %eax
	cmpl	$-1, 8(%rbx)
	je	.L2336
	movdqu	(%rbx), %xmm0
	subq	$8, %rsp
	movb	$0, -64(%rbp)
	leaq	-80(%rbp), %r8
	pushq	-48(%rbp)
	movl	$2, %r9d
	movq	%r14, %rdx
	movq	%r12, %rsi
	movb	$0, -56(%rbp)
	movq	%r13, %rdi
	pushq	-56(%rbp)
	pushq	-64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReducePropertyAccessEPNS1_4NodeES4_NS_4base8OptionalINS1_7NameRefEEES4_RKNS1_14FeedbackSourceENS1_10AccessModeE
	addq	$32, %rsp
.L2336:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2343
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2343:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29499:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSStoreInArrayLiteralEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSStoreInArrayLiteralEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSGetIteratorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSGetIteratorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSGetIteratorEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSGetIteratorEPNS1_4NodeE:
.LFB23207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 3, -48
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16PropertyAccessOfEPKNS1_8OperatorE@PLT
	movq	24(%r12), %rsi
	leaq	-128(%rbp), %rdi
	movl	$1, %ecx
	movq	%rax, %rbx
	movq	16(%r12), %rax
	movq	360(%rax), %rdx
	addq	$3856, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqu	(%rbx), %xmm0
	movq	16(%r12), %rbx
	movq	352(%rbx), %r14
	movaps	%xmm0, -112(%rbp)
	testq	%r14, %r14
	je	.L2355
.L2345:
	movdqa	-128(%rbp), %xmm1
	movb	$1, -96(%rbp)
	subq	$8, %rsp
	xorl	%edx, %edx
	movq	24(%r12), %rdi
	leaq	-112(%rbp), %rsi
	movups	%xmm1, -88(%rbp)
	movdqa	-96(%rbp), %xmm2
	movq	-80(%rbp), %rax
	movaps	%xmm2, -64(%rbp)
	movq	%rax, -48(%rbp)
	pushq	-48(%rbp)
	pushq	-56(%rbp)
	pushq	-64(%rbp)
	call	_ZN2v88internal8compiler12JSHeapBroker28GetFeedbackForPropertyAccessERKNS1_14FeedbackSourceENS1_10AccessModeENS_4base8OptionalINS1_7NameRefEEE@PLT
	addq	$32, %rsp
	movq	%rax, %rdi
	movl	(%rax), %eax
	cmpl	$4, %eax
	je	.L2346
	cmpl	$8, %eax
	je	.L2347
	testl	%eax, %eax
	je	.L2356
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2346:
	call	_ZNK2v88internal8compiler17ProcessedFeedback15AsElementAccessEv@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rax, %r8
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceElementAccessEPNS1_4NodeES4_S4_RKNS1_21ElementAccessFeedbackE
.L2350:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2357
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2356:
	.cfi_restore_state
	xorl	%eax, %eax
	testb	$2, 32(%r12)
	je	.L2350
	movl	$13, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceSoftDeoptimizeEPNS1_4NodeENS0_16DeoptimizeReasonE.part.0
	jmp	.L2350
	.p2align 4,,10
	.p2align 3
.L2347:
	call	_ZNK2v88internal8compiler17ProcessedFeedback13AsNamedAccessEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movq	%rax, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceNamedAccessEPNS1_4NodeES4_RKNS1_19NamedAccessFeedbackENS1_10AccessModeES4_
	jmp	.L2350
	.p2align 4,,10
	.p2align 3
.L2355:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%rbx)
	movq	%rax, %r14
	jmp	.L2345
.L2357:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23207:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSGetIteratorEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSGetIteratorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceNamedAccessFromNexusEPNS1_4NodeES4_RKNS1_14FeedbackSourceERKNS1_7NameRefENS1_10AccessModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceNamedAccessFromNexusEPNS1_4NodeES4_RKNS1_14FeedbackSourceERKNS1_7NameRefENS1_10AccessModeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceNamedAccessFromNexusEPNS1_4NodeES4_RKNS1_14FeedbackSourceERKNS1_7NameRefENS1_10AccessModeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceNamedAccessFromNexusEPNS1_4NodeES4_RKNS1_14FeedbackSourceERKNS1_7NameRefENS1_10AccessModeE:
.LFB23205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$120, %rsp
	movq	%rdx, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpw	$30, 16(%rax)
	jne	.L2359
	leaq	-80(%rbp), %r15
	movq	24(%r12), %rsi
	movq	48(%rax), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L2378
	movdqa	-80(%rbp), %xmm2
	movq	24(%r12), %rax
	movaps	%xmm2, -128(%rbp)
	cmpb	$0, 24(%rax)
	je	.L2362
	leaq	-112(%rbp), %r9
	movdqu	32(%rax), %xmm3
	movq	%r9, %rdi
	movq	%r9, -160(%rbp)
	movaps	%xmm3, -112(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef19global_proxy_objectEv@PLT
	leaq	-128(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	movq	%r8, -152(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %r9
	testb	%al, %al
	jne	.L2379
.L2359:
	movdqu	(%r14), %xmm0
	movb	$1, -112(%rbp)
	subq	$8, %rsp
	movl	%ebx, %edx
	movq	24(%r12), %rdi
	movq	-136(%rbp), %rsi
	movups	%xmm0, -104(%rbp)
	movdqa	-112(%rbp), %xmm1
	movq	-96(%rbp), %rax
	movaps	%xmm1, -80(%rbp)
	movq	%rax, -64(%rbp)
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZN2v88internal8compiler12JSHeapBroker28GetFeedbackForPropertyAccessERKNS1_14FeedbackSourceENS1_10AccessModeENS_4base8OptionalINS1_7NameRefEEE@PLT
	addq	$32, %rsp
	movq	%rax, %rdi
	movl	(%rax), %eax
	cmpl	$4, %eax
	je	.L2365
	cmpl	$8, %eax
	je	.L2366
	testl	%eax, %eax
	je	.L2380
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2380:
	xorl	%eax, %eax
	testb	$2, 32(%r12)
	je	.L2364
	movl	$13, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceSoftDeoptimizeEPNS1_4NodeENS0_16DeoptimizeReasonE.part.0
	jmp	.L2364
	.p2align 4,,10
	.p2align 3
.L2366:
	call	_ZNK2v88internal8compiler17ProcessedFeedback13AsNamedAccessEv@PLT
	xorl	%r9d, %r9d
	movl	%ebx, %r8d
	movq	%r13, %rsi
	movq	-144(%rbp), %rdx
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceNamedAccessEPNS1_4NodeES4_RKNS1_19NamedAccessFeedbackENS1_10AccessModeES4_
.L2364:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2381
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2365:
	.cfi_restore_state
	call	_ZNK2v88internal8compiler17ProcessedFeedback15AsElementAccessEv@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-144(%rbp), %rcx
	movq	%rax, %r8
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceElementAccessEPNS1_4NodeES4_S4_RKNS1_21ElementAccessFeedbackE
	jmp	.L2364
	.p2align 4,,10
	.p2align 3
.L2379:
	movq	24(%r12), %rax
	movq	%r8, -136(%rbp)
	cmpb	$0, 24(%rax)
	je	.L2362
	movdqu	32(%rax), %xmm4
	movq	%r9, %rdi
	movaps	%xmm4, -112(%rbp)
	call	_ZNK2v88internal8compiler16NativeContextRef19global_proxy_objectEv@PLT
	movq	-136(%rbp), %r8
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%rdx, -120(%rbp)
	movq	%r14, %rdx
	movq	%r8, %rsi
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal8compiler16JSGlobalProxyRef15GetPropertyCellERKNS1_7NameRefENS1_19SerializationPolicyE@PLT
	xorl	%eax, %eax
	cmpb	$0, -80(%rbp)
	je	.L2364
	leaq	-72(%rbp), %rax
	movq	-144(%rbp), %rcx
	xorl	%edx, %edx
	movl	%ebx, %r9d
	pushq	%rax
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	$0
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceGlobalAccessEPNS1_4NodeES4_S4_RKNS1_7NameRefENS1_10AccessModeES4_RKNS1_15PropertyCellRefE
	popq	%rdx
	popq	%rcx
	jmp	.L2364
	.p2align 4,,10
	.p2align 3
.L2378:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2362:
	leaq	.LC10(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2381:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23205:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceNamedAccessFromNexusEPNS1_4NodeES4_RKNS1_14FeedbackSourceERKNS1_7NameRefENS1_10AccessModeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceNamedAccessFromNexusEPNS1_4NodeES4_RKNS1_14FeedbackSourceERKNS1_7NameRefENS1_10AccessModeE
	.section	.rodata._ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceJSLoadNamedEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC37:
	.string	"data for function "
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceJSLoadNamedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceJSLoadNamedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceJSLoadNamedEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceJSLoadNamedEPNS1_4NodeE:
.LFB23206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13NamedAccessOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%rbx), %rdx
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	movq	%rax, %r15
	movl	$1, %ecx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	(%r15), %rax
	cmpw	$30, 16(%rax)
	jne	.L2383
	leaq	-80(%rbp), %r15
	movq	24(%r12), %rsi
	movq	48(%rax), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L2419
	leaq	-112(%rbp), %rax
	movdqa	-80(%rbp), %xmm2
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	movaps	%xmm2, -112(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	testb	%al, %al
	je	.L2385
	movq	16(%r12), %rax
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	movq	360(%rax), %rdx
	addq	$3104, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	jne	.L2420
.L2385:
	movq	-136(%rbp), %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	testb	%al, %al
	jne	.L2421
.L2383:
	cmpq	$0, 8(%rbx)
	je	.L2399
	cmpl	$-1, 16(%rbx)
	je	.L2399
	movdqu	8(%rbx), %xmm1
	movq	16(%r12), %rbx
	movaps	%xmm1, -80(%rbp)
	movq	352(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L2422
.L2400:
	leaq	-80(%rbp), %rcx
	xorl	%r9d, %r9d
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceNamedAccessFromNexusEPNS1_4NodeES4_RKNS1_14FeedbackSourceERKNS1_7NameRefENS1_10AccessModeE
	jmp	.L2398
	.p2align 4,,10
	.p2align 3
.L2420:
	movq	-136(%rbp), %rdi
	leaq	-96(%rbp), %r14
	call	_ZNK2v88internal8compiler9ObjectRef12AsJSFunctionEv@PLT
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	je	.L2390
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef10serializedEv@PLT
	testb	%al, %al
	jne	.L2390
	movq	24(%r12), %rdi
	cmpb	$0, 124(%rdi)
	je	.L2399
	call	_ZN2v88internal8compiler12JSHeapBroker5TraceEv@PLT
	movl	$8, %edx
	leaq	.LC12(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$18, %edx
	movq	%r12, %rdi
	leaq	.LC37(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_9ObjectRefE@PLT
	movl	$2, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$59, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$1368, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r13
	testq	%r13, %r13
	je	.L2423
	cmpb	$0, 56(%r13)
	je	.L2393
	movsbl	67(%r13), %esi
.L2394:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	.p2align 4,,10
	.p2align 3
.L2399:
	xorl	%eax, %eax
.L2398:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2424
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2421:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	movl	$1, %ecx
	movq	360(%rax), %rdx
	addq	$2768, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	je	.L2383
	movq	-136(%rbp), %rdi
	movq	16(%r12), %r14
	call	_ZNK2v88internal8compiler9ObjectRef8AsStringEv@PLT
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler9StringRef6lengthEv@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
.L2418:
	movq	8(%r12), %rdi
	movq	%rax, %rbx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rax
	jmp	.L2398
	.p2align 4,,10
	.p2align 3
.L2422:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%rbx)
	movq	%rax, %rdx
	jmp	.L2400
	.p2align 4,,10
	.p2align 3
.L2419:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2390:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler6MapRef18has_prototype_slotEv@PLT
	testb	%al, %al
	je	.L2399
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef13has_prototypeEv@PLT
	testb	%al, %al
	je	.L2399
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13JSFunctionRef30PrototypeRequiresRuntimeLookupEv@PLT
	testb	%al, %al
	jne	.L2399
	movq	56(%r12), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler23CompilationDependencies25DependOnPrototypePropertyERKNS1_13JSFunctionRefE@PLT
	movq	16(%r12), %rdi
	movq	%r15, %rsi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	jmp	.L2418
.L2393:
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2394
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2394
.L2424:
	call	__stack_chk_fail@PLT
.L2423:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE23206:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceJSLoadNamedEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceJSLoadNamedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSStoreNamedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSStoreNamedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSStoreNamedEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSStoreNamedEPNS1_4NodeE:
.LFB23208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13NamedAccessOfEPKNS1_8OperatorE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	cmpq	$0, 8(%rbx)
	je	.L2426
	cmpl	$-1, 16(%rbx)
	je	.L2426
	movq	(%rbx), %rdx
	movq	24(%r13), %rsi
	leaq	-80(%rbp), %r15
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqu	8(%rbx), %xmm0
	movq	%r15, %r8
	movq	%r14, %rdx
	leaq	-96(%rbp), %rcx
	movl	$1, %r9d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceNamedAccessFromNexusEPNS1_4NodeES4_RKNS1_14FeedbackSourceERKNS1_7NameRefENS1_10AccessModeE
	jmp	.L2427
	.p2align 4,,10
	.p2align 3
.L2426:
	xorl	%eax, %eax
.L2427:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2430
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2430:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23208:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSStoreNamedEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSStoreNamedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization21ReduceJSStoreNamedOwnEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization21ReduceJSStoreNamedOwnEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization21ReduceJSStoreNamedOwnEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization21ReduceJSStoreNamedOwnEPNS1_4NodeE:
.LFB23209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler25StoreNamedOwnParametersOfEPKNS1_8OperatorE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	cmpq	$0, 8(%rbx)
	je	.L2432
	cmpl	$-1, 16(%rbx)
	je	.L2432
	movq	(%rbx), %rdx
	movq	24(%r13), %rsi
	leaq	-80(%rbp), %r15
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqu	8(%rbx), %xmm0
	movq	%r15, %r8
	movq	%r14, %rdx
	leaq	-96(%rbp), %rcx
	movl	$2, %r9d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceNamedAccessFromNexusEPNS1_4NodeES4_RKNS1_14FeedbackSourceERKNS1_7NameRefENS1_10AccessModeE
	jmp	.L2433
	.p2align 4,,10
	.p2align 3
.L2432:
	xorl	%eax, %eax
.L2433:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2436
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2436:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23209:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization21ReduceJSStoreNamedOwnEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization21ReduceJSStoreNamedOwnEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler29JSNativeContextSpecialization6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler29JSNativeContextSpecialization6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler29JSNativeContextSpecialization6ReduceEPNS1_4NodeE:
.LFB23099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdi), %eax
	subw	$694, %ax
	cmpw	$90, %ax
	ja	.L2438
	leaq	.L2440(%rip), %rdx
	movzwl	%ax, %eax
	movq	%rsi, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler29JSNativeContextSpecialization6ReduceEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L2440:
	.long	.L2463-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2462-.L2440
	.long	.L2461-.L2440
	.long	.L2460-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2459-.L2440
	.long	.L2458-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2457-.L2440
	.long	.L2456-.L2440
	.long	.L2455-.L2440
	.long	.L2454-.L2440
	.long	.L2453-.L2440
	.long	.L2452-.L2440
	.long	.L2451-.L2440
	.long	.L2450-.L2440
	.long	.L2449-.L2440
	.long	.L2438-.L2440
	.long	.L2448-.L2440
	.long	.L2447-.L2440
	.long	.L2446-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2445-.L2440
	.long	.L2444-.L2440
	.long	.L2443-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2442-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2438-.L2440
	.long	.L2441-.L2440
	.long	.L2438-.L2440
	.long	.L2439-.L2440
	.section	.text._ZN2v88internal8compiler29JSNativeContextSpecialization6ReduceEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L2438:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2464:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2495
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2439:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization22ReduceJSResolvePromiseEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2444:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSAsyncFunctionRejectEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2445:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceJSAsyncFunctionEnterEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2446:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSLoadContextEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2447:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSGetSuperConstructorEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2442:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSGetIteratorEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2443:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization28ReduceJSAsyncFunctionResolveEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2441:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization22ReduceJSPromiseResolveEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2448:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSHasPropertyEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2449:
	call	_ZN2v88internal8compiler19FeedbackParameterOfEPKNS1_8OperatorE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rcx
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L2464
	xorl	%eax, %eax
	cmpl	$-1, 8(%rbx)
	je	.L2464
	movdqu	(%rbx), %xmm4
	movaps	%xmm4, -96(%rbp)
.L2494:
	subq	$8, %rsp
	movb	$0, -80(%rbp)
	movl	$2, %r9d
	pushq	-64(%rbp)
	movb	$0, -72(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
.L2493:
	leaq	-96(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReducePropertyAccessEPNS1_4NodeES4_NS_4base8OptionalINS1_7NameRefEEES4_RKNS1_14FeedbackSourceENS1_10AccessModeE
	addq	$32, %rsp
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2450:
	call	_ZN2v88internal8compiler19FeedbackParameterOfEPKNS1_8OperatorE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rcx
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L2464
	xorl	%eax, %eax
	cmpl	$-1, 8(%rbx)
	je	.L2464
	movdqu	(%rbx), %xmm3
	movaps	%xmm3, -96(%rbp)
	jmp	.L2494
	.p2align 4,,10
	.p2align 3
.L2451:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization19ReduceJSStoreGlobalEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2452:
	call	_ZN2v88internal8compiler25StoreNamedOwnParametersOfEPKNS1_8OperatorE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L2464
	xorl	%eax, %eax
	cmpl	$-1, 16(%rbx)
	je	.L2464
	movq	(%rbx), %rdx
	movq	24(%r13), %rsi
	leaq	-96(%rbp), %r15
	movl	$1, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqu	8(%rbx), %xmm2
	leaq	-80(%rbp), %rcx
	movl	$2, %r9d
	movaps	%xmm2, -80(%rbp)
.L2492:
	movq	%r15, %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization26ReduceNamedAccessFromNexusEPNS1_4NodeES4_RKNS1_14FeedbackSourceERKNS1_7NameRefENS1_10AccessModeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2453:
	call	_ZN2v88internal8compiler13NamedAccessOfEPKNS1_8OperatorE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r14
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L2464
	xorl	%eax, %eax
	cmpl	$-1, 16(%rbx)
	je	.L2464
	movq	(%rbx), %rdx
	movq	24(%r13), %rsi
	leaq	-96(%rbp), %r15
	movl	$1, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movdqu	8(%rbx), %xmm0
	leaq	-80(%rbp), %rcx
	movl	$1, %r9d
	movaps	%xmm0, -80(%rbp)
	jmp	.L2492
	.p2align 4,,10
	.p2align 3
.L2454:
	call	_ZN2v88internal8compiler16PropertyAccessOfEPKNS1_8OperatorE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rcx
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L2464
	xorl	%eax, %eax
	cmpl	$-1, 8(%rbx)
	je	.L2464
	movdqu	(%rbx), %xmm1
	subq	$8, %rsp
	movb	$0, -80(%rbp)
	movl	$1, %r9d
	pushq	-64(%rbp)
	movb	$0, -72(%rbp)
	movaps	%xmm1, -96(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	jmp	.L2493
	.p2align 4,,10
	.p2align 3
.L2455:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSLoadGlobalEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2456:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization17ReduceJSLoadNamedEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2457:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization20ReduceJSLoadPropertyEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2458:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization16ReduceJSToStringEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2459:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization16ReduceJSToObjectEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2460:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSOrdinaryHasInstanceEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2461:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization18ReduceJSInstanceOfEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2462:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization27ReduceJSHasInPrototypeChainEPNS1_4NodeE
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2463:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler29JSNativeContextSpecialization11ReduceJSAddEPNS1_4NodeE
	jmp	.L2464
.L2495:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23099:
	.size	_ZN2v88internal8compiler29JSNativeContextSpecialization6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler29JSNativeContextSpecialization6ReduceEPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler29JSNativeContextSpecializationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler29JSNativeContextSpecializationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_, @function
_GLOBAL__sub_I__ZN2v88internal8compiler29JSNativeContextSpecializationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_:
.LFB29196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29196:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler29JSNativeContextSpecializationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_, .-_GLOBAL__sub_I__ZN2v88internal8compiler29JSNativeContextSpecializationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler29JSNativeContextSpecializationC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_
	.section	.rodata.CSWTCH.542,"a"
	.align 32
	.type	CSWTCH.542, @object
	.size	CSWTCH.542, 44
CSWTCH.542:
	.long	2
	.long	1
	.long	4
	.long	3
	.long	6
	.long	5
	.long	7
	.long	8
	.long	9
	.long	11
	.long	10
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal8compiler29JSNativeContextSpecializationE
	.section	.data.rel.ro._ZTVN2v88internal8compiler29JSNativeContextSpecializationE,"awG",@progbits,_ZTVN2v88internal8compiler29JSNativeContextSpecializationE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler29JSNativeContextSpecializationE, @object
	.size	_ZTVN2v88internal8compiler29JSNativeContextSpecializationE, 56
_ZTVN2v88internal8compiler29JSNativeContextSpecializationE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler29JSNativeContextSpecializationD1Ev
	.quad	_ZN2v88internal8compiler29JSNativeContextSpecializationD0Ev
	.quad	_ZNK2v88internal8compiler29JSNativeContextSpecialization12reducer_nameEv
	.quad	_ZN2v88internal8compiler29JSNativeContextSpecialization6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC26:
	.long	4085252096
	.long	1104150527
	.align 8
.LC27:
	.long	0
	.long	0
	.align 8
.LC28:
	.long	4290772992
	.long	1106247679
	.align 8
.LC29:
	.long	0
	.long	1074790400
	.align 8
.LC30:
	.long	4290772992
	.long	1105199103
	.align 8
.LC31:
	.long	0
	.long	1083179008
	.align 8
.LC33:
	.long	0
	.long	1076101120
	.align 8
.LC34:
	.long	0
	.long	1105199103
	.align 8
.LC35:
	.long	0
	.long	1076887552
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
