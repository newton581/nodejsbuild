	.file	"simd-scalar-lowering.cc"
	.text
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i.constprop.0, @function
_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i.constprop.0:
.LFB17427:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movl	20(%rsi), %r14d
	movq	(%rax), %rax
	andl	$16777215, %r14d
	movq	(%rax), %rdi
	salq	$4, %r14
	addq	112(%rbx), %r14
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L6
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3:
	movq	%rax, (%r14)
	movl	20(%r12), %eax
	movq	0(%r13), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%rbx), %rax
	movq	(%rax), %rax
	movq	%rdx, (%rax)
	movl	20(%r12), %eax
	movq	8(%r13), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%rbx), %rax
	movq	(%rax), %rax
	movq	%rdx, 8(%rax)
	movl	20(%r12), %eax
	movq	16(%r13), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%rbx), %rax
	movq	(%rax), %rax
	movq	%rdx, 16(%rax)
	movl	20(%r12), %eax
	movq	24(%r13), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%rbx), %rax
	movq	(%rax), %rax
	movq	%rdx, 24(%rax)
	movl	20(%r12), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%rbx), %rax
	movl	$4, 12(%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3
	.cfi_endproc
.LFE17427:
	.size	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i.constprop.0, .-_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i.constprop.0
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLoweringC2EPNS1_12MachineGraphEPNS0_9SignatureINS0_21MachineRepresentationEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"placeholder"
	.section	.text._ZN2v88internal8compiler18SimdScalarLoweringC2EPNS1_12MachineGraphEPNS0_9SignatureINS0_21MachineRepresentationEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLoweringC2EPNS1_12MachineGraphEPNS0_9SignatureINS0_21MachineRepresentationEEE
	.type	_ZN2v88internal8compiler18SimdScalarLoweringC2EPNS1_12MachineGraphEPNS0_9SignatureINS0_21MachineRepresentationEEE, @function
_ZN2v88internal8compiler18SimdScalarLoweringC2EPNS1_12MachineGraphEPNS0_9SignatureINS0_21MachineRepresentationEEE:
.LFB14571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	movl	$3, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -8(%rdi)
	movq	(%rsi), %rsi
	call	_ZN2v88internal8compiler14NodeMarkerBaseC2EPNS1_5GraphEj@PLT
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	%rdi, 16(%rbx)
	movq	$8, 40(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 96(%rbx)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$63, %rdx
	jbe	.L21
	leaq	64(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L9:
	movq	40(%rbx), %rdx
	movq	%rax, 32(%rbx)
	leaq	-4(,%rdx,4), %r12
	andq	$-8, %r12
	addq	%rax, %r12
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L15
	cmpq	$31, 8(%rax)
	ja	.L22
.L15:
	movq	16(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L11
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L13:
	movq	%rax, (%r12)
	movl	$-2, %esi
	movq	%r12, 72(%rbx)
	movq	(%r12), %rdx
	movq	%r12, 104(%rbx)
	leaq	512(%rdx), %rax
	movq	%rdx, 56(%rbx)
	movq	%rax, 64(%rbx)
	movq	(%r12), %rax
	movq	%rdx, 48(%rbx)
	leaq	.LC0(%rip), %rdx
	movq	%rax, 88(%rbx)
	leaq	512(%rax), %rcx
	movq	%rax, 80(%rbx)
	movq	(%rbx), %rax
	movq	%rcx, 96(%rbx)
	movq	$0, 112(%rbx)
	movq	%r13, 120(%rbx)
	movq	(%rax), %r12
	movq	8(%rax), %rdi
	movq	8(%r12), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$-1, 136(%rbx)
	movq	%rax, 128(%rbx)
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %r8
	movl	28(%rax), %esi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	salq	$4, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L23
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L17:
	movq	(%rbx), %rax
	movq	%rdi, 112(%rbx)
	xorl	%esi, %esi
	movq	(%rax), %rax
	movl	28(%rax), %edx
	salq	$4, %rdx
	call	memset@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L24
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	(%rax), %rdx
	movq	%rdx, 24(%rbx)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L13
.L24:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14571:
	.size	_ZN2v88internal8compiler18SimdScalarLoweringC2EPNS1_12MachineGraphEPNS0_9SignatureINS0_21MachineRepresentationEEE, .-_ZN2v88internal8compiler18SimdScalarLoweringC2EPNS1_12MachineGraphEPNS0_9SignatureINS0_21MachineRepresentationEEE
	.globl	_ZN2v88internal8compiler18SimdScalarLoweringC1EPNS1_12MachineGraphEPNS0_9SignatureINS0_21MachineRepresentationEEE
	.set	_ZN2v88internal8compiler18SimdScalarLoweringC1EPNS1_12MachineGraphEPNS0_9SignatureINS0_21MachineRepresentationEEE,_ZN2v88internal8compiler18SimdScalarLoweringC2EPNS1_12MachineGraphEPNS0_9SignatureINS0_21MachineRepresentationEEE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering15MachineTypeFromENS2_8SimdTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering15MachineTypeFromENS2_8SimdTypeE
	.type	_ZN2v88internal8compiler18SimdScalarLowering15MachineTypeFromENS2_8SimdTypeE, @function
_ZN2v88internal8compiler18SimdScalarLowering15MachineTypeFromENS2_8SimdTypeE:
.LFB14574:
	.cfi_startproc
	endbr64
	movzbl	%sil, %edx
	cmpb	$5, %sil
	ja	.L26
	leaq	.L28(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering15MachineTypeFromENS2_8SimdTypeE,"a",@progbits
	.align 4
	.align 4
.L28:
	.long	.L33-.L28
	.long	.L32-.L28
	.long	.L31-.L28
	.long	.L30-.L28
	.long	.L29-.L28
	.long	.L27-.L28
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering15MachineTypeFromENS2_8SimdTypeE
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$515, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$514, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$1549, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$1548, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$1029, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$516, %eax
	ret
.L26:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE14574:
	.size	_ZN2v88internal8compiler18SimdScalarLowering15MachineTypeFromENS2_8SimdTypeE, .-_ZN2v88internal8compiler18SimdScalarLowering15MachineTypeFromENS2_8SimdTypeE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering14SetLoweredTypeEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering14SetLoweredTypeEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler18SimdScalarLowering14SetLoweredTypeEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler18SimdScalarLowering14SetLoweredTypeEPNS1_4NodeES4_:
.LFB14575:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movl	20(%rsi), %ecx
	movq	112(%rdi), %rdi
	andl	$16777215, %ecx
	movzwl	16(%rax), %eax
	salq	$4, %rcx
	addq	%rdi, %rcx
	cmpw	$681, %ax
	ja	.L36
	cmpw	$526, %ax
	jbe	.L56
	subw	$527, %ax
	cmpw	$154, %ax
	ja	.L36
	leaq	.L40(%rip), %rsi
	movzwl	%ax, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering14SetLoweredTypeEPNS1_4NodeES4_,"a",@progbits
	.align 4
	.align 4
.L40:
	.long	.L52-.L40
	.long	.L52-.L40
	.long	.L52-.L40
	.long	.L52-.L40
	.long	.L52-.L40
	.long	.L52-.L40
	.long	.L52-.L40
	.long	.L52-.L40
	.long	.L52-.L40
	.long	.L52-.L40
	.long	.L52-.L40
	.long	.L52-.L40
	.long	.L52-.L40
	.long	.L52-.L40
	.long	.L52-.L40
	.long	.L52-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L43-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L49-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L51-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L36-.L40
	.long	.L51-.L40
	.long	.L36-.L40
	.long	.L36-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.long	.L47-.L40
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering14SetLoweredTypeEPNS1_4NodeES4_
	.p2align 4,,10
	.p2align 3
.L56:
	cmpw	$512, %ax
	je	.L38
	jbe	.L57
.L36:
	movq	(%rdx), %rax
	movzwl	16(%rax), %eax
	subw	$530, %ax
	cmpw	$142, %ax
	ja	.L46
	leaq	.L48(%rip), %rsi
	movzwl	%ax, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering14SetLoweredTypeEPNS1_4NodeES4_
	.align 4
	.align 4
.L48:
	.long	.L47-.L48
	.long	.L47-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L52-.L48
	.long	.L52-.L48
	.long	.L52-.L48
	.long	.L52-.L48
	.long	.L52-.L48
	.long	.L52-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L52-.L48
	.long	.L49-.L48
	.long	.L49-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L52-.L48
	.long	.L49-.L48
	.long	.L49-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L47-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L51-.L48
	.long	.L51-.L48
	.long	.L46-.L48
	.long	.L47-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L49-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L49-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L47-.L48
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering14SetLoweredTypeEPNS1_4NodeES4_
	.p2align 4,,10
	.p2align 3
.L57:
	cmpw	$16, %ax
	je	.L47
	subl	$49, %eax
	cmpw	$1, %ax
	ja	.L36
.L47:
	movb	$3, 8(%rcx)
	ret
.L52:
	movb	$1, 8(%rcx)
	ret
.L49:
	movb	$4, 8(%rcx)
	ret
.L51:
	movb	$5, 8(%rcx)
	ret
.L43:
	movb	$2, 8(%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	movb	$0, 8(%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movzbl	8(%rdi,%rax), %eax
	movb	%al, 8(%rcx)
	ret
	.cfi_endproc
.LFE14575:
	.size	_ZN2v88internal8compiler18SimdScalarLowering14SetLoweredTypeEPNS1_4NodeES4_, .-_ZN2v88internal8compiler18SimdScalarLowering14SetLoweredTypeEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering30GetParameterCountAfterLoweringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering30GetParameterCountAfterLoweringEv
	.type	_ZN2v88internal8compiler18SimdScalarLowering30GetParameterCountAfterLoweringEv, @function
_ZN2v88internal8compiler18SimdScalarLowering30GetParameterCountAfterLoweringEv:
.LFB14577:
	.cfi_startproc
	endbr64
	movl	136(%rdi), %eax
	cmpl	$-1, %eax
	je	.L97
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	movq	120(%rdi), %rcx
	movq	8(%rcx), %rdx
	movl	%edx, %eax
	testl	%edx, %edx
	jle	.L60
	leal	-1(%rdx), %eax
	movq	(%rcx), %rsi
	movq	16(%rcx), %r8
	cmpl	$14, %eax
	jbe	.L81
	movl	%edx, %ecx
	pxor	%xmm1, %xmm1
	pxor	%xmm5, %xmm5
	addq	%r8, %rsi
	shrl	$4, %ecx
	movdqa	.LC1(%rip), %xmm7
	movq	%rsi, %rax
	pxor	%xmm4, %xmm4
	salq	$4, %rcx
	movdqa	.LC2(%rip), %xmm6
	addq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L63:
	movdqu	(%rax), %xmm0
	movdqa	%xmm5, %xmm3
	addq	$16, %rax
	pcmpeqb	%xmm7, %xmm0
	pand	%xmm6, %xmm0
	pcmpgtb	%xmm0, %xmm3
	movdqa	%xmm0, %xmm2
	punpcklbw	%xmm3, %xmm2
	punpckhbw	%xmm3, %xmm0
	movdqa	%xmm4, %xmm3
	pcmpgtw	%xmm2, %xmm3
	movdqa	%xmm2, %xmm8
	punpcklwd	%xmm3, %xmm8
	punpckhwd	%xmm3, %xmm2
	movdqa	%xmm0, %xmm3
	paddd	%xmm8, %xmm1
	paddd	%xmm2, %xmm1
	movdqa	%xmm4, %xmm2
	pcmpgtw	%xmm0, %xmm2
	punpcklwd	%xmm2, %xmm3
	punpckhwd	%xmm2, %xmm0
	paddd	%xmm3, %xmm1
	paddd	%xmm0, %xmm1
	cmpq	%rcx, %rax
	jne	.L63
	movdqa	%xmm1, %xmm0
	movl	%edx, %ecx
	psrldq	$8, %xmm0
	andl	$-16, %ecx
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	addl	%edx, %eax
	cmpl	%ecx, %edx
	je	.L60
.L61:
	movslq	%ecx, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rsi,%r8)
	leal	1(%rcx), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L60
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rsi,%r8)
	leal	2(%rcx), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L60
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rsi,%r8)
	leal	3(%rcx), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L60
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rsi,%r8)
	leal	4(%rcx), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L60
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rsi,%r8)
	leal	5(%rcx), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L60
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rsi,%r8)
	leal	6(%rcx), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L60
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rsi,%r8)
	leal	7(%rcx), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L60
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rsi,%r8)
	leal	8(%rcx), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L60
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rsi,%r8)
	leal	9(%rcx), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L60
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rsi,%r8)
	leal	10(%rcx), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L60
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rsi,%r8)
	leal	11(%rcx), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L60
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rsi,%r8)
	leal	12(%rcx), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L60
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rsi,%r8)
	leal	13(%rcx), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L60
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rsi,%r8)
	cmove	%r9d, %eax
	addl	$14, %ecx
	cmpl	%ecx, %edx
	jle	.L60
	movslq	%ecx, %rcx
	leal	3(%rax), %edx
	cmpb	$14, (%rsi,%rcx)
	cmove	%edx, %eax
.L60:
	movl	%eax, 136(%rdi)
	ret
.L81:
	movl	%edx, %eax
	xorl	%ecx, %ecx
	addq	%r8, %rsi
	jmp	.L61
	.cfi_endproc
.LFE14577:
	.size	_ZN2v88internal8compiler18SimdScalarLowering30GetParameterCountAfterLoweringEv, .-_ZN2v88internal8compiler18SimdScalarLowering30GetParameterCountAfterLoweringEv
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering8NumLanesENS2_8SimdTypeE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering8NumLanesENS2_8SimdTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering8NumLanesENS2_8SimdTypeE
	.type	_ZN2v88internal8compiler18SimdScalarLowering8NumLanesENS2_8SimdTypeE, @function
_ZN2v88internal8compiler18SimdScalarLowering8NumLanesENS2_8SimdTypeE:
.LFB14579:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	andl	$253, %eax
	je	.L100
	cmpb	$1, %al
	jne	.L107
	movl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	cmpb	$4, %sil
	je	.L102
	cmpb	$5, %sil
	jne	.L108
	movl	$16, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	movl	$8, %eax
	ret
.L108:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE14579:
	.size	_ZN2v88internal8compiler18SimdScalarLowering8NumLanesENS2_8SimdTypeE, .-_ZN2v88internal8compiler18SimdScalarLowering8NumLanesENS2_8SimdTypeE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering13GetIndexNodesEPNS1_4NodeEPS4_NS2_8SimdTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering13GetIndexNodesEPNS1_4NodeEPS4_NS2_8SimdTypeE
	.type	_ZN2v88internal8compiler18SimdScalarLowering13GetIndexNodesEPNS1_4NodeEPS4_NS2_8SimdTypeE, @function
_ZN2v88internal8compiler18SimdScalarLowering13GetIndexNodesEPNS1_4NodeEPS4_NS2_8SimdTypeE:
.LFB14580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%ecx, %eax
	andl	$253, %eax
	je	.L113
	cmpb	$1, %al
	jne	.L119
	movl	$4, -84(%rbp)
	movq	%rdx, %rax
	movl	$4, -132(%rbp)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L113:
	movl	$8, -84(%rbp)
	movq	%rdx, %rax
	movl	$2, -132(%rbp)
.L110:
	movq	-104(%rbp), %rdx
	leaq	_ZN2v88internal8compiler18SimdScalarLowering12kLaneOffsetsE(%rip), %r12
	movl	$1, %r9d
	movq	%rdx, (%rax)
	movslq	-84(%rbp), %rax
	movq	%rax, %r15
	salq	$2, %rax
	movq	%rax, -120(%rbp)
	addq	%rax, %r12
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%r13, %rax
	movl	%r15d, %r13d
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L111:
	movl	(%r12), %eax
	movl	%r13d, %esi
	movl	%r9d, -88(%rbp)
	cltd
	idivl	-84(%rbp)
	movslq	%eax, %rbx
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -96(%rbp)
	movq	(%r15), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	movq	-128(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-112(%rbp), %rax
	movl	$2, %edx
	movhps	-96(%rbp), %xmm0
	leaq	(%rax,%rbx,8), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-88(%rbp), %r9d
	addq	-120(%rbp), %r12
	movq	%rax, (%rbx)
	addl	-84(%rbp), %r13d
	addl	$1, %r9d
	cmpl	-132(%rbp), %r9d
	jne	.L111
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L120
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	cmpb	$4, %cl
	je	.L115
	cmpb	$5, %cl
	jne	.L121
	movl	$1, -84(%rbp)
	movq	%rdx, %rax
	movl	$16, -132(%rbp)
	jmp	.L110
.L115:
	movl	$2, -84(%rbp)
	movq	%rdx, %rax
	movl	$8, -132(%rbp)
	jmp	.L110
.L121:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L120:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14580:
	.size	_ZN2v88internal8compiler18SimdScalarLowering13GetIndexNodesEPNS1_4NodeEPS4_NS2_8SimdTypeE, .-_ZN2v88internal8compiler18SimdScalarLowering13GetIndexNodesEPNS1_4NodeEPS4_NS2_8SimdTypeE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering12FixUpperBitsEPNS1_4NodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering12FixUpperBitsEPNS1_4NodeEi
	.type	_ZN2v88internal8compiler18SimdScalarLowering12FixUpperBitsEPNS1_4NodeEi, @function
_ZN2v88internal8compiler18SimdScalarLowering12FixUpperBitsEPNS1_4NodeEi:
.LFB14585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%rsi, -88(%rbp)
	movq	(%rdi), %rdi
	movl	%edx, %esi
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	leaq	-64(%rbp), %r12
	movq	%rax, -72(%rbp)
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -80(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShlEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-80(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -80(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-80(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-72(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L125
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L125:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14585:
	.size	_ZN2v88internal8compiler18SimdScalarLowering12FixUpperBitsEPNS1_4NodeEi, .-_ZN2v88internal8compiler18SimdScalarLowering12FixUpperBitsEPNS1_4NodeEi
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering4MaskEPNS1_4NodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering4MaskEPNS1_4NodeEi
	.type	_ZN2v88internal8compiler18SimdScalarLowering4MaskEPNS1_4NodeEi, @function
_ZN2v88internal8compiler18SimdScalarLowering4MaskEPNS1_4NodeEi:
.LFB14587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%rsi, -64(%rbp)
	movq	(%rdi), %rdi
	movl	%edx, %esi
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -56(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	-64(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-56(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L129
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L129:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14587:
	.size	_ZN2v88internal8compiler18SimdScalarLowering4MaskEPNS1_4NodeEi, .-_ZN2v88internal8compiler18SimdScalarLowering4MaskEPNS1_4NodeEi
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering13BuildF64TruncEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering13BuildF64TruncEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimdScalarLowering13BuildF64TruncEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimdScalarLowering13BuildF64TruncEPNS1_4NodeE:
.LFB14591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Float64RoundTruncateEv@PLT
	testb	%al, %al
	je	.L131
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Float64RoundTruncateEv@PLT
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rdx, %rsi
	movl	$1, %edx
	movq	%r12, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L130:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L141
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	call	_ZN2v88internal17ExternalReference14wasm_f64_truncEv@PLT
	xorl	%edx, %edx
	movl	$13, %esi
	movq	%rax, -104(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9StackSlotENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$13, %esi
	movq	%rax, -120(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder5StoreENS1_19StoreRepresentationE@PLT
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r14
	movq	(%rdi), %r15
	movq	8(%r15), %r13
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	movl	$5, %edx
	movq	%r14, %rsi
	movq	-120(%rbp), %xmm0
	movq	%rax, %xmm1
	movq	%r13, -64(%rbp)
	movq	%r15, %rdi
	movq	%r13, %xmm2
	leaq	-96(%rbp), %r13
	punpcklqdq	%xmm1, %xmm0
	movq	%r13, %rcx
	movaps	%xmm0, -96(%rbp)
	movq	%r12, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	leaq	-104(%rbp), %rsi
	movq	%rax, %r12
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder16ExternalConstantERKNS0_17ExternalReferenceE@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -128(%rbp)
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$31, %rax
	jbe	.L142
	leaq	32(%r14), %rax
	movq	%rax, 16(%rdi)
.L134:
	movq	-128(%rbp), %xmm0
	movq	%r12, 16(%r14)
	movhps	-120(%rbp), %xmm0
	movups	%xmm0, (%r14)
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, 24(%r14)
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %r12
	movq	16(%r12), %r15
	movq	24(%r12), %rax
	subq	%r15, %rax
	cmpq	$7, %rax
	jbe	.L143
	leaq	8(%r15), %rax
	movq	%rax, 16(%r12)
.L136:
	movl	$5, %eax
	movw	%ax, (%r15)
	movq	16(%r12), %rsi
	movq	24(%r12), %rax
	subq	%rsi, %rax
	cmpq	$23, %rax
	jbe	.L144
	leaq	24(%rsi), %rax
	movq	%rax, 16(%r12)
.L138:
	movdqa	.LC4(%rip), %xmm0
	movq	%r15, 16(%rsi)
	xorl	%edx, %edx
	movups	%xmm0, (%rsi)
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE@PLT
	movq	%rax, %rsi
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, -128(%rbp)
	movq	(%rdi), %r14
	movq	8(%r14), %r12
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movl	$1549, %esi
	movq	%rax, -136(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder4LoadENS0_11MachineTypeE@PLT
	movq	%r12, %xmm3
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-120(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$4, %edx
	movq	%r14, %rdi
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-128(%rbp), %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L144:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L143:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L136
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14591:
	.size	_ZN2v88internal8compiler18SimdScalarLowering13BuildF64TruncEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimdScalarLowering13BuildF64TruncEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering15DefaultLoweringEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering15DefaultLoweringEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimdScalarLowering15DefaultLoweringEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimdScalarLowering15DefaultLoweringEPNS1_4NodeE:
.LFB14598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rsi, %rdi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	call	_ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE@PLT
	movl	%eax, %edi
	subl	$1, %edi
	movl	%edi, -52(%rbp)
	js	.L156
	movl	%eax, %edx
	cltq
	movslq	%edi, %r13
	movq	112(%r15), %rcx
	negl	%edx
	leaq	32(%rbx), %rsi
	leaq	0(,%r13,8), %rdi
	xorl	%r11d, %r11d
	movslq	%edx, %rdx
	movq	%rsi, -72(%rbp)
	addq	%rdx, %rax
	movq	%rdi, -64(%rbp)
	leaq	(%rax,%rax,2), %rax
	salq	$3, %rax
	movq	%rax, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L154:
	movzbl	23(%rbx), %esi
	andl	$15, %esi
	cmpl	$15, %esi
	je	.L147
	movq	-64(%rbp), %r8
	addq	-72(%rbp), %r8
.L148:
	movq	(%r8), %r14
	movl	20(%r14), %edx
	andl	$16777215, %edx
	salq	$4, %rdx
	addq	%rcx, %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L149
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L155
	cmpl	$15, %esi
	je	.L151
	movq	%rbx, %rsi
	cmpq	%r14, %r12
	je	.L161
.L152:
	movq	-64(%rbp), %rdi
	movq	%r8, -88(%rbp)
	leaq	0(,%rdi,4), %rax
	subq	%rax, %rdi
	movq	%rdi, %rax
	movq	-96(%rbp), %rdi
	leaq	-24(%rdi,%rax), %rax
	movq	%r14, %rdi
	addq	%rax, %rsi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r12, (%r8)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	20(%r14), %edx
	movq	112(%r15), %rcx
	andl	$16777215, %edx
	salq	$4, %rdx
	addq	%rcx, %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L160
.L161:
	movl	$1, %r11d
.L155:
	cmpq	$0, 8(%rax)
	je	.L149
	cmpl	$1, 12(%rdx)
	jle	.L160
	movl	-52(%rbp), %edi
	xorl	%edx, %edx
	leal	1(%rdi), %r12d
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L170:
	movq	(%rax), %rax
.L153:
	leaq	1(%rdx), %r13
	movq	%rbx, %rdi
	addl	%r12d, %edx
	movq	(%rax,%r13,8), %rcx
	movq	(%r15), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movl	20(%r14), %eax
	movq	112(%r15), %rcx
	movq	%r13, %rdx
	leal	1(%r13), %esi
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rcx, %rax
	cmpl	%esi, 12(%rax)
	jg	.L170
.L160:
	movl	$1, %r11d
.L149:
	subl	$1, -52(%rbp)
	movl	-52(%rbp), %eax
	subq	$8, -64(%rbp)
	cmpl	$-1, %eax
	jne	.L154
.L145:
	addq	$56, %rsp
	movl	%r11d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %rdi
	movq	(%rax), %rax
	leaq	16(%rax,%rdi), %r8
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L151:
	cmpq	%r14, %r12
	je	.L161
	movq	-72(%rbp), %rax
	movq	(%rax), %rsi
	jmp	.L152
.L156:
	xorl	%r11d, %r11d
	jmp	.L145
	.cfi_endproc
.LFE14598:
	.size	_ZN2v88internal8compiler18SimdScalarLowering15DefaultLoweringEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimdScalarLowering15DefaultLoweringEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	.type	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i, @function
_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i:
.LFB14599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rax
	movl	20(%rsi), %r15d
	movslq	%ecx, %rsi
	salq	$3, %rsi
	movq	(%rax), %rax
	andl	$16777215, %r15d
	salq	$4, %r15
	addq	112(%rbx), %r15
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L178
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L173:
	movq	%rax, (%r15)
	testl	%ecx, %ecx
	jle	.L174
	leal	-1(%rcx), %eax
	xorl	%esi, %esi
	leaq	8(,%rax,8), %rdx
	.p2align 4,,10
	.p2align 3
.L175:
	movl	20(%r12), %eax
	movq	0(%r13,%rsi), %rdi
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%rbx), %rax
	movq	(%rax), %rax
	movq	%rdi, (%rax,%rsi)
	addq	$8, %rsi
	cmpq	%rsi, %rdx
	jne	.L175
.L174:
	movl	20(%r12), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%rbx), %rax
	movl	%ecx, 12(%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movl	%ecx, -36(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-36(%rbp), %ecx
	jmp	.L173
	.cfi_endproc
.LFE14599:
	.size	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i, .-_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering11LowerLoadOpEPNS1_4NodeENS2_8SimdTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering11LowerLoadOpEPNS1_4NodeENS2_8SimdTypeE
	.type	_ZN2v88internal8compiler18SimdScalarLowering11LowerLoadOpEPNS1_4NodeENS2_8SimdTypeE, @function
_ZN2v88internal8compiler18SimdScalarLowering11LowerLoadOpEPNS1_4NodeENS2_8SimdTypeE:
.LFB14581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movzbl	%dl, %ebx
	subq	$104, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movl	%eax, %r14d
	movq	(%r12), %rax
	movzwl	16(%rax), %eax
	cmpw	$494, %ax
	je	.L180
	cmpw	$502, %ax
	je	.L181
	cmpw	$429, %ax
	je	.L256
.L255:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L180:
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	cmpb	$5, %r15b
	ja	.L192
	leaq	.L194(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering11LowerLoadOpEPNS1_4NodeENS2_8SimdTypeE,"a",@progbits
	.align 4
	.align 4
.L194:
	.long	.L199-.L194
	.long	.L236-.L194
	.long	.L197-.L194
	.long	.L196-.L194
	.long	.L195-.L194
	.long	.L193-.L194
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering11LowerLoadOpEPNS1_4NodeENS2_8SimdTypeE
	.p2align 4,,10
	.p2align 3
.L256:
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	cmpb	$5, %r15b
	ja	.L183
	leaq	.L185(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering11LowerLoadOpEPNS1_4NodeENS2_8SimdTypeE
	.align 4
	.align 4
.L185:
	.long	.L190-.L185
	.long	.L235-.L185
	.long	.L188-.L185
	.long	.L187-.L185
	.long	.L186-.L185
	.long	.L184-.L185
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering11LowerLoadOpEPNS1_4NodeENS2_8SimdTypeE
	.p2align 4,,10
	.p2align 3
.L181:
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	cmpb	$5, %r15b
	ja	.L200
	leaq	.L202(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering11LowerLoadOpEPNS1_4NodeENS2_8SimdTypeE
	.align 4
	.align 4
.L202:
	.long	.L207-.L202
	.long	.L237-.L202
	.long	.L205-.L202
	.long	.L204-.L202
	.long	.L203-.L202
	.long	.L201-.L202
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering11LowerLoadOpEPNS1_4NodeENS2_8SimdTypeE
	.p2align 4,,10
	.p2align 3
.L237:
	movl	$6, %edx
	movl	$12, %ecx
	.p2align 4,,10
	.p2align 3
.L206:
	xorl	%eax, %eax
	movb	%cl, %al
	movb	%dl, %ah
	movl	%eax, %esi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13ProtectedLoadENS0_11MachineTypeE@PLT
	movq	%rax, %rbx
.L191:
	cmpb	$14, %r14b
	jne	.L208
.L261:
	movq	32(%r12), %rax
	leaq	32(%r12), %r14
	movq	%rax, -104(%rbp)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L209
	leaq	40(%r12), %rax
.L210:
	movq	(%rax), %r10
	movl	%r15d, %eax
	andl	$253, %eax
	je	.L238
	cmpb	$1, %al
	je	.L239
	cmpb	$4, %r15b
	je	.L240
	cmpb	$5, %r15b
	jne	.L255
	movl	$16, -132(%rbp)
	movl	$128, %r8d
	.p2align 4,,10
	.p2align 3
.L211:
	movq	0(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %r8
	ja	.L257
	leaq	(%r8,%rdx), %rax
	movq	%rax, 16(%rdi)
.L213:
	movl	%r15d, %ecx
	movq	%r13, %rdi
	movq	%r10, %rsi
	movq	%r8, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal8compiler18SimdScalarLowering13GetIndexNodesEPNS1_4NodeEPS4_NS2_8SimdTypeE
	movq	0(%r13), %rax
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rdx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	%rax, %rcx
	movq	%rax, -112(%rbp)
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %r8
	ja	.L258
	addq	-112(%rbp), %r8
	movq	%rcx, %rax
	movq	%r8, 16(%rdi)
.L215:
	movq	%r12, (%rax)
	movzbl	23(%r12), %eax
	movq	(%rdx), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L216
	movq	8(%r14), %rdi
	movq	%r12, %r8
	cmpq	%rdi, %r15
	je	.L217
	leaq	8(%r14), %rax
	movq	%r12, %rsi
.L218:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L219
	movq	%rdx, -144(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-144(%rbp), %rdx
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rsi
.L219:
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L259
	movq	%r15, %rdi
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %rdx
	movq	(%rax), %r8
.L217:
	movq	%rbx, %rsi
	movq	%r8, %rdi
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movzbl	23(%r12), %eax
	movq	-120(%rbp), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L221
	cmpl	$2, %eax
	jbe	.L223
	movq	16(%r14), %rax
	addq	$24, %r14
.L225:
	movq	(%r14), %rcx
	movl	-132(%rbp), %esi
	leaq	-96(%rbp), %r14
	movq	%r12, -144(%rbp)
	movq	%rcx, -120(%rbp)
	leal	-1(%rsi), %ecx
	subl	$2, %esi
	movslq	%ecx, %rcx
	salq	$3, %rsi
	salq	$3, %rcx
	leaq	(%rdx,%rcx), %r15
	subq	$8, %rcx
	subq	%rsi, %rcx
	movq	%r14, %rsi
	movq	%r15, %r12
	movq	%r13, %r14
	addq	%rdx, %rcx
	movq	%rsi, %r13
	movq	%rcx, -128(%rbp)
	movq	-112(%rbp), %rcx
	subq	%rdx, %rcx
	movq	%rcx, %r15
	.p2align 4,,10
	.p2align 3
.L226:
	movq	(%r14), %rdx
	movq	-104(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	movq	(%rdx), %rdi
	movq	(%r12), %rdx
	movq	%rax, -80(%rbp)
	movq	-120(%rbp), %rax
	movq	%rcx, -96(%rbp)
	movq	%r13, %rcx
	movq	%rdx, -88(%rbp)
	movl	$4, %edx
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r15,%r12)
	subq	$8, %r12
	cmpq	-128(%rbp), %r12
	jne	.L226
	movq	-112(%rbp), %rax
	movq	%r14, %r13
	movq	-144(%rbp), %r12
	movq	(%rax), %rsi
	movq	8(%rax), %r14
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L227
	movq	48(%rsi), %rdi
	leaq	48(%rsi), %rbx
	cmpq	%rdi, %r14
	je	.L230
.L229:
	leaq	-72(%rsi), %r15
	testq	%rdi, %rdi
	je	.L231
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L231:
	movq	%r14, (%rbx)
	testq	%r14, %r14
	je	.L230
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L230:
	movl	-132(%rbp), %ecx
	movq	-112(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
.L179:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L260
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movl	$6, %edx
	movl	$13, %ecx
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L235:
	movl	$6, %edx
	movl	$12, %ecx
	.p2align 4,,10
	.p2align 3
.L189:
	xorl	%eax, %eax
	movb	%cl, %al
	movb	%dl, %ah
	movl	%eax, %esi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder4LoadENS0_11MachineTypeE@PLT
	movq	%rax, %rbx
	cmpb	$14, %r14b
	je	.L261
.L208:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering15DefaultLoweringEPNS1_4NodeE
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L190:
	movl	$6, %edx
	movl	$13, %ecx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L236:
	movl	$6, %edx
	movl	$12, %ecx
	.p2align 4,,10
	.p2align 3
.L198:
	xorl	%eax, %eax
	movb	%cl, %al
	movb	%dl, %ah
	movl	%eax, %esi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13UnalignedLoadENS0_11MachineTypeE@PLT
	movq	%rax, %rbx
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L199:
	movl	$6, %edx
	movl	$13, %ecx
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L223:
	movl	-132(%rbp), %eax
	leaq	8(%rdx), %r15
	leaq	-96(%rbp), %r14
	movq	%r12, -128(%rbp)
	movq	%r13, %r12
	movq	%r14, %r13
	movq	%r15, %r14
	subl	$2, %eax
	leaq	16(%rdx,%rax,8), %rax
	movq	%rax, -120(%rbp)
	movq	-112(%rbp), %rax
	subq	%rdx, %rax
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L232:
	movq	(%r12), %rax
	movq	-104(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	(%rax), %rdi
	movq	(%r14), %rax
	movq	%rsi, -96(%rbp)
	movq	%rbx, %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r15,%r14)
	addq	$8, %r14
	cmpq	%r14, -120(%rbp)
	jne	.L232
	movq	%r12, %r13
	movq	-128(%rbp), %r12
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L238:
	movl	$2, -132(%rbp)
	movl	$16, %r8d
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L209:
	movq	-104(%rbp), %rsi
	movq	16(%rsi), %rcx
	leaq	24(%rsi), %rax
	movq	%rcx, -104(%rbp)
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L221:
	movq	32(%r12), %r14
	cmpl	$2, 8(%r14)
	jle	.L223
	movq	32(%r14), %rax
	addq	$40, %r14
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L216:
	movq	32(%r12), %rsi
	movq	%r12, %r8
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r15
	je	.L217
	leaq	24(%rsi), %rax
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L259:
	movq	-112(%rbp), %rax
	movq	(%rax), %r8
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L239:
	movl	$4, -132(%rbp)
	movl	$32, %r8d
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L227:
	movq	32(%rsi), %rsi
	movq	32(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L230
	leaq	32(%rsi), %rbx
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L258:
	movq	%r8, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %rdx
	movq	%rax, -112(%rbp)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L257:
	movq	%r8, %rsi
	movq	%r10, -120(%rbp)
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %r8
	movq	-120(%rbp), %r10
	movq	%rax, %rdx
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L197:
	movl	$4, %edx
	movl	$5, %ecx
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L196:
	movl	$2, %edx
	movl	$4, %ecx
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L195:
	movl	$2, %edx
	movl	$3, %ecx
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L184:
	movl	$2, %edx
	movl	$2, %ecx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$4, %edx
	movl	$5, %ecx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$2, %edx
	movl	$4, %ecx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L205:
	movl	$4, %edx
	movl	$5, %ecx
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L204:
	movl	$2, %edx
	movl	$4, %ecx
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L203:
	movl	$2, %edx
	movl	$3, %ecx
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$2, %edx
	movl	$3, %ecx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L201:
	movl	$2, %edx
	movl	$2, %ecx
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L193:
	movl	$2, %edx
	movl	$2, %ecx
	jmp	.L198
.L200:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	jmp	.L206
.L183:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	jmp	.L189
.L192:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L240:
	movl	$8, -132(%rbp)
	movl	$64, %r8d
	jmp	.L211
.L260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14581:
	.size	_ZN2v88internal8compiler18SimdScalarLowering11LowerLoadOpEPNS1_4NodeENS2_8SimdTypeE, .-_ZN2v88internal8compiler18SimdScalarLowering11LowerLoadOpEPNS1_4NodeENS2_8SimdTypeE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering14HasReplacementEmPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering14HasReplacementEmPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimdScalarLowering14HasReplacementEmPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimdScalarLowering14HasReplacementEmPNS1_4NodeE:
.LFB14600:
	.cfi_startproc
	endbr64
	movl	20(%rdx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%rdi), %rax
	movq	(%rax), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L262
	cmpq	$0, (%rdx,%rsi,8)
	setne	%al
.L262:
	ret
	.cfi_endproc
.LFE14600:
	.size	_ZN2v88internal8compiler18SimdScalarLowering14HasReplacementEmPNS1_4NodeE, .-_ZN2v88internal8compiler18SimdScalarLowering14HasReplacementEmPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering15ReplacementTypeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering15ReplacementTypeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimdScalarLowering15ReplacementTypeEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimdScalarLowering15ReplacementTypeEPNS1_4NodeE:
.LFB14601:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%rdi), %rax
	movzbl	8(%rax), %eax
	ret
	.cfi_endproc
.LFE14601:
	.size	_ZN2v88internal8compiler18SimdScalarLowering15ReplacementTypeEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimdScalarLowering15ReplacementTypeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering15GetReplacementsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering15GetReplacementsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimdScalarLowering15GetReplacementsEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimdScalarLowering15GetReplacementsEPNS1_4NodeE:
.LFB14602:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE14602:
	.size	_ZN2v88internal8compiler18SimdScalarLowering15GetReplacementsEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimdScalarLowering15GetReplacementsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering16ReplacementCountEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering16ReplacementCountEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimdScalarLowering16ReplacementCountEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimdScalarLowering16ReplacementCountEPNS1_4NodeE:
.LFB14603:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%rdi), %rax
	movl	12(%rax), %eax
	ret
	.cfi_endproc
.LFE14603:
	.size	_ZN2v88internal8compiler18SimdScalarLowering16ReplacementCountEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimdScalarLowering16ReplacementCountEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering14Int32ToFloat32EPPNS1_4NodeES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering14Int32ToFloat32EPPNS1_4NodeES5_
	.type	_ZN2v88internal8compiler18SimdScalarLowering14Int32ToFloat32EPPNS1_4NodeES5_, @function
_ZN2v88internal8compiler18SimdScalarLowering14Int32ToFloat32EPPNS1_4NodeES5_:
.LFB14604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.L272:
	movq	0(%r13,%rbx), %r12
	testq	%r12, %r12
	je	.L270
	movq	(%r9), %rax
	movq	%r9, -80(%rbp)
	movq	(%rax), %r10
	movq	16(%rax), %rdi
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21BitcastInt32ToFloat32Ev@PLT
	movq	-72(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-80(%rbp), %r9
	movq	%rax, (%r15,%rbx)
.L271:
	addq	$8, %rbx
	cmpq	$32, %rbx
	jne	.L272
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L276
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
	movq	$0, (%r15,%rbx)
	jmp	.L271
.L276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14604:
	.size	_ZN2v88internal8compiler18SimdScalarLowering14Int32ToFloat32EPPNS1_4NodeES5_, .-_ZN2v88internal8compiler18SimdScalarLowering14Int32ToFloat32EPPNS1_4NodeES5_
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering14Float32ToInt32EPPNS1_4NodeES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering14Float32ToInt32EPPNS1_4NodeES5_
	.type	_ZN2v88internal8compiler18SimdScalarLowering14Float32ToInt32EPPNS1_4NodeES5_, @function
_ZN2v88internal8compiler18SimdScalarLowering14Float32ToInt32EPPNS1_4NodeES5_:
.LFB14605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.L280:
	movq	0(%r13,%rbx), %r12
	testq	%r12, %r12
	je	.L278
	movq	(%r9), %rax
	movq	%r9, -80(%rbp)
	movq	(%rax), %r10
	movq	16(%rax), %rdi
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21BitcastFloat32ToInt32Ev@PLT
	movq	-72(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -64(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-80(%rbp), %r9
	movq	%rax, (%r15,%rbx)
.L279:
	addq	$8, %rbx
	cmpq	$32, %rbx
	jne	.L280
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L284
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	movq	$0, (%r15,%rbx)
	jmp	.L279
.L284:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14605:
	.size	_ZN2v88internal8compiler18SimdScalarLowering14Float32ToInt32EPPNS1_4NodeES5_, .-_ZN2v88internal8compiler18SimdScalarLowering14Float32ToInt32EPPNS1_4NodeES5_
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering21PreparePhiReplacementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering21PreparePhiReplacementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimdScalarLowering21PreparePhiReplacementEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimdScalarLowering21PreparePhiReplacementEPNS1_4NodeE:
.LFB14609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler19PhiRepresentationOfEPKNS1_8OperatorE@PLT
	cmpb	$14, %al
	je	.L342
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore_state
	movq	0(%r13), %rax
	movl	20(%rax), %eax
	movl	%eax, -68(%rbp)
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r15), %rax
	movzbl	8(%rax), %ebx
	movl	%ebx, %eax
	movb	%bl, -69(%rbp)
	andl	$253, %eax
	jne	.L343
	movq	$16, -88(%rbp)
	movl	$2, -92(%rbp)
.L287:
	movq	(%r15), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	-88(%rbp), %rax
	jb	.L344
	movq	-88(%rbp), %rax
	addq	%r12, %rax
	movq	%rax, 16(%rdi)
.L289:
	movl	-68(%rbp), %eax
	movq	%r12, -80(%rbp)
	movq	%r12, %r14
	addl	$1, %eax
	movslq	%eax, %rcx
	movl	%eax, -104(%rbp)
	salq	$3, %rcx
	leaq	-8(%rcx), %rax
	movq	%rax, -64(%rbp)
	movl	-92(%rbp), %eax
	subl	$1, %eax
	leaq	8(%r12,%rax,8), %rax
	movq	%r13, %r12
	movq	%rcx, %r13
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L293:
	movq	(%r15), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %r13
	ja	.L345
	leaq	0(%r13,%rax), %rdx
	movq	%rdx, 16(%rdi)
.L341:
	movq	%rax, (%r14)
	movq	-64(%rbp), %rbx
	xorl	%esi, %esi
	movq	%r12, %rdi
	addq	$8, %r14
	addq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, (%rbx)
	cmpq	-56(%rbp), %r14
	jne	.L293
	movl	-68(%rbp), %eax
	movq	%r12, %r13
	movq	-80(%rbp), %r12
	testl	%eax, %eax
	jle	.L298
	movl	-68(%rbp), %eax
	movl	-92(%rbp), %edx
	subl	$1, %eax
	leaq	8(,%rax,8), %rsi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L299:
	movq	(%r12), %rdi
	movq	128(%r15), %rcx
	movq	%rcx, (%rdi,%rax)
	movq	8(%r12), %rdi
	movq	%rcx, (%rdi,%rax)
	cmpl	$2, %edx
	je	.L297
	movq	16(%r12), %rdi
	movq	128(%r15), %rcx
	movq	%rcx, (%rdi,%rax)
	movq	24(%r12), %rdi
	movq	%rcx, (%rdi,%rax)
	cmpl	$4, %edx
	je	.L297
	movq	32(%r12), %rdi
	movq	128(%r15), %rcx
	movq	%rcx, (%rdi,%rax)
	movq	40(%r12), %rdi
	movq	%rcx, (%rdi,%rax)
	cmpl	$6, %edx
	je	.L297
	movq	48(%r12), %rdi
	movq	128(%r15), %rcx
	movq	%rcx, (%rdi,%rax)
	movq	56(%r12), %rdi
	movq	%rcx, (%rdi,%rax)
	cmpl	$8, %edx
	je	.L297
	movq	64(%r12), %rdi
	movq	128(%r15), %rcx
	movq	%rcx, (%rdi,%rax)
	movq	72(%r12), %rdi
	movq	%rcx, (%rdi,%rax)
	cmpl	$10, %edx
	je	.L297
	movq	80(%r12), %rdi
	movq	128(%r15), %rcx
	movq	%rcx, (%rdi,%rax)
	movq	88(%r12), %rdi
	movq	%rcx, (%rdi,%rax)
	cmpl	$12, %edx
	je	.L297
	movq	96(%r12), %rdi
	movq	128(%r15), %rcx
	movq	%rcx, (%rdi,%rax)
	movq	104(%r12), %rdi
	movq	%rcx, (%rdi,%rax)
	cmpl	$14, %edx
	je	.L297
	movq	112(%r12), %rdi
	movq	128(%r15), %rcx
	movq	%rcx, (%rdi,%rax)
	movq	120(%r12), %rdi
	movq	%rcx, (%rdi,%rax)
.L297:
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L299
.L298:
	movq	(%r15), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	%rax, %rbx
	movq	%rax, -80(%rbp)
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	-88(%rbp), %rax
	jb	.L346
	movq	-88(%rbp), %rax
	addq	-80(%rbp), %rax
	movq	%rax, 16(%rdi)
.L300:
	movq	%r12, %r14
	movq	%r12, -88(%rbp)
	movq	%r15, %r12
	leaq	.L303(%rip), %rbx
	movq	%r14, %r15
	movl	-104(%rbp), %r14d
	movq	%r13, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L309:
	movq	(%r12), %rax
	movq	(%r15), %rcx
	movq	(%rax), %r13
	movq	8(%rax), %rdi
	movzbl	-69(%rbp), %eax
	cmpb	$5, %al
	ja	.L301
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering21PreparePhiReplacementEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L303:
	.long	.L314-.L303
	.long	.L307-.L303
	.long	.L306-.L303
	.long	.L305-.L303
	.long	.L304-.L303
	.long	.L302-.L303
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering21PreparePhiReplacementEPNS1_4NodeE
.L343:
	cmpb	$1, %al
	je	.L311
	cmpb	$4, %bl
	je	.L312
	cmpb	$5, %bl
	jne	.L347
	movq	$128, -88(%rbp)
	movl	$16, -92(%rbp)
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L307:
	movl	$12, %esi
	.p2align 4,,10
	.p2align 3
.L308:
	movl	-68(%rbp), %edx
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	%r14d, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	movq	%r8, (%rax,%r15)
	addq	$8, %r15
	cmpq	%r15, -56(%rbp)
	jne	.L309
	movq	-104(%rbp), %r13
	movl	-92(%rbp), %ecx
	movq	%r12, %rdi
	movq	-80(%rbp), %rdx
	addq	$72, %rsp
	popq	%rbx
	movq	%r13, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movl	$13, %esi
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L302:
	movl	$2, %esi
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L304:
	movl	$3, %esi
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L305:
	movl	$4, %esi
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L306:
	movl	$5, %esi
	jmp	.L308
.L301:
	xorl	%esi, %esi
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L345:
	movq	%r13, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L341
.L311:
	movq	$32, -88(%rbp)
	movl	$4, -92(%rbp)
	jmp	.L287
.L312:
	movq	$64, -88(%rbp)
	movl	$8, -92(%rbp)
	jmp	.L287
.L346:
	movq	-88(%rbp), %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, -80(%rbp)
	jmp	.L300
.L344:
	movq	-88(%rbp), %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L289
.L347:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE14609:
	.size	_ZN2v88internal8compiler18SimdScalarLowering21PreparePhiReplacementEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimdScalarLowering21PreparePhiReplacementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering17SmallerIntToInt32IsEEvPPNS1_4NodeES6_,"axG",@progbits,_ZN2v88internal8compiler18SimdScalarLowering17SmallerIntToInt32IsEEvPPNS1_4NodeES6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler18SimdScalarLowering17SmallerIntToInt32IsEEvPPNS1_4NodeES6_
	.type	_ZN2v88internal8compiler18SimdScalarLowering17SmallerIntToInt32IsEEvPPNS1_4NodeES6_, @function
_ZN2v88internal8compiler18SimdScalarLowering17SmallerIntToInt32IsEEvPPNS1_4NodeES6_:
.LFB15531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -120(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.L351:
	movq	(%r12), %rdi
	xorl	%esi, %esi
	movq	%r14, %rbx
	salq	$4, %rbx
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	-88(%rbp), %rdx
	addq	-120(%rbp), %rbx
	xorl	%r9d, %r9d
	movq	%rax, (%rdx,%r14,8)
.L350:
	cmpq	$0, (%rbx)
	je	.L349
	movq	(%r12), %rdi
	movl	$65535, %esi
	movl	%r9d, -108(%rbp)
	movq	(%rdi), %r13
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	%rax, -104(%rbp)
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%r13, %rdi
	movq	%rax, %rsi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-108(%rbp), %r9d
	movq	(%r12), %rdi
	movq	%rax, -104(%rbp)
	movl	%r9d, %esi
	movq	(%rdi), %r13
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -96(%rbp)
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShlEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%r15, %rcx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, -104(%rbp)
	movq	(%r12), %rax
	movq	(%rcx,%r14,8), %rdx
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Word32OrEv@PLT
	movq	%r15, %rcx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-88(%rbp), %rcx
	movl	-108(%rbp), %r9d
	movq	%rax, (%rcx,%r14,8)
.L349:
	addl	$16, %r9d
	addq	$8, %rbx
	cmpl	$32, %r9d
	jne	.L350
	addq	$1, %r14
	cmpq	$4, %r14
	jne	.L351
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L356
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L356:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15531:
	.size	_ZN2v88internal8compiler18SimdScalarLowering17SmallerIntToInt32IsEEvPPNS1_4NodeES6_, .-_ZN2v88internal8compiler18SimdScalarLowering17SmallerIntToInt32IsEEvPPNS1_4NodeES6_
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering17SmallerIntToInt32IaEEvPPNS1_4NodeES6_,"axG",@progbits,_ZN2v88internal8compiler18SimdScalarLowering17SmallerIntToInt32IaEEvPPNS1_4NodeES6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler18SimdScalarLowering17SmallerIntToInt32IaEEvPPNS1_4NodeES6_
	.type	_ZN2v88internal8compiler18SimdScalarLowering17SmallerIntToInt32IaEEvPPNS1_4NodeES6_, @function
_ZN2v88internal8compiler18SimdScalarLowering17SmallerIntToInt32IaEEvPPNS1_4NodeES6_:
.LFB15532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%rsi, -120(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.L360:
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	xorl	%r15d, %r15d
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	-88(%rbp), %rdx
	movq	%r12, %r9
	salq	$5, %r9
	addq	-120(%rbp), %r9
	movq	%rax, (%rdx,%r12,8)
.L359:
	cmpq	$0, (%r9)
	je	.L358
	movq	(%rbx), %rdi
	movl	$255, %esi
	movq	%r9, -104(%rbp)
	movq	(%rdi), %r13
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	-104(%rbp), %r9
	movq	%rax, -96(%rbp)
	movq	(%r9), %rax
	movq	%r9, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%r13, %rdi
	movq	%rax, %rsi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%rbx), %rdi
	movl	%r15d, %esi
	movq	%rax, -104(%rbp)
	movq	(%rdi), %r13
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShlEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%r14, %rcx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, -104(%rbp)
	movq	(%rbx), %rax
	movq	(%rcx,%r12,8), %rdx
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Word32OrEv@PLT
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-88(%rbp), %rcx
	movq	-112(%rbp), %r9
	movq	%rax, (%rcx,%r12,8)
.L358:
	addl	$8, %r15d
	addq	$8, %r9
	cmpl	$32, %r15d
	jne	.L359
	addq	$1, %r12
	cmpq	$4, %r12
	jne	.L360
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L365
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L365:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15532:
	.size	_ZN2v88internal8compiler18SimdScalarLowering17SmallerIntToInt32IaEEvPPNS1_4NodeES6_, .-_ZN2v88internal8compiler18SimdScalarLowering17SmallerIntToInt32IaEEvPPNS1_4NodeES6_
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering17Int32ToSmallerIntIsEEvPPNS1_4NodeES6_,"axG",@progbits,_ZN2v88internal8compiler18SimdScalarLowering17Int32ToSmallerIntIsEEvPPNS1_4NodeES6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler18SimdScalarLowering17Int32ToSmallerIntIsEEvPPNS1_4NodeES6_
	.type	_ZN2v88internal8compiler18SimdScalarLowering17Int32ToSmallerIntIsEEvPPNS1_4NodeES6_, @function
_ZN2v88internal8compiler18SimdScalarLowering17Int32ToSmallerIntIsEEvPPNS1_4NodeES6_:
.LFB15533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder23SignExtendWord16ToInt32Ev@PLT
	movl	$0, -84(%rbp)
	movq	%r13, %r9
	movq	%rax, -120(%rbp)
.L369:
	xorl	%ebx, %ebx
	cmpq	$0, (%r12)
	je	.L376
.L367:
	movq	(%r14), %rdi
	movl	%ebx, %esi
	movq	%r9, -112(%rbp)
	sall	$4, %esi
	movq	(%rdi), %r13
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -96(%rbp)
	movq	(%r12), %rax
	movq	%rax, -104(%rbp)
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-120(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-112(%rbp), %r9
	movq	%rax, (%r9,%rbx,8)
	cmpq	$1, %rbx
	je	.L368
	movl	$1, %ebx
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L376:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%r9)
.L368:
	addl	$2, -84(%rbp)
	movl	-84(%rbp), %eax
	addq	$8, %r12
	addq	$16, %r9
	cmpl	$8, %eax
	jne	.L369
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L377
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L377:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15533:
	.size	_ZN2v88internal8compiler18SimdScalarLowering17Int32ToSmallerIntIsEEvPPNS1_4NodeES6_, .-_ZN2v88internal8compiler18SimdScalarLowering17Int32ToSmallerIntIsEEvPPNS1_4NodeES6_
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering17Int32ToSmallerIntIaEEvPPNS1_4NodeES6_,"axG",@progbits,_ZN2v88internal8compiler18SimdScalarLowering17Int32ToSmallerIntIaEEvPPNS1_4NodeES6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler18SimdScalarLowering17Int32ToSmallerIntIaEEvPPNS1_4NodeES6_
	.type	_ZN2v88internal8compiler18SimdScalarLowering17Int32ToSmallerIntIaEEvPPNS1_4NodeES6_, @function
_ZN2v88internal8compiler18SimdScalarLowering17Int32ToSmallerIntIaEEvPPNS1_4NodeES6_:
.LFB15534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -112(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22SignExtendWord8ToInt32Ev@PLT
	movq	$0, -88(%rbp)
	movq	%rax, -120(%rbp)
.L382:
	movq	-112(%rbp), %rax
	movq	-88(%rbp), %rdx
	cmpq	$0, (%rax,%rdx,8)
	je	.L387
	movq	-128(%rbp), %rax
	movq	%rdx, %r9
	xorl	%ebx, %ebx
	salq	$5, %r9
	leaq	(%rax,%r9), %r15
.L381:
	movq	(%r12), %rdi
	movl	%ebx, %esi
	addl	$8, %ebx
	addq	$8, %r15
	movq	(%rdi), %r13
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	-112(%rbp), %rax
	movq	(%rax,%rcx,8), %rax
	movq	%rax, -104(%rbp)
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-120(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -8(%r15)
	cmpl	$32, %ebx
	jne	.L381
	addq	$1, -88(%rbp)
	movq	-88(%rbp), %rax
	cmpq	$4, %rax
	jne	.L382
.L389:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L388
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L387:
	.cfi_restore_state
	movq	%rdx, %rax
	movq	-128(%rbp), %rdx
	pxor	%xmm0, %xmm0
	addq	$1, -88(%rbp)
	salq	$5, %rax
	movups	%xmm0, (%rdx,%rax)
	movups	%xmm0, 16(%rdx,%rax)
	movq	-88(%rbp), %rax
	cmpq	$4, %rax
	jne	.L382
	jmp	.L389
.L388:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15534:
	.size	_ZN2v88internal8compiler18SimdScalarLowering17Int32ToSmallerIntIaEEvPPNS1_4NodeES6_, .-_ZN2v88internal8compiler18SimdScalarLowering17Int32ToSmallerIntIaEEvPPNS1_4NodeES6_
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"unimplemented code"
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	.type	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE, @function
_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE:
.LFB14608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%rdi), %rax
	movq	(%rax), %r14
	cmpb	8(%rax), %dl
	jne	.L421
.L390:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L422
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	movl	%edx, %eax
	movq	%rdi, %r12
	movq	%rsi, %r13
	andl	$253, %eax
	je	.L411
	cmpb	$1, %al
	je	.L412
	cmpb	$4, %dl
	je	.L413
	cmpb	$5, %dl
	jne	.L401
	movl	$128, %esi
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L411:
	movl	$16, %esi
.L392:
	movq	(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	%rsi, %rax
	jb	.L423
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L394:
	cmpb	$3, %dl
	je	.L424
	cmpb	$1, %dl
	je	.L425
	cmpb	$4, %dl
	je	.L426
	cmpb	$5, %dl
	jne	.L401
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r12), %rax
	cmpb	$3, 8(%rax)
	jne	.L409
	movq	%r14, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r15, %r14
	call	_ZN2v88internal8compiler18SimdScalarLowering17Int32ToSmallerIntIaEEvPPNS1_4NodeES6_
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L412:
	movl	$32, %esi
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L426:
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r12), %rax
	movzbl	8(%rax), %eax
	cmpb	$3, %al
	je	.L427
	cmpb	$1, %al
	je	.L409
.L401:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L424:
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r12), %rax
	movzbl	8(%rax), %eax
	cmpb	$1, %al
	je	.L428
	cmpb	$4, %al
	je	.L429
	cmpb	$5, %al
	jne	.L401
	movq	%r14, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r15, %r14
	call	_ZN2v88internal8compiler18SimdScalarLowering17SmallerIntToInt32IaEEvPPNS1_4NodeES6_
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L425:
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r12), %rax
	movzbl	8(%rax), %eax
	cmpb	$3, %al
	je	.L430
	cmpb	$4, %al
	jne	.L401
.L409:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L423:
	movl	%edx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-72(%rbp), %edx
	movq	%rax, %r15
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L413:
	movl	$64, %esi
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L428:
	leaq	32(%r15), %r9
	movq	%r15, %rbx
	subq	%r15, %r14
.L399:
	movq	(%r14,%rbx), %r13
	testq	%r13, %r13
	je	.L397
	movq	(%r12), %rax
	movq	%r9, -80(%rbp)
	movq	(%rax), %r10
	movq	16(%rax), %rdi
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21BitcastFloat32ToInt32Ev@PLT
	movq	-72(%rbp), %r10
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -64(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-80(%rbp), %r9
	movq	%rax, (%rbx)
.L398:
	addq	$8, %rbx
	cmpq	%r9, %rbx
	jne	.L399
.L416:
	movq	%r15, %r14
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L397:
	movq	$0, (%rbx)
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L429:
	movq	%r14, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r15, %r14
	call	_ZN2v88internal8compiler18SimdScalarLowering17SmallerIntToInt32IsEEvPPNS1_4NodeES6_
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L430:
	leaq	32(%r15), %r9
	movq	%r15, %rbx
	subq	%r15, %r14
.L406:
	movq	(%r14,%rbx), %r13
	testq	%r13, %r13
	je	.L404
	movq	(%r12), %rax
	movq	%r9, -80(%rbp)
	movq	(%rax), %r10
	movq	16(%rax), %rdi
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21BitcastInt32ToFloat32Ev@PLT
	movq	-72(%rbp), %r10
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r13, -64(%rbp)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-80(%rbp), %r9
	movq	%rax, (%rbx)
.L405:
	addq	$8, %rbx
	cmpq	%rbx, %r9
	jne	.L406
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L404:
	movq	$0, (%rbx)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L427:
	movq	%r14, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r15, %r14
	call	_ZN2v88internal8compiler18SimdScalarLowering17Int32ToSmallerIntIsEEvPPNS1_4NodeES6_
	jmp	.L390
.L422:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14608:
	.size	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE, .-_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	.type	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb, @function
_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb:
.LFB14583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$88, %rsp
	movq	%rsi, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	movq	32(%rsi), %rsi
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L432
	movq	16(%rsi), %rsi
.L432:
	movl	%r15d, %edx
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, -88(%rbp)
	movq	-112(%rbp), %rax
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L434
	movq	-112(%rbp), %rax
	movq	32(%rax), %rax
	movq	%rax, -96(%rbp)
	leaq	24(%rax), %rbx
.L434:
	movq	(%rbx), %rsi
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, -96(%rbp)
	movl	%r15d, %eax
	andl	$253, %eax
	je	.L443
	cmpb	$1, %al
	jne	.L451
	movl	$4, -116(%rbp)
	movl	$32, %esi
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L443:
	movl	$2, -116(%rbp)
	movl	$16, %esi
.L435:
	movq	(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	%rsi, %rax
	jb	.L452
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L437:
	movq	%r15, -128(%rbp)
	testb	%r14b, %r14b
	je	.L438
	movl	-116(%rbp), %eax
	leaq	-80(%rbp), %r14
	movq	%r15, %rbx
	subl	$1, %eax
	leaq	8(%r15,%rax,8), %rax
	negq	%r15
	movq	%rax, -104(%rbp)
	movq	%r14, %rax
	movq	%r15, %r14
	movq	%r13, %r15
	movq	%r12, %r13
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L439:
	movq	0(%r13), %rax
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	(%rax), %rdi
	movq	-88(%rbp), %rax
	leaq	(%rax,%r14), %rdx
	movq	-96(%rbp), %rax
	movq	(%rdx,%rbx), %xmm0
	movl	$2, %edx
	addq	%r14, %rax
	movhps	(%rax,%rbx), %xmm0
	addq	$8, %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -8(%rbx)
	cmpq	%rbx, -104(%rbp)
	jne	.L439
.L450:
	movl	-116(%rbp), %ecx
	movq	-128(%rbp), %rdx
	movq	%r13, %rdi
	movq	-112(%rbp), %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L453
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	movl	-116(%rbp), %edx
	movq	-88(%rbp), %r11
	leaq	-80(%rbp), %r14
	movq	%r13, -88(%rbp)
	movq	-96(%rbp), %rbx
	movq	%r12, %r13
	sarl	%edx
	movq	%r11, %r12
	leal	-1(%rdx), %eax
	leaq	8(%r15,%rax,8), %rax
	movq	%rax, -104(%rbp)
	movslq	%edx, %rax
	salq	$3, %rax
	movq	%rax, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L441:
	movq	0(%r13), %rax
	movdqu	(%r12), %xmm1
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-88(%rbp), %rsi
	movl	$2, %edx
	addq	$16, %r12
	addq	$16, %rbx
	movq	(%rax), %rdi
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-88(%rbp), %rsi
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movq	%rax, (%r15)
	movq	0(%r13), %rax
	movl	$2, %edx
	movdqu	-16(%rbx), %xmm2
	movq	(%rax), %rdi
	movaps	%xmm2, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-96(%rbp), %rcx
	movq	%rax, (%rcx,%r15)
	addq	$8, %r15
	cmpq	-104(%rbp), %r15
	jne	.L441
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L451:
	cmpb	$4, %r15b
	je	.L445
	cmpb	$5, %r15b
	jne	.L454
	movl	$16, -116(%rbp)
	movl	$128, %esi
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L445:
	movl	$8, -116(%rbp)
	movl	$64, %esi
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L452:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L437
.L453:
	call	__stack_chk_fail@PLT
.L454:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE14583:
	.size	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb, .-_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering24LowerBinaryOpForSmallIntEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering24LowerBinaryOpForSmallIntEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	.type	_ZN2v88internal8compiler18SimdScalarLowering24LowerBinaryOpForSmallIntEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb, @function
_ZN2v88internal8compiler18SimdScalarLowering24LowerBinaryOpForSmallIntEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb:
.LFB14586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$104, %rsp
	movq	%rsi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	movq	32(%rsi), %rsi
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L456
	movq	16(%rsi), %rsi
.L456:
	movl	%r13d, %edx
	movq	%r12, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, -88(%rbp)
	movq	-120(%rbp), %rax
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L458
	movq	-120(%rbp), %rax
	movq	32(%rax), %rax
	movq	%rax, -96(%rbp)
	leaq	24(%rax), %rbx
.L458:
	movq	(%rbx), %rsi
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, -96(%rbp)
	movl	%r13d, %eax
	andl	$253, %eax
	je	.L468
	cmpb	$1, %al
	jne	.L476
	movl	$4, -124(%rbp)
	movl	$32, %esi
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L468:
	movl	$2, -124(%rbp)
	movl	$16, %esi
.L459:
	movq	(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r10
	movq	24(%rdi), %rax
	subq	%r10, %rax
	cmpq	%rsi, %rax
	jb	.L477
	addq	%r10, %rsi
	movq	%rsi, 16(%rdi)
.L461:
	cmpb	$4, %r13b
	movq	%r10, -136(%rbp)
	movl	-124(%rbp), %eax
	setne	%r13b
	movzbl	%r13b, %r13d
	leal	16(,%r13,8), %r13d
	testb	%r15b, %r15b
	je	.L463
	subl	$1, %eax
	movq	%r10, %rbx
	movl	%r13d, -112(%rbp)
	leaq	-80(%rbp), %r15
	leaq	8(%r10,%rax,8), %rax
	negq	%rbx
	movq	%rax, -104(%rbp)
	movq	%rbx, %r13
	movq	%r10, %rbx
	.p2align 4,,10
	.p2align 3
.L464:
	movq	(%r12), %rax
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	(%rax), %rdi
	movq	-88(%rbp), %rax
	leaq	(%rax,%r13), %rdx
	movq	-96(%rbp), %rax
	movq	(%rdx,%rbx), %xmm0
	movl	$2, %edx
	addq	%r13, %rax
	movhps	(%rax,%rbx), %xmm0
	addq	$8, %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-112(%rbp), %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering12FixUpperBitsEPNS1_4NodeEi
	movq	%rax, -8(%rbx)
	cmpq	-104(%rbp), %rbx
	jne	.L464
.L465:
	movl	-124(%rbp), %ecx
	movq	-136(%rbp), %rdx
	movq	%r12, %rdi
	movq	-120(%rbp), %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L478
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	sarl	%eax
	movq	-96(%rbp), %rbx
	movq	-88(%rbp), %r11
	leaq	-80(%rbp), %r15
	leal	-1(%rax), %edx
	cltq
	movl	%r13d, -88(%rbp)
	movq	%r10, %r13
	leaq	8(%r10,%rdx,8), %rcx
	salq	$3, %rax
	movq	%rcx, -112(%rbp)
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L466:
	movq	(%r12), %rax
	movdqu	(%r11), %xmm1
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r11, -96(%rbp)
	addq	$16, %rbx
	movq	(%rax), %rdi
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering12FixUpperBitsEPNS1_4NodeEi
	movq	%r15, %rcx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%rax, 0(%r13)
	movq	(%r12), %rax
	movq	%r14, %rsi
	movdqu	-16(%rbx), %xmm2
	movq	(%rax), %rdi
	movaps	%xmm2, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-88(%rbp), %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering12FixUpperBitsEPNS1_4NodeEi
	movq	-104(%rbp), %rcx
	movq	-96(%rbp), %r11
	movq	%rax, (%rcx,%r13)
	addq	$16, %r11
	addq	$8, %r13
	cmpq	-112(%rbp), %r13
	jne	.L466
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L476:
	cmpb	$4, %r13b
	je	.L470
	cmpb	$5, %r13b
	jne	.L479
	movl	$16, -124(%rbp)
	movl	$128, %esi
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L470:
	movl	$8, -124(%rbp)
	movl	$64, %esi
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L477:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r10
	jmp	.L461
.L478:
	call	__stack_chk_fail@PLT
.L479:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE14586:
	.size	_ZN2v88internal8compiler18SimdScalarLowering24LowerBinaryOpForSmallIntEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb, .-_ZN2v88internal8compiler18SimdScalarLowering24LowerBinaryOpForSmallIntEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering19LowerConvertFromIntEPNS1_4NodeENS2_8SimdTypeES5_bi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering19LowerConvertFromIntEPNS1_4NodeENS2_8SimdTypeES5_bi
	.type	_ZN2v88internal8compiler18SimdScalarLowering19LowerConvertFromIntEPNS1_4NodeENS2_8SimdTypeES5_bi, @function
_ZN2v88internal8compiler18SimdScalarLowering19LowerConvertFromIntEPNS1_4NodeENS2_8SimdTypeES5_bi:
.LFB14593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$104, %rsp
	movq	%rsi, -136(%rbp)
	movl	%r8d, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	movq	32(%rsi), %rsi
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L481
	movq	16(%rsi), %rsi
.L481:
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	%r9d, -96(%rbp)
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	cmpb	$4, %r13b
	movl	$65535, %r13d
	movslq	-96(%rbp), %r9
	movq	%rax, %r15
	movl	$255, %eax
	cmovne	%eax, %r13d
	movl	%ebx, %eax
	andl	$253, %eax
	je	.L493
	cmpb	$1, %al
	jne	.L543
	movl	$4, -100(%rbp)
	movl	$32, %esi
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L493:
	movl	$2, -100(%rbp)
	movl	$16, %esi
.L483:
	movq	(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	%rsi, %rax
	jb	.L544
	addq	%rbx, %rsi
	movq	%rsi, 16(%rdi)
.L485:
	cmpb	$0, -88(%rbp)
	movq	%rbx, %r14
	je	.L545
	movslq	%r9d, %rdx
	leaq	16(%rbx), %rcx
	leaq	(%r15,%rdx,8), %rax
	cmpq	%rcx, %rax
	leaq	16(%r15,%rdx,8), %rcx
	setnb	%sil
	cmpq	%rcx, %rbx
	setnb	%cl
	orb	%cl, %sil
	je	.L489
	movl	-100(%rbp), %esi
	leal	-1(%rsi), %ecx
	cmpl	$3, %ecx
	jbe	.L489
	movdqu	(%rax), %xmm1
	movl	%esi, %edx
	shrl	%edx
	movups	%xmm1, (%rbx)
	movdqu	16(%rax), %xmm2
	movups	%xmm2, 16(%rbx)
	cmpl	$4, %esi
	je	.L487
	movdqu	32(%rax), %xmm3
	movups	%xmm3, 32(%rbx)
	cmpl	$3, %edx
	je	.L487
	movdqu	48(%rax), %xmm4
	movups	%xmm4, 48(%rbx)
	cmpl	$4, %edx
	je	.L487
	movdqu	64(%rax), %xmm5
	movups	%xmm5, 64(%rbx)
	cmpl	$5, %edx
	je	.L487
	movdqu	80(%rax), %xmm6
	movups	%xmm6, 80(%rbx)
	cmpl	$6, %edx
	je	.L487
	movdqu	96(%rax), %xmm7
	movups	%xmm7, 96(%rbx)
	cmpl	$7, %edx
	je	.L487
	movdqu	112(%rax), %xmm7
	movups	%xmm7, 112(%rbx)
	.p2align 4,,10
	.p2align 3
.L487:
	movl	-100(%rbp), %ecx
	movq	-136(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L546
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	cmpb	$4, %bl
	je	.L495
	cmpb	$5, %bl
	jne	.L547
	movl	$16, -100(%rbp)
	movl	$128, %esi
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L545:
	movl	-100(%rbp), %eax
	subl	$1, %eax
	leaq	8(%rbx,%rax,8), %rax
	movq	%rax, -120(%rbp)
	leaq	(%r15,%r9,8), %rax
	subq	%rbx, %rax
	movq	%rax, -112(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L488:
	movq	-112(%rbp), %rax
	movq	(%r12), %rdi
	movl	%r13d, %esi
	movq	(%rax,%rbx), %rax
	movq	(%rdi), %r15
	addq	$8, %rbx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -88(%rbp)
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movq	-128(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -8(%rbx)
	cmpq	-120(%rbp), %rbx
	jne	.L488
	jmp	.L487
.L495:
	movl	$8, -100(%rbp)
	movl	$64, %esi
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L489:
	movq	(%r15,%rdx,8), %rax
	movl	-100(%rbp), %edx
	movq	%rax, (%rbx)
	leal	1(%r9), %eax
	cltq
	movq	(%r15,%rax,8), %rax
	movq	%rax, 8(%rbx)
	cmpl	$2, %edx
	je	.L487
	leal	2(%r9), %eax
	cltq
	movq	(%r15,%rax,8), %rax
	movq	%rax, 16(%rbx)
	leal	3(%r9), %eax
	cltq
	movq	(%r15,%rax,8), %rax
	movq	%rax, 24(%rbx)
	cmpl	$4, %edx
	je	.L487
	leal	4(%r9), %eax
	cltq
	movq	(%r15,%rax,8), %rax
	movq	%rax, 32(%rbx)
	leal	5(%r9), %eax
	cltq
	movq	(%r15,%rax,8), %rax
	movq	%rax, 40(%rbx)
	cmpl	$6, %edx
	je	.L487
	leal	6(%r9), %eax
	cltq
	movq	(%r15,%rax,8), %rax
	movq	%rax, 48(%rbx)
	leal	7(%r9), %eax
	cltq
	movq	(%r15,%rax,8), %rax
	movq	%rax, 56(%rbx)
	cmpl	$8, %edx
	je	.L487
	leal	8(%r9), %eax
	cltq
	movq	(%r15,%rax,8), %rax
	movq	%rax, 64(%rbx)
	leal	9(%r9), %eax
	cltq
	movq	(%r15,%rax,8), %rax
	movq	%rax, 72(%rbx)
	cmpl	$10, %edx
	je	.L487
	leal	10(%r9), %eax
	cltq
	movq	(%r15,%rax,8), %rax
	movq	%rax, 80(%rbx)
	leal	11(%r9), %eax
	cltq
	movq	(%r15,%rax,8), %rax
	movq	%rax, 88(%rbx)
	cmpl	$12, %edx
	je	.L487
	leal	12(%r9), %eax
	cltq
	movq	(%r15,%rax,8), %rax
	movq	%rax, 96(%rbx)
	leal	13(%r9), %eax
	cltq
	movq	(%r15,%rax,8), %rax
	movq	%rax, 104(%rbx)
	cmpl	$14, %edx
	je	.L487
	leal	14(%r9), %eax
	cltq
	movq	(%r15,%rax,8), %rax
	movq	%rax, 112(%rbx)
	leal	15(%r9), %eax
	cltq
	movq	(%r15,%rax,8), %rax
	movq	%rax, 120(%rbx)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L544:
	movl	%r9d, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movslq	-96(%rbp), %r9
	movq	%rax, %rbx
	jmp	.L485
.L547:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L546:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14593:
	.size	_ZN2v88internal8compiler18SimdScalarLowering19LowerConvertFromIntEPNS1_4NodeENS2_8SimdTypeES5_bi, .-_ZN2v88internal8compiler18SimdScalarLowering19LowerConvertFromIntEPNS1_4NodeENS2_8SimdTypeES5_bi
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering12LowerShiftOpEPNS1_4NodeENS2_8SimdTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering12LowerShiftOpEPNS1_4NodeENS2_8SimdTypeE
	.type	_ZN2v88internal8compiler18SimdScalarLowering12LowerShiftOpEPNS1_4NodeENS2_8SimdTypeE, @function
_ZN2v88internal8compiler18SimdScalarLowering12LowerShiftOpEPNS1_4NodeENS2_8SimdTypeE:
.LFB14595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$120, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	44(%rdx), %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	leaq	32(%r12), %rcx
	movq	%rax, -112(%rbp)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L549
	movq	32(%r12), %rax
	leaq	16(%rax), %rcx
.L549:
	movq	(%rcx), %rsi
	movl	%ebx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, -96(%rbp)
	movl	%ebx, %eax
	andl	$253, %eax
	je	.L566
	cmpb	$1, %al
	jne	.L579
	movl	$4, -148(%rbp)
	movl	$32, %esi
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L566:
	movl	$2, -148(%rbp)
	movl	$16, %esi
.L550:
	movq	(%r14), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rsi, %rax
	jb	.L580
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L552:
	movl	-148(%rbp), %eax
	movq	%r13, %r15
	leaq	.L556(%rip), %rbx
	subl	$1, %eax
	leaq	8(%r13,%rax,8), %rax
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L564:
	movq	-96(%rbp), %rax
	subq	%r13, %rax
	movq	(%rax,%r15), %xmm0
	movq	%xmm0, (%r15)
	movq	(%r12), %rax
	movzwl	16(%rax), %eax
	cmpw	$607, %ax
	ja	.L553
	cmpw	$575, %ax
	jbe	.L554
	subw	$576, %ax
	cmpw	$31, %ax
	ja	.L554
	movzwl	%ax, %eax
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering12LowerShiftOpEPNS1_4NodeENS2_8SimdTypeE,"a",@progbits
	.align 4
	.align 4
.L556:
	.long	.L559-.L556
	.long	.L555-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L571-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L554-.L556
	.long	.L557-.L556
	.long	.L555-.L556
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering12LowerShiftOpEPNS1_4NodeENS2_8SimdTypeE
.L571:
	leaq	-80(%rbp), %rcx
.L558:
	movq	(%r14), %rax
	movq	%rcx, -136(%rbp)
	movq	%xmm0, -128(%rbp)
	movq	(%rax), %r11
	movq	16(%rax), %rdi
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShrEv@PLT
	movq	-128(%rbp), %xmm0
	movq	-136(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
.L577:
	movq	-88(%rbp), %r11
	movl	$2, %edx
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r15)
.L563:
	addq	$8, %r15
	cmpq	-104(%rbp), %r15
	jne	.L564
	movl	-148(%rbp), %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L581
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L555:
	.cfi_restore_state
	movq	(%r14), %rax
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	(%rax), %r11
	movq	16(%rax), %rdi
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
.L578:
	movdqa	-128(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movaps	%xmm0, -80(%rbp)
	jmp	.L577
.L559:
	movq	(%r14), %rax
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	(%rax), %r11
	movq	16(%rax), %rdi
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShlEv@PLT
	jmp	.L578
.L557:
	movq	(%r14), %rax
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	(%rax), %r11
	movq	16(%rax), %rdi
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShlEv@PLT
	movq	-88(%rbp), %r11
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movdqa	-128(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r11, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$16, %edx
	movq	%r14, %rdi
	movq	%rax, (%r15)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering12FixUpperBitsEPNS1_4NodeEi
	movq	%rax, (%r15)
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L553:
	cmpw	$640, %ax
	je	.L560
	jbe	.L582
	cmpw	$641, %ax
	je	.L555
	movq	%xmm0, -88(%rbp)
	cmpw	$658, %ax
	jne	.L554
	movq	(%r14), %rdi
	movl	$255, %esi
	movq	(%rdi), %r11
	movq	%r11, -144(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -128(%rbp)
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movq	-144(%rbp), %r11
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%rcx, -136(%rbp)
	movq	%r11, %rdi
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r15)
	movq	%rax, -88(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %r11
	movq	16(%rax), %rdi
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShrEv@PLT
	movq	-88(%rbp), %xmm0
	movq	-128(%rbp), %r11
	xorl	%r8d, %r8d
	movq	-136(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-112(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r15)
	jmp	.L563
.L582:
	cmpw	$625, %ax
	jne	.L554
	movq	(%r14), %rdi
	movl	$65535, %esi
	movq	%xmm0, -136(%rbp)
	movq	(%rdi), %r11
	movq	%r11, -128(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -88(%rbp)
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movq	-128(%rbp), %r11
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-136(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r11, %rdi
	movhps	-88(%rbp), %xmm0
	movq	%rcx, -88(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, (%r15)
	movq	%rax, %xmm0
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L560:
	movq	(%r14), %rax
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	(%rax), %r11
	movq	16(%rax), %rdi
	movq	%r11, -88(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShlEv@PLT
	movq	-88(%rbp), %r11
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movdqa	-128(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r11, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$24, %edx
	movq	%r14, %rdi
	movq	%rax, (%r15)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering12FixUpperBitsEPNS1_4NodeEi
	movq	%rax, (%r15)
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L579:
	cmpb	$4, %bl
	je	.L568
	cmpb	$5, %bl
	jne	.L554
	movl	$16, -148(%rbp)
	movl	$128, %esi
	jmp	.L550
.L568:
	movl	$8, -148(%rbp)
	movl	$64, %esi
	jmp	.L550
.L580:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L552
.L554:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L581:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14595:
	.size	_ZN2v88internal8compiler18SimdScalarLowering12LowerShiftOpEPNS1_4NodeENS2_8SimdTypeE, .-_ZN2v88internal8compiler18SimdScalarLowering12LowerShiftOpEPNS1_4NodeENS2_8SimdTypeE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering12LowerUnaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering12LowerUnaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler18SimdScalarLowering12LowerUnaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler18SimdScalarLowering12LowerUnaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE:
.LFB14589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$56, %rsp
	movq	%rsi, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	movq	32(%rsi), %rsi
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L584
	movq	16(%rsi), %rsi
.L584:
	movl	%r12d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, %r14
	movl	%r12d, %eax
	andl	$253, %eax
	je	.L590
	cmpb	$1, %al
	jne	.L596
	movl	$4, -84(%rbp)
	movl	$32, %esi
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L590:
	movl	$2, -84(%rbp)
	movl	$16, %esi
.L585:
	movq	(%r15), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	%rax, %rdx
	movq	%rax, -80(%rbp)
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rsi, %rax
	jb	.L597
	addq	-80(%rbp), %rsi
	movq	%rdx, %rcx
	movq	%rsi, 16(%rdi)
.L587:
	movl	-84(%rbp), %eax
	movq	%rcx, %r13
	leaq	-64(%rbp), %r12
	subq	%rcx, %r14
	subl	$1, %eax
	leaq	8(%rcx,%rax,8), %rax
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L588:
	movq	(%r15), %rax
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	(%rax), %rdi
	movq	(%r14,%r13), %rax
	addq	$8, %r13
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -8(%r13)
	cmpq	%r13, -72(%rbp)
	jne	.L588
	movl	-84(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r15, %rdi
	movq	-96(%rbp), %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L598
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L596:
	.cfi_restore_state
	cmpb	$4, %r12b
	je	.L592
	cmpb	$5, %r12b
	jne	.L599
	movl	$16, -84(%rbp)
	movl	$128, %esi
	jmp	.L585
.L592:
	movl	$8, -84(%rbp)
	movl	$64, %esi
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L597:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, -80(%rbp)
	movq	%rax, %rcx
	jmp	.L587
.L599:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L598:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14589:
	.size	_ZN2v88internal8compiler18SimdScalarLowering12LowerUnaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE, .-_ZN2v88internal8compiler18SimdScalarLowering12LowerUnaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	.type	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb, @function
_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb:
.LFB14584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$136, %rsp
	movq	%rsi, -168(%rbp)
	movq	%rcx, -152(%rbp)
	movb	%dl, -121(%rbp)
	movb	%r8b, -122(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	movq	32(%rsi), %rsi
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L601
	movq	16(%rsi), %rsi
.L601:
	movl	%r14d, %edx
	movq	%r13, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, %r12
	movq	-168(%rbp), %rax
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L603
	movq	-168(%rbp), %rax
	movq	32(%rax), %rax
	movq	%rax, -88(%rbp)
	leaq	24(%rax), %rbx
.L603:
	movq	(%rbx), %rsi
	movl	%r14d, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, %r15
	movl	%r14d, %eax
	andl	$253, %eax
	je	.L619
	cmpb	$1, %al
	jne	.L629
	movl	$4, -128(%rbp)
	movl	$32, %esi
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L619:
	movl	$2, -128(%rbp)
	movl	$16, %esi
.L604:
	movq	0(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	%rax, %rbx
	movq	%rax, -160(%rbp)
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	%rsi, %rax
	jb	.L630
	addq	-160(%rbp), %rsi
	movq	%rsi, 16(%rdi)
.L606:
	movl	-128(%rbp), %eax
	movq	%rbx, -88(%rbp)
	subl	$1, %eax
	leaq	8(%rbx,%rax,8), %rax
	movq	%rax, -120(%rbp)
	movq	%rbx, %rax
	leaq	-80(%rbp), %rbx
	subq	%rax, %r15
	subq	%rax, %r12
	movq	%r15, -136(%rbp)
	movq	%r12, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L617:
	movq	-88(%rbp), %rcx
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %rax
	cmpb	$0, -122(%rbp)
	movq	(%rax,%rcx), %rax
	movq	(%rdx,%rcx), %rcx
	movq	0(%r13), %rdx
	movq	(%rdx), %rdi
	je	.L607
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	punpcklqdq	%xmm2, %xmm0
.L628:
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	movq	-152(%rbp), %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	movq	%rax, -104(%rbp)
	movq	(%rdi), %r12
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -96(%rbp)
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%rax, -96(%rbp)
	movq	0(%r13), %rax
	movq	(%rax), %r12
	movq	8(%rax), %r14
	movq	8(%r12), %r15
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-96(%rbp), %xmm0
	movq	%r15, %xmm1
	movq	%rax, %rsi
	movq	%r12, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%r15, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%r15, -80(%rbp)
	movl	$4, %r15d
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	%rbx, %rcx
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rcx
	movzbl	-121(%rbp), %eax
	cmpb	$1, %al
	je	.L609
	cmpb	$5, %al
	ja	.L610
	leaq	.L612(%rip), %rdi
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb,"a",@progbits
	.align 4
	.align 4
.L612:
	.long	.L624-.L612
	.long	.L610-.L612
	.long	.L615-.L612
	.long	.L614-.L612
	.long	.L613-.L612
	.long	.L611-.L612
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
.L615:
	movl	$5, %r15d
.L609:
	movq	0(%r13), %rdi
	movl	$-1, %esi
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movl	$2, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	-112(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	-104(%rbp), %xmm0
	movl	$3, %edx
	movq	%rax, %rsi
	movq	%rcx, -64(%rbp)
	movq	%rbx, %rcx
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-88(%rbp), %rdx
	movq	%rax, (%rdx)
	addq	$8, %rdx
	movq	%rdx, -88(%rbp)
	cmpq	%rdx, -120(%rbp)
	jne	.L617
	movl	-128(%rbp), %ecx
	movq	-160(%rbp), %rdx
	movq	%r13, %rdi
	movq	-168(%rbp), %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L631
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L624:
	.cfi_restore_state
	movl	$13, %r15d
	jmp	.L609
.L610:
	xorl	%r15d, %r15d
	jmp	.L609
.L611:
	movl	$2, %r15d
	jmp	.L609
.L613:
	movl	$3, %r15d
	jmp	.L609
.L614:
	movl	$4, %r15d
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L607:
	movq	%rcx, %xmm0
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L629:
	cmpb	$4, %r14b
	je	.L621
	cmpb	$5, %r14b
	jne	.L632
	movl	$16, -128(%rbp)
	movl	$128, %esi
	jmp	.L604
.L621:
	movl	$8, -128(%rbp)
	movl	$64, %esi
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L630:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, -160(%rbp)
	movq	%rax, %rbx
	jmp	.L606
.L632:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L631:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14584:
	.size	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb, .-_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering21LowerSaturateBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering21LowerSaturateBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	.type	_ZN2v88internal8compiler18SimdScalarLowering21LowerSaturateBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb, @function
_ZN2v88internal8compiler18SimdScalarLowering21LowerSaturateBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb:
.LFB14588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	32(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$152, %rsp
	movq	%rsi, -184(%rbp)
	movq	%rcx, -160(%rbp)
	movb	%r8b, -98(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	movq	32(%rsi), %rsi
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L634
	movq	16(%rsi), %rsi
.L634:
	movl	%r14d, %edx
	movq	%r12, %rdi
	addq	$8, %r13
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, -136(%rbp)
	movq	-184(%rbp), %rax
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L636
	movq	-184(%rbp), %rax
	movq	32(%rax), %rax
	movq	%rax, -88(%rbp)
	leaq	24(%rax), %r13
.L636:
	movq	0(%r13), %rsi
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	cmpb	$1, %bl
	movq	%rax, -144(%rbp)
	sbbl	%eax, %eax
	cmpb	$4, %r14b
	je	.L662
	andl	$128, %eax
	addl	$127, %eax
	cmpb	$1, %bl
	movl	%eax, -108(%rbp)
	sbbl	%eax, %eax
	notl	%eax
	andl	$-128, %eax
	movl	%eax, -104(%rbp)
	movl	%r14d, %eax
	andl	$253, %eax
	jne	.L663
	movb	$2, -97(%rbp)
	movl	$16, %esi
	movl	$24, -172(%rbp)
	movl	$255, -112(%rbp)
	movl	$2, -176(%rbp)
.L641:
	movq	(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	%rax, %rbx
	movq	%rax, -168(%rbp)
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	%rax, %rsi
	ja	.L664
	addq	-168(%rbp), %rsi
	movq	%rsi, 16(%rdi)
.L644:
	movl	-176(%rbp), %eax
	movq	%rbx, %r13
	subl	$1, %eax
	leaq	8(%rbx,%rax,8), %rax
	movq	%rax, -152(%rbp)
	movq	%rbx, %rax
	leaq	-80(%rbp), %rbx
	negq	%rax
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L650:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	(%r12), %rdi
	addq	%rdx, %rax
	cmpb	$0, -98(%rbp)
	movq	(%rax,%r13), %rax
	movq	(%rdi), %r15
	movq	%rax, -88(%rbp)
	je	.L645
	movq	-144(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax,%r13), %rax
	movq	%rax, -96(%rbp)
.L646:
	movq	-88(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	-160(%rbp), %rsi
	movl	$2, %edx
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%r12), %rdi
	movl	-104(%rbp), %esi
	movq	%rax, -88(%rbp)
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -96(%rbp)
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r14, %rdi
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -96(%rbp)
	movq	(%r12), %rax
	movq	(%rax), %r15
	movq	8(%rax), %r14
	movq	8(%r15), %rax
	movq	%r14, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-96(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-96(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movl	$2, %esi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-120(%rbp), %xmm0
	movq	%r15, %rdi
	movq	%rax, %rsi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-104(%rbp), %esi
	movq	(%r12), %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movzbl	-97(%rbp), %esi
	movq	%r14, %rdi
	movl	$2, %edx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	-120(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	-96(%rbp), %xmm0
	movl	$3, %edx
	movq	%rax, %rsi
	movq	%rcx, -64(%rbp)
	movq	%rbx, %rcx
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-108(%rbp), %esi
	movq	%rax, 0(%r13)
	movq	(%r12), %rdi
	movq	%rax, %r14
	movq	(%rdi), %r15
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -88(%rbp)
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movq	%r14, %xmm1
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	-88(%rbp), %xmm0
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$2, %edx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -88(%rbp)
	movq	(%r12), %rax
	movq	(%rax), %r15
	movq	8(%rax), %r14
	movq	8(%r15), %rax
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-88(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-88(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r14, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-108(%rbp), %esi
	movq	(%r12), %rdi
	movq	%rax, -120(%rbp)
	movq	0(%r13), %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movzbl	-97(%rbp), %esi
	movl	$2, %edx
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	-120(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%rcx, -64(%rbp)
	movq	%rbx, %rcx
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	cmpb	$0, -98(%rbp)
	movq	%rax, 0(%r13)
	je	.L665
	addq	$8, %r13
	cmpq	%r13, -152(%rbp)
	jne	.L650
.L648:
	movl	-176(%rbp), %ecx
	movq	-168(%rbp), %rdx
	movq	%r12, %rdi
	movq	-184(%rbp), %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L666
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L665:
	.cfi_restore_state
	movl	-172(%rbp), %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	addq	$8, %r13
	call	_ZN2v88internal8compiler18SimdScalarLowering12FixUpperBitsEPNS1_4NodeEi
	movq	%rax, -8(%r13)
	cmpq	-152(%rbp), %r13
	jne	.L650
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L645:
	movl	-112(%rbp), %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -96(%rbp)
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-88(%rbp), %xmm0
	movq	%r15, %rdi
	movq	%rax, %rsi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%r12), %rdi
	movl	-112(%rbp), %esi
	movq	%rax, -88(%rbp)
	movq	-144(%rbp), %rax
	addq	-128(%rbp), %rax
	movq	(%rdi), %r15
	movq	(%rax,%r13), %r14
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -96(%rbp)
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movq	%r14, %xmm0
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movhps	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%rbx, %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -96(%rbp)
	movq	(%r12), %rax
	movq	(%rax), %r15
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L662:
	andl	$32768, %eax
	movb	$3, -97(%rbp)
	movl	$64, %esi
	addl	$32767, %eax
	cmpb	$1, %bl
	movl	$16, -172(%rbp)
	movl	%eax, -108(%rbp)
	sbbl	%eax, %eax
	notl	%eax
	movl	$65535, -112(%rbp)
	movl	$8, -176(%rbp)
	andl	$-32768, %eax
	movl	%eax, -104(%rbp)
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L663:
	cmpb	$1, %al
	jne	.L667
	movb	$2, -97(%rbp)
	movl	$32, %esi
	movl	$24, -172(%rbp)
	movl	$255, -112(%rbp)
	movl	$4, -176(%rbp)
	jmp	.L641
.L667:
	cmpb	$5, %r14b
	jne	.L668
	movb	$2, -97(%rbp)
	movl	$128, %esi
	movl	$24, -172(%rbp)
	movl	$255, -112(%rbp)
	movl	$16, -176(%rbp)
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L664:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, -168(%rbp)
	movq	%rax, %rbx
	jmp	.L644
.L668:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L666:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14588:
	.size	_ZN2v88internal8compiler18SimdScalarLowering21LowerSaturateBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb, .-_ZN2v88internal8compiler18SimdScalarLowering21LowerSaturateBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering14LowerIntMinMaxEPNS1_4NodeEPKNS1_8OperatorEbNS2_8SimdTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering14LowerIntMinMaxEPNS1_4NodeEPKNS1_8OperatorEbNS2_8SimdTypeE
	.type	_ZN2v88internal8compiler18SimdScalarLowering14LowerIntMinMaxEPNS1_4NodeEPKNS1_8OperatorEbNS2_8SimdTypeE, @function
_ZN2v88internal8compiler18SimdScalarLowering14LowerIntMinMaxEPNS1_4NodeEPKNS1_8OperatorEbNS2_8SimdTypeE:
.LFB14590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$152, %rsp
	movq	%rsi, -184(%rbp)
	movq	%rdx, -160(%rbp)
	movb	%cl, -161(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	movq	32(%rsi), %rsi
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L670
	movq	16(%rsi), %rsi
.L670:
	movl	%r12d, %edx
	movq	%r15, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, -136(%rbp)
	movq	-184(%rbp), %rax
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L672
	movq	-184(%rbp), %rax
	movq	32(%rax), %rax
	movq	%rax, -88(%rbp)
	leaq	24(%rax), %rbx
.L672:
	movq	(%rbx), %rsi
	movl	%r12d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, -144(%rbp)
	movl	%r12d, %eax
	andl	$253, %eax
	je	.L683
	cmpb	$1, %al
	jne	.L691
	movl	$4, -168(%rbp)
	movl	$32, %esi
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L683:
	movl	$2, -168(%rbp)
	movl	$16, %esi
.L673:
	movq	(%r15), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	%rax, %rbx
	movq	%rax, -176(%rbp)
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	%rsi, %rax
	jb	.L692
	addq	-176(%rbp), %rsi
	movq	%rsi, 16(%rdi)
.L675:
	cmpb	$3, %r12b
	je	.L687
	cmpb	$4, %r12b
	je	.L688
	cmpb	$5, %r12b
	jne	.L677
	movb	$2, -162(%rbp)
.L676:
	movl	-168(%rbp), %eax
	movq	-176(%rbp), %rcx
	movq	%r15, -120(%rbp)
	leaq	-80(%rbp), %rbx
	subl	$1, %eax
	movq	%rcx, %r11
	leaq	8(%rcx,%rax,8), %rax
	negq	%rcx
	movq	%r11, %r12
	movq	%rax, -128(%rbp)
	movq	%rcx, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L681:
	movq	-120(%rbp), %r15
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-136(%rbp), %r10
	movq	-144(%rbp), %r11
	movq	(%r15), %rax
	movq	-160(%rbp), %rsi
	movq	(%rax), %rdi
	movq	-152(%rbp), %rax
	addq	%rax, %r10
	addq	%rax, %r11
	movq	(%r10,%r12), %xmm0
	movq	%r10, -112(%rbp)
	movq	%r11, -104(%rbp)
	movhps	(%r11,%r12), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -88(%rbp)
	movq	(%r15), %rax
	movq	(%rax), %r13
	movq	8(%rax), %r15
	movq	8(%r13), %r14
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-88(%rbp), %xmm0
	movq	%r14, %xmm1
	movq	%rax, %rsi
	movq	%r13, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r14, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r14, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	cmpb	$0, -161(%rbp)
	movq	-104(%rbp), %r11
	movq	-112(%rbp), %r10
	movq	%rax, %r14
	je	.L678
	movq	(%r11,%r12), %xmm0
	movq	(%r10,%r12), %rax
	movl	$2, %edx
	movq	%r15, %rdi
	movzbl	-162(%rbp), %esi
	addq	$8, %r12
	movq	%xmm0, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$3, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r14, -64(%rbp)
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -8(%r12)
	cmpq	-128(%rbp), %r12
	jne	.L681
.L690:
	movq	-120(%rbp), %r15
	movl	-168(%rbp), %ecx
	movq	-176(%rbp), %rdx
	movq	-184(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L693
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L691:
	.cfi_restore_state
	cmpb	$4, %r12b
	je	.L685
	cmpb	$5, %r12b
	jne	.L677
	movl	$16, -168(%rbp)
	movl	$128, %esi
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L688:
	movb	$3, -162(%rbp)
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L678:
	movq	(%r10,%r12), %xmm0
	movq	(%r11,%r12), %rax
	movl	$2, %edx
	movq	%r15, %rdi
	movzbl	-162(%rbp), %esi
	addq	$8, %r12
	movq	%xmm0, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$3, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r14, -64(%rbp)
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -8(%r12)
	cmpq	%r12, -128(%rbp)
	jne	.L681
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L687:
	movb	$4, -162(%rbp)
	jmp	.L676
.L685:
	movl	$8, -168(%rbp)
	movl	$64, %esi
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L692:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, -176(%rbp)
	jmp	.L675
.L693:
	call	__stack_chk_fail@PLT
.L677:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE14590:
	.size	_ZN2v88internal8compiler18SimdScalarLowering14LowerIntMinMaxEPNS1_4NodeEPKNS1_8OperatorEbNS2_8SimdTypeE, .-_ZN2v88internal8compiler18SimdScalarLowering14LowerIntMinMaxEPNS1_4NodeEPKNS1_8OperatorEbNS2_8SimdTypeE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering9LowerPackEPNS1_4NodeENS2_8SimdTypeES5_b,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering9LowerPackEPNS1_4NodeENS2_8SimdTypeES5_b
	.type	_ZN2v88internal8compiler18SimdScalarLowering9LowerPackEPNS1_4NodeENS2_8SimdTypeES5_b, @function
_ZN2v88internal8compiler18SimdScalarLowering9LowerPackEPNS1_4NodeENS2_8SimdTypeES5_b:
.LFB14594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rsi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$168, %rsp
	movq	%rsi, -200(%rbp)
	movb	%r8b, -81(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	movq	32(%rsi), %rsi
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L695
	movq	16(%rsi), %rsi
.L695:
	movl	%r13d, %edx
	movq	%r12, %rdi
	addq	$8, %r15
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, -96(%rbp)
	movq	-200(%rbp), %rax
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L697
	movq	-200(%rbp), %rax
	movq	32(%rax), %rax
	movq	%rax, -104(%rbp)
	leaq	24(%rax), %r15
.L697:
	movq	(%r15), %rsi
	movq	%r12, %rdi
	movl	%r13d, %edx
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, -208(%rbp)
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	testb	%r14b, %r14b
	je	.L698
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movq	%rax, -112(%rbp)
	cmpb	$4, %bl
	je	.L737
	movq	(%r12), %rdi
	movl	$-128, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%r12), %rdi
	movl	$127, %esi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movl	$0, -180(%rbp)
	movq	%rax, -120(%rbp)
	movl	%ebx, %eax
	andl	$253, %eax
	je	.L720
.L740:
	cmpb	$1, %al
	jne	.L738
	movb	$2, -82(%rbp)
	movl	$32, %esi
	movl	$2, %ebx
	movl	$-2, -184(%rbp)
	movl	$4, -88(%rbp)
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L698:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	movq	%rax, -112(%rbp)
	cmpb	$4, %bl
	je	.L739
	movq	(%r12), %rdi
	movl	$255, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movl	$24, -180(%rbp)
	movq	%rax, -120(%rbp)
	movl	%ebx, %eax
	movq	$0, -176(%rbp)
	andl	$253, %eax
	jne	.L740
.L720:
	movb	$2, -82(%rbp)
	movl	$16, %esi
	movl	$1, %ebx
	movl	$-1, -184(%rbp)
	movl	$2, -88(%rbp)
.L704:
	movq	(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	%rax, %rdx
	movq	%rax, -192(%rbp)
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rsi, %rax
	jb	.L741
	addq	-192(%rbp), %rsi
	movq	%rsi, 16(%rdi)
.L706:
	movl	-88(%rbp), %eax
	movq	%rdx, %r10
	cmpl	%ebx, %eax
	cmovle	%eax, %ebx
	movq	-96(%rbp), %rax
	xorl	%r14d, %r14d
	movl	%r14d, -96(%rbp)
	movq	%r12, %r14
	subq	%rdx, %rax
	movl	%ebx, -160(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rax, -168(%rbp)
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L707:
	movq	-120(%rbp), %xmm1
	movq	-112(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	%r10, -152(%rbp)
	movhps	-104(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm1, -144(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -104(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %r13
	movq	8(%rax), %r15
	movq	8(%r13), %r12
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%r12, %xmm2
	movq	%rax, %rsi
	movq	%r13, %rdi
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movzbl	-82(%rbp), %esi
	movl	$2, %edx
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$3, %edx
	movdqa	-144(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r12, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-152(%rbp), %r10
	cmpb	$0, -81(%rbp)
	movq	%rax, (%r10)
	je	.L742
.L708:
	addl	$1, -96(%rbp)
	addq	$8, %r10
	movl	-96(%rbp), %eax
	cmpl	-160(%rbp), %eax
	jge	.L743
.L711:
	movq	-168(%rbp), %rax
	cmpb	$0, -81(%rbp)
	movq	(%rax,%r10), %rax
	movq	%rax, -104(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %rdi
	je	.L707
	movq	-104(%rbp), %xmm0
	movq	-112(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	-176(%rbp), %xmm4
	movl	$2, %edx
	movq	%r10, -152(%rbp)
	movdqa	%xmm4, %xmm1
	punpcklqdq	%xmm4, %xmm0
	movhps	-104(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm1, -144(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%rax, -104(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %r13
	movq	8(%rax), %r15
	movq	8(%r13), %r12
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%r12, %xmm6
	movq	%rax, %rsi
	movq	%r13, %rdi
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movzbl	-82(%rbp), %esi
	movl	$2, %edx
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movdqa	-144(%rbp), %xmm1
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-152(%rbp), %r10
	movq	%rax, -104(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %rdi
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L742:
	movl	-180(%rbp), %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal8compiler18SimdScalarLowering12FixUpperBitsEPNS1_4NodeEi
	movq	-104(%rbp), %r10
	movq	%rax, (%r10)
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L743:
	movq	%r14, %r12
	movl	%eax, %r14d
	cmpl	%eax, -88(%rbp)
	jle	.L714
	movq	-192(%rbp), %rcx
	movslq	%r14d, %rax
	movq	-208(%rbp), %rdx
	movl	%r14d, -96(%rbp)
	leaq	(%rcx,%rax,8), %r10
	movslq	-184(%rbp), %rax
	leaq	(%rdx,%rax,8), %rax
	subq	%rcx, %rax
	movq	%rax, -160(%rbp)
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L712:
	movq	-120(%rbp), %xmm1
	movq	-112(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	%r10, -152(%rbp)
	movhps	-104(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm1, -144(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -104(%rbp)
	movq	(%r12), %rax
	movq	(%rax), %r13
	movq	8(%rax), %r15
	movq	8(%r13), %r14
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%r14, %xmm3
	movq	%rax, %rsi
	movq	%r13, %rdi
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r14, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r14, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r15, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-128(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movzbl	-82(%rbp), %esi
	movl	$2, %edx
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$3, %edx
	movdqa	-144(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r14, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-152(%rbp), %r10
	cmpb	$0, -81(%rbp)
	movq	%rax, (%r10)
	je	.L744
	addl	$1, -96(%rbp)
	addq	$8, %r10
	movl	-96(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	jle	.L714
.L716:
	movq	-160(%rbp), %rax
	cmpb	$0, -81(%rbp)
	movq	(%rax,%r10), %rax
	movq	%rax, -104(%rbp)
	movq	(%r12), %rax
	movq	(%rax), %rdi
	je	.L712
	movq	-104(%rbp), %xmm0
	movq	-112(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	-176(%rbp), %xmm5
	movl	$2, %edx
	movq	%r10, -152(%rbp)
	movdqa	%xmm5, %xmm1
	punpcklqdq	%xmm5, %xmm0
	movhps	-104(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm1, -144(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%rax, -104(%rbp)
	movq	(%r12), %rax
	movq	(%rax), %r14
	movq	8(%rax), %r15
	movq	8(%r14), %r13
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%r13, %xmm7
	movq	%rax, %rsi
	movq	%r14, %rdi
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r13, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r13, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r15, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-128(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movzbl	-82(%rbp), %esi
	movl	$2, %edx
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movdqa	-144(%rbp), %xmm1
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r13, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-152(%rbp), %r10
	movq	%rax, -104(%rbp)
	movq	(%r12), %rax
	movq	(%rax), %rdi
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L744:
	movl	-180(%rbp), %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal8compiler18SimdScalarLowering12FixUpperBitsEPNS1_4NodeEi
	movq	-104(%rbp), %r10
	addl	$1, -96(%rbp)
	movq	%rax, (%r10)
	movl	-96(%rbp), %eax
	addq	$8, %r10
	cmpl	%eax, -88(%rbp)
	jg	.L716
.L714:
	movl	-88(%rbp), %ecx
	movq	-192(%rbp), %rdx
	movq	%r12, %rdi
	movq	-200(%rbp), %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L745
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L737:
	.cfi_restore_state
	movq	(%r12), %rdi
	movl	$-32768, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%r12), %rdi
	movl	$32767, %esi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movl	$0, -180(%rbp)
	movq	%rax, -120(%rbp)
.L703:
	movb	$3, -82(%rbp)
	movl	$64, %esi
	movl	$4, %ebx
	movl	$-4, -184(%rbp)
	movl	$8, -88(%rbp)
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L739:
	movq	(%r12), %rdi
	movl	$65535, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movl	$16, -180(%rbp)
	movq	%rax, -120(%rbp)
	movq	$0, -176(%rbp)
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L738:
	cmpb	$4, %bl
	je	.L722
	cmpb	$5, %bl
	jne	.L746
	movb	$2, -82(%rbp)
	movl	$128, %esi
	movl	$8, %ebx
	movl	$-8, -184(%rbp)
	movl	$16, -88(%rbp)
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L741:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, -192(%rbp)
	movq	%rax, %rdx
	jmp	.L706
.L722:
	movb	$2, -82(%rbp)
	movl	$64, %esi
	movl	$4, %ebx
	movl	$-4, -184(%rbp)
	movl	$8, -88(%rbp)
	jmp	.L704
.L745:
	call	__stack_chk_fail@PLT
.L746:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE14594:
	.size	_ZN2v88internal8compiler18SimdScalarLowering9LowerPackEPNS1_4NodeENS2_8SimdTypeES5_b, .-_ZN2v88internal8compiler18SimdScalarLowering9LowerPackEPNS1_4NodeENS2_8SimdTypeES5_b
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering13LowerNotEqualEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering13LowerNotEqualEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler18SimdScalarLowering13LowerNotEqualEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler18SimdScalarLowering13LowerNotEqualEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE:
.LFB14596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	32(%rsi), %rbx
	subq	$136, %rsp
	movq	%rsi, -168(%rbp)
	movq	%rcx, -136(%rbp)
	movb	%dl, -121(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	movq	32(%rsi), %rsi
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L748
	movq	16(%rsi), %rsi
.L748:
	movl	%r13d, %edx
	movq	%r15, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, %r12
	movq	-168(%rbp), %rax
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L750
	movq	-168(%rbp), %rax
	movq	32(%rax), %rax
	movq	%rax, -88(%rbp)
	leaq	24(%rax), %rbx
.L750:
	movq	(%rbx), %rsi
	movl	%r13d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, %r14
	movl	%r13d, %eax
	andl	$253, %eax
	je	.L764
	cmpb	$1, %al
	jne	.L773
	movl	$4, -128(%rbp)
	movl	$32, %esi
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L764:
	movl	$2, -128(%rbp)
	movl	$16, %esi
.L751:
	movq	(%r15), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	%rax, %rbx
	movq	%rax, -160(%rbp)
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	%rsi, %rax
	jb	.L774
	addq	-160(%rbp), %rsi
	movq	%rsi, 16(%rdi)
.L753:
	movl	-128(%rbp), %eax
	movq	%rbx, %r9
	subl	$1, %eax
	leaq	8(%rbx,%rax,8), %rax
	movq	%rax, -120(%rbp)
	movq	%rbx, %rax
	leaq	-80(%rbp), %rbx
	subq	%rax, %r12
	subq	%rax, %r14
	movq	%r12, -144(%rbp)
	movq	%r14, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L762:
	movq	(%r15), %rax
	movq	-136(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	%r9, -104(%rbp)
	movq	(%rax), %rdi
	movq	-144(%rbp), %rax
	movq	(%rax,%r9), %rax
	movq	%rax, -88(%rbp)
	movq	-152(%rbp), %rax
	movq	-88(%rbp), %xmm0
	movhps	(%rax,%r9), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -88(%rbp)
	movq	(%r15), %rax
	movq	(%rax), %r12
	movq	8(%rax), %r13
	movq	8(%r12), %r14
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-88(%rbp), %xmm0
	movq	%r14, %xmm1
	movq	%rax, %rsi
	movq	%r12, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%r14, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%r14, -80(%rbp)
	movl	$4, %r14d
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-104(%rbp), %r9
	movq	%rax, %rcx
	movzbl	-121(%rbp), %eax
	cmpb	$1, %al
	je	.L754
	cmpb	$5, %al
	ja	.L755
	leaq	.L757(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering13LowerNotEqualEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE,"a",@progbits
	.align 4
	.align 4
.L757:
	.long	.L769-.L757
	.long	.L755-.L757
	.long	.L760-.L757
	.long	.L759-.L757
	.long	.L758-.L757
	.long	.L756-.L757
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering13LowerNotEqualEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE
.L760:
	movl	$5, %r14d
.L754:
	movq	(%r15), %rdi
	movl	$-1, %esi
	movq	%r9, -112(%rbp)
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%r15), %rdi
	xorl	%esi, %esi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movl	$2, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	-104(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%rcx, -64(%rbp)
	movq	%rbx, %rcx
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-112(%rbp), %r9
	movq	%rax, (%r9)
	addq	$8, %r9
	cmpq	%r9, -120(%rbp)
	jne	.L762
	movl	-128(%rbp), %ecx
	movq	-160(%rbp), %rdx
	movq	%r15, %rdi
	movq	-168(%rbp), %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L775
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L769:
	.cfi_restore_state
	movl	$13, %r14d
	jmp	.L754
.L756:
	movl	$2, %r14d
	jmp	.L754
.L755:
	xorl	%r14d, %r14d
	jmp	.L754
.L758:
	movl	$3, %r14d
	jmp	.L754
.L759:
	movl	$4, %r14d
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L773:
	cmpb	$4, %r13b
	je	.L766
	cmpb	$5, %r13b
	jne	.L776
	movl	$16, -128(%rbp)
	movl	$128, %esi
	jmp	.L751
.L766:
	movl	$8, -128(%rbp)
	movl	$64, %esi
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L774:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, -160(%rbp)
	movq	%rax, %rbx
	jmp	.L753
.L776:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L775:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14596:
	.size	_ZN2v88internal8compiler18SimdScalarLowering13LowerNotEqualEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE, .-_ZN2v88internal8compiler18SimdScalarLowering13LowerNotEqualEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering12LowerStoreOpEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering12LowerStoreOpEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimdScalarLowering12LowerStoreOpEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimdScalarLowering12LowerStoreOpEPNS1_4NodeE:
.LFB14582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rsi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	20(%rsi), %eax
	movl	%eax, %edx
	xorl	$251658240, %edx
	andl	$251658240, %edx
	leaq	48(%rsi), %rdx
	jne	.L779
	movq	32(%rsi), %rsi
	leaq	32(%rsi), %rdx
.L779:
	movq	(%rdx), %rdx
	movq	112(%rbx), %rcx
	andl	$16777215, %eax
	salq	$4, %rax
	movl	20(%rdx), %edx
	andl	$16777215, %edx
	salq	$4, %rdx
	movzbl	8(%rcx,%rdx), %r14d
	movb	%r14b, 8(%rcx,%rax)
	movq	(%r12), %rdi
	movzwl	16(%rdi), %eax
	cmpw	$495, %ax
	je	.L780
	cmpw	$503, %ax
	je	.L781
	cmpw	$431, %ax
	je	.L875
.L873:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L780:
	call	_ZN2v88internal8compiler30UnalignedStoreRepresentationOfEPKNS1_8OperatorE@PLT
	movzbl	(%rax), %r13d
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	cmpb	$5, %r14b
	ja	.L792
	leaq	.L794(%rip), %rcx
	movzbl	%r14b, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering12LowerStoreOpEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L794:
	.long	.L799-.L794
	.long	.L846-.L794
	.long	.L797-.L794
	.long	.L796-.L794
	.long	.L795-.L794
	.long	.L793-.L794
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering12LowerStoreOpEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L875:
	call	_ZN2v88internal8compiler21StoreRepresentationOfEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movzbl	(%rax), %r13d
	call	_ZN2v88internal8compiler21StoreRepresentationOfEPKNS1_8OperatorE@PLT
	movzbl	1(%rax), %esi
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	cmpb	$5, %r14b
	ja	.L783
	leaq	.L785(%rip), %rcx
	movzbl	%r14b, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering12LowerStoreOpEPNS1_4NodeE
	.align 4
	.align 4
.L785:
	.long	.L790-.L785
	.long	.L845-.L785
	.long	.L788-.L785
	.long	.L787-.L785
	.long	.L786-.L785
	.long	.L784-.L785
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering12LowerStoreOpEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L781:
	call	_ZN2v88internal8compiler21StoreRepresentationOfEPKNS1_8OperatorE@PLT
	movzbl	(%rax), %r13d
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	cmpb	$5, %r14b
	ja	.L800
	leaq	.L802(%rip), %rcx
	movzbl	%r14b, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering12LowerStoreOpEPNS1_4NodeE
	.align 4
	.align 4
.L802:
	.long	.L807-.L802
	.long	.L847-.L802
	.long	.L805-.L802
	.long	.L804-.L802
	.long	.L803-.L802
	.long	.L801-.L802
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering12LowerStoreOpEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L847:
	movl	$12, %esi
	.p2align 4,,10
	.p2align 3
.L806:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14ProtectedStoreENS0_21MachineRepresentationE@PLT
	movq	%rax, -144(%rbp)
.L791:
	cmpb	$14, %r13b
	jne	.L808
.L879:
	movq	32(%r12), %rax
	movq	%rax, -112(%rbp)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L809
	leaq	8(%r15), %rax
.L810:
	movq	(%rax), %r13
	movl	%r14d, %eax
	andl	$253, %eax
	je	.L848
	cmpb	$1, %al
	je	.L849
	cmpb	$4, %r14b
	je	.L850
	cmpb	$5, %r14b
	jne	.L873
	movl	$16, -148(%rbp)
	movl	$128, %r8d
	.p2align 4,,10
	.p2align 3
.L811:
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	%rax, %rcx
	movq	%rax, -104(%rbp)
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %r8
	ja	.L876
	movq	%rcx, %rax
	addq	%r8, %rax
	movq	%rax, 16(%rdi)
.L813:
	movq	-104(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal8compiler18SimdScalarLowering13GetIndexNodesEPNS1_4NodeEPS4_NS2_8SimdTypeE
	movzbl	23(%r12), %eax
	movq	-120(%rbp), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L814
	leaq	16(%r15), %rax
.L815:
	movq	(%rax), %r9
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rax, %r8
	ja	.L877
	addq	%r13, %r8
	movq	%r8, 16(%rdi)
.L817:
	movq	%r12, 0(%r13)
	movl	%r14d, %edx
	movq	%r9, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	0(%r13), %rsi
	movq	%rax, -136(%rbp)
	movq	(%rax), %r14
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L818
	movq	48(%rsi), %rdi
	leaq	32(%rsi), %rax
	cmpq	%rdi, %r14
	je	.L819
	leaq	48(%rsi), %rax
.L820:
	subq	$72, %rsi
	testq	%rdi, %rdi
	je	.L822
	movq	%rax, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rsi
.L822:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L823
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L823:
	movq	0(%r13), %rsi
	movq	-104(%rbp), %rax
	movzbl	23(%rsi), %edx
	movq	(%rax), %r8
	leaq	32(%rsi), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L843
	movq	32(%rsi), %rsi
	leaq	16(%rsi), %rax
.L843:
	movq	8(%rax), %rdi
	cmpq	%rdi, %r8
	je	.L827
	addq	$8, %rax
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L828
	movq	%r14, %rsi
	movq	%rax, -128(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %r8
.L828:
	movq	%r8, (%rax)
	testq	%r8, %r8
	je	.L827
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L827:
	movq	-144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movzbl	23(%r12), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L829
	testb	$12, %al
	je	.L831
	movq	24(%r15), %rax
	addq	$32, %r15
.L833:
	movl	-148(%rbp), %edi
	movq	(%r15), %rcx
	leaq	-96(%rbp), %r15
	movq	%r12, -160(%rbp)
	movq	-104(%rbp), %rsi
	movq	-144(%rbp), %r12
	leal	-1(%rdi), %edx
	movq	%rcx, -120(%rbp)
	leal	-2(%rdi), %ecx
	movslq	%edx, %rdx
	salq	$3, %rcx
	salq	$3, %rdx
	leaq	(%rsi,%rdx), %r14
	subq	$8, %rdx
	subq	%rcx, %rdx
	movq	-136(%rbp), %rcx
	addq	%rsi, %rdx
	subq	%rsi, %rcx
	movq	%rdx, -128(%rbp)
	movq	%r15, %rsi
	movq	%rbx, %r15
	movq	%rcx, -136(%rbp)
	movq	%rsi, %rbx
	.p2align 4,,10
	.p2align 3
.L834:
	movq	(%r15), %rdx
	movq	-136(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-112(%rbp), %rsi
	movq	(%rdx), %rdi
	movq	(%rcx,%r14), %rdx
	movq	(%r14), %rcx
	movq	%rax, -72(%rbp)
	movq	-120(%rbp), %rax
	movq	%rsi, -96(%rbp)
	movq	%r12, %rsi
	movq	%rcx, -88(%rbp)
	movq	%rbx, %rcx
	movq	%rdx, -80(%rbp)
	movl	$5, %edx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdx
	subq	-104(%rbp), %rdx
	movq	%rax, (%rdx,%r14)
	subq	$8, %r14
	cmpq	%r14, -128(%rbp)
	jne	.L834
	movq	0(%r13), %rsi
	movq	%r15, %rbx
	movq	-160(%rbp), %r12
	movq	8(%r13), %r15
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L835
	movq	56(%rsi), %rdi
	leaq	56(%rsi), %rax
	cmpq	%rdi, %r15
	je	.L838
.L837:
	leaq	-96(%rsi), %r14
	testq	%rdi, %rdi
	je	.L839
	movq	%r14, %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rax
.L839:
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L838
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L838:
	movl	-148(%rbp), %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
.L777:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L878
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L807:
	.cfi_restore_state
	movl	$13, %esi
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L845:
	movl	$12, %edx
	.p2align 4,,10
	.p2align 3
.L789:
	xorl	%eax, %eax
	movl	%esi, %ecx
	movb	%dl, %al
	movb	%cl, %ah
	movl	%eax, %esi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder5StoreENS1_19StoreRepresentationE@PLT
	movq	%rax, -144(%rbp)
	cmpb	$14, %r13b
	je	.L879
.L808:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering15DefaultLoweringEPNS1_4NodeE
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L790:
	movl	$13, %edx
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L846:
	movl	$12, %esi
	.p2align 4,,10
	.p2align 3
.L798:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14UnalignedStoreENS0_21MachineRepresentationE@PLT
	movq	%rax, -144(%rbp)
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L799:
	movl	$13, %esi
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L848:
	movl	$2, -148(%rbp)
	movl	$16, %r8d
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L831:
	movl	-148(%rbp), %eax
	movq	-104(%rbp), %rcx
	leaq	-96(%rbp), %r15
	subl	$2, %eax
	leaq	8(%rcx), %r14
	leaq	16(%rcx,%rax,8), %rax
	movq	%rax, -120(%rbp)
	movq	-136(%rbp), %rax
	movq	%r12, -136(%rbp)
	movq	-144(%rbp), %r12
	subq	%rcx, %rax
	movq	%rax, -128(%rbp)
	movq	%r15, %rax
	movq	%rbx, %r15
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L840:
	movq	(%r15), %rax
	movq	(%r14), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	-112(%rbp), %rcx
	movq	(%rax), %rdi
	movq	-128(%rbp), %rax
	movq	(%rax,%r14), %rax
	movq	%rcx, -96(%rbp)
	movq	%rbx, %rcx
	movq	%rdx, -88(%rbp)
	movl	$3, %edx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movq	%r13, %rax
	subq	-104(%rbp), %rax
	movq	%r8, (%rax,%r14)
	addq	$8, %r14
	cmpq	%r14, -120(%rbp)
	jne	.L840
	movq	-136(%rbp), %r12
	movq	%r15, %rbx
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L809:
	movq	-112(%rbp), %rdi
	movq	16(%rdi), %rcx
	leaq	24(%rdi), %rax
	movq	%rcx, -112(%rbp)
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L829:
	movq	32(%r12), %r15
	cmpl	$3, 8(%r15)
	jle	.L831
	movq	40(%r15), %rax
	addq	$48, %r15
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L818:
	movq	32(%rsi), %rsi
	movq	32(%rsi), %rdi
	leaq	16(%rsi), %rax
	cmpq	%rdi, %r14
	je	.L821
	leaq	32(%rsi), %rax
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L814:
	movq	32(%r12), %rax
	addq	$32, %rax
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L849:
	movl	$4, -148(%rbp)
	movl	$32, %r8d
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L819:
	movq	-104(%rbp), %rcx
	movq	(%rcx), %r8
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L835:
	movq	32(%rsi), %rsi
	movq	40(%rsi), %rdi
	cmpq	%rdi, %r15
	je	.L838
	leaq	40(%rsi), %rax
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L821:
	movq	-104(%rbp), %rdi
	movq	(%rdi), %r8
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L876:
	movq	%r8, %rsi
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %r8
	movq	%rax, -104(%rbp)
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L877:
	movq	%r8, %rsi
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %r9
	movq	%rax, %r13
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L793:
	movl	$2, %esi
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L796:
	movl	$4, %esi
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L795:
	movl	$3, %esi
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L784:
	movl	$2, %edx
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L788:
	movl	$5, %edx
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L787:
	movl	$4, %edx
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L786:
	movl	$3, %edx
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L805:
	movl	$5, %esi
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L804:
	movl	$4, %esi
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L803:
	movl	$3, %esi
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L797:
	movl	$5, %esi
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L801:
	movl	$2, %esi
	jmp	.L806
.L783:
	xorl	%edx, %edx
	jmp	.L789
.L800:
	xorl	%esi, %esi
	jmp	.L806
.L792:
	xorl	%esi, %esi
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L850:
	movl	$8, -148(%rbp)
	movl	$64, %r8d
	jmp	.L811
.L878:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14582:
	.size	_ZN2v88internal8compiler18SimdScalarLowering12LowerStoreOpEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimdScalarLowering12LowerStoreOpEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering21LowerConvertFromFloatEPNS1_4NodeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering21LowerConvertFromFloatEPNS1_4NodeEb
	.type	_ZN2v88internal8compiler18SimdScalarLowering21LowerConvertFromFloatEPNS1_4NodeEb, @function
_ZN2v88internal8compiler18SimdScalarLowering21LowerConvertFromFloatEPNS1_4NodeEb:
.LFB14592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$184, %rsp
	movq	%rsi, -216(%rbp)
	movzbl	23(%rax), %eax
	movb	%dl, -201(%rbp)
	movq	32(%rsi), %rsi
	andl	$15, %eax
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	cmpl	$15, %eax
	jne	.L881
	movq	16(%rsi), %rsi
.L881:
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	pxor	%xmm0, %xmm0
	movq	%rax, -184(%rbp)
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder15Float64ConstantEd@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -192(%rbp)
	movq	(%r12), %rax
	movq	(%rax), %r13
	movq	8(%rax), %rdi
	testb	%bl, %bl
	je	.L901
	movsd	.LC9(%rip), %xmm0
	call	_ZN2v88internal8compiler21CommonOperatorBuilder15Float64ConstantEd@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	.LC7(%rip), %xmm0
	movq	%rax, -152(%rbp)
	movq	(%r12), %rax
	movq	(%rax), %r13
	movq	8(%rax), %rdi
.L888:
	call	_ZN2v88internal8compiler21CommonOperatorBuilder15Float64ConstantEd@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	leaq	-128(%rbp), %rbx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%r9d, %r9d
	movq	%rax, -200(%rbp)
	leaq	-96(%rbp), %rax
	movq	%r9, %r14
	movq	%rax, -160(%rbp)
.L887:
	movq	(%r12), %rax
	movq	-184(%rbp), %rdx
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	movq	(%rdx,%r14), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22ChangeFloat32ToFloat64Ev@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r15, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -136(%rbp)
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder12Float64EqualEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	-136(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -144(%rbp)
	movq	(%r12), %rax
	movq	(%rax), %r13
	movq	8(%rax), %r15
	movq	8(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-144(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-176(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-144(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-144(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movl	$2, %esi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	-176(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movl	$2, %edx
	movl	$13, %esi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r13, %rdi
	movq	-144(%rbp), %rcx
	movq	%rax, %rsi
	movq	-136(%rbp), %xmm0
	movq	%rcx, -112(%rbp)
	movhps	-192(%rbp), %xmm0
	movq	%rbx, %rcx
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -144(%rbp)
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Float64LessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	-144(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-152(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -136(%rbp)
	movq	(%r12), %rax
	movq	(%rax), %r13
	movq	8(%rax), %r15
	movq	8(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-136(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-176(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movl	$2, %esi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	-176(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movl	$2, %edx
	movl	$13, %esi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r13, %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %rsi
	movq	-152(%rbp), %xmm0
	movq	%rcx, -112(%rbp)
	movhps	-144(%rbp), %xmm0
	movq	%rbx, %rcx
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -136(%rbp)
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Float64LessThanEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	-200(%rbp), %xmm1
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-136(%rbp), %xmm1
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm1, -176(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -136(%rbp)
	movq	(%r12), %rax
	movq	(%rax), %r13
	movq	8(%rax), %r15
	movq	8(%r13), %rax
	movq	%r15, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-136(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r13, %rdi
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movl	$2, %esi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	-144(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movl	$2, %edx
	movl	$13, %esi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	$3, %edx
	movq	-136(%rbp), %rcx
	movdqa	-176(%rbp), %xmm1
	movq	%rax, %rsi
	movq	%rcx, -112(%rbp)
	movq	%rbx, %rcx
	movaps	%xmm1, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering13BuildF64TruncEPNS1_4NodeE
	cmpb	$0, -201(%rbp)
	movq	%rax, %r13
	movq	(%r12), %rax
	movq	(%rax), %r15
	movq	16(%rax), %rdi
	je	.L884
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeFloat64ToInt32Ev@PLT
.L900:
	movl	$1, %edx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	%r13, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-160(%rbp), %rdx
	movq	%rax, (%rdx,%r14)
	addq	$8, %r14
	cmpq	$32, %r14
	jne	.L887
	movq	-160(%rbp), %rdx
	movq	-216(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i.constprop.0
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L902
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L884:
	.cfi_restore_state
	call	_ZN2v88internal8compiler22MachineOperatorBuilder23TruncateFloat64ToUint32Ev@PLT
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L901:
	pxor	%xmm0, %xmm0
	call	_ZN2v88internal8compiler21CommonOperatorBuilder15Float64ConstantEd@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movsd	.LC6(%rip), %xmm0
	movq	%rax, -152(%rbp)
	movq	(%r12), %rax
	movq	(%rax), %r13
	movq	8(%rax), %rdi
	jmp	.L888
.L902:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14592:
	.size	_ZN2v88internal8compiler18SimdScalarLowering21LowerConvertFromFloatEPNS1_4NodeEb, .-_ZN2v88internal8compiler18SimdScalarLowering21LowerConvertFromFloatEPNS1_4NodeEb
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering9LowerNodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering9LowerNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler18SimdScalarLowering9LowerNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler18SimdScalarLowering9LowerNodeEPNS1_4NodeE:
.LFB14597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	20(%r13), %ecx
	movq	112(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%ecx, %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movzbl	8(%rsi,%rax), %ebx
	movl	%ebx, %r12d
	andl	$253, %r12d
	je	.L1194
	cmpb	$1, %r12b
	jne	.L1307
	movq	0(%r13), %rdi
	movl	$32, %r15d
	movl	$4, %r10d
	cmpw	$681, 16(%rdi)
	jbe	.L1308
	.p2align 4,,10
	.p2align 3
.L905:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering15DefaultLoweringEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L903:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1309
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1194:
	.cfi_restore_state
	movl	$16, %r15d
	movl	$2, %r10d
.L904:
	movq	0(%r13), %rdi
	cmpw	$681, 16(%rdi)
	ja	.L905
.L1308:
	movzwl	16(%rdi), %eax
	leaq	.L907(%rip), %r8
	movslq	(%r8,%rax,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering9LowerNodeEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L907:
	.long	.L1004-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L1003-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L1002-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L1001-.L907
	.long	.L1000-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L999-.L907
	.long	.L905-.L907
	.long	.L998-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L999-.L907
	.long	.L998-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L999-.L907
	.long	.L998-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L942-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L942-.L907
	.long	.L941-.L907
	.long	.L940-.L907
	.long	.L997-.L907
	.long	.L996-.L907
	.long	.L995-.L907
	.long	.L994-.L907
	.long	.L993-.L907
	.long	.L993-.L907
	.long	.L992-.L907
	.long	.L991-.L907
	.long	.L990-.L907
	.long	.L989-.L907
	.long	.L988-.L907
	.long	.L987-.L907
	.long	.L986-.L907
	.long	.L985-.L907
	.long	.L984-.L907
	.long	.L983-.L907
	.long	.L982-.L907
	.long	.L981-.L907
	.long	.L980-.L907
	.long	.L942-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L942-.L907
	.long	.L941-.L907
	.long	.L940-.L907
	.long	.L979-.L907
	.long	.L978-.L907
	.long	.L977-.L907
	.long	.L938-.L907
	.long	.L921-.L907
	.long	.L921-.L907
	.long	.L976-.L907
	.long	.L975-.L907
	.long	.L974-.L907
	.long	.L973-.L907
	.long	.L932-.L907
	.long	.L931-.L907
	.long	.L972-.L907
	.long	.L971-.L907
	.long	.L970-.L907
	.long	.L969-.L907
	.long	.L968-.L907
	.long	.L967-.L907
	.long	.L966-.L907
	.long	.L965-.L907
	.long	.L964-.L907
	.long	.L921-.L907
	.long	.L920-.L907
	.long	.L919-.L907
	.long	.L963-.L907
	.long	.L962-.L907
	.long	.L961-.L907
	.long	.L960-.L907
	.long	.L942-.L907
	.long	.L941-.L907
	.long	.L940-.L907
	.long	.L959-.L907
	.long	.L958-.L907
	.long	.L938-.L907
	.long	.L921-.L907
	.long	.L921-.L907
	.long	.L957-.L907
	.long	.L937-.L907
	.long	.L936-.L907
	.long	.L956-.L907
	.long	.L935-.L907
	.long	.L934-.L907
	.long	.L933-.L907
	.long	.L932-.L907
	.long	.L931-.L907
	.long	.L955-.L907
	.long	.L954-.L907
	.long	.L953-.L907
	.long	.L952-.L907
	.long	.L951-.L907
	.long	.L950-.L907
	.long	.L949-.L907
	.long	.L948-.L907
	.long	.L921-.L907
	.long	.L947-.L907
	.long	.L923-.L907
	.long	.L922-.L907
	.long	.L920-.L907
	.long	.L919-.L907
	.long	.L946-.L907
	.long	.L945-.L907
	.long	.L944-.L907
	.long	.L943-.L907
	.long	.L942-.L907
	.long	.L941-.L907
	.long	.L940-.L907
	.long	.L939-.L907
	.long	.L938-.L907
	.long	.L921-.L907
	.long	.L921-.L907
	.long	.L937-.L907
	.long	.L936-.L907
	.long	.L935-.L907
	.long	.L934-.L907
	.long	.L933-.L907
	.long	.L932-.L907
	.long	.L931-.L907
	.long	.L930-.L907
	.long	.L929-.L907
	.long	.L928-.L907
	.long	.L927-.L907
	.long	.L926-.L907
	.long	.L925-.L907
	.long	.L924-.L907
	.long	.L923-.L907
	.long	.L922-.L907
	.long	.L921-.L907
	.long	.L920-.L907
	.long	.L919-.L907
	.long	.L918-.L907
	.long	.L917-.L907
	.long	.L916-.L907
	.long	.L915-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L914-.L907
	.long	.L913-.L907
	.long	.L912-.L907
	.long	.L911-.L907
	.long	.L910-.L907
	.long	.L909-.L907
	.long	.L908-.L907
	.long	.L905-.L907
	.long	.L905-.L907
	.long	.L906-.L907
	.long	.L906-.L907
	.long	.L906-.L907
	.long	.L906-.L907
	.long	.L906-.L907
	.long	.L906-.L907
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering9LowerNodeEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1307:
	cmpb	$4, %bl
	je	.L1196
	cmpb	$5, %bl
	jne	.L1105
	movl	$128, %r15d
	movl	$16, %r10d
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L1196:
	movl	$64, %r15d
	movl	$8, %r10d
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L921:
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering12LowerShiftOpEPNS1_4NodeENS2_8SimdTypeE
	jmp	.L903
.L942:
	movq	(%r14), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%r15, %rax
	jb	.L1310
	addq	%rdx, %r15
	movq	%r15, 16(%rdi)
.L1122:
	leal	-1(%r10), %eax
	movq	%rdx, %r8
	leaq	32(%r13), %rdi
	leaq	8(%rdx,%rax,8), %rsi
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1312:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L1124
	movq	%rax, (%rdx)
.L1125:
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	je	.L1311
.L1126:
	movzbl	23(%r13), %eax
	movq	(%rdi), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1123
	movq	16(%rcx), %rcx
.L1123:
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L1312
.L1124:
	movq	%rcx, (%rdx)
	jmp	.L1125
.L906:
	xorl	$251658240, %ecx
	movq	32(%r13), %rax
	andl	$251658240, %ecx
	jne	.L1184
	movq	16(%rax), %rax
.L1184:
	movl	20(%rax), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rax, %rsi
	movzbl	8(%rsi), %eax
	movl	%eax, %edx
	andl	$253, %edx
	je	.L1211
	cmpb	$1, %dl
	je	.L1212
	cmpb	$4, %al
	je	.L1213
	cmpb	$5, %al
	jne	.L1105
	movl	$16, %ebx
	.p2align 4,,10
	.p2align 3
.L1185:
	movq	(%r14), %rax
	movq	(%rsi), %r12
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	%rax, %rcx
	movq	%rax, -192(%rbp)
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%r15, %rax
	jb	.L1313
	addq	-192(%rbp), %r15
	movq	%r15, 16(%rdi)
.L1187:
	movq	(%r14), %rdi
	movl	$1, %esi
	movl	%r10d, -120(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	(%r14), %rdi
	xorl	%esi, %esi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movl	-120(%rbp), %r10d
	movq	%rax, -160(%rbp)
	movq	%rax, %xmm5
	movq	0(%r13), %rax
	movzwl	16(%rax), %eax
	movl	%eax, %edx
	andl	$-3, %edx
	cmpw	$677, %dx
	je	.L1214
	cmpw	$681, %ax
	jne	.L1314
.L1214:
	movq	-184(%rbp), %rax
	movq	%rax, -144(%rbp)
.L1188:
	leal	-1(%rbx), %eax
	movq	%r12, -120(%rbp)
	leaq	-96(%rbp), %rbx
	leaq	8(%r12,%rax,8), %rax
	movl	%r10d, -200(%rbp)
	movq	%rax, -176(%rbp)
	movq	%r13, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	(%r14), %rax
	movq	-120(%rbp), %rdx
	movq	16(%rax), %rdi
	movq	(%rdx), %r13
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	%r13, %xmm0
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movhps	-160(%rbp), %xmm0
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%rax, -128(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %r12
	movq	8(%rax), %r13
	movq	8(%r12), %r15
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-128(%rbp), %xmm0
	movq	%r15, %xmm4
	movq	%rax, %rsi
	movq	%r12, %rdi
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%r15, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%r15, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movl	$2, %edx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	-128(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-152(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r15
	movq	-168(%rbp), %rax
	movq	(%rax), %rax
	movzwl	16(%rax), %eax
	movl	%eax, %edx
	andl	$-3, %edx
	cmpw	$677, %dx
	je	.L1189
	cmpw	$681, %ax
	jne	.L1315
.L1189:
	movl	$2, %edx
	movl	$4, %esi
	movq	%r13, %rdi
	movq	-160(%rbp), %xmm0
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$3, %edx
	movdqa	-144(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%r15, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	addq	$8, -120(%rbp)
	movq	%rax, -144(%rbp)
	movq	-120(%rbp), %rax
	cmpq	-176(%rbp), %rax
	jne	.L1192
.L1305:
	movq	-192(%rbp), %rbx
	movl	-200(%rbp), %r10d
	movq	-144(%rbp), %rax
	movq	-168(%rbp), %r13
	leaq	16(%rbx), %rdi
	leaq	8(%rbx), %rdx
	movq	$0, 8(%rbx)
	movq	%rax, (%rbx)
	andq	$-8, %rdi
	leal	-2(%r10), %eax
	movq	%r13, %rsi
	subq	%rdi, %rdx
	leaq	8(,%rax,8), %rax
	movl	%eax, %ecx
	addl	%edx, %eax
	movq	%rbx, %rdx
	shrl	$3, %eax
	movq	$0, (%rbx,%rcx)
	movl	%eax, %ecx
	xorl	%eax, %eax
	rep stosq
	movl	%r10d, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	jmp	.L903
.L941:
	movl	44(%rdi), %eax
	movl	%eax, -120(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	%r15, %rax
	jb	.L1316
	addq	%r12, %r15
	movq	%r15, 16(%rdi)
.L1128:
	movzbl	23(%r13), %eax
	movq	32(%r13), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1129
	movq	16(%rsi), %rsi
.L1129:
	movl	%ebx, %edx
	movq	%r14, %rdi
	movl	%r10d, -144(%rbp)
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movslq	-120(%rbp), %rbx
	leaq	16(%r12), %rdi
	movq	%r13, %rsi
	movl	-144(%rbp), %r10d
	andq	$-8, %rdi
	leaq	8(%r12), %rdx
	movq	(%rax,%rbx,8), %rax
	subq	%rdi, %rdx
	movq	$0, 8(%r12)
	movq	%rax, (%r12)
	leal	-2(%r10), %eax
	leaq	8(,%rax,8), %rax
	movl	%eax, %ecx
	addl	%edx, %eax
	movq	%r12, %rdx
	shrl	$3, %eax
	movq	$0, (%r12,%rcx)
	movl	%eax, %ecx
	xorl	%eax, %eax
	rep stosq
	movl	%r10d, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	jmp	.L903
.L940:
	movl	44(%rdi), %edx
	xorl	$251658240, %ecx
	movq	32(%r13), %rsi
	andl	$251658240, %ecx
	movl	%edx, -120(%rbp)
	je	.L1130
	movq	40(%r13), %r8
.L1131:
	movl	%ebx, %edx
	movq	%r14, %rdi
	movq	%r8, -128(%rbp)
	movl	%r10d, -144(%rbp)
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movl	-144(%rbp), %r10d
	movq	-128(%rbp), %r8
	movq	%rax, %r12
	movq	(%r14), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%r15, %rax
	jb	.L1317
	addq	%rdx, %r15
	movq	%r15, 16(%rdi)
.L1133:
	leaq	15(%r12), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	jbe	.L1134
	leal	-1(%r10), %eax
	cmpl	$3, %eax
	jbe	.L1134
	movdqu	(%r12), %xmm7
	movl	%r10d, %eax
	shrl	%eax
	movups	%xmm7, (%rdx)
	movdqu	16(%r12), %xmm7
	movups	%xmm7, 16(%rdx)
	cmpl	$4, %r10d
	je	.L1135
	movdqu	32(%r12), %xmm5
	movups	%xmm5, 32(%rdx)
	cmpl	$3, %eax
	je	.L1135
	movdqu	48(%r12), %xmm5
	movups	%xmm5, 48(%rdx)
	cmpl	$4, %eax
	je	.L1135
	movdqu	64(%r12), %xmm7
	movups	%xmm7, 64(%rdx)
	cmpl	$5, %eax
	je	.L1135
	movdqu	80(%r12), %xmm7
	movups	%xmm7, 80(%rdx)
	cmpl	$6, %eax
	je	.L1135
	movdqu	96(%r12), %xmm5
	movups	%xmm5, 96(%rdx)
	cmpl	$7, %eax
	je	.L1135
	movdqu	112(%r12), %xmm5
	movups	%xmm5, 112(%rdx)
.L1135:
	movl	20(%r8), %eax
	movslq	-120(%rbp), %rbx
	andl	$16777215, %eax
	leaq	(%rdx,%rbx,8), %rcx
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L1136
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L1136
	movq	%rax, (%rcx)
.L1137:
	movl	%r10d, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	jmp	.L903
.L920:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	movl	%ebx, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerIntMinMaxEPNS1_4NodeEPKNS1_8OperatorEbNS2_8SimdTypeE
	jmp	.L903
.L999:
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering11LowerLoadOpEPNS1_4NodeENS2_8SimdTypeE
	jmp	.L903
.L938:
	xorl	$251658240, %ecx
	movq	32(%r13), %rsi
	andl	$251658240, %ecx
	jne	.L1103
	movq	16(%rsi), %rsi
.L1103:
	movl	%ebx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, -120(%rbp)
	testb	%r12b, %r12b
	je	.L1207
	cmpb	$1, %r12b
	je	.L1208
	cmpb	$4, %bl
	je	.L1209
	cmpb	$5, %bl
	jne	.L1105
	movl	$16, -168(%rbp)
	movl	$128, %esi
	.p2align 4,,10
	.p2align 3
.L1104:
	movq	(%r14), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	%rax, %rbx
	movq	%rax, -160(%rbp)
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	%rsi, %rax
	jb	.L1318
	addq	-160(%rbp), %rsi
	movq	%rsi, 16(%rdi)
.L1107:
	movq	(%r14), %rax
	xorl	%esi, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-160(%rbp), %rbx
	movq	-120(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movl	-168(%rbp), %eax
	subl	$1, %eax
	leaq	8(%rbx,%rax,8), %rax
	movq	%rax, -128(%rbp)
	movq	%rbx, %rax
	leaq	-96(%rbp), %rbx
	subq	%rax, %rcx
	movq	%rax, %r12
	movq	%rbx, %rax
	movq	%r14, %rbx
	movq	%rcx, -152(%rbp)
	movq	%rax, %r14
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1108:
	cmpl	$639, %eax
	je	.L1319
.L1109:
	addq	$8, %r12
	cmpq	-128(%rbp), %r12
	je	.L1320
.L1110:
	movq	(%rbx), %rax
	movq	-152(%rbp), %rdx
	movq	16(%rax), %rdi
	movq	(%rdx,%r12), %rcx
	movq	(%rax), %r15
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-144(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	movq	%rax, %rsi
	movq	0(%r13), %rax
	movzwl	16(%rax), %eax
	cmpl	$605, %eax
	jne	.L1108
	movl	$16, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering12FixUpperBitsEPNS1_4NodeEi
	movq	%rax, (%r12)
	jmp	.L1109
.L931:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movl	%ebx, %r8d
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerIntMinMaxEPNS1_4NodeEPKNS1_8OperatorEbNS2_8SimdTypeE
	jmp	.L903
.L932:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movl	%ebx, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerIntMinMaxEPNS1_4NodeEPKNS1_8OperatorEbNS2_8SimdTypeE
	jmp	.L903
.L919:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	movl	%ebx, %r8d
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerIntMinMaxEPNS1_4NodeEPKNS1_8OperatorEbNS2_8SimdTypeE
	jmp	.L903
.L998:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering12LowerStoreOpEPNS1_4NodeE
	jmp	.L903
.L933:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32MulEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering24LowerBinaryOpForSmallIntEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L923:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	xorl	%r8d, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering21LowerSaturateBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L993:
	xorl	$251658240, %ecx
	movq	32(%r13), %rsi
	andl	$251658240, %ecx
	jne	.L1114
	movq	16(%rsi), %rsi
.L1114:
	movl	%ebx, %edx
	movq	%r14, %rdi
	movl	%r10d, -120(%rbp)
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movl	-120(%rbp), %r10d
	movq	%rax, %r12
	movq	(%r14), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	%rax, %rbx
	movq	%rax, -160(%rbp)
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	%r15, %rax
	jb	.L1321
	addq	-160(%rbp), %r15
	movq	%r15, 16(%rdi)
.L1116:
	movq	(%r14), %rax
	movss	.LC10(%rip), %xmm0
	movl	%r10d, -120(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder15Float32ConstantEf@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-120(%rbp), %r10d
	movq	-160(%rbp), %rbx
	movq	%rax, -144(%rbp)
	leal	-1(%r10), %eax
	movq	%rbx, %r15
	movl	%r10d, -168(%rbp)
	leaq	8(%rbx,%rax,8), %rax
	movq	%rax, -128(%rbp)
	movq	%rbx, %rax
	leaq	-96(%rbp), %rbx
	subq	%rax, %r12
	movq	%r12, -152(%rbp)
	movq	%r14, %r12
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1117:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float32DivEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-144(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	addq	$8, %r15
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -8(%r15)
	cmpq	-128(%rbp), %r15
	je	.L1322
.L1120:
	movq	-152(%rbp), %rax
	movq	(%rax,%r15), %rax
	movq	%rax, -120(%rbp)
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	movq	0(%r13), %rax
	cmpw	$535, 16(%rax)
	jne	.L1117
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Float32SqrtEv@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%rax, %rsi
	movq	-120(%rbp), %rax
	movl	$1, %edx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -120(%rbp)
	movq	(%r12), %rax
	movq	(%rax), %r14
	movq	16(%rax), %rdi
	jmp	.L1117
.L934:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering21LowerSaturateBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L935:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering24LowerBinaryOpForSmallIntEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L936:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering21LowerSaturateBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L937:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering24LowerBinaryOpForSmallIntEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L922:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	xorl	%r8d, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering21LowerSaturateBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L946:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L971:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerNotEqualEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE
	jmp	.L903
.L945:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21Uint32LessThanOrEqualEv@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L954:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerNotEqualEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE
	jmp	.L903
.L1000:
	movq	120(%r14), %rdi
	movl	136(%r14), %eax
	movq	8(%rdi), %rdx
	movl	%edx, %ecx
	cmpl	$-1, %eax
	je	.L1323
.L1028:
	cmpl	%ecx, %eax
	je	.L903
	movq	0(%r13), %rdi
	call	_ZN2v88internal8compiler16ParameterIndexOfEPKNS1_8OperatorE@PLT
	movl	%eax, %edx
	movq	120(%r14), %rax
	testl	%edx, %edx
	jle	.L1202
	movq	(%rax), %rcx
	movq	16(%rax), %rsi
	leal	-1(%rdx), %eax
	cmpl	$14, %eax
	jbe	.L1203
	pxor	%xmm1, %xmm1
	pxor	%xmm7, %xmm7
	pxor	%xmm6, %xmm6
	addq	%rcx, %rsi
	movl	%edx, %ecx
	movdqa	.LC1(%rip), %xmm5
	movdqa	.LC2(%rip), %xmm4
	movq	%rsi, %rax
	shrl	$4, %ecx
	salq	$4, %rcx
	addq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L1052:
	movdqu	(%rax), %xmm0
	movdqa	%xmm7, %xmm2
	movdqa	%xmm6, %xmm8
	addq	$16, %rax
	pcmpeqb	%xmm5, %xmm0
	pand	%xmm4, %xmm0
	pcmpgtb	%xmm0, %xmm2
	movdqa	%xmm0, %xmm3
	punpcklbw	%xmm2, %xmm3
	punpckhbw	%xmm2, %xmm0
	pcmpgtw	%xmm3, %xmm8
	movdqa	%xmm3, %xmm2
	punpcklwd	%xmm8, %xmm2
	punpckhwd	%xmm8, %xmm3
	paddd	%xmm2, %xmm1
	movdqa	%xmm6, %xmm2
	pcmpgtw	%xmm0, %xmm2
	paddd	%xmm3, %xmm1
	movdqa	%xmm0, %xmm3
	punpcklwd	%xmm2, %xmm3
	punpckhwd	%xmm2, %xmm0
	paddd	%xmm3, %xmm1
	paddd	%xmm0, %xmm1
	cmpq	%rax, %rcx
	jne	.L1052
	movdqa	%xmm1, %xmm0
	movl	%edx, %eax
	psrldq	$8, %xmm0
	andl	$-16, %eax
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ebx
	leal	(%rbx,%rdx), %r15d
	testb	$15, %dl
	je	.L1053
.L1051:
	movslq	%eax, %rcx
	leal	3(%r15), %edi
	cmpb	$14, (%rsi,%rcx)
	leal	1(%rax), %ecx
	cmove	%edi, %r15d
	cmpl	%ecx, %edx
	jle	.L1053
	movslq	%ecx, %rcx
	leal	3(%r15), %edi
	cmpb	$14, (%rsi,%rcx)
	leal	2(%rax), %ecx
	cmove	%edi, %r15d
	cmpl	%ecx, %edx
	jle	.L1053
	movslq	%ecx, %rcx
	leal	3(%r15), %edi
	cmpb	$14, (%rsi,%rcx)
	leal	3(%rax), %ecx
	cmove	%edi, %r15d
	cmpl	%ecx, %edx
	jle	.L1053
	movslq	%ecx, %rcx
	leal	3(%r15), %edi
	cmpb	$14, (%rsi,%rcx)
	leal	4(%rax), %ecx
	cmove	%edi, %r15d
	cmpl	%edx, %ecx
	jge	.L1053
	movslq	%ecx, %rcx
	leal	3(%r15), %edi
	cmpb	$14, (%rsi,%rcx)
	leal	5(%rax), %ecx
	cmove	%edi, %r15d
	cmpl	%ecx, %edx
	jle	.L1053
	movslq	%ecx, %rcx
	leal	3(%r15), %edi
	cmpb	$14, (%rsi,%rcx)
	leal	6(%rax), %ecx
	cmove	%edi, %r15d
	cmpl	%ecx, %edx
	jle	.L1053
	movslq	%ecx, %rcx
	leal	3(%r15), %edi
	cmpb	$14, (%rsi,%rcx)
	leal	7(%rax), %ecx
	cmove	%edi, %r15d
	cmpl	%ecx, %edx
	jle	.L1053
	movslq	%ecx, %rcx
	leal	3(%r15), %edi
	cmpb	$14, (%rsi,%rcx)
	leal	8(%rax), %ecx
	cmove	%edi, %r15d
	cmpl	%edx, %ecx
	jge	.L1053
	movslq	%ecx, %rcx
	leal	3(%r15), %edi
	cmpb	$14, (%rsi,%rcx)
	leal	9(%rax), %ecx
	cmove	%edi, %r15d
	cmpl	%edx, %ecx
	jge	.L1053
	movslq	%ecx, %rcx
	leal	3(%r15), %edi
	cmpb	$14, (%rsi,%rcx)
	leal	10(%rax), %ecx
	cmove	%edi, %r15d
	cmpl	%ecx, %edx
	jle	.L1053
	movslq	%ecx, %rcx
	leal	3(%r15), %edi
	cmpb	$14, (%rsi,%rcx)
	leal	11(%rax), %ecx
	cmove	%edi, %r15d
	cmpl	%ecx, %edx
	jle	.L1053
	movslq	%ecx, %rcx
	leal	3(%r15), %edi
	cmpb	$14, (%rsi,%rcx)
	leal	12(%rax), %ecx
	cmove	%edi, %r15d
	cmpl	%ecx, %edx
	jle	.L1053
	movslq	%ecx, %rcx
	leal	3(%r15), %edi
	cmpb	$14, (%rsi,%rcx)
	leal	13(%rax), %ecx
	cmove	%edi, %r15d
	cmpl	%ecx, %edx
	jle	.L1053
	movslq	%ecx, %rcx
	cmpb	$14, (%rsi,%rcx)
	jne	.L1067
	addl	$3, %r15d
.L1067:
	addl	$14, %eax
	cmpl	%eax, %edx
	jle	.L1053
	cltq
	cmpb	$14, (%rsi,%rax)
	jne	.L1053
	addl	$3, %r15d
.L1053:
	cmpl	%r15d, %edx
	jne	.L903
.L1050:
	movq	(%r14), %rax
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	120(%r14), %rdx
	movq	%r13, -96(%rbp)
	movslq	%r15d, %rax
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	addq	16(%rdx), %rax
	addq	(%rdx), %rax
	cmpb	$14, (%rax)
	je	.L1204
	leaq	-96(%rbp), %rbx
.L1071:
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i.constprop.0
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L977:
	movl	$3, %ecx
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	$4, %r9d
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering19LowerConvertFromIntEPNS1_4NodeENS2_8SimdTypeES5_bi
	jmp	.L903
.L991:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float32AddEv@PLT
	xorl	%r8d, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L909:
	xorl	$251658240, %ecx
	movq	32(%r13), %rax
	leaq	32(%r13), %r12
	andl	$251658240, %ecx
	je	.L1138
	movl	20(%rax), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rsi,%rax), %rax
	movq	%rax, -176(%rbp)
	leaq	40(%r13), %rax
.L1139:
	movq	(%rax), %rsi
	movl	%ebx, %edx
	movq	%r14, %rdi
	movl	%r10d, -120(%rbp)
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movl	-120(%rbp), %r10d
	movq	%rax, -152(%rbp)
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1140
	leaq	16(%r12), %rax
.L1141:
	movq	(%rax), %rsi
	movl	%ebx, %edx
	movq	%r14, %rdi
	movl	%r10d, -120(%rbp)
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movl	-120(%rbp), %r10d
	movq	%rax, -160(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	%rax, %rbx
	movq	%rax, -184(%rbp)
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	%r15, %rax
	jb	.L1324
	addq	-184(%rbp), %r15
	movq	%rbx, %r12
	movq	%r15, 16(%rdi)
.L1143:
	leal	-1(%r10), %eax
	movl	%r10d, -192(%rbp)
	movq	%r12, %r15
	leaq	-96(%rbp), %rbx
	leaq	8(%r12,%rax,8), %rax
	movq	%r13, -200(%rbp)
	negq	%r12
	movq	%r14, %r13
	movq	%rax, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L1144:
	movq	-160(%rbp), %rdx
	movq	0(%r13), %rax
	leaq	(%rdx,%r12), %r10
	movq	-152(%rbp), %rdx
	movq	16(%rax), %rdi
	movq	(%r10,%r15), %rcx
	movq	(%rax), %r14
	movq	%r10, -128(%rbp)
	addq	%r12, %rdx
	movq	(%rdx,%r15), %xmm0
	movq	%rcx, -120(%rbp)
	movq	%xmm0, -144(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32XorEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rbx, %rcx
	movq	-144(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-176(%rbp), %rcx
	movq	%rax, -120(%rbp)
	movq	0(%r13), %rax
	leaq	(%rcx,%r12), %rdx
	movq	(%rdx,%r15), %xmm0
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	movq	%xmm0, -144(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-144(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-128(%rbp), %r10
	movq	%rax, -120(%rbp)
	movq	0(%r13), %rax
	movq	(%r10,%r15), %xmm0
	addq	$8, %r15
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	movq	%xmm0, -144(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32XorEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-144(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -8(%r15)
	cmpq	-168(%rbp), %r15
	jne	.L1144
	movq	%r13, %r14
	movl	-192(%rbp), %r10d
	movq	-200(%rbp), %r13
	movq	-184(%rbp), %rdx
	movq	%r14, %rdi
	movl	%r10d, %ecx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	jmp	.L903
.L910:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32XorEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L911:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Word32OrEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L912:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L913:
	xorl	$251658240, %ecx
	movq	32(%r13), %rsi
	andl	$251658240, %ecx
	jne	.L1112
	movq	16(%rsi), %rsi
.L1112:
	movl	%ebx, %edx
	movq	%r14, %rdi
	leaq	-96(%rbp), %rbx
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movl	$-1, %esi
	movq	%rax, -144(%rbp)
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	leaq	-112(%rbp), %r12
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, -152(%rbp)
	xorl	%r9d, %r9d
	movq	%rax, -128(%rbp)
	movq	%r9, %r15
	movq	%rbx, -120(%rbp)
	movq	%r12, %rbx
.L1113:
	movq	(%r14), %rax
	movq	-144(%rbp), %rcx
	movq	16(%rax), %rdi
	movq	(%rcx,%r15), %r13
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32XorEv@PLT
	movq	%r13, %xmm0
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movhps	-128(%rbp), %xmm0
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-120(%rbp), %rdx
	movq	%rax, (%rdx,%r15)
	addq	$8, %r15
	cmpq	$32, %r15
	jne	.L1113
	movq	-152(%rbp), %r13
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i.constprop.0
	jmp	.L903
.L914:
	leaq	-96(%rbp), %rbx
	leaq	-64(%rbp), %r12
	movq	%rbx, %r15
.L1111:
	movq	(%r14), %rdi
	xorl	%esi, %esi
	addq	$8, %r15
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, -8(%r15)
	cmpq	%r12, %r15
	jne	.L1111
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i.constprop.0
	jmp	.L903
.L915:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21Uint32LessThanOrEqualEv@PLT
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L916:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L917:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21Uint32LessThanOrEqualEv@PLT
	xorl	%r8d, %r8d
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L918:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	xorl	%r8d, %r8d
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L924:
	xorl	%r8d, %r8d
	movl	$5, %ecx
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering9LowerPackEPNS1_4NodeENS2_8SimdTypeES5_b
	jmp	.L903
.L925:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Int32LessThanOrEqualEv@PLT
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L926:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L927:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Int32LessThanOrEqualEv@PLT
	xorl	%r8d, %r8d
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L928:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	xorl	%r8d, %r8d
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L983:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Float32LessThanEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L984:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder12Float32EqualEv@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerNotEqualEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE
	jmp	.L903
.L985:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder12Float32EqualEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L986:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float32MaxEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L987:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float32MinEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L988:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float32DivEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L989:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float32MulEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L990:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float32SubEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L979:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering21LowerConvertFromFloatEPNS1_4NodeEb
	jmp	.L903
.L980:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22Float32LessThanOrEqualEv@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L981:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Float32LessThanEv@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L982:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22Float32LessThanOrEqualEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L978:
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	$3, %ecx
	movl	$4, %edx
	call	_ZN2v88internal8compiler18SimdScalarLowering19LowerConvertFromIntEPNS1_4NodeENS2_8SimdTypeES5_bi
	jmp	.L903
.L951:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L944:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L947:
	xorl	%r8d, %r8d
	movl	$4, %ecx
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering9LowerPackEPNS1_4NodeENS2_8SimdTypeES5_b
	jmp	.L903
.L948:
	movl	$8, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	$4, %ecx
	movl	$5, %edx
	call	_ZN2v88internal8compiler18SimdScalarLowering19LowerConvertFromIntEPNS1_4NodeENS2_8SimdTypeES5_bi
	jmp	.L903
.L949:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$4, %ecx
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering19LowerConvertFromIntEPNS1_4NodeENS2_8SimdTypeES5_bi
	jmp	.L903
.L950:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Int32LessThanOrEqualEv@PLT
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L968:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L969:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Int32LessThanOrEqualEv@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L970:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L952:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Int32LessThanOrEqualEv@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L953:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L929:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerNotEqualEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE
	jmp	.L903
.L956:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	xorl	%r8d, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering24LowerBinaryOpForSmallIntEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L957:
	movl	$4, %ecx
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering9LowerPackEPNS1_4NodeENS2_8SimdTypeES5_b
	jmp	.L903
.L992:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float32AddEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L955:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L958:
	movl	$4, %ecx
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	$8, %r9d
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering19LowerConvertFromIntEPNS1_4NodeENS2_8SimdTypeES5_bi
	jmp	.L903
.L959:
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	$4, %ecx
	movl	$5, %edx
	call	_ZN2v88internal8compiler18SimdScalarLowering19LowerConvertFromIntEPNS1_4NodeENS2_8SimdTypeES5_bi
	jmp	.L903
.L960:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21Uint32LessThanOrEqualEv@PLT
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L961:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L962:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21Uint32LessThanOrEqualEv@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L963:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L994:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float32NegEv@PLT
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler18SimdScalarLowering12LowerUnaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE
	jmp	.L903
.L995:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float32AbsEv@PLT
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler18SimdScalarLowering12LowerUnaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE
	jmp	.L903
.L1001:
	call	_ZN2v88internal8compiler16CallDescriptorOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18SimdScalarLowering15DefaultLoweringEPNS1_4NodeE
	testb	%al, %al
	jne	.L1091
	movq	24(%rbx), %rax
	cmpq	$1, (%rax)
	jne	.L903
	movq	16(%rax), %rax
	cmpb	$0, 5(%rax)
	jne	.L903
	cmpb	$14, 4(%rax)
	jne	.L903
.L1091:
	movq	(%r14), %rax
	movq	%rbx, %rsi
	movq	8(%rax), %r12
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler31GetI32WasmCallDescriptorForSimdEPNS0_4ZoneEPNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	24(%rbx), %rax
	cmpq	$1, (%rax)
	jne	.L903
	movq	16(%rax), %rax
	cmpb	$0, 5(%rax)
	jne	.L903
	cmpb	$14, 4(%rax)
	jne	.L903
	leaq	-96(%rbp), %rbx
	movq	%r13, -120(%rbp)
	xorl	%r15d, %r15d
	leaq	-112(%rbp), %r12
	movq	%rbx, %r13
	movq	%r14, %rbx
.L1092:
	movq	(%rbx), %rax
	movq	%r15, %rsi
	movq	(%rax), %r14
	movq	8(%rax), %rdi
	movq	8(%r14), %rcx
	movq	%rcx, -144(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10ProjectionEm@PLT
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	$2, %edx
	movq	-120(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 0(%r13,%r15,8)
	addq	$1, %r15
	cmpq	$4, %r15
	jne	.L1092
	movq	%rbx, %r14
	movq	%r13, %rbx
	movq	-120(%rbp), %r13
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i.constprop.0
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L1002:
	movl	%r10d, -120(%rbp)
	call	_ZN2v88internal8compiler19PhiRepresentationOfEPKNS1_8OperatorE@PLT
	cmpb	$14, %al
	jne	.L1093
	movl	20(%r13), %eax
	movl	%eax, %ecx
	andl	$16777215, %ecx
	salq	$4, %rcx
	addq	112(%r14), %rcx
	movq	(%rcx), %rdx
	movq	0(%r13), %rcx
	movl	20(%rcx), %ecx
	testl	%ecx, %ecx
	jle	.L903
	movl	-120(%rbp), %r10d
	leaq	32(%r13), %rcx
	movb	%bl, -184(%rbp)
	xorl	%r15d, %r15d
	movq	%rcx, -168(%rbp)
	leal	-1(%r10), %ecx
	movq	%r14, -192(%rbp)
	movl	%ecx, -176(%rbp)
	movq	%r13, -200(%rbp)
	movq	%r15, %r13
	movq	%rdx, %r15
	.p2align 4,,10
	.p2align 3
.L1102:
	xorl	$251658240, %eax
	leaq	0(,%r13,8), %r12
	testl	$251658240, %eax
	movq	-168(%rbp), %rax
	je	.L1094
	addq	%r12, %rax
.L1095:
	movzbl	-184(%rbp), %edx
	movq	(%rax), %rsi
	xorl	%r14d, %r14d
	movq	-192(%rbp), %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	leaq	1(%r13), %rdx
	movl	-176(%rbp), %r13d
	movq	%rax, %rbx
	leaq	0(,%rdx,4), %rax
	movq	%rdx, -160(%rbp)
	subq	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	movq	%rax, -120(%rbp)
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1326:
	addq	%r12, %rdx
	movq	(%rdx), %rdi
	cmpq	%rdi, %r8
	je	.L1098
.L1097:
	addq	-120(%rbp), %rsi
	testq	%rdi, %rdi
	je	.L1100
	movq	%rdx, -152(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-152(%rbp), %rdx
	movq	-128(%rbp), %r8
	movq	-144(%rbp), %rsi
.L1100:
	movq	%r8, (%rdx)
	testq	%r8, %r8
	je	.L1098
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1098:
	leaq	1(%r14), %rdx
	cmpq	%r14, %r13
	je	.L1325
	movq	%rdx, %r14
.L1101:
	movq	(%r15,%r14,8), %rsi
	movq	(%rbx,%r14,8), %r8
	movzbl	23(%rsi), %ecx
	leaq	32(%rsi), %rdx
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L1326
	movq	32(%rsi), %rsi
	leaq	16(%rsi,%r12), %rdx
	movq	(%rdx), %rdi
	cmpq	%rdi, %r8
	jne	.L1097
	jmp	.L1098
.L1004:
	movq	120(%r14), %rdi
	movl	136(%r14), %eax
	movq	8(%rdi), %rdx
	movl	%edx, %ecx
	cmpl	$-1, %eax
	je	.L1327
.L1005:
	cmpl	%ecx, %eax
	je	.L903
	movq	0(%r13), %rdx
	subl	%ecx, %eax
	addl	32(%rdx), %eax
	movl	%eax, %esi
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5StartEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L903
.L972:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L1003:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering15DefaultLoweringEPNS1_4NodeE
	movq	120(%r14), %rax
	movq	(%rax), %rdx
	testl	%edx, %edx
	jle	.L903
	movq	16(%rax), %rdi
	leal	-1(%rdx), %eax
	cmpl	$14, %eax
	jbe	.L1205
	movl	%edx, %ecx
	pxor	%xmm1, %xmm1
	pxor	%xmm7, %xmm7
	movq	%rdi, %rax
	shrl	$4, %ecx
	movdqa	.LC1(%rip), %xmm5
	movdqa	.LC2(%rip), %xmm4
	pxor	%xmm6, %xmm6
	salq	$4, %rcx
	addq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L1073:
	movdqu	(%rax), %xmm0
	movdqa	%xmm7, %xmm2
	movdqa	%xmm6, %xmm8
	addq	$16, %rax
	pcmpeqb	%xmm5, %xmm0
	pand	%xmm4, %xmm0
	pcmpgtb	%xmm0, %xmm2
	movdqa	%xmm0, %xmm3
	punpcklbw	%xmm2, %xmm3
	punpckhbw	%xmm2, %xmm0
	pcmpgtw	%xmm3, %xmm8
	movdqa	%xmm3, %xmm2
	punpcklwd	%xmm8, %xmm2
	punpckhwd	%xmm8, %xmm3
	paddd	%xmm2, %xmm1
	movdqa	%xmm6, %xmm2
	pcmpgtw	%xmm0, %xmm2
	paddd	%xmm3, %xmm1
	movdqa	%xmm0, %xmm3
	punpcklwd	%xmm2, %xmm3
	punpckhwd	%xmm2, %xmm0
	paddd	%xmm3, %xmm1
	paddd	%xmm0, %xmm1
	cmpq	%rcx, %rax
	jne	.L1073
	movdqa	%xmm1, %xmm0
	movl	%edx, %eax
	psrldq	$8, %xmm0
	andl	$-16, %eax
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %esi
	addl	%edx, %esi
	testb	$15, %dl
	je	.L1074
.L1072:
	movslq	%eax, %rcx
	cmpb	$14, (%rdi,%rcx)
	jne	.L1075
	addl	$3, %esi
.L1075:
	leal	1(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L1074
	movslq	%ecx, %rcx
	cmpb	$14, (%rdi,%rcx)
	jne	.L1076
	addl	$3, %esi
.L1076:
	leal	2(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L1074
	movslq	%ecx, %rcx
	cmpb	$14, (%rdi,%rcx)
	jne	.L1077
	addl	$3, %esi
.L1077:
	leal	3(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L1074
	movslq	%ecx, %rcx
	cmpb	$14, (%rdi,%rcx)
	jne	.L1078
	addl	$3, %esi
.L1078:
	leal	4(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L1074
	movslq	%ecx, %rcx
	cmpb	$14, (%rdi,%rcx)
	jne	.L1079
	addl	$3, %esi
.L1079:
	leal	5(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L1074
	movslq	%ecx, %rcx
	cmpb	$14, (%rdi,%rcx)
	jne	.L1080
	addl	$3, %esi
.L1080:
	leal	6(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L1074
	movslq	%ecx, %rcx
	cmpb	$14, (%rdi,%rcx)
	jne	.L1081
	addl	$3, %esi
.L1081:
	leal	7(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L1074
	movslq	%ecx, %rcx
	cmpb	$14, (%rdi,%rcx)
	jne	.L1082
	addl	$3, %esi
.L1082:
	leal	8(%rax), %ecx
	cmpl	%edx, %ecx
	jge	.L1074
	movslq	%ecx, %rcx
	cmpb	$14, (%rdi,%rcx)
	jne	.L1083
	addl	$3, %esi
.L1083:
	leal	9(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L1074
	movslq	%ecx, %rcx
	cmpb	$14, (%rdi,%rcx)
	jne	.L1084
	addl	$3, %esi
.L1084:
	leal	10(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L1074
	movslq	%ecx, %rcx
	cmpb	$14, (%rdi,%rcx)
	jne	.L1085
	addl	$3, %esi
.L1085:
	leal	11(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L1074
	movslq	%ecx, %rcx
	cmpb	$14, (%rdi,%rcx)
	jne	.L1086
	addl	$3, %esi
.L1086:
	leal	12(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L1074
	movslq	%ecx, %rcx
	cmpb	$14, (%rdi,%rcx)
	jne	.L1087
	addl	$3, %esi
.L1087:
	leal	13(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L1074
	movslq	%ecx, %rcx
	cmpb	$14, (%rdi,%rcx)
	jne	.L1088
	addl	$3, %esi
.L1088:
	addl	$14, %eax
	cmpl	%eax, %edx
	jle	.L1074
	cltq
	cmpb	$14, (%rdi,%rax)
	jne	.L1074
	addl	$3, %esi
.L1074:
	cmpl	%esi, %edx
	je	.L903
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6ReturnEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L903
.L975:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	xorl	%r8d, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L964:
	movl	$4, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	$3, %ecx
	movl	$4, %edx
	call	_ZN2v88internal8compiler18SimdScalarLowering19LowerConvertFromIntEPNS1_4NodeENS2_8SimdTypeES5_bi
	jmp	.L903
.L965:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering19LowerConvertFromIntEPNS1_4NodeENS2_8SimdTypeES5_bi
	jmp	.L903
.L966:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering21LowerConvertFromFloatEPNS1_4NodeEb
	jmp	.L903
.L967:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Int32LessThanOrEqualEv@PLT
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L930:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	xorl	%r8d, %r8d
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L939:
	movl	$5, %ecx
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering9LowerPackEPNS1_4NodeENS2_8SimdTypeES5_b
	jmp	.L903
.L943:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21Uint32LessThanOrEqualEv@PLT
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler18SimdScalarLowering14LowerCompareOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L997:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder19RoundInt32ToFloat32Ev@PLT
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler18SimdScalarLowering12LowerUnaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE
	jmp	.L903
.L976:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L908:
	call	_ZN2v88internal8compiler14S8x16ShuffleOfEPKNS1_8OperatorE@PLT
	leaq	32(%r13), %r15
	movq	%rax, %r12
	movzbl	23(%r13), %eax
	movq	%r15, %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1145
	movq	32(%r13), %rax
	leaq	16(%rax), %rcx
.L1145:
	movq	(%rcx), %rsi
	movl	%ebx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	%rax, %r8
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1146
	leaq	8(%r15), %rax
.L1147:
	movq	(%rax), %rsi
	movl	%ebx, %edx
	movq	%r14, %rdi
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal8compiler18SimdScalarLowering23GetReplacementsWithTypeEPNS1_4NodeENS2_8SimdTypeE
	movq	-120(%rbp), %r8
	movq	%rax, %r15
	movq	(%r14), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$127, %rax
	jbe	.L1328
	leaq	128(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1149:
	movzbl	(%r12), %edx
	leaq	0(,%rdx,8), %rcx
	cmpb	$15, %dl
	jbe	.L1150
	movq	-128(%r15,%rcx), %rax
.L1151:
	movq	%rax, (%rbx)
	movzbl	1(%r12), %edx
	leaq	0(,%rdx,8), %rcx
	cmpb	$15, %dl
	jbe	.L1152
	movq	-128(%r15,%rcx), %rax
.L1153:
	movq	%rax, 8(%rbx)
	movzbl	2(%r12), %edx
	leaq	0(,%rdx,8), %rcx
	cmpb	$15, %dl
	jbe	.L1154
	movq	-128(%r15,%rcx), %rax
.L1155:
	movq	%rax, 16(%rbx)
	movzbl	3(%r12), %edx
	leaq	0(,%rdx,8), %rcx
	cmpb	$15, %dl
	jbe	.L1156
	movq	-128(%r15,%rcx), %rax
.L1157:
	movq	%rax, 24(%rbx)
	movzbl	4(%r12), %edx
	leaq	0(,%rdx,8), %rcx
	cmpb	$15, %dl
	jbe	.L1158
	movq	-128(%r15,%rcx), %rax
.L1159:
	movq	%rax, 32(%rbx)
	movzbl	5(%r12), %edx
	leaq	0(,%rdx,8), %rcx
	cmpb	$15, %dl
	jbe	.L1160
	movq	-128(%r15,%rcx), %rax
.L1161:
	movq	%rax, 40(%rbx)
	movzbl	6(%r12), %edx
	leaq	0(,%rdx,8), %rcx
	cmpb	$15, %dl
	jbe	.L1162
	movq	-128(%r15,%rcx), %rax
.L1163:
	movq	%rax, 48(%rbx)
	movzbl	7(%r12), %edx
	leaq	0(,%rdx,8), %rcx
	cmpb	$15, %dl
	jbe	.L1164
	movq	-128(%r15,%rcx), %rax
.L1165:
	movq	%rax, 56(%rbx)
	movzbl	8(%r12), %edx
	leaq	0(,%rdx,8), %rcx
	cmpb	$15, %dl
	jbe	.L1166
	movq	-128(%r15,%rcx), %rax
.L1167:
	movq	%rax, 64(%rbx)
	movzbl	9(%r12), %edx
	leaq	0(,%rdx,8), %rcx
	cmpb	$15, %dl
	jbe	.L1168
	movq	-128(%r15,%rcx), %rax
.L1169:
	movq	%rax, 72(%rbx)
	movzbl	10(%r12), %edx
	leaq	0(,%rdx,8), %rcx
	cmpb	$15, %dl
	jbe	.L1170
	movq	-128(%r15,%rcx), %rax
.L1171:
	movq	%rax, 80(%rbx)
	movzbl	11(%r12), %edx
	leaq	0(,%rdx,8), %rcx
	cmpb	$15, %dl
	jbe	.L1172
	movq	-128(%r15,%rcx), %rax
.L1173:
	movq	%rax, 88(%rbx)
	movzbl	12(%r12), %edx
	leaq	0(,%rdx,8), %rcx
	cmpb	$15, %dl
	jbe	.L1174
	movq	-128(%r15,%rcx), %rax
.L1175:
	movq	%rax, 96(%rbx)
	movzbl	13(%r12), %edx
	leaq	0(,%rdx,8), %rcx
	cmpb	$15, %dl
	jbe	.L1176
	movq	-128(%r15,%rcx), %rax
.L1177:
	movq	%rax, 104(%rbx)
	movzbl	14(%r12), %edx
	leaq	0(,%rdx,8), %rcx
	cmpb	$15, %dl
	jbe	.L1178
	movq	-128(%r15,%rcx), %rax
.L1179:
	movq	%rax, 112(%rbx)
	movzbl	15(%r12), %edx
	leaq	0(,%rdx,8), %rcx
	cmpb	$15, %dl
	ja	.L1180
	movq	(%r8,%rdx,8), %rax
.L1181:
	movq	%rax, 120(%rbx)
	movq	(%r14), %rax
	movl	20(%r13), %r12d
	movq	(%rax), %rax
	andl	$16777215, %r12d
	movq	(%rax), %rdi
	salq	$4, %r12
	addq	112(%r14), %r12
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$127, %rdx
	jbe	.L1329
	leaq	128(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1183:
	movq	%rax, (%r12)
	movl	20(%r13), %eax
	movq	(%rbx), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	movq	%rdx, (%rax)
	movl	20(%r13), %eax
	movq	8(%rbx), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	movq	%rdx, 8(%rax)
	movl	20(%r13), %eax
	movq	16(%rbx), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	movq	%rdx, 16(%rax)
	movl	20(%r13), %eax
	movq	24(%rbx), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	movq	%rdx, 24(%rax)
	movl	20(%r13), %eax
	movq	32(%rbx), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	movq	%rdx, 32(%rax)
	movl	20(%r13), %eax
	movq	40(%rbx), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	movq	%rdx, 40(%rax)
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	48(%rbx), %rdx
	movq	(%rax), %rax
	movq	%rdx, 48(%rax)
	movl	20(%r13), %eax
	movq	56(%rbx), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	movq	%rdx, 56(%rax)
	movl	20(%r13), %eax
	movq	64(%rbx), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	movq	%rdx, 64(%rax)
	movl	20(%r13), %eax
	movq	72(%rbx), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	movq	%rdx, 72(%rax)
	movl	20(%r13), %eax
	movq	80(%rbx), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	movq	%rdx, 80(%rax)
	movl	20(%r13), %eax
	movq	88(%rbx), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	movq	%rdx, 88(%rax)
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	movq	96(%rbx), %rdx
	movq	%rdx, 96(%rax)
	movl	20(%r13), %eax
	movq	104(%rbx), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	movq	%rdx, 104(%rax)
	movl	20(%r13), %eax
	movq	112(%rbx), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	movq	%rdx, 112(%rax)
	movl	20(%r13), %eax
	movq	120(%rbx), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movq	(%rax), %rax
	movq	%rdx, 120(%rax)
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%r14), %rax
	movl	$16, 12(%rax)
	jmp	.L903
.L974:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L973:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32MulEv@PLT
	movl	$1, %r8d
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering13LowerBinaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorEb
	jmp	.L903
.L996:
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20RoundUint32ToFloat32Ev@PLT
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler18SimdScalarLowering12LowerUnaryOpEPNS1_4NodeENS2_8SimdTypeEPKNS1_8OperatorE
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L1325:
	movq	-200(%rbp), %rbx
	movq	-160(%rbp), %r13
	movq	(%rbx), %rax
	cmpl	%r13d, 20(%rax)
	jle	.L903
	movl	20(%rbx), %eax
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1315:
	movl	$2, %edx
	movl	$4, %esi
	movq	%r13, %rdi
	movq	-144(%rbp), %xmm0
	movhps	-184(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movl	$3, %edx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movdqa	-144(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%r15, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	addq	$8, -120(%rbp)
	movq	-120(%rbp), %rdx
	movq	%rax, -144(%rbp)
	cmpq	-176(%rbp), %rdx
	jne	.L1192
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1094:
	movq	(%rax), %rax
	leaq	16(%rax,%r12), %rax
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1319:
	movl	$24, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering12FixUpperBitsEPNS1_4NodeEi
	movq	%rax, (%r12)
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1311:
	movl	%r10d, %ecx
	movq	%r8, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L1320:
	movl	-168(%rbp), %ecx
	movq	-160(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L1322:
	movl	-168(%rbp), %r10d
	movq	-160(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%r10d, %ecx
	call	_ZN2v88internal8compiler18SimdScalarLowering11ReplaceNodeEPNS1_4NodeEPS4_i
	jmp	.L903
.L1136:
	movq	%r8, (%rcx)
	jmp	.L1137
.L1211:
	movl	$2, %ebx
	jmp	.L1185
.L1207:
	movl	$2, -168(%rbp)
	movl	$16, %esi
	jmp	.L1104
.L1180:
	movq	-128(%r15,%rcx), %rax
	jmp	.L1181
.L1178:
	movq	(%r8,%rdx,8), %rax
	jmp	.L1179
.L1176:
	movq	(%r8,%rdx,8), %rax
	jmp	.L1177
.L1174:
	movq	(%r8,%rdx,8), %rax
	jmp	.L1175
.L1172:
	movq	(%r8,%rdx,8), %rax
	jmp	.L1173
.L1170:
	movq	(%r8,%rdx,8), %rax
	jmp	.L1171
.L1168:
	movq	(%r8,%rdx,8), %rax
	jmp	.L1169
.L1166:
	movq	(%r8,%rdx,8), %rax
	jmp	.L1167
.L1164:
	movq	(%r8,%rdx,8), %rax
	jmp	.L1165
.L1162:
	movq	(%r8,%rdx,8), %rax
	jmp	.L1163
.L1160:
	movq	(%r8,%rdx,8), %rax
	jmp	.L1161
.L1158:
	movq	(%r8,%rdx,8), %rax
	jmp	.L1159
.L1156:
	movq	(%r8,%rdx,8), %rax
	jmp	.L1157
.L1154:
	movq	(%r8,%rdx,8), %rax
	jmp	.L1155
.L1152:
	movq	(%r8,%rdx,8), %rax
	jmp	.L1153
.L1150:
	movq	(%r8,%rdx,8), %rax
	jmp	.L1151
.L1093:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering15DefaultLoweringEPNS1_4NodeE
	jmp	.L903
.L1323:
	testl	%edx, %edx
	jle	.L1200
	leal	-1(%rdx), %eax
	movq	(%rdi), %rcx
	movq	16(%rdi), %r8
	cmpl	$14, %eax
	jbe	.L1201
	movl	%edx, %esi
	pxor	%xmm1, %xmm1
	pxor	%xmm7, %xmm7
	addq	%r8, %rcx
	shrl	$4, %esi
	movdqa	.LC1(%rip), %xmm5
	movq	%rcx, %rax
	pxor	%xmm6, %xmm6
	salq	$4, %rsi
	movdqa	.LC2(%rip), %xmm4
	addq	%rcx, %rsi
	.p2align 4,,10
	.p2align 3
.L1031:
	movdqu	(%rax), %xmm0
	movdqa	%xmm7, %xmm2
	movdqa	%xmm6, %xmm8
	addq	$16, %rax
	pcmpeqb	%xmm5, %xmm0
	pand	%xmm4, %xmm0
	pcmpgtb	%xmm0, %xmm2
	movdqa	%xmm0, %xmm3
	punpcklbw	%xmm2, %xmm3
	punpckhbw	%xmm2, %xmm0
	pcmpgtw	%xmm3, %xmm8
	movdqa	%xmm3, %xmm2
	punpcklwd	%xmm8, %xmm2
	punpckhwd	%xmm8, %xmm3
	paddd	%xmm2, %xmm1
	movdqa	%xmm6, %xmm2
	pcmpgtw	%xmm0, %xmm2
	paddd	%xmm3, %xmm1
	movdqa	%xmm0, %xmm3
	punpcklwd	%xmm2, %xmm3
	punpckhwd	%xmm2, %xmm0
	paddd	%xmm3, %xmm1
	paddd	%xmm0, %xmm1
	cmpq	%rax, %rsi
	jne	.L1031
	movdqa	%xmm1, %xmm0
	movl	%edx, %esi
	psrldq	$8, %xmm0
	andl	$-16, %esi
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	addl	%edx, %eax
	testb	$15, %dl
	je	.L1029
.L1030:
	movslq	%esi, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	1(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L1029
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	2(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L1029
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	3(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L1029
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	4(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L1029
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	5(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L1029
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	6(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L1029
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	7(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L1029
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	8(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L1029
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	9(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L1029
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	10(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%r8d, %edx
	jle	.L1029
	movslq	%r8d, %r8
	cmpb	$14, (%rcx,%r8)
	jne	.L1043
	addl	$3, %eax
.L1043:
	leal	11(%rsi), %r8d
	cmpl	%r8d, %edx
	jle	.L1029
	movslq	%r8d, %r8
	cmpb	$14, (%rcx,%r8)
	jne	.L1044
	addl	$3, %eax
.L1044:
	leal	12(%rsi), %r8d
	cmpl	%r8d, %edx
	jle	.L1029
	movslq	%r8d, %r8
	cmpb	$14, (%rcx,%r8)
	jne	.L1045
	addl	$3, %eax
.L1045:
	leal	13(%rsi), %r8d
	cmpl	%r8d, %edx
	jle	.L1029
	movslq	%r8d, %r8
	cmpb	$14, (%rcx,%r8)
	jne	.L1046
	addl	$3, %eax
.L1046:
	addl	$14, %esi
	cmpl	%esi, %edx
	jle	.L1029
	movslq	%esi, %rsi
	cmpb	$14, (%rcx,%rsi)
	jne	.L1029
	addl	$3, %eax
.L1029:
	movl	%eax, 136(%r14)
	movl	8(%rdi), %ecx
	jmp	.L1028
.L1140:
	movq	32(%r13), %rax
	addq	$32, %rax
	jmp	.L1141
.L1138:
	movq	16(%rax), %rcx
	addq	$24, %rax
	movl	20(%rcx), %ecx
	andl	$16777215, %ecx
	salq	$4, %rcx
	movq	(%rsi,%rcx), %rcx
	movq	%rcx, -176(%rbp)
	jmp	.L1139
.L1146:
	movq	32(%r13), %rax
	addq	$24, %rax
	jmp	.L1147
.L1327:
	testl	%edx, %edx
	jle	.L1198
	leal	-1(%rdx), %eax
	movq	(%rdi), %rcx
	movq	16(%rdi), %r8
	cmpl	$14, %eax
	jbe	.L1199
	movl	%edx, %esi
	pxor	%xmm1, %xmm1
	pxor	%xmm7, %xmm7
	addq	%r8, %rcx
	shrl	$4, %esi
	movdqa	.LC1(%rip), %xmm5
	movq	%rcx, %rax
	pxor	%xmm6, %xmm6
	salq	$4, %rsi
	movdqa	.LC2(%rip), %xmm4
	addq	%rcx, %rsi
	.p2align 4,,10
	.p2align 3
.L1008:
	movdqu	(%rax), %xmm0
	movdqa	%xmm7, %xmm2
	movdqa	%xmm6, %xmm8
	addq	$16, %rax
	pcmpeqb	%xmm5, %xmm0
	pand	%xmm4, %xmm0
	pcmpgtb	%xmm0, %xmm2
	movdqa	%xmm0, %xmm3
	punpcklbw	%xmm2, %xmm3
	punpckhbw	%xmm2, %xmm0
	pcmpgtw	%xmm3, %xmm8
	movdqa	%xmm3, %xmm2
	punpcklwd	%xmm8, %xmm2
	punpckhwd	%xmm8, %xmm3
	paddd	%xmm2, %xmm1
	movdqa	%xmm6, %xmm2
	pcmpgtw	%xmm0, %xmm2
	paddd	%xmm3, %xmm1
	movdqa	%xmm0, %xmm3
	punpcklwd	%xmm2, %xmm3
	punpckhwd	%xmm2, %xmm0
	paddd	%xmm3, %xmm1
	paddd	%xmm0, %xmm1
	cmpq	%rax, %rsi
	jne	.L1008
	movdqa	%xmm1, %xmm0
	movl	%edx, %esi
	psrldq	$8, %xmm0
	andl	$-16, %esi
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	addl	%edx, %eax
	testb	$15, %dl
	je	.L1006
.L1007:
	movslq	%esi, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	1(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%edx, %r8d
	jge	.L1006
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	2(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%edx, %r8d
	jge	.L1006
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	3(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%edx, %r8d
	jge	.L1006
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	4(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%edx, %r8d
	jge	.L1006
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	5(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%edx, %r8d
	jge	.L1006
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	6(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%edx, %r8d
	jge	.L1006
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	7(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%edx, %r8d
	jge	.L1006
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	8(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%edx, %r8d
	jge	.L1006
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	9(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%edx, %r8d
	jge	.L1006
	movslq	%r8d, %r8
	leal	3(%rax), %r9d
	cmpb	$14, (%rcx,%r8)
	leal	10(%rsi), %r8d
	cmove	%r9d, %eax
	cmpl	%edx, %r8d
	jge	.L1006
	movslq	%r8d, %r8
	cmpb	$14, (%rcx,%r8)
	jne	.L1020
	addl	$3, %eax
.L1020:
	leal	11(%rsi), %r8d
	cmpl	%edx, %r8d
	jge	.L1006
	movslq	%r8d, %r8
	cmpb	$14, (%rcx,%r8)
	jne	.L1021
	addl	$3, %eax
.L1021:
	leal	12(%rsi), %r8d
	cmpl	%edx, %r8d
	jge	.L1006
	movslq	%r8d, %r8
	cmpb	$14, (%rcx,%r8)
	jne	.L1022
	addl	$3, %eax
.L1022:
	leal	13(%rsi), %r8d
	cmpl	%edx, %r8d
	jge	.L1006
	movslq	%r8d, %r8
	cmpb	$14, (%rcx,%r8)
	jne	.L1023
	addl	$3, %eax
.L1023:
	addl	$14, %esi
	cmpl	%edx, %esi
	jge	.L1006
	movslq	%esi, %rsi
	cmpb	$14, (%rcx,%rsi)
	jne	.L1006
	addl	$3, %eax
.L1006:
	movl	%eax, 136(%r14)
	movl	8(%rdi), %ecx
	jmp	.L1005
.L1130:
	movq	24(%rsi), %r8
	movq	16(%rsi), %rsi
	jmp	.L1131
.L1314:
	movq	%xmm5, -144(%rbp)
	jmp	.L1188
.L1208:
	movl	$4, -168(%rbp)
	movl	$32, %esi
	jmp	.L1104
.L1212:
	movl	$4, %ebx
	jmp	.L1185
.L1134:
	movq	(%r12), %rax
	movq	%rax, (%rdx)
	movq	8(%r12), %rax
	movq	%rax, 8(%rdx)
	cmpl	$2, %r10d
	je	.L1135
	movq	16(%r12), %rax
	movq	%rax, 16(%rdx)
	movq	24(%r12), %rax
	movq	%rax, 24(%rdx)
	cmpl	$4, %r10d
	je	.L1135
	movq	32(%r12), %rax
	movq	%rax, 32(%rdx)
	movq	40(%r12), %rax
	movq	%rax, 40(%rdx)
	cmpl	$6, %r10d
	je	.L1135
	movq	48(%r12), %rax
	movq	%rax, 48(%rdx)
	movq	56(%r12), %rax
	movq	%rax, 56(%rdx)
	cmpl	$8, %r10d
	je	.L1135
	movq	64(%r12), %rax
	movq	%rax, 64(%rdx)
	movq	72(%r12), %rax
	movq	%rax, 72(%rdx)
	cmpl	$10, %r10d
	je	.L1135
	movq	80(%r12), %rax
	movq	%rax, 80(%rdx)
	movq	88(%r12), %rax
	movq	%rax, 88(%rdx)
	cmpl	$12, %r10d
	je	.L1135
	movq	96(%r12), %rax
	movq	%rax, 96(%rdx)
	movq	104(%r12), %rax
	movq	%rax, 104(%rdx)
	cmpl	$14, %r10d
	je	.L1135
	movq	112(%r12), %rax
	movq	%rax, 112(%rdx)
	movq	120(%r12), %rax
	movq	%rax, 120(%rdx)
	jmp	.L1135
.L1321:
	movq	%r15, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-120(%rbp), %r10d
	movq	%rax, -160(%rbp)
	jmp	.L1116
.L1328:
	movl	$128, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %r8
	movq	%rax, %rbx
	jmp	.L1149
.L1316:
	movq	%r15, %rsi
	movl	%r10d, -144(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-144(%rbp), %r10d
	movq	%rax, %r12
	jmp	.L1128
.L1318:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, -160(%rbp)
	jmp	.L1107
.L1324:
	movq	%r15, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-120(%rbp), %r10d
	movq	%rax, -184(%rbp)
	movq	%rax, %r12
	jmp	.L1143
.L1329:
	movl	$128, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1183
.L1317:
	movq	%r15, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-144(%rbp), %r10d
	movq	-128(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L1133
.L1310:
	movq	%r15, %rsi
	movl	%r10d, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-120(%rbp), %r10d
	movq	%rax, %rdx
	jmp	.L1122
.L1313:
	movq	%r15, %rsi
	movl	%r10d, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-120(%rbp), %r10d
	movq	%rax, -192(%rbp)
	jmp	.L1187
.L1213:
	movl	$8, %ebx
	jmp	.L1185
.L1209:
	movl	$8, -168(%rbp)
	movl	$64, %esi
	jmp	.L1104
.L1205:
	movl	%edx, %esi
	xorl	%eax, %eax
	jmp	.L1072
.L1202:
	movl	%edx, %r15d
	jmp	.L1050
.L1203:
	movl	%edx, %r15d
	xorl	%eax, %eax
	addq	%rcx, %rsi
	jmp	.L1051
.L1309:
	call	__stack_chk_fail@PLT
.L1198:
	movl	%edx, %eax
	jmp	.L1006
.L1200:
	movl	%edx, %eax
	jmp	.L1029
.L1199:
	movl	%edx, %eax
	xorl	%esi, %esi
	addq	%r8, %rcx
	jmp	.L1007
.L1201:
	movl	%edx, %eax
	xorl	%esi, %esi
	addq	%r8, %rcx
	jmp	.L1030
.L1204:
	leaq	-96(%rbp), %rbx
	movl	$1, %r9d
	leaq	-112(%rbp), %r12
	movq	%r13, -120(%rbp)
	movq	%rbx, %rax
	movq	%r12, %r13
	movq	%r9, %rbx
	movq	%rax, %r9
.L1070:
	movq	(%r14), %rax
	leal	(%r15,%rbx), %esi
	xorl	%edx, %edx
	movq	%r9, -128(%rbp)
	movq	(%rax), %r12
	movq	8(%rax), %rdi
	movq	8(%r12), %rcx
	movq	%rcx, -144(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	-144(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rcx, -112(%rbp)
	movq	%r13, %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-128(%rbp), %r9
	movq	%rax, (%r9,%rbx,8)
	addq	$1, %rbx
	cmpq	$4, %rbx
	jne	.L1070
	movq	-120(%rbp), %r13
	movq	%r9, %rbx
	jmp	.L1071
.L1105:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE14597:
	.size	_ZN2v88internal8compiler18SimdScalarLowering9LowerNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler18SimdScalarLowering9LowerNodeEPNS1_4NodeE
	.section	.text._ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb,"axG",@progbits,_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	.type	_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb, @function
_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb:
.LFB16455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	88(%rdi), %rax
	movq	56(%rdi), %rsi
	movq	24(%rdi), %rbx
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	1(%r14,%rcx), %r8
	leaq	(%r8,%r8), %rcx
	cmpq	%rcx, %rbx
	jbe	.L1331
	subq	%r8, %rbx
	shrq	%rbx
	salq	$3, %rbx
	testb	%dl, %dl
	leaq	(%rbx,%r14,8), %rcx
	cmovne	%rcx, %rbx
	addq	$8, %rax
	addq	16(%rdi), %rbx
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	%rbx, %rsi
	jbe	.L1333
	cmpq	%rax, %rsi
	je	.L1334
	movq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1331:
	cmpq	%r14, %rbx
	movq	%r14, %rax
	movq	(%rdi), %rdi
	cmovnb	%rbx, %rax
	movq	16(%rdi), %r15
	leaq	2(%rbx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L1341
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L1336:
	movq	%rcx, %rax
	subq	%r8, %rax
	shrq	%rax
	salq	$3, %rax
	testb	%dl, %dl
	leaq	(%rax,%r14,8), %rsi
	cmovne	%rsi, %rax
	movq	56(%r12), %rsi
	leaq	(%r15,%rax), %rbx
	movq	88(%r12), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1338
	subq	%rsi, %rdx
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L1338:
	movq	24(%r12), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L1339
	movq	16(%r12), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L1339:
	movq	%r15, 16(%r12)
	movq	%rcx, 24(%r12)
.L1334:
	movq	%rbx, 56(%r12)
	movq	(%rbx), %rax
	movq	(%rbx), %xmm0
	addq	%r13, %rbx
	addq	$512, %rax
	movq	%rbx, 88(%r12)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%r12)
	movq	(%rbx), %rax
	movq	%rax, 72(%r12)
	addq	$512, %rax
	movq	%rax, 80(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1333:
	.cfi_restore_state
	cmpq	%rax, %rsi
	je	.L1334
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1341:
	movl	%edx, -68(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	movl	-68(%rbp), %edx
	movq	%rax, %r15
	jmp	.L1336
	.cfi_endproc
.LFE16455:
	.size	_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb, .-_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	.section	.rodata._ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_:
.LFB15940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rax
	cmpq	40(%rdi), %rax
	je	.L1343
	movq	(%rsi), %rcx
	movl	8(%rsi), %edx
	movq	%rcx, -16(%rax)
	movl	%edx, -8(%rax)
	subq	$16, 32(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1343:
	.cfi_restore_state
	movq	56(%rdi), %r13
	movq	88(%rdi), %rdx
	subq	%r13, %rdx
	sarq	$3, %rdx
	subq	$1, %rdx
	movq	%rdx, %rcx
	movq	64(%rdi), %rdx
	subq	72(%rdi), %rdx
	salq	$5, %rcx
	sarq	$4, %rdx
	addq	%rcx, %rdx
	movq	48(%rdi), %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$134217727, %rax
	je	.L1355
	cmpq	16(%rdi), %r13
	je	.L1356
.L1346:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L1347
	cmpq	$31, 8(%rax)
	ja	.L1357
.L1347:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1358
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1348:
	movq	%rax, -8(%r13)
	movq	56(%rbx), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 56(%rbx)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 40(%rbx)
	movq	%rdx, 48(%rbx)
	leaq	496(%rax), %rdx
	movq	%rdx, 32(%rbx)
	movq	(%r12), %rcx
	movl	8(%r12), %edx
	movq	%rcx, 496(%rax)
	movl	%edx, 504(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1356:
	.cfi_restore_state
	movl	$1, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	movq	56(%rbx), %r13
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1357:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L1348
	.p2align 4,,10
	.p2align 3
.L1358:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1348
.L1355:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE15940:
	.size	_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_
	.section	.text._ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_:
.LFB15933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %rcx
	movq	64(%rdi), %rax
	leaq	-16(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L1360
	movq	(%rsi), %rcx
	movl	8(%rsi), %edx
	movq	%rcx, (%rax)
	movl	%edx, 8(%rax)
	addq	$16, 64(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1360:
	.cfi_restore_state
	movq	88(%rdi), %r13
	subq	72(%rdi), %rax
	sarq	$4, %rax
	movq	%r13, %rdx
	subq	56(%rdi), %rdx
	sarq	$3, %rdx
	subq	$1, %rdx
	salq	$5, %rdx
	addq	%rax, %rdx
	movq	48(%rdi), %rax
	subq	32(%rdi), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$134217727, %rax
	je	.L1372
	movq	24(%rdi), %rsi
	movq	%r13, %rax
	subq	16(%rdi), %rax
	sarq	$3, %rax
	subq	%rax, %rsi
	cmpq	$1, %rsi
	jbe	.L1373
.L1363:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L1364
	cmpq	$31, 8(%rax)
	ja	.L1374
.L1364:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1375
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1365:
	movq	%rax, 8(%r13)
	movl	8(%r12), %edx
	movq	(%r12), %rcx
	movq	64(%rbx), %rax
	movl	%edx, 8(%rax)
	movq	%rcx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1373:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	movq	88(%rbx), %r13
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L1374:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1375:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1365
.L1372:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE15933:
	.size	_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.section	.text._ZN2v88internal8compiler18SimdScalarLowering10LowerGraphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18SimdScalarLowering10LowerGraphEv
	.type	_ZN2v88internal8compiler18SimdScalarLowering10LowerGraphEv, @function
_ZN2v88internal8compiler18SimdScalarLowering10LowerGraphEv:
.LFB14573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	16(%rdi), %r12
	movq	%r13, %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	movl	$0, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rdx
	movl	8(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 16(%rdx)
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rax
	movl	20(%rax), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	112(%rbx), %rax
	movb	$3, 8(%rax)
	movq	80(%rbx), %rcx
	cmpq	%rcx, 48(%rbx)
	jne	.L1377
	jmp	.L1376
	.p2align 4,,10
	.p2align 3
.L1382:
	leal	1(%rdx), %eax
	leaq	32(%r8), %rcx
	salq	$3, %rdx
	movl	%eax, -8(%rsi)
	movzbl	23(%r8), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1388
	addq	%rcx, %rdx
	movl	8(%rbx), %eax
	movq	(%rdx), %r14
	movl	16(%r14), %edx
	cmpl	%edx, %eax
	ja	.L1390
.L1405:
	cmpb	%al, %dl
	je	.L1390
.L1387:
	movq	80(%rbx), %rcx
	cmpq	48(%rbx), %rcx
	je	.L1376
.L1377:
	movq	88(%rbx), %rdi
	movq	104(%rbx), %r9
	movq	%rcx, %rsi
	cmpq	%rcx, %rdi
	je	.L1403
.L1379:
	movq	-16(%rsi), %r8
	movslq	-8(%rsi), %rdx
	movzbl	23(%r8), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1381
	movq	32(%r8), %rax
	movl	8(%rax), %eax
.L1381:
	cmpl	%eax, %edx
	jne	.L1382
	cmpq	%rcx, %rdi
	je	.L1383
	subq	$16, %rcx
	movq	%rcx, 80(%rbx)
.L1384:
	movl	8(%rbx), %eax
	movq	-16(%rsi), %rdx
	movq	%rbx, %rdi
	addl	$2, %eax
	movl	%eax, 16(%rdx)
	movq	-16(%rsi), %rsi
	call	_ZN2v88internal8compiler18SimdScalarLowering9LowerNodeEPNS1_4NodeE
	movq	80(%rbx), %rcx
	cmpq	48(%rbx), %rcx
	jne	.L1377
.L1376:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1404
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1403:
	.cfi_restore_state
	movq	-8(%r9), %rax
	leaq	512(%rax), %rsi
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1388:
	movq	32(%r8), %rax
	leaq	16(%rax,%rdx), %rdx
	movl	8(%rbx), %eax
	movq	(%rdx), %r14
	movl	16(%r14), %edx
	cmpl	%edx, %eax
	jbe	.L1405
.L1390:
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering14SetLoweredTypeEPNS1_4NodeES4_
	movq	(%r14), %rax
	movzwl	16(%rax), %eax
	cmpl	$35, %eax
	je	.L1406
	movq	%r14, -64(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$0, -56(%rbp)
	cmpl	$1, %eax
	je	.L1397
	cmpl	$36, %eax
	je	.L1397
	call	_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
.L1392:
	movl	8(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 16(%r14)
	jmp	.L1387
	.p2align 4,,10
	.p2align 3
.L1397:
	call	_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_
	jmp	.L1392
	.p2align 4,,10
	.p2align 3
.L1383:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L1385
	cmpq	$32, 8(%rax)
	ja	.L1386
.L1385:
	movq	$32, 8(%rcx)
	movq	24(%rbx), %rax
	movq	%rax, (%rcx)
	movq	104(%rbx), %r9
	movq	%rcx, 24(%rbx)
.L1386:
	leaq	-8(%r9), %rax
	movq	%rax, 104(%rbx)
	movq	-8(%r9), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 88(%rbx)
	addq	$496, %rax
	movq	%rdx, 96(%rbx)
	movq	%rax, 80(%rbx)
	jmp	.L1384
	.p2align 4,,10
	.p2align 3
.L1406:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18SimdScalarLowering21PreparePhiReplacementEPNS1_4NodeE
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, -64(%rbp)
	movl	$0, -56(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler18SimdScalarLowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_
	jmp	.L1392
.L1404:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14573:
	.size	_ZN2v88internal8compiler18SimdScalarLowering10LowerGraphEv, .-_ZN2v88internal8compiler18SimdScalarLowering10LowerGraphEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler18SimdScalarLoweringC2EPNS1_12MachineGraphEPNS0_9SignatureINS0_21MachineRepresentationEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler18SimdScalarLoweringC2EPNS1_12MachineGraphEPNS0_9SignatureINS0_21MachineRepresentationEEE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler18SimdScalarLoweringC2EPNS1_12MachineGraphEPNS0_9SignatureINS0_21MachineRepresentationEEE:
.LFB17358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE17358:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler18SimdScalarLoweringC2EPNS1_12MachineGraphEPNS0_9SignatureINS0_21MachineRepresentationEEE, .-_GLOBAL__sub_I__ZN2v88internal8compiler18SimdScalarLoweringC2EPNS1_12MachineGraphEPNS0_9SignatureINS0_21MachineRepresentationEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler18SimdScalarLoweringC2EPNS1_12MachineGraphEPNS0_9SignatureINS0_21MachineRepresentationEEE
	.globl	_ZN2v88internal8compiler18SimdScalarLowering12kLaneOffsetsE
	.section	.rodata._ZN2v88internal8compiler18SimdScalarLowering12kLaneOffsetsE,"a"
	.align 32
	.type	_ZN2v88internal8compiler18SimdScalarLowering12kLaneOffsetsE, @object
	.size	_ZN2v88internal8compiler18SimdScalarLowering12kLaneOffsetsE, 64
_ZN2v88internal8compiler18SimdScalarLowering12kLaneOffsetsE:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.align 16
.LC2:
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.align 16
.LC4:
	.quad	0
	.quad	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC6:
	.long	4292870144
	.long	1106247679
	.align 8
.LC7:
	.long	4290772992
	.long	1105199103
	.align 8
.LC9:
	.long	0
	.long	-1042284544
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC10:
	.long	1065353216
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
