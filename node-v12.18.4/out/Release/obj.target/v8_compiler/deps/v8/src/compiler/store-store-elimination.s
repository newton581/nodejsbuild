	.file	"store-store-elimination.cc"
	.text
	.section	.text._ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder14HasBeenVisitedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder14HasBeenVisitedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder14HasBeenVisitedEPNS1_4NodeE, @function
_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder14HasBeenVisitedEPNS1_4NodeE:
.LFB10923:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %eax
	movq	176(%rdi), %rdx
	andl	$16777215, %eax
	cmpq	$0, (%rdx,%rax,8)
	setne	%al
	ret
	.cfi_endproc
.LFE10923:
	.size	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder14HasBeenVisitedEPNS1_4NodeE, .-_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder14HasBeenVisitedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder23CannotObserveStoreFieldEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder23CannotObserveStoreFieldEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder23CannotObserveStoreFieldEPNS1_4NodeE, @function
_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder23CannotObserveStoreFieldEPNS1_4NodeE:
.LFB10929:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movzwl	16(%rax), %ecx
	movl	%ecx, %eax
	andl	$-3, %eax
	cmpw	$429, %ax
	sete	%al
	cmpw	$241, %cx
	sete	%dl
	orb	%dl, %al
	jne	.L3
	movl	$1, %eax
	cmpl	$36, %ecx
	jne	.L7
.L3:
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	cmpl	$246, %ecx
	sete	%al
	cmpl	$510, %ecx
	sete	%dl
	orl	%edx, %eax
	cmpl	$56, %ecx
	sete	%dl
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE10929:
	.size	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder23CannotObserveStoreFieldEPNS1_4NodeE, .-_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder23CannotObserveStoreFieldEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21StoreStoreElimination16UnobservablesSetC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21StoreStoreElimination16UnobservablesSetC2Ev
	.type	_ZN2v88internal8compiler21StoreStoreElimination16UnobservablesSetC2Ev, @function
_ZN2v88internal8compiler21StoreStoreElimination16UnobservablesSetC2Ev:
.LFB10934:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	ret
	.cfi_endproc
.LFE10934:
	.size	_ZN2v88internal8compiler21StoreStoreElimination16UnobservablesSetC2Ev, .-_ZN2v88internal8compiler21StoreStoreElimination16UnobservablesSetC2Ev
	.globl	_ZN2v88internal8compiler21StoreStoreElimination16UnobservablesSetC1Ev
	.set	_ZN2v88internal8compiler21StoreStoreElimination16UnobservablesSetC1Ev,_ZN2v88internal8compiler21StoreStoreElimination16UnobservablesSetC2Ev
	.section	.text._ZN2v88internal8compiler21StoreStoreElimination16UnobservablesSet12VisitedEmptyEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21StoreStoreElimination16UnobservablesSet12VisitedEmptyEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler21StoreStoreElimination16UnobservablesSet12VisitedEmptyEPNS0_4ZoneE, @function
_ZN2v88internal8compiler21StoreStoreElimination16UnobservablesSet12VisitedEmptyEPNS0_4ZoneE:
.LFB10936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$55, %rdx
	jbe	.L13
	leaq	56(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L11:
	leaq	16(%rax), %rdx
	movq	%rdi, (%rax)
	movl	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rdx, 32(%rax)
	movq	%rdx, 40(%rax)
	movq	$0, 48(%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movl	$56, %esi
	movq	%rdi, -8(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-8(%rbp), %rdi
	jmp	.L11
	.cfi_endproc
.LFE10936:
	.size	_ZN2v88internal8compiler21StoreStoreElimination16UnobservablesSet12VisitedEmptyEPNS0_4ZoneE, .-_ZN2v88internal8compiler21StoreStoreElimination16UnobservablesSet12VisitedEmptyEPNS0_4ZoneE
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB12235:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L22
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L16:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L16
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE12235:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_:
.LFB12246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L39
	movq	(%rsi), %rsi
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L46:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L28
.L47:
	movq	%rax, %r12
.L27:
	movq	32(%r12), %rcx
	cmpq	%rcx, %rsi
	jb	.L46
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L47
.L28:
	testb	%dl, %dl
	jne	.L26
	cmpq	%rcx, %rsi
	jbe	.L32
.L37:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L48
.L33:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$39, %rax
	jbe	.L49
	leaq	40(%r13), %rax
	movq	%rax, 16(%rdi)
.L35:
	movq	(%r14), %rax
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movq	%r15, %r12
.L26:
	cmpq	%r12, 32(%rbx)
	je	.L37
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r14), %rcx
	cmpq	%rcx, 32(%rax)
	jb	.L37
	movq	%rax, %r12
.L32:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movq	32(%r12), %rax
	cmpq	%rax, (%r14)
	setb	%r8b
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$40, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L35
	.cfi_endproc
.LFE12246:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_:
.LFB12294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L67
	movl	(%rsi), %ecx
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L78:
	je	.L77
.L55:
	movq	24(%r12), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	je	.L53
.L79:
	movq	%rax, %r12
.L52:
	movl	32(%r12), %edx
	cmpl	%edx, %ecx
	jnb	.L78
	movq	16(%r12), %rax
	movl	$1, %esi
.L81:
	testq	%rax, %rax
	jne	.L79
.L53:
	testb	%sil, %sil
	jne	.L51
	movq	%r12, %rsi
	cmpl	%edx, %ecx
	ja	.L64
	cmpl	%edx, %ecx
	je	.L80
.L59:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movl	4(%r13), %eax
	cmpl	%eax, 36(%r12)
	jbe	.L55
	movq	16(%r12), %rax
	movl	$1, %esi
	jmp	.L81
.L84:
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L82
.L60:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$39, %rax
	jbe	.L83
	leaq	40(%r14), %rax
	movq	%rax, 16(%rdi)
.L62:
	movq	0(%r13), %rax
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r14, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%r14)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	%r15, %r12
.L51:
	cmpq	32(%rbx), %r12
	je	.L64
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	0(%r13), %ecx
	movl	32(%rax), %edx
	cmpl	%ecx, %edx
	jb	.L64
	movq	%r12, %rsi
	movq	%rax, %r12
	cmpl	%edx, %ecx
	jne	.L59
.L80:
	movl	36(%r12), %eax
	cmpl	%eax, 4(%r13)
	jbe	.L59
	testq	%rsi, %rsi
	jne	.L84
	xorl	%r12d, %r12d
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L82:
	movl	32(%r12), %eax
	cmpl	%eax, 0(%r13)
	jb	.L60
	movl	$0, %r8d
	jne	.L60
	xorl	%r8d, %r8d
	movl	4(%r13), %eax
	cmpl	%eax, 36(%r12)
	seta	%r8b
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$40, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r14
	jmp	.L62
	.cfi_endproc
.LFE12294:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, .-_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.section	.rodata._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.type	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, @function
_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_:
.LFB12603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$3, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L103
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L104
.L87:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L95
	cmpq	$63, 8(%rax)
	ja	.L105
.L95:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L106
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L96:
	movq	%rax, 8(%r13)
	movq	(%r12), %rdx
	movq	64(%rbx), %rax
	movq	%rdx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L107
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L108
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L92:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L93
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L93:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L94
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L94:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L90:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L105:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L107:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L89
	cmpq	%r13, %rsi
	je	.L90
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L89:
	cmpq	%r13, %rsi
	je	.L90
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L106:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L92
.L103:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12603:
	.size	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, .-_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.section	.text._ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder14MarkForRevisitEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder14MarkForRevisitEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder14MarkForRevisitEPNS1_4NodeE, @function
_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder14MarkForRevisitEPNS1_4NodeE:
.LFB10922:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	20(%rsi), %ecx
	movq	128(%rdi), %rdi
	movq	%rsi, -24(%rbp)
	movl	$1, %esi
	movl	%ecx, %edx
	salq	%cl, %rsi
	andl	$16777215, %edx
	shrq	$6, %rdx
	testq	%rsi, (%rdi,%rdx,8)
	je	.L114
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movq	104(%rbx), %rdi
	movq	88(%rbx), %rcx
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rcx
	je	.L111
	movq	%rax, (%rcx)
	addq	$8, 88(%rbx)
.L112:
	movq	-24(%rbp), %rax
	movl	$1, %edx
	movl	20(%rax), %ecx
	movq	%rcx, %rax
	salq	%cl, %rdx
	shrq	$3, %rax
	andl	$2097144, %eax
	addq	128(%rbx), %rax
	orq	%rdx, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	leaq	-24(%rbp), %rsi
	leaq	24(%rbx), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L112
	.cfi_endproc
.LFE10922:
	.size	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder14MarkForRevisitEPNS1_4NodeE, .-_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder14MarkForRevisitEPNS1_4NodeE
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB12641:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L123
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L117:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L117
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE12641:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE7_M_copyINSB_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSF_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE7_M_copyINSB_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSF_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE7_M_copyINSB_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSF_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE7_M_copyINSB_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSF_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE7_M_copyINSB_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSF_PSt18_Rb_tree_node_baseRT_:
.LFB12650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rcx), %r15
	testq	%r15, %r15
	je	.L127
	movq	8(%r15), %rax
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	je	.L128
	cmpq	24(%rax), %r15
	je	.L189
	movq	$0, 16(%rax)
.L134:
	movq	32(%rbx), %rax
	movq	%rax, 32(%r15)
	movl	(%rbx), %eax
	movq	$0, 16(%r15)
	movl	%eax, (%r15)
	movq	$0, 24(%r15)
	movq	%rdx, 8(%r15)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L135
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE7_M_copyINSB_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSF_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r15)
.L135:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L126
	movq	8(%r14), %rbx
	movq	%r15, %rdx
	testq	%rbx, %rbx
	je	.L137
.L191:
	movq	8(%rbx), %rax
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.L138
	cmpq	24(%rax), %rbx
	je	.L190
	movq	$0, 16(%rax)
.L144:
	movq	32(%r12), %rax
	movq	%rax, 32(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%rdx)
	movq	%rdx, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L145
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE7_M_copyINSB_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSF_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L126
.L146:
	movq	%rbx, %rdx
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	jne	.L191
.L137:
	movq	16(%r14), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$39, %rax
	jbe	.L192
	leaq	40(%rbx), %rax
	movq	%rax, 16(%rdi)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L145:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L146
.L126:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movq	$0, (%r14)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L190:
	movq	$0, 24(%rax)
	movq	8(%r14), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L144
	movq	%rcx, 8(%r14)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L141
	.p2align 4,,10
	.p2align 3
.L142:
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L142
.L141:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L144
	movq	%rax, 8(%r14)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L127:
	movq	16(%rcx), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$39, %rax
	jbe	.L193
	leaq	40(%r15), %rax
	movq	%rax, 16(%rdi)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L128:
	movq	$0, (%rcx)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L189:
	movq	$0, 24(%rax)
	movq	8(%rcx), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L134
	movq	%rcx, 8(%r14)
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L131
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%rax, 8(%r14)
	movq	%rax, %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L132
.L131:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L134
	movq	%rax, 8(%r14)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L192:
	movl	$40, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L144
.L193:
	movl	$40, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L134
	.cfi_endproc
.LFE12650:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE7_M_copyINSB_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSF_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE7_M_copyINSB_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSF_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet3AddENS2_17UnobservableStoreEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet3AddENS2_17UnobservableStoreEPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet3AddENS2_17UnobservableStoreEPNS0_4ZoneE, @function
_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet3AddENS2_17UnobservableStoreEPNS0_4ZoneE:
.LFB10938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L195
	movl	-72(%rbp), %edi
	movl	-68(%rbp), %r8d
	movq	%rsi, %rcx
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L235:
	je	.L234
.L199:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L197
.L196:
	cmpl	%edi, 32(%rax)
	jnb	.L235
	movq	24(%rax), %rax
.L238:
	testq	%rax, %rax
	jne	.L196
.L197:
	cmpq	%rcx, %rsi
	je	.L195
	cmpl	32(%rcx), %edi
	jnb	.L236
.L195:
	movq	16(%rdx), %rbx
	movq	24(%rdx), %rax
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L237
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdx)
.L202:
	movq	%rdx, (%rbx)
	leaq	16(%rbx), %rdx
	movl	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	%rdx, 32(%rbx)
	movq	%rdx, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	(%r12), %r12
	cmpq	%rbx, %r12
	je	.L203
	movq	24(%r12), %rsi
	pxor	%xmm0, %xmm0
	movq	%rbx, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	testq	%rsi, %rsi
	je	.L203
	leaq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE7_M_copyINSB_20_Reuse_or_alloc_nodeEEEPSt13_Rb_tree_nodeIS4_EPKSF_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L205
	movq	%rcx, 32(%rbx)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L206:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L206
	movq	-64(%rbp), %r13
	movq	-48(%rbp), %r14
	movq	%rcx, 40(%rbx)
	movq	48(%r12), %rdx
	movq	%rax, 24(%rbx)
	movq	%rdx, 48(%rbx)
	testq	%r13, %r13
	je	.L203
.L210:
	movq	24(%r13), %r12
	testq	%r12, %r12
	je	.L208
.L209:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L209
.L208:
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L210
.L203:
	leaq	-72(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	%rbx, %rax
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L234:
	cmpl	36(%rax), %r8d
	jbe	.L199
	movq	24(%rax), %rax
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L236:
	movl	36(%rcx), %eax
	je	.L239
.L201:
	movq	(%r12), %rax
.L211:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L240
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	cmpl	%eax, %r8d
	jb	.L195
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L237:
	movq	%rdx, %rdi
	movl	$56, %esi
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L202
.L240:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10938:
	.size	_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet3AddENS2_17UnobservableStoreEPNS0_4ZoneE, .-_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet3AddENS2_17UnobservableStoreEPNS0_4ZoneE
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_:
.LFB12654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	24(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L259
	movl	(%rsi), %r8d
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L261:
	je	.L260
.L247:
	movq	24(%rbx), %rax
	xorl	%r9d, %r9d
	testq	%rax, %rax
	je	.L245
.L262:
	movq	%rax, %rbx
.L244:
	movl	32(%rbx), %ecx
	cmpl	%r8d, %ecx
	jbe	.L261
	movq	16(%rbx), %rax
	movl	$1, %r9d
.L264:
	testq	%rax, %rax
	jne	.L262
.L245:
	movq	%rbx, %rdx
	testb	%r9b, %r9b
	jne	.L243
	cmpl	%r8d, %ecx
	jb	.L252
.L265:
	je	.L263
.L253:
	movq	%rbx, %rax
	xorl	%edx, %edx
.L251:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	movl	36(%rbx), %eax
	cmpl	%eax, 4(%r12)
	jnb	.L247
	movq	16(%rbx), %rax
	movl	$1, %r9d
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L259:
	leaq	16(%rdi), %rbx
.L243:
	xorl	%eax, %eax
	movq	%rbx, %rdx
	cmpq	32(%rdi), %rbx
	je	.L251
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%r12), %r8d
	movq	%rbx, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %rbx
	cmpl	%r8d, %ecx
	jnb	.L265
.L252:
	xorl	%eax, %eax
.L266:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	movl	36(%rbx), %eax
	cmpl	%eax, 4(%r12)
	jbe	.L253
	xorl	%eax, %eax
	jmp	.L266
	.cfi_endproc
.LFE12654:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_, .-_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_
	.section	.text._ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet16RemoveSameOffsetEjPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet16RemoveSameOffsetEjPNS0_4ZoneE
	.type	_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet16RemoveSameOffsetEjPNS0_4ZoneE, @function
_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet16RemoveSameOffsetEjPNS0_4ZoneE:
.LFB10941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdx), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdx), %rax
	subq	%r12, %rax
	cmpq	$55, %rax
	jbe	.L288
	leaq	56(%r12), %rax
	movq	%rax, 16(%rdx)
.L269:
	leaq	16(%r12), %rax
	movq	%rdx, (%r12)
	movl	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	%rax, 40(%r12)
	movq	$0, 48(%r12)
	movq	(%r15), %rbx
	leaq	-64(%rbp), %r15
	movq	%rax, -72(%rbp)
	movq	32(%rbx), %r14
	addq	$16, %rbx
	cmpq	%rbx, %r14
	je	.L276
	.p2align 4,,10
	.p2align 3
.L270:
	movq	32(%r14), %rax
	movq	%rax, %rcx
	movq	%rax, -64(%rbp)
	shrq	$32, %rcx
	cmpl	%r13d, %ecx
	je	.L271
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_
	testq	%rdx, %rdx
	je	.L271
	movl	$1, %r9d
	testq	%rax, %rax
	jne	.L273
	cmpq	%rdx, -72(%rbp)
	jne	.L289
.L273:
	movq	(%r12), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$39, %rax
	jbe	.L290
	leaq	40(%rsi), %rax
	movq	%rax, 16(%rdi)
.L275:
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rcx
	movl	%r9d, %edi
	movq	%rax, 32(%rsi)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r12)
.L271:
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, %rbx
	jne	.L270
.L276:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L291
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movl	32(%rdx), %eax
	cmpl	%eax, -64(%rbp)
	jb	.L273
	movl	$0, %r9d
	jne	.L273
	xorl	%r9d, %r9d
	movl	-60(%rbp), %eax
	cmpl	%eax, 36(%rdx)
	seta	%r9b
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L290:
	movl	$40, %esi
	movl	%r9d, -84(%rbp)
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %rdx
	movl	-84(%rbp), %r9d
	movq	%rax, %rsi
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L288:
	movq	%rdx, %rdi
	movl	$56, %esi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L269
.L291:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10941:
	.size	_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet16RemoveSameOffsetEjPNS0_4ZoneE, .-_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet16RemoveSameOffsetEjPNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder12RecomputeSetEPNS1_4NodeERKNS2_16UnobservablesSetE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"RedundantStoreFinder:   #%d is StoreField[+%d,%s](#%d), unobservable\n"
	.align 8
.LC2:
	.string	"RedundantStoreFinder:   #%d is StoreField[+%d,%s](#%d), observable, recording in set\n"
	.align 8
.LC3:
	.string	"RedundantStoreFinder:   #%d is LoadField[+%d,%s](#%d), removing all offsets [+%d] from set\n"
	.align 8
.LC4:
	.string	"RedundantStoreFinder:   #%d:%s can observe nothing, set stays unchanged\n"
	.align 8
.LC5:
	.string	"RedundantStoreFinder:   #%d:%s might observe anything, recording empty set\n"
	.section	.text._ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder12RecomputeSetEPNS1_4NodeERKNS2_16UnobservablesSetE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder12RecomputeSetEPNS1_4NodeERKNS2_16UnobservablesSetE
	.type	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder12RecomputeSetEPNS1_4NodeERKNS2_16UnobservablesSetE, @function
_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder12RecomputeSetEPNS1_4NodeERKNS2_16UnobservablesSetE:
.LFB10928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	movq	(%rsi), %rdi
	movzwl	16(%rdi), %edx
	cmpw	$240, %dx
	je	.L293
	cmpw	$245, %dx
	jne	.L294
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L334
.L295:
	call	_ZN2v88internal8compiler13FieldAccessOfEPKNS1_8OperatorE@PLT
	movl	20(%r14), %r13d
	movq	(%r12), %rcx
	movl	4(%rax), %r15d
	movzbl	_ZN2v88internal28FLAG_trace_store_eliminationE(%rip), %edx
	andl	$16777215, %r13d
	testq	%rcx, %rcx
	je	.L297
	leaq	16(%rcx), %rsi
	movq	24(%rcx), %rcx
	movzbl	_ZN2v88internal28FLAG_trace_store_eliminationE(%rip), %edx
	testq	%rcx, %rcx
	je	.L297
	movq	%rsi, %rdi
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L336:
	je	.L335
.L302:
	movq	%rcx, %rdi
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L300
.L299:
	cmpl	32(%rcx), %r13d
	jbe	.L336
	movq	24(%rcx), %rcx
.L343:
	testq	%rcx, %rcx
	jne	.L299
.L300:
	cmpq	%rdi, %rsi
	je	.L297
	movl	32(%rdi), %ecx
	cmpl	%ecx, %r13d
	jb	.L297
	cmpl	36(%rdi), %r15d
	jnb	.L305
	cmpl	%ecx, %r13d
	je	.L297
.L305:
	testb	%dl, %dl
	jne	.L337
.L315:
	leaq	-56(%rbp), %rsi
	leaq	200(%rbx), %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	(%r12), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	testb	%dl, %dl
	jne	.L338
.L307:
	movq	%r15, %rsi
	movq	16(%rbx), %rdx
	andl	$16777215, %r13d
	movq	%r12, %rdi
	salq	$32, %rsi
	orq	%r13, %rsi
	call	_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet3AddENS2_17UnobservableStoreEPNS0_4ZoneE
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L308
	movq	16(%r14), %r14
.L308:
	call	_ZN2v88internal8compiler13FieldAccessOfEPKNS1_8OperatorE@PLT
	cmpb	$0, _ZN2v88internal28FLAG_trace_store_eliminationE(%rip)
	movl	4(%rax), %r13d
	jne	.L339
.L309:
	movq	16(%rbx), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet16RemoveSameOffsetEjPNS0_4ZoneE
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	movzwl	%dx, %ecx
	andl	$-3, %edx
	movzbl	_ZN2v88internal28FLAG_trace_store_eliminationE(%rip), %esi
	cmpw	$429, %dx
	je	.L310
	cmpl	$241, %ecx
	je	.L310
	cmpl	$36, %ecx
	je	.L310
	cmpl	$246, %ecx
	sete	%r8b
	cmpl	$510, %ecx
	sete	%dl
	orb	%dl, %r8b
	jne	.L310
	cmpl	$56, %ecx
	jne	.L340
	.p2align 4,,10
	.p2align 3
.L310:
	testb	%sil, %sil
	jne	.L341
.L313:
	movq	(%r12), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	movq	16(%r14), %r14
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L338:
	movzbl	32(%rax), %edi
	call	_ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE@PLT
	movl	%r13d, %r8d
	movl	%r15d, %edx
	leaq	.LC2(%rip), %rdi
	movq	%rax, %rcx
	movq	-56(%rbp), %rax
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L340:
	testb	%sil, %sil
	jne	.L342
.L312:
	movq	256(%rbx), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	cmpl	36(%rcx), %r15d
	jbe	.L302
	movq	24(%rcx), %rcx
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L339:
	movzbl	32(%rax), %edi
	movl	20(%r14), %r14d
	call	_ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE@PLT
	andl	$16777215, %r14d
	movl	%r13d, %r9d
	movl	%r13d, %edx
	movq	%rax, %rcx
	movq	-56(%rbp), %rax
	movl	%r14d, %r8d
	leaq	.LC3(%rip), %rdi
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L342:
	movl	20(%rax), %esi
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rdi
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L341:
	movl	20(%rax), %esi
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rdi
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L337:
	movzbl	32(%rax), %edi
	call	_ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE@PLT
	movl	%r13d, %r8d
	movl	%r15d, %edx
	leaq	.LC1(%rip), %rdi
	movq	%rax, %rcx
	movq	-56(%rbp), %rax
	movl	20(%rax), %esi
	xorl	%eax, %eax
	andl	$16777215, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L315
	.cfi_endproc
.LFE10928:
	.size	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder12RecomputeSetEPNS1_4NodeERKNS2_16UnobservablesSetE, .-_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder12RecomputeSetEPNS1_4NodeERKNS2_16UnobservablesSetE
	.section	.text._ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB12947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$6, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L358
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L346:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L347
	movq	%r15, %rbx
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L348:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L359
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L349:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L347
.L352:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L348
	cmpq	$63, 8(%rax)
	jbe	.L348
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L352
.L347:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$63, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,8), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L358:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L346
	.cfi_endproc
.LFE12947:
	.size	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERKS4_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERKS4_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERKS4_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERKS4_:
.LFB13109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	je	.L387
	movl	(%rdx), %r14d
	movq	%rsi, %rbx
	cmpl	%r14d, 32(%rsi)
	jbe	.L388
.L365:
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rbx, 32(%r13)
	je	.L380
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	32(%rax), %r14d
	jbe	.L389
.L369:
	cmpq	$0, 24(%rdx)
	movl	$0, %eax
	cmovne	%rbx, %rax
	cmovne	%rbx, %rdx
.L380:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	.cfi_restore_state
	movl	36(%rsi), %eax
	movl	4(%rdx), %r15d
	je	.L390
	jnb	.L391
.L371:
	cmpq	%rbx, 40(%r13)
	je	.L385
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	movl	36(%rax), %eax
	cmpl	32(%rdx), %r14d
	jb	.L373
	jne	.L362
	cmpl	%r15d, %eax
	ja	.L373
.L362:
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_
	.p2align 4,,10
	.p2align 3
.L387:
	.cfi_restore_state
	cmpq	$0, 48(%rdi)
	je	.L362
	movq	40(%rdi), %rbx
	movl	32(%rbx), %eax
	cmpl	%eax, (%rdx)
	jbe	.L392
.L385:
	addq	$8, %rsp
	movq	%rbx, %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore_state
	jne	.L362
	movl	36(%rax), %eax
	cmpl	%eax, 4(%r12)
	jbe	.L362
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L391:
	addq	$8, %rsp
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L390:
	.cfi_restore_state
	cmpl	%r15d, %eax
	ja	.L365
	jb	.L371
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L392:
	jne	.L362
	movl	4(%rdx), %eax
	cmpl	%eax, 36(%rbx)
	jb	.L385
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L373:
	cmpq	$0, 24(%rbx)
	movl	$0, %eax
	cmovne	%rdx, %rax
	cmove	%rbx, %rdx
	jmp	.L380
	.cfi_endproc
.LFE13109:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERKS4_, .-_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERKS4_
	.section	.text._ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet9IntersectERKS3_S5_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet9IntersectERKS3_S5_PNS0_4ZoneE
	.type	_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet9IntersectERKS3_S5_PNS0_4ZoneE, @function
_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet9IntersectERKS3_S5_PNS0_4ZoneE:
.LFB10937:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L438
	cmpq	$0, 48(%rax)
	je	.L438
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L394
	cmpq	$0, 48(%rax)
	je	.L394
	movq	16(%rcx), %rbx
	movq	24(%rcx), %rax
	movq	%rdi, %r13
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L442
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rcx)
.L396:
	leaq	16(%rbx), %r14
	movq	%rcx, (%rbx)
	movl	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	%r14, 32(%rbx)
	movq	%r14, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	(%r12), %rax
	movq	%r14, -64(%rbp)
	leaq	16(%rax), %rsi
	movq	32(%rax), %r12
	movq	0(%r13), %rax
	movq	%rsi, -56(%rbp)
	movq	32(%rax), %r15
	leaq	16(%rax), %r13
	cmpq	%r12, %rsi
	je	.L399
	.p2align 4,,10
	.p2align 3
.L441:
	cmpq	%r15, %r13
	je	.L399
	movl	32(%r12), %eax
	cmpl	%eax, 32(%r15)
	jb	.L400
	je	.L443
	ja	.L406
.L411:
	movq	%r14, %rsi
	leaq	32(%r15), %rdx
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler21StoreStoreElimination17UnobservableStoreES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERKS4_
	movq	%rax, %r14
	testq	%rdx, %rdx
	je	.L407
	movl	$1, %r10d
	cmpq	%rdx, -64(%rbp)
	je	.L408
	testq	%rax, %rax
	je	.L444
.L408:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$39, %rax
	jbe	.L445
	leaq	40(%r14), %rax
	movq	%rax, 16(%rdi)
.L410:
	movq	32(%r15), %rax
	movq	-64(%rbp), %rcx
	movq	%r14, %rsi
	movl	%r10d, %edi
	movq	%rax, 32(%r14)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
.L407:
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L405:
	cmpq	%r12, -56(%rbp)
	jne	.L441
.L399:
	addq	$40, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%rdx), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L443:
	movl	36(%r15), %eax
	cmpl	%eax, 36(%r12)
	ja	.L400
	jnb	.L411
	.p2align 4,,10
	.p2align 3
.L406:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L444:
	movl	32(%rdx), %eax
	cmpl	%eax, 32(%r15)
	jb	.L408
	movl	$0, %r10d
	jne	.L408
	xorl	%r10d, %r10d
	movl	36(%r15), %eax
	cmpl	%eax, 36(%rdx)
	seta	%r10b
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L442:
	movq	%rcx, %rdi
	movl	$56, %esi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rbx
	jmp	.L396
.L445:
	movl	$40, %esi
	movq	%rdx, -80(%rbp)
	movl	%r10d, -68(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-68(%rbp), %r10d
	movq	-80(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L410
	.cfi_endproc
.LFE10937:
	.size	_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet9IntersectERKS3_S5_PNS0_4ZoneE, .-_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet9IntersectERKS3_S5_PNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder24RecomputeUseIntersectionEPNS1_4NodeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder24RecomputeUseIntersectionEPNS1_4NodeE.part.0, @function
_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder24RecomputeUseIntersectionEPNS1_4NodeE.part.0:
.LFB13339:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -56(%rbp)
	testq	%r13, %r13
	je	.L457
	movq	0(%r13), %rbx
	movq	%rdi, %r14
	movl	$1, %r12d
.L458:
	movl	16(%r13), %ecx
	movq	%r13, %rdi
	movl	%ecx, %edx
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rax
	leaq	0(%r13,%rax,8), %rax
	leaq	32(%rax), %rsi
	addq	$16, %rax
	andl	$1, %ecx
	cmovne	%rsi, %rax
	leaq	(%rax,%rdx,8), %rsi
	call	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L451
	movl	16(%r13), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	0(%r13,%rax,8), %rax
	jne	.L452
	movq	(%rax), %rax
.L452:
	movl	20(%rax), %eax
	movq	176(%r14), %rdx
	andl	$16777215, %eax
	movq	(%rdx,%rax,8), %rax
	movq	%rax, -48(%rbp)
	testb	%r12b, %r12b
	je	.L453
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L470
.L454:
	cmpq	$0, 48(%rax)
	jne	.L471
.L457:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	movq	-56(%rbp), %rax
	jne	.L472
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L471:
	.cfi_restore_state
	xorl	%r12d, %r12d
.L451:
	testq	%rbx, %rbx
	je	.L457
	movq	%rbx, %r13
	movq	(%rbx), %rbx
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L453:
	movq	16(%r14), %rcx
	leaq	256(%r14), %rdx
	leaq	-48(%rbp), %rsi
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal8compiler21StoreStoreElimination16UnobservablesSet9IntersectERKS3_S5_PNS0_4ZoneE
	movq	%rax, -56(%rbp)
.L455:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L457
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L470:
	movq	256(%r14), %rax
	movq	%rax, -56(%rbp)
	jmp	.L455
.L472:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13339:
	.size	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder24RecomputeUseIntersectionEPNS1_4NodeE.part.0, .-_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder24RecomputeUseIntersectionEPNS1_4NodeE.part.0
	.section	.text._ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder24RecomputeUseIntersectionEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder24RecomputeUseIntersectionEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder24RecomputeUseIntersectionEPNS1_4NodeE, @function
_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder24RecomputeUseIntersectionEPNS1_4NodeE:
.LFB10932:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	cmpb	$0, 36(%rax)
	jne	.L474
	movq	256(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	jmp	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder24RecomputeUseIntersectionEPNS1_4NodeE.part.0
	.cfi_endproc
.LFE10932:
	.size	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder24RecomputeUseIntersectionEPNS1_4NodeE, .-_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder24RecomputeUseIntersectionEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder18VisitEffectfulNodeEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"RedundantStoreFinder: - Revisiting: #%d:%s\n"
	.align 8
.LC7:
	.string	"RedundantStoreFinder: + No change: stabilized. Not visiting effect inputs.\n"
	.align 8
.LC8:
	.string	"RedundantStoreFinder:     marking #%d:%s for revisit\n"
	.section	.text._ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder18VisitEffectfulNodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder18VisitEffectfulNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder18VisitEffectfulNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder18VisitEffectfulNodeEPNS1_4NodeE:
.LFB10931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	20(%rsi), %esi
	movq	176(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r15), %rax
	andl	$16777215, %esi
	movl	%esi, %ecx
	cmpq	$0, (%rdx,%rcx,8)
	je	.L478
	cmpb	$0, _ZN2v88internal28FLAG_trace_store_eliminationE(%rip)
	jne	.L507
.L478:
	cmpb	$0, 36(%rax)
	jne	.L479
	movq	256(%r14), %rax
.L480:
	leaq	-72(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder12RecomputeSetEPNS1_4NodeERKNS2_16UnobservablesSetE
	movq	176(%r14), %rdx
	movq	%rax, %rbx
	movl	20(%r15), %eax
	andl	$16777215, %eax
	leaq	(%rdx,%rax,8), %r12
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L481
	movq	48(%rax), %rdx
	testq	%rbx, %rbx
	je	.L482
	cmpq	%rdx, 48(%rbx)
	je	.L508
.L481:
	movq	%rbx, (%r12)
	movq	(%r15), %rax
	xorl	%r12d, %r12d
	movl	$1, %r13d
	movl	24(%rax), %eax
	testl	%eax, %eax
	jg	.L488
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L491:
	movl	20(%rbx), %ecx
	movq	128(%r14), %rdx
	movq	%r13, %rdi
	movq	%rbx, -64(%rbp)
	movl	%ecx, %eax
	salq	%cl, %rdi
	andl	$16777215, %eax
	shrq	$6, %rax
	testq	%rdi, (%rdx,%rax,8)
	je	.L509
	movq	(%r15), %rax
	addl	$1, %r12d
	cmpl	24(%rax), %r12d
	jge	.L477
.L488:
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	cmpb	$0, _ZN2v88internal28FLAG_trace_store_eliminationE(%rip)
	movq	%rax, %rbx
	je	.L491
	movq	(%rax), %rax
	movl	20(%rbx), %esi
	leaq	.LC8(%rip), %rdi
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L509:
	movq	104(%r14), %rax
	movq	88(%r14), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L493
	movq	%rbx, (%rdx)
	addq	$8, 88(%r14)
.L494:
	movq	-64(%rbp), %rax
	movq	%r13, %rdx
	addl	$1, %r12d
	movl	20(%rax), %ecx
	movq	%rcx, %rax
	salq	%cl, %rdx
	shrq	$3, %rax
	andl	$2097144, %eax
	addq	128(%r14), %rax
	orq	%rdx, (%rax)
	movq	(%r15), %rax
	cmpl	24(%rax), %r12d
	jl	.L488
.L477:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L510
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L479:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder24RecomputeUseIntersectionEPNS1_4NodeE.part.0
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L507:
	movq	8(%rax), %rdx
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r15), %rax
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L493:
	leaq	-64(%rbp), %rsi
	leaq	24(%r14), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L482:
	testq	%rdx, %rdx
	jne	.L481
.L484:
	cmpb	$0, _ZN2v88internal28FLAG_trace_store_eliminationE(%rip)
	je	.L477
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L477
.L508:
	movq	32(%rax), %r13
	leaq	16(%rax), %rdx
	movq	32(%rbx), %r8
	cmpq	%r13, %rdx
	je	.L484
.L487:
	movl	32(%r8), %eax
	cmpl	%eax, 32(%r13)
	jne	.L481
	movl	36(%r13), %eax
	cmpl	%eax, 36(%r8)
	jne	.L481
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %r13
	movq	%r8, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-96(%rbp), %rdx
	movq	%rax, %r8
	cmpq	%r13, %rdx
	jne	.L487
	jmp	.L484
.L510:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10931:
	.size	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder18VisitEffectfulNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder18VisitEffectfulNodeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder5VisitEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder5VisitEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder5VisitEPNS1_4NodeE, @function
_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder5VisitEPNS1_4NodeE:
.LFB10930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	20(%rsi), %eax
	movq	176(%rdi), %rdx
	andl	$16777215, %eax
	leaq	(%rdx,%rax,8), %rax
	movq	(%rsi), %rdx
	cmpq	$0, (%rax)
	je	.L524
	movl	24(%rdx), %eax
	testl	%eax, %eax
	jg	.L521
.L511:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	movl	28(%rdx), %esi
	testl	%esi, %esi
	jle	.L513
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L517:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	176(%r13), %rdx
	movq	%rax, %rsi
	movl	20(%rax), %eax
	andl	$16777215, %eax
	cmpq	$0, (%rdx,%rax,8)
	je	.L525
	movq	(%r12), %rax
	addl	$1, %ebx
	cmpl	28(%rax), %ebx
	jl	.L517
.L516:
	movl	24(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L518
.L521:
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder18VisitEffectfulNodeEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore_state
	movq	%r13, %rdi
	addl	$1, %ebx
	call	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder14MarkForRevisitEPNS1_4NodeE
	movq	(%r12), %rax
	cmpl	%ebx, 28(%rax)
	jg	.L517
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L518:
	movl	20(%r12), %eax
	movq	176(%r13), %rdx
	andl	$16777215, %eax
	leaq	(%rdx,%rax,8), %rax
	cmpq	$0, (%rax)
	jne	.L511
.L519:
	movq	256(%r13), %rdx
	movq	%rdx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L513:
	.cfi_restore_state
	movl	24(%rdx), %edx
	testl	%edx, %edx
	jle	.L519
	jmp	.L521
	.cfi_endproc
.LFE10930:
	.size	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder5VisitEPNS1_4NodeE, .-_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder5VisitEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder4FindEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder4FindEv
	.type	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder4FindEv, @function
_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder4FindEv:
.LFB10921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	(%rax), %rax
	movq	16(%rax), %rsi
	call	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder5VisitEPNS1_4NodeE
	movq	56(%rbx), %rax
	cmpq	%rax, 88(%rbx)
	je	.L526
	movl	$1, %r12d
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L528:
	movq	-8(%rax), %rsi
	subq	$8, %rax
	movq	%rax, 88(%rbx)
.L532:
	movl	20(%rsi), %ecx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movq	%rcx, %rax
	salq	%cl, %rdx
	shrq	$3, %rax
	notq	%rdx
	andl	$2097144, %eax
	addq	128(%rbx), %rax
	andq	%rdx, (%rax)
	call	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder5VisitEPNS1_4NodeE
	movq	88(%rbx), %rax
	cmpq	%rax, 56(%rbx)
	je	.L526
.L527:
	movq	8(%rbx), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	88(%rbx), %rax
	movq	96(%rbx), %rdx
	cmpq	%rdx, %rax
	jne	.L528
	movq	112(%rbx), %rax
	movq	-8(%rax), %rcx
	movq	504(%rcx), %rsi
	movq	32(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L529
	cmpq	$64, 8(%rcx)
	ja	.L530
.L529:
	movq	$64, 8(%rdx)
	movq	32(%rbx), %rax
	movq	%rax, (%rdx)
	movq	112(%rbx), %rax
	movq	%rdx, 32(%rbx)
.L530:
	leaq	-8(%rax), %rdx
	movq	%rdx, 112(%rbx)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 96(%rbx)
	addq	$504, %rax
	movq	%rdx, 104(%rbx)
	movq	%rax, 88(%rbx)
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L526:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10921:
	.size	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder4FindEv, .-_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder4FindEv
	.section	.rodata._ZN2v88internal8compiler21StoreStoreElimination3RunEPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"cannot create std::vector larger than max_size()"
	.align 8
.LC10:
	.string	"StoreStoreElimination::Run: Eliminating node #%d:%s\n"
	.section	.text._ZN2v88internal8compiler21StoreStoreElimination3RunEPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21StoreStoreElimination3RunEPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler21StoreStoreElimination3RunEPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE, @function
_ZN2v88internal8compiler21StoreStoreElimination3RunEPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE:
.LFB10924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm5
	movq	%rdi, %xmm0
	xorl	%esi, %esi
	punpcklqdq	%xmm5, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	-496(%rbp), %rdi
	subq	$480, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -304(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -288(%rbp)
	movq	%rdx, -496(%rbp)
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	$0, -472(%rbp)
	movaps	%xmm0, -464(%rbp)
	movaps	%xmm0, -448(%rbp)
	movaps	%xmm0, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -480(%rbp)
	je	.L607
	movdqa	-496(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	leaq	-400(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm6, -400(%rbp)
	movaps	%xmm0, -352(%rbp)
	movaps	%xmm0, -336(%rbp)
	movaps	%xmm0, -320(%rbp)
	movq	$0, -384(%rbp)
	movq	$0, -376(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-368(%rbp), %xmm7
	movq	-464(%rbp), %xmm3
	movq	-456(%rbp), %r9
	movq	-440(%rbp), %r8
	movaps	%xmm7, -464(%rbp)
	movq	-424(%rbp), %rcx
	movdqa	-336(%rbp), %xmm7
	movq	-432(%rbp), %xmm1
	movq	-408(%rbp), %rdx
	movaps	%xmm7, -432(%rbp)
	movq	%r9, %xmm7
	movq	-448(%rbp), %xmm2
	movdqa	-352(%rbp), %xmm6
	punpcklqdq	%xmm7, %xmm3
	movq	-488(%rbp), %rax
	movq	-416(%rbp), %xmm0
	movups	%xmm3, -248(%rbp)
	movq	%r8, %xmm3
	movq	-496(%rbp), %xmm4
	movq	-480(%rbp), %r11
	punpcklqdq	%xmm3, %xmm2
	movq	-472(%rbp), %r10
	movdqa	-320(%rbp), %xmm5
	movaps	%xmm6, -448(%rbp)
	movups	%xmm2, -232(%rbp)
	movq	%rcx, %xmm2
	movq	%rax, %xmm6
	movq	-384(%rbp), %rsi
	punpcklqdq	%xmm2, %xmm1
	movq	-376(%rbp), %rdi
	punpcklqdq	%xmm6, %xmm4
	movq	%r11, -264(%rbp)
	movups	%xmm1, -216(%rbp)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -480(%rbp)
	movq	%rdi, -472(%rbp)
	movq	%r10, -256(%rbp)
	movaps	%xmm5, -416(%rbp)
	movups	%xmm4, -280(%rbp)
	movups	%xmm0, -200(%rbp)
	testq	%rsi, %rsi
	je	.L541
	movq	-408(%rbp), %rcx
	movq	-440(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L542
	.p2align 4,,10
	.p2align 3
.L545:
	testq	%rax, %rax
	je	.L543
	cmpq	$64, 8(%rax)
	ja	.L544
.L543:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-488(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -488(%rbp)
.L544:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L545
	movq	-472(%rbp), %rdi
	movq	-480(%rbp), %rsi
.L542:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L541
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L541:
	movq	(%rbx), %rax
	movl	28(%rax), %r13d
	movq	%r12, -184(%rbp)
	movq	$0, -176(%rbp)
	movl	$0, -168(%rbp)
	movq	%r13, %r14
	movq	$0, -160(%rbp)
	movl	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	testq	%r13, %r13
	jne	.L608
.L550:
	movl	28(%rax), %edx
	cmpq	$268435455, %rdx
	ja	.L609
	movq	16(%r12), %rdi
	movq	24(%r12), %rax
	movq	%r12, -136(%rbp)
	leaq	0(,%rdx,8), %r13
	movq	$0, -128(%rbp)
	xorl	%ebx, %ebx
	movq	$0, -120(%rbp)
	subq	%rdi, %rax
	movq	$0, -112(%rbp)
	testq	%rdx, %rdx
	je	.L552
	movq	%r13, %rsi
	cmpq	%r13, %rax
	jb	.L610
	addq	%rdi, %rsi
	movq	%rsi, 16(%r12)
.L554:
	leaq	(%rdi,%r13), %rbx
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%rdi, -128(%rbp)
	movq	%rbx, -112(%rbp)
	call	memset@PLT
	movq	16(%r12), %rdi
	movq	24(%r12), %rax
	subq	%rdi, %rax
.L552:
	movq	%rbx, -120(%rbp)
	leaq	-88(%rbp), %rbx
	movq	%r12, -104(%rbp)
	movl	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	%rbx, -72(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$0, -56(%rbp)
	cmpq	$55, %rax
	jbe	.L611
	leaq	56(%rdi), %rax
	movq	%rax, 16(%r12)
.L556:
	leaq	16(%rdi), %rax
	movq	%r12, (%rdi)
	leaq	.LC10(%rip), %r13
	movl	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	%rax, 32(%rdi)
	movq	%rax, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	%rdi, -48(%rbp)
	leaq	-304(%rbp), %rdi
	call	_ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder4FindEv
	movq	-72(%rbp), %r12
	cmpq	%rbx, %r12
	jne	.L557
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L560:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rax, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties11ReplaceUsesEPNS1_4NodeES4_S4_S4_S4_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rbx, %rax
	je	.L562
.L557:
	cmpb	$0, _ZN2v88internal28FLAG_trace_store_eliminationE(%rip)
	movq	32(%r12), %r14
	je	.L560
	movq	(%r14), %rax
	movl	20(%r14), %esi
	movq	%r13, %rdi
	movq	8(%rax), %rdx
	andl	$16777215, %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L562:
	movq	-80(%rbp), %rbx
	leaq	-104(%rbp), %r13
	testq	%rbx, %rbx
	je	.L558
.L559:
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L563
.L564:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L564
.L563:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L559
.L558:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L539
	movq	-192(%rbp), %rcx
	movq	-224(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L566
	movq	-272(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L569:
	testq	%rax, %rax
	je	.L567
	cmpq	$64, 8(%rax)
	ja	.L568
.L567:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-272(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -272(%rbp)
.L568:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L569
	movq	-264(%rbp), %rax
.L566:
	movq	-256(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L539
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L539:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L612
	addq	$480, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	movdqa	-496(%rbp), %xmm5
	movq	-472(%rbp), %rax
	movq	$0, -264(%rbp)
	movdqa	-464(%rbp), %xmm1
	movdqa	-448(%rbp), %xmm2
	movdqa	-432(%rbp), %xmm3
	movdqa	-416(%rbp), %xmm6
	movq	%rax, -256(%rbp)
	movups	%xmm5, -280(%rbp)
	movups	%xmm1, -248(%rbp)
	movups	%xmm2, -232(%rbp)
	movups	%xmm3, -216(%rbp)
	movups	%xmm6, -200(%rbp)
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L608:
	movq	16(%r12), %rdi
	movq	24(%r12), %rax
	leaq	63(%r13), %rdx
	shrq	$6, %rdx
	salq	$3, %rdx
	subq	%rdi, %rax
	movq	%rdx, %rsi
	cmpq	%rax, %rdx
	ja	.L613
	addq	%rdi, %rsi
	movq	%rsi, 16(%r12)
.L549:
	leaq	(%rdi,%rdx), %rax
	sarq	$6, %r13
	andl	$63, %r14d
	movq	%rdi, -176(%rbp)
	movq	%rax, -144(%rbp)
	leaq	(%rdi,%r13,8), %rax
	movl	$0, -168(%rbp)
	movq	%rax, -160(%rbp)
	movl	%r14d, -152(%rbp)
	testq	%rdi, %rdi
	je	.L605
	xorl	%esi, %esi
	call	memset@PLT
.L605:
	movq	(%rbx), %rax
	jmp	.L550
.L610:
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L611:
	movq	%r12, %rdi
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L556
.L613:
	movq	%r12, %rdi
	movq	%rdx, -504(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-504(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L549
.L609:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L612:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10924:
	.size	_ZN2v88internal8compiler21StoreStoreElimination3RunEPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE, .-_ZN2v88internal8compiler21StoreStoreElimination3RunEPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder4FindEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder4FindEv, @function
_GLOBAL__sub_I__ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder4FindEv:
.LFB13198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE13198:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder4FindEv, .-_GLOBAL__sub_I__ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder4FindEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler21StoreStoreElimination20RedundantStoreFinder4FindEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
