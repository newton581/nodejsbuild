	.file	"js-generic-lowering.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.rodata._ZNK2v88internal8compiler17JSGenericLowering12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"JSGenericLowering"
	.section	.text._ZNK2v88internal8compiler17JSGenericLowering12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler17JSGenericLowering12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler17JSGenericLowering12reducer_nameEv
	.type	_ZNK2v88internal8compiler17JSGenericLowering12reducer_nameEv, @function
_ZNK2v88internal8compiler17JSGenericLowering12reducer_nameEv:
.LFB12939:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE12939:
	.size	_ZNK2v88internal8compiler17JSGenericLowering12reducer_nameEv, .-_ZNK2v88internal8compiler17JSGenericLowering12reducer_nameEv
	.section	.text._ZN2v88internal8compiler17JSGenericLoweringD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLoweringD2Ev
	.type	_ZN2v88internal8compiler17JSGenericLoweringD2Ev, @function
_ZN2v88internal8compiler17JSGenericLoweringD2Ev:
.LFB15911:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE15911:
	.size	_ZN2v88internal8compiler17JSGenericLoweringD2Ev, .-_ZN2v88internal8compiler17JSGenericLoweringD2Ev
	.globl	_ZN2v88internal8compiler17JSGenericLoweringD1Ev
	.set	_ZN2v88internal8compiler17JSGenericLoweringD1Ev,_ZN2v88internal8compiler17JSGenericLoweringD2Ev
	.section	.text._ZN2v88internal26ArrayConstructorDescriptorD2Ev,"axG",@progbits,_ZN2v88internal26ArrayConstructorDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26ArrayConstructorDescriptorD2Ev
	.type	_ZN2v88internal26ArrayConstructorDescriptorD2Ev, @function
_ZN2v88internal26ArrayConstructorDescriptorD2Ev:
.LFB19179:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE19179:
	.size	_ZN2v88internal26ArrayConstructorDescriptorD2Ev, .-_ZN2v88internal26ArrayConstructorDescriptorD2Ev
	.weak	_ZN2v88internal26ArrayConstructorDescriptorD1Ev
	.set	_ZN2v88internal26ArrayConstructorDescriptorD1Ev,_ZN2v88internal26ArrayConstructorDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal26ArrayConstructorDescriptorD0Ev,"axG",@progbits,_ZN2v88internal26ArrayConstructorDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26ArrayConstructorDescriptorD0Ev
	.type	_ZN2v88internal26ArrayConstructorDescriptorD0Ev, @function
_ZN2v88internal26ArrayConstructorDescriptorD0Ev:
.LFB19181:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE19181:
	.size	_ZN2v88internal26ArrayConstructorDescriptorD0Ev, .-_ZN2v88internal26ArrayConstructorDescriptorD0Ev
	.section	.text._ZN2v88internal8compiler17JSGenericLoweringD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLoweringD0Ev
	.type	_ZN2v88internal8compiler17JSGenericLoweringD0Ev, @function
_ZN2v88internal8compiler17JSGenericLoweringD0Ev:
.LFB15913:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE15913:
	.size	_ZN2v88internal8compiler17JSGenericLoweringD0Ev, .-_ZN2v88internal8compiler17JSGenericLoweringD0Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal26ArrayConstructorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal26ArrayConstructorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26ArrayConstructorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal26ArrayConstructorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal26ArrayConstructorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB9105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$5, %r9d
	xorl	%esi, %esi
	movl	$4, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-18(%rbp), %r8
	movabsq	$145248819041797896, %rax
	movq	%rax, -18(%rbp)
	movl	$1800, %eax
	movw	%ax, -10(%rbp)
	call	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L13
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L13:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9105:
	.size	_ZN2v88internal26ArrayConstructorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal26ArrayConstructorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal26ArrayConstructorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal26ArrayConstructorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26ArrayConstructorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal26ArrayConstructorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal26ArrayConstructorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB9109:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movl	$1, %esi
	jmp	_ZN2v88internal23CallInterfaceDescriptor35JSDefaultInitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataEi@PLT
	.cfi_endproc
.LFE9109:
	.size	_ZN2v88internal26ArrayConstructorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal26ArrayConstructorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal8compiler17JSGenericLoweringC2EPNS1_7JSGraphEPNS1_15AdvancedReducer6EditorEPNS1_12JSHeapBrokerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLoweringC2EPNS1_7JSGraphEPNS1_15AdvancedReducer6EditorEPNS1_12JSHeapBrokerE
	.type	_ZN2v88internal8compiler17JSGenericLoweringC2EPNS1_7JSGraphEPNS1_15AdvancedReducer6EditorEPNS1_12JSHeapBrokerE, @function
_ZN2v88internal8compiler17JSGenericLoweringC2EPNS1_7JSGraphEPNS1_15AdvancedReducer6EditorEPNS1_12JSHeapBrokerE:
.LFB15908:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler17JSGenericLoweringE(%rip), %rax
	movq	%rdx, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rsi, 16(%rdi)
	movq	%rcx, 24(%rdi)
	ret
	.cfi_endproc
.LFE15908:
	.size	_ZN2v88internal8compiler17JSGenericLoweringC2EPNS1_7JSGraphEPNS1_15AdvancedReducer6EditorEPNS1_12JSHeapBrokerE, .-_ZN2v88internal8compiler17JSGenericLoweringC2EPNS1_7JSGraphEPNS1_15AdvancedReducer6EditorEPNS1_12JSHeapBrokerE
	.globl	_ZN2v88internal8compiler17JSGenericLoweringC1EPNS1_7JSGraphEPNS1_15AdvancedReducer6EditorEPNS1_12JSHeapBrokerE
	.set	_ZN2v88internal8compiler17JSGenericLoweringC1EPNS1_7JSGraphEPNS1_15AdvancedReducer6EditorEPNS1_12JSHeapBrokerE,_ZN2v88internal8compiler17JSGenericLoweringC2EPNS1_7JSGraphEPNS1_15AdvancedReducer6EditorEPNS1_12JSHeapBrokerE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	.type	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE, @function
_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE:
.LFB15962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	(%rdx), %r14
	movq	(%rsi), %rdi
	leaq	-64(%rbp), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdx), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movq	%rdx, -64(%rbp)
	movzbl	18(%rdi), %r8d
	movq	%rax, -56(%rbp)
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15962:
	.size	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE, .-_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering10LowerJSAddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering10LowerJSAddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering10LowerJSAddEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering10LowerJSAddEPNS1_4NodeE:
.LFB15915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$434, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L25:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15915:
	.size	_ZN2v88internal8compiler17JSGenericLowering10LowerJSAddEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering10LowerJSAddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering15LowerJSSubtractEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering15LowerJSSubtractEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering15LowerJSSubtractEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering15LowerJSSubtractEPNS1_4NodeE:
.LFB15925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$435, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L29
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L29:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15925:
	.size	_ZN2v88internal8compiler17JSGenericLowering15LowerJSSubtractEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering15LowerJSSubtractEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering15LowerJSMultiplyEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering15LowerJSMultiplyEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering15LowerJSMultiplyEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering15LowerJSMultiplyEPNS1_4NodeE:
.LFB15926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$436, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L33:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15926:
	.size	_ZN2v88internal8compiler17JSGenericLowering15LowerJSMultiplyEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering15LowerJSMultiplyEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering13LowerJSDivideEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering13LowerJSDivideEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering13LowerJSDivideEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering13LowerJSDivideEPNS1_4NodeE:
.LFB15927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$437, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L37:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15927:
	.size	_ZN2v88internal8compiler17JSGenericLowering13LowerJSDivideEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering13LowerJSDivideEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering14LowerJSModulusEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering14LowerJSModulusEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering14LowerJSModulusEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering14LowerJSModulusEPNS1_4NodeE:
.LFB15928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$438, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L41:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15928:
	.size	_ZN2v88internal8compiler17JSGenericLowering14LowerJSModulusEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering14LowerJSModulusEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering19LowerJSExponentiateEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering19LowerJSExponentiateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering19LowerJSExponentiateEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering19LowerJSExponentiateEPNS1_4NodeE:
.LFB15929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$439, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L45:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15929:
	.size	_ZN2v88internal8compiler17JSGenericLowering19LowerJSExponentiateEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering19LowerJSExponentiateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseAndEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseAndEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseAndEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseAndEPNS1_4NodeE:
.LFB15930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$440, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L49:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15930:
	.size	_ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseAndEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseAndEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering16LowerJSBitwiseOrEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering16LowerJSBitwiseOrEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering16LowerJSBitwiseOrEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering16LowerJSBitwiseOrEPNS1_4NodeE:
.LFB15931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$441, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L53:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15931:
	.size	_ZN2v88internal8compiler17JSGenericLowering16LowerJSBitwiseOrEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering16LowerJSBitwiseOrEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseXorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseXorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseXorEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseXorEPNS1_4NodeE:
.LFB15932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$442, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L57:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15932:
	.size	_ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseXorEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseXorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering16LowerJSShiftLeftEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering16LowerJSShiftLeftEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering16LowerJSShiftLeftEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering16LowerJSShiftLeftEPNS1_4NodeE:
.LFB15933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$443, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L61:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15933:
	.size	_ZN2v88internal8compiler17JSGenericLowering16LowerJSShiftLeftEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering16LowerJSShiftLeftEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering17LowerJSShiftRightEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering17LowerJSShiftRightEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering17LowerJSShiftRightEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering17LowerJSShiftRightEPNS1_4NodeE:
.LFB15934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$444, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L65:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15934:
	.size	_ZN2v88internal8compiler17JSGenericLowering17LowerJSShiftRightEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering17LowerJSShiftRightEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering24LowerJSShiftRightLogicalEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering24LowerJSShiftRightLogicalEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering24LowerJSShiftRightLogicalEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering24LowerJSShiftRightLogicalEPNS1_4NodeE:
.LFB15935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$445, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L69:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15935:
	.size	_ZN2v88internal8compiler17JSGenericLowering24LowerJSShiftRightLogicalEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering24LowerJSShiftRightLogicalEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering15LowerJSLessThanEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering15LowerJSLessThanEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering15LowerJSLessThanEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering15LowerJSLessThanEPNS1_4NodeE:
.LFB15936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$446, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L73:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15936:
	.size	_ZN2v88internal8compiler17JSGenericLowering15LowerJSLessThanEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering15LowerJSLessThanEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering22LowerJSLessThanOrEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering22LowerJSLessThanOrEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering22LowerJSLessThanOrEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering22LowerJSLessThanOrEqualEPNS1_4NodeE:
.LFB15937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$447, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L77:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15937:
	.size	_ZN2v88internal8compiler17JSGenericLowering22LowerJSLessThanOrEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering22LowerJSLessThanOrEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering18LowerJSGreaterThanEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering18LowerJSGreaterThanEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering18LowerJSGreaterThanEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering18LowerJSGreaterThanEPNS1_4NodeE:
.LFB15938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$448, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15938:
	.size	_ZN2v88internal8compiler17JSGenericLowering18LowerJSGreaterThanEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering18LowerJSGreaterThanEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering25LowerJSGreaterThanOrEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering25LowerJSGreaterThanOrEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering25LowerJSGreaterThanOrEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering25LowerJSGreaterThanOrEqualEPNS1_4NodeE:
.LFB15939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$449, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L85:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15939:
	.size	_ZN2v88internal8compiler17JSGenericLowering25LowerJSGreaterThanOrEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering25LowerJSGreaterThanOrEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseNotEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseNotEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseNotEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseNotEPNS1_4NodeE:
.LFB15940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$454, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L89:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15940:
	.size	_ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseNotEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering17LowerJSBitwiseNotEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering16LowerJSDecrementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering16LowerJSDecrementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering16LowerJSDecrementEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering16LowerJSDecrementEPNS1_4NodeE:
.LFB15941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$455, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L93:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15941:
	.size	_ZN2v88internal8compiler17JSGenericLowering16LowerJSDecrementEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering16LowerJSDecrementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering16LowerJSIncrementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering16LowerJSIncrementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering16LowerJSIncrementEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering16LowerJSIncrementEPNS1_4NodeE:
.LFB15942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$456, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L97:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15942:
	.size	_ZN2v88internal8compiler17JSGenericLowering16LowerJSIncrementEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering16LowerJSIncrementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering13LowerJSNegateEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering13LowerJSNegateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering13LowerJSNegateEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering13LowerJSNegateEPNS1_4NodeE:
.LFB15943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$457, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L101:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15943:
	.size	_ZN2v88internal8compiler17JSGenericLowering13LowerJSNegateEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering13LowerJSNegateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering18LowerJSHasPropertyEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering18LowerJSHasPropertyEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering18LowerJSHasPropertyEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering18LowerJSHasPropertyEPNS1_4NodeE:
.LFB15944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$159, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L105:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15944:
	.size	_ZN2v88internal8compiler17JSGenericLowering18LowerJSHasPropertyEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering18LowerJSHasPropertyEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering12LowerJSEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering12LowerJSEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering12LowerJSEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering12LowerJSEqualEPNS1_4NodeE:
.LFB15945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$450, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L109:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15945:
	.size	_ZN2v88internal8compiler17JSGenericLowering12LowerJSEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering12LowerJSEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering15LowerJSToLengthEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering15LowerJSToLengthEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering15LowerJSToLengthEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering15LowerJSToLengthEPNS1_4NodeE:
.LFB15946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$108, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L113:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15946:
	.size	_ZN2v88internal8compiler17JSGenericLowering15LowerJSToLengthEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering15LowerJSToLengthEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering15LowerJSToNumberEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering15LowerJSToNumberEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering15LowerJSToNumberEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering15LowerJSToNumberEPNS1_4NodeE:
.LFB15947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$102, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L117:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15947:
	.size	_ZN2v88internal8compiler17JSGenericLowering15LowerJSToNumberEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering15LowerJSToNumberEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering28LowerJSToNumberConvertBigIntEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering28LowerJSToNumberConvertBigIntEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering28LowerJSToNumberConvertBigIntEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering28LowerJSToNumberConvertBigIntEPNS1_4NodeE:
.LFB15948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$103, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L121:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15948:
	.size	_ZN2v88internal8compiler17JSGenericLowering28LowerJSToNumberConvertBigIntEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering28LowerJSToNumberConvertBigIntEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering16LowerJSToNumericEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering16LowerJSToNumericEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering16LowerJSToNumericEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering16LowerJSToNumericEPNS1_4NodeE:
.LFB15949:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$104, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L125:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15949:
	.size	_ZN2v88internal8compiler17JSGenericLowering16LowerJSToNumericEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering16LowerJSToNumericEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering13LowerJSToNameEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering13LowerJSToNameEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering13LowerJSToNameEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering13LowerJSToNameEPNS1_4NodeE:
.LFB15950:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$99, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L129:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15950:
	.size	_ZN2v88internal8compiler17JSGenericLowering13LowerJSToNameEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering13LowerJSToNameEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering15LowerJSToObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering15LowerJSToObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering15LowerJSToObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering15LowerJSToObjectEPNS1_4NodeE:
.LFB15951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$91, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L133
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L133:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15951:
	.size	_ZN2v88internal8compiler17JSGenericLowering15LowerJSToObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering15LowerJSToObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering15LowerJSToStringEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering15LowerJSToStringEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering15LowerJSToStringEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering15LowerJSToStringEPNS1_4NodeE:
.LFB15952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$785, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L137
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L137:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15952:
	.size	_ZN2v88internal8compiler17JSGenericLowering15LowerJSToStringEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering15LowerJSToStringEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering21LowerJSForInEnumerateEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering21LowerJSForInEnumerateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering21LowerJSForInEnumerateEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering21LowerJSForInEnumerateEPNS1_4NodeE:
.LFB15953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$491, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L141
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L141:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15953:
	.size	_ZN2v88internal8compiler17JSGenericLowering21LowerJSForInEnumerateEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering21LowerJSForInEnumerateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering25LowerJSAsyncFunctionEnterEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering25LowerJSAsyncFunctionEnterEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering25LowerJSAsyncFunctionEnterEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering25LowerJSAsyncFunctionEnterEPNS1_4NodeE:
.LFB15954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$226, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L145:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15954:
	.size	_ZN2v88internal8compiler17JSGenericLowering25LowerJSAsyncFunctionEnterEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering25LowerJSAsyncFunctionEnterEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering26LowerJSAsyncFunctionRejectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering26LowerJSAsyncFunctionRejectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering26LowerJSAsyncFunctionRejectEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering26LowerJSAsyncFunctionRejectEPNS1_4NodeE:
.LFB15955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$227, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L149
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L149:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15955:
	.size	_ZN2v88internal8compiler17JSGenericLowering26LowerJSAsyncFunctionRejectEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering26LowerJSAsyncFunctionRejectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering27LowerJSAsyncFunctionResolveEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering27LowerJSAsyncFunctionResolveEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering27LowerJSAsyncFunctionResolveEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering27LowerJSAsyncFunctionResolveEPNS1_4NodeE:
.LFB15956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$228, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L153:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15956:
	.size	_ZN2v88internal8compiler17JSGenericLowering27LowerJSAsyncFunctionResolveEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering27LowerJSAsyncFunctionResolveEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering21LowerJSFulfillPromiseEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering21LowerJSFulfillPromiseEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering21LowerJSFulfillPromiseEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering21LowerJSFulfillPromiseEPNS1_4NodeE:
.LFB15957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$493, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L157
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L157:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15957:
	.size	_ZN2v88internal8compiler17JSGenericLowering21LowerJSFulfillPromiseEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering21LowerJSFulfillPromiseEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering25LowerJSPerformPromiseThenEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering25LowerJSPerformPromiseThenEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering25LowerJSPerformPromiseThenEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering25LowerJSPerformPromiseThenEPNS1_4NodeE:
.LFB15958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$504, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L161
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L161:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15958:
	.size	_ZN2v88internal8compiler17JSGenericLowering25LowerJSPerformPromiseThenEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering25LowerJSPerformPromiseThenEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering21LowerJSPromiseResolveEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering21LowerJSPromiseResolveEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering21LowerJSPromiseResolveEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering21LowerJSPromiseResolveEPNS1_4NodeE:
.LFB15959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$510, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L165:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15959:
	.size	_ZN2v88internal8compiler17JSGenericLowering21LowerJSPromiseResolveEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering21LowerJSPromiseResolveEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering20LowerJSRejectPromiseEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering20LowerJSRejectPromiseEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering20LowerJSRejectPromiseEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering20LowerJSRejectPromiseEPNS1_4NodeE:
.LFB15960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$494, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L169
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L169:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15960:
	.size	_ZN2v88internal8compiler17JSGenericLowering20LowerJSRejectPromiseEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering20LowerJSRejectPromiseEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering21LowerJSResolvePromiseEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering21LowerJSResolvePromiseEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering21LowerJSResolvePromiseEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering21LowerJSResolvePromiseEPNS1_4NodeE:
.LFB15961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$495, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L173
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L173:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15961:
	.size	_ZN2v88internal8compiler17JSGenericLowering21LowerJSResolvePromiseEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering21LowerJSResolvePromiseEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS7_INS1_8Operator8PropertyEhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS7_INS1_8Operator8PropertyEhEE
	.type	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS7_INS1_8Operator8PropertyEhEE, @function
_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS7_INS1_8Operator8PropertyEhEE:
.LFB15963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	leaq	-64(%rbp), %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -64(%rbp)
	movq	16(%rdx), %rax
	movq	%rax, -56(%rbp)
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	0(%r13), %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r14, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L177
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L177:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15963:
	.size	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS7_INS1_8Operator8PropertyEhEE, .-_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS7_INS1_8Operator8PropertyEhEE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	.type	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi, @function
_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi:
.LFB15964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movl	%r14d, %edi
	movzbl	%al, %r8d
	movq	0(%r13), %rax
	movl	%r8d, -64(%rbp)
	movzbl	18(%rax), %ecx
	movb	%cl, -56(%rbp)
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	testl	%r12d, %r12d
	movzbl	-56(%rbp), %ecx
	movl	-64(%rbp), %r8d
	movq	%rax, %r15
	jns	.L179
	movsbl	24(%rax), %r12d
.L179:
	movq	16(%rbx), %rax
	movl	%r12d, %edx
	movl	%r14d, %esi
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE@PLT
	movq	16(%rbx), %r8
	movl	%r14d, %edi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	16(%rbx), %rdi
	movl	%r12d, %esi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movsbl	25(%r15), %esi
	movq	16(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7JSGraph18CEntryStubConstantEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	movq	%r13, %rdi
	xorl	%edx, %edx
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	-56(%rbp), %r10
	movq	%r13, %rdi
	leal	1(%r12), %edx
	movq	(%rax), %rax
	movq	%r10, %rcx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r14, %rcx
	movq	%r13, %rdi
	leal	2(%r12), %edx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	-64(%rbp), %r9
	movq	8(%rax), %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	.cfi_endproc
.LFE15964:
	.size	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi, .-_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	.section	.text._ZN2v88internal8compiler17JSGenericLowering18LowerJSStrictEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering18LowerJSStrictEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering18LowerJSStrictEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering18LowerJSStrictEqualEPNS1_4NodeE:
.LFB15965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$64, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_@PLT
	movq	16(%r12), %rax
	movl	$453, %edx
	leaq	-80(%rbp), %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movl	$4, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	-80(%rbp), %rax
	xorl	%ecx, %ecx
	leaq	-48(%rbp), %rdx
	movl	$112, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -40(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS7_INS1_8Operator8PropertyEhEE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L184
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L184:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15965:
	.size	_ZN2v88internal8compiler17JSGenericLowering18LowerJSStrictEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering18LowerJSStrictEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering19LowerJSLoadPropertyEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering19LowerJSLoadPropertyEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering19LowerJSLoadPropertyEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering19LowerJSLoadPropertyEPNS1_4NodeE:
.LFB15967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movzbl	%al, %r15d
	call	_ZN2v88internal8compiler16PropertyAccessOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L186
	addq	$72, %rax
.L187:
	movq	16(%r12), %r8
	movq	%r14, %rdi
	movq	(%rax), %rbx
	movq	%r8, -120(%rbp)
	call	_ZNK2v88internal8compiler14FeedbackSource5indexEv@PLT
	pxor	%xmm0, %xmm0
	movq	-120(%rbp), %r8
	cvtsi2sdl	%eax, %xmm0
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rdi
	movl	$2, %edx
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	(%rbx), %rax
	movq	24(%r12), %rdi
	movq	%r14, %rsi
	cmpw	$41, 16(%rax)
	je	.L188
	call	_ZNK2v88internal8compiler12JSHeapBroker11GetFeedbackERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdi
	movl	(%rax), %eax
	cmpl	$4, %eax
	je	.L212
	cmpl	$8, %eax
	je	.L213
	testl	%eax, %eax
	jne	.L199
.L192:
	movl	$375, %edx
.L193:
	movq	16(%r12), %rax
	leaq	-112(%rbp), %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
.L211:
	movq	-112(%rbp), %rax
	leaq	-80(%rbp), %rdx
	movl	%r15d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L214
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	call	_ZNK2v88internal8compiler12JSHeapBroker11GetFeedbackERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdi
	movl	(%rax), %eax
	cmpl	$4, %eax
	je	.L215
	cmpl	$8, %eax
	je	.L216
	testl	%eax, %eax
	jne	.L199
.L198:
	movl	$373, %edx
.L200:
	movq	16(%r12), %rax
	leaq	-112(%rbp), %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	16(%r12), %rdi
	movq	(%r14), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$3, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L186:
	movq	32(%rax), %rax
	addq	$56, %rax
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L213:
	call	_ZNK2v88internal8compiler17ProcessedFeedback13AsNamedAccessEv@PLT
	movq	32(%rax), %rbx
	cmpq	%rbx, 40(%rax)
	sete	%al
.L190:
	movl	$376, %edx
	testb	%al, %al
	jne	.L193
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L212:
	call	_ZNK2v88internal8compiler17ProcessedFeedback15AsElementAccessEv@PLT
	movq	%rax, %rdi
	call	_ZNK2v88internal8compiler21ElementAccessFeedback17transition_groupsEv@PLT
	movq	8(%rax), %rbx
	cmpq	%rbx, 16(%rax)
	sete	%al
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L215:
	call	_ZNK2v88internal8compiler17ProcessedFeedback15AsElementAccessEv@PLT
	movq	%rax, %rdi
	call	_ZNK2v88internal8compiler21ElementAccessFeedback17transition_groupsEv@PLT
	movq	8(%rax), %rbx
	cmpq	%rbx, 16(%rax)
	sete	%al
.L196:
	movl	$374, %edx
	testb	%al, %al
	jne	.L200
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L216:
	call	_ZNK2v88internal8compiler17ProcessedFeedback13AsNamedAccessEv@PLT
	movq	32(%rax), %rbx
	cmpq	%rbx, 40(%rax)
	sete	%al
	jmp	.L196
.L214:
	call	__stack_chk_fail@PLT
.L199:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE15967:
	.size	_ZN2v88internal8compiler17JSGenericLowering19LowerJSLoadPropertyEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering19LowerJSLoadPropertyEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering16LowerJSLoadNamedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering16LowerJSLoadNamedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering16LowerJSLoadNamedEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering16LowerJSLoadNamedEPNS1_4NodeE:
.LFB15968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movzbl	%al, %r14d
	call	_ZN2v88internal8compiler13NamedAccessOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L218
	addq	$72, %rax
.L219:
	movq	16(%r12), %rdi
	movq	(%rbx), %rsi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	cmpq	$0, 8(%rbx)
	movq	16(%r12), %r8
	je	.L221
	cmpl	$-1, 16(%rbx)
	je	.L221
	leaq	8(%rbx), %r9
	movq	%r8, -128(%rbp)
	movq	%r9, %rdi
	movq	%r9, -120(%rbp)
	call	_ZNK2v88internal8compiler14FeedbackSource5indexEv@PLT
	pxor	%xmm0, %xmm0
	movq	-128(%rbp), %r8
	cvtsi2sdl	%eax, %xmm0
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	(%r15), %rax
	movq	-120(%rbp), %r9
	cmpw	$41, 16(%rax)
	je	.L247
	movq	24(%r12), %rdi
	movq	%r9, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker11GetFeedbackERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdi
	movl	(%rax), %eax
	cmpl	$4, %eax
	je	.L248
	cmpl	$8, %eax
	je	.L249
	testl	%eax, %eax
	jne	.L228
.L230:
	movl	$371, %edx
.L229:
	movq	16(%r12), %rax
	leaq	-112(%rbp), %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L221:
	movq	360(%r8), %rsi
	leaq	-112(%rbp), %rdi
	movl	$710, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
.L246:
	movq	-112(%rbp), %rax
	leaq	-80(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L250
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	movq	32(%rax), %rax
	addq	$56, %rax
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L247:
	movq	24(%r12), %rdi
	movq	%r9, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker11GetFeedbackERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdi
	movl	(%rax), %eax
	cmpl	$4, %eax
	je	.L251
	cmpl	$8, %eax
	je	.L252
	testl	%eax, %eax
	jne	.L228
.L236:
	movl	$368, %edx
.L235:
	movq	16(%r12), %rax
	leaq	-112(%rbp), %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	16(%r12), %rdi
	movq	8(%rbx), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$3, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L248:
	call	_ZNK2v88internal8compiler17ProcessedFeedback15AsElementAccessEv@PLT
	movq	%rax, %rdi
	call	_ZNK2v88internal8compiler21ElementAccessFeedback17transition_groupsEv@PLT
	movq	8(%rax), %rbx
	cmpq	%rbx, 16(%rax)
	sete	%al
.L226:
	testb	%al, %al
	je	.L230
	movl	$372, %edx
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L249:
	call	_ZNK2v88internal8compiler17ProcessedFeedback13AsNamedAccessEv@PLT
	movq	32(%rax), %rbx
	cmpq	%rbx, 40(%rax)
	sete	%al
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L251:
	call	_ZNK2v88internal8compiler17ProcessedFeedback15AsElementAccessEv@PLT
	movq	%rax, %rdi
	call	_ZNK2v88internal8compiler21ElementAccessFeedback17transition_groupsEv@PLT
	movq	8(%rax), %rcx
	cmpq	%rcx, 16(%rax)
	sete	%al
.L233:
	testb	%al, %al
	je	.L236
	movl	$369, %edx
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L252:
	call	_ZNK2v88internal8compiler17ProcessedFeedback13AsNamedAccessEv@PLT
	movq	32(%rax), %rcx
	cmpq	%rcx, 40(%rax)
	sete	%al
	jmp	.L233
.L250:
	call	__stack_chk_fail@PLT
.L228:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE15968:
	.size	_ZN2v88internal8compiler17JSGenericLowering16LowerJSLoadNamedEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering16LowerJSLoadNamedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering17LowerJSLoadGlobalEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering17LowerJSLoadGlobalEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering17LowerJSLoadGlobalEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering17LowerJSLoadGlobalEPNS1_4NodeE:
.LFB15969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movzbl	%al, %r14d
	call	_ZN2v88internal8compiler22LoadGlobalParametersOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L254
	addq	$72, %rax
.L255:
	movq	16(%r12), %rdi
	movq	(%rbx), %rsi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %r8
	leaq	8(%rbx), %rdi
	movq	%r8, -120(%rbp)
	call	_ZNK2v88internal8compiler14FeedbackSource5indexEv@PLT
	pxor	%xmm0, %xmm0
	movq	-120(%rbp), %r8
	cvtsi2sdl	%eax, %xmm0
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	(%r15), %rax
	movl	24(%rbx), %edx
	leaq	-112(%rbp), %rdi
	cmpw	$41, 16(%rax)
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	je	.L256
	call	_ZN2v88internal11CodeFactory12LoadGlobalICEPNS0_7IsolateENS0_10TypeofModeE@PLT
.L260:
	movq	-112(%rbp), %rax
	leaq	-80(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L261
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	movq	32(%rax), %rax
	addq	$56, %rax
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L256:
	call	_ZN2v88internal11CodeFactory27LoadGlobalICInOptimizedCodeEPNS0_7IsolateENS0_10TypeofModeE@PLT
	movq	16(%r12), %rdi
	movq	8(%rbx), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	jmp	.L260
.L261:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15969:
	.size	_ZN2v88internal8compiler17JSGenericLowering17LowerJSLoadGlobalEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering17LowerJSLoadGlobalEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering18LowerJSGetIteratorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering18LowerJSGetIteratorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering18LowerJSGetIteratorEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering18LowerJSGetIteratorEPNS1_4NodeE:
.LFB15970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movzbl	%al, %r14d
	call	_ZN2v88internal8compiler16PropertyAccessOfEPKNS1_8OperatorE@PLT
	movq	16(%r12), %r15
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler14FeedbackSource5indexEv@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	movq	(%rbx), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rax
	leaq	-112(%rbp), %rdi
	movl	$819, %edx
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rax
	leaq	-80(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L265
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L265:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15970:
	.size	_ZN2v88internal8compiler17JSGenericLowering18LowerJSGetIteratorEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering18LowerJSGetIteratorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering20LowerJSStorePropertyEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering20LowerJSStorePropertyEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering20LowerJSStorePropertyEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering20LowerJSStorePropertyEPNS1_4NodeE:
.LFB15971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movzbl	%al, %r14d
	call	_ZN2v88internal8compiler16PropertyAccessOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L267
	addq	$72, %rax
.L268:
	movq	16(%r13), %r8
	movq	%rbx, %rdi
	movq	(%rax), %r15
	movq	%r8, -120(%rbp)
	call	_ZNK2v88internal8compiler14FeedbackSource5indexEv@PLT
	pxor	%xmm0, %xmm0
	movq	-120(%rbp), %r8
	cvtsi2sdl	%eax, %xmm0
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r12, %rdi
	movl	$3, %edx
	movq	%rax, %rcx
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	(%r15), %rax
	leaq	-112(%rbp), %rdi
	cmpw	$41, 16(%rax)
	movq	16(%r13), %rax
	movq	360(%rax), %rsi
	je	.L269
	movl	$382, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
.L273:
	movq	-112(%rbp), %rax
	leaq	-80(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L274
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	movq	32(%rax), %rax
	addq	$56, %rax
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L269:
	movl	$381, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	16(%r13), %rdi
	movq	(%rbx), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	jmp	.L273
.L274:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15971:
	.size	_ZN2v88internal8compiler17JSGenericLowering20LowerJSStorePropertyEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering20LowerJSStorePropertyEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering17LowerJSStoreNamedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering17LowerJSStoreNamedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering17LowerJSStoreNamedEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering17LowerJSStoreNamedEPNS1_4NodeE:
.LFB15972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movzbl	%al, %r14d
	call	_ZN2v88internal8compiler13NamedAccessOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L276
	addq	$72, %rax
.L277:
	movq	16(%r13), %rdi
	movq	(%rbx), %rsi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	cmpq	$0, 8(%rbx)
	je	.L278
	cmpl	$-1, 16(%rbx)
	je	.L278
	movq	16(%r13), %r8
	leaq	8(%rbx), %rdi
	movq	%r8, -120(%rbp)
	call	_ZNK2v88internal8compiler14FeedbackSource5indexEv@PLT
	pxor	%xmm0, %xmm0
	movq	-120(%rbp), %r8
	cvtsi2sdl	%eax, %xmm0
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$3, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	(%r15), %rax
	cmpw	$41, 16(%rax)
	je	.L287
	movq	16(%r13), %rax
	leaq	-112(%rbp), %rdi
	movl	$380, %edx
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
.L286:
	movq	-112(%rbp), %rax
	leaq	-80(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L278:
	movl	$-1, %ecx
	movl	$252, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
.L275:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L288
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	movq	32(%rax), %rax
	addq	$56, %rax
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L287:
	movq	16(%r13), %rax
	leaq	-112(%rbp), %rdi
	movl	$379, %edx
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	16(%r13), %rdi
	movq	8(%rbx), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	jmp	.L286
.L288:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15972:
	.size	_ZN2v88internal8compiler17JSGenericLowering17LowerJSStoreNamedEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering17LowerJSStoreNamedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering20LowerJSStoreNamedOwnEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering20LowerJSStoreNamedOwnEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering20LowerJSStoreNamedOwnEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering20LowerJSStoreNamedOwnEPNS1_4NodeE:
.LFB15973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movzbl	%al, %r14d
	call	_ZN2v88internal8compiler25StoreNamedOwnParametersOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L290
	addq	$72, %rax
.L291:
	movq	16(%r12), %rdi
	movq	(%rbx), %rsi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %r8
	leaq	8(%rbx), %rdi
	movq	%r8, -120(%rbp)
	call	_ZNK2v88internal8compiler14FeedbackSource5indexEv@PLT
	pxor	%xmm0, %xmm0
	movq	-120(%rbp), %r8
	cvtsi2sdl	%eax, %xmm0
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rdi
	movl	$3, %edx
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	(%r15), %rax
	leaq	-112(%rbp), %rdi
	cmpw	$41, 16(%rax)
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	je	.L292
	call	_ZN2v88internal11CodeFactory10StoreOwnICEPNS0_7IsolateE@PLT
.L296:
	movq	-112(%rbp), %rax
	leaq	-80(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L297
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	movq	32(%rax), %rax
	addq	$56, %rax
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L292:
	call	_ZN2v88internal11CodeFactory25StoreOwnICInOptimizedCodeEPNS0_7IsolateE@PLT
	movq	16(%r12), %rdi
	movq	8(%rbx), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$4, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	jmp	.L296
.L297:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15973:
	.size	_ZN2v88internal8compiler17JSGenericLowering20LowerJSStoreNamedOwnEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering20LowerJSStoreNamedOwnEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering18LowerJSStoreGlobalEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering18LowerJSStoreGlobalEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering18LowerJSStoreGlobalEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering18LowerJSStoreGlobalEPNS1_4NodeE:
.LFB15974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movzbl	%al, %r14d
	call	_ZN2v88internal8compiler23StoreGlobalParametersOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L299
	addq	$72, %rax
.L300:
	movq	16(%r12), %rdi
	movq	8(%rbx), %rsi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %r8
	leaq	16(%rbx), %rdi
	movq	%r8, -120(%rbp)
	call	_ZNK2v88internal8compiler14FeedbackSource5indexEv@PLT
	pxor	%xmm0, %xmm0
	movq	-120(%rbp), %r8
	cvtsi2sdl	%eax, %xmm0
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r13, %rdi
	movl	$2, %edx
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	(%r15), %rax
	leaq	-112(%rbp), %rdi
	cmpw	$41, 16(%rax)
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	je	.L301
	movl	$378, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
.L305:
	movq	-112(%rbp), %rax
	leaq	-80(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L306
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	movq	32(%rax), %rax
	addq	$56, %rax
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L301:
	movl	$377, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	16(%r12), %rdi
	movq	16(%rbx), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$3, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	jmp	.L305
.L306:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15974:
	.size	_ZN2v88internal8compiler17JSGenericLowering18LowerJSStoreGlobalEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering18LowerJSStoreGlobalEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering33LowerJSStoreDataPropertyInLiteralEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering33LowerJSStoreDataPropertyInLiteralEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering33LowerJSStoreDataPropertyInLiteralEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering33LowerJSStoreDataPropertyInLiteralEPNS1_4NodeE:
.LFB15975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%r12), %rbx
	subq	$24, %rsp
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler19FeedbackParameterOfEPKNS1_8OperatorE@PLT
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, %r15
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	16(%r13), %rax
	movl	$2, %ecx
	movq	%r12, %rdi
	movl	$4, %edx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node12InsertInputsEPNS0_4ZoneEii@PLT
	movq	16(%r13), %rdi
	movq	(%r15), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r14
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L308
	movq	32(%rbx), %rdi
	cmpq	%rdi, %r14
	je	.L311
	leaq	64(%r12), %rax
	movq	%r12, %rsi
.L310:
	subq	$120, %rsi
	testq	%rdi, %rdi
	je	.L312
	movq	%rax, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rsi
.L312:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L311
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L311:
	movq	%r15, %rdi
	movq	16(%r13), %r14
	call	_ZNK2v88internal8compiler14FeedbackSource5indexEv@PLT
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, %r14
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L313
	movq	40(%rbx), %rdi
	cmpq	%rdi, %r14
	je	.L316
	addq	$40, %rbx
	movq	%r12, %rsi
.L315:
	leaq	-144(%rsi), %r15
	testq	%rdi, %rdi
	je	.L317
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L317:
	movq	%r14, (%rbx)
	testq	%r14, %r14
	je	.L316
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L316:
	addq	$24, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$-1, %ecx
	popq	%rbx
	movl	$216, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	movq	32(%r12), %rsi
	movq	56(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L316
	leaq	56(%rsi), %rbx
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L308:
	movq	32(%r12), %rsi
	movq	48(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L311
	leaq	48(%rsi), %rax
	jmp	.L310
	.cfi_endproc
.LFE15975:
	.size	_ZN2v88internal8compiler17JSGenericLowering33LowerJSStoreDataPropertyInLiteralEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering33LowerJSStoreDataPropertyInLiteralEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering26LowerJSStoreInArrayLiteralEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering26LowerJSStoreInArrayLiteralEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering26LowerJSStoreInArrayLiteralEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering26LowerJSStoreInArrayLiteralEPNS1_4NodeE:
.LFB15976:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$383, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-112(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%r13), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movzbl	%al, %r14d
	call	_ZN2v88internal8compiler19FeedbackParameterOfEPKNS1_8OperatorE@PLT
	movq	8(%r13), %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rax, %rbx
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %rdi
	movq	16(%r13), %r15
	call	_ZNK2v88internal8compiler14FeedbackSource5indexEv@PLT
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$3, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r13), %rdi
	movq	(%rbx), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	-112(%rbp), %rax
	leaq	-80(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L334
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L334:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15976:
	.size	_ZN2v88internal8compiler17JSGenericLowering26LowerJSStoreInArrayLiteralEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering26LowerJSStoreInArrayLiteralEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering21LowerJSDeletePropertyEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering21LowerJSDeletePropertyEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering21LowerJSDeletePropertyEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering21LowerJSDeletePropertyEPNS1_4NodeE:
.LFB15977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$160, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L338
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L338:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15977:
	.size	_ZN2v88internal8compiler17JSGenericLowering21LowerJSDeletePropertyEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering21LowerJSDeletePropertyEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering26LowerJSGetSuperConstructorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering26LowerJSGetSuperConstructorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering26LowerJSGetSuperConstructorEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering26LowerJSGetSuperConstructorEPNS1_4NodeE:
.LFB15978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$110, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L342
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L342:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15978:
	.size	_ZN2v88internal8compiler17JSGenericLowering26LowerJSGetSuperConstructorEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering26LowerJSGetSuperConstructorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering26LowerJSHasInPrototypeChainEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering26LowerJSHasInPrototypeChainEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering26LowerJSHasInPrototypeChainEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering26LowerJSHasInPrototypeChainEPNS1_4NodeE:
.LFB15979:
	.cfi_startproc
	endbr64
	movl	$-1, %ecx
	movl	$226, %edx
	jmp	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	.cfi_endproc
.LFE15979:
	.size	_ZN2v88internal8compiler17JSGenericLowering26LowerJSHasInPrototypeChainEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering26LowerJSHasInPrototypeChainEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering17LowerJSInstanceOfEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering17LowerJSInstanceOfEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering17LowerJSInstanceOfEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering17LowerJSInstanceOfEPNS1_4NodeE:
.LFB15980:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$490, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L347
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L347:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15980:
	.size	_ZN2v88internal8compiler17JSGenericLowering17LowerJSInstanceOfEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering17LowerJSInstanceOfEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering26LowerJSOrdinaryHasInstanceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering26LowerJSOrdinaryHasInstanceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering26LowerJSOrdinaryHasInstanceEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering26LowerJSOrdinaryHasInstanceEPNS1_4NodeE:
.LFB15981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$489, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L351
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L351:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15981:
	.size	_ZN2v88internal8compiler17JSGenericLowering26LowerJSOrdinaryHasInstanceEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering26LowerJSOrdinaryHasInstanceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering18LowerJSLoadContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering18LowerJSLoadContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering18LowerJSLoadContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering18LowerJSLoadContextEPNS1_4NodeE:
.LFB15982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE15982:
	.size	_ZN2v88internal8compiler17JSGenericLowering18LowerJSLoadContextEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering18LowerJSLoadContextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering19LowerJSStoreContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering19LowerJSStoreContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering19LowerJSStoreContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering19LowerJSStoreContextEPNS1_4NodeE:
.LFB15983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE15983:
	.size	_ZN2v88internal8compiler17JSGenericLowering19LowerJSStoreContextEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering19LowerJSStoreContextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering13LowerJSCreateEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering13LowerJSCreateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering13LowerJSCreateEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering13LowerJSCreateEPNS1_4NodeE:
.LFB15984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$31, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L359
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L359:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15984:
	.size	_ZN2v88internal8compiler17JSGenericLowering13LowerJSCreateEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering13LowerJSCreateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering22LowerJSCreateArgumentsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering22LowerJSCreateArgumentsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering22LowerJSCreateArgumentsEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering22LowerJSCreateArgumentsEPNS1_4NodeE:
.LFB15985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler21CreateArgumentsTypeOfEPKNS1_8OperatorE@PLT
	movzbl	(%rax), %eax
	cmpb	$1, %al
	je	.L361
	cmpb	$2, %al
	je	.L362
	movl	$-1, %ecx
	movl	$312, %edx
	testb	%al, %al
	je	.L365
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movl	$-1, %ecx
	movl	$309, %edx
.L365:
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movl	$-1, %ecx
	popq	%r13
	movl	$313, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	.cfi_endproc
.LFE15985:
	.size	_ZN2v88internal8compiler17JSGenericLowering22LowerJSCreateArgumentsEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering22LowerJSCreateArgumentsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering18LowerJSCreateArrayEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering18LowerJSCreateArrayEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering18LowerJSCreateArrayEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering18LowerJSCreateArrayEPNS1_4NodeE:
.LFB15986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler23CreateArrayParametersOfEPKNS1_8OperatorE@PLT
	movq	(%r12), %rcx
	leaq	-80(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	(%rax), %r14
	movq	%rax, -88(%rbp)
	leaq	16+_ZTVN2v88internal26ArrayConstructorDescriptorE(%rip), %rdx
	leaq	240+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	16(%rbx), %rax
	movzbl	18(%rcx), %r8d
	punpcklqdq	%xmm1, %xmm0
	leal	1(%r14), %edx
	movl	$1, %ecx
	movaps	%xmm0, -80(%rbp)
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler7JSGraph28ArrayConstructorStubConstantEv@PLT
	movq	16(%rbx), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	-88(%rbp), %r10
	movq	16(%rbx), %rdi
	movq	%rax, %r14
	movq	8(%r10), %rsi
	testq	%rsi, %rsi
	je	.L371
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r9
.L368:
	movq	16(%rbx), %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	xorl	%edx, %edx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	$3, %edx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %r9
	movq	%r12, %rdi
	movl	$4, %edx
	movq	(%rax), %rax
	movq	%r9, %rcx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	-88(%rbp), %r8
	movq	%r12, %rdi
	movl	$5, %edx
	movq	(%rax), %rax
	movq	%r8, %rcx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L372
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rax, %r9
	jmp	.L368
.L372:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15986:
	.size	_ZN2v88internal8compiler17JSGenericLowering18LowerJSCreateArrayEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering18LowerJSCreateArrayEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateArrayIteratorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateArrayIteratorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateArrayIteratorEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateArrayIteratorEPNS1_4NodeE:
.LFB15987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE15987:
	.size	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateArrayIteratorEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateArrayIteratorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering32LowerJSCreateAsyncFunctionObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering32LowerJSCreateAsyncFunctionObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering32LowerJSCreateAsyncFunctionObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering32LowerJSCreateAsyncFunctionObjectEPNS1_4NodeE:
.LFB15988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE15988:
	.size	_ZN2v88internal8compiler17JSGenericLowering32LowerJSCreateAsyncFunctionObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering32LowerJSCreateAsyncFunctionObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering31LowerJSCreateCollectionIteratorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering31LowerJSCreateCollectionIteratorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering31LowerJSCreateCollectionIteratorEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering31LowerJSCreateCollectionIteratorEPNS1_4NodeE:
.LFB15989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE15989:
	.size	_ZN2v88internal8compiler17JSGenericLowering31LowerJSCreateCollectionIteratorEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering31LowerJSCreateCollectionIteratorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateBoundFunctionEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateBoundFunctionEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateBoundFunctionEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateBoundFunctionEPNS1_4NodeE:
.LFB15990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE15990:
	.size	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateBoundFunctionEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateBoundFunctionEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering20LowerJSObjectIsArrayEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering20LowerJSObjectIsArrayEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering20LowerJSObjectIsArrayEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering20LowerJSObjectIsArrayEPNS1_4NodeE:
.LFB15991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE15991:
	.size	_ZN2v88internal8compiler17JSGenericLowering20LowerJSObjectIsArrayEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering20LowerJSObjectIsArrayEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering19LowerJSCreateObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering19LowerJSCreateObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering19LowerJSCreateObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering19LowerJSCreateObjectEPNS1_4NodeE:
.LFB15992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$461, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L386
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L386:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15992:
	.size	_ZN2v88internal8compiler17JSGenericLowering19LowerJSCreateObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering19LowerJSCreateObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering15LowerJSParseIntEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering15LowerJSParseIntEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering15LowerJSParseIntEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering15LowerJSParseIntEPNS1_4NodeE:
.LFB15993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$427, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L390
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L390:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15993:
	.size	_ZN2v88internal8compiler17JSGenericLowering15LowerJSParseIntEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering15LowerJSParseIntEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering17LowerJSRegExpTestEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering17LowerJSRegExpTestEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering17LowerJSRegExpTestEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering17LowerJSRegExpTestEPNS1_4NodeE:
.LFB15994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$873, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L394
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L394:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15994:
	.size	_ZN2v88internal8compiler17JSGenericLowering17LowerJSRegExpTestEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering17LowerJSRegExpTestEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering20LowerJSCreateClosureEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering20LowerJSCreateClosureEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering20LowerJSCreateClosureEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering20LowerJSCreateClosureEPNS1_4NodeE:
.LFB15995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -40
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler25CreateClosureParametersOfEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rdi
	movq	(%rax), %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	movq	8(%rbx), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movl	$4, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	cmpb	$0, 24(%rbx)
	jne	.L396
	movq	16(%r12), %rax
	movl	$32, %edx
	leaq	-96(%rbp), %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	0(%r13), %rdi
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-64(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movzbl	%al, %ecx
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
.L395:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L400
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	movl	$-1, %ecx
	movl	$307, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	jmp	.L395
.L400:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15995:
	.size	_ZN2v88internal8compiler17JSGenericLowering20LowerJSCreateClosureEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering20LowerJSCreateClosureEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering28LowerJSCreateFunctionContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering28LowerJSCreateFunctionContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering28LowerJSCreateFunctionContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering28LowerJSCreateFunctionContextEPNS1_4NodeE:
.LFB15996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler33CreateFunctionContextParametersOfEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movq	(%rax), %r15
	movl	8(%rax), %r14d
	movzbl	12(%rax), %ebx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	cmpb	$1, _ZN2v88internal46FLAG_test_small_max_function_context_stub_sizeE(%rip)
	movq	16(%r12), %rdi
	movzbl	%al, %r8d
	sbbl	%eax, %eax
	andl	$16367, %eax
	movl	%r8d, -116(%rbp)
	addl	$10, %eax
	cmpl	%r14d, %eax
	jl	.L403
	leaq	-112(%rbp), %r9
	movq	360(%rdi), %rsi
	movzbl	%bl, %edx
	movq	%r9, %rdi
	call	_ZN2v88internal11CodeFactory22FastNewFunctionContextEPNS0_7IsolateENS0_9ScopeTypeE@PLT
	movq	16(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	-112(%rbp), %rax
	movl	-116(%rbp), %r8d
	movq	%r13, %rsi
	leaq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	%r8d, %ecx
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
.L401:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L408
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movl	$-1, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$308, %edx
	call	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	jmp	.L401
.L408:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15996:
	.size	_ZN2v88internal8compiler17JSGenericLowering28LowerJSCreateFunctionContextEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering28LowerJSCreateFunctionContextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering28LowerJSCreateGeneratorObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering28LowerJSCreateGeneratorObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering28LowerJSCreateGeneratorObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering28LowerJSCreateGeneratorObjectEPNS1_4NodeE:
.LFB15997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movl	$351, %edx
	leaq	-96(%rbp), %rdi
	movzbl	%al, %r14d
	movq	16(%r13), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L412
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L412:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15997:
	.size	_ZN2v88internal8compiler17JSGenericLowering28LowerJSCreateGeneratorObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering28LowerJSCreateGeneratorObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering29LowerJSCreateIterResultObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering29LowerJSCreateIterResultObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering29LowerJSCreateIterResultObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering29LowerJSCreateIterResultObjectEPNS1_4NodeE:
.LFB15998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE15998:
	.size	_ZN2v88internal8compiler17JSGenericLowering29LowerJSCreateIterResultObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering29LowerJSCreateIterResultObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering27LowerJSCreateStringIteratorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering27LowerJSCreateStringIteratorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering27LowerJSCreateStringIteratorEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering27LowerJSCreateStringIteratorEPNS1_4NodeE:
.LFB15999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE15999:
	.size	_ZN2v88internal8compiler17JSGenericLowering27LowerJSCreateStringIteratorEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering27LowerJSCreateStringIteratorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateKeyValueArrayEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateKeyValueArrayEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateKeyValueArrayEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateKeyValueArrayEPNS1_4NodeE:
.LFB16000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16000:
	.size	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateKeyValueArrayEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateKeyValueArrayEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering20LowerJSCreatePromiseEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering20LowerJSCreatePromiseEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering20LowerJSCreatePromiseEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering20LowerJSCreatePromiseEPNS1_4NodeE:
.LFB16001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16001:
	.size	_ZN2v88internal8compiler17JSGenericLowering20LowerJSCreatePromiseEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering20LowerJSCreatePromiseEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering23LowerJSCreateTypedArrayEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering23LowerJSCreateTypedArrayEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering23LowerJSCreateTypedArrayEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering23LowerJSCreateTypedArrayEPNS1_4NodeE:
.LFB16002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$915, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L424
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L424:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16002:
	.size	_ZN2v88internal8compiler17JSGenericLowering23LowerJSCreateTypedArrayEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering23LowerJSCreateTypedArrayEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateLiteralArrayEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateLiteralArrayEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateLiteralArrayEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateLiteralArrayEPNS1_4NodeE:
.LFB16003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler25CreateLiteralParametersOfEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rdi
	movq	8(%rbx), %rsi
	movzbl	%al, %r14d
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	leaq	8(%rbx), %rdi
	movq	16(%r12), %r15
	call	_ZNK2v88internal8compiler14FeedbackSource5indexEv@PLT
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	movq	(%rbx), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdi
	movl	$2, %edx
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movl	28(%rbx), %eax
	movq	16(%r12), %rdi
	testb	$1, %al
	je	.L427
	cmpl	$16375, 24(%rbx)
	jle	.L431
.L427:
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$3, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movl	$-1, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$186, %edx
	call	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
.L425:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L432
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	.cfi_restore_state
	movq	360(%rdi), %rsi
	leaq	-112(%rbp), %r8
	movl	$37, %edx
	movq	%r8, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rax
	leaq	-80(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	jmp	.L425
.L432:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16003:
	.size	_ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateLiteralArrayEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateLiteralArrayEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering30LowerJSCreateEmptyLiteralArrayEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering30LowerJSCreateEmptyLiteralArrayEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering30LowerJSCreateEmptyLiteralArrayEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering30LowerJSCreateEmptyLiteralArrayEPNS1_4NodeE:
.LFB16004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movzbl	%al, %r15d
	call	_ZN2v88internal8compiler19FeedbackParameterOfEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rdi
	movq	(%rax), %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	%r14, %rdi
	movq	16(%r12), %rbx
	call	_ZNK2v88internal8compiler14FeedbackSource5indexEv@PLT
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movl	$4, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	16(%r12), %rax
	leaq	-112(%rbp), %rdi
	movl	$36, %edx
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rax
	leaq	-80(%rbp), %rdx
	movl	%r15d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L436
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L436:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16004:
	.size	_ZN2v88internal8compiler17JSGenericLowering30LowerJSCreateEmptyLiteralArrayEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering30LowerJSCreateEmptyLiteralArrayEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering30LowerJSCreateArrayFromIterableEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering30LowerJSCreateArrayFromIterableEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering30LowerJSCreateArrayFromIterableEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering30LowerJSCreateArrayFromIterableEPNS1_4NodeE:
.LFB16005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$393, %edx
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L440
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L440:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16005:
	.size	_ZN2v88internal8compiler17JSGenericLowering30LowerJSCreateArrayFromIterableEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering30LowerJSCreateArrayFromIterableEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateLiteralObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateLiteralObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateLiteralObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateLiteralObjectEPNS1_4NodeE:
.LFB16006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler25CreateLiteralParametersOfEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rdi
	movq	8(%rbx), %rsi
	movzbl	%al, %r14d
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	leaq	8(%rbx), %rdi
	movq	16(%r12), %r15
	call	_ZNK2v88internal8compiler14FeedbackSource5indexEv@PLT
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	movq	(%rbx), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	pxor	%xmm0, %xmm0
	movq	16(%r12), %rdi
	cvtsi2sdl	28(%rbx), %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$3, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	testb	$1, 28(%rbx)
	je	.L442
	cmpl	$2730, 24(%rbx)
	jle	.L449
.L442:
	movl	$-1, %ecx
	movl	$188, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
.L441:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L450
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	movq	16(%r12), %rax
	leaq	-112(%rbp), %rdi
	movl	$38, %edx
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rax
	leaq	-80(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	jmp	.L441
.L450:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16006:
	.size	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateLiteralObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateLiteralObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering18LowerJSCloneObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering18LowerJSCloneObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering18LowerJSCloneObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering18LowerJSCloneObjectEPNS1_4NodeE:
.LFB16007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler23CloneObjectParametersOfEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movl	$388, %edx
	leaq	-112(%rbp), %rdi
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	pxor	%xmm0, %xmm0
	movq	16(%r12), %rdi
	cvtsi2sdl	16(%rbx), %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	%rbx, %rdi
	movq	16(%r12), %r15
	call	_ZNK2v88internal8compiler14FeedbackSource5indexEv@PLT
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	movq	(%rbx), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$3, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	-112(%rbp), %rax
	leaq	-80(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L454
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L454:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16007:
	.size	_ZN2v88internal8compiler17JSGenericLowering18LowerJSCloneObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering18LowerJSCloneObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering31LowerJSCreateEmptyLiteralObjectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering31LowerJSCreateEmptyLiteralObjectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering31LowerJSCreateEmptyLiteralObjectEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering31LowerJSCreateEmptyLiteralObjectEPNS1_4NodeE:
.LFB16008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16008:
	.size	_ZN2v88internal8compiler17JSGenericLowering31LowerJSCreateEmptyLiteralObjectEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering31LowerJSCreateEmptyLiteralObjectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateLiteralRegExpEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateLiteralRegExpEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateLiteralRegExpEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateLiteralRegExpEPNS1_4NodeE:
.LFB16009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler25CreateLiteralParametersOfEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movl	$35, %edx
	leaq	-112(%rbp), %rdi
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	16(%r12), %rdi
	movq	8(%rbx), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	leaq	8(%rbx), %rdi
	movq	16(%r12), %r15
	call	_ZNK2v88internal8compiler14FeedbackSource5indexEv@PLT
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	movq	(%rbx), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	pxor	%xmm0, %xmm0
	movq	16(%r12), %rdi
	cvtsi2sdl	28(%rbx), %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$3, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	-112(%rbp), %rax
	leaq	-80(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L460
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L460:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16009:
	.size	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateLiteralRegExpEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateLiteralRegExpEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateCatchContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateCatchContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateCatchContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateCatchContextEPNS1_4NodeE:
.LFB16010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16010:
	.size	_ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateCatchContextEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateCatchContextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering24LowerJSCreateWithContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering24LowerJSCreateWithContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering24LowerJSCreateWithContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering24LowerJSCreateWithContextEPNS1_4NodeE:
.LFB16011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16011:
	.size	_ZN2v88internal8compiler17JSGenericLowering24LowerJSCreateWithContextEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering24LowerJSCreateWithContextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateBlockContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateBlockContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateBlockContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateBlockContextEPNS1_4NodeE:
.LFB16012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler11ScopeInfoOfEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdi
	xorl	%edx, %edx
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$-1, %ecx
	popq	%r12
	movl	$314, %edx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	.cfi_endproc
.LFE16012:
	.size	_ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateBlockContextEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateBlockContextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering30LowerJSConstructForwardVarargsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering30LowerJSConstructForwardVarargsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering30LowerJSConstructForwardVarargsEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering30LowerJSConstructForwardVarargsEPNS1_4NodeE:
.LFB16013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler35ConstructForwardVarargsParametersOfEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movl	(%rax), %r10d
	movzwl	%r10w, %r13d
	movl	%r10d, -112(%rbp)
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-80(%rbp), %rdi
	leal	-1(%r13), %r15d
	movzbl	%al, %r14d
	movq	16(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal11CodeFactory23ConstructForwardVarargsEPNS0_7IsolateE@PLT
	xorl	%r9d, %r9d
	movl	%r14d, %ecx
	movl	%r15d, %edx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	xorl	%r8d, %r8d
	leaq	-96(%rbp), %rsi
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-80(%rbp), %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	16(%rbx), %rdi
	leal	-2(%r13), %esi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movl	-112(%rbp), %r10d
	movq	16(%rbx), %rdi
	movq	%rax, %r13
	movl	%r10d, %esi
	shrl	$16, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movzbl	23(%r12), %edx
	movq	%rax, %r9
	movslq	%r15d, %rax
	andl	$15, %edx
	salq	$3, %rax
	cmpl	$15, %edx
	je	.L468
	leaq	32(%r12), %rcx
	addq	%rcx, %rax
.L469:
	movq	(%rax), %r10
	movq	16(%rbx), %rdi
	movq	%r9, -128(%rbp)
	movq	%r10, -120(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	16(%rbx), %rax
	movq	-104(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	-120(%rbp), %r10
	movq	%r12, %rdi
	movl	$2, %edx
	movq	(%rax), %rax
	movq	%r10, %rcx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$3, %edx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	-128(%rbp), %r9
	movq	%r12, %rdi
	movl	$4, %edx
	movq	(%rax), %rax
	movq	%r9, %rcx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	-112(%rbp), %r8
	movq	%r12, %rdi
	movl	$5, %edx
	movq	(%rax), %rax
	movq	%r8, %rcx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r14, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L472
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_restore_state
	movq	32(%r12), %rdx
	leaq	16(%rdx,%rax), %rax
	jmp	.L469
.L472:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16013:
	.size	_ZN2v88internal8compiler17JSGenericLowering30LowerJSConstructForwardVarargsEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering30LowerJSConstructForwardVarargsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering16LowerJSConstructEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering16LowerJSConstructEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering16LowerJSConstructEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering16LowerJSConstructEPNS1_4NodeE:
.LFB16014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21ConstructParametersOfEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movl	(%rax), %r13d
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-80(%rbp), %rdi
	movzbl	%al, %r14d
	movq	16(%rbx), %rax
	leal	-1(%r13), %r15d
	movq	360(%rax), %rsi
	call	_ZN2v88internal11CodeFactory9ConstructEPNS0_7IsolateE@PLT
	xorl	%r8d, %r8d
	movl	%r14d, %ecx
	movl	%r15d, %edx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	xorl	%r9d, %r9d
	leaq	-96(%rbp), %rsi
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-80(%rbp), %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	16(%rbx), %rdi
	leal	-2(%r13), %esi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movzbl	23(%r12), %edx
	movq	%rax, %r8
	movslq	%r15d, %rax
	andl	$15, %edx
	salq	$3, %rax
	cmpl	$15, %edx
	je	.L474
	leaq	32(%r12), %rcx
	addq	%rcx, %rax
.L475:
	movq	(%rax), %r9
	movq	16(%rbx), %rdi
	movq	%r8, -120(%rbp)
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	16(%rbx), %rax
	movq	-104(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	-112(%rbp), %r9
	movq	%r12, %rdi
	movl	$2, %edx
	movq	(%rax), %rax
	movq	%r9, %rcx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	-120(%rbp), %r8
	movq	%r12, %rdi
	movl	$3, %edx
	movq	(%rax), %rax
	movq	%r8, %rcx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$4, %edx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r14, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L478
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_restore_state
	movq	32(%r12), %rdx
	leaq	16(%rdx,%rax), %rax
	jmp	.L475
.L478:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16014:
	.size	_ZN2v88internal8compiler17JSGenericLowering16LowerJSConstructEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering16LowerJSConstructEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering29LowerJSConstructWithArrayLikeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering29LowerJSConstructWithArrayLikeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering29LowerJSConstructWithArrayLikeEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering29LowerJSConstructWithArrayLikeEPNS1_4NodeE:
.LFB16015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$26, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-80(%rbp), %rdi
	leaq	32(%r12), %r13
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	movzbl	%al, %ecx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-96(%rbp), %rsi
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-80(%rbp), %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movzbl	23(%r12), %edx
	movq	-120(%rbp), %rcx
	movq	%rax, -112(%rbp)
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L480
	movq	8(%r13), %r15
	leaq	48(%r12), %rdx
.L481:
	movq	(%rdx), %r14
	movq	16(%rbx), %rdx
	movq	%r12, %rdi
	movq	(%rdx), %rsi
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L482
	movq	16(%r13), %rdi
	cmpq	%rdi, %r14
	je	.L483
	leaq	16(%r13), %rdx
	movq	%r12, %rsi
.L484:
	subq	$72, %rsi
	testq	%rdi, %rdi
	je	.L486
	movq	%rdx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rsi
.L486:
	movq	%r14, (%rdx)
	testq	%r14, %r14
	je	.L487
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L487:
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L483
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rdx
.L485:
	movq	24(%rdx), %rdi
	cmpq	%r15, %rdi
	je	.L490
	leaq	24(%rdx), %rax
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L483:
	movq	24(%r13), %rdi
	cmpq	%r15, %rdi
	je	.L490
	leaq	24(%r13), %rax
	movq	%r12, %rsi
.L489:
	subq	$96, %rsi
	testq	%rdi, %rdi
	je	.L491
	movq	%rax, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rsi
.L491:
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L490
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L490:
	movq	16(%rbx), %rax
	movq	-112(%rbp), %rcx
	movl	$4, %edx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	-104(%rbp), %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L506
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	.cfi_restore_state
	movq	32(%r12), %rsi
	movq	32(%rsi), %rdi
	leaq	16(%rsi), %rdx
	cmpq	%rdi, %r14
	je	.L485
	leaq	32(%rsi), %rdx
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L480:
	movq	32(%r12), %rdx
	movq	24(%rdx), %r15
	addq	$32, %rdx
	jmp	.L481
.L506:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16015:
	.size	_ZN2v88internal8compiler17JSGenericLowering29LowerJSConstructWithArrayLikeEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering29LowerJSConstructWithArrayLikeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering26LowerJSConstructWithSpreadEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering26LowerJSConstructWithSpreadEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering26LowerJSConstructWithSpreadEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering26LowerJSConstructWithSpreadEPNS1_4NodeE:
.LFB16016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21ConstructParametersOfEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movl	(%rax), %r11d
	leal	-2(%r11), %r15d
	movl	%r11d, -112(%rbp)
	leal	-1(%r11), %ebx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-80(%rbp), %rdi
	movzbl	%al, %r13d
	movq	16(%r14), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal11CodeFactory19ConstructWithSpreadEPNS0_7IsolateE@PLT
	xorl	%r8d, %r8d
	movl	%r13d, %ecx
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	%r15d, %edx
	leaq	-96(%rbp), %rsi
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%r14), %rdi
	movq	-80(%rbp), %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	-112(%rbp), %r11d
	movq	16(%r14), %rdi
	movq	%rax, -104(%rbp)
	leal	-3(%r11), %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	%rax, %r8
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L508
	leaq	32(%r12), %rdx
	movslq	%ebx, %rax
	movq	(%rdx,%rax,8), %r11
	movslq	%r15d, %rax
	leaq	(%rdx,%rax,8), %rax
.L509:
	movq	(%rax), %r9
	movq	16(%r14), %rdi
	movq	%r8, -128(%rbp)
	movq	%r11, -136(%rbp)
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	16(%r14), %rdx
	movq	-104(%rbp), %rcx
	movq	%r12, %rdi
	movq	(%rdx), %rsi
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r14), %rdx
	movq	-136(%rbp), %r11
	movq	%r12, %rdi
	movq	(%rdx), %rsi
	movq	%r11, %rcx
	movl	$2, %edx
	movq	(%rsi), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r14), %rdx
	movq	-128(%rbp), %r8
	movq	%r12, %rdi
	movq	(%rdx), %rsi
	movq	%r8, %rcx
	movl	$3, %edx
	movq	(%rsi), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r14), %rdx
	movq	-120(%rbp), %r9
	movq	%r12, %rdi
	movq	(%rdx), %rsi
	movq	%r9, %rcx
	movl	$4, %edx
	movq	(%rsi), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r14), %rdx
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rdx), %rsi
	movq	%rax, %rcx
	movl	$5, %edx
	movq	(%rsi), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r14), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L512
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	.cfi_restore_state
	movq	32(%r12), %rax
	movslq	%ebx, %rdx
	addq	$16, %rax
	movq	(%rax,%rdx,8), %r11
	movslq	%r15d, %rdx
	leaq	(%rax,%rdx,8), %rax
	jmp	.L509
.L512:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16016:
	.size	_ZN2v88internal8compiler17JSGenericLowering26LowerJSConstructWithSpreadEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering26LowerJSConstructWithSpreadEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering25LowerJSCallForwardVarargsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering25LowerJSCallForwardVarargsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering25LowerJSCallForwardVarargsEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering25LowerJSCallForwardVarargsEPNS1_4NodeE:
.LFB16017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler30CallForwardVarargsParametersOfEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movl	(%rax), %r13d
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-80(%rbp), %rdi
	movzbl	%al, %r15d
	movq	16(%rbx), %rax
	movl	%r13d, %r14d
	andl	$32767, %r14d
	movq	360(%rax), %rsi
	call	_ZN2v88internal11CodeFactory18CallForwardVarargsEPNS0_7IsolateE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leal	-1(%r14), %edx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	%r15d, %ecx
	leaq	-96(%rbp), %rsi
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-80(%rbp), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	16(%rbx), %rdi
	leal	-2(%r14), %esi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movl	%r13d, %esi
	movq	16(%rbx), %rdi
	shrl	$15, %esi
	movq	%rax, %r14
	andl	$32767, %esi
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	-104(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	$2, %edx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$3, %edx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r15, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L516
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L516:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16017:
	.size	_ZN2v88internal8compiler17JSGenericLowering25LowerJSCallForwardVarargsEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering25LowerJSCallForwardVarargsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering11LowerJSCallEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering11LowerJSCallEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering11LowerJSCallEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering11LowerJSCallEPNS1_4NodeE:
.LFB16018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16CallParametersOfEPKNS1_8OperatorE@PLT
	leaq	-80(%rbp), %rdi
	movl	(%rax), %edx
	movq	16(%rbx), %rax
	movl	%edx, %r13d
	movq	360(%rax), %rsi
	shrl	$29, %edx
	andl	$3, %edx
	andl	$268435455, %r13d
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leal	-1(%r13), %edx
	movzbl	%al, %ecx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-96(%rbp), %rsi
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-80(%rbp), %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	16(%rbx), %rdi
	leal	-2(%r13), %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	xorl	%edx, %edx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$2, %edx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r14, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L520
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L520:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16018:
	.size	_ZN2v88internal8compiler17JSGenericLowering11LowerJSCallEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering11LowerJSCallEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering24LowerJSCallWithArrayLikeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering24LowerJSCallWithArrayLikeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering24LowerJSCallWithArrayLikeEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering24LowerJSCallWithArrayLikeEPNS1_4NodeE:
.LFB16019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-80(%rbp), %rdi
	leaq	32(%r12), %r15
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal11CodeFactory17CallWithArrayLikeEPNS0_7IsolateE@PLT
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movzbl	%al, %ecx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %edx
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-80(%rbp), %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %rcx
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L522
	movq	8(%r15), %r13
	leaq	48(%r12), %rax
.L523:
	movq	(%rax), %r14
	movq	16(%rbx), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L524
	movq	24(%r15), %rdi
	cmpq	%r13, %rdi
	je	.L525
	leaq	24(%r15), %rax
	movq	%r12, %rsi
.L526:
	subq	$96, %rsi
	testq	%rdi, %rdi
	je	.L528
	movq	%rax, -120(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rsi
.L528:
	movq	%r13, (%rax)
	testq	%r13, %r13
	je	.L529
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L529:
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L525
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rax
.L527:
	movq	16(%rax), %rdi
	cmpq	%rdi, %r14
	je	.L532
	leaq	16(%rax), %r8
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L525:
	movq	16(%r15), %rdi
	cmpq	%rdi, %r14
	je	.L532
	leaq	16(%r15), %r8
	movq	%r12, %rsi
.L531:
	leaq	-72(%rsi), %r15
	testq	%rdi, %rdi
	je	.L533
	movq	%r15, %rsi
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-112(%rbp), %r8
.L533:
	movq	%r14, (%r8)
	testq	%r14, %r14
	je	.L532
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L532:
	movq	16(%rbx), %rax
	movq	-104(%rbp), %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L548
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	movq	32(%r12), %rsi
	movq	40(%rsi), %rdi
	leaq	16(%rsi), %rax
	cmpq	%r13, %rdi
	je	.L527
	leaq	40(%rsi), %rax
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L522:
	movq	32(%r12), %rax
	movq	24(%rax), %r13
	addq	$32, %rax
	jmp	.L523
.L548:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16019:
	.size	_ZN2v88internal8compiler17JSGenericLowering24LowerJSCallWithArrayLikeEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering24LowerJSCallWithArrayLikeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering21LowerJSCallWithSpreadEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering21LowerJSCallWithSpreadEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering21LowerJSCallWithSpreadEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering21LowerJSCallWithSpreadEPNS1_4NodeE:
.LFB16020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16CallParametersOfEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movl	(%rax), %r13d
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-80(%rbp), %rdi
	movzbl	%al, %r14d
	movq	16(%rbx), %rax
	andl	$268435455, %r13d
	movq	360(%rax), %rsi
	call	_ZN2v88internal11CodeFactory14CallWithSpreadEPNS0_7IsolateE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%r14d, %ecx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leal	-2(%r13), %edx
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	leaq	-96(%rbp), %rsi
	movq	%rax, -88(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	16(%rbx), %rdi
	movq	-80(%rbp), %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	16(%rbx), %rdi
	leal	-3(%r13), %esi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	movq	-104(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r15, %rcx
	movq	%r12, %rdi
	movl	$2, %edx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movzbl	23(%r12), %edx
	leal	1(%r13), %eax
	salq	$3, %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L550
	leaq	32(%r12), %rcx
	addq	%rcx, %rax
.L551:
	movq	16(%rbx), %rdx
	movq	(%rax), %rcx
	movq	%r12, %rdi
	movq	(%rdx), %rsi
	movl	$3, %edx
	movq	(%rsi), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	leal	2(%r13), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	16(%rbx), %rax
	movq	%r14, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L554
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L550:
	.cfi_restore_state
	movq	32(%r12), %rdx
	leaq	16(%rdx,%rax), %rax
	jmp	.L551
.L554:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16020:
	.size	_ZN2v88internal8compiler17JSGenericLowering21LowerJSCallWithSpreadEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering21LowerJSCallWithSpreadEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering18LowerJSCallRuntimeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering18LowerJSCallRuntimeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering18LowerJSCallRuntimeEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering18LowerJSCallRuntimeEPNS1_4NodeE:
.LFB16021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rsi), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler23CallRuntimeParametersOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movl	8(%rax), %ecx
	movl	(%rax), %edx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	.cfi_endproc
.LFE16021:
	.size	_ZN2v88internal8compiler17JSGenericLowering18LowerJSCallRuntimeEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering18LowerJSCallRuntimeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering16LowerJSForInNextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering16LowerJSForInNextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering16LowerJSForInNextEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering16LowerJSForInNextEPNS1_4NodeE:
.LFB16022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16022:
	.size	_ZN2v88internal8compiler17JSGenericLowering16LowerJSForInNextEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering16LowerJSForInNextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering19LowerJSForInPrepareEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering19LowerJSForInPrepareEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering19LowerJSForInPrepareEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering19LowerJSForInPrepareEPNS1_4NodeE:
.LFB16023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16023:
	.size	_ZN2v88internal8compiler17JSGenericLowering19LowerJSForInPrepareEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering19LowerJSForInPrepareEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering18LowerJSLoadMessageEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering18LowerJSLoadMessageEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering18LowerJSLoadMessageEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering18LowerJSLoadMessageEPNS1_4NodeE:
.LFB16024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16024:
	.size	_ZN2v88internal8compiler17JSGenericLowering18LowerJSLoadMessageEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering18LowerJSLoadMessageEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering19LowerJSStoreMessageEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering19LowerJSStoreMessageEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering19LowerJSStoreMessageEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering19LowerJSStoreMessageEPNS1_4NodeE:
.LFB16025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16025:
	.size	_ZN2v88internal8compiler17JSGenericLowering19LowerJSStoreMessageEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering19LowerJSStoreMessageEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering17LowerJSLoadModuleEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering17LowerJSLoadModuleEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering17LowerJSLoadModuleEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering17LowerJSLoadModuleEPNS1_4NodeE:
.LFB16026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16026:
	.size	_ZN2v88internal8compiler17JSGenericLowering17LowerJSLoadModuleEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering17LowerJSLoadModuleEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering18LowerJSStoreModuleEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering18LowerJSStoreModuleEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering18LowerJSStoreModuleEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering18LowerJSStoreModuleEPNS1_4NodeE:
.LFB16027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16027:
	.size	_ZN2v88internal8compiler17JSGenericLowering18LowerJSStoreModuleEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering18LowerJSStoreModuleEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering21LowerJSGeneratorStoreEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering21LowerJSGeneratorStoreEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering21LowerJSGeneratorStoreEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering21LowerJSGeneratorStoreEPNS1_4NodeE:
.LFB16028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16028:
	.size	_ZN2v88internal8compiler17JSGenericLowering21LowerJSGeneratorStoreEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering21LowerJSGeneratorStoreEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering35LowerJSGeneratorRestoreContinuationEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering35LowerJSGeneratorRestoreContinuationEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering35LowerJSGeneratorRestoreContinuationEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering35LowerJSGeneratorRestoreContinuationEPNS1_4NodeE:
.LFB16029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16029:
	.size	_ZN2v88internal8compiler17JSGenericLowering35LowerJSGeneratorRestoreContinuationEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering35LowerJSGeneratorRestoreContinuationEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering30LowerJSGeneratorRestoreContextEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering30LowerJSGeneratorRestoreContextEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering30LowerJSGeneratorRestoreContextEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering30LowerJSGeneratorRestoreContextEPNS1_4NodeE:
.LFB16030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16030:
	.size	_ZN2v88internal8compiler17JSGenericLowering30LowerJSGeneratorRestoreContextEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering30LowerJSGeneratorRestoreContextEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering38LowerJSGeneratorRestoreInputOrDebugPosEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering38LowerJSGeneratorRestoreInputOrDebugPosEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering38LowerJSGeneratorRestoreInputOrDebugPosEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering38LowerJSGeneratorRestoreInputOrDebugPosEPNS1_4NodeE:
.LFB16031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16031:
	.size	_ZN2v88internal8compiler17JSGenericLowering38LowerJSGeneratorRestoreInputOrDebugPosEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering38LowerJSGeneratorRestoreInputOrDebugPosEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering31LowerJSGeneratorRestoreRegisterEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering31LowerJSGeneratorRestoreRegisterEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering31LowerJSGeneratorRestoreRegisterEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering31LowerJSGeneratorRestoreRegisterEPNS1_4NodeE:
.LFB16032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE16032:
	.size	_ZN2v88internal8compiler17JSGenericLowering31LowerJSGeneratorRestoreRegisterEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering31LowerJSGeneratorRestoreRegisterEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering17LowerJSStackCheckEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering17LowerJSStackCheckEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering17LowerJSStackCheckEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering17LowerJSStackCheckEPNS1_4NodeE:
.LFB16033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	16(%r13), %rdi
	xorl	%esi, %esi
	movq	%rax, -104(%rbp)
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler12MachineGraph14IntPtrConstantEl@PLT
	movq	16(%r13), %r15
	movq	%rax, -112(%rbp)
	movq	360(%r15), %rdi
	call	_ZN2v88internal17ExternalReference18address_of_jslimitEPNS0_7IsolateE@PLT
	movq	%r15, %rdi
	leaq	-96(%rbp), %r15
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$5, %esi
	movq	%rax, -120(%rbp)
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder4LoadENS0_11MachineTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-120(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-128(%rbp), %xmm0
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rbx
	movq	16(%r13), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder23StackPointerGreaterThanEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movl	$1, %esi
	movq	%rax, -112(%rbp)
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	movq	16(%r13), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	-104(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r14, -96(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -104(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	-112(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r14, -96(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	16(%r13), %rax
	movl	$2, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	%r12, %xmm1
	movq	%r15, %rcx
	xorl	%r8d, %r8d
	movq	-104(%rbp), %xmm0
	movl	$2, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, %r14
	movq	%rax, -104(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	-112(%rbp), %r9
	movq	%r15, %rcx
	xorl	%r8d, %r8d
	movq	%r12, %xmm2
	movq	%rbx, %xmm0
	movq	%rax, %rsi
	movq	%r14, -80(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movq	%r9, %rdi
	movl	$3, %edx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %r8
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14NodeProperties11ReplaceUsesEPNS1_4NodeES4_S4_S4_S4_@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	movq	%r15, %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	24(%r14), %r15
	testq	%r15, %r15
	je	.L580
	movq	(%r15), %rbx
	.p2align 4,,10
	.p2align 3
.L595:
	movl	16(%r15), %ecx
	movq	%r15, %rdi
	movl	%ecx, %edx
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rax
	leaq	(%r15,%rax,8), %rax
	leaq	32(%rax), %rsi
	addq	$16, %rax
	andl	$1, %ecx
	cmovne	%rsi, %rax
	leaq	(%rax,%rdx,8), %r14
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L584
	movl	16(%r15), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r15,%rax,8), %r8
	movq	(%r8), %rdi
	jne	.L585
	movq	(%rdi), %rax
	cmpw	$6, 16(%rax)
	je	.L596
.L586:
	movq	%rdi, %r8
	movq	(%rdi), %rdi
.L592:
	cmpw	$7, 16(%rdi)
	je	.L618
.L584:
	testq	%rbx, %rbx
	je	.L580
	movq	%rbx, %r15
	movq	(%rbx), %rbx
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L580:
	movl	$-1, %ecx
	movl	$159, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L619
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L585:
	.cfi_restore_state
	cmpw	$6, 16(%rdi)
	jne	.L592
	movq	%r8, %rdi
.L596:
	movq	-104(%rbp), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	call	_ZN2v88internal8compiler14NodeProperties11ReplaceUsesEPNS1_4NodeES4_S4_S4_S4_@PLT
	movl	16(%r15), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r15,%rax,8), %rsi
	jne	.L587
	movq	(%rsi), %rsi
.L587:
	movq	-104(%rbp), %rdi
	movl	$1, %edx
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	movq	(%r14), %rdi
	cmpq	%rdi, %r12
	je	.L616
	testq	%rdi, %rdi
	je	.L590
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L590:
	movq	%r12, (%r14)
	testq	%r12, %r12
	je	.L616
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L616:
	movl	16(%r15), %eax
	movl	%eax, %edx
	andl	$1, %eax
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%r15,%rdx,8), %r8
	movq	(%r8), %rdi
	testb	%al, %al
	je	.L586
	cmpw	$7, 16(%rdi)
	jne	.L584
	.p2align 4,,10
	.p2align 3
.L618:
	movq	%r8, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	(%r14), %rdi
	cmpq	%rdi, %r12
	je	.L584
	testq	%rdi, %rdi
	je	.L594
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L594:
	movq	%r12, (%r14)
	testq	%r12, %r12
	je	.L584
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L584
.L619:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16033:
	.size	_ZN2v88internal8compiler17JSGenericLowering17LowerJSStackCheckEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering17LowerJSStackCheckEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering6ReduceEPNS1_4NodeE:
.LFB15914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -40
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdi), %eax
	subw	$682, %ax
	cmpw	$106, %ax
	ja	.L621
	leaq	.L623(%rip), %rdx
	movzwl	%ax, %eax
	movq	%rsi, %rbx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler17JSGenericLowering6ReduceEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L623:
	.long	.L705-.L623
	.long	.L704-.L623
	.long	.L703-.L623
	.long	.L702-.L623
	.long	.L701-.L623
	.long	.L700-.L623
	.long	.L699-.L623
	.long	.L698-.L623
	.long	.L697-.L623
	.long	.L696-.L623
	.long	.L695-.L623
	.long	.L694-.L623
	.long	.L693-.L623
	.long	.L692-.L623
	.long	.L691-.L623
	.long	.L690-.L623
	.long	.L689-.L623
	.long	.L688-.L623
	.long	.L687-.L623
	.long	.L686-.L623
	.long	.L685-.L623
	.long	.L684-.L623
	.long	.L683-.L623
	.long	.L682-.L623
	.long	.L681-.L623
	.long	.L680-.L623
	.long	.L679-.L623
	.long	.L678-.L623
	.long	.L677-.L623
	.long	.L676-.L623
	.long	.L675-.L623
	.long	.L674-.L623
	.long	.L673-.L623
	.long	.L672-.L623
	.long	.L671-.L623
	.long	.L670-.L623
	.long	.L669-.L623
	.long	.L668-.L623
	.long	.L625-.L623
	.long	.L625-.L623
	.long	.L625-.L623
	.long	.L667-.L623
	.long	.L625-.L623
	.long	.L666-.L623
	.long	.L625-.L623
	.long	.L665-.L623
	.long	.L625-.L623
	.long	.L625-.L623
	.long	.L664-.L623
	.long	.L663-.L623
	.long	.L662-.L623
	.long	.L661-.L623
	.long	.L625-.L623
	.long	.L625-.L623
	.long	.L660-.L623
	.long	.L659-.L623
	.long	.L658-.L623
	.long	.L657-.L623
	.long	.L656-.L623
	.long	.L655-.L623
	.long	.L654-.L623
	.long	.L653-.L623
	.long	.L652-.L623
	.long	.L651-.L623
	.long	.L650-.L623
	.long	.L649-.L623
	.long	.L648-.L623
	.long	.L625-.L623
	.long	.L625-.L623
	.long	.L647-.L623
	.long	.L625-.L623
	.long	.L625-.L623
	.long	.L646-.L623
	.long	.L645-.L623
	.long	.L644-.L623
	.long	.L643-.L623
	.long	.L642-.L623
	.long	.L641-.L623
	.long	.L640-.L623
	.long	.L639-.L623
	.long	.L638-.L623
	.long	.L637-.L623
	.long	.L636-.L623
	.long	.L635-.L623
	.long	.L634-.L623
	.long	.L633-.L623
	.long	.L625-.L623
	.long	.L625-.L623
	.long	.L632-.L623
	.long	.L625-.L623
	.long	.L625-.L623
	.long	.L625-.L623
	.long	.L625-.L623
	.long	.L625-.L623
	.long	.L625-.L623
	.long	.L625-.L623
	.long	.L625-.L623
	.long	.L625-.L623
	.long	.L631-.L623
	.long	.L630-.L623
	.long	.L629-.L623
	.long	.L628-.L623
	.long	.L627-.L623
	.long	.L626-.L623
	.long	.L625-.L623
	.long	.L624-.L623
	.long	.L622-.L623
	.section	.text._ZN2v88internal8compiler17JSGenericLowering6ReduceEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L625:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L668:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$393, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L712:
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
.L713:
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r13d, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
.L706:
	movq	%rbx, %rax
.L709:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L714
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L669:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering18LowerJSCreateArrayEPNS1_4NodeE
	jmp	.L706
.L670:
	call	_ZN2v88internal8compiler21CreateArgumentsTypeOfEPKNS1_8OperatorE@PLT
	movzbl	(%rax), %eax
	cmpb	$1, %al
	je	.L707
	cmpb	$2, %al
	je	.L708
	testb	%al, %al
	jne	.L706
	movl	$-1, %ecx
	movl	$312, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	jmp	.L706
.L671:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$31, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L672:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering18LowerJSCloneObjectEPNS1_4NodeE
	jmp	.L706
.L673:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$457, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L674:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$456, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L675:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$455, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L676:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$454, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L677:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$427, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L678:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$785, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L679:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$91, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L680:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$104, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L681:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$103, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L682:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$102, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L683:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$99, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L684:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$108, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L685:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$489, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L686:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$490, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L687:
	movl	$-1, %ecx
	movl	$226, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	jmp	.L706
.L688:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$439, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L689:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$438, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L690:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$437, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L691:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$436, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L692:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$435, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L693:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$434, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L694:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$445, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L695:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$444, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L696:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$443, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L697:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$440, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L698:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$442, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L699:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$441, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L700:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$449, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L701:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$447, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L702:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$448, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L703:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$446, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L704:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_@PLT
	movq	16(%r12), %rax
	leaq	-96(%rbp), %rdi
	movl	$453, %edx
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%rbx, %rdi
	movl	$4, %esi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	xorl	%ecx, %ecx
	movl	$112, %r8d
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS7_INS1_8Operator8PropertyEhEE
	jmp	.L706
.L705:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$450, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L622:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal11CodeFactory23HandleDebuggerStatementEPNS0_7IsolateE@PLT
	jmp	.L713
.L624:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$873, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L626:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering17LowerJSStackCheckEPNS1_4NodeE
	jmp	.L706
.L627:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$495, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L628:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$494, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L629:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$510, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L630:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$504, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L631:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$493, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L632:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering18LowerJSGetIteratorEPNS1_4NodeE
	jmp	.L706
.L633:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$491, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L634:
	call	_ZN2v88internal8compiler23CallRuntimeParametersOfEPKNS1_8OperatorE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	8(%rax), %ecx
	movl	(%rax), %edx
	call	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	jmp	.L706
.L635:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$228, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L636:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$227, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L637:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$226, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L638:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering26LowerJSConstructWithSpreadEPNS1_4NodeE
	jmp	.L706
.L639:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering29LowerJSConstructWithArrayLikeEPNS1_4NodeE
	jmp	.L706
.L640:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering16LowerJSConstructEPNS1_4NodeE
	jmp	.L706
.L641:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering30LowerJSConstructForwardVarargsEPNS1_4NodeE
	jmp	.L706
.L642:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering21LowerJSCallWithSpreadEPNS1_4NodeE
	jmp	.L706
.L643:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering24LowerJSCallWithArrayLikeEPNS1_4NodeE
	jmp	.L706
.L644:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering25LowerJSCallForwardVarargsEPNS1_4NodeE
	jmp	.L706
.L645:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering11LowerJSCallEPNS1_4NodeE
	jmp	.L706
.L646:
	call	_ZN2v88internal8compiler11ScopeInfoOfEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movl	$-1, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$314, %edx
	call	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	jmp	.L706
.L647:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering28LowerJSCreateFunctionContextEPNS1_4NodeE
	jmp	.L706
.L648:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$110, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L649:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$159, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L650:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$160, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L651:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering26LowerJSStoreInArrayLiteralEPNS1_4NodeE
	jmp	.L706
.L652:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering33LowerJSStoreDataPropertyInLiteralEPNS1_4NodeE
	jmp	.L706
.L653:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering18LowerJSStoreGlobalEPNS1_4NodeE
	jmp	.L706
.L654:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering20LowerJSStoreNamedOwnEPNS1_4NodeE
	jmp	.L706
.L655:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering17LowerJSStoreNamedEPNS1_4NodeE
	jmp	.L706
.L656:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering20LowerJSStorePropertyEPNS1_4NodeE
	jmp	.L706
.L657:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering17LowerJSLoadGlobalEPNS1_4NodeE
	jmp	.L706
.L658:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering16LowerJSLoadNamedEPNS1_4NodeE
	jmp	.L706
.L659:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering19LowerJSLoadPropertyEPNS1_4NodeE
	jmp	.L706
.L660:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$915, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L661:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$461, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	jmp	.L712
.L662:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateLiteralRegExpEPNS1_4NodeE
	jmp	.L706
.L663:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering26LowerJSCreateLiteralObjectEPNS1_4NodeE
	jmp	.L706
.L664:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering25LowerJSCreateLiteralArrayEPNS1_4NodeE
	jmp	.L706
.L665:
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movl	$351, %edx
	movzbl	%al, %r13d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node11RemoveInputEi@PLT
	jmp	.L713
.L666:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering30LowerJSCreateEmptyLiteralArrayEPNS1_4NodeE
	jmp	.L706
.L667:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering20LowerJSCreateClosureEPNS1_4NodeE
	jmp	.L706
.L621:
	xorl	%eax, %eax
	jmp	.L709
.L708:
	movl	$-1, %ecx
	movl	$309, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	jmp	.L706
.L707:
	movl	$-1, %ecx
	movl	$313, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSGenericLowering22ReplaceWithRuntimeCallEPNS1_4NodeENS0_7Runtime10FunctionIdEi
	jmp	.L706
.L714:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15914:
	.size	_ZN2v88internal8compiler17JSGenericLowering6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering6ReduceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler17JSGenericLowering15LowerJSDebuggerEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17JSGenericLowering15LowerJSDebuggerEPNS1_4NodeE
	.type	_ZN2v88internal8compiler17JSGenericLowering15LowerJSDebuggerEPNS1_4NodeE, @function
_ZN2v88internal8compiler17JSGenericLowering15LowerJSDebuggerEPNS1_4NodeE:
.LFB16034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	leaq	-96(%rbp), %rdi
	movzbl	%al, %r14d
	movq	16(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal11CodeFactory23HandleDebuggerStatementEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler17JSGenericLowering19ReplaceWithStubCallEPNS1_4NodeENS0_8CallableENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L718
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L718:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16034:
	.size	_ZN2v88internal8compiler17JSGenericLowering15LowerJSDebuggerEPNS1_4NodeE, .-_ZN2v88internal8compiler17JSGenericLowering15LowerJSDebuggerEPNS1_4NodeE
	.section	.text._ZNK2v88internal8compiler17JSGenericLowering4zoneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17JSGenericLowering4zoneEv
	.type	_ZNK2v88internal8compiler17JSGenericLowering4zoneEv, @function
_ZNK2v88internal8compiler17JSGenericLowering4zoneEv:
.LFB16035:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE16035:
	.size	_ZNK2v88internal8compiler17JSGenericLowering4zoneEv, .-_ZNK2v88internal8compiler17JSGenericLowering4zoneEv
	.section	.text._ZNK2v88internal8compiler17JSGenericLowering7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17JSGenericLowering7isolateEv
	.type	_ZNK2v88internal8compiler17JSGenericLowering7isolateEv, @function
_ZNK2v88internal8compiler17JSGenericLowering7isolateEv:
.LFB16036:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE16036:
	.size	_ZNK2v88internal8compiler17JSGenericLowering7isolateEv, .-_ZNK2v88internal8compiler17JSGenericLowering7isolateEv
	.section	.text._ZNK2v88internal8compiler17JSGenericLowering5graphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17JSGenericLowering5graphEv
	.type	_ZNK2v88internal8compiler17JSGenericLowering5graphEv, @function
_ZNK2v88internal8compiler17JSGenericLowering5graphEv:
.LFB16037:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE16037:
	.size	_ZNK2v88internal8compiler17JSGenericLowering5graphEv, .-_ZNK2v88internal8compiler17JSGenericLowering5graphEv
	.section	.text._ZNK2v88internal8compiler17JSGenericLowering6commonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17JSGenericLowering6commonEv
	.type	_ZNK2v88internal8compiler17JSGenericLowering6commonEv, @function
_ZNK2v88internal8compiler17JSGenericLowering6commonEv:
.LFB16038:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE16038:
	.size	_ZNK2v88internal8compiler17JSGenericLowering6commonEv, .-_ZNK2v88internal8compiler17JSGenericLowering6commonEv
	.section	.text._ZNK2v88internal8compiler17JSGenericLowering7machineEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17JSGenericLowering7machineEv
	.type	_ZNK2v88internal8compiler17JSGenericLowering7machineEv, @function
_ZNK2v88internal8compiler17JSGenericLowering7machineEv:
.LFB16039:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	16(%rax), %rax
	ret
	.cfi_endproc
.LFE16039:
	.size	_ZNK2v88internal8compiler17JSGenericLowering7machineEv, .-_ZNK2v88internal8compiler17JSGenericLowering7machineEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler17JSGenericLoweringC2EPNS1_7JSGraphEPNS1_15AdvancedReducer6EditorEPNS1_12JSHeapBrokerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler17JSGenericLoweringC2EPNS1_7JSGraphEPNS1_15AdvancedReducer6EditorEPNS1_12JSHeapBrokerE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler17JSGenericLoweringC2EPNS1_7JSGraphEPNS1_15AdvancedReducer6EditorEPNS1_12JSHeapBrokerE:
.LFB19219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE19219:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler17JSGenericLoweringC2EPNS1_7JSGraphEPNS1_15AdvancedReducer6EditorEPNS1_12JSHeapBrokerE, .-_GLOBAL__sub_I__ZN2v88internal8compiler17JSGenericLoweringC2EPNS1_7JSGraphEPNS1_15AdvancedReducer6EditorEPNS1_12JSHeapBrokerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler17JSGenericLoweringC2EPNS1_7JSGraphEPNS1_15AdvancedReducer6EditorEPNS1_12JSHeapBrokerE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal26ArrayConstructorDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal26ArrayConstructorDescriptorE,"awG",@progbits,_ZTVN2v88internal26ArrayConstructorDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal26ArrayConstructorDescriptorE, @object
	.size	_ZTVN2v88internal26ArrayConstructorDescriptorE, 48
_ZTVN2v88internal26ArrayConstructorDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal26ArrayConstructorDescriptorD1Ev
	.quad	_ZN2v88internal26ArrayConstructorDescriptorD0Ev
	.quad	_ZN2v88internal26ArrayConstructorDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal26ArrayConstructorDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.weak	_ZTVN2v88internal8compiler17JSGenericLoweringE
	.section	.data.rel.ro._ZTVN2v88internal8compiler17JSGenericLoweringE,"awG",@progbits,_ZTVN2v88internal8compiler17JSGenericLoweringE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler17JSGenericLoweringE, @object
	.size	_ZTVN2v88internal8compiler17JSGenericLoweringE, 56
_ZTVN2v88internal8compiler17JSGenericLoweringE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler17JSGenericLoweringD1Ev
	.quad	_ZN2v88internal8compiler17JSGenericLoweringD0Ev
	.quad	_ZNK2v88internal8compiler17JSGenericLowering12reducer_nameEv
	.quad	_ZN2v88internal8compiler17JSGenericLowering6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
