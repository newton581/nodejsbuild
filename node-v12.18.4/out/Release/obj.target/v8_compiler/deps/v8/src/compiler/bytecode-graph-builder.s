	.file	"bytecode-graph-builder.cc"
	.text
	.section	.rodata._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.0, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.0:
.LFB31504:
	.cfi_startproc
	movq	48(%rdi), %rax
	movq	56(%rdi), %rdx
	movslq	92(%rdi), %r8
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r8
	jnb	.L7
	movq	%rsi, (%rax,%r8,8)
	ret
.L7:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE31504:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.0, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.0
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_.constprop.0, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_.constprop.0:
.LFB31502:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$1, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$8, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%rax, %r14
	movl	404(%rbx), %eax
	cmpl	$1, %eax
	jle	.L9
	movq	408(%rbx), %r9
.L10:
	movl	$1, %ecx
	movq	%r9, %rdi
	movq	%r12, %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	%r13, 8(%r9)
	movq	16(%rbx), %rax
	movq	%r14, %rsi
	movq	%r9, %rcx
	movl	$1, %r8d
	movl	$2, %edx
	movq	(%rax), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	leal	66(%rax), %r15d
	movslq	%r15d, %rsi
	movq	16(%rdi), %r9
	movq	24(%rdi), %rax
	salq	$3, %rsi
	subq	%r9, %rax
	cmpq	%rax, %rsi
	ja	.L14
	addq	%r9, %rsi
	movq	%rsi, 16(%rdi)
.L12:
	movq	%r9, 408(%rbx)
	movl	%r15d, 404(%rbx)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L14:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r9
	jmp	.L12
	.cfi_endproc
.LFE31502:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_.constprop.0, .-_ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_.constprop.0
	.section	.rodata._ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC1:
	.string	"IsName()"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler7NameRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L18
.L15:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6IsNameEv@PLT
	testb	%al, %al
	jne	.L15
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9594:
	.size	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC3:
	.string	"IsScopeInfo()"
	.section	.text._ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler12ScopeInfoRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L22
.L19:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef11IsScopeInfoEv@PLT
	testb	%al, %al
	jne	.L19
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9693:
	.size	_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler12ScopeInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC4:
	.string	"IsSharedFunctionInfo()"
	.section	.text._ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler21SharedFunctionInfoRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L26
.L23:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef20IsSharedFunctionInfoEv@PLT
	testb	%al, %al
	jne	.L23
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9699:
	.size	_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler21SharedFunctionInfoRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler9StringRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC5:
	.string	"IsString()"
	.section	.text._ZN2v88internal8compiler9StringRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler9StringRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9StringRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler9StringRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler9StringRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler7NameRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	testb	%bl, %bl
	jne	.L30
.L27:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef8IsStringEv@PLT
	testb	%al, %al
	jne	.L27
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9705:
	.size	_ZN2v88internal8compiler9StringRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler9StringRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler9StringRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler9StringRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler9StringRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPKS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPKS3_
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPKS3_, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPKS3_:
.LFB24411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rsi), %rax
	movq	16(%rsi), %rdi
	movq	32(%rsi), %rdx
	movq	24(%rsi), %rcx
	movq	8(%rsi), %r8
	movq	%rax, (%rbx)
	movq	%rdi, 16(%rbx)
	movq	8(%rax), %rdi
	movq	$0, 64(%rbx)
	movq	%rdi, 40(%rbx)
	movups	%xmm0, 48(%rbx)
	movq	80(%rsi), %rax
	movq	%rdx, 32(%rbx)
	movq	72(%rsi), %rdx
	movq	%rax, 80(%rbx)
	movq	88(%rsi), %rax
	movq	%rdx, 72(%rbx)
	leaq	40(%rbx), %rdx
	movq	%rax, 88(%rbx)
	leaq	40(%rsi), %rax
	movq	%r8, 8(%rbx)
	movq	%rcx, 24(%rbx)
	cmpq	%rax, %rdx
	je	.L31
	movq	56(%rsi), %r13
	movq	48(%rsi), %r12
	movq	%r13, %r14
	subq	%r12, %r14
	jne	.L55
	xorl	%eax, %eax
	cmpq	%r13, %r12
	je	.L37
	movq	48(%rbx), %rax
.L37:
	movq	%rax, 56(%rbx)
.L31:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	leaq	7(%r14), %rsi
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L56
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L35:
	cmpq	%r13, %r12
	je	.L41
	subq	$8, %r13
	leaq	15(%r12), %rdx
	subq	%r12, %r13
	subq	%rax, %rdx
	movq	%r13, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L38
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L38
	addq	$1, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rsi, %rcx
	subq	%rax, %rdi
	shrq	%rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L39:
	movdqu	(%rdx,%rdi), %xmm1
	addq	$16, %rdx
	movups	%xmm1, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L39
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %r12
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L41
	movq	(%r12), %rdx
	movq	%rdx, (%rcx)
.L41:
	movq	%rax, 48(%rbx)
	addq	%r14, %rax
	movq	%rax, 64(%rbx)
	movq	%rax, 56(%rbx)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	8(%rax,%r13), %rsi
	movq	%rax, %rdx
	subq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rdx,%r12), %rcx
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rsi, %rdx
	jne	.L43
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L56:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L35
	.cfi_endproc
.LFE24411:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPKS3_, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPKS3_
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC1EPKS3_
	.set	_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC1EPKS3_,_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPKS3_
	.section	.text._ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment21RegisterToValuesIndexENS0_11interpreter8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment21RegisterToValuesIndexENS0_11interpreter8RegisterE
	.type	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment21RegisterToValuesIndexENS0_11interpreter8RegisterE, @function
_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment21RegisterToValuesIndexENS0_11interpreter8RegisterE:
.LFB24413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%esi, -4(%rbp)
	testl	%esi, %esi
	js	.L61
	movl	88(%rdi), %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movl	12(%rdi), %esi
	leaq	-4(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24413:
	.size	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment21RegisterToValuesIndexENS0_11interpreter8RegisterE, .-_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment21RegisterToValuesIndexENS0_11interpreter8RegisterE
	.section	.text._ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment17LookupAccumulatorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment17LookupAccumulatorEv
	.type	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment17LookupAccumulatorEv, @function
_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment17LookupAccumulatorEv:
.LFB24414:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movq	56(%rdi), %rdx
	movslq	92(%rdi), %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L67
	movq	(%rax,%rsi,8), %rax
	ret
.L67:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24414:
	.size	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment17LookupAccumulatorEv, .-_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment17LookupAccumulatorEv
	.section	.text._ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment20LookupGeneratorStateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment20LookupGeneratorStateEv
	.type	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment20LookupGeneratorStateEv, @function
_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment20LookupGeneratorStateEv:
.LFB24415:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	ret
	.cfi_endproc
.LFE24415:
	.size	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment20LookupGeneratorStateEv, .-_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment20LookupGeneratorStateEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment18BindGeneratorStateEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment18BindGeneratorStateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment18BindGeneratorStateEPNS1_4NodeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment18BindGeneratorStateEPNS1_4NodeE:
.LFB24418:
	.cfi_startproc
	endbr64
	movq	%rsi, 80(%rdi)
	ret
	.cfi_endproc
.LFE24418:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment18BindGeneratorStateEPNS1_4NodeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment18BindGeneratorStateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv:
.LFB24422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	8(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$95, %rax
	jbe	.L96
	leaq	96(%r12), %rax
	movq	%rax, 16(%rdi)
.L72:
	movq	(%rbx), %rax
	movq	32(%rbx), %rdx
	pxor	%xmm0, %xmm0
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rcx
	movq	%rax, (%r12)
	movq	%rdx, 32(%r12)
	movq	%rdi, 8(%r12)
	movq	%rsi, 16(%r12)
	movq	%rcx, 24(%r12)
	movq	8(%rax), %rdi
	movq	$0, 64(%r12)
	movq	%rdi, 40(%r12)
	movups	%xmm0, 48(%r12)
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdx
	movq	%rax, 80(%r12)
	movq	88(%rbx), %rax
	movq	%rdx, 72(%r12)
	leaq	40(%r12), %rdx
	movq	%rax, 88(%r12)
	leaq	40(%rbx), %rax
	cmpq	%rax, %rdx
	je	.L70
	movq	48(%rbx), %r13
	movq	56(%rbx), %rbx
	movq	%rbx, %r14
	subq	%r13, %r14
	jne	.L97
	xorl	%eax, %eax
	cmpq	%rbx, %r13
	je	.L78
	movq	48(%r12), %rax
.L78:
	movq	%rax, 56(%r12)
.L70:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	leaq	7(%r14), %rsi
	andq	$-8, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L98
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L76:
	cmpq	%rbx, %r13
	je	.L82
	subq	$8, %rbx
	leaq	15(%r13), %rdx
	subq	%r13, %rbx
	subq	%rax, %rdx
	movq	%rbx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L79
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L79
	addq	$1, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%rsi, %rcx
	subq	%rax, %rdi
	shrq	%rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L80:
	movdqu	(%rdx,%rdi), %xmm1
	addq	$16, %rdx
	movups	%xmm1, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L80
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %r13
	addq	%rax, %rcx
	cmpq	%rsi, %rdx
	je	.L82
	movq	0(%r13), %rdx
	movq	%rdx, (%rcx)
.L82:
	movq	%rax, 48(%r12)
	addq	%r14, %rax
	movq	%rax, 64(%r12)
	movq	%rax, 56(%r12)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$96, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L79:
	leaq	8(%rax,%rbx), %rsi
	movq	%rax, %rdx
	subq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L84:
	movq	(%rdx,%r13), %rcx
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rsi, %rdx
	jne	.L84
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L98:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L76
	.cfi_endproc
.LFE24422:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment17FillWithOsrValuesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment17FillWithOsrValuesEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment17FillWithOsrValuesEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment17FillWithOsrValuesEv:
.LFB24425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-64(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movq	(%rax), %r12
	movq	8(%rax), %rdi
	movq	8(%r12), %rbx
	movq	%rbx, -72(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder8OsrValueEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 16(%r15)
	movq	56(%r15), %rax
	subq	48(%r15), %rax
	sarq	$3, %rax
	testl	%eax, %eax
	jle	.L99
	subl	$1, %eax
	xorl	%r12d, %r12d
	movq	%rax, -80(%rbp)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%rax, %r12
.L104:
	movl	%r12d, %esi
	movl	%r12d, %eax
	cmpl	88(%r15), %r12d
	jl	.L101
	leal	2(%r12), %esi
.L101:
	cmpl	92(%r15), %eax
	movl	$-1, %eax
	cmovge	%eax, %esi
	movq	(%r15), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder8OsrValueEi@PLT
	movq	56(%r15), %rdx
	movq	%rax, %rsi
	movq	48(%r15), %rax
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r12
	jnb	.L109
	leaq	(%rax,%r12,8), %r14
	movq	-72(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r14)
	leaq	1(%r12), %rax
	cmpq	-80(%rbp), %r12
	jne	.L107
.L99:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L109:
	.cfi_restore_state
	movq	%r12, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24425:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment17FillWithOsrValuesEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment17FillWithOsrValuesEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment24StateValuesRequireUpdateEPPNS1_4NodeES6_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment24StateValuesRequireUpdateEPPNS1_4NodeES6_i
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment24StateValuesRequireUpdateEPPNS1_4NodeES6_i, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment24StateValuesRequireUpdateEPPNS1_4NodeES6_i:
.LFB24426:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L119
	movzbl	23(%rdi), %eax
	leaq	32(%rdi), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L114
	movq	32(%rdi), %rsi
	movl	8(%rsi), %eax
	addq	$16, %rsi
.L114:
	movl	$1, %r8d
	cmpl	%eax, %ecx
	jne	.L111
	testl	%ecx, %ecx
	jle	.L118
	leal	-1(%rcx), %edi
	xorl	%eax, %eax
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L121:
	leaq	1(%rax), %rcx
	cmpq	%rax, %rdi
	je	.L118
	movq	%rcx, %rax
.L115:
	movq	(%rsi,%rax,8), %rcx
	cmpq	%rcx, (%rdx,%rax,8)
	je	.L121
.L119:
	movl	$1, %r8d
.L111:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE24426:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment24StateValuesRequireUpdateEPPNS1_4NodeES6_i, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment24StateValuesRequireUpdateEPPNS1_4NodeES6_i
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment18PrepareForLoopExitEPNS1_4NodeERKNS1_23BytecodeLoopAssignmentsEPKNS1_21BytecodeLivenessStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment18PrepareForLoopExitEPNS1_4NodeERKNS1_23BytecodeLoopAssignmentsEPKNS1_21BytecodeLivenessStateE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment18PrepareForLoopExitEPNS1_4NodeERKNS1_23BytecodeLoopAssignmentsEPKNS1_21BytecodeLivenessStateE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment18PrepareForLoopExitEPNS1_4NodeERKNS1_23BytecodeLoopAssignmentsEPKNS1_21BytecodeLivenessStateE:
.LFB24427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rsi, -96(%rbp)
	movq	24(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder8LoopExitEv@PLT
	movq	%r15, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	leaq	-80(%rbp), %r15
	movhps	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	32(%rbx), %rcx
	movq	%rax, 24(%rbx)
	movq	%rax, -96(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -104(%rbp)
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder14LoopExitEffectEv@PLT
	movl	$2, %edx
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rcx
	xorl	%r14d, %r14d
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	12(%rbx), %edx
	movq	%rax, 32(%rbx)
	testl	%edx, %edx
	jle	.L129
	.p2align 4,,10
	.p2align 3
.L123:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler23BytecodeLoopAssignments17ContainsParameterEi@PLT
	testb	%al, %al
	jne	.L165
	addq	$1, %r14
	cmpl	%r14d, 12(%rbx)
	jg	.L123
.L129:
	movl	8(%rbx), %eax
	xorl	%r14d, %r14d
	testl	%eax, %eax
	jle	.L134
	.p2align 4,,10
	.p2align 3
.L124:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler23BytecodeLoopAssignments13ContainsLocalEi@PLT
	testb	%al, %al
	je	.L130
	testq	%r13, %r13
	je	.L131
	cmpl	$1, 12(%r13)
	movq	16(%r13), %rax
	je	.L133
	movl	%r14d, %edx
	sarl	$6, %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
.L133:
	btq	%r14, %rax
	jnc	.L130
.L131:
	movq	(%rbx), %rax
	movq	48(%rbx), %rcx
	movq	16(%rax), %rdx
	movl	88(%rbx), %eax
	addl	%r14d, %eax
	movq	(%rdx), %r10
	movq	8(%rdx), %rdi
	cltq
	movq	(%rcx,%rax,8), %rax
	movq	%r10, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13LoopExitValueEv@PLT
	movq	-112(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-104(%rbp), %xmm0
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	48(%rbx), %rdx
	movq	%rax, %r8
	movl	88(%rbx), %eax
	addl	%r14d, %eax
	cltq
	movq	%r8, (%rdx,%rax,8)
.L130:
	addl	$1, %r14d
	cmpl	8(%rbx), %r14d
	jl	.L124
.L134:
	testq	%r13, %r13
	je	.L125
	movl	8(%r13), %edx
	cmpl	$1, 12(%r13)
	movq	16(%r13), %rcx
	leal	-1(%rdx), %eax
	je	.L136
	addl	$62, %edx
	testl	%eax, %eax
	cmovns	%eax, %edx
	sarl	$6, %edx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
.L136:
	cltd
	shrl	$26, %edx
	addl	%edx, %eax
	andl	$63, %eax
	subl	%edx, %eax
	btq	%rax, %rcx
	jc	.L125
.L137:
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.L122
	movq	%rax, %xmm0
	movq	(%rbx), %rax
	movhps	-96(%rbp), %xmm0
	movq	16(%rax), %rax
	movaps	%xmm0, -96(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13LoopExitValueEv@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 80(%rbx)
.L122:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L166
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	(%rbx), %rax
	movslq	92(%rbx), %rcx
	movq	48(%rbx), %rdx
	movq	16(%rax), %rax
	movq	(%rdx,%rcx,8), %r12
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13LoopExitValueEv@PLT
	movq	%r12, %xmm0
	movq	%r15, %rcx
	xorl	%r8d, %r8d
	movhps	-96(%rbp), %xmm0
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movslq	92(%rbx), %rdx
	movq	48(%rbx), %rcx
	movq	%rax, (%rcx,%rdx,8)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L165:
	movq	(%rbx), %rax
	movq	48(%rbx), %rdx
	movq	16(%rax), %rax
	movq	(%rdx,%r14,8), %rcx
	movq	(%rax), %r10
	movq	8(%rax), %rdi
	movq	%rcx, -104(%rbp)
	movq	%r10, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13LoopExitValueEv@PLT
	movq	-112(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-104(%rbp), %xmm0
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	48(%rbx), %rdx
	movq	%rax, (%rdx,%r14,8)
	addq	$1, %r14
	cmpl	%r14d, 12(%rbx)
	jg	.L123
	jmp	.L129
.L166:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24427:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment18PrepareForLoopExitEPNS1_4NodeERKNS1_23BytecodeLoopAssignmentsEPKNS1_21BytecodeLivenessStateE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment18PrepareForLoopExitEPNS1_4NodeERKNS1_23BytecodeLoopAssignmentsEPKNS1_21BytecodeLivenessStateE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment17UpdateStateValuesEPPNS1_4NodeES6_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment17UpdateStateValuesEPPNS1_4NodeES6_i
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment17UpdateStateValuesEPPNS1_4NodeES6_i, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment17UpdateStateValuesEPPNS1_4NodeES6_i:
.LFB24428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rdx
	movq	%rsi, %rbx
	testq	%rdx, %rdx
	je	.L168
	movzbl	23(%rdx), %eax
	leaq	32(%rdx), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L170
	movq	32(%rdx), %rcx
	movl	8(%rcx), %eax
	addq	$16, %rcx
.L170:
	cmpl	%eax, %r12d
	jne	.L168
	testl	%r12d, %r12d
	jle	.L167
	leal	-1(%r12), %esi
	xorl	%eax, %eax
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L177:
	leaq	1(%rax), %rdx
	cmpq	%rax, %rsi
	je	.L167
	movq	%rdx, %rax
.L172:
	movq	0(%r13,%rax,8), %rdi
	cmpq	%rdi, (%rcx,%rax,8)
	je	.L177
.L168:
	movq	(%r14), %rax
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11StateValuesEiNS1_15SparseInputMaskE@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	%r12d, %edx
	movq	%rax, %rsi
	movq	(%r14), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%rbx)
.L167:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24428:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment17UpdateStateValuesEPPNS1_4NodeES6_i, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment17UpdateStateValuesEPPNS1_4NodeES6_i
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment23GetStateValuesFromCacheEPPNS1_4NodeEiPKNS0_9BitVectorEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment23GetStateValuesFromCacheEPPNS1_4NodeEiPKNS0_9BitVectorEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment23GetStateValuesFromCacheEPPNS1_4NodeEiPKNS0_9BitVectorEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment23GetStateValuesFromCacheEPPNS1_4NodeEiPKNS0_9BitVectorEi:
.LFB24429:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movslq	%edx, %rdx
	addq	$464, %rdi
	jmp	_ZN2v88internal8compiler16StateValuesCache16GetNodeForValuesEPPNS1_4NodeEmPKNS0_9BitVectorEi@PLT
	.cfi_endproc
.LFE24429:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment23GetStateValuesFromCacheEPPNS1_4NodeEiPKNS0_9BitVectorEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment23GetStateValuesFromCacheEPPNS1_4NodeEiPKNS0_9BitVectorEi
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder20CreateFeedbackSourceEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder20CreateFeedbackSourceEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder20CreateFeedbackSourceEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder20CreateFeedbackSourceEi:
.LFB24480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	64(%rdi), %rdx
	movq	56(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rdi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	-32(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L182
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L182:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24480:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder20CreateFeedbackSourceEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder20CreateFeedbackSourceEi
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18AdvanceIteratorsToEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18AdvanceIteratorsToEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18AdvanceIteratorsToEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18AdvanceIteratorsToEi:
.LFB24487:
	.cfi_startproc
	endbr64
	movl	144(%rdi), %eax
	cmpl	%eax, %esi
	je	.L191
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	136(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv@PLT
	movl	144(%rbx), %eax
	cmpl	%eax, %r12d
	je	.L194
.L186:
	movq	128(%rbx), %rdx
	cmpl	$-1, 24(%rdx)
	je	.L185
	cmpl	%eax, 32(%rdx)
	jne	.L185
	movq	40(%rdx), %rax
	movq	536(%rbx), %rcx
	movabsq	$140735340871680, %rdx
	andq	544(%rbx), %rdx
	andl	$2147483646, %eax
	orq	%rdx, %rax
	movq	%rax, 16(%rcx)
	movq	128(%rbx), %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L194:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE24487:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18AdvanceIteratorsToEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18AdvanceIteratorsToEi
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder35RemoveMergeEnvironmentsBeforeOffsetEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder35RemoveMergeEnvironmentsBeforeOffsetEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder35RemoveMergeEnvironmentsBeforeOffsetEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder35RemoveMergeEnvironmentsBeforeOffsetEi:
.LFB24508:
	.cfi_startproc
	endbr64
	cmpq	$0, 240(%rdi)
	je	.L200
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	208(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	224(%rdi), %r12
	cmpq	%r13, %r12
	jne	.L197
.L195:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	subq	$1, 240(%r15)
	cmpq	%rbx, %r13
	je	.L195
	movq	%rbx, %r12
.L197:
	cmpl	32(%r12), %r14d
	jge	.L204
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE24508:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder35RemoveMergeEnvironmentsBeforeOffsetEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder35RemoveMergeEnvironmentsBeforeOffsetEi
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaZeroEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaZeroEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaZeroEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaZeroEv:
.LFB24515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L208
	movq	%rax, (%rcx,%rsi,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L208:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24515:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaZeroEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaZeroEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11VisitLdaSmiEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitLdaSmiEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitLdaSmiEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitLdaSmiEv:
.LFB24516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %r12
	addq	$136, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetImmediateOperandEi@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L212
	movq	%rax, (%rcx,%rsi,8)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L212:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24516:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitLdaSmiEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitLdaSmiEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16VisitLdaConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitLdaConstantEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitLdaConstantEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitLdaConstantEv:
.LFB24517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$136, %rdi
	leaq	-48(%rbp), %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-120(%rdi), %rax
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L217
	movq	%rax, (%rcx,%rsi,8)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L218
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L217:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L218:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24517:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitLdaConstantEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitLdaConstantEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder17VisitLdaUndefinedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitLdaUndefinedEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitLdaUndefinedEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitLdaUndefinedEv:
.LFB24518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L222
	movq	%rax, (%rcx,%rsi,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L222:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24518:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitLdaUndefinedEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitLdaUndefinedEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaNullEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaNullEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaNullEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaNullEv:
.LFB24519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZN2v88internal8compiler7JSGraph12NullConstantEv@PLT
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L226
	movq	%rax, (%rcx,%rsi,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L226:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24519:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaNullEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaNullEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder15VisitLdaTheHoleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitLdaTheHoleEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitLdaTheHoleEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitLdaTheHoleEv:
.LFB24520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L230
	movq	%rax, (%rcx,%rsi,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L230:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24520:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitLdaTheHoleEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitLdaTheHoleEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaTrueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaTrueEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaTrueEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaTrueEv:
.LFB24521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L234
	movq	%rax, (%rcx,%rsi,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L234:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24521:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaTrueEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaTrueEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder13VisitLdaFalseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitLdaFalseEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitLdaFalseEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitLdaFalseEv:
.LFB24522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L238
	movq	%rax, (%rcx,%rsi,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L238:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24522:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitLdaFalseEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitLdaFalseEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder9VisitStarEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitStarEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitStarEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitStarEv:
.LFB24524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	168(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	56(%r12), %rdx
	movq	48(%r12), %rax
	movslq	92(%r12), %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L246
	movq	(%rax,%rsi,8), %rbx
	addq	$136, %rdi
	xorl	%esi, %esi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, -28(%rbp)
	testl	%eax, %eax
	js	.L247
	addl	88(%r12), %eax
.L242:
	movq	48(%r12), %rcx
	movq	56(%r12), %rdx
	cltq
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L248
	movq	%rbx, (%rcx,%rax,8)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L249
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	movl	12(%r12), %esi
	leaq	-28(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	jmp	.L242
.L248:
	movq	%rax, %rsi
.L246:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L249:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24524:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitStarEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitStarEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16VisitPushContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitPushContextEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitPushContextEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitPushContextEv:
.LFB24560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	168(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	56(%r13), %rdx
	movq	48(%r13), %rax
	movslq	92(%r13), %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L257
	movq	(%rax,%rsi,8), %rbx
	movq	%rdi, %r14
	xorl	%esi, %esi
	leaq	136(%rdi), %rdi
	movq	16(%r13), %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, -44(%rbp)
	testl	%eax, %eax
	js	.L258
	addl	88(%r13), %eax
.L253:
	movq	48(%r13), %rcx
	movq	56(%r13), %rdx
	cltq
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L259
	movq	%r12, (%rcx,%rax,8)
	movq	168(%r14), %rax
	movq	%rbx, 16(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L260
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movl	12(%r13), %esi
	leaq	-44(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	jmp	.L253
.L259:
	movq	%rax, %rsi
.L257:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24560:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitPushContextEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitPushContextEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder22VisitGetTemplateObjectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitGetTemplateObjectEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitGetTemplateObjectEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitGetTemplateObjectEv:
.LFB24585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	136(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	%eax, %r12d
	movq	16(%rbx), %rax
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	leaq	-96(%rbp), %rdi
	movl	$1, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	subq	$8, %rsp
	movq	56(%rbx), %rcx
	movl	%r12d, %r9d
	movdqu	40(%rbx), %xmm0
	movq	64(%rbx), %r8
	pushq	$0
	leaq	-64(%rbp), %rdi
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdx
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler21SharedFunctionInfoRef17GetTemplateObjectENS1_9ObjectRefENS1_17FeedbackVectorRefENS0_12FeedbackSlotENS1_19SerializationPolicyE@PLT
	movq	168(%rbx), %r12
	movq	16(%rbx), %rdi
	leaq	-80(%rbp), %rsi
	movq	%rdx, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	48(%r12), %rcx
	movq	56(%r12), %rdx
	movslq	92(%r12), %rsi
	popq	%rdi
	subq	%rcx, %rdx
	popq	%r8
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L265
	movq	%rax, (%rcx,%rsi,8)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L266
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L265:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L266:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24585:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitGetTemplateObjectEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitGetTemplateObjectEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder22GetBinaryOperationHintEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder22GetBinaryOperationHintEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder22GetBinaryOperationHintEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder22GetBinaryOperationHintEi:
.LFB24621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$136, %rdi
	leaq	-48(%rbp), %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	64(%rbx), %rdx
	movq	56(%rbx), %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker29GetFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L270
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L270:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24621:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder22GetBinaryOperationHintEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder22GetBinaryOperationHintEi
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder23GetCompareOperationHintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder23GetCompareOperationHintEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder23GetCompareOperationHintEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder23GetCompareOperationHintEv:
.LFB24622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$136, %rdi
	leaq	-48(%rbp), %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	64(%rbx), %rdx
	movq	56(%rbx), %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker30GetFeedbackForCompareOperationERKNS1_14FeedbackSourceE@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L274
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L274:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24622:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder23GetCompareOperationHintEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder23GetCompareOperationHintEv
	.section	.rodata._ZN2v88internal8compiler20BytecodeGraphBuilder12GetForInModeEi.str1.1,"aMS",@progbits,1
.LC6:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder12GetForInModeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder12GetForInModeEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder12GetForInModeEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder12GetForInModeEi:
.LFB24623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$136, %rdi
	leaq	-48(%rbp), %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	56(%rbx), %rsi
	movq	64(%rbx), %rdx
	movq	%r12, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker19GetFeedbackForForInERKNS1_14FeedbackSourceE@PLT
	cmpb	$3, %al
	ja	.L276
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	movzbl	%al, %eax
	leaq	CSWTCH.612(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	jne	.L280
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L276:
	.cfi_restore_state
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L280:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24623:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder12GetForInModeEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder12GetForInModeEi
	.section	.text._ZNK2v88internal8compiler20BytecodeGraphBuilder20ComputeCallFrequencyEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler20BytecodeGraphBuilder20ComputeCallFrequencyEi
	.type	_ZNK2v88internal8compiler20BytecodeGraphBuilder20ComputeCallFrequencyEi, @function
_ZNK2v88internal8compiler20BytecodeGraphBuilder20ComputeCallFrequencyEi:
.LFB24624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movss	72(%rdi), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	ucomiss	%xmm0, %xmm0
	movss	.LC7(%rip), %xmm0
	jp	.L283
	movq	64(%rdi), %rdx
	movl	%esi, %ecx
	leaq	-48(%rbp), %r12
	movq	56(%rdi), %rsi
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker18GetFeedbackForCallERKNS1_14FeedbackSourceE@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %rdi
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L290
.L283:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L291
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	call	_ZNK2v88internal8compiler17ProcessedFeedback6AsCallEv@PLT
	pxor	%xmm0, %xmm0
	movss	32(%rax), %xmm1
	ucomiss	%xmm0, %xmm1
	jnp	.L292
.L288:
	mulss	72(%rbx), %xmm1
	movaps	%xmm1, %xmm0
	jmp	.L283
.L292:
	jne	.L288
	jmp	.L283
.L291:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24624:
	.size	_ZNK2v88internal8compiler20BytecodeGraphBuilder20ComputeCallFrequencyEi, .-_ZNK2v88internal8compiler20BytecodeGraphBuilder20ComputeCallFrequencyEi
	.section	.text._ZNK2v88internal8compiler20BytecodeGraphBuilder18GetSpeculationModeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler20BytecodeGraphBuilder18GetSpeculationModeEi
	.type	_ZNK2v88internal8compiler20BytecodeGraphBuilder18GetSpeculationModeEi, @function
_ZNK2v88internal8compiler20BytecodeGraphBuilder18GetSpeculationModeEi:
.LFB24625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-48(%rbp), %r12
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	56(%rdi), %rsi
	movq	64(%rdi), %rdx
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker18GetFeedbackForCallERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %rdi
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L299
	movl	$1, %eax
.L293:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L300
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	call	_ZNK2v88internal8compiler17ProcessedFeedback6AsCallEv@PLT
	movl	36(%rax), %eax
	jmp	.L293
.L300:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24625:
	.size	_ZNK2v88internal8compiler20BytecodeGraphBuilder18GetSpeculationModeEi, .-_ZNK2v88internal8compiler20BytecodeGraphBuilder18GetSpeculationModeEi
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak0Ev
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak0Ev, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak0Ev:
.LFB31466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE31466:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak0Ev, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak0Ev
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak1Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak1Ev
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak1Ev, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak1Ev:
.LFB31468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE31468:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak1Ev, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak1Ev
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak2Ev
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak2Ev, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak2Ev:
.LFB31470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE31470:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak2Ev, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak2Ev
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak3Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak3Ev
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak3Ev, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak3Ev:
.LFB31472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE31472:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak3Ev, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak3Ev
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak4Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak4Ev
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak4Ev, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak4Ev:
.LFB31474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE31474:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak4Ev, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak4Ev
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak5Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak5Ev
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak5Ev, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak5Ev:
.LFB31476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE31476:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak5Ev, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak5Ev
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak6Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak6Ev
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak6Ev, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak6Ev:
.LFB31478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE31478:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak6Ev, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitDebugBreak6Ev
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder19VisitDebugBreakWideEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitDebugBreakWideEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitDebugBreakWideEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitDebugBreakWideEv:
.LFB31462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE31462:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitDebugBreakWideEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitDebugBreakWideEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder24VisitDebugBreakExtraWideEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitDebugBreakExtraWideEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitDebugBreakExtraWideEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitDebugBreakExtraWideEv:
.LFB31464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE31464:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitDebugBreakExtraWideEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitDebugBreakExtraWideEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder9VisitWideEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitWideEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitWideEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitWideEv:
.LFB24732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24732:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitWideEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitWideEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder14VisitExtraWideEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitExtraWideEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitExtraWideEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitExtraWideEv:
.LFB31460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE31460:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitExtraWideEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitExtraWideEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder12VisitIllegalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitIllegalEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitIllegalEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitIllegalEv:
.LFB31480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE31480:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitIllegalEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitIllegalEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder21EnsureInputBufferSizeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder21EnsureInputBufferSizeEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder21EnsureInputBufferSizeEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder21EnsureInputBufferSizeEi:
.LFB24765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	404(%rdi), %eax
	movq	%rdi, %rbx
	cmpl	%esi, %eax
	jl	.L326
	popq	%rbx
	movq	408(%rdi), %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	leal	64(%rax,%rsi), %r12d
	movslq	%r12d, %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L331
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L329:
	movl	%r12d, 404(%rbx)
	movq	%rax, 408(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L329
	.cfi_endproc
.LFE24765:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder21EnsureInputBufferSizeEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder21EnsureInputBufferSizeEi
	.section	.rodata._ZN2v88internal8compiler20BytecodeGraphBuilder30ExitThenEnterExceptionHandlersEi.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder30ExitThenEnterExceptionHandlersEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder30ExitThenEnterExceptionHandlersEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder30ExitThenEnterExceptionHandlersEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder30ExitThenEnterExceptionHandlersEi:
.LFB24766:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movdqu	40(%rdi), %xmm4
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm4, -80(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	leaq	-96(%rbp), %rdi
	movq	%rdx, -120(%rbp)
	movq	%rax, -128(%rbp)
	movdqa	-128(%rbp), %xmm5
	movaps	%xmm5, -96(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeArrayRef18handler_table_sizeEv@PLT
	movdqu	40(%r15), %xmm6
	movq	%r12, %rdi
	movl	%eax, %r14d
	movaps	%xmm6, -80(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	movq	%r12, %rdi
	leaq	-112(%rbp), %r12
	movq	%rdx, -120(%rbp)
	movq	%rax, -128(%rbp)
	movdqa	-128(%rbp), %xmm7
	movaps	%xmm7, -80(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeArrayRef21handler_table_addressEv@PLT
	xorl	%ecx, %ecx
	movl	%r14d, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal12HandlerTableC1EmiNS1_12EncodingModeE@PLT
	movq	368(%r15), %rax
	movq	336(%r15), %rcx
	cmpq	%rcx, %rax
	je	.L333
	movq	376(%r15), %rdx
	movq	392(%r15), %rsi
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L335:
	cmpl	-12(%rax), %r13d
	jl	.L333
	subq	$16, %rax
	movq	%rax, 368(%r15)
	cmpq	%rcx, %rax
	je	.L333
.L334:
	cmpq	%rax, %rdx
	jne	.L335
	movq	-8(%rsi), %rdx
	cmpl	500(%rdx), %r13d
	jl	.L333
	movq	312(%r15), %rdi
	testq	%rdi, %rdi
	je	.L338
	cmpq	$32, 8(%rdi)
	movq	%rsi, %rdx
	ja	.L339
.L338:
	movq	$32, 8(%rax)
	movq	312(%r15), %rdx
	movq	%rdx, (%rax)
	movq	392(%r15), %rdx
	movq	%rax, 312(%r15)
	movq	336(%r15), %rcx
.L339:
	leaq	-8(%rdx), %rsi
	movq	%rsi, 392(%r15)
	movq	-8(%rdx), %rdx
	leaq	512(%rdx), %rax
	movq	%rdx, 376(%r15)
	movq	%rax, 384(%r15)
	leaq	496(%rdx), %rax
	movq	%rax, 368(%r15)
	cmpq	%rcx, %rax
	jne	.L334
	.p2align 4,,10
	.p2align 3
.L333:
	movq	%r12, %rdi
	call	_ZNK2v88internal12HandlerTable20NumberOfRangeEntriesEv@PLT
	movl	400(%r15), %esi
	movl	%eax, %r14d
	cmpl	%esi, %eax
	jg	.L340
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L370:
	movd	-132(%rbp), %xmm1
	movd	-128(%rbp), %xmm2
	punpckldq	%xmm0, %xmm1
	movd	%ebx, %xmm0
	punpckldq	%xmm2, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	addq	$16, 368(%r15)
.L343:
	movl	400(%r15), %eax
	leal	1(%rax), %esi
	movl	%esi, 400(%r15)
	cmpl	%r14d, %esi
	jge	.L332
.L340:
	movq	%r12, %rdi
	call	_ZNK2v88internal12HandlerTable13GetRangeStartEi@PLT
	movl	%eax, %ebx
	cmpl	%eax, %r13d
	jl	.L332
	movl	400(%r15), %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal12HandlerTable11GetRangeEndEi@PLT
	movl	400(%r15), %esi
	movq	%r12, %rdi
	movl	%eax, -128(%rbp)
	call	_ZNK2v88internal12HandlerTable15GetRangeHandlerEi@PLT
	movl	400(%r15), %esi
	movq	%r12, %rdi
	movl	%eax, -132(%rbp)
	call	_ZNK2v88internal12HandlerTable12GetRangeDataEi@PLT
	movq	384(%r15), %rcx
	movd	%eax, %xmm0
	movq	368(%r15), %rax
	leaq	-16(%rcx), %rdx
	cmpq	%rdx, %rax
	jne	.L370
	movq	392(%r15), %rdx
	movq	360(%r15), %rsi
	subq	376(%r15), %rax
	movq	%rdx, %rcx
	sarq	$4, %rax
	subq	%rsi, %rcx
	movq	%rcx, %r9
	sarq	$3, %r9
	leaq	-1(%r9), %rdi
	salq	$5, %rdi
	addq	%rax, %rdi
	movq	352(%r15), %rax
	subq	336(%r15), %rax
	sarq	$4, %rax
	addq	%rdi, %rax
	cmpq	$134217727, %rax
	je	.L371
	movq	320(%r15), %r10
	movq	328(%r15), %rdi
	movq	%rdx, %rax
	subq	%r10, %rax
	movq	%rdi, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L372
.L345:
	movq	312(%r15), %rax
	testq	%rax, %rax
	je	.L353
	cmpq	$31, 8(%rax)
	ja	.L373
.L353:
	movq	304(%r15), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	$511, %rcx
	jbe	.L374
	leaq	512(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L354:
	movd	-132(%rbp), %xmm1
	movd	-128(%rbp), %xmm3
	movq	%rax, 8(%rdx)
	movq	368(%r15), %rax
	punpckldq	%xmm0, %xmm1
	movd	%ebx, %xmm0
	punpckldq	%xmm3, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	movq	392(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 392(%r15)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 376(%r15)
	movq	%rdx, 384(%r15)
	movq	%rax, 368(%r15)
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L332:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L375
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	addq	$2, %r9
	leaq	(%r9,%r9), %rax
	cmpq	%rax, %rdi
	jbe	.L346
	subq	%r9, %rdi
	addq	$8, %rdx
	shrq	%rdi
	leaq	(%r10,%rdi,8), %r9
	movq	%rdx, %r10
	subq	%rsi, %r10
	cmpq	%r9, %rsi
	jbe	.L347
	cmpq	%rdx, %rsi
	je	.L348
	movq	%r9, %rdi
	movq	%r10, %rdx
	movq	%rcx, -152(%rbp)
	movd	%xmm0, -144(%rbp)
	call	memmove@PLT
	movd	-144(%rbp), %xmm0
	movq	-152(%rbp), %rcx
	movq	%rax, %r9
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L373:
	movq	(%rax), %rcx
	movq	%rcx, 312(%r15)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L346:
	testq	%rdi, %rdi
	movl	$1, %eax
	cmovne	%rdi, %rax
	leaq	2(%rdi,%rax), %r11
	movq	304(%r15), %rdi
	leaq	0(,%r11,8), %rsi
	movq	16(%rdi), %r10
	movq	24(%rdi), %rax
	subq	%r10, %rax
	cmpq	%rax, %rsi
	ja	.L376
	addq	%r10, %rsi
	movq	%rsi, 16(%rdi)
.L350:
	movq	%r11, %rax
	movq	360(%r15), %rsi
	subq	%r9, %rax
	shrq	%rax
	leaq	(%r10,%rax,8), %r9
	movq	392(%r15), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L351
	movq	%r9, %rdi
	subq	%rsi, %rdx
	movq	%r10, -168(%rbp)
	movq	%r11, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movd	%xmm0, -144(%rbp)
	call	memmove@PLT
	movq	-168(%rbp), %r10
	movq	-160(%rbp), %r11
	movq	-152(%rbp), %rcx
	movd	-144(%rbp), %xmm0
	movq	%rax, %r9
.L351:
	movq	328(%r15), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L352
	movq	320(%r15), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L352:
	movq	%r10, 320(%r15)
	movq	%r11, 328(%r15)
.L348:
	movq	%r9, 360(%r15)
	movq	(%r9), %rax
	leaq	(%r9,%rcx), %rdx
	movq	(%r9), %xmm1
	movq	%rdx, 392(%r15)
	addq	$512, %rax
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm1
	movups	%xmm1, 344(%r15)
	movq	(%rdx), %rax
	movq	%rax, 376(%r15)
	addq	$512, %rax
	movq	%rax, 384(%r15)
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L347:
	cmpq	%rdx, %rsi
	je	.L348
	movq	%r9, %rax
	movq	%r10, %rdx
	movq	%r9, -152(%rbp)
	subq	%r10, %rax
	movq	%rcx, -144(%rbp)
	leaq	8(%rax,%rcx), %rdi
	movd	%xmm0, -160(%rbp)
	call	memmove@PLT
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %r9
	movd	-160(%rbp), %xmm0
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L374:
	movl	$512, %esi
	movq	%rdx, -152(%rbp)
	movd	%xmm0, -144(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movd	-144(%rbp), %xmm0
	movq	-152(%rbp), %rdx
	jmp	.L354
.L376:
	movq	%r11, -168(%rbp)
	movq	%r9, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movd	%xmm0, -144(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movd	-144(%rbp), %xmm0
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %r9
	movq	-168(%rbp), %r11
	movq	%rax, %r10
	jmp	.L350
.L371:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L375:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24766:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder30ExitThenEnterExceptionHandlersEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder30ExitThenEnterExceptionHandlersEi
	.section	.rodata._ZN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState17ProcessOsrPreludeEv.str1.1,"aMS",@progbits,1
.LC10:
	.string	"0 <= osr_entry_point_"
.LC11:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState17ProcessOsrPreludeEv,"axG",@progbits,_ZN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState17ProcessOsrPreludeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState17ProcessOsrPreludeEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState17ProcessOsrPreludeEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState17ProcessOsrPreludeEv:
.LFB24500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	160(%rax), %rdi
	movq	8(%rax), %r12
	movl	296(%rdi), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	js	.L446
	movl	%eax, %r9d
	xorl	%eax, %eax
	movq	%r12, -56(%rbp)
	xorl	%ebx, %ebx
	movabsq	$4611686018427387900, %r13
	xorl	%r15d, %r15d
	movq	%rax, %r12
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L447:
	movl	%r9d, (%r15)
	addq	$4, %r15
.L381:
	movq	(%r14), %rax
	movq	160(%rax), %rdi
.L378:
	movl	%r9d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis14GetLoopInfoForEi@PLT
	movl	(%rax), %r9d
	cmpl	$-1, %r9d
	je	.L379
	cmpq	%r15, %rbx
	jne	.L447
	subq	%r12, %rbx
	movq	%rbx, %rax
	movq	%rbx, %rdx
	sarq	$2, %rax
	cmpq	$536870911, %rax
	je	.L448
	testq	%rax, %rax
	je	.L422
	leaq	(%rax,%rax), %rdi
	movl	$2147483648, %esi
	movl	$2147483644, %ebx
	cmpq	%rdi, %rax
	jbe	.L449
.L383:
	movq	-56(%rbp), %rcx
	movq	16(%rcx), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -72(%rbp)
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L450
	movq	-56(%rbp), %rcx
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx)
.L386:
	movl	%r9d, (%rax,%rdx)
	addq	%rax, %rbx
	leaq	4(%rax), %rsi
	cmpq	%r15, %r12
	je	.L425
.L452:
	leaq	-4(%r15), %r8
	leaq	15(%rax), %rdx
	subq	%r12, %r8
	subq	%r12, %rdx
	movq	%r8, %rsi
	shrq	$2, %rsi
	cmpq	$30, %rdx
	jbe	.L426
	testq	%r13, %rsi
	je	.L426
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	$2, %rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L389:
	movdqu	(%r12,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L389
	movq	%rsi, %rdi
	andq	$-4, %rdi
	leaq	0(,%rdi,4), %rdx
	leaq	(%r12,%rdx), %rcx
	addq	%rax, %rdx
	cmpq	%rsi, %rdi
	je	.L391
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	leaq	4(%rcx), %rsi
	cmpq	%rsi, %r15
	je	.L391
	movl	4(%rcx), %esi
	movl	%esi, 4(%rdx)
	leaq	8(%rcx), %rsi
	cmpq	%rsi, %r15
	je	.L391
	movl	8(%rcx), %ecx
	movl	%ecx, 8(%rdx)
.L391:
	leaq	8(%rax,%r8), %r15
.L387:
	movq	%rax, %r12
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L449:
	testq	%rdi, %rdi
	jne	.L451
	xorl	%eax, %eax
	movl	$4, %esi
	xorl	%ebx, %ebx
	movl	%r9d, (%rax,%rdx)
	cmpq	%r15, %r12
	jne	.L452
.L425:
	movq	%rsi, %r15
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L379:
	movq	(%r14), %r13
	movq	%r12, -56(%rbp)
	movl	144(%r13), %esi
	cmpq	%r15, %r12
	je	.L392
	movl	-4(%r15), %ebx
	cmpl	%esi, %ebx
	je	.L417
.L393:
	leaq	136(%r13), %r12
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv@PLT
	movl	144(%r13), %esi
	cmpl	%ebx, %esi
	je	.L453
.L397:
	movq	128(%r13), %rax
	cmpl	$-1, 24(%rax)
	je	.L396
	cmpl	%esi, 32(%rax)
	jne	.L396
	movq	40(%rax), %rax
	movq	536(%r13), %rcx
	movabsq	$140735340871680, %rdx
	andq	544(%r13), %rdx
	andl	$2147483646, %eax
	orq	%rdx, %rax
	movq	%rax, 16(%rcx)
	movq	128(%r13), %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L453:
	movq	(%r14), %r13
	movl	144(%r13), %esi
	cmpq	%r15, -56(%rbp)
	je	.L395
	.p2align 4,,10
	.p2align 3
.L417:
	movl	-4(%r15), %ebx
	cmpl	%esi, %ebx
	je	.L399
	leaq	136(%r13), %r12
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L400:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv@PLT
	movl	144(%r13), %esi
	cmpl	%esi, %ebx
	je	.L454
.L401:
	movq	128(%r13), %rax
	cmpl	$-1, 24(%rax)
	je	.L400
	cmpl	%esi, 32(%rax)
	jne	.L400
	movq	40(%rax), %rax
	movq	536(%r13), %rcx
	movabsq	$140735340871680, %rdx
	andq	544(%r13), %rdx
	andl	$2147483646, %eax
	orq	%rdx, %rax
	movq	%rax, 16(%rcx)
	movq	128(%r13), %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L454:
	movq	(%r14), %r13
	movl	144(%r13), %esi
.L399:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder30ExitThenEnterExceptionHandlersEi
	movq	(%r14), %rdx
	movq	88(%r14), %rdi
	movq	128(%rdx), %rax
	movl	400(%rdx), %r9d
	leaq	-48(%rdi), %rdx
	movl	24(%rax), %r8d
	movl	32(%rax), %ecx
	movq	40(%rax), %r13
	movzbl	48(%rax), %r12d
	movl	56(%rax), %ebx
	movq	72(%r14), %rax
	cmpq	%rdx, %rax
	je	.L402
	movl	%r9d, (%rax)
	movl	%r8d, 8(%rax)
	movl	%ecx, 16(%rax)
	movq	%r13, 24(%rax)
	movb	%r12b, 32(%rax)
	movl	%ebx, 40(%rax)
	addq	$48, 72(%r14)
.L403:
	movq	(%r14), %r13
	subq	$4, %r15
	movl	144(%r13), %esi
	cmpq	-56(%rbp), %r15
	jne	.L417
.L395:
	movl	-60(%rbp), %r12d
	cmpl	%esi, %r12d
	je	.L418
	leaq	136(%r13), %rbx
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L419:
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv@PLT
	movl	144(%r13), %esi
	cmpl	%esi, %r12d
	je	.L455
.L420:
	movq	128(%r13), %rax
	cmpl	$-1, 24(%rax)
	je	.L419
	cmpl	32(%rax), %esi
	jne	.L419
	movq	40(%rax), %rax
	movq	536(%r13), %rcx
	movabsq	$140735340871680, %rdx
	andq	544(%r13), %rdx
	andl	$2147483646, %eax
	orq	%rdx, %rax
	movq	%rax, 16(%rcx)
	movq	128(%r13), %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L402:
	movq	96(%r14), %rdx
	movq	64(%r14), %r11
	movabsq	$-6148914691236517205, %r10
	subq	80(%r14), %rax
	movq	%rdx, %rdi
	sarq	$4, %rax
	imulq	%r10, %rax
	subq	%r11, %rdi
	movq	%rdi, -72(%rbp)
	sarq	$3, %rdi
	leaq	-5(%rdi,%rdi,4), %rsi
	leaq	(%rax,%rsi,2), %rsi
	movq	56(%r14), %rax
	subq	40(%r14), %rax
	sarq	$4, %rax
	imulq	%r10, %rax
	addq	%rsi, %rax
	cmpq	$44739242, %rax
	je	.L456
	movq	24(%r14), %r10
	movq	32(%r14), %rsi
	movq	%rdx, %rax
	subq	%r10, %rax
	movq	%r10, -80(%rbp)
	movq	%rsi, %r10
	sarq	$3, %rax
	subq	%rax, %r10
	cmpq	$1, %r10
	jbe	.L457
.L405:
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.L413
	cmpq	$9, 8(%rax)
	ja	.L458
.L413:
	movq	8(%r14), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rsi
	subq	%rax, %rsi
	cmpq	$479, %rsi
	jbe	.L459
	leaq	480(%rax), %rsi
	movq	%rsi, 16(%rdi)
.L414:
	movq	%rax, 8(%rdx)
	movq	72(%r14), %rax
	movl	%r8d, 8(%rax)
	movl	%r9d, (%rax)
	movl	%ecx, 16(%rax)
	movq	%r13, 24(%rax)
	movb	%r12b, 32(%rax)
	movl	%ebx, 40(%rax)
	movq	96(%r14), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 96(%r14)
	movq	8(%rax), %rax
	leaq	480(%rax), %rdx
	movq	%rax, 80(%r14)
	movq	%rdx, 88(%r14)
	movq	%rax, 72(%r14)
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L455:
	movq	(%r14), %r13
.L418:
	movl	-60(%rbp), %r15d
	movq	%r13, %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder30ExitThenEnterExceptionHandlersEi
	movq	(%r14), %rbx
	movl	%r15d, %esi
	movq	160(%rbx), %rdi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis14GetLoopInfoForEi@PLT
	movl	(%rax), %eax
	movl	%eax, 180(%rbx)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	movl	$8, %esi
	movl	$4, %ebx
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L457:
	addq	$2, %rdi
	leaq	(%rdi,%rdi), %rax
	movq	%rdi, -88(%rbp)
	cmpq	%rax, %rsi
	jbe	.L406
	movq	-80(%rbp), %rax
	subq	%rdi, %rsi
	addq	$8, %rdx
	shrq	%rsi
	leaq	(%rax,%rsi,8), %r10
	movq	%rdx, %rax
	subq	%r11, %rax
	cmpq	%r10, %r11
	jbe	.L407
	cmpq	%rdx, %r11
	je	.L408
	movq	%r10, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	movl	%r8d, -96(%rbp)
	movl	%r9d, -88(%rbp)
	movl	%ecx, -80(%rbp)
	call	memmove@PLT
	movl	-80(%rbp), %ecx
	movl	-88(%rbp), %r9d
	movl	-96(%rbp), %r8d
	movq	%rax, %r10
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L458:
	movq	(%rax), %rsi
	movq	%rsi, 16(%r14)
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L392:
	movl	-60(%rbp), %eax
	movl	%eax, %ebx
	cmpl	%esi, %eax
	jne	.L393
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L426:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L388:
	movl	(%r12,%rdx,4), %edi
	movl	%edi, (%rax,%rdx,4)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L388
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L406:
	testq	%rsi, %rsi
	movl	$1, %eax
	movq	8(%r14), %rdi
	cmovne	%rsi, %rax
	movq	16(%rdi), %r11
	leaq	2(%rsi,%rax), %rax
	movq	%rax, -80(%rbp)
	leaq	0(,%rax,8), %rsi
	movq	24(%rdi), %rax
	subq	%r11, %rax
	cmpq	%rax, %rsi
	ja	.L460
	addq	%r11, %rsi
	movq	%rsi, 16(%rdi)
.L410:
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	shrq	%rax
	movq	64(%r14), %rsi
	leaq	(%r11,%rax,8), %r10
	movq	96(%r14), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L411
	movq	%r10, %rdi
	subq	%rsi, %rdx
	movq	%r11, -104(%rbp)
	movl	%r8d, -64(%rbp)
	movl	%r9d, -96(%rbp)
	movl	%ecx, -88(%rbp)
	call	memmove@PLT
	movq	-104(%rbp), %r11
	movl	-64(%rbp), %r8d
	movl	-96(%rbp), %r9d
	movl	-88(%rbp), %ecx
	movq	%rax, %r10
.L411:
	movq	32(%r14), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L412
	movq	24(%r14), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L412:
	movq	-80(%rbp), %rax
	movq	%r11, 24(%r14)
	movq	%rax, 32(%r14)
.L408:
	movq	%r10, 64(%r14)
	movq	(%r10), %rax
	movq	(%r10), %xmm0
	movq	-72(%rbp), %rdx
	addq	$480, %rax
	movq	%rax, %xmm2
	addq	%r10, %rdx
	punpcklqdq	%xmm2, %xmm0
	movq	%rdx, 96(%r14)
	movups	%xmm0, 48(%r14)
	movq	(%rdx), %rax
	movq	%rax, 80(%r14)
	addq	$480, %rax
	movq	%rax, 88(%r14)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L407:
	cmpq	%rdx, %r11
	je	.L408
	movl	%ecx, -88(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rax, %rdx
	movq	%r11, %rsi
	movl	%r8d, -64(%rbp)
	leaq	8(%rcx), %rdi
	movl	%r9d, -96(%rbp)
	subq	%rax, %rdi
	movq	%r10, -80(%rbp)
	addq	%r10, %rdi
	call	memmove@PLT
	movq	-80(%rbp), %r10
	movl	-88(%rbp), %ecx
	movl	-96(%rbp), %r9d
	movl	-64(%rbp), %r8d
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L446:
	leaq	.LC10(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L459:
	movl	$480, %esi
	movq	%rdx, -96(%rbp)
	movl	%r8d, -88(%rbp)
	movl	%r9d, -80(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-72(%rbp), %ecx
	movl	-80(%rbp), %r9d
	movl	-88(%rbp), %r8d
	movq	-96(%rbp), %rdx
	jmp	.L414
.L450:
	movq	-56(%rbp), %rdi
	movl	%r9d, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movl	-80(%rbp), %r9d
	jmp	.L386
.L460:
	movl	%r8d, -104(%rbp)
	movl	%r9d, -64(%rbp)
	movl	%ecx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-96(%rbp), %ecx
	movl	-64(%rbp), %r9d
	movl	-104(%rbp), %r8d
	movq	%rax, %r11
	jmp	.L410
.L448:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L456:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L451:
	cmpq	$536870911, %rdi
	movl	$536870911, %ebx
	cmovbe	%rdi, %rbx
	salq	$2, %rbx
	leaq	7(%rbx), %rsi
	andq	$-8, %rsi
	jmp	.L383
	.cfi_endproc
.LFE24500:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState17ProcessOsrPreludeEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState17ProcessOsrPreludeEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_:
.LFB24768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%esi, %rbx
	movl	$8, %esi
	movl	%ebx, %edx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	leal	1(%rbx), %edx
	movq	%rax, %r15
	movl	404(%r12), %eax
	cmpl	%eax, %edx
	jg	.L462
	movq	408(%r12), %r9
.L463:
	movq	%rbx, %rcx
	movq	%r9, %rdi
	movq	%r13, %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	%r14, (%r9,%rbx,8)
	movq	16(%r12), %rax
	movq	%r15, %rsi
	movq	%r9, %rcx
	movl	$1, %r8d
	movq	(%rax), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	.p2align 4,,10
	.p2align 3
.L462:
	.cfi_restore_state
	movq	8(%r12), %rdi
	leal	65(%rax,%rbx), %ecx
	movslq	%ecx, %rsi
	movq	16(%rdi), %r9
	movq	24(%rdi), %rax
	salq	$3, %rsi
	subq	%r9, %rax
	cmpq	%rax, %rsi
	ja	.L467
	addq	%r9, %rsi
	movq	%rsi, 16(%rdi)
.L465:
	movq	%r9, 408(%r12)
	movl	%ecx, 404(%r12)
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L467:
	movl	%ecx, -56(%rbp)
	movl	%edx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %edx
	movl	-56(%rbp), %ecx
	movq	%rax, %r9
	jmp	.L465
	.cfi_endproc
.LFE24768:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_, .-_ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder12NewEffectPhiEiPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder12NewEffectPhiEiPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder12NewEffectPhiEiPNS1_4NodeES4_, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder12NewEffectPhiEiPNS1_4NodeES4_:
.LFB24769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%esi, %rbx
	movl	%ebx, %esi
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	leal	1(%rbx), %edx
	movq	%rax, %r15
	movl	404(%r12), %eax
	cmpl	%eax, %edx
	jg	.L469
	movq	408(%r12), %r9
.L470:
	movq	%rbx, %rcx
	movq	%r9, %rdi
	movq	%r13, %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	%r14, (%r9,%rbx,8)
	movq	16(%r12), %rax
	movq	%r15, %rsi
	movq	%r9, %rcx
	movl	$1, %r8d
	movq	(%rax), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	movq	8(%r12), %rdi
	leal	65(%rax,%rbx), %ecx
	movslq	%ecx, %rsi
	movq	16(%rdi), %r9
	movq	24(%rdi), %rax
	salq	$3, %rsi
	subq	%r9, %rax
	cmpq	%rax, %rsi
	ja	.L474
	addq	%r9, %rsi
	movq	%rsi, 16(%rdi)
.L472:
	movq	%r9, 408(%r12)
	movl	%ecx, 404(%r12)
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L474:
	movl	%ecx, -56(%rbp)
	movl	%edx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %edx
	movl	-56(%rbp), %ecx
	movq	%rax, %r9
	jmp	.L472
	.cfi_endproc
.LFE24769:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder12NewEffectPhiEiPNS1_4NodeES4_, .-_ZN2v88internal8compiler20BytecodeGraphBuilder12NewEffectPhiEiPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder12MergeControlEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder12MergeControlEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder12MergeControlEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder12MergeControlEPNS1_4NodeES4_:
.LFB24770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	28(%rax), %ecx
	movzwl	16(%rax), %eax
	leal	1(%rcx), %esi
	movq	16(%rdi), %rcx
	movq	8(%rcx), %rdi
	cmpl	$1, %eax
	je	.L482
	cmpl	$10, %eax
	je	.L483
	movq	%rdx, %xmm1
	movq	%rbx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movdqa	-80(%rbp), %xmm0
	leaq	-64(%rbp), %rcx
	movl	$1, %r8d
	movq	%rax, %rsi
	movq	16(%r12), %rax
	movl	$2, %edx
	movaps	%xmm0, -64(%rbp)
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
.L475:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L484
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
.L481:
	movq	%rax, %r14
	movq	16(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L482:
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4LoopEi@PLT
	jmp	.L481
.L484:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24770:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder12MergeControlEPNS1_4NodeES4_, .-_ZN2v88internal8compiler20BytecodeGraphBuilder12MergeControlEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11MergeEffectEPNS1_4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11MergeEffectEPNS1_4NodeES4_S4_
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11MergeEffectEPNS1_4NodeES4_S4_, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11MergeEffectEPNS1_4NodeES4_S4_:
.LFB24771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rcx), %rax
	movl	28(%rax), %r15d
	movq	(%rsi), %rax
	cmpw	$36, 16(%rax)
	je	.L500
.L486:
	cmpq	%r13, %r12
	je	.L485
	movq	%r12, %rdx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder12NewEffectPhiEiPNS1_4NodeES4_
	movq	%rax, %r12
	leal	-1(%r15), %eax
	movzbl	23(%r12), %edx
	cltq
	salq	$3, %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L488
	leaq	32(%r12,%rax), %rbx
	movq	%r12, %rdx
	movq	(%rbx), %rdi
	cmpq	%rdi, %r13
	je	.L485
.L489:
	negl	%r15d
	movslq	%r15d, %r15
	leaq	(%r15,%r15,2), %rax
	leaq	(%rdx,%rax,8), %r14
	testq	%rdi, %rdi
	je	.L490
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L490:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L485
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L485:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L500:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-56(%rbp), %rcx
	cmpq	%rax, %rcx
	jne	.L486
	movq	16(%rbx), %rax
	leal	-1(%r15), %edx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movl	%r15d, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L488:
	movq	32(%r12), %rdx
	leaq	16(%rdx,%rax), %rbx
	movq	(%rbx), %rdi
	cmpq	%rdi, %r13
	jne	.L489
	jmp	.L485
	.cfi_endproc
.LFE24771:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11MergeEffectEPNS1_4NodeES4_S4_, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11MergeEffectEPNS1_4NodeES4_S4_
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder10MergeValueEPNS1_4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder10MergeValueEPNS1_4NodeES4_S4_
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder10MergeValueEPNS1_4NodeES4_S4_, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder10MergeValueEPNS1_4NodeES4_S4_:
.LFB24772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rcx), %rax
	movl	28(%rax), %r15d
	movq	(%rsi), %rax
	cmpw	$35, 16(%rax)
	je	.L516
.L502:
	cmpq	%r13, %r12
	je	.L501
	movq	%r12, %rdx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_
	movq	%rax, %r12
	leal	-1(%r15), %eax
	movzbl	23(%r12), %edx
	cltq
	salq	$3, %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L504
	leaq	32(%r12,%rax), %rbx
	movq	%r12, %rdx
	movq	(%rbx), %rdi
	cmpq	%rdi, %r13
	je	.L501
.L505:
	negl	%r15d
	movslq	%r15d, %r15
	leaq	(%r15,%r15,2), %rax
	leaq	(%rdx,%rax,8), %r14
	testq	%rdi, %rdi
	je	.L506
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L506:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L501
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L501:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-56(%rbp), %rcx
	cmpq	%rax, %rcx
	jne	.L502
	movq	16(%rbx), %rax
	leal	-1(%r15), %edx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%rbx), %rax
	movl	%r15d, %edx
	movl	$8, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L504:
	movq	32(%r12), %rdx
	leaq	16(%rdx,%rax), %rbx
	movq	(%rbx), %rdi
	cmpq	%rdi, %r13
	jne	.L505
	jmp	.L501
	.cfi_endproc
.LFE24772:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder10MergeValueEPNS1_4NodeES4_S4_, .-_ZN2v88internal8compiler20BytecodeGraphBuilder10MergeValueEPNS1_4NodeES4_S4_
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment5MergeEPS3_PKNS1_21BytecodeLivenessStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment5MergeEPS3_PKNS1_21BytecodeLivenessStateE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment5MergeEPS3_PKNS1_21BytecodeLivenessStateE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment5MergeEPS3_PKNS1_21BytecodeLivenessStateE:
.LFB24423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rdx, -64(%rbp)
	movq	24(%rsi), %rdx
	movq	24(%rdi), %rsi
	movq	(%rdi), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder12MergeControlEPNS1_4NodeES4_
	movq	32(%rbx), %rsi
	movq	(%rbx), %rdi
	movq	%rax, 24(%rbx)
	movq	32(%r14), %rdx
	movq	%rax, %rcx
	movq	%rax, %r15
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11MergeEffectEPNS1_4NodeES4_S4_
	movq	16(%rbx), %rsi
	movq	(%rbx), %rdi
	movq	%r15, %rcx
	movq	%rax, 32(%rbx)
	movq	16(%r14), %rdx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder10MergeValueEPNS1_4NodeES4_S4_
	movl	12(%rbx), %edx
	movq	%rax, 16(%rbx)
	testl	%edx, %edx
	jg	.L518
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L522:
	cmpq	%r12, %r9
	movq	%r9, -56(%rbp)
	je	.L523
	movq	%r12, %rdx
	movl	%r8d, %esi
	movq	%r15, %rcx
	movq	%r10, %rdi
	movq	%r11, -80(%rbp)
	movl	%r8d, -72(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_
	movl	-72(%rbp), %r8d
	movq	-80(%rbp), %r11
	movq	%rax, %r12
	movq	-56(%rbp), %r9
	movzbl	23(%r12), %edx
	leal	-1(%r8), %eax
	cltq
	andl	$15, %edx
	salq	$3, %rax
	cmpl	$15, %edx
	je	.L524
	leaq	32(%r12,%rax), %rax
	movq	%r12, %rdx
	movq	(%rax), %rdi
	cmpq	%rdi, %r9
	je	.L523
.L525:
	negl	%r8d
	movslq	%r8d, %r8
	leaq	(%r8,%r8,2), %rcx
	leaq	(%rdx,%rcx,8), %rsi
	testq	%rdi, %rdi
	je	.L526
	movq	%rax, -88(%rbp)
	movq	%r11, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %r11
	movq	-72(%rbp), %r9
	movq	-56(%rbp), %rsi
.L526:
	movq	%r9, (%rax)
	testq	%r9, %r9
	je	.L523
	movq	%r9, %rdi
	movq	%r11, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %r11
.L523:
	movq	%r12, (%r11)
	addq	$1, %r13
	cmpl	%r13d, 12(%rbx)
	jle	.L527
.L518:
	movq	48(%r14), %rax
	movq	(%rbx), %r10
	movq	(%rax,%r13,8), %r9
	movq	48(%rbx), %rax
	leaq	(%rax,%r13,8), %r11
	movq	(%r15), %rax
	movq	(%r11), %r12
	movl	28(%rax), %r8d
	movq	(%r12), %rax
	cmpw	$35, 16(%rax)
	jne	.L522
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%r8d, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-56(%rbp), %r9
	movq	-72(%rbp), %r11
	cmpq	%rax, %r15
	movq	-80(%rbp), %r10
	movl	-88(%rbp), %r8d
	jne	.L522
	movq	16(%r10), %rax
	leal	-1(%r8), %edx
	movq	%r9, %rcx
	movq	%r12, %rdi
	movq	%r11, -80(%rbp)
	addq	$1, %r13
	movq	(%rax), %rax
	movl	%r8d, -72(%rbp)
	movq	%r10, -56(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	-56(%rbp), %r10
	movl	-72(%rbp), %r8d
	movl	$8, %esi
	movq	16(%r10), %rax
	movl	%r8d, %edx
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-80(%rbp), %r11
	movq	%r12, (%r11)
	cmpl	%r13d, 12(%rbx)
	jg	.L518
	.p2align 4,,10
	.p2align 3
.L527:
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jle	.L563
	xorl	%r12d, %r12d
	cmpq	$0, -64(%rbp)
	je	.L528
	.p2align 4,,10
	.p2align 3
.L535:
	movl	88(%rbx), %eax
	movq	-64(%rbp), %rcx
	movq	48(%rbx), %rdx
	movq	(%rbx), %rdi
	addl	%r12d, %eax
	cmpl	$1, 12(%rcx)
	cltq
	leaq	(%rdx,%rax,8), %r13
	movq	16(%rcx), %rdx
	je	.L534
	movl	%r12d, %ecx
	sarl	$6, %ecx
	movslq	%ecx, %rcx
	movq	(%rdx,%rcx,8), %rdx
.L534:
	btq	%r12, %rdx
	jnc	.L564
	movq	48(%r14), %rdx
	movq	0(%r13), %rsi
	movq	%r15, %rcx
	addl	$1, %r12d
	movq	(%rdx,%rax,8), %rdx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder10MergeValueEPNS1_4NodeES4_S4_
	movq	%rax, 0(%r13)
	cmpl	8(%rbx), %r12d
	jl	.L535
.L532:
	movslq	92(%rbx), %rcx
	movq	48(%rbx), %r12
	movq	(%rbx), %rdi
	salq	$3, %rcx
	addq	%rcx, %r12
.L521:
	movq	-64(%rbp), %r10
	movl	8(%r10), %edx
	cmpl	$1, 12(%r10)
	movq	16(%r10), %rsi
	leal	-1(%rdx), %eax
	je	.L538
	addl	$62, %edx
	testl	%eax, %eax
	cmovns	%eax, %edx
	sarl	$6, %edx
	movslq	%edx, %rdx
	movq	(%rsi,%rdx,8), %rsi
.L538:
	cltd
	shrl	$26, %edx
	addl	%edx, %eax
	andl	$63, %eax
	subl	%edx, %eax
	btq	%rax, %rsi
	jnc	.L565
.L520:
	movq	48(%r14), %rax
	movq	(%r12), %rsi
	movq	(%rax,%rcx), %rdx
	movq	%r15, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder10MergeValueEPNS1_4NodeES4_S4_
	movq	%rax, (%r12)
.L539:
	movq	80(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L517
	movq	80(%r14), %rdx
	movq	(%rbx), %rdi
	movq	%r15, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder10MergeValueEPNS1_4NodeES4_S4_
	movq	%rax, 80(%rbx)
.L517:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	movq	32(%r12), %rdx
	leaq	16(%rdx,%rax), %rax
	movq	(%rax), %rdi
	cmpq	%rdi, %r9
	jne	.L525
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L528:
	movl	88(%rbx), %eax
	movq	48(%rbx), %rdx
	movq	%r15, %rcx
	movq	(%rbx), %rdi
	addl	%r12d, %eax
	addl	$1, %r12d
	cltq
	leaq	(%rdx,%rax,8), %r13
	movq	48(%r14), %rdx
	movq	0(%r13), %rsi
	movq	(%rdx,%rax,8), %rdx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder10MergeValueEPNS1_4NodeES4_S4_
	movq	%rax, 0(%r13)
	cmpl	8(%rbx), %r12d
	jl	.L528
	movslq	92(%rbx), %rcx
	movq	48(%rbx), %r12
	movq	(%rbx), %rdi
	salq	$3, %rcx
	addq	%rcx, %r12
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L564:
	movq	16(%rdi), %rdi
	addl	$1, %r12d
	call	_ZN2v88internal8compiler7JSGraph20OptimizedOutConstantEv@PLT
	movq	%rax, 0(%r13)
	cmpl	%r12d, 8(%rbx)
	jg	.L535
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L565:
	movq	16(%rdi), %rdi
	call	_ZN2v88internal8compiler7JSGraph20OptimizedOutConstantEv@PLT
	movq	%rax, (%r12)
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L563:
	movslq	92(%rbx), %rcx
	movq	48(%rbx), %r12
	movq	(%rbx), %rdi
	salq	$3, %rcx
	addq	%rcx, %r12
	cmpq	$0, -64(%rbp)
	jne	.L521
	jmp	.L520
	.cfi_endproc
.LFE24423:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment5MergeEPS3_PKNS1_21BytecodeLivenessStateE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment5MergeEPS3_PKNS1_21BytecodeLivenessStateE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder24SwitchToMergeEnvironmentEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder24SwitchToMergeEnvironmentEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder24SwitchToMergeEnvironmentEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder24SwitchToMergeEnvironmentEi:
.LFB24735:
	.cfi_startproc
	endbr64
	movq	216(%rdi), %rax
	testq	%rax, %rax
	je	.L577
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	208(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L580:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L569
.L568:
	cmpl	%esi, 32(%rax)
	jge	.L580
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L568
.L569:
	cmpq	%r12, %rdx
	je	.L566
	cmpl	%esi, 32(%r12)
	jg	.L566
	cmpq	$0, 168(%rbx)
	movb	$1, 416(%rbx)
	je	.L572
	movq	160(%rbx), %rdi
	movq	40(%r12), %r13
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movq	168(%rbx), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment5MergeEPS3_PKNS1_21BytecodeLivenessStateE
.L572:
	movq	40(%r12), %rax
	movq	%rax, 168(%rbx)
.L566:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L577:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE24735:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder24SwitchToMergeEnvironmentEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder24SwitchToMergeEnvironmentEi
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder20UpdateSourcePositionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder20UpdateSourcePositionEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder20UpdateSourcePositionEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder20UpdateSourcePositionEi:
.LFB24773:
	.cfi_startproc
	endbr64
	movq	128(%rdi), %rax
	cmpl	$-1, 24(%rax)
	je	.L581
	cmpl	32(%rax), %esi
	je	.L583
.L581:
	ret
	.p2align 4,,10
	.p2align 3
.L583:
	movq	40(%rax), %rax
	movq	536(%rdi), %rcx
	movabsq	$140735340871680, %rdx
	andq	544(%rdi), %rdx
	andl	$2147483646, %eax
	orq	%rdx, %rax
	movq	%rax, 16(%rcx)
	movq	128(%rdi), %rdi
	jmp	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	.cfi_endproc
.LFE24773:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder20UpdateSourcePositionEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder20UpdateSourcePositionEi
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB28319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L622
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L600
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L623
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L586:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L624
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L589:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L623:
	testq	%rdx, %rdx
	jne	.L625
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L587:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L590
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L603
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L603
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L592:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L592
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L594
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L594:
	leaq	16(%rax,%r8), %rcx
.L590:
	cmpq	%r14, %r12
	je	.L595
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L604
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L604
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L597:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L597
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L599
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L599:
	leaq	8(%rcx,%r9), %rcx
.L595:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L600:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L604:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L596:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L596
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L603:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L591:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L591
	jmp	.L594
.L624:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L589
.L622:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L625:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L586
	.cfi_endproc
.LFE28319:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder27MergeControlToLeaveFunctionEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder27MergeControlToLeaveFunctionEPNS1_4NodeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder27MergeControlToLeaveFunctionEPNS1_4NodeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder27MergeControlToLeaveFunctionEPNS1_4NodeE:
.LFB24738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, -24(%rbp)
	movq	448(%rdi), %rsi
	cmpq	456(%rdi), %rsi
	je	.L627
	movq	-24(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 448(%rdi)
.L628:
	movq	$0, 168(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L627:
	.cfi_restore_state
	leaq	-24(%rbp), %rdx
	leaq	432(%rdi), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L628
	.cfi_endproc
.LFE24738:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder27MergeControlToLeaveFunctionEPNS1_4NodeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder27MergeControlToLeaveFunctionEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder19ApplyEarlyReductionENS1_18JSTypeHintLowering14LoweringResultE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder19ApplyEarlyReductionENS1_18JSTypeHintLowering14LoweringResultE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder19ApplyEarlyReductionENS1_18JSTypeHintLowering14LoweringResultE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder19ApplyEarlyReductionENS1_18JSTypeHintLowering14LoweringResultE:
.LFB24764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	40(%rbp), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	16(%rbp), %eax
	cmpl	$2, %eax
	je	.L637
	cmpl	$1, %eax
	jne	.L630
	movq	168(%rdi), %rax
	movq	32(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movq	168(%rdi), %rax
	movq	%rdx, 24(%rax)
.L630:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L638
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L637:
	.cfi_restore_state
	movq	%rdx, -32(%rbp)
	movq	448(%rdi), %rsi
	cmpq	456(%rdi), %rsi
	je	.L632
	movq	%rdx, (%rsi)
	addq	$8, 448(%rdi)
.L633:
	movq	$0, 168(%rbx)
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L632:
	leaq	-32(%rbp), %rdx
	leaq	432(%rdi), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L633
.L638:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24764:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder19ApplyEarlyReductionENS1_18JSTypeHintLowering14LoweringResultE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder19ApplyEarlyReductionENS1_18JSTypeHintLowering14LoweringResultE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder30TryBuildSimplifiedForInPrepareEPNS1_4NodeENS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder30TryBuildSimplifiedForInPrepareEPNS1_4NodeENS0_12FeedbackSlotE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder30TryBuildSimplifiedForInPrepareEPNS1_4NodeENS0_12FeedbackSlotE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder30TryBuildSimplifiedForInPrepareEPNS1_4NodeENS0_12FeedbackSlotE:
.LFB24756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$80, %rsi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	88(%rsi), %rax
	movq	32(%rax), %rcx
	movq	24(%rax), %r8
	call	_ZNK2v88internal8compiler18JSTypeHintLowering27ReduceForInPrepareOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE@PLT
	movl	(%r12), %eax
	movq	24(%r12), %rdx
	cmpl	$2, %eax
	je	.L646
	cmpl	$1, %eax
	jne	.L639
	movq	168(%rbx), %rax
	movq	16(%r12), %rcx
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
.L639:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L647
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L646:
	.cfi_restore_state
	movq	%rdx, -32(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L641
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L642:
	movq	$0, 168(%rbx)
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L641:
	leaq	-32(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L642
.L647:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24756:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder30TryBuildSimplifiedForInPrepareEPNS1_4NodeENS0_12FeedbackSlotE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder30TryBuildSimplifiedForInPrepareEPNS1_4NodeENS0_12FeedbackSlotE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder26TryBuildSimplifiedToNumberEPNS1_4NodeENS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder26TryBuildSimplifiedToNumberEPNS1_4NodeENS0_12FeedbackSlotE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder26TryBuildSimplifiedToNumberEPNS1_4NodeENS0_12FeedbackSlotE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder26TryBuildSimplifiedToNumberEPNS1_4NodeENS0_12FeedbackSlotE:
.LFB24757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$80, %rsi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	88(%rsi), %rax
	movq	32(%rax), %rcx
	movq	24(%rax), %r8
	call	_ZNK2v88internal8compiler18JSTypeHintLowering23ReduceToNumberOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE@PLT
	movl	(%r12), %eax
	movq	24(%r12), %rdx
	cmpl	$2, %eax
	je	.L655
	cmpl	$1, %eax
	jne	.L648
	movq	168(%rbx), %rax
	movq	16(%r12), %rcx
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
.L648:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L656
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L655:
	.cfi_restore_state
	movq	%rdx, -32(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L650
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L651:
	movq	$0, 168(%rbx)
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L650:
	leaq	-32(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L651
.L656:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24757:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder26TryBuildSimplifiedToNumberEPNS1_4NodeENS0_12FeedbackSlotE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder26TryBuildSimplifiedToNumberEPNS1_4NodeENS0_12FeedbackSlotE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder25TryBuildSimplifiedUnaryOpEPKNS1_8OperatorEPNS1_4NodeENS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder25TryBuildSimplifiedUnaryOpEPKNS1_8OperatorEPNS1_4NodeENS0_12FeedbackSlotE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder25TryBuildSimplifiedUnaryOpEPKNS1_8OperatorEPNS1_4NodeENS0_12FeedbackSlotE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder25TryBuildSimplifiedUnaryOpEPKNS1_8OperatorEPNS1_4NodeENS0_12FeedbackSlotE:
.LFB24753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$80, %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	88(%rsi), %rax
	movq	24(%rax), %r9
	movq	32(%rax), %rax
	pushq	%r8
	movq	%rax, %r8
	call	_ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE@PLT
	movl	(%r12), %eax
	popq	%rcx
	movq	24(%r12), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L664
	cmpl	$1, %eax
	jne	.L657
	movq	168(%rbx), %rax
	movq	16(%r12), %rcx
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
.L657:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L665
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L664:
	.cfi_restore_state
	movq	%rdx, -32(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L659
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L660:
	movq	$0, 168(%rbx)
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L659:
	leaq	-32(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L660
.L665:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24753:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder25TryBuildSimplifiedUnaryOpEPKNS1_8OperatorEPNS1_4NodeENS0_12FeedbackSlotE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder25TryBuildSimplifiedUnaryOpEPKNS1_8OperatorEPNS1_4NodeENS0_12FeedbackSlotE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedLoadNamedEPKNS1_8OperatorEPNS1_4NodeENS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedLoadNamedEPKNS1_8OperatorEPNS1_4NodeENS0_12FeedbackSlotE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedLoadNamedEPKNS1_8OperatorEPNS1_4NodeENS0_12FeedbackSlotE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedLoadNamedEPKNS1_8OperatorEPNS1_4NodeENS0_12FeedbackSlotE:
.LFB24760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$80, %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	88(%rsi), %rax
	movq	24(%rax), %r9
	movq	32(%rax), %rax
	pushq	%r8
	movq	%rax, %r8
	call	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE@PLT
	movl	(%r12), %eax
	popq	%rcx
	movq	24(%r12), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L673
	cmpl	$1, %eax
	jne	.L666
	movq	168(%rbx), %rax
	movq	16(%r12), %rcx
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
.L666:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L674
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L673:
	.cfi_restore_state
	movq	%rdx, -32(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L668
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L669:
	movq	$0, 168(%rbx)
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L668:
	leaq	-32(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L669
.L674:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24760:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedLoadNamedEPKNS1_8OperatorEPNS1_4NodeENS0_12FeedbackSlotE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedLoadNamedEPKNS1_8OperatorEPNS1_4NodeENS0_12FeedbackSlotE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder26TryBuildSimplifiedBinaryOpEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder26TryBuildSimplifiedBinaryOpEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder26TryBuildSimplifiedBinaryOpEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder26TryBuildSimplifiedBinaryOpEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE:
.LFB24754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$80, %rsi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	88(%rsi), %rax
	movq	32(%rax), %rdi
	pushq	%r9
	pushq	24(%rax)
	movq	%rdi, %r9
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE@PLT
	movl	(%r12), %eax
	popq	%rcx
	movq	24(%r12), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L682
	cmpl	$1, %eax
	jne	.L675
	movq	168(%rbx), %rax
	movq	16(%r12), %rcx
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
.L675:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L683
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L682:
	.cfi_restore_state
	movq	%rdx, -32(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L677
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L678:
	movq	$0, 168(%rbx)
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L677:
	leaq	-32(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L678
.L683:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24754:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder26TryBuildSimplifiedBinaryOpEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder26TryBuildSimplifiedBinaryOpEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedLoadKeyedEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedLoadKeyedEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedLoadKeyedEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedLoadKeyedEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE:
.LFB24761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$80, %rsi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	88(%rsi), %rax
	movq	32(%rax), %rdi
	pushq	%r9
	pushq	24(%rax)
	movq	%rdi, %r9
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE@PLT
	movl	(%r12), %eax
	popq	%rcx
	movq	24(%r12), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L691
	cmpl	$1, %eax
	jne	.L684
	movq	168(%rbx), %rax
	movq	16(%r12), %rcx
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
.L684:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L692
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L691:
	.cfi_restore_state
	movq	%rdx, -32(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L686
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L687:
	movq	$0, 168(%rbx)
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L686:
	leaq	-32(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L687
.L692:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24761:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedLoadKeyedEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedLoadKeyedEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder28TryBuildSimplifiedStoreNamedEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder28TryBuildSimplifiedStoreNamedEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder28TryBuildSimplifiedStoreNamedEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder28TryBuildSimplifiedStoreNamedEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE:
.LFB24762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$80, %rsi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	88(%rsi), %rax
	movq	32(%rax), %rdi
	pushq	%r9
	pushq	24(%rax)
	movq	%rdi, %r9
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE@PLT
	movl	(%r12), %eax
	popq	%rcx
	movq	24(%r12), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L700
	cmpl	$1, %eax
	jne	.L693
	movq	168(%rbx), %rax
	movq	16(%r12), %rcx
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
.L693:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L701
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L700:
	.cfi_restore_state
	movq	%rdx, -32(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L695
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L696:
	movq	$0, 168(%rbx)
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L695:
	leaq	-32(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L696
.L701:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24762:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder28TryBuildSimplifiedStoreNamedEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder28TryBuildSimplifiedStoreNamedEPKNS1_8OperatorEPNS1_4NodeES7_NS0_12FeedbackSlotE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder22TryBuildSimplifiedCallEPKNS1_8OperatorEPKPNS1_4NodeEiNS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder22TryBuildSimplifiedCallEPKNS1_8OperatorEPKPNS1_4NodeEiNS0_12FeedbackSlotE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder22TryBuildSimplifiedCallEPKNS1_8OperatorEPKPNS1_4NodeEiNS0_12FeedbackSlotE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder22TryBuildSimplifiedCallEPKNS1_8OperatorEPKPNS1_4NodeEiNS0_12FeedbackSlotE:
.LFB24758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$80, %rsi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	88(%rsi), %rax
	movq	32(%rax), %rdi
	pushq	%r9
	pushq	24(%rax)
	movq	%rdi, %r9
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering19ReduceCallOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE@PLT
	movl	(%r12), %eax
	popq	%rcx
	movq	24(%r12), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L709
	cmpl	$1, %eax
	jne	.L702
	movq	168(%rbx), %rax
	movq	16(%r12), %rcx
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
.L702:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L710
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L709:
	.cfi_restore_state
	movq	%rdx, -32(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L704
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L705:
	movq	$0, 168(%rbx)
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L704:
	leaq	-32(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L705
.L710:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24758:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder22TryBuildSimplifiedCallEPKNS1_8OperatorEPKPNS1_4NodeEiNS0_12FeedbackSlotE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder22TryBuildSimplifiedCallEPKNS1_8OperatorEPKPNS1_4NodeEiNS0_12FeedbackSlotE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedConstructEPKNS1_8OperatorEPKPNS1_4NodeEiNS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedConstructEPKNS1_8OperatorEPKPNS1_4NodeEiNS0_12FeedbackSlotE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedConstructEPKNS1_8OperatorEPKPNS1_4NodeEiNS0_12FeedbackSlotE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedConstructEPKNS1_8OperatorEPKPNS1_4NodeEiNS0_12FeedbackSlotE:
.LFB24759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$80, %rsi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	88(%rsi), %rax
	movq	32(%rax), %rdi
	pushq	%r9
	pushq	24(%rax)
	movq	%rdi, %r9
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceConstructOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE@PLT
	movl	(%r12), %eax
	popq	%rcx
	movq	24(%r12), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L718
	cmpl	$1, %eax
	jne	.L711
	movq	168(%rbx), %rax
	movq	16(%r12), %rcx
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
.L711:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L719
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L718:
	.cfi_restore_state
	movq	%rdx, -32(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L713
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L714:
	movq	$0, 168(%rbx)
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L713:
	leaq	-32(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L714
.L719:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24759:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedConstructEPKNS1_8OperatorEPKPNS1_4NodeEiNS0_12FeedbackSlotE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedConstructEPKNS1_8OperatorEPKPNS1_4NodeEiNS0_12FeedbackSlotE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedForInNextEPNS1_4NodeES4_S4_S4_NS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedForInNextEPNS1_4NodeES4_S4_S4_NS0_12FeedbackSlotE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedForInNextEPNS1_4NodeES4_S4_S4_NS0_12FeedbackSlotE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedForInNextEPNS1_4NodeES4_S4_S4_NS0_12FeedbackSlotE:
.LFB24755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$80, %rsi
	subq	$24, %rsp
	movl	16(%rbp), %edi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	88(%rsi), %rax
	pushq	%rdi
	movq	%r12, %rdi
	pushq	24(%rax)
	pushq	32(%rax)
	call	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceForInNextOperationEPNS1_4NodeES4_S4_S4_S4_S4_NS0_12FeedbackSlotE@PLT
	movl	(%r12), %eax
	movq	24(%r12), %rdx
	addq	$32, %rsp
	cmpl	$2, %eax
	je	.L727
	cmpl	$1, %eax
	jne	.L720
	movq	168(%rbx), %rax
	movq	16(%r12), %rcx
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
.L720:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L728
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L727:
	.cfi_restore_state
	movq	%rdx, -32(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L722
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L723:
	movq	$0, 168(%rbx)
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L722:
	leaq	-32(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L723
.L728:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24755:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedForInNextEPNS1_4NodeES4_S4_S4_NS0_12FeedbackSlotE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder27TryBuildSimplifiedForInNextEPNS1_4NodeES4_S4_S4_NS0_12FeedbackSlotE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder28TryBuildSimplifiedStoreKeyedEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder28TryBuildSimplifiedStoreKeyedEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder28TryBuildSimplifiedStoreKeyedEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder28TryBuildSimplifiedStoreKeyedEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE:
.LFB24763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	addq	$80, %rsi
	subq	$24, %rsp
	movl	16(%rbp), %edi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	88(%rsi), %rax
	pushq	%rdi
	movq	%r12, %rdi
	pushq	24(%rax)
	pushq	32(%rax)
	call	_ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_S7_NS0_12FeedbackSlotE@PLT
	movl	(%r12), %eax
	movq	24(%r12), %rdx
	addq	$32, %rsp
	cmpl	$2, %eax
	je	.L736
	cmpl	$1, %eax
	jne	.L729
	movq	168(%rbx), %rax
	movq	16(%r12), %rcx
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
.L729:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L737
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L736:
	.cfi_restore_state
	movq	%rdx, -32(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L731
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L732:
	movq	$0, 168(%rbx)
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L731:
	leaq	-32(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L732
.L737:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24763:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder28TryBuildSimplifiedStoreKeyedEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder28TryBuildSimplifiedStoreKeyedEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_.str1.1,"aMS",@progbits,1
.LC12:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_:
.LFB28328:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L848
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	24(%r12), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jb	.L741
	movq	%rdi, %r8
	movq	(%rcx), %xmm0
	subq	%rsi, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L742
	leaq	0(,%rdx,8), %r14
	movq	%rdi, %rdx
	subq	%r14, %rdx
	cmpq	%rdx, %rdi
	je	.L789
	leaq	-8(%rdi), %rax
	movl	$16, %ecx
	subq	%rdx, %rax
	subq	%r14, %rcx
	shrq	$3, %rax
	testq	%rcx, %rcx
	leaq	16(%rdi), %rcx
	setle	%sil
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L790
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L790
	leaq	1(%rax), %r8
	xorl	%eax, %eax
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L745:
	movdqu	(%rdx,%rax), %xmm2
	movups	%xmm2, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L745
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	leaq	(%rdx,%rcx), %rsi
	addq	%rdi, %rcx
	cmpq	%rax, %r8
	je	.L747
	movq	(%rsi), %rax
	movq	%rax, (%rcx)
.L747:
	movq	16(%r12), %rax
.L743:
	addq	%r14, %rax
	movq	%rax, 16(%r12)
	cmpq	%rdx, %rbx
	je	.L748
	subq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%xmm0, -40(%rbp)
	subq	%rdx, %rdi
	call	memmove@PLT
	movq	-40(%rbp), %xmm0
.L748:
	leaq	(%rbx,%r14), %rdx
	cmpq	%rdx, %rbx
	je	.L738
	subq	%rbx, %rdx
	movq	%rbx, %rax
	subq	$8, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rdx, %rdx
	je	.L788
	movq	%rcx, %rdx
	movdqa	%xmm0, %xmm1
	shrq	%rdx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rdx
	addq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L752:
	movups	%xmm1, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L752
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	(%rbx,%rax,8), %r13
	cmpq	%rax, %rcx
	je	.L738
.L788:
	movq	%xmm0, 0(%r13)
.L738:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L848:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L742:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	subq	%rax, %rdx
	je	.L791
	cmpq	$1, %rdx
	je	.L792
	movq	%rdx, %rsi
	movdqa	%xmm0, %xmm1
	xorl	%eax, %eax
	shrq	%rsi
	punpcklqdq	%xmm1, %xmm1
	.p2align 4,,10
	.p2align 3
.L756:
	movq	%rax, %rcx
	addq	$1, %rax
	salq	$4, %rcx
	movups	%xmm1, (%rdi,%rcx)
	cmpq	%rax, %rsi
	jne	.L756
	movq	%rdx, %rcx
	andq	$-2, %rcx
	leaq	(%rdi,%rcx,8), %rax
	cmpq	%rcx, %rdx
	je	.L757
.L755:
	movq	%xmm0, (%rax)
.L757:
	leaq	(%rdi,%rdx,8), %rsi
.L754:
	movq	%rsi, 16(%r12)
	cmpq	%rbx, %rdi
	je	.L758
	subq	%rbx, %rdi
	leaq	15(%rbx), %rcx
	movq	%rbx, %rdx
	leaq	-8(%rdi), %r9
	subq	%rsi, %rcx
	movq	%r9, %rax
	shrq	$3, %rax
	cmpq	$30, %rcx
	jbe	.L793
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L793
	leaq	1(%rax), %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L760:
	movdqu	(%rbx,%rax), %xmm3
	movups	%xmm3, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L760
	movq	%rcx, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rax
	leaq	(%rbx,%rax), %rdi
	addq	%rax, %rsi
	cmpq	%r9, %rcx
	je	.L761
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
.L761:
	addq	%r8, 16(%r12)
	movq	%rcx, %rax
.L787:
	movq	%rax, %rcx
	movdqa	%xmm0, %xmm1
	shrq	%rcx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rcx
	addq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L765:
	movups	%xmm1, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L765
	movq	%rax, %rdx
	andq	$-2, %rdx
	leaq	(%rbx,%rdx,8), %r13
	cmpq	%rax, %rdx
	jne	.L788
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L741:
	movq	8(%r12), %rsi
	movl	$268435455, %r8d
	movq	%r8, %rax
	subq	%rsi, %rdi
	sarq	$3, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L851
	cmpq	%rdi, %rdx
	movq	%rdi, %rax
	cmovnb	%rdx, %rax
	addq	%rax, %rdi
	setc	%al
	subq	%rsi, %r13
	movzbl	%al, %eax
	testq	%rax, %rax
	jne	.L794
	testq	%rdi, %rdi
	jne	.L852
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L770:
	leaq	0(,%rdx,8), %r9
	leaq	(%rax,%r13), %r11
	leaq	0(%r13,%r9), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	leaq	8(%rcx), %rsi
	setnb	%dil
	cmpq	%rsi, %r11
	setnb	%sil
	orb	%sil, %dil
	je	.L796
	leaq	-1(%rdx), %rsi
	cmpq	$3, %rsi
	jbe	.L796
	movq	(%rcx), %xmm0
	leaq	-2(%rdx), %rdi
	xorl	%esi, %esi
	shrq	%rdi
	addq	$1, %rdi
	punpcklqdq	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L774:
	movq	%rsi, %r10
	addq	$1, %rsi
	salq	$4, %r10
	movups	%xmm0, (%r11,%r10)
	cmpq	%rsi, %rdi
	ja	.L774
	leaq	(%rdi,%rdi), %rsi
	salq	$4, %rdi
	addq	%r11, %rdi
	cmpq	%rsi, %rdx
	je	.L776
	movq	(%rcx), %rdx
	movq	%rdx, (%rdi)
.L776:
	movq	8(%r12), %rcx
	cmpq	%rcx, %rbx
	je	.L797
	leaq	-8(%rbx), %r10
	leaq	15(%rcx), %rdx
	subq	%rcx, %r10
	subq	%rax, %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L798
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L798
	leaq	1(%rsi), %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L779:
	movdqu	(%rcx,%rdx), %xmm4
	movups	%xmm4, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L779
	movq	%r11, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %rcx
	addq	%rax, %rsi
	cmpq	%rdx, %r11
	je	.L781
	movq	(%rcx), %rdx
	movq	%rdx, (%rsi)
.L781:
	leaq	8(%rax,%r10), %rdi
.L777:
	movq	16(%r12), %rdx
	leaq	(%rdi,%r9), %rcx
	cmpq	%rdx, %rbx
	je	.L782
	subq	%rbx, %rdx
	leaq	-8(%rdx), %r10
	leaq	16(%rdi,%r9), %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %rbx
	leaq	16(%rbx), %rdx
	setnb	%dil
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %dil
	je	.L799
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L799
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L784:
	movdqu	(%rbx,%rdx), %xmm5
	movups	%xmm5, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L784
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rdi
	addq	%rdi, %rbx
	addq	%rcx, %rdi
	cmpq	%rdx, %rsi
	je	.L786
	movq	(%rbx), %rdx
	movq	%rdx, (%rdi)
.L786:
	leaq	8(%rcx,%r10), %rcx
.L782:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%r8, 24(%r12)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L794:
	.cfi_restore_state
	movl	$2147483640, %esi
	movl	$2147483640, %r14d
.L769:
	movq	(%r12), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L853
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L772:
	leaq	(%rax,%r14), %r8
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L796:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L773:
	movq	(%rcx), %rdi
	movq	%rdi, (%r11,%rsi,8)
	addq	$1, %rsi
	cmpq	%rsi, %rdx
	jne	.L773
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L790:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L744:
	movq	(%rdx,%rcx,8), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rax, %rsi
	jne	.L744
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L793:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L759:
	movq	(%rbx,%rcx,8), %rdi
	movq	%rdi, (%rsi,%rcx,8)
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	%rax, %rdi
	jne	.L759
	addq	%r8, 16(%r12)
	addq	$1, %rax
	testq	%r9, %r9
	je	.L788
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L798:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L778:
	movq	(%rcx,%rdx,8), %rdi
	movq	%rdi, (%rax,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L778
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L799:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L783:
	movq	(%rbx,%rdx,8), %rdi
	movq	%rdi, (%rcx,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L783
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L791:
	movq	%rdi, %rsi
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L789:
	movq	%rdi, %rax
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L758:
	addq	%r8, %rsi
	movq	%rsi, 16(%r12)
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L797:
	movq	%rax, %rdi
	jmp	.L777
.L792:
	movq	%rdi, %rax
	jmp	.L755
.L853:
	movq	%r8, %rdi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	jmp	.L772
.L852:
	cmpq	$268435455, %rdi
	cmova	%r8, %rdi
	leaq	0(,%rdi,8), %r14
	movq	%r14, %rsi
	jmp	.L769
.L851:
	leaq	.LC12(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE28328:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	.section	.rodata._ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"%this"
.LC14:
	.string	"%context"
.LC15:
	.string	"%new.target"
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE:
.LFB24407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movl	%edx, -104(%rbp)
	movl	%r8d, -100(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movups	%xmm0, 24(%rdi)
	pxor	%xmm0, %xmm0
	movq	%rsi, (%rdi)
	movq	%rax, 40(%rdi)
	leaq	40(%rdi), %rax
	movl	%edx, 8(%rdi)
	movl	%ecx, 12(%rdi)
	movq	$0, 48(%rdi)
	movq	%rax, -88(%rbp)
	movups	%xmm0, 56(%rdi)
	movups	%xmm0, 72(%rdi)
	testl	%ecx, %ecx
	jle	.L867
	leaq	-72(%rbp), %rcx
	movq	%rsi, %rax
	leaq	-64(%rbp), %r13
	xorl	%r14d, %r14d
	movq	%rcx, -96(%rbp)
	leaq	.LC13(%rip), %rdx
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L872:
	addl	$1, %r14d
	movq	%rax, (%rsi)
	addq	$8, 56(%r15)
	cmpl	%r14d, %r12d
	je	.L857
.L858:
	movq	(%r15), %rax
	xorl	%edx, %edx
.L865:
	movq	16(%rax), %rax
	movl	%r14d, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	movq	(%r15), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	56(%r15), %rsi
	movq	%rax, -72(%rbp)
	cmpq	64(%r15), %rsi
	jne	.L872
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rdi
	addl	$1, %r14d
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	cmpl	%r14d, %r12d
	jne	.L858
	.p2align 4,,10
	.p2align 3
.L857:
	movq	56(%r15), %rax
	subq	48(%r15), %rax
	sarq	$3, %rax
.L855:
	movl	%eax, 88(%r15)
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	56(%r15), %rsi
	movslq	-104(%rbp), %rdx
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %rdi
	movq	%rax, -72(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	movq	56(%r15), %rsi
	movq	%rsi, %rax
	subq	48(%r15), %rax
	sarq	$3, %rax
	movl	%eax, 92(%r15)
	cmpq	64(%r15), %rsi
	je	.L859
	movq	-72(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 56(%r15)
.L860:
	movq	(%r15), %rax
	leal	2(%r12), %esi
	leaq	.LC14(%rip), %rdx
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	movq	(%r15), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-100(%rbp), %r14d
	movq	%rax, 16(%r15)
	cmpl	$2147483647, %r14d
	jne	.L873
.L854:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L874
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L873:
	.cfi_restore_state
	movq	(%r15), %rax
	leaq	.LC15(%rip), %rdx
	movl	%r12d, %esi
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	movq	(%r15), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	%r14d, -76(%rbp)
	movq	%rax, %rbx
	testl	%r14d, %r14d
	js	.L875
	movl	-100(%rbp), %esi
	addl	88(%r15), %esi
.L863:
	movq	48(%r15), %rax
	movq	56(%r15), %rdx
	movslq	%esi, %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L876
	movq	%rbx, (%rax,%rsi,8)
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L859:
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L875:
	movl	12(%r15), %esi
	leaq	-76(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	movl	%eax, %esi
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L867:
	leaq	-72(%rbp), %rcx
	xorl	%eax, %eax
	leaq	-64(%rbp), %r13
	movq	%rcx, -96(%rbp)
	jmp	.L855
.L876:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L874:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24407:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC1EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE
	.set	_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC1EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE,_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB28395:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L885
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L879:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L879
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L885:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE28395:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_
	.type	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_, @function
_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_:
.LFB28518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L950
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
.L890:
	movq	(%rcx), %rax
	leaq	16(%r13), %rcx
	movq	%r15, %r12
	movq	$0, 40(%rbx)
	movl	(%rax), %r14d
	movl	%r14d, 32(%rbx)
	cmpq	%r15, %rcx
	je	.L951
	movl	32(%r15), %r15d
	cmpl	%r15d, %r14d
	jge	.L903
	movq	32(%r13), %r15
	cmpq	%r12, %r15
	je	.L930
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	cmpl	32(%rax), %r14d
	jle	.L905
	cmpq	$0, 24(%rax)
	je	.L893
.L930:
	movq	%r12, %rax
.L925:
	testq	%rax, %rax
	setne	%al
.L924:
	cmpq	%r12, %rcx
	je	.L938
	testb	%al, %al
	je	.L952
.L938:
	movl	$1, %edi
.L915:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L903:
	.cfi_restore_state
	jle	.L901
	cmpq	%r12, 40(%r13)
	je	.L947
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	cmpl	32(%rax), %r14d
	jge	.L913
	cmpq	$0, 24(%r12)
	je	.L914
	movq	%rax, %r12
	movl	$1, %edi
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L896:
	movq	%rdx, %r12
	testb	%dil, %dil
	jne	.L894
.L899:
	cmpl	%esi, %r14d
	jg	.L902
	movq	%rdx, %r12
.L901:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L951:
	.cfi_restore_state
	cmpq	$0, 48(%r13)
	je	.L892
	movq	40(%r13), %rax
	cmpl	32(%rax), %r14d
	jg	.L893
.L892:
	movq	24(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L895
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L954:
	movq	16(%rdx), %rax
	movl	$1, %edi
.L898:
	testq	%rax, %rax
	je	.L896
	movq	%rax, %rdx
.L895:
	movl	32(%rdx), %esi
	cmpl	%esi, %r14d
	jl	.L954
	movq	24(%rdx), %rax
	xorl	%edi, %edi
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L893:
	movq	%rax, %r12
	xorl	%eax, %eax
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L952:
	movl	32(%r12), %r15d
.L914:
	xorl	%edi, %edi
	cmpl	%r15d, %r14d
	setl	%dil
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L905:
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L907
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L956:
	movq	16(%r12), %rax
	movl	$1, %esi
.L910:
	testq	%rax, %rax
	je	.L908
	movq	%rax, %r12
.L907:
	movl	32(%r12), %edx
	cmpl	%edx, %r14d
	jl	.L956
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L910
.L955:
	movq	%rcx, %r12
.L906:
	cmpq	%r12, %r15
	je	.L933
.L949:
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	movq	%r12, %rdi
	movl	32(%rax), %edx
	movq	%rax, %r12
.L921:
	cmpl	%edx, %r14d
	jle	.L901
.L922:
	movq	%rdi, %r12
.L902:
	testq	%r12, %r12
	je	.L901
.L947:
	xorl	%eax, %eax
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L950:
	movl	$48, %esi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %rbx
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L953:
	movq	%r15, %rdx
.L894:
	cmpq	%rdx, 32(%r13)
	je	.L928
	movq	%rdx, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movl	32(%rax), %esi
	movq	%rdx, %r12
	movq	%rax, %rdx
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L908:
	movq	%r12, %rdi
	testb	%sil, %sil
	je	.L921
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L913:
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L917
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L958:
	movq	16(%r12), %rax
	movl	$1, %esi
.L920:
	testq	%rax, %rax
	je	.L918
	movq	%rax, %r12
.L917:
	movl	32(%r12), %edx
	cmpl	%edx, %r14d
	jl	.L958
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L918:
	movq	%r12, %rdi
	testb	%sil, %sil
	je	.L921
.L916:
	cmpq	%r12, 32(%r13)
	jne	.L949
	movq	%r12, %rdi
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L928:
	movq	%rdx, %r12
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L933:
	movq	%r15, %rdi
	jmp	.L922
.L957:
	movq	%rcx, %r12
	jmp	.L916
	.cfi_endproc
.LFE28518:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_, .-_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS1_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS1_
	.type	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS1_, @function
_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS1_:
.LFB29367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r15
	je	.L1009
	movl	(%rdx), %r14d
	cmpl	32(%rsi), %r14d
	jge	.L970
	movq	32(%rdi), %rbx
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rsi, %rbx
	je	.L962
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	32(%rax), %r14d
	jle	.L972
	movl	$0, %ebx
	cmpq	$0, 24(%rax)
	movq	%rbx, %rax
	cmovne	%r12, %rdx
	cmovne	%r12, %rax
.L962:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L970:
	.cfi_restore_state
	jle	.L981
	movq	40(%rdi), %rdx
	cmpq	%rsi, %rdx
	je	.L1008
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	32(%rax), %r14d
	jge	.L983
	movl	$0, %ebx
	cmpq	$0, 24(%r12)
	movq	%rbx, %rax
	cmovne	%rdx, %rax
	cmove	%r12, %rdx
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L1009:
	cmpq	$0, 48(%rdi)
	je	.L961
	movq	40(%rdi), %rdx
	movl	(%r14), %eax
	cmpl	%eax, 32(%rdx)
	jl	.L1008
.L961:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L992
	movl	(%r14), %esi
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	16(%rbx), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L965
.L1011:
	movq	%rax, %rbx
.L964:
	movl	32(%rbx), %edx
	cmpl	%edx, %esi
	jl	.L1010
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L1011
.L965:
	movq	%rbx, %r12
	testb	%cl, %cl
	jne	.L963
.L968:
	xorl	%eax, %eax
	cmpl	%esi, %edx
	cmovl	%rax, %rbx
	cmovge	%rax, %r12
.L969:
	addq	$8, %rsp
	movq	%rbx, %rax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L981:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1008:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L972:
	.cfi_restore_state
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L975
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1013:
	movq	16(%r12), %rax
	movl	$1, %esi
.L978:
	testq	%rax, %rax
	je	.L976
	movq	%rax, %r12
.L975:
	movl	32(%r12), %ecx
	cmpl	%ecx, %r14d
	jl	.L1013
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L992:
	movq	%r12, %rbx
.L963:
	cmpq	%rbx, 32(%r13)
	je	.L994
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%r14), %esi
	movl	32(%rax), %edx
	movq	%rax, %rbx
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L976:
	movq	%r12, %rdx
	testb	%sil, %sil
	jne	.L974
.L979:
	xorl	%eax, %eax
	cmpl	%ecx, %r14d
	cmovg	%rax, %r12
	cmovle	%rax, %rdx
.L980:
	movq	%r12, %rax
	jmp	.L962
.L1012:
	movq	%r15, %r12
.L974:
	cmpq	%r12, %rbx
	je	.L998
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %r12
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L983:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L986
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1015:
	movq	16(%rbx), %rax
	movl	$1, %esi
.L989:
	testq	%rax, %rax
	je	.L987
	movq	%rax, %rbx
.L986:
	movl	32(%rbx), %ecx
	cmpl	%ecx, %r14d
	jl	.L1015
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L987:
	movq	%rbx, %rdx
	testb	%sil, %sil
	jne	.L985
.L990:
	xorl	%eax, %eax
	cmpl	%ecx, %r14d
	cmovg	%rax, %rbx
	cmovle	%rax, %rdx
.L991:
	movq	%rbx, %rax
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L994:
	movq	%rbx, %r12
	xorl	%ebx, %ebx
	jmp	.L969
.L1014:
	movq	%r15, %rbx
.L985:
	cmpq	%rbx, 32(%r13)
	je	.L1002
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %rbx
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L998:
	movq	%r12, %rdx
	xorl	%r12d, %r12d
	jmp	.L980
.L1002:
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	jmp	.L991
	.cfi_endproc
.LFE29367:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS1_, .-_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS1_
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsUntilLoopEiPKNS1_21BytecodeLivenessStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsUntilLoopEiPKNS1_21BytecodeLivenessStateE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsUntilLoopEiPKNS1_21BytecodeLivenessStateE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsUntilLoopEiPKNS1_21BytecodeLivenessStateE:
.LFB24740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$40, %rsp
	movl	144(%rdi), %esi
	movq	%rdx, -64(%rbp)
	movq	160(%rdi), %rdi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetLoopOffsetForEi@PLT
	cmpl	%ebx, 180(%r13)
	leaq	192(%r13), %rcx
	movl	%eax, %r14d
	movl	%ebx, %eax
	cmovge	180(%r13), %eax
	movq	%rcx, -72(%rbp)
	movl	%eax, -52(%rbp)
	cmpl	%eax, %r14d
	jle	.L1016
	leaq	208(%r13), %rbx
	.p2align 4,,10
	.p2align 3
.L1029:
	movq	216(%r13), %rax
	movq	%rbx, %r15
	testq	%rax, %rax
	jne	.L1019
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1037:
	movq	%rax, %r15
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1020
.L1019:
	cmpl	%r14d, 32(%rax)
	jge	.L1037
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1019
.L1020:
	cmpq	%r15, %rbx
	je	.L1018
	cmpl	%r14d, 32(%r15)
	jle	.L1023
.L1018:
	movq	192(%r13), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L1038
	leaq	48(%r12), %rax
	movq	%rax, 16(%rdi)
.L1025:
	movl	%r14d, 32(%r12)
	movq	-72(%rbp), %rdi
	movq	%r15, %rsi
	leaq	32(%r12), %rdx
	movq	$0, 40(%r12)
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS1_
	movq	%rax, %r15
	testq	%rdx, %rdx
	je	.L1023
	testq	%rax, %rax
	jne	.L1031
	cmpq	%rdx, %rbx
	jne	.L1039
.L1031:
	movl	$1, %edi
.L1027:
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r12, %r15
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 240(%r13)
.L1023:
	movq	40(%r15), %rax
	movq	160(%r13), %rdi
	movl	%r14d, %esi
	movq	24(%rax), %r12
	call	_ZNK2v88internal8compiler16BytecodeAnalysis14GetLoopInfoForEi@PLT
	movq	168(%r13), %rdi
	movq	-64(%rbp), %rcx
	movq	%rax, %r15
	leaq	8(%rax), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment18PrepareForLoopExitEPNS1_4NodeERKNS1_23BytecodeLoopAssignmentsEPKNS1_21BytecodeLivenessStateE
	movl	(%r15), %r14d
	cmpl	-52(%rbp), %r14d
	jg	.L1029
.L1016:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1039:
	.cfi_restore_state
	xorl	%edi, %edi
	movl	32(%rdx), %eax
	cmpl	%eax, 32(%r12)
	setl	%dil
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1038:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1025
	.cfi_endproc
.LFE24740:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsUntilLoopEiPKNS1_21BytecodeLivenessStateE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsUntilLoopEiPKNS1_21BytecodeLivenessStateE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi:
.LFB24737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	%esi, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	144(%rdi), %esi
	jg	.L1053
.L1041:
	movq	216(%rbx), %rax
	leaq	208(%rbx), %rdx
	testq	%rax, %rax
	je	.L1051
	movl	-68(%rbp), %ecx
	movq	%rdx, %r12
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1054:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1044
.L1043:
	cmpl	%ecx, 32(%rax)
	jge	.L1054
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1043
.L1044:
	cmpq	%r12, %rdx
	je	.L1042
	cmpl	32(%r12), %ecx
	jl	.L1042
	movq	40(%r12), %r13
	testq	%r13, %r13
	je	.L1055
.L1048:
	movq	160(%rbx), %rdi
	movl	-68(%rbp), %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movq	168(%rbx), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment5MergeEPS3_PKNS1_21BytecodeLivenessStateE
.L1049:
	movq	$0, 168(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1056
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1051:
	.cfi_restore_state
	movq	%rdx, %r12
	.p2align 4,,10
	.p2align 3
.L1042:
	leaq	-68(%rbp), %rax
	movq	%r12, %rsi
	leaq	-48(%rbp), %rcx
	leaq	192(%rbx), %rdi
	leaq	-49(%rbp), %r8
	movq	%rax, -48(%rbp)
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_
	movq	%rax, %r12
	movq	40(%r12), %r13
	testq	%r13, %r13
	jne	.L1048
.L1055:
	movq	16(%rbx), %rax
	movl	$1, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rax
	movq	%rax, 40(%r12)
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1053:
	movq	160(%rdi), %rdi
	movl	%esi, %r12d
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movq	160(%rbx), %rdi
	movl	%r12d, %esi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetLoopOffsetForEi@PLT
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsUntilLoopEiPKNS1_21BytecodeLivenessStateE
	jmp	.L1041
.L1056:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24737:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder9BuildJumpEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildJumpEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildJumpEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildJumpEv:
.LFB24742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	136(%rdi), %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi
	.cfi_endproc
.LFE24742:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildJumpEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildJumpEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0:
.LFB31505:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$120, %rsp
	movq	%rdx, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	%r12, %rdi
	movb	%al, -104(%rbp)
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movzbl	-104(%rbp), %r8d
	movl	28(%r12), %r10d
	movl	24(%r12), %r15d
	movq	-112(%rbp), %rcx
	movl	%eax, %r11d
	testb	%r8b, %r8b
	jne	.L1060
	testb	%al, %al
	jne	.L1060
	cmpl	$1, %r10d
	je	.L1061
	cmpl	$1, %r15d
	jne	.L1098
.L1061:
	movq	368(%rbx), %rax
	movl	$1, %r13d
	movq	%rax, -104(%rbp)
	movq	336(%rbx), %rax
	movq	%rax, -112(%rbp)
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1060:
	movq	368(%rbx), %rax
	movq	%rax, -104(%rbp)
	movq	336(%rbx), %rax
	movq	%rax, -112(%rbp)
	testb	%r8b, %r8b
	je	.L1086
	xorl	%r13d, %r13d
	testb	%r11b, %r11b
	setne	%r13b
	addl	$2, %r13d
.L1064:
	xorl	%eax, %eax
	cmpl	$1, %r10d
	sete	%al
	addl	%eax, %r13d
	xorl	%eax, %eax
	cmpl	$1, %r15d
	sete	%al
	addl	%eax, %r13d
	movl	404(%rbx), %eax
	cmpl	%eax, %r13d
	jg	.L1067
	movq	408(%rbx), %r14
.L1068:
	movq	(%rcx), %rax
	leaq	8(%r14), %r9
	movq	%rax, (%r14)
	testb	%r8b, %r8b
	jne	.L1099
.L1072:
	testb	%r11b, %r11b
	je	.L1075
	movq	16(%rbx), %r11
	leaq	8(%r9), %rax
	movq	%rax, -120(%rbp)
	movq	352(%r11), %rax
	testq	%rax, %rax
	je	.L1100
.L1076:
	movq	%rax, (%r9)
	movq	-120(%rbp), %r9
.L1075:
	cmpl	$1, %r15d
	jne	.L1077
	movq	168(%rbx), %rax
	addq	$8, %r9
	movq	32(%rax), %rax
	movq	%rax, -8(%r9)
.L1077:
	cmpl	$1, %r10d
	jne	.L1078
	movq	168(%rbx), %rax
	movq	24(%rax), %rax
	movq	%rax, (%r9)
.L1078:
	movq	16(%rbx), %rax
	movl	%r13d, %edx
	movq	%r12, %rsi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movl	40(%rax), %edx
	testl	%edx, %edx
	jle	.L1079
	movq	168(%rbx), %rax
	movq	%r12, 24(%rax)
	movq	(%r12), %rax
.L1079:
	cmpb	$0, 36(%rax)
	je	.L1080
	movq	168(%rbx), %rax
	movq	%r12, 32(%rax)
	movq	(%r12), %rax
.L1080:
	testb	$32, 18(%rax)
	jne	.L1082
	movq	-104(%rbp), %rcx
	cmpq	%rcx, -112(%rbp)
	je	.L1082
	movq	368(%rbx), %rax
	cmpq	376(%rbx), %rax
	je	.L1101
.L1084:
	movl	-8(%rax), %r9d
	movl	-4(%rax), %r10d
	leaq	-80(%rbp), %r14
	movq	168(%rbx), %rdi
	movl	%r9d, -120(%rbp)
	movl	%r10d, -104(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv
	movq	%rax, -112(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11IfExceptionEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	movq	168(%rbx), %rax
	movq	32(%rax), %rax
	movq	%r12, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-104(%rbp), %r10d
	movq	168(%rbx), %rdi
	movq	%rax, %r13
	movl	%r10d, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%rbx), %rdx
	movq	%r13, %rsi
	movq	%rax, -104(%rbp)
	movq	%r13, 24(%rdx)
	movq	168(%rbx), %rdx
	movq	%r13, 32(%rdx)
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.0
	movq	-104(%rbp), %rax
	movl	-120(%rbp), %r9d
	movq	%rbx, %rdi
	movq	168(%rbx), %rdx
	movl	%r9d, %esi
	movq	%rax, 16(%rdx)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi
	movq	-112(%rbp), %r11
	movq	%r11, 168(%rbx)
	movq	(%r12), %rax
	testb	$32, 18(%rax)
	je	.L1102
.L1082:
	cmpl	$1, %r15d
	je	.L1103
.L1059:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1104
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1102:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfSuccessEv@PLT
	movl	$1, %edx
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	168(%rbx), %rdx
	movq	%rax, 24(%rdx)
	cmpl	$1, %r15d
	jne	.L1059
.L1103:
	movq	(%r12), %rax
	testb	$16, 18(%rax)
	jne	.L1059
	movb	$1, 416(%rbx)
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	%r12, %rdi
	movl	%r10d, -128(%rbp)
	movb	%r11b, -120(%rbp)
	call	_ZN2v88internal8compiler18OperatorProperties17NeedsExactContextEPKNS1_8OperatorE@PLT
	movzbl	-120(%rbp), %r11d
	movl	-128(%rbp), %r10d
	testb	%al, %al
	je	.L1073
	movq	168(%rbx), %rax
	movq	16(%rax), %rax
.L1074:
	movq	%rax, 8(%r14)
	leaq	16(%r14), %r9
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1067:
	movq	8(%rbx), %rdi
	leal	64(%r13,%rax), %edx
	movslq	%edx, %rsi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	salq	$3, %rsi
	subq	%r14, %rax
	cmpq	%rax, %rsi
	ja	.L1105
	addq	%r14, %rsi
	movq	%rsi, 16(%rdi)
.L1070:
	movq	%r14, 408(%rbx)
	movl	%edx, 404(%rbx)
	jmp	.L1068
	.p2align 4,,10
	.p2align 3
.L1098:
	movq	16(%rbx), %rax
	movq	%r12, %rsi
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1073:
	movdqu	24(%rbx), %xmm0
	movq	16(%rbx), %rdi
	leaq	-96(%rbp), %rsi
	movl	%r10d, -128(%rbp)
	movb	%r11b, -120(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movl	-128(%rbp), %r10d
	movzbl	-120(%rbp), %r11d
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1101:
	movq	392(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1086:
	movl	$2, %r13d
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1100:
	movq	(%r11), %rax
	movq	8(%r11), %rdi
	movq	%r9, -152(%rbp)
	movl	%r10d, -140(%rbp)
	movq	%r11, -136(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	-128(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-136(%rbp), %r11
	movq	-152(%rbp), %r9
	movl	-140(%rbp), %r10d
	movq	%rax, 352(%r11)
	jmp	.L1076
	.p2align 4,,10
	.p2align 3
.L1105:
	movq	%rcx, -152(%rbp)
	movl	%edx, -140(%rbp)
	movl	%r10d, -136(%rbp)
	movb	%r11b, -128(%rbp)
	movb	%r8b, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-120(%rbp), %r8d
	movzbl	-128(%rbp), %r11d
	movl	-136(%rbp), %r10d
	movl	-140(%rbp), %edx
	movq	%rax, %r14
	movq	-152(%rbp), %rcx
	jmp	.L1070
.L1104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE31505:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0, .-_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	.section	.rodata._ZN2v88internal8compiler20BytecodeGraphBuilder18GetFunctionClosureEv.str1.1,"aMS",@progbits,1
.LC16:
	.string	"%closure"
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18GetFunctionClosureEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18GetFunctionClosureEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18GetFunctionClosureEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18GetFunctionClosureEv:
.LFB24478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	424(%rdi), %rax
	testq	%rax, %rax
	je	.L1110
.L1106:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1111
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1110:
	.cfi_restore_state
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movl	$-1, %esi
	leaq	.LC16(%rip), %rdx
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	leaq	-32(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, 424(%rbx)
	jmp	.L1106
.L1111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24478:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18GetFunctionClosureEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18GetFunctionClosureEv
	.section	.text._ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	.type	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE, @function
_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE:
.LFB24416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-36(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$32, %rsp
	movl	%esi, -36(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter8Register18is_current_contextEv@PLT
	testb	%al, %al
	je	.L1113
	movq	16(%rbx), %rax
.L1112:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1121
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1113:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter8Register19is_function_closureEv@PLT
	testb	%al, %al
	jne	.L1122
	movl	-36(%rbp), %eax
	movl	%eax, -28(%rbp)
	testl	%eax, %eax
	js	.L1123
	addl	88(%rbx), %eax
.L1117:
	movq	48(%rbx), %rcx
	movq	56(%rbx), %rdx
	cltq
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L1124
	movq	(%rcx,%rax,8), %rax
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1123:
	movl	12(%rbx), %esi
	leaq	-28(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1122:
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder18GetFunctionClosureEv
	jmp	.L1112
.L1121:
	call	__stack_chk_fail@PLT
.L1124:
	movq	%rax, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24416:
	.size	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE, .-_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb:
.LFB24767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movslq	%edx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$120, %rsp
	movq	%rcx, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	%r12, %rdi
	movb	%al, -104(%rbp)
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movzbl	-104(%rbp), %ecx
	movl	28(%r12), %r11d
	movzbl	%r14b, %r10d
	movl	24(%r12), %r15d
	movl	%eax, %r8d
	testb	%cl, %cl
	jne	.L1126
	testb	%al, %al
	jne	.L1126
	cmpl	$1, %r11d
	je	.L1127
	cmpl	$1, %r15d
	jne	.L1170
.L1127:
	movq	368(%rbx), %rax
	movl	%r13d, %r14d
	movq	%rax, -120(%rbp)
	movq	336(%rbx), %rax
	movq	%rax, -104(%rbp)
	jmp	.L1130
	.p2align 4,,10
	.p2align 3
.L1126:
	movq	368(%rbx), %rax
	leal	1(%r13), %r14d
	movq	%rax, -120(%rbp)
	movq	336(%rbx), %rax
	movq	%rax, -104(%rbp)
	testb	%cl, %cl
	je	.L1130
	leal	2(%r13), %eax
	testb	%r8b, %r8b
	cmovne	%eax, %r14d
.L1130:
	xorl	%eax, %eax
	cmpl	$1, %r11d
	sete	%al
	addl	%eax, %r14d
	xorl	%eax, %eax
	cmpl	$1, %r15d
	sete	%al
	addl	%eax, %r14d
	movl	404(%rbx), %eax
	cmpl	%eax, %r14d
	jg	.L1133
	movq	408(%rbx), %r9
.L1134:
	testl	%r13d, %r13d
	jle	.L1137
	movq	-112(%rbp), %rsi
	movq	%r9, %rdi
	leal	0(,%r13,8), %edx
	movl	%r10d, -152(%rbp)
	movslq	%edx, %rdx
	movl	%r11d, -140(%rbp)
	movb	%r8b, -136(%rbp)
	movb	%cl, -128(%rbp)
	call	memcpy@PLT
	movl	-152(%rbp), %r10d
	movl	-140(%rbp), %r11d
	movzbl	-136(%rbp), %r8d
	movzbl	-128(%rbp), %ecx
	movq	%rax, %r9
.L1137:
	leaq	(%r9,%r13,8), %r13
	testb	%cl, %cl
	jne	.L1171
.L1138:
	testb	%r8b, %r8b
	je	.L1141
	movq	16(%rbx), %rax
	leaq	8(%r13), %rsi
	movq	%rsi, -112(%rbp)
	movq	%rax, -128(%rbp)
	movq	352(%rax), %rax
	testq	%rax, %rax
	je	.L1172
.L1142:
	movq	%rax, 0(%r13)
	movq	-112(%rbp), %r13
.L1141:
	cmpl	$1, %r15d
	jne	.L1143
	movq	168(%rbx), %rax
	addq	$8, %r13
	movq	32(%rax), %rax
	movq	%rax, -8(%r13)
.L1143:
	cmpl	$1, %r11d
	jne	.L1144
	movq	168(%rbx), %rax
	movq	24(%rax), %rax
	movq	%rax, 0(%r13)
.L1144:
	movq	16(%rbx), %rax
	movl	%r14d, %edx
	movq	%r12, %rsi
	movl	%r10d, %r8d
	movq	%r9, %rcx
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movl	40(%rax), %edx
	testl	%edx, %edx
	jle	.L1145
	movq	168(%rbx), %rax
	movq	%r12, 24(%rax)
	movq	(%r12), %rax
.L1145:
	cmpb	$0, 36(%rax)
	je	.L1146
	movq	168(%rbx), %rax
	movq	%r12, 32(%rax)
	movq	(%r12), %rax
.L1146:
	testb	$32, 18(%rax)
	jne	.L1148
	movq	-104(%rbp), %rdx
	cmpq	%rdx, -120(%rbp)
	je	.L1148
	movq	368(%rbx), %rax
	cmpq	376(%rbx), %rax
	je	.L1173
.L1150:
	movl	-8(%rax), %r9d
	movl	-4(%rax), %r10d
	leaq	-80(%rbp), %r14
	movq	168(%rbx), %rdi
	movl	%r9d, -120(%rbp)
	movl	%r10d, -104(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv
	movq	%rax, -112(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11IfExceptionEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	movq	168(%rbx), %rax
	movq	32(%rax), %rax
	movq	%r12, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	-104(%rbp), %r10d
	movq	168(%rbx), %rdi
	movq	%rax, %r13
	movl	%r10d, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%rbx), %rdx
	movq	%r13, %rsi
	movq	%rax, -104(%rbp)
	movq	%r13, 24(%rdx)
	movq	168(%rbx), %rdx
	movq	%r13, 32(%rdx)
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.0
	movq	-104(%rbp), %rax
	movl	-120(%rbp), %r9d
	movq	%rbx, %rdi
	movq	168(%rbx), %rdx
	movl	%r9d, %esi
	movq	%rax, 16(%rdx)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi
	movq	-112(%rbp), %r11
	movq	%r11, 168(%rbx)
	movq	(%r12), %rax
	testb	$32, 18(%rax)
	je	.L1174
.L1148:
	cmpl	$1, %r15d
	je	.L1175
.L1125:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1176
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1174:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfSuccessEv@PLT
	movl	$1, %edx
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	(%rax), %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	168(%rbx), %rdx
	movq	%rax, 24(%rdx)
	cmpl	$1, %r15d
	jne	.L1125
.L1175:
	movq	(%r12), %rax
	testb	$16, 18(%rax)
	jne	.L1125
	movb	$1, 416(%rbx)
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1171:
	movq	%r12, %rdi
	movl	%r10d, -140(%rbp)
	movq	%r9, -136(%rbp)
	movl	%r11d, -128(%rbp)
	movb	%r8b, -112(%rbp)
	call	_ZN2v88internal8compiler18OperatorProperties17NeedsExactContextEPKNS1_8OperatorE@PLT
	movzbl	-112(%rbp), %r8d
	movl	-128(%rbp), %r11d
	testb	%al, %al
	movq	-136(%rbp), %r9
	movl	-140(%rbp), %r10d
	je	.L1139
	movq	168(%rbx), %rax
	movq	16(%rax), %rax
.L1140:
	movq	%rax, 0(%r13)
	addq	$8, %r13
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1133:
	movq	8(%rbx), %rdi
	leal	64(%r14,%rax), %edx
	movslq	%edx, %rsi
	movq	16(%rdi), %r9
	movq	24(%rdi), %rax
	salq	$3, %rsi
	subq	%r9, %rax
	cmpq	%rax, %rsi
	ja	.L1177
	addq	%r9, %rsi
	movq	%rsi, 16(%rdi)
.L1136:
	movq	%r9, 408(%rbx)
	movl	%edx, 404(%rbx)
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1170:
	movq	16(%rbx), %rax
	movq	-112(%rbp), %rcx
	movq	%r12, %rsi
	movl	%r10d, %r8d
	movl	%r13d, %edx
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1139:
	movdqu	24(%rbx), %xmm0
	movq	16(%rbx), %rdi
	leaq	-96(%rbp), %rsi
	movl	%r10d, -140(%rbp)
	movq	%r9, -136(%rbp)
	movl	%r11d, -128(%rbp)
	movb	%r8b, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movl	-140(%rbp), %r10d
	movl	-128(%rbp), %r11d
	movq	-136(%rbp), %r9
	movzbl	-112(%rbp), %r8d
	jmp	.L1140
	.p2align 4,,10
	.p2align 3
.L1173:
	movq	392(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1172:
	movq	-128(%rbp), %rax
	movl	%r10d, -144(%rbp)
	movq	%r9, -152(%rbp)
	movq	(%rax), %rsi
	movq	8(%rax), %rdi
	movl	%r11d, -140(%rbp)
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	-136(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-128(%rbp), %rsi
	movl	-144(%rbp), %r10d
	movq	-152(%rbp), %r9
	movl	-140(%rbp), %r11d
	movq	%rax, 352(%rsi)
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1177:
	movl	%r10d, -144(%rbp)
	movl	%edx, -152(%rbp)
	movl	%r11d, -140(%rbp)
	movb	%r8b, -136(%rbp)
	movb	%cl, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-128(%rbp), %ecx
	movzbl	-136(%rbp), %r8d
	movl	-140(%rbp), %r11d
	movl	-152(%rbp), %edx
	movq	%rax, %r9
	movl	-144(%rbp), %r10d
	jmp	.L1136
.L1176:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24767:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb, .-_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment14PrepareForLoopERKNS1_23BytecodeLoopAssignmentsEPKNS1_21BytecodeLivenessStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment14PrepareForLoopERKNS1_23BytecodeLoopAssignmentsEPKNS1_21BytecodeLivenessStateE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment14PrepareForLoopERKNS1_23BytecodeLoopAssignmentsEPKNS1_21BytecodeLivenessStateE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment14PrepareForLoopERKNS1_23BytecodeLoopAssignmentsEPKNS1_21BytecodeLivenessStateE:
.LFB24424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4LoopEi@PLT
	movl	$1, %r8d
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	xorl	%r12d, %r12d
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	32(%rbx), %rdx
	movq	(%rbx), %rdi
	movl	$1, %esi
	movq	%rax, %rcx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder12NewEffectPhiEiPNS1_4NodeES4_
	movq	16(%rbx), %rsi
	movq	(%rbx), %rdi
	movq	%r14, %rdx
	movq	%rax, 32(%rbx)
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_.constprop.0
	movl	12(%rbx), %edx
	movq	%rax, 16(%rbx)
	testl	%edx, %edx
	jle	.L1185
	.p2align 4,,10
	.p2align 3
.L1179:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler23BytecodeLoopAssignments17ContainsParameterEi@PLT
	testb	%al, %al
	jne	.L1207
	addq	$1, %r12
	cmpl	%r12d, 12(%rbx)
	jg	.L1179
.L1185:
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jle	.L1197
	xorl	%r12d, %r12d
	testq	%r15, %r15
	je	.L1186
	.p2align 4,,10
	.p2align 3
.L1193:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler23BytecodeLoopAssignments13ContainsLocalEi@PLT
	testb	%al, %al
	je	.L1188
	cmpl	$1, 12(%r15)
	movq	16(%r15), %rax
	je	.L1192
	movl	%r12d, %edx
	sarl	$6, %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
.L1192:
	btq	%r12, %rax
	jnc	.L1188
	movl	88(%rbx), %eax
	movq	48(%rbx), %rdx
	movq	(%rbx), %rdi
	addl	%r12d, %eax
	cltq
	leaq	(%rdx,%rax,8), %rcx
	movq	%r14, %rdx
	movq	(%rcx), %rsi
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_.constprop.0
	movq	-112(%rbp), %rcx
	movq	%rax, (%rcx)
.L1188:
	addl	$1, %r12d
	cmpl	8(%rbx), %r12d
	jl	.L1193
.L1197:
	movq	80(%rbx), %rsi
	movq	(%rbx), %rdi
	testq	%rsi, %rsi
	je	.L1182
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_.constprop.0
	movq	(%rbx), %rdi
	movq	%rax, 80(%rbx)
.L1182:
	movq	16(%rdi), %rax
	movq	-120(%rbp), %xmm0
	movq	%r14, %xmm2
	movq	8(%rax), %rdi
	punpcklqdq	%xmm2, %xmm0
	movq	(%rax), %r12
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TerminateEv@PLT
	movdqa	-112(%rbp), %xmm0
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%rbx), %rdi
	movq	%rax, -88(%rbp)
	movq	448(%rdi), %rsi
	cmpq	456(%rdi), %rsi
	je	.L1198
	movq	%rax, (%rsi)
	addq	$8, 448(%rdi)
.L1178:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1208
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1195:
	.cfi_restore_state
	addl	$1, %r12d
	cmpl	8(%rbx), %r12d
	jge	.L1197
.L1186:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler23BytecodeLoopAssignments13ContainsLocalEi@PLT
	testb	%al, %al
	je	.L1195
	movl	88(%rbx), %eax
	movq	48(%rbx), %rdx
	movq	(%rbx), %rdi
	addl	%r12d, %eax
	addl	$1, %r12d
	cltq
	leaq	(%rdx,%rax,8), %r15
	movq	%r14, %rdx
	movq	(%r15), %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_.constprop.0
	movq	%rax, (%r15)
	cmpl	8(%rbx), %r12d
	jl	.L1186
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1207:
	movq	48(%rbx), %rax
	movq	(%rbx), %rdi
	movq	%r14, %rdx
	leaq	(%rax,%r12,8), %rcx
	addq	$1, %r12
	movq	(%rcx), %rsi
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder6NewPhiEiPNS1_4NodeES4_.constprop.0
	movq	-112(%rbp), %rcx
	movq	%rax, (%rcx)
	cmpl	%r12d, 12(%rbx)
	jg	.L1179
	jmp	.L1185
	.p2align 4,,10
	.p2align 3
.L1198:
	leaq	-88(%rbp), %rdx
	addq	$432, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1178
.L1208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24424:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment14PrepareForLoopERKNS1_23BytecodeLoopAssignmentsEPKNS1_21BytecodeLivenessStateE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment14PrepareForLoopERKNS1_23BytecodeLoopAssignmentsEPKNS1_21BytecodeLivenessStateE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder27BuildLoadNativeContextFieldEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder27BuildLoadNativeContextFieldEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder27BuildLoadNativeContextFieldEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder27BuildLoadNativeContextFieldEi:
.LFB24479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rdx
	movl	$1, %ecx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11LoadContextEmmb@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movdqu	24(%rbx), %xmm0
	movq	16(%rbx), %rdi
	leaq	-48(%rbp), %rsi
	movq	%rax, %r12
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1212
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1212:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24479:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder27BuildLoadNativeContextFieldEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder27BuildLoadNativeContextFieldEi
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder15BuildLoadGlobalENS1_7NameRefEjNS0_10TypeofModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildLoadGlobalENS1_7NameRefEjNS0_10TypeofModeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildLoadGlobalENS1_7NameRefEjNS0_10TypeofModeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildLoadGlobalENS1_7NameRefEjNS0_10TypeofModeE:
.LFB24526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$64, %rsp
	movq	%rsi, -96(%rbp)
	movq	56(%rdi), %rsi
	movq	%rdx, -88(%rbp)
	movq	64(%rdi), %rdx
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	16(%r12), %rax
	leaq	-96(%rbp), %rdi
	movq	368(%rax), %r15
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	leaq	-72(%rbp), %rsi
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder10LoadGlobalERKNS0_6HandleINS0_4NameEEERKNS1_14FeedbackSourceENS0_10TypeofModeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1216
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1216:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24526:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildLoadGlobalENS1_7NameRefEjNS0_10TypeofModeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildLoadGlobalENS1_7NameRefEjNS0_10TypeofModeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder26VisitLdaCurrentContextSlotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitLdaCurrentContextSlotEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitLdaCurrentContextSlotEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitLdaCurrentContextSlotEv:
.LFB24535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	addq	$136, %rdi
	movq	368(%rax), %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	%eax, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11LoadContextEmmb@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1220
	movq	%rax, (%rcx,%rsi,8)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1220:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24535:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitLdaCurrentContextSlotEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitLdaCurrentContextSlotEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder35VisitLdaImmutableCurrentContextSlotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder35VisitLdaImmutableCurrentContextSlotEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder35VisitLdaImmutableCurrentContextSlotEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder35VisitLdaImmutableCurrentContextSlotEv:
.LFB24536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	addq	$136, %rdi
	movq	368(%rax), %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movl	$1, %ecx
	xorl	%esi, %esi
	movl	%eax, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11LoadContextEmmb@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1224
	movq	%rax, (%rcx,%rsi,8)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1224:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24536:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder35VisitLdaImmutableCurrentContextSlotEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder35VisitLdaImmutableCurrentContextSlotEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCreateClosureEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCreateClosureEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCreateClosureEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCreateClosureEv:
.LFB24562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	136(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r12, %rdi
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler21SharedFunctionInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetFlagOperandEi@PLT
	movl	$67, %esi
	movl	%eax, -100(%rbp)
	movq	16(%rbx), %rax
	movq	360(%rax), %rdi
	movq	368(%rax), %r15
	addq	$41184, %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	leaq	56(%rbx), %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler17FeedbackVectorRef22GetClosureFeedbackCellEi@PLT
	leaq	-80(%rbp), %rdi
	movq	%rdx, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal8compiler15FeedbackCellRef6objectEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef6objectEv@PLT
	movl	-100(%rbp), %r8d
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%rax, %rsi
	movq	%r15, %rdi
	andl	$1, %r8d
	call	_ZN2v88internal8compiler17JSOperatorBuilder13CreateClosureENS0_6HandleINS0_18SharedFunctionInfoEEENS3_INS0_12FeedbackCellEEENS3_INS0_4CodeEEENS0_14AllocationTypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1229
	movq	%rax, (%rcx,%rsi,8)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1230
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1229:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24562:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCreateClosureEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCreateClosureEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateBlockContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateBlockContextEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateBlockContextEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateBlockContextEv:
.LFB24563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$136, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-120(%rdi), %rax
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	368(%rax), %r13
	call	_ZNK2v88internal8compiler12ScopeInfoRef6objectEv@PLT
	leaq	-72(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder18CreateBlockContextERKNS0_6HandleINS0_9ScopeInfoEEE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1235
	movq	%rax, (%rcx,%rsi,8)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1236
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1235:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1236:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24563:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateBlockContextEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateBlockContextEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCreateFunctionContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCreateFunctionContextEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCreateFunctionContextEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCreateFunctionContextEv:
.LFB24564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	136(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r12, %rdi
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	movq	16(%rbx), %rax
	movq	368(%rax), %r14
	call	_ZNK2v88internal8compiler12ScopeInfoRef6objectEv@PLT
	movl	$2, %ecx
	movl	%r12d, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder21CreateFunctionContextENS0_6HandleINS0_9ScopeInfoEEEiNS0_9ScopeTypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1241
	movq	%rax, (%rcx,%rsi,8)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1242
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1241:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1242:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24564:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCreateFunctionContextEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCreateFunctionContextEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder22VisitCreateEvalContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitCreateEvalContextEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitCreateEvalContextEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitCreateEvalContextEv:
.LFB24565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	136(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r12, %rdi
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	movq	16(%rbx), %rax
	movq	368(%rax), %r14
	call	_ZNK2v88internal8compiler12ScopeInfoRef6objectEv@PLT
	movl	$1, %ecx
	movl	%r12d, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder21CreateFunctionContextENS0_6HandleINS0_9ScopeInfoEEEiNS0_9ScopeTypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1247
	movq	%rax, (%rcx,%rsi,8)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1248
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1247:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1248:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24565:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitCreateEvalContextEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitCreateEvalContextEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateEmptyArrayLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateEmptyArrayLiteralEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateEmptyArrayLiteralEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateEmptyArrayLiteralEv:
.LFB24577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$136, %rdi
	leaq	-48(%rbp), %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	64(%rbx), %rdx
	movq	56(%rbx), %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	16(%rbx), %rax
	movq	%r12, %rsi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder23CreateEmptyLiteralArrayERKNS1_14FeedbackSourceE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1253
	movq	%rax, (%rcx,%rsi,8)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1254
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1253:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1254:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24577:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateEmptyArrayLiteralEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateEmptyArrayLiteralEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder22VisitStaModuleVariableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitStaModuleVariableEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitStaModuleVariableEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitStaModuleVariableEv:
.LFB24559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	136(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetImmediateOperandEi@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movl	$1, %ecx
	movl	$2, %edx
	movl	%eax, %esi
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11LoadContextEmmb@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, -72(%rbp)
	movq	168(%r12), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1259
	movq	16(%r12), %rax
	movq	(%rcx,%rsi,8), %r14
	movl	%r13d, %esi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11StoreModuleEi@PLT
	movq	%r14, %xmm1
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	-72(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	movl	$2, %edx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1260
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1259:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24559:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitStaModuleVariableEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitStaModuleVariableEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder13VisitTestNullEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitTestNullEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitTestNullEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitTestNullEv:
.LFB24673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1266
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	movq	(%rcx,%rsi,8), %r12
	call	_ZN2v88internal8compiler7JSGraph12NullConstantEv@PLT
	movq	%rax, -56(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movhps	-56(%rbp), %xmm0
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1266
	movq	%rax, (%rcx,%rsi,8)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1267
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1266:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1267:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24673:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitTestNullEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitTestNullEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18VisitTestUndefinedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitTestUndefinedEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitTestUndefinedEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitTestUndefinedEv:
.LFB24674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1273
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	movq	(%rcx,%rsi,8), %r12
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rax, -56(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movhps	-56(%rbp), %xmm0
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1273
	movq	%rax, (%rcx,%rsi,8)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1274
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1273:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1274:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24674:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitTestUndefinedEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitTestUndefinedEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder20ProcessCallArgumentsEPKNS1_8OperatorEPKPNS1_4NodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder20ProcessCallArgumentsEPKNS1_8OperatorEPKPNS1_4NodeEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder20ProcessCallArgumentsEPKNS1_8OperatorEPKPNS1_4NodeEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder20ProcessCallArgumentsEPKNS1_8OperatorEPKPNS1_4NodeEi:
.LFB24587:
	.cfi_startproc
	endbr64
	movq	%rdx, %r9
	xorl	%r8d, %r8d
	movl	%ecx, %edx
	movq	%r9, %rcx
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	.cfi_endproc
.LFE24587:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder20ProcessCallArgumentsEPKNS1_8OperatorEPKPNS1_4NodeEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder20ProcessCallArgumentsEPKNS1_8OperatorEPKPNS1_4NodeEi
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder25ProcessConstructArgumentsEPKNS1_8OperatorEPKPNS1_4NodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder25ProcessConstructArgumentsEPKNS1_8OperatorEPKPNS1_4NodeEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder25ProcessConstructArgumentsEPKNS1_8OperatorEPKPNS1_4NodeEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder25ProcessConstructArgumentsEPKNS1_8OperatorEPKPNS1_4NodeEi:
.LFB31450:
	.cfi_startproc
	endbr64
	movq	%rdx, %r9
	xorl	%r8d, %r8d
	movl	%ecx, %edx
	movq	%r9, %rcx
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	.cfi_endproc
.LFE31450:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder25ProcessConstructArgumentsEPKNS1_8OperatorEPKPNS1_4NodeEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder25ProcessConstructArgumentsEPKNS1_8OperatorEPKPNS1_4NodeEi
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder15VisitPopContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitPopContextEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitPopContextEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitPopContextEv:
.LFB24561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	168(%rdi), %r12
	leaq	136(%rdi), %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%rax, %r8
	movq	168(%rbx), %rax
	movq	%r8, 16(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24561:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitPopContextEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitPopContextEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder9VisitLdarEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitLdarEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitLdarEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitLdarEv:
.LFB24523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	168(%rdi), %r12
	addq	$136, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1282
	movq	%rax, (%rcx,%rsi,8)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1282:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24523:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitLdarEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitLdarEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder8VisitMovEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitMovEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitMovEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitMovEv:
.LFB24525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	136(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	168(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movl	$1, %esi
	movq	%r13, %rdi
	movq	168(%r12), %r12
	movq	%rax, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, -44(%rbp)
	testl	%eax, %eax
	js	.L1289
	addl	88(%r12), %eax
.L1285:
	movq	48(%r12), %rcx
	movq	56(%r12), %rdx
	cltq
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L1290
	movq	%rbx, (%rcx,%rax,8)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1291
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1289:
	.cfi_restore_state
	movl	12(%r12), %esi
	leaq	-44(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	jmp	.L1285
.L1291:
	call	__stack_chk_fail@PLT
.L1290:
	movq	%rax, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24525:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitMovEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitMovEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder19VisitLdaContextSlotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitLdaContextSlotEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitLdaContextSlotEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitLdaContextSlotEv:
.LFB24533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	136(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movq	%r13, %rdi
	movq	368(%rax), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movl	%r12d, %edx
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11LoadContextEmmb@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %r14
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_@PLT
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1295
	movq	%r12, (%rcx,%rsi,8)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1295:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24533:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitLdaContextSlotEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitLdaContextSlotEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder28VisitLdaImmutableContextSlotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitLdaImmutableContextSlotEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitLdaImmutableContextSlotEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitLdaImmutableContextSlotEv:
.LFB24534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	136(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movq	%r13, %rdi
	movq	368(%rax), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movl	%r12d, %edx
	movq	%r14, %rdi
	movl	$1, %ecx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11LoadContextEmmb@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %r14
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_@PLT
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1299
	movq	%r12, (%rcx,%rsi,8)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1299:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24534:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitLdaImmutableContextSlotEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitLdaImmutableContextSlotEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder23VisitTestReferenceEqualEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitTestReferenceEqualEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitTestReferenceEqualEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitTestReferenceEqualEv:
.LFB24669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$136, %rdi
	subq	$48, %rsp
	movq	32(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%rax, -56(%rbp)
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1305
	movq	16(%rbx), %rax
	movq	(%rcx,%rsi,8), %r12
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%r12, %xmm1
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	-56(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1305
	movq	%rax, (%rcx,%rsi,8)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1306
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1305:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1306:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24669:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitTestReferenceEqualEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitTestReferenceEqualEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE:
.LFB24430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movl	12(%rdi), %ecx
	movq	48(%rdi), %r9
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	56(%rdi), %rax
	cmpl	8(%rdi), %ecx
	je	.L1333
	cmpq	%r9, %rax
	je	.L1311
	leaq	72(%rdi), %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment17UpdateStateValuesEPPNS1_4NodeES6_i
.L1310:
	movq	48(%rbx), %rdi
	movq	56(%rbx), %rdx
	testq	%r12, %r12
	movl	$0, %eax
	leaq	8(%r12), %rcx
	movslq	88(%rbx), %rsi
	cmove	%rax, %rcx
	subq	%rdi, %rdx
	movl	8(%rbx), %eax
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1332
	movslq	%eax, %rdx
	movq	(%rbx), %rax
	leaq	(%rdi,%rsi,8), %rsi
	xorl	%r8d, %r8d
	leaq	464(%rax), %rdi
	call	_ZN2v88internal8compiler16StateValuesCache16GetNodeForValuesEPPNS1_4NodeEmPKNS0_9BitVectorEi@PLT
	movq	%rax, %r15
	testq	%r12, %r12
	je	.L1314
	movl	8(%r12), %edx
	cmpl	$1, 12(%r12)
	movq	16(%r12), %rcx
	leal	-1(%rdx), %eax
	je	.L1316
	addl	$62, %edx
	testl	%eax, %eax
	cmovns	%eax, %edx
	sarl	$6, %edx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
.L1316:
	cltd
	shrl	$26, %edx
	addl	%edx, %eax
	andl	$63, %eax
	subl	%edx, %eax
	btq	%rax, %rcx
	jc	.L1314
.L1317:
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler7JSGraph20OptimizedOutConstantEv@PLT
	movq	%rax, -120(%rbp)
.L1319:
	movq	(%rbx), %rax
	movq	%r13, %rdx
	movl	%r14d, %esi
	movq	120(%rax), %rcx
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10FrameStateENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_22FrameStateFunctionInfoE@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r13
	movq	16(%rdi), %rax
	movq	(%rax), %r14
	movq	8(%r14), %r12
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder18GetFunctionClosureEv
	movq	72(%rbx), %rsi
	movq	16(%rbx), %rcx
	xorl	%r8d, %r8d
	movl	$6, %edx
	movq	%r14, %rdi
	movq	%r15, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	-120(%rbp), %rsi
	movq	%rcx, -88(%rbp)
	leaq	-112(%rbp), %rcx
	movq	%rsi, -96(%rbp)
	movq	%r13, %rsi
	movq	%rax, -80(%rbp)
	movq	%r12, -72(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1334
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1314:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L1317
	movq	48(%rbx), %rax
	movq	56(%rbx), %rdx
	movslq	92(%rbx), %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1332
	movq	(%rax,%rsi,8), %rax
	movq	%rax, -120(%rbp)
	jmp	.L1319
	.p2align 4,,10
	.p2align 3
.L1333:
	cmpq	%r9, %rax
	je	.L1311
	movq	(%rdi), %rax
	movslq	%ecx, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r9, %rsi
	leaq	464(%rax), %rdi
	call	_ZN2v88internal8compiler16StateValuesCache16GetNodeForValuesEPPNS1_4NodeEmPKNS0_9BitVectorEi@PLT
	movq	%rax, 72(%rbx)
	jmp	.L1310
.L1332:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1311:
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1334:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24430:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder17PrepareFrameStateEPNS1_4NodeENS1_23OutputFrameStateCombineE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder17PrepareFrameStateEPNS1_4NodeENS1_23OutputFrameStateCombineE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder17PrepareFrameStateEPNS1_4NodeENS1_23OutputFrameStateCombineE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder17PrepareFrameStateEPNS1_4NodeENS1_23OutputFrameStateCombineE:
.LFB24486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L1338
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1338:
	.cfi_restore_state
	movl	144(%rbx), %r14d
	movq	160(%rbx), %rdi
	movl	%r14d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%rbx), %rdi
	movl	%r14d, %esi
	movq	%r13, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE24486:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder17PrepareFrameStateEPNS1_4NodeENS1_23OutputFrameStateCombineE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder17PrepareFrameStateEPNS1_4NodeENS1_23OutputFrameStateCombineE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0:
.LFB31438:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movb	$0, 416(%rdi)
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10CheckpointEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movl	144(%rbx), %r13d
	movq	160(%rbx), %rdi
	movq	%rax, %r12
	movl	%r13d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movq	168(%rbx), %rdi
	movl	%r13d, %esi
	movq	$-1, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE31438:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0, .-_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv:
.LFB24485:
	.cfi_startproc
	endbr64
	cmpb	$0, 416(%rdi)
	jne	.L1343
	ret
	.p2align 4,,10
	.p2align 3
.L1343:
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	.cfi_endproc
.LFE24485:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18VisitForInContinueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitForInContinueEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitForInContinueEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitForInContinueEv:
.LFB24724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1349
.L1345:
	leaq	136(%rbx), %r12
	movq	168(%rbx), %r13
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%rbx), %r13
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	xorl	%esi, %esi
	movq	%rax, -80(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeNumberLessThanENS1_19NumberOperationHintE@PLT
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	-72(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-80(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1350
	movq	%rax, (%rcx,%rsi,8)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1351
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1349:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1345
.L1351:
	call	__stack_chk_fail@PLT
.L1350:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24724:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitForInContinueEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitForInContinueEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder15VisitStackCheckEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitStackCheckEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitStackCheckEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitStackCheckEv:
.LFB24707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 416(%rdi)
	jne	.L1356
.L1353:
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10StackCheckEv@PLT
	movq	%rbx, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %r12
	movq	168(%rbx), %rax
	movq	(%r12), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L1357
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1356:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1357:
	movl	144(%rbx), %r13d
	movq	160(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%rbx), %rdi
	movl	%r13d, %esi
	movq	$-1, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE24707:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitStackCheckEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitStackCheckEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder13VisitDebuggerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitDebuggerEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitDebuggerEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitDebuggerEv:
.LFB24711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 416(%rdi)
	jne	.L1362
.L1359:
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8DebuggerEv@PLT
	movq	%rbx, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %r12
	movq	168(%rbx), %rax
	movq	(%r12), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L1363
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1362:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1363:
	movl	144(%rbx), %r13d
	movq	160(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%rbx), %rdi
	movl	%r13d, %esi
	movq	$-1, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE24711:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitDebuggerEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitDebuggerEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCollectTypeProfileEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCollectTypeProfileEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCollectTypeProfileEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCollectTypeProfileEv:
.LFB24532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1373
.L1365:
	xorl	%esi, %esi
	leaq	136(%rbx), %rdi
	movq	16(%rbx), %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetImmediateOperandEi@PLT
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -72(%rbp)
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1374
	movq	16(%rbx), %rdi
	movq	(%rcx,%rsi,8), %r13
	leaq	56(%rbx), %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movl	$208, %esi
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movq	%r13, %xmm1
	movq	%rbx, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	movl	$3, %edx
	movq	%r12, -48(%rbp)
	movq	-72(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %r12
	movq	168(%rbx), %rax
	movq	(%r12), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L1375
.L1364:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1376
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1373:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1375:
	movl	144(%rbx), %r13d
	movq	160(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%rbx), %rdi
	movl	%r13d, %esi
	movq	$-1, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	jmp	.L1364
.L1376:
	call	__stack_chk_fail@PLT
.L1374:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24532:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCollectTypeProfileEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCollectTypeProfileEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment16RecordAfterStateEPNS1_4NodeENS3_24FrameStateAttachmentModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment16RecordAfterStateEPNS1_4NodeENS3_24FrameStateAttachmentModeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment16RecordAfterStateEPNS1_4NodeENS3_24FrameStateAttachmentModeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment16RecordAfterStateEPNS1_4NodeENS3_24FrameStateAttachmentModeE:
.LFB24421:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	je	.L1387
	ret
	.p2align 4,,10
	.p2align 3
.L1387:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rbx
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L1388
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1388:
	.cfi_restore_state
	movl	144(%rbx), %r13d
	movq	160(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%rbx), %rdi
	movl	%r13d, %esi
	movq	$-1, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	.cfi_restore 3
	movq	%rax, %rsi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE24421:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment16RecordAfterStateEPNS1_4NodeENS3_24FrameStateAttachmentModeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment16RecordAfterStateEPNS1_4NodeENS3_24FrameStateAttachmentModeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder31VisitStaNamedPropertyNoFeedbackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitStaNamedPropertyNoFeedbackEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitStaNamedPropertyNoFeedbackEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitStaNamedPropertyNoFeedbackEv:
.LFB24555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1398
.L1390:
	movq	168(%rbx), %r12
	movq	48(%r12), %rax
	movq	56(%r12), %rdx
	movslq	92(%r12), %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1399
	leaq	136(%rbx), %r15
	movq	(%rax,%rsi,8), %r13
	xorl	%esi, %esi
	leaq	-112(%rbp), %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%r15, %rdi
	movl	$1, %esi
	movq	%rax, -120(%rbp)
	movq	16(%rbx), %rax
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetFlagOperandEi@PLT
	movq	%r14, %rdi
	movq	$0, -96(%rbp)
	movl	%eax, %r12d
	movq	16(%rbx), %rax
	movl	$-1, -88(%rbp)
	movq	368(%rax), %r15
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	movl	%r12d, %esi
	leaq	-96(%rbp), %rcx
	andl	$1, %esi
	movq	%rax, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10StoreNamedENS0_12LanguageModeENS0_6HandleINS0_4NameEEERKNS1_14FeedbackSourceE@PLT
	movq	%r13, %xmm1
	movq	%rbx, %rdi
	xorl	%r8d, %r8d
	movq	-120(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %r12
	movq	168(%rbx), %rax
	movq	(%r12), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L1400
.L1389:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1401
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1398:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1390
	.p2align 4,,10
	.p2align 3
.L1400:
	movl	144(%rbx), %r13d
	movq	160(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%rbx), %rdi
	movl	%r13d, %esi
	movq	$-1, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	jmp	.L1389
.L1401:
	call	__stack_chk_fail@PLT
.L1399:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24555:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitStaNamedPropertyNoFeedbackEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitStaNamedPropertyNoFeedbackEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder22VisitStaInArrayLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitStaInArrayLiteralEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitStaInArrayLiteralEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitStaInArrayLiteralEv:
.LFB24530:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1417
.L1403:
	movq	168(%rbx), %r12
	movq	48(%r12), %rax
	movq	56(%r12), %rdx
	movslq	92(%r12), %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1418
	leaq	136(%rbx), %r13
	movq	(%rax,%rsi,8), %r14
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%rbx), %r15
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$2, %esi
	movq	%rax, %r15
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	leaq	-112(%rbp), %r10
	movq	64(%rbx), %rdx
	movq	56(%rbx), %rsi
	movl	%eax, %ecx
	movq	%r10, %rdi
	movq	%r10, -152(%rbp)
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	-112(%rbp), %rax
	leaq	-128(%rbp), %rsi
	movq	%rax, -128(%rbp)
	movl	-104(%rbp), %eax
	movl	%eax, -120(%rbp)
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder19StoreInArrayLiteralERKNS1_14FeedbackSourceE@PLT
	movl	-120(%rbp), %edx
	subq	$8, %rsp
	leaq	80(%rbx), %rsi
	movq	%rax, %r13
	movq	168(%rbx), %rax
	movq	%r14, %r9
	movq	%r15, %r8
	pushq	%rdx
	movq	-152(%rbp), %r10
	movq	%r13, %rdx
	movq	%r12, %rcx
	pushq	24(%rax)
	pushq	32(%rax)
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_S7_NS0_12FeedbackSlotE@PLT
	movl	-112(%rbp), %eax
	movq	-88(%rbp), %rdx
	addq	$32, %rsp
	cmpl	$2, %eax
	je	.L1419
	cmpl	$1, %eax
	je	.L1420
	movq	%r12, %xmm0
	movq	%r15, %xmm1
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	punpcklqdq	%xmm1, %xmm0
	leaq	-80(%rbp), %rcx
	movl	$3, %edx
	movq	%rbx, %rdi
	movq	%r14, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %r12
.L1411:
	movq	168(%rbx), %rax
	movq	(%r12), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L1421
.L1402:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1422
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1420:
	.cfi_restore_state
	movq	168(%rbx), %rax
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %r12
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1417:
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1403
	.p2align 4,,10
	.p2align 3
.L1421:
	movl	144(%rbx), %r13d
	movq	160(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%rbx), %rdi
	movl	%r13d, %esi
	movq	$-1, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	jmp	.L1402
	.p2align 4,,10
	.p2align 3
.L1419:
	movq	%rdx, -136(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L1406
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L1407:
	movq	$0, 168(%rbx)
	jmp	.L1402
	.p2align 4,,10
	.p2align 3
.L1406:
	leaq	-136(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1407
.L1422:
	call	__stack_chk_fail@PLT
.L1418:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24530:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitStaInArrayLiteralEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitStaInArrayLiteralEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1:
.LFB31503:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %r13
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L1430
.L1424:
	movq	48(%rbx), %rax
	movq	56(%rbx), %rdx
	movslq	92(%rbx), %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1431
	movq	%r12, (%rax,%rsi,8)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1430:
	.cfi_restore_state
	movl	144(%r13), %r14d
	movq	160(%r13), %rdi
	movl	%r14d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%r13), %rdi
	movl	%r14d, %esi
	xorl	%edx, %edx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	jmp	.L1424
.L1431:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE31503:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder14VisitLdaGlobalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitLdaGlobalEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitLdaGlobalEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitLdaGlobalEv:
.LFB24527:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1436
.L1433:
	movq	16(%rbx), %rax
	leaq	136(%rbx), %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	leaq	-96(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	-64(%rbp), %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	56(%rbx), %rsi
	movq	64(%rbx), %rdx
	movq	%r12, %rdi
	movdqa	-96(%rbp), %xmm0
	movl	%eax, %ecx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	16(%rbx), %rax
	leaq	-80(%rbp), %rdi
	movq	368(%rax), %r13
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	leaq	-104(%rbp), %rsi
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder10LoadGlobalERKNS0_6HandleINS0_4NameEEERKNS1_14FeedbackSourceENS0_10TypeofModeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1437
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1436:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1433
.L1437:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24527:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitLdaGlobalEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitLdaGlobalEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder26VisitLdaGlobalInsideTypeofEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitLdaGlobalInsideTypeofEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitLdaGlobalInsideTypeofEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitLdaGlobalInsideTypeofEv:
.LFB24528:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1442
.L1439:
	movq	16(%rbx), %rax
	leaq	136(%rbx), %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	leaq	-96(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	-64(%rbp), %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	56(%rbx), %rsi
	movq	64(%rbx), %rdx
	movq	%r12, %rdi
	movdqa	-96(%rbp), %xmm0
	movl	%eax, %ecx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	16(%rbx), %rax
	leaq	-80(%rbp), %rdi
	movq	368(%rax), %r13
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	xorl	%ecx, %ecx
	leaq	-104(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder10LoadGlobalERKNS0_6HandleINS0_4NameEEERKNS1_14FeedbackSourceENS0_10TypeofModeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1443
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1442:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1439
.L1443:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24528:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitLdaGlobalInsideTypeofEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitLdaGlobalInsideTypeofEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18VisitStaLookupSlotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitStaLookupSlotEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitStaLookupSlotEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitStaLookupSlotEv:
.LFB24549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1452
.L1445:
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1453
	movq	16(%rbx), %r14
	leaq	136(%rbx), %r13
	movq	(%rcx,%rsi,8), %r12
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	-96(%rbp), %r15
	movq	360(%r14), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetFlagOperandEi@PLT
	movq	16(%rbx), %rdx
	movl	$320, %esi
	movq	368(%rdx), %rdi
	testb	$1, %al
	je	.L1454
.L1447:
	movq	-112(%rbp), %xmm0
	movq	%r12, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movdqa	-112(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1455
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1454:
	.cfi_restore_state
	xorl	%esi, %esi
	testb	$2, %al
	setne	%sil
	addl	$318, %esi
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1452:
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1445
.L1453:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1455:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24549:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitStaLookupSlotEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitStaLookupSlotEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder21VisitLdaKeyedPropertyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitLdaKeyedPropertyEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitLdaKeyedPropertyEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitLdaKeyedPropertyEv:
.LFB24552:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1467
.L1457:
	movq	168(%rbx), %r12
	movq	48(%r12), %rax
	movq	56(%r12), %rdx
	movslq	92(%r12), %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1468
	leaq	136(%rbx), %r13
	movq	(%rax,%rsi,8), %r14
	xorl	%esi, %esi
	leaq	-112(%rbp), %r15
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	64(%rbx), %rdx
	movq	56(%rbx), %rsi
	movq	%r15, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	-112(%rbp), %rax
	leaq	-128(%rbp), %rsi
	movq	%rax, -128(%rbp)
	movl	-104(%rbp), %eax
	movl	%eax, -120(%rbp)
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder12LoadPropertyERKNS1_14FeedbackSourceE@PLT
	movl	-120(%rbp), %edx
	leaq	80(%rbx), %rsi
	movq	%r12, %rcx
	movq	%rax, %r13
	movq	168(%rbx), %rax
	movq	%r14, %r8
	movq	%r15, %rdi
	movq	32(%rax), %r9
	pushq	%rdx
	movq	%r13, %rdx
	pushq	24(%rax)
	call	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE@PLT
	movl	-112(%rbp), %eax
	popq	%rcx
	movq	-88(%rbp), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L1469
	cmpl	$1, %eax
	je	.L1470
	movq	%r14, %xmm1
	movq	%r12, %xmm0
	movq	%r13, %rsi
	xorl	%r8d, %r8d
	punpcklqdq	%xmm1, %xmm0
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movq	%rbx, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %rsi
.L1464:
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
.L1456:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1471
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1470:
	.cfi_restore_state
	movq	168(%rbx), %rax
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1464
	.p2align 4,,10
	.p2align 3
.L1467:
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1469:
	movq	%rdx, -136(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L1460
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L1461:
	movq	$0, 168(%rbx)
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1460:
	leaq	-136(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1461
.L1471:
	call	__stack_chk_fail@PLT
.L1468:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24552:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitLdaKeyedPropertyEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitLdaKeyedPropertyEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE:
.LFB24620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1483
.L1473:
	leaq	136(%rbx), %r14
	movq	168(%rbx), %r12
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%rax, %r12
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1484
	movq	(%rcx,%rsi,8), %r15
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	leaq	80(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdx
	movl	%eax, %r8d
	movq	168(%rbx), %rax
	leaq	-112(%rbp), %rdi
	movq	32(%rax), %r9
	pushq	%r8
	movq	%r15, %r8
	pushq	24(%rax)
	call	_ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE@PLT
	movl	-112(%rbp), %eax
	popq	%rcx
	movq	-88(%rbp), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L1485
	cmpl	$1, %eax
	je	.L1486
	movq	%r15, %xmm1
	movq	%r12, %xmm0
	movq	%r13, %rsi
	xorl	%r8d, %r8d
	punpcklqdq	%xmm1, %xmm0
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movq	%rbx, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %rsi
.L1480:
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
.L1472:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1487
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1486:
	.cfi_restore_state
	movq	168(%rbx), %rax
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1483:
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1473
	.p2align 4,,10
	.p2align 3
.L1485:
	movq	%rdx, -120(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L1476
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L1477:
	movq	$0, 168(%rbx)
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1476:
	leaq	-120(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1477
.L1487:
	call	__stack_chk_fail@PLT
.L1484:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24620:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder8VisitAddEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitAddEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitAddEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitAddEv:
.LFB24630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$136, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-120(%rdi), %rax
	movq	368(%rax), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	64(%r12), %rdx
	movq	56(%r12), %rsi
	movq	%r13, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker29GetFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler17JSOperatorBuilder3AddENS0_19BinaryOperationHintE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1491
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1491:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24630:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitAddEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitAddEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder8VisitSubEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitSubEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitSubEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitSubEv:
.LFB24631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8SubtractEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	.cfi_endproc
.LFE24631:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitSubEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitSubEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder8VisitMulEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitMulEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitMulEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitMulEv:
.LFB24632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8MultiplyEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	.cfi_endproc
.LFE24632:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitMulEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitMulEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder8VisitDivEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitDivEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitDivEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitDivEv:
.LFB24633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder6DivideEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	.cfi_endproc
.LFE24633:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitDivEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitDivEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder8VisitModEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitModEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitModEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitModEv:
.LFB24634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder7ModulusEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	.cfi_endproc
.LFE24634:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitModEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitModEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder8VisitExpEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitExpEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitExpEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitExpEv:
.LFB24635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder12ExponentiateEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	.cfi_endproc
.LFE24635:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitExpEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitExpEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder14VisitBitwiseOrEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitBitwiseOrEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitBitwiseOrEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitBitwiseOrEv:
.LFB24636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder9BitwiseOrEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	.cfi_endproc
.LFE24636:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitBitwiseOrEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitBitwiseOrEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseXorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseXorEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseXorEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseXorEv:
.LFB24637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10BitwiseXorEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	.cfi_endproc
.LFE24637:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseXorEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseXorEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseAndEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseAndEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseAndEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseAndEv:
.LFB24638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10BitwiseAndEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	.cfi_endproc
.LFE24638:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseAndEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseAndEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder14VisitShiftLeftEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitShiftLeftEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitShiftLeftEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitShiftLeftEv:
.LFB24639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder9ShiftLeftEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	.cfi_endproc
.LFE24639:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitShiftLeftEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitShiftLeftEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder15VisitShiftRightEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitShiftRightEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitShiftRightEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitShiftRightEv:
.LFB24640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10ShiftRightEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	.cfi_endproc
.LFE24640:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitShiftRightEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitShiftRightEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder22VisitShiftRightLogicalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitShiftRightLogicalEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitShiftRightLogicalEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitShiftRightLogicalEv:
.LFB24641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder17ShiftRightLogicalEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	.cfi_endproc
.LFE24641:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitShiftRightLogicalEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitShiftRightLogicalEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder19VisitTestInstanceOfEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitTestInstanceOfEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitTestInstanceOfEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitTestInstanceOfEv:
.LFB24671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$136, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	64(%r12), %rdx
	movq	56(%r12), %rsi
	movq	%r13, %rdi
	movl	%eax, %ecx
	movq	16(%r12), %rax
	movq	368(%rax), %r14
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10InstanceOfERKNS1_14FeedbackSourceE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1517
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1517:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24671:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitTestInstanceOfEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitTestInstanceOfEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder27VisitTestGreaterThanOrEqualEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitTestGreaterThanOrEqualEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitTestGreaterThanOrEqualEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitTestGreaterThanOrEqualEv:
.LFB24668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$136, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-120(%rdi), %rax
	movq	368(%rax), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	64(%r12), %rdx
	movq	56(%r12), %rsi
	movq	%r13, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker30GetFeedbackForCompareOperationERKNS1_14FeedbackSourceE@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler17JSOperatorBuilder18GreaterThanOrEqualENS0_20CompareOperationHintE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1521
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1521:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24668:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitTestGreaterThanOrEqualEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitTestGreaterThanOrEqualEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder24VisitTestLessThanOrEqualEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitTestLessThanOrEqualEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitTestLessThanOrEqualEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitTestLessThanOrEqualEv:
.LFB24667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$136, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-120(%rdi), %rax
	movq	368(%rax), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	64(%r12), %rdx
	movq	56(%r12), %rsi
	movq	%r13, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker30GetFeedbackForCompareOperationERKNS1_14FeedbackSourceE@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler17JSOperatorBuilder15LessThanOrEqualENS0_20CompareOperationHintE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1525
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1525:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24667:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitTestLessThanOrEqualEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitTestLessThanOrEqualEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder20VisitTestGreaterThanEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitTestGreaterThanEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitTestGreaterThanEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitTestGreaterThanEv:
.LFB24666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$136, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-120(%rdi), %rax
	movq	368(%rax), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	64(%r12), %rdx
	movq	56(%r12), %rsi
	movq	%r13, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker30GetFeedbackForCompareOperationERKNS1_14FeedbackSourceE@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11GreaterThanENS0_20CompareOperationHintE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1529
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1529:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24666:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitTestGreaterThanEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitTestGreaterThanEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder17VisitTestLessThanEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitTestLessThanEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitTestLessThanEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitTestLessThanEv:
.LFB24665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$136, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-120(%rdi), %rax
	movq	368(%rax), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	64(%r12), %rdx
	movq	56(%r12), %rsi
	movq	%r13, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker30GetFeedbackForCompareOperationERKNS1_14FeedbackSourceE@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8LessThanENS0_20CompareOperationHintE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1533
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1533:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24665:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitTestLessThanEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitTestLessThanEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder20VisitTestEqualStrictEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitTestEqualStrictEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitTestEqualStrictEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitTestEqualStrictEv:
.LFB24664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$136, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-120(%rdi), %rax
	movq	368(%rax), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	64(%r12), %rdx
	movq	56(%r12), %rsi
	movq	%r13, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker30GetFeedbackForCompareOperationERKNS1_14FeedbackSourceE@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11StrictEqualENS0_20CompareOperationHintE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1537
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1537:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24664:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitTestEqualStrictEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitTestEqualStrictEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder14VisitTestEqualEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitTestEqualEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitTestEqualEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitTestEqualEv:
.LFB24663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$136, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-120(%rdi), %rax
	movq	368(%rax), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	64(%r12), %rdx
	movq	56(%r12), %rsi
	movq	%r13, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker30GetFeedbackForCompareOperationERKNS1_14FeedbackSourceE@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler17JSOperatorBuilder5EqualENS0_20CompareOperationHintE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1541
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1541:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24663:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitTestEqualEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitTestEqualEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder14BuildCompareOpEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildCompareOpEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildCompareOpEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildCompareOpEPKNS1_8OperatorE:
.LFB31452:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
	.cfi_endproc
.LFE31452:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildCompareOpEPKNS1_8OperatorE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildCompareOpEPKNS1_8OperatorE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE:
.LFB24642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1554
.L1544:
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1555
	leaq	136(%rbx), %r15
	movq	(%rcx,%rsi,8), %r14
	xorl	%esi, %esi
	movq	16(%rbx), %r13
	movq	%r15, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetImmediateOperandEi@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	leaq	80(%rbx), %rsi
	movq	%r14, %rcx
	movq	%r12, %rdx
	movl	%eax, %r8d
	movq	168(%rbx), %rax
	leaq	-112(%rbp), %rdi
	movq	32(%rax), %r9
	pushq	%r8
	movq	%r13, %r8
	pushq	24(%rax)
	call	_ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE@PLT
	movl	-112(%rbp), %eax
	popq	%rcx
	movq	-88(%rbp), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L1556
	cmpl	$1, %eax
	je	.L1557
	movq	%r13, %xmm1
	movq	%r14, %xmm0
	movq	%r12, %rsi
	xorl	%r8d, %r8d
	punpcklqdq	%xmm1, %xmm0
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movq	%rbx, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %rsi
.L1551:
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
.L1543:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1558
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1557:
	.cfi_restore_state
	movq	168(%rbx), %rax
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1551
	.p2align 4,,10
	.p2align 3
.L1554:
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1556:
	movq	%rdx, -120(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L1547
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L1548:
	movq	$0, 168(%rbx)
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1547:
	leaq	-120(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1548
.L1558:
	call	__stack_chk_fail@PLT
.L1555:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24642:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11VisitAddSmiEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitAddSmiEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitAddSmiEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitAddSmiEv:
.LFB24643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$136, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-120(%rdi), %rax
	movq	368(%rax), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	64(%r12), %rdx
	movq	56(%r12), %rsi
	movq	%r13, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker29GetFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler17JSOperatorBuilder3AddENS0_19BinaryOperationHintE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1562
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1562:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24643:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitAddSmiEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitAddSmiEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11VisitSubSmiEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitSubSmiEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitSubSmiEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitSubSmiEv:
.LFB24644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8SubtractEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
	.cfi_endproc
.LFE24644:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitSubSmiEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitSubSmiEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11VisitMulSmiEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitMulSmiEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitMulSmiEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitMulSmiEv:
.LFB24645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8MultiplyEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
	.cfi_endproc
.LFE24645:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitMulSmiEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitMulSmiEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11VisitDivSmiEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitDivSmiEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitDivSmiEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitDivSmiEv:
.LFB24646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder6DivideEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
	.cfi_endproc
.LFE24646:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitDivSmiEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitDivSmiEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11VisitModSmiEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitModSmiEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitModSmiEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitModSmiEv:
.LFB24647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder7ModulusEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
	.cfi_endproc
.LFE24647:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitModSmiEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitModSmiEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11VisitExpSmiEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitExpSmiEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitExpSmiEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitExpSmiEv:
.LFB24648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder12ExponentiateEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
	.cfi_endproc
.LFE24648:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitExpSmiEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitExpSmiEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder17VisitBitwiseOrSmiEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitBitwiseOrSmiEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitBitwiseOrSmiEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitBitwiseOrSmiEv:
.LFB24649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder9BitwiseOrEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
	.cfi_endproc
.LFE24649:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitBitwiseOrSmiEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitBitwiseOrSmiEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18VisitBitwiseXorSmiEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitBitwiseXorSmiEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitBitwiseXorSmiEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitBitwiseXorSmiEv:
.LFB24650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10BitwiseXorEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
	.cfi_endproc
.LFE24650:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitBitwiseXorSmiEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitBitwiseXorSmiEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18VisitBitwiseAndSmiEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitBitwiseAndSmiEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitBitwiseAndSmiEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitBitwiseAndSmiEv:
.LFB24651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10BitwiseAndEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
	.cfi_endproc
.LFE24651:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitBitwiseAndSmiEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitBitwiseAndSmiEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder17VisitShiftLeftSmiEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitShiftLeftSmiEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitShiftLeftSmiEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitShiftLeftSmiEv:
.LFB24652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder9ShiftLeftEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
	.cfi_endproc
.LFE24652:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitShiftLeftSmiEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitShiftLeftSmiEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18VisitShiftRightSmiEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitShiftRightSmiEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitShiftRightSmiEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitShiftRightSmiEv:
.LFB24653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10ShiftRightEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
	.cfi_endproc
.LFE24653:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitShiftRightSmiEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitShiftRightSmiEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder25VisitShiftRightLogicalSmiEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitShiftRightLogicalSmiEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitShiftRightLogicalSmiEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitShiftRightLogicalSmiEv:
.LFB24654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder17ShiftRightLogicalEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
	.cfi_endproc
.LFE24654:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitShiftRightLogicalSmiEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitShiftRightLogicalSmiEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11BuildDeleteENS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildDeleteENS0_12LanguageModeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildDeleteENS0_12LanguageModeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildDeleteENS0_12LanguageModeE:
.LFB24658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1590
.L1586:
	movq	168(%rbx), %r13
	movq	48(%r13), %rax
	movq	56(%r13), %rdx
	movslq	92(%r13), %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1591
	movq	(%rax,%rsi,8), %r14
	leaq	136(%rbx), %rdi
	xorl	%esi, %esi
	movzbl	%r12b, %r12d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	pxor	%xmm0, %xmm0
	movq	16(%rbx), %rdi
	cvtsi2sdl	%r12d, %xmm0
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder14DeletePropertyEv@PLT
	movq	%r14, %xmm1
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	movl	$3, %edx
	movq	%r12, -48(%rbp)
	movq	-72(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1592
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1590:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1586
.L1592:
	call	__stack_chk_fail@PLT
.L1591:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24658:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildDeleteENS0_12LanguageModeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildDeleteENS0_12LanguageModeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder25VisitDeletePropertyStrictEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitDeletePropertyStrictEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitDeletePropertyStrictEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitDeletePropertyStrictEv:
.LFB24659:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildDeleteENS0_12LanguageModeE
	.cfi_endproc
.LFE24659:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitDeletePropertyStrictEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitDeletePropertyStrictEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder25VisitDeletePropertySloppyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitDeletePropertySloppyEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitDeletePropertySloppyEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitDeletePropertySloppyEv:
.LFB24660:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildDeleteENS0_12LanguageModeE
	.cfi_endproc
.LFE24660:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitDeletePropertySloppyEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitDeletePropertySloppyEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder9BuildCallENS0_19ConvertReceiverModeEPKPNS1_4NodeEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildCallENS0_19ConvertReceiverModeEPKPNS1_4NodeEmi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildCallENS0_19ConvertReceiverModeEPKPNS1_4NodeEmi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildCallENS0_19ConvertReceiverModeEPKPNS1_4NodeEmi:
.LFB24589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movl	%esi, -140(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1613
.L1596:
	movq	56(%rbx), %rsi
	leaq	-96(%rbp), %r12
	movq	64(%rbx), %rdx
	movl	%r15d, %ecx
	movq	%r12, %rdi
	leaq	56(%rbx), %r14
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movss	72(%rbx), %xmm0
	movq	-96(%rbp), %rax
	ucomiss	%xmm0, %xmm0
	movq	%rax, -112(%rbp)
	movl	-88(%rbp), %eax
	movl	%eax, -104(%rbp)
	jp	.L1609
	movq	56(%rbx), %rsi
	movq	8(%r14), %rdx
	movl	%r15d, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker18GetFeedbackForCallERKNS1_14FeedbackSourceE@PLT
	pxor	%xmm0, %xmm0
	movl	(%rax), %r8d
	movq	%rax, %rdi
	testl	%r8d, %r8d
	jne	.L1614
.L1597:
	movq	56(%rbx), %rsi
	movq	8(%r14), %rdx
	movl	%r15d, %ecx
	movq	%r12, %rdi
	movss	%xmm0, -124(%rbp)
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker18GetFeedbackForCallERKNS1_14FeedbackSourceE@PLT
	movl	$1, %r9d
	movl	(%rax), %edi
	testl	%edi, %edi
	jne	.L1615
.L1601:
	movq	16(%rbx), %rax
	leaq	-112(%rbp), %rcx
	leaq	-124(%rbp), %rdx
	movq	%r13, %rsi
	movl	-140(%rbp), %r8d
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder4CallEmRKNS1_13CallFrequencyERKNS1_14FeedbackSourceENS0_19ConvertReceiverModeENS0_15SpeculationModeE@PLT
	movl	-104(%rbp), %edx
	leaq	80(%rbx), %rsi
	movl	%r13d, %r8d
	movq	%rax, %r15
	movq	168(%rbx), %rax
	movq	-136(%rbp), %rcx
	movq	%r12, %rdi
	movq	32(%rax), %r9
	pushq	%rdx
	movq	%r15, %rdx
	pushq	24(%rax)
	call	_ZNK2v88internal8compiler18JSTypeHintLowering19ReduceCallOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE@PLT
	movl	-96(%rbp), %eax
	popq	%rcx
	movq	-72(%rbp), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L1616
	cmpl	$1, %eax
	je	.L1617
	movq	-136(%rbp), %rcx
	movq	%r15, %rsi
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %rsi
.L1607:
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
.L1595:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1618
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1614:
	.cfi_restore_state
	call	_ZNK2v88internal8compiler17ProcessedFeedback6AsCallEv@PLT
	pxor	%xmm0, %xmm0
	movss	32(%rax), %xmm1
	ucomiss	%xmm0, %xmm1
	jnp	.L1619
.L1611:
	mulss	72(%rbx), %xmm1
	movaps	%xmm1, %xmm0
	jmp	.L1597
	.p2align 4,,10
	.p2align 3
.L1617:
	movq	168(%rbx), %rax
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1613:
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1615:
	movq	%rax, %rdi
	call	_ZNK2v88internal8compiler17ProcessedFeedback6AsCallEv@PLT
	movl	36(%rax), %r9d
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1616:
	movq	%rdx, -120(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L1603
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L1604:
	movq	$0, 168(%rbx)
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L1609:
	movss	.LC7(%rip), %xmm0
	jmp	.L1597
	.p2align 4,,10
	.p2align 3
.L1603:
	leaq	-120(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1604
.L1619:
	jne	.L1611
	jmp	.L1597
.L1618:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24589:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildCallENS0_19ConvertReceiverModeEPKPNS1_4NodeEmi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildCallENS0_19ConvertReceiverModeEPKPNS1_4NodeEmi
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16BuildCallVarArgsENS0_19ConvertReceiverModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildCallVarArgsENS0_19ConvertReceiverModeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildCallVarArgsENS0_19ConvertReceiverModeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildCallVarArgsENS0_19ConvertReceiverModeE:
.LFB24591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	136(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	xorl	%esi, %esi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	168(%rdi), %r15
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, %esi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -56(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r13, %rdi
	movl	$2, %esi
	movl	%eax, %ebx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movq	%r13, %rdi
	movl	$3, %esi
	movl	%eax, %r15d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movl	%eax, %r13d
	testl	%r12d, %r12d
	jne	.L1621
	movq	16(%r14), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rax, -64(%rbp)
.L1622:
	movq	8(%r14), %rdi
	leal	2(%r15), %ecx
	movslq	%ecx, %rcx
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L1629
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L1624:
	movq	-56(%rbp), %xmm0
	movhps	-64(%rbp), %xmm0
	movups	%xmm0, (%rdx)
	testl	%r15d, %r15d
	jle	.L1625
	leaq	16(%rdx), %r8
	addl	%ebx, %r15d
	.p2align 4,,10
	.p2align 3
.L1626:
	movq	168(%r14), %rdi
	movl	%ebx, %esi
	movq	%r8, -72(%rbp)
	addl	$1, %ebx
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	-72(%rbp), %r8
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rax, (%r8)
	addq	$8, %r8
	cmpl	%ebx, %r15d
	jne	.L1626
.L1625:
	addq	$40, %rsp
	movl	%r13d, %r8d
	movl	%r12d, %esi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildCallENS0_19ConvertReceiverModeEPKPNS1_4NodeEmi
	.p2align 4,,10
	.p2align 3
.L1621:
	.cfi_restore_state
	movq	168(%r14), %rdi
	movl	%ebx, %esi
	subl	$1, %r15d
	addl	$1, %ebx
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%rax, -64(%rbp)
	jmp	.L1622
	.p2align 4,,10
	.p2align 3
.L1629:
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L1624
	.cfi_endproc
.LFE24591:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildCallVarArgsENS0_19ConvertReceiverModeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildCallVarArgsENS0_19ConvertReceiverModeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder20VisitCallAnyReceiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitCallAnyReceiverEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitCallAnyReceiverEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitCallAnyReceiverEv:
.LFB24592:
	.cfi_startproc
	endbr64
	movl	$2, %esi
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildCallVarArgsENS0_19ConvertReceiverModeE
	.cfi_endproc
.LFE24592:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitCallAnyReceiverEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitCallAnyReceiverEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder17VisitCallPropertyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitCallPropertyEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitCallPropertyEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitCallPropertyEv:
.LFB24594:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildCallVarArgsENS0_19ConvertReceiverModeE
	.cfi_endproc
.LFE24594:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitCallPropertyEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitCallPropertyEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCallUndefinedReceiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCallUndefinedReceiverEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCallUndefinedReceiverEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCallUndefinedReceiverEv:
.LFB24598:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildCallVarArgsENS0_19ConvertReceiverModeE
	.cfi_endproc
.LFE24598:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCallUndefinedReceiverEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCallUndefinedReceiverEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty0Ev
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty0Ev, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty0Ev:
.LFB24595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	136(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	168(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%r12), %r14
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movl	$2, %esi
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	-72(%rbp), %xmm0
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movl	%eax, %r8d
	movl	$2, %ecx
	movl	$1, %esi
	movhps	-80(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildCallENS0_19ConvertReceiverModeEPKPNS1_4NodeEmi
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1636
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1636:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24595:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty0Ev, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty0Ev
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty1Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty1Ev
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty1Ev, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty1Ev:
.LFB24596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	136(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	168(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%r12), %r14
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%r12), %r14
	movl	$2, %esi
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movl	$3, %esi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -48(%rbp)
	movq	-72(%rbp), %xmm0
	movl	%eax, %r8d
	movl	$3, %ecx
	movl	$1, %esi
	movhps	-80(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildCallENS0_19ConvertReceiverModeEPKPNS1_4NodeEmi
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1640
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1640:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24596:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty1Ev, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty1Ev
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty2Ev
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty2Ev, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty2Ev:
.LFB24597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	136(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$88, %rsp
	movq	168(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%r12), %r14
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%r12), %r14
	movl	$2, %esi
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%r12), %r14
	movl	$3, %esi
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movl	$4, %esi
	movq	%r13, %rdi
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	-88(%rbp), %xmm0
	leaq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	%eax, %r8d
	movl	$4, %ecx
	movl	$1, %esi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	-104(%rbp), %xmm0
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildCallENS0_19ConvertReceiverModeEPKPNS1_4NodeEmi
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1644
	addq	$88, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1644:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24597:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty2Ev, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty2Ev
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver0Ev
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver0Ev, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver0Ev:
.LFB24599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	136(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	168(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	16(%r12), %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	-72(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, %r8d
	leaq	-64(%rbp), %rdx
	movl	$2, %ecx
	movhps	-80(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildCallENS0_19ConvertReceiverModeEPKPNS1_4NodeEmi
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1648
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1648:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24599:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver0Ev, .-_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver0Ev
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver1Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver1Ev
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver1Ev, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver1Ev:
.LFB24600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	136(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	168(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	16(%r12), %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	168(%r12), %r14
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movl	$2, %esi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	-72(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, %r8d
	leaq	-64(%rbp), %rdx
	movl	$3, %ecx
	movq	%rbx, -48(%rbp)
	movhps	-80(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildCallENS0_19ConvertReceiverModeEPKPNS1_4NodeEmi
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1652
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1652:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24600:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver1Ev, .-_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver1Ev
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver2Ev
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver2Ev, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver2Ev:
.LFB24601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	136(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$88, %rsp
	movq	168(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	16(%r12), %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	168(%r12), %r14
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%r12), %r14
	movl	$2, %esi
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movl	$3, %esi
	movq	%r13, %rdi
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	-88(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, %r8d
	leaq	-80(%rbp), %rdx
	movl	$4, %ecx
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	-104(%rbp), %xmm0
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder9BuildCallENS0_19ConvertReceiverModeEPKPNS1_4NodeEmi
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1656
	addq	$88, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1656:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24601:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver2Ev, .-_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver2Ev
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder19VisitCallNoFeedbackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitCallNoFeedbackEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitCallNoFeedbackEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitCallNoFeedbackEv:
.LFB24593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1666
.L1658:
	leaq	136(%rbx), %r12
	movq	168(%rbx), %r13
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movl	%eax, %r13d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	leaq	-80(%rbp), %rcx
	leaq	-84(%rbp), %rdx
	movl	$1, %r9d
	leal	1(%rax), %r14d
	movl	%eax, %r15d
	movq	16(%rbx), %rax
	movl	$2, %r8d
	movslq	%r14d, %r12
	movq	$0, -80(%rbp)
	movq	368(%rax), %rdi
	movq	%r12, %rsi
	movl	$-1, -72(%rbp)
	movl	$0xbf800000, -84(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder4CallEmRKNS1_13CallFrequencyERKNS1_14FeedbackSourceENS0_19ConvertReceiverModeENS0_15SpeculationModeE@PLT
	movq	168(%rbx), %rdi
	movl	%r13d, %esi
	movq	%rax, -112(%rbp)
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	8(%rbx), %rdi
	leal	1(%r13), %edx
	leaq	0(,%r12,8), %rsi
	movq	%rax, -120(%rbp)
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1667
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L1660:
	movq	-104(%rbp), %xmm0
	movhps	-120(%rbp), %xmm0
	movups	%xmm0, (%rcx)
	cmpl	$1, %r15d
	jle	.L1661
	movl	%edx, %r12d
	addl	%r13d, %r15d
	leaq	16(%rcx), %rdx
	.p2align 4,,10
	.p2align 3
.L1662:
	movq	168(%rbx), %rdi
	movl	%r12d, %esi
	movq	%rcx, -120(%rbp)
	addl	$1, %r12d
	movq	%rdx, -104(%rbp)
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	-104(%rbp), %rdx
	movq	-120(%rbp), %rcx
	movq	%rax, (%rdx)
	addq	$8, %rdx
	cmpl	%r12d, %r15d
	jne	.L1662
.L1661:
	movq	-112(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	%r14d, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1668
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1666:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1667:
	movl	%edx, -124(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-124(%rbp), %edx
	movq	%rax, %rcx
	jmp	.L1660
.L1668:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24593:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitCallNoFeedbackEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitCallNoFeedbackEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder19VisitCallWithSpreadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitCallWithSpreadEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitCallWithSpreadEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitCallWithSpreadEv:
.LFB24602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1690
.L1670:
	leaq	136(%r15), %r13
	movq	168(%r15), %r12
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	168(%r15), %rdi
	movl	%eax, %esi
	movl	%eax, -132(%rbp)
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movl	$2, %esi
	movq	%r13, %rdi
	movq	%rax, -152(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movq	8(%r15), %rdi
	movl	-132(%rbp), %ecx
	movl	%eax, %ebx
	leal	-1(%rax), %eax
	movl	%eax, -132(%rbp)
	movq	16(%rdi), %r12
	leal	1(%rbx), %r14d
	leal	1(%rcx), %edx
	movq	24(%rdi), %rax
	movslq	%r14d, %rsi
	salq	$3, %rsi
	subq	%r12, %rax
	cmpq	%rax, %rsi
	ja	.L1691
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L1672:
	movq	-144(%rbp), %xmm0
	movl	-132(%rbp), %r9d
	movhps	-152(%rbp), %xmm0
	movups	%xmm0, (%r12)
	testl	%r9d, %r9d
	jle	.L1673
	leaq	16(%r12), %r8
	addl	%ecx, %ebx
	.p2align 4,,10
	.p2align 3
.L1674:
	movq	168(%r15), %rdi
	movl	%edx, %esi
	movq	%r8, -152(%rbp)
	movl	%edx, -144(%rbp)
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movl	-144(%rbp), %edx
	movq	-152(%rbp), %r8
	addl	$1, %edx
	movq	%rax, (%r8)
	addq	$8, %r8
	cmpl	%ebx, %edx
	jne	.L1674
.L1673:
	movq	%r13, %rdi
	movl	$3, %esi
	leaq	-96(%rbp), %r13
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	64(%r15), %rdx
	movq	56(%r15), %rsi
	movq	%r13, %rdi
	movl	%eax, %ebx
	leaq	56(%r15), %rax
	movl	%ebx, %ecx
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movss	72(%r15), %xmm0
	movq	-96(%rbp), %rdx
	ucomiss	%xmm0, %xmm0
	movq	%rdx, -112(%rbp)
	movl	-88(%rbp), %edx
	movl	%edx, -104(%rbp)
	jp	.L1686
	movq	-144(%rbp), %rax
	movq	56(%r15), %rsi
	movl	%ebx, %ecx
	movq	%r13, %rdi
	movq	8(%rax), %rdx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%r15), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker18GetFeedbackForCallERKNS1_14FeedbackSourceE@PLT
	pxor	%xmm0, %xmm0
	movl	(%rax), %r8d
	movq	%rax, %rdi
	testl	%r8d, %r8d
	jne	.L1692
.L1675:
	movq	16(%r15), %rax
	leaq	-112(%rbp), %rcx
	leaq	-124(%rbp), %rdx
	movl	%r14d, %esi
	movl	$1, %r8d
	movss	%xmm0, -124(%rbp)
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder14CallWithSpreadEjRKNS1_13CallFrequencyERKNS1_14FeedbackSourceENS0_15SpeculationModeE@PLT
	movl	-104(%rbp), %edx
	leaq	80(%r15), %rsi
	movq	%r12, %rcx
	movq	%rax, %rbx
	movq	168(%r15), %rax
	movl	-132(%rbp), %r8d
	movq	%r13, %rdi
	movq	32(%rax), %r9
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	24(%rax)
	call	_ZNK2v88internal8compiler18JSTypeHintLowering19ReduceCallOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE@PLT
	movl	-96(%rbp), %eax
	popq	%rcx
	movq	-72(%rbp), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L1693
	cmpl	$1, %eax
	je	.L1694
	movq	%rbx, %rsi
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movl	%r14d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %rsi
.L1684:
	movq	168(%r15), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
.L1669:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1695
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1692:
	.cfi_restore_state
	call	_ZNK2v88internal8compiler17ProcessedFeedback6AsCallEv@PLT
	pxor	%xmm0, %xmm0
	movss	32(%rax), %xmm1
	ucomiss	%xmm0, %xmm1
	jnp	.L1696
.L1687:
	mulss	72(%r15), %xmm1
	movaps	%xmm1, %xmm0
	jmp	.L1675
	.p2align 4,,10
	.p2align 3
.L1694:
	movq	168(%r15), %rax
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	%rcx, 32(%rax)
	movq	168(%r15), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1684
	.p2align 4,,10
	.p2align 3
.L1690:
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1670
	.p2align 4,,10
	.p2align 3
.L1693:
	movq	%rdx, -120(%rbp)
	movq	448(%r15), %rsi
	cmpq	456(%r15), %rsi
	je	.L1680
	movq	%rdx, (%rsi)
	addq	$8, 448(%r15)
.L1681:
	movq	$0, 168(%r15)
	jmp	.L1669
	.p2align 4,,10
	.p2align 3
.L1686:
	movss	.LC7(%rip), %xmm0
	jmp	.L1675
	.p2align 4,,10
	.p2align 3
.L1691:
	movl	%ecx, -156(%rbp)
	movl	%edx, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-136(%rbp), %edx
	movl	-156(%rbp), %ecx
	movq	%rax, %r12
	jmp	.L1672
	.p2align 4,,10
	.p2align 3
.L1680:
	leaq	-120(%rbp), %rdx
	leaq	432(%r15), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1681
.L1696:
	jne	.L1687
	jmp	.L1675
.L1695:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24602:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitCallWithSpreadEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitCallWithSpreadEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16VisitCallRuntimeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitCallRuntimeEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitCallRuntimeEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitCallRuntimeEv:
.LFB24605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1712
.L1698:
	leaq	136(%rbx), %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetRuntimeIdOperandEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movl	%eax, %r14d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movl	%eax, %r13d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movl	%r14d, %esi
	movl	%eax, %edx
	movq	16(%rbx), %rax
	movq	%rdx, %r12
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdEm@PLT
	movq	8(%rbx), %rdi
	movslq	%r12d, %rsi
	movq	%rax, %r15
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L1713
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L1700:
	movq	%rdx, %rcx
	testl	%r12d, %r12d
	jle	.L1701
	leal	(%r12,%r13), %eax
	movl	%eax, -84(%rbp)
	.p2align 4,,10
	.p2align 3
.L1702:
	movq	168(%rbx), %rdi
	movl	%r13d, %esi
	movq	%rcx, -80(%rbp)
	addl	$1, %r13d
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	%rax, (%rdx)
	addq	$8, %rdx
	cmpl	%r13d, -84(%rbp)
	jne	.L1702
.L1701:
	xorl	%r8d, %r8d
	movl	%r12d, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movl	%r14d, %edi
	call	_ZN2v88internal7Runtime14IsNonReturningENS1_10FunctionIdE@PLT
	testb	%al, %al
	jne	.L1714
.L1697:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1715
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1712:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1698
	.p2align 4,,10
	.p2align 3
.L1714:
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5ThrowEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	448(%rbx), %rsi
	movq	%rax, -64(%rbp)
	cmpq	456(%rbx), %rsi
	je	.L1704
	movq	%rax, (%rsi)
	addq	$8, 448(%rbx)
.L1705:
	movq	$0, 168(%rbx)
	jmp	.L1697
	.p2align 4,,10
	.p2align 3
.L1713:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1700
	.p2align 4,,10
	.p2align 3
.L1704:
	leaq	-64(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1705
.L1715:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24605:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitCallRuntimeEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitCallRuntimeEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallJSRuntimeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallJSRuntimeEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallJSRuntimeEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallJSRuntimeEv:
.LFB24603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1725
.L1717:
	leaq	136(%rbx), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor28GetNativeContextIndexOperandEi@PLT
	xorl	%esi, %esi
	movl	$1, %ecx
	movslq	%eax, %rdx
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11LoadContextEmmb@PLT
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movdqu	24(%rbx), %xmm1
	leaq	-80(%rbp), %rcx
	movq	16(%rbx), %rdi
	movq	%rcx, %rsi
	movq	%rcx, -104(%rbp)
	movq	%rax, %r13
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_@PLT
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	$2, %esi
	movl	%eax, %r12d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movq	-104(%rbp), %rcx
	leaq	-84(%rbp), %rdx
	movl	$1, %r9d
	movl	%eax, %r10d
	movq	16(%rbx), %rax
	movl	$2, %r8d
	movl	$0x7fc00000, -84(%rbp)
	leal	2(%r10), %r15d
	movl	%r10d, -116(%rbp)
	movq	368(%rax), %rdi
	movslq	%r15d, %rsi
	movq	$0, -80(%rbp)
	movq	%rsi, -112(%rbp)
	movl	$-1, -72(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder4CallEmRKNS1_13CallFrequencyERKNS1_14FeedbackSourceENS0_19ConvertReceiverModeENS0_15SpeculationModeE@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	8(%rbx), %rdi
	movq	-112(%rbp), %rsi
	movq	%rax, -104(%rbp)
	movl	-116(%rbp), %r10d
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	salq	$3, %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L1726
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L1719:
	movq	%r13, %xmm0
	movhps	-104(%rbp), %xmm0
	movups	%xmm0, (%rcx)
	testl	%r10d, %r10d
	jle	.L1720
	leal	(%r10,%r12), %eax
	leaq	16(%rcx), %r13
	movl	%eax, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L1721:
	movq	168(%rbx), %rdi
	movl	%r12d, %esi
	movq	%rcx, -104(%rbp)
	addl	$1, %r12d
	addq	$8, %r13
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	-104(%rbp), %rcx
	movq	%rax, -8(%r13)
	cmpl	%r12d, -112(%rbp)
	jne	.L1721
.L1720:
	xorl	%r8d, %r8d
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1727
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1725:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L1726:
	movl	%r10d, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-112(%rbp), %r10d
	movq	%rax, %rcx
	jmp	.L1719
.L1727:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24603:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallJSRuntimeEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallJSRuntimeEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder20VisitInvokeIntrinsicEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitInvokeIntrinsicEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitInvokeIntrinsicEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitInvokeIntrinsicEv:
.LFB24611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 416(%rdi)
	jne	.L1736
.L1729:
	leaq	136(%rbx), %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor21GetIntrinsicIdOperandEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movl	%eax, %r14d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movl	%eax, %r13d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movl	%r14d, %esi
	movl	%eax, %edx
	movq	16(%rbx), %rax
	movq	%rdx, %r12
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdEm@PLT
	movq	8(%rbx), %rdi
	movslq	%r12d, %rsi
	movq	%rax, %r15
	salq	$3, %rsi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	%rax, %rsi
	ja	.L1737
	addq	%r14, %rsi
	movq	%rsi, 16(%rdi)
.L1731:
	movq	%r14, %rcx
	testl	%r12d, %r12d
	jle	.L1732
	leal	(%r12,%r13), %eax
	movl	%eax, -60(%rbp)
	.p2align 4,,10
	.p2align 3
.L1733:
	movq	168(%rbx), %rdi
	movl	%r13d, %esi
	movq	%rcx, -56(%rbp)
	addl	$1, %r13d
	addq	$8, %r14
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	-56(%rbp), %rcx
	movq	%rax, -8(%r14)
	cmpl	%r13d, -60(%rbp)
	jne	.L1733
.L1732:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movl	%r12d, %edx
	xorl	%r8d, %r8d
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	.p2align 4,,10
	.p2align 3
.L1736:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L1737:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L1731
	.cfi_endproc
.LFE24611:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitInvokeIntrinsicEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitInvokeIntrinsicEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder14VisitConstructEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitConstructEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitConstructEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitConstructEv:
.LFB24609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1760
.L1739:
	leaq	136(%rbx), %r12
	xorl	%esi, %esi
	leaq	56(%rbx), %r13
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movl	%eax, %r14d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movl	%eax, -152(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	leaq	-96(%rbp), %r12
	movl	%eax, -132(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	56(%rbx), %rsi
	movq	8(%r13), %rdx
	movq	%r12, %rdi
	movl	%eax, %ecx
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	-96(%rbp), %rax
	movq	168(%rbx), %rdi
	movq	56(%rdi), %rdx
	movslq	92(%rdi), %rsi
	movq	%rax, -112(%rbp)
	movl	-88(%rbp), %eax
	movl	%eax, -104(%rbp)
	movq	48(%rdi), %rax
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1761
	movq	(%rax,%rsi,8), %rax
	movl	%r14d, %esi
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movss	72(%rbx), %xmm0
	movq	%rax, %r14
	ucomiss	%xmm0, %xmm0
	jp	.L1756
	movq	56(%rbx), %rsi
	movq	8(%r13), %rdx
	movl	%r15d, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker18GetFeedbackForCallERKNS1_14FeedbackSourceE@PLT
	pxor	%xmm0, %xmm0
	movl	(%rax), %r8d
	movq	%rax, %rdi
	testl	%r8d, %r8d
	jne	.L1762
.L1741:
	movl	-132(%rbp), %eax
	leaq	-112(%rbp), %rcx
	leaq	-124(%rbp), %rdx
	movss	%xmm0, -124(%rbp)
	leal	2(%rax), %r15d
	movq	16(%rbx), %rax
	movl	%r15d, %esi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder9ConstructEjRKNS1_13CallFrequencyERKNS1_14FeedbackSourceE@PLT
	movq	8(%rbx), %rdi
	movslq	%r15d, %r8
	movq	%rax, -144(%rbp)
	salq	$3, %r8
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	movq	%r8, %rsi
	subq	%r13, %rax
	cmpq	%rax, %r8
	ja	.L1763
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1746:
	movl	-132(%rbp), %eax
	movq	%r14, 0(%r13)
	testl	%eax, %eax
	jle	.L1747
	movl	-152(%rbp), %r14d
	leaq	8(%r13), %r9
	addl	%r14d, %eax
	movl	%eax, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L1748:
	movq	168(%rbx), %rdi
	movl	%r14d, %esi
	movq	%r9, -160(%rbp)
	addl	$1, %r14d
	movq	%r8, -152(%rbp)
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	-160(%rbp), %r9
	movq	-152(%rbp), %r8
	movq	%rax, (%r9)
	addq	$8, %r9
	cmpl	%r14d, -136(%rbp)
	jne	.L1748
.L1747:
	movq	-168(%rbp), %rax
	leaq	80(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, -8(%r13,%r8)
	movq	168(%rbx), %rax
	movl	-104(%rbp), %edx
	movl	-132(%rbp), %r8d
	movq	32(%rax), %r9
	pushq	%rdx
	movq	-144(%rbp), %rdx
	pushq	24(%rax)
	call	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceConstructOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE@PLT
	movl	-96(%rbp), %eax
	popq	%rcx
	movq	-72(%rbp), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L1764
	cmpl	$1, %eax
	je	.L1765
	movq	-144(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	%r15d, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %rsi
.L1754:
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
.L1738:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1766
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1762:
	.cfi_restore_state
	call	_ZNK2v88internal8compiler17ProcessedFeedback6AsCallEv@PLT
	pxor	%xmm0, %xmm0
	movss	32(%rax), %xmm1
	ucomiss	%xmm0, %xmm1
	jnp	.L1767
.L1757:
	mulss	72(%rbx), %xmm1
	movaps	%xmm1, %xmm0
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1765:
	movq	168(%rbx), %rax
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1760:
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1739
	.p2align 4,,10
	.p2align 3
.L1764:
	movq	%rdx, -120(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L1750
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L1751:
	movq	$0, 168(%rbx)
	jmp	.L1738
	.p2align 4,,10
	.p2align 3
.L1756:
	movss	.LC7(%rip), %xmm0
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1763:
	movq	%r8, -160(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-160(%rbp), %r8
	movq	%rax, %r13
	jmp	.L1746
	.p2align 4,,10
	.p2align 3
.L1750:
	leaq	-120(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1751
.L1767:
	jne	.L1757
	jmp	.L1741
.L1761:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1766:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24609:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitConstructEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitConstructEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder24VisitConstructWithSpreadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitConstructWithSpreadEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitConstructWithSpreadEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitConstructWithSpreadEv:
.LFB24610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1790
.L1769:
	leaq	136(%rbx), %r12
	xorl	%esi, %esi
	leaq	56(%rbx), %r13
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movl	%eax, %r14d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movl	%eax, -152(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	leaq	-96(%rbp), %r12
	movl	%eax, -132(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	56(%rbx), %rsi
	movq	8(%r13), %rdx
	movq	%r12, %rdi
	movl	%eax, %ecx
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	-96(%rbp), %rax
	movq	168(%rbx), %rdi
	movq	56(%rdi), %rdx
	movslq	92(%rdi), %rsi
	movq	%rax, -112(%rbp)
	movl	-88(%rbp), %eax
	movl	%eax, -104(%rbp)
	movq	48(%rdi), %rax
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1791
	movq	(%rax,%rsi,8), %rax
	movl	%r14d, %esi
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movss	72(%rbx), %xmm0
	movq	%rax, %r14
	ucomiss	%xmm0, %xmm0
	jp	.L1786
	movq	56(%rbx), %rsi
	movq	8(%r13), %rdx
	movl	%r15d, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker18GetFeedbackForCallERKNS1_14FeedbackSourceE@PLT
	pxor	%xmm0, %xmm0
	movl	(%rax), %r8d
	movq	%rax, %rdi
	testl	%r8d, %r8d
	jne	.L1792
.L1771:
	movl	-132(%rbp), %eax
	leaq	-112(%rbp), %rcx
	leaq	-124(%rbp), %rdx
	movss	%xmm0, -124(%rbp)
	leal	2(%rax), %r15d
	movq	16(%rbx), %rax
	movl	%r15d, %esi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder19ConstructWithSpreadEjRKNS1_13CallFrequencyERKNS1_14FeedbackSourceE@PLT
	movq	8(%rbx), %rdi
	movslq	%r15d, %r8
	movq	%rax, -144(%rbp)
	salq	$3, %r8
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	movq	%r8, %rsi
	subq	%r13, %rax
	cmpq	%rax, %r8
	ja	.L1793
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1776:
	movl	-132(%rbp), %eax
	movq	%r14, 0(%r13)
	testl	%eax, %eax
	jle	.L1777
	movl	-152(%rbp), %r14d
	leaq	8(%r13), %r9
	addl	%r14d, %eax
	movl	%eax, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L1778:
	movq	168(%rbx), %rdi
	movl	%r14d, %esi
	movq	%r9, -160(%rbp)
	addl	$1, %r14d
	movq	%r8, -152(%rbp)
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	-160(%rbp), %r9
	movq	-152(%rbp), %r8
	movq	%rax, (%r9)
	addq	$8, %r9
	cmpl	%r14d, -136(%rbp)
	jne	.L1778
.L1777:
	movq	-168(%rbp), %rax
	leaq	80(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, -8(%r13,%r8)
	movq	168(%rbx), %rax
	movl	-104(%rbp), %edx
	movl	-132(%rbp), %r8d
	movq	32(%rax), %r9
	pushq	%rdx
	movq	-144(%rbp), %rdx
	pushq	24(%rax)
	call	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceConstructOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE@PLT
	movl	-96(%rbp), %eax
	popq	%rcx
	movq	-72(%rbp), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L1794
	cmpl	$1, %eax
	je	.L1795
	movq	-144(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	%r15d, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %rsi
.L1784:
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
.L1768:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1796
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1792:
	.cfi_restore_state
	call	_ZNK2v88internal8compiler17ProcessedFeedback6AsCallEv@PLT
	pxor	%xmm0, %xmm0
	movss	32(%rax), %xmm1
	ucomiss	%xmm0, %xmm1
	jnp	.L1797
.L1787:
	mulss	72(%rbx), %xmm1
	movaps	%xmm1, %xmm0
	jmp	.L1771
	.p2align 4,,10
	.p2align 3
.L1795:
	movq	168(%rbx), %rax
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L1790:
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1769
	.p2align 4,,10
	.p2align 3
.L1794:
	movq	%rdx, -120(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L1780
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L1781:
	movq	$0, 168(%rbx)
	jmp	.L1768
	.p2align 4,,10
	.p2align 3
.L1786:
	movss	.LC7(%rip), %xmm0
	jmp	.L1771
	.p2align 4,,10
	.p2align 3
.L1793:
	movq	%r8, -160(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-160(%rbp), %r8
	movq	%rax, %r13
	jmp	.L1776
	.p2align 4,,10
	.p2align 3
.L1780:
	leaq	-120(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1781
.L1797:
	jne	.L1787
	jmp	.L1771
.L1791:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1796:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24610:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitConstructWithSpreadEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitConstructWithSpreadEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11VisitTestInEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitTestInEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitTestInEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitTestInEv:
.LFB24670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1803
.L1799:
	movq	168(%rbx), %r12
	movq	48(%r12), %rax
	movq	56(%r12), %rdx
	movslq	92(%r12), %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1804
	leaq	136(%rbx), %r14
	movq	(%rax,%rsi,8), %r13
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %r12
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	64(%rbx), %rdx
	movq	56(%rbx), %rsi
	movq	%r12, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	16(%rbx), %rax
	movq	%r12, %rsi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11HasPropertyERKNS1_14FeedbackSourceE@PLT
	movq	%r13, %xmm0
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movhps	-88(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1805
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1803:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1799
.L1805:
	call	__stack_chk_fail@PLT
.L1804:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24670:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitTestInEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitTestInEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateRegExpLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateRegExpLiteralEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateRegExpLiteralEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateRegExpLiteralEv:
.LFB24572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	136(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r12, %rdi
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9StringRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	64(%rbx), %rdx
	movq	56(%rbx), %rsi
	movq	%r13, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetFlagOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %r12d
	movq	16(%rbx), %rax
	movq	368(%rax), %r15
	call	_ZNK2v88internal8compiler9StringRef6objectEv@PLT
	movl	%r12d, %ecx
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder19CreateLiteralRegExpENS0_6HandleINS0_6StringEEERKNS1_14FeedbackSourceEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1809
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1809:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24572:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateRegExpLiteralEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateRegExpLiteralEv
	.section	.rodata._ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateArrayLiteralEv.str1.1,"aMS",@progbits,1
.LC18:
	.string	"IsHeapObject()"
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateArrayLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateArrayLiteralEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateArrayLiteralEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateArrayLiteralEv:
.LFB24573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	136(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r12, %rdi
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L1814
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	-80(%rbp), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	64(%rbx), %rdx
	movq	56(%rbx), %rsi
	movq	%r14, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetFlagOperandEi@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZNK2v88internal8compiler30ArrayBoilerplateDescriptionRef25constants_elements_lengthEv@PLT
	movq	%r13, %rdi
	movl	%eax, -100(%rbp)
	movq	16(%rbx), %rax
	movq	368(%rax), %r15
	call	_ZNK2v88internal8compiler30ArrayBoilerplateDescriptionRef6objectEv@PLT
	movl	%r12d, %ecx
	movl	-100(%rbp), %r8d
	movq	%r14, %rdx
	andl	$31, %ecx
	movq	%rax, %rsi
	movq	%r15, %rdi
	orl	$2, %ecx
	call	_ZN2v88internal8compiler17JSOperatorBuilder18CreateLiteralArrayENS0_6HandleINS0_27ArrayBoilerplateDescriptionEEERKNS1_14FeedbackSourceEii@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1815
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1814:
	.cfi_restore_state
	leaq	.LC18(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1815:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24573:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateArrayLiteralEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateArrayLiteralEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateObjectLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateObjectLiteralEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateObjectLiteralEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateObjectLiteralEv:
.LFB24579:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	136(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r12, %rdi
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L1820
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	-80(%rbp), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	64(%rbx), %rdx
	movq	56(%rbx), %rsi
	movq	%r14, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetFlagOperandEi@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZNK2v88internal8compiler31ObjectBoilerplateDescriptionRef4sizeEv@PLT
	movq	%r13, %rdi
	movl	%eax, -100(%rbp)
	movq	16(%rbx), %rax
	movq	368(%rax), %r15
	call	_ZNK2v88internal8compiler31ObjectBoilerplateDescriptionRef6objectEv@PLT
	movl	-100(%rbp), %r8d
	movl	%r12d, %ecx
	movq	%r14, %rdx
	andl	$31, %ecx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder19CreateLiteralObjectENS0_6HandleINS0_28ObjectBoilerplateDescriptionEEERKNS1_14FeedbackSourceEii@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1821
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1820:
	.cfi_restore_state
	leaq	.LC18(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1821:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24579:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateObjectLiteralEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateObjectLiteralEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder14VisitForInNextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitForInNextEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitForInNextEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitForInNextEv:
.LFB24725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1834
.L1823:
	leaq	136(%r14), %rbx
	movq	168(%r14), %r12
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%r14), %r12
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movl	$2, %esi
	movq	%rbx, %rdi
	movq	%rax, -184(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	168(%r14), %rdi
	movl	%eax, %esi
	movl	%eax, %r13d
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%r14), %rdi
	leal	1(%r13), %esi
	movq	%rax, %r12
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%r14), %rdx
	movq	%rax, -176(%rbp)
	movq	16(%r14), %rax
	movq	32(%rdx), %rdi
	movq	24(%rdx), %r15
	movq	(%rax), %r13
	movq	%rdi, -192(%rbp)
	movq	8(%rax), %rdi
	movq	%rdi, -200(%rbp)
	call	_ZN2v88internal8compiler10BitsetType13UnsignedSmallEv@PLT
	movq	-200(%rbp), %rdi
	movl	%eax, %esi
	orl	$1, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TypeGuardENS1_4TypeE@PLT
	movq	%r15, -80(%rbp)
	leaq	-96(%rbp), %r15
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$3, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	-184(%rbp), %xmm0
	movhps	-192(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$3, %esi
	movq	%rbx, %rdi
	movq	%rax, %r13
	movq	168(%r14), %rax
	movq	%r13, 32(%rax)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	subq	$8, %rsp
	leaq	-128(%rbp), %rdi
	movq	%r13, %r9
	movl	%eax, %r8d
	movq	168(%r14), %rax
	movq	-160(%rbp), %rdx
	leaq	80(%r14), %rsi
	pushq	%r8
	movq	-176(%rbp), %rcx
	movq	%r12, %r8
	pushq	24(%rax)
	pushq	32(%rax)
	call	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceForInNextOperationEPNS1_4NodeES4_S4_S4_S4_S4_NS0_12FeedbackSlotE@PLT
	movl	-128(%rbp), %eax
	movq	-104(%rbp), %rdx
	addq	$32, %rsp
	cmpl	$2, %eax
	je	.L1835
	cmpl	$1, %eax
	jne	.L1828
	movq	168(%r14), %rax
	movq	-112(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movq	168(%r14), %rax
	movq	%rdx, 24(%rax)
.L1828:
	movq	16(%r14), %rax
	movl	$3, %esi
	movq	%rbx, %rdi
	movq	368(%rax), %r8
	movq	%r8, -192(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	leaq	-144(%rbp), %r9
	movq	56(%r14), %rsi
	movq	64(%r14), %rdx
	movq	%r9, %rdi
	movl	%eax, %ecx
	movq	%r9, -184(%rbp)
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	-184(%rbp), %r9
	movq	(%r14), %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker19GetFeedbackForForInERKNS1_14FeedbackSourceE@PLT
	movq	-192(%rbp), %r8
	cmpb	$3, %al
	ja	.L1836
	movzbl	%al, %eax
	movq	%r13, %xmm2
	movq	%r12, %xmm0
	movq	%r8, %rdi
	movq	-160(%rbp), %xmm1
	leaq	CSWTCH.612(%rip), %rdx
	punpcklqdq	%xmm2, %xmm0
	movzbl	(%rdx,%rax), %esi
	movaps	%xmm0, -160(%rbp)
	movhps	-176(%rbp), %xmm1
	movaps	%xmm1, -176(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder9ForInNextENS1_9ForInModeE@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movdqa	-176(%rbp), %xmm1
	movdqa	-160(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$4, %edx
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%r14), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
.L1822:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1837
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1834:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1823
	.p2align 4,,10
	.p2align 3
.L1835:
	movq	%rdx, -144(%rbp)
	movq	448(%r14), %rsi
	cmpq	456(%r14), %rsi
	je	.L1825
	movq	%rdx, (%rsi)
	addq	$8, 448(%r14)
.L1826:
	movq	$0, 168(%r14)
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1825:
	leaq	-144(%rbp), %rdx
	leaq	432(%r14), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1826
.L1837:
	call	__stack_chk_fail@PLT
.L1836:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24725:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitForInNextEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitForInNextEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder14VisitForInStepEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitForInStepEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitForInStepEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitForInStepEv:
.LFB24726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1842
.L1839:
	xorl	%esi, %esi
	movq	168(%rbx), %r12
	leaq	136(%rbx), %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	16(%rbx), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler7JSGraph11OneConstantEv@PLT
	xorl	%esi, %esi
	movq	%rax, -56(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeSafeIntegerAddENS1_19NumberOperationHintE@PLT
	xorl	%r8d, %r8d
	leaq	-48(%rbp), %rcx
	movq	%rbx, %rdi
	movq	-64(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-56(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1843
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1842:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1839
.L1843:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24726:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitForInStepEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitForInStepEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE:
.LFB24417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testl	%edx, %edx
	je	.L1852
.L1845:
	movq	48(%rbx), %rax
	movq	56(%rbx), %rdx
	movslq	92(%rbx), %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1853
	movq	%r12, (%rax,%rsi,8)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1852:
	.cfi_restore_state
	movq	(%rdi), %r13
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	je	.L1845
	movl	144(%r13), %r14d
	movq	160(%r13), %rdi
	movl	%r14d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%r13), %rdi
	movl	%r14d, %esi
	xorl	%edx, %edx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	jmp	.L1845
.L1853:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24417:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder21VisitStaKeyedPropertyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitStaKeyedPropertyEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitStaKeyedPropertyEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitStaKeyedPropertyEv:
.LFB24557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1869
.L1855:
	movq	168(%rbx), %r12
	movq	48(%r12), %rax
	movq	56(%r12), %rdx
	movslq	92(%r12), %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1870
	leaq	136(%rbx), %r13
	movq	(%rax,%rsi,8), %r14
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%rbx), %r15
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$2, %esi
	leaq	-128(%rbp), %r13
	movq	%rax, %r15
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	leaq	-112(%rbp), %r10
	movq	64(%rbx), %rdx
	movq	56(%rbx), %rsi
	movl	%eax, %ecx
	movq	%r10, %rdi
	movq	%r10, -152(%rbp)
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	-112(%rbp), %rax
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, -128(%rbp)
	movl	-104(%rbp), %eax
	movl	%eax, -120(%rbp)
	call	_ZNK2v88internal8compiler12JSHeapBroker19GetFeedbackSlotKindERKNS1_14FeedbackSourceE@PLT
	movq	%r13, %rdx
	cmpl	$3, %eax
	movq	16(%rbx), %rax
	setg	%sil
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder13StorePropertyENS0_12LanguageModeERKNS1_14FeedbackSourceE@PLT
	movl	-120(%rbp), %edx
	subq	$8, %rsp
	leaq	80(%rbx), %rsi
	movq	%rax, %r13
	movq	168(%rbx), %rax
	movq	%r14, %r9
	movq	%r15, %r8
	pushq	%rdx
	movq	-152(%rbp), %r10
	movq	%r13, %rdx
	movq	%r12, %rcx
	pushq	24(%rax)
	pushq	32(%rax)
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_S7_NS0_12FeedbackSlotE@PLT
	movl	-112(%rbp), %eax
	movq	-88(%rbp), %rdx
	addq	$32, %rsp
	cmpl	$2, %eax
	je	.L1871
	cmpl	$1, %eax
	je	.L1872
	movq	%r12, %xmm0
	movq	%r15, %xmm1
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	punpcklqdq	%xmm1, %xmm0
	leaq	-80(%rbp), %rcx
	movl	$3, %edx
	movq	%rbx, %rdi
	movq	%r14, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %r12
.L1863:
	movq	168(%rbx), %rax
	movq	(%r12), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L1873
.L1854:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1874
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1872:
	.cfi_restore_state
	movq	168(%rbx), %rax
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %r12
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1863
	.p2align 4,,10
	.p2align 3
.L1869:
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1855
	.p2align 4,,10
	.p2align 3
.L1873:
	movl	144(%rbx), %r13d
	movq	160(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%rbx), %rdi
	movl	%r13d, %esi
	movq	$-1, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	jmp	.L1854
	.p2align 4,,10
	.p2align 3
.L1871:
	movq	%rdx, -136(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L1858
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L1859:
	movq	$0, 168(%rbx)
	jmp	.L1854
	.p2align 4,,10
	.p2align 3
.L1858:
	leaq	-136(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1859
.L1874:
	call	__stack_chk_fail@PLT
.L1870:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24557:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitStaKeyedPropertyEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitStaKeyedPropertyEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder29VisitStaDataPropertyInLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitStaDataPropertyInLiteralEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitStaDataPropertyInLiteralEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitStaDataPropertyInLiteralEv:
.LFB24531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1890
.L1876:
	leaq	136(%rbx), %r14
	movq	168(%rbx), %r12
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	168(%rbx), %r13
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%rax, %r13
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1891
	movq	(%rcx,%rsi,8), %r15
	movq	%r14, %rdi
	movl	$2, %esi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetFlagOperandEi@PLT
	movq	%r14, %rdi
	movl	$3, %esi
	movl	%eax, -180(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	leaq	-128(%rbp), %r11
	movq	64(%rbx), %rdx
	movq	56(%rbx), %rsi
	movl	%eax, %ecx
	movq	%r11, %rdi
	movq	%r11, -176(%rbp)
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	-128(%rbp), %rax
	leaq	-144(%rbp), %rsi
	movq	%rax, -144(%rbp)
	movl	-120(%rbp), %eax
	movl	%eax, -136(%rbp)
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder26StoreDataPropertyInLiteralERKNS1_14FeedbackSourceE@PLT
	subq	$8, %rsp
	leaq	80(%rbx), %rsi
	movq	%r15, %r9
	movl	-136(%rbp), %edx
	movq	%rax, %r14
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	168(%rbx), %rax
	movq	-176(%rbp), %r11
	pushq	%rdx
	movq	%r14, %rdx
	pushq	24(%rax)
	movq	%r11, %rdi
	pushq	32(%rax)
	call	_ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_S7_NS0_12FeedbackSlotE@PLT
	movl	-128(%rbp), %eax
	movq	-104(%rbp), %rdx
	addq	$32, %rsp
	cmpl	$2, %eax
	je	.L1892
	cmpl	$1, %eax
	je	.L1893
	pxor	%xmm0, %xmm0
	movq	%r13, %xmm2
	movq	16(%rbx), %rdi
	movq	%r12, %xmm1
	cvtsi2sdl	-180(%rbp), %xmm0
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -176(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%r15, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm1
	movq	%rax, %xmm3
	leaq	-96(%rbp), %rcx
	movq	%rbx, %rdi
	punpcklqdq	%xmm3, %xmm0
	movl	$4, %edx
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %r12
.L1884:
	movq	168(%rbx), %rax
	movq	(%r12), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L1894
.L1875:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1895
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1893:
	.cfi_restore_state
	movq	168(%rbx), %rax
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %r12
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1884
	.p2align 4,,10
	.p2align 3
.L1890:
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1876
	.p2align 4,,10
	.p2align 3
.L1894:
	movl	144(%rbx), %r13d
	movq	160(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%rbx), %rdi
	movl	%r13d, %esi
	movq	$-1, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L1892:
	movq	%rdx, -152(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L1879
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L1880:
	movq	$0, 168(%rbx)
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L1879:
	leaq	-152(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1880
.L1895:
	call	__stack_chk_fail@PLT
.L1891:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24531:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitStaDataPropertyInLiteralEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitStaDataPropertyInLiteralEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder15BuildNamedStoreENS2_9StoreModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildNamedStoreENS2_9StoreModeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildNamedStoreENS2_9StoreModeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildNamedStoreENS2_9StoreModeE:
.LFB24553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L1913
.L1897:
	movq	168(%rbx), %r12
	movq	48(%r12), %rax
	movq	56(%r12), %rdx
	movslq	92(%r12), %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1914
	leaq	136(%rbx), %r15
	movq	(%rax,%rsi,8), %rax
	xorl	%esi, %esi
	leaq	-144(%rbp), %r14
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%r15, %rdi
	movl	$1, %esi
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rdi
	movl	$2, %esi
	leaq	-112(%rbp), %r15
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	56(%rbx), %rsi
	movq	64(%rbx), %rdx
	movq	%r15, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	-112(%rbp), %rax
	movq	%rax, -128(%rbp)
	movl	-104(%rbp), %eax
	movl	%eax, -120(%rbp)
	cmpl	$1, %r13d
	je	.L1915
	leaq	-128(%rbp), %rcx
	movq	(%rbx), %rdi
	movq	%rcx, %rsi
	movq	%rcx, -184(%rbp)
	call	_ZNK2v88internal8compiler12JSHeapBroker19GetFeedbackSlotKindERKNS1_14FeedbackSourceE@PLT
	movq	%r14, %rdi
	movl	%eax, -172(%rbp)
	movq	16(%rbx), %rax
	movq	368(%rax), %r13
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	cmpl	$3, -172(%rbp)
	movq	-184(%rbp), %rcx
	movq	%r13, %rdi
	movq	%rax, %rdx
	setg	%sil
	call	_ZN2v88internal8compiler17JSOperatorBuilder10StoreNamedENS0_12LanguageModeENS0_6HandleINS0_4NameEEERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %r13
.L1900:
	movq	168(%rbx), %rax
	movl	-120(%rbp), %edx
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	-168(%rbp), %r8
	leaq	80(%rbx), %rsi
	movq	32(%rax), %r9
	pushq	%rdx
	movq	%r13, %rdx
	pushq	24(%rax)
	call	_ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE@PLT
	movl	-112(%rbp), %eax
	popq	%rcx
	movq	-88(%rbp), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L1916
	cmpl	$1, %eax
	je	.L1917
	movq	%r12, %xmm0
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movhps	-168(%rbp), %xmm0
	movl	$2, %edx
	movq	%rbx, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %r12
.L1907:
	movq	168(%rbx), %rax
	movq	(%r12), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L1918
.L1896:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1919
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1917:
	.cfi_restore_state
	movq	168(%rbx), %rax
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %r12
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1907
	.p2align 4,,10
	.p2align 3
.L1915:
	movq	16(%rbx), %rax
	movq	%r14, %rdi
	movq	368(%rax), %r13
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	leaq	-128(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler17JSOperatorBuilder13StoreNamedOwnENS0_6HandleINS0_4NameEEERKNS1_14FeedbackSourceE@PLT
	movq	%rax, %r13
	jmp	.L1900
	.p2align 4,,10
	.p2align 3
.L1913:
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1897
	.p2align 4,,10
	.p2align 3
.L1918:
	movl	144(%rbx), %r13d
	movq	160(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%rbx), %rdi
	movl	%r13d, %esi
	movq	$-1, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	jmp	.L1896
	.p2align 4,,10
	.p2align 3
.L1916:
	movq	%rdx, -152(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L1902
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L1903:
	movq	$0, 168(%rbx)
	jmp	.L1896
	.p2align 4,,10
	.p2align 3
.L1902:
	leaq	-152(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1903
.L1919:
	call	__stack_chk_fail@PLT
.L1914:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24553:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildNamedStoreENS2_9StoreModeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildNamedStoreENS2_9StoreModeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder21VisitStaNamedPropertyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitStaNamedPropertyEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitStaNamedPropertyEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitStaNamedPropertyEv:
.LFB24554:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildNamedStoreENS2_9StoreModeE
	.cfi_endproc
.LFE24554:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitStaNamedPropertyEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitStaNamedPropertyEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder24VisitStaNamedOwnPropertyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitStaNamedOwnPropertyEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitStaNamedOwnPropertyEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitStaNamedOwnPropertyEv:
.LFB24556:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildNamedStoreENS2_9StoreModeE
	.cfi_endproc
.LFE24556:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitStaNamedOwnPropertyEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitStaNamedOwnPropertyEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment12BindRegisterENS0_11interpreter8RegisterEPNS1_4NodeENS3_24FrameStateAttachmentModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment12BindRegisterENS0_11interpreter8RegisterEPNS1_4NodeENS3_24FrameStateAttachmentModeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment12BindRegisterENS0_11interpreter8RegisterEPNS1_4NodeENS3_24FrameStateAttachmentModeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment12BindRegisterENS0_11interpreter8RegisterEPNS1_4NodeENS3_24FrameStateAttachmentModeE:
.LFB24419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%esi, -60(%rbp)
	testl	%esi, %esi
	js	.L1933
	movl	88(%rdi), %ebx
	addl	%esi, %ebx
.L1924:
	testl	%r14d, %r14d
	je	.L1934
.L1925:
	movq	48(%r12), %rax
	movq	56(%r12), %rdx
	movslq	%ebx, %rbx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rbx
	jnb	.L1935
	movq	%r13, (%rax,%rbx,8)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1936
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1934:
	.cfi_restore_state
	movl	92(%r12), %eax
	movq	0(%r13), %rdi
	movq	(%r12), %r14
	subl	%ebx, %eax
	movslq	%eax, %r15
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	je	.L1925
	movl	144(%r14), %esi
	movq	160(%r14), %rdi
	movl	%esi, -68(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%r14), %rdi
	movl	-68(%rbp), %esi
	movq	%r15, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	jmp	.L1925
	.p2align 4,,10
	.p2align 3
.L1933:
	movl	12(%rdi), %esi
	leaq	-60(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	movl	%eax, %ebx
	jmp	.L1924
.L1935:
	movq	%rbx, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1936:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24419:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment12BindRegisterENS0_11interpreter8RegisterEPNS1_4NodeENS3_24FrameStateAttachmentModeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment12BindRegisterENS0_11interpreter8RegisterEPNS1_4NodeENS3_24FrameStateAttachmentModeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder27BuildSwitchOnGeneratorStateERKNS0_10ZoneVectorINS1_16ResumeJumpTargetEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder27BuildSwitchOnGeneratorStateERKNS0_10ZoneVectorINS1_16ResumeJumpTargetEEEb
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder27BuildSwitchOnGeneratorStateERKNS0_10ZoneVectorINS1_16ResumeJumpTargetEEEb, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder27BuildSwitchOnGeneratorStateERKNS0_10ZoneVectorINS1_16ResumeJumpTargetEEEb:
.LFB24729:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	%edx, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	testb	%dl, %dl
	setne	%dl
	movq	80(%rax), %rbx
	movq	16(%rsi), %rax
	movzbl	%dl, %edx
	subq	8(%rsi), %rax
	sarq	$2, %rax
	imulq	%rcx, %rax
	leal	1(%rdx,%rax), %esi
	movq	16(%rdi), %rax
	movslq	%esi, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SwitchEm@PLT
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	8(%r13), %rbx
	movq	16(%r13), %r14
	movq	168(%r12), %r13
	cmpq	%rbx, %r14
	jne	.L1943
	jmp	.L1944
	.p2align 4,,10
	.p2align 3
.L1940:
	movq	%r12, %rdi
	addq	$12, %rbx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi
	movq	%r13, 168(%r12)
	cmpq	%rbx, %r14
	je	.L1944
.L1943:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv
	movl	(%rbx), %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r13
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfValueEiiNS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movl	4(%rbx), %esi
	cmpl	8(%rbx), %esi
	jne	.L1940
	movq	.LC19(%rip), %rax
	movq	16(%r12), %rdi
	movq	168(%r12), %r15
	movq	%rax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, 80(%r15)
	movl	4(%rbx), %esi
	jmp	.L1940
	.p2align 4,,10
	.p2align 3
.L1944:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv
	xorl	%esi, %esi
	movq	%rax, %rbx
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfDefaultENS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	16(%r12), %rax
	movl	$13, %esi
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12RuntimeAbortENS0_11AbortReasonE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5ThrowEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	448(%r12), %rsi
	movq	%rax, -72(%rbp)
	cmpq	456(%r12), %rsi
	je	.L1945
	movq	%rax, (%rsi)
	addq	$8, 448(%r12)
.L1946:
	cmpb	$0, -84(%rbp)
	movq	%rbx, 168(%r12)
	jne	.L1952
	movq	$0, 168(%r12)
.L1937:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1953
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1952:
	.cfi_restore_state
	movq	16(%r12), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$-2, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfValueEiiNS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L1945:
	leaq	-72(%rbp), %rdx
	leaq	432(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1946
.L1953:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24729:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder27BuildSwitchOnGeneratorStateERKNS0_10ZoneVectorINS1_16ResumeJumpTargetEEEb, .-_ZN2v88internal8compiler20BytecodeGraphBuilder27BuildSwitchOnGeneratorStateERKNS0_10ZoneVectorINS1_16ResumeJumpTargetEEEb
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder26BuildLoopHeaderEnvironmentEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildLoopHeaderEnvironmentEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildLoopHeaderEnvironmentEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildLoopHeaderEnvironmentEi:
.LFB24736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	%esi, -84(%rbp)
	movq	160(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler16BytecodeAnalysis12IsLoopHeaderEi@PLT
	testb	%al, %al
	jne	.L1967
.L1954:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1968
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1967:
	.cfi_restore_state
	movb	$1, 416(%rbx)
	movq	160(%rbx), %rdi
	movl	-84(%rbp), %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis14GetLoopInfoForEi@PLT
	movq	160(%rbx), %rdi
	movl	-84(%rbp), %esi
	movq	%rax, %r12
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	leaq	8(%r12), %rsi
	movq	40(%r12), %r13
	movq	168(%rbx), %rdi
	movq	%rax, %rdx
	movq	32(%r12), %r14
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment14PrepareForLoopERKNS1_23BytecodeLoopAssignmentsEPKNS1_21BytecodeLivenessStateE
	movq	216(%rbx), %rax
	movq	168(%rbx), %r9
	leaq	208(%rbx), %rcx
	testq	%rax, %rax
	je	.L1965
	movl	-84(%rbp), %edx
	movq	%rcx, %r15
	jmp	.L1958
	.p2align 4,,10
	.p2align 3
.L1969:
	movq	%rax, %r15
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1959
.L1958:
	cmpl	%edx, 32(%rax)
	jge	.L1969
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1958
.L1959:
	cmpq	%r15, %rcx
	je	.L1957
	cmpl	32(%r15), %edx
	jge	.L1962
.L1957:
	leaq	-84(%rbp), %rax
	movq	%r15, %rsi
	leaq	-64(%rbp), %rcx
	movq	%r9, -96(%rbp)
	leaq	192(%rbx), %rdi
	leaq	-65(%rbp), %r8
	movq	%rax, -64(%rbp)
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_
	movq	-96(%rbp), %r9
	movq	%rax, %r15
.L1962:
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv
	movq	%rax, 40(%r15)
	cmpq	%r13, %r14
	je	.L1954
	leaq	24(%r12), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder27BuildSwitchOnGeneratorStateERKNS0_10ZoneVectorINS1_16ResumeJumpTargetEEEb
	movq	16(%rbx), %rdi
	movsd	.LC19(%rip), %xmm0
	movq	168(%rbx), %r12
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, 80(%r12)
	jmp	.L1954
	.p2align 4,,10
	.p2align 3
.L1965:
	movq	%rcx, %r15
	jmp	.L1957
.L1968:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24736:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildLoopHeaderEnvironmentEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildLoopHeaderEnvironmentEi
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11Environment26BindRegistersToProjectionsENS0_11interpreter8RegisterEPNS1_4NodeENS3_24FrameStateAttachmentModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment26BindRegistersToProjectionsENS0_11interpreter8RegisterEPNS1_4NodeENS3_24FrameStateAttachmentModeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment26BindRegistersToProjectionsENS0_11interpreter8RegisterEPNS1_4NodeENS3_24FrameStateAttachmentModeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment26BindRegistersToProjectionsENS0_11interpreter8RegisterEPNS1_4NodeENS3_24FrameStateAttachmentModeE:
.LFB24420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%esi, -68(%rbp)
	testl	%esi, %esi
	js	.L1986
	addl	88(%rdi), %esi
	movslq	%esi, %r12
.L1972:
	movq	(%rbx), %rdi
	testl	%r13d, %r13d
	je	.L1987
.L1973:
	leaq	-64(%rbp), %rax
	xorl	%r14d, %r14d
	movq	%rax, -96(%rbp)
	movl	32(%rdi), %eax
	testl	%eax, %eax
	jle	.L1970
	.p2align 4,,10
	.p2align 3
.L1977:
	movq	(%r15), %r13
	movq	%r14, %rsi
	movq	16(%r13), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10ProjectionEm@PLT
	movq	56(%r15), %rdx
	movq	%rax, %rsi
	movq	48(%r15), %rax
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%r12, %rdx
	jbe	.L1988
	movq	-96(%rbp), %rdx
	leaq	(%rax,%r12,8), %rcx
	movq	%r13, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rcx, -88(%rbp)
	addq	$1, %r14
	addq	$1, %r12
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	-88(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	(%rbx), %rax
	cmpl	%r14d, 32(%rax)
	jg	.L1977
.L1970:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1989
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1987:
	.cfi_restore_state
	movl	92(%r15), %r13d
	movq	(%r15), %r14
	subl	%r12d, %r13d
	movslq	%r13d, %r13
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L1990
.L1974:
	movq	(%rbx), %rdi
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1986:
	movl	12(%rdi), %esi
	leaq	-68(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	movslq	%eax, %r12
	jmp	.L1972
.L1990:
	movl	144(%r14), %esi
	movq	160(%r14), %rdi
	movl	%esi, -88(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%r14), %rdi
	movl	-88(%rbp), %esi
	movq	%r13, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	jmp	.L1974
.L1988:
	movq	%r12, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1989:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24420:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment26BindRegistersToProjectionsENS0_11interpreter8RegisterEPNS1_4NodeENS3_24FrameStateAttachmentModeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment26BindRegistersToProjectionsENS0_11interpreter8RegisterEPNS1_4NodeENS3_24FrameStateAttachmentModeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCallRuntimeForPairEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCallRuntimeForPairEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCallRuntimeForPairEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCallRuntimeForPairEv:
.LFB24606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	cmpb	$0, 416(%rdi)
	jne	.L1999
.L1992:
	leaq	136(%rbx), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetRuntimeIdOperandEi@PLT
	movq	%r14, %rdi
	movl	$1, %esi
	movl	%eax, %r15d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	$2, %esi
	movl	%eax, %r13d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movq	%r14, %rdi
	movl	$3, %esi
	movl	%eax, %r12d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%r15d, %esi
	movl	%r12d, %edx
	movl	%eax, %r14d
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdEm@PLT
	movq	8(%rbx), %rdi
	movslq	%r12d, %rsi
	movq	%rax, %r15
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L2000
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L1994:
	movq	%rdx, %rcx
	testl	%r12d, %r12d
	jle	.L1995
	leal	(%r12,%r13), %eax
	movl	%eax, -68(%rbp)
	.p2align 4,,10
	.p2align 3
.L1996:
	movq	168(%rbx), %rdi
	movl	%r13d, %esi
	movq	%rcx, -64(%rbp)
	addl	$1, %r13d
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movq	%rax, (%rdx)
	addq	$8, %rdx
	cmpl	%r13d, -68(%rbp)
	jne	.L1996
.L1995:
	movl	%r12d, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	xorl	%r8d, %r8d
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdi
	addq	$40, %rsp
	movl	%r14d, %esi
	popq	%rbx
	movq	%rax, %rdx
	popq	%r12
	xorl	%ecx, %ecx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment26BindRegistersToProjectionsENS0_11interpreter8RegisterEPNS1_4NodeENS3_24FrameStateAttachmentModeE
	.p2align 4,,10
	.p2align 3
.L1999:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L1992
	.p2align 4,,10
	.p2align 3
.L2000:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1994
	.cfi_endproc
.LFE24606:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCallRuntimeForPairEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCallRuntimeForPairEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder14VisitStaGlobalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitStaGlobalEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitStaGlobalEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitStaGlobalEv:
.LFB24529:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L2010
.L2002:
	movq	16(%rbx), %rax
	leaq	136(%rbx), %r13
	xorl	%esi, %esi
	leaq	-96(%rbp), %r12
	movq	%r13, %rdi
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rdi
	movl	$1, %esi
	leaq	-80(%rbp), %r13
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	56(%rbx), %rsi
	movq	64(%rbx), %rdx
	movq	%r13, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2011
	movq	(%rbx), %rdi
	movq	(%rcx,%rsi,8), %r15
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker19GetFeedbackSlotKindERKNS1_14FeedbackSourceE@PLT
	movq	%r12, %rdi
	movl	%eax, -116(%rbp)
	movq	16(%rbx), %rax
	movq	368(%rax), %r14
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	cmpl	$3, -116(%rbp)
	leaq	-104(%rbp), %rdx
	movq	%r13, %rcx
	setg	%sil
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder11StoreGlobalENS0_12LanguageModeERKNS0_6HandleINS0_4NameEEERKNS1_14FeedbackSourceE@PLT
	movq	%rbx, %rdi
	leaq	-64(%rbp), %rdx
	movq	%r15, -64(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, %r12
	movq	168(%rbx), %rax
	movq	(%r12), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L2012
.L2001:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2013
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2010:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L2002
	.p2align 4,,10
	.p2align 3
.L2012:
	movl	144(%rbx), %r13d
	movq	160(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%rbx), %rdi
	movl	%r13d, %esi
	movq	$-1, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	jmp	.L2001
.L2013:
	call	__stack_chk_fail@PLT
.L2011:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24529:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitStaGlobalEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitStaGlobalEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder19VisitStaContextSlotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitStaContextSlotEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitStaContextSlotEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitStaContextSlotEv:
.LFB24537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	136(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r13, %rdi
	movq	368(%rax), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movl	%r12d, %edx
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler17JSOperatorBuilder12StoreContextEmm@PLT
	movq	168(%rbx), %rdx
	movq	%rax, %rsi
	movslq	92(%rdx), %r8
	movq	48(%rdx), %rax
	movq	56(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r8
	jnb	.L2018
	movq	(%rax,%r8,8), %rax
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	xorl	%esi, %esi
	movq	168(%rbx), %r14
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceContextInputEPNS1_4NodeES4_@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2019
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2018:
	.cfi_restore_state
	movq	%r8, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2019:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24537:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitStaContextSlotEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitStaContextSlotEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder26VisitStaCurrentContextSlotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitStaCurrentContextSlotEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitStaCurrentContextSlotEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitStaCurrentContextSlotEv:
.LFB24538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$136, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-120(%rdi), %rax
	movq	368(%rax), %r13
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	xorl	%esi, %esi
	movl	%eax, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder12StoreContextEmm@PLT
	movq	168(%r12), %rdx
	movq	%rax, %rsi
	movslq	92(%rdx), %r8
	movq	48(%rdx), %rax
	movq	56(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r8
	jnb	.L2024
	movq	(%rax,%r8,8), %rax
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2025
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2024:
	.cfi_restore_state
	movq	%r8, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2025:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24538:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitStaCurrentContextSlotEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitStaCurrentContextSlotEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18BuildLdaLookupSlotENS0_10TypeofModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18BuildLdaLookupSlotENS0_10TypeofModeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18BuildLdaLookupSlotENS0_10TypeofModeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18BuildLdaLookupSlotENS0_10TypeofModeE:
.LFB24539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L2032
.L2027:
	movq	16(%rbx), %r12
	xorl	%esi, %esi
	leaq	136(%rbx), %rdi
	leaq	-64(%rbp), %r14
	movq	360(%r12), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	xorl	%esi, %esi
	cmpl	$1, %r13d
	movq	%rax, %r12
	movq	16(%rbx), %rax
	setne	%sil
	addl	$303, %esi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -48(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2033
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2032:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L2027
.L2033:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24539:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18BuildLdaLookupSlotENS0_10TypeofModeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18BuildLdaLookupSlotENS0_10TypeofModeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder21VisitLdaNamedPropertyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitLdaNamedPropertyEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitLdaNamedPropertyEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitLdaNamedPropertyEv:
.LFB24550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L2044
.L2035:
	leaq	136(%rbx), %r12
	movq	168(%rbx), %r13
	xorl	%esi, %esi
	leaq	-128(%rbp), %r15
	movq	%r12, %rdi
	leaq	-96(%rbp), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	movq	16(%rbx), %rax
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	56(%rbx), %rsi
	movq	64(%rbx), %rdx
	movq	%r14, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	-96(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -112(%rbp)
	movl	-88(%rbp), %eax
	movl	%eax, -104(%rbp)
	movq	16(%rbx), %rax
	movq	368(%rax), %r12
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	leaq	-112(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler17JSOperatorBuilder9LoadNamedENS0_6HandleINS0_4NameEEERKNS1_14FeedbackSourceE@PLT
	subq	$8, %rsp
	leaq	80(%rbx), %rsi
	movq	%r13, %rcx
	movq	%rax, %r12
	movq	168(%rbx), %rax
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	24(%rax), %r9
	movq	32(%rax), %r8
	movl	-104(%rbp), %eax
	pushq	%rax
	call	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE@PLT
	movl	-96(%rbp), %eax
	popq	%rcx
	movq	-72(%rbp), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L2045
	cmpl	$1, %eax
	je	.L2046
	movq	%r12, %rsi
	leaq	-64(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, %rsi
.L2041:
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
.L2034:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2047
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2046:
	.cfi_restore_state
	movq	168(%rbx), %rax
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L2041
	.p2align 4,,10
	.p2align 3
.L2044:
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L2035
	.p2align 4,,10
	.p2align 3
.L2045:
	movq	%rdx, -136(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L2037
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L2038:
	movq	$0, 168(%rbx)
	jmp	.L2034
	.p2align 4,,10
	.p2align 3
.L2037:
	leaq	-136(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2038
.L2047:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24550:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitLdaNamedPropertyEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitLdaNamedPropertyEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder31VisitLdaNamedPropertyNoFeedbackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitLdaNamedPropertyNoFeedbackEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitLdaNamedPropertyNoFeedbackEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitLdaNamedPropertyNoFeedbackEv:
.LFB24551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L2052
.L2049:
	leaq	136(%rbx), %r13
	movq	168(%rbx), %r12
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$1, %esi
	leaq	-80(%rbp), %r13
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	16(%rbx), %rax
	movq	%r13, %rdi
	movq	$0, -64(%rbp)
	movl	$-1, -56(%rbp)
	movq	368(%rax), %r14
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	leaq	-64(%rbp), %rdx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder9LoadNamedENS0_6HandleINS0_4NameEEERKNS1_14FeedbackSourceE@PLT
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -48(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2053
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2052:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L2049
.L2053:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24551:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitLdaNamedPropertyNoFeedbackEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitLdaNamedPropertyNoFeedbackEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder22VisitLdaModuleVariableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitLdaModuleVariableEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitLdaModuleVariableEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitLdaModuleVariableEv:
.LFB24558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	136(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetImmediateOperandEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movl	%eax, %r13d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movl	$1, %ecx
	movl	$2, %edx
	movl	%eax, %esi
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11LoadContextEmmb@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movl	%r13d, %esi
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10LoadModuleEi@PLT
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -48(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2058
	movq	%rax, (%rcx,%rsi,8)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2059
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2058:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2059:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24558:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitLdaModuleVariableEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitLdaModuleVariableEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder12BuildUnaryOpEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder12BuildUnaryOpEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder12BuildUnaryOpEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder12BuildUnaryOpEPKNS1_8OperatorE:
.LFB24619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L2071
.L2061:
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2072
	movq	(%rcx,%rsi,8), %r13
	leaq	136(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	subq	$8, %rsp
	leaq	80(%rbx), %rsi
	movq	%r12, %rdx
	movl	%eax, %r10d
	movq	168(%rbx), %rax
	movq	%r13, %rcx
	leaq	-80(%rbp), %rdi
	movq	24(%rax), %r9
	movq	32(%rax), %r8
	pushq	%r10
	call	_ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE@PLT
	movl	-80(%rbp), %eax
	popq	%rcx
	movq	-56(%rbp), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L2073
	cmpl	$1, %eax
	je	.L2074
	movq	%r12, %rsi
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r13, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, %rsi
.L2068:
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
.L2060:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2075
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2074:
	.cfi_restore_state
	movq	168(%rbx), %rax
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L2068
	.p2align 4,,10
	.p2align 3
.L2071:
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L2061
	.p2align 4,,10
	.p2align 3
.L2073:
	movq	%rdx, -88(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L2064
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L2065:
	movq	$0, 168(%rbx)
	jmp	.L2060
	.p2align 4,,10
	.p2align 3
.L2064:
	leaq	-88(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2065
.L2075:
	call	__stack_chk_fail@PLT
.L2072:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24619:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder12BuildUnaryOpEPKNS1_8OperatorE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder12BuildUnaryOpEPKNS1_8OperatorE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseNotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseNotEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseNotEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseNotEv:
.LFB24626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10BitwiseNotEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder12BuildUnaryOpEPKNS1_8OperatorE
	.cfi_endproc
.LFE24626:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseNotEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitBitwiseNotEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder8VisitDecEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitDecEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitDecEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitDecEv:
.LFB24627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder9DecrementEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder12BuildUnaryOpEPKNS1_8OperatorE
	.cfi_endproc
.LFE24627:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitDecEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitDecEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder8VisitIncEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitIncEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitIncEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitIncEv:
.LFB24628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder9IncrementEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder12BuildUnaryOpEPKNS1_8OperatorE
	.cfi_endproc
.LFE24628:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitIncEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitIncEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11VisitNegateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitNegateEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitNegateEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitNegateEv:
.LFB24629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder6NegateEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder12BuildUnaryOpEPKNS1_8OperatorE
	.cfi_endproc
.LFE24629:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitNegateEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitNegateEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder24VisitToBooleanLogicalNotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitToBooleanLogicalNotEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitToBooleanLogicalNotEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitToBooleanLogicalNotEv:
.LFB24656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2089
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movq	(%rcx,%rsi,8), %r12
	leaq	-48(%rbp), %r13
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9ToBooleanEv@PLT
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%r12, -48(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10BooleanNotEv@PLT
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%r12, -48(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2089
	movq	%rax, (%rcx,%rsi,8)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2090
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2089:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2090:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24656:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitToBooleanLogicalNotEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitToBooleanLogicalNotEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder15VisitLogicalNotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitLogicalNotEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitLogicalNotEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitLogicalNotEv:
.LFB24655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2096
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movq	(%rcx,%rsi,8), %r12
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10BooleanNotEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2096
	movq	%rax, (%rcx,%rsi,8)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2097
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2096:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2097:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24655:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitLogicalNotEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitLogicalNotEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11VisitTypeOfEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitTypeOfEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitTypeOfEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitTypeOfEv:
.LFB24657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2103
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movq	(%rcx,%rsi,8), %r12
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder6TypeOfEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2103
	movq	%rax, (%rcx,%rsi,8)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2104
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2103:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24657:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitTypeOfEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitTypeOfEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder24VisitGetSuperConstructorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitGetSuperConstructorEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitGetSuperConstructorEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitGetSuperConstructorEv:
.LFB24661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2116
	movq	16(%rdi), %rax
	movq	%rdi, %r12
	movq	(%rcx,%rsi,8), %rbx
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder19GetSuperConstructorEv@PLT
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	leaq	136(%r12), %rdi
	xorl	%esi, %esi
	movq	168(%r12), %r13
	movq	%rax, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	js	.L2117
	addl	88(%r13), %eax
	movslq	%eax, %r12
.L2108:
	movl	92(%r13), %eax
	movq	(%rbx), %rdi
	movq	0(%r13), %r14
	subl	%r12d, %eax
	movslq	%eax, %r15
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L2118
.L2109:
	movq	48(%r13), %rcx
	movq	56(%r13), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r12
	jnb	.L2119
	movq	%rbx, (%rcx,%r12,8)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2120
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2118:
	.cfi_restore_state
	movl	144(%r14), %esi
	movq	160(%r14), %rdi
	movl	%esi, -84(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%r14), %rdi
	movl	-84(%rbp), %esi
	movq	%r15, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	jmp	.L2109
	.p2align 4,,10
	.p2align 3
.L2117:
	movl	12(%r13), %esi
	leaq	-68(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	movslq	%eax, %r12
	jmp	.L2108
.L2119:
	movq	%r12, %rsi
.L2116:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2120:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24661:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitGetSuperConstructorEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitGetSuperConstructorEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder21VisitTestUndetectableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitTestUndetectableEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitTestUndetectableEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitTestUndetectableEv:
.LFB24672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2126
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movq	(%rcx,%rsi,8), %r12
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20ObjectIsUndetectableEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2126
	movq	%rax, (%rcx,%rsi,8)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2127
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2126:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24672:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitTestUndetectableEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitTestUndetectableEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder15VisitTestTypeOfEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitTestTypeOfEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitTestTypeOfEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitTestTypeOfEv:
.LFB24675:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	168(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment17LookupAccumulatorEv
	leaq	136(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetFlagOperandEi@PLT
	movzbl	%al, %edi
	call	_ZN2v88internal11interpreter15TestTypeOfFlags6DecodeEh@PLT
	cmpb	$8, %al
	ja	.L2129
	leaq	.L2131(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler20BytecodeGraphBuilder15VisitTestTypeOfEv,"a",@progbits
	.align 4
	.align 4
.L2131:
	.long	.L2139-.L2131
	.long	.L2138-.L2131
	.long	.L2137-.L2131
	.long	.L2136-.L2131
	.long	.L2135-.L2131
	.long	.L2134-.L2131
	.long	.L2133-.L2131
	.long	.L2132-.L2131
	.long	.L2130-.L2131
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder15VisitTestTypeOfEv
	.p2align 4,,10
	.p2align 3
.L2132:
	movq	16(%rbx), %rdi
	leaq	-80(%rbp), %r13
	movq	(%rdi), %r15
	call	_ZN2v88internal8compiler7JSGraph12NullConstantEv@PLT
	movq	%rax, -88(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rsi
	movhps	-88(%rbp), %xmm0
	movl	$2, %edx
	movq	%r15, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	%rax, -88(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder19ObjectIsNonCallableEv@PLT
	movq	%r12, -80(%rbp)
	movq	-96(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r13, %rcx
	movl	$1, %edx
.L2143:
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movl	$8, %esi
	movq	%rax, -96(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r14, -64(%rbp)
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
.L2129:
	movq	168(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.0
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2144
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2139:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ObjectIsNumberEv@PLT
.L2142:
	movq	%rax, %rsi
	leaq	-80(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, %r13
	jmp	.L2129
	.p2align 4,,10
	.p2align 3
.L2138:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ObjectIsStringEv@PLT
	jmp	.L2142
	.p2align 4,,10
	.p2align 3
.L2137:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ObjectIsSymbolEv@PLT
	jmp	.L2142
	.p2align 4,,10
	.p2align 3
.L2136:
	movq	16(%rbx), %rdi
	leaq	-80(%rbp), %r13
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	%rax, -88(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%r12, %xmm0
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movhps	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%rbx, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	16(%rbx), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	%rax, -96(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%r12, %xmm0
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movhps	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%rbx, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	xorl	%edx, %edx
	movl	$8, %esi
	movq	%rax, -96(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SelectENS0_21MachineRepresentationENS1_10BranchHintE@PLT
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movq	%r14, -64(%rbp)
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %r13
	jmp	.L2129
	.p2align 4,,10
	.p2align 3
.L2135:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ObjectIsBigIntEv@PLT
	jmp	.L2142
	.p2align 4,,10
	.p2align 3
.L2134:
	movq	16(%rbx), %rax
	leaq	-80(%rbp), %r13
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20ObjectIsUndetectableEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -88(%rbp)
	movq	(%rdi), %r9
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12NullConstantEv@PLT
	movq	%rax, -96(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movhps	-96(%rbp), %xmm0
	movq	-104(%rbp), %r9
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -80(%rbp)
	jmp	.L2143
	.p2align 4,,10
	.p2align 3
.L2133:
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26ObjectIsDetectableCallableEv@PLT
	movq	%r13, %rdi
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	jmp	.L2129
.L2130:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2144:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24675:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitTestTypeOfEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitTestTypeOfEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder17BuildCastOperatorEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder17BuildCastOperatorEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder17BuildCastOperatorEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder17BuildCastOperatorEPKNS1_8OperatorE:
.LFB24676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %r8
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r8
	jnb	.L2156
	movq	(%rcx,%r8,8), %rax
	movq	%rdi, %r12
	leaq	-64(%rbp), %rdx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	leaq	136(%r12), %rdi
	xorl	%esi, %esi
	movq	168(%r12), %r13
	movq	%rax, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	js	.L2157
	addl	88(%r13), %eax
	movslq	%eax, %r12
.L2148:
	movl	92(%r13), %eax
	movq	(%rbx), %rdi
	movq	0(%r13), %r14
	subl	%r12d, %eax
	movslq	%eax, %r15
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L2158
.L2149:
	movq	48(%r13), %rcx
	movq	56(%r13), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r12
	jnb	.L2159
	movq	%rbx, (%rcx,%r12,8)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2160
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2158:
	.cfi_restore_state
	movl	144(%r14), %esi
	movq	160(%r14), %rdi
	movl	%esi, -84(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%r14), %rdi
	movl	-84(%rbp), %esi
	movq	%r15, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	jmp	.L2149
	.p2align 4,,10
	.p2align 3
.L2157:
	movl	12(%r13), %esi
	leaq	-68(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	movslq	%eax, %r12
	jmp	.L2148
.L2156:
	movq	%r8, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2160:
	call	__stack_chk_fail@PLT
.L2159:
	movq	%r12, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24676:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder17BuildCastOperatorEPKNS1_8OperatorE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder17BuildCastOperatorEPKNS1_8OperatorE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToNumberEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToNumberEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToNumberEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToNumberEv:
.LFB24680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L2172
.L2162:
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2173
	movq	(%rcx,%rsi,8), %r12
	leaq	136(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	168(%rbx), %rdx
	leaq	-64(%rbp), %rdi
	leaq	80(%rbx), %rsi
	movl	%eax, %r9d
	movq	32(%rdx), %rcx
	movq	24(%rdx), %r8
	movq	%r12, %rdx
	call	_ZNK2v88internal8compiler18JSTypeHintLowering23ReduceToNumberOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE@PLT
	movl	-64(%rbp), %eax
	movq	-40(%rbp), %rdx
	cmpl	$2, %eax
	je	.L2174
	cmpl	$1, %eax
	je	.L2175
.L2167:
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8ToNumberEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, %rsi
.L2168:
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2176
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2175:
	.cfi_restore_state
	movq	168(%rbx), %rax
	movq	-48(%rbp), %rcx
	movq	-56(%rbp), %rsi
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L2168
	.p2align 4,,10
	.p2align 3
.L2172:
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2174:
	movq	%rdx, -72(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L2165
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L2166:
	movq	$0, 168(%rbx)
	jmp	.L2167
	.p2align 4,,10
	.p2align 3
.L2165:
	leaq	-72(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2166
.L2176:
	call	__stack_chk_fail@PLT
.L2173:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24680:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToNumberEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToNumberEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder14VisitToNumericEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitToNumericEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitToNumericEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitToNumericEv:
.LFB24681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L2188
.L2178:
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2189
	movq	(%rcx,%rsi,8), %r12
	leaq	136(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	168(%rbx), %rdx
	leaq	-64(%rbp), %rdi
	leaq	80(%rbx), %rsi
	movl	%eax, %r9d
	movq	32(%rdx), %rcx
	movq	24(%rdx), %r8
	movq	%r12, %rdx
	call	_ZNK2v88internal8compiler18JSTypeHintLowering23ReduceToNumberOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE@PLT
	movl	-64(%rbp), %eax
	movq	-40(%rbp), %rdx
	cmpl	$2, %eax
	je	.L2190
	cmpl	$1, %eax
	je	.L2191
.L2183:
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder9ToNumericEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, %rsi
.L2184:
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2192
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2191:
	.cfi_restore_state
	movq	168(%rbx), %rax
	movq	-48(%rbp), %rcx
	movq	-56(%rbp), %rsi
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L2184
	.p2align 4,,10
	.p2align 3
.L2188:
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L2178
	.p2align 4,,10
	.p2align 3
.L2190:
	movq	%rdx, -72(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L2181
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L2182:
	movq	$0, 168(%rbx)
	jmp	.L2183
	.p2align 4,,10
	.p2align 3
.L2181:
	leaq	-72(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2182
.L2192:
	call	__stack_chk_fail@PLT
.L2189:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24681:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitToNumericEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitToNumericEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToStringEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToStringEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToStringEv:
.LFB24679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2197
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movq	(%rcx,%rsi,8), %r12
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8ToStringEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2198
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2197:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2198:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24679:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToStringEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToStringEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateArrayFromIterableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateArrayFromIterableEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateArrayFromIterableEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateArrayFromIterableEv:
.LFB24578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2203
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movq	(%rcx,%rsi,8), %r12
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder23CreateArrayFromIterableEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2204
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2203:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24578:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateArrayFromIterableEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateArrayFromIterableEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16VisitCloneObjectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitCloneObjectEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitCloneObjectEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitCloneObjectEv:
.LFB24584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L2209
.L2206:
	leaq	136(%rbx), %r14
	movq	168(%rbx), %r12
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%r14, %rdi
	movl	$1, %esi
	movq	%rax, %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetFlagOperandEi@PLT
	movq	%r14, %rdi
	movl	$2, %esi
	leaq	-80(%rbp), %r14
	movl	%eax, %r13d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	56(%rbx), %rsi
	movq	64(%rbx), %rdx
	movq	%r14, %rdi
	movl	%eax, %ecx
	movq	16(%rbx), %rax
	movq	368(%rax), %r15
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CloneObjectERKNS1_14FeedbackSourceEi@PLT
	leaq	-64(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -64(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2210
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2209:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L2206
.L2210:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24584:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitCloneObjectEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitCloneObjectEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateCatchContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateCatchContextEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateCatchContextEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateCatchContextEv:
.LFB24566:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	136(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	168(%rbx), %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$1, %esi
	leaq	-64(%rbp), %r13
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	16(%rbx), %rax
	movq	%r13, %rdi
	movq	368(%rax), %r14
	call	_ZNK2v88internal8compiler12ScopeInfoRef6objectEv@PLT
	leaq	-72(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder18CreateCatchContextERKNS0_6HandleINS0_9ScopeInfoEEE@PLT
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -48(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2215
	movq	%rax, (%rcx,%rsi,8)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2216
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2215:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2216:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24566:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateCatchContextEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateCatchContextEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder22VisitCreateWithContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitCreateWithContextEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitCreateWithContextEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitCreateWithContextEv:
.LFB24567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	136(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	168(%rdi), %r12
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%r13, %rdi
	movl	$1, %esi
	leaq	-64(%rbp), %r13
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler12ScopeInfoRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	16(%rbx), %rax
	movq	%r13, %rdi
	movq	368(%rax), %r14
	call	_ZNK2v88internal8compiler12ScopeInfoRef6objectEv@PLT
	leaq	-72(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder17CreateWithContextERKNS0_6HandleINS0_9ScopeInfoEEE@PLT
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -48(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2221
	movq	%rax, (%rcx,%rsi,8)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2222
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2221:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2222:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24567:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitCreateWithContextEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitCreateWithContextEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder19VisitForInEnumerateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitForInEnumerateEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitForInEnumerateEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitForInEnumerateEv:
.LFB24722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$136, %rdi
	subq	$16, %rsp
	movq	32(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder14ForInEnumerateEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2226
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2226:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24722:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitForInEnumerateEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitForInEnumerateEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder17VisitForInPrepareEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitForInPrepareEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitForInPrepareEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitForInPrepareEv:
.LFB24723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L2240
.L2228:
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2241
	leaq	136(%rbx), %r13
	movq	(%rcx,%rsi,8), %r12
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	168(%rbx), %rdx
	leaq	-96(%rbp), %rdi
	leaq	80(%rbx), %rsi
	movl	%eax, %r9d
	movq	32(%rdx), %rcx
	movq	24(%rdx), %r8
	movq	%r12, %rdx
	call	_ZNK2v88internal8compiler18JSTypeHintLowering27ReduceForInPrepareOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE@PLT
	movl	-96(%rbp), %eax
	movq	-72(%rbp), %rdx
	cmpl	$2, %eax
	je	.L2242
	cmpl	$1, %eax
	jne	.L2234
	movq	168(%rbx), %rax
	movq	-80(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
.L2234:
	movq	16(%rbx), %rax
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-112(%rbp), %r15
	movq	368(%rax), %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor14GetSlotOperandEi@PLT
	movq	56(%rbx), %rsi
	movq	64(%rbx), %rdx
	movq	%r15, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker19GetFeedbackForForInERKNS1_14FeedbackSourceE@PLT
	cmpb	$3, %al
	ja	.L2243
	leaq	CSWTCH.612(%rip), %rdx
	movzbl	%al, %eax
	movq	%r14, %rdi
	movzbl	(%rdx,%rax), %esi
	call	_ZN2v88internal8compiler17JSOperatorBuilder12ForInPrepareENS1_9ForInModeE@PLT
	leaq	-64(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -64(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %r14
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment26BindRegistersToProjectionsENS0_11interpreter8RegisterEPNS1_4NodeENS3_24FrameStateAttachmentModeE
.L2227:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2244
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2240:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L2228
	.p2align 4,,10
	.p2align 3
.L2242:
	movq	%rdx, -112(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L2231
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L2232:
	movq	$0, 168(%rbx)
	jmp	.L2227
	.p2align 4,,10
	.p2align 3
.L2231:
	leaq	-112(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2232
.L2244:
	call	__stack_chk_fail@PLT
.L2241:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2243:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24723:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitForInPrepareEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitForInPrepareEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder22VisitSetPendingMessageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitSetPendingMessageEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitSetPendingMessageEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitSetPendingMessageEv:
.LFB24708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11LoadMessageEv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %r12
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2250
	movq	16(%rbx), %rax
	movq	(%rcx,%rsi,8), %r13
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder12StoreMessageEv@PLT
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r13, -48(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2250
	movq	%r12, (%rcx,%rsi,8)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2251
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2250:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2251:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24708:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitSetPendingMessageEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitSetPendingMessageEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder27VisitSwitchOnGeneratorStateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitSwitchOnGeneratorStateEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitSwitchOnGeneratorStateEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitSwitchOnGeneratorStateEv:
.LFB24730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$136, %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	32(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r13, %rdi
	leaq	-64(%rbp), %r13
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	16(%r12), %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rax, -80(%rbp)
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-72(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movhps	-80(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, %rbx
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%r12), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv
	movq	%rax, %rbx
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder28GeneratorRestoreContinuationEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	-72(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%r12), %rdx
	movq	%rax, 80(%rdx)
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder23GeneratorRestoreContextEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	-72(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, 16(%rdx)
	movq	160(%r12), %rax
	xorl	%edx, %edx
	leaq	152(%rax), %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder27BuildSwitchOnGeneratorStateERKNS0_10ZoneVectorINS1_16ResumeJumpTargetEEEb
	movq	16(%r12), %rax
	movq	%rbx, 168(%r12)
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2255
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2255:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24730:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitSwitchOnGeneratorStateEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitSwitchOnGeneratorStateEv
	.section	.rodata._ZN2v88internal8compiler20BytecodeGraphBuilder20VisitResumeGeneratorEv.str1.1,"aMS",@progbits,1
.LC20:
	.string	"0 == first_reg.index()"
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder20VisitResumeGeneratorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitResumeGeneratorEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitResumeGeneratorEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitResumeGeneratorEv:
.LFB24731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	136(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	168(%rdi), %r12
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	testl	%eax, %eax
	jne	.L2279
	movl	144(%r15), %esi
	movq	160(%r15), %rdi
	leaq	-80(%rbp), %r13
	movl	%eax, %ebx
	leaq	-64(%rbp), %r14
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movdqu	40(%r15), %xmm0
	movq	%r13, %rdi
	movq	%rax, %r12
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	movq	%r13, %rdi
	movq	%rdx, -104(%rbp)
	movq	%rax, -112(%rbp)
	movdqa	-112(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeArrayRef15parameter_countEv@PLT
	movq	168(%r15), %rsi
	movl	8(%rsi), %edx
	testl	%edx, %edx
	jle	.L2271
	leal	-1(%rax), %r13d
	testq	%r12, %r12
	je	.L2261
	.p2align 4,,10
	.p2align 3
.L2268:
	cmpl	$1, 12(%r12)
	movq	16(%r12), %rax
	je	.L2267
	movl	%ebx, %edx
	sarl	$6, %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
.L2267:
	btq	%rbx, %rax
	jnc	.L2265
	movq	16(%r15), %rax
	leal	0(%r13,%rbx), %esi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder24GeneratorRestoreRegisterEi@PLT
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	-88(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%r15), %rdx
	movl	88(%rdx), %esi
	movq	48(%rdx), %rdi
	movq	56(%rdx), %rdx
	addl	%ebx, %esi
	subq	%rdi, %rdx
	movslq	%esi, %rsi
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2259
	movq	%rax, (%rdi,%rsi,8)
	movq	168(%r15), %rsi
.L2265:
	addl	$1, %ebx
	cmpl	8(%rsi), %ebx
	jl	.L2268
.L2271:
	movq	16(%r15), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder31GeneratorRestoreInputOrDebugPosEv@PLT
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	-88(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%r15), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2259
	movq	%rax, (%rcx,%rsi,8)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2280
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2261:
	.cfi_restore_state
	movq	16(%r15), %rax
	leal	0(%r13,%rbx), %esi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder24GeneratorRestoreRegisterEi@PLT
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	-88(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%r15), %rdx
	movl	88(%rdx), %esi
	movq	48(%rdx), %rcx
	movq	56(%rdx), %rdx
	addl	%ebx, %esi
	subq	%rcx, %rdx
	movslq	%esi, %rsi
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2259
	movq	%rax, (%rcx,%rsi,8)
	movq	168(%r15), %rax
	addl	$1, %ebx
	cmpl	8(%rax), %ebx
	jl	.L2261
	jmp	.L2271
	.p2align 4,,10
	.p2align 3
.L2279:
	leaq	.LC20(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2259:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2280:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24731:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitResumeGeneratorEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitResumeGeneratorEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16VisitGetIteratorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitGetIteratorEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitGetIteratorEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitGetIteratorEv:
.LFB24727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L2290
.L2282:
	leaq	136(%rbx), %r12
	movq	168(%rbx), %r13
	xorl	%esi, %esi
	leaq	-80(%rbp), %r14
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	64(%rbx), %rdx
	movq	56(%rbx), %rsi
	movq	%r14, %rdi
	movl	%eax, %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	-80(%rbp), %rax
	leaq	-96(%rbp), %rsi
	movq	%rax, -96(%rbp)
	movl	-72(%rbp), %eax
	movl	%eax, -88(%rbp)
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11GetIteratorERKNS1_14FeedbackSourceE@PLT
	subq	$8, %rsp
	leaq	80(%rbx), %rsi
	movq	%r13, %rcx
	movq	%rax, %r12
	movq	168(%rbx), %rax
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	24(%rax), %r9
	movq	32(%rax), %r8
	movl	-88(%rbp), %eax
	pushq	%rax
	call	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE@PLT
	movl	-80(%rbp), %eax
	popq	%rcx
	movq	-56(%rbp), %rdx
	popq	%rsi
	cmpl	$2, %eax
	je	.L2291
	cmpl	$1, %eax
	jne	.L2287
	movq	168(%rbx), %rax
	movq	-64(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movq	168(%rbx), %rax
	movq	%rdx, 24(%rax)
.L2287:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rdx
	movq	%r13, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
.L2281:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2292
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2290:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L2282
	.p2align 4,,10
	.p2align 3
.L2291:
	movq	%rdx, -104(%rbp)
	movq	448(%rbx), %rsi
	cmpq	456(%rbx), %rsi
	je	.L2284
	movq	%rdx, (%rsi)
	addq	$8, 448(%rbx)
.L2285:
	movq	$0, 168(%rbx)
	jmp	.L2281
	.p2align 4,,10
	.p2align 3
.L2284:
	leaq	-104(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2285
.L2292:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24727:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitGetIteratorEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitGetIteratorEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16BuildSwitchOnSmiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildSwitchOnSmiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildSwitchOnSmiEPNS1_4NodeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildSwitchOnSmiEPNS1_4NodeE:
.LFB24705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r15
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-128(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	136(%rdi), %rsi
	movq	%r15, %rdi
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor25GetJumpTableTargetOffsetsEv@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal11interpreter22JumpTableTargetOffsets4sizeEv@PLT
	leal	1(%rax), %esi
	movq	16(%r12), %rax
	movslq	%esi, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6SwitchEm@PLT
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter22JumpTableTargetOffsets5beginEv@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter22JumpTableTargetOffsets3endEv@PLT
	jmp	.L2295
	.p2align 4,,10
	.p2align 3
.L2298:
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratordeEv@PLT
	movq	168(%r12), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv
	movl	%ebx, %esi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r15
	movq	16(%r12), %rax
	sarq	$32, %rbx
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfValueEiiNS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi
	movq	%r15, 168(%r12)
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorppEv@PLT
.L2295:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorneERKS3_@PLT
	testb	%al, %al
	jne	.L2298
	movq	16(%r12), %rax
	xorl	%esi, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9IfDefaultENS1_10BranchHintE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2299
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2299:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24705:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildSwitchOnSmiEPNS1_4NodeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildSwitchOnSmiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder26VisitSwitchOnSmiNoFeedbackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitSwitchOnSmiNoFeedbackEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitSwitchOnSmiNoFeedbackEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitSwitchOnSmiNoFeedbackEv:
.LFB24706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L2305
.L2301:
	movq	168(%r12), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2306
	movq	16(%r12), %rax
	movq	(%rcx,%rsi,8), %rbx
	leaq	-48(%rbp), %rsi
	movq	$0, -48(%rbp)
	movl	$-1, -40(%rbp)
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder8CheckSmiERKNS1_14FeedbackSourceE@PLT
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildSwitchOnSmiEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2307
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2305:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L2301
.L2307:
	call	__stack_chk_fail@PLT
.L2306:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24706:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitSwitchOnSmiNoFeedbackEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitSwitchOnSmiNoFeedbackEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder22CheckContextExtensionsEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder22CheckContextExtensionsEj
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder22CheckContextExtensionsEj, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder22CheckContextExtensionsEj:
.LFB24542:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.L2315
	leal	-1(%rsi), %eax
	movq	%rdi, %rbx
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	movq	%rax, -104(%rbp)
	leaq	-80(%rbp), %r14
	jmp	.L2313
	.p2align 4,,10
	.p2align 3
.L2310:
	movl	144(%rbx), %esi
	movq	160(%rbx), %rdi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movq	168(%rbx), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment5MergeEPS3_PKNS1_21BytecodeLivenessStateE
.L2317:
	movq	16(%rbx), %rax
	movq	%r12, 168(%rbx)
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	leaq	1(%r15), %rax
	cmpq	-104(%rbp), %r15
	je	.L2308
	movq	%rax, %r15
.L2313:
	movq	16(%rbx), %rax
	xorl	%ecx, %ecx
	movl	$2, %edx
	movq	%r15, %rsi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11LoadContextEmmb@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	16(%rbx), %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	movq	%rax, -88(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%r12, -80(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	testq	%r13, %r13
	jne	.L2310
	movq	16(%rbx), %rax
	movl	$1, %esi
	movq	168(%rbx), %r13
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	jmp	.L2317
	.p2align 4,,10
	.p2align 3
.L2315:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L2308:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2318
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2318:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24542:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder22CheckContextExtensionsEj, .-_ZN2v88internal8compiler20BytecodeGraphBuilder22CheckContextExtensionsEj
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder25BuildLdaLookupContextSlotENS0_10TypeofModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder25BuildLdaLookupContextSlotENS0_10TypeofModeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder25BuildLdaLookupContextSlotENS0_10TypeofModeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder25BuildLdaLookupContextSlotENS0_10TypeofModeE:
.LFB24543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	136(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movl	$2, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	movl	%eax, %r12d
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22CheckContextExtensionsEj
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movl	%r12d, %esi
	xorl	%ecx, %ecx
	movl	%eax, %edx
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11LoadContextEmmb@PLT
	movq	168(%rbx), %r12
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	48(%r12), %rcx
	movq	56(%r12), %rdx
	movslq	92(%r12), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2329
	movq	%rax, (%rcx,%rsi,8)
	testq	%r14, %r14
	je	.L2319
	movq	16(%rbx), %rax
	movl	$1, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %r12
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r14, 168(%rbx)
	movq	16(%rbx), %r14
	leaq	-80(%rbp), %r15
	movq	360(%r14), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	xorl	%esi, %esi
	cmpl	$1, %r13d
	movq	%rax, %r14
	movq	16(%rbx), %rax
	setne	%sil
	addl	$303, %esi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	leaq	-64(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r14, -64(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movl	144(%rbx), %esi
	movq	160(%rbx), %rdi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment5MergeEPS3_PKNS1_21BytecodeLivenessStateE
	movq	%r12, 168(%rbx)
	movb	$1, 416(%rbx)
.L2319:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2330
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2329:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2330:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24543:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder25BuildLdaLookupContextSlotENS0_10TypeofModeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder25BuildLdaLookupContextSlotENS0_10TypeofModeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder25VisitLdaLookupContextSlotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitLdaLookupContextSlotEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitLdaLookupContextSlotEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitLdaLookupContextSlotEv:
.LFB24544:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder25BuildLdaLookupContextSlotENS0_10TypeofModeE
	.cfi_endproc
.LFE24544:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitLdaLookupContextSlotEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitLdaLookupContextSlotEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder37VisitLdaLookupContextSlotInsideTypeofEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder37VisitLdaLookupContextSlotInsideTypeofEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder37VisitLdaLookupContextSlotInsideTypeofEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder37VisitLdaLookupContextSlotInsideTypeofEv:
.LFB24545:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder25BuildLdaLookupContextSlotENS0_10TypeofModeE
	.cfi_endproc
.LFE24545:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder37VisitLdaLookupContextSlotInsideTypeofEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder37VisitLdaLookupContextSlotInsideTypeofEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder24BuildLdaLookupGlobalSlotENS0_10TypeofModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder24BuildLdaLookupGlobalSlotENS0_10TypeofModeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder24BuildLdaLookupGlobalSlotENS0_10TypeofModeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder24BuildLdaLookupGlobalSlotENS0_10TypeofModeE:
.LFB24546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	136(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$104, %rsp
	movl	%esi, -132(%rbp)
	movl	$2, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22CheckContextExtensionsEj
	cmpb	$0, 416(%rbx)
	movq	%rax, %r13
	jne	.L2343
.L2334:
	movq	16(%rbx), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-80(%rbp), %r14
	movq	360(%rax), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	leaq	-112(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movq	56(%rbx), %rsi
	movq	64(%rbx), %rdx
	movq	%r14, %rdi
	movdqa	-112(%rbp), %xmm0
	movl	%eax, %ecx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	16(%rbx), %rax
	leaq	-96(%rbp), %rdi
	movq	368(%rax), %r15
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	movl	-132(%rbp), %ecx
	leaq	-120(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler17JSOperatorBuilder10LoadGlobalERKNS0_6HandleINS0_4NameEEERKNS1_14FeedbackSourceENS0_10TypeofModeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	testq	%r13, %r13
	je	.L2333
	movq	16(%rbx), %rax
	movl	$1, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %r15
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r13, 168(%rbx)
	movq	16(%rbx), %r13
	movq	360(%r13), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler7NameRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	xorl	%esi, %esi
	cmpl	$1, -132(%rbp)
	movq	%rax, %r12
	movq	16(%rbx), %rax
	setne	%sil
	addl	$303, %esi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	leaq	-64(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -64(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movl	144(%rbx), %esi
	movq	160(%rbx), %rdi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%rbx), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment5MergeEPS3_PKNS1_21BytecodeLivenessStateE
	movq	%r15, 168(%rbx)
	movb	$1, 416(%rbx)
.L2333:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2344
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2343:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L2334
.L2344:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24546:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder24BuildLdaLookupGlobalSlotENS0_10TypeofModeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder24BuildLdaLookupGlobalSlotENS0_10TypeofModeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder24VisitLdaLookupGlobalSlotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitLdaLookupGlobalSlotEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitLdaLookupGlobalSlotEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitLdaLookupGlobalSlotEv:
.LFB24547:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder24BuildLdaLookupGlobalSlotENS0_10TypeofModeE
	.cfi_endproc
.LFE24547:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitLdaLookupGlobalSlotEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitLdaLookupGlobalSlotEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder36VisitLdaLookupGlobalSlotInsideTypeofEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder36VisitLdaLookupGlobalSlotInsideTypeofEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder36VisitLdaLookupGlobalSlotInsideTypeofEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder36VisitLdaLookupGlobalSlotInsideTypeofEv:
.LFB24548:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder24BuildLdaLookupGlobalSlotENS0_10TypeofModeE
	.cfi_endproc
.LFE24548:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder36VisitLdaLookupGlobalSlotInsideTypeofEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder36VisitLdaLookupGlobalSlotInsideTypeofEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE:
.LFB24743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%r12), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv
	movq	%rax, %rbx
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	leaq	136(%r12), %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi
	movq	16(%r12), %rax
	movq	%rbx, 168(%r12)
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2350
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2350:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24743:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder24BuildJumpIfToBooleanTrueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder24BuildJumpIfToBooleanTrueEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder24BuildJumpIfToBooleanTrueEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder24BuildJumpIfToBooleanTrueEv:
.LFB24749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2355
	movq	16(%rdi), %rax
	movq	%rdi, %r12
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9ToBooleanEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2356
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2355:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2356:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24749:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder24BuildJumpIfToBooleanTrueEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder24BuildJumpIfToBooleanTrueEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder21BuildJumpIfJSReceiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder21BuildJumpIfJSReceiverEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder21BuildJumpIfJSReceiverEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder21BuildJumpIfJSReceiverEv:
.LFB24752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2361
	movq	16(%rdi), %rax
	movq	%rdi, %r12
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16ObjectIsReceiverEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2362
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2361:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2362:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24752:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder21BuildJumpIfJSReceiverEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder21BuildJumpIfJSReceiverEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16BuildJumpIfEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildJumpIfEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildJumpIfEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildJumpIfEqualEPNS1_4NodeE:
.LFB24745:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2367
	movq	16(%rdi), %rax
	movq	(%rcx,%rsi,8), %rbx
	movq	%rdi, %r12
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movhps	-56(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2368
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2367:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2368:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24745:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildJumpIfEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildJumpIfEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder15VisitJumpIfNullEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitJumpIfNullEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitJumpIfNullEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitJumpIfNullEv:
.LFB31482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler7JSGraph12NullConstantEv@PLT
	movq	%rax, -56(%rbp)
	movq	168(%r12), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2373
	movq	16(%r12), %rax
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movhps	-56(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2374
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2373:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2374:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE31482:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitJumpIfNullEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitJumpIfNullEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder20VisitJumpIfUndefinedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitJumpIfUndefinedEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitJumpIfUndefinedEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitJumpIfUndefinedEv:
.LFB31486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rax, -56(%rbp)
	movq	168(%r12), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2379
	movq	16(%r12), %rax
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movhps	-56(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2380
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2379:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2380:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE31486:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitJumpIfUndefinedEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitJumpIfUndefinedEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder26VisitJumpIfUndefinedOrNullEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitJumpIfUndefinedOrNullEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitJumpIfUndefinedOrNullEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitJumpIfUndefinedOrNullEv:
.LFB31490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rax, -72(%rbp)
	movq	168(%r12), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2386
	movq	16(%r12), %rax
	movq	(%rcx,%rsi,8), %rbx
	leaq	-64(%rbp), %r13
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movhps	-72(%rbp), %xmm0
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph12NullConstantEv@PLT
	movq	%rax, -72(%rbp)
	movq	168(%r12), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2386
	movq	16(%r12), %rax
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movhps	-72(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2387
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2386:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2387:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE31490:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitJumpIfUndefinedOrNullEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitJumpIfUndefinedOrNullEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfNullConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfNullConstantEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfNullConstantEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfNullConstantEv:
.LFB24695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler7JSGraph12NullConstantEv@PLT
	movq	%rax, -56(%rbp)
	movq	168(%r12), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2392
	movq	16(%r12), %rax
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movhps	-56(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2393
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2392:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2393:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24695:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfNullConstantEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfNullConstantEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder28VisitJumpIfUndefinedConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitJumpIfUndefinedConstantEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitJumpIfUndefinedConstantEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitJumpIfUndefinedConstantEv:
.LFB24699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rax, -56(%rbp)
	movq	168(%r12), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2398
	movq	16(%r12), %rax
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movhps	-56(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2399
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2398:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24699:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitJumpIfUndefinedConstantEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitJumpIfUndefinedConstantEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder34VisitJumpIfUndefinedOrNullConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder34VisitJumpIfUndefinedOrNullConstantEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder34VisitJumpIfUndefinedOrNullConstantEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder34VisitJumpIfUndefinedOrNullConstantEv:
.LFB24703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rax, -72(%rbp)
	movq	168(%r12), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2405
	movq	16(%r12), %rax
	movq	(%rcx,%rsi,8), %rbx
	leaq	-64(%rbp), %r13
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movhps	-72(%rbp), %xmm0
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph12NullConstantEv@PLT
	movq	%rax, -72(%rbp)
	movq	168(%r12), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2405
	movq	16(%r12), %rax
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movhps	-72(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2406
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2405:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2406:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24703:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder34VisitJumpIfUndefinedOrNullConstantEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder34VisitJumpIfUndefinedOrNullConstantEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder14BuildJumpIfNotEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildJumpIfNotEPNS1_4NodeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildJumpIfNotEPNS1_4NodeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildJumpIfNotEPNS1_4NodeE:
.LFB24744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%r12), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv
	movq	%rax, %rbx
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	leaq	136(%r12), %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi
	movq	16(%r12), %rax
	movq	%rbx, 168(%r12)
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2410
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2410:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24744:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildJumpIfNotEPNS1_4NodeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildJumpIfNotEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder25BuildJumpIfToBooleanFalseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder25BuildJumpIfToBooleanFalseEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder25BuildJumpIfToBooleanFalseEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder25BuildJumpIfToBooleanFalseEv:
.LFB24750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2415
	movq	16(%rdi), %rax
	movq	%rdi, %r12
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9ToBooleanEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildJumpIfNotEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2416
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2415:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2416:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24750:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder25BuildJumpIfToBooleanFalseEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder25BuildJumpIfToBooleanFalseEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder19BuildJumpIfNotEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder19BuildJumpIfNotEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder19BuildJumpIfNotEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder19BuildJumpIfNotEqualEPNS1_4NodeE:
.LFB24746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2421
	movq	16(%rdi), %rax
	movq	(%rcx,%rsi,8), %rbx
	movq	%rdi, %r12
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movhps	-56(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildJumpIfNotEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2422
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2421:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2422:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24746:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder19BuildJumpIfNotEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder19BuildJumpIfNotEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18BuildJumpIfNotHoleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18BuildJumpIfNotHoleEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18BuildJumpIfNotHoleEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18BuildJumpIfNotHoleEv:
.LFB24751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2427
	movq	%rdi, %r12
	movq	16(%rdi), %rdi
	movq	(%rcx,%rsi,8), %rbx
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	movq	%rax, -56(%rbp)
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movhps	-56(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildJumpIfNotEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2428
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2427:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2428:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24751:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18BuildJumpIfNotHoleEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18BuildJumpIfNotHoleEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18VisitJumpIfNotNullEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitJumpIfNotNullEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitJumpIfNotNullEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitJumpIfNotNullEv:
.LFB31484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler7JSGraph12NullConstantEv@PLT
	movq	%rax, -56(%rbp)
	movq	168(%r12), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2433
	movq	16(%r12), %rax
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movhps	-56(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildJumpIfNotEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2434
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2433:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2434:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE31484:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitJumpIfNotNullEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitJumpIfNotNullEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfNotUndefinedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfNotUndefinedEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfNotUndefinedEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfNotUndefinedEv:
.LFB31488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rax, -56(%rbp)
	movq	168(%r12), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2439
	movq	16(%r12), %rax
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movhps	-56(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildJumpIfNotEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2440
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2439:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2440:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE31488:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfNotUndefinedEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfNotUndefinedEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder26VisitJumpIfNotNullConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitJumpIfNotNullConstantEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitJumpIfNotNullConstantEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitJumpIfNotNullConstantEv:
.LFB24697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler7JSGraph12NullConstantEv@PLT
	movq	%rax, -56(%rbp)
	movq	168(%r12), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2445
	movq	16(%r12), %rax
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movhps	-56(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildJumpIfNotEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2446
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2445:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2446:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24697:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitJumpIfNotNullConstantEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitJumpIfNotNullConstantEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder31VisitJumpIfNotUndefinedConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitJumpIfNotUndefinedConstantEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitJumpIfNotUndefinedConstantEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitJumpIfNotUndefinedConstantEv:
.LFB24701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rax, -56(%rbp)
	movq	168(%r12), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2451
	movq	16(%r12), %rax
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movhps	-56(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildJumpIfNotEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2452
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2451:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2452:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24701:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitJumpIfNotUndefinedConstantEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitJumpIfNotUndefinedConstantEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16BuildJumpIfFalseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildJumpIfFalseEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildJumpIfFalseEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildJumpIfFalseEv:
.LFB24747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2460
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movq	(%rcx,%rsi,8), %r12
	movl	$2, %edx
	xorl	%esi, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -48(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %r13
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	48(%r13), %rcx
	movq	56(%r13), %rdx
	movslq	92(%r13), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2460
	movq	%rax, (%rcx,%rsi,8)
	leaq	136(%rbx), %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi
	movq	16(%rbx), %rax
	movq	%r12, 168(%rbx)
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %r12
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	48(%r12), %rcx
	movq	56(%r12), %rdx
	movslq	92(%r12), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2460
	movq	%rax, (%rcx,%rsi,8)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2461
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2460:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2461:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24747:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildJumpIfFalseEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildJumpIfFalseEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder24VisitJumpIfFalseConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitJumpIfFalseConstantEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitJumpIfFalseConstantEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitJumpIfFalseConstantEv:
.LFB24687:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildJumpIfFalseEv
	.cfi_endproc
.LFE24687:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitJumpIfFalseConstantEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitJumpIfFalseConstantEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder16VisitJumpIfFalseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitJumpIfFalseEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitJumpIfFalseEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitJumpIfFalseEv:
.LFB31494:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildJumpIfFalseEv
	.cfi_endproc
.LFE31494:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitJumpIfFalseEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitJumpIfFalseEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder15BuildJumpIfTrueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildJumpIfTrueEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildJumpIfTrueEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildJumpIfTrueEv:
.LFB24748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2471
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movq	(%rcx,%rsi,8), %r12
	movl	$2, %edx
	xorl	%esi, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -48(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %r13
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	48(%r13), %rcx
	movq	56(%r13), %rdx
	movslq	92(%r13), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2471
	movq	%rax, (%rcx,%rsi,8)
	leaq	136(%rbx), %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi
	movq	16(%rbx), %rax
	movq	%r12, 168(%rbx)
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %r12
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	48(%r12), %rcx
	movq	56(%r12), %rdx
	movslq	92(%r12), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2471
	movq	%rax, (%rcx,%rsi,8)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2472
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2471:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2472:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24748:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildJumpIfTrueEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildJumpIfTrueEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfTrueConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfTrueConstantEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfTrueConstantEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfTrueConstantEv:
.LFB24685:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildJumpIfTrueEv
	.cfi_endproc
.LFE24685:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfTrueConstantEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfTrueConstantEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder15VisitJumpIfTrueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitJumpIfTrueEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitJumpIfTrueEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitJumpIfTrueEv:
.LFB31492:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildJumpIfTrueEv
	.cfi_endproc
.LFE31492:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitJumpIfTrueEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitJumpIfTrueEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11VisitToNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitToNameEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitToNameEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitToNameEv:
.LFB24677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder6ToNameEv@PLT
	movq	168(%r12), %rdx
	movq	%rax, %rsi
	movslq	92(%rdx), %r8
	movq	48(%rdx), %rax
	movq	56(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r8
	jnb	.L2486
	movq	(%rax,%r8,8), %rax
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	leaq	136(%r12), %rdi
	xorl	%esi, %esi
	movq	168(%r12), %r13
	movq	%rax, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	js	.L2487
	addl	88(%r13), %eax
	movslq	%eax, %r12
.L2478:
	movl	92(%r13), %eax
	movq	(%rbx), %rdi
	movq	0(%r13), %r14
	subl	%r12d, %eax
	movslq	%eax, %r15
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L2488
.L2479:
	movq	48(%r13), %rcx
	movq	56(%r13), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r12
	jnb	.L2489
	movq	%rbx, (%rcx,%r12,8)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2490
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2488:
	.cfi_restore_state
	movl	144(%r14), %esi
	movq	160(%r14), %rdi
	movl	%esi, -84(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%r14), %rdi
	movl	-84(%rbp), %esi
	movq	%r15, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	jmp	.L2479
	.p2align 4,,10
	.p2align 3
.L2487:
	movl	12(%r13), %esi
	leaq	-68(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	movslq	%eax, %r12
	jmp	.L2478
.L2486:
	movq	%r8, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2490:
	call	__stack_chk_fail@PLT
.L2489:
	movq	%r12, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24677:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitToNameEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitToNameEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToObjectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToObjectEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToObjectEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToObjectEv:
.LFB24678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8ToObjectEv@PLT
	movq	168(%r12), %rdx
	movq	%rax, %rsi
	movslq	92(%rdx), %r8
	movq	48(%rdx), %rax
	movq	56(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r8
	jnb	.L2502
	movq	(%rax,%r8,8), %rax
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	leaq	136(%r12), %rdi
	xorl	%esi, %esi
	movq	168(%r12), %r13
	movq	%rax, %rbx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	js	.L2503
	addl	88(%r13), %eax
	movslq	%eax, %r12
.L2494:
	movl	92(%r13), %eax
	movq	(%rbx), %rdi
	movq	0(%r13), %r14
	subl	%r12d, %eax
	movslq	%eax, %r15
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L2504
.L2495:
	movq	48(%r13), %rcx
	movq	56(%r13), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r12
	jnb	.L2505
	movq	%rbx, (%rcx,%r12,8)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2506
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2504:
	.cfi_restore_state
	movl	144(%r14), %esi
	movq	160(%r14), %rdi
	movl	%esi, -84(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%r14), %rdi
	movl	-84(%rbp), %esi
	movq	%r15, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	jmp	.L2495
	.p2align 4,,10
	.p2align 3
.L2503:
	movl	12(%r13), %esi
	leaq	-68(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	movslq	%eax, %r12
	jmp	.L2494
.L2502:
	movq	%r8, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2506:
	call	__stack_chk_fail@PLT
.L2505:
	movq	%r12, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24678:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToObjectEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToObjectEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder21VisitJumpIfJSReceiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitJumpIfJSReceiverEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitJumpIfJSReceiverEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitJumpIfJSReceiverEv:
.LFB31496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2511
	movq	16(%rdi), %rax
	movq	%rdi, %r12
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16ObjectIsReceiverEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2512
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2511:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2512:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE31496:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitJumpIfJSReceiverEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitJumpIfJSReceiverEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder32VisitJumpIfToBooleanTrueConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder32VisitJumpIfToBooleanTrueConstantEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder32VisitJumpIfToBooleanTrueConstantEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder32VisitJumpIfToBooleanTrueConstantEv:
.LFB24689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2517
	movq	16(%rdi), %rax
	movq	%rdi, %r12
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9ToBooleanEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2518
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2517:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2518:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24689:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder32VisitJumpIfToBooleanTrueConstantEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder32VisitJumpIfToBooleanTrueConstantEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder29VisitJumpIfJSReceiverConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitJumpIfJSReceiverConstantEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitJumpIfJSReceiverConstantEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitJumpIfJSReceiverConstantEv:
.LFB24693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2523
	movq	16(%rdi), %rax
	movq	%rdi, %r12
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder16ObjectIsReceiverEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2524
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2523:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2524:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24693:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitJumpIfJSReceiverConstantEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitJumpIfJSReceiverConstantEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder33VisitJumpIfToBooleanFalseConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder33VisitJumpIfToBooleanFalseConstantEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder33VisitJumpIfToBooleanFalseConstantEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder33VisitJumpIfToBooleanFalseConstantEv:
.LFB24691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2529
	movq	16(%rdi), %rax
	movq	%rdi, %r12
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9ToBooleanEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildJumpIfNotEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2530
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2529:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2530:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24691:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder33VisitJumpIfToBooleanFalseConstantEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder33VisitJumpIfToBooleanFalseConstantEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder24VisitJumpIfToBooleanTrueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitJumpIfToBooleanTrueEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitJumpIfToBooleanTrueEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitJumpIfToBooleanTrueEv:
.LFB31498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2535
	movq	16(%rdi), %rax
	movq	%rdi, %r12
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9ToBooleanEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildJumpIfEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2536
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2535:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2536:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE31498:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitJumpIfToBooleanTrueEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitJumpIfToBooleanTrueEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder25VisitJumpIfToBooleanFalseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitJumpIfToBooleanFalseEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitJumpIfToBooleanFalseEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitJumpIfToBooleanFalseEv:
.LFB31500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2541
	movq	16(%rdi), %rax
	movq	%rdi, %r12
	movq	(%rcx,%rsi,8), %rbx
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9ToBooleanEv@PLT
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder14BuildJumpIfNotEPNS1_4NodeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2542
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2541:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2542:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE31500:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitJumpIfToBooleanFalseEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder25VisitJumpIfToBooleanFalseEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateRestParameterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateRestParameterEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateRestParameterEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateRestParameterEv:
.LFB24571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder15CreateArgumentsENS0_19CreateArgumentsTypeE@PLT
	movq	%rax, %r12
	movq	424(%rbx), %rax
	testq	%rax, %rax
	je	.L2548
.L2544:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2549
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2548:
	.cfi_restore_state
	movq	16(%rbx), %rax
	leaq	.LC16(%rip), %rdx
	movl	$-1, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, 424(%rbx)
	jmp	.L2544
.L2549:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24571:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateRestParameterEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateRestParameterEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateUnmappedArgumentsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateUnmappedArgumentsEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateUnmappedArgumentsEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateUnmappedArgumentsEv:
.LFB24570:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder15CreateArgumentsENS0_19CreateArgumentsTypeE@PLT
	movq	%rax, %r12
	movq	424(%rbx), %rax
	testq	%rax, %rax
	je	.L2555
.L2551:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2556
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2555:
	.cfi_restore_state
	movq	16(%rbx), %rax
	leaq	.LC16(%rip), %rdx
	movl	$-1, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, 424(%rbx)
	jmp	.L2551
.L2556:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24570:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateUnmappedArgumentsEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateUnmappedArgumentsEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCreateMappedArgumentsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCreateMappedArgumentsEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCreateMappedArgumentsEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCreateMappedArgumentsEv:
.LFB24569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder15CreateArgumentsENS0_19CreateArgumentsTypeE@PLT
	movq	%rax, %r12
	movq	424(%rbx), %rax
	testq	%rax, %rax
	je	.L2562
.L2558:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2563
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2562:
	.cfi_restore_state
	movq	16(%rbx), %rax
	leaq	.LC16(%rip), %rdx
	movl	$-1, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, 424(%rbx)
	jmp	.L2558
.L2563:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24569:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCreateMappedArgumentsEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCreateMappedArgumentsEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder29VisitCreateEmptyObjectLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitCreateEmptyObjectLiteralEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitCreateEmptyObjectLiteralEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitCreateEmptyObjectLiteralEv:
.LFB24583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	424(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	testq	%r12, %r12
	je	.L2570
.L2565:
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder24CreateEmptyLiteralObjectEv@PLT
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%r12, -48(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdx
	movq	48(%rdx), %rcx
	movslq	92(%rdx), %rsi
	movq	56(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2571
	movq	%rax, (%rcx,%rsi,8)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2572
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2570:
	.cfi_restore_state
	movq	8(%rax), %rdi
	leaq	.LC16(%rip), %rdx
	movl	$-1, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, 424(%rbx)
	movq	%rax, %r12
	movq	16(%rbx), %rax
	jmp	.L2565
.L2571:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2572:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24583:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitCreateEmptyObjectLiteralEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitCreateEmptyObjectLiteralEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder20BuildCreateArgumentsENS0_19CreateArgumentsTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder20BuildCreateArgumentsENS0_19CreateArgumentsTypeE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder20BuildCreateArgumentsENS0_19CreateArgumentsTypeE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder20BuildCreateArgumentsENS0_19CreateArgumentsTypeE:
.LFB24568:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder15CreateArgumentsENS0_19CreateArgumentsTypeE@PLT
	movq	%rax, %r12
	movq	424(%rbx), %rax
	testq	%rax, %rax
	je	.L2578
.L2574:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2579
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2578:
	.cfi_restore_state
	movq	16(%rbx), %rax
	leaq	.LC16(%rip), %rdx
	movl	$-1, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, 424(%rbx)
	jmp	.L2574
.L2579:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24568:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder20BuildCreateArgumentsENS0_19CreateArgumentsTypeE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder20BuildCreateArgumentsENS0_19CreateArgumentsTypeE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18VisitLdaLookupSlotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitLdaLookupSlotEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitLdaLookupSlotEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitLdaLookupSlotEv:
.LFB24540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L2584
.L2581:
	movq	16(%rbx), %r12
	xorl	%esi, %esi
	leaq	136(%rbx), %rdi
	leaq	-64(%rbp), %r13
	movq	360(%r12), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movl	$303, %esi
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -48(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2585
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2584:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L2581
.L2585:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24540:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitLdaLookupSlotEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitLdaLookupSlotEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder30VisitLdaLookupSlotInsideTypeofEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitLdaLookupSlotInsideTypeofEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitLdaLookupSlotInsideTypeofEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitLdaLookupSlotInsideTypeofEv:
.LFB24541:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rdi)
	jne	.L2590
.L2587:
	movq	16(%rbx), %r12
	xorl	%esi, %esi
	leaq	136(%rbx), %rdi
	leaq	-64(%rbp), %r13
	movq	360(%r12), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%rbx), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movl	$304, %esi
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -48(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2591
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2590:
	.cfi_restore_state
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22PrepareEagerCheckpointEv.part.0
	jmp	.L2587
.L2591:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24541:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitLdaLookupSlotInsideTypeofEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitLdaLookupSlotInsideTypeofEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder20VisitIncBlockCounterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitIncBlockCounterEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitIncBlockCounterEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitIncBlockCounterEv:
.LFB24721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	424(%rdi), %rbx
	movq	16(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L2599
.L2593:
	xorl	%esi, %esi
	leaq	136(%r12), %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movl	%eax, %eax
	cvtsi2sdq	%rax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$468, %esi
	movq	%rax, -72(%rbp)
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movhps	-72(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2600
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2599:
	.cfi_restore_state
	movq	8(%r13), %rdi
	leaq	.LC16(%rip), %rdx
	movl	$-1, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	16(%r12), %r13
	movq	%rax, 424(%r12)
	movq	%rax, %rbx
	jmp	.L2593
.L2600:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24721:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitIncBlockCounterEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitIncBlockCounterEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder18ProcessCallVarArgsENS0_19ConvertReceiverModeEPNS1_4NodeENS0_11interpreter8RegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder18ProcessCallVarArgsENS0_19ConvertReceiverModeEPNS1_4NodeENS0_11interpreter8RegisterEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder18ProcessCallVarArgsENS0_19ConvertReceiverModeEPNS1_4NodeENS0_11interpreter8RegisterEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder18ProcessCallVarArgsENS0_19ConvertReceiverModeEPNS1_4NodeENS0_11interpreter8RegisterEi:
.LFB24590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$72, %rsp
	movq	%rdx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jne	.L2602
	movq	16(%rdi), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rax, -88(%rbp)
.L2603:
	movq	8(%r12), %rdi
	leal	2(%r14), %esi
	movslq	%esi, %rsi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	salq	$3, %rsi
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L2618
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L2605:
	movq	-96(%rbp), %xmm0
	movhps	-88(%rbp), %xmm0
	movups	%xmm0, (%r15)
	testl	%r14d, %r14d
	jle	.L2601
	leaq	16(%r15), %rax
	movq	%rax, -88(%rbp)
	leal	(%r14,%rbx), %eax
	leaq	-72(%rbp), %r14
	movl	%eax, -96(%rbp)
	leaq	-68(%rbp), %rax
	movq	%rax, -104(%rbp)
	jmp	.L2615
	.p2align 4,,10
	.p2align 3
.L2619:
	movq	16(%r13), %rax
.L2608:
	movq	-88(%rbp), %rcx
	addl	$1, %ebx
	movq	%rax, (%rcx)
	addq	$8, %rcx
	movq	%rcx, -88(%rbp)
	cmpl	%ebx, -96(%rbp)
	je	.L2601
.L2615:
	movq	%r14, %rdi
	movl	%ebx, -72(%rbp)
	movq	168(%r12), %r13
	call	_ZNK2v88internal11interpreter8Register18is_current_contextEv@PLT
	testb	%al, %al
	jne	.L2619
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter8Register19is_function_closureEv@PLT
	testb	%al, %al
	jne	.L2620
	movl	-72(%rbp), %eax
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	js	.L2621
	addl	88(%r13), %eax
.L2612:
	movq	48(%r13), %rcx
	movq	56(%r13), %rdx
	cltq
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L2622
	movq	(%rcx,%rax,8), %rax
	jmp	.L2608
	.p2align 4,,10
	.p2align 3
.L2601:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2623
	addq	$72, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2620:
	.cfi_restore_state
	movq	0(%r13), %r13
	movq	424(%r13), %rax
	testq	%rax, %rax
	jne	.L2608
	movq	16(%r13), %rax
	leaq	.LC16(%rip), %rdx
	movl	$-1, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	leaq	-64(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, 424(%r13)
	jmp	.L2608
	.p2align 4,,10
	.p2align 3
.L2602:
	movq	168(%rdi), %rdi
	movl	%ecx, %esi
	addl	$1, %ebx
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%rax, -88(%rbp)
	jmp	.L2603
	.p2align 4,,10
	.p2align 3
.L2621:
	movl	12(%r13), %esi
	movq	-104(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	jmp	.L2612
	.p2align 4,,10
	.p2align 3
.L2618:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L2605
.L2622:
	movq	%rax, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2623:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24590:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder18ProcessCallVarArgsENS0_19ConvertReceiverModeEPNS1_4NodeENS0_11interpreter8RegisterEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder18ProcessCallVarArgsENS0_19ConvertReceiverModeEPNS1_4NodeENS0_11interpreter8RegisterEi
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder29GetCallArgumentsFromRegistersEPNS1_4NodeES4_NS0_11interpreter8RegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder29GetCallArgumentsFromRegistersEPNS1_4NodeES4_NS0_11interpreter8RegisterEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder29GetCallArgumentsFromRegistersEPNS1_4NodeES4_NS0_11interpreter8RegisterEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder29GetCallArgumentsFromRegistersEPNS1_4NodeES4_NS0_11interpreter8RegisterEi:
.LFB24586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	8(%rdi), %rdi
	leal	2(%r8), %esi
	movq	%rdx, -104(%rbp)
	movslq	%esi, %rsi
	salq	$3, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%rax, %rcx
	movq	%rax, -96(%rbp)
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L2639
	addq	-96(%rbp), %rsi
	movq	%rcx, %rax
	movq	%rsi, 16(%rdi)
.L2626:
	movq	-88(%rbp), %xmm0
	movhps	-104(%rbp), %xmm0
	movups	%xmm0, (%rax)
	testl	%r8d, %r8d
	jle	.L2624
	leaq	16(%rax), %rbx
	leal	(%r8,%r14), %eax
	movl	%eax, -88(%rbp)
	leaq	-68(%rbp), %rax
	leaq	-72(%rbp), %r15
	movq	%rax, -104(%rbp)
	jmp	.L2636
	.p2align 4,,10
	.p2align 3
.L2640:
	movq	16(%r12), %rax
.L2629:
	movq	%rax, (%rbx)
	addl	$1, %r14d
	addq	$8, %rbx
	cmpl	%r14d, -88(%rbp)
	je	.L2624
.L2636:
	movq	%r15, %rdi
	movl	%r14d, -72(%rbp)
	movq	168(%r13), %r12
	call	_ZNK2v88internal11interpreter8Register18is_current_contextEv@PLT
	testb	%al, %al
	jne	.L2640
	movq	%r15, %rdi
	call	_ZNK2v88internal11interpreter8Register19is_function_closureEv@PLT
	testb	%al, %al
	jne	.L2641
	movl	-72(%rbp), %eax
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	js	.L2642
	addl	88(%r12), %eax
.L2633:
	movq	48(%r12), %rsi
	movq	56(%r12), %rdx
	cltq
	subq	%rsi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L2643
	movq	(%rsi,%rax,8), %rax
	jmp	.L2629
	.p2align 4,,10
	.p2align 3
.L2624:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2644
	movq	-96(%rbp), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2641:
	.cfi_restore_state
	movq	(%r12), %r12
	movq	424(%r12), %rax
	testq	%rax, %rax
	jne	.L2629
	movq	16(%r12), %rax
	leaq	.LC16(%rip), %rdx
	movl	$-1, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, 424(%r12)
	jmp	.L2629
	.p2align 4,,10
	.p2align 3
.L2642:
	movl	12(%r12), %esi
	movq	-104(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	jmp	.L2633
	.p2align 4,,10
	.p2align 3
.L2639:
	movl	%r8d, -108(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-108(%rbp), %r8d
	movq	%rax, -96(%rbp)
	jmp	.L2626
.L2643:
	movq	%rax, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2644:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24586:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder29GetCallArgumentsFromRegistersEPNS1_4NodeES4_NS0_11interpreter8RegisterEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder29GetCallArgumentsFromRegistersEPNS1_4NodeES4_NS0_11interpreter8RegisterEi
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder20ProcessCallArgumentsEPKNS1_8OperatorEPNS1_4NodeENS0_11interpreter8RegisterEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder20ProcessCallArgumentsEPKNS1_8OperatorEPNS1_4NodeENS0_11interpreter8RegisterEm
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder20ProcessCallArgumentsEPKNS1_8OperatorEPNS1_4NodeENS0_11interpreter8RegisterEm, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder20ProcessCallArgumentsEPKNS1_8OperatorEPNS1_4NodeENS0_11interpreter8RegisterEm:
.LFB24588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leal	1(%r15), %ebx
	subq	$88, %rsp
	movq	%rsi, -112(%rbp)
	movl	%ecx, %esi
	movq	168(%rdi), %rdi
	movq	%r8, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	-104(%rbp), %r8
	movq	8(%r12), %rdi
	movq	%rax, -88(%rbp)
	leal	1(%r8), %eax
	movq	16(%rdi), %r14
	movslq	%eax, %rsi
	movl	%eax, -104(%rbp)
	movq	24(%rdi), %rax
	salq	$3, %rsi
	subq	%r14, %rax
	cmpq	%rax, %rsi
	ja	.L2661
	addq	%r14, %rsi
	movq	%rsi, 16(%rdi)
.L2647:
	movq	-96(%rbp), %xmm0
	movhps	-88(%rbp), %xmm0
	movups	%xmm0, (%r14)
	cmpl	$1, %r8d
	jle	.L2656
	leaq	16(%r14), %rax
	movq	%rax, -88(%rbp)
	leal	(%r15,%r8), %eax
	leaq	-72(%rbp), %r15
	movl	%eax, -96(%rbp)
	leaq	-68(%rbp), %rax
	movq	%rax, -120(%rbp)
	jmp	.L2657
	.p2align 4,,10
	.p2align 3
.L2662:
	movq	16(%r13), %rax
.L2650:
	movq	-88(%rbp), %rcx
	addl	$1, %ebx
	movq	%rax, (%rcx)
	addq	$8, %rcx
	movq	%rcx, -88(%rbp)
	cmpl	-96(%rbp), %ebx
	je	.L2656
.L2657:
	movq	%r15, %rdi
	movl	%ebx, -72(%rbp)
	movq	168(%r12), %r13
	call	_ZNK2v88internal11interpreter8Register18is_current_contextEv@PLT
	testb	%al, %al
	jne	.L2662
	movq	%r15, %rdi
	call	_ZNK2v88internal11interpreter8Register19is_function_closureEv@PLT
	testb	%al, %al
	jne	.L2663
	movl	-72(%rbp), %eax
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	js	.L2664
	addl	88(%r13), %eax
.L2654:
	movq	48(%r13), %rsi
	movq	56(%r13), %rdx
	cltq
	subq	%rsi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L2665
	movq	(%rsi,%rax,8), %rax
	jmp	.L2650
	.p2align 4,,10
	.p2align 3
.L2656:
	movl	-104(%rbp), %edx
	movq	-112(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2666
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2663:
	.cfi_restore_state
	movq	0(%r13), %r13
	movq	424(%r13), %rax
	testq	%rax, %rax
	jne	.L2650
	movq	16(%r13), %rax
	leaq	.LC16(%rip), %rdx
	movl	$-1, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	leaq	-64(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, 424(%r13)
	jmp	.L2650
	.p2align 4,,10
	.p2align 3
.L2664:
	movl	12(%r13), %esi
	movq	-120(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	jmp	.L2654
	.p2align 4,,10
	.p2align 3
.L2661:
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %r8
	movq	%rax, %r14
	jmp	.L2647
.L2665:
	movq	%rax, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2666:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24588:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder20ProcessCallArgumentsEPKNS1_8OperatorEPNS1_4NodeENS0_11interpreter8RegisterEm, .-_ZN2v88internal8compiler20BytecodeGraphBuilder20ProcessCallArgumentsEPKNS1_8OperatorEPNS1_4NodeENS0_11interpreter8RegisterEm
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder33GetConstructArgumentsFromRegisterEPNS1_4NodeES4_NS0_11interpreter8RegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder33GetConstructArgumentsFromRegisterEPNS1_4NodeES4_NS0_11interpreter8RegisterEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder33GetConstructArgumentsFromRegisterEPNS1_4NodeES4_NS0_11interpreter8RegisterEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder33GetConstructArgumentsFromRegisterEPNS1_4NodeES4_NS0_11interpreter8RegisterEi:
.LFB24607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%rdx, -112(%rbp)
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	2(%r8), %eax
	cltq
	salq	$3, %rax
	movq	%rax, -104(%rbp)
	movq	%rax, %rsi
	movq	16(%rdi), %rax
	movq	%rax, %rcx
	movq	%rax, -96(%rbp)
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L2683
	addq	-96(%rbp), %rsi
	movq	%rcx, %rax
	movq	%rsi, 16(%rdi)
.L2669:
	movq	%rbx, (%rax)
	testl	%r8d, %r8d
	jle	.L2678
	leaq	8(%rax), %rbx
	leal	(%r8,%r14), %eax
	movl	%eax, -84(%rbp)
	leaq	-68(%rbp), %rax
	leaq	-72(%rbp), %r15
	movq	%rax, -120(%rbp)
	jmp	.L2679
	.p2align 4,,10
	.p2align 3
.L2684:
	movq	16(%r12), %rax
.L2672:
	movq	%rax, (%rbx)
	addl	$1, %r14d
	addq	$8, %rbx
	cmpl	%r14d, -84(%rbp)
	je	.L2678
.L2679:
	movq	%r15, %rdi
	movl	%r14d, -72(%rbp)
	movq	168(%r13), %r12
	call	_ZNK2v88internal11interpreter8Register18is_current_contextEv@PLT
	testb	%al, %al
	jne	.L2684
	movq	%r15, %rdi
	call	_ZNK2v88internal11interpreter8Register19is_function_closureEv@PLT
	testb	%al, %al
	jne	.L2685
	movl	-72(%rbp), %eax
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	js	.L2686
	addl	88(%r12), %eax
.L2676:
	movq	48(%r12), %rsi
	movq	56(%r12), %rdx
	cltq
	subq	%rsi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L2687
	movq	(%rsi,%rax,8), %rax
	jmp	.L2672
	.p2align 4,,10
	.p2align 3
.L2678:
	movq	-96(%rbp), %rax
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	%rdx, -8(%rax,%rcx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2688
	movq	-96(%rbp), %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2685:
	.cfi_restore_state
	movq	(%r12), %r12
	movq	424(%r12), %rax
	testq	%rax, %rax
	jne	.L2672
	movq	16(%r12), %rax
	leaq	.LC16(%rip), %rdx
	movl	$-1, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, 424(%r12)
	jmp	.L2672
	.p2align 4,,10
	.p2align 3
.L2686:
	movl	12(%r12), %esi
	movq	-120(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	jmp	.L2676
	.p2align 4,,10
	.p2align 3
.L2683:
	movl	%r8d, -84(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-84(%rbp), %r8d
	movq	%rax, -96(%rbp)
	jmp	.L2669
.L2687:
	movq	%rax, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2688:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24607:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder33GetConstructArgumentsFromRegisterEPNS1_4NodeES4_NS0_11interpreter8RegisterEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder33GetConstructArgumentsFromRegisterEPNS1_4NodeES4_NS0_11interpreter8RegisterEi
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder27ProcessCallRuntimeArgumentsEPKNS1_8OperatorENS0_11interpreter8RegisterEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder27ProcessCallRuntimeArgumentsEPKNS1_8OperatorENS0_11interpreter8RegisterEm
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder27ProcessCallRuntimeArgumentsEPKNS1_8OperatorENS0_11interpreter8RegisterEm, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder27ProcessCallRuntimeArgumentsEPKNS1_8OperatorENS0_11interpreter8RegisterEm:
.LFB24604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -112(%rbp)
	movq	8(%rdi), %rdi
	movslq	%ecx, %rsi
	movq	%rcx, -96(%rbp)
	salq	$3, %rsi
	movq	16(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	%rax, %rsi
	ja	.L2706
	addq	%r14, %rsi
	movq	%rcx, %rax
	movq	%rsi, 16(%rdi)
.L2691:
	movl	%eax, %ecx
	movq	%r14, -104(%rbp)
	leaq	-72(%rbp), %r13
	addl	%r15d, %ecx
	movl	%ecx, -84(%rbp)
	leaq	-68(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	testl	%eax, %eax
	jg	.L2701
	jmp	.L2700
	.p2align 4,,10
	.p2align 3
.L2707:
	movq	16(%rbx), %rax
.L2694:
	movq	%rax, (%r14)
	addl	$1, %r15d
	addq	$8, %r14
	cmpl	%r15d, -84(%rbp)
	je	.L2700
.L2701:
	movq	%r13, %rdi
	movl	%r15d, -72(%rbp)
	movq	168(%r12), %rbx
	call	_ZNK2v88internal11interpreter8Register18is_current_contextEv@PLT
	testb	%al, %al
	jne	.L2707
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter8Register19is_function_closureEv@PLT
	testb	%al, %al
	jne	.L2708
	movl	-72(%rbp), %eax
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	js	.L2709
	addl	88(%rbx), %eax
.L2698:
	movq	48(%rbx), %rsi
	movq	56(%rbx), %rdx
	cltq
	subq	%rsi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L2710
	movq	(%rsi,%rax,8), %rax
	jmp	.L2694
	.p2align 4,,10
	.p2align 3
.L2700:
	movq	-104(%rbp), %rcx
	movl	-96(%rbp), %edx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	-112(%rbp), %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2711
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2708:
	.cfi_restore_state
	movq	(%rbx), %rbx
	movq	424(%rbx), %rax
	testq	%rax, %rax
	jne	.L2694
	movq	16(%rbx), %rax
	leaq	.LC16(%rip), %rdx
	movl	$-1, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	leaq	-64(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, 424(%rbx)
	jmp	.L2694
	.p2align 4,,10
	.p2align 3
.L2709:
	movl	12(%rbx), %esi
	movq	-120(%rbp), %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	jmp	.L2698
	.p2align 4,,10
	.p2align 3
.L2706:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	movq	-96(%rbp), %rax
	jmp	.L2691
.L2710:
	movq	%rax, %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2711:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24604:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder27ProcessCallRuntimeArgumentsEPKNS1_8OperatorENS0_11interpreter8RegisterEm, .-_ZN2v88internal8compiler20BytecodeGraphBuilder27ProcessCallRuntimeArgumentsEPKNS1_8OperatorENS0_11interpreter8RegisterEm
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder9VisitJumpEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitJumpEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitJumpEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitJumpEv:
.LFB31456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	136(%rdi), %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi
	.cfi_endproc
.LFE31456:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitJumpEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitJumpEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder17VisitJumpConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitJumpConstantEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitJumpConstantEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitJumpConstantEv:
.LFB31458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	136(%rdi), %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi
	.cfi_endproc
.LFE31458:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitJumpConstantEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitJumpConstantEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder13VisitJumpLoopEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitJumpLoopEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitJumpLoopEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitJumpLoopEv:
.LFB31454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	136(%rdi), %rdi
	subq	$8, %rsp
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi
	.cfi_endproc
.LFE31454:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitJumpLoopEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitJumpLoopEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder29BuildLoopExitsForFunctionExitEPKNS1_21BytecodeLivenessStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder29BuildLoopExitsForFunctionExitEPKNS1_21BytecodeLivenessStateE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder29BuildLoopExitsForFunctionExitEPKNS1_21BytecodeLivenessStateE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder29BuildLoopExitsForFunctionExitEPKNS1_21BytecodeLivenessStateE:
.LFB24741:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movl	$-1, %esi
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsUntilLoopEiPKNS1_21BytecodeLivenessStateE
	.cfi_endproc
.LFE24741:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder29BuildLoopExitsForFunctionExitEPKNS1_21BytecodeLivenessStateE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder29BuildLoopExitsForFunctionExitEPKNS1_21BytecodeLivenessStateE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder10VisitThrowEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder10VisitThrowEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder10VisitThrowEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder10VisitThrowEv:
.LFB24612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movl	144(%rdi), %esi
	movq	160(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movl	$-1, %esi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsUntilLoopEiPKNS1_21BytecodeLivenessStateE
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2725
	movq	16(%rbx), %rax
	movq	(%rcx,%rsi,8), %r12
	movl	$160, %esi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	leaq	-32(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment15BindAccumulatorEPNS1_4NodeENS3_24FrameStateAttachmentModeE.constprop.1
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5ThrowEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	448(%rbx), %rsi
	movq	%rax, -40(%rbp)
	cmpq	456(%rbx), %rsi
	je	.L2721
	movq	%rax, (%rsi)
	addq	$8, 448(%rbx)
.L2722:
	movq	$0, 168(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2726
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2721:
	.cfi_restore_state
	leaq	-40(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2722
.L2726:
	call	__stack_chk_fail@PLT
.L2725:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24612:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder10VisitThrowEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder10VisitThrowEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder10VisitAbortEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder10VisitAbortEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder10VisitAbortEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder10VisitAbortEv:
.LFB24613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	144(%rdi), %esi
	movq	160(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movl	$-1, %esi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsUntilLoopEiPKNS1_21BytecodeLivenessStateE
	leaq	136(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor15GetIndexOperandEi@PLT
	movl	%eax, %esi
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12RuntimeAbortENS0_11AbortReasonE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5ThrowEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	448(%rbx), %rsi
	movq	%rax, -32(%rbp)
	cmpq	456(%rbx), %rsi
	je	.L2728
	movq	%rax, (%rsi)
	addq	$8, 448(%rbx)
.L2729:
	movq	$0, 168(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2732
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2728:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2729
.L2732:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24613:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder10VisitAbortEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder10VisitAbortEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder12VisitReThrowEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitReThrowEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitReThrowEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitReThrowEv:
.LFB24614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movl	144(%rdi), %esi
	movq	160(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movl	$-1, %esi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsUntilLoopEiPKNS1_21BytecodeLivenessStateE
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2739
	movq	16(%rbx), %rax
	movq	(%rcx,%rsi,8), %r12
	movl	$156, %esi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	leaq	-32(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, -32(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5ThrowEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	448(%rbx), %rsi
	movq	%rax, -40(%rbp)
	cmpq	456(%rbx), %rsi
	je	.L2735
	movq	%rax, (%rsi)
	addq	$8, 448(%rbx)
.L2736:
	movq	$0, 168(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2740
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2735:
	.cfi_restore_state
	leaq	-40(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2736
.L2740:
	call	__stack_chk_fail@PLT
.L2739:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24614:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitReThrowEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitReThrowEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder22BuildHoleCheckAndThrowEPNS1_4NodeENS0_7Runtime10FunctionIdES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder22BuildHoleCheckAndThrowEPNS1_4NodeENS0_7Runtime10FunctionIdES4_
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder22BuildHoleCheckAndThrowEPNS1_4NodeENS0_7Runtime10FunctionIdES4_, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder22BuildHoleCheckAndThrowEPNS1_4NodeENS0_7Runtime10FunctionIdES4_:
.LFB24615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2754
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movq	(%rcx,%rsi,8), %r14
	movl	$1, %edx
	movl	$2, %esi
	leaq	-64(%rbp), %r13
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%r15, -64(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment4CopyEv
	movq	%rax, %r15
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movl	144(%rbx), %esi
	movq	160(%rbx), %rdi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movl	$-1, %esi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsUntilLoopEiPKNS1_21BytecodeLivenessStateE
	movq	16(%rbx), %rax
	movl	%r12d, %esi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder11CallRuntimeENS0_7Runtime10FunctionIdE@PLT
	movq	%rax, %rsi
	cmpl	$173, %r12d
	je	.L2755
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %r12
.L2744:
	movq	168(%rbx), %rax
	movq	(%r12), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L2756
.L2745:
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5ThrowEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	448(%rbx), %rsi
	movq	%rax, -72(%rbp)
	cmpq	456(%rbx), %rsi
	je	.L2746
	movq	%rax, (%rsi)
	addq	$8, 448(%rbx)
.L2747:
	movq	16(%rbx), %rax
	movq	%r15, 168(%rbx)
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2754
	movq	%r14, (%rcx,%rsi,8)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2757
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2755:
	.cfi_restore_state
	movq	-88(%rbp), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	movq	%rax, %r12
	jmp	.L2744
	.p2align 4,,10
	.p2align 3
.L2756:
	movl	144(%r13), %esi
	movq	160(%r13), %rdi
	movl	%esi, -88(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi@PLT
	movq	168(%r13), %rdi
	movl	-88(%rbp), %esi
	movq	$-1, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment10CheckpointENS0_9BailoutIdENS1_23OutputFrameStateCombineEPKNS1_21BytecodeLivenessStateE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties22ReplaceFrameStateInputEPNS1_4NodeES4_@PLT
	jmp	.L2745
	.p2align 4,,10
	.p2align 3
.L2746:
	leaq	-72(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2747
.L2754:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2757:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24615:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder22BuildHoleCheckAndThrowEPNS1_4NodeENS0_7Runtime10FunctionIdES4_, .-_ZN2v88internal8compiler20BytecodeGraphBuilder22BuildHoleCheckAndThrowEPNS1_4NodeENS0_7Runtime10FunctionIdES4_
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder30VisitThrowReferenceErrorIfHoleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitThrowReferenceErrorIfHoleEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitThrowReferenceErrorIfHoleEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitThrowReferenceErrorIfHoleEv:
.LFB24616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$64, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2762
	movq	%rdi, %r12
	movq	16(%rdi), %rdi
	movq	(%rcx,%rsi,8), %r13
	leaq	-80(%rbp), %r15
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	movq	%rax, -88(%rbp)
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%r13, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movhps	-88(%rbp), %xmm0
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	16(%r12), %r14
	xorl	%esi, %esi
	leaq	136(%r12), %rdi
	movq	%rax, %r13
	movq	360(%r14), %rdx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor26GetConstantForIndexOperandEiPNS0_7IsolateE@PLT
	movq	(%r12), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movl	$173, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22BuildHoleCheckAndThrowEPNS1_4NodeENS0_7Runtime10FunctionIdES4_
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2763
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2762:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2763:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24616:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitThrowReferenceErrorIfHoleEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitThrowReferenceErrorIfHoleEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder30VisitThrowSuperNotCalledIfHoleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitThrowSuperNotCalledIfHoleEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitThrowSuperNotCalledIfHoleEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitThrowSuperNotCalledIfHoleEv:
.LFB24617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2768
	movq	%rdi, %r12
	movq	16(%rdi), %rdi
	movq	(%rcx,%rsi,8), %rbx
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	movq	%rax, -56(%rbp)
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movhps	-56(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	xorl	%ecx, %ecx
	movl	$44, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22BuildHoleCheckAndThrowEPNS1_4NodeENS0_7Runtime10FunctionIdES4_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2769
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2768:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2769:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24617:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitThrowSuperNotCalledIfHoleEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitThrowSuperNotCalledIfHoleEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder37VisitThrowSuperAlreadyCalledIfNotHoleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder37VisitThrowSuperAlreadyCalledIfNotHoleEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder37VisitThrowSuperAlreadyCalledIfNotHoleEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder37VisitThrowSuperAlreadyCalledIfNotHoleEv:
.LFB24618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	168(%rdi), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2774
	movq	%rdi, %r12
	movq	16(%rdi), %rdi
	movq	(%rcx,%rsi,8), %rbx
	leaq	-64(%rbp), %r13
	call	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	movq	%rax, -72(%rbp)
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movhps	-72(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	%rax, %rbx
	movq	16(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10BooleanNotEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb.constprop.0
	xorl	%ecx, %ecx
	movl	$43, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder22BuildHoleCheckAndThrowEPNS1_4NodeENS0_7Runtime10FunctionIdES4_
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2775
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2774:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2775:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24618:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder37VisitThrowSuperAlreadyCalledIfNotHoleEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder37VisitThrowSuperAlreadyCalledIfNotHoleEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11BuildReturnEPKNS1_21BytecodeLivenessStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildReturnEPKNS1_21BytecodeLivenessStateE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildReturnEPKNS1_21BytecodeLivenessStateE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildReturnEPKNS1_21BytecodeLivenessStateE:
.LFB24709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movl	$-1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsUntilLoopEiPKNS1_21BytecodeLivenessStateE
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movq	%rax, -72(%rbp)
	movq	168(%rbx), %rax
	movq	48(%rax), %rcx
	movq	56(%rax), %rdx
	movslq	92(%rax), %rsi
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2782
	movq	16(%rbx), %rax
	movq	(%rcx,%rsi,8), %r12
	movl	$1, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6ReturnEi@PLT
	movq	%r12, %xmm1
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	-72(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movq	448(%rbx), %rsi
	movq	%rax, -56(%rbp)
	cmpq	456(%rbx), %rsi
	je	.L2778
	movq	%rax, (%rsi)
	addq	$8, 448(%rbx)
.L2779:
	movq	$0, 168(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2783
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2778:
	.cfi_restore_state
	leaq	-56(%rbp), %rdx
	leaq	432(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2779
.L2783:
	call	__stack_chk_fail@PLT
.L2782:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24709:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildReturnEPKNS1_21BytecodeLivenessStateE, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildReturnEPKNS1_21BytecodeLivenessStateE
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11VisitReturnEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitReturnEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitReturnEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitReturnEv:
.LFB24710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	144(%rdi), %esi
	movq	160(%rdi), %rdi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildReturnEPKNS1_21BytecodeLivenessStateE
	.cfi_endproc
.LFE24710:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitReturnEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitReturnEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder21VisitSuspendGeneratorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitSuspendGeneratorEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitSuspendGeneratorEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitSuspendGeneratorEv:
.LFB24728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	136(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	168(%rdi), %r14
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	testl	%eax, %eax
	jne	.L2814
	movl	$2, %esi
	movq	%r13, %rdi
	leaq	-80(%rbp), %r14
	movl	%eax, %ebx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movdqu	40(%r12), %xmm1
	movq	%r14, %rdi
	movl	%eax, -92(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	movq	%r14, %rdi
	movq	%rdx, -104(%rbp)
	movq	%rax, -112(%rbp)
	movdqa	-112(%rbp), %xmm2
	movaps	%xmm2, -80(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeArrayRef15parameter_countEv@PLT
	movl	$3, %esi
	movq	%r13, %rdi
	movq	16(%r12), %r14
	movl	%eax, -112(%rbp)
	leal	-1(%rax), %r15d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	pxor	%xmm0, %xmm0
	movq	16(%r12), %rdi
	movl	144(%r12), %esi
	movq	%rax, -128(%rbp)
	leal	53(%rsi), %eax
	cvtsi2sdl	%eax, %xmm0
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	144(%r12), %esi
	movq	160(%r12), %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movl	-112(%rbp), %edx
	movq	8(%r12), %rdi
	movq	%rax, -120(%rbp)
	movl	-92(%rbp), %eax
	leal	2(%rdx,%rax), %esi
	movq	16(%rdi), %rax
	movslq	%esi, %rsi
	movq	%rax, %rdx
	movq	%rax, -112(%rbp)
	movq	24(%rdi), %rax
	salq	$3, %rsi
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L2815
	addq	-112(%rbp), %rsi
	movq	%rdx, %rax
	movq	%rsi, 16(%rdi)
.L2789:
	movq	-88(%rbp), %xmm0
	movq	%r13, 16(%rax)
	leaq	24(%rax), %rdx
	xorl	%r13d, %r13d
	movhps	-128(%rbp), %xmm0
	movups	%xmm0, (%rax)
	testl	%r15d, %r15d
	jle	.L2816
	.p2align 4,,10
	.p2align 3
.L2793:
	movl	%r13d, %edi
	movq	168(%r12), %r14
	movl	%r15d, %esi
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal11interpreter8Register18FromParameterIndexEii@PLT
	movl	%r13d, -128(%rbp)
	addl	$1, %r13d
	movl	%eax, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	-88(%rbp), %rdx
	movq	%rax, (%rdx)
	addq	$8, %rdx
	cmpl	%r15d, %r13d
	jne	.L2793
	movl	-128(%rbp), %r14d
	addl	$4, %r14d
.L2794:
	movl	-92(%rbp), %eax
	testl	%eax, %eax
	jle	.L2791
	movq	-112(%rbp), %rax
	addq	$32, %rax
	movq	%rax, -128(%rbp)
	jmp	.L2792
	.p2align 4,,10
	.p2align 3
.L2798:
	addl	$1, %ebx
	cmpl	%ebx, -92(%rbp)
	je	.L2817
.L2792:
	movq	-120(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L2795
	cmpl	$1, 12(%rcx)
	movq	16(%rcx), %rax
	je	.L2797
	movl	%ebx, %edx
	sarl	$6, %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
.L2797:
	btq	%rbx, %rax
	jnc	.L2798
.L2795:
	leal	(%r15,%rbx), %eax
	movl	%eax, -88(%rbp)
	cmpl	%eax, %r13d
	jge	.L2802
	movl	%r13d, %r8d
	movslq	%r13d, %rcx
	movq	-112(%rbp), %rsi
	movq	-128(%rbp), %rdx
	notl	%r8d
	addl	%r8d, %eax
	leaq	24(%rsi,%rcx,8), %r14
	addq	%rcx, %rax
	leaq	(%rdx,%rax,8), %r13
	.p2align 4,,10
	.p2align 3
.L2800:
	movq	16(%r12), %rdi
	addq	$8, %r14
	call	_ZN2v88internal8compiler7JSGraph20OptimizedOutConstantEv@PLT
	movq	%rax, -8(%r14)
	cmpq	%r13, %r14
	jne	.L2800
.L2799:
	movl	-88(%rbp), %eax
	movq	-112(%rbp), %rsi
	movq	168(%r12), %rdi
	leal	1(%rax), %r13d
	addl	$3, %eax
	cltq
	leaq	(%rsi,%rax,8), %r14
	movl	%ebx, %esi
	addl	$1, %ebx
	call	_ZNK2v88internal8compiler20BytecodeGraphBuilder11Environment14LookupRegisterENS0_11interpreter8RegisterE
	movq	%rax, (%r14)
	cmpl	%ebx, -92(%rbp)
	jne	.L2792
.L2817:
	leal	3(%r13), %r14d
.L2791:
	movq	16(%r12), %rax
	movl	%r13d, %esi
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder14GeneratorStoreEi@PLT
	movq	-112(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	%r14d, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder8MakeNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	movl	144(%r12), %esi
	movq	160(%r12), %rdi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildReturnEPKNS1_21BytecodeLivenessStateE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2818
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2802:
	.cfi_restore_state
	movl	%r13d, -88(%rbp)
	jmp	.L2799
.L2814:
	leaq	.LC20(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2816:
	movl	$3, %r14d
	jmp	.L2794
.L2815:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, -112(%rbp)
	jmp	.L2789
.L2818:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24728:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitSuspendGeneratorEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitSuspendGeneratorEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder19VisitSingleBytecodeEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitSingleBytecodeEv.part.0, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitSingleBytecodeEv.part.0:
.LFB31448:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	136(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	cmpb	$-74, %al
	ja	.L2819
	leaq	.L2822(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler20BytecodeGraphBuilder19VisitSingleBytecodeEv.part.0,"a",@progbits
	.align 4
	.align 4
.L2822:
	.long	.L2821-.L2822
	.long	.L2821-.L2822
	.long	.L2821-.L2822
	.long	.L2821-.L2822
	.long	.L2821-.L2822
	.long	.L2821-.L2822
	.long	.L2821-.L2822
	.long	.L2821-.L2822
	.long	.L2821-.L2822
	.long	.L2821-.L2822
	.long	.L2821-.L2822
	.long	.L2981-.L2822
	.long	.L2980-.L2822
	.long	.L2979-.L2822
	.long	.L2978-.L2822
	.long	.L2977-.L2822
	.long	.L2976-.L2822
	.long	.L2975-.L2822
	.long	.L2974-.L2822
	.long	.L2973-.L2822
	.long	.L2972-.L2822
	.long	.L2971-.L2822
	.long	.L2970-.L2822
	.long	.L2969-.L2822
	.long	.L2968-.L2822
	.long	.L2967-.L2822
	.long	.L2966-.L2822
	.long	.L2965-.L2822
	.long	.L2964-.L2822
	.long	.L2963-.L2822
	.long	.L2962-.L2822
	.long	.L2961-.L2822
	.long	.L2960-.L2822
	.long	.L2959-.L2822
	.long	.L2958-.L2822
	.long	.L2957-.L2822
	.long	.L2956-.L2822
	.long	.L2955-.L2822
	.long	.L2954-.L2822
	.long	.L2953-.L2822
	.long	.L2952-.L2822
	.long	.L2951-.L2822
	.long	.L2950-.L2822
	.long	.L2949-.L2822
	.long	.L2948-.L2822
	.long	.L2947-.L2822
	.long	.L2946-.L2822
	.long	.L2945-.L2822
	.long	.L2944-.L2822
	.long	.L2943-.L2822
	.long	.L2942-.L2822
	.long	.L2941-.L2822
	.long	.L2940-.L2822
	.long	.L2939-.L2822
	.long	.L2938-.L2822
	.long	.L2937-.L2822
	.long	.L2936-.L2822
	.long	.L2935-.L2822
	.long	.L2934-.L2822
	.long	.L2933-.L2822
	.long	.L2932-.L2822
	.long	.L2931-.L2822
	.long	.L2930-.L2822
	.long	.L2929-.L2822
	.long	.L2928-.L2822
	.long	.L2927-.L2822
	.long	.L2926-.L2822
	.long	.L2925-.L2822
	.long	.L2924-.L2822
	.long	.L2923-.L2822
	.long	.L2922-.L2822
	.long	.L2921-.L2822
	.long	.L2920-.L2822
	.long	.L2919-.L2822
	.long	.L2918-.L2822
	.long	.L2917-.L2822
	.long	.L2916-.L2822
	.long	.L2915-.L2822
	.long	.L2914-.L2822
	.long	.L2913-.L2822
	.long	.L2912-.L2822
	.long	.L2911-.L2822
	.long	.L2910-.L2822
	.long	.L2909-.L2822
	.long	.L2908-.L2822
	.long	.L2907-.L2822
	.long	.L2906-.L2822
	.long	.L2905-.L2822
	.long	.L2904-.L2822
	.long	.L2903-.L2822
	.long	.L2902-.L2822
	.long	.L2901-.L2822
	.long	.L2900-.L2822
	.long	.L2899-.L2822
	.long	.L2898-.L2822
	.long	.L2897-.L2822
	.long	.L2896-.L2822
	.long	.L2895-.L2822
	.long	.L2894-.L2822
	.long	.L2893-.L2822
	.long	.L2892-.L2822
	.long	.L2891-.L2822
	.long	.L2890-.L2822
	.long	.L2889-.L2822
	.long	.L2888-.L2822
	.long	.L2887-.L2822
	.long	.L2886-.L2822
	.long	.L2885-.L2822
	.long	.L2884-.L2822
	.long	.L2883-.L2822
	.long	.L2882-.L2822
	.long	.L2881-.L2822
	.long	.L2880-.L2822
	.long	.L2879-.L2822
	.long	.L2878-.L2822
	.long	.L2877-.L2822
	.long	.L2876-.L2822
	.long	.L2875-.L2822
	.long	.L2874-.L2822
	.long	.L2873-.L2822
	.long	.L2872-.L2822
	.long	.L2871-.L2822
	.long	.L2870-.L2822
	.long	.L2869-.L2822
	.long	.L2868-.L2822
	.long	.L2867-.L2822
	.long	.L2866-.L2822
	.long	.L2865-.L2822
	.long	.L2864-.L2822
	.long	.L2863-.L2822
	.long	.L2862-.L2822
	.long	.L2861-.L2822
	.long	.L2860-.L2822
	.long	.L2859-.L2822
	.long	.L2858-.L2822
	.long	.L2857-.L2822
	.long	.L2856-.L2822
	.long	.L2855-.L2822
	.long	.L2854-.L2822
	.long	.L2854-.L2822
	.long	.L2854-.L2822
	.long	.L2849-.L2822
	.long	.L2848-.L2822
	.long	.L2847-.L2822
	.long	.L2846-.L2822
	.long	.L2845-.L2822
	.long	.L2851-.L2822
	.long	.L2850-.L2822
	.long	.L2844-.L2822
	.long	.L2853-.L2822
	.long	.L2852-.L2822
	.long	.L2853-.L2822
	.long	.L2852-.L2822
	.long	.L2851-.L2822
	.long	.L2850-.L2822
	.long	.L2849-.L2822
	.long	.L2848-.L2822
	.long	.L2847-.L2822
	.long	.L2846-.L2822
	.long	.L2845-.L2822
	.long	.L2844-.L2822
	.long	.L2843-.L2822
	.long	.L2842-.L2822
	.long	.L2841-.L2822
	.long	.L2840-.L2822
	.long	.L2839-.L2822
	.long	.L2838-.L2822
	.long	.L2837-.L2822
	.long	.L2836-.L2822
	.long	.L2835-.L2822
	.long	.L2834-.L2822
	.long	.L2833-.L2822
	.long	.L2832-.L2822
	.long	.L2831-.L2822
	.long	.L2830-.L2822
	.long	.L2829-.L2822
	.long	.L2828-.L2822
	.long	.L2827-.L2822
	.long	.L2826-.L2822
	.long	.L2825-.L2822
	.long	.L2824-.L2822
	.long	.L2823-.L2822
	.long	.L2821-.L2822
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder19VisitSingleBytecodeEv.part.0
	.p2align 4,,10
	.p2align 3
.L2821:
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2854:
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movl	%eax, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder29MergeIntoSuccessorEnvironmentEi
	.p2align 4,,10
	.p2align 3
.L2852:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder25BuildJumpIfToBooleanFalseEv
	.p2align 4,,10
	.p2align 3
.L2853:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder24BuildJumpIfToBooleanTrueEv
	.p2align 4,,10
	.p2align 3
.L2844:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder21BuildJumpIfJSReceiverEv
	.p2align 4,,10
	.p2align 3
.L2850:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildJumpIfFalseEv
	.p2align 4,,10
	.p2align 3
.L2851:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildJumpIfTrueEv
	.p2align 4,,10
	.p2align 3
.L2845:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder34VisitJumpIfUndefinedOrNullConstantEv
	.p2align 4,,10
	.p2align 3
.L2846:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitJumpIfNotUndefinedConstantEv
	.p2align 4,,10
	.p2align 3
.L2847:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitJumpIfUndefinedConstantEv
	.p2align 4,,10
	.p2align 3
.L2848:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitJumpIfNotNullConstantEv
	.p2align 4,,10
	.p2align 3
.L2849:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitJumpIfNullConstantEv
.L2855:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$2, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder20BuildCreateArgumentsENS0_19CreateArgumentsTypeE
.L2858:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitCreateWithContextEv
.L2859:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitCreateEvalContextEv
.L2860:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitCreateFunctionContextEv
.L2861:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateCatchContextEv
.L2870:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateArrayLiteralEv
.L2871:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateRegExpLiteralEv
.L2872:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToStringEv
.L2873:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToObjectEv
.L2874:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitToNumericEv
.L2875:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitToNumberEv
.L2876:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitToNameEv
.L2877:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitTestTypeOfEv
.L2878:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitTestUndefinedEv
.L2879:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitTestNullEv
.L2880:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitTestUndetectableEv
.L2881:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitTestInEv
.L2882:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitTestInstanceOfEv
.L2883:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitTestReferenceEqualEv
.L2884:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitTestGreaterThanOrEqualEv
.L2885:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitTestLessThanOrEqualEv
.L2887:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitTestLessThanEv
.L2888:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitTestEqualStrictEv
.L2889:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitTestEqualEv
.L2890:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitConstructWithSpreadEv
.L2891:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitConstructEv
.L2892:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitInvokeIntrinsicEv
.L2893:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallJSRuntimeEv
.L2894:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCallRuntimeForPairEv
.L2895:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitCallRuntimeEv
.L2896:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitCallWithSpreadEv
.L2897:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitCallNoFeedbackEv
.L2898:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver2Ev
.L2899:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver1Ev
.L2900:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitCallUndefinedReceiver0Ev
.L2901:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildCallVarArgsENS0_19ConvertReceiverModeE
.L2902:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty2Ev
.L2903:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty1Ev
.L2904:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCallProperty0Ev
.L2905:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildCallVarArgsENS0_19ConvertReceiverModeE
.L2906:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$2, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder16BuildCallVarArgsENS0_19ConvertReceiverModeE
.L2907:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitGetSuperConstructorEv
.L2908:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildDeleteENS0_12LanguageModeE
.L2909:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildDeleteENS0_12LanguageModeE
.L2910:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitTypeOfEv
.L2911:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitLogicalNotEv
.L2912:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitToBooleanLogicalNotEv
.L2913:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10BitwiseNotEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder12BuildUnaryOpEPKNS1_8OperatorE
.L2914:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder6NegateEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder12BuildUnaryOpEPKNS1_8OperatorE
.L2915:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder9DecrementEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder12BuildUnaryOpEPKNS1_8OperatorE
.L2916:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder9IncrementEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder12BuildUnaryOpEPKNS1_8OperatorE
.L2917:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder17ShiftRightLogicalEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
.L2886:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitTestGreaterThanEv
.L2951:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitLdaNamedPropertyNoFeedbackEv
.L2952:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitLdaNamedPropertyEv
.L2953:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitMovEv
.L2954:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitStarEv
.L2955:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder9VisitLdarEv
.L2956:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitStaLookupSlotEv
.L2957:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder24BuildLdaLookupGlobalSlotENS0_10TypeofModeE
.L2958:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder25BuildLdaLookupContextSlotENS0_10TypeofModeE
.L2959:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder18BuildLdaLookupSlotENS0_10TypeofModeE
.L2960:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder24BuildLdaLookupGlobalSlotENS0_10TypeofModeE
.L2961:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder25BuildLdaLookupContextSlotENS0_10TypeofModeE
.L2962:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder18BuildLdaLookupSlotENS0_10TypeofModeE
.L2963:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitStaCurrentContextSlotEv
.L2964:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitStaContextSlotEv
.L2965:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder35VisitLdaImmutableCurrentContextSlotEv
.L2966:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitLdaCurrentContextSlotEv
.L2967:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitLdaImmutableContextSlotEv
.L2968:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitLdaContextSlotEv
.L2969:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitPopContextEv
.L2970:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitPushContextEv
.L2971:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitStaGlobalEv
.L2972:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitLdaGlobalInsideTypeofEv
.L2973:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitLdaGlobalEv
.L2974:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitLdaConstantEv
.L2975:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitLdaFalseEv
.L2976:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaTrueEv
.L2977:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitLdaTheHoleEv
.L2978:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaNullEv
.L2979:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitLdaUndefinedEv
.L2980:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitLdaSmiEv
.L2981:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitLdaZeroEv
.L2918:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10ShiftRightEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
.L2919:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder9ShiftLeftEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
.L2920:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10BitwiseAndEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
.L2921:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10BitwiseXorEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
.L2922:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder9BitwiseOrEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
.L2923:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder12ExponentiateEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
.L2924:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder7ModulusEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
.L2925:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder6DivideEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
.L2926:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8MultiplyEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
.L2927:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8SubtractEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildBinaryOpWithImmediateEPKNS1_8OperatorE
.L2928:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder11VisitAddSmiEv
.L2929:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder17ShiftRightLogicalEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
.L2930:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10ShiftRightEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
.L2931:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder9ShiftLeftEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
.L2932:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10BitwiseAndEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
.L2933:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10BitwiseXorEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
.L2934:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder9BitwiseOrEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
.L2935:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder12ExponentiateEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
.L2936:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder7ModulusEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
.L2937:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder6DivideEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
.L2938:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8MultiplyEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
.L2939:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8SubtractEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13BuildBinaryOpEPKNS1_8OperatorE
.L2940:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder8VisitAddEv
.L2941:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCollectTypeProfileEv
.L2942:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitStaDataPropertyInLiteralEv
.L2943:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitStaInArrayLiteralEv
.L2944:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitStaKeyedPropertyEv
.L2945:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildNamedStoreENS2_9StoreModeE
.L2946:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder31VisitStaNamedPropertyNoFeedbackEv
.L2947:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder15BuildNamedStoreENS2_9StoreModeE
.L2948:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitStaModuleVariableEv
.L2949:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitLdaModuleVariableEv
.L2950:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitLdaKeyedPropertyEv
.L2862:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder23VisitCreateBlockContextEv
.L2863:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitCreateClosureEv
.L2864:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitGetTemplateObjectEv
.L2865:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitCloneObjectEv
.L2866:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder29VisitCreateEmptyObjectLiteralEv
.L2867:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder24VisitCreateObjectLiteralEv
.L2868:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateEmptyArrayLiteralEv
.L2869:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder28VisitCreateArrayFromIterableEv
.L2856:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder20BuildCreateArgumentsENS0_19CreateArgumentsTypeE
.L2857:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder20BuildCreateArgumentsENS0_19CreateArgumentsTypeE
.L2842:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitForInEnumerateEv
.L2843:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder26VisitSwitchOnSmiNoFeedbackEv
.L2838:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitForInStepEv
.L2839:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitForInNextEv
.L2840:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder18VisitForInContinueEv
.L2841:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder17VisitForInPrepareEv
.L2823:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder10VisitAbortEv
.L2824:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitIncBlockCounterEv
.L2825:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder13VisitDebuggerEv
.L2826:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder16VisitGetIteratorEv
.L2827:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder20VisitResumeGeneratorEv
.L2828:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder21VisitSuspendGeneratorEv
.L2829:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder27VisitSwitchOnGeneratorStateEv
.L2830:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder37VisitThrowSuperAlreadyCalledIfNotHoleEv
.L2831:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitThrowSuperNotCalledIfHoleEv
.L2832:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder30VisitThrowReferenceErrorIfHoleEv
.L2833:
	.cfi_restore_state
	movl	144(%r12), %esi
	movq	160(%r12), %rdi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder11BuildReturnEPKNS1_21BytecodeLivenessStateE
.L2834:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder12VisitReThrowEv
.L2835:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder10VisitThrowEv
.L2836:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder22VisitSetPendingMessageEv
.L2837:
	.cfi_restore_state
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder15VisitStackCheckEv
.L2819:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE31448:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitSingleBytecodeEv.part.0, .-_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitSingleBytecodeEv.part.0
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder19VisitSingleBytecodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitSingleBytecodeEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitSingleBytecodeEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitSingleBytecodeEv:
.LFB24513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	552(%rdi), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	128(%r12), %rax
	movl	144(%r12), %r13d
	cmpl	$-1, 24(%rax)
	je	.L2984
	cmpl	32(%rax), %r13d
	je	.L3002
.L2984:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder30ExitThenEnterExceptionHandlersEi
	movq	216(%r12), %rax
	leaq	208(%r12), %rdx
	movq	%rdx, %rbx
	testq	%rax, %rax
	jne	.L2985
	jmp	.L3003
	.p2align 4,,10
	.p2align 3
.L3004:
	movq	%rax, %rbx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2987
.L2985:
	cmpl	32(%rax), %r13d
	jle	.L3004
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2985
.L2987:
	movq	168(%r12), %rax
	cmpq	%rbx, %rdx
	je	.L2986
	cmpl	32(%rbx), %r13d
	jge	.L3005
.L2986:
	testq	%rax, %rax
	je	.L2983
.L3007:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildLoopHeaderEnvironmentEi
	cmpb	$0, 184(%r12)
	je	.L2992
	leaq	136(%r12), %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	cmpb	$-89, %al
	je	.L3006
.L2992:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitSingleBytecodeEv.part.0
	.p2align 4,,10
	.p2align 3
.L3005:
	.cfi_restore_state
	movb	$1, 416(%r12)
	testq	%rax, %rax
	je	.L2990
	movq	160(%r12), %rdi
	movq	40(%rbx), %r14
	movl	%r13d, %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movq	168(%r12), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment5MergeEPS3_PKNS1_21BytecodeLivenessStateE
.L2990:
	movq	40(%rbx), %rax
	movq	%rax, 168(%r12)
	testq	%rax, %rax
	jne	.L3007
.L2983:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3006:
	.cfi_restore_state
	movb	$0, 184(%r12)
	jmp	.L2983
	.p2align 4,,10
	.p2align 3
.L3002:
	movq	40(%rax), %rax
	movq	536(%r12), %rcx
	movabsq	$140735340871680, %rdx
	andq	544(%r12), %rdx
	andl	$2147483646, %eax
	orq	%rdx, %rax
	movq	%rax, 16(%rcx)
	movq	128(%r12), %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	jmp	.L2984
	.p2align 4,,10
	.p2align 3
.L3003:
	movq	168(%r12), %rax
	jmp	.L2986
	.cfi_endproc
.LFE24513:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitSingleBytecodeEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitSingleBytecodeEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsForBranchEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsForBranchEi
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsForBranchEi, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsForBranchEi:
.LFB24739:
	.cfi_startproc
	endbr64
	cmpl	144(%rdi), %esi
	jg	.L3013
	ret
	.p2align 4,,10
	.p2align 3
.L3013:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	160(%rdi), %rdi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movq	160(%r12), %rdi
	movl	%r13d, %esi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetLoopOffsetForEi@PLT
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsUntilLoopEiPKNS1_21BytecodeLivenessStateE
	.cfi_endproc
.LFE24739:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsForBranchEi, .-_ZN2v88internal8compiler20BytecodeGraphBuilder23BuildLoopExitsForBranchEi
	.section	.text._ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16ExceptionHandlerENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16ExceptionHandlerENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16ExceptionHandlerENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16ExceptionHandlerENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16ExceptionHandlerENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB29989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$5, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L3028
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L3016:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L3017
	movq	%r15, %rbx
	jmp	.L3022
	.p2align 4,,10
	.p2align 3
.L3018:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L3029
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3019:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L3017
.L3022:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L3018
	cmpq	$31, 8(%rax)
	jbe	.L3018
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L3022
.L3017:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$31, %r13d
	salq	$4, %r13
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	addq	%rax, %r13
	leaq	512(%rax), %rcx
	movq	%rax, 72(%r12)
	movq	%r13, 64(%r12)
	movq	%rcx, 80(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3029:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3019
	.p2align 4,,10
	.p2align 3
.L3028:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L3016
	.cfi_endproc
.LFE29989:
	.size	_ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16ExceptionHandlerENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16ExceptionHandlerENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilderC2EPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_16NativeContextRefERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilderC2EPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_16NativeContextRefERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilderC2EPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_16NativeContextRefERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE, @function
_ZN2v88internal8compiler20BytecodeGraphBuilderC2EPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_16NativeContextRefERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE:
.LFB24476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%r9, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	addq	$80, %rdi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-160(%rbp), %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movdqu	(%r9), %xmm7
	movdqu	(%r8), %xmm6
	movq	%rdx, -272(%rbp)
	movq	40(%rbp), %rdx
	movdqu	(%rcx), %xmm5
	movq	24(%rbp), %rbx
	movl	56(%rbp), %r9d
	movhps	-272(%rbp), %xmm0
	movq	%rdx, -296(%rbp)
	movq	64(%rbp), %rdx
	movq	(%rax), %rcx
	movq	8(%rax), %r8
	movq	%rdx, -304(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	movq	32(%rbp), %rdx
	movups	%xmm0, -80(%rdi)
	movups	%xmm7, -24(%rdi)
	movss	(%rdx), %xmm0
	movups	%xmm6, -40(%rdi)
	movl	$2, %edx
	movups	%xmm5, -56(%rdi)
	movss	%xmm0, -8(%rdi)
	movq	%rbx, -64(%rdi)
	andl	$4, %r9d
	cmovne	%edx, %r9d
	movq	%rbx, %rdx
	call	_ZN2v88internal8compiler18JSTypeHintLoweringC1EPNS1_12JSHeapBrokerEPNS1_7JSGraphENS1_17FeedbackVectorRefENS_4base5FlagsINS2_4FlagEiEE@PLT
	movq	16(%r15), %rax
	movq	%r13, %rdi
	movq	8(%rax), %r14
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef6objectEv@PLT
	movdqu	40(%r15), %xmm5
	movq	%r12, %rdi
	movq	%rax, -336(%rbp)
	movaps	%xmm5, -160(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	movq	%rax, -288(%rbp)
	leaq	-256(%rbp), %rax
	movq	%rdx, -280(%rbp)
	movdqa	-288(%rbp), %xmm6
	movq	%rax, %rdi
	movq	%rax, -288(%rbp)
	movaps	%xmm6, -256(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeArrayRef14register_countEv@PLT
	movdqu	40(%r15), %xmm7
	movq	%r12, %rdi
	movl	%eax, -328(%rbp)
	movaps	%xmm7, -160(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	movq	%r12, %rdi
	movq	%rdx, -312(%rbp)
	movq	%rax, -320(%rbp)
	movdqa	-320(%rbp), %xmm1
	movaps	%xmm1, -160(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeArrayRef15parameter_countEv@PLT
	movl	-328(%rbp), %ecx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	-336(%rbp), %r8
	movl	%eax, %edx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder28CreateFrameStateFunctionInfoENS1_14FrameStateTypeEiiNS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movdqu	40(%r15), %xmm2
	movq	%r12, %rdi
	movq	$0, 128(%r15)
	movq	%rax, 120(%r15)
	movaps	%xmm2, -160(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	movl	$24, %edi
	movq	%rdx, %r14
	movq	%rax, -320(%rbp)
	call	_Znwm@PLT
	movq	-320(%rbp), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal8compiler20OffHeapBytecodeArrayC1ENS1_16BytecodeArrayRefE@PLT
	movq	-320(%rbp), %rax
	leaq	136(%r15), %rdi
	movq	%r12, %rsi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal11interpreter21BytecodeArrayIteratorC1ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EE@PLT
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3032
	movq	(%rdi), %rax
	call	*72(%rax)
.L3032:
	movdqu	40(%r15), %xmm3
	movq	(%r15), %r9
	movq	%r12, %rdi
	movzbl	_ZN2v88internal24FLAG_concurrent_inliningE(%rip), %r14d
	movaps	%xmm3, -160(%rbp)
	movq	%r9, -328(%rbp)
	xorl	$1, %r14d
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	movq	%r12, %rdi
	movzbl	%r14b, %r14d
	movq	%rdx, -312(%rbp)
	movq	%rax, -320(%rbp)
	movdqa	-320(%rbp), %xmm5
	movaps	%xmm5, -160(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeArrayRef6objectEv@PLT
	movl	56(%rbp), %ecx
	movl	16(%rbp), %edx
	movl	%r14d, %r8d
	movq	-328(%rbp), %r9
	movq	%rax, %rsi
	shrl	%ecx
	andl	$1, %ecx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler12JSHeapBroker19GetBytecodeAnalysisENS0_6HandleINS0_13BytecodeArrayEEENS0_9BailoutIdEbNS1_19SerializationPolicyE@PLT
	cmpl	$-1, 16(%rbp)
	movq	-272(%rbp), %rcx
	movq	$0, 168(%r15)
	movq	%rax, 160(%r15)
	movzbl	56(%rbp), %eax
	pxor	%xmm0, %xmm0
	setne	176(%r15)
	xorl	%esi, %esi
	movq	-288(%rbp), %rdi
	movb	%al, 184(%r15)
	leaq	208(%r15), %rax
	andb	$1, 184(%r15)
	movq	%rax, 224(%r15)
	movq	%rax, 232(%r15)
	leaq	264(%r15), %rax
	movl	$-1, 180(%r15)
	movq	%rcx, 192(%r15)
	movl	$0, 208(%r15)
	movq	$0, 216(%r15)
	movq	$0, 240(%r15)
	movq	%rcx, 248(%r15)
	movl	$0, 264(%r15)
	movq	$0, 272(%r15)
	movq	%rax, 280(%r15)
	movq	%rax, 288(%r15)
	movq	$0, 296(%r15)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	%rcx, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16ExceptionHandlerENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	pxor	%xmm0, %xmm0
	je	.L3076
	movdqa	-256(%rbp), %xmm6
	xorl	%esi, %esi
	movq	%r12, %rdi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16ExceptionHandlerENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-112(%rbp), %xmm4
	movq	-248(%rbp), %rax
	movq	-208(%rbp), %xmm2
	movq	-200(%rbp), %rdi
	movaps	%xmm4, -208(%rbp)
	movq	-216(%rbp), %r8
	movq	-256(%rbp), %xmm4
	movq	%rax, %xmm5
	movq	-224(%rbp), %xmm3
	movq	-184(%rbp), %r10
	punpcklqdq	%xmm5, %xmm4
	movdqa	-128(%rbp), %xmm7
	movq	-192(%rbp), %xmm1
	movups	%xmm4, 304(%r15)
	movq	%r8, %xmm4
	movq	-168(%rbp), %rsi
	movq	-176(%rbp), %xmm0
	punpcklqdq	%xmm4, %xmm3
	movq	-240(%rbp), %r11
	movq	-232(%rbp), %r9
	movaps	%xmm7, -224(%rbp)
	movups	%xmm3, 336(%r15)
	movq	%rdi, %xmm3
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	punpcklqdq	%xmm3, %xmm2
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %rcx
	movq	%r11, 320(%r15)
	movups	%xmm2, 352(%r15)
	movq	%r10, %xmm2
	punpcklqdq	%xmm2, %xmm1
	movq	%rdx, -240(%rbp)
	movups	%xmm1, 368(%r15)
	movq	%rsi, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rcx, -232(%rbp)
	movq	%r9, 328(%r15)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movups	%xmm0, 384(%r15)
	testq	%rdx, %rdx
	je	.L3034
	movq	-168(%rbp), %rsi
	leaq	8(%rsi), %rdi
	movq	-200(%rbp), %rsi
	cmpq	%rsi, %rdi
	jbe	.L3035
	.p2align 4,,10
	.p2align 3
.L3038:
	testq	%rax, %rax
	je	.L3036
	cmpq	$32, 8(%rax)
	ja	.L3037
.L3036:
	movq	(%rsi), %rax
	movq	$32, 8(%rax)
	movq	-248(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -248(%rbp)
.L3037:
	addq	$8, %rsi
	cmpq	%rsi, %rdi
	ja	.L3038
	movq	-232(%rbp), %rcx
	movq	-240(%rbp), %rdx
.L3035:
	leaq	0(,%rcx,8), %rax
	cmpq	$15, %rax
	jbe	.L3034
	movq	%rcx, 8(%rdx)
	movq	$0, (%rdx)
.L3034:
	movq	-272(%rbp), %rax
	movb	$1, 416(%r15)
	movq	%rbx, %rsi
	leaq	464(%r15), %rdi
	movq	$0, 400(%r15)
	movq	%rax, 432(%r15)
	movq	$0, 408(%r15)
	movq	$0, 424(%r15)
	movq	$0, 440(%r15)
	movq	$0, 448(%r15)
	movq	$0, 456(%r15)
	call	_ZN2v88internal8compiler16StateValuesCacheC1EPNS1_7JSGraphE@PLT
	movq	-296(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, 536(%r15)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef13StartPositionEv@PLT
	movl	48(%rbp), %ebx
	movabsq	$-140735340871681, %rdx
	addl	$1, %eax
	cltq
	addq	%rax, %rax
	andq	%rdx, %rax
	leal	1(%rbx), %edx
	movslq	%edx, %rdx
	salq	$31, %rdx
	orq	%rdx, %rax
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	movq	%rax, 544(%r15)
	movq	-304(%rbp), %rax
	movq	%rax, 552(%r15)
	je	.L3040
	movdqu	40(%r15), %xmm1
	movq	%r12, %rdi
	movaps	%xmm1, -160(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	movq	-288(%rbp), %rdi
	movq	%rdx, -264(%rbp)
	movq	%rax, -272(%rbp)
	movdqa	-272(%rbp), %xmm2
	movaps	%xmm2, -256(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeArrayRef21source_positions_sizeEv@PLT
	movdqu	40(%r15), %xmm3
	movq	%r12, %rdi
	movl	%eax, %ebx
	movaps	%xmm3, -160(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	movq	%r12, %rdi
	movslq	%ebx, %r12
	movq	%rdx, -264(%rbp)
	movq	%rax, -272(%rbp)
	movdqa	-272(%rbp), %xmm6
	movaps	%xmm6, -160(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeArrayRef24source_positions_addressEv@PLT
	movl	$64, %edi
	movq	%rax, %r13
	call	_Znwm@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6VectorIKhEENS1_15IterationFilterE@PLT
	movq	128(%r15), %rdi
	movq	%rbx, 128(%r15)
	testq	%rdi, %rdi
	je	.L3030
.L3075:
	movl	$64, %esi
	call	_ZdlPvm@PLT
.L3030:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3077
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3040:
	.cfi_restore_state
	movdqu	40(%r15), %xmm7
	movq	16(%r15), %rax
	movq	%r12, %rdi
	movq	360(%rax), %rbx
	movaps	%xmm7, -160(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	movq	%rdx, -264(%rbp)
	movdqa	-272(%rbp), %xmm1
	movaps	%xmm1, -160(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeArrayRef6objectEv@PLT
	movq	(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L3078
.L3043:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37280(%rax), %rsi
	je	.L3079
	movq	7(%rsi), %rsi
.L3047:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3049
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L3050:
	movl	$64, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6HandleINS0_9ByteArrayEEENS1_15IterationFilterE@PLT
	movq	128(%r15), %rdi
	movq	%rbx, 128(%r15)
	testq	%rdi, %rdi
	jne	.L3075
	jmp	.L3030
	.p2align 4,,10
	.p2align 3
.L3076:
	movdqa	-256(%rbp), %xmm2
	movq	-232(%rbp), %rax
	movq	$0, 320(%r15)
	movdqa	-224(%rbp), %xmm3
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm6
	movq	%rax, 328(%r15)
	movups	%xmm2, 304(%r15)
	movups	%xmm3, 336(%r15)
	movups	%xmm5, 352(%r15)
	movups	%xmm4, 368(%r15)
	movups	%xmm6, 384(%r15)
	jmp	.L3034
	.p2align 4,,10
	.p2align 3
.L3049:
	movq	41088(%rbx), %r12
	cmpq	%r12, 41096(%rbx)
	je	.L3080
.L3051:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L3050
	.p2align 4,,10
	.p2align 3
.L3078:
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	leaq	-37592(%rdx), %rcx
	cmpq	-37504(%rdx), %rsi
	je	.L3044
	cmpq	312(%rcx), %rsi
	je	.L3044
	movq	-1(%rsi), %rdx
	cmpw	$70, 11(%rdx)
	jne	.L3043
	jmp	.L3047
	.p2align 4,,10
	.p2align 3
.L3044:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36616(%rax), %rsi
	jmp	.L3047
	.p2align 4,,10
	.p2align 3
.L3079:
	movq	-36616(%rax), %rsi
	jmp	.L3047
	.p2align 4,,10
	.p2align 3
.L3080:
	movq	%rbx, %rdi
	movq	%rsi, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L3051
.L3077:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24476:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilderC2EPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_16NativeContextRefERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE, .-_ZN2v88internal8compiler20BytecodeGraphBuilderC2EPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_16NativeContextRefERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilderC1EPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_16NativeContextRefERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE
	.set	_ZN2v88internal8compiler20BytecodeGraphBuilderC1EPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_16NativeContextRefERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE,_ZN2v88internal8compiler20BytecodeGraphBuilderC2EPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_16NativeContextRefERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE
	.section	.text._ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState15IteratorsStatesENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState15IteratorsStatesENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState15IteratorsStatesENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState15IteratorsStatesENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState15IteratorsStatesENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm:
.LFB30007:
	.cfi_startproc
	endbr64
	movabsq	$-3689348814741910323, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	mulq	%rdx
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	shrq	$3, %rdx
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	3(%rdx), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rdx), %rbx
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L3095
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L3083:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L3084
	movq	%r15, %rbx
	jmp	.L3089
	.p2align 4,,10
	.p2align 3
.L3085:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$479, %rdx
	jbe	.L3096
	leaq	480(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3086:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L3084
.L3089:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L3085
	cmpq	$9, 8(%rax)
	jbe	.L3085
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L3089
.L3084:
	movq	%r15, 56(%r12)
	movq	(%r15), %rax
	leaq	480(%rax), %rdx
	movq	%rax, 40(%r12)
	movq	%rdx, 48(%r12)
	leaq	-8(%r14), %rdx
	movq	%rdx, 88(%r12)
	movq	-8(%r14), %rcx
	movq	%rax, 32(%r12)
	movq	%r13, %rax
	leaq	480(%rcx), %rdx
	movq	%rcx, 72(%r12)
	movq	%rdx, 80(%r12)
	movabsq	$-3689348814741910323, %rdx
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %r13
	leaq	0(%r13,%r13,2), %rax
	salq	$4, %rax
	addq	%rax, %rcx
	movq	%rcx, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3096:
	.cfi_restore_state
	movl	$480, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3086
	.p2align 4,,10
	.p2align 3
.L3095:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L3083
	.cfi_endproc
.LFE30007:
	.size	_ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState15IteratorsStatesENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState15IteratorsStatesENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder29AdvanceToOsrEntryAndPeelLoopsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder29AdvanceToOsrEntryAndPeelLoopsEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder29AdvanceToOsrEntryAndPeelLoopsEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder29AdvanceToOsrEntryAndPeelLoopsEv:
.LFB24509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$344, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rdi, -160(%rbp)
	leaq	-352(%rbp), %rdi
	movq	$0, -344(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -336(%rbp)
	movq	$0, -328(%rbp)
	movaps	%xmm0, -320(%rbp)
	movaps	%xmm0, -304(%rbp)
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState15IteratorsStatesENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm
	cmpq	$0, -336(%rbp)
	je	.L3178
	movdqa	-352(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	leaq	-256(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm1, -256(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState15IteratorsStatesENS1_22RecyclingZoneAllocatorIS5_EEE17_M_initialize_mapEm
	movq	-312(%rbp), %r9
	movdqa	-192(%rbp), %xmm7
	movq	-320(%rbp), %xmm3
	movq	-296(%rbp), %r8
	movq	-288(%rbp), %xmm1
	movq	-280(%rbp), %rcx
	movaps	%xmm7, -288(%rbp)
	movq	%r9, %xmm7
	movq	-304(%rbp), %xmm2
	punpcklqdq	%xmm7, %xmm3
	movq	-264(%rbp), %rdx
	movups	%xmm3, -120(%rbp)
	movq	%r8, %xmm3
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	punpcklqdq	%xmm3, %xmm2
	movq	-344(%rbp), %rax
	movq	-272(%rbp), %xmm0
	movups	%xmm2, -104(%rbp)
	movq	%rcx, %xmm2
	movq	-352(%rbp), %xmm4
	movq	-336(%rbp), %r11
	punpcklqdq	%xmm2, %xmm1
	movq	-328(%rbp), %r10
	movq	-240(%rbp), %rsi
	movaps	%xmm5, -320(%rbp)
	movq	-232(%rbp), %rdi
	movdqa	-176(%rbp), %xmm5
	movaps	%xmm6, -304(%rbp)
	movq	%rax, %xmm6
	movups	%xmm1, -88(%rbp)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm6, %xmm4
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -336(%rbp)
	movq	%rdi, -328(%rbp)
	movq	%r11, -136(%rbp)
	movq	%r10, -128(%rbp)
	movaps	%xmm5, -272(%rbp)
	movups	%xmm4, -152(%rbp)
	movups	%xmm0, -72(%rbp)
	testq	%rsi, %rsi
	je	.L3099
	movq	-264(%rbp), %rcx
	movq	-296(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L3100
	.p2align 4,,10
	.p2align 3
.L3103:
	testq	%rax, %rax
	je	.L3101
	cmpq	$10, 8(%rax)
	ja	.L3102
.L3101:
	movq	(%rdx), %rax
	movq	$10, 8(%rax)
	movq	-344(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -344(%rbp)
.L3102:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L3103
	movq	-328(%rbp), %rdi
	movq	-336(%rbp), %rsi
.L3100:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L3099
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L3099:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder16OsrIteratorState17ProcessOsrPreludeEv
	movq	160(%rbx), %rax
	movl	296(%rax), %r12d
	testl	%r12d, %r12d
	js	.L3179
	movq	168(%rbx), %rdi
	leaq	208(%rbx), %r13
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment17FillWithOsrValuesEv
	movq	160(%rbx), %rdi
	movl	%r12d, %esi
	leaq	136(%rbx), %r12
	call	_ZNK2v88internal8compiler16BytecodeAnalysis14GetLoopInfoForEi@PLT
	movl	(%rax), %eax
	movl	%eax, -372(%rbp)
	cmpl	$-1, %eax
	je	.L3138
	.p2align 4,,10
	.p2align 3
.L3139:
	movq	160(%rbx), %rdi
	movl	-372(%rbp), %esi
	call	_ZNK2v88internal8compiler16BytecodeAnalysis14GetLoopInfoForEi@PLT
	movq	%r12, %rdi
	movq	%rax, -384(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv@PLT
	testb	%al, %al
	jne	.L3109
	.p2align 4,,10
	.p2align 3
.L3184:
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	cmpb	$-118, %al
	je	.L3110
.L3113:
	movq	552(%rbx), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	128(%rbx), %rax
	movl	144(%rbx), %r15d
	cmpl	$-1, 24(%rax)
	je	.L3111
	cmpl	32(%rax), %r15d
	je	.L3180
.L3111:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movq	%r13, %r14
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder30ExitThenEnterExceptionHandlersEi
	movq	216(%rbx), %rax
	testq	%rax, %rax
	jne	.L3114
	jmp	.L3181
	.p2align 4,,10
	.p2align 3
.L3182:
	movq	%rax, %r14
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3116
.L3114:
	cmpl	32(%rax), %r15d
	jle	.L3182
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3114
.L3116:
	movq	168(%rbx), %rax
	cmpq	%r14, %r13
	je	.L3115
	cmpl	32(%r14), %r15d
	jl	.L3115
	movb	$1, 416(%rbx)
	testq	%rax, %rax
	je	.L3119
	movq	40(%r14), %r9
	movq	160(%rbx), %rdi
	movl	%r15d, %esi
	movq	%r9, -360(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movq	-360(%rbp), %r9
	movq	168(%rbx), %rsi
	movq	%rax, %rdx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment5MergeEPS3_PKNS1_21BytecodeLivenessStateE
.L3119:
	movq	40(%r14), %rax
	movq	%rax, 168(%rbx)
.L3115:
	testq	%rax, %rax
	je	.L3120
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildLoopHeaderEnvironmentEi
	cmpb	$0, 184(%rbx)
	je	.L3121
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	cmpb	$-89, %al
	je	.L3183
.L3121:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitSingleBytecodeEv.part.0
.L3120:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv@PLT
	testb	%al, %al
	je	.L3184
.L3109:
	movl	144(%rbx), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder30ExitThenEnterExceptionHandlersEi
	movq	216(%rbx), %rax
	movl	144(%rbx), %r8d
	testq	%rax, %rax
	je	.L3123
	movq	%r13, %r15
	jmp	.L3124
	.p2align 4,,10
	.p2align 3
.L3185:
	movq	%rax, %r15
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3125
.L3124:
	cmpl	32(%rax), %r8d
	jle	.L3185
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3124
.L3125:
	cmpq	%r15, %r13
	je	.L3123
	cmpl	32(%r15), %r8d
	jl	.L3123
	cmpq	$0, 168(%rbx)
	movb	$1, 416(%rbx)
	je	.L3128
	movq	40(%r15), %r9
	movq	160(%rbx), %rdi
	movl	%r8d, %esi
	movq	%r9, -360(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movq	-360(%rbp), %r9
	movq	168(%rbx), %rsi
	movq	%rax, %rdx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment5MergeEPS3_PKNS1_21BytecodeLivenessStateE
	movl	144(%rbx), %r8d
.L3128:
	movq	40(%r15), %rax
	movq	%rax, 168(%rbx)
.L3123:
	cmpq	$0, 240(%rbx)
	je	.L3132
	movq	224(%rbx), %r15
	cmpq	%r15, %r13
	jne	.L3133
	jmp	.L3132
	.p2align 4,,10
	.p2align 3
.L3186:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, -368(%rbp)
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	-368(%rbp), %rdx
	subq	$1, 240(%rbx)
	movl	-360(%rbp), %r8d
	cmpq	%rdx, %r13
	je	.L3132
	movq	%rdx, %r15
.L3133:
	cmpl	%r8d, 32(%r15)
	movl	%r8d, -360(%rbp)
	jle	.L3186
.L3132:
	movq	-384(%rbp), %rax
	movl	-372(%rbp), %esi
	movl	(%rax), %r15d
	movq	-160(%rbp), %rax
	leaq	136(%rax), %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayAccessor9SetOffsetEi@PLT
	movq	-160(%rbp), %rdx
	movq	-88(%rbp), %rax
	movl	%r15d, 180(%rdx)
	cmpq	-80(%rbp), %rax
	je	.L3187
.L3131:
	movdqu	-48(%rax), %xmm5
	movaps	%xmm5, -256(%rbp)
	movdqu	-32(%rax), %xmm6
	movaps	%xmm6, -240(%rbp)
	movdqu	-16(%rax), %xmm7
	movaps	%xmm7, -224(%rbp)
	movl	-40(%rax), %edi
	movl	-8(%rax), %esi
	movl	-48(%rax), %ecx
	movq	128(%rdx), %rax
	movl	%edi, 24(%rax)
	movdqa	-240(%rbp), %xmm4
	movups	%xmm4, 32(%rax)
	movzbl	-224(%rbp), %edi
	movl	%esi, 56(%rax)
	movb	%dil, 48(%rax)
	movq	-88(%rbp), %rax
	movl	%ecx, 400(%rdx)
	movq	-80(%rbp), %rdx
	cmpq	%rdx, %rax
	je	.L3134
	subq	$48, %rax
	movq	%rax, -88(%rbp)
.L3135:
	movq	-384(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -372(%rbp)
	cmpl	$-1, %eax
	jne	.L3139
.L3138:
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L3097
	movq	-64(%rbp), %rcx
	movq	-96(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L3140
	movq	-144(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L3143:
	testq	%rax, %rax
	je	.L3141
	cmpq	$10, 8(%rax)
	ja	.L3142
.L3141:
	movq	(%rdx), %rax
	movq	$10, 8(%rax)
	movq	-144(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -144(%rbp)
.L3142:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L3143
	movq	-136(%rbp), %rax
.L3140:
	movq	-128(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L3097
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L3097:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3188
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3110:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	cmpl	-372(%rbp), %eax
	jne	.L3113
	jmp	.L3109
	.p2align 4,,10
	.p2align 3
.L3180:
	movq	40(%rax), %rax
	movq	536(%rbx), %rcx
	movabsq	$140735340871680, %rdx
	andq	544(%rbx), %rdx
	andl	$2147483646, %eax
	orq	%rdx, %rax
	movq	%rax, 16(%rcx)
	movq	128(%rbx), %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	jmp	.L3111
	.p2align 4,,10
	.p2align 3
.L3181:
	movq	168(%rbx), %rax
	jmp	.L3115
	.p2align 4,,10
	.p2align 3
.L3183:
	movb	$0, 184(%rbx)
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L3187:
	movq	-64(%rbp), %rax
	movq	-8(%rax), %rax
	addq	$480, %rax
	jmp	.L3131
	.p2align 4,,10
	.p2align 3
.L3134:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L3136
	cmpq	$10, 8(%rax)
	ja	.L3137
.L3136:
	movq	$10, 8(%rdx)
	movq	-144(%rbp), %rax
	movq	%rax, (%rdx)
	movq	%rdx, -144(%rbp)
.L3137:
	movq	-64(%rbp), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, -64(%rbp)
	movq	-8(%rax), %rax
	leaq	480(%rax), %rdx
	movq	%rax, -80(%rbp)
	addq	$432, %rax
	movq	%rdx, -72(%rbp)
	movq	%rax, -88(%rbp)
	jmp	.L3135
.L3178:
	movdqa	-352(%rbp), %xmm2
	movdqa	-320(%rbp), %xmm3
	movq	$0, -136(%rbp)
	movq	-328(%rbp), %rax
	movdqa	-272(%rbp), %xmm1
	movups	%xmm2, -152(%rbp)
	movdqa	-304(%rbp), %xmm2
	movups	%xmm3, -120(%rbp)
	movdqa	-288(%rbp), %xmm3
	movq	%rax, -128(%rbp)
	movups	%xmm2, -104(%rbp)
	movups	%xmm3, -88(%rbp)
	movups	%xmm1, -72(%rbp)
	jmp	.L3099
.L3179:
	leaq	.LC10(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3188:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24509:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder29AdvanceToOsrEntryAndPeelLoopsEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder29AdvanceToOsrEntryAndPeelLoopsEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder14VisitBytecodesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitBytecodesEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitBytecodesEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitBytecodesEv:
.LFB24514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	160(%rdi), %rax
	movq	160(%rax), %rdx
	cmpq	%rdx, 168(%rax)
	je	.L3190
	movq	168(%rdi), %r12
	movsd	.LC19(%rip), %xmm0
	movq	16(%rdi), %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, 80(%r12)
.L3190:
	cmpb	$0, 176(%rbx)
	jne	.L3221
.L3191:
	leaq	136(%rbx), %r12
	xorl	%r14d, %r14d
	leaq	208(%rbx), %r13
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv@PLT
	testb	%al, %al
	jne	.L3192
	.p2align 4,,10
	.p2align 3
.L3226:
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	leal	-41(%rax), %ecx
	cmpb	$54, %cl
	ja	.L3193
	movabsq	$18014398509482017, %rax
	shrq	%cl, %rax
	testb	$1, %al
	movl	$1, %eax
	cmovne	%eax, %r14d
.L3193:
	movq	552(%rbx), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	128(%rbx), %rax
	movl	144(%rbx), %r15d
	cmpl	$-1, 24(%rax)
	je	.L3194
	cmpl	32(%rax), %r15d
	je	.L3222
.L3194:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder30ExitThenEnterExceptionHandlersEi
	movq	216(%rbx), %rax
	movq	%r13, %rcx
	testq	%rax, %rax
	jne	.L3195
	jmp	.L3223
	.p2align 4,,10
	.p2align 3
.L3224:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3197
.L3195:
	cmpl	32(%rax), %r15d
	jle	.L3224
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3195
.L3197:
	movq	168(%rbx), %rax
	cmpq	%rcx, %r13
	je	.L3196
	cmpl	32(%rcx), %r15d
	jl	.L3196
	movb	$1, 416(%rbx)
	testq	%rax, %rax
	je	.L3200
	movq	40(%rcx), %r9
	movq	160(%rbx), %rdi
	movl	%r15d, %esi
	movq	%rcx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi@PLT
	movq	-56(%rbp), %r9
	movq	168(%rbx), %rsi
	movq	%rax, %rdx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11Environment5MergeEPS3_PKNS1_21BytecodeLivenessStateE
	movq	-64(%rbp), %rcx
.L3200:
	movq	40(%rcx), %rax
	movq	%rax, 168(%rbx)
.L3196:
	testq	%rax, %rax
	je	.L3201
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder26BuildLoopHeaderEnvironmentEi
	cmpb	$0, 184(%rbx)
	je	.L3202
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	cmpb	$-89, %al
	je	.L3225
.L3202:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder19VisitSingleBytecodeEv.part.0
.L3201:
	movq	%r12, %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv@PLT
	testb	%al, %al
	je	.L3226
.L3192:
	cmpb	$1, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	je	.L3189
	testb	%r14b, %r14b
	jne	.L3227
.L3189:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3222:
	.cfi_restore_state
	movq	40(%rax), %rax
	movq	536(%rbx), %rcx
	movabsq	$140735340871680, %rdx
	andq	544(%rbx), %rdx
	andl	$2147483646, %eax
	orq	%rdx, %rax
	movq	%rax, 16(%rcx)
	movq	128(%rbx), %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	jmp	.L3194
	.p2align 4,,10
	.p2align 3
.L3223:
	movq	168(%rbx), %rax
	jmp	.L3196
	.p2align 4,,10
	.p2align 3
.L3225:
	movb	$0, 184(%rbx)
	jmp	.L3201
	.p2align 4,,10
	.p2align 3
.L3221:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder29AdvanceToOsrEntryAndPeelLoopsEv
	jmp	.L3191
	.p2align 4,,10
	.p2align 3
.L3227:
	movq	16(%rbx), %rax
	movl	$71, %esi
	movq	360(%rax), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	.cfi_endproc
.LFE24514:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitBytecodesEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitBytecodesEv
	.section	.text._ZN2v88internal8compiler20BytecodeGraphBuilder11CreateGraphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20BytecodeGraphBuilder11CreateGraphEv
	.type	_ZN2v88internal8compiler20BytecodeGraphBuilder11CreateGraphEv, @function
_ZN2v88internal8compiler20BytecodeGraphBuilder11CreateGraphEv:
.LFB24481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	536(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	544(%rdi), %rax
	movq	16(%r13), %rdx
	movq	%rdx, -216(%rbp)
	testb	$1, %al
	jne	.L3229
	movq	%rax, %rcx
	movq	%rax, %rdx
	shrq	%rcx
	shrq	$31, %rdx
	andl	$1073741823, %ecx
	movzwl	%dx, %edx
	orl	%edx, %ecx
	jne	.L3229
.L3230:
	movdqu	40(%rbx), %xmm0
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	movaps	%xmm0, -160(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	movq	%r12, %rdi
	movq	%rdx, -232(%rbp)
	movq	%rax, -240(%rbp)
	movdqa	-240(%rbp), %xmm1
	movaps	%xmm1, -160(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeArrayRef15parameter_countEv@PLT
	movq	16(%rbx), %rdx
	leal	4(%rax), %esi
	movq	8(%rdx), %rdi
	movq	(%rdx), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5StartEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, 8(%r14)
	movq	16(%rbx), %rax
	movdqu	40(%rbx), %xmm2
	movq	(%rax), %rax
	movq	8(%rax), %r9
	movaps	%xmm2, -160(%rbp)
	movq	%r9, -248(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	leaq	-176(%rbp), %rdi
	movq	%rdx, -232(%rbp)
	movq	%rax, -240(%rbp)
	movdqa	-240(%rbp), %xmm3
	movaps	%xmm3, -176(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeArrayRef41incoming_new_target_or_generator_registerEv@PLT
	movdqu	40(%rbx), %xmm4
	movq	%r12, %rdi
	movl	%eax, %r15d
	movaps	%xmm4, -160(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	leaq	-192(%rbp), %rdi
	movq	%rdx, -232(%rbp)
	movq	%rax, -240(%rbp)
	movdqa	-240(%rbp), %xmm5
	movaps	%xmm5, -192(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeArrayRef15parameter_countEv@PLT
	movdqu	40(%rbx), %xmm6
	movq	%r12, %rdi
	movl	%eax, %r14d
	movaps	%xmm6, -160(%rbp)
	call	_ZNK2v88internal8compiler21SharedFunctionInfoRef16GetBytecodeArrayEv@PLT
	leaq	-208(%rbp), %rdi
	movq	%rdx, -232(%rbp)
	movq	%rax, -240(%rbp)
	movdqa	-240(%rbp), %xmm7
	movaps	%xmm7, -208(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeArrayRef14register_countEv@PLT
	movl	%r15d, %r8d
	movl	%r14d, %ecx
	movq	%rbx, %rsi
	movq	-248(%rbp), %r9
	movl	%eax, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC1EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE
	movq	%r12, 168(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder14VisitBytecodesEv
	movq	440(%rbx), %r14
	movq	448(%rbx), %r12
	movq	16(%rbx), %rax
	subq	%r14, %r12
	sarq	$3, %r12
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	movslq	%r12d, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3EndEm@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	%r12d, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	%r8, 16(%rax)
	movq	-216(%rbp), %rax
	movq	%rax, 16(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3236
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3229:
	.cfi_restore_state
	movq	%rax, 16(%r13)
	jmp	.L3230
.L3236:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24481:
	.size	_ZN2v88internal8compiler20BytecodeGraphBuilder11CreateGraphEv, .-_ZN2v88internal8compiler20BytecodeGraphBuilder11CreateGraphEv
	.section	.rodata._ZN2v88internal8compiler22BuildGraphFromBytecodeEPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE.str1.1,"aMS",@progbits,1
.LC21:
	.string	"storage_.is_populated_"
	.section	.text._ZN2v88internal8compiler22BuildGraphFromBytecodeEPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler22BuildGraphFromBytecodeEPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE
	.type	_ZN2v88internal8compiler22BuildGraphFromBytecodeEPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE, @function
_ZN2v88internal8compiler22BuildGraphFromBytecodeEPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE:
.LFB24774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$600, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rbp), %rax
	movq	24(%rbp), %rsi
	movq	%fs:40, %rbx
	movq	%rbx, -40(%rbp)
	xorl	%ebx, %ebx
	cmpb	$0, 24(%r10)
	movq	48(%rbp), %rdi
	je	.L3286
	subq	$8, %rsp
	movdqu	32(%r10), %xmm0
	leaq	-608(%rbp), %r12
	leaq	-624(%rbp), %r13
	pushq	%rdi
	movl	40(%rbp), %edi
	movaps	%xmm0, -624(%rbp)
	pushq	%rdi
	movl	32(%rbp), %edi
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rsi
	movq	%r10, %rsi
	pushq	%rax
	pushq	%r9
	movq	%rcx, %r9
	movq	%r13, %rcx
	pushq	%r8
	movq	%rdx, %r8
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler20BytecodeGraphBuilderC1EPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_16NativeContextRefERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE
	addq	$64, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20BytecodeGraphBuilder11CreateGraphEv
	movq	-288(%rbp), %rax
	testq	%rax, %rax
	je	.L3239
	movq	-216(%rbp), %rcx
	movq	-248(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L3240
	movq	-296(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L3243:
	testq	%rax, %rax
	je	.L3241
	cmpq	$32, 8(%rax)
	ja	.L3242
.L3241:
	movq	(%rdx), %rax
	movq	$32, 8(%rax)
	movq	-296(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -296(%rbp)
.L3242:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L3243
	movq	-288(%rbp), %rax
.L3240:
	movq	-280(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L3239
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L3239:
	movq	-336(%rbp), %r12
	testq	%r12, %r12
	je	.L3245
	leaq	-360(%rbp), %r13
.L3248:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L3246
.L3247:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L3247
.L3246:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L3248
.L3245:
	movq	-392(%rbp), %r12
	testq	%r12, %r12
	je	.L3249
	leaq	-416(%rbp), %r13
.L3252:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L3250
.L3251:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L3251
.L3250:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L3252
.L3249:
	movq	-472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3253
	movq	(%rdi), %rax
	call	*72(%rax)
.L3253:
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3237
	movl	$64, %esi
	call	_ZdlPvm@PLT
.L3237:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3287
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3286:
	.cfi_restore_state
	leaq	.LC21(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3287:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24774:
	.size	_ZN2v88internal8compiler22BuildGraphFromBytecodeEPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE, .-_ZN2v88internal8compiler22BuildGraphFromBytecodeEPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE:
.LFB31150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE31150:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE, .-_GLOBAL__sub_I__ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler20BytecodeGraphBuilder11EnvironmentC2EPS2_iiNS0_11interpreter8RegisterEPNS1_4NodeE
	.section	.rodata.CSWTCH.612,"a"
	.type	CSWTCH.612, @object
	.size	CSWTCH.612, 4
CSWTCH.612:
	.byte	0
	.byte	0
	.byte	1
	.byte	2
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata._ZStL19piecewise_construct,"a"
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC7:
	.long	2143289344
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC19:
	.long	0
	.long	-1073741824
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
