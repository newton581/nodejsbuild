	.file	"redundancy-elimination.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler21RedundancyElimination12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"RedundancyElimination"
	.section	.text._ZNK2v88internal8compiler21RedundancyElimination12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler21RedundancyElimination12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler21RedundancyElimination12reducer_nameEv
	.type	_ZNK2v88internal8compiler21RedundancyElimination12reducer_nameEv, @function
_ZNK2v88internal8compiler21RedundancyElimination12reducer_nameEv:
.LFB10163:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10163:
	.size	_ZNK2v88internal8compiler21RedundancyElimination12reducer_nameEv, .-_ZNK2v88internal8compiler21RedundancyElimination12reducer_nameEv
	.section	.text._ZN2v88internal8compiler21RedundancyEliminationD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RedundancyEliminationD2Ev
	.type	_ZN2v88internal8compiler21RedundancyEliminationD2Ev, @function
_ZN2v88internal8compiler21RedundancyEliminationD2Ev:
.LFB10426:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10426:
	.size	_ZN2v88internal8compiler21RedundancyEliminationD2Ev, .-_ZN2v88internal8compiler21RedundancyEliminationD2Ev
	.globl	_ZN2v88internal8compiler21RedundancyEliminationD1Ev
	.set	_ZN2v88internal8compiler21RedundancyEliminationD1Ev,_ZN2v88internal8compiler21RedundancyEliminationD2Ev
	.section	.text._ZN2v88internal8compiler21RedundancyEliminationD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RedundancyEliminationD0Ev
	.type	_ZN2v88internal8compiler21RedundancyEliminationD0Ev, @function
_ZN2v88internal8compiler21RedundancyEliminationD0Ev:
.LFB10428:
	.cfi_startproc
	endbr64
	movl	$56, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10428:
	.size	_ZN2v88internal8compiler21RedundancyEliminationD0Ev, .-_ZN2v88internal8compiler21RedundancyEliminationD0Ev
	.section	.text._ZN2v88internal8compiler21RedundancyEliminationC2EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RedundancyEliminationC2EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler21RedundancyEliminationC2EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE, @function
_ZN2v88internal8compiler21RedundancyEliminationC2EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE:
.LFB10423:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler21RedundancyEliminationE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rdx, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	%rdx, 48(%rdi)
	ret
	.cfi_endproc
.LFE10423:
	.size	_ZN2v88internal8compiler21RedundancyEliminationC2EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE, .-_ZN2v88internal8compiler21RedundancyEliminationC2EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler21RedundancyEliminationC1EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler21RedundancyEliminationC1EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE,_ZN2v88internal8compiler21RedundancyEliminationC2EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks4CopyEPNS0_4ZoneEPKS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks4CopyEPNS0_4ZoneEPKS3_
	.type	_ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks4CopyEPNS0_4ZoneEPKS3_, @function
_ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks4CopyEPNS0_4ZoneEPKS3_:
.LFB10430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L10
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
	movdqu	(%rbx), %xmm0
	movups	%xmm0, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movdqu	(%rbx), %xmm0
	movups	%xmm0, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10430:
	.size	_ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks4CopyEPNS0_4ZoneEPKS3_, .-_ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks4CopyEPNS0_4ZoneEPKS3_
	.section	.text._ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks5EmptyEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks5EmptyEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks5EmptyEPNS0_4ZoneE, @function
_ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks5EmptyEPNS0_4ZoneE:
.LFB10431:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L18
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10431:
	.size	_ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks5EmptyEPNS0_4ZoneE, .-_ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks5EmptyEPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks6EqualsEPKS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks6EqualsEPKS3_
	.type	_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks6EqualsEPKS3_, @function
_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks6EqualsEPKS3_:
.LFB10432:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rcx
	xorl	%eax, %eax
	cmpq	%rcx, 8(%rdi)
	jne	.L19
	movq	(%rdi), %rdx
	movq	(%rsi), %rax
	cmpq	%rax, %rdx
	jne	.L21
.L23:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	8(%rdx), %rdx
	movq	8(%rax), %rax
	cmpq	%rax, %rdx
	je	.L23
.L21:
	movq	(%rax), %rcx
	cmpq	%rcx, (%rdx)
	je	.L26
	xorl	%eax, %eax
.L19:
	ret
	.cfi_endproc
.LFE10432:
	.size	_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks6EqualsEPKS3_, .-_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks6EqualsEPKS3_
	.section	.text._ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks5MergeEPKS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks5MergeEPKS3_
	.type	_ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks5MergeEPKS3_, @function
_ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks5MergeEPKS3_:
.LFB10433:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rax
	movq	8(%rdi), %rcx
	movq	(%rsi), %r8
	cmpq	%rcx, %rax
	jbe	.L40
	.p2align 4,,10
	.p2align 3
.L28:
	subq	$1, %rax
	movq	8(%r8), %r8
	cmpq	%rcx, %rax
	jne	.L28
	movq	(%rdi), %rdx
.L29:
	subq	$1, %rax
	cmpq	%r8, %rdx
	je	.L41
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%rax, 8(%rdi)
	movq	8(%rdx), %rdx
	subq	$1, %rax
	movq	%rdx, (%rdi)
	movq	8(%r8), %r8
	cmpq	%r8, %rdx
	jne	.L34
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movq	(%rdi), %rdx
	jnb	.L42
	subq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L32:
	movq	8(%rdx), %rdx
	movq	%rcx, %rsi
	movq	%rcx, 8(%rdi)
	subq	$1, %rcx
	movq	%rdx, (%rdi)
	cmpq	%rax, %rsi
	jne	.L32
	subq	$1, %rax
	cmpq	%r8, %rdx
	jne	.L34
.L41:
	ret
.L42:
	movq	%rcx, %rax
	jmp	.L29
	.cfi_endproc
.LFE10433:
	.size	_ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks5MergeEPKS3_, .-_ZN2v88internal8compiler21RedundancyElimination16EffectPathChecks5MergeEPKS3_
	.section	.text._ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks8AddCheckEPNS0_4ZoneEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks8AddCheckEPNS0_4ZoneEPNS1_4NodeE
	.type	_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks8AddCheckEPNS0_4ZoneEPNS1_4NodeE, @function
_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks8AddCheckEPNS0_4ZoneEPNS1_4NodeE:
.LFB10434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$24, %rsp
	movq	16(%rsi), %r14
	movq	24(%rsi), %rax
	subq	%r14, %rax
	cmpq	$15, %rax
	jbe	.L49
	leaq	16(%r14), %rax
	movq	%rax, 16(%rsi)
.L45:
	movq	0(%r13), %rax
	movq	%rdx, (%r14)
	movq	%rax, 8(%r14)
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L50
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
.L47:
	movq	8(%r13), %rdx
	movq	%r14, (%rax)
	addq	$1, %rdx
	movq	%rdx, 8(%rax)
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L47
	.cfi_endproc
.LFE10434:
	.size	_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks8AddCheckEPNS0_4ZoneEPNS1_4NodeE, .-_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks8AddCheckEPNS0_4ZoneEPNS1_4NodeE
	.section	.text._ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks11LookupCheckEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks11LookupCheckEPNS1_4NodeE
	.type	_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks11LookupCheckEPNS1_4NodeE, @function
_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks11LookupCheckEPNS1_4NodeE:
.LFB10437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.L65(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	.L67(%rip), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-64(%rbp), %rbx
	subq	$40, %rsp
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	jne	.L52
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L121:
	movzwl	16(%rdi), %esi
	movzwl	16(%rcx), %ecx
	movl	%esi, %eax
	cmpw	$222, %si
	je	.L119
	cmpl	$227, %esi
	je	.L120
	cmpl	$108, %esi
	jne	.L58
	cmpw	$109, %cx
	je	.L54
	cmpw	$108, %cx
	je	.L54
.L64:
	movq	8(%r13), %r13
	testq	%r13, %r13
	je	.L79
.L52:
	movq	0(%r13), %rdx
	movq	(%r12), %rcx
	movq	(%rdx), %rdi
	cmpq	%rcx, %rdi
	jne	.L121
.L54:
	movslq	20(%rdi), %rax
	leaq	32(%rdx), %r8
	movq	%rax, %rcx
	leaq	-8(,%rax,8), %rax
.L76:
	subl	$1, %ecx
	js	.L71
.L122:
	movzbl	23(%rdx), %esi
	andl	$15, %esi
	cmpl	$15, %esi
	je	.L72
	leaq	(%r8,%rax), %rsi
.L73:
	movq	(%rsi), %rdi
	movzbl	23(%r12), %esi
	andl	$15, %esi
	cmpl	$15, %esi
	je	.L74
	movq	32(%r12,%rax), %rsi
	subq	$8, %rax
	cmpq	%rsi, %rdi
	jne	.L64
	subl	$1, %ecx
	jns	.L122
.L71:
	movq	8(%r12), %rsi
	movq	0(%r13), %rdx
	testq	%rsi, %rsi
	je	.L51
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L51
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rsi
	je	.L117
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L64
.L117:
	movq	0(%r13), %rdx
.L51:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L123
	addq	$40, %rsp
	movq	%rdx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movq	(%r8), %rsi
	leaq	16(%rsi,%rax), %rsi
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L74:
	movq	32(%r12), %rsi
	movq	16(%rsi,%rax), %rsi
	subq	$8, %rax
	cmpq	%rsi, %rdi
	je	.L76
	movq	8(%r13), %r13
	testq	%r13, %r13
	jne	.L52
	.p2align 4,,10
	.p2align 3
.L79:
	xorl	%edx, %edx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L119:
	cmpw	$225, %cx
	je	.L54
.L56:
	cmpw	%cx, %ax
	jne	.L64
.L62:
	subw	$198, %ax
	cmpw	$29, %ax
	ja	.L64
	movzwl	%ax, %eax
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks11LookupCheckEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L65:
	.long	.L54-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L54-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L54-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L64-.L65
	.long	.L54-.L65
	.long	.L64-.L65
	.long	.L54-.L65
	.section	.text._ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks11LookupCheckEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L120:
	cmpw	$221, %cx
	jne	.L56
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L58:
	cmpl	$223, %esi
	jne	.L60
	cmpw	$224, %cx
	jne	.L64
	jmp	.L54
.L123:
	call	__stack_chk_fail@PLT
.L60:
	cmpw	%cx, %si
	jne	.L64
	cmpw	$118, %si
	ja	.L62
	cmpw	$95, %si
	jbe	.L64
	subl	$106, %eax
	cmpw	$6, %ax
	ja	.L54
	movzwl	%ax, %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks11LookupCheckEPNS1_4NodeE
	.align 4
	.align 4
.L67:
	.long	.L66-.L67
	.long	.L66-.L67
	.long	.L54-.L67
	.long	.L66-.L67
	.long	.L68-.L67
	.long	.L68-.L67
	.long	.L66-.L67
	.section	.text._ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks11LookupCheckEPNS1_4NodeE
.L66:
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal8compiler26CheckMinusZeroParametersOfEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler26CheckMinusZeroParametersOfEPKNS1_8OperatorE@PLT
	movq	-72(%rbp), %rcx
	movzbl	(%rax), %eax
	cmpb	%al, (%rcx)
	jne	.L64
	movq	-80(%rbp), %rdx
	movq	(%rdx), %rdi
	jmp	.L54
.L68:
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal8compiler28CheckTaggedInputParametersOfEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler28CheckTaggedInputParametersOfEPKNS1_8OperatorE@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	%rax, %r8
	movzbl	(%rcx), %eax
	cmpb	%al, (%r8)
	je	.L83
	testb	%al, %al
	jne	.L64
.L83:
	movq	(%rdx), %rdi
	jmp	.L54
	.cfi_endproc
.LFE10437:
	.size	_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks11LookupCheckEPNS1_4NodeE, .-_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks11LookupCheckEPNS1_4NodeE
	.section	.text._ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks20LookupBoundsCheckForEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks20LookupBoundsCheckForEPNS1_4NodeE
	.type	_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks20LookupBoundsCheckForEPNS1_4NodeE, @function
_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks20LookupBoundsCheckForEPNS1_4NodeE:
.LFB10438:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L128
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L126:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L134
.L128:
	movq	(%rax), %rdx
	movq	(%rdx), %rcx
	cmpw	$218, 16(%rcx)
	jne	.L126
	movzbl	23(%rdx), %ecx
	movq	32(%rdx), %rdi
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L127
	movq	16(%rdi), %rdi
.L127:
	cmpq	%rdi, %rsi
	jne	.L126
	movq	%rdx, %rax
.L124:
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	ret
	.cfi_endproc
.LFE10438:
	.size	_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks20LookupBoundsCheckForEPNS1_4NodeE, .-_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks20LookupBoundsCheckForEPNS1_4NodeE
	.section	.text._ZNK2v88internal8compiler21RedundancyElimination24PathChecksForEffectNodes3GetEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler21RedundancyElimination24PathChecksForEffectNodes3GetEPNS1_4NodeE
	.type	_ZNK2v88internal8compiler21RedundancyElimination24PathChecksForEffectNodes3GetEPNS1_4NodeE, @function
_ZNK2v88internal8compiler21RedundancyElimination24PathChecksForEffectNodes3GetEPNS1_4NodeE:
.LFB10439:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	xorl	%r8d, %r8d
	movl	20(%rsi), %edx
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L135
	movq	(%rcx,%rdx,8), %r8
.L135:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE10439:
	.size	_ZNK2v88internal8compiler21RedundancyElimination24PathChecksForEffectNodes3GetEPNS1_4NodeE, .-_ZNK2v88internal8compiler21RedundancyElimination24PathChecksForEffectNodes3GetEPNS1_4NodeE
	.section	.rodata._ZNSt6vectorIPKN2v88internal8compiler21RedundancyElimination16EffectPathChecksENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIPKN2v88internal8compiler21RedundancyElimination16EffectPathChecksENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_,"axG",@progbits,_ZNSt6vectorIPKN2v88internal8compiler21RedundancyElimination16EffectPathChecksENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPKN2v88internal8compiler21RedundancyElimination16EffectPathChecksENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	.type	_ZNSt6vectorIPKN2v88internal8compiler21RedundancyElimination16EffectPathChecksENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_, @function
_ZNSt6vectorIPKN2v88internal8compiler21RedundancyElimination16EffectPathChecksENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_:
.LFB11579:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L248
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	24(%r12), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jb	.L141
	movq	%rdi, %r8
	movq	(%rcx), %xmm0
	subq	%rsi, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L142
	leaq	0(,%rdx,8), %r14
	movq	%rdi, %rdx
	subq	%r14, %rdx
	cmpq	%rdx, %rdi
	je	.L189
	leaq	-8(%rdi), %rax
	movl	$16, %ecx
	subq	%rdx, %rax
	subq	%r14, %rcx
	shrq	$3, %rax
	testq	%rcx, %rcx
	leaq	16(%rdi), %rcx
	setle	%sil
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L190
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L190
	leaq	1(%rax), %r8
	xorl	%eax, %eax
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L145:
	movdqu	(%rdx,%rax), %xmm2
	movups	%xmm2, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L145
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	leaq	(%rdx,%rcx), %rsi
	addq	%rdi, %rcx
	cmpq	%rax, %r8
	je	.L147
	movq	(%rsi), %rax
	movq	%rax, (%rcx)
.L147:
	movq	16(%r12), %rax
.L143:
	addq	%r14, %rax
	movq	%rax, 16(%r12)
	cmpq	%rdx, %rbx
	je	.L148
	subq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%xmm0, -40(%rbp)
	subq	%rdx, %rdi
	call	memmove@PLT
	movq	-40(%rbp), %xmm0
.L148:
	leaq	(%rbx,%r14), %rdx
	cmpq	%rdx, %rbx
	je	.L138
	subq	%rbx, %rdx
	movq	%rbx, %rax
	subq	$8, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rdx, %rdx
	je	.L188
	movq	%rcx, %rdx
	movdqa	%xmm0, %xmm1
	shrq	%rdx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rdx
	addq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L152:
	movups	%xmm1, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L152
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	(%rbx,%rax,8), %r13
	cmpq	%rax, %rcx
	je	.L138
.L188:
	movq	%xmm0, 0(%r13)
.L138:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	subq	%rax, %rdx
	je	.L191
	cmpq	$1, %rdx
	je	.L192
	movq	%rdx, %rsi
	movdqa	%xmm0, %xmm1
	xorl	%eax, %eax
	shrq	%rsi
	punpcklqdq	%xmm1, %xmm1
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%rax, %rcx
	addq	$1, %rax
	salq	$4, %rcx
	movups	%xmm1, (%rdi,%rcx)
	cmpq	%rax, %rsi
	jne	.L156
	movq	%rdx, %rcx
	andq	$-2, %rcx
	leaq	(%rdi,%rcx,8), %rax
	cmpq	%rcx, %rdx
	je	.L157
.L155:
	movq	%xmm0, (%rax)
.L157:
	leaq	(%rdi,%rdx,8), %rsi
.L154:
	movq	%rsi, 16(%r12)
	cmpq	%rbx, %rdi
	je	.L158
	subq	%rbx, %rdi
	leaq	15(%rbx), %rcx
	movq	%rbx, %rdx
	leaq	-8(%rdi), %r9
	subq	%rsi, %rcx
	movq	%r9, %rax
	shrq	$3, %rax
	cmpq	$30, %rcx
	jbe	.L193
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L193
	leaq	1(%rax), %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L160:
	movdqu	(%rbx,%rax), %xmm3
	movups	%xmm3, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L160
	movq	%rcx, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rax
	leaq	(%rbx,%rax), %rdi
	addq	%rax, %rsi
	cmpq	%r9, %rcx
	je	.L161
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
.L161:
	addq	%r8, 16(%r12)
	movq	%rcx, %rax
.L187:
	movq	%rax, %rcx
	movdqa	%xmm0, %xmm1
	shrq	%rcx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rcx
	addq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L165:
	movups	%xmm1, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L165
	movq	%rax, %rdx
	andq	$-2, %rdx
	leaq	(%rbx,%rdx,8), %r13
	cmpq	%rax, %rdx
	jne	.L188
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L141:
	movq	8(%r12), %rsi
	movl	$268435455, %r8d
	movq	%r8, %rax
	subq	%rsi, %rdi
	sarq	$3, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L251
	cmpq	%rdi, %rdx
	movq	%rdi, %rax
	cmovnb	%rdx, %rax
	addq	%rax, %rdi
	setc	%al
	subq	%rsi, %r13
	movzbl	%al, %eax
	testq	%rax, %rax
	jne	.L194
	testq	%rdi, %rdi
	jne	.L252
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L170:
	leaq	0(,%rdx,8), %r9
	leaq	(%rax,%r13), %r11
	leaq	0(%r13,%r9), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	leaq	8(%rcx), %rsi
	setnb	%dil
	cmpq	%rsi, %r11
	setnb	%sil
	orb	%sil, %dil
	je	.L196
	leaq	-1(%rdx), %rsi
	cmpq	$3, %rsi
	jbe	.L196
	movq	(%rcx), %xmm0
	leaq	-2(%rdx), %rdi
	xorl	%esi, %esi
	shrq	%rdi
	addq	$1, %rdi
	punpcklqdq	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L174:
	movq	%rsi, %r10
	addq	$1, %rsi
	salq	$4, %r10
	movups	%xmm0, (%r11,%r10)
	cmpq	%rsi, %rdi
	ja	.L174
	leaq	(%rdi,%rdi), %rsi
	salq	$4, %rdi
	addq	%r11, %rdi
	cmpq	%rsi, %rdx
	je	.L176
	movq	(%rcx), %rdx
	movq	%rdx, (%rdi)
.L176:
	movq	8(%r12), %rcx
	cmpq	%rcx, %rbx
	je	.L197
	leaq	-8(%rbx), %r10
	leaq	15(%rcx), %rdx
	subq	%rcx, %r10
	subq	%rax, %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L198
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L198
	leaq	1(%rsi), %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L179:
	movdqu	(%rcx,%rdx), %xmm4
	movups	%xmm4, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L179
	movq	%r11, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %rcx
	addq	%rax, %rsi
	cmpq	%rdx, %r11
	je	.L181
	movq	(%rcx), %rdx
	movq	%rdx, (%rsi)
.L181:
	leaq	8(%rax,%r10), %rdi
.L177:
	movq	16(%r12), %rdx
	leaq	(%rdi,%r9), %rcx
	cmpq	%rdx, %rbx
	je	.L182
	subq	%rbx, %rdx
	leaq	-8(%rdx), %r10
	leaq	16(%rdi,%r9), %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %rbx
	leaq	16(%rbx), %rdx
	setnb	%dil
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %dil
	je	.L199
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L199
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L184:
	movdqu	(%rbx,%rdx), %xmm5
	movups	%xmm5, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L184
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rdi
	addq	%rdi, %rbx
	addq	%rcx, %rdi
	cmpq	%rdx, %rsi
	je	.L186
	movq	(%rbx), %rdx
	movq	%rdx, (%rdi)
.L186:
	leaq	8(%rcx,%r10), %rcx
.L182:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%r8, 24(%r12)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movl	$2147483640, %esi
	movl	$2147483640, %r14d
.L169:
	movq	(%r12), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L253
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L172:
	leaq	(%rax,%r14), %r8
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L196:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L173:
	movq	(%rcx), %rdi
	movq	%rdi, (%r11,%rsi,8)
	addq	$1, %rsi
	cmpq	%rsi, %rdx
	jne	.L173
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L190:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L144:
	movq	(%rdx,%rcx,8), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rax, %rsi
	jne	.L144
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L193:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L159:
	movq	(%rbx,%rcx,8), %rdi
	movq	%rdi, (%rsi,%rcx,8)
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	%rax, %rdi
	jne	.L159
	addq	%r8, 16(%r12)
	addq	$1, %rax
	testq	%r9, %r9
	je	.L188
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L198:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L178:
	movq	(%rcx,%rdx,8), %rdi
	movq	%rdi, (%rax,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L178
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L199:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L183:
	movq	(%rbx,%rdx,8), %rdi
	movq	%rdi, (%rcx,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L183
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%rdi, %rsi
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L189:
	movq	%rdi, %rax
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L158:
	addq	%r8, %rsi
	movq	%rsi, 16(%r12)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L197:
	movq	%rax, %rdi
	jmp	.L177
.L192:
	movq	%rdi, %rax
	jmp	.L155
.L253:
	movq	%r8, %rdi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	jmp	.L172
.L252:
	cmpq	$268435455, %rdi
	cmova	%r8, %rdi
	leaq	0(,%rdi,8), %r14
	movq	%r14, %rsi
	jmp	.L169
.L251:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11579:
	.size	_ZNSt6vectorIPKN2v88internal8compiler21RedundancyElimination16EffectPathChecksENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_, .-_ZNSt6vectorIPKN2v88internal8compiler21RedundancyElimination16EffectPathChecksENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	.section	.text._ZN2v88internal8compiler21RedundancyElimination24PathChecksForEffectNodes3SetEPNS1_4NodeEPKNS2_16EffectPathChecksE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RedundancyElimination24PathChecksForEffectNodes3SetEPNS1_4NodeEPKNS2_16EffectPathChecksE
	.type	_ZN2v88internal8compiler21RedundancyElimination24PathChecksForEffectNodes3SetEPNS1_4NodeEPKNS2_16EffectPathChecksE, @function
_ZN2v88internal8compiler21RedundancyElimination24PathChecksForEffectNodes3SetEPNS1_4NodeEPKNS2_16EffectPathChecksE:
.LFB10440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -32
	movl	20(%rsi), %ebx
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rcx
	movq	%rsi, %rax
	andl	$16777215, %ebx
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L260
.L255:
	movq	%r13, (%rcx,%rbx,8)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L261
	addq	$32, %rsp
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	leaq	1(%rbx), %rdx
	movq	$0, -32(%rbp)
	cmpq	%rax, %rdx
	ja	.L262
	jnb	.L255
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L255
	movq	%rax, 16(%rdi)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L262:
	leaq	-32(%rbp), %rcx
	subq	%rax, %rdx
	movq	%rdi, -40(%rbp)
	call	_ZNSt6vectorIPKN2v88internal8compiler21RedundancyElimination16EffectPathChecksENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	-40(%rbp), %rdi
	movq	8(%rdi), %rcx
	jmp	.L255
.L261:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10440:
	.size	_ZN2v88internal8compiler21RedundancyElimination24PathChecksForEffectNodes3SetEPNS1_4NodeEPKNS2_16EffectPathChecksE, .-_ZN2v88internal8compiler21RedundancyElimination24PathChecksForEffectNodes3SetEPNS1_4NodeEPKNS2_16EffectPathChecksE
	.section	.text._ZN2v88internal8compiler21RedundancyElimination12UpdateChecksEPNS1_4NodeEPKNS2_16EffectPathChecksE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RedundancyElimination12UpdateChecksEPNS1_4NodeEPKNS2_16EffectPathChecksE
	.type	_ZN2v88internal8compiler21RedundancyElimination12UpdateChecksEPNS1_4NodeEPKNS2_16EffectPathChecksE, @function
_ZN2v88internal8compiler21RedundancyElimination12UpdateChecksEPNS1_4NodeEPKNS2_16EffectPathChecksE:
.LFB10448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	20(%rsi), %r13d
	movq	32(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rcx
	movq	%rsi, %rax
	andl	$16777215, %r13d
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r13
	jnb	.L277
	leaq	(%rcx,%r13,8), %rcx
	movq	(%rcx), %rax
	cmpq	%rax, %rdx
	je	.L266
	testq	%rax, %rax
	je	.L267
	movq	8(%rax), %rdi
	cmpq	%rdi, 8(%rdx)
	jne	.L267
	movq	(%rdx), %rdx
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	jne	.L268
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L278:
	movq	8(%rdx), %rdx
	movq	8(%rax), %rax
	cmpq	%rax, %rdx
	je	.L266
.L268:
	movq	(%rax), %rdi
	cmpq	%rdi, (%rdx)
	je	.L278
.L267:
	movq	%r14, (%rcx)
	movq	%rbx, %rax
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L266:
	xorl	%eax, %eax
.L271:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L279
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L266
	leaq	1(%r13), %rdx
	movq	$0, -48(%rbp)
	movq	%rdi, %r12
	cmpq	%rdx, %rax
	jb	.L280
	jbe	.L270
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L270
	movq	%rax, 32(%rdi)
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L280:
	leaq	-48(%rbp), %rcx
	subq	%rax, %rdx
	leaq	16(%rdi), %rdi
	call	_ZNSt6vectorIPKN2v88internal8compiler21RedundancyElimination16EffectPathChecksENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	24(%r12), %rcx
.L270:
	leaq	(%rcx,%r13,8), %rcx
	movq	%rbx, %rax
	movq	%r14, (%rcx)
	jmp	.L271
.L279:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10448:
	.size	_ZN2v88internal8compiler21RedundancyElimination12UpdateChecksEPNS1_4NodeEPKNS2_16EffectPathChecksE, .-_ZN2v88internal8compiler21RedundancyElimination12UpdateChecksEPNS1_4NodeEPKNS2_16EffectPathChecksE
	.section	.text._ZN2v88internal8compiler21RedundancyElimination15ReduceCheckNodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RedundancyElimination15ReduceCheckNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RedundancyElimination15ReduceCheckNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler21RedundancyElimination15ReduceCheckNodeEPNS1_4NodeE:
.LFB10441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r12), %rcx
	movl	20(%rax), %edx
	movq	32(%r12), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L282
	movq	(%rcx,%rdx,8), %r14
	testq	%r14, %r14
	je	.L282
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21RedundancyElimination16EffectPathChecks11LookupCheckEPNS1_4NodeE
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L285
	movq	8(%r12), %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	(%rdi), %rax
	call	*32(%rax)
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	movq	48(%r12), %r15
	movq	16(%r15), %rbx
	movq	24(%r15), %rax
	subq	%rbx, %rax
	cmpq	$15, %rax
	jbe	.L291
	leaq	16(%rbx), %rax
	movq	%rax, 16(%r15)
.L287:
	movq	(%r14), %rax
	movq	%r13, (%rbx)
	movq	%rax, 8(%rbx)
	movq	16(%r15), %rdx
	movq	24(%r15), %rax
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L292
	leaq	16(%rdx), %rax
	movq	%rax, 16(%r15)
.L289:
	movq	8(%r14), %rax
	movq	%rbx, (%rdx)
	movq	%r13, %rsi
	movq	%r12, %rdi
	addq	$1, %rax
	movq	%rax, 8(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21RedundancyElimination12UpdateChecksEPNS1_4NodeEPKNS2_16EffectPathChecksE
.L291:
	.cfi_restore_state
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L287
.L292:
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L289
	.cfi_endproc
.LFE10441:
	.size	_ZN2v88internal8compiler21RedundancyElimination15ReduceCheckNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler21RedundancyElimination15ReduceCheckNodeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21RedundancyElimination33ReduceSpeculativeNumberComparisonEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RedundancyElimination33ReduceSpeculativeNumberComparisonEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RedundancyElimination33ReduceSpeculativeNumberComparisonEPNS1_4NodeE, @function
_ZN2v88internal8compiler21RedundancyElimination33ReduceSpeculativeNumberComparisonEPNS1_4NodeE:
.LFB10443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21NumberOperationHintOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r14), %rcx
	movl	20(%rax), %edx
	movq	32(%r14), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L294
	movq	(%rcx,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L294
	testb	%r13b, %r13b
	jne	.L297
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal8compiler10BitsetType13UnsignedSmallEv@PLT
	movq	-88(%rbp), %rdx
	movl	%eax, %esi
	orl	$1, %esi
	cmpq	%rsi, -72(%rbp)
	jne	.L331
.L298:
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal8compiler10BitsetType13UnsignedSmallEv@PLT
	movq	-88(%rbp), %rdx
	movl	%eax, %esi
	orl	$1, %esi
	cmpq	%rsi, -64(%rbp)
	jne	.L332
.L297:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21RedundancyElimination12UpdateChecksEPNS1_4NodeEPKNS2_16EffectPathChecksE
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L294:
	xorl	%eax, %eax
.L296:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L333
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	leaq	-64(%rbp), %r15
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-88(%rbp), %rdx
	testb	%al, %al
	jne	.L297
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L308
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L305:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L297
.L308:
	movq	(%rax), %r13
	movq	0(%r13), %rcx
	cmpw	$218, 16(%rcx)
	jne	.L305
	movzbl	23(%r13), %ecx
	movq	32(%r13), %rsi
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L306
	movq	16(%rsi), %rsi
.L306:
	cmpq	%rsi, %rbx
	jne	.L305
	movq	8(%r13), %rsi
	cmpq	%rsi, -64(%rbp)
	je	.L297
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-88(%rbp), %rdx
	testb	%al, %al
	jne	.L297
	movl	$1, %edx
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L331:
	leaq	-72(%rbp), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-88(%rbp), %rdx
	testb	%al, %al
	jne	.L298
	movq	(%rdx), %rax
	movq	-96(%rbp), %rdi
	testq	%rax, %rax
	jne	.L302
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L299:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L298
.L302:
	movq	(%rax), %r13
	movq	0(%r13), %rcx
	cmpw	$218, 16(%rcx)
	jne	.L299
	movzbl	23(%r13), %ecx
	movq	32(%r13), %rsi
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L300
	movq	16(%rsi), %rsi
.L300:
	cmpq	%rsi, %r15
	jne	.L299
	movq	8(%r13), %rsi
	cmpq	%rsi, -72(%rbp)
	je	.L298
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-88(%rbp), %rdx
	testb	%al, %al
	jne	.L298
	xorl	%edx, %edx
.L330:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21RedundancyElimination33ReduceSpeculativeNumberComparisonEPNS1_4NodeE
	testq	%rax, %rax
	cmove	%r12, %rax
	jmp	.L296
.L333:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10443:
	.size	_ZN2v88internal8compiler21RedundancyElimination33ReduceSpeculativeNumberComparisonEPNS1_4NodeE, .-_ZN2v88internal8compiler21RedundancyElimination33ReduceSpeculativeNumberComparisonEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21RedundancyElimination32ReduceSpeculativeNumberOperationEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RedundancyElimination32ReduceSpeculativeNumberOperationEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RedundancyElimination32ReduceSpeculativeNumberOperationEPNS1_4NodeE, @function
_ZN2v88internal8compiler21RedundancyElimination32ReduceSpeculativeNumberOperationEPNS1_4NodeE:
.LFB10444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r13), %rcx
	movl	20(%rax), %edx
	movq	32(%r13), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L335
	movq	(%rcx,%rdx,8), %r15
	testq	%r15, %r15
	je	.L335
	movq	(%r15), %rax
	testq	%rax, %rax
	jne	.L342
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L339:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L338
.L342:
	movq	(%rax), %r14
	movq	(%r14), %rdx
	cmpw	$218, 16(%rdx)
	jne	.L339
	movzbl	23(%r14), %edx
	movq	32(%r14), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L340
	movq	16(%rcx), %rcx
.L340:
	cmpq	%rcx, %rbx
	jne	.L339
	movq	8(%rbx), %rax
	movq	8(%r14), %rsi
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rsi
	jne	.L356
.L338:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RedundancyElimination12UpdateChecksEPNS1_4NodeEPKNS2_16EffectPathChecksE
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L335:
	xorl	%eax, %eax
.L337:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L357
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	leaq	-64(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L338
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	jmp	.L338
.L357:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10444:
	.size	_ZN2v88internal8compiler21RedundancyElimination32ReduceSpeculativeNumberOperationEPNS1_4NodeE, .-_ZN2v88internal8compiler21RedundancyElimination32ReduceSpeculativeNumberOperationEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21RedundancyElimination25TakeChecksFromFirstEffectEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RedundancyElimination25TakeChecksFromFirstEffectEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RedundancyElimination25TakeChecksFromFirstEffectEPNS1_4NodeE, @function
_ZN2v88internal8compiler21RedundancyElimination25TakeChecksFromFirstEffectEPNS1_4NodeE:
.LFB10447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r12), %rcx
	movl	20(%rax), %edx
	movq	32(%r12), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L362
	movq	(%rcx,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L362
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21RedundancyElimination12UpdateChecksEPNS1_4NodeEPKNS2_16EffectPathChecksE
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10447:
	.size	_ZN2v88internal8compiler21RedundancyElimination25TakeChecksFromFirstEffectEPNS1_4NodeE, .-_ZN2v88internal8compiler21RedundancyElimination25TakeChecksFromFirstEffectEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21RedundancyElimination15ReduceOtherNodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RedundancyElimination15ReduceOtherNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RedundancyElimination15ReduceOtherNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler21RedundancyElimination15ReduceOtherNodeEPNS1_4NodeE:
.LFB10446:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	cmpl	$1, 24(%rax)
	je	.L376
.L365:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	cmpb	$1, 36(%rax)
	jne	.L365
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r13), %rsi
	movq	32(%r13), %rdx
	movl	20(%rax), %ecx
	xorl	%eax, %eax
	subq	%rsi, %rdx
	andl	$16777215, %ecx
	sarq	$3, %rdx
	cmpq	%rdx, %rcx
	jnb	.L367
	movq	(%rsi,%rcx,8), %rax
	testq	%rax, %rax
	je	.L367
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21RedundancyElimination12UpdateChecksEPNS1_4NodeEPKNS2_16EffectPathChecksE
.L367:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10446:
	.size	_ZN2v88internal8compiler21RedundancyElimination15ReduceOtherNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler21RedundancyElimination15ReduceOtherNodeEPNS1_4NodeE
	.section	.text.unlikely._ZN2v88internal8compiler21RedundancyElimination15ReduceEffectPhiEPNS1_4NodeE,"ax",@progbits
	.align 2
.LCOLDB2:
	.section	.text._ZN2v88internal8compiler21RedundancyElimination15ReduceEffectPhiEPNS1_4NodeE,"ax",@progbits
.LHOTB2:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RedundancyElimination15ReduceEffectPhiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RedundancyElimination15ReduceEffectPhiEPNS1_4NodeE, @function
_ZN2v88internal8compiler21RedundancyElimination15ReduceEffectPhiEPNS1_4NodeE:
.LFB10442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpw	$1, 16(%rax)
	je	.L413
	movq	0(%r13), %rax
	xorl	%r14d, %r14d
	movl	24(%rax), %ebx
	testl	%ebx, %ebx
	jle	.L386
.L381:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r12), %rcx
	movl	20(%rax), %edx
	movq	32(%r12), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L384
	cmpq	$0, (%rcx,%rdx,8)
	je	.L384
	addl	$1, %r14d
	cmpl	%r14d, %ebx
	jne	.L381
.L386:
	xorl	%esi, %esi
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r12), %rcx
	movl	20(%rax), %edx
	movq	32(%r12), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L383
	movq	(%rcx,%rdx,8), %r15
.L383:
	movq	48(%r12), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$15, %rax
	jbe	.L414
	leaq	16(%r14), %rax
	movq	%rax, 16(%rdi)
.L388:
	movdqu	(%r15), %xmm0
	movl	$1, %r15d
	movups	%xmm0, (%r14)
	cmpl	$1, %ebx
	jle	.L395
	.p2align 4,,10
	.p2align 3
.L389:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r12), %rcx
	movl	20(%rax), %edx
	movq	32(%r12), %rax
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L390
	movq	(%rcx,%rdx,8), %rax
	movq	8(%r14), %rcx
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	cmpq	%rcx, %rdx
	jbe	.L415
	.p2align 4,,10
	.p2align 3
.L391:
	subq	$1, %rdx
	movq	8(%rsi), %rsi
	cmpq	%rcx, %rdx
	jne	.L391
	movq	(%r14), %rax
.L392:
	cmpq	%rax, %rsi
	je	.L397
	.p2align 4,,10
	.p2align 3
.L394:
	subq	$1, %rdx
	movq	%rdx, 8(%r14)
	movq	8(%rax), %rax
	movq	%rax, (%r14)
	movq	8(%rsi), %rsi
	cmpq	%rsi, %rax
	jne	.L394
.L397:
	addl	$1, %r15d
	cmpl	%r15d, %ebx
	jne	.L389
.L395:
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21RedundancyElimination12UpdateChecksEPNS1_4NodeEPKNS2_16EffectPathChecksE
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	xorl	%eax, %eax
.L410:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r12), %rsi
	movq	32(%r12), %rdx
	movl	20(%rax), %ecx
	xorl	%eax, %eax
	subq	%rsi, %rdx
	andl	$16777215, %ecx
	sarq	$3, %rdx
	cmpq	%rdx, %rcx
	jnb	.L410
	movq	(%rsi,%rcx,8), %rax
	testq	%rax, %rax
	je	.L410
	movq	%rax, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21RedundancyElimination12UpdateChecksEPNS1_4NodeEPKNS2_16EffectPathChecksE
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L415:
	movq	(%r14), %rax
	jnb	.L416
	subq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L396:
	movq	8(%rax), %rax
	movq	%rcx, %rdi
	movq	%rcx, 8(%r14)
	subq	$1, %rcx
	movq	%rax, (%r14)
	cmpq	%rdi, %rdx
	jne	.L396
	jmp	.L392
.L414:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L388
.L416:
	movq	%rcx, %rdx
	jmp	.L392
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal8compiler21RedundancyElimination15ReduceEffectPhiEPNS1_4NodeE
	.cfi_startproc
	.type	_ZN2v88internal8compiler21RedundancyElimination15ReduceEffectPhiEPNS1_4NodeE.cold, @function
_ZN2v88internal8compiler21RedundancyElimination15ReduceEffectPhiEPNS1_4NodeE.cold:
.LFSB10442:
.L390:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE10442:
	.section	.text._ZN2v88internal8compiler21RedundancyElimination15ReduceEffectPhiEPNS1_4NodeE
	.size	_ZN2v88internal8compiler21RedundancyElimination15ReduceEffectPhiEPNS1_4NodeE, .-_ZN2v88internal8compiler21RedundancyElimination15ReduceEffectPhiEPNS1_4NodeE
	.section	.text.unlikely._ZN2v88internal8compiler21RedundancyElimination15ReduceEffectPhiEPNS1_4NodeE
	.size	_ZN2v88internal8compiler21RedundancyElimination15ReduceEffectPhiEPNS1_4NodeE.cold, .-_ZN2v88internal8compiler21RedundancyElimination15ReduceEffectPhiEPNS1_4NodeE.cold
.LCOLDE2:
	.section	.text._ZN2v88internal8compiler21RedundancyElimination15ReduceEffectPhiEPNS1_4NodeE
.LHOTE2:
	.section	.text._ZN2v88internal8compiler21RedundancyElimination6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RedundancyElimination6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RedundancyElimination6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler21RedundancyElimination6ReduceEPNS1_4NodeE:
.LFB10429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	24(%rdi), %rcx
	movq	32(%rdi), %rax
	movl	20(%rsi), %edx
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L418
	xorl	%eax, %eax
	cmpq	$0, (%rcx,%rdx,8)
	je	.L418
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	.cfi_restore_state
	movq	0(%r13), %rcx
	movzwl	16(%rcx), %eax
	cmpw	$61, %ax
	je	.L437
	jbe	.L454
	cmpw	$150, %ax
	ja	.L425
	cmpw	$148, %ax
	ja	.L426
	cmpw	$118, %ax
	jbe	.L455
	subl	$122, %eax
	cmpw	$2, %ax
	ja	.L424
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21RedundancyElimination33ReduceSpeculativeNumberComparisonEPNS1_4NodeE
	movq	%rax, %r14
.L420:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	cmpw	$199, %ax
	je	.L426
	jbe	.L456
	cmpw	$219, %ax
	ja	.L432
	cmpw	$217, %ax
	ja	.L428
.L424:
	xorl	%r14d, %r14d
	cmpl	$1, 24(%rcx)
	jne	.L420
	.p2align 4,,10
	.p2align 3
.L457:
	cmpb	$1, 36(%rcx)
	jne	.L420
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	24(%r12), %rsi
	movl	20(%rax), %ecx
	movq	32(%r12), %rax
	subq	%rsi, %rax
	andl	$16777215, %ecx
	sarq	$3, %rax
	cmpq	%rax, %rcx
	jnb	.L420
	movq	(%rsi,%rcx,8), %r14
	testq	%r14, %r14
	je	.L420
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21RedundancyElimination12UpdateChecksEPNS1_4NodeEPKNS2_16EffectPathChecksE
	movq	%rax, %r14
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L432:
	subw	$221, %ax
	cmpw	$11, %ax
	ja	.L424
.L428:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21RedundancyElimination15ReduceCheckNodeEPNS1_4NodeE
	movq	%rax, %r14
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L454:
	testw	%ax, %ax
	je	.L422
	cmpw	$36, %ax
	jne	.L424
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21RedundancyElimination15ReduceEffectPhiEPNS1_4NodeE
	movq	%rax, %r14
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L455:
	cmpw	$88, %ax
	ja	.L428
	xorl	%r14d, %r14d
	cmpl	$1, 24(%rcx)
	jne	.L420
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L456:
	cmpw	$161, %ax
	ja	.L431
	cmpw	$159, %ax
	jbe	.L424
.L426:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21RedundancyElimination32ReduceSpeculativeNumberOperationEPNS1_4NodeE
	movq	%rax, %r14
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L431:
	cmpw	$198, %ax
	jne	.L424
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21RedundancyElimination15ReduceCheckNodeEPNS1_4NodeE
	movq	%rax, %r14
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L422:
	movq	48(%r12), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	$15, %rax
	jbe	.L458
	leaq	16(%rdx), %rax
	movq	%rax, 16(%rdi)
.L434:
	movq	$0, (%rdx)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, 8(%rdx)
	call	_ZN2v88internal8compiler21RedundancyElimination12UpdateChecksEPNS1_4NodeEPKNS2_16EffectPathChecksE
	movq	%rax, %r14
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L437:
	xorl	%r14d, %r14d
	jmp	.L420
.L458:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L434
	.cfi_endproc
.LFE10429:
	.size	_ZN2v88internal8compiler21RedundancyElimination6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler21RedundancyElimination6ReduceEPNS1_4NodeE
	.section	.text.unlikely._ZN2v88internal8compiler21RedundancyElimination11ReduceStartEPNS1_4NodeE,"ax",@progbits
	.align 2
.LCOLDB3:
	.section	.text._ZN2v88internal8compiler21RedundancyElimination11ReduceStartEPNS1_4NodeE,"ax",@progbits
.LHOTB3:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21RedundancyElimination11ReduceStartEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21RedundancyElimination11ReduceStartEPNS1_4NodeE, @function
_ZN2v88internal8compiler21RedundancyElimination11ReduceStartEPNS1_4NodeE:
.LFB10445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	48(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$15, %rax
	jbe	.L479
	leaq	16(%rbx), %rax
	movq	%rax, 16(%rdi)
.L461:
	movq	$0, (%rbx)
	movq	$0, 8(%rbx)
	movq	32(%r12), %rsi
	movq	24(%r12), %rcx
	movl	20(%r13), %r14d
	movq	%rsi, %rax
	subq	%rcx, %rax
	andl	$16777215, %r14d
	sarq	$3, %rax
	cmpq	%rax, %r14
	jnb	.L462
	leaq	(%rcx,%r14,8), %rdx
	movq	(%rdx), %rax
	cmpq	%rbx, %rax
	je	.L469
	testq	%rax, %rax
	je	.L464
	cmpq	$0, 8(%rax)
	jne	.L464
	movq	(%rax), %r13
	testq	%r13, %r13
	jne	.L480
.L463:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L481
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L462:
	leaq	1(%r14), %rdx
	movq	$0, -48(%rbp)
	cmpq	%rdx, %rax
	jb	.L482
	jbe	.L467
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L467
	movq	%rax, 32(%r12)
.L467:
	leaq	(%rcx,%r14,8), %rdx
.L464:
	movq	%rbx, (%rdx)
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L479:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L482:
	leaq	-48(%rbp), %rcx
	subq	%rax, %rdx
	leaq	16(%r12), %rdi
	call	_ZNSt6vectorIPKN2v88internal8compiler21RedundancyElimination16EffectPathChecksENS1_13ZoneAllocatorIS6_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS6_S9_EEmRKS6_
	movq	24(%r12), %rcx
	jmp	.L467
.L481:
	call	__stack_chk_fail@PLT
.L480:
	jmp	.L476
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal8compiler21RedundancyElimination11ReduceStartEPNS1_4NodeE
	.cfi_startproc
	.type	_ZN2v88internal8compiler21RedundancyElimination11ReduceStartEPNS1_4NodeE.cold, @function
_ZN2v88internal8compiler21RedundancyElimination11ReduceStartEPNS1_4NodeE.cold:
.LFSB10445:
.L476:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE10445:
	.section	.text._ZN2v88internal8compiler21RedundancyElimination11ReduceStartEPNS1_4NodeE
	.size	_ZN2v88internal8compiler21RedundancyElimination11ReduceStartEPNS1_4NodeE, .-_ZN2v88internal8compiler21RedundancyElimination11ReduceStartEPNS1_4NodeE
	.section	.text.unlikely._ZN2v88internal8compiler21RedundancyElimination11ReduceStartEPNS1_4NodeE
	.size	_ZN2v88internal8compiler21RedundancyElimination11ReduceStartEPNS1_4NodeE.cold, .-_ZN2v88internal8compiler21RedundancyElimination11ReduceStartEPNS1_4NodeE.cold
.LCOLDE3:
	.section	.text._ZN2v88internal8compiler21RedundancyElimination11ReduceStartEPNS1_4NodeE
.LHOTE3:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler21RedundancyEliminationC2EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler21RedundancyEliminationC2EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler21RedundancyEliminationC2EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE:
.LFB12158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12158:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler21RedundancyEliminationC2EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler21RedundancyEliminationC2EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler21RedundancyEliminationC2EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE
	.weak	_ZTVN2v88internal8compiler21RedundancyEliminationE
	.section	.data.rel.ro._ZTVN2v88internal8compiler21RedundancyEliminationE,"awG",@progbits,_ZTVN2v88internal8compiler21RedundancyEliminationE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler21RedundancyEliminationE, @object
	.size	_ZTVN2v88internal8compiler21RedundancyEliminationE, 56
_ZTVN2v88internal8compiler21RedundancyEliminationE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler21RedundancyEliminationD1Ev
	.quad	_ZN2v88internal8compiler21RedundancyEliminationD0Ev
	.quad	_ZNK2v88internal8compiler21RedundancyElimination12reducer_nameEv
	.quad	_ZN2v88internal8compiler21RedundancyElimination6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
