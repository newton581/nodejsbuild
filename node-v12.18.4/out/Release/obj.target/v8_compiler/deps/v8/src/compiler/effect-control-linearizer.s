	.file	"effect-control-linearizer.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB26673:
	.cfi_startproc
	testq	%rsi, %rsi
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L5:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L5
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE26673:
	.size	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.text._ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE24_M_get_insert_unique_posERS2_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE24_M_get_insert_unique_posERS2_, @function
_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE24_M_get_insert_unique_posERS2_:
.LFB27619:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	24(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L30
	movl	(%rsi), %r8d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L31:
	jne	.L20
	movl	4(%r12), %eax
	cmpl	%eax, 36(%rbx)
	jg	.L19
.L20:
	movq	24(%rbx), %rax
	xorl	%r9d, %r9d
	testq	%rax, %rax
	je	.L18
.L32:
	movq	%rax, %rbx
.L17:
	movl	32(%rbx), %ecx
	cmpl	%r8d, %ecx
	jle	.L31
.L19:
	movq	16(%rbx), %rax
	movl	$1, %r9d
	testq	%rax, %rax
	jne	.L32
.L18:
	movq	%rbx, %rdx
	testb	%r9b, %r9b
	jne	.L16
	cmpl	%r8d, %ecx
	jge	.L33
.L25:
	xorl	%eax, %eax
.L24:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	leaq	16(%rdi), %rbx
.L16:
	xorl	%eax, %eax
	movq	%rbx, %rdx
	cmpq	32(%rdi), %rbx
	je	.L24
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%r12), %r8d
	movq	%rbx, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %rbx
	cmpl	%r8d, %ecx
	jl	.L25
.L33:
	jne	.L26
	movl	4(%r12), %eax
	cmpl	%eax, 36(%rbx)
	jl	.L25
.L26:
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27619:
	.size	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE24_M_get_insert_unique_posERS2_, .-_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE24_M_get_insert_unique_posERS2_
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_116RemoveRenameNodeEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_116RemoveRenameNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_116RemoveRenameNodeEPNS1_4NodeE:
.LFB23243:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %r13
	testq	%r13, %r13
	je	.L39
	movq	0(%r13), %rbx
	.p2align 4,,10
	.p2align 3
.L49:
	movl	16(%r13), %ecx
	movq	%r13, %rdi
	movl	%ecx, %edx
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rax
	leaq	0(%r13,%rax,8), %rax
	leaq	32(%rax), %rsi
	addq	$16, %rax
	andl	$1, %ecx
	cmovne	%rsi, %rax
	leaq	(%rax,%rdx,8), %r14
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	jne	.L74
	movzbl	23(%r12), %eax
	movq	32(%r12), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L47
	movq	16(%r15), %r15
.L47:
	movq	(%r14), %rdi
	cmpq	%r15, %rdi
	je	.L44
.L73:
	testq	%rdi, %rdi
	je	.L48
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L48:
	movq	%r15, (%r14)
	testq	%r15, %r15
	je	.L44
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L44:
	testq	%rbx, %rbx
	je	.L39
	movq	%rbx, %r13
	movq	(%rbx), %rbx
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L39:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node4KillEv@PLT
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	(%r14), %rdi
	movq	%rax, %r15
	cmpq	%rdi, %rax
	jne	.L73
	jmp	.L44
	.cfi_endproc
.LFE23243:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_116RemoveRenameNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_116RemoveRenameNodeEPNS1_4NodeE
	.section	.text._ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0, @function
_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0:
.LFB29009:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$63, %rax
	jbe	.L110
	leaq	64(%rbx), %rax
	movq	%rax, 16(%rdi)
.L77:
	movq	(%rdx), %rax
	pxor	%xmm0, %xmm0
	leaq	16(%r13), %r15
	leaq	32(%rbx), %rsi
	movq	$0, 56(%rbx)
	movq	%r12, %r14
	movq	(%rax), %rax
	movups	%xmm0, 40(%rbx)
	movq	%rax, 32(%rbx)
	cmpq	%r12, %r15
	je	.L111
	movl	32(%rbx), %edx
	movl	32(%r12), %ecx
	cmpl	%ecx, %edx
	jge	.L112
.L82:
	movl	%edx, -56(%rbp)
	movq	%rsi, -64(%rbp)
	cmpq	%r12, 32(%r13)
	je	.L96
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	-56(%rbp), %edx
	cmpl	32(%rax), %edx
	jle	.L113
.L87:
	cmpq	$0, 24(%rax)
	jne	.L96
	movq	%rax, %r14
.L80:
	xorl	%eax, %eax
.L118:
	cmpq	%r14, %r15
	jne	.L114
	.p2align 4,,10
	.p2align 3
.L100:
	movl	$1, %edi
.L93:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
	addq	$40, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	jne	.L83
	movl	36(%r12), %eax
	cmpl	%eax, 36(%rbx)
	jl	.L82
.L83:
	cmpl	%ecx, %edx
	jle	.L115
.L84:
	movl	%edx, -64(%rbp)
	movl	%ecx, -56(%rbp)
	movq	%rsi, -72(%rbp)
	cmpq	%r12, 40(%r13)
	je	.L99
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movl	-64(%rbp), %edx
	movl	-56(%rbp), %ecx
	cmpl	32(%rax), %edx
	jl	.L90
	movq	-72(%rbp), %rsi
	je	.L116
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE24_M_get_insert_unique_posERS2_
	movq	%rax, %r14
	testq	%rdx, %rdx
	jne	.L117
.L89:
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	cmpq	$0, 48(%r13)
	je	.L91
	movq	40(%r13), %r14
	movl	32(%rbx), %eax
	cmpl	%eax, 32(%r14)
	jl	.L80
	jne	.L91
	movl	36(%rbx), %eax
	cmpl	%eax, 36(%r14)
	jge	.L91
	xorl	%eax, %eax
	jmp	.L118
.L117:
	movq	%rax, %r12
	movq	%rdx, %r14
	.p2align 4,,10
	.p2align 3
.L96:
	testq	%r12, %r12
	setne	%al
	cmpq	%r14, %r15
	je	.L100
.L114:
	testb	%al, %al
	jne	.L100
	movl	32(%rbx), %edx
	movl	32(%r14), %ecx
	movl	$1, %edi
	cmpl	%edx, %ecx
	jg	.L93
.L92:
	xorl	%edi, %edi
	cmpl	%edx, %ecx
	jne	.L93
	xorl	%edi, %edi
	movl	36(%r14), %eax
	cmpl	%eax, 36(%rbx)
	setl	%dil
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L113:
	movq	-64(%rbp), %rsi
	jne	.L91
	movl	36(%rbx), %ecx
	cmpl	%ecx, 36(%rax)
	jl	.L87
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L115:
	movl	36(%rbx), %eax
	cmpl	%eax, 36(%r12)
	jge	.L89
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$64, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L116:
	movl	36(%rax), %edi
	cmpl	%edi, 36(%rbx)
	jge	.L91
.L90:
	cmpq	$0, 24(%r12)
	je	.L92
	movq	%rax, %r14
	movl	$1, %edi
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L99:
	xorl	%r12d, %r12d
	jmp	.L96
	.cfi_endproc
.LFE29009:
	.size	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0, .-_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0
	.section	.text._ZNSt3mapISt4pairIiiEN2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataESt4lessIS1_ENS3_13ZoneAllocatorIS0_IKS1_S6_EEEEixEOS1_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt3mapISt4pairIiiEN2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataESt4lessIS1_ENS3_13ZoneAllocatorIS0_IKS1_S6_EEEEixEOS1_, @function
_ZNSt3mapISt4pairIiiEN2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataESt4lessIS1_ENS3_13ZoneAllocatorIS0_IKS1_S6_EEEEixEOS1_:
.LFB25492:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	leaq	16(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	24(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L128
	movl	(%rsi), %eax
	movq	%r8, %rsi
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L130:
	jne	.L124
	movl	4(%rcx), %r9d
	cmpl	%r9d, 36(%rdx)
	jl	.L123
.L124:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L122
.L121:
	cmpl	%eax, 32(%rdx)
	jge	.L130
.L123:
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L121
.L122:
	cmpq	%rsi, %r8
	je	.L120
	cmpl	32(%rsi), %eax
	jge	.L131
.L120:
	leaq	-16(%rbp), %rdx
	movq	%rcx, -16(%rbp)
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0
	movq	%rax, %rsi
.L126:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	leaq	40(%rsi), %rax
	jne	.L132
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	jne	.L126
	movl	36(%rsi), %eax
	cmpl	%eax, 4(%rcx)
	jge	.L126
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r8, %rsi
	jmp	.L120
.L132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25492:
	.size	_ZNSt3mapISt4pairIiiEN2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataESt4lessIS1_ENS3_13ZoneAllocatorIS0_IKS1_S6_EEEEixEOS1_, .-_ZNSt3mapISt4pairIiiEN2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataESt4lessIS1_ENS3_13ZoneAllocatorIS0_IKS1_S6_EEEEixEOS1_
	.section	.rodata._ZNSt6vectorIN2v88internal8compiler12_GLOBAL__N_116PendingEffectPhiENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal8compiler12_GLOBAL__N_116PendingEffectPhiENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal8compiler12_GLOBAL__N_116PendingEffectPhiENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIN2v88internal8compiler12_GLOBAL__N_116PendingEffectPhiENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB27425:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %r12
	movq	%r14, %rax
	subq	%r12, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L149
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r12, %rdx
	testq	%rax, %rax
	je	.L143
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L150
	movl	$2147483632, %esi
	movl	$2147483632, %r8d
.L135:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L151
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L138:
	addq	%rax, %r8
	leaq	16(%rax), %rsi
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L150:
	testq	%rcx, %rcx
	jne	.L152
	movl	$16, %esi
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L136:
	movdqa	(%r15), %xmm3
	movups	%xmm3, (%rax,%rdx)
	cmpq	%r12, %rbx
	je	.L139
	leaq	-16(%rbx), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	subq	%r12, %rdi
	movq	%rdi, %rsi
	shrq	$4, %rsi
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L140:
	movdqu	(%r12,%rdx), %xmm1
	addq	$1, %rcx
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rsi
	ja	.L140
	leaq	32(%rax,%rdi), %rsi
.L139:
	cmpq	%r14, %rbx
	je	.L141
	subq	%rbx, %r14
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	-16(%r14), %r9
	movq	%r9, %rdi
	shrq	$4, %rdi
	addq	$1, %rdi
	.p2align 4,,10
	.p2align 3
.L142:
	movdqu	(%rbx,%rdx), %xmm2
	addq	$1, %rcx
	movups	%xmm2, (%rsi,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rcx
	jb	.L142
	leaq	16(%rsi,%r9), %rsi
.L141:
	movq	%rax, %xmm0
	movq	%rsi, %xmm4
	movq	%r8, 24(%r13)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movl	$16, %esi
	movl	$16, %r8d
	jmp	.L135
.L151:
	movq	%rdx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rdx
	jmp	.L138
.L152:
	cmpq	$134217727, %rcx
	movl	$134217727, %r8d
	cmovbe	%rcx, %r8
	salq	$4, %r8
	movq	%r8, %rsi
	jmp	.L135
.L149:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE27425:
	.size	_ZNSt6vectorIN2v88internal8compiler12_GLOBAL__N_116PendingEffectPhiENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIN2v88internal8compiler12_GLOBAL__N_116PendingEffectPhiENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_118UpdateBlockControlEPNS1_10BasicBlockEPNS2_21BlockEffectControlMapE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_118UpdateBlockControlEPNS1_10BasicBlockEPNS2_21BlockEffectControlMapE, @function
_ZN2v88internal8compiler12_GLOBAL__N_118UpdateBlockControlEPNS1_10BasicBlockEPNS2_21BlockEffectControlMapE:
.LFB23241:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	72(%rdi), %rax
	movq	(%rax), %r14
	movq	(%r14), %rax
	cmpw	$22, 16(%rax)
	je	.L153
	movslq	28(%rax), %rcx
	movq	144(%rdi), %rax
	movq	%rdi, %r12
	subq	136(%rdi), %rax
	sarq	$3, %rax
	cmpq	%rax, %rcx
	jne	.L153
	testl	%ecx, %ecx
	jle	.L153
	leaq	16(%rsi), %rax
	movq	%rsi, %rbx
	xorl	%r15d, %r15d
	movq	%rax, -88(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L165:
	movl	%r15d, %esi
	movq	%r14, %rdi
	movl	%r15d, %r13d
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	4(%r12), %edi
	movq	24(%rbx), %rdx
	movq	%rax, %rcx
	movq	136(%r12), %rax
	movq	-88(%rbp), %rsi
	movq	(%rax,%r15,8), %rax
	movl	4(%rax), %eax
	movl	%edi, -60(%rbp)
	movl	%eax, -64(%rbp)
	testq	%rdx, %rdx
	jne	.L157
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L169:
	jne	.L160
	cmpl	36(%rdx), %edi
	jg	.L159
.L160:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L158
.L157:
	cmpl	32(%rdx), %eax
	jle	.L169
.L159:
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L157
.L158:
	cmpq	-88(%rbp), %rsi
	je	.L156
	cmpl	32(%rsi), %eax
	jl	.L156
	jne	.L162
	cmpl	36(%rsi), %edi
	jge	.L162
	.p2align 4,,10
	.p2align 3
.L156:
	movq	-104(%rbp), %rax
	leaq	-72(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0
	movq	-96(%rbp), %rcx
	movq	%rax, %rsi
.L162:
	movq	48(%rsi), %rsi
	cmpq	%rcx, %rsi
	je	.L163
	movl	%r13d, %edx
	movq	%r14, %rdi
	addq	$1, %r15
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	movq	(%r14), %rax
	cmpl	%r15d, 28(%rax)
	jg	.L165
	.p2align 4,,10
	.p2align 3
.L153:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L170
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movq	(%r14), %rax
	addq	$1, %r15
	cmpl	%r15d, 28(%rax)
	jg	.L165
	jmp	.L153
.L170:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23241:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_118UpdateBlockControlEPNS1_10BasicBlockEPNS2_21BlockEffectControlMapE, .-_ZN2v88internal8compiler12_GLOBAL__N_118UpdateBlockControlEPNS1_10BasicBlockEPNS2_21BlockEffectControlMapE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_115UpdateEffectPhiEPNS1_4NodeEPNS1_10BasicBlockEPNS2_21BlockEffectControlMapEPNS1_7JSGraphE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_115UpdateEffectPhiEPNS1_4NodeEPNS1_10BasicBlockEPNS2_21BlockEffectControlMapEPNS1_7JSGraphE.isra.0, @function
_ZN2v88internal8compiler12_GLOBAL__N_115UpdateEffectPhiEPNS1_4NodeEPNS1_10BasicBlockEPNS2_21BlockEffectControlMapEPNS1_7JSGraphE.isra.0:
.LFB29010:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jle	.L171
	leaq	16(%rdx), %rax
	leaq	32(%rdi), %r12
	movq	%rdi, %r14
	movq	%rsi, %r13
	movq	%rax, -88(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rdx, %r15
	xorl	%ebx, %ebx
	movq	%r12, -120(%rbp)
	movq	%rax, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L188:
	movzbl	23(%r14), %eax
	leaq	0(,%rbx,8), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L173
	movq	%r12, %rax
.L174:
	movq	(%rax), %r11
	movq	136(%r13), %rax
	movl	4(%r13), %edi
	movq	24(%r15), %rdx
	movq	(%rax,%r8), %rax
	movq	-88(%rbp), %rsi
	movl	4(%rax), %eax
	movl	%edi, -60(%rbp)
	movl	%eax, -64(%rbp)
	testq	%rdx, %rdx
	jne	.L176
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L203:
	jne	.L179
	cmpl	36(%rdx), %edi
	jg	.L178
.L179:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L177
.L176:
	cmpl	32(%rdx), %eax
	jle	.L203
.L178:
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L176
.L177:
	cmpq	%rsi, -88(%rbp)
	je	.L175
	cmpl	32(%rsi), %eax
	jl	.L175
	jne	.L181
	cmpl	36(%rsi), %edi
	jge	.L181
	.p2align 4,,10
	.p2align 3
.L175:
	movq	-112(%rbp), %rax
	leaq	-72(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r8, -104(%rbp)
	movq	%r11, -96(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %r11
	movq	%rax, %rsi
.L181:
	movq	40(%rsi), %r9
	cmpq	%r9, %r11
	je	.L202
	movzbl	23(%r14), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L184
	movq	(%r12), %rdi
	movq	%r12, %r8
	movq	%r14, %rsi
	cmpq	%rdi, %r9
	je	.L202
.L185:
	leaq	1(%rbx), %rdx
	leaq	0(,%rdx,4), %r11
	movq	%rdx, %rax
	subq	%r11, %rax
	leaq	(%rsi,%rax,8), %rbx
	testq	%rdi, %rdi
	je	.L187
	movq	%rbx, %rsi
	movq	%rdx, -128(%rbp)
	movq	%r8, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-128(%rbp), %rdx
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %r9
.L187:
	movq	%r9, (%r8)
	testq	%r9, %r9
	je	.L183
	movq	%rbx, %rsi
	movq	%r9, %rdi
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %rdx
.L183:
	movq	(%r14), %rax
	movq	%rdx, %rbx
	addq	$8, %r12
	cmpl	%edx, 24(%rax)
	jg	.L188
.L171:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L204
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	leaq	1(%rbx), %rdx
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L173:
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	leaq	16(%rax,%r8), %rax
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L184:
	movq	-120(%rbp), %rax
	leaq	1(%rbx), %rdx
	movq	(%rax), %rsi
	leaq	16(%rsi,%r8), %r8
	movq	(%r8), %rdi
	cmpq	%rdi, %r9
	jne	.L185
	jmp	.L183
.L204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29010:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_115UpdateEffectPhiEPNS1_4NodeEPNS1_10BasicBlockEPNS2_21BlockEffectControlMapEPNS1_7JSGraphE.isra.0, .-_ZN2v88internal8compiler12_GLOBAL__N_115UpdateEffectPhiEPNS1_4NodeEPNS1_10BasicBlockEPNS2_21BlockEffectControlMapEPNS1_7JSGraphE.isra.0
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer33LowerChangeFloat64ToTaggedPointerEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer33LowerChangeFloat64ToTaggedPointerEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer33LowerChangeFloat64ToTaggedPointerEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer33LowerChangeFloat64ToTaggedPointerEPNS1_4NodeE:
.LFB23261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L206
	movq	16(%rbx), %rbx
.L206:
	leaq	48(%rdi), %r12
	movl	$16, %esi
	leaq	-112(%rbp), %r14
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L209
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L209:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23261:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer33LowerChangeFloat64ToTaggedPointerEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer33LowerChangeFloat64ToTaggedPointerEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler23EffectControlLinearizer34LowerChangeInt31ToCompressedSignedEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"machine()->Is64() && SmiValuesAre31Bits()"
	.section	.rodata._ZN2v88internal8compiler23EffectControlLinearizer34LowerChangeInt31ToCompressedSignedEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer34LowerChangeInt31ToCompressedSignedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer34LowerChangeInt31ToCompressedSignedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer34LowerChangeInt31ToCompressedSignedEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer34LowerChangeInt31ToCompressedSignedEPNS1_4NodeE:
.LFB23263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23263:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer34LowerChangeInt31ToCompressedSignedEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer34LowerChangeInt31ToCompressedSignedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeInt31ToTaggedSignedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeInt31ToTaggedSignedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeInt31ToTaggedSignedEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeInt31ToTaggedSignedEPNS1_4NodeE:
.LFB23264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L213
	movq	16(%r12), %r12
.L213:
	movq	(%rdi), %rax
	leaq	48(%rdi), %r13
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L216
.L214:
	movq	%r13, %rdi
	movl	$32, %esi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	%rax, %r12
	jmp	.L214
	.cfi_endproc
.LFE23264:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeInt31ToTaggedSignedEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeInt31ToTaggedSignedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeInt32ToTaggedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeInt32ToTaggedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeInt32ToTaggedEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeInt32ToTaggedEPNS1_4NodeE:
.LFB29018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L218
	movq	16(%r12), %r12
.L218:
	movq	(%rdi), %rax
	leaq	48(%rdi), %r13
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L221
.L219:
	movq	%r13, %rdi
	movl	$32, %esi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	%rax, %r12
	jmp	.L219
	.cfi_endproc
.LFE29018:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeInt32ToTaggedEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeInt32ToTaggedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeTaggedSignedToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeTaggedSignedToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeTaggedSignedToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeTaggedSignedToInt32EPNS1_4NodeE:
.LFB23269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L223
	movq	16(%r13), %r13
.L223:
	movq	(%rdi), %rax
	leaq	48(%rdi), %r12
	movl	$32, %esi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L226
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	.cfi_endproc
.LFE23269:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeTaggedSignedToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeTaggedSignedToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer34LowerChangeCompressedSignedToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer34LowerChangeCompressedSignedToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer34LowerChangeCompressedSignedToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer34LowerChangeCompressedSignedToInt32EPNS1_4NodeE:
.LFB29012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE29012:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer34LowerChangeCompressedSignedToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer34LowerChangeCompressedSignedToInt32EPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeTaggedSignedToInt64EPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"machine()->Is64()"
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeTaggedSignedToInt64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeTaggedSignedToInt64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeTaggedSignedToInt64EPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeTaggedSignedToInt64EPNS1_4NodeE:
.LFB23271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L230
	movq	16(%r13), %r13
.L230:
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	jne	.L233
	leaq	48(%rdi), %r12
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23271:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeTaggedSignedToInt64EPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeTaggedSignedToInt64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer22LowerChangeTaggedToBitEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer22LowerChangeTaggedToBitEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer22LowerChangeTaggedToBitEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer22LowerChangeTaggedToBitEPNS1_4NodeE:
.LFB23272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L235
	movq	16(%r13), %r13
.L235:
	leaq	48(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12TrueConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE23272:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer22LowerChangeTaggedToBitEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer22LowerChangeTaggedToBitEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer16LowerPoisonIndexEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer16LowerPoisonIndexEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer16LowerPoisonIndexEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer16LowerPoisonIndexEPNS1_4NodeE:
.LFB23284:
	.cfi_startproc
	endbr64
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L238
	movq	16(%r8), %r8
.L238:
	cmpl	$1, 24(%rdi)
	je	.L240
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	addq	$48, %rdi
	movq	%r8, %rsi
	jmp	_ZN2v88internal8compiler14GraphAssembler25Word32PoisonOnSpeculationEPNS1_4NodeE@PLT
	.cfi_endproc
.LFE23284:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer16LowerPoisonIndexEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer16LowerPoisonIndexEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer18LowerCheckReceiverEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer18LowerCheckReceiverEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer18LowerCheckReceiverEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer18LowerCheckReceiverEPNS1_4NodeES4_:
.LFB23288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L242
	movq	16(%r14), %r14
.L242:
	leaq	-112(%rbp), %r13
	leaq	48(%rdi), %r12
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$1024, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler21Uint32LessThanOrEqualEPNS1_4NodeES4_@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$22, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r12, %rdi
	movq	$0, -112(%rbp)
	movl	$-1, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L245
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L245:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23288:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer18LowerCheckReceiverEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer18LowerCheckReceiverEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer35LowerCheckReceiverOrNullOrUndefinedEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer35LowerCheckReceiverOrNullOrUndefinedEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer35LowerCheckReceiverOrNullOrUndefinedEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer35LowerCheckReceiverOrNullOrUndefinedEPNS1_4NodeES4_:
.LFB23289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	32(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L247
	movq	16(%r15), %r15
.L247:
	leaq	-112(%rbp), %r13
	leaq	48(%rdi), %r12
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$67, %esi
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi@PLT
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler21Uint32LessThanOrEqualEPNS1_4NodeES4_@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$23, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r12, %rdi
	movq	$0, -112(%rbp)
	movl	$-1, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler18BooleanMapConstantEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$23, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r12, %rdi
	movq	$0, -112(%rbp)
	movl	$-1, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L250
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L250:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23289:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer35LowerCheckReceiverOrNullOrUndefinedEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer35LowerCheckReceiverOrNullOrUndefinedEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckSymbolEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckSymbolEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckSymbolEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckSymbolEPNS1_4NodeES4_:
.LFB23290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L252
	movq	16(%r14), %r14
.L252:
	leaq	-112(%rbp), %r13
	leaq	48(%rbx), %r12
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	addq	$176, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-120(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	%r15, %r8
	movq	%r13, %rdx
	movl	$27, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r12, %rdi
	movq	$0, -112(%rbp)
	movl	$-1, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L255
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L255:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23290:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckSymbolEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckSymbolEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckStringEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckStringEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckStringEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckStringEPNS1_4NodeES4_:
.LFB23291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	32(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L257
	movq	16(%r15), %r15
.L257:
	movq	(%rsi), %rdi
	leaq	-112(%rbp), %r13
	addq	$48, %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	-120(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32LessThanEPNS1_4NodeES4_@PLT
	movq	%rbx, %r8
	movq	%r14, %rdx
	movl	$26, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L260
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L260:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23291:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckStringEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckStringEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer28LowerCheckInternalizedStringEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer28LowerCheckInternalizedStringEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer28LowerCheckInternalizedStringEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer28LowerCheckInternalizedStringEPNS1_4NodeES4_:
.LFB23292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L262
	movq	16(%r14), %r14
.L262:
	leaq	-112(%rbp), %r13
	leaq	48(%rdi), %r12
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$-32, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-120(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$36, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r12, %rdi
	movq	$0, -112(%rbp)
	movl	$-1, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L265
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L265:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23292:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer28LowerCheckInternalizedStringEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer28LowerCheckInternalizedStringEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer12LowerCheckIfEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer12LowerCheckIfEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer12LowerCheckIfEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer12LowerCheckIfEPNS1_4NodeES4_:
.LFB23293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L267
	movq	16(%r13), %r13
.L267:
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler19CheckIfParametersOfEPKNS1_8OperatorE@PLT
	leaq	48(%rbx), %rdi
	movq	%r12, %r8
	movq	%r13, %rcx
	movzbl	(%rax), %esi
	addq	$8, %rsp
	leaq	8(%rax), %rdx
	movl	$1, %r9d
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	.cfi_endproc
.LFE23293:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer12LowerCheckIfEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer12LowerCheckIfEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer17LowerStringConcatEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer17LowerStringConcatEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer17LowerStringConcatEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer17LowerStringConcatEPNS1_4NodeE:
.LFB23294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L270
	movq	40(%rsi), %r12
	addq	$48, %rsi
.L271:
	movq	(%rbx), %rax
	movq	(%rsi), %r13
	xorl	%edx, %edx
	leaq	-128(%rbp), %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal11CodeFactory9StringAddEPNS0_7IsolateENS0_14StringAddFlagsE@PLT
	movq	-112(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$112, %r8d
	leaq	-144(%rbp), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -136(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -144(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	leaq	48(%rbx), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	(%rbx), %rdi
	movq	-128(%rbp), %rsi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%rax, -160(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %xmm1
	movdqu	72(%rbx), %xmm2
	xorl	%r8d, %r8d
	movq	-160(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-96(%rbp), %rcx
	movaps	%xmm2, -64(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r13, %xmm0
	movhps	-152(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movl	24(%rax), %edx
	movl	28(%rax), %eax
	leal	4(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L274
	subq	$-128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
	movq	32(%rsi), %rsi
	movq	24(%rsi), %r12
	addq	$32, %rsi
	jmp	.L271
.L274:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23294:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer17LowerStringConcatEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer17LowerStringConcatEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32AddEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32AddEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32AddEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32AddEPNS1_4NodeES4_:
.LFB23301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	-8(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L277
	leaq	16(%r8), %rsi
	movq	16(%r8), %r8
	addq	$8, %rsi
.L277:
	leaq	48(%rdi), %r12
	movq	(%rsi), %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20Int32AddWithOverflowEPNS1_4NodeES4_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%rbx, %r8
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$1, %r9d
	movl	$29, %esi
	movq	$0, -64(%rbp)
	movl	$-1, -56(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L280
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L280:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23301:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32AddEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32AddEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32SubEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32SubEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32SubEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32SubEPNS1_4NodeES4_:
.LFB23302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	-8(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L283
	leaq	16(%r8), %rsi
	movq	16(%r8), %r8
	addq	$8, %rsi
.L283:
	leaq	48(%rdi), %r12
	movq	(%rsi), %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20Int32SubWithOverflowEPNS1_4NodeES4_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%rbx, %r8
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$1, %r9d
	movl	$29, %esi
	movq	$0, -64(%rbp)
	movl	$-1, -56(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L286
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L286:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23302:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32SubEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32SubEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckedUint32DivEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckedUint32DivEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckedUint32DivEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckedUint32DivEPNS1_4NodeES4_:
.LFB23306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -88(%rbp)
	movq	-8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L289
	leaq	16(%r13), %rsi
	movq	16(%r13), %r13
	addq	$8, %rsi
.L289:
	leaq	48(%rdi), %r12
	movq	(%rsi), %rbx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%rax, %r14
	movq	(%rbx), %rax
	cmpw	$23, 16(%rax)
	je	.L315
.L290:
	movq	%r14, %rdx
	movq	%rbx, %rsi
	leaq	-80(%rbp), %r15
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	-88(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$1, %r9d
	movl	$5, %esi
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9Uint32DivEPNS1_4NodeES4_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler8Int32MulEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	-88(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$1, %r9d
	movl	$16, %esi
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
.L287:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L316
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	movl	44(%rax), %r15d
	testl	%r15d, %r15d
	je	.L290
	leal	-1(%r15), %esi
	testl	%r15d, %esi
	jne	.L290
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi@PLT
	movq	%rax, %rbx
	cmpl	$65535, %r15d
	jbe	.L303
	movl	$31, -112(%rbp)
	shrl	$16, %r15d
	movl	$23, %r10d
	movl	$22, %edx
	movl	$30, -116(%rbp)
	movl	$21, %r9d
	movl	$19, %ecx
	movl	$18, %edi
	movl	$29, -108(%rbp)
	movl	$17, %eax
	movl	$20, %r8d
	movl	$24, %r11d
	movl	$27, -92(%rbp)
	movl	$16, %esi
	movl	$26, -104(%rbp)
	movl	$25, -96(%rbp)
	movl	$28, -100(%rbp)
.L291:
	cmpl	$255, %r15d
	jbe	.L292
	movl	-112(%rbp), %r10d
	movl	-116(%rbp), %edx
	shrl	$8, %r15d
	movl	%r11d, %esi
	movl	-108(%rbp), %r9d
	movl	-92(%rbp), %ecx
	movl	-104(%rbp), %edi
	movl	-96(%rbp), %eax
	movl	-100(%rbp), %r8d
.L292:
	cmpl	$15, %r15d
	jbe	.L293
	shrl	$4, %r15d
	movl	%r10d, %ecx
	movl	%edx, %edi
	movl	%r9d, %eax
	movl	%r8d, %esi
.L293:
	cmpl	$4, %r15d
	je	.L304
	ja	.L295
	cmpl	$1, %r15d
	je	.L296
	cmpl	$2, %r15d
	jne	.L298
.L297:
	movl	%eax, %esi
.L296:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	-88(%rbp), %r8
	leaq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$1, %r9d
	movl	$16, %esi
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9Word32ShrEPNS1_4NodeES4_@PLT
	movq	%rax, %r14
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L295:
	cmpl	$8, %r15d
	jne	.L298
	movl	%ecx, %eax
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L303:
	movl	$15, -112(%rbp)
	movl	$7, %r10d
	xorl	%esi, %esi
	movl	$6, %edx
	movl	$14, -116(%rbp)
	movl	$5, %r9d
	movl	$3, %ecx
	movl	$2, %edi
	movl	$13, -108(%rbp)
	movl	$1, %eax
	movl	$4, %r8d
	movl	$8, %r11d
	movl	$11, -92(%rbp)
	movl	$10, -104(%rbp)
	movl	$9, -96(%rbp)
	movl	$12, -100(%rbp)
	jmp	.L291
.L298:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L304:
	movl	%edi, %ecx
	movl	%ecx, %eax
	jmp	.L297
.L316:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23306:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckedUint32DivEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckedUint32DivEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer35LowerCheckedInt32ToCompressedSignedEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer35LowerCheckedInt32ToCompressedSignedEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer35LowerCheckedInt32ToCompressedSignedEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer35LowerCheckedInt32ToCompressedSignedEPNS1_4NodeES4_:
.LFB23309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	%rdx, %rbx
	movq	32(%rsi), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L318
	movq	16(%r13), %r13
.L318:
	movq	(%rsi), %rdi
	addq	$48, %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler20Int32AddWithOverflowEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rbx, %r8
	movl	$16, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	popq	%rbx
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler10ProjectionEiPNS1_4NodeE@PLT
	.cfi_endproc
.LFE23309:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer35LowerCheckedInt32ToCompressedSignedEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer35LowerCheckedInt32ToCompressedSignedEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedInt32ToTaggedSignedEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedInt32ToTaggedSignedEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedInt32ToTaggedSignedEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedInt32ToTaggedSignedEPNS1_4NodeES4_:
.LFB23310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L321
	movq	16(%r13), %r13
.L321:
	movq	(%rsi), %rdi
	leaq	48(%rbx), %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler20Int32AddWithOverflowEPNS1_4NodeES4_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r15, %r8
	movq	%r14, %rdx
	movl	$16, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	(%rbx), %rdx
	movq	16(%rdx), %rdx
	cmpb	$5, 16(%rdx)
	je	.L324
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	.cfi_endproc
.LFE23310:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedInt32ToTaggedSignedEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedInt32ToTaggedSignedEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedInt64ToInt32EPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedInt64ToInt32EPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedInt64ToInt32EPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedInt64ToInt32EPNS1_4NodeES4_:
.LFB23311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L326
	movq	16(%r15), %r15
.L326:
	movq	(%rsi), %rdi
	addq	$48, %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word64EqualEPNS1_4NodeES4_@PLT
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$1, %r9d
	movl	$16, %esi
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23311:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedInt64ToInt32EPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedInt64ToInt32EPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedInt64ToTaggedSignedEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedInt64ToTaggedSignedEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedInt64ToTaggedSignedEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedInt64ToTaggedSignedEPNS1_4NodeES4_:
.LFB23312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	%rdx, %rbx
	movq	32(%rsi), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L329
	movq	16(%r13), %r13
.L329:
	movq	(%rsi), %rdi
	addq	$48, %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word64EqualEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%rbx, %r8
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$1, %r9d
	movl	$16, %esi
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r12, %rdi
	movl	$32, %esi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	popq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE23312:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedInt64ToTaggedSignedEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedInt64ToTaggedSignedEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedUint32ToInt32EPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedUint32ToInt32EPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedUint32ToInt32EPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedUint32ToInt32EPNS1_4NodeES4_:
.LFB23314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	%rdx, %rbx
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L332
	movq	16(%r14), %r14
.L332:
	movq	(%rsi), %rdi
	addq	$48, %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$1, %r9d
	movl	$16, %esi
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23314:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedUint32ToInt32EPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedUint32ToInt32EPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedUint32ToTaggedSignedEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedUint32ToTaggedSignedEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedUint32ToTaggedSignedEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedUint32ToTaggedSignedEPNS1_4NodeES4_:
.LFB23315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L335
	movq	16(%r13), %r13
.L335:
	movq	(%rsi), %rdi
	leaq	48(%rbx), %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movl	$2147483647, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler21Uint32LessThanOrEqualEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r15, %r8
	movl	$16, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%rax, %rdx
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L338
.L336:
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeUint32ToUint64EPNS1_4NodeE@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L336
	.cfi_endproc
.LFE23315:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedUint32ToTaggedSignedEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedUint32ToTaggedSignedEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedUint64BoundsEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedUint64BoundsEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedUint64BoundsEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedUint64BoundsEPNS1_4NodeES4_:
.LFB23316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%rax, %r14
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L340
	movq	32(%rbx), %r15
	addq	$40, %rbx
.L341:
	movq	(%rbx), %rdx
	addq	$48, %r12
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14Uint64LessThanEPNS1_4NodeES4_@PLT
	movq	%r13, %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	xorl	%r9d, %r9d
	movl	$28, %esi
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	movq	32(%rbx), %rbx
	movq	16(%rbx), %r15
	addq	$24, %rbx
	jmp	.L341
	.cfi_endproc
.LFE23316:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedUint64BoundsEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedUint64BoundsEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedUint64ToInt32EPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedUint64ToInt32EPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedUint64ToInt32EPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedUint64ToInt32EPNS1_4NodeES4_:
.LFB23317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	%rdx, %rbx
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L344
	movq	16(%r14), %r14
.L344:
	movq	(%rsi), %rdi
	addq	$48, %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rdi
	movl	$2147483647, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler21Uint64LessThanOrEqualEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$16, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	popq	%rbx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	.cfi_endproc
.LFE23317:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedUint64ToInt32EPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedUint64ToInt32EPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedUint64ToTaggedSignedEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedUint64ToTaggedSignedEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedUint64ToTaggedSignedEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedUint64ToTaggedSignedEPNS1_4NodeES4_:
.LFB23318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movzbl	23(%rsi), %eax
	movq	%rdx, %rbx
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L347
	movq	16(%r14), %r14
.L347:
	movq	(%rsi), %rdi
	addq	$48, %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rdi
	movl	$2147483647, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler21Uint64LessThanOrEqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%rbx, %r8
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$1, %r9d
	movl	$16, %esi
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r12, %rdi
	movl	$32, %esi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	popq	%rbx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE23318:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedUint64ToTaggedSignedEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedUint64ToTaggedSignedEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedTaggedSignedToInt32EPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedTaggedSignedToInt32EPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedTaggedSignedToInt32EPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedTaggedSignedToInt32EPNS1_4NodeES4_:
.LFB23323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L350
	movq	16(%r14), %r14
.L350:
	movq	(%rsi), %rdi
	leaq	48(%rbx), %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	-56(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movl	$25, %esi
	movq	%r12, %rdi
	movq	%r15, %r8
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	(%rbx), %rax
	movl	$32, %esi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L353
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	addq	$24, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	.cfi_endproc
.LFE23323:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedTaggedSignedToInt32EPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedTaggedSignedToInt32EPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedTaggedToTaggedSignedEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedTaggedToTaggedSignedEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedTaggedToTaggedSignedEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedTaggedToTaggedSignedEPNS1_4NodeES4_:
.LFB23328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L355
	movq	16(%r14), %r14
.L355:
	movq	(%rsi), %rdi
	addq	$48, %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$1, %r9d
	movl	$25, %esi
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23328:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedTaggedToTaggedSignedEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedTaggedToTaggedSignedEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer33LowerCheckedTaggedToTaggedPointerEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer33LowerCheckedTaggedToTaggedPointerEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer33LowerCheckedTaggedToTaggedPointerEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer33LowerCheckedTaggedToTaggedPointerEPNS1_4NodeES4_:
.LFB23329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L358
	movq	16(%r14), %r14
.L358:
	movq	(%rsi), %rdi
	addq	$48, %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$1, %r9d
	movl	$31, %esi
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23329:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer33LowerCheckedTaggedToTaggedPointerEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer33LowerCheckedTaggedToTaggedPointerEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckBigIntEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckBigIntEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckBigIntEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckBigIntEPNS1_4NodeES4_:
.LFB23330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L361
	movq	16(%r14), %r14
.L361:
	movq	(%rsi), %rdi
	addq	$48, %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	-112(%rbp), %r15
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$31, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler17BigIntMapConstantEv@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$36, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L364
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L364:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23330:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckBigIntEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckBigIntEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer18LowerBigIntAsUintNEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer18LowerBigIntAsUintNEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer18LowerBigIntAsUintNEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer18LowerBigIntAsUintNEPNS1_4NodeES4_:
.LFB23331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	movl	44(%rax), %ecx
	cmpl	$64, %ecx
	jne	.L366
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L371
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	leaq	32(%rsi), %r13
	movl	$1, %esi
	leaq	48(%rdi), %r12
	salq	%cl, %rsi
	movq	%r12, %rdi
	subq	$1, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl@PLT
	movq	%rax, %rdx
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L369
	movq	32(%rbx), %r13
	addq	$16, %r13
.L369:
	movq	0(%r13), %rsi
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler9Word64AndEPNS1_4NodeES4_@PLT
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	movq	16(%r8), %r8
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23331:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer18LowerBigIntAsUintNEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer18LowerBigIntAsUintNEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer36LowerCheckedCompressedToTaggedSignedEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer36LowerCheckedCompressedToTaggedSignedEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer36LowerCheckedCompressedToTaggedSignedEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer36LowerCheckedCompressedToTaggedSignedEPNS1_4NodeES4_:
.LFB23336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L373
	movq	16(%r14), %r14
.L373:
	movq	(%rsi), %rdi
	addq	$48, %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$25, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	addq	$8, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler36ChangeCompressedSignedToTaggedSignedEPNS1_4NodeE@PLT
	.cfi_endproc
.LFE23336:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer36LowerCheckedCompressedToTaggedSignedEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer36LowerCheckedCompressedToTaggedSignedEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer37LowerCheckedCompressedToTaggedPointerEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer37LowerCheckedCompressedToTaggedPointerEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer37LowerCheckedCompressedToTaggedPointerEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer37LowerCheckedCompressedToTaggedPointerEPNS1_4NodeES4_:
.LFB23337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L376
	movq	16(%r14), %r14
.L376:
	movq	(%rsi), %rdi
	addq	$48, %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$31, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	addq	$8, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler38ChangeCompressedPointerToTaggedPointerEPNS1_4NodeE@PLT
	.cfi_endproc
.LFE23337:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer37LowerCheckedCompressedToTaggedPointerEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer37LowerCheckedCompressedToTaggedPointerEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer36LowerCheckedTaggedToCompressedSignedEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer36LowerCheckedTaggedToCompressedSignedEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer36LowerCheckedTaggedToCompressedSignedEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer36LowerCheckedTaggedToCompressedSignedEPNS1_4NodeES4_:
.LFB23338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L379
	movq	16(%r14), %r14
.L379:
	movq	(%rsi), %rdi
	addq	$48, %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$25, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	addq	$8, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler36ChangeTaggedSignedToCompressedSignedEPNS1_4NodeE@PLT
	.cfi_endproc
.LFE23338:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer36LowerCheckedTaggedToCompressedSignedEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer36LowerCheckedTaggedToCompressedSignedEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer37LowerCheckedTaggedToCompressedPointerEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer37LowerCheckedTaggedToCompressedPointerEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer37LowerCheckedTaggedToCompressedPointerEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer37LowerCheckedTaggedToCompressedPointerEPNS1_4NodeES4_:
.LFB23339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L382
	movq	16(%r14), %r14
.L382:
	movq	(%rsi), %rdi
	addq	$48, %r12
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$31, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	addq	$8, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler38ChangeTaggedPointerToCompressedPointerEPNS1_4NodeE@PLT
	.cfi_endproc
.LFE23339:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer37LowerCheckedTaggedToCompressedPointerEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer37LowerCheckedTaggedToCompressedPointerEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer13LowerAllocateEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer13LowerAllocateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer13LowerAllocateEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer13LowerAllocateEPNS1_4NodeE:
.LFB23342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzbl	23(%rsi), %eax
	movq	%rdi, %rbx
	movq	32(%rsi), %r12
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L385
	movq	16(%r12), %r12
.L385:
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler16AllocationTypeOfEPKNS1_8OperatorE@PLT
	leaq	48(%rbx), %rdi
	movq	%r12, %rdx
	popq	%rbx
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	.cfi_endproc
.LFE23342:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer13LowerAllocateEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer13LowerAllocateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer19LowerNumberToStringEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer19LowerNumberToStringEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer19LowerNumberToStringEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer19LowerNumberToStringEPNS1_4NodeE:
.LFB23343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	32(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L388
	movq	16(%r12), %r12
.L388:
	movq	(%rbx), %rax
	leaq	-128(%rbp), %rdi
	movl	$105, %edx
	leaq	48(%rbx), %r15
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$112, %r8d
	leaq	-144(%rbp), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -136(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -144(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	-128(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%rax, -152(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r13, -80(%rbp)
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movq	%r12, %xmm1
	movq	-152(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movq	80(%rbx), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -64(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	3(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L391
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L391:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23343:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer19LowerNumberToStringEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer19LowerNumberToStringEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer24LowerNumberIsFloat64HoleEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer24LowerNumberIsFloat64HoleEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer24LowerNumberIsFloat64HoleEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer24LowerNumberIsFloat64HoleEPNS1_4NodeE:
.LFB23349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L393
	movq	16(%r14), %r14
.L393:
	leaq	48(%rdi), %r12
	movl	$-524289, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE@PLT
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE23349:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer24LowerNumberIsFloat64HoleEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer24LowerNumberIsFloat64HoleEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer19LowerNumberIsFiniteEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer19LowerNumberIsFiniteEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer19LowerNumberIsFiniteEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer19LowerNumberIsFiniteEPNS1_4NodeE:
.LFB23350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L396
	movq	16(%r8), %r8
.L396:
	leaq	48(%rdi), %r12
	movq	%r8, %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	movq	%rax, %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE23350:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer19LowerNumberIsFiniteEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer19LowerNumberIsFiniteEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer16LowerNumberIsNaNEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer16LowerNumberIsNaNEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer16LowerNumberIsNaNEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer16LowerNumberIsNaNEPNS1_4NodeE:
.LFB23359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L399
	movq	16(%r8), %r8
.L399:
	leaq	48(%rdi), %r12
	movq	%r8, %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE23359:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer16LowerNumberIsNaNEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer16LowerNumberIsNaNEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer16LowerObjectIsSmiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer16LowerObjectIsSmiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer16LowerObjectIsSmiEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer16LowerObjectIsSmiEPNS1_4NodeE:
.LFB23363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L402
	movq	16(%r14), %r14
.L402:
	leaq	48(%rdi), %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE23363:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer16LowerObjectIsSmiEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer16LowerObjectIsSmiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer11LowerTypeOfEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer11LowerTypeOfEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer11LowerTypeOfEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer11LowerTypeOfEPNS1_4NodeE:
.LFB23367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	32(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L405
	movq	16(%r12), %r12
.L405:
	movq	(%rbx), %rax
	leaq	-128(%rbp), %rdi
	movl	$109, %edx
	leaq	48(%rbx), %r15
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$112, %r8d
	leaq	-144(%rbp), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -136(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -144(%rbp)
	movl	$16, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	-128(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%rax, -152(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r13, -80(%rbp)
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movq	%r12, %xmm1
	movq	-152(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movq	80(%rbx), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -64(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	3(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L408
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L408:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23367:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer11LowerTypeOfEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer11LowerTypeOfEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer14LowerToBooleanEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer14LowerToBooleanEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer14LowerToBooleanEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer14LowerToBooleanEPNS1_4NodeE:
.LFB23368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	32(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L410
	movq	16(%r12), %r12
.L410:
	movq	(%rbx), %rax
	leaq	-128(%rbp), %rdi
	movl	$92, %edx
	leaq	48(%rbx), %r15
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$112, %r8d
	leaq	-144(%rbp), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -136(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -144(%rbp)
	movl	$16, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	-128(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%rax, -152(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r13, -80(%rbp)
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movq	%r12, %xmm1
	movq	-152(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movq	80(%rbx), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -64(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	3(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L413
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L413:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23368:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer14LowerToBooleanEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer14LowerToBooleanEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25LowerNewArgumentsElementsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25LowerNewArgumentsElementsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25LowerNewArgumentsElementsEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer25LowerNewArgumentsElementsEPNS1_4NodeE:
.LFB23373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	leaq	48(%rbx), %r15
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%r12), %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler33NewArgumentsElementsMappedCountOfEPKNS1_8OperatorE@PLT
	leaq	-144(%rbp), %rdi
	movl	$87, %edx
	movl	%eax, %r14d
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-128(%rbp), %rax
	movq	(%r12), %rcx
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdi
	leaq	-160(%rbp), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -152(%rbp)
	movq	(%rbx), %rax
	movzbl	18(%rcx), %r8d
	movq	%rdi, -160(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movl	%r14d, %esi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14GraphAssembler11SmiConstantEi@PLT
	movq	-144(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r13, %rsi
	movq	%rax, -176(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, -80(%rbp)
	leaq	-112(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-176(%rbp), %xmm0
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movhps	-184(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movq	80(%rbx), %rax
	movaps	%xmm0, -112(%rbp)
	movq	-192(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	5(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L417
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L417:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23373:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25LowerNewArgumentsElementsEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer25LowerNewArgumentsElementsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer14LowerSameValueEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer14LowerSameValueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer14LowerSameValueEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer14LowerSameValueEPNS1_4NodeE:
.LFB23375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	-8(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L420
	leaq	16(%r12), %rsi
	movq	16(%r12), %r12
	addq	$8, %rsi
.L420:
	movq	(%rbx), %rax
	movq	(%rsi), %r13
	movl	$451, %edx
	leaq	48(%rbx), %r15
	leaq	-144(%rbp), %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-128(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$112, %r8d
	leaq	-160(%rbp), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -152(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -160(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	-144(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%rax, -176(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %xmm1
	movdqu	72(%rbx), %xmm2
	xorl	%r8d, %r8d
	movq	-176(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-112(%rbp), %rcx
	movaps	%xmm2, -80(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%r13, %xmm0
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	24(%rax), %edx
	movl	28(%rax), %eax
	leal	4(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L423
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L423:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23375:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer14LowerSameValueEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer14LowerSameValueEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25LowerSameValueNumbersOnlyEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25LowerSameValueNumbersOnlyEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25LowerSameValueNumbersOnlyEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer25LowerSameValueNumbersOnlyEPNS1_4NodeE:
.LFB23376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	-8(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L426
	leaq	16(%r12), %rsi
	movq	16(%r12), %r12
	addq	$8, %rsi
.L426:
	movq	(%rbx), %rax
	movq	(%rsi), %r13
	movl	$452, %edx
	leaq	48(%rbx), %r15
	leaq	-144(%rbp), %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-128(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$112, %r8d
	leaq	-160(%rbp), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -152(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -160(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	-144(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%rax, -176(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %xmm1
	movdqu	72(%rbx), %xmm2
	xorl	%r8d, %r8d
	movq	-176(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-112(%rbp), %rcx
	movaps	%xmm2, -80(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%r13, %xmm0
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	24(%rax), %edx
	movl	28(%rax), %eax
	leal	4(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L429
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L429:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23376:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25LowerSameValueNumbersOnlyEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer25LowerSameValueNumbersOnlyEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer14LowerDeadValueEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer14LowerDeadValueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer14LowerDeadValueEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer14LowerDeadValueEPNS1_4NodeE:
.LFB23378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpw	$59, 16(%rax)
	je	.L431
	leaq	48(%rbx), %rdi
	call	_ZN2v88internal8compiler14GraphAssembler11UnreachableEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
.L431:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23378:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer14LowerDeadValueEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer14LowerDeadValueEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer19LowerStringToNumberEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer19LowerStringToNumberEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer19LowerStringToNumberEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer19LowerStringToNumberEPNS1_4NodeE:
.LFB23379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	32(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L434
	movq	16(%r12), %r12
.L434:
	movq	(%rbx), %rax
	leaq	-128(%rbp), %rdi
	movl	$98, %edx
	leaq	48(%rbx), %r15
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$112, %r8d
	leaq	-144(%rbp), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -136(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -144(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	-128(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%rax, -152(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r13, -80(%rbp)
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movq	%r12, %xmm1
	movq	-152(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movq	80(%rbx), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -64(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	3(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L437
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L437:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23379:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer19LowerStringToNumberEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer19LowerStringToNumberEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer22LowerStringCodePointAtEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer22LowerStringCodePointAtEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer22LowerStringCodePointAtEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer22LowerStringCodePointAtEPNS1_4NodeE:
.LFB23381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	-8(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L440
	leaq	16(%r12), %rsi
	movq	16(%r12), %r12
	addq	$8, %rsi
.L440:
	movq	(%rbx), %rax
	movq	(%rsi), %r13
	movl	$47, %edx
	leaq	48(%rbx), %r15
	leaq	-144(%rbp), %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-128(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$48, %r8d
	leaq	-160(%rbp), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -152(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -160(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	-144(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%rax, -176(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %xmm1
	movdqu	72(%rbx), %xmm2
	xorl	%r8d, %r8d
	movq	-176(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-112(%rbp), %rcx
	movaps	%xmm2, -80(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%r13, %xmm0
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	24(%rax), %edx
	movl	28(%rax), %eax
	leal	4(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L443
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L443:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23381:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer22LowerStringCodePointAtEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer22LowerStringCodePointAtEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer26LowerStringToLowerCaseIntlEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringToLowerCaseIntlEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringToLowerCaseIntlEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringToLowerCaseIntlEPNS1_4NodeE:
.LFB23384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	32(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L445
	movq	16(%r12), %r12
.L445:
	movq	(%rbx), %rax
	leaq	-128(%rbp), %rdi
	movl	$1056, %edx
	leaq	48(%rbx), %r15
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$96, %r8d
	leaq	-144(%rbp), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -136(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -144(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	-128(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%rax, -152(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r13, -80(%rbp)
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movq	%r12, %xmm1
	movq	-152(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movq	80(%rbx), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -64(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	3(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L448
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L448:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23384:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringToLowerCaseIntlEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringToLowerCaseIntlEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer26LowerStringToUpperCaseIntlEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringToUpperCaseIntlEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringToUpperCaseIntlEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringToUpperCaseIntlEPNS1_4NodeE:
.LFB23385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L450
	movq	16(%r13), %r13
.L450:
	movq	(%rbx), %rax
	xorl	%r8d, %r8d
	movl	$96, %ecx
	movl	$1, %edx
	leaq	48(%rbx), %r12
	movl	$185, %esi
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$185, %edi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler18CEntryStubConstantEi@PLT
	movq	%r15, %rsi
	movq	%rax, -136(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r14, -80(%rbp)
	leaq	-112(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-136(%rbp), %xmm0
	movq	%r13, %xmm1
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -72(%rbp)
	movq	80(%rbx), %rax
	movaps	%xmm0, -112(%rbp)
	movq	-128(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	5(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L453
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L453:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23385:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringToUpperCaseIntlEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringToUpperCaseIntlEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer18LowerStringIndexOfEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer18LowerStringIndexOfEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer18LowerStringIndexOfEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer18LowerStringIndexOfEPNS1_4NodeE:
.LFB23387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L455
	movq	40(%rsi), %rax
	addq	$48, %rsi
	movq	%rax, -184(%rbp)
.L456:
	movq	(%rsi), %rax
	leaq	-144(%rbp), %rdi
	movl	$52, %edx
	leaq	48(%rbx), %r12
	movq	%rax, -176(%rbp)
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-128(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$112, %r8d
	leaq	-160(%rbp), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -152(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -160(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	-144(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%rax, -168(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r15, -80(%rbp)
	leaq	-112(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-168(%rbp), %xmm0
	movq	%r13, %xmm1
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -72(%rbp)
	movq	80(%rbx), %rax
	movaps	%xmm0, -112(%rbp)
	movq	-184(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movhps	-176(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	5(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L459
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_restore_state
	movq	24(%r13), %rax
	leaq	16(%r13), %rsi
	movq	16(%r13), %r13
	addq	$16, %rsi
	movq	%rax, -184(%rbp)
	jmp	.L456
.L459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23387:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer18LowerStringIndexOfEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer18LowerStringIndexOfEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer26LowerStringFromCodePointAtEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringFromCodePointAtEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringFromCodePointAtEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringFromCodePointAtEPNS1_4NodeE:
.LFB23388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	-8(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L462
	leaq	16(%r12), %rsi
	movq	16(%r12), %r12
	addq	$8, %rsi
.L462:
	movq	(%rbx), %rax
	movq	(%rsi), %r13
	movl	$48, %edx
	leaq	48(%rbx), %r15
	leaq	-144(%rbp), %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-128(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$112, %r8d
	leaq	-160(%rbp), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -152(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -160(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	-144(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%rax, -176(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %xmm1
	movdqu	72(%rbx), %xmm2
	xorl	%r8d, %r8d
	movq	-176(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-112(%rbp), %rcx
	movaps	%xmm2, -80(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%r13, %xmm0
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	24(%rax), %edx
	movl	28(%rax), %eax
	leal	4(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L465
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L465:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23388:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringFromCodePointAtEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringFromCodePointAtEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer17LowerStringLengthEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer17LowerStringLengthEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer17LowerStringLengthEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer17LowerStringLengthEPNS1_4NodeE:
.LFB23389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L467
	movq	16(%r13), %r13
.L467:
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder15ForStringLengthEv@PLT
	leaq	48(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L470
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L470:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23389:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer17LowerStringLengthEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer17LowerStringLengthEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer21LowerStringComparisonERKNS0_8CallableEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringComparisonERKNS0_8CallableEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringComparisonERKNS0_8CallableEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringComparisonERKNS0_8CallableEPNS1_4NodeE:
.LFB23390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	-8(%rdx), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	-17(%rdx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L473
	leaq	16(%r12), %rdx
	movq	16(%r12), %r12
	addq	$8, %rdx
.L473:
	movq	(%rdx), %rax
	xorl	%r9d, %r9d
	movl	$112, %r8d
	leaq	-128(%rbp), %rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	leaq	48(%rbx), %r14
	movq	%rax, -152(%rbp)
	movq	16(%r13), %rax
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -120(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -128(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	0(%r13), %rsi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, %rsi
	movq	%rax, -144(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %xmm1
	movdqu	72(%rbx), %xmm2
	xorl	%r8d, %r8d
	movq	-144(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-112(%rbp), %rcx
	movaps	%xmm2, -80(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-152(%rbp), %xmm0
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	24(%rax), %edx
	movl	28(%rax), %eax
	leal	4(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L476
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L476:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23390:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringComparisonERKNS0_8CallableEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringComparisonERKNS0_8CallableEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer20LowerStringSubstringEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer20LowerStringSubstringEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer20LowerStringSubstringEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer20LowerStringSubstringEPNS1_4NodeE:
.LFB23391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rsi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	48(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rsi), %rax
	movq	%rax, -168(%rbp)
	movq	16(%rdx), %rax
	movzbl	16(%rax), %ecx
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L478
	movq	8(%r15), %r13
	cmpb	$5, %cl
	je	.L479
.L480:
	movq	16(%r15), %rax
	addq	$16, %r15
	movq	%rax, -176(%rbp)
	cmpb	$5, %cl
	je	.L490
.L484:
	movq	360(%rdx), %rsi
	leaq	-144(%rbp), %rdi
	movl	$55, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-128(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$112, %r8d
	leaq	-160(%rbp), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -152(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -160(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rsi
	movq	%rax, -184(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r15, -80(%rbp)
	leaq	-112(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-184(%rbp), %xmm0
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movhps	-168(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movq	80(%rbx), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%r13, %xmm0
	movhps	-176(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	5(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L491
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	.cfi_restore_state
	movq	-168(%rbp), %rdi
	leaq	16(%rdi), %rax
	movq	16(%rdi), %rdi
	movq	8(%rax), %r13
	movq	%rdi, -168(%rbp)
	cmpb	$5, %cl
	je	.L479
.L481:
	leaq	16(%rax), %r15
	movq	(%r15), %rax
	movq	%rax, -176(%rbp)
	cmpb	$5, %cl
	jne	.L484
.L490:
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	(%rbx), %rdx
	movq	%rax, -176(%rbp)
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L479:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	%rax, %r13
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L492
	movq	(%rbx), %rdx
	movq	16(%rdx), %rax
	movzbl	16(%rax), %ecx
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L492:
	movq	(%rbx), %rdx
	movq	32(%r12), %r15
	movq	16(%rdx), %rcx
	leaq	16(%r15), %rax
	movzbl	16(%rcx), %ecx
	jmp	.L481
.L491:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23391:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer20LowerStringSubstringEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer20LowerStringSubstringEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer16LowerStringEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer16LowerStringEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer16LowerStringEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer16LowerStringEqualEPNS1_4NodeE:
.LFB23392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$49, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r14, %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringComparisonERKNS0_8CallableEPNS1_4NodeE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L496
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L496:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23392:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer16LowerStringEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer16LowerStringEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer19LowerStringLessThanEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer19LowerStringLessThanEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer19LowerStringLessThanEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer19LowerStringLessThanEPNS1_4NodeE:
.LFB23393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$53, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r14, %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringComparisonERKNS0_8CallableEPNS1_4NodeE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L500
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L500:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23393:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer19LowerStringLessThanEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer19LowerStringLessThanEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer26LowerStringLessThanOrEqualEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringLessThanOrEqualEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringLessThanOrEqualEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringLessThanOrEqualEPNS1_4NodeE:
.LFB23394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$54, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r14, %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringComparisonERKNS0_8CallableEPNS1_4NodeE
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L504
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L504:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23394:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringLessThanOrEqualEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringLessThanOrEqualEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer14LowerBigIntAddEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer14LowerBigIntAddEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer14LowerBigIntAddEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer14LowerBigIntAddEPNS1_4NodeES4_:
.LFB23395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	-8(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L507
	leaq	16(%r12), %rsi
	movq	16(%r12), %r12
	addq	$8, %rsi
.L507:
	movq	(%rsi), %rax
	leaq	-144(%rbp), %rdi
	movl	$788, %edx
	leaq	48(%rbx), %r13
	movq	%rax, -184(%rbp)
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-128(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	leaq	-160(%rbp), %r10
	xorl	%r9d, %r9d
	movq	%r10, %rsi
	movl	$56, %r8d
	movq	%r10, -192(%rbp)
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -152(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -160(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	(%rbx), %rdi
	movq	-144(%rbp), %rsi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, %rsi
	movq	%rax, -176(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, %xmm1
	xorl	%r8d, %r8d
	leaq	-112(%rbp), %rcx
	movq	-176(%rbp), %xmm0
	movdqu	72(%rbx), %xmm2
	movq	%rax, %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	-184(%rbp), %xmm0
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	24(%rax), %edx
	movl	28(%rax), %eax
	leal	4(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, 72(%rbx)
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r14, %r8
	movl	$1, %esi
	movq	%r13, %rdi
	movq	-192(%rbp), %r10
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L510
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L510:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23395:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer14LowerBigIntAddEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer14LowerBigIntAddEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer17LowerBigIntNegateEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer17LowerBigIntNegateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer17LowerBigIntNegateEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer17LowerBigIntNegateEPNS1_4NodeE:
.LFB23396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$790, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	-112(%rbp), %rdi
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-128(%rbp), %rsi
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$56, %r8d
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -120(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -128(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	leaq	48(%rbx), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	leaq	32(%r12), %rdx
	movq	%rax, %r13
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L512
	movq	32(%r12), %rdx
	addq	$16, %rdx
.L512:
	movq	(%rbx), %rdi
	movq	-112(%rbp), %rsi
	movq	(%rdx), %r12
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%rax, -136(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r13, -64(%rbp)
	leaq	-80(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movq	%r12, %xmm1
	movq	-136(%rbp), %xmm0
	movq	%rax, -56(%rbp)
	movq	80(%rbx), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -48(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	3(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L516
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L516:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23396:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer17LowerBigIntNegateEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer17LowerBigIntNegateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer23LowerCheckNotTaggedHoleEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer23LowerCheckNotTaggedHoleEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer23LowerCheckNotTaggedHoleEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer23LowerCheckNotTaggedHoleEPNS1_4NodeES4_:
.LFB23398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L518
	movq	16(%r13), %r13
.L518:
	leaq	48(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15TheHoleConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	leaq	-64(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$1, %r9d
	movl	$6, %esi
	movq	$0, -64(%rbp)
	movl	$-1, -56(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L521
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L521:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23398:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer23LowerCheckNotTaggedHoleEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer23LowerCheckNotTaggedHoleEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer22LowerCheckEqualsSymbolEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer22LowerCheckEqualsSymbolEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer22LowerCheckEqualsSymbolEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer22LowerCheckEqualsSymbolEPNS1_4NodeES4_:
.LFB23401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	subq	$32, %rsp
	movq	-8(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L524
	leaq	16(%r8), %rsi
	movq	16(%r8), %r8
	addq	$8, %rsi
.L524:
	movq	(%rsi), %rdx
	leaq	48(%rdi), %r12
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	leaq	-48(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$1, %r9d
	movl	$38, %esi
	movq	$0, -48(%rbp)
	movl	$-1, -40(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L527
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L527:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23401:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer22LowerCheckEqualsSymbolEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer22LowerCheckEqualsSymbolEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer27AllocateHeapNumberWithValueEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer27AllocateHeapNumberWithValueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer27AllocateHeapNumberWithValueEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer27AllocateHeapNumberWithValueEPNS1_4NodeE:
.LFB23402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$16, %esi
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	48(%rdi), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L531
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L531:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23402:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer27AllocateHeapNumberWithValueEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer27AllocateHeapNumberWithValueEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer17ChangeIntPtrToSmiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer17ChangeIntPtrToSmiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer17ChangeIntPtrToSmiEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer17ChangeIntPtrToSmiEPNS1_4NodeE:
.LFB23403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$32, %esi
	pushq	%r12
	.cfi_offset 12, -32
	leaq	48(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE23403:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer17ChangeIntPtrToSmiEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer17ChangeIntPtrToSmiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer19ChangeInt32ToIntPtrEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer19ChangeInt32ToIntPtrEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer19ChangeInt32ToIntPtrEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer19ChangeInt32ToIntPtrEPNS1_4NodeE:
.LFB23404:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	%rsi, %rax
	movq	16(%rdx), %rdx
	cmpb	$5, 16(%rdx)
	je	.L536
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	addq	$48, %rdi
	jmp	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	.cfi_endproc
.LFE23404:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer19ChangeInt32ToIntPtrEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer19ChangeInt32ToIntPtrEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer19ChangeIntPtrToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer19ChangeIntPtrToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer19ChangeIntPtrToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer19ChangeIntPtrToInt32EPNS1_4NodeE:
.LFB23405:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	%rsi, %rax
	movq	16(%rdx), %rdx
	cmpb	$5, 16(%rdx)
	je	.L539
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	addq	$48, %rdi
	jmp	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	.cfi_endproc
.LFE23405:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer19ChangeIntPtrToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer19ChangeIntPtrToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer26ChangeInt32ToCompressedSmiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer26ChangeInt32ToCompressedSmiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer26ChangeInt32ToCompressedSmiEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer26ChangeInt32ToCompressedSmiEPNS1_4NodeE:
.LFB29014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE29014:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer26ChangeInt32ToCompressedSmiEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer26ChangeInt32ToCompressedSmiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer16ChangeInt32ToSmiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer16ChangeInt32ToSmiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer16ChangeInt32ToSmiEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer16ChangeInt32ToSmiEPNS1_4NodeE:
.LFB23407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	48(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	%rsi, %r12
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L545
.L543:
	movq	%r13, %rdi
	movl	$32, %esi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	.p2align 4,,10
	.p2align 3
.L545:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	%rax, %r12
	jmp	.L543
	.cfi_endproc
.LFE23407:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer16ChangeInt32ToSmiEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer16ChangeInt32ToSmiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer16ChangeInt64ToSmiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer16ChangeInt64ToSmiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer16ChangeInt64ToSmiEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer16ChangeInt64ToSmiEPNS1_4NodeE:
.LFB23408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$32, %esi
	pushq	%r12
	.cfi_offset 12, -32
	leaq	48(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE23408:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer16ChangeInt64ToSmiEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer16ChangeInt64ToSmiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer21ChangeUint32ToUintPtrEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer21ChangeUint32ToUintPtrEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer21ChangeUint32ToUintPtrEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer21ChangeUint32ToUintPtrEPNS1_4NodeE:
.LFB23409:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	%rsi, %rax
	movq	16(%rdx), %rdx
	cmpb	$5, 16(%rdx)
	je	.L550
	ret
	.p2align 4,,10
	.p2align 3
.L550:
	addq	$48, %rdi
	jmp	_ZN2v88internal8compiler14GraphAssembler20ChangeUint32ToUint64EPNS1_4NodeE@PLT
	.cfi_endproc
.LFE23409:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer21ChangeUint32ToUintPtrEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer21ChangeUint32ToUintPtrEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer17ChangeUint32ToSmiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer17ChangeUint32ToSmiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer17ChangeUint32ToSmiEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer17ChangeUint32ToSmiEPNS1_4NodeE:
.LFB23410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	48(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movl	$32, %esi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$24, %rsp
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%rax, %rdx
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L554
.L552:
	addq	$24, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	.p2align 4,,10
	.p2align 3
.L554:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeUint32ToUint64EPNS1_4NodeE@PLT
	movq	-40(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L552
	.cfi_endproc
.LFE23410:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer17ChangeUint32ToSmiEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer17ChangeUint32ToSmiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer17ChangeSmiToIntPtrEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer17ChangeSmiToIntPtrEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer17ChangeSmiToIntPtrEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer17ChangeSmiToIntPtrEPNS1_4NodeE:
.LFB23411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$32, %esi
	pushq	%r12
	.cfi_offset 12, -32
	leaq	48(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE23411:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer17ChangeSmiToIntPtrEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer17ChangeSmiToIntPtrEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer16ChangeSmiToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer16ChangeSmiToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer16ChangeSmiToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer16ChangeSmiToInt32EPNS1_4NodeE:
.LFB23412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	48(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	%rsi, %r12
	movq	%r13, %rdi
	movl	$32, %esi
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L560
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	.cfi_endproc
.LFE23412:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer16ChangeSmiToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer16ChangeSmiToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer26ChangeCompressedSmiToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer26ChangeCompressedSmiToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer26ChangeCompressedSmiToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer26ChangeCompressedSmiToInt32EPNS1_4NodeE:
.LFB29016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE29016:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer26ChangeCompressedSmiToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer26ChangeCompressedSmiToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer16ChangeSmiToInt64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer16ChangeSmiToInt64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer16ChangeSmiToInt64EPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer16ChangeSmiToInt64EPNS1_4NodeE:
.LFB23414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	jne	.L566
	leaq	48(%rdi), %r12
	movq	%rsi, %r13
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	.p2align 4,,10
	.p2align 3
.L566:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23414:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer16ChangeSmiToInt64EPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer16ChangeSmiToInt64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer11ObjectIsSmiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer11ObjectIsSmiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer11ObjectIsSmiEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer11ObjectIsSmiEPNS1_4NodeE:
.LFB23415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	48(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE23415:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer11ObjectIsSmiEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer11ObjectIsSmiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer21CompressedObjectIsSmiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer21CompressedObjectIsSmiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer21CompressedObjectIsSmiEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer21CompressedObjectIsSmiEPNS1_4NodeE:
.LFB23416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	48(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE23416:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer21CompressedObjectIsSmiEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer21CompressedObjectIsSmiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer19SmiMaxValueConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer19SmiMaxValueConstantEv
	.type	_ZN2v88internal8compiler23EffectControlLinearizer19SmiMaxValueConstantEv, @function
_ZN2v88internal8compiler23EffectControlLinearizer19SmiMaxValueConstantEv:
.LFB23417:
	.cfi_startproc
	endbr64
	addq	$48, %rdi
	movl	$2147483647, %esi
	jmp	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	.cfi_endproc
.LFE23417:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer19SmiMaxValueConstantEv, .-_ZN2v88internal8compiler23EffectControlLinearizer19SmiMaxValueConstantEv
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer20SmiShiftBitsConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer20SmiShiftBitsConstantEv
	.type	_ZN2v88internal8compiler23EffectControlLinearizer20SmiShiftBitsConstantEv, @function
_ZN2v88internal8compiler23EffectControlLinearizer20SmiShiftBitsConstantEv:
.LFB23418:
	.cfi_startproc
	endbr64
	addq	$48, %rdi
	movl	$32, %esi
	jmp	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	.cfi_endproc
.LFE23418:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer20SmiShiftBitsConstantEv, .-_ZN2v88internal8compiler23EffectControlLinearizer20SmiShiftBitsConstantEv
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer27LowerPlainPrimitiveToNumberEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer27LowerPlainPrimitiveToNumberEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer27LowerPlainPrimitiveToNumberEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer27LowerPlainPrimitiveToNumberEPNS1_4NodeE:
.LFB23419:
	.cfi_startproc
	endbr64
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L574
	movq	16(%r8), %r8
.L574:
	addq	$48, %rdi
	movq	%r8, %rsi
	jmp	_ZN2v88internal8compiler14GraphAssembler8ToNumberEPNS1_4NodeE@PLT
	.cfi_endproc
.LFE23419:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer27LowerPlainPrimitiveToNumberEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer27LowerPlainPrimitiveToNumberEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer17BuildReverseBytesENS0_17ExternalArrayTypeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer17BuildReverseBytesENS0_17ExternalArrayTypeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer17BuildReverseBytesENS0_17ExternalArrayTypeEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer17BuildReverseBytesENS0_17ExternalArrayTypeEPNS1_4NodeE:
.LFB23426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	leaq	.L578(%rip), %rdx
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler23EffectControlLinearizer17BuildReverseBytesENS0_17ExternalArrayTypeEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L578:
	.long	.L577-.L578
	.long	.L579-.L578
	.long	.L579-.L578
	.long	.L584-.L578
	.long	.L583-.L578
	.long	.L582-.L578
	.long	.L582-.L578
	.long	.L581-.L578
	.long	.L580-.L578
	.long	.L579-.L578
	.long	.L577-.L578
	.long	.L577-.L578
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer17BuildReverseBytesENS0_17ExternalArrayTypeEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L579:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L582:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rsi
	addq	$48, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler18Word32ReverseBytesEPNS1_4NodeE@PLT
	.p2align 4,,10
	.p2align 3
.L584:
	.cfi_restore_state
	leaq	48(%rdi), %r12
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler18Word32ReverseBytesEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movl	$16, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler9Word32SarEPNS1_4NodeES4_@PLT
	.p2align 4,,10
	.p2align 3
.L583:
	.cfi_restore_state
	leaq	48(%rdi), %r12
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler18Word32ReverseBytesEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movl	$16, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler9Word32ShrEPNS1_4NodeES4_@PLT
	.p2align 4,,10
	.p2align 3
.L580:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	48(%rdi), %r12
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L587
	call	_ZN2v88internal8compiler14GraphAssembler23Float64ExtractLowWord32EPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler18Word32ReverseBytesEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler18Word32ReverseBytesEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler22Float64InsertLowWord32EPNS1_4NodeES4_@PLT
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler23Float64InsertHighWord32EPNS1_4NodeES4_@PLT
	.p2align 4,,10
	.p2align 3
.L581:
	.cfi_restore_state
	leaq	48(%rdi), %r12
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler21BitcastFloat32ToInt32EPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler18Word32ReverseBytesEPNS1_4NodeE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler21BitcastInt32ToFloat32EPNS1_4NodeE@PLT
	.p2align 4,,10
	.p2align 3
.L587:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler21BitcastFloat64ToInt64EPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler18Word64ReverseBytesEPNS1_4NodeE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler21BitcastInt64ToFloat64EPNS1_4NodeE@PLT
.L577:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23426:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer17BuildReverseBytesENS0_17ExternalArrayTypeEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer17BuildReverseBytesENS0_17ExternalArrayTypeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer21LowerLoadTypedElementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer21LowerLoadTypedElementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer21LowerLoadTypedElementEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer21LowerLoadTypedElementEPNS1_4NodeE:
.LFB23429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19ExternalArrayTypeOfEPKNS1_8OperatorE@PLT
	movl	%eax, %r13d
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L589
	movq	32(%rbx), %rsi
	movq	40(%rbx), %r8
	addq	$56, %rbx
	movq	-8(%rbx), %r14
.L590:
	addq	$48, %r12
	movq	%r8, -88(%rbp)
	movq	(%rbx), %r15
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler6RetainEPNS1_4NodeE@PLT
	movq	-88(%rbp), %r8
	movq	(%r8), %rdx
	movzwl	16(%rdx), %eax
	cmpl	$23, %eax
	je	.L601
	cmpl	$24, %eax
	je	.L602
.L593:
	movq	%r14, %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler16UnsafePointerAddEPNS1_4NodeES4_@PLT
	movq	%rax, %r14
.L594:
	leaq	-80(%rbp), %rbx
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder20ForTypedArrayElementENS0_17ExternalArrayTypeEbNS0_15LoadSensitivityE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler11LoadElementERKNS1_13ElementAccessEPNS1_4NodeES7_@PLT
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L603
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L602:
	.cfi_restore_state
	movq	48(%rdx), %rax
.L592:
	testq	%rax, %rax
	jne	.L593
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L601:
	movslq	44(%rdx), %rax
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L589:
	movq	32(%rbx), %rbx
	movq	16(%rbx), %rsi
	movq	24(%rbx), %r8
	addq	$40, %rbx
	movq	-8(%rbx), %r14
	jmp	.L590
.L603:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23429:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer21LowerLoadTypedElementEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer21LowerLoadTypedElementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer22LowerStoreTypedElementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer22LowerStoreTypedElementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer22LowerStoreTypedElementEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer22LowerStoreTypedElementEPNS1_4NodeE:
.LFB23430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19ExternalArrayTypeOfEPKNS1_8OperatorE@PLT
	movl	%eax, %r13d
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L605
	movq	32(%rbx), %rsi
	movq	40(%rbx), %r8
	addq	$64, %rbx
	movq	-16(%rbx), %r15
	movq	-8(%rbx), %r14
.L606:
	addq	$48, %r12
	movq	%r8, -88(%rbp)
	movq	(%rbx), %rbx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler6RetainEPNS1_4NodeE@PLT
	movq	-88(%rbp), %r8
	movq	(%r8), %rdx
	movzwl	16(%rdx), %eax
	cmpl	$23, %eax
	je	.L617
	cmpl	$24, %eax
	je	.L618
.L609:
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler16UnsafePointerAddEPNS1_4NodeES4_@PLT
	movq	%rax, %r15
.L610:
	leaq	-80(%rbp), %r9
	movl	$1, %ecx
	movl	$1, %edx
	movl	%r13d, %esi
	movq	%r9, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder20ForTypedArrayElementENS0_17ExternalArrayTypeEbNS0_15LoadSensitivityE@PLT
	movq	-88(%rbp), %r9
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L619
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L618:
	.cfi_restore_state
	movq	48(%rdx), %rax
.L608:
	testq	%rax, %rax
	jne	.L609
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L617:
	movslq	44(%rdx), %rax
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L605:
	movq	32(%rbx), %rbx
	movq	16(%rbx), %rsi
	movq	24(%rbx), %r8
	addq	$48, %rbx
	movq	-16(%rbx), %r15
	movq	-8(%rbx), %r14
	jmp	.L606
.L619:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23430:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer22LowerStoreTypedElementEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer22LowerStoreTypedElementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer20TransitionElementsToEPNS1_4NodeES4_NS0_12ElementsKindES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer20TransitionElementsToEPNS1_4NodeES4_NS0_12ElementsKindES5_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer20TransitionElementsToEPNS1_4NodeES4_NS0_12ElementsKindES5_, @function
_ZN2v88internal8compiler23EffectControlLinearizer20TransitionElementsToEPNS1_4NodeES4_NS0_12ElementsKindES5_:
.LFB23431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	%rdx, -200(%rbp)
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$3, %r8b
	je	.L634
	call	_ZN2v88internal8compiler20DoubleMapParameterOfEPKNS1_8OperatorE@PLT
	leaq	48(%rbx), %r14
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r13
	testb	%r12b, %r12b
	je	.L624
.L623:
	cmpb	$4, %r12b
	je	.L630
	cmpb	$2, %r12b
	je	.L631
	cmpb	$6, %r12b
	je	.L632
	cmpb	%r12b, %r15b
	je	.L626
	cmpb	$1, %r12b
	jbe	.L622
.L627:
	movq	(%rbx), %rax
	xorl	%r8d, %r8d
	movl	$96, %ecx
	movl	$2, %edx
	movl	$10, %esi
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movl	$2, %esi
	movq	%r14, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$10, %edi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler18CEntryStubConstantEi@PLT
	movq	%r12, %rsi
	movq	%rax, -232(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movdqu	72(%rbx), %xmm1
	leaq	-128(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-232(%rbp), %xmm0
	movq	%rax, %rsi
	movaps	%xmm1, -80(%rbp)
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r13, %xmm0
	movhps	-224(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-216(%rbp), %xmm0
	movhps	-208(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	24(%rax), %edx
	movl	28(%rax), %eax
	leal	6(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
.L620:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L635
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L624:
	.cfi_restore_state
	cmpb	$1, %r15b
	jne	.L622
.L626:
	leaq	-192(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-200(%rbp), %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L634:
	call	_ZN2v88internal8compiler18FastMapParameterOfEPKNS1_8OperatorE@PLT
	leaq	48(%rbx), %r14
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r13
	testb	%r12b, %r12b
	jne	.L623
.L622:
	leal	-2(%r15), %r8d
	cmpb	$1, %r8b
	ja	.L627
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L632:
	movl	$7, %eax
.L625:
	cmpb	%al, %r15b
	jne	.L627
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L630:
	movl	$5, %eax
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L631:
	movl	$3, %eax
	jmp	.L625
.L635:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23431:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer20TransitionElementsToEPNS1_4NodeES4_NS0_12ElementsKindES5_, .-_ZN2v88internal8compiler23EffectControlLinearizer20TransitionElementsToEPNS1_4NodeES4_NS0_12ElementsKindES5_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25IsElementsKindGreaterThanEPNS1_4NodeENS0_12ElementsKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25IsElementsKindGreaterThanEPNS1_4NodeENS0_12ElementsKindE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25IsElementsKindGreaterThanEPNS1_4NodeENS0_12ElementsKindE, @function
_ZN2v88internal8compiler23EffectControlLinearizer25IsElementsKindGreaterThanEPNS1_4NodeENS0_12ElementsKindE:
.LFB23432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movzbl	%dl, %esi
	pushq	%r12
	.cfi_offset 12, -32
	leaq	48(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE23432:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25IsElementsKindGreaterThanEPNS1_4NodeENS0_12ElementsKindE, .-_ZN2v88internal8compiler23EffectControlLinearizer25IsElementsKindGreaterThanEPNS1_4NodeENS0_12ElementsKindE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer17LowerRuntimeAbortEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer17LowerRuntimeAbortEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer17LowerRuntimeAbortEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer17LowerRuntimeAbortEPNS1_4NodeE:
.LFB23437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	48(%rbx), %r13
	subq	$104, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13AbortReasonOfEPKNS1_8OperatorE@PLT
	xorl	%r8d, %r8d
	movl	$96, %ecx
	movl	$1, %edx
	movzbl	%al, %r12d
	movq	(%rbx), %rax
	movl	$348, %esi
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$348, %edi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	pxor	%xmm0, %xmm0
	movq	(%rbx), %rdi
	cvtsi2sdl	%r12d, %xmm0
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler18CEntryStubConstantEi@PLT
	movq	%r15, %rsi
	movq	%rax, -136(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r14, -80(%rbp)
	xorl	%r8d, %r8d
	leaq	-112(%rbp), %rcx
	movq	-136(%rbp), %xmm0
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movhps	-144(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movq	80(%rbx), %rax
	movaps	%xmm0, -112(%rbp)
	movq	-128(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	5(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L641
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L641:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23437:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer17LowerRuntimeAbortEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer17LowerRuntimeAbortEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer15LowerAssertTypeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer15LowerAssertTypeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer15LowerAssertTypeEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer15LowerAssertTypeEPNS1_4NodeE:
.LFB23438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-168(%rbp), %rdi
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	48(%rax), %rax
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal8compiler4Type7AsRangeEv@PLT
	leaq	32(%r12), %rdx
	movq	%rax, %r15
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L643
	movq	32(%r12), %rax
	leaq	16(%rax), %rdx
.L643:
	leaq	48(%rbx), %r14
	movsd	8(%r15), %xmm0
	movq	(%rdx), %r13
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14NumberConstantEd@PLT
	movsd	16(%r15), %xmm0
	movq	%r14, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14NumberConstantEd@PLT
	leaq	-144(%rbp), %rdi
	movl	$787, %edx
	movq	%rax, -200(%rbp)
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-128(%rbp), %rax
	movq	(%r12), %rcx
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -152(%rbp)
	movq	(%rbx), %rax
	movzbl	18(%rcx), %r8d
	movq	%rsi, -160(%rbp)
	xorl	%ecx, %ecx
	leaq	-160(%rbp), %rsi
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	-144(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, %rsi
	movq	%rax, -184(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r12, -80(%rbp)
	xorl	%r8d, %r8d
	leaq	-112(%rbp), %rcx
	movq	-184(%rbp), %xmm0
	movq	%r13, %xmm1
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -72(%rbp)
	movq	80(%rbx), %rax
	movaps	%xmm0, -112(%rbp)
	movq	-192(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	5(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L647
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L647:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23438:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer15LowerAssertTypeEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer15LowerAssertTypeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer28LowerFindOrderedHashMapEntryEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer28LowerFindOrderedHashMapEntryEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer28LowerFindOrderedHashMapEntryEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer28LowerFindOrderedHashMapEntryEPNS1_4NodeE:
.LFB23446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	leaq	48(%rbx), %r13
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	leaq	-128(%rbp), %rdi
	movl	$395, %edx
	movq	%rax, -176(%rbp)
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rax
	movq	(%r12), %rcx
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdi
	leaq	-144(%rbp), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -136(%rbp)
	movq	(%rbx), %rax
	movzbl	18(%rcx), %r8d
	movq	%rdi, -144(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	-128(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rsi
	movq	%rax, -160(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movdqu	72(%rbx), %xmm1
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-160(%rbp), %xmm0
	movq	%rax, %rsi
	movaps	%xmm1, -64(%rbp)
	movhps	-168(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-176(%rbp), %xmm0
	movhps	-152(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movl	24(%rax), %edx
	movl	28(%rax), %eax
	leal	4(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L651
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L651:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23446:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer28LowerFindOrderedHashMapEntryEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer28LowerFindOrderedHashMapEntryEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer19ComputeUnseededHashEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer19ComputeUnseededHashEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer19ComputeUnseededHashEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer19ComputeUnseededHashEPNS1_4NodeE:
.LFB23447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	$15, %esi
	pushq	%r12
	.cfi_offset 12, -40
	leaq	48(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32ShlEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movl	$-1, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32XorEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler8Int32AddEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movl	$12, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32ShrEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32XorEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32ShlEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8Int32AddEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32ShrEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32XorEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movl	$2057, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8Int32MulEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movl	$16, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32ShrEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32XorEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movl	$1073741823, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	.cfi_endproc
.LFE23447:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer19ComputeUnseededHashEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer19ComputeUnseededHashEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer12LowerDateNowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer12LowerDateNowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer12LowerDateNowEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer12LowerDateNowEPNS1_4NodeE:
.LFB23454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	$96, %ecx
	movl	$62, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	48(%rbx), %r12
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$62, %edi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler18CEntryStubConstantEi@PLT
	movq	%r13, %rsi
	movq	%rax, -128(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	-128(%rbp), %xmm0
	movdqu	72(%rbx), %xmm1
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	leaq	-96(%rbp), %rcx
	movhps	-120(%rbp), %xmm0
	movaps	%xmm1, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-112(%rbp), %xmm0
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movl	24(%rax), %edx
	movl	28(%rax), %eax
	leal	4(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L657
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L657:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23454:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer12LowerDateNowEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer12LowerDateNowEPNS1_4NodeE
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB26747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L696
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L674
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L697
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L660:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L698
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L663:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L697:
	testq	%rdx, %rdx
	jne	.L699
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L661:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L664
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L677
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L677
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L666:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L666
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L668
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L668:
	leaq	16(%rax,%r8), %rcx
.L664:
	cmpq	%r14, %r12
	je	.L669
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L678
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L678
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L671:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L671
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L673
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L673:
	leaq	8(%rcx,%r9), %rcx
.L669:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L678:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L670:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L670
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L677:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L665:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L665
	jmp	.L668
.L698:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L663
.L696:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L699:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L660
	.cfi_endproc
.LFE26747:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_.str1.1,"aMS",@progbits,1
.LC6:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_:
.LFB26783:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L810
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	24(%r12), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jb	.L703
	movq	%rdi, %r8
	movq	(%rcx), %xmm0
	subq	%rsi, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L704
	leaq	0(,%rdx,8), %r14
	movq	%rdi, %rdx
	subq	%r14, %rdx
	cmpq	%rdx, %rdi
	je	.L751
	leaq	-8(%rdi), %rax
	movl	$16, %ecx
	subq	%rdx, %rax
	subq	%r14, %rcx
	shrq	$3, %rax
	testq	%rcx, %rcx
	leaq	16(%rdi), %rcx
	setle	%sil
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L752
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L752
	leaq	1(%rax), %r8
	xorl	%eax, %eax
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L707:
	movdqu	(%rdx,%rax), %xmm2
	movups	%xmm2, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L707
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	leaq	(%rdx,%rcx), %rsi
	addq	%rdi, %rcx
	cmpq	%rax, %r8
	je	.L709
	movq	(%rsi), %rax
	movq	%rax, (%rcx)
.L709:
	movq	16(%r12), %rax
.L705:
	addq	%r14, %rax
	movq	%rax, 16(%r12)
	cmpq	%rdx, %rbx
	je	.L710
	subq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%xmm0, -40(%rbp)
	subq	%rdx, %rdi
	call	memmove@PLT
	movq	-40(%rbp), %xmm0
.L710:
	leaq	(%rbx,%r14), %rdx
	cmpq	%rdx, %rbx
	je	.L700
	subq	%rbx, %rdx
	movq	%rbx, %rax
	subq	$8, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rdx, %rdx
	je	.L750
	movq	%rcx, %rdx
	movdqa	%xmm0, %xmm1
	shrq	%rdx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rdx
	addq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L714:
	movups	%xmm1, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L714
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	(%rbx,%rax,8), %r13
	cmpq	%rax, %rcx
	je	.L700
.L750:
	movq	%xmm0, 0(%r13)
.L700:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L810:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L704:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	subq	%rax, %rdx
	je	.L753
	cmpq	$1, %rdx
	je	.L754
	movq	%rdx, %rsi
	movdqa	%xmm0, %xmm1
	xorl	%eax, %eax
	shrq	%rsi
	punpcklqdq	%xmm1, %xmm1
	.p2align 4,,10
	.p2align 3
.L718:
	movq	%rax, %rcx
	addq	$1, %rax
	salq	$4, %rcx
	movups	%xmm1, (%rdi,%rcx)
	cmpq	%rax, %rsi
	jne	.L718
	movq	%rdx, %rcx
	andq	$-2, %rcx
	leaq	(%rdi,%rcx,8), %rax
	cmpq	%rcx, %rdx
	je	.L719
.L717:
	movq	%xmm0, (%rax)
.L719:
	leaq	(%rdi,%rdx,8), %rsi
.L716:
	movq	%rsi, 16(%r12)
	cmpq	%rbx, %rdi
	je	.L720
	subq	%rbx, %rdi
	leaq	15(%rbx), %rcx
	movq	%rbx, %rdx
	leaq	-8(%rdi), %r9
	subq	%rsi, %rcx
	movq	%r9, %rax
	shrq	$3, %rax
	cmpq	$30, %rcx
	jbe	.L755
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L755
	leaq	1(%rax), %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L722:
	movdqu	(%rbx,%rax), %xmm3
	movups	%xmm3, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L722
	movq	%rcx, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rax
	leaq	(%rbx,%rax), %rdi
	addq	%rax, %rsi
	cmpq	%r9, %rcx
	je	.L723
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
.L723:
	addq	%r8, 16(%r12)
	movq	%rcx, %rax
.L749:
	movq	%rax, %rcx
	movdqa	%xmm0, %xmm1
	shrq	%rcx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rcx
	addq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L727:
	movups	%xmm1, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L727
	movq	%rax, %rdx
	andq	$-2, %rdx
	leaq	(%rbx,%rdx,8), %r13
	cmpq	%rax, %rdx
	jne	.L750
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L703:
	movq	8(%r12), %rsi
	movl	$268435455, %r8d
	movq	%r8, %rax
	subq	%rsi, %rdi
	sarq	$3, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L813
	cmpq	%rdi, %rdx
	movq	%rdi, %rax
	cmovnb	%rdx, %rax
	addq	%rax, %rdi
	setc	%al
	subq	%rsi, %r13
	movzbl	%al, %eax
	testq	%rax, %rax
	jne	.L756
	testq	%rdi, %rdi
	jne	.L814
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L732:
	leaq	0(,%rdx,8), %r9
	leaq	(%rax,%r13), %r11
	leaq	0(%r13,%r9), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	leaq	8(%rcx), %rsi
	setnb	%dil
	cmpq	%rsi, %r11
	setnb	%sil
	orb	%sil, %dil
	je	.L758
	leaq	-1(%rdx), %rsi
	cmpq	$3, %rsi
	jbe	.L758
	movq	(%rcx), %xmm0
	leaq	-2(%rdx), %rdi
	xorl	%esi, %esi
	shrq	%rdi
	addq	$1, %rdi
	punpcklqdq	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L736:
	movq	%rsi, %r10
	addq	$1, %rsi
	salq	$4, %r10
	movups	%xmm0, (%r11,%r10)
	cmpq	%rsi, %rdi
	ja	.L736
	leaq	(%rdi,%rdi), %rsi
	salq	$4, %rdi
	addq	%r11, %rdi
	cmpq	%rsi, %rdx
	je	.L738
	movq	(%rcx), %rdx
	movq	%rdx, (%rdi)
.L738:
	movq	8(%r12), %rcx
	cmpq	%rcx, %rbx
	je	.L759
	leaq	-8(%rbx), %r10
	leaq	15(%rcx), %rdx
	subq	%rcx, %r10
	subq	%rax, %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L760
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L760
	leaq	1(%rsi), %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L741:
	movdqu	(%rcx,%rdx), %xmm4
	movups	%xmm4, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L741
	movq	%r11, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %rcx
	addq	%rax, %rsi
	cmpq	%rdx, %r11
	je	.L743
	movq	(%rcx), %rdx
	movq	%rdx, (%rsi)
.L743:
	leaq	8(%rax,%r10), %rdi
.L739:
	movq	16(%r12), %rdx
	leaq	(%rdi,%r9), %rcx
	cmpq	%rdx, %rbx
	je	.L744
	subq	%rbx, %rdx
	leaq	-8(%rdx), %r10
	leaq	16(%rdi,%r9), %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %rbx
	leaq	16(%rbx), %rdx
	setnb	%dil
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %dil
	je	.L761
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L761
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L746:
	movdqu	(%rbx,%rdx), %xmm5
	movups	%xmm5, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L746
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rdi
	addq	%rdi, %rbx
	addq	%rcx, %rdi
	cmpq	%rdx, %rsi
	je	.L748
	movq	(%rbx), %rdx
	movq	%rdx, (%rdi)
.L748:
	leaq	8(%rcx,%r10), %rcx
.L744:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%r8, 24(%r12)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L756:
	.cfi_restore_state
	movl	$2147483640, %esi
	movl	$2147483640, %r14d
.L731:
	movq	(%r12), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L815
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L734:
	leaq	(%rax,%r14), %r8
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L758:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L735:
	movq	(%rcx), %rdi
	movq	%rdi, (%r11,%rsi,8)
	addq	$1, %rsi
	cmpq	%rsi, %rdx
	jne	.L735
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L752:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L706:
	movq	(%rdx,%rcx,8), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rax, %rsi
	jne	.L706
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L755:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L721:
	movq	(%rbx,%rcx,8), %rdi
	movq	%rdi, (%rsi,%rcx,8)
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	%rax, %rdi
	jne	.L721
	addq	%r8, 16(%r12)
	addq	$1, %rax
	testq	%r9, %r9
	je	.L750
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L760:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L740:
	movq	(%rcx,%rdx,8), %rdi
	movq	%rdi, (%rax,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L740
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L761:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L745:
	movq	(%rbx,%rdx,8), %rdi
	movq	%rdi, (%rcx,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L745
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L753:
	movq	%rdi, %rsi
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L751:
	movq	%rdi, %rax
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L720:
	addq	%r8, %rsi
	movq	%rsi, 16(%r12)
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L759:
	movq	%rax, %rdi
	jmp	.L739
.L754:
	movq	%rdi, %rax
	jmp	.L717
.L815:
	movq	%r8, %rdi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	jmp	.L734
.L814:
	cmpq	$268435455, %rdi
	cmova	%r8, %rdi
	leaq	0(,%rdi,8), %r14
	movq	%r14, %rsi
	jmp	.L731
.L813:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE26783:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,"axG",@progbits,_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.type	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, @function
_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_:
.LFB26792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	32(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 4(%rsi)
	movq	8(%rsi), %rax
	je	.L853
	testl	%eax, %eax
	jne	.L830
	movq	%r14, 24(%rsi)
	movq	24(%rdi), %rdx
	movq	%rdx, 16(%rsi)
.L819:
	addq	$1, %rax
	movq	%rax, 8(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L854
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L830:
	.cfi_restore_state
	movl	%eax, %r13d
	movq	16(%r12), %rax
	movq	24(%rsi), %rdi
	movq	(%rax), %r15
	cmpl	$1, %r13d
	je	.L855
	movq	(%r15), %rsi
	movq	%r14, %rdx
	leal	1(%r13), %r14d
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%r12), %rax
	movl	%r14d, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	24(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%rbx), %rdi
	movslq	%r13d, %rax
	movq	24(%r12), %r15
	salq	$3, %rax
	movzbl	23(%rdi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L832
	leaq	32(%rdi,%rax), %rax
	movq	(%rax), %r8
	cmpq	%r8, %r15
	je	.L834
.L833:
	notl	%r13d
	movslq	%r13d, %r13
	leaq	0(%r13,%r13,2), %rdx
	leaq	(%rdi,%rdx,8), %r13
	testq	%r8, %r8
	je	.L835
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %rax
.L835:
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L851
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L851:
	movq	16(%rbx), %rdi
.L834:
	movq	16(%r12), %rax
	movq	24(%rbx), %rdx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%r12), %rax
	movl	%r14d, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L852:
	movq	8(%rbx), %rax
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L853:
	testl	%eax, %eax
	je	.L856
	movq	24(%rsi), %rsi
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L820
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %r15
	cmpq	%rdi, %r14
	je	.L823
.L822:
	leaq	-48(%rsi), %r13
	testq	%rdi, %rdi
	je	.L824
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L824:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L823
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L823:
	movq	16(%rbx), %rsi
	movq	24(%r12), %r13
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L825
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %r14
	cmpq	%rdi, %r13
	je	.L852
.L827:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L828
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L828:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L852
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	8(%rbx), %rax
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L856:
	movq	16(%rdi), %rax
	movq	%r14, %xmm0
	movl	$2, %esi
	leaq	-80(%rbp), %r14
	punpcklqdq	%xmm0, %xmm0
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4LoopEi@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r14, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, 24(%rbx)
	movq	%rax, %r13
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$3, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r13, -64(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%rbx), %r13
	movq	%rax, 16(%rbx)
	movq	%rax, -96(%rbp)
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TerminateEv@PLT
	movq	%r13, %xmm2
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rdx
	movq	16(%r12), %rax
	movq	8(%rax), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	8(%rbx), %rax
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L832:
	movq	32(%rdi), %rdx
	leaq	16(%rdx,%rax), %rax
	movq	(%rax), %r8
	cmpq	%r8, %r15
	je	.L834
	movq	%rdx, %rdi
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L855:
	movq	%r14, %xmm3
	movq	%rdi, %xmm0
	movq	8(%rax), %rdi
	movl	$2, %esi
	punpcklqdq	%xmm3, %xmm0
	leaq	-80(%rbp), %r14
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r14, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, 24(%rbx)
	movq	%rax, %r13
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	movq	16(%rbx), %r12
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movhps	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r15, %rdi
	movq	%r13, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 16(%rbx)
	movq	8(%rbx), %rax
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L820:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L823
	leaq	24(%rsi), %r15
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L825:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L852
	leaq	24(%rsi), %r14
	jmp	.L827
.L854:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26792:
	.size	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, .-_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.section	.text._ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,"axG",@progbits,_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.type	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, @function
_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_:
.LFB25549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rsi, -88(%rbp)
	movq	32(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	4(%rdx), %eax
	movl	$1, %edx
	testl	%eax, %eax
	movq	16(%rdi), %rax
	sete	%sil
	movq	8(%rax), %rdi
	addl	%esi, %esi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%r12, %xmm1
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, 32(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 32(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L862
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L862:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25549:
	.size	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, .-_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.section	.rodata._ZN2v88internal8compiler23EffectControlLinearizer14LowerCheckMapsEPNS1_4NodeES4_.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer14LowerCheckMapsEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer14LowerCheckMapsEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer14LowerCheckMapsEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer14LowerCheckMapsEPNS1_4NodeES4_:
.LFB23285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$312, %rsp
	movq	%rdx, -312(%rbp)
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CheckMapsParametersOfEPKNS1_8OperatorE@PLT
	leaq	32(%r12), %rdx
	movq	%rax, -280(%rbp)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L864
	movq	32(%r12), %rdx
	addq	$16, %rdx
.L864:
	movq	-280(%rbp), %rax
	movq	(%rdx), %r15
	leaq	48(%rbx), %r12
	movq	8(%rax), %rdx
	movl	(%rax), %eax
	movq	%r15, -304(%rbp)
	movq	%rdx, %rcx
	movl	%eax, -288(%rbp)
	andl	$1, %eax
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L865
	testq	%rcx, %rcx
	je	.L866
	movq	14(%rdx), %r13
	subq	6(%rdx), %r13
	sarq	$3, %r13
	movq	%r13, -288(%rbp)
	testl	%eax, %eax
	je	.L867
	leaq	-272(%rbp), %rcx
	movb	$0, -208(%rbp)
	movq	%rcx, %r14
	movq	%rcx, %rdi
	movq	%rcx, -320(%rbp)
	movl	$1, -204(%rbp)
	movq	$0, -200(%rbp)
	movb	$0, -160(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	cmpq	$0, -288(%rbp)
	movq	%rax, %r14
	je	.L904
.L891:
	movq	-288(%rbp), %rax
	xorl	%r13d, %r13d
	subq	$1, %rax
	movq	%rax, -296(%rbp)
	.p2align 4,,10
	.p2align 3
.L874:
	movq	-280(%rbp), %rax
	movq	8(%rax), %rsi
	testb	$3, %sil
	je	.L870
	movq	6(%rsi), %rcx
	movq	14(%rsi), %rdx
	movslq	%r13d, %rax
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L906
	movq	(%rcx,%rax,8), %rsi
.L870:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	%rax, %rsi
	cmpq	%r13, -296(%rbp)
	je	.L907
	leaq	-112(%rbp), %r15
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	leaq	-208(%rbp), %rdx
	movq	%r15, %rcx
	addq	$1, %r13
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, 72(%rbx)
	cmpq	-288(%rbp), %r13
	jb	.L874
.L868:
	movdqa	-144(%rbp), %xmm4
	movq	-320(%rbp), %r13
	movb	$1, -160(%rbp)
	movups	%xmm4, 72(%rbx)
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder15ForMapBitField3Ev@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r13, -320(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$16777216, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	xorl	%r9d, %r9d
	movl	$37, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	-280(%rbp), %rax
	movq	-312(%rbp), %r8
	addq	$16, %rax
	movq	%rax, %rdx
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	(%rbx), %rax
	xorl	%r8d, %r8d
	movl	$96, %ecx
	movl	$1, %edx
	movl	$262, %esi
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$262, %edi
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler18CEntryStubConstantEi@PLT
	movq	%r14, %rsi
	movq	%rax, -344(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r13, -80(%rbp)
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-344(%rbp), %xmm0
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movhps	-304(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movq	80(%rbx), %rax
	movaps	%xmm0, -112(%rbp)
	movq	-336(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movhps	-296(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	5(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, 72(%rbx)
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	xorl	%r9d, %r9d
	movl	$7, %esi
	movq	%r12, %rdi
	movq	-312(%rbp), %r8
	movq	-328(%rbp), %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-320(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-304(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%rax, %r14
	movq	-288(%rbp), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, -304(%rbp)
	testq	%rax, %rax
	je	.L908
	movq	%rbx, -296(%rbp)
	.p2align 4,,10
	.p2align 3
.L875:
	movq	-280(%rbp), %rax
	movq	8(%rax), %rsi
	testb	$3, %sil
	je	.L878
	movq	6(%rsi), %rcx
	movq	14(%rsi), %rdx
	movslq	%r13d, %rax
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L906
	movq	(%rcx,%rax,8), %rsi
.L878:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	%rax, %rsi
	cmpq	%r13, -304(%rbp)
	je	.L909
	leaq	-208(%rbp), %rbx
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movb	$0, -112(%rbp)
	addq	$1, %r13
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movq	-296(%rbp), %rax
	movdqa	-96(%rbp), %xmm3
	movups	%xmm3, 72(%rax)
	cmpq	-288(%rbp), %r13
	jne	.L875
	movq	%rbx, %rdx
	movq	%rax, %rbx
.L881:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-192(%rbp), %xmm5
	movups	%xmm5, 72(%rbx)
.L863:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L910
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L867:
	.cfi_restore_state
	leaq	-272(%rbp), %r14
	movb	$0, -160(%rbp)
	movl	$1, -156(%rbp)
	movq	%r14, %rdi
	movq	$0, -152(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-304(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%rax, %r14
	testq	%r13, %r13
	je	.L911
.L883:
	movq	-288(%rbp), %rax
	xorl	%r13d, %r13d
	subq	$1, %rax
	movq	%rax, -296(%rbp)
	.p2align 4,,10
	.p2align 3
.L890:
	movq	-280(%rbp), %rax
	movq	8(%rax), %rsi
	testb	$3, %sil
	je	.L885
	movq	6(%rsi), %rcx
	movq	14(%rsi), %rdx
	movslq	%r13d, %rax
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L906
	movq	(%rcx,%rax,8), %rsi
.L885:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	%rax, %rsi
	cmpq	%r13, -296(%rbp)
	je	.L912
	leaq	-112(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	leaq	-160(%rbp), %r15
	movl	$1, -108(%rbp)
	addq	$1, %r13
	movq	%r15, %rdx
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movdqa	-96(%rbp), %xmm2
	movups	%xmm2, 72(%rbx)
	cmpq	-288(%rbp), %r13
	jne	.L890
.L888:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm6
	movups	%xmm6, 72(%rbx)
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L912:
	movq	-280(%rbp), %rdx
	xorl	%r9d, %r9d
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	-312(%rbp), %r8
	movl	$37, %esi
	leaq	-160(%rbp), %r15
	addq	$16, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L907:
	leaq	-160(%rbp), %rcx
	leaq	-208(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
.L904:
	leaq	-112(%rbp), %r15
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L909:
	movq	-328(%rbp), %rdx
	xorl	%r9d, %r9d
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	-312(%rbp), %r8
	movl	$37, %esi
	movq	-296(%rbp), %rbx
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	leaq	-208(%rbp), %rdx
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L866:
	testl	%eax, %eax
	je	.L913
	leaq	-272(%rbp), %rax
	movb	$0, -208(%rbp)
	movq	%rax, %rdi
	movq	%rax, %r15
	movq	%rax, -320(%rbp)
	movl	$1, -204(%rbp)
	movq	$0, -200(%rbp)
	movb	$0, -160(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-304(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	$1, -288(%rbp)
	movq	%rax, %r14
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L865:
	testl	%eax, %eax
	jne	.L894
	leaq	-272(%rbp), %r13
	movb	$0, -160(%rbp)
	leaq	-160(%rbp), %r15
	movl	$1, -156(%rbp)
	movq	%r13, %rdi
	movq	$0, -152(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-304(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L913:
	leaq	-272(%rbp), %r13
	movb	$0, -160(%rbp)
	movl	$1, -156(%rbp)
	movq	%r13, %rdi
	movq	$0, -152(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-304(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	$1, -288(%rbp)
	movq	%rax, %r14
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L894:
	leaq	-272(%rbp), %rax
	leaq	-112(%rbp), %r15
	movb	$0, -208(%rbp)
	movq	%rax, %rdi
	movq	%rax, %r14
	movq	%rax, -320(%rbp)
	movl	$1, -204(%rbp)
	movq	$0, -200(%rbp)
	movb	$0, -160(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-304(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	$0, -288(%rbp)
	movq	%rax, %r14
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L908:
	leaq	-208(%rbp), %rdx
	jmp	.L881
.L906:
	movq	%rax, %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L910:
	call	__stack_chk_fail@PLT
.L911:
	leaq	-160(%rbp), %r15
	jmp	.L888
	.cfi_endproc
.LFE23285:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer14LowerCheckMapsEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer14LowerCheckMapsEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32MulEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32MulEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32MulEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32MulEPNS1_4NodeES4_:
.LFB23308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -184(%rbp)
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler20CheckMinusZeroModeOfEPKNS1_8OperatorE@PLT
	movb	%al, -192(%rbp)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L915
	movq	32(%r12), %rbx
	addq	$40, %r12
.L916:
	movq	-184(%rbp), %rax
	movq	(%r12), %r11
	movq	%rbx, %rsi
	leaq	-176(%rbp), %r15
	leaq	48(%rax), %r12
	movq	%r11, %rdx
	movq	%r11, -200(%rbp)
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20Int32MulWithOverflowEPNS1_4NodeES4_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler10ProjectionEiPNS1_4NodeE@PLT
	movq	%r14, %r8
	movq	%r15, %rdx
	movl	$29, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	movl	$1, %r9d
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10ProjectionEiPNS1_4NodeE@PLT
	cmpb	$0, -192(%rbp)
	movq	-200(%rbp), %r11
	movq	%rax, %r13
	je	.L920
.L914:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L921
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L915:
	.cfi_restore_state
	movq	32(%r12), %r12
	movq	16(%r12), %rbx
	addq	$24, %r12
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L920:
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movq	%r11, -208(%rbp)
	movb	$0, -160(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	leaq	-112(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -192(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-208(%rbp), %r11
	movq	-184(%rbp), %rax
	movb	$1, -160(%rbp)
	movq	%r11, %rdx
	movups	%xmm0, 72(%rax)
	call	_ZN2v88internal8compiler14GraphAssembler8Word32OrEPNS1_4NodeES4_@PLT
	movq	-200(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_@PLT
	movq	%r14, %r8
	movq	%r15, %rdx
	movl	$18, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	movl	$1, %r9d
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-192(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-184(%rbp), %rax
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, 72(%rax)
	jmp	.L914
.L921:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23308:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32MulEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32MulEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedUint32BoundsEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedUint32BoundsEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedUint32BoundsEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedUint32BoundsEPNS1_4NodeES4_:
.LFB23313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L923
	leaq	40(%rsi), %rax
.L924:
	movq	(%rax), %rdx
	movq	(%rsi), %rdi
	leaq	48(%rbx), %r15
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal8compiler23CheckBoundsParametersOfEPKNS1_8OperatorE@PLT
	movq	-168(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32LessThanEPNS1_4NodeES4_@PLT
	movl	16(%r12), %edx
	testl	%edx, %edx
	je	.L925
	cmpl	$1, %edx
	jne	.L922
	xorl	%r9d, %r9d
	movq	%r14, %r8
	movq	%rax, %rcx
	movq	%r12, %rdx
	movl	$28, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
.L922:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L929
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L923:
	.cfi_restore_state
	leaq	16(%r13), %rax
	movq	16(%r13), %r13
	addq	$8, %rax
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L925:
	leaq	-112(%rbp), %r12
	movl	$2, %r8d
	movq	%rax, %rsi
	movq	%r15, %rdi
	leaq	-160(%rbp), %rcx
	movq	%r12, %rdx
	movb	$0, -160(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movdqa	-144(%rbp), %xmm0
	movq	%r15, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler11UnreachableEv@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, 72(%rbx)
	jmp	.L922
.L929:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23313:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedUint32BoundsEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedUint32BoundsEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt32ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt32ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt32ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_, @function
_ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt32ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_:
.LFB23319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	48(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movq	%rdi, -176(%rbp)
	movq	%r12, %rdi
	movl	%esi, -168(%rbp)
	movq	%rcx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14GraphAssembler19RoundFloat64ToInt32EPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeInt32ToFloat64EPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	movq	%r15, %r8
	movq	%rbx, %rdx
	movl	$17, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movl	-168(%rbp), %r11d
	testb	%r11b, %r11b
	je	.L934
.L930:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L935
	addq	$152, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L934:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$0, -160(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	leaq	-112(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -184(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-176(%rbp), %rax
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rax)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE@PLT
	movq	-168(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_@PLT
	movq	%r15, %r8
	movq	%rbx, %rdx
	movl	$18, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-184(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-176(%rbp), %rax
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, 72(%rax)
	jmp	.L930
.L935:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23319:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt32ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_, .-_ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt32ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer26LowerCheckedFloat64ToInt32EPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer26LowerCheckedFloat64ToInt32EPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer26LowerCheckedFloat64ToInt32EPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer26LowerCheckedFloat64ToInt32EPNS1_4NodeES4_:
.LFB23320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler26CheckMinusZeroParametersOfEPKNS1_8OperatorE@PLT
	movzbl	23(%rbx), %edx
	leaq	32(%rbx), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L937
	movq	32(%rbx), %rcx
	addq	$16, %rcx
.L937:
	movq	(%rcx), %rcx
	movzbl	(%rax), %esi
	addq	$8, %rsp
	movq	%r13, %r8
	popq	%rbx
	movq	%r12, %rdi
	leaq	8(%rax), %rdx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt32ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_
	.cfi_endproc
.LFE23320:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer26LowerCheckedFloat64ToInt32EPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer26LowerCheckedFloat64ToInt32EPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt64ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt64ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt64ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_, @function
_ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt64ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_:
.LFB23321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	48(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movq	%rdi, -176(%rbp)
	movq	%r12, %rdi
	movl	%esi, -168(%rbp)
	movq	%rcx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14GraphAssembler22TruncateFloat64ToInt64EPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeInt64ToFloat64EPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	movq	%r15, %r8
	movq	%rbx, %rdx
	movl	$17, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movl	-168(%rbp), %r11d
	testb	%r11b, %r11b
	je	.L944
.L940:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L945
	addq	$152, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L944:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$0, -160(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word64EqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	leaq	-112(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -184(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-176(%rbp), %rax
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rax)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE@PLT
	movq	-168(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_@PLT
	movq	%r15, %r8
	movq	%rbx, %rdx
	movl	$18, %esi
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-184(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-176(%rbp), %rax
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, 72(%rax)
	jmp	.L940
.L945:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23321:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt64ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_, .-_ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt64ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer26LowerCheckedFloat64ToInt64EPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer26LowerCheckedFloat64ToInt64EPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer26LowerCheckedFloat64ToInt64EPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer26LowerCheckedFloat64ToInt64EPNS1_4NodeES4_:
.LFB23322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler26CheckMinusZeroParametersOfEPKNS1_8OperatorE@PLT
	movzbl	23(%rbx), %edx
	leaq	32(%rbx), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L947
	movq	32(%rbx), %rcx
	addq	$16, %rcx
.L947:
	movq	(%rcx), %rcx
	movzbl	(%rax), %esi
	addq	$8, %rsp
	movq	%r13, %r8
	popq	%rbx
	movq	%r12, %rdi
	leaq	8(%rax), %rdx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt64ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_
	.cfi_endproc
.LFE23322:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer26LowerCheckedFloat64ToInt64EPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer26LowerCheckedFloat64ToInt64EPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer40BuildCheckedHeapNumberOrOddballToFloat64ENS1_20CheckTaggedInputModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer40BuildCheckedHeapNumberOrOddballToFloat64ENS1_20CheckTaggedInputModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer40BuildCheckedHeapNumberOrOddballToFloat64ENS1_20CheckTaggedInputModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_, @function
_ZN2v88internal8compiler23EffectControlLinearizer40BuildCheckedHeapNumberOrOddballToFloat64ENS1_20CheckTaggedInputModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_:
.LFB23326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	48(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$168, %rsp
	movq	%r8, -200(%rbp)
	movl	%esi, -184(%rbp)
	movq	%rdx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movl	-184(%rbp), %r9d
	movq	%rax, %rcx
	testb	%r9b, %r9b
	je	.L951
	cmpb	$1, %r9b
	jne	.L953
	leaq	-112(%rbp), %r11
	movq	%rax, %rsi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movq	%r11, %rdx
	movq	%r11, -184(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$67, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	-200(%rbp), %r8
	movl	$1, %r9d
	movq	%r12, %rdi
	movq	-192(%rbp), %rdx
	movq	%rax, %rcx
	movl	$24, %esi
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-184(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm0
	movups	%xmm0, 72(%rbx)
.L953:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L956
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L951:
	.cfi_restore_state
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %rdx
	movl	$21, %esi
	movq	%r12, %rdi
	movl	$1, %r9d
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	jmp	.L953
.L956:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23326:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer40BuildCheckedHeapNumberOrOddballToFloat64ENS1_20CheckTaggedInputModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_, .-_ZN2v88internal8compiler23EffectControlLinearizer40BuildCheckedHeapNumberOrOddballToFloat64ENS1_20CheckTaggedInputModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckFloat64HoleEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckFloat64HoleEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckFloat64HoleEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckFloat64HoleEPNS1_4NodeES4_:
.LFB23397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler28CheckFloat64HoleParametersOfEPKNS1_8OperatorE@PLT
	leaq	32(%r12), %rdx
	movq	%rax, -168(%rbp)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L958
	movq	32(%r12), %rdx
	addq	$16, %rdx
.L958:
	movq	(%rdx), %r15
	leaq	48(%rbx), %r12
	leaq	-112(%rbp), %r13
	movb	$0, -112(%rbp)
	movq	%r12, %rdi
	movb	$0, -160(%rbp)
	movq	%r15, %rdx
	movq	%r15, %rsi
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$2, %r8d
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movdqa	-144(%rbp), %xmm0
	movq	%r12, %rdi
	movl	$-524289, %esi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE@PLT
	movq	-176(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r14, %r8
	movl	$6, %esi
	movq	%r12, %rdi
	movq	-168(%rbp), %rdx
	movq	%rax, %rcx
	movl	$1, %r9d
	addq	$8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L962
	addq	$136, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L962:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23397:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckFloat64HoleEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckFloat64HoleEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer34LowerCheckEqualsInternalizedStringEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer34LowerCheckEqualsInternalizedStringEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer34LowerCheckEqualsInternalizedStringEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer34LowerCheckEqualsInternalizedStringEPNS1_4NodeES4_:
.LFB23400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$392, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-8(%rsi), %rax
	movq	%rax, -376(%rbp)
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L965
	movq	-376(%rbp), %rax
	leaq	16(%rax), %rsi
	movq	16(%rax), %rax
	addq	$8, %rsi
	movq	%rax, -376(%rbp)
.L965:
	movq	(%rsi), %r14
	leaq	48(%r15), %r12
	movb	$0, -112(%rbp)
	movq	-376(%rbp), %rsi
	movq	%r12, %rdi
	movb	$0, -256(%rbp)
	movq	%r14, %rdx
	movb	$0, -208(%rbp)
	movl	$1, -252(%rbp)
	movq	$0, -248(%rbp)
	movl	$0, -204(%rbp)
	movq	$0, -200(%rbp)
	movb	$0, -160(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	leaq	-208(%rbp), %rcx
	movl	$2, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-256(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movdqa	-192(%rbp), %xmm2
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$1, -208(%rbp)
	movups	%xmm2, 72(%r15)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-368(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$38, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	movl	$1, %r9d
	movq	$0, -368(%rbp)
	movl	$-1, -360(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, -400(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	-400(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$45, %esi
	movq	%r12, %rdi
	movq	%rax, -400(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-400(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	movq	%r9, -408(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rdx
	movq	%rax, %rsi
	movq	%r11, -400(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$37, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-408(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	-400(%rbp), %r11
	leaq	-112(%rbp), %rcx
	movq	%r12, %rdi
	movl	$2, %r8d
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movdqa	-96(%rbp), %xmm3
	movq	%r12, %rdi
	movb	$1, -112(%rbp)
	movl	$32, %esi
	movups	%xmm3, 72(%r15)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$-32, %esi
	movq	%r12, %rdi
	movq	%rax, -400(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-408(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movq	-400(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$38, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	movl	$1, %r9d
	movq	$0, -368(%rbp)
	movl	$-1, -360(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	(%r15), %rax
	movq	(%rax), %rax
	movq	(%rax), %r8
	movq	16(%r8), %rdx
	movq	24(%r8), %rax
	subq	%rdx, %rax
	cmpq	$7, %rax
	jbe	.L972
	leaq	8(%rdx), %rax
	movq	%rax, 16(%r8)
.L967:
	movl	$1800, %eax
	movl	$329480, (%rdx)
	movw	%ax, 4(%rdx)
	movq	%r8, -424(%rbp)
	movq	%rdx, -416(%rbp)
	call	_ZN2v88internal17ExternalReference31try_internalize_string_functionEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%rax, -400(%rbp)
	movq	(%r15), %rax
	movq	360(%rax), %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	-424(%rbp), %r8
	movq	-416(%rbp), %rdx
	movq	%rax, -408(%rbp)
	movq	16(%r8), %rsi
	movq	24(%r8), %rax
	subq	%rsi, %rax
	cmpq	$23, %rax
	jbe	.L973
	leaq	24(%rsi), %rax
	movq	%rax, 16(%r8)
.L969:
	movdqa	.LC8(%rip), %xmm1
	movq	%rdx, 16(%rsi)
	xorl	%edx, %edx
	movq	-400(%rbp), %xmm0
	movups	%xmm1, (%rsi)
	movq	(%r15), %rax
	movhps	-408(%rbp), %xmm0
	movq	(%rax), %rax
	movaps	%xmm0, -400(%rbp)
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE@PLT
	movq	%rax, %rsi
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r14, -288(%rbp)
	xorl	%r8d, %r8d
	movdqa	-400(%rbp), %xmm0
	movq	%rax, %rsi
	movq	72(%r15), %rax
	leaq	-304(%rbp), %rcx
	movaps	%xmm0, -304(%rbp)
	movq	%rax, -280(%rbp)
	movq	80(%r15), %rax
	movq	%rax, -272(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	3(%rdx,%rax), %edx
	movq	64(%r15), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-376(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, 72(%r15)
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$38, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	movl	$1, %r9d
	movq	$0, -368(%rbp)
	movl	$-1, -360(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm4
	movq	%r13, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm4, 72(%r15)
	call	_ZN2v88internal8compiler13AccessBuilder19ForThinStringActualEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	-376(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	%rbx, %r8
	movq	%r13, %rdx
	movl	$38, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	movl	$1, %r9d
	movq	$0, -368(%rbp)
	movl	$-1, -360(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-240(%rbp), %xmm5
	movups	%xmm5, 72(%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L974
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L972:
	.cfi_restore_state
	movq	%r8, %rdi
	movl	$8, %esi
	movq	%r8, -400(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-400(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L973:
	movl	$24, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-416(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L969
.L974:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23400:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer34LowerCheckEqualsInternalizedStringEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer34LowerCheckEqualsInternalizedStringEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer27LowerTransitionElementsKindEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer27LowerTransitionElementsKindEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer27LowerTransitionElementsKindEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer27LowerTransitionElementsKindEPNS1_4NodeE:
.LFB23424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$296, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler20ElementsTransitionOfEPKNS1_8OperatorE@PLT
	leaq	32(%r12), %rdx
	movq	8(%rax), %rsi
	movq	16(%rax), %r13
	movzbl	(%rax), %ecx
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L976
	movq	32(%r12), %rdx
	addq	$16, %rdx
.L976:
	leaq	48(%rbx), %r12
	movq	(%rdx), %r14
	movb	%cl, -304(%rbp)
	leaq	-288(%rbp), %r15
	movq	%r12, %rdi
	movb	$0, -224(%rbp)
	movl	$0, -220(%rbp)
	movq	$0, -216(%rbp)
	movb	$0, -176(%rbp)
	movl	$1, -172(%rbp)
	movq	$0, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-176(%rbp), %r13
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	-296(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	leaq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-208(%rbp), %xmm1
	movzbl	-304(%rbp), %ecx
	movb	$1, -224(%rbp)
	movups	%xmm1, 72(%rbx)
	testb	%cl, %cl
	je	.L977
	cmpb	$1, %cl
	jne	.L979
	movq	(%rbx), %rax
	xorl	%r8d, %r8d
	movl	$96, %ecx
	movl	$2, %edx
	movl	$10, %esi
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$10, %edi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler18CEntryStubConstantEi@PLT
	movq	%r15, %rsi
	movq	%rax, -328(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r14, %xmm3
	movdqu	72(%rbx), %xmm4
	xorl	%r8d, %r8d
	movq	-328(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-128(%rbp), %rcx
	movaps	%xmm4, -80(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-312(%rbp), %xmm0
	movhps	-320(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-304(%rbp), %xmm0
	movhps	-296(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	24(%rax), %edx
	movl	28(%rax), %eax
	leal	6(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 72(%rbx)
.L979:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-160(%rbp), %xmm2
	movups	%xmm2, 72(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L983
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L977:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-312(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	jmp	.L979
.L983:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23424:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer27LowerTransitionElementsKindEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer27LowerTransitionElementsKindEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer28LowerStoreSignedSmallElementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer28LowerStoreSignedSmallElementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer28LowerStoreSignedSmallElementEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer28LowerStoreSignedSmallElementEPNS1_4NodeE:
.LFB23436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L985
	movq	40(%rsi), %r15
	addq	$48, %rsi
.L986:
	leaq	-224(%rbp), %r13
	movq	(%rsi), %rax
	leaq	48(%rbx), %r12
	movq	%r13, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder15ForMapBitField2Ev@PLT
	movq	-240(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$248, %esi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-240(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-240(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9Word32ShrEPNS1_4NodeES4_@PLT
	movq	%r13, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movb	$0, -160(%rbp)
	movq	%rax, %r14
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-240(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movb	$0, -206(%rbp)
	movq	-232(%rbp), %r8
	orl	$1, %eax
	movl	%eax, %ecx
	movl	$518, %eax
	movw	%ax, -208(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -216(%rbp)
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L991
.L987:
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%r8, -240(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-240(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %r8
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_@PLT
	leaq	-112(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -240(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r12, %rdi
	movq	-232(%rbp), %rsi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeInt32ToFloat64EPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder26ForFixedDoubleArrayElementEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	-232(%rbp), %r8
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_@PLT
	movq	-240(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L992
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L985:
	.cfi_restore_state
	leaq	16(%r14), %rsi
	movq	8(%rsi), %r15
	movq	16(%r14), %r14
	addq	$16, %rsi
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L991:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	%rax, %r8
	jmp	.L987
.L992:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23436:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer28LowerStoreSignedSmallElementEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer28LowerStoreSignedSmallElementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,"axG",@progbits,_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.type	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, @function
_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_:
.LFB25555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	4(%rdx), %edx
	movq	32(%rdi), %r12
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	testl	%edx, %edx
	movl	$1, %edx
	sete	%sil
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%r12, %xmm1
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, 32(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 32(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L996
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L996:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25555:
	.size	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, .-_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckNumberEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckNumberEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckNumberEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckNumberEPNS1_4NodeES4_:
.LFB23287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L998
	movq	16(%r13), %r13
.L998:
	movq	(%rsi), %rdi
	leaq	48(%rbx), %r12
	movq	%r8, -240(%rbp)
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movq	%rax, %r14
	movb	$0, -160(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	-112(%rbp), %r15
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	leaq	-224(%rbp), %rsi
	movb	$1, -160(%rbp)
	movq	%rsi, %rdi
	movq	%rsi, -232(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-232(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	-232(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movl	$21, %esi
	movq	%r12, %rdi
	movq	-240(%rbp), %r8
	movq	%rax, %rcx
	movl	$1, %r9d
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1001
	addq	$200, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1001:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23287:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckNumberEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckNumberEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer36LowerTransitionAndStoreNumberElementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer36LowerTransitionAndStoreNumberElementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer36LowerTransitionAndStoreNumberElementEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer36LowerTransitionAndStoreNumberElementEPNS1_4NodeE:
.LFB23434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	32(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1003
	movq	40(%rsi), %r9
	leaq	48(%rsi), %rax
.L1004:
	movq	(%rax), %r10
	leaq	-224(%rbp), %r13
	leaq	48(%rbx), %r12
	movq	%r9, -248(%rbp)
	movq	%r13, %rdi
	movq	%r10, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder15ForMapBitField2Ev@PLT
	movq	-232(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$248, %esi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-232(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-232(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9Word32ShrEPNS1_4NodeES4_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movq	%rax, -232(%rbp)
	movb	$0, -160(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$0, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-232(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_@PLT
	leaq	-112(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-232(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rdx
	movq	%rax, %rsi
	movq	%r11, -232(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10DebugBreakEv@PLT
	movq	-232(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm0
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	$5, %r8d
	movl	$1, %ecx
	movq	%rbx, %rdi
	movb	$1, -112(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler23EffectControlLinearizer20TransitionElementsToEPNS1_4NodeES4_NS0_12ElementsKindES5_
	movq	-232(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm1
	movq	%r13, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm1, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	-240(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler17Float64SilenceNaNEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13AccessBuilder26ForFixedDoubleArrayElementEv@PLT
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	-248(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1007
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1003:
	.cfi_restore_state
	leaq	16(%r15), %rax
	movq	8(%rax), %r9
	movq	16(%r15), %r15
	addq	$16, %rax
	jmp	.L1004
.L1007:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23434:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer36LowerTransitionAndStoreNumberElementEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer36LowerTransitionAndStoreNumberElementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer39LowerTransitionAndStoreNonNumberElementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer39LowerTransitionAndStoreNonNumberElementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer39LowerTransitionAndStoreNonNumberElementEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer39LowerTransitionAndStoreNonNumberElementEPNS1_4NodeE:
.LFB23435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$280, %rsp
	movq	32(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1009
	movq	40(%rsi), %rax
	movq	%rax, -304(%rbp)
	leaq	48(%rsi), %rax
.L1010:
	leaq	-272(%rbp), %r13
	movq	(%rax), %rax
	leaq	48(%r14), %r12
	movq	%r13, %rdi
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder15ForMapBitField2Ev@PLT
	movq	-312(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$248, %esi
	movq	%r12, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-312(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-312(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9Word32ShrEPNS1_4NodeES4_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movb	$0, -208(%rbp)
	movq	%rax, -312(%rbp)
	movl	$1, -204(%rbp)
	movq	$0, -200(%rbp)
	movb	$0, -160(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -112(%rbp)
	movl	$0, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-312(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-312(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_@PLT
	leaq	-112(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	leaq	-208(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -312(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r14, %rdi
	movl	$3, %r8d
	movdqa	-144(%rbp), %xmm0
	movq	%rbx, %rsi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%r14)
	call	_ZN2v88internal8compiler23EffectControlLinearizer20TransitionElementsToEPNS1_4NodeES4_NS0_12ElementsKindES5_
	movq	-312(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	%r15, %rdx
	movq	%r14, %rdi
	movl	$3, %r8d
	movl	$5, %ecx
	movq	%rbx, %rsi
	movb	$1, -112(%rbp)
	movups	%xmm1, 72(%r14)
	call	_ZN2v88internal8compiler23EffectControlLinearizer20TransitionElementsToEPNS1_4NodeES4_NS0_12ElementsKindES5_
	movq	-312(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-192(%rbp), %xmm2
	movq	%r13, %rdi
	movb	$1, -208(%rbp)
	movups	%xmm2, 72(%r14)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$1, %edx
	movl	$3, %esi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementENS0_12ElementsKindENS0_15LoadSensitivityE@PLT
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler20ValueTypeParameterOfEPKNS1_8OperatorE@PLT
	movq	%rax, -280(%rbp)
	cmpq	$897, %rax
	jne	.L1011
.L1013:
	movq	-280(%rbp), %rax
	movb	$0, -254(%rbp)
	movq	%rax, -264(%rbp)
.L1012:
	movq	-296(%rbp), %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-304(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1016
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1011:
	.cfi_restore_state
	leaq	-280(%rbp), %rdi
	movl	$897, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L1013
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	24(%r15), %rcx
	leaq	16(%r15), %rax
	movq	16(%r15), %r15
	addq	$16, %rax
	movq	%rcx, -304(%rbp)
	jmp	.L1010
.L1016:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23435:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer39LowerTransitionAndStoreNonNumberElementEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer39LowerTransitionAndStoreNonNumberElementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,"axG",@progbits,_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.type	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, @function
_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_:
.LFB26794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	32(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 4(%rsi)
	movq	8(%rsi), %rax
	je	.L1074
	testl	%eax, %eax
	je	.L1075
	movl	%eax, %r15d
	movq	16(%r12), %rax
	movq	24(%rsi), %rdi
	movq	(%rax), %r14
	cmpl	$1, %r15d
	je	.L1076
	movq	(%r14), %rsi
	movslq	%r15d, %r14
	salq	$3, %r14
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%r12), %rax
	leal	1(%r15), %esi
	movl	%esi, -96(%rbp)
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	24(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%rbx), %rdi
	movq	24(%r12), %r8
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1038
	leaq	32(%rdi,%r14), %rdx
	movq	(%rdx), %r9
	cmpq	%r9, %r8
	je	.L1040
.L1039:
	movl	%r15d, %eax
	notl	%eax
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %rsi
	testq	%r9, %r9
	je	.L1041
	movq	%r9, %rdi
	movq	%rdx, -128(%rbp)
	movq	%r8, -120(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %r8
	movq	-112(%rbp), %rsi
.L1041:
	movq	%r8, (%rdx)
	testq	%r8, %r8
	je	.L1071
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1071:
	movq	16(%rbx), %rdi
.L1040:
	movq	16(%r12), %rax
	movq	24(%rbx), %rdx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%r12), %rax
	movl	-96(%rbp), %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	32(%rbx), %rdi
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1043
	leaq	32(%rdi,%r14), %r14
	movq	(%r14), %r8
	cmpq	%r8, %r13
	je	.L1045
.L1044:
	movl	%r15d, %ecx
	notl	%ecx
	movslq	%ecx, %rcx
	leaq	(%rcx,%rcx,2), %rax
	leaq	(%rdi,%rax,8), %rsi
	testq	%r8, %r8
	je	.L1046
	movq	%r8, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-112(%rbp), %rsi
.L1046:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L1072
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1072:
	movq	32(%rbx), %rdi
.L1045:
	movq	16(%r12), %rax
	movq	24(%rbx), %rdx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%r12), %rax
	movzbl	48(%rbx), %esi
	movl	-96(%rbp), %edx
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	32(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L1073:
	movq	8(%rbx), %rax
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1075:
	movq	%rdx, 24(%rsi)
	movq	24(%rdi), %rdx
	movq	%r13, 32(%rsi)
	movq	%rdx, 16(%rsi)
.L1020:
	addq	$1, %rax
	movq	%rax, 8(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1077
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1074:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L1078
	movq	24(%rsi), %rsi
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1021
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %r15
	cmpq	%rdi, %rdx
	je	.L1024
.L1023:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L1025
	movq	%r14, %rsi
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %rdx
.L1025:
	movq	%rdx, (%r15)
	testq	%rdx, %rdx
	je	.L1024
	movq	%r14, %rsi
	movq	%rdx, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1024:
	movq	16(%rbx), %rsi
	movq	24(%r12), %r14
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1026
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %r15
	cmpq	%rdi, %r14
	je	.L1029
.L1028:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L1030
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1030:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L1029
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1029:
	movq	32(%rbx), %rsi
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1031
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %r14
	cmpq	%rdi, %r13
	je	.L1073
.L1033:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L1034
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1034:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L1073
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	8(%rbx), %rax
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1078:
	movq	16(%rdi), %rax
	movq	%rdx, %xmm0
	movq	%r13, %xmm1
	movl	$2, %esi
	punpcklqdq	%xmm1, %xmm1
	punpcklqdq	%xmm0, %xmm0
	leaq	-80(%rbp), %r13
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4LoopEi@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r13, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, 24(%rbx)
	movq	%rax, %r14
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r14, -64(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%rbx), %r14
	movq	%rax, 16(%rbx)
	movq	%rax, -96(%rbp)
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TerminateEv@PLT
	movq	%r14, %xmm3
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rdx
	movq	16(%r12), %rax
	movq	8(%rax), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	16(%r12), %rax
	movzbl	48(%rbx), %esi
	movl	$2, %edx
	movq	24(%rbx), %r14
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movdqa	-112(%rbp), %xmm1
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, %rdi
	movq	%r14, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 32(%rbx)
	movq	8(%rbx), %rax
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1038:
	movq	32(%rdi), %rax
	leaq	16(%rax,%r14), %rdx
	movq	(%rdx), %r9
	cmpq	%r9, %r8
	je	.L1040
	movq	%rax, %rdi
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1076:
	movq	%rdx, %xmm4
	movq	%rdi, %xmm0
	movq	8(%rax), %rdi
	leaq	-80(%rbp), %r15
	punpcklqdq	%xmm4, %xmm0
	movl	$2, %esi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r15, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, 24(%rbx)
	movq	24(%r12), %rcx
	movq	%rax, -120(%rbp)
	movq	16(%r12), %rax
	movq	%rcx, -96(%rbp)
	movq	16(%rbx), %rcx
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	-120(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rcx
	movq	%rdx, -64(%rbp)
	movl	$3, %edx
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	32(%rbx), %rcx
	movzbl	48(%rbx), %esi
	movl	$2, %edx
	movq	%rax, 16(%rbx)
	movq	16(%r12), %rax
	movq	%rcx, -96(%rbp)
	movq	24(%rbx), %r12
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r13, %xmm5
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r12, -64(%rbp)
	movl	$3, %edx
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 32(%rbx)
	movq	8(%rbx), %rax
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	32(%rdi), %rax
	leaq	16(%rax,%r14), %r14
	movq	(%r14), %r8
	cmpq	%r8, %r13
	je	.L1045
	movq	%rax, %rdi
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L1073
	leaq	24(%rsi), %r14
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1026:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L1029
	leaq	24(%rsi), %r15
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1021:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %rdx
	je	.L1024
	leaq	24(%rsi), %r15
	jmp	.L1023
.L1077:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26794:
	.size	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, .-_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer26LowerChangeFloat64ToTaggedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer26LowerChangeFloat64ToTaggedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer26LowerChangeFloat64ToTaggedEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer26LowerChangeFloat64ToTaggedEPNS1_4NodeE:
.LFB23260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$344, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler20CheckMinusZeroModeOfEPKNS1_8OperatorE@PLT
	leaq	32(%r12), %rdx
	movl	%eax, %r15d
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1080
	movq	32(%r12), %rdx
	addq	$16, %rdx
.L1080:
	movq	(%rdx), %r14
	leaq	48(%rbx), %r12
	movb	$0, -112(%rbp)
	movq	%r12, %rdi
	movl	$1, -108(%rbp)
	movq	%r14, %rsi
	movq	$0, -104(%rbp)
	movb	$8, -64(%rbp)
	movb	$0, -304(%rbp)
	movl	$0, -300(%rbp)
	movq	$0, -296(%rbp)
	movb	$0, -256(%rbp)
	movl	$1, -252(%rbp)
	movq	$0, -248(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler19RoundFloat64ToInt32EPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeInt32ToFloat64EPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	leaq	-256(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	leaq	-304(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%r8, -376(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-240(%rbp), %xmm0
	testb	%r15b, %r15b
	movb	$1, -256(%rbp)
	movq	-376(%rbp), %r8
	movups	%xmm0, 72(%rbx)
	je	.L1086
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1087
.L1082:
	movl	$32, %esi
	movq	%r12, %rdi
	leaq	-368(%rbp), %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	leaq	-112(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%rax, %rdx
	movq	%r8, -384(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-288(%rbp), %xmm1
	movq	%r12, %rdi
	movl	$16, %esi
	movb	$1, -304(%rbp)
	movups	%xmm1, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	%r15, %rdi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-376(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	-384(%rbp), %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm2
	movq	-80(%rbp), %rax
	movups	%xmm2, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1088
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1086:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r8, -384(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$0, -208(%rbp)
	movq	%rax, %rdx
	movq	%rax, %r15
	movl	$0, -204(%rbp)
	movq	$0, -200(%rbp)
	movb	$0, -160(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	leaq	-208(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	leaq	-160(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -376(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-192(%rbp), %xmm3
	movq	%r14, %rsi
	movq	%r12, %rdi
	movb	$1, -208(%rbp)
	movups	%xmm3, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_@PLT
	movq	-384(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-376(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm4
	movq	(%rbx), %rax
	movups	%xmm4, 72(%rbx)
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	jne	.L1082
.L1087:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	%rax, %r13
	jmp	.L1082
.L1088:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23260:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer26LowerChangeFloat64ToTaggedEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer26LowerChangeFloat64ToTaggedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer22LowerChangeBitToTaggedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer22LowerChangeBitToTaggedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer22LowerChangeBitToTaggedEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer22LowerChangeBitToTaggedEPNS1_4NodeE:
.LFB23262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	32(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1090
	movq	16(%r8), %r8
.L1090:
	leaq	48(%rbx), %r12
	movq	%r8, %rsi
	movb	$0, -96(%rbp)
	leaq	-96(%rbp), %r13
	leaq	-144(%rbp), %rdx
	movq	%r12, %rdi
	movb	$0, -144(%rbp)
	movl	$1, -140(%rbp)
	movq	$0, -136(%rbp)
	movl	$1, -92(%rbp)
	movq	$0, -88(%rbp)
	movb	$8, -48(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13FalseConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-128(%rbp), %xmm0
	movq	%r12, %rdi
	movb	$1, -144(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler12TrueConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-80(%rbp), %xmm1
	movq	-64(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1093
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1093:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23262:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer22LowerChangeBitToTaggedEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer22LowerChangeBitToTaggedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeInt64ToTaggedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeInt64ToTaggedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeInt64ToTaggedEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeInt64ToTaggedEPNS1_4NodeE:
.LFB23266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1095
	movq	16(%r13), %r13
.L1095:
	leaq	48(%rbx), %r12
	movq	%r13, %rsi
	movb	$0, -160(%rbp)
	leaq	-224(%rbp), %r14
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$8, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word64EqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	leaq	-112(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%rax, %rdx
	movq	%r8, -240(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeInt64ToFloat64EPNS1_4NodeE@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	-232(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	-240(%rbp), %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1098
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1098:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23266:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeInt64ToTaggedEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeInt64ToTaggedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint32ToTaggedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint32ToTaggedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint32ToTaggedEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint32ToTaggedEPNS1_4NodeE:
.LFB23267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1100
	movq	16(%r13), %r13
.L1100:
	leaq	48(%rbx), %r12
	movl	$2147483647, %esi
	movb	$0, -160(%rbp)
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$8, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler21Uint32LessThanOrEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1105
.L1101:
	movq	%r12, %rdi
	leaq	-224(%rbp), %r14
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	leaq	-112(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%rax, %rdx
	movq	%r8, -240(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler21ChangeUint32ToFloat64EPNS1_4NodeE@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	-232(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	-240(%rbp), %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1106
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1105:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rdx, -232(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeUint32ToUint64EPNS1_4NodeE@PLT
	movq	-232(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L1101
.L1106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23267:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint32ToTaggedEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint32ToTaggedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint64ToTaggedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint64ToTaggedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint64ToTaggedEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint64ToTaggedEPNS1_4NodeE:
.LFB23268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1108
	movq	16(%r13), %r13
.L1108:
	leaq	48(%rbx), %r12
	movl	$2147483647, %esi
	movb	$0, -160(%rbp)
	leaq	-224(%rbp), %r14
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$8, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler21Uint64LessThanOrEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	leaq	-112(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%rax, %rdx
	movq	%r8, -240(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeInt64ToFloat64EPNS1_4NodeE@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	-232(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	-240(%rbp), %r8
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1111
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1111:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23268:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint64ToTaggedEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint64ToTaggedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeTaggedToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeTaggedToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeTaggedToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeTaggedToInt32EPNS1_4NodeE:
.LFB23276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1113
	movq	16(%r13), %r13
.L1113:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -160(%rbp)
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$4, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	leaq	-160(%rbp), %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movl	$32, %esi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1118
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%rax, %rdx
.L1115:
	leaq	-112(%rbp), %r14
	movq	%r12, %rdi
	leaq	-224(%rbp), %r15
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r15, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt32EPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1119
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1118:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%rax, %rdx
	jmp	.L1115
.L1119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23276:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeTaggedToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeTaggedToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeTaggedToUint32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeTaggedToUint32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeTaggedToUint32EPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeTaggedToUint32EPNS1_4NodeE:
.LFB23277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1121
	movq	16(%r13), %r13
.L1121:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -160(%rbp)
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$4, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	leaq	-160(%rbp), %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movl	$32, %esi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1126
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%rax, %rdx
.L1123:
	leaq	-112(%rbp), %r14
	movq	%r12, %rdi
	leaq	-224(%rbp), %r15
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r15, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler21ChangeFloat64ToUint32EPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1127
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1126:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%rax, %rdx
	jmp	.L1123
.L1127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23277:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeTaggedToUint32EPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeTaggedToUint32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeTaggedToInt64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeTaggedToInt64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeTaggedToInt64EPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeTaggedToInt64EPNS1_4NodeE:
.LFB23278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1129
	movq	16(%r13), %r13
.L1129:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -160(%rbp)
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$5, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	jne	.L1133
	movl	$32, %esi
	movq	%r12, %rdi
	leaq	-112(%rbp), %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-224(%rbp), %r15
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r15, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt64EPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1134
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1133:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23278:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeTaggedToInt64EPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeTaggedToInt64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer31LowerChangeTaggedToTaggedSignedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer31LowerChangeTaggedToTaggedSignedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer31LowerChangeTaggedToTaggedSignedEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer31LowerChangeTaggedToTaggedSignedEPNS1_4NodeE:
.LFB23280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1136
	movq	16(%r13), %r13
.L1136:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -160(%rbp)
	leaq	-224(%rbp), %r15
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$4, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	-112(%rbp), %r14
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r15, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt32EPNS1_4NodeE@PLT
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1140
.L1137:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1141
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1140:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	%rax, %r13
	jmp	.L1137
.L1141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23280:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer31LowerChangeTaggedToTaggedSignedEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer31LowerChangeTaggedToTaggedSignedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer35LowerChangeCompressedToTaggedSignedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer35LowerChangeCompressedToTaggedSignedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer35LowerChangeCompressedToTaggedSignedEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer35LowerChangeCompressedToTaggedSignedEPNS1_4NodeE:
.LFB23281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1143
	movq	16(%r13), %r13
.L1143:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -160(%rbp)
	leaq	-224(%rbp), %r15
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$4, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	-112(%rbp), %r14
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler36ChangeCompressedSignedToTaggedSignedEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler24ChangeCompressedToTaggedEPNS1_4NodeE@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt32EPNS1_4NodeE@PLT
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1147
.L1144:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1148
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1147:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	%rax, %r13
	jmp	.L1144
.L1148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23281:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer35LowerChangeCompressedToTaggedSignedEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer35LowerChangeCompressedToTaggedSignedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer35LowerChangeTaggedToCompressedSignedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer35LowerChangeTaggedToCompressedSignedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer35LowerChangeTaggedToCompressedSignedEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer35LowerChangeTaggedToCompressedSignedEPNS1_4NodeE:
.LFB23282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-208(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	leaq	48(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$176, %rsp
	movzbl	23(%rsi), %ecx
	movq	32(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	32(%rsi), %rax
	andl	$15, %ecx
	addq	$16, %rdx
	cmpl	$15, %ecx
	cmove	%rdx, %rax
	movq	(%rax), %r13
	movb	$0, -144(%rbp)
	movl	$0, -140(%rbp)
	movq	%r13, %rsi
	movb	$0, -96(%rbp)
	movq	$0, -136(%rbp)
	movl	$1, -92(%rbp)
	movq	$0, -88(%rbp)
	movb	$4, -48(%rbp)
	call	_ZN2v88internal8compiler23EffectControlLinearizer11ObjectIsSmiEPNS1_4NodeE
	leaq	-144(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler36ChangeTaggedSignedToCompressedSignedEPNS1_4NodeE@PLT
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-128(%rbp), %xmm0
	movq	%r14, %rdi
	movb	$1, -144(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt32EPNS1_4NodeE@PLT
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23282:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer35LowerChangeTaggedToCompressedSignedEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer35LowerChangeTaggedToCompressedSignedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer28LowerTruncateTaggedToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer28LowerTruncateTaggedToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer28LowerTruncateTaggedToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer28LowerTruncateTaggedToFloat64EPNS1_4NodeE:
.LFB23283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1154
	movq	16(%r13), %r13
.L1154:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -160(%rbp)
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$13, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	leaq	-160(%rbp), %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movl	$32, %esi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1159
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%rax, %rsi
.L1156:
	movq	%r12, %rdi
	leaq	-112(%rbp), %r14
	leaq	-224(%rbp), %r15
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeInt32ToFloat64EPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r15, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1160
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1159:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%rax, %rsi
	jmp	.L1156
.L1160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23283:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer28LowerTruncateTaggedToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer28LowerTruncateTaggedToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer26LowerChangeTaggedToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer26LowerChangeTaggedToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer26LowerChangeTaggedToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer26LowerChangeTaggedToFloat64EPNS1_4NodeE:
.LFB23279:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler23EffectControlLinearizer28LowerTruncateTaggedToFloat64EPNS1_4NodeE
	.cfi_endproc
.LFE23279:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer26LowerChangeTaggedToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer26LowerChangeTaggedToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer16LowerCompareMapsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCompareMapsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCompareMapsEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer16LowerCompareMapsEPNS1_4NodeE:
.LFB23286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$264, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler23CompareMapsParametersOfEPKNS1_8OperatorE@PLT
	movq	$0, -280(%rbp)
	movq	%rax, -288(%rbp)
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L1163
	movq	$1, -280(%rbp)
	testq	%rdx, %rdx
	je	.L1163
	movq	14(%rax), %rdx
	subq	6(%rax), %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	movq	%rax, -280(%rbp)
.L1163:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1164
	movq	16(%r15), %r15
.L1164:
	leaq	-272(%rbp), %r14
	leaq	48(%rbx), %r12
	movb	$0, -112(%rbp)
	movq	%r14, %rdi
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$1, -64(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	xorl	%r14d, %r14d
	cmpq	$0, -280(%rbp)
	leaq	-208(%rbp), %r15
	movq	%rax, -296(%rbp)
	je	.L1179
	.p2align 4,,10
	.p2align 3
.L1165:
	movq	-288(%rbp), %rax
	movq	(%rax), %rsi
	testb	$3, %sil
	je	.L1167
	movq	6(%rsi), %rax
	movq	14(%rsi), %rdx
	movslq	%r14d, %r8
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r8
	jnb	.L1180
	movq	(%rax,%r8,8), %rsi
.L1167:
	movq	%r12, %rdi
	leaq	-112(%rbp), %r13
	addq	$1, %r14
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-296(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-160(%rbp), %rdx
	movb	$0, -208(%rbp)
	movl	$1, -204(%rbp)
	movq	$0, -200(%rbp)
	movb	$0, -160(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movdqa	-144(%rbp), %xmm0
	movq	%r12, %rdi
	movl	$1, %esi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-192(%rbp), %xmm1
	movups	%xmm1, 72(%rbx)
	cmpq	-280(%rbp), %r14
	jne	.L1165
.L1169:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm2
	movq	-80(%rbp), %rax
	movups	%xmm2, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1181
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1179:
	.cfi_restore_state
	leaq	-112(%rbp), %r13
	jmp	.L1169
.L1180:
	movq	%r8, %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1181:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23286:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCompareMapsEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer16LowerCompareMapsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32DivEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32DivEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32DivEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32DivEPNS1_4NodeES4_:
.LFB23303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$344, %rsp
	movq	-8(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1184
	leaq	16(%r14), %rsi
	movq	16(%r14), %r14
	addq	$8, %rsi
.L1184:
	leaq	48(%rbx), %r12
	movq	(%rsi), %r13
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%rax, -344(%rbp)
	movq	0(%r13), %rax
	cmpw	$23, 16(%rax)
	je	.L1210
.L1185:
	movq	-344(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movb	$0, -304(%rbp)
	movl	$1, -300(%rbp)
	movq	$0, -296(%rbp)
	movb	$0, -256(%rbp)
	movl	$0, -252(%rbp)
	movq	$0, -248(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$4, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_@PLT
	leaq	-256(%rbp), %rcx
	movl	$2, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-304(%rbp), %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movdqa	-288(%rbp), %xmm1
	movb	$1, -304(%rbp)
	movups	%xmm1, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler8Int32DivEPNS1_4NodeES4_@PLT
	leaq	-112(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%rax, %rdx
	movq	%r11, -368(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-240(%rbp), %xmm2
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-344(%rbp), %rdx
	movb	$1, -256(%rbp)
	movups	%xmm2, 72(%rbx)
	movb	$0, -208(%rbp)
	movl	$0, -204(%rbp)
	movq	$0, -200(%rbp)
	movb	$0, -160(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r15, %r8
	movl	$5, %esi
	movq	%r12, %rdi
	leaq	-336(%rbp), %r10
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	$0, -336(%rbp)
	movq	%r10, %rdx
	movq	%r10, -352(%rbp)
	movl	$-1, -328(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-344(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r15, %r8
	movl	$18, %esi
	movq	%r12, %rdi
	movq	-352(%rbp), %r10
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	$0, -336(%rbp)
	movl	$-1, -328(%rbp)
	movq	%r10, %rdx
	movq	%r10, -360(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	(%rbx), %rax
	movl	$-2147483648, %esi
	movq	%r12, %rdi
	movq	(%rax), %r9
	movq	%r9, -352(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%rax, -344(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	-352(%rbp), %r9
	movq	%r14, %xmm0
	xorl	%r8d, %r8d
	movhps	-344(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r9, %rdi
	leaq	-320(%rbp), %rcx
	movaps	%xmm0, -320(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	leaq	-160(%rbp), %rcx
	movl	$2, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-208(%rbp), %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movdqa	-192(%rbp), %xmm3
	movq	%r12, %rdi
	movl	$-1, %esi
	movb	$1, -208(%rbp)
	movups	%xmm3, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r15, %r8
	movl	$29, %esi
	movq	%r12, %rdi
	movq	-360(%rbp), %r10
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	$0, -336(%rbp)
	movl	$-1, -328(%rbp)
	movq	%r10, %rdx
	movq	%r10, -352(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler8Int32DivEPNS1_4NodeES4_@PLT
	movq	-368(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	movq	%r11, -344(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movdqa	-144(%rbp), %xmm4
	movb	$1, -160(%rbp)
	movups	%xmm4, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler8Int32DivEPNS1_4NodeES4_@PLT
	movq	-344(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rdx
	movq	%r12, %rdi
	movb	$1, -112(%rbp)
	movups	%xmm5, 72(%rbx)
	movq	-80(%rbp), %rbx
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler8Int32MulEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r15, %r8
	movl	$16, %esi
	movq	%r12, %rdi
	movq	-352(%rbp), %r10
	movq	%rax, %rcx
	movl	$1, %r9d
	movq	$0, -336(%rbp)
	movl	$-1, -328(%rbp)
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
.L1182:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1211
	addq	$344, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1210:
	.cfi_restore_state
	movl	44(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L1185
	leal	-1(%rcx), %esi
	movl	%ecx, -360(%rbp)
	movl	%esi, %r8d
	andl	%ecx, %r8d
	movl	%r8d, -352(%rbp)
	jne	.L1185
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	-360(%rbp), %ecx
	movl	-352(%rbp), %r8d
	movq	%rax, %r13
	cmpl	$65535, %ecx
	jle	.L1198
	movl	$31, -380(%rbp)
	sarl	$16, %ecx
	movl	$23, %ebx
	movl	$22, %r10d
	movl	$30, -384(%rbp)
	movl	$21, %r9d
	movl	$19, %edx
	movl	$18, %esi
	movl	$29, -352(%rbp)
	movl	$17, %eax
	movl	$20, %edi
	movl	$24, %r11d
	movl	$27, -376(%rbp)
	movl	$16, %r8d
	movl	$26, -372(%rbp)
	movl	$25, -360(%rbp)
	movl	$28, -368(%rbp)
.L1186:
	cmpl	$255, %ecx
	jle	.L1187
	movl	-380(%rbp), %ebx
	movl	-384(%rbp), %r10d
	sarl	$8, %ecx
	movl	%r11d, %r8d
	movl	-352(%rbp), %r9d
	movl	-376(%rbp), %edx
	movl	-372(%rbp), %esi
	movl	-360(%rbp), %eax
	movl	-368(%rbp), %edi
.L1187:
	cmpl	$15, %ecx
	jle	.L1188
	sarl	$4, %ecx
	movl	%ebx, %edx
	movl	%r10d, %esi
	movl	%r9d, %eax
	movl	%edi, %r8d
.L1188:
	cmpl	$4, %ecx
	je	.L1199
	jg	.L1190
	cmpl	$1, %ecx
	je	.L1191
	cmpl	$2, %ecx
	jne	.L1193
.L1192:
	movl	%eax, %r8d
.L1191:
	movl	%r8d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movq	-344(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r15, %r8
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	-336(%rbp), %rdx
	movl	$1, %r9d
	movq	$0, -336(%rbp)
	movl	$-1, -328(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9Word32SarEPNS1_4NodeES4_@PLT
	movq	%rax, %rbx
	jmp	.L1182
	.p2align 4,,10
	.p2align 3
.L1190:
	cmpl	$8, %ecx
	jne	.L1193
	movl	%edx, %eax
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1198:
	movl	$15, -380(%rbp)
	movl	$7, %ebx
	movl	$6, %r10d
	movl	$5, %r9d
	movl	$14, -384(%rbp)
	movl	$3, %edx
	movl	$2, %esi
	movl	$1, %eax
	movl	$13, -352(%rbp)
	movl	$4, %edi
	movl	$8, %r11d
	movl	$11, -376(%rbp)
	movl	$10, -372(%rbp)
	movl	$9, -360(%rbp)
	movl	$12, -368(%rbp)
	jmp	.L1186
.L1193:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1199:
	movl	%esi, %edx
	movl	%edx, %eax
	jmp	.L1192
.L1211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23303:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32DivEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32DivEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer14BuildUint32ModEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer14BuildUint32ModEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer14BuildUint32ModEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer14BuildUint32ModEPNS1_4NodeES4_:
.LFB23304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$1, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	48(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, -160(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$4, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8Int32SubEPNS1_4NodeES4_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movq	-168(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9Uint32ModEPNS1_4NodeES4_@PLT
	leaq	-112(%rbp), %r13
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movdqa	-144(%rbp), %xmm0
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1215
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1215:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23304:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer14BuildUint32ModEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer14BuildUint32ModEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckedUint32ModEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckedUint32ModEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckedUint32ModEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckedUint32ModEPNS1_4NodeES4_:
.LFB23307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	-8(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1218
	leaq	16(%r14), %rsi
	movq	16(%r14), %r14
	addq	$8, %rsi
.L1218:
	leaq	48(%r12), %r13
	movq	(%rsi), %r15
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%rbx, %r8
	leaq	-80(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movl	$1, %r9d
	movl	$5, %esi
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer14BuildUint32ModEPNS1_4NodeES4_
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1221
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1221:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23307:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckedUint32ModEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckedUint32ModEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32ModEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32ModEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32ModEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32ModEPNS1_4NodeES4_:
.LFB23305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$280, %rsp
	movq	-8(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1224
	leaq	16(%r14), %rsi
	movq	16(%r14), %r14
	addq	$8, %rsi
.L1224:
	leaq	48(%rbx), %r12
	movq	(%rsi), %r15
	xorl	%esi, %esi
	movq	%r8, -304(%rbp)
	movq	%r12, %rdi
	movb	$4, -128(%rbp)
	movb	$0, -272(%rbp)
	movl	$0, -268(%rbp)
	movq	$0, -264(%rbp)
	movb	$0, -224(%rbp)
	movl	$0, -220(%rbp)
	movq	$0, -216(%rbp)
	movb	$0, -176(%rbp)
	movl	$1, -172(%rbp)
	movq	$0, -168(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$4, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler20Int32LessThanOrEqualEPNS1_4NodeES4_@PLT
	leaq	-272(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	leaq	-176(%rbp), %r11
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -296(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movdqa	-256(%rbp), %xmm0
	movb	$1, -272(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler8Int32SubEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	-304(%rbp), %r8
	movl	$1, %r9d
	movq	%r12, %rdi
	leaq	-288(%rbp), %r10
	movq	%rax, %rcx
	movl	$5, %esi
	movq	$0, -288(%rbp)
	movq	%r10, %rdx
	movq	%r8, -312(%rbp)
	movq	%r10, -304(%rbp)
	movl	$-1, -280(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	-296(%rbp), %r11
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	-112(%rbp), %r15
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movdqa	-160(%rbp), %xmm1
	movq	-144(%rbp), %r9
	movb	$1, -176(%rbp)
	movups	%xmm1, 72(%rbx)
	movq	%r9, -296(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_@PLT
	leaq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-296(%rbp), %r9
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler23EffectControlLinearizer14BuildUint32ModEPNS1_4NodeES4_
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movdqa	-208(%rbp), %xmm2
	movb	$1, -224(%rbp)
	movups	%xmm2, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler8Int32SubEPNS1_4NodeES4_@PLT
	movq	-296(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Uint32ModEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	-304(%rbp), %r10
	movl	$1, %r9d
	movq	%r12, %rdi
	movq	-312(%rbp), %r8
	movq	%rax, %rcx
	movl	$18, %esi
	movq	$0, -288(%rbp)
	movl	$-1, -280(%rbp)
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler8Int32SubEPNS1_4NodeES4_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm3
	movq	-80(%rbp), %rax
	movups	%xmm3, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1227
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1227:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23305:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32ModEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32ModEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedTaggedToInt32EPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedTaggedToInt32EPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedTaggedToInt32EPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedTaggedToInt32EPNS1_4NodeES4_:
.LFB23324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler26CheckMinusZeroParametersOfEPKNS1_8OperatorE@PLT
	leaq	32(%r12), %rdx
	movq	%rax, -232(%rbp)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1229
	movq	32(%r12), %rdx
	addq	$16, %rdx
.L1229:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movq	(%rdx), %r15
	movb	$0, -160(%rbp)
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$4, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	leaq	-160(%rbp), %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movl	$32, %esi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1235
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%rax, %rdx
.L1231:
	leaq	-112(%rbp), %r10
	movq	%r12, %rdi
	leaq	-224(%rbp), %r14
	movq	%r10, %rsi
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r14, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	%r13, %r8
	movl	$21, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	-232(%rbp), %rax
	movl	$1, %r9d
	leaq	8(%rax), %r11
	movq	%r11, %rdx
	movq	%r11, -240(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	-240(%rbp), %r11
	movq	%r13, %r8
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	-232(%rbp), %rax
	movq	%r11, %rdx
	movzbl	(%rax), %esi
	call	_ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt32ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_
	movq	-248(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1236
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1235:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%rax, %rdx
	jmp	.L1231
.L1236:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23324:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedTaggedToInt32EPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedTaggedToInt32EPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedTaggedToInt64EPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedTaggedToInt64EPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedTaggedToInt64EPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedTaggedToInt64EPNS1_4NodeES4_:
.LFB23325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler26CheckMinusZeroParametersOfEPKNS1_8OperatorE@PLT
	leaq	32(%r12), %rdx
	movq	%rax, -232(%rbp)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1238
	movq	32(%r12), %rdx
	addq	$16, %rdx
.L1238:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movq	(%rdx), %r14
	movb	$0, -160(%rbp)
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$5, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	jne	.L1243
	movl	$32, %esi
	movq	%r12, %rdi
	leaq	-224(%rbp), %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	leaq	-112(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%rax, %rdx
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r15, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	%r13, %r8
	movl	$21, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	-232(%rbp), %rax
	movl	$1, %r9d
	leaq	8(%rax), %r11
	movq	%r11, %rdx
	movq	%r11, -240(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	-240(%rbp), %r11
	movq	%r13, %r8
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	-232(%rbp), %rax
	movq	%r11, %rdx
	movzbl	(%rax), %esi
	call	_ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt64ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_
	movq	-248(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1244
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1243:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1244:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23325:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedTaggedToInt64EPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedTaggedToInt64EPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer27LowerCheckedTaggedToFloat64EPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer27LowerCheckedTaggedToFloat64EPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer27LowerCheckedTaggedToFloat64EPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer27LowerCheckedTaggedToFloat64EPNS1_4NodeES4_:
.LFB23327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler28CheckTaggedInputParametersOfEPKNS1_8OperatorE@PLT
	leaq	32(%r12), %rdx
	movq	%rax, %r13
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1246
	movq	32(%r12), %rdx
	addq	$16, %rdx
.L1246:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movq	(%rdx), %r15
	movb	$0, -160(%rbp)
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$13, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	-168(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movzbl	0(%r13), %esi
	movq	%r14, %r8
	movq	%r15, %rcx
	leaq	8(%r13), %rdx
	movq	%rbx, %rdi
	leaq	-112(%rbp), %r13
	call	_ZN2v88internal8compiler23EffectControlLinearizer40BuildCheckedHeapNumberOrOddballToFloat64ENS1_20CheckTaggedInputModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movl	$32, %esi
	movq	%r12, %rdi
	movdqa	-144(%rbp), %xmm0
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1252
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%rax, %rsi
.L1248:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeInt32ToFloat64EPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1253
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1252:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%rax, %rsi
	jmp	.L1248
.L1253:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23327:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer27LowerCheckedTaggedToFloat64EPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer27LowerCheckedTaggedToFloat64EPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint64ToBigIntEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint64ToBigIntEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint64ToBigIntEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint64ToBigIntEPNS1_4NodeE:
.LFB23332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$248, %rsp
	movq	32(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1255
	movq	16(%r8), %r8
.L1255:
	movq	(%rbx), %rdi
	leaq	48(%rbx), %r12
	movq	%r8, -280(%rbp)
	leaq	-272(%rbp), %r13
	movq	360(%rdi), %rax
	leaq	448(%rax), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movq	%rax, %r15
	movb	$8, -64(%rbp)
	movb	$0, -208(%rbp)
	movl	$1, -204(%rbp)
	movq	$0, -200(%rbp)
	movb	$0, -160(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-280(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	movq	%r8, -288(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler11Word64EqualEPNS1_4NodeES4_@PLT
	leaq	-208(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r12, %rdi
	movl	$24, %esi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder17ForBigIntBitfieldEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-280(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder24ForBigIntOptionalPaddingEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-280(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder32ForBigIntLeastSignificantDigit64Ev@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-288(%rbp), %r8
	movq	%r8, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	leaq	-112(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%r8, -280(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-192(%rbp), %xmm1
	movq	%r12, %rdi
	movl	$16, %esi
	movb	$1, -208(%rbp)
	movups	%xmm1, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13AccessBuilder17ForBigIntBitfieldEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13AccessBuilder24ForBigIntOptionalPaddingEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	-280(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm2
	movq	-80(%rbp), %rax
	movups	%xmm2, 72(%rbx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1258
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1258:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23332:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint64ToBigIntEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint64ToBigIntEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer27LowerTruncateBigIntToUint64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer27LowerTruncateBigIntToUint64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer27LowerTruncateBigIntToUint64EPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer27LowerTruncateBigIntToUint64EPNS1_4NodeE:
.LFB23335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	48(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$248, %rsp
	movq	32(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	andl	$15, %eax
	movq	$0, -104(%rbp)
	movb	$5, -64(%rbp)
	movb	$0, -208(%rbp)
	movl	$1, -204(%rbp)
	movq	$0, -200(%rbp)
	movb	$0, -160(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	cmpl	$15, %eax
	jne	.L1260
	movq	16(%r8), %r8
.L1260:
	leaq	-272(%rbp), %r13
	movq	%r8, -280(%rbp)
	leaq	-112(%rbp), %r15
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder17ForBigIntBitfieldEv@PLT
	movq	-280(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r13, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder32ForBigIntLeastSignificantDigit64Ev@PLT
	movq	-280(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	leaq	-208(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-192(%rbp), %xmm1
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$1, -208(%rbp)
	movups	%xmm1, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler8Int64SubEPNS1_4NodeES4_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm2
	movq	-80(%rbp), %rax
	movups	%xmm2, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1263
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1263:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23335:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer27LowerTruncateBigIntToUint64EPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer27LowerTruncateBigIntToUint64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer27LowerTruncateTaggedToWord32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer27LowerTruncateTaggedToWord32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer27LowerTruncateTaggedToWord32EPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer27LowerTruncateTaggedToWord32EPNS1_4NodeE:
.LFB23340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1265
	movq	16(%r13), %r13
.L1265:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -160(%rbp)
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$4, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	leaq	-160(%rbp), %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movl	$32, %esi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1270
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%rax, %rdx
.L1267:
	leaq	-112(%rbp), %r14
	movq	%r12, %rdi
	leaq	-224(%rbp), %r15
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r15, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler23TruncateFloat64ToWord32EPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1271
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1270:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%rax, %rdx
	jmp	.L1267
.L1271:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23340:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer27LowerTruncateTaggedToWord32EPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer27LowerTruncateTaggedToWord32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer34LowerCheckedTruncateTaggedToWord32EPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer34LowerCheckedTruncateTaggedToWord32EPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer34LowerCheckedTruncateTaggedToWord32EPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer34LowerCheckedTruncateTaggedToWord32EPNS1_4NodeES4_:
.LFB23341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler28CheckTaggedInputParametersOfEPKNS1_8OperatorE@PLT
	leaq	32(%r12), %rdx
	movq	%rax, %r13
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1273
	movq	32(%r12), %rdx
	addq	$16, %rdx
.L1273:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movq	(%rdx), %r15
	movb	$0, -160(%rbp)
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$4, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	-168(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	leaq	-160(%rbp), %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movl	$32, %esi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1279
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%rax, %rdx
.L1275:
	leaq	-112(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rcx
	leaq	8(%r13), %rdx
	movq	%r14, %r8
	movdqa	-144(%rbp), %xmm0
	movb	$1, -160(%rbp)
	movq	%rbx, %rdi
	movups	%xmm0, 72(%rbx)
	movzbl	0(%r13), %esi
	call	_ZN2v88internal8compiler23EffectControlLinearizer40BuildCheckedHeapNumberOrOddballToFloat64ENS1_20CheckTaggedInputModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler23TruncateFloat64ToWord32EPNS1_4NodeE@PLT
	movq	-168(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1280
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1279:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%rax, %rdx
	jmp	.L1275
.L1280:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23341:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer34LowerCheckedTruncateTaggedToWord32EPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer34LowerCheckedTruncateTaggedToWord32EPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer28LowerObjectIsArrayBufferViewEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer28LowerObjectIsArrayBufferViewEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer28LowerObjectIsArrayBufferViewEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer28LowerObjectIsArrayBufferViewEPNS1_4NodeE:
.LFB23344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$176, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1282
	movq	16(%r14), %r14
.L1282:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -144(%rbp)
	movq	%r12, %rdi
	movb	$0, -96(%rbp)
	movl	$0, -140(%rbp)
	movq	$0, -136(%rbp)
	movl	$1, -92(%rbp)
	movq	$0, -88(%rbp)
	movb	$1, -48(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-208(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-144(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$1086, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8Int32SubEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-96(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32LessThanEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-128(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$1, -144(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-80(%rbp), %xmm1
	movq	-64(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1285
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1285:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23344:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer28LowerObjectIsArrayBufferViewEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer28LowerObjectIsArrayBufferViewEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsBigIntEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsBigIntEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsBigIntEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsBigIntEPNS1_4NodeE:
.LFB23345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$176, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1287
	movq	16(%r14), %r14
.L1287:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -144(%rbp)
	movq	%r12, %rdi
	movb	$0, -96(%rbp)
	movl	$0, -140(%rbp)
	movq	$0, -136(%rbp)
	movl	$1, -92(%rbp)
	movq	$0, -88(%rbp)
	movb	$1, -48(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-208(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-144(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler17BigIntMapConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-96(%rbp), %r13
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-128(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$1, -144(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-80(%rbp), %xmm1
	movq	-64(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1290
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1290:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23345:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsBigIntEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsBigIntEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer21LowerObjectIsCallableEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer21LowerObjectIsCallableEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer21LowerObjectIsCallableEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer21LowerObjectIsCallableEPNS1_4NodeE:
.LFB23346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$176, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1292
	movq	16(%r14), %r14
.L1292:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -144(%rbp)
	movq	%r12, %rdi
	movb	$0, -96(%rbp)
	movl	$0, -140(%rbp)
	movq	$0, -136(%rbp)
	movl	$1, -92(%rbp)
	movq	$0, -88(%rbp)
	movb	$1, -48(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-208(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-144(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder14ForMapBitFieldEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-96(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-128(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$1, -144(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-80(%rbp), %xmm1
	movq	-64(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1295
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1295:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23346:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer21LowerObjectIsCallableEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer21LowerObjectIsCallableEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsConstructorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsConstructorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsConstructorEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsConstructorEPNS1_4NodeE:
.LFB23347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$176, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1297
	movq	16(%r14), %r14
.L1297:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -144(%rbp)
	movq	%r12, %rdi
	movb	$0, -96(%rbp)
	movl	$0, -140(%rbp)
	movq	$0, -136(%rbp)
	movl	$1, -92(%rbp)
	movq	$0, -88(%rbp)
	movb	$1, -48(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-208(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-144(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder14ForMapBitFieldEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-96(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-128(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$1, -144(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-80(%rbp), %xmm1
	movq	-64(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1300
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1300:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23347:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsConstructorEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsConstructorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer31LowerObjectIsDetectableCallableEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer31LowerObjectIsDetectableCallableEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer31LowerObjectIsDetectableCallableEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer31LowerObjectIsDetectableCallableEPNS1_4NodeE:
.LFB23348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$176, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1302
	movq	16(%r14), %r14
.L1302:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -144(%rbp)
	movq	%r12, %rdi
	movb	$0, -96(%rbp)
	movl	$0, -140(%rbp)
	movq	$0, -136(%rbp)
	movl	$1, -92(%rbp)
	movq	$0, -88(%rbp)
	movb	$1, -48(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-208(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-144(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder14ForMapBitFieldEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$18, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-96(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-128(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$1, -144(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-80(%rbp), %xmm1
	movq	-64(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1305
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1305:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23348:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer31LowerObjectIsDetectableCallableEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer31LowerObjectIsDetectableCallableEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsNonCallableEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsNonCallableEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsNonCallableEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsNonCallableEPNS1_4NodeE:
.LFB23360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1307
	movq	16(%r14), %r14
.L1307:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -160(%rbp)
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$1, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-224(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rdx
	movq	%rax, %rsi
	movq	%r8, -232(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$1024, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler21Uint32LessThanOrEqualEPNS1_4NodeES4_@PLT
	movq	-232(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder14ForMapBitFieldEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-112(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1310
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1310:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23360:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsNonCallableEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsNonCallableEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsNumberEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsNumberEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsNumberEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsNumberEPNS1_4NodeE:
.LFB23361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$176, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1312
	movq	16(%r14), %r14
.L1312:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -144(%rbp)
	movq	%r12, %rdi
	movb	$0, -96(%rbp)
	movl	$1, -140(%rbp)
	movq	$0, -136(%rbp)
	movl	$1, -92(%rbp)
	movq	$0, -88(%rbp)
	movb	$1, -48(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-208(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-144(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-96(%rbp), %r13
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-128(%rbp), %xmm0
	movq	%r12, %rdi
	movl	$1, %esi
	movb	$1, -144(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-80(%rbp), %xmm1
	movq	-64(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1315
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1315:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23361:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsNumberEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsNumberEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer21LowerObjectIsReceiverEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer21LowerObjectIsReceiverEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer21LowerObjectIsReceiverEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer21LowerObjectIsReceiverEPNS1_4NodeE:
.LFB23362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$176, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1317
	movq	16(%r14), %r14
.L1317:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -144(%rbp)
	movq	%r12, %rdi
	movb	$0, -96(%rbp)
	movl	$0, -140(%rbp)
	movq	$0, -136(%rbp)
	movl	$1, -92(%rbp)
	movq	$0, -88(%rbp)
	movb	$1, -48(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-208(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-144(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$1024, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-96(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler21Uint32LessThanOrEqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-128(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$1, -144(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-80(%rbp), %xmm1
	movq	-64(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1320
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1320:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23362:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer21LowerObjectIsReceiverEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer21LowerObjectIsReceiverEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsStringEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsStringEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsStringEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsStringEPNS1_4NodeE:
.LFB23364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$176, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1322
	movq	16(%r14), %r14
.L1322:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -144(%rbp)
	movq	%r12, %rdi
	movb	$0, -96(%rbp)
	movl	$0, -140(%rbp)
	movq	$0, -136(%rbp)
	movl	$1, -92(%rbp)
	movq	$0, -88(%rbp)
	movb	$1, -48(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-208(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-144(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-96(%rbp), %r13
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32LessThanEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-128(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$1, -144(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-80(%rbp), %xmm1
	movq	-64(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1325
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1325:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23364:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsStringEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsStringEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsSymbolEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsSymbolEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsSymbolEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsSymbolEPNS1_4NodeE:
.LFB23365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$176, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1327
	movq	16(%r14), %r14
.L1327:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -144(%rbp)
	movq	%r12, %rdi
	movb	$0, -96(%rbp)
	movl	$0, -140(%rbp)
	movq	$0, -136(%rbp)
	movl	$1, -92(%rbp)
	movq	$0, -88(%rbp)
	movb	$1, -48(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-208(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-144(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-96(%rbp), %r13
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-128(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$1, -144(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-80(%rbp), %xmm1
	movq	-64(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1330
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1330:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23365:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsSymbolEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsSymbolEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25LowerObjectIsUndetectableEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25LowerObjectIsUndetectableEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25LowerObjectIsUndetectableEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer25LowerObjectIsUndetectableEPNS1_4NodeE:
.LFB23366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$176, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1332
	movq	16(%r14), %r14
.L1332:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -144(%rbp)
	movq	%r12, %rdi
	movb	$0, -96(%rbp)
	movl	$0, -140(%rbp)
	movq	$0, -136(%rbp)
	movl	$1, -92(%rbp)
	movq	$0, -88(%rbp)
	movb	$1, -48(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-208(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-144(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder14ForMapBitFieldEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-96(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-128(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$1, -144(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-80(%rbp), %xmm1
	movq	-64(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1335
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1335:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23366:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25LowerObjectIsUndetectableEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer25LowerObjectIsUndetectableEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer18LowerNewConsStringEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer18LowerNewConsStringEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer18LowerNewConsStringEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer18LowerNewConsStringEPNS1_4NodeE:
.LFB23374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$264, %rsp
	movq	32(%rsi), %r9
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1337
	movq	40(%rsi), %r15
	addq	$48, %rsi
.L1338:
	leaq	-272(%rbp), %r13
	leaq	48(%rbx), %r12
	movq	%r9, -296(%rbp)
	movq	(%rsi), %r14
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	-280(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	-280(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	-288(%rbp), %r8
	movq	%r12, %rdi
	movb	$0, -208(%rbp)
	movq	%rax, %rdx
	movb	$0, -160(%rbp)
	movq	%r8, %rsi
	movb	$0, -112(%rbp)
	movl	$1, -204(%rbp)
	movq	$0, -200(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$7, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-280(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-280(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	leaq	-208(%rbp), %rcx
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	$2, %r8d
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movdqa	-192(%rbp), %xmm0
	movq	(%rbx), %rdi
	movb	$1, -208(%rbp)
	movups	%xmm0, 72(%rbx)
	movq	360(%rdi), %rax
	leaq	752(%rax), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-112(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	movq	%r8, -280(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm1
	movq	(%rbx), %rdi
	movb	$1, -160(%rbp)
	movups	%xmm1, 72(%rbx)
	movq	360(%rdi), %rax
	leaq	760(%rax), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-280(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-80(%rbp), %rcx
	movq	%r12, %rdi
	movb	$1, -112(%rbp)
	movdqa	-96(%rbp), %xmm2
	movl	$32, %esi
	movq	%rcx, -280(%rbp)
	movups	%xmm2, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-280(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder16ForNameHashFieldEv@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-280(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder15ForStringLengthEv@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-296(%rbp), %r9
	movq	%r9, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForConsStringFirstEv@PLT
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder19ForConsStringSecondEv@PLT
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1341
	addq	$264, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1337:
	.cfi_restore_state
	leaq	16(%r9), %rsi
	movq	8(%rsi), %r15
	movq	16(%r9), %r9
	addq	$16, %rsi
	jmp	.L1338
.L1341:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23374:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer18LowerNewConsStringEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer18LowerNewConsStringEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer17LoadFromSeqStringEPNS1_4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer17LoadFromSeqStringEPNS1_4NodeES4_S4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer17LoadFromSeqStringEPNS1_4NodeES4_S4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer17LoadFromSeqStringEPNS1_4NodeES4_S4_:
.LFB23382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	leaq	-160(%rbp), %rdx
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rcx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	48(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, -160(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$4, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder28ForSeqTwoByteStringCharacterEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler11LoadElementERKNS1_13ElementAccessEPNS1_4NodeES7_@PLT
	leaq	-112(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%rax, %rdx
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r13, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder28ForSeqOneByteStringCharacterEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler11LoadElementERKNS1_13ElementAccessEPNS1_4NodeES7_@PLT
	movq	-200(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1345
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1345:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23382:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer17LoadFromSeqStringEPNS1_4NodeES4_S4_, .-_ZN2v88internal8compiler23EffectControlLinearizer17LoadFromSeqStringEPNS1_4NodeES4_S4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer29LowerStringFromSingleCharCodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer29LowerStringFromSingleCharCodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer29LowerStringFromSingleCharCodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer29LowerStringFromSingleCharCodeEPNS1_4NodeE:
.LFB23383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$264, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1347
	movq	16(%r13), %r13
.L1347:
	leaq	48(%rbx), %r12
	movl	$65535, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movl	$255, %esi
	movq	%r12, %rdi
	movb	$0, -208(%rbp)
	movq	%rax, %r15
	movb	$0, -160(%rbp)
	movl	$0, -204(%rbp)
	movq	$0, -200(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$8, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler21Uint32LessThanOrEqualEPNS1_4NodeES4_@PLT
	leaq	-208(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	360(%rax), %rsi
	addq	$4616, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, %r9
	movq	%rax, -280(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	cmpb	$4, 16(%rax)
	je	.L1348
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeUint32ToUint64EPNS1_4NodeE@PLT
	movq	%rax, %r9
.L1348:
	leaq	-272(%rbp), %r13
	movq	%r9, -288(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	-288(%rbp), %r9
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-280(%rbp), %rdx
	movq	%r9, %rcx
	movq	%r9, -304(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler11LoadElementERKNS1_13ElementAccessEPNS1_4NodeES7_@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler17UndefinedConstantEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	leaq	-112(%rbp), %r10
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -296(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r12, %rdi
	movl	$24, %esi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	addq	$184, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-288(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder16ForNameHashFieldEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-288(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder15ForStringLengthEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-288(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movq	%r15, %r8
	movl	$2, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler5StoreENS1_19StoreRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-304(%rbp), %r9
	movq	-280(%rbp), %rdx
	movq	%r9, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_@PLT
	movq	-296(%rbp), %r10
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -288(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-192(%rbp), %xmm1
	movq	%r12, %rdi
	movl	$24, %esi
	movb	$1, -208(%rbp)
	movups	%xmm1, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	addq	$744, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-280(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder16ForNameHashFieldEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-280(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder15ForStringLengthEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-280(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %r8
	movq	%r14, %rdx
	movl	$3, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler5StoreENS1_19StoreRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	-288(%rbp), %r10
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm2
	movq	-80(%rbp), %rax
	movups	%xmm2, 72(%rbx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1352
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1352:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23383:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer29LowerStringFromSingleCharCodeEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer29LowerStringFromSingleCharCodeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer30LowerStringFromSingleCodePointEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer30LowerStringFromSingleCodePointEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer30LowerStringFromSingleCodePointEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer30LowerStringFromSingleCodePointEPNS1_4NodeE:
.LFB23386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$312, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1354
	movq	16(%r14), %r14
.L1354:
	leaq	48(%rbx), %r12
	movl	$65535, %esi
	movb	$0, -256(%rbp)
	movq	%r12, %rdi
	movb	$0, -208(%rbp)
	movl	$0, -252(%rbp)
	movq	$0, -248(%rbp)
	movl	$0, -204(%rbp)
	movq	$0, -200(%rbp)
	movb	$0, -160(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$8, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler21Uint32LessThanOrEqualEPNS1_4NodeES4_@PLT
	leaq	-256(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$255, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler21Uint32LessThanOrEqualEPNS1_4NodeES4_@PLT
	leaq	-208(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	360(%rax), %rsi
	addq	$4616, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %r10
	movq	%rax, -328(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	cmpb	$4, 16(%rax)
	je	.L1355
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeUint32ToUint64EPNS1_4NodeE@PLT
	movq	%rax, %r10
.L1355:
	leaq	-320(%rbp), %r13
	movq	%r10, -336(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	-336(%rbp), %r10
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-328(%rbp), %rdx
	movq	%r10, %rcx
	movq	%r10, -352(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler11LoadElementERKNS1_13ElementAccessEPNS1_4NodeES7_@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler17UndefinedConstantEv@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	leaq	-112(%rbp), %r9
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -344(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r12, %rdi
	movl	$24, %esi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	addq	$184, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-336(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder16ForNameHashFieldEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-336(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder15ForStringLengthEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-336(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %r8
	movq	%r15, %rdx
	movl	$2, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler5StoreENS1_19StoreRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementEv@PLT
	movq	%r15, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-352(%rbp), %r10
	movq	-328(%rbp), %rdx
	movq	%r10, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_@PLT
	movq	-344(%rbp), %r9
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -336(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-192(%rbp), %xmm1
	movq	%r12, %rdi
	movl	$24, %esi
	movb	$1, -208(%rbp)
	movups	%xmm1, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	addq	$744, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-328(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder16ForNameHashFieldEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-328(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder15ForStringLengthEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-328(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %r8
	movq	%r15, %rdx
	movl	$3, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler5StoreENS1_19StoreRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	-336(%rbp), %r9
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-240(%rbp), %xmm2
	movq	%r12, %rdi
	movl	$55232, %esi
	movb	$1, -256(%rbp)
	movups	%xmm2, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$10, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32ShrEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler8Int32AddEPNS1_4NodeES4_@PLT
	movl	$56320, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$1023, %esi
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movq	-328(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8Int32AddEPNS1_4NodeES4_@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32ShlEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler8Word32OrEPNS1_4NodeES4_@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	360(%rax), %rsi
	addq	$744, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-328(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder16ForNameHashFieldEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-328(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder15ForStringLengthEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-328(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %r8
	movq	%r14, %rdx
	movl	$4, %esi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler5StoreENS1_19StoreRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	-336(%rbp), %r9
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm3
	movq	-80(%rbp), %rax
	movups	%xmm3, 72(%rbx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1359
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1359:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23386:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer30LowerStringFromSingleCodePointEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer30LowerStringFromSingleCodePointEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer33LowerConvertTaggedHoleToUndefinedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer33LowerConvertTaggedHoleToUndefinedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer33LowerConvertTaggedHoleToUndefinedEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer33LowerConvertTaggedHoleToUndefinedEPNS1_4NodeE:
.LFB23399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$112, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1361
	movq	16(%r13), %r13
.L1361:
	leaq	48(%rbx), %r12
	movb	$0, -96(%rbp)
	leaq	-96(%rbp), %r14
	movq	%r12, %rdi
	movb	$8, -48(%rbp)
	movb	$0, -144(%rbp)
	movl	$0, -140(%rbp)
	movq	$0, -136(%rbp)
	movl	$1, -92(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15TheHoleConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	leaq	-144(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-128(%rbp), %xmm0
	movq	%r12, %rdi
	movb	$1, -144(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler17UndefinedConstantEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-80(%rbp), %xmm1
	movq	-64(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1364
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1364:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23399:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer33LowerConvertTaggedHoleToUndefinedEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer33LowerConvertTaggedHoleToUndefinedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer27LowerPlainPrimitiveToWord32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer27LowerPlainPrimitiveToWord32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer27LowerPlainPrimitiveToWord32EPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer27LowerPlainPrimitiveToWord32EPNS1_4NodeE:
.LFB23420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1366
	movq	16(%r13), %r13
.L1366:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -208(%rbp)
	movq	%r12, %rdi
	movb	$0, -160(%rbp)
	movl	$0, -204(%rbp)
	movq	$0, -200(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$4, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	leaq	-208(%rbp), %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movl	$32, %esi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1373
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%rax, %rdx
.L1368:
	leaq	-112(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-192(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$1, -208(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler8ToNumberEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	-272(%rbp), %r15
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler23TruncateFloat64ToWord32EPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movl	$32, %esi
	movq	%r12, %rdi
	movdqa	-144(%rbp), %xmm1
	movb	$1, -160(%rbp)
	movups	%xmm1, 72(%rbx)
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1374
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%rax, %rdx
.L1370:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm2
	movq	-80(%rbp), %rax
	movups	%xmm2, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1375
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1373:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%rax, %rdx
	jmp	.L1368
	.p2align 4,,10
	.p2align 3
.L1374:
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%rax, %rdx
	jmp	.L1370
.L1375:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23420:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer27LowerPlainPrimitiveToWord32EPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer27LowerPlainPrimitiveToWord32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer28LowerPlainPrimitiveToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer28LowerPlainPrimitiveToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer28LowerPlainPrimitiveToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer28LowerPlainPrimitiveToFloat64EPNS1_4NodeE:
.LFB23421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1377
	movq	16(%r13), %r13
.L1377:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -208(%rbp)
	movq	%r12, %rdi
	movb	$0, -160(%rbp)
	movl	$0, -204(%rbp)
	movq	$0, -200(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$13, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	leaq	-208(%rbp), %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movl	$32, %esi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1384
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%rax, %rsi
.L1379:
	movq	%r12, %rdi
	leaq	-112(%rbp), %r14
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeInt32ToFloat64EPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-192(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$1, -208(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler8ToNumberEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	-272(%rbp), %r15
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movl	$32, %esi
	movq	%r12, %rdi
	movdqa	-144(%rbp), %xmm1
	movb	$1, -160(%rbp)
	movups	%xmm1, 72(%rbx)
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1385
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%rax, %rsi
.L1381:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeInt32ToFloat64EPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm2
	movq	-80(%rbp), %rax
	movups	%xmm2, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1386
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1384:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%rax, %rsi
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1385:
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%rax, %rsi
	jmp	.L1381
.L1386:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23421:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer28LowerPlainPrimitiveToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer28LowerPlainPrimitiveToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer31LowerEnsureWritableFastElementsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer31LowerEnsureWritableFastElementsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer31LowerEnsureWritableFastElementsEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer31LowerEnsureWritableFastElementsEPNS1_4NodeE:
.LFB23422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-8(%rsi), %rax
	movq	%rax, -296(%rbp)
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1389
	movq	-296(%rbp), %rax
	leaq	16(%rax), %rsi
	movq	16(%rax), %rax
	addq	$8, %rsi
	movq	%rax, -296(%rbp)
.L1389:
	movq	(%rsi), %r13
	leaq	-272(%rbp), %r15
	leaq	48(%rbx), %r12
	movb	$0, -112(%rbp)
	movq	%r15, %rdi
	movb	$8, -64(%rbp)
	movb	$0, -160(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler21FixedArrayMapConstantEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-112(%rbp), %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movl	$84, %edx
	movdqa	-144(%rbp), %xmm1
	movb	$1, -160(%rbp)
	movups	%xmm1, 72(%rbx)
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-256(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$112, %r8d
	leaq	-288(%rbp), %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -280(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -288(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	-272(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r13, %rsi
	movq	%rax, -304(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r15, -192(%rbp)
	leaq	-208(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-304(%rbp), %xmm0
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movq	%rax, -184(%rbp)
	movq	80(%rbx), %rax
	movhps	-296(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -176(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	3(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, 72(%rbx)
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm2
	movq	-80(%rbp), %rax
	movups	%xmm2, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1392
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1392:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23422:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer31LowerEnsureWritableFastElementsEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer31LowerEnsureWritableFastElementsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer26LowerMaybeGrowFastElementsEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer26LowerMaybeGrowFastElementsEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer26LowerMaybeGrowFastElementsEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler23EffectControlLinearizer26LowerMaybeGrowFastElementsEPNS1_4NodeES4_:
.LFB23423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$296, %rsp
	movq	%rdx, -328(%rbp)
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler28GrowFastElementsParametersOfEPKNS1_8OperatorE@PLT
	movdqu	(%rax), %xmm1
	movaps	%xmm1, -272(%rbp)
	movq	16(%rax), %rax
	movq	%rax, -256(%rbp)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1394
	movq	32(%r12), %rax
	movq	40(%r12), %r13
	addq	$56, %r12
	movq	-8(%r12), %r14
	movq	%rax, -320(%rbp)
.L1395:
	movq	(%r12), %rdx
	leaq	48(%rbx), %r12
	movq	%r14, %rsi
	movb	$0, -112(%rbp)
	movq	%r12, %rdi
	movl	$1, -108(%rbp)
	leaq	-112(%rbp), %r15
	movq	$0, -104(%rbp)
	movb	$8, -64(%rbp)
	movb	$0, -208(%rbp)
	movl	$0, -204(%rbp)
	movq	$0, -200(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32LessThanEPNS1_4NodeES4_@PLT
	leaq	-208(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-192(%rbp), %xmm2
	movq	(%rbx), %rax
	movb	$1, -208(%rbp)
	cmpb	$0, -272(%rbp)
	leaq	-240(%rbp), %rdi
	movups	%xmm2, 72(%rbx)
	movq	360(%rax), %rsi
	jne	.L1396
	movl	$85, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
.L1397:
	movq	-224(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	leaq	-288(%rbp), %rsi
	movl	$112, %r8d
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -280(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -288(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movq	%rax, -296(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1401
.L1398:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	-240(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r13, %rsi
	movq	%rax, -304(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movdqu	72(%rbx), %xmm3
	xorl	%r8d, %r8d
	movq	-304(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-160(%rbp), %rcx
	movhps	-320(%rbp), %xmm0
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	-312(%rbp), %xmm0
	movhps	-296(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movl	24(%rax), %edx
	movl	28(%rax), %eax
	leal	4(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, 72(%rbx)
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	-328(%rbp), %r8
	movl	$1, %r9d
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	-264(%rbp), %rdx
	movl	$3, %esi
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm4
	movq	-80(%rbp), %rax
	movups	%xmm4, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1402
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1396:
	.cfi_restore_state
	movl	$86, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L1397
	.p2align 4,,10
	.p2align 3
.L1394:
	movq	32(%r12), %r12
	movq	16(%r12), %rcx
	movq	24(%r12), %r13
	addq	$40, %r12
	movq	-8(%r12), %r14
	movq	%rcx, -320(%rbp)
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1401:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	%rax, %r14
	jmp	.L1398
.L1402:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23423:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer26LowerMaybeGrowFastElementsEPNS1_4NodeES4_, .-_ZN2v88internal8compiler23EffectControlLinearizer26LowerMaybeGrowFastElementsEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer21LowerLoadFieldByIndexEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer21LowerLoadFieldByIndexEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer21LowerLoadFieldByIndexEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer21LowerLoadFieldByIndexEPNS1_4NodeE:
.LFB23425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$360, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-8(%rsi), %rax
	movq	%rax, -344(%rbp)
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1405
	movq	-344(%rbp), %rax
	leaq	16(%rax), %rsi
	movq	16(%rax), %rax
	addq	$8, %rsi
	movq	%rax, -344(%rbp)
.L1405:
	leaq	48(%rbx), %r12
	movq	(%rsi), %r15
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%rax, %r8
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1409
.L1406:
	movq	%r8, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -392(%rbp)
	movb	$0, -272(%rbp)
	movl	$0, -268(%rbp)
	movq	$0, -264(%rbp)
	movb	$0, -176(%rbp)
	movl	$1, -172(%rbp)
	movq	$0, -168(%rbp)
	movb	$8, -128(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-272(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler11IntLessThanEPNS1_4NodeES4_@PLT
	leaq	-112(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rdx
	movq	%rax, %rsi
	movq	%r10, -384(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$23, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-336(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movq	-344(%rbp), %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	leaq	-176(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%rax, %rdx
	movq	%r9, -376(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm0
	movq	%r13, %rdi
	movb	$1, -112(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	-344(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$7, %esi
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6IntSubEPNS1_4NodeES4_@PLT
	movq	-360(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rcx, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	-368(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movq	-352(%rbp), %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	-376(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rsi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movdqa	-256(%rbp), %xmm1
	movq	-392(%rbp), %r8
	movb	$1, -272(%rbp)
	movl	$1, -108(%rbp)
	movups	%xmm1, 72(%rbx)
	movq	%r8, %rdx
	movq	$0, -104(%rbp)
	movb	$13, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movb	$0, -224(%rbp)
	movl	$1, -220(%rbp)
	movq	%rax, %rsi
	movq	%rax, %r15
	movq	$0, -216(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler11IntLessThanEPNS1_4NodeES4_@PLT
	leaq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$23, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	-352(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movq	-344(%rbp), %rdx
	movl	$1549, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	-384(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	movq	%r10, -368(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-208(%rbp), %xmm2
	movq	%r13, %rdi
	movb	$1, -224(%rbp)
	movups	%xmm2, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	-344(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$7, %esi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6IntSubEPNS1_4NodeES4_@PLT
	movq	-344(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rcx, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	-352(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movq	-360(%rbp), %r8
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	-368(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-80(%rbp), %r8
	movq	%r12, %rdi
	movb	$1, -112(%rbp)
	movdqa	-96(%rbp), %xmm3
	movl	$16, %esi
	movq	%r8, -344(%rbp)
	movups	%xmm3, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-344(%rbp), %r8
	movq	%r8, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	-376(%rbp), %r9
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-160(%rbp), %xmm4
	movq	-144(%rbp), %rax
	movups	%xmm4, 72(%rbx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1410
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1409:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -352(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	-352(%rbp), %r8
	movq	%rax, %r15
	jmp	.L1406
.L1410:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23425:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer21LowerLoadFieldByIndexEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer21LowerLoadFieldByIndexEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer24LowerLoadDataViewElementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer24LowerLoadDataViewElementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer24LowerLoadDataViewElementEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer24LowerLoadDataViewElementEPNS1_4NodeE:
.LFB23427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$168, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19ExternalArrayTypeOfEPKNS1_8OperatorE@PLT
	movl	%eax, %r14d
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1412
	movq	32(%rbx), %rsi
	movq	40(%rbx), %r15
	addq	$56, %rbx
	movq	-8(%rbx), %r8
.L1413:
	movq	(%rbx), %r9
	leaq	48(%r12), %r13
	movq	%r8, -208(%rbp)
	movq	%r13, %rdi
	movq	%r9, -200(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6RetainEPNS1_4NodeE@PLT
	leaq	-192(%rbp), %rdi
	movl	$1, %ecx
	movl	%r14d, %esi
	movl	$1, %edx
	call	_ZN2v88internal8compiler13AccessBuilder20ForTypedArrayElementENS0_17ExternalArrayTypeEbNS0_15LoadSensitivityE@PLT
	movq	-208(%rbp), %r8
	movq	%r15, %rdx
	movq	%r13, %rdi
	movzbl	-176(%rbp), %ebx
	movzwl	-176(%rbp), %esi
	movq	%r8, %rcx
	movb	%bl, %sil
	call	_ZN2v88internal8compiler14GraphAssembler13LoadUnalignedENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	-200(%rbp), %r9
	movb	%bl, -64(%rbp)
	movq	%r13, %rdi
	movq	%rax, %r15
	leaq	-112(%rbp), %rbx
	movb	$0, -112(%rbp)
	leaq	-160(%rbp), %rdx
	movq	%r9, %rsi
	movb	$0, -160(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movdqa	-144(%rbp), %xmm0
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%r12)
	call	_ZN2v88internal8compiler23EffectControlLinearizer17BuildReverseBytesENS0_17ExternalArrayTypeEPNS1_4NodeE
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%r12)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1416
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1412:
	.cfi_restore_state
	movq	32(%rbx), %rbx
	movq	16(%rbx), %rsi
	movq	24(%rbx), %r15
	addq	$40, %rbx
	movq	-8(%rbx), %r8
	jmp	.L1413
.L1416:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23427:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer24LowerLoadDataViewElementEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer24LowerLoadDataViewElementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25LowerStoreDataViewElementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25LowerStoreDataViewElementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25LowerStoreDataViewElementEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer25LowerStoreDataViewElementEPNS1_4NodeE:
.LFB23428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$184, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler19ExternalArrayTypeOfEPKNS1_8OperatorE@PLT
	movl	%eax, %r14d
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1418
	movq	32(%rbx), %rsi
	movq	40(%rbx), %r11
	addq	$64, %rbx
	movq	-16(%rbx), %r9
	movq	-8(%rbx), %r15
.L1419:
	movq	(%rbx), %r10
	leaq	48(%r12), %r13
	movq	%r11, -216(%rbp)
	movq	%r13, %rdi
	movq	%r9, -208(%rbp)
	movq	%r10, -200(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6RetainEPNS1_4NodeE@PLT
	movl	$1, %ecx
	movl	$1, %edx
	movl	%r14d, %esi
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal8compiler13AccessBuilder20ForTypedArrayElementENS0_17ExternalArrayTypeEbNS0_15LoadSensitivityE@PLT
	movq	-200(%rbp), %r10
	movq	%r13, %rdi
	movb	$0, -112(%rbp)
	movzbl	-176(%rbp), %ebx
	leaq	-160(%rbp), %rdx
	movb	$0, -160(%rbp)
	movq	%r10, %rsi
	movl	$1, -108(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -104(%rbp)
	movb	%bl, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	leaq	-112(%rbp), %r10
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%r10, %rsi
	movq	%r10, -200(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movdqa	-144(%rbp), %xmm0
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%r12)
	call	_ZN2v88internal8compiler23EffectControlLinearizer17BuildReverseBytesENS0_17ExternalArrayTypeEPNS1_4NodeE
	movq	-200(%rbp), %r10
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %r8
	movl	%ebx, %esi
	movq	-208(%rbp), %r9
	movq	-216(%rbp), %r11
	movq	%r13, %rdi
	movb	$1, -112(%rbp)
	movups	%xmm1, 72(%r12)
	movq	%r9, %rcx
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler14StoreUnalignedENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1422
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1418:
	.cfi_restore_state
	movq	32(%rbx), %rbx
	movq	16(%rbx), %rsi
	movq	24(%rbx), %r11
	addq	$48, %rbx
	movq	-16(%rbx), %r9
	movq	-8(%rbx), %r15
	jmp	.L1419
.L1422:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23428:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25LowerStoreDataViewElementEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer25LowerStoreDataViewElementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer20LowerConvertReceiverEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer20LowerConvertReceiverEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer20LowerConvertReceiverEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer20LowerConvertReceiverEPNS1_4NodeE:
.LFB23439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$344, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21ConvertReceiverModeOfEPKNS1_8OperatorE@PLT
	movzbl	23(%r12), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L1424
	movq	32(%r12), %r13
	addq	$40, %r12
.L1425:
	movq	(%r12), %r14
	cmpl	$1, %eax
	je	.L1426
	cmpl	$2, %eax
	je	.L1427
	testl	%eax, %eax
	je	.L1423
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1426:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -160(%rbp)
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$8, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	-320(%rbp), %r15
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rdx
	movq	%rax, %rsi
	movq	%r9, -368(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r15, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	-360(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$1024, %esi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi@PLT
	movq	-360(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32LessThanEPNS1_4NodeES4_@PLT
	movq	-368(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	leaq	-112(%rbp), %r10
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -376(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm1
	movq	(%rbx), %rax
	movb	$1, -160(%rbp)
	leaq	-352(%rbp), %rdi
	movl	$91, %edx
	movups	%xmm1, 72(%rbx)
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-336(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$112, %r8d
	movq	%r15, %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -312(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -320(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r15, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder29ForJSGlobalProxyNativeContextEv@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	-352(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-368(%rbp), %r8
	movq	%rax, -360(%rbp)
	movq	64(%rbx), %rax
	movq	%r8, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r13, %xmm2
	movq	%r14, -192(%rbp)
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movq	-360(%rbp), %xmm0
	leaq	-208(%rbp), %rcx
	movq	%rax, -184(%rbp)
	movq	80(%rbx), %rax
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -176(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	3(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-376(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, 72(%rbx)
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm3
	movq	-80(%rbp), %r14
	movups	%xmm3, 72(%rbx)
.L1423:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1436
	addq	$344, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1424:
	.cfi_restore_state
	movq	32(%r12), %r12
	movq	16(%r12), %r13
	addq	$24, %r12
	jmp	.L1425
	.p2align 4,,10
	.p2align 3
.L1427:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movb	$0, -208(%rbp)
	movq	%r12, %rdi
	movb	$0, -160(%rbp)
	movl	$0, -204(%rbp)
	movq	$0, -200(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$8, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	-320(%rbp), %r15
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-208(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rdx
	movq	%rax, %rsi
	movq	%r9, -368(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r15, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	-360(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$1024, %esi
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi@PLT
	movq	-360(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler14Uint32LessThanEPNS1_4NodeES4_@PLT
	movq	-368(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	leaq	-112(%rbp), %r10
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -376(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-192(%rbp), %xmm4
	movq	%r12, %rdi
	movb	$1, -208(%rbp)
	movups	%xmm4, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler17UndefinedConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rdx
	movq	%rax, %rsi
	movq	%r8, -360(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12NullConstantEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	-360(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movl	$91, %edx
	leaq	-352(%rbp), %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-336(%rbp), %rax
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$112, %r8d
	movq	%r15, %rsi
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -312(%rbp)
	movq	(%rbx), %rax
	movq	%rcx, -320(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%r15, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder29ForJSGlobalProxyNativeContextEv@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	-352(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-368(%rbp), %r8
	movq	%rax, -360(%rbp)
	movq	64(%rbx), %rax
	movq	%r8, %rsi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r15, -240(%rbp)
	movq	%r13, %xmm5
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movq	-360(%rbp), %xmm0
	leaq	-256(%rbp), %rcx
	movq	%rax, -232(%rbp)
	movq	80(%rbx), %rax
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	%rax, -224(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	3(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-376(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, 72(%rbx)
	movq	%rax, %rdx
	movq	%r10, %rsi
	movq	%r10, -360(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm6
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	-360(%rbp), %r10
	movb	$1, -160(%rbp)
	movups	%xmm6, 72(%rbx)
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm7
	movq	-80(%rbp), %r14
	movups	%xmm7, 72(%rbx)
	jmp	.L1423
.L1436:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23439:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer20LowerConvertReceiverEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer20LowerConvertReceiverEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,"axG",@progbits,_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.type	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, @function
_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_:
.LFB25556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rsi, -88(%rbp)
	movq	32(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	4(%rdx), %eax
	movl	$1, %edx
	testl	%eax, %eax
	movq	16(%rdi), %rax
	sete	%sil
	movq	8(%rax), %rdi
	addl	%esi, %esi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%r14, %xmm1
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	-88(%rbp), %xmm0
	leaq	-80(%rbp), %r14
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r14, %rcx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rax, 32(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 32(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1442
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1442:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25556:
	.size	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, .-_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer20LowerArgumentsLengthEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer20LowerArgumentsLengthEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer20LowerArgumentsLengthEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer20LowerArgumentsLengthEPNS1_4NodeE:
.LFB23369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%r12), %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler22FormalParameterCountOfEPKNS1_8OperatorE@PLT
	movq	(%r12), %rdi
	leaq	48(%rbx), %r12
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler14IsRestLengthOfEPKNS1_8OperatorE@PLT
	movb	$0, -160(%rbp)
	movq	%r12, %rdi
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$6, -64(%rbp)
	testb	%al, %al
	je	.L1444
	call	_ZN2v88internal8compiler14GraphAssembler16LoadFramePointerEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler11SmiConstantEi@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-112(%rbp), %r15
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	-168(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r12, %rdi
	movq	$-24, %rsi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$518, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler11SmiConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6IntSubEPNS1_4NodeES4_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler11SmiConstantEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler11SmiConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11IntLessThanEPNS1_4NodeES4_@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, 72(%rbx)
	movq	-80(%rbp), %rax
.L1443:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1448
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1444:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler16LoadFramePointerEv@PLT
	movl	%r14d, %esi
	movq	%r12, %rdi
	leaq	-112(%rbp), %r14
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler11SmiConstantEi@PLT
	movq	-168(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm2
	movq	%r12, %rdi
	movq	$-24, %rsi
	movb	$1, -160(%rbp)
	movups	%xmm2, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$518, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm3
	movups	%xmm3, 72(%rbx)
	movq	-80(%rbp), %rax
	jmp	.L1443
.L1448:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23369:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer20LowerArgumentsLengthEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer20LowerArgumentsLengthEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer19LowerArgumentsFrameEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer19LowerArgumentsFrameEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer19LowerArgumentsFrameEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer19LowerArgumentsFrameEPNS1_4NodeE:
.LFB23370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	48(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$5, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler16LoadFramePointerEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	$-8, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movl	$38, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-112(%rbp), %r15
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movups	%xmm0, 72(%rbx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1452
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1452:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23370:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer19LowerArgumentsFrameEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer19LowerArgumentsFrameEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer20LowerNumberSameValueEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer20LowerNumberSameValueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer20LowerNumberSameValueEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer20LowerNumberSameValueEPNS1_4NodeE:
.LFB23377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	-8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1455
	leaq	16(%r13), %rsi
	movq	16(%r13), %r13
	addq	$8, %rsi
.L1455:
	movq	(%rsi), %r15
	leaq	48(%rbx), %r12
	movq	%r13, %rsi
	movb	$0, -112(%rbp)
	movq	%r12, %rdi
	movb	$1, -64(%rbp)
	leaq	-112(%rbp), %r14
	movq	%r15, %rdx
	movb	$0, -160(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	movq	-168(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	movq	-168(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1458
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1458:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23377:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer20LowerNumberSameValueEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer20LowerNumberSameValueEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer39LowerFindOrderedHashMapEntryForInt32KeyEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer39LowerFindOrderedHashMapEntryForInt32KeyEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer39LowerFindOrderedHashMapEntryForInt32KeyEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer39LowerFindOrderedHashMapEntryForInt32KeyEPNS1_4NodeE:
.LFB23448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$408, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	48(%rbx), %r12
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, -392(%rbp)
	call	_ZN2v88internal8compiler23EffectControlLinearizer19ComputeUnseededHashEPNS1_4NodeE
	movq	%rax, %r8
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1465
.L1460:
	leaq	-384(%rbp), %r14
	movq	%r8, -408(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder37ForOrderedHashMapOrSetNumberOfBucketsEv@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -400(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-400(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	movq	%r10, -424(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6IntSubEPNS1_4NodeES4_@PLT
	movq	-408(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movl	$39, %esi
	movq	%r12, %rdi
	movq	%rax, -400(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-400(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movl	$518, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movb	$0, -176(%rbp)
	movq	%rax, %rdx
	leaq	-176(%rbp), %rax
	movb	$5, -128(%rbp)
	movq	%rax, %rsi
	movb	$0, -112(%rbp)
	movq	%rax, -416(%rbp)
	movl	$2, -172(%rbp)
	movq	$0, -168(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$5, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-160(%rbp), %xmm0
	movq	-144(%rbp), %r15
	movq	$-1, %rsi
	movq	%r12, %rdi
	movb	$1, -176(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-112(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6IntMulEPNS1_4NodeES4_@PLT
	movq	-424(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movl	$39, %esi
	movq	%r12, %rdi
	movq	%rax, -400(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-400(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movl	$1800, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$0, -320(%rbp)
	movq	%rax, %r15
	movb	$0, -272(%rbp)
	movl	$1, -316(%rbp)
	movq	$0, -312(%rbp)
	movl	$1, -268(%rbp)
	movq	$0, -264(%rbp)
	movb	$0, -224(%rbp)
	movl	$0, -220(%rbp)
	movq	$0, -216(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	-424(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	leaq	-224(%rbp), %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movl	$32, %esi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1466
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%rax, %rsi
.L1462:
	movq	-392(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	leaq	-320(%rbp), %r9
	leaq	-272(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r9, %rdx
	movl	$2, %r8d
	movq	%rax, %rsi
	movq	%r9, -432(%rbp)
	movq	%rcx, -440(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movdqa	-208(%rbp), %xmm1
	movq	%r12, %rdi
	movb	$1, -224(%rbp)
	movups	%xmm1, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	-424(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	-440(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -424(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-392(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeInt32ToFloat64EPNS1_4NodeE@PLT
	movq	%r14, %rdi
	movq	%rax, -392(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	-392(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	movq	-432(%rbp), %r9
	movq	-424(%rbp), %rcx
	movq	%r12, %rdi
	movl	$2, %r8d
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movdqa	-304(%rbp), %xmm2
	movq	-400(%rbp), %r15
	movq	%r12, %rdi
	movq	-408(%rbp), %rsi
	movb	$1, -320(%rbp)
	movups	%xmm2, 72(%rbx)
	movq	%r15, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-256(%rbp), %xmm3
	movq	%r12, %rdi
	movl	$55, %esi
	movb	$1, -272(%rbp)
	movups	%xmm3, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movl	$518, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	-416(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm4
	movq	-80(%rbp), %rax
	movups	%xmm4, 72(%rbx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1467
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1466:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%rax, %rsi
	jmp	.L1462
	.p2align 4,,10
	.p2align 3
.L1465:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeUint32ToUint64EPNS1_4NodeE@PLT
	movq	%rax, %r8
	jmp	.L1460
.L1467:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23448:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer39LowerFindOrderedHashMapEntryForInt32KeyEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer39LowerFindOrderedHashMapEntryForInt32KeyEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,"axG",@progbits,_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.type	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, @function
_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_:
.LFB25557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	4(%rdx), %edx
	movq	32(%rdi), %r14
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	testl	%edx, %edx
	movl	$1, %edx
	sete	%sil
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%r14, %xmm1
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	-88(%rbp), %xmm0
	leaq	-80(%rbp), %r14
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r14, %rcx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r12, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rax, 32(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 32(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1471
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1471:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25557:
	.size	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, .-_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer26TruncateTaggedPointerToBitEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm1EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer26TruncateTaggedPointerToBitEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm1EEE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer26TruncateTaggedPointerToBitEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm1EEE, @function
_ZN2v88internal8compiler23EffectControlLinearizer26TruncateTaggedPointerToBitEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm1EEE:
.LFB23273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -232(%rbp)
	movq	32(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1473
	movq	16(%rbx), %rbx
.L1473:
	movq	-232(%rbp), %rax
	xorl	%esi, %esi
	movb	$0, -112(%rbp)
	leaq	-224(%rbp), %r14
	movb	$0, -160(%rbp)
	leaq	48(%rax), %r12
	movl	$0, -108(%rbp)
	movq	%r12, %rdi
	movq	$0, -104(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13FalseConstantEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler19EmptyStringConstantEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r14, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder14ForMapBitFieldEv@PLT
	movq	-240(%rbp), %r9
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r9, %rdx
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-240(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	-248(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	movq	%r9, -240(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler17BigIntMapConstantEv@PLT
	movq	-240(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	leaq	-112(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm1
	movq	%r14, %rdi
	movq	-232(%rbp), %rax
	movb	$1, -160(%rbp)
	movups	%xmm1, 72(%rax)
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64AbsEPNS1_4NodeE@PLT
	movq	-256(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm2
	movq	%r14, %rdi
	movb	$1, -112(%rbp)
	movq	-232(%rbp), %rax
	movups	%xmm2, 72(%rax)
	call	_ZN2v88internal8compiler13AccessBuilder17ForBigIntBitfieldEv@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$2147483646, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-240(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-232(%rbp), %rax
	pxor	%xmm0, %xmm0
	movups	%xmm0, 72(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1476
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1476:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23273:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer26TruncateTaggedPointerToBitEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm1EEE, .-_ZN2v88internal8compiler23EffectControlLinearizer26TruncateTaggedPointerToBitEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm1EEE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer31LowerTruncateTaggedPointerToBitEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer31LowerTruncateTaggedPointerToBitEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer31LowerTruncateTaggedPointerToBitEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer31LowerTruncateTaggedPointerToBitEPNS1_4NodeE:
.LFB23275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-80(%rbp), %rdx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -80(%rbp)
	movl	$1, -76(%rbp)
	movq	$0, -72(%rbp)
	movb	$1, -32(%rbp)
	call	_ZN2v88internal8compiler23EffectControlLinearizer26TruncateTaggedPointerToBitEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm1EEE
	movdqa	-64(%rbp), %xmm0
	movq	-48(%rbp), %rax
	movups	%xmm0, 72(%rbx)
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1480
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1480:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23275:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer31LowerTruncateTaggedPointerToBitEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer31LowerTruncateTaggedPointerToBitEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer24LowerTruncateTaggedToBitEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer24LowerTruncateTaggedToBitEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer24LowerTruncateTaggedToBitEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer24LowerTruncateTaggedToBitEPNS1_4NodeE:
.LFB23274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	48(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	32(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	andl	$15, %eax
	movq	$0, -104(%rbp)
	movb	$1, -64(%rbp)
	movb	$0, -160(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	cmpl	$15, %eax
	jne	.L1482
	movq	16(%r15), %r15
.L1482:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	-112(%rbp), %r14
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer26TruncateTaggedPointerToBitEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm1EEE
	movdqa	-144(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1485
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1485:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23274:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer24LowerTruncateTaggedToBitEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer24LowerTruncateTaggedToBitEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25LowerObjectIsFiniteNumberEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25LowerObjectIsFiniteNumberEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25LowerObjectIsFiniteNumberEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer25LowerObjectIsFiniteNumberEPNS1_4NodeE:
.LFB23351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	32(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1487
	movq	16(%r14), %r14
.L1487:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movq	%rax, %r13
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$1, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	-112(%rbp), %r15
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	-192(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movups	%xmm0, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1490
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1490:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23351:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25LowerObjectIsFiniteNumberEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer25LowerObjectIsFiniteNumberEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer22LowerObjectIsMinusZeroEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer22LowerObjectIsMinusZeroEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer22LowerObjectIsMinusZeroEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer22LowerObjectIsMinusZeroEPNS1_4NodeE:
.LFB23356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1492
	movq	16(%r13), %r13
.L1492:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	leaq	-112(%rbp), %r15
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movq	%rax, -184(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$1, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	-176(%rbp), %r14
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	-184(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	-184(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	%r14, %rsi
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1497
	call	_ZN2v88internal8compiler14GraphAssembler23Float64ExtractLowWord32EPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-192(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	-184(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE@PLT
	movl	$-2147483648, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
.L1494:
	movdqa	-96(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movups	%xmm0, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1498
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1497:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler21BitcastFloat64ToInt64EPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movabsq	$-9223372036854775808, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word64EqualEPNS1_4NodeES4_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	jmp	.L1494
.L1498:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23356:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer22LowerObjectIsMinusZeroEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer22LowerObjectIsMinusZeroEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer22LowerNumberIsMinusZeroEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer22LowerNumberIsMinusZeroEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer22LowerNumberIsMinusZeroEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer22LowerNumberIsMinusZeroEPNS1_4NodeE:
.LFB23357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1500
	movq	16(%r13), %r13
.L1500:
	movq	(%rbx), %rax
	leaq	48(%rbx), %r12
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1505
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$1, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler23Float64ExtractLowWord32EPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-112(%rbp), %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE@PLT
	movl	$-2147483648, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movups	%xmm0, 72(%rbx)
.L1499:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1506
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1505:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler21BitcastFloat64ToInt64EPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movabsq	$-9223372036854775808, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word64EqualEPNS1_4NodeES4_@PLT
	jmp	.L1499
.L1506:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23357:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer22LowerNumberIsMinusZeroEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer22LowerNumberIsMinusZeroEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer16LowerObjectIsNaNEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer16LowerObjectIsNaNEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer16LowerObjectIsNaNEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer16LowerObjectIsNaNEPNS1_4NodeE:
.LFB23358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	32(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1508
	movq	16(%r15), %r15
.L1508:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movq	%rax, %r13
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$1, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	-176(%rbp), %r14
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-112(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%r8, %rdx
	movq	%rax, %rsi
	movq	%r8, -192(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	-192(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	movq	%r8, -184(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	-184(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movups	%xmm0, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1511
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1511:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23358:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer16LowerObjectIsNaNEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer16LowerObjectIsNaNEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer22LowerNewDoubleElementsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer22LowerNewDoubleElementsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer22LowerNewDoubleElementsEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer22LowerNewDoubleElementsEPNS1_4NodeE:
.LFB23371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16AllocationTypeOfEPKNS1_8OperatorE@PLT
	leaq	32(%r12), %rdx
	movl	%eax, %r13d
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1513
	movq	32(%r12), %rdx
	addq	$16, %rdx
.L1513:
	leaq	48(%rbx), %r12
	movq	(%rdx), %r15
	xorl	%esi, %esi
	movb	$0, -176(%rbp)
	movq	%r12, %rdi
	movb	$7, -128(%rbp)
	movl	$1, -172(%rbp)
	movq	$0, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r14
	movq	360(%rdi), %rax
	leaq	288(%rax), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-176(%rbp), %r11
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r11, %rdx
	movq	%rax, %rcx
	movq	%r11, -272(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	leaq	-240(%rbp), %r13
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler27FixedDoubleArrayMapConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-248(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	%r13, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-248(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15TheHoleConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	-248(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movq	%rax, -264(%rbp)
	movl	$2, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$5, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	leaq	-112(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%rax, %rdx
	movq	%r9, -256(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm0
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-80(%rbp), %r10
	movb	$1, -112(%rbp)
	movups	%xmm0, 72(%rbx)
	movq	%r10, %rsi
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12UintLessThanEPNS1_4NodeES4_@PLT
	movq	-272(%rbp), %r11
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rdx
	movl	$1549, %eax
	movq	%r13, %rsi
	movq	-248(%rbp), %r10
	movq	-264(%rbp), %r8
	movq	%r12, %rdi
	movb	$1, -240(%rbp)
	movl	$16, -236(%rbp)
	movq	%r10, %rcx
	movw	%ax, -224(%rbp)
	movq	$8395871, -232(%rbp)
	movb	$0, -222(%rbp)
	movl	$1, -220(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-248(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movq	-256(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-160(%rbp), %xmm1
	movq	-144(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1517
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1517:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23371:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer22LowerNewDoubleElementsEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer22LowerNewDoubleElementsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer27LowerNewSmiOrObjectElementsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer27LowerNewSmiOrObjectElementsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer27LowerNewSmiOrObjectElementsEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer27LowerNewSmiOrObjectElementsEPNS1_4NodeE:
.LFB23372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16AllocationTypeOfEPKNS1_8OperatorE@PLT
	leaq	32(%r12), %rdx
	movl	%eax, %r13d
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1519
	movq	32(%r12), %rdx
	addq	$16, %rdx
.L1519:
	leaq	48(%rbx), %r12
	movq	(%rdx), %r15
	xorl	%esi, %esi
	movb	$0, -176(%rbp)
	movq	%r12, %rdi
	movb	$7, -128(%rbp)
	movl	$1, -172(%rbp)
	movq	$0, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r14
	movq	360(%rdi), %rax
	leaq	288(%rax), %rsi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-176(%rbp), %r11
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r11, %rdx
	movq	%rax, %rcx
	movq	%r11, -272(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	-240(%rbp), %r14
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler21FixedArrayMapConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	-248(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	%r14, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder19ForFixedArrayLengthEv@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	-248(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15TheHoleConstantEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movq	%rax, -264(%rbp)
	movl	$2, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$5, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	leaq	-112(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%rax, %rdx
	movq	%r9, -256(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm0
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-80(%rbp), %r10
	movb	$1, -112(%rbp)
	movups	%xmm0, 72(%rbx)
	movq	%r10, %rsi
	movq	%r10, -248(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12UintLessThanEPNS1_4NodeES4_@PLT
	movq	-272(%rbp), %r11
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$4294967295, %eax
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	-248(%rbp), %r10
	movq	-264(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	movl	$1800, %eax
	movb	$1, -240(%rbp)
	movq	%r10, %rcx
	movw	%ax, -224(%rbp)
	movl	$16, -236(%rbp)
	movb	$0, -222(%rbp)
	movl	$1, -220(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-248(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movq	-256(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-160(%rbp), %xmm1
	movq	-144(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1523
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1523:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23372:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer27LowerNewSmiOrObjectElementsEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer27LowerNewSmiOrObjectElementsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer30LowerTransitionAndStoreElementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer30LowerTransitionAndStoreElementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer30LowerTransitionAndStoreElementEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer30LowerTransitionAndStoreElementEPNS1_4NodeE:
.LFB23433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$424, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rsi), %rax
	movq	%rax, -432(%rbp)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1525
	movq	40(%rsi), %rax
	movq	%rax, -424(%rbp)
	leaq	48(%rsi), %rax
.L1526:
	leaq	-416(%rbp), %r13
	leaq	48(%rbx), %r12
	movq	%r9, -464(%rbp)
	movq	(%rax), %r14
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	-432(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13AccessBuilder15ForMapBitField2Ev@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$248, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32ShrEPNS1_4NodeES4_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movq	%rax, %r15
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$4, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -440(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	-440(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	leaq	-112(%rbp), %r11
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%r11, %rdx
	movq	%rax, %rsi
	movq	%r11, -440(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$1, %esi
	movq	%r12, %rdi
	movb	$0, -352(%rbp)
	movl	$0, -348(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -304(%rbp)
	movl	$0, -300(%rbp)
	movq	$0, -296(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_@PLT
	leaq	-352(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_@PLT
	movq	-440(%rbp), %r11
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rdx
	movq	%r11, -448(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, -440(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	-440(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	leaq	-304(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-448(%rbp), %r11
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -456(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-336(%rbp), %xmm0
	movq	%r13, %rdi
	movb	$1, -352(%rbp)
	movb	$0, -160(%rbp)
	movups	%xmm0, 72(%rbx)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rdx
	movq	%rax, %rsi
	movq	%r10, -448(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-432(%rbp), %r15
	movl	$5, %r8d
	movq	%rbx, %rdi
	movq	-464(%rbp), %r9
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	%r9, -440(%rbp)
	call	_ZN2v88internal8compiler23EffectControlLinearizer20TransitionElementsToEPNS1_4NodeES4_NS0_12ElementsKindES5_
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-456(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	movq	%r11, -432(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movdqa	-144(%rbp), %xmm1
	movq	-440(%rbp), %r9
	movl	$3, %r8d
	movb	$1, -160(%rbp)
	movups	%xmm1, 72(%rbx)
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer20TransitionElementsToEPNS1_4NodeES4_NS0_12ElementsKindES5_
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-432(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$5, %ecx
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movdqa	-288(%rbp), %xmm2
	movq	-440(%rbp), %r9
	movl	$3, %r8d
	movb	$1, -304(%rbp)
	movq	%r9, %rsi
	movups	%xmm2, 72(%rbx)
	call	_ZN2v88internal8compiler23EffectControlLinearizer20TransitionElementsToEPNS1_4NodeES4_NS0_12ElementsKindES5_
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-432(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-80(%rbp), %r8
	movq	%r13, %rdi
	movb	$1, -112(%rbp)
	movdqa	-96(%rbp), %xmm3
	movq	%r8, -432(%rbp)
	movups	%xmm3, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder19ForJSObjectElementsEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movb	$0, -256(%rbp)
	movq	%rax, %r15
	movb	$0, -208(%rbp)
	movl	$1, -252(%rbp)
	movq	$0, -248(%rbp)
	movl	$1, -204(%rbp)
	movq	$0, -200(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-432(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_@PLT
	leaq	-256(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$1, %edx
	movl	$3, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder20ForFixedArrayElementENS0_12ElementsKindENS0_15LoadSensitivityE@PLT
	movq	%r14, %r8
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	-424(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_@PLT
	leaq	-208(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -432(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-240(%rbp), %xmm4
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$1, -256(%rbp)
	movups	%xmm4, 72(%rbx)
	movb	$0, -160(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -440(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	-440(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	-448(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	movl	$32, %esi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1531
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%rax, %rsi
.L1528:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20ChangeInt32ToFloat64EPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, -440(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder26ForFixedDoubleArrayElementEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-440(%rbp), %r8
	movq	-424(%rbp), %rcx
	call	_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_@PLT
	movq	-432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm5
	movq	%r13, %rdi
	movb	$1, -160(%rbp)
	movups	%xmm5, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler17Float64SilenceNaNEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder26ForFixedDoubleArrayElementEv@PLT
	movq	%r14, %r8
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	-424(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_@PLT
	movq	-432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-192(%rbp), %xmm6
	movups	%xmm6, 72(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1532
	addq	$424, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1525:
	.cfi_restore_state
	movq	-432(%rbp), %rcx
	leaq	16(%rcx), %rax
	movq	16(%rcx), %rcx
	addq	$16, %rax
	movq	%rcx, -432(%rbp)
	movq	-8(%rax), %rcx
	movq	%rcx, -424(%rbp)
	jmp	.L1526
	.p2align 4,,10
	.p2align 3
.L1531:
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%rax, %rsi
	jmp	.L1528
.L1532:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23433:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer30LowerTransitionAndStoreElementEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer30LowerTransitionAndStoreElementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer19LowerFloat64RoundUpEPNS1_4NodeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler23EffectControlLinearizer19LowerFloat64RoundUpEPNS1_4NodeE.part.0, @function
_ZN2v88internal8compiler23EffectControlLinearizer19LowerFloat64RoundUpEPNS1_4NodeE.part.0:
.LFB28972:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$360, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1534
	movq	16(%r13), %r13
.L1534:
	leaq	48(%rbx), %r12
	pxor	%xmm0, %xmm0
	movb	$0, -368(%rbp)
	leaq	-112(%rbp), %r15
	movq	%r12, %rdi
	movb	$0, -320(%rbp)
	movl	$0, -364(%rbp)
	movq	$0, -360(%rbp)
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	movb	$0, -272(%rbp)
	movl	$0, -268(%rbp)
	movq	$0, -264(%rbp)
	movb	$0, -224(%rbp)
	movl	$0, -220(%rbp)
	movq	$0, -216(%rbp)
	movb	$0, -176(%rbp)
	movl	$1, -172(%rbp)
	movq	$0, -168(%rbp)
	movb	$13, -128(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$13, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movsd	.LC9(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movsd	.LC10(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	-376(%rbp), %r10
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, -392(%rbp)
	movq	%r10, %rsi
	movq	%r10, -384(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_@PLT
	leaq	-368(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler22Float64LessThanOrEqualEPNS1_4NodeES4_@PLT
	leaq	-320(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64AddEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_@PLT
	movq	-376(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-376(%rbp), %r8
	movq	-392(%rbp), %r9
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10Float64AddEPNS1_4NodeES4_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movdqa	-304(%rbp), %xmm1
	movb	$1, -320(%rbp)
	movups	%xmm1, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-352(%rbp), %xmm2
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-384(%rbp), %r10
	movb	$1, -368(%rbp)
	movups	%xmm2, 72(%rbx)
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	leaq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movsd	.LC11(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler22Float64LessThanOrEqualEPNS1_4NodeES4_@PLT
	leaq	-272(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movsd	.LC12(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10Float64AddEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	-376(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r14
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_@PLT
	leaq	-176(%rbp), %r10
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%r10, %rdx
	movq	%rax, %rsi
	movq	%r10, -376(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-392(%rbp), %r9
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	-376(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-160(%rbp), %xmm3
	movq	-384(%rbp), %r8
	movq	%r12, %rdi
	movq	-144(%rbp), %rdx
	movb	$1, -176(%rbp)
	movq	%r8, %rsi
	movups	%xmm3, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movdqa	-256(%rbp), %xmm4
	movb	$1, -272(%rbp)
	movups	%xmm4, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movdqa	-208(%rbp), %xmm5
	movb	$1, -224(%rbp)
	movups	%xmm5, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm6
	movq	-80(%rbp), %rdx
	movups	%xmm6, 72(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1537
	addq	$360, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1537:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28972:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer19LowerFloat64RoundUpEPNS1_4NodeE.part.0, .-_ZN2v88internal8compiler23EffectControlLinearizer19LowerFloat64RoundUpEPNS1_4NodeE.part.0
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer19LowerFloat64RoundUpEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer19LowerFloat64RoundUpEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer19LowerFloat64RoundUpEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer19LowerFloat64RoundUpEPNS1_4NodeE:
.LFB23440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Float64RoundUpEv@PLT
	testb	%al, %al
	jne	.L1541
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler23EffectControlLinearizer19LowerFloat64RoundUpEPNS1_4NodeE.part.0
	.p2align 4,,10
	.p2align 3
.L1541:
	.cfi_restore_state
	popq	%r12
	xorl	%eax, %eax
	xorl	%edx, %edx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23440:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer19LowerFloat64RoundUpEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer19LowerFloat64RoundUpEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer21BuildFloat64RoundDownEPNS1_4NodeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler23EffectControlLinearizer21BuildFloat64RoundDownEPNS1_4NodeE.part.0, @function
_ZN2v88internal8compiler23EffectControlLinearizer21BuildFloat64RoundDownEPNS1_4NodeE.part.0:
.LFB28973:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	48(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$344, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, -352(%rbp)
	movl	$0, -348(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -304(%rbp)
	movl	$0, -300(%rbp)
	movq	$0, -296(%rbp)
	movb	$0, -256(%rbp)
	movl	$0, -252(%rbp)
	movq	$0, -248(%rbp)
	movb	$0, -208(%rbp)
	movl	$1, -204(%rbp)
	movq	$0, -200(%rbp)
	movb	$0, -160(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$13, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movsd	.LC9(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	-360(%rbp), %r9
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	%r9, %rsi
	movq	%r9, -376(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_@PLT
	leaq	-352(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler22Float64LessThanOrEqualEPNS1_4NodeES4_@PLT
	leaq	-304(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movsd	.LC10(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10Float64AddEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_@PLT
	movq	-360(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-368(%rbp), %r10
	movq	-360(%rbp), %r8
	movq	%r12, %rdi
	movq	%r10, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movdqa	-288(%rbp), %xmm1
	movb	$1, -304(%rbp)
	movups	%xmm1, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-336(%rbp), %xmm2
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-376(%rbp), %r9
	movb	$1, -352(%rbp)
	movups	%xmm2, 72(%rbx)
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movsd	.LC11(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler22Float64LessThanOrEqualEPNS1_4NodeES4_@PLT
	leaq	-256(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movsd	.LC12(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10Float64AddEPNS1_4NodeES4_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	-360(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_@PLT
	leaq	-208(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-368(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-192(%rbp), %xmm3
	movq	%r12, %rdi
	movsd	.LC13(%rip), %xmm0
	movb	$1, -208(%rbp)
	movups	%xmm3, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movdqa	-240(%rbp), %xmm4
	movb	$1, -256(%rbp)
	movups	%xmm4, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movdqa	-144(%rbp), %xmm5
	movb	$1, -160(%rbp)
	movups	%xmm5, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm6
	movq	-80(%rbp), %rax
	movups	%xmm6, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1546
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1546:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28973:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer21BuildFloat64RoundDownEPNS1_4NodeE.part.0, .-_ZN2v88internal8compiler23EffectControlLinearizer21BuildFloat64RoundDownEPNS1_4NodeE.part.0
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer21BuildFloat64RoundDownEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer21BuildFloat64RoundDownEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer21BuildFloat64RoundDownEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer21BuildFloat64RoundDownEPNS1_4NodeE:
.LFB23441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder16Float64RoundDownEv@PLT
	testb	%al, %al
	je	.L1548
	leaq	48(%r12), %rdi
	movq	%r13, %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler16Float64RoundDownEPNS1_4NodeE@PLT
	.p2align 4,,10
	.p2align 3
.L1548:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler23EffectControlLinearizer21BuildFloat64RoundDownEPNS1_4NodeE.part.0
	.cfi_endproc
.LFE23441:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer21BuildFloat64RoundDownEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer21BuildFloat64RoundDownEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer21LowerFloat64RoundDownEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer21LowerFloat64RoundDownEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer21LowerFloat64RoundDownEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer21LowerFloat64RoundDownEPNS1_4NodeE:
.LFB23442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder16Float64RoundDownEv@PLT
	testb	%al, %al
	je	.L1551
	addq	$8, %rsp
	xorl	%eax, %eax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1551:
	.cfi_restore_state
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1557
.L1553:
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder16Float64RoundDownEv@PLT
	testb	%al, %al
	jne	.L1558
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21BuildFloat64RoundDownEPNS1_4NodeE.part.0
	movq	%rax, %rdx
.L1555:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1558:
	.cfi_restore_state
	leaq	48(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler16Float64RoundDownEPNS1_4NodeE@PLT
	movq	%rax, %rdx
	jmp	.L1555
	.p2align 4,,10
	.p2align 3
.L1557:
	movq	16(%r13), %r13
	jmp	.L1553
	.cfi_endproc
.LFE23442:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer21LowerFloat64RoundDownEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer21LowerFloat64RoundDownEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25LowerFloat64RoundTiesEvenEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25LowerFloat64RoundTiesEvenEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25LowerFloat64RoundTiesEvenEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer25LowerFloat64RoundTiesEvenEPNS1_4NodeE:
.LFB23443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Float64RoundTiesEvenEv@PLT
	testb	%al, %al
	je	.L1560
	xorl	%eax, %eax
	xorl	%edx, %edx
.L1561:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1567
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1560:
	.cfi_restore_state
	movzbl	23(%r12), %eax
	movq	32(%r12), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1568
.L1562:
	movq	(%rbx), %rax
	movb	$13, -64(%rbp)
	leaq	48(%rbx), %r12
	movb	$0, -160(%rbp)
	movq	16(%rax), %rdi
	movb	$0, -112(%rbp)
	movl	$1, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder16Float64RoundDownEv@PLT
	movq	%r14, %rsi
	testb	%al, %al
	jne	.L1569
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21BuildFloat64RoundDownEPNS1_4NodeE.part.0
	movq	%rax, %r13
.L1564:
	movq	%r14, %rsi
	movq	%r13, %rdx
	leaq	-112(%rbp), %r14
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movsd	.LC14(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	-168(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -176(%rbp)
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movsd	.LC10(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	-168(%rbp), %r9
	movq	-176(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	%r9, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64AddEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-144(%rbp), %xmm1
	movq	%r12, %rdi
	movsd	.LC15(%rip), %xmm0
	movb	$1, -160(%rbp)
	movups	%xmm1, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10Float64ModEPNS1_4NodeES4_@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	-168(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64AddEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm2
	movq	-80(%rbp), %rdx
	movl	$1, %eax
	movups	%xmm2, 72(%rbx)
	jmp	.L1561
	.p2align 4,,10
	.p2align 3
.L1569:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler16Float64RoundDownEPNS1_4NodeE@PLT
	movq	%rax, %r13
	jmp	.L1564
	.p2align 4,,10
	.p2align 3
.L1568:
	movq	16(%r14), %r14
	jmp	.L1562
.L1567:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23443:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25LowerFloat64RoundTiesEvenEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer25LowerFloat64RoundTiesEvenEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE.part.0, @function
_ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE.part.0:
.LFB28976:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	48(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$360, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, -368(%rbp)
	movl	$0, -364(%rbp)
	movq	$0, -360(%rbp)
	movb	$0, -320(%rbp)
	movl	$0, -316(%rbp)
	movq	$0, -312(%rbp)
	movb	$0, -272(%rbp)
	movl	$0, -268(%rbp)
	movq	$0, -264(%rbp)
	movb	$0, -224(%rbp)
	movl	$0, -220(%rbp)
	movq	$0, -216(%rbp)
	movb	$0, -176(%rbp)
	movl	$1, -172(%rbp)
	movq	$0, -168(%rbp)
	movb	$13, -128(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$13, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movsd	.LC9(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movsd	.LC10(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	-376(%rbp), %r10
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, -392(%rbp)
	movq	%r10, %rsi
	movq	%r10, -384(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_@PLT
	leaq	-368(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler22Float64LessThanOrEqualEPNS1_4NodeES4_@PLT
	leaq	-320(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64AddEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_@PLT
	movq	-376(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-376(%rbp), %r8
	movq	-392(%rbp), %r9
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movdqa	-304(%rbp), %xmm1
	movb	$1, -320(%rbp)
	movups	%xmm1, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-352(%rbp), %xmm2
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-384(%rbp), %r10
	movb	$1, -368(%rbp)
	movups	%xmm2, 72(%rbx)
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	leaq	-224(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movsd	.LC11(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler22Float64LessThanOrEqualEPNS1_4NodeES4_@PLT
	leaq	-272(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movsd	.LC12(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10Float64AddEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	-376(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r14
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_@PLT
	leaq	-176(%rbp), %r10
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%r10, %rdx
	movq	%rax, %rsi
	movq	%r10, -376(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-392(%rbp), %r9
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	-376(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-160(%rbp), %xmm3
	movq	-384(%rbp), %r8
	movq	%r12, %rdi
	movq	-144(%rbp), %rdx
	movb	$1, -176(%rbp)
	movq	%r8, %rsi
	movups	%xmm3, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movdqa	-256(%rbp), %xmm4
	movb	$1, -272(%rbp)
	movups	%xmm4, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movdqa	-208(%rbp), %xmm5
	movb	$1, -224(%rbp)
	movups	%xmm5, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm6
	movq	-80(%rbp), %rax
	movups	%xmm6, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1573
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1573:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28976:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE.part.0, .-_ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE.part.0
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE:
.LFB23444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Float64RoundTruncateEv@PLT
	testb	%al, %al
	je	.L1575
	leaq	48(%r12), %rdi
	movq	%r13, %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler20Float64RoundTruncateEPNS1_4NodeE@PLT
	.p2align 4,,10
	.p2align 3
.L1575:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE.part.0
	.cfi_endproc
.LFE23444:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer20LowerNumberIsIntegerEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer20LowerNumberIsIntegerEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer20LowerNumberIsIntegerEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer20LowerNumberIsIntegerEPNS1_4NodeE:
.LFB23352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1578
	movq	16(%r14), %r14
.L1578:
	movq	(%r12), %rax
	leaq	48(%r12), %r13
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Float64RoundTruncateEv@PLT
	movq	%r14, %rsi
	testb	%al, %al
	je	.L1579
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20Float64RoundTruncateEPNS1_4NodeE@PLT
	movq	%rax, %rdx
.L1580:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	.p2align 4,,10
	.p2align 3
.L1579:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE.part.0
	movq	%rax, %rdx
	jmp	.L1580
	.cfi_endproc
.LFE23352:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer20LowerNumberIsIntegerEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer20LowerNumberIsIntegerEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer25LowerFloat64RoundTruncateEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer25LowerFloat64RoundTruncateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer25LowerFloat64RoundTruncateEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer25LowerFloat64RoundTruncateEPNS1_4NodeE:
.LFB23445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Float64RoundTruncateEv@PLT
	testb	%al, %al
	je	.L1583
	addq	$8, %rsp
	xorl	%eax, %eax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1583:
	.cfi_restore_state
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1589
.L1585:
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Float64RoundTruncateEv@PLT
	testb	%al, %al
	jne	.L1590
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE.part.0
	movq	%rax, %rdx
.L1587:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1590:
	.cfi_restore_state
	leaq	48(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20Float64RoundTruncateEPNS1_4NodeE@PLT
	movq	%rax, %rdx
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1589:
	movq	16(%r13), %r13
	jmp	.L1585
	.cfi_endproc
.LFE23445:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer25LowerFloat64RoundTruncateEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer25LowerFloat64RoundTruncateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer20LowerObjectIsIntegerEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer20LowerObjectIsIntegerEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer20LowerObjectIsIntegerEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer20LowerObjectIsIntegerEPNS1_4NodeE:
.LFB23353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	32(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1592
	movq	16(%r15), %r15
.L1592:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movq	%rax, %r13
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$1, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	-112(%rbp), %r14
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	-176(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	-192(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Float64RoundTruncateEv@PLT
	movq	%r13, %rsi
	testb	%al, %al
	je	.L1593
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20Float64RoundTruncateEPNS1_4NodeE@PLT
	movq	%rax, %rdx
.L1594:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1597
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1593:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE.part.0
	movq	%rax, %rdx
	jmp	.L1594
.L1597:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23353:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer20LowerObjectIsIntegerEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer20LowerObjectIsIntegerEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer24LowerNumberIsSafeIntegerEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer24LowerNumberIsSafeIntegerEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer24LowerNumberIsSafeIntegerEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer24LowerNumberIsSafeIntegerEPNS1_4NodeE:
.LFB23354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	32(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1599
	movq	16(%r15), %r15
.L1599:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movb	$1, -64(%rbp)
	movq	%rax, %r14
	movq	(%rbx), %rax
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	16(%rax), %rdi
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Float64RoundTruncateEv@PLT
	movq	%r15, %rsi
	testb	%al, %al
	je	.L1600
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20Float64RoundTruncateEPNS1_4NodeE@PLT
	movq	%rax, %r13
.L1601:
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-112(%rbp), %r15
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movsd	.LC16(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler10Float64AbsEPNS1_4NodeE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler22Float64LessThanOrEqualEPNS1_4NodeES4_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1604
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1600:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE.part.0
	movq	%rax, %r13
	jmp	.L1601
.L1604:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23354:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer24LowerNumberIsSafeIntegerEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer24LowerNumberIsSafeIntegerEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsSafeIntegerEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsSafeIntegerEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsSafeIntegerEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsSafeIntegerEPNS1_4NodeE:
.LFB23355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	32(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1606
	movq	16(%r15), %r15
.L1606:
	leaq	48(%rbx), %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$0, -112(%rbp)
	movq	%rax, %r14
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$1, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-112(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-176(%rbp), %r14
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv@PLT
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	-184(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForHeapNumberValueEv@PLT
	movq	%r14, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%rax, -192(%rbp)
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Float64RoundTruncateEv@PLT
	movq	-192(%rbp), %rsi
	testb	%al, %al
	je	.L1607
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20Float64RoundTruncateEPNS1_4NodeE@PLT
	movq	-192(%rbp), %rsi
	movq	%rax, %r14
.L1608:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	movq	-184(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJPNS1_4NodeEEEEvS5_PNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movsd	.LC16(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler10Float64AbsEPNS1_4NodeE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler22Float64LessThanOrEqualEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-96(%rbp), %xmm1
	movq	-80(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1611
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1607:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal8compiler23EffectControlLinearizer25BuildFloat64RoundTruncateEPNS1_4NodeE.part.0
	movq	-192(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1608
.L1611:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23355:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsSafeIntegerEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsSafeIntegerEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeES5_EEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,"axG",@progbits,_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeES5_EEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeES5_EEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.type	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeES5_EEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, @function
_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeES5_EEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_:
.LFB26808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -128(%rbp)
	movq	32(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movhps	-128(%rbp), %xmm1
	cmpl	$2, 4(%rsi)
	movq	8(%rsi), %rax
	movups	%xmm1, -104(%rbp)
	je	.L1679
	testl	%eax, %eax
	jne	.L1634
	movq	%rdx, %xmm0
	movq	%r12, 24(%rsi)
	movq	24(%rdi), %rdx
	movq	%rcx, 40(%rsi)
	movq	%rdx, 16(%rsi)
	movq	%xmm0, 32(%rsi)
.L1628:
	addq	$1, %rax
	movq	%rax, 8(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1680
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1634:
	.cfi_restore_state
	movl	%eax, %ebx
	movq	16(%r14), %rax
	movq	24(%rsi), %rdi
	movq	(%rax), %r15
	cmpl	$1, %ebx
	je	.L1681
	movq	(%r15), %rsi
	movq	%r12, %rdx
	leal	1(%rbx), %r15d
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%r14), %rax
	movl	%r15d, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	24(%r13), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movslq	%ebx, %rax
	notl	%ebx
	movq	16(%r13), %rdi
	movslq	%ebx, %rbx
	leaq	0(,%rax,8), %rcx
	movq	24(%r14), %r12
	leaq	(%rbx,%rbx,2), %rax
	movq	%rcx, -128(%rbp)
	salq	$3, %rax
	movq	%rax, -136(%rbp)
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1637
	leaq	32(%rdi,%rcx), %rbx
	movq	(%rbx), %r8
	cmpq	%r8, %r12
	je	.L1639
.L1638:
	movq	-136(%rbp), %rax
	leaq	(%rdi,%rax), %rsi
	testq	%r8, %r8
	je	.L1640
	movq	%r8, %rdi
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-144(%rbp), %rsi
.L1640:
	movq	%r12, (%rbx)
	testq	%r12, %r12
	je	.L1677
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1677:
	movq	16(%r13), %rdi
.L1639:
	movq	16(%r14), %rax
	movq	24(%r13), %rdx
	leaq	-112(%rbp), %r12
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%r14), %rax
	movl	%r15d, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	16(%r13), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	xorl	%eax, %eax
.L1647:
	movq	32(%r13,%rax,8), %rdi
	leaq	1(%rax), %rbx
	movq	(%r12,%rbx,8), %r8
	movzbl	23(%rdi), %edx
	leaq	32(%rdi), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L1642
	addq	-128(%rbp), %rax
	movq	(%rax), %r11
	cmpq	%r11, %r8
	je	.L1644
.L1643:
	movq	-136(%rbp), %rcx
	leaq	(%rdi,%rcx), %rsi
	testq	%r11, %r11
	je	.L1645
	movq	%r11, %rdi
	movq	%rax, -160(%rbp)
	movq	%r8, -152(%rbp)
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-160(%rbp), %rax
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %rsi
.L1645:
	movq	%r8, (%rax)
	testq	%r8, %r8
	je	.L1678
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1678:
	movq	24(%r13,%rbx,8), %rdi
.L1644:
	movq	16(%r14), %rax
	movq	24(%r13), %rdx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%r14), %rax
	movzbl	55(%r13,%rbx), %esi
	movl	%r15d, %edx
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	24(%r13,%rbx,8), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movl	$1, %eax
	cmpq	$2, %rbx
	jne	.L1647
.L1615:
	movq	8(%r13), %rax
	jmp	.L1628
	.p2align 4,,10
	.p2align 3
.L1679:
	testl	%eax, %eax
	je	.L1682
	movq	24(%rsi), %rsi
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1617
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %rbx
	cmpq	%r12, %rdi
	je	.L1620
.L1619:
	leaq	-48(%rsi), %r15
	testq	%rdi, %rdi
	je	.L1621
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1621:
	movq	%r12, (%rbx)
	testq	%r12, %r12
	je	.L1620
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1620:
	movq	16(%r13), %rsi
	movq	24(%r14), %r14
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1622
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %rbx
	cmpq	%rdi, %r14
	je	.L1626
.L1624:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L1625
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L1625:
	movq	%r14, (%rbx)
	testq	%r14, %r14
	je	.L1626
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1626:
	xorl	%eax, %eax
	leaq	-112(%rbp), %r12
.L1627:
	movq	32(%r13,%rax,8), %rsi
	leaq	1(%rax), %rbx
	movq	(%r12,%rbx,8), %r14
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1629
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %r15
	cmpq	%rdi, %r14
	je	.L1633
.L1631:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L1632
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-128(%rbp), %rsi
.L1632:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L1633
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1633:
	movl	$1, %eax
	cmpq	$2, %rbx
	jne	.L1627
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1682:
	movq	16(%rdi), %rax
	movq	%r12, %xmm0
	movl	$2, %esi
	leaq	-80(%rbp), %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4LoopEi@PLT
	movdqa	-128(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%rbx, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, 24(%r13)
	movq	%rax, %r12
	movq	16(%r14), %rax
	movq	24(%r14), %rcx
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	movq	%rcx, -128(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$3, %edx
	movq	-128(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r12, -64(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r13), %r12
	movq	%rax, 16(%r13)
	movq	%rax, -128(%rbp)
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TerminateEv@PLT
	movq	%r12, %xmm3
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	-128(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	leaq	-112(%rbp), %r12
	movl	$2, %edx
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rdx
	movq	16(%r14), %rax
	movq	8(%rax), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	xorl	%edx, %edx
.L1616:
	movq	16(%r14), %rax
	movq	24(%r13), %rcx
	leaq	1(%rdx), %r9
	movzbl	56(%r13,%rdx), %esi
	movl	$2, %edx
	movq	%r9, -136(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	movq	%rcx, -144(%rbp)
	movq	(%r12,%r9,8), %rcx
	movq	%rcx, -128(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movl	$3, %edx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	-144(%rbp), %rcx
	movq	-128(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%rcx, -64(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-136(%rbp), %r9
	movl	$1, %edx
	movq	%rax, 24(%r13,%r9,8)
	cmpq	$2, %r9
	jne	.L1616
	movq	8(%r13), %rax
	jmp	.L1628
	.p2align 4,,10
	.p2align 3
.L1637:
	movq	32(%rdi), %rax
	leaq	16(%rax,%rcx), %rbx
	movq	(%rbx), %r8
	cmpq	%r8, %r12
	je	.L1639
	movq	%rax, %rdi
	jmp	.L1638
	.p2align 4,,10
	.p2align 3
.L1642:
	movq	32(%rdi), %rdx
	movq	-128(%rbp), %rax
	leaq	16(%rdx,%rax), %rax
	movq	(%rax), %r11
	cmpq	%r11, %r8
	je	.L1644
	movq	%rdx, %rdi
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1629:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L1633
	leaq	24(%rsi), %r15
	jmp	.L1631
	.p2align 4,,10
	.p2align 3
.L1681:
	movq	%r12, %xmm4
	movq	%rdi, %xmm0
	movq	8(%rax), %rdi
	leaq	-80(%rbp), %rbx
	punpcklqdq	%xmm4, %xmm0
	movl	$2, %esi
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movdqa	-128(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%rbx, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, 24(%r13)
	movq	%rax, %r12
	movq	24(%r14), %rcx
	movq	16(%r14), %rax
	movq	%rcx, -128(%rbp)
	movq	16(%r13), %rcx
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movl	$3, %edx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	-136(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r12, -64(%rbp)
	leaq	-112(%rbp), %r12
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%edx, %edx
	movq	%rax, 16(%r13)
.L1636:
	movq	24(%r13), %rcx
	leaq	1(%rdx), %r15
	movq	16(%r14), %rax
	movzbl	56(%r13,%rdx), %esi
	movl	$2, %edx
	movq	(%rax), %r11
	movq	8(%rax), %rdi
	movq	%rcx, -144(%rbp)
	movq	(%r12,%r15,8), %rcx
	movq	%r11, -152(%rbp)
	movq	%rcx, -128(%rbp)
	movq	24(%r13,%r15,8), %rcx
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	-144(%rbp), %rcx
	movl	$3, %edx
	xorl	%r8d, %r8d
	movq	-136(%rbp), %xmm0
	movq	-152(%rbp), %r11
	movq	%rax, %rsi
	movq	%rcx, -64(%rbp)
	movq	%rbx, %rcx
	movhps	-128(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %edx
	movq	%rax, 24(%r13,%r15,8)
	cmpq	$2, %r15
	jne	.L1636
	movq	8(%r13), %rax
	jmp	.L1628
	.p2align 4,,10
	.p2align 3
.L1617:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%r12, %rdi
	je	.L1620
	leaq	24(%rsi), %rbx
	jmp	.L1619
	.p2align 4,,10
	.p2align 3
.L1622:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L1626
	leaq	24(%rsi), %rbx
	jmp	.L1624
.L1680:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26808:
	.size	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeES5_EEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, .-_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeES5_EEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer21LowerStringCharCodeAtEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringCharCodeAtEPNS1_4NodeE
	.type	_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringCharCodeAtEPNS1_4NodeE, @function
_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringCharCodeAtEPNS1_4NodeE:
.LFB23380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$840, %rsp
	movq	-8(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	-17(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1685
	leaq	16(%rdx), %rsi
	movq	16(%rdx), %rdx
	addq	$8, %rsi
.L1685:
	movl	$1288, %eax
	leaq	48(%rbx), %r12
	movq	(%rsi), %rcx
	movl	$1288, %esi
	movw	%ax, -200(%rbp)
	leaq	-256(%rbp), %rax
	movq	%r12, %rdi
	leaq	-816(%rbp), %r13
	movw	%si, -136(%rbp)
	movq	%rax, %rsi
	movq	%rax, -864(%rbp)
	movb	$0, -256(%rbp)
	movl	$2, -252(%rbp)
	movq	$0, -248(%rbp)
	movb	$0, -192(%rbp)
	movl	$1, -188(%rbp)
	movq	$0, -184(%rbp)
	movb	$0, -320(%rbp)
	movl	$1, -316(%rbp)
	movq	$0, -312(%rbp)
	movb	$4, -272(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeES5_EEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-240(%rbp), %xmm1
	movq	-224(%rbp), %r15
	movq	%r13, %rdi
	movq	-216(%rbp), %rax
	movb	$1, -256(%rbp)
	movups	%xmm1, 72(%rbx)
	movq	%rax, -824(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder6ForMapEv@PLT
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13AccessBuilder18ForMapInstanceTypeEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$7, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r14, -832(%rbp)
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movb	$0, -752(%rbp)
	movq	%rax, %r14
	movb	$0, -704(%rbp)
	movl	$1, -748(%rbp)
	movq	$0, -744(%rbp)
	movl	$1, -700(%rbp)
	movq	$0, -696(%rbp)
	movb	$0, -656(%rbp)
	movl	$1, -652(%rbp)
	movq	$0, -648(%rbp)
	movb	$0, -608(%rbp)
	movl	$1, -604(%rbp)
	movq	$0, -600(%rbp)
	movb	$0, -560(%rbp)
	movl	$1, -556(%rbp)
	movq	$0, -552(%rbp)
	movb	$0, -512(%rbp)
	movl	$1, -508(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -464(%rbp)
	movl	$1, -460(%rbp)
	movq	$0, -456(%rbp)
	movb	$0, -416(%rbp)
	movl	$0, -412(%rbp)
	movq	$0, -408(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler20Int32LessThanOrEqualEPNS1_4NodeES4_@PLT
	leaq	-704(%rbp), %rcx
	movl	$2, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-752(%rbp), %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movdqa	-736(%rbp), %xmm2
	movq	%r12, %rdi
	movl	$1, %esi
	movb	$1, -752(%rbp)
	movups	%xmm2, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	leaq	-656(%rbp), %rcx
	movl	$2, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-608(%rbp), %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movdqa	-688(%rbp), %xmm3
	movq	%r12, %rdi
	movl	$5, %esi
	movb	$1, -704(%rbp)
	movups	%xmm3, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	leaq	-560(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	leaq	-512(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	leaq	-416(%rbp), %r9
	movl	$2, %r8d
	movq	%r12, %rdi
	movq	%r9, %rcx
	leaq	-464(%rbp), %rdx
	movq	%rax, %rsi
	movq	%r9, -848(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movdqa	-640(%rbp), %xmm4
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$1, -656(%rbp)
	movups	%xmm4, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, -840(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-832(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movq	-840(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	-320(%rbp), %r14
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	-824(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler23EffectControlLinearizer17LoadFromSeqStringEPNS1_4NodeES4_S4_
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-592(%rbp), %xmm5
	movq	%r13, %rdi
	movb	$1, -608(%rbp)
	movups	%xmm5, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder19ForConsStringSecondEv@PLT
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%rax, -840(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler19EmptyStringConstantEv@PLT
	movq	-840(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	-848(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder18ForConsStringFirstEv@PLT
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	leaq	-192(%rbp), %r10
	movq	-824(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%rax, %rdx
	movq	%r10, -840(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeES5_EEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-544(%rbp), %xmm6
	movq	%r13, %rdi
	movb	$1, -560(%rbp)
	movups	%xmm6, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder19ForThinStringActualEv@PLT
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	-840(%rbp), %r10
	movq	-824(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	movq	%r10, -856(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeES5_EEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-496(%rbp), %xmm7
	movq	%r12, %rdi
	movl	$16, %esi
	movb	$1, -512(%rbp)
	movups	%xmm7, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	movq	%rax, -840(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-832(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movq	-840(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	-848(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6GotoIfIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder29ForExternalStringResourceDataEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movb	$0, -128(%rbp)
	movq	%rax, -848(%rbp)
	movb	$0, -368(%rbp)
	movl	$1, -364(%rbp)
	movq	$0, -360(%rbp)
	movl	$1, -124(%rbp)
	movq	$0, -120(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, -840(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-832(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_@PLT
	movq	-840(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	leaq	-128(%rbp), %r11
	movl	$2, %r8d
	movq	%r12, %rdi
	movq	%r11, %rdx
	movq	%rax, %rsi
	leaq	-368(%rbp), %rcx
	movq	%r11, -872(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE@PLT
	movdqa	-352(%rbp), %xmm1
	movl	$770, %esi
	movq	%r12, %rdi
	movq	-848(%rbp), %r9
	movq	-824(%rbp), %rcx
	movb	$1, -368(%rbp)
	movups	%xmm1, 72(%rbx)
	movq	%r9, %rdx
	movq	%r9, -832(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-112(%rbp), %xmm2
	movq	%r12, %rdi
	movb	$1, -128(%rbp)
	movl	$1, %esi
	movups	%xmm2, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-824(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	-832(%rbp), %r9
	movl	$771, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-448(%rbp), %xmm3
	movq	%r13, %rdi
	movb	$1, -464(%rbp)
	movups	%xmm3, 72(%rbx)
	call	_ZN2v88internal8compiler13AccessBuilder21ForSlicedStringOffsetEv@PLT
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	movq	%rax, -832(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder21ForSlicedStringParentEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-832(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	-824(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movq	-856(%rbp), %r10
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeES5_EEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%rbx), %rax
	xorl	%r8d, %r8d
	movl	$96, %ecx
	movdqa	-400(%rbp), %xmm4
	movl	$2, %edx
	movl	$327, %esi
	movb	$1, -416(%rbp)
	movups	%xmm4, 72(%rbx)
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage24GetRuntimeCallDescriptorEPNS0_4ZoneENS0_7Runtime10FunctionIdEiNS_4base5FlagsINS1_8Operator8PropertyEhEENS8_INS1_14CallDescriptor4FlagEiEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -832(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movl	$327, %edi
	movq	%rax, -840(%rbp)
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, -848(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-824(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -856(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler18CEntryStubConstantEi@PLT
	movq	%r13, %rsi
	movq	%rax, -824(%rbp)
	movq	64(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r15, %xmm5
	movdqu	72(%rbx), %xmm6
	xorl	%r8d, %r8d
	movq	-824(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-872(%rbp), %r11
	movaps	%xmm6, -80(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movq	%r11, %rcx
	movaps	%xmm0, -128(%rbp)
	movq	-856(%rbp), %xmm0
	movhps	-848(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-840(%rbp), %xmm0
	movhps	-832(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movl	24(%rax), %edx
	movl	28(%rax), %eax
	leal	6(%rdx,%rax), %edx
	movq	64(%rbx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%rax, 72(%rbx)
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1690
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%rax, %rdx
.L1687:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-176(%rbp), %xmm7
	movq	%r12, %rdi
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %rdx
	movq	-864(%rbp), %rsi
	movb	$1, -192(%rbp)
	movups	%xmm7, 72(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeES5_EEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movdqa	-304(%rbp), %xmm1
	movq	-288(%rbp), %rax
	movups	%xmm1, 72(%rbx)
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1691
	addq	$840, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1690:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%rax, %rdx
	jmp	.L1687
.L1691:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23380:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringCharCodeAtEPNS1_4NodeE, .-_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringCharCodeAtEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler23EffectControlLinearizer20TryWireInStateEffectEPNS1_4NodeES4_PS4_S5_.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"No frame state (zapped by #%d: %s)"
	.align 8
.LC18:
	.string	"Effect control linearizer lowering of '%s': value output count does not agree."
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer20TryWireInStateEffectEPNS1_4NodeES4_PS4_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer20TryWireInStateEffectEPNS1_4NodeES4_PS4_S5_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer20TryWireInStateEffectEPNS1_4NodeES4_PS4_S5_, @function
_ZN2v88internal8compiler23EffectControlLinearizer20TryWireInStateEffectEPNS1_4NodeES4_PS4_S5_:
.LFB23259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	48(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$168, %rsp
	movq	%rdx, -192(%rbp)
	movq	(%rcx), %rsi
	movq	(%r8), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -184(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler5ResetEPNS1_4NodeES4_@PLT
	movq	(%r12), %rdi
	movzwl	16(%rdi), %eax
	subl	$60, %eax
	cmpw	$333, %ax
	ja	.L1878
	leaq	.L1695(%rip), %rdx
	movzwl	%ax, %eax
	movq	-184(%rbp), %r9
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler23EffectControlLinearizer20TryWireInStateEffectEPNS1_4NodeES4_PS4_S5_,"a",@progbits
	.align 4
	.align 4
.L1695:
	.long	.L1844-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1835-.L1695
	.long	.L1843-.L1695
	.long	.L1842-.L1695
	.long	.L1841-.L1695
	.long	.L1840-.L1695
	.long	.L1839-.L1695
	.long	.L1822-.L1695
	.long	.L1838-.L1695
	.long	.L1837-.L1695
	.long	.L1836-.L1695
	.long	.L1835-.L1695
	.long	.L1834-.L1695
	.long	.L1833-.L1695
	.long	.L1832-.L1695
	.long	.L1831-.L1695
	.long	.L1830-.L1695
	.long	.L1829-.L1695
	.long	.L1828-.L1695
	.long	.L1827-.L1695
	.long	.L1826-.L1695
	.long	.L1825-.L1695
	.long	.L1824-.L1695
	.long	.L1823-.L1695
	.long	.L1822-.L1695
	.long	.L1821-.L1695
	.long	.L1820-.L1695
	.long	.L1819-.L1695
	.long	.L1818-.L1695
	.long	.L1817-.L1695
	.long	.L1816-.L1695
	.long	.L1815-.L1695
	.long	.L1814-.L1695
	.long	.L1813-.L1695
	.long	.L1812-.L1695
	.long	.L1811-.L1695
	.long	.L1810-.L1695
	.long	.L1809-.L1695
	.long	.L1808-.L1695
	.long	.L1807-.L1695
	.long	.L1806-.L1695
	.long	.L1805-.L1695
	.long	.L1804-.L1695
	.long	.L1803-.L1695
	.long	.L1802-.L1695
	.long	.L1801-.L1695
	.long	.L1800-.L1695
	.long	.L1799-.L1695
	.long	.L1798-.L1695
	.long	.L1797-.L1695
	.long	.L1796-.L1695
	.long	.L1795-.L1695
	.long	.L1794-.L1695
	.long	.L1793-.L1695
	.long	.L1792-.L1695
	.long	.L1791-.L1695
	.long	.L1790-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1789-.L1695
	.long	.L1788-.L1695
	.long	.L1787-.L1695
	.long	.L1786-.L1695
	.long	.L1785-.L1695
	.long	.L1784-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1783-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1782-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1781-.L1695
	.long	.L1780-.L1695
	.long	.L1779-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1778-.L1695
	.long	.L1777-.L1695
	.long	.L1776-.L1695
	.long	.L1878-.L1695
	.long	.L1775-.L1695
	.long	.L1774-.L1695
	.long	.L1773-.L1695
	.long	.L1772-.L1695
	.long	.L1771-.L1695
	.long	.L1770-.L1695
	.long	.L1769-.L1695
	.long	.L1768-.L1695
	.long	.L1767-.L1695
	.long	.L1766-.L1695
	.long	.L1765-.L1695
	.long	.L1764-.L1695
	.long	.L1878-.L1695
	.long	.L1763-.L1695
	.long	.L1762-.L1695
	.long	.L1761-.L1695
	.long	.L1760-.L1695
	.long	.L1759-.L1695
	.long	.L1758-.L1695
	.long	.L1757-.L1695
	.long	.L1756-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1755-.L1695
	.long	.L1754-.L1695
	.long	.L1753-.L1695
	.long	.L1752-.L1695
	.long	.L1751-.L1695
	.long	.L1750-.L1695
	.long	.L1749-.L1695
	.long	.L1748-.L1695
	.long	.L1747-.L1695
	.long	.L1878-.L1695
	.long	.L1746-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1745-.L1695
	.long	.L1878-.L1695
	.long	.L1744-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1743-.L1695
	.long	.L1878-.L1695
	.long	.L1742-.L1695
	.long	.L1741-.L1695
	.long	.L1740-.L1695
	.long	.L1739-.L1695
	.long	.L1738-.L1695
	.long	.L1737-.L1695
	.long	.L1736-.L1695
	.long	.L1735-.L1695
	.long	.L1734-.L1695
	.long	.L1733-.L1695
	.long	.L1732-.L1695
	.long	.L1731-.L1695
	.long	.L1730-.L1695
	.long	.L1729-.L1695
	.long	.L1728-.L1695
	.long	.L1727-.L1695
	.long	.L1726-.L1695
	.long	.L1725-.L1695
	.long	.L1724-.L1695
	.long	.L1723-.L1695
	.long	.L1722-.L1695
	.long	.L1721-.L1695
	.long	.L1720-.L1695
	.long	.L1719-.L1695
	.long	.L1718-.L1695
	.long	.L1717-.L1695
	.long	.L1716-.L1695
	.long	.L1715-.L1695
	.long	.L1714-.L1695
	.long	.L1713-.L1695
	.long	.L1712-.L1695
	.long	.L1711-.L1695
	.long	.L1710-.L1695
	.long	.L1709-.L1695
	.long	.L1708-.L1695
	.long	.L1878-.L1695
	.long	.L1707-.L1695
	.long	.L1706-.L1695
	.long	.L1705-.L1695
	.long	.L1704-.L1695
	.long	.L1703-.L1695
	.long	.L1702-.L1695
	.long	.L1701-.L1695
	.long	.L1700-.L1695
	.long	.L1699-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1878-.L1695
	.long	.L1698-.L1695
	.long	.L1878-.L1695
	.long	.L1697-.L1695
	.long	.L1696-.L1695
	.long	.L1694-.L1695
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer20TryWireInStateEffectEPNS1_4NodeES4_PS4_S5_
	.p2align 4,,10
	.p2align 3
.L1878:
	xorl	%eax, %eax
.L1692:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1888
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1822:
	.cfi_restore_state
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer28LowerTruncateTaggedToFloat64EPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L1845:
	xorl	%eax, %eax
	testq	%r14, %r14
	setne	%al
.L1854:
	cmpl	32(%rdi), %eax
	jne	.L1889
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20ExtractCurrentEffectEv@PLT
	movq	%r15, %rdi
	movq	%rax, (%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler21ExtractCurrentControlEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, 0(%r13)
	movq	(%rbx), %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler14NodeProperties11ReplaceUsesEPNS1_4NodeES4_S4_S4_S4_@PLT
	movl	$1, %eax
	jmp	.L1692
.L1835:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1793:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer36LowerCheckedCompressedToTaggedSignedEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1794:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer33LowerCheckedTaggedToTaggedPointerEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1795:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedTaggedToTaggedSignedEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1796:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedTaggedToInt64EPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1797:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer27LowerCheckedTaggedToFloat64EPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1798:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer34LowerCheckedTruncateTaggedToWord32EPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1799:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer25LowerCheckedTaggedToInt32EPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1800:
	cmpq	$0, -192(%rbp)
	je	.L1890
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedTaggedSignedToInt32EPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1801:
	movq	%r9, -184(%rbp)
	call	_ZN2v88internal8compiler26CheckMinusZeroParametersOfEPKNS1_8OperatorE@PLT
	movzbl	23(%r12), %edx
	leaq	32(%r12), %rcx
	movq	-184(%rbp), %r9
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L1862
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rcx
.L1862:
	movq	(%rcx), %rcx
	movzbl	(%rax), %esi
	movq	%r9, %rdi
	leaq	8(%rax), %rdx
	movq	-192(%rbp), %r8
	call	_ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt64ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1803:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedUint64ToTaggedSignedEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1804:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1860
	movq	16(%r14), %r14
.L1860:
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movl	$2147483647, %esi
	movq	%r15, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler21Uint64LessThanOrEqualEPNS1_4NodeES4_@PLT
	movq	-184(%rbp), %r11
	movl	$1, %r9d
	movq	%r15, %rdi
	movq	-192(%rbp), %r8
	movq	%rax, %rcx
	movl	$16, %esi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1802:
	movq	%r9, -184(%rbp)
	call	_ZN2v88internal8compiler26CheckMinusZeroParametersOfEPKNS1_8OperatorE@PLT
	movzbl	23(%r12), %edx
	leaq	32(%r12), %rcx
	movq	-184(%rbp), %r9
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L1861
	movq	32(%r12), %rsi
	leaq	16(%rsi), %rcx
.L1861:
	movq	(%rcx), %rcx
	movzbl	(%rax), %esi
	movq	%r9, %rdi
	leaq	8(%rax), %rdx
	movq	-192(%rbp), %r8
	call	_ZN2v88internal8compiler23EffectControlLinearizer26BuildCheckedFloat64ToInt32ENS1_21CheckForMinusZeroModeERKNS1_14FeedbackSourceEPNS1_4NodeES8_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1805:
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	%rax, %r11
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1858
	movq	32(%r12), %r14
	leaq	40(%r12), %rax
.L1859:
	movq	(%rax), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r11, -184(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14Uint64LessThanEPNS1_4NodeES4_@PLT
	movq	%r15, %rdi
	xorl	%r9d, %r9d
	movl	$28, %esi
	movq	-184(%rbp), %r11
	movq	-192(%rbp), %r8
	movq	%rax, %rcx
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	(%r12), %rdi
	jmp	.L1845
.L1806:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer32LowerCheckedUint32ToTaggedSignedEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1809:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedInt64ToTaggedSignedEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1810:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1856
	movq	16(%rdx), %rdx
.L1856:
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	movq	-184(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, -200(%rbp)
	movq	%rdx, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	-184(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word64EqualEPNS1_4NodeES4_@PLT
	movq	-200(%rbp), %r11
	movq	%r15, %rdi
	movq	-192(%rbp), %r8
	movq	%rax, %rcx
	movl	$1, %r9d
	movl	$16, %esi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	(%r12), %rdi
	jmp	.L1845
.L1811:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer31LowerCheckedInt32ToTaggedSignedEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1812:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer35LowerCheckedInt32ToCompressedSignedEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1807:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1857
	movq	16(%r14), %r14
.L1857:
	call	_ZN2v88internal8compiler17CheckParametersOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_@PLT
	movq	-184(%rbp), %r11
	movq	%r15, %rdi
	movq	-192(%rbp), %r8
	movq	%rax, %rcx
	movl	$1, %r9d
	movl	$16, %esi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	(%r12), %rdi
	jmp	.L1845
.L1808:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer24LowerCheckedUint32BoundsEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1717:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1868
	movq	16(%r8), %r8
.L1868:
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r8, -184(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-184(%rbp), %r8
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_@PLT
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1718:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21LowerObjectIsReceiverEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1719:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsNumberEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1720:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsNonCallableEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1721:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1867
	movq	16(%rsi), %rsi
.L1867:
	movq	%rsi, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1722:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer16LowerObjectIsNaNEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1723:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer22LowerNumberIsMinusZeroEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1724:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer22LowerObjectIsMinusZeroEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1725:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer31LowerObjectIsDetectableCallableEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1726:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsConstructorEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1727:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21LowerObjectIsCallableEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1728:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsBigIntEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1729:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer28LowerObjectIsArrayBufferViewEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1730:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer20LowerObjectIsIntegerEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1731:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer24LowerNumberIsSafeIntegerEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1732:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer24LowerObjectIsSafeIntegerEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1733:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer20LowerNumberIsIntegerEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1734:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer25LowerObjectIsFiniteNumberEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1735:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1872
	movq	16(%rsi), %rsi
.L1872:
	movq	%rsi, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1736:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1871
	movq	16(%r8), %r8
.L1871:
	movl	$-524289, %esi
	movq	%r15, %rdi
	movq	%r8, -184(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi@PLT
	movq	-184(%rbp), %r8
	movq	%r15, %rdi
	movq	%rax, %r14
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE@PLT
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1737:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer14LowerToBooleanEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1738:
	movq	%r9, %rdi
	movq	%r12, %rsi
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler23EffectControlLinearizer39LowerTransitionAndStoreNonNumberElementEPNS1_4NodeE
	movq	(%r12), %rdi
	xorl	%eax, %eax
	jmp	.L1854
.L1739:
	movq	%r9, %rdi
	movq	%r12, %rsi
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler23EffectControlLinearizer36LowerTransitionAndStoreNumberElementEPNS1_4NodeE
	movq	(%r12), %rdi
	xorl	%eax, %eax
	jmp	.L1854
.L1740:
	movq	%r9, %rdi
	movq	%r12, %rsi
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler23EffectControlLinearizer30LowerTransitionAndStoreElementEPNS1_4NodeE
	movq	(%r12), %rdi
	xorl	%eax, %eax
	jmp	.L1854
.L1741:
	movq	%r9, %rdi
	movq	%r12, %rsi
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler23EffectControlLinearizer28LowerStoreSignedSmallElementEPNS1_4NodeE
	movq	(%r12), %rdi
	xorl	%eax, %eax
	jmp	.L1854
.L1742:
	movq	%r9, %rdi
	movq	%r12, %rsi
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler23EffectControlLinearizer25LowerStoreDataViewElementEPNS1_4NodeE
	movq	(%r12), %rdi
	xorl	%eax, %eax
	jmp	.L1854
.L1743:
	movq	%r9, %rdi
	movq	%r12, %rsi
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler23EffectControlLinearizer22LowerStoreTypedElementEPNS1_4NodeE
	movq	(%r12), %rdi
	xorl	%eax, %eax
	jmp	.L1854
.L1744:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer24LowerLoadDataViewElementEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1745:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21LowerLoadTypedElementEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1746:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21LowerLoadFieldByIndexEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1747:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1874
	movq	16(%r14), %r14
.L1874:
	call	_ZN2v88internal8compiler16AllocationTypeOfEPKNS1_8OperatorE@PLT
	movq	%r14, %rdx
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1748:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer11LowerTypeOfEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1749:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer33LowerConvertTaggedHoleToUndefinedEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1750:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer20LowerConvertReceiverEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1751:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCompareMapsEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1752:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L1875
	leaq	40(%r12), %rax
.L1876:
	movq	(%rax), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movl	$1, %r9d
	movl	$38, %esi
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	movq	-192(%rbp), %r8
	movq	%rax, %rcx
	leaq	-176(%rbp), %rdx
.L1887:
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	(%r12), %rdi
	xorl	%eax, %eax
	jmp	.L1854
.L1753:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler23EffectControlLinearizer34LowerCheckEqualsInternalizedStringEPNS1_4NodeES4_
	movq	(%r12), %rdi
	xorl	%eax, %eax
	jmp	.L1854
.L1813:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32MulEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1814:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckedUint32ModEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1815:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckedUint32DivEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1816:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32ModEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1817:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32DivEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1818:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32SubEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1819:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer20LowerCheckedInt32AddEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1820:
	movq	%r9, %rdi
	leaq	-112(%rbp), %rdx
	movq	%r12, %rsi
	movb	$0, -112(%rbp)
	movq	%r9, -184(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$1, -64(%rbp)
	call	_ZN2v88internal8compiler23EffectControlLinearizer26TruncateTaggedPointerToBitEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm1EEE
	movdqa	-96(%rbp), %xmm0
	movq	-184(%rbp), %r9
	movq	-80(%rbp), %r14
	movups	%xmm0, 72(%r9)
	movq	(%r12), %rdi
	jmp	.L1845
.L1821:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer24LowerTruncateTaggedToBitEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1823:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer27LowerTruncateTaggedToWord32EPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1824:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer27LowerTruncateBigIntToUint64EPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1825:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint64ToBigIntEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1826:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer22LowerChangeBitToTaggedEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1827:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1852
	movq	16(%r14), %r14
.L1852:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler12TrueConstantEv@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1828:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer33LowerChangeFloat64ToTaggedPointerEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1829:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer26LowerChangeFloat64ToTaggedEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1757:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckStringEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1758:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer35LowerCheckReceiverOrNullOrUndefinedEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1759:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer18LowerCheckReceiverEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1760:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer28LowerCheckInternalizedStringEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1833:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1848
	movq	16(%r14), %r14
.L1848:
	movq	(%r9), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1891
.L1849:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1834:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1846
	movq	16(%r14), %r14
.L1846:
	movq	(%r9), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L1892
.L1847:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1837:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer35LowerChangeCompressedToTaggedSignedEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1838:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer31LowerChangeTaggedToTaggedSignedEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1755:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21LowerCheckFloat64HoleEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1756:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckSymbolEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1754:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1873
	movq	16(%r14), %r14
.L1873:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler15TheHoleConstantEv@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_@PLT
	movq	-192(%rbp), %r8
	movq	%r15, %rdi
	leaq	-176(%rbp), %rdx
	movq	%rax, %rcx
	movl	$6, %esi
	movl	$1, %r9d
	movq	$0, -176(%rbp)
	movl	$-1, -168(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE@PLT
	movq	(%r12), %rdi
	jmp	.L1845
.L1781:
	movl	44(%rdi), %ecx
	leaq	32(%r12), %r14
	cmpl	$64, %ecx
	jne	.L1864
	movzbl	23(%r12), %eax
	movq	32(%r12), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1845
	movq	16(%r14), %r14
	jmp	.L1845
.L1782:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer19LowerNumberToStringEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1708:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer18LowerNewConsStringEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1709:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer25LowerNewArgumentsElementsEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1710:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer27LowerNewSmiOrObjectElementsEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1711:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer22LowerNewDoubleElementsEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1700:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer15LowerAssertTypeEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1701:
	movq	%r9, %rdi
	movq	%r12, %rsi
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler23EffectControlLinearizer17LowerRuntimeAbortEPNS1_4NodeE
	movq	(%r12), %rdi
	xorl	%eax, %eax
	jmp	.L1854
.L1702:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1853
	movq	16(%r14), %r14
.L1853:
	cmpl	$1, 24(%r9)
	jne	.L1845
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler25Word32PoisonOnSpeculationEPNS1_4NodeE@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1703:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer39LowerFindOrderedHashMapEntryForInt32KeyEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1698:
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21LowerFloat64RoundDownEPNS1_4NodeE
	testb	%al, %al
	je	.L1692
	movq	(%r12), %rdi
	movq	%rdx, %r14
	jmp	.L1845
.L1699:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer12LowerDateNowEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1761:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckNumberEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1762:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler23EffectControlLinearizer14LowerCheckMapsEPNS1_4NodeES4_
	movq	(%r12), %rdi
	xorl	%eax, %eax
	jmp	.L1854
.L1763:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1855
	movq	16(%r14), %r14
.L1855:
	call	_ZN2v88internal8compiler19CheckIfParametersOfEPKNS1_8OperatorE@PLT
	movq	-192(%rbp), %r8
	movl	$1, %r9d
	movq	%r14, %rcx
	movzbl	(%rax), %esi
	leaq	8(%rax), %rdx
	jmp	.L1887
.L1764:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer20LowerStringSubstringEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1765:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringToUpperCaseIntlEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1766:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringToLowerCaseIntlEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1767:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1870
	movq	16(%rdx), %rdx
.L1870:
	leaq	-176(%rbp), %r14
	movq	%rdx, -184(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13AccessBuilder15ForStringLengthEv@PLT
	movq	-184(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1768:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer18LowerStringIndexOfEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1769:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer26LowerStringFromCodePointAtEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1770:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer30LowerStringFromSingleCodePointEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1771:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer29LowerStringFromSingleCharCodeEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1772:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer22LowerStringCodePointAtEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1773:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringCharCodeAtEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1774:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer19LowerStringToNumberEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1775:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer17LowerStringConcatEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1776:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer28LowerPlainPrimitiveToFloat64EPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1777:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer27LowerPlainPrimitiveToWord32EPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1778:
	movzbl	23(%r12), %eax
	movq	32(%r12), %rsi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1877
	movq	16(%rsi), %rsi
.L1877:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler8ToNumberEPNS1_4NodeE@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1779:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer16LowerCheckBigIntEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1780:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer17LowerBigIntNegateEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1697:
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer25LowerFloat64RoundTiesEvenEPNS1_4NodeE
	testb	%al, %al
	je	.L1692
	movq	(%r12), %rdi
	movq	%rdx, %r14
	jmp	.L1845
.L1716:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsStringEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1839:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeTaggedToUint32EPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1844:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpw	$59, 16(%rax)
	je	.L1886
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler11UnreachableEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
.L1886:
	movq	(%r12), %rdi
	movq	%r12, %r14
	movl	$1, %eax
	jmp	.L1854
.L1842:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1850
	movq	16(%r14), %r14
.L1850:
	movq	(%r9), %rax
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	jne	.L1893
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1843:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer30LowerChangeTaggedSignedToInt32EPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1789:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer14LowerSameValueEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1790:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer37LowerCheckedTaggedToCompressedPointerEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1791:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer36LowerCheckedTaggedToCompressedSignedEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1792:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer37LowerCheckedCompressedToTaggedPointerEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1712:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer20LowerArgumentsLengthEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1713:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer19LowerArgumentsFrameEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1714:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer25LowerObjectIsUndetectableEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1715:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer19LowerObjectIsSymbolEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1785:
	movq	(%r9), %rax
	leaq	-176(%rbp), %r14
	movl	$53, %edx
	movq	%r9, -184(%rbp)
	movq	%r14, %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-184(%rbp), %r9
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringComparisonERKNS0_8CallableEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1786:
	movq	(%r9), %rax
	leaq	-176(%rbp), %r14
	movl	$49, %edx
	movq	%r9, -184(%rbp)
	movq	%r14, %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-184(%rbp), %r9
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringComparisonERKNS0_8CallableEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1787:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer20LowerNumberSameValueEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1788:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer25LowerSameValueNumbersOnlyEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1783:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer14LowerBigIntAddEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1784:
	movq	(%r9), %rax
	leaq	-176(%rbp), %r14
	movl	$54, %edx
	movq	%r9, -184(%rbp)
	movq	%r14, %rdi
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-184(%rbp), %r9
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer21LowerStringComparisonERKNS0_8CallableEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1841:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeTaggedToInt32EPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1830:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint64ToTaggedEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1831:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer25LowerChangeUint32ToTaggedEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1832:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeInt64ToTaggedEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1704:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer28LowerFindOrderedHashMapEntryEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1705:
	movq	%r9, %rdi
	movq	%r12, %rsi
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler23EffectControlLinearizer27LowerTransitionElementsKindEPNS1_4NodeE
	movq	(%r12), %rdi
	xorl	%eax, %eax
	jmp	.L1854
.L1706:
	movq	-192(%rbp), %rdx
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer26LowerMaybeGrowFastElementsEPNS1_4NodeES4_
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1707:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer31LowerEnsureWritableFastElementsEPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1696:
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer25LowerFloat64RoundTruncateEPNS1_4NodeE
	testb	%al, %al
	je	.L1692
	movq	(%r12), %rdi
	movq	%rdx, %r14
	jmp	.L1845
.L1840:
	movq	%r9, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23EffectControlLinearizer24LowerChangeTaggedToInt64EPNS1_4NodeE
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1694:
	movq	(%r9), %rax
	movq	%r9, -184(%rbp)
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Float64RoundUpEv@PLT
	testb	%al, %al
	jne	.L1878
	movq	-184(%rbp), %r9
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer19LowerFloat64RoundUpEPNS1_4NodeE.part.0
	movq	%rdx, %r14
	testb	%al, %al
	je	.L1878
	movq	(%r12), %rdi
	jmp	.L1845
.L1836:
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer35LowerChangeTaggedToCompressedSignedEPNS1_4NodeE
.L1864:
	movl	$1, %eax
	movq	%r15, %rdi
	salq	%cl, %rax
	leaq	-1(%rax), %rsi
	call	_ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl@PLT
	movq	%rax, %rdx
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1866
	movq	32(%r12), %rax
	leaq	16(%rax), %r14
.L1866:
	movq	(%r14), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler9Word64AndEPNS1_4NodeES4_@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	jmp	.L1845
.L1875:
	leaq	16(%rsi), %rax
	movq	16(%rsi), %rsi
	addq	$8, %rax
	jmp	.L1876
.L1858:
	movq	32(%r12), %rax
	movq	16(%rax), %r14
	addq	$24, %rax
	jmp	.L1859
.L1891:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	%rax, %r14
	jmp	.L1849
.L1892:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE@PLT
	movq	%rax, %r14
	jmp	.L1847
.L1893:
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1889:
	movq	8(%rdi), %rsi
	xorl	%eax, %eax
	leaq	.LC18(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1888:
	call	__stack_chk_fail@PLT
.L1890:
	movq	88(%r9), %rax
	leaq	.LC17(%rip), %rdi
	movq	(%rax), %rdx
	movl	20(%rax), %esi
	xorl	%eax, %eax
	movq	8(%rdx), %rdx
	andl	$16777215, %esi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23259:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer20TryWireInStateEffectEPNS1_4NodeES4_PS4_S5_, .-_ZN2v88internal8compiler23EffectControlLinearizer20TryWireInStateEffectEPNS1_4NodeES4_PS4_S5_
	.section	.rodata._ZN2v88internal8compiler23EffectControlLinearizer11ProcessNodeEPNS1_4NodeEPS4_S5_S5_.str1.1,"aMS",@progbits,1
.LC19:
	.string	""
.LC20:
	.string	"process node"
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer11ProcessNodeEPNS1_4NodeEPS4_S5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer11ProcessNodeEPNS1_4NodeEPS4_S5_S5_
	.type	_ZN2v88internal8compiler23EffectControlLinearizer11ProcessNodeEPNS1_4NodeEPS4_S5_S5_, @function
_ZN2v88internal8compiler23EffectControlLinearizer11ProcessNodeEPNS1_4NodeEPS4_S5_S5_:
.LFB23258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%rcx, -112(%rbp)
	movq	32(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE@PLT
	movq	32(%rbx), %rcx
	movq	16(%rcx), %rdx
	movq	%rcx, -104(%rbp)
	movq	%rdx, -88(%rbp)
	testb	$1, %al
	jne	.L1895
	movq	%rax, %rcx
	movq	%rax, %rdx
	shrq	$31, %rcx
	shrq	%rdx
	movzwl	%cx, %ecx
	andl	$1073741823, %edx
	orl	%edx, %ecx
	jne	.L1895
	movq	40(%rbx), %r13
	testq	%r13, %r13
	je	.L1897
	.p2align 4,,10
	.p2align 3
.L1943:
	movq	16(%r13), %rax
	movq	48(%r13), %rdx
	movq	%r12, %r8
	movq	%rbx, %rdi
	leaq	.LC20(%rip), %rsi
	movq	-112(%rbp), %rcx
	movq	%rax, -128(%rbp)
	movq	24(%r13), %rax
	movq	%rax, -136(%rbp)
	movl	32(%r13), %eax
	movl	%eax, -140(%rbp)
	movq	40(%r13), %rax
	movq	%rax, -120(%rbp)
	movl	20(%r15), %eax
	movq	%rsi, 24(%r13)
	movq	%r15, %rsi
	andl	$16777215, %eax
	movq	%rdx, 16(%r13)
	movl	$1, 32(%r13)
	movq	%rax, 40(%r13)
	movq	(%r14), %rdx
	call	_ZN2v88internal8compiler23EffectControlLinearizer20TryWireInStateEffectEPNS1_4NodeES4_PS4_S5_
	testb	%al, %al
	jne	.L1898
	cmpb	$0, 28(%rbx)
	movq	(%r15), %rdi
	je	.L1935
.L1899:
	movzwl	16(%rdi), %eax
	cmpl	$40, %eax
	je	.L1936
	cmpl	$39, %eax
	je	.L1937
	cmpl	$58, %eax
	je	.L1938
	cmpl	$38, %eax
	je	.L1939
	movl	24(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L1940
.L1905:
	movl	28(%rdi), %edx
	testl	%edx, %edx
	jle	.L1907
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L1908:
	movq	(%r12), %rsi
	movl	%r14d, %edx
	movq	%r15, %rdi
	addl	$1, %r14d
	call	_ZN2v88internal8compiler14NodeProperties19ReplaceControlInputEPNS1_4NodeES4_i@PLT
	movq	(%r15), %rdi
	cmpl	28(%rdi), %r14d
	jl	.L1908
.L1907:
	movl	40(%rdi), %eax
	testl	%eax, %eax
	jle	.L1909
	movq	%r15, (%r12)
	movq	(%r15), %rdi
.L1909:
	cmpw	$59, 16(%rdi)
	je	.L1941
.L1901:
	testq	%r13, %r13
	je	.L1916
.L1898:
	movl	-140(%rbp), %eax
	movq	-128(%rbp), %xmm0
	movl	%eax, 32(%r13)
	movq	-120(%rbp), %rax
	movhps	-136(%rbp), %xmm0
	movups	%xmm0, 16(%r13)
	movq	%rax, 40(%r13)
.L1916:
	movq	-88(%rbp), %rax
	movq	-104(%rbp), %rbx
	movq	%rax, 16(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1942
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1895:
	.cfi_restore_state
	movq	-104(%rbp), %rsi
	movq	%rax, 16(%rsi)
	movq	40(%rbx), %r13
	testq	%r13, %r13
	jne	.L1943
.L1897:
	movq	-112(%rbp), %rcx
	movq	(%r14), %rdx
	movq	%r12, %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler23EffectControlLinearizer20TryWireInStateEffectEPNS1_4NodeES4_PS4_S5_
	testb	%al, %al
	jne	.L1916
	leaq	.LC19(%rip), %rax
	cmpb	$0, 28(%rbx)
	movq	(%r15), %rdi
	movq	%rax, -136(%rbp)
	movq	%rax, -128(%rbp)
	movabsq	$-9223372036854775808, %rax
	movq	%rax, -120(%rbp)
	jne	.L1899
.L1935:
	testb	$16, 18(%rdi)
	jne	.L1899
	movq	$0, (%r14)
	movq	%r15, 88(%rbx)
	movq	(%r15), %rdi
	jmp	.L1899
	.p2align 4,,10
	.p2align 3
.L1938:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116RemoveRenameNodeEPNS1_4NodeE
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1940:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	-112(%rbp), %rsi
	movq	(%rsi), %rsi
	cmpq	%rax, %rsi
	je	.L1906
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
.L1906:
	movq	(%r15), %rdi
	cmpb	$0, 36(%rdi)
	je	.L1905
	movq	-112(%rbp), %rax
	movq	%r15, (%rax)
	movq	(%r15), %rdi
	jmp	.L1905
	.p2align 4,,10
	.p2align 3
.L1941:
	movq	-112(%rbp), %rax
	movq	(%rbx), %r15
	movq	(%rax), %rdx
	movq	(%rdx), %rax
	movzwl	16(%rax), %eax
	cmpl	$61, %eax
	je	.L1911
	movq	(%r12), %rsi
	movq	(%r15), %r14
	leaq	-80(%rbp), %rcx
	movq	8(%r15), %r15
	movq	%rsi, -152(%rbp)
	cmpl	$59, %eax
	jne	.L1944
.L1912:
	movq	%r15, %rdi
	movq	%rcx, -160(%rbp)
	movq	%rdx, -176(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5ThrowEv@PLT
	movq	-176(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-160(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rdx, %xmm0
	movl	$2, %edx
	movhps	-152(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	(%rbx), %r15
.L1911:
	movq	352(%r15), %rax
	testq	%rax, %rax
	je	.L1945
.L1913:
	movq	-112(%rbp), %rbx
	movq	%rax, (%r12)
	movq	%rax, (%rbx)
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1936:
	movb	$0, 28(%rbx)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116RemoveRenameNodeEPNS1_4NodeE
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1937:
	call	_ZN2v88internal8compiler21RegionObservabilityOfEPKNS1_8OperatorE@PLT
	movq	%r15, %rdi
	movb	%al, 28(%rbx)
	call	_ZN2v88internal8compiler12_GLOBAL__N_116RemoveRenameNodeEPNS1_4NodeE
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1939:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movq	%rax, (%r14)
	jmp	.L1901
.L1944:
	movq	%rdx, %xmm0
	movq	%r15, %rdi
	movq	%rcx, -160(%rbp)
	movhps	-152(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11UnreachableEv@PLT
	movl	$2, %edx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movdqa	-176(%rbp), %xmm0
	movq	-160(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rcx, -176(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-176(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L1912
.L1945:
	movq	8(%r15), %rdi
	movq	(%r15), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r15)
	jmp	.L1913
.L1942:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23258:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer11ProcessNodeEPNS1_4NodeEPS4_S5_S5_, .-_ZN2v88internal8compiler23EffectControlLinearizer11ProcessNodeEPNS1_4NodeEPS4_S5_S5_
	.section	.rodata._ZN2v88internal8compiler23EffectControlLinearizer3RunEv.str1.1,"aMS",@progbits,1
.LC21:
	.string	"clone branch"
	.section	.text._ZN2v88internal8compiler23EffectControlLinearizer3RunEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23EffectControlLinearizer3RunEv
	.type	_ZN2v88internal8compiler23EffectControlLinearizer3RunEv, @function
_ZN2v88internal8compiler23EffectControlLinearizer3RunEv:
.LFB23248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$536, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	leaq	-128(%rbp), %rdi
	movl	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%rax, -432(%rbp)
	movq	%rax, -144(%rbp)
	movq	%rax, -240(%rbp)
	movq	%rax, -208(%rbp)
	movq	8(%rbx), %rax
	movq	%rdi, -328(%rbp)
	movq	80(%rax), %rdx
	movq	88(%rax), %rax
	movq	%rdi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%rax, -360(%rbp)
	cmpq	%rax, %rdx
	je	.L1947
	movq	%rdx, -344(%rbp)
	movq	%rbx, %r14
	movq	$0, -368(%rbp)
	movq	$0, -352(%rbp)
	movq	$0, -376(%rbp)
	.p2align 4,,10
	.p2align 3
.L2072:
	movq	-344(%rbp), %rax
	movq	(%rax), %rbx
	movq	72(%rbx), %rax
	movq	144(%rbx), %rcx
	movq	(%rax), %rax
	movq	%rax, -320(%rbp)
	movq	136(%rbx), %rax
	cmpq	%rax, %rcx
	je	.L1948
	movl	4(%rbx), %esi
	jmp	.L1950
	.p2align 4,,10
	.p2align 3
.L2226:
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L1948
.L1950:
	movq	(%rax), %rdx
	cmpl	4(%rdx), %esi
	jg	.L2226
	movq	-352(%rbp), %rdi
	cmpq	%rdi, -368(%rbp)
	je	.L2227
	movq	%rdi, %rax
	movq	%rbx, (%rdi)
	addq	$8, %rax
	movq	%rax, -352(%rbp)
.L1951:
	movq	72(%rbx), %rdi
	movq	80(%rbx), %rsi
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	movl	$1, %eax
	subq	%rdi, %rsi
	sarq	$3, %rsi
	movq	%rsi, %r12
	cmpq	$1, %rsi
	ja	.L1964
	jmp	.L2228
	.p2align 4,,10
	.p2align 3
.L2229:
	cmpl	$35, %edx
	je	.L1962
	cmpl	$18, %edx
	jne	.L2113
	movq	%rcx, %r13
.L1962:
	addq	$1, %rax
	cmpq	%rsi, %rax
	je	.L1963
.L1964:
	movq	(%rdi,%rax,8), %rcx
	movq	(%rcx), %rdx
	movzwl	16(%rdx), %edx
	cmpl	$36, %edx
	jne	.L2229
	addq	$1, %rax
	movq	%rcx, %r15
	cmpq	%rsi, %rax
	jne	.L1964
.L1963:
	testq	%r15, %r15
	jne	.L2230
.L1965:
	movq	$0, -312(%rbp)
	movq	8(%r14), %rax
	cmpq	104(%rax), %rbx
	je	.L2231
	movq	-320(%rbp), %rax
	movq	(%rax), %rax
	cmpw	$22, 16(%rax)
	jne	.L2232
	.p2align 4,,10
	.p2align 3
.L1971:
	testq	%r13, %r13
	je	.L1992
	movq	-312(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
.L1992:
	movq	$0, -304(%rbp)
	movq	8(%r14), %rax
	cmpq	104(%rax), %rbx
	jne	.L2233
.L1994:
	movq	72(%rbx), %rdx
	movq	80(%rbx), %rax
	leaq	-320(%rbp), %r8
	leaq	-312(%rbp), %r15
	leaq	-304(%rbp), %r13
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r12
	jnb	.L2017
	movq	%r12, %rax
	movq	%rbx, %r12
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2011:
	movq	(%rdx,%rbx,8), %rsi
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r8, -336(%rbp)
	addq	$1, %rbx
	call	_ZN2v88internal8compiler23EffectControlLinearizer11ProcessNodeEPNS1_4NodeEPS4_S5_S5_
	movq	72(%r12), %rdx
	movq	80(%r12), %rax
	movq	-336(%rbp), %r8
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L2011
	movq	%r12, %rbx
.L2017:
	movl	52(%rbx), %eax
	cmpl	$3, %eax
	je	.L2012
	ja	.L2013
	cmpl	$2, %eax
	jne	.L2015
.L2014:
	leaq	-312(%rbp), %r15
	movq	56(%rbx), %rsi
	movq	%r14, %rdi
	leaq	-304(%rbp), %r13
	leaq	-320(%rbp), %r8
	movq	%r15, %rcx
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler23EffectControlLinearizer11ProcessNodeEPNS1_4NodeEPS4_S5_S5_
	.p2align 4,,10
	.p2align 3
.L2015:
	leaq	-176(%rbp), %r12
	movq	112(%rbx), %r15
	movq	104(%rbx), %r13
	movq	%r12, %rax
	movq	%rbx, %r12
	movq	%rax, %rbx
	cmpq	%r13, %r15
	je	.L2084
	.p2align 4,,10
	.p2align 3
.L2085:
	movq	0(%r13), %rax
	movq	-120(%rbp), %rdx
	movq	-328(%rbp), %rsi
	movl	4(%rax), %ecx
	movl	4(%r12), %eax
	movl	%ecx, -172(%rbp)
	movl	%eax, -176(%rbp)
	testq	%rdx, %rdx
	jne	.L2075
	jmp	.L2074
	.p2align 4,,10
	.p2align 3
.L2234:
	jne	.L2078
	cmpl	36(%rdx), %ecx
	jg	.L2077
.L2078:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L2076
.L2075:
	cmpl	32(%rdx), %eax
	jle	.L2234
.L2077:
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2075
.L2076:
	cmpq	-328(%rbp), %rsi
	je	.L2074
	cmpl	32(%rsi), %eax
	jl	.L2074
	jne	.L2080
	cmpl	36(%rsi), %ecx
	jge	.L2080
	.p2align 4,,10
	.p2align 3
.L2074:
	leaq	-272(%rbp), %rdx
	leaq	-144(%rbp), %rdi
	movq	%rbx, -272(%rbp)
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0
	movq	%rax, %rsi
.L2080:
	cmpq	$0, 40(%rsi)
	je	.L2235
.L2081:
	cmpq	$0, 48(%rsi)
	je	.L2236
.L2082:
	movq	-304(%rbp), %rax
	addq	$8, %r13
	movq	%rax, 56(%rsi)
	cmpq	%r13, %r15
	jne	.L2085
.L2084:
	addq	$8, -344(%rbp)
	movq	-344(%rbp), %rax
	cmpq	%rax, -360(%rbp)
	jne	.L2072
	movq	-376(%rbp), %rdi
	cmpq	%rdi, -352(%rbp)
	je	.L2225
	movq	-352(%rbp), %r13
	movq	%rdi, %rbx
	leaq	-144(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L2088:
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler12_GLOBAL__N_118UpdateBlockControlEPNS1_10BasicBlockEPNS2_21BlockEffectControlMapE
	cmpq	%rbx, %r13
	jne	.L2088
.L2225:
	movq	-224(%rbp), %r13
	movq	-232(%rbp), %rbx
	cmpq	%rbx, %r13
	je	.L1947
	leaq	-144(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L2092:
	movq	8(%rbx), %rsi
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	addq	$16, %rbx
	call	_ZN2v88internal8compiler12_GLOBAL__N_115UpdateEffectPhiEPNS1_4NodeEPNS1_10BasicBlockEPNS2_21BlockEffectControlMapEPNS1_7JSGraphE.isra.0
	cmpq	%rbx, %r13
	jne	.L2092
.L1947:
	movq	-120(%rbp), %rdx
	leaq	-144(%rbp), %rcx
	testq	%rdx, %rdx
	je	.L1946
.L2091:
	movq	24(%rdx), %rax
	testq	%rax, %rax
	je	.L2093
.L2094:
	movq	24(%rax), %rsi
	movq	%rcx, %rdi
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L2094
.L2093:
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2091
.L1946:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2237
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2236:
	.cfi_restore_state
	movq	-320(%rbp), %rax
	movq	%rax, 48(%rsi)
	jmp	.L2082
	.p2align 4,,10
	.p2align 3
.L2235:
	movq	-312(%rbp), %rax
	movq	%rax, 40(%rsi)
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L2013:
	subl	$4, %eax
	cmpl	$4, %eax
	ja	.L2015
	jmp	.L2014
	.p2align 4,,10
	.p2align 3
.L1948:
	leaq	-144(%rbp), %rdi
	movq	%rdi, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_118UpdateBlockControlEPNS1_10BasicBlockEPNS2_21BlockEffectControlMapE
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L2230:
	movq	136(%rbx), %rax
	movq	144(%rbx), %rcx
	cmpq	%rax, %rcx
	je	.L1966
	movl	4(%rbx), %esi
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L2238:
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L1966
.L1968:
	movq	(%rax), %rdx
	cmpl	4(%rdx), %esi
	jg	.L2238
	movq	%r15, %xmm0
	movq	%rbx, %xmm2
	movq	-224(%rbp), %rsi
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -176(%rbp)
	cmpq	-216(%rbp), %rsi
	je	.L2239
	movups	%xmm0, (%rsi)
	addq	$16, -224(%rbp)
.L1970:
	movq	%r15, -312(%rbp)
	jmp	.L1971
	.p2align 4,,10
	.p2align 3
.L2012:
	movq	56(%rbx), %rsi
	leaq	-312(%rbp), %r15
	movq	%r14, %rdi
	leaq	-304(%rbp), %r13
	movq	%r13, %rdx
	movq	%r15, %rcx
	leaq	-320(%rbp), %r8
	call	_ZN2v88internal8compiler23EffectControlLinearizer11ProcessNodeEPNS1_4NodeEPS4_S5_S5_
	movq	(%r14), %rax
	movq	32(%r14), %r15
	movq	56(%rbx), %r13
	movq	40(%r14), %r12
	movq	8(%rax), %rdi
	movq	(%rax), %rax
	movq	%r13, %rsi
	movq	%rax, -408(%rbp)
	movq	16(%r14), %rax
	movq	%rdi, -416(%rbp)
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE@PLT
	movq	16(%r15), %rdx
	movq	%rdx, -280(%rbp)
	testb	$1, %al
	jne	.L2018
	movq	%rax, %rcx
	movq	%rax, %rdx
	shrq	%rcx
	shrq	$31, %rdx
	andl	$1073741823, %ecx
	movzwl	%dx, %edx
	orl	%edx, %ecx
	je	.L2019
.L2018:
	movq	%rax, 16(%r15)
.L2019:
	testq	%r12, %r12
	je	.L2020
	movq	16(%r12), %rax
	movq	48(%r12), %rdx
	leaq	.LC21(%rip), %rdi
	xorl	%esi, %esi
	movq	%rax, -336(%rbp)
	movq	24(%r12), %rax
	movq	%rax, -384(%rbp)
	movl	32(%r12), %eax
	movl	%eax, -396(%rbp)
	movq	40(%r12), %rax
	movq	%rax, -392(%rbp)
	movl	20(%r13), %eax
	movq	%rdi, 24(%r12)
	movq	%r13, %rdi
	andl	$16777215, %eax
	movq	%rdx, 16(%r12)
	movl	$1, 32(%r12)
	movq	%rax, 40(%r12)
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r9
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L2021
.L2102:
	movl	16(%rax), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %ecx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	jne	.L2022
	movq	(%rdx), %rdx
.L2022:
	cmpq	%rdx, %r13
	je	.L2240
.L2041:
	testq	%r12, %r12
	je	.L2070
.L2021:
	movq	-336(%rbp), %xmm0
	movl	-396(%rbp), %eax
	movl	%eax, 32(%r12)
	movq	-392(%rbp), %rax
	movhps	-384(%rbp), %xmm0
	movups	%xmm0, 16(%r12)
	movq	%rax, 40(%r12)
.L2070:
	movq	-280(%rbp), %rax
	movq	%rax, 16(%r15)
	jmp	.L2015
	.p2align 4,,10
	.p2align 3
.L2233:
	movq	136(%rbx), %rsi
	movl	4(%rbx), %eax
	movq	-328(%rbp), %r8
	movq	(%rsi), %rdx
	movl	4(%rdx), %ecx
	movq	-120(%rbp), %rdx
	movl	%eax, -172(%rbp)
	movl	%ecx, -176(%rbp)
	testq	%rdx, %rdx
	jne	.L1996
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L2241:
	jne	.L1999
	cmpl	36(%rdx), %eax
	jg	.L1998
.L1999:
	movq	%rdx, %r8
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L1997
.L1996:
	cmpl	32(%rdx), %ecx
	jle	.L2241
.L1998:
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1996
.L1997:
	cmpq	-328(%rbp), %r8
	je	.L1995
	cmpl	32(%r8), %ecx
	jl	.L1995
	je	.L2242
.L2001:
	movq	144(%rbx), %rax
	movq	56(%r8), %rcx
	subq	%rsi, %rax
	movq	%rcx, -304(%rbp)
	cmpq	$15, %rax
	jbe	.L1994
	movl	$1, %r13d
	movq	%r12, -336(%rbp)
	movq	%rbx, %r12
	leaq	-176(%rbp), %r15
	movq	%r13, %rbx
	movq	-328(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L2016:
	movq	(%rsi,%rbx,8), %rax
	movl	4(%r12), %edi
	movq	%r13, %rsi
	movq	-120(%rbp), %rdx
	movl	4(%rax), %eax
	movl	%edi, -172(%rbp)
	movl	%eax, -176(%rbp)
	testq	%rdx, %rdx
	jne	.L2004
	jmp	.L2003
	.p2align 4,,10
	.p2align 3
.L2243:
	jne	.L2007
	cmpl	36(%rdx), %edi
	jg	.L2006
.L2007:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L2005
.L2004:
	cmpl	32(%rdx), %eax
	jle	.L2243
.L2006:
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2004
.L2005:
	cmpq	%r13, %rsi
	je	.L2003
	cmpl	32(%rsi), %eax
	jl	.L2003
	jne	.L2009
	cmpl	36(%rsi), %edi
	jge	.L2009
	.p2align 4,,10
	.p2align 3
.L2003:
	leaq	-272(%rbp), %rdx
	leaq	-144(%rbp), %rdi
	movq	%r15, -272(%rbp)
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0
	movq	-304(%rbp), %rcx
	movq	%rax, %rsi
.L2009:
	cmpq	%rcx, 56(%rsi)
	jne	.L2244
	movq	136(%r12), %rsi
	movq	144(%r12), %rax
	addq	$1, %rbx
	subq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L2016
	movq	%r12, %rbx
	movq	-336(%rbp), %r12
	jmp	.L1994
	.p2align 4,,10
	.p2align 3
.L2113:
	movq	%rax, %r12
	jmp	.L1963
	.p2align 4,,10
	.p2align 3
.L1966:
	leaq	-144(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rdi, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_115UpdateEffectPhiEPNS1_4NodeEPNS1_10BasicBlockEPNS2_21BlockEffectControlMapEPNS1_7JSGraphE.isra.0
	jmp	.L1970
.L2242:
	cmpl	36(%r8), %eax
	jge	.L2001
	.p2align 4,,10
	.p2align 3
.L1995:
	movq	%r8, %rsi
	leaq	-176(%rbp), %rax
	leaq	-272(%rbp), %rdx
	leaq	-144(%rbp), %rdi
	movq	%rax, -272(%rbp)
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0
	movq	136(%rbx), %rsi
	movq	%rax, %r8
	jmp	.L2001
	.p2align 4,,10
	.p2align 3
.L2244:
	movq	(%r14), %rax
	movq	%r12, %rbx
	movq	-336(%rbp), %r12
	movq	$0, -304(%rbp)
	movq	(%rax), %rax
	movq	16(%rax), %rax
	movq	%rax, 88(%r14)
	jmp	.L1994
.L2020:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movabsq	$-9223372036854775808, %rdi
	movq	%rax, %r9
	movq	24(%rax), %rax
	movq	%rdi, -392(%rbp)
	leaq	.LC19(%rip), %rdi
	movq	%rdi, -384(%rbp)
	movq	%rdi, -336(%rbp)
	testq	%rax, %rax
	jne	.L2102
	jmp	.L2070
.L2231:
	movq	(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -312(%rbp)
	jmp	.L1971
.L2232:
	movq	136(%rbx), %rdx
	cmpq	%rdx, 144(%rbx)
	je	.L1972
	xorl	%r15d, %r15d
	movq	%r13, -336(%rbp)
	xorl	%r8d, %r8d
	leaq	-176(%rbp), %rax
	movq	%r15, %r13
	movq	-328(%rbp), %r15
	movq	%r12, -384(%rbp)
	movq	%rbx, %r12
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1982:
	movq	(%rdx,%r13,8), %rdx
	movl	4(%r12), %eax
	movq	%r15, %rsi
	movl	4(%rdx), %ecx
	movq	-120(%rbp), %rdx
	movl	%eax, -172(%rbp)
	movl	%ecx, -176(%rbp)
	testq	%rdx, %rdx
	jne	.L1974
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L2245:
	jne	.L1977
	cmpl	36(%rdx), %eax
	jg	.L1976
.L1977:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L1975
.L1974:
	cmpl	32(%rdx), %ecx
	jle	.L2245
.L1976:
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1974
.L1975:
	cmpq	%r15, %rsi
	je	.L1973
	cmpl	32(%rsi), %ecx
	jl	.L1973
	jne	.L1979
	cmpl	36(%rsi), %eax
	jge	.L1979
	.p2align 4,,10
	.p2align 3
.L1973:
	leaq	-272(%rbp), %rdx
	leaq	-144(%rbp), %rdi
	movq	%rbx, -272(%rbp)
	call	_ZNSt8_Rb_treeISt4pairIiiES0_IKS1_N2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataEESt10_Select1stIS8_ESt4lessIS1_ENS4_13ZoneAllocatorIS8_EEE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJOS1_EESK_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0
	movq	-312(%rbp), %r8
	movq	%rax, %rsi
.L1979:
	movq	40(%rsi), %rax
	testq	%r8, %r8
	je	.L2246
	cmpq	%r8, %rax
	jne	.L2247
.L1981:
	movq	136(%r12), %rdx
	movq	144(%r12), %rax
	addq	$1, %r13
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r13
	jb	.L1982
	movq	%r12, %rbx
	movq	-336(%rbp), %r13
	movq	-384(%rbp), %r12
	testq	%r8, %r8
	je	.L1972
	movq	-320(%rbp), %rdi
	movq	(%rdi), %rax
	cmpw	$7, 16(%rax)
	jne	.L1971
	xorl	%edx, %edx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14NodeProperties18ReplaceEffectInputEPNS1_4NodeES4_i@PLT
	movq	-320(%rbp), %rax
	movq	%rax, -312(%rbp)
	jmp	.L1971
	.p2align 4,,10
	.p2align 3
.L2246:
	movq	%rax, -312(%rbp)
	movq	%rax, %r8
	jmp	.L1981
.L2227:
	movq	-368(%rbp), %r13
	subq	-376(%rbp), %r13
	movq	%r13, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L2248
	testq	%rax, %rax
	je	.L2107
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L2249
	movl	$2147483640, %esi
	movl	$2147483640, %r12d
.L1953:
	movq	-432(%rbp), %rax
	movq	16(%rax), %rdx
	movq	24(%rax), %rax
	movq	%rax, -336(%rbp)
	subq	%rdx, %rax
	cmpq	%rsi, %rax
	jb	.L2250
	movq	-432(%rbp), %rax
	addq	%rdx, %rsi
	movq	%rsi, 16(%rax)
.L1956:
	leaq	(%rdx,%r12), %rax
	movq	%rax, -368(%rbp)
	leaq	8(%rdx), %rax
.L1954:
	movq	-352(%rbp), %rdi
	movq	-376(%rbp), %r8
	movq	%rbx, (%rdx,%r13)
	cmpq	%r8, %rdi
	je	.L2110
	subq	$8, %rdi
	leaq	15(%rdx), %rax
	movq	%rdi, %rsi
	subq	%r8, %rax
	subq	%r8, %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L2111
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L2111
	leaq	1(%rcx), %rdi
	xorl	%eax, %eax
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1959:
	movdqu	(%r8,%rax), %xmm1
	movups	%xmm1, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L1959
	movq	%rdi, %r8
	movq	-376(%rbp), %rax
	andq	$-2, %r8
	leaq	0(,%r8,8), %rcx
	addq	%rcx, %rax
	addq	%rdx, %rcx
	cmpq	%r8, %rdi
	je	.L1961
	movq	(%rax), %rax
	movq	%rax, (%rcx)
.L1961:
	leaq	16(%rdx,%rsi), %rax
	movq	%rax, -352(%rbp)
.L1957:
	movq	%rdx, -376(%rbp)
	jmp	.L1951
.L2240:
	cmpq	$0, (%rax)
	jne	.L2041
	movq	(%r9), %rax
	cmpw	$35, 16(%rax)
	jne	.L2041
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r9, -440(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-440(%rbp), %r9
	movq	%rax, -456(%rbp)
	movq	(%rax), %rax
	cmpw	$10, 16(%rax)
	jne	.L2041
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	cmpq	%rax, -456(%rbp)
	jne	.L2041
	leaq	-272(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13BranchMatcherC1EPNS1_4NodeE@PLT
	movq	-424(%rbp), %rax
	movq	$0, -168(%rbp)
	movq	$0, -160(%rbp)
	movq	-440(%rbp), %r9
	movq	%rax, -176(%rbp)
	movq	-456(%rbp), %rax
	movq	$0, -152(%rbp)
	movq	24(%rax), %rcx
	leaq	-296(%rbp), %rax
	movq	%rax, -464(%rbp)
	testq	%rcx, %rcx
	je	.L2037
	movq	%r14, -448(%rbp)
	movq	%rbx, -424(%rbp)
	movq	%r9, %rbx
	movq	%r12, -440(%rbp)
	movq	%rcx, %r12
.L2024:
	movl	16(%r12), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r12,%rax,8), %rax
	jne	.L2027
	movq	(%rax), %rax
.L2027:
	movq	%rax, -296(%rbp)
	cmpq	%rax, %rbx
	je	.L2028
	cmpq	%rax, %r13
	je	.L2028
	movq	(%rax), %rdx
	movzwl	16(%rdx), %edx
	subl	$35, %edx
	cmpl	$1, %edx
	ja	.L2215
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2030
	movq	(%rdx), %r14
	movq	%rbx, -472(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
.L2035:
	movl	16(%r14), %ecx
	movl	%ecx, %eax
	shrl	%eax
	andl	$1, %ecx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r14,%rax,8), %rdi
	movq	(%rdi), %rax
	jne	.L2031
	movq	%rax, %rdi
	movq	(%rax), %rax
.L2031:
	cmpl	$1, 28(%rax)
	jne	.L2215
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%r14), %ecx
	movq	%rax, %rdi
	movl	%ecx, %esi
	shrl	%esi
	andl	$1, %ecx
	movl	%esi, %eax
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r14,%rax,8), %rax
	jne	.L2032
	movq	(%rax), %rax
.L2032:
	movq	(%rax), %rax
	movzwl	16(%rax), %eax
	subl	$35, %eax
	cmpl	$1, %eax
	jbe	.L2251
.L2033:
	cmpq	%rdi, -264(%rbp)
	je	.L2034
	cmpq	-256(%rbp), %rdi
	jne	.L2215
.L2034:
	testq	%rbx, %rbx
	je	.L2216
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	jmp	.L2035
.L2107:
	movl	$8, %esi
	movl	$8, %r12d
	jmp	.L1953
.L2228:
	movl	$1, %r12d
	jmp	.L1965
.L2249:
	testq	%rdx, %rdx
	jne	.L2252
	movq	$0, -368(%rbp)
	movl	$8, %eax
	xorl	%edx, %edx
	jmp	.L1954
.L2247:
	movq	$0, -312(%rbp)
	movq	%r12, %rbx
	movq	-336(%rbp), %r13
	movq	-384(%rbp), %r12
.L1972:
	movq	-200(%rbp), %rdx
	cmpq	-192(%rbp), %rdx
	je	.L2222
	movq	%rdx, -192(%rbp)
.L2222:
	movq	(%r14), %r15
	movq	%rdx, %rsi
	movq	352(%r15), %rax
	testq	%rax, %rax
	je	.L2253
.L1984:
	movq	%rax, -176(%rbp)
	movq	%rsi, %rcx
	movq	144(%rbx), %rax
	subq	136(%rbx), %rax
	subq	%rdx, %rcx
	movq	%rax, %rdi
	sarq	$3, %rcx
	sarq	$3, %rdi
	cmpq	%rcx, %rdi
	ja	.L2254
	jnb	.L1986
	addq	%rdx, %rax
	cmpq	%rsi, %rax
	je	.L1986
	movq	%rax, -192(%rbp)
	movq	%rax, %rsi
	jmp	.L1986
.L2239:
	leaq	-176(%rbp), %rdx
	leaq	-240(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler12_GLOBAL__N_116PendingEffectPhiENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1970
.L2254:
	subq	%rcx, %rdi
	leaq	-176(%rbp), %r8
	movq	%rdi, %rdx
	movq	%r8, %rcx
	leaq	-208(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	movq	-192(%rbp), %rsi
.L1986:
	cmpq	%rsi, -184(%rbp)
	je	.L1987
	movq	-320(%rbp), %rax
	movq	%rax, (%rsi)
	movq	-192(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -192(%rbp)
.L1988:
	movq	(%r14), %rax
	movq	144(%rbx), %rsi
	movq	%rdx, -384(%rbp)
	subq	136(%rbx), %rsi
	movq	-200(%rbp), %r15
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	sarq	$3, %rsi
	movq	%r9, -336(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	-384(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-336(%rbp), %r9
	movq	%rax, %rsi
	subq	%r15, %rdx
	sarq	$3, %rdx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -312(%rbp)
	movq	%rax, %r8
	movq	-320(%rbp), %rax
	movq	(%rax), %rax
	cmpw	$1, 16(%rax)
	je	.L2255
	leaq	-144(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rdi, %rdx
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_115UpdateEffectPhiEPNS1_4NodeEPNS1_10BasicBlockEPNS2_21BlockEffectControlMapEPNS1_7JSGraphE.isra.0
	jmp	.L1971
.L2111:
	movq	-376(%rbp), %r8
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1958:
	movq	(%r8,%rax,8), %rdi
	movq	%rdi, (%rdx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rcx, %rdi
	jne	.L1958
	jmp	.L1961
.L2255:
	movq	%r8, %xmm0
	movq	%rbx, %xmm3
	movq	-224(%rbp), %rsi
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -176(%rbp)
	cmpq	-216(%rbp), %rsi
	je	.L1990
	movups	%xmm0, (%rsi)
	addq	$16, -224(%rbp)
	jmp	.L1971
.L2110:
	movq	%rax, -352(%rbp)
	jmp	.L1957
.L2253:
	movq	(%r15), %r9
	movq	8(%r15), %rdi
	movq	%r9, -336(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	-336(%rbp), %r9
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-200(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	%rax, 352(%r15)
	jmp	.L1984
.L2250:
	movq	-432(%rbp), %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1956
.L1987:
	leaq	-320(%rbp), %r8
	leaq	-208(%rbp), %rdi
	movq	%r8, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-192(%rbp), %rdx
	jmp	.L1988
.L2216:
	movq	-472(%rbp), %rbx
.L2030:
	movq	-160(%rbp), %rsi
	cmpq	-152(%rbp), %rsi
	je	.L2036
	movq	-296(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -160(%rbp)
.L2028:
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L2024
	movq	%rbx, %r9
	movq	-440(%rbp), %r12
	movq	-424(%rbp), %rbx
	movq	-448(%rbp), %r14
.L2037:
	movq	0(%r13), %rdi
	movq	%r9, -448(%rbp)
	call	_ZN2v88internal8compiler12BranchHintOfEPKNS1_8OperatorE@PLT
	movq	-408(%rbp), %rdi
	movq	-448(%rbp), %r9
	movb	%al, -480(%rbp)
	movq	-456(%rbp), %rax
	movq	(%rdi), %rdi
	movq	(%rax), %rax
	movq	24(%rdi), %rcx
	movl	28(%rax), %eax
	leal	(%rax,%rax), %esi
	movl	%eax, -424(%rbp)
	movq	16(%rdi), %rax
	movslq	%esi, %rsi
	salq	$3, %rsi
	subq	%rax, %rcx
	movq	%rax, -440(%rbp)
	cmpq	%rcx, %rsi
	ja	.L2256
	addq	-440(%rbp), %rsi
	movq	%rsi, 16(%rdi)
.L2038:
	movslq	-424(%rbp), %rax
	movq	-440(%rbp), %rdi
	movq	%rax, %rcx
	leaq	(%rdi,%rax,8), %rax
	movq	%rax, -560(%rbp)
	testl	%ecx, %ecx
	jle	.L2039
	leaq	-80(%rbp), %rcx
	subq	%rdi, %rax
	xorl	%r10d, %r10d
	movq	%r15, -512(%rbp)
	movq	%rcx, -448(%rbp)
	movq	-416(%rbp), %r15
	movq	%rax, -472(%rbp)
	movq	%r14, -528(%rbp)
	movq	-408(%rbp), %r14
	movq	%rbx, -496(%rbp)
	movl	%r10d, %ebx
	movq	%r12, -504(%rbp)
	movq	%rdi, %r12
	movq	%r13, -520(%rbp)
	movq	%r9, %r13
.L2040:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-456(%rbp), %rdi
	movl	%ebx, %esi
	addl	$1, %ebx
	movq	%rax, -464(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movzbl	-480(%rbp), %esi
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r14, %rdi
	movq	-464(%rbp), %xmm0
	movq	-448(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-488(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, -464(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	-448(%rbp), %rcx
	movq	-464(%rbp), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r15, %rdi
	movq	%rax, (%r12)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-448(%rbp), %rcx
	movq	-464(%rbp), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-472(%rbp), %rdi
	movq	%rax, (%rdi,%r12)
	addq	$8, %r12
	cmpl	%ebx, -424(%rbp)
	jne	.L2040
	movq	-264(%rbp), %rax
	movq	-256(%rbp), %rdi
	movq	%r13, %r9
	xorl	%esi, %esi
	movq	-504(%rbp), %r12
	movq	-496(%rbp), %rbx
	movq	%r9, -504(%rbp)
	movq	%rdi, -448(%rbp)
	movq	%rax, %rdi
	movq	-520(%rbp), %r13
	movq	%rax, -464(%rbp)
	movq	-528(%rbp), %r14
	movq	-512(%rbp), %r15
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	-448(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movl	-424(%rbp), %eax
	movq	-440(%rbp), %rdi
	movq	%r13, -488(%rbp)
	movq	-504(%rbp), %r9
	movq	%r14, -496(%rbp)
	movq	%r12, %r13
	movq	%rbx, %r12
	subl	$1, %eax
	movq	%rdi, %rbx
	leaq	8(%rdi,%rax,8), %rax
	movq	%r9, %r14
	movq	%rax, -480(%rbp)
.L2043:
	movq	-408(%rbp), %rax
	movq	(%rbx), %rdx
	movq	-464(%rbp), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	-472(%rbp), %rdi
	movq	-408(%rbp), %rax
	movq	(%rdi,%rbx), %rdx
	movq	(%rax), %rsi
	addq	$8, %rbx
	movq	-448(%rbp), %rdi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	cmpq	%rbx, -480(%rbp)
	jne	.L2043
	movq	%r12, %rbx
	movq	%r14, %r9
	movq	%r13, %r12
	movq	-496(%rbp), %r14
	movq	-488(%rbp), %r13
	jmp	.L2101
	.p2align 4,,10
	.p2align 3
.L2251:
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %rdi
	jmp	.L2033
.L1990:
	leaq	-176(%rbp), %rdx
	leaq	-240(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler12_GLOBAL__N_116PendingEffectPhiENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1971
.L2215:
	movq	-424(%rbp), %rbx
	movq	-440(%rbp), %r12
	movq	-448(%rbp), %r14
	jmp	.L2041
.L2036:
	movq	-464(%rbp), %rdx
	leaq	-176(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2028
.L2248:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2237:
	call	__stack_chk_fail@PLT
.L2252:
	movl	$268435455, %r12d
	cmpq	$268435455, %rdx
	cmova	%r12, %rdx
	leaq	0(,%rdx,8), %r12
	movq	%r12, %rsi
	jmp	.L1953
.L2039:
	movq	-264(%rbp), %rax
	movq	-256(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r9, -472(%rbp)
	movq	%rdi, -448(%rbp)
	movq	%rax, %rdi
	movq	%rax, -464(%rbp)
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	-448(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	-472(%rbp), %r9
.L2101:
	movl	-424(%rbp), %esi
	movq	-416(%rbp), %rdi
	movq	%r9, -504(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	-264(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movl	-424(%rbp), %esi
	movq	-416(%rbp), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	-256(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	104(%rbx), %rax
	movq	-264(%rbp), %rcx
	leaq	-296(%rbp), %rsi
	leaq	-144(%rbp), %rdi
	movq	%rsi, -480(%rbp)
	movq	(%rax), %rdx
	movq	%rcx, -496(%rbp)
	movq	%rdi, -472(%rbp)
	movq	72(%rdx), %rdx
	movq	(%rdx), %r8
	cmpq	%rcx, %r8
	movq	%r8, -488(%rbp)
	setne	%dl
	movzbl	%dl, %edx
	movq	(%rax,%rdx,8), %rax
	movl	4(%rbx), %edx
	movl	4(%rax), %eax
	movl	%edx, -296(%rbp)
	movl	%eax, -292(%rbp)
	call	_ZNSt3mapISt4pairIiiEN2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataESt4lessIS1_ENS3_13ZoneAllocatorIS0_IKS1_S6_EEEEixEOS1_
	movq	-488(%rbp), %r8
	xorl	%edx, %edx
	movq	-496(%rbp), %rcx
	movq	%rax, -416(%rbp)
	movq	104(%rbx), %rax
	cmpq	%rcx, %r8
	movq	-472(%rbp), %rdi
	movq	-480(%rbp), %rsi
	sete	%dl
	movq	(%rax,%rdx,8), %rax
	movl	4(%rbx), %edx
	movl	4(%rax), %eax
	movl	%edx, -296(%rbp)
	movl	%eax, -292(%rbp)
	call	_ZNSt3mapISt4pairIiiEN2v88internal8compiler12_GLOBAL__N_122BlockEffectControlDataESt4lessIS1_ENS3_13ZoneAllocatorIS0_IKS1_S6_EEEEixEOS1_
	movq	-160(%rbp), %rdi
	movq	-504(%rbp), %r9
	movq	%rax, -472(%rbp)
	movq	-168(%rbp), %rax
	movq	%rdi, -480(%rbp)
	cmpq	%rdi, %rax
	je	.L2067
	movl	-424(%rbp), %edi
	movq	%rbx, -512(%rbp)
	movq	%r12, -520(%rbp)
	leal	1(%rdi), %ecx
	movq	%r15, -528(%rbp)
	movl	%ecx, -400(%rbp)
	movq	%rax, %rcx
	leal	-1(%rdi), %eax
	leaq	8(,%rax,8), %rdi
	movq	%r9, -536(%rbp)
	movq	%rdi, -504(%rbp)
	movq	%r14, -552(%rbp)
	movq	%r13, -544(%rbp)
	movq	%rcx, %r13
.L2068:
	movq	0(%r13), %r12
	movl	-424(%rbp), %edx
	xorl	%eax, %eax
	leaq	32(%r12), %rcx
	testl	%edx, %edx
	jle	.L2051
.L2053:
	movzbl	23(%r12), %edx
	movq	-440(%rbp), %rdi
	andl	$15, %edx
	leaq	(%rdi,%rax), %rsi
	cmpl	$15, %edx
	je	.L2050
	movq	(%rax,%rcx), %rdx
	addq	$8, %rax
	movq	%rdx, (%rsi)
	cmpq	-504(%rbp), %rax
	jne	.L2053
	jmp	.L2051
.L2050:
	movq	(%rcx), %rdx
	movq	16(%rdx,%rax), %rdx
	addq	$8, %rax
	movq	%rdx, (%rsi)
	cmpq	%rax, -504(%rbp)
	jne	.L2053
.L2051:
	movq	-560(%rbp), %rbx
	movq	-464(%rbp), %rax
	xorl	%r8d, %r8d
	movl	-400(%rbp), %r15d
	movq	-408(%rbp), %r14
	movq	%rax, (%rbx)
	movq	-440(%rbp), %rcx
	movq	(%r12), %rsi
	movl	%r15d, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movl	%r15d, %edx
	movq	%rax, -488(%rbp)
	movq	-448(%rbp), %rax
	movq	-440(%rbp), %rcx
	movq	%rax, (%rbx)
	movq	(%r12), %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rdi
	movq	%rax, -496(%rbp)
	call	_ZNK2v88internal8compiler4Node8UseCountEv@PLT
	testl	%eax, %eax
	jne	.L2257
.L2048:
	movq	(%r12), %rax
	cmpw	$36, 16(%rax)
	jne	.L2065
	movq	-416(%rbp), %rax
	movq	-488(%rbp), %rdi
	movq	%rdi, (%rax)
	movq	-472(%rbp), %rax
	movq	-496(%rbp), %rdi
	movq	%rdi, (%rax)
.L2065:
	movq	%r12, %rdi
	addq	$8, %r13
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	cmpq	%r13, -480(%rbp)
	jne	.L2068
	movq	-512(%rbp), %rbx
	movq	-520(%rbp), %r12
	movq	-528(%rbp), %r15
	movq	-536(%rbp), %r9
	movq	-544(%rbp), %r13
	movq	-552(%rbp), %r14
.L2067:
	cmpq	56(%rbx), %r13
	je	.L2258
.L2046:
	movq	%r13, %rdi
	movq	%r9, -408(%rbp)
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	movq	-408(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	movq	-456(%rbp), %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	testq	%r12, %r12
	jne	.L2021
	jmp	.L2070
.L2257:
	movq	24(%r12), %r14
	testq	%r14, %r14
	je	.L2048
	movq	(%r14), %rbx
.L2064:
	movl	16(%r14), %edx
	movl	%edx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%r14,%rcx,8), %rdi
	leaq	32(%rdi,%rax), %r15
	jne	.L2056
	leaq	16(%rdi,%rax), %r15
	movq	(%rdi), %rdi
.L2056:
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	16(%r14), %edx
	movq	%rax, %rdi
	movl	%edx, %esi
	shrl	%esi
	andl	$1, %edx
	movl	%esi, %eax
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r14,%rax,8), %rax
	jne	.L2057
	movq	(%rax), %rax
.L2057:
	movq	(%rax), %rax
	movzwl	16(%rax), %eax
	subl	$35, %eax
	cmpl	$1, %eax
	jbe	.L2259
.L2058:
	cmpq	%rdi, -264(%rbp)
	movq	-496(%rbp), %r8
	movq	(%r15), %rdi
	cmove	-488(%rbp), %r8
	cmpq	%rdi, %r8
	je	.L2061
	testq	%rdi, %rdi
	je	.L2062
	movq	%r14, %rsi
	movq	%r8, -568(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-568(%rbp), %r8
.L2062:
	movq	%r8, (%r15)
	testq	%r8, %r8
	je	.L2061
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L2061:
	testq	%rbx, %rbx
	je	.L2048
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	jmp	.L2064
.L2259:
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %rdi
	jmp	.L2058
.L2258:
	movq	-416(%rbp), %rax
	movq	-464(%rbp), %rdi
	movq	%rdi, 8(%rax)
	movq	-472(%rbp), %rax
	movq	-448(%rbp), %rdi
	movq	%rdi, 8(%rax)
	jmp	.L2046
.L2256:
	movq	%r9, -448(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-448(%rbp), %r9
	movq	%rax, -440(%rbp)
	jmp	.L2038
	.cfi_endproc
.LFE23248:
	.size	_ZN2v88internal8compiler23EffectControlLinearizer3RunEv, .-_ZN2v88internal8compiler23EffectControlLinearizer3RunEv
	.section	.text._ZN2v88internal8compiler22LinearizeEffectControlEPNS1_7JSGraphEPNS1_8ScheduleEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS1_20MaskArrayIndexEnableE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler22LinearizeEffectControlEPNS1_7JSGraphEPNS1_8ScheduleEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS1_20MaskArrayIndexEnableE
	.type	_ZN2v88internal8compiler22LinearizeEffectControlEPNS1_7JSGraphEPNS1_8ScheduleEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS1_20MaskArrayIndexEnableE, @function
_ZN2v88internal8compiler22LinearizeEffectControlEPNS1_7JSGraphEPNS1_8ScheduleEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS1_20MaskArrayIndexEnableE:
.LFB23455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm1
	movq	%rdi, %xmm0
	movq	%rdi, %r10
	punpcklqdq	%xmm1, %xmm0
	movq	%r10, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	leaq	-80(%rbp), %rdi
	.cfi_offset 12, -24
	leaq	-128(%rbp), %r12
	subq	$136, %rsp
	movq	%r8, -136(%rbp)
	movq	%rdx, %r8
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -128(%rbp)
	movq	%rcx, %xmm0
	xorl	%ecx, %ecx
	movhps	-136(%rbp), %xmm0
	movq	%rdx, -112(%rbp)
	xorl	%edx, %edx
	movl	%r9d, -104(%rbp)
	movaps	%xmm0, -96(%rbp)
	movb	$0, -100(%rbp)
	call	_ZN2v88internal8compiler14GraphAssemblerC1EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE@PLT
	movq	%r12, %rdi
	movq	$0, -40(%rbp)
	call	_ZN2v88internal8compiler23EffectControlLinearizer3RunEv
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2263
	addq	$136, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2263:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23455:
	.size	_ZN2v88internal8compiler22LinearizeEffectControlEPNS1_7JSGraphEPNS1_8ScheduleEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS1_20MaskArrayIndexEnableE, .-_ZN2v88internal8compiler22LinearizeEffectControlEPNS1_7JSGraphEPNS1_8ScheduleEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS1_20MaskArrayIndexEnableE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler23EffectControlLinearizer3RunEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler23EffectControlLinearizer3RunEv, @function
_GLOBAL__sub_I__ZN2v88internal8compiler23EffectControlLinearizer3RunEv:
.LFB28777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28777:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler23EffectControlLinearizer3RunEv, .-_GLOBAL__sub_I__ZN2v88internal8compiler23EffectControlLinearizer3RunEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler23EffectControlLinearizer3RunEv
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC8:
	.quad	1
	.quad	2
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC9:
	.long	0
	.long	1127219200
	.align 8
.LC10:
	.long	0
	.long	1072693248
	.align 8
.LC11:
	.long	0
	.long	-1020264448
	.align 8
.LC12:
	.long	0
	.long	-2147483648
	.align 8
.LC13:
	.long	0
	.long	-1074790400
	.align 8
.LC14:
	.long	0
	.long	1071644672
	.align 8
.LC15:
	.long	0
	.long	1073741824
	.align 8
.LC16:
	.long	4294967295
	.long	1128267775
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
