	.file	"escape-analysis-reducer.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler21EscapeAnalysisReducer12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"EscapeAnalysisReducer"
	.section	.text._ZNK2v88internal8compiler21EscapeAnalysisReducer12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler21EscapeAnalysisReducer12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler21EscapeAnalysisReducer12reducer_nameEv
	.type	_ZNK2v88internal8compiler21EscapeAnalysisReducer12reducer_nameEv, @function
_ZNK2v88internal8compiler21EscapeAnalysisReducer12reducer_nameEv:
.LFB11008:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE11008:
	.size	_ZNK2v88internal8compiler21EscapeAnalysisReducer12reducer_nameEv, .-_ZNK2v88internal8compiler21EscapeAnalysisReducer12reducer_nameEv
	.section	.text._ZN2v88internal8compiler21EscapeAnalysisReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphENS1_20EscapeAnalysisResultEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21EscapeAnalysisReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphENS1_20EscapeAnalysisResultEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler21EscapeAnalysisReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphENS1_20EscapeAnalysisResultEPNS0_4ZoneE, @function
_ZN2v88internal8compiler21EscapeAnalysisReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphENS1_20EscapeAnalysisResultEPNS0_4ZoneE:
.LFB11081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler21EscapeAnalysisReducerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	128(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$112, %rdi
	subq	$16, %rsp
	movq	%rsi, -104(%rdi)
	movl	$100, %esi
	movq	%rax, -112(%rdi)
	movq	%r8, -80(%rdi)
	movq	$0, -72(%rdi)
	movq	$0, -64(%rdi)
	movq	$0, -56(%rdi)
	movq	(%rdx), %rax
	movq	%rdx, -96(%rdi)
	movq	%rcx, -88(%rdi)
	movq	%rax, -48(%rdi)
	movq	%r8, -40(%rdi)
	movq	%r14, -32(%rdi)
	movq	$1, -24(%rdi)
	movq	$0, -16(%rdi)
	movq	$0, -8(%rdi)
	movl	$0x3f800000, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	cmpq	88(%rbx), %rax
	jbe	.L4
	movq	%rax, %r13
	cmpq	$1, %rax
	je	.L10
	movq	72(%rbx), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdx, %rsi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	%rax, %rdx
	ja	.L11
	addq	%r14, %rsi
	movq	%rsi, 16(%rdi)
.L8:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	memset@PLT
.L6:
	movq	%r14, 80(%rbx)
	movq	%r13, 88(%rbx)
.L4:
	leaq	184(%rbx), %rax
	movq	%r12, 136(%rbx)
	movq	%r12, 168(%rbx)
	movq	%r12, 224(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 160(%rbx)
	movl	$0, 184(%rbx)
	movq	$0, 192(%rbx)
	movq	%rax, 200(%rbx)
	movq	%rax, 208(%rbx)
	movq	$0, 216(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	$0, 128(%rbx)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L8
	.cfi_endproc
.LFE11081:
	.size	_ZN2v88internal8compiler21EscapeAnalysisReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphENS1_20EscapeAnalysisResultEPNS0_4ZoneE, .-_ZN2v88internal8compiler21EscapeAnalysisReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphENS1_20EscapeAnalysisResultEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler21EscapeAnalysisReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphENS1_20EscapeAnalysisResultEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler21EscapeAnalysisReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphENS1_20EscapeAnalysisResultEPNS0_4ZoneE,_ZN2v88internal8compiler21EscapeAnalysisReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphENS1_20EscapeAnalysisResultEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler21EscapeAnalysisReducer11ReplaceNodeEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21EscapeAnalysisReducer11ReplaceNodeEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler21EscapeAnalysisReducer11ReplaceNodeEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler21EscapeAnalysisReducer11ReplaceNodeEPNS1_4NodeES4_:
.LFB11083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler20EscapeAnalysisResult16GetVirtualObjectEPNS1_4NodeE@PLT
	movq	(%r12), %rdx
	cmpw	$61, 16(%rdx)
	je	.L16
	testq	%rax, %rax
	je	.L14
	cmpb	$0, 32(%rax)
	je	.L16
.L14:
	movq	8(%r12), %rax
	movq	8(%r13), %r15
	movq	%rax, -64(%rbp)
	cmpq	%r15, %rax
	je	.L16
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L23
	.p2align 4,,10
	.p2align 3
.L16:
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
.L15:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L24
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%rbx), %rax
	movq	-72(%rbp), %r9
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%r9, %rdx
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%rbx), %rax
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%rbx), %rax
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%r15, %rsi
	movq	%rax, 8(%r13)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TypeGuardENS1_4TypeE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	8(%rbx), %rdi
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%r13, %rdx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L15
.L24:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11083:
	.size	_ZN2v88internal8compiler21EscapeAnalysisReducer11ReplaceNodeEPNS1_4NodeES4_, .-_ZN2v88internal8compiler21EscapeAnalysisReducer11ReplaceNodeEPNS1_4NodeES4_
	.section	.rodata._ZNK2v88internal8compiler21EscapeAnalysisReducer17VerifyReplacementEv.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"Escape analysis failed to remove node %s#%d\n"
	.section	.text._ZNK2v88internal8compiler21EscapeAnalysisReducer17VerifyReplacementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler21EscapeAnalysisReducer17VerifyReplacementEv
	.type	_ZNK2v88internal8compiler21EscapeAnalysisReducer17VerifyReplacementEv, @function
_ZNK2v88internal8compiler21EscapeAnalysisReducer17VerifyReplacementEv:
.LFB11111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	224(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	leaq	-144(%rbp), %rdi
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler8AllNodesC1EPNS0_4ZoneEPKNS1_5GraphEb@PLT
	movq	-136(%rbp), %rbx
	movq	-128(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L25
	leaq	-152(%rbp), %r14
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L25
.L28:
	movq	(%rbx), %r12
	movq	(%r12), %rax
	cmpw	$237, 16(%rax)
	jne	.L27
	movq	24(%r15), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler20EscapeAnalysisResult16GetVirtualObjectEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L27
	cmpb	$0, 32(%rax)
	jne	.L27
	movq	(%r12), %rax
	movl	20(%r12), %edx
	leaq	.LC2(%rip), %rdi
	movq	8(%rax), %rsi
	andl	$16777215, %edx
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L25:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L35
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L35:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11111:
	.size	_ZNK2v88internal8compiler21EscapeAnalysisReducer17VerifyReplacementEv, .-_ZNK2v88internal8compiler21EscapeAnalysisReducer17VerifyReplacementEv
	.section	.text._ZN2v88internal8compiler13NodeHashCache11ConstructorC2EPS2_PKNS1_8OperatorEiPPNS1_4NodeENS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13NodeHashCache11ConstructorC2EPS2_PKNS1_8OperatorEiPPNS1_4NodeENS1_4TypeE
	.type	_ZN2v88internal8compiler13NodeHashCache11ConstructorC2EPS2_PKNS1_8OperatorEiPPNS1_4NodeENS1_4TypeE, @function
_ZN2v88internal8compiler13NodeHashCache11ConstructorC2EPS2_PKNS1_8OperatorEiPPNS1_4NodeENS1_4TypeE:
.LFB11118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -88(%rbp)
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	88(%rsi), %rdx
	movq	%r9, -96(%rbp)
	cmpq	%rdx, 80(%rsi)
	je	.L37
	movq	-8(%rdx), %rdi
	subq	$8, %rdx
	movq	%rdx, 88(%rsi)
	movzbl	23(%rdi), %edx
	movq	%rdi, 16(%r15)
	andl	$15, %edx
	movl	%edx, %r14d
	cmpl	$15, %edx
	jne	.L39
	movq	32(%rdi), %rax
	movl	8(%rax), %r14d
.L39:
	cmpl	%r14d, %r12d
	jle	.L59
.L40:
	testl	%r12d, %r12d
	jle	.L41
	testl	%r14d, %r14d
	jle	.L52
	cmpl	%r14d, %r12d
	cmovle	%r12d, %r14d
	xorl	%ebx, %ebx
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	(%r9,%rsi), %rax
	leaq	1(%rbx), %r9
	movq	(%rax), %r10
	cmpq	%r8, %r10
	je	.L45
.L44:
	leaq	1(%rbx), %r9
	leaq	0(,%r9,4), %rsi
	movq	%r9, %rcx
	subq	%rsi, %rcx
	leaq	(%rdi,%rcx,8), %rsi
	testq	%r10, %r10
	je	.L46
	movq	%r10, %rdi
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rsi
.L46:
	movq	%r8, (%rax)
	testq	%r8, %r8
	je	.L60
	movq	%r8, %rdi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	16(%r15), %rdi
	movq	-56(%rbp), %r9
.L45:
	leal	1(%rbx), %eax
	movq	%r9, %rbx
	cmpl	%r9d, %r14d
	jle	.L61
.L48:
	movzbl	23(%rdi), %eax
	movq	0(%r13,%rbx,8), %r8
	leaq	0(,%rbx,8), %rsi
	leaq	32(%rdi), %r9
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L62
	movq	32(%rdi), %r11
	leaq	1(%rbx), %r9
	leaq	16(%r11,%rsi), %rax
	movq	(%rax), %r10
	cmpq	%r8, %r10
	je	.L45
	movq	%r11, %rdi
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L60:
	leal	1(%rbx), %eax
	movq	16(%r15), %rdi
	movq	%r9, %rbx
	cmpl	%r9d, %r14d
	jg	.L48
	.p2align 4,,10
	.p2align 3
.L61:
	cmpl	%eax, %r12d
	jle	.L41
.L42:
	movslq	%eax, %rbx
	.p2align 4,,10
	.p2align 3
.L50:
	movq	(%r15), %rax
	movq	0(%r13,%rbx,8), %rdx
	addq	$1, %rbx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%r15), %rdi
	cmpl	%ebx, %r12d
	jg	.L50
.L41:
	movq	-88(%rbp), %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%r15), %rax
.L51:
	movq	-96(%rbp), %rcx
	movq	%rcx, 8(%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L59:
	movl	%r12d, %esi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	16(%r15), %rdi
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L37:
	movq	-88(%rbp), %rsi
	movq	(%rax), %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	%r12d, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 16(%r15)
	jmp	.L51
	.cfi_endproc
.LFE11118:
	.size	_ZN2v88internal8compiler13NodeHashCache11ConstructorC2EPS2_PKNS1_8OperatorEiPPNS1_4NodeENS1_4TypeE, .-_ZN2v88internal8compiler13NodeHashCache11ConstructorC2EPS2_PKNS1_8OperatorEiPPNS1_4NodeENS1_4TypeE
	.globl	_ZN2v88internal8compiler13NodeHashCache11ConstructorC1EPS2_PKNS1_8OperatorEiPPNS1_4NodeENS1_4TypeE
	.set	_ZN2v88internal8compiler13NodeHashCache11ConstructorC1EPS2_PKNS1_8OperatorEiPPNS1_4NodeENS1_4TypeE,_ZN2v88internal8compiler13NodeHashCache11ConstructorC2EPS2_PKNS1_8OperatorEiPPNS1_4NodeENS1_4TypeE
	.section	.text._ZN2v88internal8compiler13NodeHashCache11Constructor11MutableNodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13NodeHashCache11Constructor11MutableNodeEv
	.type	_ZN2v88internal8compiler13NodeHashCache11Constructor11MutableNodeEv, @function
_ZN2v88internal8compiler13NodeHashCache11Constructor11MutableNodeEv:
.LFB11121:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L97
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %rax
	movq	88(%rax), %rdx
	cmpq	%rdx, 80(%rax)
	je	.L98
	movq	-8(%rdx), %rdx
	movq	%rdx, 16(%rdi)
	subq	$8, 88(%rax)
	movq	8(%rdi), %rax
	movzbl	23(%rax), %r13d
	andl	$15, %r13d
	cmpl	$15, %r13d
	je	.L99
.L67:
	movq	16(%rbx), %rdi
	movzbl	23(%rdi), %r8d
	andl	$15, %r8d
	movl	%r8d, %r12d
	cmpl	$15, %r8d
	je	.L100
.L69:
	cmpl	%r13d, %r12d
	jge	.L101
.L70:
	testl	%r13d, %r13d
	jle	.L71
	testl	%r12d, %r12d
	jle	.L85
	cmpl	%r13d, %r12d
	movl	%r12d, %r8d
	cmovg	%r13d, %r8d
	xorl	%r12d, %r12d
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L74:
	movzbl	23(%rdi), %edx
	movq	(%rcx), %r9
	leaq	32(%rdi), %r14
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L75
	addq	%rsi, %r14
	leaq	1(%r12), %r15
	movq	(%r14), %r10
	cmpq	%r10, %r9
	je	.L77
.L76:
	leaq	1(%r12), %r15
	leaq	0(,%r15,4), %rax
	movq	%r15, %rcx
	subq	%rax, %rcx
	leaq	(%rdi,%rcx,8), %rsi
	testq	%r10, %r10
	je	.L78
	movq	%r10, %rdi
	movq	%r9, -72(%rbp)
	movl	%r8d, -60(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-72(%rbp), %r9
	movl	-60(%rbp), %r8d
	movq	-56(%rbp), %rsi
.L78:
	movq	%r9, (%r14)
	testq	%r9, %r9
	je	.L102
	movq	%r9, %rdi
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	8(%rbx), %rax
	movq	16(%rbx), %rdi
	movl	-56(%rbp), %r8d
.L77:
	leal	1(%r12), %r14d
	movq	%r15, %r12
	cmpl	%r15d, %r8d
	jle	.L103
.L80:
	movzbl	23(%rax), %edx
	leaq	32(%rax), %rcx
	leaq	0(,%r12,8), %rsi
	addq	%rsi, %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L74
	movq	32(%rax), %rdx
	leaq	16(%rdx,%rsi), %rcx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L75:
	movq	32(%rdi), %rdx
	leaq	1(%r12), %r15
	leaq	16(%rdx,%rsi), %r14
	movq	(%r14), %r10
	cmpq	%r10, %r9
	je	.L77
	movq	%rdx, %rdi
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L102:
	leal	1(%r12), %r14d
	movq	8(%rbx), %rax
	movq	16(%rbx), %rdi
	movq	%r15, %r12
	cmpl	%r15d, %r8d
	jg	.L80
	.p2align 4,,10
	.p2align 3
.L103:
	cmpl	%r14d, %r13d
	jle	.L71
.L72:
	movslq	%r14d, %r12
	salq	$3, %r12
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L104:
	movq	32(%rax,%r12), %rdx
.L96:
	movq	(%rbx), %rax
	addl	$1, %r14d
	addq	$8, %r12
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	8(%rbx), %rax
	movq	16(%rbx), %rdi
	cmpl	%r14d, %r13d
	jle	.L71
.L84:
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L104
	movq	32(%rax), %rax
	movq	16(%rax,%r12), %rdx
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L71:
	movq	8(%rax), %rax
	movq	%rax, 8(%rdi)
	movq	8(%rbx), %rax
	movq	16(%rbx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%rbx), %rax
.L63:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L100:
	movq	32(%rdi), %rdx
	movl	8(%rdx), %r12d
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L99:
	movq	32(%rax), %rdx
	movl	8(%rdx), %r13d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L101:
	movl	%r13d, %esi
	call	_ZN2v88internal8compiler4Node14TrimInputCountEi@PLT
	movq	16(%rbx), %rdi
	movq	8(%rbx), %rax
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L98:
	movq	8(%rdi), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph9CloneNodeEPKNS1_4NodeE@PLT
	movq	%rax, 16(%rbx)
	jmp	.L63
	.cfi_endproc
.LFE11121:
	.size	_ZN2v88internal8compiler13NodeHashCache11Constructor11MutableNodeEv, .-_ZN2v88internal8compiler13NodeHashCache11Constructor11MutableNodeEv
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC3:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB12398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L143
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L121
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L144
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L107:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L145
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L110:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L144:
	testq	%rdx, %rdx
	jne	.L146
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L108:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L111
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L124
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L124
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L113:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L113
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L115
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L115:
	leaq	16(%rax,%r8), %rcx
.L111:
	cmpq	%r14, %r12
	je	.L116
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L125
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L125
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L118:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L118
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L120
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L120:
	leaq	8(%rcx,%r9), %rcx
.L116:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L125:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L117:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L117
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L124:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L112:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L112
	jmp	.L115
.L145:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L110
.L143:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L146:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L107
	.cfi_endproc
.LFE12398:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.rodata._ZN2v88internal8compiler21EscapeAnalysisReducer8FinalizeEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler21EscapeAnalysisReducer8FinalizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21EscapeAnalysisReducer8FinalizeEv
	.type	_ZN2v88internal8compiler21EscapeAnalysisReducer8FinalizeEv, @function
_ZN2v88internal8compiler21EscapeAnalysisReducer8FinalizeEv:
.LFB11115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	addq	$184, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -184(%rbp)
	movq	200(%rdi), %r14
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	leaq	-80(%rbp), %rbx
	movq	%rax, -168(%rbp)
	movq	%rbx, -224(%rbp)
	cmpq	%rax, %r14
	jne	.L148
	jmp	.L147
.L171:
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, -168(%rbp)
	je	.L147
.L148:
	movq	32(%r14), %r12
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler33NewArgumentsElementsMappedCountOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	cmpw	$278, 16(%rax)
	jne	.L171
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rdi
	movq	%rax, -176(%rbp)
	cmpw	$279, 16(%rdi)
	jne	.L171
	call	_ZN2v88internal8compiler14IsRestLengthOfEPKNS1_8OperatorE@PLT
	movb	$2, -192(%rbp)
	testb	%al, %al
	je	.L212
.L150:
	movq	-176(%rbp), %rax
	movq	24(%rax), %r15
	testq	%r15, %r15
	je	.L151
	movq	(%r15), %r9
	movl	%r13d, -208(%rbp)
	xorl	%edx, %edx
	movq	%r12, -200(%rbp)
	movq	%r15, %r12
	movq	%rbx, -216(%rbp)
	movq	%r9, %r15
	movq	%rdx, %rbx
	.p2align 4,,10
	.p2align 3
.L157:
	movl	16(%r12), %edi
	movl	%edi, %ecx
	shrl	%ecx
	leaq	3(%rcx,%rcx,2), %rax
	salq	$3, %rcx
	andl	$1, %edi
	leaq	(%r12,%rax,8), %rsi
	movq	(%rsi), %rax
	leaq	32(%rsi,%rcx), %r13
	jne	.L153
	movq	(%rax), %rax
	leaq	16(%rsi,%rcx), %r13
.L153:
	movzwl	16(%rax), %ecx
	cmpw	$48, %cx
	ja	.L154
	movabsq	$365037860421632, %rax
	btq	%rcx, %rax
	jnc	.L154
	testq	%rbx, %rbx
	je	.L213
.L155:
	movq	0(%r13), %rdi
	cmpq	%rdi, %rbx
	je	.L154
	testq	%rdi, %rdi
	je	.L156
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L156:
	movq	%rbx, 0(%r13)
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L154:
	testq	%r15, %r15
	je	.L205
	movq	%r15, %r12
	movq	(%r15), %r15
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L205:
	movq	-200(%rbp), %r12
	movl	-208(%rbp), %r13d
	movq	-216(%rbp), %rbx
.L151:
	movq	-184(%rbp), %rax
	movq	$0, -88(%rbp)
	movq	24(%r12), %rdi
	movq	$0, -104(%rbp)
	movq	224(%rax), %rax
	movq	$0, -96(%rbp)
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L158
	movq	(%rdi), %r15
	.p2align 4,,10
	.p2align 3
.L172:
	movl	16(%rdi), %ecx
	movl	%ecx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rdx
	salq	$3, %rax
	andl	$1, %ecx
	leaq	(%rdi,%rdx,8), %rdx
	leaq	32(%rdx,%rax), %rsi
	jne	.L160
	leaq	16(%rdx,%rax), %rsi
	movq	(%rdx), %rdx
.L160:
	movq	%rdx, -152(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties11IsValueEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L170
	movq	-152(%rbp), %rax
	leaq	-144(%rbp), %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal8compiler4Node8UseEdges5emptyEv@PLT
	testb	%al, %al
	jne	.L170
	movq	-152(%rbp), %rdx
	movq	(%rdx), %rdi
	movzwl	16(%rdi), %eax
	cmpw	$240, %ax
	je	.L162
	ja	.L163
	leal	-42(%rax), %edx
	cmpw	$6, %dx
	ja	.L171
	movabsq	$365037860421632, %rdx
	btq	%rax, %rdx
	jnc	.L171
.L170:
	testq	%r15, %r15
	je	.L158
.L214:
	movq	%r15, %rdi
	movq	(%r15), %r15
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L163:
	cmpw	$241, %ax
	jne	.L171
	testl	%r13d, %r13d
	jne	.L171
	movq	-96(%rbp), %rsi
	cmpq	-88(%rbp), %rsi
	je	.L169
	movq	%rdx, (%rsi)
	addq	$8, -96(%rbp)
	testq	%r15, %r15
	jne	.L214
	.p2align 4,,10
	.p2align 3
.L158:
	movq	-184(%rbp), %r15
	movzbl	-192(%rbp), %esi
	movq	16(%r15), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder22ArgumentsElementsStateENS0_19CreateArgumentsTypeE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	movq	%r15, %r13
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$16777217, 8(%rax)
	movq	8(%r15), %rdi
	movq	%rax, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-104(%rbp), %r15
	movq	-96(%rbp), %r10
	cmpq	%r15, %r10
	je	.L171
	movq	%r12, -192(%rbp)
	movq	%r15, %r12
	movq	%rbx, -200(%rbp)
	movq	%r13, %rbx
	movq	%r14, -208(%rbp)
	movq	%r10, %r14
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L173:
	movq	-192(%rbp), %rdi
	movl	$1, %esi
	addq	$8, %r12
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	movq	%r15, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpq	%r12, %r14
	je	.L215
.L176:
	movq	(%r12), %r15
	movq	(%r15), %rax
	movzwl	16(%rax), %eax
	cmpw	$240, %ax
	je	.L173
	cmpw	$241, %ax
	jne	.L174
	movl	$1, %esi
	movq	%r15, %rdi
	addq	$8, %r12
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, -216(%rbp)
	movq	16(%rbx), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14NumberSubtractEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	$2, %edx
	movq	-176(%rbp), %xmm0
	movq	-224(%rbp), %rcx
	movq	%rax, %rsi
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	movq	-200(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	552(%rax), %rax
	movq	%rax, 8(%r13)
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
	movq	16(%rbx), %rax
	leaq	-144(%rbp), %r13
	movq	%r13, %rdi
	movq	376(%rax), %r8
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder16ForStackArgumentEv@PLT
	movq	-216(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11LoadElementERKNS1_13ElementAccessE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	cmpq	%r12, %r14
	jne	.L176
.L215:
	movq	-208(%rbp), %r14
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L147:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L216
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	call	_ZN2v88internal8compiler13FieldAccessOfEPKNS1_8OperatorE@PLT
	cmpl	$8, 4(%rax)
	jne	.L171
	movq	-96(%rbp), %rsi
	cmpq	-88(%rbp), %rsi
	je	.L169
	movq	-152(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -96(%rbp)
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L213:
	movq	-184(%rbp), %rax
	movzbl	-192(%rbp), %esi
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder20ArgumentsLengthStateENS0_19CreateArgumentsTypeE@PLT
	movq	%rbx, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	$16777217, 8(%rax)
	movq	%rax, %rbx
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L212:
	testl	%r13d, %r13d
	sete	-192(%rbp)
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L169:
	leaq	-152(%rbp), %rdx
	leaq	-112(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L170
.L174:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L216:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11115:
	.size	_ZN2v88internal8compiler21EscapeAnalysisReducer8FinalizeEv, .-_ZN2v88internal8compiler21EscapeAnalysisReducer8FinalizeEv
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB12428:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L225
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L219:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L219
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE12428:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZN2v88internal8compiler21EscapeAnalysisReducerD2Ev,"axG",@progbits,_ZN2v88internal8compiler21EscapeAnalysisReducerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler21EscapeAnalysisReducerD2Ev
	.type	_ZN2v88internal8compiler21EscapeAnalysisReducerD2Ev, @function
_ZN2v88internal8compiler21EscapeAnalysisReducerD2Ev:
.LFB13404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler21EscapeAnalysisReducerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	192(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L229
	leaq	168(%rdi), %r14
.L232:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L230
.L231:
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L231
.L230:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L232
.L229:
	movq	96(%r13), %rax
	testq	%rax, %rax
	je	.L233
	.p2align 4,,10
	.p2align 3
.L234:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L234
.L233:
	movq	88(%r13), %rax
	movq	80(%r13), %rdi
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	leaq	0(,%rax,8), %rdx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	memset@PLT
	.cfi_endproc
.LFE13404:
	.size	_ZN2v88internal8compiler21EscapeAnalysisReducerD2Ev, .-_ZN2v88internal8compiler21EscapeAnalysisReducerD2Ev
	.weak	_ZN2v88internal8compiler21EscapeAnalysisReducerD1Ev
	.set	_ZN2v88internal8compiler21EscapeAnalysisReducerD1Ev,_ZN2v88internal8compiler21EscapeAnalysisReducerD2Ev
	.section	.text._ZN2v88internal8compiler21EscapeAnalysisReducerD0Ev,"axG",@progbits,_ZN2v88internal8compiler21EscapeAnalysisReducerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler21EscapeAnalysisReducerD0Ev
	.type	_ZN2v88internal8compiler21EscapeAnalysisReducerD0Ev, @function
_ZN2v88internal8compiler21EscapeAnalysisReducerD0Ev:
.LFB13406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler21EscapeAnalysisReducerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	192(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%rbx, %rbx
	je	.L249
	leaq	168(%rdi), %r14
.L252:
	movq	24(%rbx), %r13
	testq	%r13, %r13
	je	.L250
.L251:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L251
.L250:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L252
.L249:
	movq	96(%r12), %rax
	testq	%rax, %rax
	je	.L253
	.p2align 4,,10
	.p2align 3
.L254:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L254
.L253:
	movq	88(%r12), %rax
	movq	80(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$232, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13406:
	.size	_ZN2v88internal8compiler21EscapeAnalysisReducerD0Ev, .-_ZN2v88internal8compiler21EscapeAnalysisReducerD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC5:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm:
.LFB12430:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L296
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$268435455, %esi
	subq	$24, %rsp
	movq	16(%rdi), %rcx
	movq	24(%r13), %rax
	movq	%rcx, %rdx
	subq	8(%rdi), %rdx
	subq	%rcx, %rax
	movq	%rsi, %rdi
	movq	%rdx, %r14
	sarq	$3, %rax
	sarq	$3, %r14
	subq	%r14, %rdi
	cmpq	%rbx, %rax
	jb	.L270
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 16(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L299
	cmpq	%r14, %rbx
	movq	%r14, %r15
	movq	0(%r13), %rdi
	cmovnb	%rbx, %r15
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	addq	%r14, %r15
	cmpq	$268435455, %r15
	cmova	%rsi, %r15
	subq	%r12, %rax
	salq	$3, %r15
	movq	%r15, %rsi
	cmpq	%r15, %rax
	jb	.L300
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L274:
	leaq	0(,%rbx,8), %r8
	leaq	(%r12,%rdx), %rdi
	xorl	%esi, %esi
	movq	%r8, %rdx
	call	memset@PLT
	movq	16(%r13), %rax
	movq	8(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L279
	subq	$8, %rax
	leaq	15(%rdx), %rcx
	subq	%rdx, %rax
	subq	%r12, %rcx
	movq	%rax, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rcx
	jbe	.L276
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rsi
	je	.L276
	addq	$1, %rsi
	movq	%rdx, %rdi
	movq	%r12, %rax
	movq	%rsi, %rcx
	subq	%r12, %rdi
	shrq	%rcx
	salq	$4, %rcx
	addq	%r12, %rcx
	.p2align 4,,10
	.p2align 3
.L277:
	movdqu	(%rax,%rdi), %xmm0
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rcx
	jne	.L277
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	addq	%rcx, %rdx
	addq	%r12, %rcx
	cmpq	%rax, %rsi
	je	.L279
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
.L279:
	addq	%r14, %rbx
	movq	%r12, 8(%r13)
	leaq	(%r12,%rbx,8), %rax
	addq	%r15, %r12
	movq	%rax, 16(%r13)
	movq	%r12, 24(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L276:
	.cfi_restore_state
	leaq	8(%rax,%r12), %rsi
	subq	%r12, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L281:
	movq	(%rax,%rdx), %rcx
	addq	$8, %rax
	movq	%rcx, -8(%rax)
	cmpq	%rax, %rsi
	jne	.L281
	jmp	.L279
.L300:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L274
.L299:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12430:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	.section	.text._ZN2v88internal8compiler21EscapeAnalysisReducer12ObjectIdNodeEPKNS1_13VirtualObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21EscapeAnalysisReducer12ObjectIdNodeEPKNS1_13VirtualObjectE
	.type	_ZN2v88internal8compiler21EscapeAnalysisReducer12ObjectIdNodeEPKNS1_13VirtualObjectE, @function
_ZN2v88internal8compiler21EscapeAnalysisReducer12ObjectIdNodeEPKNS1_13VirtualObjectE:
.LFB11085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	48(%rdi), %rcx
	movq	%rdi, %rbx
	movq	40(%rdi), %rdx
	movl	36(%rsi), %r12d
	movq	%rcx, %rax
	subq	%rdx, %rax
	movq	%r12, %r13
	sarq	$3, %rax
	cmpq	%rax, %r12
	jnb	.L306
.L302:
	movq	(%rdx,%r12,8), %rax
	testq	%rax, %rax
	je	.L307
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	leal	1(%r12), %esi
	cmpq	%rax, %rsi
	ja	.L308
	jnb	.L302
	leaq	(%rdx,%rsi,8), %rax
	cmpq	%rax, %rcx
	je	.L302
	movq	%rax, 48(%rdi)
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L307:
	movq	16(%rbx), %rax
	movl	%r13d, %esi
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder8ObjectIdEj@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	$73859073, 8(%rax)
	movq	40(%rbx), %rdx
	movq	%rax, (%rdx,%r12,8)
	movq	40(%rbx), %rax
	popq	%rbx
	movq	(%rax,%r12,8), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	subq	%rax, %rsi
	leaq	32(%rdi), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	40(%rbx), %rdx
	jmp	.L302
	.cfi_endproc
.LFE11085:
	.size	_ZN2v88internal8compiler21EscapeAnalysisReducer12ObjectIdNodeEPKNS1_13VirtualObjectE, .-_ZN2v88internal8compiler21EscapeAnalysisReducer12ObjectIdNodeEPKNS1_13VirtualObjectE
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_:
.LFB12431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L323
	movq	(%rsi), %rsi
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L330:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L312
.L331:
	movq	%rax, %r12
.L311:
	movq	32(%r12), %rcx
	cmpq	%rcx, %rsi
	jb	.L330
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L331
.L312:
	testb	%dl, %dl
	jne	.L310
	cmpq	%rcx, %rsi
	jbe	.L316
.L321:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L332
.L317:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$39, %rax
	jbe	.L333
	leaq	40(%r13), %rax
	movq	%rax, 16(%rdi)
.L319:
	movq	(%r14), %rax
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	movq	%r15, %r12
.L310:
	cmpq	%r12, 32(%rbx)
	je	.L321
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r14), %rcx
	cmpq	%rcx, 32(%rax)
	jb	.L321
	movq	%rax, %r12
.L316:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movq	32(%r12), %rax
	cmpq	%rax, (%r14)
	setb	%r8b
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L333:
	movl	$40, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L319
	.cfi_endproc
.LFE12431:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.section	.text._ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS4_,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS4_
	.type	_ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS4_, @function
_ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS4_:
.LFB12482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler14NodeProperties8HashCodeEPNS1_4NodeE@PLT
	xorl	%edx, %edx
	movq	%rax, %r12
	divq	16(%rbx)
	movq	8(%rbx), %rax
	movq	(%rax,%rdx,8), %r15
	testq	%r15, %r15
	je	.L335
	movq	(%r15), %r13
	movq	%rdx, %r8
	movq	16(%r13), %rcx
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L352:
	movq	0(%r13), %rsi
	testq	%rsi, %rsi
	je	.L335
.L350:
	movq	16(%rsi), %rcx
	xorl	%edx, %edx
	movq	%r13, %r15
	movq	%rcx, %rax
	divq	16(%rbx)
	cmpq	%rdx, %r8
	jne	.L335
	movq	%rsi, %r13
.L340:
	cmpq	%rcx, %r12
	jne	.L352
	movq	8(%r13), %rsi
	movq	(%r14), %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties6EqualsEPNS1_4NodeES4_@PLT
	testb	%al, %al
	jne	.L338
	movq	0(%r13), %rsi
	movq	-56(%rbp), %r8
	testq	%rsi, %rsi
	jne	.L350
.L335:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L335
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12482:
	.size	_ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS4_, .-_ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS4_
	.section	.text._ZN2v88internal8compiler13NodeHashCache5QueryEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13NodeHashCache5QueryEPNS1_4NodeE
	.type	_ZN2v88internal8compiler13NodeHashCache5QueryEPNS1_4NodeE, @function
_ZN2v88internal8compiler13NodeHashCache5QueryEPNS1_4NodeE:
.LFB11116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -8(%rbp)
	leaq	-8(%rbp), %rsi
	call	_ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS4_
	testq	%rax, %rax
	je	.L353
	movq	8(%rax), %rax
.L353:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11116:
	.size	_ZN2v88internal8compiler13NodeHashCache5QueryEPNS1_4NodeE, .-_ZN2v88internal8compiler13NodeHashCache5QueryEPNS1_4NodeE
	.section	.rodata._ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb.str1.1,"aMS",@progbits,1
.LC6:
	.string	"vector<bool>::_M_fill_insert"
	.section	.text._ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb,"axG",@progbits,_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb
	.type	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb, @function
_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb:
.LFB12811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	testq	%rcx, %rcx
	je	.L359
	movq	%rdi, %rbx
	movq	8(%rdi), %rax
	movq	%rsi, -80(%rbp)
	movq	%rsi, %r12
	movq	24(%rbx), %r9
	movq	40(%rbx), %rsi
	movq	%rcx, %r13
	movl	32(%rdi), %edx
	movl	-56(%rbp), %r14d
	movq	%r9, %rcx
	subq	%rax, %rsi
	subq	%rax, %rcx
	movq	%rsi, %rax
	movq	%rdx, %rdi
	leaq	(%rdx,%rcx,8), %rcx
	salq	$3, %rax
	subq	%rcx, %rax
	cmpq	%r13, %rax
	jb	.L361
	leaq	0(%r13,%rdx), %rax
	testq	%rax, %rax
	leaq	63(%rax), %rcx
	cmovns	%rax, %rcx
	sarq	$6, %rcx
	leaq	(%r9,%rcx,8), %r8
	movq	%rax, %rcx
	sarq	$63, %rcx
	shrq	$58, %rcx
	addq	%rcx, %rax
	andl	$63, %eax
	subq	%rcx, %rax
	jns	.L362
	addq	$64, %rax
	subq	$8, %r8
.L362:
	movabsq	$-9223372036854775808, %r15
	movq	%r9, %rcx
	movl	%r14d, %r11d
	subq	%r12, %rcx
	movq	%r11, -88(%rbp)
	leaq	(%rdx,%rcx,8), %rsi
	subq	%r11, %rsi
	movl	$1, %r11d
	testq	%rsi, %rsi
	jle	.L371
	.p2align 4,,10
	.p2align 3
.L363:
	testl	%edi, %edi
	je	.L366
.L443:
	subl	$1, %edi
	movq	%r11, %r10
	movl	%edi, %ecx
	salq	%cl, %r10
	testl	%eax, %eax
	je	.L368
.L444:
	subl	$1, %eax
	movq	%r11, %rdx
	movl	%eax, %ecx
	salq	%cl, %rdx
.L369:
	movq	(%r8), %rcx
	testq	%r10, (%r9)
	je	.L370
	orq	%rcx, %rdx
	movq	%rdx, (%r8)
	subq	$1, %rsi
	jne	.L363
.L371:
	movq	-88(%rbp), %rax
	addq	%r13, %rax
	leaq	63(%rax), %rdx
	cmovns	%rax, %rdx
	sarq	$6, %rdx
	leaq	(%r12,%rdx,8), %r8
	cqto
	shrq	$58, %rdx
	leaq	(%rax,%rdx), %r15
	andl	$63, %r15d
	subq	%rdx, %r15
	jns	.L365
	addq	$64, %r15
	subq	$8, %r8
.L365:
	movl	%r15d, %r9d
	cmpq	%r12, %r8
	je	.L373
	testl	%r14d, %r14d
	jne	.L437
	movq	%r8, %r10
	subq	%r12, %r10
	cmpb	$0, -72(%rbp)
	je	.L377
.L376:
	movq	-80(%rbp), %rdi
	movq	%r10, %rdx
	movl	$-1, %esi
	movl	%r9d, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	memset@PLT
	testq	%r15, %r15
	movq	-72(%rbp), %r8
	movl	-88(%rbp), %r9d
	je	.L379
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r9d, %ecx
	shrq	%cl, %rax
	orq	%rax, (%r8)
	.p2align 4,,10
	.p2align 3
.L379:
	movl	32(%rbx), %ecx
	movq	24(%rbx), %rdx
	addq	%rcx, %r13
	leaq	63(%r13), %rax
	cmovns	%r13, %rax
	sarq	$6, %rax
	leaq	(%rdx,%rax,8), %rdx
	movq	%r13, %rax
	sarq	$63, %rax
	shrq	$58, %rax
	addq	%rax, %r13
	andl	$63, %r13d
	subq	%rax, %r13
	jns	.L434
	addq	$64, %r13
	subq	$8, %rdx
.L434:
	movq	%rdx, 24(%rbx)
.L435:
	movl	%r13d, 32(%rbx)
.L359:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	movabsq	$17179869120, %rax
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %r13
	ja	.L438
	cmpq	%rcx, %r13
	movq	%rcx, %rdx
	cmovnb	%r13, %rdx
	addq	%rdx, %rcx
	jc	.L387
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	addq	$63, %rcx
	shrq	$6, %rcx
	leaq	0(,%rcx,8), %rax
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
.L388:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L439
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L390:
	movq	8(%rbx), %rsi
	movq	%r12, %rdx
	subq	%rsi, %rdx
	cmpq	%r12, %rsi
	je	.L391
	movq	%r15, %rdi
	movq	%rdx, -96(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %rdx
.L391:
	movl	%r14d, %esi
	leaq	(%r15,%rdx), %rdi
	movq	%rsi, -96(%rbp)
	testq	%rsi, %rsi
	je	.L419
	xorl	%edx, %edx
	movq	%r12, %r9
	movl	$1, %r10d
	movl	%edx, %ecx
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L395:
	addl	$1, %ecx
	subq	$1, %rsi
	je	.L440
.L397:
	movq	%r10, %rdx
	movq	(%rdi), %r11
	salq	%cl, %rdx
	movq	%rdx, %rax
	movq	%r11, %r8
	notq	%rax
	orq	%rdx, %r8
	andq	%r11, %rax
	testq	%rdx, (%r9)
	cmovne	%r8, %rax
	movq	%rax, (%rdi)
	cmpl	$63, %ecx
	jne	.L395
	addq	$8, %r9
	addq	$8, %rdi
	xorl	%ecx, %ecx
	subq	$1, %rsi
	jne	.L397
.L440:
	movl	%ecx, %ecx
	movq	%rcx, %rdx
.L392:
	addq	%rcx, %r13
	leaq	63(%r13), %rax
	cmovns	%r13, %rax
	sarq	$6, %rax
	leaq	(%rdi,%rax,8), %r8
	movq	%r13, %rax
	sarq	$63, %rax
	shrq	$58, %rax
	leaq	0(%r13,%rax), %r9
	andl	$63, %r9d
	subq	%rax, %r9
	jns	.L398
	addq	$64, %r9
	subq	$8, %r8
.L398:
	movl	%r9d, %r13d
	cmpq	%rdi, %r8
	je	.L399
	testl	%edx, %edx
	je	.L400
	movl	%edx, %ecx
	leaq	8(%rdi), %rsi
	movq	$-1, %rax
	movq	%r8, %rdx
	salq	%cl, %rax
	subq	%rsi, %rdx
	cmpb	$0, -72(%rbp)
	movq	(%rdi), %rcx
	je	.L401
	orq	%rcx, %rax
	movq	%rax, (%rdi)
	movq	%rsi, %rdi
.L402:
	movl	$-1, %esi
	movq	%r8, -104(%rbp)
	movq	%r9, -72(%rbp)
	call	memset@PLT
	movq	-72(%rbp), %r9
	movq	-104(%rbp), %r8
	testq	%r9, %r9
	jne	.L441
	.p2align 4,,10
	.p2align 3
.L405:
	movq	24(%rbx), %rax
	movl	32(%rbx), %edx
	subq	%r12, %rax
	leaq	(%rdx,%rax,8), %rdx
	subq	-96(%rbp), %rdx
	testq	%rdx, %rdx
	jle	.L409
	movq	-80(%rbp), %r9
	movl	$1, %edi
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L412:
	addl	$1, %r14d
	cmpl	$63, %r13d
	je	.L442
.L414:
	addl	$1, %r13d
	subq	$1, %rdx
	je	.L409
.L416:
	movq	(%r8), %rsi
	movl	%r13d, %ecx
	movq	%rdi, %rax
	movq	%rdi, %r10
	salq	%cl, %rax
	movl	%r14d, %ecx
	salq	%cl, %r10
	movq	%rsi, %rcx
	orq	%rax, %rcx
	notq	%rax
	andq	%rsi, %rax
	testq	%r10, (%r9)
	cmovne	%rcx, %rax
	movq	%rax, (%r8)
	cmpl	$63, %r14d
	jne	.L412
	addq	$8, %r9
	xorl	%r14d, %r14d
	cmpl	$63, %r13d
	jne	.L414
.L442:
	addq	$8, %r8
	xorl	%r13d, %r13d
	subq	$1, %rdx
	jne	.L416
.L409:
	cmpq	$0, 8(%rbx)
	je	.L417
	movq	$0, 24(%rbx)
	movl	$0, 32(%rbx)
	movq	$0, 8(%rbx)
	movl	$0, 16(%rbx)
.L417:
	movq	-88(%rbp), %rax
	movq	%r15, 8(%rbx)
	movl	$0, 16(%rbx)
	addq	%r15, %rax
	movq	%r8, 24(%rbx)
	movq	%rax, 40(%rbx)
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L370:
	notq	%rdx
	andq	%rcx, %rdx
	movq	%rdx, (%r8)
	subq	$1, %rsi
	je	.L371
	testl	%edi, %edi
	jne	.L443
.L366:
	subq	$8, %r9
	movq	%r15, %r10
	movl	$63, %edi
	testl	%eax, %eax
	jne	.L444
.L368:
	subq	$8, %r8
	movq	%r15, %rdx
	movl	$63, %eax
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L437:
	movl	%r14d, %ecx
	leaq	8(%r12), %rdx
	movq	$-1, %rax
	movq	%r8, %r10
	salq	%cl, %rax
	subq	%rdx, %r10
	cmpb	$0, -72(%rbp)
	movq	(%r12), %rcx
	je	.L375
	orq	%rcx, %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, (%r12)
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L375:
	notq	%rax
	movq	%rdx, -80(%rbp)
	andq	%rcx, %rax
	movq	%rax, (%r12)
.L377:
	movq	-80(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r10, %rdx
	movl	%r9d, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	memset@PLT
	testq	%r15, %r15
	movq	-72(%rbp), %r8
	movl	-88(%rbp), %r9d
	je	.L379
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r9d, %ecx
	shrq	%cl, %rax
	notq	%rax
	andq	%rax, (%r8)
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L373:
	cmpl	%r14d, %r15d
	je	.L379
	movq	$-1, %rdx
	movl	$64, %ecx
	subl	%r15d, %ecx
	movq	%rdx, %rax
	shrq	%cl, %rax
	movl	%r14d, %ecx
	salq	%cl, %rdx
	movq	(%r8), %rcx
	andq	%rdx, %rax
	movq	%rax, %rdx
	orq	%rcx, %rax
	notq	%rdx
	andq	%rcx, %rdx
	cmpb	$0, -72(%rbp)
	cmove	%rdx, %rax
	movq	%rax, (%r8)
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L400:
	movq	%r8, %rdx
	subq	%rdi, %rdx
	cmpb	$0, -72(%rbp)
	jne	.L402
.L403:
	xorl	%esi, %esi
	movq	%r8, -104(%rbp)
	movq	%r9, -72(%rbp)
	call	memset@PLT
	movq	-72(%rbp), %r9
	movq	-104(%rbp), %r8
	testq	%r9, %r9
	je	.L405
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r13d, %ecx
	shrq	%cl, %rax
	notq	%rax
	andq	%rax, (%r8)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L399:
	cmpl	%edx, %r9d
	je	.L405
	movq	$-1, %rsi
	movl	$64, %ecx
	subl	%r9d, %ecx
	movq	%rsi, %rax
	shrq	%cl, %rax
	movl	%edx, %ecx
	salq	%cl, %rsi
	movq	(%r8), %rcx
	andq	%rsi, %rax
	movq	%rax, %rdx
	orq	%rcx, %rax
	notq	%rdx
	andq	%rcx, %rdx
	cmpb	$0, -72(%rbp)
	cmove	%rdx, %rax
	movq	%rax, (%r8)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L441:
	movl	$64, %ecx
	movq	$-1, %rax
	subl	%r13d, %ecx
	shrq	%cl, %rax
	orq	%rax, (%r8)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L401:
	notq	%rax
	andq	%rcx, %rax
	movq	%rax, (%rdi)
	movq	%rsi, %rdi
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L439:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L419:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.L392
.L438:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L387:
	movq	$2147483640, -88(%rbp)
	movl	$2147483640, %esi
	jmp	.L388
	.cfi_endproc
.LFE12811:
	.size	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb, .-_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB12819:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L459
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L455
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L460
.L447:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L454:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L461
	testq	%r13, %r13
	jg	.L450
	testq	%r9, %r9
	jne	.L453
.L451:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L450
.L453:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L460:
	testq	%rsi, %rsi
	jne	.L448
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L450:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L451
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L455:
	movl	$8, %r14d
	jmp	.L447
.L459:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L448:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L447
	.cfi_endproc
.LFE12819:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm
	.type	_ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm, @function
_ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm:
.LFB12968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L486
.L463:
	movq	%r14, 16(%r12)
	movq	8(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L472
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L473:
	addq	$1, 32(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L486:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L487
	movq	(%rbx), %r8
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L488
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L467:
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %rdi
.L465:
	movq	24(%rbx), %rsi
	movq	$0, 24(%rbx)
	testq	%rsi, %rsi
	je	.L468
	xorl	%r9d, %r9d
	leaq	24(%rbx), %r10
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L470:
	movq	(%r8), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L471:
	testq	%rsi, %rsi
	je	.L468
.L469:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	16(%rcx), %rax
	divq	%r13
	leaq	(%rdi,%rdx,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L470
	movq	24(%rbx), %r8
	movq	%r8, (%rcx)
	movq	%rcx, 24(%rbx)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L475
	movq	%rcx, (%rdi,%r9,8)
	movq	%rdx, %r9
	testq	%rsi, %rsi
	jne	.L469
	.p2align 4,,10
	.p2align 3
.L468:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r13, 16(%rbx)
	divq	%r13
	movq	%rdi, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L472:
	movq	24(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 24(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L474
	movq	16(%rax), %rax
	xorl	%edx, %edx
	divq	16(%rbx)
	movq	8(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L474:
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L475:
	movq	%rdx, %r9
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L487:
	movq	$0, 56(%rbx)
	leaq	56(%rbx), %rdi
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L488:
	movq	%r8, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L467
	.cfi_endproc
.LFE12968:
	.size	_ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm, .-_ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm
	.section	.text._ZN2v88internal8compiler13NodeHashCache11Constructor3GetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13NodeHashCache11Constructor3GetEv
	.type	_ZN2v88internal8compiler13NodeHashCache11Constructor3GetEv, @function
_ZN2v88internal8compiler13NodeHashCache11Constructor3GetEv:
.LFB11120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	leaq	8(%rdx), %rdi
	testq	%rax, %rax
	je	.L519
	leaq	-64(%rbp), %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS4_
	testq	%rax, %rax
	je	.L520
	movq	8(%rax), %r13
	movq	(%rbx), %r12
	testq	%r13, %r13
	je	.L495
	movq	88(%r12), %rsi
	cmpq	96(%r12), %rsi
	je	.L496
	movq	16(%rbx), %rax
	movq	%rax, (%rsi)
	addq	$8, 88(%r12)
.L493:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L521
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	.cfi_restore_state
	movq	(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L495:
	movq	16(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8HashCodeEPNS1_4NodeE@PLT
	xorl	%edx, %edx
	movq	%rax, %r15
	divq	24(%r12)
	movq	16(%r12), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	testq	%r8, %r8
	je	.L501
	movq	(%r8), %r14
	movq	16(%r14), %rcx
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L504:
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.L501
	movq	16(%rsi), %rcx
	xorl	%edx, %edx
	movq	%r14, %r8
	movq	%rcx, %rax
	divq	24(%r12)
	cmpq	%rdx, %r9
	jne	.L501
	movq	%rsi, %r14
.L505:
	cmpq	%rcx, %r15
	jne	.L504
	movq	8(%r14), %rsi
	movq	%r13, %rdi
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties6EqualsEPNS1_4NodeES4_@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
	testb	%al, %al
	je	.L504
	cmpq	$0, (%r8)
	jne	.L493
.L501:
	movq	8(%r12), %rdi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	$23, %rax
	jbe	.L522
	leaq	24(%rcx), %rax
	movq	%rax, 16(%rdi)
.L506:
	movq	%r13, 8(%rcx)
	leaq	8(%r12), %rdi
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	$0, (%rcx)
	movl	$1, %r8d
	call	_ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L519:
	movq	8(%rbx), %rax
	leaq	-64(%rbp), %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt10_HashtableIPN2v88internal8compiler4NodeES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS2_13NodeHashCache10NodeEqualsENS9_12NodeHashCodeENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS4_
	testq	%rax, %rax
	je	.L492
	movq	8(%rax), %r13
	testq	%r13, %r13
	jne	.L493
.L492:
	movq	8(%rbx), %r13
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L496:
	leaq	16(%rbx), %rdx
	leaq	72(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L522:
	movl	$24, %esi
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L506
.L521:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11120:
	.size	_ZN2v88internal8compiler13NodeHashCache11Constructor3GetEv, .-_ZN2v88internal8compiler13NodeHashCache11Constructor3GetEv
	.section	.rodata._ZN2v88internal8compiler21EscapeAnalysisReducer16ReduceDeoptStateEPNS1_4NodeES4_PNS1_12DeduplicatorE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"(field) != nullptr"
.LC8:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler21EscapeAnalysisReducer16ReduceDeoptStateEPNS1_4NodeES4_PNS1_12DeduplicatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21EscapeAnalysisReducer16ReduceDeoptStateEPNS1_4NodeES4_PNS1_12DeduplicatorE
	.type	_ZN2v88internal8compiler21EscapeAnalysisReducer16ReduceDeoptStateEPNS1_4NodeES4_PNS1_12DeduplicatorE, @function
_ZN2v88internal8compiler21EscapeAnalysisReducer16ReduceDeoptStateEPNS1_4NodeES4_PNS1_12DeduplicatorE:
.LFB11098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -152(%rbp)
	movq	(%rsi), %rdx
	movq	%rcx, -144(%rbp)
	movq	%rsi, -136(%rbp)
	movzwl	16(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpw	$41, %cx
	je	.L578
	cmpl	$42, %ecx
	je	.L579
	movq	24(%rdi), %rdx
	movq	%rsi, %rdi
	movq	%rdx, -80(%rbp)
	cmpw	$58, %cx
	jne	.L544
	.p2align 4,,10
	.p2align 3
.L545:
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	cmpw	$58, 16(%rax)
	je	.L545
.L544:
	leaq	-80(%rbp), %r14
	movq	%rdi, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler20EscapeAnalysisResult16GetVirtualObjectEPNS1_4NodeE@PLT
	movq	%rax, %r12
	movq	-136(%rbp), %rax
	testq	%r12, %r12
	je	.L523
	cmpb	$0, 32(%r12)
	je	.L580
	.p2align 4,,10
	.p2align 3
.L523:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L581
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L579:
	.cfi_restore_state
	leaq	64(%rdi), %rax
	movq	$0, -64(%rbp)
	xorl	%ebx, %ebx
	leaq	-80(%rbp), %r14
	movq	%rax, -80(%rbp)
	movq	-136(%rbp), %rax
	movq	%rax, -72(%rbp)
	movl	20(%rdx), %edx
	movq	%rax, %r12
	testl	%edx, %edx
	jle	.L543
.L539:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21EscapeAnalysisReducer16ReduceDeoptStateEPNS1_4NodeES4_PNS1_12DeduplicatorE
	cmpq	$0, -64(%rbp)
	movq	%rax, %r13
	je	.L540
.L542:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13NodeHashCache11Constructor11MutableNodeEv
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler14NodeProperties17ReplaceValueInputEPNS1_4NodeES4_i@PLT
.L541:
	movq	(%r12), %rax
	addl	$1, %ebx
	cmpl	20(%rax), %ebx
	jl	.L539
.L543:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13NodeHashCache11Constructor3GetEv
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L578:
	leaq	64(%rdi), %rax
	movq	%rsi, -72(%rbp)
	leaq	C.51.361419(%rip), %r13
	movl	$5, %ebx
	movq	%rax, -80(%rbp)
	movq	%rsi, %rax
	leaq	-80(%rbp), %r14
	addq	$32, %rax
	movq	$0, -64(%rbp)
	movq	%rax, -168(%rbp)
	movq	%rdi, -160(%rbp)
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L527:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13NodeHashCache11Constructor11MutableNodeEv
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L531
	leaq	32(%rax,%r12), %r12
	movq	(%r12), %rdi
	cmpq	%rdi, %r15
	je	.L530
.L532:
	notl	%ebx
	movslq	%ebx, %rbx
	leaq	(%rbx,%rbx,2), %rdx
	leaq	(%rax,%rdx,8), %rbx
	testq	%rdi, %rdi
	je	.L534
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L534:
	movq	%r15, (%r12)
	testq	%r15, %r15
	je	.L530
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L530:
	addq	$4, %r13
	leaq	24+C.51.361419(%rip), %rax
	cmpq	%r13, %rax
	je	.L543
	movl	0(%r13), %ebx
.L536:
	movq	-136(%rbp), %rax
	movslq	%ebx, %r12
	salq	$3, %r12
	movzbl	23(%rax), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	movq	-168(%rbp), %rax
	je	.L525
	addq	%r12, %rax
.L526:
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movq	(%rax), %rsi
	movq	-160(%rbp), %rdi
	call	_ZN2v88internal8compiler21EscapeAnalysisReducer16ReduceDeoptStateEPNS1_4NodeES4_PNS1_12DeduplicatorE
	cmpq	$0, -64(%rbp)
	movq	%rax, %r15
	jne	.L527
	movq	-72(%rbp), %rcx
	movzbl	23(%rcx), %eax
	leaq	32(%rcx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L528
	leaq	(%rdx,%r12), %rax
.L529:
	cmpq	(%rax), %r15
	jne	.L527
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L525:
	movq	(%rax), %rax
	leaq	16(%rax,%r12), %rax
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L531:
	movq	32(%rax), %rax
	leaq	16(%rax,%r12), %r12
	movq	(%r12), %rdi
	cmpq	%rdi, %r15
	jne	.L532
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L528:
	movq	32(%rcx), %rax
	leaq	16(%rax,%r12), %rax
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L540:
	movq	-72(%rbp), %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	cmpq	%rax, %r13
	jne	.L542
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L580:
	movq	-144(%rbp), %rcx
	movl	36(%r12), %ebx
	movq	24(%rcx), %rsi
	movq	8(%rcx), %rax
	movq	%rbx, %r13
	movl	32(%rcx), %edx
	movq	%rsi, %rcx
	subq	%rax, %rcx
	movq	%rdx, %rdi
	leaq	(%rdx,%rcx,8), %rdx
	cmpq	%rdx, %rbx
	jnb	.L582
.L546:
	shrq	$6, %rbx
	movl	%r13d, %ecx
	leaq	(%rax,%rbx,8), %rsi
	movl	$1, %eax
	movq	(%rsi), %rdx
	salq	%cl, %rax
	movq	%rax, %rcx
	orq	%rdx, %rcx
	movq	%rcx, (%rsi)
	testq	%rdx, %rax
	jne	.L583
	movq	56(%r12), %rax
	subq	48(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	sarq	$2, %rax
	movaps	%xmm0, -112(%rbp)
	sall	$3, %eax
	testl	%eax, %eax
	jle	.L562
	leaq	-112(%rbp), %rax
	movq	%r14, -160(%rbp)
	xorl	%ebx, %ebx
	movq	-152(%rbp), %r14
	movq	%rax, -176(%rbp)
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L551:
	cmpq	%rax, %r13
	je	.L552
	movq	-144(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21EscapeAnalysisReducer16ReduceDeoptStateEPNS1_4NodeES4_PNS1_12DeduplicatorE
	movq	-104(%rbp), %rsi
	movq	%rax, -80(%rbp)
	cmpq	-96(%rbp), %rsi
	je	.L553
	movq	%rax, (%rsi)
	addq	$8, -104(%rbp)
.L552:
	movq	56(%r12), %rax
	subq	48(%r12), %rax
	addl	$8, %ebx
	sarq	$2, %rax
	sall	$3, %eax
	cmpl	%eax, %ebx
	jge	.L584
.L555:
	movq	24(%r15), %rax
	movq	-160(%rbp), %rdi
	movq	%r14, %rcx
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler20EscapeAnalysisResult21GetVirtualObjectFieldEPKNS1_13VirtualObjectEiPNS1_4NodeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L585
	movq	16(%r15), %r10
	movq	352(%r10), %rax
	testq	%rax, %rax
	jne	.L551
	movq	(%r10), %r11
	movq	8(%r10), %rdi
	movq	%r10, -152(%rbp)
	movq	%r11, -168(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	-168(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-152(%rbp), %r10
	movq	%rax, 352(%r10)
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L583:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21EscapeAnalysisReducer12ObjectIdNodeEPKNS1_13VirtualObjectE
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L582:
	leal	1(%rbx), %ecx
	movq	%rcx, %r8
	cmpq	%rcx, %rdx
	jbe	.L547
	sarq	$6, %rcx
	andl	$63, %r8d
	leaq	(%rax,%rcx,8), %rdx
	movq	-144(%rbp), %rcx
	movq	%rdx, 24(%rcx)
	movl	%r8d, 32(%rcx)
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L585:
	leaq	.LC7(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L584:
	movq	-112(%rbp), %rbx
	movq	-104(%rbp), %r13
	movq	-160(%rbp), %r14
	subq	%rbx, %r13
	sarq	$3, %r13
.L549:
	movq	-136(%rbp), %rax
	movl	36(%r12), %esi
	movl	%r13d, %edx
	movq	8(%rax), %r9
	movq	16(%r15), %rax
	movq	8(%rax), %rdi
	movq	%r9, -136(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11ObjectStateEji@PLT
	leaq	64(%r15), %rsi
	movq	%rbx, %r8
	movl	%r13d, %ecx
	movq	-136(%rbp), %r9
	movq	%rax, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13NodeHashCache11ConstructorC1EPS2_PKNS1_8OperatorEiPPNS1_4NodeENS1_4TypeE
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13NodeHashCache11Constructor3GetEv
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L523
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rax
	jmp	.L523
.L547:
	movl	%edi, -120(%rbp)
	subq	%rdx, %rcx
	movq	-120(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	-144(%rbp), %rdi
	movq	%rsi, -128(%rbp)
	call	_ZNSt6vectorIbN2v88internal13ZoneAllocatorIbEEE14_M_fill_insertESt13_Bit_iteratormb
	movq	-144(%rbp), %rax
	movq	8(%rax), %rax
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L553:
	movq	-160(%rbp), %rdx
	movq	-176(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L552
.L562:
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	jmp	.L549
.L581:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11098:
	.size	_ZN2v88internal8compiler21EscapeAnalysisReducer16ReduceDeoptStateEPNS1_4NodeES4_PNS1_12DeduplicatorE, .-_ZN2v88internal8compiler21EscapeAnalysisReducer16ReduceDeoptStateEPNS1_4NodeES4_PNS1_12DeduplicatorE
	.section	.text._ZN2v88internal8compiler21EscapeAnalysisReducer22ReduceFrameStateInputsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21EscapeAnalysisReducer22ReduceFrameStateInputsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21EscapeAnalysisReducer22ReduceFrameStateInputsEPNS1_4NodeE, @function
_ZN2v88internal8compiler21EscapeAnalysisReducer22ReduceFrameStateInputsEPNS1_4NodeE:
.LFB11094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$88, %rsp
	movl	20(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-112(%rbp), %rax
	movq	%rax, -128(%rbp)
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L608:
	cmpl	%ebx, %eax
	jle	.L586
	leaq	32(%r12), %r9
	leaq	0(,%rbx,8), %r15
	leaq	(%r9,%r15), %rax
.L591:
	movq	(%rax), %rsi
	leaq	1(%rbx), %r14
	movq	(%rsi), %rax
	cmpw	$41, 16(%rax)
	je	.L607
.L592:
	movq	%r14, %rbx
.L598:
	movl	%edx, %eax
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L608
	movq	32(%r12), %rax
	leaq	32(%r12), %r9
	cmpl	%ebx, 8(%rax)
	jle	.L586
	leaq	0(,%rbx,8), %r15
	leaq	16(%rax,%r15), %rax
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L607:
	movq	-128(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%r9, -120(%rbp)
	movq	224(%r13), %rax
	movq	$0, -104(%rbp)
	movl	$0, -96(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -88(%rbp)
	movl	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN2v88internal8compiler21EscapeAnalysisReducer16ReduceDeoptStateEPNS1_4NodeES4_PNS1_12DeduplicatorE
	movl	20(%r12), %edx
	movq	-120(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L592
	movl	%edx, %eax
	xorl	$251658240, %eax
	testl	$251658240, %eax
	je	.L595
	movq	32(%r12,%rbx,8), %rdi
	leaq	(%r9,%rbx,8), %r15
	movq	%r12, %rax
	cmpq	%rdi, %r10
	je	.L592
.L596:
	leaq	1(%rbx), %r14
	leaq	0(,%r14,4), %rdx
	movq	%r14, %rcx
	subq	%rdx, %rcx
	leaq	(%rax,%rcx,8), %rbx
	testq	%rdi, %rdi
	je	.L597
	movq	%rbx, %rsi
	movq	%r10, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-120(%rbp), %r10
.L597:
	movq	%r10, (%r15)
	movq	%rbx, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	20(%r12), %edx
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L586:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L609
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L595:
	.cfi_restore_state
	movq	(%r9), %rax
	leaq	16(%rax,%r15), %r15
	movq	(%r15), %rdi
	cmpq	%rdi, %r10
	jne	.L596
	jmp	.L592
.L609:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11094:
	.size	_ZN2v88internal8compiler21EscapeAnalysisReducer22ReduceFrameStateInputsEPNS1_4NodeE, .-_ZN2v88internal8compiler21EscapeAnalysisReducer22ReduceFrameStateInputsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21EscapeAnalysisReducer6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21EscapeAnalysisReducer6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21EscapeAnalysisReducer6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler21EscapeAnalysisReducer6ReduceEPNS1_4NodeE:
.LFB11086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-32(%rbp), %r13
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%rsi, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	%r13, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal8compiler20EscapeAnalysisResult16GetReplacementOfEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L611
	movq	-40(%rbp), %rsi
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21EscapeAnalysisReducer11ReplaceNodeEPNS1_4NodeES4_
.L612:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L631
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L611:
	.cfi_restore_state
	movq	-40(%rbp), %rdi
	movq	(%rdi), %rdx
	movzwl	16(%rdx), %eax
	cmpw	$237, %ax
	je	.L613
	ja	.L614
	cmpw	$40, %ax
	jne	.L632
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, %rsi
	movq	(%rax), %rax
	cmpw	$39, 16(%rax)
	je	.L633
.L630:
	xorl	%eax, %eax
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L632:
	cmpw	$58, %ax
	jne	.L616
.L613:
	movq	24(%r12), %rax
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal8compiler20EscapeAnalysisResult16GetVirtualObjectEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L630
	cmpb	$0, 32(%rax)
	jne	.L630
.L629:
	movq	8(%r12), %rdi
	movq	-40(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	(%rdi), %rax
	movq	%rsi, %rdx
	call	*32(%rax)
	xorl	%eax, %eax
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L614:
	cmpw	$282, %ax
	jne	.L616
	leaq	-40(%rbp), %rsi
	leaq	168(%r12), %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	xorl	%eax, %eax
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L616:
	movl	24(%rdx), %eax
	testl	%eax, %eax
	jle	.L630
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21EscapeAnalysisReducer22ReduceFrameStateInputsEPNS1_4NodeE
	xorl	%eax, %eax
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L633:
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rsi, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L629
.L631:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11086:
	.size	_ZN2v88internal8compiler21EscapeAnalysisReducer6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler21EscapeAnalysisReducer6ReduceEPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler21EscapeAnalysisReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphENS1_20EscapeAnalysisResultEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler21EscapeAnalysisReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphENS1_20EscapeAnalysisResultEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler21EscapeAnalysisReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphENS1_20EscapeAnalysisResultEPNS0_4ZoneE:
.LFB13448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE13448:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler21EscapeAnalysisReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphENS1_20EscapeAnalysisResultEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler21EscapeAnalysisReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphENS1_20EscapeAnalysisResultEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler21EscapeAnalysisReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphENS1_20EscapeAnalysisResultEPNS0_4ZoneE
	.section	.rodata.C.51.361419,"a"
	.align 16
	.type	C.51.361419, @object
	.size	C.51.361419, 24
C.51.361419:
	.long	5
	.long	4
	.long	0
	.long	3
	.long	1
	.long	2
	.weak	_ZTVN2v88internal8compiler21EscapeAnalysisReducerE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler21EscapeAnalysisReducerE,"awG",@progbits,_ZTVN2v88internal8compiler21EscapeAnalysisReducerE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler21EscapeAnalysisReducerE, @object
	.size	_ZTVN2v88internal8compiler21EscapeAnalysisReducerE, 56
_ZTVN2v88internal8compiler21EscapeAnalysisReducerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler21EscapeAnalysisReducerD1Ev
	.quad	_ZN2v88internal8compiler21EscapeAnalysisReducerD0Ev
	.quad	_ZNK2v88internal8compiler21EscapeAnalysisReducer12reducer_nameEv
	.quad	_ZN2v88internal8compiler21EscapeAnalysisReducer6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler21EscapeAnalysisReducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
