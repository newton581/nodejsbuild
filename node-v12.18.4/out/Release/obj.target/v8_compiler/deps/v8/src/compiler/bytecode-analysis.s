	.file	"bytecode-analysis.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1554:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1554:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB22308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE22308:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB22478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22478:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB22309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22309:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB22479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE22479:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_117UpdateOutLivenessENS0_11interpreter8BytecodeEPNS1_21BytecodeLivenessStateES6_RKNS3_21BytecodeArrayAccessorENS0_6HandleINS0_13BytecodeArrayEEERKNS1_19BytecodeLivenessMapE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_117UpdateOutLivenessENS0_11interpreter8BytecodeEPNS1_21BytecodeLivenessStateES6_RKNS3_21BytecodeArrayAccessorENS0_6HandleINS0_13BytecodeArrayEEERKNS1_19BytecodeLivenessMapE, @function
_ZN2v88internal8compiler12_GLOBAL__N_117UpdateOutLivenessENS0_11interpreter8BytecodeEPNS1_21BytecodeLivenessStateES6_RKNS3_21BytecodeArrayAccessorENS0_6HandleINS0_13BytecodeArrayEEERKNS1_19BytecodeLivenessMapE:
.LFB17967:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%r8, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	80(%rdi), %eax
	cmpb	$1, %al
	jbe	.L76
	movl	8(%rcx), %eax
	movl	%edi, %ebx
	movq	%r9, %r13
	movl	%eax, -180(%rbp)
	leal	117(%rdi), %eax
	cmpb	$21, %al
	jbe	.L77
	cmpb	$-95, %dil
	je	.L58
	cmpb	$-81, %dil
	je	.L58
.L22:
	testq	%r12, %r12
	je	.L31
	leal	118(%rbx), %eax
	cmpb	$2, %al
	jbe	.L38
.L32:
	movl	12(%r15), %eax
	cmpl	$1, %eax
	je	.L78
	testl	%eax, %eax
	jle	.L31
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L37:
	movq	16(%r12), %rcx
	movq	16(%r15), %rdx
	movq	(%rcx,%rax,8), %rcx
	orq	%rcx, (%rdx,%rax,8)
	addq	$1, %rax
	cmpl	%eax, 12(%r15)
	jg	.L37
.L31:
	cmpb	$37, %bl
	je	.L11
	cmpb	$11, %bl
	jne	.L38
	.p2align 4,,10
	.p2align 3
.L11:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movq	%rcx, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi@PLT
	movq	(%rax), %rsi
	movl	12(%r15), %eax
	cmpl	$1, %eax
	je	.L80
	testl	%eax, %eax
	jle	.L19
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L21:
	movq	16(%rsi), %rcx
	movq	16(%r15), %rdx
	movq	(%rcx,%rax,8), %rcx
	orq	%rcx, (%rdx,%rax,8)
	addq	$1, %rax
	cmpl	%eax, 12(%r15)
	jg	.L21
.L19:
	testq	%r12, %r12
	je	.L54
	leal	118(%rbx), %eax
	cmpb	$2, %al
	ja	.L32
.L54:
	leal	-112(%rbx), %eax
	cmpb	$3, %al
	jbe	.L11
.L34:
	leal	118(%rbx), %eax
	cmpb	$22, %al
	ja	.L39
	addl	$107, %ebx
	cmpb	$3, %bl
	ja	.L11
.L40:
	movq	-176(%rbp), %rax
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal12HandlerTableC1ENS0_13BytecodeArrayE@PLT
	movl	-180(%rbp), %esi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	-128(%rbp), %rdx
	call	_ZN2v88internal12HandlerTable11LookupRangeEiPiPNS1_15CatchPredictionE@PLT
	movl	%eax, %esi
	cmpl	$-1, %eax
	je	.L11
	movl	8(%r15), %ecx
	cmpl	$1, 12(%r15)
	movq	16(%r15), %rbx
	leal	-1(%rcx), %edx
	je	.L44
	addl	$62, %ecx
	testl	%edx, %edx
	cmovns	%edx, %ecx
	sarl	$6, %ecx
	movslq	%ecx, %rcx
	movq	(%rbx,%rcx,8), %rbx
.L44:
	movl	%edx, %eax
	movq	%r13, %rdi
	sarl	$31, %eax
	shrl	$26, %eax
	leal	(%rdx,%rax), %ecx
	andl	$63, %ecx
	subl	%eax, %ecx
	shrq	%cl, %rbx
	call	_ZNK2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi@PLT
	andl	$1, %ebx
	movq	(%rax), %rsi
	movl	12(%r15), %eax
	cmpl	$1, %eax
	je	.L81
	testl	%eax, %eax
	jle	.L47
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L48:
	movq	16(%r15), %rdx
	movq	16(%rsi), %rcx
	movq	(%rcx,%rax,8), %rcx
	orq	%rcx, (%rdx,%rax,8)
	addq	$1, %rax
	movl	12(%r15), %edx
	cmpl	%eax, %edx
	jg	.L48
	movl	-128(%rbp), %eax
	cmpl	$1, %edx
	je	.L82
.L49:
	testl	%eax, %eax
	leal	63(%rax), %edx
	movl	%eax, %esi
	movq	16(%r15), %rdi
	cmovns	%eax, %edx
	sarl	$31, %esi
	shrl	$26, %esi
	leal	(%rax,%rsi), %ecx
	sarl	$6, %edx
	andl	$63, %ecx
	movslq	%edx, %rdx
	subl	%esi, %ecx
	movl	$1, %esi
	movq	%rsi, %rax
	salq	%cl, %rax
	orq	%rax, (%rdi,%rdx,8)
	testb	%bl, %bl
	jne	.L11
	movl	8(%r15), %eax
	cmpl	$1, 12(%r15)
	leal	-1(%rax), %ecx
	je	.L53
	addl	$62, %eax
	movl	%ecx, %edx
	testl	%ecx, %ecx
	movq	16(%r15), %rdi
	cmovns	%ecx, %eax
	sarl	$31, %edx
	shrl	$26, %edx
	addl	%edx, %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%edx, %ecx
	salq	%cl, %rsi
	notq	%rsi
	andq	%rsi, (%rdi,%rax,8)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L38:
	cmpb	$39, %bl
	ja	.L54
	movabsq	$824898482176, %rax
	btq	%rbx, %rax
	jc	.L11
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L76:
	movl	12(%rsi), %edx
	cmpl	$1, %edx
	je	.L83
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L11
	.p2align 4,,10
	.p2align 3
.L15:
	movq	16(%r12), %rcx
	movq	16(%r15), %rdx
	movq	(%rcx,%rax,8), %rcx
	orq	%rcx, (%rdx,%rax,8)
	addq	$1, %rax
	cmpl	%eax, 12(%r15)
	jg	.L15
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L83:
	movq	16(%r12), %rax
	orq	%rax, 16(%rsi)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L78:
	movq	16(%r12), %rax
	orq	%rax, 16(%r15)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L80:
	movq	16(%rsi), %rax
	orq	%rax, 16(%r15)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L58:
	leaq	-160(%rbp), %r8
	movq	%rcx, %rsi
	leaq	-128(%rbp), %r14
	movq	%r8, %rdi
	movq	%r8, -168(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor25GetJumpTableTargetOffsetsEv@PLT
	movq	-168(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, %rsi
	movq	%r8, -192(%rbp)
	call	_ZNK2v88internal11interpreter22JumpTableTargetOffsets5beginEv@PLT
	movq	-192(%rbp), %r8
	leaq	-96(%rbp), %rdi
	movq	%rdi, -168(%rbp)
	movq	%r8, %rsi
	call	_ZNK2v88internal11interpreter22JumpTableTargetOffsets3endEv@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	movq	-168(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorneERKS3_@PLT
	testb	%al, %al
	je	.L22
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratordeEv@PLT
	movq	%r13, %rdi
	sarq	$32, %rax
	movq	%rax, %rsi
	call	_ZNK2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi@PLT
	movl	12(%r15), %edx
	movq	(%rax), %rcx
	cmpl	$1, %edx
	je	.L84
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L26
	.p2align 4,,10
	.p2align 3
.L27:
	movq	16(%rcx), %rsi
	movq	16(%r15), %rdx
	movq	(%rsi,%rax,8), %rsi
	orq	%rsi, (%rdx,%rax,8)
	addq	$1, %rax
	cmpl	%eax, 12(%r15)
	jg	.L27
.L26:
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorppEv@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L84:
	movq	16(%rcx), %rax
	orq	%rax, 16(%r15)
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorppEv@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L81:
	movl	-128(%rbp), %eax
	movq	16(%r15), %rdx
	orq	16(%rsi), %rdx
.L46:
	btsq	%rax, %rdx
	movq	%rdx, 16(%r15)
	testb	%bl, %bl
	jne	.L11
	movl	8(%r15), %eax
	leal	-1(%rax), %ecx
.L53:
	movq	$-2, %rax
	rolq	%cl, %rax
	andq	%rax, 16(%r15)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L39:
	cmpb	$-95, %bl
	je	.L11
	cmpb	$-81, %bl
	je	.L11
	jmp	.L40
.L47:
	movl	-128(%rbp), %eax
	jmp	.L49
.L79:
	call	__stack_chk_fail@PLT
.L82:
	movq	16(%r15), %rdx
	jmp	.L46
	.cfi_endproc
.LFE17967:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_117UpdateOutLivenessENS0_11interpreter8BytecodeEPNS1_21BytecodeLivenessStateES6_RKNS3_21BytecodeArrayAccessorENS0_6HandleINS0_13BytecodeArrayEEERKNS1_19BytecodeLivenessMapE, .-_ZN2v88internal8compiler12_GLOBAL__N_117UpdateOutLivenessENS0_11interpreter8BytecodeEPNS1_21BytecodeLivenessStateES6_RKNS3_21BytecodeArrayAccessorENS0_6HandleINS0_13BytecodeArrayEEERKNS1_19BytecodeLivenessMapE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_116UpdateInLivenessENS0_11interpreter8BytecodeEPNS1_21BytecodeLivenessStateERKNS3_21BytecodeArrayAccessorE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_116UpdateInLivenessENS0_11interpreter8BytecodeEPNS1_21BytecodeLivenessStateERKNS3_21BytecodeArrayAccessorE, @function
_ZN2v88internal8compiler12_GLOBAL__N_116UpdateInLivenessENS0_11interpreter8BytecodeEPNS1_21BytecodeLivenessStateERKNS3_21BytecodeArrayAccessorE:
.LFB17966:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %eax
	movzbl	%dil, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%rdi, -64(%rbp)
	cmpb	$-80, %al
	je	.L149
	cmpb	$-79, %al
	je	.L150
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rax
	movl	(%rax,%rdi,4), %eax
	movl	%eax, -52(%rbp)
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandTypesE(%rip), %rax
	movq	(%rax,%rdi,8), %r12
	leaq	_ZN2v88internal11interpreter9Bytecodes15kAccumulatorUseE(%rip), %rax
	testb	$2, (%rax,%rdi)
	jne	.L151
.L94:
	movl	-52(%rbp), %edx
	testl	%edx, %edx
	jle	.L96
	xorl	%r9d, %r9d
	movl	$1, %r15d
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L154:
	cmpb	$13, %al
	jne	.L148
	movl	%r9d, %esi
	movq	%rbx, %rdi
	movl	%r9d, -68(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movl	%eax, -56(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movl	-68(%rbp), %r9d
	movl	-56(%rbp), %edx
	addl	$2, %r9d
	testl	%edx, %edx
	jns	.L152
	.p2align 4,,10
	.p2align 3
.L101:
	cmpl	%r9d, -52(%rbp)
	jle	.L153
.L116:
	movslq	%r9d, %rax
	leal	1(%r9), %r14d
	movzbl	(%r12,%rax), %eax
	cmpb	$14, %al
	je	.L97
	ja	.L98
	cmpb	$12, %al
	jne	.L154
	movl	%r9d, %esi
	movq	%rbx, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%r14d, %r9d
	movl	%eax, %ecx
	testl	%eax, %eax
	js	.L101
	cmpl	$1, 12(%r13)
	movq	16(%r13), %rdx
	je	.L155
	movq	%r15, %rdi
	sarl	$6, %eax
	salq	%cl, %rdi
	cltq
	movq	%rdi, %rcx
	notq	%rcx
	andq	%rcx, (%rdx,%rax,8)
	cmpl	%r9d, -52(%rbp)
	jg	.L116
	.p2align 4,,10
	.p2align 3
.L153:
	movq	-64(%rbp), %rdi
	leaq	_ZN2v88internal11interpreter9Bytecodes15kAccumulatorUseE(%rip), %rax
	testb	$1, (%rax,%rdi)
	jne	.L132
.L119:
	xorl	%r14d, %r14d
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L157:
	cmpb	$9, %al
	je	.L156
.L122:
	cmpl	%r15d, -52(%rbp)
	jle	.L85
.L159:
	movl	%r15d, %r14d
.L131:
	movslq	%r14d, %rax
	leal	1(%r14), %r15d
	movzbl	(%r12,%rax), %eax
	cmpb	$10, %al
	je	.L120
	cmpb	$11, %al
	jne	.L157
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, %ecx
	testl	%eax, %eax
	js	.L122
	cmpl	$1, 12(%r13)
	leal	1(%rax), %esi
	movq	16(%r13), %rax
	je	.L158
	movl	%ecx, %edx
	movl	$1, %edi
	sarl	$6, %edx
	salq	%cl, %rdi
	movslq	%edx, %rdx
	orq	%rdi, (%rax,%rdx,8)
	cmpl	$1, 12(%r13)
	je	.L125
	movl	%esi, %eax
	movq	16(%r13), %rdx
	movl	$1, %edi
	movl	%esi, %ecx
	sarl	$6, %eax
	salq	%cl, %rdi
	cltq
	orq	%rdi, (%rdx,%rax,8)
	cmpl	%r15d, -52(%rbp)
	jg	.L159
.L85:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	cmpb	$15, %al
	jne	.L148
	movl	%r9d, %esi
	movq	%rbx, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%r14d, %r9d
	movl	%eax, %ecx
	testl	%eax, %eax
	js	.L101
	cmpl	$1, 12(%r13)
	leal	1(%rax), %r11d
	leal	2(%rcx), %esi
	movq	16(%r13), %rax
	je	.L160
	movl	%ecx, %edx
	movq	%r15, %rdi
	sarl	$6, %edx
	salq	%cl, %rdi
	movslq	%edx, %rdx
	movq	%rdi, %rcx
	notq	%rcx
	andq	%rcx, (%rax,%rdx,8)
	cmpl	$1, 12(%r13)
	je	.L111
	movl	%r11d, %ecx
	movl	%r11d, %eax
	movq	%r15, %rdi
	movq	16(%r13), %rdx
	sarl	$6, %eax
	salq	%cl, %rdi
	cltq
	movq	%rdi, %rcx
	notq	%rcx
	andq	%rcx, (%rdx,%rax,8)
	cmpl	$1, 12(%r13)
	je	.L113
	movl	%esi, %eax
	movq	16(%r13), %r10
	movq	%r15, %rdx
	movl	%esi, %ecx
	sarl	$6, %eax
	salq	%cl, %rdx
	cltq
	notq	%rdx
	andq	%rdx, (%r10,%rax,8)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%r15, %rdx
	salq	%cl, %rdx
	notq	%rdx
	andq	%rdx, %rax
	movq	%rax, 16(%r13)
.L111:
	movq	%r15, %rax
	movl	%r11d, %ecx
	salq	%cl, %rax
	notq	%rax
	andq	%rax, 16(%r13)
.L113:
	movq	%r15, %rax
	movl	%esi, %ecx
	salq	%cl, %rax
	notq	%rax
	andq	%rax, 16(%r13)
	.p2align 4,,10
	.p2align 3
.L148:
	movl	%r14d, %r9d
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L97:
	movl	%r9d, %esi
	movq	%rbx, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%r14d, %r9d
	movl	%eax, %ecx
	testl	%eax, %eax
	js	.L101
	cmpl	$1, 12(%r13)
	movq	16(%r13), %rsi
	leal	1(%rax), %edx
	je	.L161
	movq	%r15, %rdi
	sarl	$6, %eax
	salq	%cl, %rdi
	cltq
	movq	%rdi, %rcx
	notq	%rcx
	andq	%rcx, (%rsi,%rax,8)
	cmpl	$1, 12(%r13)
	je	.L108
	movl	%edx, %eax
	movl	%edx, %ecx
	movq	%r15, %rdi
	movq	16(%r13), %rsi
	sarl	$6, %eax
	salq	%cl, %rdi
	cltq
	movq	%rdi, %rdx
	notq	%rdx
	andq	%rdx, (%rsi,%rax,8)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L120:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%r15d, %esi
	movq	%rbx, %rdi
	leal	2(%r14), %r15d
	movl	%eax, -64(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movl	-64(%rbp), %edx
	testl	%edx, %edx
	js	.L122
	testl	%eax, %eax
	je	.L122
	movl	%edx, %esi
	leal	(%rax,%rdx), %r9d
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L127:
	testl	%esi, %esi
	leal	63(%rsi), %eax
	movl	%esi, %r10d
	movl	$1, %edi
	cmovns	%esi, %eax
	sarl	$31, %r10d
	shrl	$26, %r10d
	leal	(%rsi,%r10), %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%r10d, %ecx
	salq	%cl, %rdi
	orq	%rdi, (%rdx,%rax,8)
.L128:
	addl	$1, %esi
	cmpl	%r9d, %esi
	je	.L122
.L129:
	cmpl	$1, 12(%r13)
	movq	16(%r13), %rdx
	jne	.L127
	btsq	%rsi, %rdx
	movq	%rdx, 16(%r13)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L156:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, %ecx
	testl	%eax, %eax
	js	.L122
	cmpl	$1, 12(%r13)
	movq	16(%r13), %rax
	je	.L162
	movl	%ecx, %edx
	movl	$1, %edi
	sarl	$6, %edx
	salq	%cl, %rdi
	movslq	%edx, %rdx
	orq	%rdi, (%rax,%rdx,8)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%r15, %rax
	salq	%cl, %rax
	notq	%rax
	andq	%rax, %rsi
	movq	%rsi, 16(%r13)
.L108:
	movq	%r15, %rax
	movl	%edx, %ecx
	movl	%r14d, %r9d
	salq	%cl, %rax
	notq	%rax
	andq	%rax, 16(%r13)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%r15, %rax
	salq	%cl, %rax
	notq	%rax
	andq	%rax, %rdx
	movq	%rdx, 16(%r13)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L96:
	movq	-64(%rbp), %rdi
	leaq	_ZN2v88internal11interpreter9Bytecodes15kAccumulatorUseE(%rip), %rax
	testb	$1, (%rax,%rdi)
	je	.L85
	.p2align 4,,10
	.p2align 3
.L132:
	movl	8(%r13), %edx
	cmpl	$1, 12(%r13)
	movq	16(%r13), %rsi
	leal	-1(%rdx), %eax
	je	.L163
	addl	$62, %edx
	movl	%eax, %r9d
	testl	%eax, %eax
	cmovns	%eax, %edx
	sarl	$31, %r9d
	shrl	$26, %r9d
	leal	(%rax,%r9), %ecx
	sarl	$6, %edx
	movl	$1, %eax
	andl	$63, %ecx
	movslq	%edx, %rdx
	subl	%r9d, %ecx
	salq	%cl, %rax
	orq	%rax, (%rsi,%rdx,8)
.L118:
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	jg	.L119
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L152:
	testl	%eax, %eax
	je	.L101
	movl	%edx, %esi
	leal	(%rax,%rdx), %r10d
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L104:
	testl	%esi, %esi
	leal	63(%rsi), %eax
	movl	%esi, %r11d
	movq	%r15, %rdi
	cmovns	%esi, %eax
	sarl	$31, %r11d
	shrl	$26, %r11d
	leal	(%rsi,%r11), %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%r11d, %ecx
	salq	%cl, %rdi
	movq	%rdi, %rcx
	notq	%rcx
	andq	%rcx, (%rdx,%rax,8)
.L105:
	addl	$1, %esi
	cmpl	%esi, %r10d
	je	.L101
.L106:
	cmpl	$1, 12(%r13)
	movq	16(%r13), %rdx
	jne	.L104
	movq	%r15, %rax
	movl	%esi, %ecx
	salq	%cl, %rax
	notq	%rax
	andq	%rax, %rdx
	movq	%rdx, 16(%r13)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L158:
	btsq	%rcx, %rax
	movq	%rax, 16(%r13)
.L125:
	movl	$1, %eax
	movl	%esi, %ecx
	salq	%cl, %rax
	orq	%rax, 16(%r13)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L162:
	btsq	%rcx, %rax
	movq	%rax, 16(%r13)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L151:
	movl	8(%rsi), %eax
	cmpl	$1, 12(%rsi)
	movq	16(%rsi), %rdx
	leal	-1(%rax), %ecx
	je	.L164
	addl	$62, %eax
	movl	%ecx, %esi
	testl	%ecx, %ecx
	cmovns	%ecx, %eax
	sarl	$31, %esi
	shrl	$26, %esi
	addl	%esi, %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%esi, %ecx
	movq	$-2, %rsi
	rolq	%cl, %rsi
	andq	%rsi, (%rdx,%rax,8)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L164:
	btrq	%rcx, %rdx
	movq	%rdx, 16(%rsi)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L163:
	btsq	%rax, %rsi
	movq	%rsi, 16(%r13)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L149:
	xorl	%esi, %esi
	movq	%rdx, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	cmpl	$1, 12(%r13)
	movl	%eax, %ecx
	je	.L165
	testl	%ecx, %ecx
	movl	%ecx, %edx
	leal	63(%rax), %eax
	movq	16(%r13), %rdi
	cmovns	%ecx, %eax
	sarl	$31, %edx
	movl	$1, %esi
	shrl	$26, %edx
	addl	%edx, %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%edx, %ecx
	movq	%rsi, %rdx
	salq	%cl, %rdx
	orq	%rdx, (%rdi,%rax,8)
	movl	8(%r13), %edx
	cmpl	$1, 12(%r13)
	movq	16(%r13), %rdi
	leal	-1(%rdx), %eax
	je	.L88
	addl	$62, %edx
	movl	%eax, %r8d
	testl	%eax, %eax
	cmovns	%eax, %edx
	sarl	$31, %r8d
	shrl	$26, %r8d
	leal	(%rax,%r8), %ecx
	sarl	$6, %edx
	andl	$63, %ecx
	movslq	%edx, %rdx
	subl	%r8d, %ecx
	salq	%cl, %rsi
	orq	%rsi, (%rdi,%rdx,8)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L150:
	xorl	%esi, %esi
	movq	%rdx, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	cmpl	$1, 12(%r13)
	movl	%eax, %ecx
	je	.L166
	testl	%ecx, %ecx
	movl	%ecx, %edx
	leal	63(%rax), %eax
	movq	16(%r13), %rsi
	cmovns	%ecx, %eax
	sarl	$31, %edx
	shrl	$26, %edx
	addl	%edx, %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%edx, %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	orq	%rdx, (%rsi,%rax,8)
	jmp	.L85
.L165:
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, 16(%r13)
	movl	8(%r13), %eax
	movq	16(%r13), %rdi
	subl	$1, %eax
.L88:
	btsq	%rax, %rdi
	movq	%rdi, 16(%r13)
	jmp	.L85
.L166:
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, 16(%r13)
	jmp	.L85
	.cfi_endproc
.LFE17966:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_116UpdateInLivenessENS0_11interpreter8BytecodeEPNS1_21BytecodeLivenessStateERKNS3_21BytecodeArrayAccessorE, .-_ZN2v88internal8compiler12_GLOBAL__N_116UpdateInLivenessENS0_11interpreter8BytecodeEPNS1_21BytecodeLivenessStateERKNS3_21BytecodeArrayAccessorE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_114UpdateLivenessENS0_11interpreter8BytecodeERKNS1_16BytecodeLivenessEPPNS1_21BytecodeLivenessStateERKNS3_21BytecodeArrayAccessorENS0_6HandleINS0_13BytecodeArrayEEERKNS1_19BytecodeLivenessMapE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_114UpdateLivenessENS0_11interpreter8BytecodeERKNS1_16BytecodeLivenessEPPNS1_21BytecodeLivenessStateERKNS3_21BytecodeArrayAccessorENS0_6HandleINS0_13BytecodeArrayEEERKNS1_19BytecodeLivenessMapE.isra.0, @function
_ZN2v88internal8compiler12_GLOBAL__N_114UpdateLivenessENS0_11interpreter8BytecodeERKNS1_16BytecodeLivenessEPPNS1_21BytecodeLivenessStateERKNS3_21BytecodeArrayAccessorENS0_6HandleINS0_13BytecodeArrayEEERKNS1_19BytecodeLivenessMapE.isra.0:
.LFB22363:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	movq	%r9, %r8
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r14, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%r12), %rdx
	movq	(%r15), %rsi
	movq	16(%rbp), %r9
	call	_ZN2v88internal8compiler12_GLOBAL__N_117UpdateOutLivenessENS0_11interpreter8BytecodeEPNS1_21BytecodeLivenessStateES6_RKNS3_21BytecodeArrayAccessorENS0_6HandleINS0_13BytecodeArrayEEERKNS1_19BytecodeLivenessMapE
	movq	(%r15), %rcx
	movq	(%rbx), %rdx
	movl	12(%rcx), %eax
	movq	16(%rcx), %rsi
	movl	12(%rdx), %ecx
	cmpl	$1, %ecx
	je	.L183
	cmpl	$1, %eax
	je	.L184
	testl	%eax, %eax
	jle	.L176
	leal	-1(%rax), %ecx
	leaq	8(,%rcx,8), %r9
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L175:
	movq	(%rsi,%rcx), %r8
	movq	16(%rdx), %rdi
	movq	%r8, (%rdi,%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %r9
	jne	.L175
	movl	12(%rdx), %ecx
.L176:
	cmpl	%ecx, %eax
	jge	.L169
	movslq	%eax, %rcx
	salq	$3, %rcx
	.p2align 4,,10
	.p2align 3
.L177:
	movq	16(%rdx), %rsi
	addl	$1, %eax
	movq	$0, (%rsi,%rcx)
	addq	$8, %rcx
	cmpl	%eax, 12(%rdx)
	jg	.L177
.L169:
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movl	%r13d, %edi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116UpdateInLivenessENS0_11interpreter8BytecodeEPNS1_21BytecodeLivenessStateERKNS3_21BytecodeArrayAccessorE
	movq	(%rbx), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movq	16(%rdx), %rcx
	movq	%rsi, (%rcx)
	cmpl	$1, 12(%rdx)
	movl	$8, %ecx
	jle	.L169
	.p2align 4,,10
	.p2align 3
.L171:
	movq	16(%rdx), %rsi
	addl	$1, %eax
	movq	$0, (%rsi,%rcx)
	addq	$8, %rcx
	cmpl	%eax, 12(%rdx)
	jg	.L171
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L183:
	movq	%rsi, 16(%rdx)
	jmp	.L169
	.cfi_endproc
.LFE22363:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_114UpdateLivenessENS0_11interpreter8BytecodeERKNS1_16BytecodeLivenessEPPNS1_21BytecodeLivenessStateERKNS3_21BytecodeArrayAccessorENS0_6HandleINS0_13BytecodeArrayEEERKNS1_19BytecodeLivenessMapE.isra.0, .-_ZN2v88internal8compiler12_GLOBAL__N_114UpdateLivenessENS0_11interpreter8BytecodeERKNS1_16BytecodeLivenessEPPNS1_21BytecodeLivenessStateERKNS3_21BytecodeArrayAccessorENS0_6HandleINS0_13BytecodeArrayEEERKNS1_19BytecodeLivenessMapE.isra.0
	.section	.text._ZN2v88internal8compiler23BytecodeLoopAssignmentsC2EiiPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23BytecodeLoopAssignmentsC2EiiPNS0_4ZoneE
	.type	_ZN2v88internal8compiler23BytecodeLoopAssignmentsC2EiiPNS0_4ZoneE, @function
_ZN2v88internal8compiler23BytecodeLoopAssignmentsC2EiiPNS0_4ZoneE:
.LFB17924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	16(%rcx), %r13
	movq	24(%rcx), %rax
	movl	%esi, (%rdi)
	subq	%r13, %rax
	cmpq	$15, %rax
	jbe	.L199
	leaq	16(%r13), %rax
	movq	%rax, 16(%rcx)
.L187:
	addl	%ebx, %edx
	movl	%edx, 0(%r13)
	cmpl	$64, %edx
	jle	.L200
	subl	$1, %edx
	movq	$0, 8(%r13)
	sarl	$6, %edx
	addl	$1, %edx
	movl	%edx, 4(%r13)
	movslq	%edx, %rdx
	movq	16(%rcx), %rax
	leaq	0(,%rdx,8), %rsi
	movq	24(%rcx), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L201
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx)
.L191:
	movl	4(%r13), %edx
	movq	%rax, 8(%r13)
	cmpl	$1, %edx
	je	.L202
	testl	%edx, %edx
	jle	.L189
	movq	$0, (%rax)
	cmpl	$1, 4(%r13)
	movl	$8, %edx
	movl	$1, %eax
	jle	.L189
	.p2align 4,,10
	.p2align 3
.L194:
	movq	8(%r13), %rcx
	addl	$1, %eax
	movq	$0, (%rcx,%rdx)
	addq	$8, %rdx
	cmpl	%eax, 4(%r13)
	jg	.L194
.L189:
	movq	%r13, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movl	$1, 4(%r13)
	movq	$0, 8(%r13)
	movq	%r13, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movq	$0, 8(%r13)
	movq	%r13, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movq	%rcx, %rdi
	movl	$16, %esi
	movl	%edx, -44(%rbp)
	movq	%rcx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rcx
	movl	-44(%rbp), %edx
	movq	%rax, %r13
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L201:
	movq	%rcx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L191
	.cfi_endproc
.LFE17924:
	.size	_ZN2v88internal8compiler23BytecodeLoopAssignmentsC2EiiPNS0_4ZoneE, .-_ZN2v88internal8compiler23BytecodeLoopAssignmentsC2EiiPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler23BytecodeLoopAssignmentsC1EiiPNS0_4ZoneE
	.set	_ZN2v88internal8compiler23BytecodeLoopAssignmentsC1EiiPNS0_4ZoneE,_ZN2v88internal8compiler23BytecodeLoopAssignmentsC2EiiPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler23BytecodeLoopAssignments3AddENS0_11interpreter8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23BytecodeLoopAssignments3AddENS0_11interpreter8RegisterE
	.type	_ZN2v88internal8compiler23BytecodeLoopAssignments3AddENS0_11interpreter8RegisterE, @function
_ZN2v88internal8compiler23BytecodeLoopAssignments3AddENS0_11interpreter8RegisterE:
.LFB17926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rbx
	movl	(%rdi), %r8d
	movl	%esi, -20(%rbp)
	testl	%esi, %esi
	js	.L209
	addl	%r8d, %esi
	cmpl	$1, 4(%rbx)
	movq	8(%rbx), %rdx
	je	.L210
	testl	%esi, %esi
	leal	63(%rsi), %eax
	movl	%esi, %edi
	cmovns	%esi, %eax
	sarl	$31, %edi
	shrl	$26, %edi
	leal	(%rsi,%rdi), %ecx
	sarl	$6, %eax
	movl	$1, %esi
	andl	$63, %ecx
	cltq
	subl	%edi, %ecx
	salq	%cl, %rsi
	orq	%rsi, (%rdx,%rax,8)
.L203:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	btsq	%rsi, %rdx
	movq	%rdx, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	leaq	-20(%rbp), %rdi
	movl	%r8d, %esi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	cmpl	$1, 4(%rbx)
	movl	%eax, %ecx
	je	.L211
	testl	%ecx, %ecx
	movl	%ecx, %edx
	leal	63(%rax), %eax
	movq	8(%rbx), %rsi
	cmovns	%ecx, %eax
	sarl	$31, %edx
	shrl	$26, %edx
	addl	%edx, %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%edx, %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	orq	%rdx, (%rsi,%rax,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, 8(%rbx)
	jmp	.L203
	.cfi_endproc
.LFE17926:
	.size	_ZN2v88internal8compiler23BytecodeLoopAssignments3AddENS0_11interpreter8RegisterE, .-_ZN2v88internal8compiler23BytecodeLoopAssignments3AddENS0_11interpreter8RegisterE
	.section	.text._ZN2v88internal8compiler23BytecodeLoopAssignments7AddListENS0_11interpreter8RegisterEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23BytecodeLoopAssignments7AddListENS0_11interpreter8RegisterEj
	.type	_ZN2v88internal8compiler23BytecodeLoopAssignments7AddListENS0_11interpreter8RegisterEj, @function
_ZN2v88internal8compiler23BytecodeLoopAssignments7AddListENS0_11interpreter8RegisterEj:
.LFB17927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	%esi, -52(%rbp)
	testl	%esi, %esi
	js	.L228
	testl	%edx, %edx
	je	.L212
	movl	%esi, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L218:
	testl	%ecx, %ecx
	leal	63(%rcx), %eax
	movl	%ecx, %r8d
	movq	%rsi, %r9
	cmovns	%ecx, %eax
	sarl	$31, %r8d
	addl	$1, %edx
	shrl	$26, %r8d
	addl	%r8d, %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%r8d, %ecx
	salq	%cl, %r9
	orq	%r9, (%rdi,%rax,8)
	cmpl	%edx, %r12d
	je	.L212
.L229:
	movl	-52(%rbp), %ecx
.L220:
	movq	8(%rbx), %rax
	addl	(%rbx), %ecx
	addl	%edx, %ecx
	cmpl	$1, 4(%rax)
	movq	8(%rax), %rdi
	jne	.L218
	btsq	%rcx, %rdi
	addl	$1, %edx
	movq	%rdi, 8(%rax)
	cmpl	%edx, %r12d
	jne	.L229
.L212:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L212
	xorl	%r15d, %r15d
	leaq	-52(%rbp), %r14
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L215:
	testl	%ecx, %ecx
	leal	63(%rcx), %eax
	movl	%ecx, %edx
	movq	8(%r13), %rsi
	cmovns	%ecx, %eax
	sarl	$31, %edx
	shrl	$26, %edx
	addl	%edx, %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%edx, %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	orq	%rdx, (%rsi,%rax,8)
.L216:
	addl	$1, %r15d
	cmpl	%r15d, %r12d
	je	.L212
.L217:
	movq	8(%rbx), %r13
	movl	(%rbx), %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	cmpl	$1, 4(%r13)
	leal	(%rax,%r15), %ecx
	jne	.L215
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, 8(%r13)
	jmp	.L216
	.cfi_endproc
.LFE17927:
	.size	_ZN2v88internal8compiler23BytecodeLoopAssignments7AddListENS0_11interpreter8RegisterEj, .-_ZN2v88internal8compiler23BytecodeLoopAssignments7AddListENS0_11interpreter8RegisterEj
	.section	.text._ZN2v88internal8compiler23BytecodeLoopAssignments5UnionERKS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler23BytecodeLoopAssignments5UnionERKS2_
	.type	_ZN2v88internal8compiler23BytecodeLoopAssignments5UnionERKS2_, @function
_ZN2v88internal8compiler23BytecodeLoopAssignments5UnionERKS2_:
.LFB17928:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	movq	8(%rsi), %rsi
	movl	4(%rdx), %ecx
	cmpl	$1, %ecx
	je	.L237
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L230
	.p2align 4,,10
	.p2align 3
.L233:
	movq	8(%rsi), %rdi
	movq	8(%rdx), %rcx
	movq	(%rdi,%rax,8), %rdi
	orq	%rdi, (%rcx,%rax,8)
	addq	$1, %rax
	cmpl	%eax, 4(%rdx)
	jg	.L233
.L230:
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	movq	8(%rsi), %rax
	orq	%rax, 8(%rdx)
	ret
	.cfi_endproc
.LFE17928:
	.size	_ZN2v88internal8compiler23BytecodeLoopAssignments5UnionERKS2_, .-_ZN2v88internal8compiler23BytecodeLoopAssignments5UnionERKS2_
	.section	.text._ZNK2v88internal8compiler23BytecodeLoopAssignments17ContainsParameterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler23BytecodeLoopAssignments17ContainsParameterEi
	.type	_ZNK2v88internal8compiler23BytecodeLoopAssignments17ContainsParameterEi, @function
_ZNK2v88internal8compiler23BytecodeLoopAssignments17ContainsParameterEi:
.LFB17929:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	cmpl	$1, 4(%rdx)
	movq	8(%rdx), %rax
	je	.L240
	testl	%esi, %esi
	leal	63(%rsi), %edx
	cmovns	%esi, %edx
	sarl	$6, %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
.L240:
	movl	%esi, %edx
	sarl	$31, %edx
	shrl	$26, %edx
	leal	(%rsi,%rdx), %ecx
	andl	$63, %ecx
	subl	%edx, %ecx
	shrq	%cl, %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE17929:
	.size	_ZNK2v88internal8compiler23BytecodeLoopAssignments17ContainsParameterEi, .-_ZNK2v88internal8compiler23BytecodeLoopAssignments17ContainsParameterEi
	.section	.text._ZNK2v88internal8compiler23BytecodeLoopAssignments13ContainsLocalEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler23BytecodeLoopAssignments13ContainsLocalEi
	.type	_ZNK2v88internal8compiler23BytecodeLoopAssignments13ContainsLocalEi, @function
_ZNK2v88internal8compiler23BytecodeLoopAssignments13ContainsLocalEi:
.LFB17930:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	addl	(%rdi), %esi
	cmpl	$1, 4(%rdx)
	movq	8(%rdx), %rax
	je	.L243
	testl	%esi, %esi
	leal	63(%rsi), %edx
	cmovns	%esi, %edx
	sarl	$6, %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
.L243:
	movl	%esi, %edx
	sarl	$31, %edx
	shrl	$26, %edx
	leal	(%rsi,%rdx), %ecx
	andl	$63, %ecx
	subl	%edx, %ecx
	shrq	%cl, %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE17930:
	.size	_ZNK2v88internal8compiler23BytecodeLoopAssignments13ContainsLocalEi, .-_ZNK2v88internal8compiler23BytecodeLoopAssignments13ContainsLocalEi
	.section	.text._ZN2v88internal8compiler16ResumeJumpTargetC2Eiii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16ResumeJumpTargetC2Eiii
	.type	_ZN2v88internal8compiler16ResumeJumpTargetC2Eiii, @function
_ZN2v88internal8compiler16ResumeJumpTargetC2Eiii:
.LFB17932:
	.cfi_startproc
	endbr64
	movl	%esi, (%rdi)
	movl	%edx, 4(%rdi)
	movl	%ecx, 8(%rdi)
	ret
	.cfi_endproc
.LFE17932:
	.size	_ZN2v88internal8compiler16ResumeJumpTargetC2Eiii, .-_ZN2v88internal8compiler16ResumeJumpTargetC2Eiii
	.globl	_ZN2v88internal8compiler16ResumeJumpTargetC1Eiii
	.set	_ZN2v88internal8compiler16ResumeJumpTargetC1Eiii,_ZN2v88internal8compiler16ResumeJumpTargetC2Eiii
	.section	.text._ZN2v88internal8compiler16ResumeJumpTarget4LeafEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16ResumeJumpTarget4LeafEii
	.type	_ZN2v88internal8compiler16ResumeJumpTarget4LeafEii, @function
_ZN2v88internal8compiler16ResumeJumpTarget4LeafEii:
.LFB17934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -12(%rbp)
	movl	%esi, -8(%rbp)
	movq	-12(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17934:
	.size	_ZN2v88internal8compiler16ResumeJumpTarget4LeafEii, .-_ZN2v88internal8compiler16ResumeJumpTarget4LeafEii
	.section	.text._ZN2v88internal8compiler16ResumeJumpTarget12AtLoopHeaderEiRKS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16ResumeJumpTarget12AtLoopHeaderEiRKS2_
	.type	_ZN2v88internal8compiler16ResumeJumpTarget12AtLoopHeaderEiRKS2_, @function
_ZN2v88internal8compiler16ResumeJumpTarget12AtLoopHeaderEiRKS2_:
.LFB17935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -8(%rbp)
	movl	(%rsi), %eax
	movl	4(%rsi), %edx
	movl	%eax, -12(%rbp)
	movq	-12(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17935:
	.size	_ZN2v88internal8compiler16ResumeJumpTarget12AtLoopHeaderEiRKS2_, .-_ZN2v88internal8compiler16ResumeJumpTarget12AtLoopHeaderEiRKS2_
	.section	.text._ZNK2v88internal8compiler16BytecodeAnalysis12IsLoopHeaderEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16BytecodeAnalysis12IsLoopHeaderEi
	.type	_ZNK2v88internal8compiler16BytecodeAnalysis12IsLoopHeaderEi, @function
_ZNK2v88internal8compiler16BytecodeAnalysis12IsLoopHeaderEi:
.LFB18004:
	.cfi_startproc
	endbr64
	movq	264(%rdi), %rax
	leaq	256(%rdi), %rcx
	testq	%rax, %rax
	je	.L255
	movq	%rcx, %rdx
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L257:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L252
.L251:
	cmpl	%esi, 32(%rax)
	jge	.L257
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L251
.L252:
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	je	.L249
	cmpl	%esi, 32(%rdx)
	setle	%al
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	xorl	%eax, %eax
.L249:
	ret
	.cfi_endproc
.LFE18004:
	.size	_ZNK2v88internal8compiler16BytecodeAnalysis12IsLoopHeaderEi, .-_ZNK2v88internal8compiler16BytecodeAnalysis12IsLoopHeaderEi
	.section	.text._ZNK2v88internal8compiler16BytecodeAnalysis16GetLoopOffsetForEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16BytecodeAnalysis16GetLoopOffsetForEi
	.type	_ZNK2v88internal8compiler16BytecodeAnalysis16GetLoopOffsetForEi, @function
_ZNK2v88internal8compiler16BytecodeAnalysis16GetLoopOffsetForEi:
.LFB18005:
	.cfi_startproc
	endbr64
	movq	208(%rdi), %rax
	leaq	200(%rdi), %rcx
	testq	%rax, %rax
	je	.L269
	movq	%rcx, %rdx
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L276:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L261
.L260:
	cmpl	%esi, 32(%rax)
	jg	.L276
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L260
.L261:
	cmpq	%rdx, %rcx
	je	.L269
	movl	36(%rdx), %eax
	cmpl	%esi, %eax
	jle	.L258
	movq	264(%rdi), %rax
	leaq	256(%rdi), %rdx
	testq	%rax, %rax
	jne	.L265
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L264
.L265:
	cmpl	%esi, 32(%rax)
	jg	.L277
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L265
.L264:
	movl	40(%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	movl	$-1, %eax
.L258:
	ret
	.cfi_endproc
.LFE18005:
	.size	_ZNK2v88internal8compiler16BytecodeAnalysis16GetLoopOffsetForEi, .-_ZNK2v88internal8compiler16BytecodeAnalysis16GetLoopOffsetForEi
	.section	.text._ZNK2v88internal8compiler16BytecodeAnalysis14GetLoopInfoForEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16BytecodeAnalysis14GetLoopInfoForEi
	.type	_ZNK2v88internal8compiler16BytecodeAnalysis14GetLoopInfoForEi, @function
_ZNK2v88internal8compiler16BytecodeAnalysis14GetLoopInfoForEi:
.LFB18006:
	.cfi_startproc
	endbr64
	movq	264(%rdi), %rdx
	leaq	256(%rdi), %rax
	testq	%rdx, %rdx
	je	.L279
	movq	%rax, %rcx
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L288:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L281
.L280:
	cmpl	%esi, 32(%rdx)
	jge	.L288
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L280
.L281:
	cmpq	%rcx, %rax
	je	.L279
	cmpl	%esi, 32(%rcx)
	cmovle	%rcx, %rax
.L279:
	addq	$40, %rax
	ret
	.cfi_endproc
.LFE18006:
	.size	_ZNK2v88internal8compiler16BytecodeAnalysis14GetLoopInfoForEi, .-_ZNK2v88internal8compiler16BytecodeAnalysis14GetLoopInfoForEi
	.section	.text._ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi
	.type	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi, @function
_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi:
.LFB18007:
	.cfi_startproc
	endbr64
	cmpb	$0, 20(%rdi)
	je	.L292
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$304, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18007:
	.size	_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi, .-_ZNK2v88internal8compiler16BytecodeAnalysis16GetInLivenessForEi
	.section	.text._ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi
	.type	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi, @function
_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi:
.LFB18008:
	.cfi_startproc
	endbr64
	cmpb	$0, 20(%rdi)
	je	.L300
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$304, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	8(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE18008:
	.size	_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi, .-_ZNK2v88internal8compiler16BytecodeAnalysis17GetOutLivenessForEi
	.section	.rodata._ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo.str1.1,"aMS",@progbits,1
.LC0:
	.string	" -> "
.LC1:
	.string	" | "
.LC2:
	.string	": "
.LC3:
	.string	"L"
.LC4:
	.string	"."
	.section	.text.unlikely._ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo,"ax",@progbits
	.align 2
.LCOLDB5:
	.section	.text._ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo,"ax",@progbits
.LHOTB5:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo
	.type	_ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo, @function
_ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo:
.LFB18009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	.LC4(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rdi, -112(%rbp)
	movq	(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11interpreter21BytecodeArrayIteratorC1ENS0_6HandleINS0_13BytecodeArrayEEE@PLT
	movq	-88(%rbp), %rdi
	leaq	304(%rbx), %rax
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv@PLT
	testb	%al, %al
	jne	.L306
	.p2align 4,,10
	.p2align 3
.L341:
	movq	-112(%rbp), %r15
	cmpb	$0, 20(%r15)
	je	.L307
	movl	-72(%rbp), %ebx
	movq	-104(%rbp), %rdi
	movl	%ebx, %esi
	movl	%ebx, -92(%rbp)
	call	_ZNK2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi@PLT
	cmpb	$0, 20(%r15)
	movq	(%rax), %r14
	je	.L329
	movq	-104(%rbp), %rdi
	movl	%ebx, %esi
	call	_ZNK2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi@PLT
	movq	8(%rax), %rbx
.L308:
	movl	8(%r14), %edx
	testl	%edx, %edx
	jle	.L309
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L325:
	cmpl	$1, 12(%r14)
	movq	16(%r14), %rax
	je	.L311
	movl	%r15d, %edx
	sarl	$6, %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
.L311:
	btq	%r15, %rax
	jnc	.L339
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	addl	$1, %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 8(%r14)
	jg	.L325
.L309:
	movl	$4, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jle	.L314
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L327:
	cmpl	$1, 12(%rbx)
	movq	16(%rbx), %rax
	je	.L316
	movl	%r15d, %edx
	sarl	$6, %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rax
.L316:
	btq	%r15, %rax
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	jc	.L338
	movl	$1, %edx
	movq	%r13, %rsi
.L338:
	movq	%r12, %rdi
	addl	$1, %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	8(%rbx), %r15d
	jl	.L327
.L314:
	movl	$3, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-92(%rbp), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-88(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor7PrintToERSo@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %rbx
	testq	%rbx, %rbx
	je	.L340
	cmpb	$0, 56(%rbx)
	je	.L320
	movsbl	67(%rbx), %esi
.L321:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-88(%rbp), %rdi
	call	_ZN2v88internal11interpreter21BytecodeArrayIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayIterator4doneEv@PLT
	testb	%al, %al
	je	.L341
.L306:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L323
	movq	(%rdi), %rax
	call	*72(%rax)
.L323:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L342
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	addl	$1, %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	8(%r14), %r15d
	jl	.L325
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L320:
	movq	%rbx, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%rbx), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L321
	movq	%rbx, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L321
.L329:
	xorl	%ebx, %ebx
	jmp	.L308
.L342:
	call	__stack_chk_fail@PLT
.L340:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo
	.cfi_startproc
	.type	_ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo.cold, @function
_ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo.cold:
.LFSB18009:
.L307:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	8, %eax
	ud2
	.cfi_endproc
.LFE18009:
	.section	.text._ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo
	.size	_ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo, .-_ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo
	.section	.text.unlikely._ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo
	.size	_ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo.cold, .-_ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo.cold
.LCOLDE5:
	.section	.text._ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo
.LHOTE5:
	.section	.rodata._ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_.str1.1,"aMS",@progbits,1
.LC6:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_:
.LFB20138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$-6148914691236517205, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	8(%rdi), %r14
	movq	%r13, %rax
	subq	%r14, %rax
	sarq	$2, %rax
	imulq	%rdx, %rax
	cmpq	$178956970, %rax
	je	.L359
	movq	%rsi, %rcx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	subq	%r14, %rcx
	testq	%rax, %rax
	je	.L353
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L360
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L345:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L361
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L348:
	leaq	(%rax,%rdx), %rdi
	leaq	12(%rax), %r8
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L360:
	testq	%rdx, %rdx
	jne	.L362
	movl	$12, %r8d
	xorl	%edi, %edi
	xorl	%eax, %eax
.L346:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%rcx)
	movl	8(%r15), %edx
	movl	%edx, 8(%rax,%rcx)
	cmpq	%r14, %rbx
	je	.L349
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L350:
	movq	(%rdx), %rsi
	addq	$12, %rdx
	addq	$12, %rcx
	movq	%rsi, -12(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	cmpq	%rdx, %rbx
	jne	.L350
	leaq	-12(%rbx), %rdx
	subq	%r14, %rdx
	shrq	$2, %rdx
	leaq	24(%rax,%rdx,4), %r8
.L349:
	cmpq	%r13, %rbx
	je	.L351
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L352:
	movq	(%rdx), %rsi
	addq	$12, %rdx
	addq	$12, %rcx
	movq	%rsi, -12(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	cmpq	%rdx, %r13
	jne	.L352
	subq	%rbx, %r13
	leaq	-12(%r13), %rdx
	shrq	$2, %rdx
	leaq	12(%r8,%rdx,4), %r8
.L351:
	movq	%rax, %xmm0
	movq	%r8, %xmm1
	movq	%rdi, 24(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	movl	$16, %esi
	movl	$12, %edx
	jmp	.L345
.L361:
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	jmp	.L348
.L362:
	cmpq	$178956970, %rdx
	movl	$178956970, %eax
	cmova	%rax, %rdx
	imulq	$12, %rdx, %rdx
	movq	%rdx, %rsi
	jmp	.L345
.L359:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE20138:
	.size	_ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_:
.LFB21051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r12
	cmpq	24(%rdi), %r12
	je	.L364
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	movl	8(%rsi), %eax
	movl	%eax, 8(%r12)
	addq	$12, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r12, %r15
	movabsq	$-6148914691236517205, %rdx
	subq	%r14, %r15
	movq	%r15, %rax
	sarq	$2, %rax
	imulq	%rdx, %rax
	cmpq	$178956970, %rax
	je	.L378
	testq	%rax, %rax
	je	.L373
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L379
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L367:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L380
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L370:
	leaq	(%rax,%rdx), %rdi
	leaq	12(%rax), %rdx
.L368:
	movq	0(%r13), %rcx
	movq	%rcx, (%rax,%r15)
	movl	8(%r13), %ecx
	movl	%ecx, 8(%rax,%r15)
	cmpq	%r14, %r12
	je	.L371
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L372:
	movq	(%rdx), %rsi
	addq	$12, %rdx
	addq	$12, %rcx
	movq	%rsi, -12(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	cmpq	%rdx, %r12
	jne	.L372
	subq	$12, %r12
	subq	%r14, %r12
	shrq	$2, %r12
	leaq	24(%rax,%r12,4), %rdx
.L371:
	movq	%rax, %xmm0
	movq	%rdx, %xmm1
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L381
	movl	$12, %edx
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L373:
	movl	$16, %esi
	movl	$12, %edx
	jmp	.L367
.L380:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	jmp	.L370
.L378:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L381:
	cmpq	$178956970, %rdx
	movl	$178956970, %eax
	cmova	%rax, %rdx
	imulq	$12, %rdx, %rdx
	movq	%rdx, %rsi
	jmp	.L367
	.cfi_endproc
.LFE21051:
	.size	_ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiEN2v88internal13ZoneAllocatorIS2_EEE16_M_insert_uniqueIS2_EES0_ISt17_Rb_tree_iteratorIS2_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiEN2v88internal13ZoneAllocatorIS2_EEE16_M_insert_uniqueIS2_EES0_ISt17_Rb_tree_iteratorIS2_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiEN2v88internal13ZoneAllocatorIS2_EEE16_M_insert_uniqueIS2_EES0_ISt17_Rb_tree_iteratorIS2_EbEOT_
	.type	_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiEN2v88internal13ZoneAllocatorIS2_EEE16_M_insert_uniqueIS2_EES0_ISt17_Rb_tree_iteratorIS2_EbEOT_, @function
_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiEN2v88internal13ZoneAllocatorIS2_EEE16_M_insert_uniqueIS2_EES0_ISt17_Rb_tree_iteratorIS2_EbEOT_:
.LFB21057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L396
	movl	(%rsi), %esi
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L403:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L385
.L404:
	movq	%rax, %r12
.L384:
	movl	32(%r12), %ecx
	cmpl	%ecx, %esi
	jl	.L403
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L404
.L385:
	testb	%dl, %dl
	jne	.L383
	cmpl	%ecx, %esi
	jle	.L389
.L394:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L405
.L390:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$39, %rax
	jbe	.L406
	leaq	40(%r13), %rax
	movq	%rax, 16(%rdi)
.L392:
	movq	(%r14), %rax
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	movq	%r15, %r12
.L383:
	cmpq	%r12, 32(%rbx)
	je	.L394
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	32(%rax), %ecx
	cmpl	%ecx, (%r14)
	jg	.L394
	movq	%rax, %r12
.L389:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movl	32(%r12), %eax
	cmpl	%eax, (%r14)
	setl	%r8b
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L406:
	movl	$40, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L392
	.cfi_endproc
.LFE21057:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiEN2v88internal13ZoneAllocatorIS2_EEE16_M_insert_uniqueIS2_EES0_ISt17_Rb_tree_iteratorIS2_EbEOT_, .-_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiEN2v88internal13ZoneAllocatorIS2_EEE16_M_insert_uniqueIS2_EES0_ISt17_Rb_tree_iteratorIS2_EbEOT_
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8LoopInfoEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8LoopInfoEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8LoopInfoEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_
	.type	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8LoopInfoEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_, @function
_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8LoopInfoEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_:
.LFB21075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L421
	movl	(%rsi), %esi
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L428:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L410
.L429:
	movq	%rax, %r12
.L409:
	movl	32(%r12), %ecx
	cmpl	%ecx, %esi
	jl	.L428
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L429
.L410:
	testb	%dl, %dl
	jne	.L408
	cmpl	%ecx, %esi
	jle	.L414
.L419:
	movl	$1, %r8d
	cmpq	%r15, %r12
	jne	.L430
.L415:
	movq	(%r14), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$95, %rax
	jbe	.L431
	leaq	96(%r13), %rax
	movq	%rax, 16(%rdi)
.L417:
	movl	(%rbx), %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	%r8d, %edi
	movl	%eax, 32(%r13)
	movl	8(%rbx), %eax
	movdqu	16(%rbx), %xmm1
	movl	%eax, 40(%r13)
	movups	%xmm1, 48(%r13)
	movq	32(%rbx), %rax
	movq	%rax, 64(%r13)
	movq	56(%rbx), %rax
	movdqu	40(%rbx), %xmm2
	movq	$0, 56(%rbx)
	movq	%rax, 88(%r13)
	movups	%xmm2, 72(%r13)
	movups	%xmm0, 40(%rbx)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r14)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	movq	%r15, %r12
.L408:
	cmpq	%r12, 32(%r14)
	je	.L419
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%rbx), %ecx
	cmpl	%ecx, 32(%rax)
	jl	.L419
	movq	%rax, %r12
.L414:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movl	32(%r12), %eax
	cmpl	%eax, (%rbx)
	setl	%r8b
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L431:
	movl	$96, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L417
	.cfi_endproc
.LFE21075:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8LoopInfoEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_, .-_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8LoopInfoEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_
	.section	.rodata._ZN2v88internal8compiler16BytecodeAnalysis8PushLoopEii.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZN2v88internal8compiler16BytecodeAnalysis8PushLoopEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16BytecodeAnalysis8PushLoopEii
	.type	_ZN2v88internal8compiler16BytecodeAnalysis8PushLoopEii, @function
_ZN2v88internal8compiler16BytecodeAnalysis8PushLoopEii:
.LFB17977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	88(%rdi), %rax
	cmpq	96(%rdi), %rax
	je	.L454
.L433:
	leaq	-128(%rbp), %r14
	leaq	184(%rbx), %rdi
	movl	-16(%rax), %r15d
	movl	%edx, -128(%rbp)
	movq	%r14, %rsi
	movl	%r12d, -124(%rbp)
	call	_ZNSt8_Rb_treeIiSt4pairIKiiESt10_Select1stIS2_ESt4lessIiEN2v88internal13ZoneAllocatorIS2_EEE16_M_insert_uniqueIS2_EES0_ISt17_Rb_tree_iteratorIS2_EbEOT_
	movq	(%rbx), %rax
	movq	8(%rbx), %r13
	leaq	-184(%rbp), %rdi
	movq	(%rax), %rcx
	movl	39(%rcx), %eax
	movl	43(%rcx), %esi
	movq	%r13, %rcx
	movl	%r15d, -192(%rbp)
	testl	%eax, %eax
	leal	7(%rax), %edx
	cmovns	%eax, %edx
	sarl	$3, %esi
	sarl	$3, %edx
	call	_ZN2v88internal8compiler23BytecodeLoopAssignmentsC1EiiPNS0_4ZoneE
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r13, -96(%rbp)
	movl	-192(%rbp), %eax
	movdqu	-184(%rbp), %xmm1
	movl	%r12d, -128(%rbp)
	leaq	240(%rbx), %rdi
	movq	%r13, -168(%rbp)
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movl	%eax, -120(%rbp)
	movq	$0, -72(%rbp)
	movaps	%xmm1, -112(%rbp)
	movups	%xmm0, -88(%rbp)
	call	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8LoopInfoEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE16_M_insert_uniqueIS6_EES0_ISt17_Rb_tree_iteratorIS6_EbEOT_
	movq	104(%rbx), %rcx
	leaq	40(%rax), %r13
	movq	88(%rbx), %rax
	leaq	-16(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L434
	movl	%r12d, (%rax)
	movq	%r13, 8(%rax)
	addq	$16, 88(%rbx)
.L432:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L455
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	.cfi_restore_state
	movq	112(%rdi), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L434:
	movq	112(%rbx), %r14
	movq	80(%rbx), %rsi
	subq	96(%rbx), %rax
	movq	%r14, %r15
	sarq	$4, %rax
	subq	%rsi, %r15
	movq	%r15, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$5, %rdx
	addq	%rax, %rdx
	movq	72(%rbx), %rax
	subq	56(%rbx), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$134217727, %rax
	je	.L456
	movq	40(%rbx), %rdi
	movq	48(%rbx), %rdx
	movq	%r14, %rax
	subq	%rdi, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L457
.L437:
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L445
	cmpq	$31, 8(%rax)
	ja	.L458
.L445:
	movq	24(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L459
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L446:
	movq	%rax, 8(%r14)
	movq	88(%rbx), %rax
	movq	%r13, 8(%rax)
	movl	%r12d, (%rax)
	movq	112(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 112(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 96(%rbx)
	movq	%rdx, 104(%rbx)
	movq	%rax, 88(%rbx)
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L457:
	addq	$2, %rcx
	leaq	(%rcx,%rcx), %rax
	cmpq	%rax, %rdx
	ja	.L460
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	24(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r14
	leaq	2(%rdx,%rax), %r8
	movq	24(%rdi), %rax
	leaq	0(,%r8,8), %rsi
	subq	%r14, %rax
	cmpq	%rax, %rsi
	ja	.L461
	addq	%r14, %rsi
	movq	%rsi, 16(%rdi)
.L442:
	movq	%r8, %rax
	movq	80(%rbx), %rsi
	subq	%rcx, %rax
	shrq	%rax
	leaq	(%r14,%rax,8), %rcx
	movq	112(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L443
	movq	%rcx, %rdi
	subq	%rsi, %rdx
	movq	%r8, -200(%rbp)
	call	memmove@PLT
	movq	-200(%rbp), %r8
	movq	%rax, %rcx
.L443:
	movq	48(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L444
	movq	40(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L444:
	movq	%r14, 40(%rbx)
	movq	%r8, 48(%rbx)
.L440:
	movq	%rcx, 80(%rbx)
	movq	(%rcx), %rax
	leaq	(%rcx,%r15), %r14
	movq	(%rcx), %xmm0
	movq	%r14, 112(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	(%r14), %rax
	movq	%rax, 96(%rbx)
	addq	$512, %rax
	movq	%rax, 104(%rbx)
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L460:
	subq	%rcx, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%rdi,%rdx,8), %rcx
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%rcx, %rsi
	jbe	.L439
	cmpq	%r14, %rsi
	je	.L440
	movq	%rcx, %rdi
	call	memmove@PLT
	movq	%rax, %rcx
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L458:
	movq	(%rax), %rdx
	movq	%rdx, 32(%rbx)
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L439:
	cmpq	%r14, %rsi
	je	.L440
	leaq	8(%r15), %rdi
	movq	%rcx, -200(%rbp)
	subq	%rdx, %rdi
	addq	%rcx, %rdi
	call	memmove@PLT
	movq	-200(%rbp), %rcx
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L459:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L446
.L461:
	movq	%r8, -208(%rbp)
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %r8
	movq	%rax, %r14
	jmp	.L442
.L455:
	call	__stack_chk_fail@PLT
.L456:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE17977:
	.size	_ZN2v88internal8compiler16BytecodeAnalysis8PushLoopEii, .-_ZN2v88internal8compiler16BytecodeAnalysis8PushLoopEii
	.section	.text._ZNSt5dequeIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_:
.LFB21654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	80(%rdi), %rcx
	movq	64(%rdi), %rax
	leaq	-16(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L463
	movl	(%rsi), %ecx
	movq	8(%rsi), %rdx
	movl	%ecx, (%rax)
	movq	%rdx, 8(%rax)
	addq	$16, 64(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	subq	72(%rdi), %rax
	movq	%r13, %r14
	sarq	$4, %rax
	subq	%rsi, %r14
	movq	%r14, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$5, %rdx
	addq	%rax, %rdx
	movq	48(%rdi), %rax
	subq	32(%rdi), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$134217727, %rax
	je	.L482
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rdi
	movq	%r13, %rax
	subq	%rdi, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L483
.L466:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L474
	cmpq	$31, 8(%rax)
	ja	.L484
.L474:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L485
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L475:
	movq	%rax, 8(%r13)
	movq	8(%r12), %rdx
	movl	(%r12), %ecx
	movq	64(%rbx), %rax
	movq	%rdx, 8(%rax)
	movl	%ecx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L486
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L487
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L471:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L472
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L472:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L473
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L473:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L469:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L486:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rdi,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L468
	cmpq	%r13, %rsi
	je	.L469
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L484:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L468:
	cmpq	%r13, %rsi
	je	.L469
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L485:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L475
.L487:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L471
.L482:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21654:
	.size	_ZNSt5dequeIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.section	.text._ZN2v88internal8compiler16BytecodeAnalysis7AnalyzeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16BytecodeAnalysis7AnalyzeEv
	.type	_ZN2v88internal8compiler16BytecodeAnalysis7AnalyzeEv, @function
_ZN2v88internal8compiler16BytecodeAnalysis7AnalyzeEv:
.LFB17970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-464(%rbp), %r14
	pushq	%r13
	movq	%r14, %rsi
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	24(%rdi), %rdi
	pushq	%r12
	pushq	%rbx
	subq	$616, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$-1, -464(%rbp)
	movq	$0, -456(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movl	16(%r13), %eax
	movq	8(%r13), %rdx
	movq	%r14, %rdi
	movq	$0, -568(%rbp)
	movq	0(%r13), %rsi
	movl	%eax, -648(%rbp)
	call	_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC1ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneE@PLT
	movq	-424(%rbp), %rax
	movq	%r14, %rdi
	subq	-432(%rbp), %rax
	sarq	$2, %rax
	subl	$1, %eax
	movl	%eax, -408(%rbp)
	call	_ZN2v88internal11interpreter27BytecodeArrayRandomIterator21UpdateOffsetFromIndexEv@PLT
	movq	%r14, %rdi
	movl	$-1, -644(%rbp)
	call	_ZNK2v88internal11interpreter27BytecodeArrayRandomIterator7IsValidEv@PLT
	testb	%al, %al
	je	.L489
	.p2align 4,,10
	.p2align 3
.L688:
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	movl	-456(%rbp), %ebx
	movb	%al, -584(%rbp)
	movl	%ebx, -592(%rbp)
	cmpb	$-81, %al
	je	.L681
	cmpb	$-118, -584(%rbp)
	je	.L682
	movq	112(%r13), %rdi
	movq	88(%r13), %rax
	movq	96(%r13), %rsi
	movq	%rdi, %rdx
	subq	80(%r13), %rdx
	movq	%rax, -600(%rbp)
	sarq	$3, %rdx
	subq	%rsi, %rax
	subq	$1, %rdx
	sarq	$4, %rax
	movq	%rdx, %rcx
	salq	$5, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	72(%r13), %rax
	subq	56(%r13), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L507
	cmpq	%rsi, -600(%rbp)
	je	.L683
.L508:
	movq	-600(%rbp), %rax
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rbx
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandTypesE(%rip), %rdx
	xorl	%r9d, %r9d
	movq	-8(%rax), %r15
	leaq	8(%r15), %rax
	movq	%rax, -624(%rbp)
	movzbl	-584(%rbp), %eax
	movl	(%rbx,%rax,4), %edi
	movq	(%rdx,%rax,8), %rax
	leaq	-496(%rbp), %rbx
	movl	%edi, -608(%rbp)
	movq	%rax, -616(%rbp)
	testl	%edi, %edi
	jle	.L534
	movq	%r13, -640(%rbp)
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L684:
	cmpb	$13, %al
	jne	.L516
	movl	%r9d, %esi
	movq	%r14, %rdi
	movl	%r9d, -632(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor23GetRegisterCountOperandEi@PLT
	movq	-624(%rbp), %rdi
	movl	%r13d, %esi
	movl	%eax, %edx
	call	_ZN2v88internal8compiler23BytecodeLoopAssignments7AddListENS0_11interpreter8RegisterEj
	movl	-632(%rbp), %r9d
	leal	2(%r9), %r12d
.L516:
	cmpl	%r12d, -608(%rbp)
	jle	.L668
.L685:
	movl	%r12d, %r9d
.L509:
	movq	-616(%rbp), %rdi
	movslq	%r9d, %rax
	leal	1(%r9), %r12d
	movzbl	(%rdi,%rax), %eax
	cmpb	$14, %al
	je	.L512
	ja	.L513
	cmpb	$12, %al
	jne	.L684
	movl	%r9d, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movq	-624(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler23BytecodeLoopAssignments3AddENS0_11interpreter8RegisterE
	cmpl	%r12d, -608(%rbp)
	jg	.L685
.L668:
	movq	-640(%rbp), %r13
.L534:
	cmpb	$-80, -584(%rbp)
	je	.L686
.L511:
	movq	-600(%rbp), %rax
	movl	-592(%rbp), %ebx
	cmpl	%ebx, -16(%rax)
	je	.L687
.L492:
	cmpb	$0, 20(%r13)
	jne	.L551
	movl	-408(%rbp), %ebx
.L495:
	subl	$1, %ebx
	movq	%r14, %rdi
	movl	%ebx, -408(%rbp)
	call	_ZN2v88internal11interpreter27BytecodeArrayRandomIterator21UpdateOffsetFromIndexEv@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter27BytecodeArrayRandomIterator7IsValidEv@PLT
	testb	%al, %al
	jne	.L688
.L489:
	movzbl	20(%r13), %eax
	movb	%al, -624(%rbp)
	testb	%al, %al
	je	.L607
	movq	136(%r13), %rax
	movq	128(%r13), %rdi
	leaq	304(%r13), %rbx
	movq	%rax, -616(%rbp)
	movq	%rdi, -584(%rbp)
	cmpq	%rdi, %rax
	je	.L588
	movq	%r13, -592(%rbp)
	.p2align 4,,10
	.p2align 3
.L587:
	movq	-584(%rbp), %rax
	movq	%r14, %rdi
	movl	(%rax), %eax
	movl	%eax, -408(%rbp)
	call	_ZN2v88internal11interpreter27BytecodeArrayRandomIterator21UpdateOffsetFromIndexEv@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	movl	-456(%rbp), %r8d
	movq	%rbx, %rdi
	movl	%eax, %esi
	movl	%eax, %r12d
	movl	%r8d, -600(%rbp)
	call	_ZN2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi@PLT
	movl	-600(%rbp), %r8d
	movq	%rbx, %rdi
	movq	%rax, %r15
	movq	%rax, -608(%rbp)
	movl	%r8d, %esi
	call	_ZN2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi@PLT
	movq	(%r15), %r10
	movq	8(%rax), %rcx
	movl	12(%rcx), %edx
	cmpl	$1, %edx
	je	.L689
	testl	%edx, %edx
	jle	.L562
	movq	16(%rcx), %rsi
	movzbl	-624(%rbp), %r11d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L561:
	movq	16(%r10), %r9
	leaq	(%rsi,%rdx,8), %rsi
	movq	(%rsi), %rdi
	movq	(%r9,%rdx,8), %r15
	orq	%rdi, %r15
	movq	%r15, (%rsi)
	movq	16(%rcx), %rsi
	cmpq	%rdi, (%rsi,%rdx,8)
	cmovne	%r11d, %r8d
	addq	$1, %rdx
	cmpl	%edx, 12(%rcx)
	jg	.L561
	testb	%r8b, %r8b
	jne	.L690
.L562:
	addq	$4, -584(%rbp)
	movq	-584(%rbp), %rax
	cmpq	%rax, -616(%rbp)
	jne	.L587
	movq	-592(%rbp), %r13
.L588:
	cmpl	$-1, -644(%rbp)
	jne	.L691
.L558:
	cmpb	$0, _ZN2v88internal31FLAG_trace_environment_livenessE(%rip)
	jne	.L692
.L607:
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L488
	movq	(%rdi), %rax
	call	*72(%rax)
.L488:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L693
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L513:
	.cfi_restore_state
	cmpb	$15, %al
	jne	.L516
	movl	%r9d, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, -496(%rbp)
	testl	%eax, %eax
	js	.L617
	movq	16(%r15), %rcx
	movl	8(%r15), %edx
	addl	%eax, %edx
	cmpl	$1, 4(%rcx)
	movq	8(%rcx), %rsi
	je	.L526
	testl	%edx, %edx
	leal	63(%rdx), %eax
	movl	%edx, %edi
	cmovns	%edx, %eax
	sarl	$31, %edi
	shrl	$26, %edi
	leal	(%rdx,%rdi), %ecx
	sarl	$6, %eax
	movl	$1, %edx
	andl	$63, %ecx
	cltq
	subl	%edi, %ecx
	salq	%cl, %rdx
	orq	%rdx, (%rsi,%rax,8)
	movl	-496(%rbp), %eax
.L527:
	movq	16(%r15), %rdi
	movl	8(%r15), %edx
	addl	%eax, %edx
	cmpl	$1, 4(%rdi)
	movq	8(%rdi), %rsi
	leal	1(%rdx), %ecx
	je	.L528
	testl	%ecx, %ecx
	leal	64(%rdx), %eax
	movl	%ecx, %edx
	cmovns	%ecx, %eax
	sarl	$31, %edx
	shrl	$26, %edx
	addl	%edx, %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%edx, %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	orq	%rdx, (%rsi,%rax,8)
	movl	-496(%rbp), %eax
.L529:
	movq	16(%r15), %rsi
	addl	8(%r15), %eax
	leal	2(%rax), %ecx
	cmpl	$1, 4(%rsi)
	movq	8(%rsi), %rdx
	je	.L530
	addl	$65, %eax
.L673:
	testl	%ecx, %ecx
	movl	%ecx, %esi
	movl	$1, %edi
	cmovns	%ecx, %eax
	sarl	$31, %esi
	shrl	$26, %esi
	addl	%esi, %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%esi, %ecx
	salq	%cl, %rdi
	orq	%rdi, (%rdx,%rax,8)
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L512:
	movl	%r9d, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor18GetRegisterOperandEi@PLT
	movl	%eax, -496(%rbp)
	testl	%eax, %eax
	js	.L615
	movq	16(%r15), %rcx
	movl	8(%r15), %edx
	addl	%eax, %edx
	cmpl	$1, 4(%rcx)
	movq	8(%rcx), %rsi
	je	.L519
	testl	%edx, %edx
	leal	63(%rdx), %eax
	movl	%edx, %edi
	cmovns	%edx, %eax
	sarl	$31, %edi
	shrl	$26, %edi
	leal	(%rdx,%rdi), %ecx
	sarl	$6, %eax
	movl	$1, %edx
	andl	$63, %ecx
	cltq
	subl	%edi, %ecx
	salq	%cl, %rdx
	orq	%rdx, (%rsi,%rax,8)
	movl	-496(%rbp), %eax
.L520:
	movq	16(%r15), %rsi
	addl	8(%r15), %eax
	leal	1(%rax), %ecx
	addl	$64, %eax
	cmpl	$1, 4(%rsi)
	movq	8(%rsi), %rdx
	jne	.L673
.L530:
	btsq	%rcx, %rdx
	movq	%rdx, 8(%rsi)
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L681:
	movq	112(%r13), %rdi
	movl	-408(%rbp), %eax
	movq	96(%r13), %rsi
	movq	%rdi, %rdx
	subq	80(%r13), %rdx
	movl	%eax, -644(%rbp)
	sarq	$3, %rdx
	movq	88(%r13), %rax
	subq	$1, %rdx
	movq	%rdx, %rcx
	movq	%rax, -600(%rbp)
	subq	%rsi, %rax
	sarq	$4, %rax
	salq	$5, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	72(%r13), %rax
	subq	56(%r13), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L492
	cmpq	%rsi, -600(%rbp)
	jne	.L508
.L683:
	movq	-8(%rdi), %rax
	addq	$512, %rax
	movq	%rax, -600(%rbp)
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L551:
	movq	0(%r13), %rax
	leaq	304(%r13), %rbx
	movq	8(%r13), %rcx
	movl	-592(%rbp), %esi
	movq	%rbx, %rdi
	movq	(%rax), %rax
	movl	39(%rax), %eax
	testl	%eax, %eax
	leal	7(%rax), %edx
	cmovns	%eax, %edx
	sarl	$3, %edx
	call	_ZN2v88internal8compiler19BytecodeLivenessMap18InitializeLivenessEiiPNS0_4ZoneE@PLT
	subq	$8, %rsp
	movzbl	-584(%rbp), %edi
	movq	%r14, %r8
	pushq	%rbx
	movq	0(%r13), %r9
	movq	%rax, %rsi
	leaq	-568(%rbp), %rcx
	leaq	8(%rax), %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_114UpdateLivenessENS0_11interpreter8BytecodeERKNS1_16BytecodeLivenessEPPNS1_21BytecodeLivenessStateERKNS3_21BytecodeArrayAccessorENS0_6HandleINS0_13BytecodeArrayEEERKNS1_19BytecodeLivenessMapE.isra.0
	popq	%rsi
	movl	-408(%rbp), %ebx
	popq	%rdi
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L615:
	movl	%r12d, -632(%rbp)
	xorl	%r13d, %r13d
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L523:
	testl	%ecx, %ecx
	leal	63(%rcx), %eax
	movl	%ecx, %edx
	movq	8(%r12), %rsi
	cmovns	%ecx, %eax
	sarl	$31, %edx
	shrl	$26, %edx
	addl	%edx, %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%edx, %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	orq	%rdx, (%rsi,%rax,8)
	cmpl	$1, %r13d
	je	.L674
.L616:
	movl	$1, %r13d
.L518:
	movq	16(%r15), %r12
	movl	8(%r15), %esi
	movq	%rbx, %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	cmpl	$1, 4(%r12)
	leal	(%rax,%r13), %ecx
	jne	.L523
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, 8(%r12)
	cmpl	$1, %r13d
	jne	.L616
	.p2align 4,,10
	.p2align 3
.L674:
	movl	-632(%rbp), %r12d
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L617:
	movl	%r12d, -632(%rbp)
	xorl	%r13d, %r13d
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L532:
	testl	%ecx, %ecx
	leal	63(%rcx), %eax
	movl	%ecx, %edx
	movq	8(%r12), %rsi
	cmovns	%ecx, %eax
	sarl	$31, %edx
	shrl	$26, %edx
	addl	%edx, %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%edx, %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	orq	%rdx, (%rsi,%rax,8)
.L533:
	addl	$1, %r13d
	cmpl	$3, %r13d
	je	.L674
.L525:
	movq	16(%r15), %r12
	movl	8(%r15), %esi
	movq	%rbx, %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	cmpl	$1, 4(%r12)
	leal	(%rax,%r13), %ecx
	jne	.L532
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, 8(%r12)
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L682:
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor21current_bytecode_sizeEv@PLT
	movl	-592(%rbp), %r15d
	movq	%r14, %rdi
	leal	(%rax,%r15), %r12d
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor19GetJumpTargetOffsetEv@PLT
	movl	%r12d, %edx
	movq	%r13, %rdi
	movl	%eax, %esi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler16BytecodeAnalysis8PushLoopEii
	cmpl	%r15d, -648(%rbp)
	jne	.L494
	movl	%ebx, 296(%r13)
.L494:
	cmpb	$0, 20(%r13)
	movl	-408(%rbp), %ebx
	je	.L495
	movq	136(%r13), %r12
	cmpq	144(%r13), %r12
	je	.L496
	movl	%ebx, (%r12)
	addq	$4, 136(%r13)
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L507:
	cmpb	$-80, -584(%rbp)
	jne	.L492
	movl	$3, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor21current_bytecode_sizeEv@PLT
	movl	%ebx, -496(%rbp)
	leaq	-496(%rbp), %rbx
	addl	-592(%rbp), %eax
	leaq	152(%r13), %rdi
	movq	%rbx, %rsi
	movl	%eax, -492(%rbp)
	movl	%eax, -488(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L687:
	movq	88(%r13), %rsi
	movq	96(%r13), %rdi
	cmpq	%rdi, %rsi
	je	.L537
	subq	$16, %rsi
	movq	112(%r13), %r9
	movq	%rsi, 88(%r13)
.L538:
	movq	%r9, %rdx
	subq	80(%r13), %rdx
	movq	%rsi, %rax
	sarq	$3, %rdx
	subq	%rdi, %rax
	subq	$1, %rdx
	sarq	$4, %rax
	movq	%rdx, %rcx
	salq	$5, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	72(%r13), %rax
	subq	56(%r13), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L541
	cmpq	%rdi, %rsi
	je	.L694
.L542:
	movq	-8(%rsi), %r12
	movq	16(%r15), %rdi
	movq	16(%r12), %rcx
	movl	4(%rcx), %edx
	cmpl	$1, %edx
	je	.L695
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L544
	.p2align 4,,10
	.p2align 3
.L545:
	movq	8(%rdi), %rsi
	movq	8(%rcx), %rdx
	movq	(%rsi,%rax,8), %rsi
	orq	%rsi, (%rdx,%rax,8)
	addq	$1, %rax
	cmpl	%eax, 4(%rcx)
	jg	.L545
.L544:
	movq	32(%r15), %rbx
	movq	40(%r15), %rcx
	cmpq	%rcx, %rbx
	je	.L492
	leaq	24(%r12), %rdi
	leaq	-496(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L549:
	movl	4(%rbx), %eax
	movl	(%rbx), %edx
	movl	-592(%rbp), %esi
	movl	%edx, -496(%rbp)
	movl	%esi, -492(%rbp)
	movl	%eax, -488(%rbp)
	movq	40(%r12), %rsi
	cmpq	48(%r12), %rsi
	je	.L547
	movq	-496(%rbp), %rax
	addq	$12, %rbx
	movq	%rax, (%rsi)
	movl	-488(%rbp), %eax
	movl	%eax, 8(%rsi)
	addq	$12, 40(%r12)
	cmpq	%rbx, %rcx
	jne	.L549
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L526:
	btsq	%rdx, %rsi
	movq	%rsi, 8(%rcx)
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L528:
	btsq	%rcx, %rsi
	movq	%rsi, 8(%rdi)
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L519:
	btsq	%rdx, %rsi
	movq	%rsi, 8(%rcx)
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L547:
	movq	%r15, %rdx
	movq	%rcx, -608(%rbp)
	addq	$12, %rbx
	movq	%rdi, -600(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-608(%rbp), %rcx
	movq	-600(%rbp), %rdi
	cmpq	%rbx, %rcx
	jne	.L549
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L689:
	movq	16(%rcx), %rsi
	movq	16(%r10), %rdx
	orq	%rsi, %rdx
	cmpq	%rdx, %rsi
	movq	%rdx, 16(%rcx)
	setne	%r8b
	testb	%r8b, %r8b
	je	.L562
.L690:
	movq	8(%rax), %rcx
	movq	(%rax), %rsi
	movl	12(%rcx), %edx
	movq	16(%rcx), %r9
	movl	12(%rsi), %ecx
	cmpl	$1, %ecx
	je	.L696
	cmpl	$1, %edx
	je	.L697
	testl	%edx, %edx
	jle	.L573
	leal	-1(%rdx), %ecx
	leaq	8(,%rcx,8), %r10
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L572:
	movq	(%rcx,%r9), %r8
	movq	16(%rsi), %rdi
	movq	%r8, (%rdi,%rcx)
	addq	$8, %rcx
	cmpq	%r10, %rcx
	jne	.L572
	movl	12(%rsi), %ecx
.L573:
	cmpl	%ecx, %edx
	jge	.L566
	movslq	%edx, %rcx
	salq	$3, %rcx
	.p2align 4,,10
	.p2align 3
.L574:
	movq	16(%rsi), %rdi
	addl	$1, %edx
	movq	$0, (%rdi,%rcx)
	addq	$8, %rcx
	cmpl	%edx, 12(%rsi)
	jg	.L574
.L566:
	movq	(%rax), %rax
	.p2align 4,,10
	.p2align 3
.L676:
	movq	%r14, %rdi
	subl	$1, -408(%rbp)
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal11interpreter27BytecodeArrayRandomIterator21UpdateOffsetFromIndexEv@PLT
	cmpl	-456(%rbp), %r12d
	jge	.L698
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	movl	-456(%rbp), %esi
	movq	%rbx, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi@PLT
	movq	%r14, %rcx
	movl	%r15d, %edi
	movq	%rbx, %r9
	movq	8(%rax), %rsi
	movq	%rax, %r13
	movq	-592(%rbp), %rax
	movq	-568(%rbp), %rdx
	movq	(%rax), %r8
	call	_ZN2v88internal8compiler12_GLOBAL__N_117UpdateOutLivenessENS0_11interpreter8BytecodeEPNS1_21BytecodeLivenessStateES6_RKNS3_21BytecodeArrayAccessorENS0_6HandleINS0_13BytecodeArrayEEERKNS1_19BytecodeLivenessMapE
	movq	8(%r13), %rsi
	movq	0(%r13), %rcx
	movl	12(%rsi), %edx
	movq	16(%rsi), %rdi
	movl	12(%rcx), %esi
	cmpl	$1, %esi
	je	.L699
	cmpl	$1, %edx
	je	.L700
	testl	%edx, %edx
	jle	.L584
	leal	-1(%rdx), %esi
	leaq	8(,%rsi,8), %r10
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L583:
	movq	(%rdi,%rsi), %r8
	movq	16(%rcx), %rax
	movq	%r8, (%rax,%rsi)
	addq	$8, %rsi
	cmpq	%r10, %rsi
	jne	.L583
	movl	12(%rcx), %esi
.L584:
	cmpl	%esi, %edx
	jge	.L577
	movslq	%edx, %rsi
	salq	$3, %rsi
	.p2align 4,,10
	.p2align 3
.L585:
	movq	16(%rcx), %rax
	addl	$1, %edx
	movq	$0, (%rax,%rsi)
	addq	$8, %rsi
	cmpl	%edx, 12(%rcx)
	jg	.L585
.L577:
	movq	0(%r13), %rsi
	movq	%r14, %rdx
	movl	%r15d, %edi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116UpdateInLivenessENS0_11interpreter8BytecodeEPNS1_21BytecodeLivenessStateERKNS3_21BytecodeArrayAccessorE
	movq	0(%r13), %rax
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L699:
	movq	%rdi, 16(%rcx)
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L700:
	movq	16(%rcx), %rsi
	movq	%rdi, (%rsi)
	cmpl	$1, 12(%rcx)
	movl	$8, %esi
	jle	.L577
	.p2align 4,,10
	.p2align 3
.L579:
	movq	16(%rcx), %rax
	addl	$1, %edx
	movq	$0, (%rax,%rsi)
	addq	$8, %rsi
	cmpl	%edx, 12(%rcx)
	jg	.L579
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L698:
	movq	-592(%rbp), %rax
	movq	-568(%rbp), %rdx
	movq	%r14, %rdi
	movq	(%rax), %r8
	movq	-608(%rbp), %rax
	movq	%rdx, -600(%rbp)
	movq	8(%rax), %r12
	movq	%r8, -632(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	movq	-632(%rbp), %r8
	movq	%rbx, %r9
	movq	%r14, %rcx
	movq	-600(%rbp), %rdx
	movl	%eax, %edi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_117UpdateOutLivenessENS0_11interpreter8BytecodeEPNS1_21BytecodeLivenessStateES6_RKNS3_21BytecodeArrayAccessorENS0_6HandleINS0_13BytecodeArrayEEERKNS1_19BytecodeLivenessMapE
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L686:
	movl	$3, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor27GetUnsignedImmediateOperandEi@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor21current_bytecode_sizeEv@PLT
	addl	-592(%rbp), %eax
	movl	%ebx, -496(%rbp)
	movl	%eax, -492(%rbp)
	movl	%eax, -488(%rbp)
	movq	40(%r15), %rsi
	cmpq	48(%r15), %rsi
	je	.L535
	movq	-496(%rbp), %rax
	movq	%rax, (%rsi)
	movl	-488(%rbp), %eax
	movl	%eax, 8(%rsi)
	addq	$12, 40(%r15)
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L541:
	movq	32(%r15), %rax
	movq	40(%r15), %r15
	cmpq	%r15, %rax
	je	.L492
	leaq	152(%r13), %rdi
	leaq	-496(%rbp), %rbx
	movq	%r13, -600(%rbp)
	movl	-592(%rbp), %r13d
	movq	%r14, -608(%rbp)
	movq	%rbx, %r12
	movq	%rax, %r14
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L550:
	movl	4(%r14), %edx
	movl	(%r14), %ecx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	addq	$12, %r14
	movl	%r13d, -492(%rbp)
	movl	%ecx, -496(%rbp)
	movl	%edx, -488(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	cmpq	%r14, %r15
	jne	.L550
	movq	-600(%rbp), %r13
	movq	-608(%rbp), %r14
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L691:
	movl	-644(%rbp), %eax
	movq	%r14, %rdi
	leaq	304(%r13), %r15
	leaq	-560(%rbp), %r12
	leaq	-528(%rbp), %rbx
	movl	%eax, -408(%rbp)
	call	_ZN2v88internal11interpreter27BytecodeArrayRandomIterator21UpdateOffsetFromIndexEv@PLT
	movl	-456(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rax, -592(%rbp)
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor25GetJumpTableTargetOffsetsEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal11interpreter22JumpTableTargetOffsets5beginEv@PLT
	leaq	-496(%rbp), %rax
	movq	%r12, %rsi
	xorl	%r12d, %r12d
	movq	%rax, %rdi
	movq	%rax, -584(%rbp)
	call	_ZNK2v88internal11interpreter22JumpTableTargetOffsets3endEv@PLT
	movq	%r13, -600(%rbp)
	movq	%r14, -608(%rbp)
	.p2align 4,,10
	.p2align 3
.L595:
	movq	-584(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorneERKS3_@PLT
	movl	%eax, %r13d
	testb	%al, %al
	je	.L589
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratordeEv@PLT
	movq	-592(%rbp), %rdi
	sarq	$32, %rax
	movq	8(%rdi), %r14
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi@PLT
	movq	(%rax), %r10
	movl	12(%r14), %eax
	cmpl	$1, %eax
	je	.L701
	testl	%eax, %eax
	jle	.L592
	movq	16(%r14), %rdx
	xorl	%esi, %esi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L594:
	movq	16(%r10), %rdi
	leaq	(%rdx,%rax,8), %rdx
	movq	(%rdx), %rcx
	movq	(%rdi,%rax,8), %r11
	orq	%rcx, %r11
	movq	%r11, (%rdx)
	movq	16(%r14), %rdx
	cmpq	%rcx, (%rdx,%rax,8)
	cmovne	%r13d, %esi
	addq	$1, %rax
	cmpl	%eax, 12(%r14)
	jg	.L594
.L591:
	testb	%sil, %sil
	cmovne	%esi, %r12d
.L592:
	movq	%rbx, %rdi
	call	_ZN2v88internal11interpreter22JumpTableTargetOffsets8iteratorppEv@PLT
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L692:
	leaq	-320(%rbp), %r12
	leaq	-400(%rbp), %r14
	movq	%r12, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	%rbx, -320(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZNK2v88internal8compiler16BytecodeAnalysis15PrintLivenessToERSo
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdi
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r12, %rdi
	movq	%rbx, -320(%rbp)
	movq	%rax, -400(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L701:
	movq	16(%r14), %rdx
	movq	16(%r10), %rax
	orq	%rdx, %rax
	cmpq	%rax, %rdx
	movq	%rax, 16(%r14)
	setne	%sil
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L696:
	movq	%r9, 16(%rsi)
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L697:
	movq	16(%rsi), %rcx
	movq	%r9, (%rcx)
	cmpl	$1, 12(%rsi)
	movl	$8, %ecx
	jle	.L566
	.p2align 4,,10
	.p2align 3
.L568:
	movq	16(%rsi), %rdi
	addl	$1, %edx
	movq	$0, (%rdi,%rcx)
	addq	$8, %rcx
	cmpl	%edx, 12(%rsi)
	jg	.L568
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L589:
	movq	-600(%rbp), %r13
	movq	-608(%rbp), %r14
	testb	%r12b, %r12b
	je	.L558
	movq	-592(%rbp), %rax
	movq	8(%rax), %rdx
	movq	(%rax), %rcx
	movq	16(%rdx), %r8
	movl	12(%rdx), %eax
	movl	12(%rcx), %edx
	cmpl	$1, %edx
	je	.L702
	cmpl	$1, %eax
	je	.L703
	testl	%eax, %eax
	jle	.L604
	leal	-1(%rax), %edx
	leaq	8(,%rdx,8), %r9
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L603:
	movq	(%r8,%rdx), %rdi
	movq	16(%rcx), %rsi
	movq	%rdi, (%rsi,%rdx)
	addq	$8, %rdx
	cmpq	%rdx, %r9
	jne	.L603
	movl	12(%rcx), %edx
.L604:
	cmpl	%edx, %eax
	jge	.L597
	movslq	%eax, %rdx
	salq	$3, %rdx
	.p2align 4,,10
	.p2align 3
.L605:
	movq	16(%rcx), %rsi
	addl	$1, %eax
	movq	$0, (%rsi,%rdx)
	addq	$8, %rdx
	cmpl	%eax, 12(%rcx)
	jg	.L605
.L597:
	movq	-592(%rbp), %rbx
	movq	%r14, %rdx
	movl	$-81, %edi
	movq	(%rbx), %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116UpdateInLivenessENS0_11interpreter8BytecodeEPNS1_21BytecodeLivenessStateERKNS3_21BytecodeArrayAccessorE
	movq	(%rbx), %rax
	movq	%r14, %rdi
	subl	$1, -408(%rbp)
	leaq	-568(%rbp), %rbx
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal11interpreter27BytecodeArrayRandomIterator21UpdateOffsetFromIndexEv@PLT
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L704:
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor16current_bytecodeEv@PLT
	movl	-456(%rbp), %esi
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	_ZN2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi@PLT
	subq	$8, %rsp
	movq	%rbx, %rcx
	movq	%r14, %r8
	pushq	%r15
	movq	0(%r13), %r9
	leaq	8(%rax), %rdx
	movq	%rax, %rsi
	movl	%r12d, %edi
	call	_ZN2v88internal8compiler12_GLOBAL__N_114UpdateLivenessENS0_11interpreter8BytecodeERKNS1_16BytecodeLivenessEPPNS1_21BytecodeLivenessStateERKNS3_21BytecodeArrayAccessorENS0_6HandleINS0_13BytecodeArrayEEERKNS1_19BytecodeLivenessMapE.isra.0
	movq	%r14, %rdi
	subl	$1, -408(%rbp)
	call	_ZN2v88internal11interpreter27BytecodeArrayRandomIterator21UpdateOffsetFromIndexEv@PLT
	popq	%rdx
	popq	%rcx
.L606:
	movq	%r14, %rdi
	call	_ZNK2v88internal11interpreter27BytecodeArrayRandomIterator7IsValidEv@PLT
	testb	%al, %al
	jne	.L704
	cmpb	$0, _ZN2v88internal31FLAG_trace_environment_livenessE(%rip)
	je	.L607
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L695:
	movq	8(%rdi), %rax
	orq	%rax, 8(%rcx)
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L694:
	movq	-8(%r9), %rsi
	addq	$512, %rsi
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L537:
	movq	32(%r13), %rax
	testq	%rax, %rax
	je	.L539
	cmpq	$32, 8(%rax)
	ja	.L540
.L539:
	movq	$32, 8(%rdi)
	movq	32(%r13), %rax
	movq	%rax, (%rdi)
	movq	%rdi, 32(%r13)
.L540:
	movq	112(%r13), %rax
	leaq	-8(%rax), %r9
	movq	%r9, 112(%r13)
	movq	-8(%rax), %rdi
	leaq	512(%rdi), %rax
	leaq	496(%rdi), %rsi
	movq	%rdi, 96(%r13)
	movq	%rax, 104(%r13)
	movq	%rsi, 88(%r13)
	jmp	.L538
.L496:
	movq	128(%r13), %r15
	movq	%r12, %r9
	subq	%r15, %r9
	movq	%r9, %rax
	sarq	$2, %rax
	cmpq	$536870911, %rax
	je	.L705
	testq	%rax, %rax
	je	.L610
	leaq	(%rax,%rax), %rdx
	movl	$2147483648, %esi
	movl	$2147483644, %r8d
	cmpq	%rdx, %rax
	jbe	.L706
.L498:
	movq	120(%r13), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rsi, %rax
	jb	.L707
	leaq	(%rdx,%rsi), %rax
	movq	%rax, 16(%rdi)
.L501:
	addq	%rdx, %r8
	leaq	4(%rdx), %rax
	jmp	.L499
.L706:
	testq	%rdx, %rdx
	jne	.L708
	movl	$4, %eax
	xorl	%r8d, %r8d
	xorl	%edx, %edx
.L499:
	movl	%ebx, (%rdx,%r9)
	cmpq	%r15, %r12
	je	.L502
	leaq	-4(%r12), %r9
	leaq	15(%rdx), %rax
	subq	%r15, %r9
	subq	%r15, %rax
	movq	%r9, %rsi
	shrq	$2, %rsi
	cmpq	$30, %rax
	jbe	.L613
	movabsq	$4611686018427387900, %rax
	testq	%rax, %rsi
	je	.L613
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdi
	shrq	$2, %rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L504:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L504
	movq	%rsi, %rdi
	andq	$-4, %rdi
	leaq	0(,%rdi,4), %rax
	leaq	(%r15,%rax), %rcx
	addq	%rdx, %rax
	cmpq	%rsi, %rdi
	je	.L506
	movl	(%rcx), %esi
	movl	%esi, (%rax)
	leaq	4(%rcx), %rsi
	cmpq	%rsi, %r12
	je	.L506
	movl	4(%rcx), %esi
	movl	%esi, 4(%rax)
	leaq	8(%rcx), %rsi
	cmpq	%rsi, %r12
	je	.L506
	movl	8(%rcx), %ecx
	movl	%ecx, 8(%rax)
.L506:
	leaq	8(%rdx,%r9), %rax
.L502:
	movq	%rdx, %xmm0
	movq	%rax, %xmm3
	movq	%r8, 144(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 128(%r13)
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L535:
	leaq	-496(%rbp), %rbx
	leaq	24(%r15), %rdi
	movq	%rbx, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler16ResumeJumpTargetENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L511
.L703:
	movq	16(%rcx), %rdx
	movq	%r8, (%rdx)
	cmpl	$1, 12(%rcx)
	movl	$8, %edx
	jle	.L597
	.p2align 4,,10
	.p2align 3
.L599:
	movq	16(%rcx), %rsi
	addl	$1, %eax
	movq	$0, (%rsi,%rdx)
	addq	$8, %rdx
	cmpl	%eax, 12(%rcx)
	jg	.L599
	jmp	.L597
.L707:
	movq	%r8, -608(%rbp)
	movq	%r9, -600(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-600(%rbp), %r9
	movq	-608(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L501
.L702:
	movq	%r8, 16(%rcx)
	jmp	.L597
.L610:
	movl	$8, %esi
	movl	$4, %r8d
	jmp	.L498
.L613:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L503:
	movl	(%r15,%rax,4), %ecx
	movl	%ecx, (%rdx,%rax,4)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rsi, %rcx
	jne	.L503
	jmp	.L506
.L708:
	cmpq	$536870911, %rdx
	movl	$536870911, %r8d
	cmovbe	%rdx, %r8
	salq	$2, %r8
	leaq	7(%r8), %rax
	andq	$-8, %rax
	movq	%rax, %rsi
	jmp	.L498
.L705:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L693:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17970:
	.size	_ZN2v88internal8compiler16BytecodeAnalysis7AnalyzeEv, .-_ZN2v88internal8compiler16BytecodeAnalysis7AnalyzeEv
	.section	.text._ZNSt11_Deque_baseIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB22076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$5, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L723
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L711:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L712
	movq	%r15, %rbx
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L713:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L724
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L714:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L712
.L717:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L713
	cmpq	$31, 8(%rax)
	jbe	.L713
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L717
.L712:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$31, %r13d
	salq	$4, %r13
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	addq	%rax, %r13
	leaq	512(%rax), %rcx
	movq	%rax, 72(%r12)
	movq	%r13, 64(%r12)
	movq	%rcx, 80(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L724:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L723:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L711
	.cfi_endproc
.LFE22076:
	.size	_ZNSt11_Deque_baseIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZN2v88internal8compiler16BytecodeAnalysisC2ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneENS0_9BailoutIdEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16BytecodeAnalysisC2ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneENS0_9BailoutIdEb
	.type	_ZN2v88internal8compiler16BytecodeAnalysisC2ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneENS0_9BailoutIdEb, @function
_ZN2v88internal8compiler16BytecodeAnalysisC2ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneENS0_9BailoutIdEb:
.LFB17964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, (%rdi)
	xorl	%esi, %esi
	movq	%rdx, 8(%rdi)
	movl	%ecx, 16(%rdi)
	movb	%r8b, 20(%rdi)
	leaq	-240(%rbp), %rdi
	movq	%rdx, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -224(%rbp)
	je	.L742
	movdqa	-240(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	leaq	-144(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler16BytecodeAnalysis14LoopStackEntryENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movq	-200(%rbp), %r9
	movq	-128(%rbp), %rsi
	movdqa	-80(%rbp), %xmm5
	movq	-184(%rbp), %r8
	movq	-208(%rbp), %xmm3
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %xmm1
	movaps	%xmm5, -176(%rbp)
	movq	%r9, %xmm5
	movq	-192(%rbp), %xmm2
	punpcklqdq	%xmm5, %xmm3
	movq	-152(%rbp), %rdx
	movq	-120(%rbp), %rdi
	movups	%xmm3, 56(%r12)
	movq	%r8, %xmm3
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	punpcklqdq	%xmm3, %xmm2
	movq	-232(%rbp), %rax
	movq	-160(%rbp), %xmm0
	movups	%xmm2, 72(%r12)
	movq	%rcx, %xmm2
	movq	-240(%rbp), %xmm4
	movq	-224(%rbp), %r11
	punpcklqdq	%xmm2, %xmm1
	movq	-216(%rbp), %r10
	movaps	%xmm6, -208(%rbp)
	movdqa	-64(%rbp), %xmm6
	movaps	%xmm7, -192(%rbp)
	movq	%rax, %xmm7
	movups	%xmm1, 88(%r12)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm7, %xmm4
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -224(%rbp)
	movq	%rdi, -216(%rbp)
	movq	%r11, 40(%r12)
	movq	%r10, 48(%r12)
	movaps	%xmm6, -160(%rbp)
	movups	%xmm4, 24(%r12)
	movups	%xmm0, 104(%r12)
	testq	%rsi, %rsi
	je	.L727
	movq	-152(%rbp), %rcx
	movq	-184(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L728
	.p2align 4,,10
	.p2align 3
.L731:
	testq	%rax, %rax
	je	.L729
	cmpq	$32, 8(%rax)
	ja	.L730
.L729:
	movq	(%rdx), %rax
	movq	$32, 8(%rax)
	movq	-232(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -232(%rbp)
.L730:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L731
	movq	-216(%rbp), %rdi
	movq	-224(%rbp), %rsi
.L728:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L727
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L727:
	leaq	200(%r12), %rax
	movq	%r13, %rdx
	movq	%r13, 120(%r12)
	leaq	304(%r12), %rdi
	movq	%rax, 216(%r12)
	movq	%rax, 224(%r12)
	leaq	256(%r12), %rax
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movq	$0, 144(%r12)
	movq	%r13, 152(%r12)
	movq	$0, 160(%r12)
	movq	$0, 168(%r12)
	movq	$0, 176(%r12)
	movq	%r13, 184(%r12)
	movl	$0, 200(%r12)
	movq	$0, 208(%r12)
	movq	$0, 232(%r12)
	movq	%r13, 240(%r12)
	movl	$0, 256(%r12)
	movq	$0, 264(%r12)
	movq	%rax, 272(%r12)
	movq	%rax, 280(%r12)
	movq	$0, 288(%r12)
	movq	(%rbx), %rax
	movl	$-1, 296(%r12)
	movslq	11(%rax), %rsi
	call	_ZN2v88internal8compiler19BytecodeLivenessMapC1EiPNS0_4ZoneE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16BytecodeAnalysis7AnalyzeEv
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L743
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L742:
	.cfi_restore_state
	movdqa	-240(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm7
	movq	$0, 40(%r12)
	movq	-216(%rbp), %rax
	movdqa	-160(%rbp), %xmm1
	movups	%xmm6, 24(%r12)
	movdqa	-192(%rbp), %xmm6
	movups	%xmm7, 56(%r12)
	movdqa	-176(%rbp), %xmm7
	movq	%rax, 48(%r12)
	movups	%xmm6, 72(%r12)
	movups	%xmm7, 88(%r12)
	movups	%xmm1, 104(%r12)
	jmp	.L727
.L743:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17964:
	.size	_ZN2v88internal8compiler16BytecodeAnalysisC2ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneENS0_9BailoutIdEb, .-_ZN2v88internal8compiler16BytecodeAnalysisC2ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneENS0_9BailoutIdEb
	.globl	_ZN2v88internal8compiler16BytecodeAnalysisC1ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneENS0_9BailoutIdEb
	.set	_ZN2v88internal8compiler16BytecodeAnalysisC1ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneENS0_9BailoutIdEb,_ZN2v88internal8compiler16BytecodeAnalysisC2ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneENS0_9BailoutIdEb
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler23BytecodeLoopAssignmentsC2EiiPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler23BytecodeLoopAssignmentsC2EiiPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler23BytecodeLoopAssignmentsC2EiiPNS0_4ZoneE:
.LFB22335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22335:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler23BytecodeLoopAssignmentsC2EiiPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler23BytecodeLoopAssignmentsC2EiiPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler23BytecodeLoopAssignmentsC2EiiPNS0_4ZoneE
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
