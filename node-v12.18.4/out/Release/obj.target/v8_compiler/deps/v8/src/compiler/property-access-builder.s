	.file	"property-access-builder.cc"
	.text
	.section	.rodata._ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC0:
	.string	"IsJSReceiver()"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler13JSReceiverRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L5
.L1:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSReceiverEv@PLT
	testb	%al, %al
	jne	.L1
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9619:
	.size	_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler13JSReceiverRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler13JSReceiverRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC2:
	.string	"IsJSObject()"
	.section	.text._ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler11JSObjectRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	testb	%bl, %bl
	jne	.L9
.L6:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10IsJSObjectEv@PLT
	testb	%al, %al
	jne	.L6
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9625:
	.size	_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC3:
	.string	"IsMap()"
	.section	.text._ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler6MapRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB9727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L13
.L10:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef5IsMapEv@PLT
	testb	%al, %al
	jne	.L10
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9727:
	.size	_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler6MapRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.text._ZNK2v88internal8compiler21PropertyAccessBuilder5graphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler21PropertyAccessBuilder5graphEv
	.type	_ZNK2v88internal8compiler21PropertyAccessBuilder5graphEv, @function
_ZNK2v88internal8compiler21PropertyAccessBuilder5graphEv:
.LFB20163:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE20163:
	.size	_ZNK2v88internal8compiler21PropertyAccessBuilder5graphEv, .-_ZNK2v88internal8compiler21PropertyAccessBuilder5graphEv
	.section	.text._ZNK2v88internal8compiler21PropertyAccessBuilder7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler21PropertyAccessBuilder7isolateEv
	.type	_ZNK2v88internal8compiler21PropertyAccessBuilder7isolateEv, @function
_ZNK2v88internal8compiler21PropertyAccessBuilder7isolateEv:
.LFB20164:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE20164:
	.size	_ZNK2v88internal8compiler21PropertyAccessBuilder7isolateEv, .-_ZNK2v88internal8compiler21PropertyAccessBuilder7isolateEv
	.section	.text._ZNK2v88internal8compiler21PropertyAccessBuilder6commonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler21PropertyAccessBuilder6commonEv
	.type	_ZNK2v88internal8compiler21PropertyAccessBuilder6commonEv, @function
_ZNK2v88internal8compiler21PropertyAccessBuilder6commonEv:
.LFB20165:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE20165:
	.size	_ZNK2v88internal8compiler21PropertyAccessBuilder6commonEv, .-_ZNK2v88internal8compiler21PropertyAccessBuilder6commonEv
	.section	.text._ZNK2v88internal8compiler21PropertyAccessBuilder10simplifiedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler21PropertyAccessBuilder10simplifiedEv
	.type	_ZNK2v88internal8compiler21PropertyAccessBuilder10simplifiedEv, @function
_ZNK2v88internal8compiler21PropertyAccessBuilder10simplifiedEv:
.LFB20166:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	376(%rax), %rax
	ret
	.cfi_endproc
.LFE20166:
	.size	_ZNK2v88internal8compiler21PropertyAccessBuilder10simplifiedEv, .-_ZNK2v88internal8compiler21PropertyAccessBuilder10simplifiedEv
	.section	.text._ZN2v88internal8compiler17HasOnlyStringMapsEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler17HasOnlyStringMapsEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	.type	_ZN2v88internal8compiler17HasOnlyStringMapsEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE, @function
_ZN2v88internal8compiler17HasOnlyStringMapsEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE:
.LFB20167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rsi), %rbx
	movq	16(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	%r13, %rbx
	je	.L19
	movq	%rdi, %r12
	leaq	-64(%rbp), %r14
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L26:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L19
.L22:
	movq	(%rbx), %rdx
	movq	%r14, %rdi
	movl	$1, %ecx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef11IsStringMapEv@PLT
	testb	%al, %al
	jne	.L26
.L18:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L27
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L18
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20167:
	.size	_ZN2v88internal8compiler17HasOnlyStringMapsEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE, .-_ZN2v88internal8compiler17HasOnlyStringMapsEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	.section	.text._ZN2v88internal8compiler21PropertyAccessBuilder19TryBuildStringCheckEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEPPNS1_4NodeESE_SD_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21PropertyAccessBuilder19TryBuildStringCheckEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEPPNS1_4NodeESE_SD_
	.type	_ZN2v88internal8compiler21PropertyAccessBuilder19TryBuildStringCheckEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEPPNS1_4NodeESE_SD_, @function
_ZN2v88internal8compiler21PropertyAccessBuilder19TryBuildStringCheckEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEPPNS1_4NodeESE_SD_:
.LFB20169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	8(%rdx), %rbx
	movq	16(%rdx), %r15
	movq	%rdi, -112(%rbp)
	movq	%r9, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r15, %rbx
	jne	.L32
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L38:
	addq	$8, %rbx
	movq	-104(%rbp), %rsi
	cmpq	%rbx, %r15
	je	.L29
.L32:
	movq	(%rbx), %rdx
	movq	%r14, %rdi
	movl	$1, %ecx
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef11IsStringMapEv@PLT
	testb	%al, %al
	jne	.L38
.L28:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L39
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	-112(%rbp), %rax
	movq	0(%r13), %rcx
	movq	%r14, %rsi
	movq	(%r12), %rbx
	movq	(%rax), %rax
	movq	%rcx, -104(%rbp)
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckStringERKNS1_14FeedbackSourceE@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	-120(%rbp), %rax
	movhps	-104(%rbp), %xmm0
	leaq	-80(%rbp), %rcx
	movl	$3, %edx
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 0(%r13)
	movq	%rax, (%r12)
	movl	$1, %eax
	jmp	.L28
.L39:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20169:
	.size	_ZN2v88internal8compiler21PropertyAccessBuilder19TryBuildStringCheckEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEPPNS1_4NodeESE_SD_, .-_ZN2v88internal8compiler21PropertyAccessBuilder19TryBuildStringCheckEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEPPNS1_4NodeESE_SD_
	.section	.text._ZN2v88internal8compiler21PropertyAccessBuilder19TryBuildNumberCheckEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEPPNS1_4NodeESE_SD_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21PropertyAccessBuilder19TryBuildNumberCheckEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEPPNS1_4NodeESE_SD_
	.type	_ZN2v88internal8compiler21PropertyAccessBuilder19TryBuildNumberCheckEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEPPNS1_4NodeESE_SD_, @function
_ZN2v88internal8compiler21PropertyAccessBuilder19TryBuildNumberCheckEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEPPNS1_4NodeESE_SD_:
.LFB20170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	8(%rdx), %rbx
	movq	16(%rdx), %r15
	movq	%rdi, -112(%rbp)
	movq	%r9, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r15, %rbx
	jne	.L44
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L42:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L41
.L44:
	movq	(%rbx), %rdx
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef13instance_typeEv@PLT
	movq	-104(%rbp), %rsi
	cmpw	$65, %ax
	je	.L42
	xorl	%eax, %eax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L41:
	movq	-112(%rbp), %rax
	movq	0(%r13), %rcx
	movq	%r14, %rsi
	movq	(%r12), %rbx
	movq	(%rax), %rax
	movq	%rcx, -104(%rbp)
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11CheckNumberERKNS1_14FeedbackSourceE@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	-120(%rbp), %rax
	movhps	-104(%rbp), %xmm0
	leaq	-80(%rbp), %rcx
	movl	$3, %edx
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 0(%r13)
	movq	%rax, (%r12)
	movl	$1, %eax
.L40:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L50
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L50:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20170:
	.size	_ZN2v88internal8compiler21PropertyAccessBuilder19TryBuildNumberCheckEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEPPNS1_4NodeESE_SD_, .-_ZN2v88internal8compiler21PropertyAccessBuilder19TryBuildNumberCheckEPNS1_12JSHeapBrokerERKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEEPPNS1_4NodeESE_SD_
	.section	.text._ZN2v88internal8compiler21PropertyAccessBuilder15BuildCheckValueEPNS1_4NodeEPS4_S4_NS0_6HandleINS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21PropertyAccessBuilder15BuildCheckValueEPNS1_4NodeEPS4_S4_NS0_6HandleINS0_10HeapObjectEEE
	.type	_ZN2v88internal8compiler21PropertyAccessBuilder15BuildCheckValueEPNS1_4NodeEPS4_S4_NS0_6HandleINS0_10HeapObjectEEE, @function
_ZN2v88internal8compiler21PropertyAccessBuilder15BuildCheckValueEPNS1_4NodeEPS4_S4_NS0_6HandleINS0_10HeapObjectEEE:
.LFB20172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%r8, %rsi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	cmpw	$30, 16(%rax)
	jne	.L52
	movq	%rbx, %r13
	cmpq	48(%rax), %r8
	je	.L51
.L52:
	movq	(%r12), %rdi
	call	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rax, %r13
	movq	(%r12), %rax
	movq	(%rax), %r9
	movq	376(%rax), %rdi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder14ReferenceEqualEv@PLT
	movq	-104(%rbp), %r9
	movq	%r13, %xmm1
	xorl	%r8d, %r8d
	movq	%rbx, %xmm0
	leaq	-80(%rbp), %rbx
	movq	%rax, %rsi
	movl	$2, %edx
	punpcklqdq	%xmm1, %xmm0
	movq	%r9, %rdi
	movq	%rbx, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%r14), %rcx
	leaq	-96(%rbp), %rdx
	movl	$39, %esi
	movq	%rax, -112(%rbp)
	movq	(%r12), %rax
	movq	%rcx, -104(%rbp)
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder7CheckIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$3, %edx
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%r15, -64(%rbp)
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r14)
.L51:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L57:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20172:
	.size	_ZN2v88internal8compiler21PropertyAccessBuilder15BuildCheckValueEPNS1_4NodeEPS4_S4_NS0_6HandleINS0_10HeapObjectEEE, .-_ZN2v88internal8compiler21PropertyAccessBuilder15BuildCheckValueEPNS1_4NodeEPS4_S4_NS0_6HandleINS0_10HeapObjectEEE
	.section	.text._ZN2v88internal8compiler21PropertyAccessBuilder13ResolveHolderERKNS1_18PropertyAccessInfoEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21PropertyAccessBuilder13ResolveHolderERKNS1_18PropertyAccessInfoEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21PropertyAccessBuilder13ResolveHolderERKNS1_18PropertyAccessInfoEPNS1_4NodeE, @function
_ZN2v88internal8compiler21PropertyAccessBuilder13ResolveHolderERKNS1_18PropertyAccessInfoEPNS1_4NodeE:
.LFB20173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	88(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L61
	movq	(%rdi), %r13
	movq	8(%rdi), %rsi
	leaq	-48(%rbp), %r12
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
.L58:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L63
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	%rdx, %rax
	jmp	.L58
.L63:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20173:
	.size	_ZN2v88internal8compiler21PropertyAccessBuilder13ResolveHolderERKNS1_18PropertyAccessInfoEPNS1_4NodeE, .-_ZN2v88internal8compiler21PropertyAccessBuilder13ResolveHolderERKNS1_18PropertyAccessInfoEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler21PropertyAccessBuilder21ConvertRepresentationENS0_14RepresentationE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler21PropertyAccessBuilder21ConvertRepresentationENS0_14RepresentationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21PropertyAccessBuilder21ConvertRepresentationENS0_14RepresentationE
	.type	_ZN2v88internal8compiler21PropertyAccessBuilder21ConvertRepresentationENS0_14RepresentationE, @function
_ZN2v88internal8compiler21PropertyAccessBuilder21ConvertRepresentationENS0_14RepresentationE:
.LFB20174:
	.cfi_startproc
	endbr64
	subl	$1, %edi
	cmpb	$3, %dil
	ja	.L65
	movzbl	%dil, %edi
	leaq	CSWTCH.142(%rip), %rax
	movzbl	(%rax,%rdi), %eax
	ret
.L65:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20174:
	.size	_ZN2v88internal8compiler21PropertyAccessBuilder21ConvertRepresentationENS0_14RepresentationE, .-_ZN2v88internal8compiler21PropertyAccessBuilder21ConvertRepresentationENS0_14RepresentationE
	.section	.rodata._ZN2v88internal8compiler21PropertyAccessBuilder29TryBuildLoadConstantDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"IsHeapObject()"
	.section	.text._ZN2v88internal8compiler21PropertyAccessBuilder29TryBuildLoadConstantDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21PropertyAccessBuilder29TryBuildLoadConstantDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21PropertyAccessBuilder29TryBuildLoadConstantDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeE, @function
_ZN2v88internal8compiler21PropertyAccessBuilder29TryBuildLoadConstantDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeE:
.LFB20175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$3, (%rdx)
	jne	.L88
	movq	88(%rdx), %r15
	leaq	-96(%rbp), %rax
	movq	%rdi, %r14
	movq	%rdx, %rbx
	movq	%rax, -136(%rbp)
	leaq	-80(%rbp), %r12
	testq	%r15, %r15
	je	.L107
.L72:
	movq	%r15, %rdx
	movq	-136(%rbp), %r15
	movq	8(%r14), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	96(%rbx), %rcx
	movzbl	104(%rbx), %edx
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler11JSObjectRef18GetOwnDataPropertyENS0_14RepresentationENS0_10FieldIndexENS1_19SerializationPolicyE@PLT
	cmpb	$0, -80(%rbp)
	je	.L88
	movq	(%r14), %rdi
	leaq	-72(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%rax, %r15
.L69:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L108
	addq	$136, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	xorl	%r15d, %r15d
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L107:
	movq	(%rcx), %rax
	cmpw	$30, 16(%rax)
	jne	.L69
	movq	48(%rax), %rax
	movq	8(%rdi), %rsi
	leaq	-112(%rbp), %r13
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L75
	movdqa	-112(%rbp), %xmm0
	movq	%r12, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef10IsJSObjectEv@PLT
	testb	%al, %al
	je	.L69
	movq	-136(%rbp), %r13
	movq	8(%r14), %rsi
	xorl	%ecx, %ecx
	movq	-160(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L75
	movdqa	-96(%rbp), %xmm1
	movq	%r12, %rdi
	movaps	%xmm1, -80(%rbp)
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	16(%rbx), %r11
	movq	%rax, -128(%rbp)
	movq	24(%rbx), %rax
	movq	%rdx, -120(%rbp)
	movq	%rax, %rdx
	movq	%rax, -168(%rbp)
	subq	%r11, %rdx
	movq	%rdx, %rax
	sarq	$5, %rdx
	sarq	$3, %rax
	testq	%rdx, %rdx
	jle	.L76
	movq	%rdx, %rax
	leaq	-128(%rbp), %r13
	salq	$5, %rax
	addq	%r11, %rax
	movq	%rax, -152(%rbp)
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L113:
	movq	8(%r11), %rdx
	movq	8(%r14), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	movq	-144(%rbp), %r11
	testb	%al, %al
	jne	.L109
	movq	16(%r11), %rdx
	movq	8(%r14), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r11, -144(%rbp)
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	movq	-144(%rbp), %r11
	testb	%al, %al
	jne	.L110
	movq	24(%r11), %rdx
	movq	8(%r14), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r11, -144(%rbp)
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	movq	-144(%rbp), %r11
	testb	%al, %al
	jne	.L111
	addq	$32, %r11
	cmpq	%r11, -152(%rbp)
	je	.L112
.L81:
	movq	(%r11), %rdx
	movq	8(%r14), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r11, -144(%rbp)
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	movq	-144(%rbp), %r11
	testb	%al, %al
	je	.L113
.L77:
	cmpq	%r11, -168(%rbp)
	je	.L69
	movq	8(%r14), %rsi
	movq	-160(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L75
	movdqa	-80(%rbp), %xmm2
	movq	-136(%rbp), %rdi
	movaps	%xmm2, -96(%rbp)
	call	_ZNK2v88internal8compiler9ObjectRef10AsJSObjectEv@PLT
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal8compiler11JSObjectRef6objectEv@PLT
	movq	%rax, %r15
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L112:
	movq	-168(%rbp), %rax
	subq	%r11, %rax
	sarq	$3, %rax
.L76:
	leaq	-128(%rbp), %r13
	cmpq	$2, %rax
	je	.L82
	cmpq	$3, %rax
	je	.L83
	leaq	-128(%rbp), %r13
	cmpq	$1, %rax
	jne	.L69
.L84:
	movq	(%r11), %rdx
	movq	8(%r14), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r11, -144(%rbp)
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	movq	-144(%rbp), %r11
	testb	%al, %al
	je	.L69
	jmp	.L77
.L109:
	addq	$8, %r11
	jmp	.L77
.L110:
	addq	$16, %r11
	jmp	.L77
.L111:
	addq	$24, %r11
	jmp	.L77
.L83:
	movq	(%r11), %rdx
	movq	8(%r14), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	leaq	-128(%rbp), %r13
	movq	%r11, -144(%rbp)
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	movq	-144(%rbp), %r11
	testb	%al, %al
	jne	.L77
	addq	$8, %r11
.L82:
	movq	(%r11), %rdx
	movq	8(%r14), %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r11, -144(%rbp)
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	movq	-144(%rbp), %r11
	testb	%al, %al
	jne	.L77
	addq	$8, %r11
	jmp	.L84
.L108:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20175:
	.size	_ZN2v88internal8compiler21PropertyAccessBuilder29TryBuildLoadConstantDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeE, .-_ZN2v88internal8compiler21PropertyAccessBuilder29TryBuildLoadConstantDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21PropertyAccessBuilder18BuildLoadDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeEPSA_SB_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21PropertyAccessBuilder18BuildLoadDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeEPSA_SB_
	.type	_ZN2v88internal8compiler21PropertyAccessBuilder18BuildLoadDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeEPSA_SB_, @function
_ZN2v88internal8compiler21PropertyAccessBuilder18BuildLoadDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeEPSA_SB_:
.LFB20178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$248, %rsp
	movq	%r8, -216(%rbp)
	movq	%r9, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21PropertyAccessBuilder29TryBuildLoadConstantDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeE
	testq	%rax, %rax
	je	.L148
.L114:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L149
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movq	112(%r14), %rax
	movq	96(%r14), %r10
	movq	%rax, -248(%rbp)
	movzbl	104(%r14), %eax
	subl	$1, %eax
	cmpb	$3, %al
	ja	.L116
	leaq	CSWTCH.142(%rip), %rdx
	movzbl	%al, %eax
	movzbl	(%rdx,%rax), %eax
	movq	88(%r14), %rdx
	movb	%al, -217(%rbp)
	testq	%rdx, %rdx
	je	.L117
	movq	(%r12), %rbx
	movq	8(%r12), %rsi
	leaq	-144(%rbp), %r15
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%r10, -240(%rbp)
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	-240(%rbp), %r10
	movq	%rax, %rbx
.L117:
	movq	%r10, %rax
	andl	$16384, %eax
	movq	%rax, -264(%rbp)
	je	.L150
.L119:
	movl	%r10d, %eax
	movq	%r13, %rdi
	andl	$16383, %eax
	movl	%eax, -240(%rbp)
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	movq	%rax, %r15
	movzbl	-217(%rbp), %eax
	cmpb	$14, %al
	ja	.L116
	leaq	.L121(%rip), %rsi
	movzbl	%al, %ecx
	movslq	(%rsi,%rcx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler21PropertyAccessBuilder18BuildLoadDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeEPSA_SB_,"a",@progbits
	.align 4
	.align 4
.L121:
	.long	.L140-.L121
	.long	.L140-.L121
	.long	.L140-.L121
	.long	.L126-.L121
	.long	.L126-.L121
	.long	.L130-.L121
	.long	.L126-.L121
	.long	.L140-.L121
	.long	.L125-.L121
	.long	.L126-.L121
	.long	.L125-.L121
	.long	.L125-.L121
	.long	.L122-.L121
	.long	.L122-.L121
	.long	.L120-.L121
	.section	.text._ZN2v88internal8compiler21PropertyAccessBuilder18BuildLoadDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeEPSA_SB_
	.p2align 4,,10
	.p2align 3
.L140:
	movzbl	-217(%rbp), %eax
	movb	%al, -256(%rbp)
.L128:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler18PropertyAccessInfo17GetConstFieldInfoEv@PLT
	movl	-240(%rbp), %edx
	movzbl	-217(%rbp), %ecx
	movb	$1, -208(%rbp)
	movzbl	-256(%rbp), %edi
	movq	%r15, -200(%rbp)
	movl	%edx, -204(%rbp)
	movq	-248(%rbp), %rdx
	movq	$0, -192(%rbp)
	movq	%rdx, -184(%rbp)
	movb	%cl, -176(%rbp)
	movb	%dil, -175(%rbp)
	movb	$5, -174(%rbp)
	movl	$0, -172(%rbp)
	movq	%rax, -168(%rbp)
	movb	$0, -160(%rbp)
	cmpb	$13, %cl
	je	.L151
	cmpb	$7, %cl
	je	.L136
	cmpb	$10, %cl
	je	.L136
.L138:
	movq	-216(%rbp), %rax
	leaq	-80(%rbp), %r15
	movq	(%rax), %r13
.L135:
	movq	(%r12), %rax
	movq	-232(%rbp), %rdx
	leaq	-208(%rbp), %rsi
	movq	376(%rax), %rdi
	movq	(%rax), %r12
	movq	(%rdx), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, %xmm1
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, %rdi
	punpcklqdq	%xmm1, %xmm0
	movq	%r14, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-216(%rbp), %rbx
	movq	%rax, (%rbx)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L126:
	movb	$2, -256(%rbp)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L125:
	movb	$7, -256(%rbp)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L122:
	movb	$6, -256(%rbp)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L151:
	cmpq	$0, -264(%rbp)
	jne	.L138
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler7NameRef6objectEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler18PropertyAccessInfo17GetConstFieldInfoEv@PLT
	movl	-240(%rbp), %edi
	movb	$3, -110(%rbp)
	movl	$1799, %edx
	movw	%dx, -112(%rbp)
	movq	-232(%rbp), %rdx
	leaq	-144(%rbp), %rsi
	movl	%edi, -140(%rbp)
	movq	-216(%rbp), %rdi
	movq	%rax, -104(%rbp)
	movq	(%r12), %rax
	movq	%r13, -136(%rbp)
	movq	(%rdx), %r15
	movq	(%rdi), %r13
	movb	$1, -144(%rbp)
	movq	$0, -128(%rbp)
	movq	$16777217, -120(%rbp)
	movl	$0, -108(%rbp)
	movb	$0, -96(%rbp)
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r13, %xmm2
	movq	%r15, -64(%rbp)
	leaq	-80(%rbp), %r15
	movq	%rbx, %xmm0
	movq	%r14, %rdi
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	punpcklqdq	%xmm2, %xmm0
	movq	%r15, %rcx
	movl	$3, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-216(%rbp), %rdi
	movl	$8, -204(%rbp)
	movq	$0, -200(%rbp)
	movq	%rax, %rbx
	movq	%rax, %r13
	movq	%rax, (%rdi)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L150:
	movq	(%r12), %rax
	movq	-216(%rbp), %rdi
	leaq	-144(%rbp), %rsi
	movq	%r10, -288(%rbp)
	movq	-232(%rbp), %rdx
	movq	%rsi, -256(%rbp)
	movq	(%rdi), %rcx
	movq	(%rax), %r9
	movq	%rsi, %rdi
	movq	376(%rax), %r8
	movq	(%rdx), %r15
	movq	%r9, -280(%rbp)
	movq	%rcx, -240(%rbp)
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal8compiler13AccessBuilder39ForJSObjectPropertiesOrHashKnownPointerEv@PLT
	movq	-272(%rbp), %r8
	movq	-256(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	-280(%rbp), %r9
	movq	%r15, -64(%rbp)
	leaq	-80(%rbp), %r15
	movq	%rbx, %xmm0
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movhps	-240(%rbp), %xmm0
	movq	%r9, %rdi
	movl	$3, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-216(%rbp), %rdi
	movq	-288(%rbp), %r10
	movq	%rax, %rbx
	movq	%rax, (%rdi)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L136:
	movq	128(%r14), %r13
	testq	%r13, %r13
	je	.L138
	leaq	-144(%rbp), %r14
	movq	8(%r12), %rsi
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	je	.L138
	movq	16(%r12), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler23CompilationDependencies17DependOnStableMapERKNS1_6MapRefE@PLT
	movq	%r13, -192(%rbp)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L120:
	movb	$0, -256(%rbp)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L130:
	movb	$4, -256(%rbp)
	jmp	.L128
.L149:
	call	__stack_chk_fail@PLT
.L116:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20178:
	.size	_ZN2v88internal8compiler21PropertyAccessBuilder18BuildLoadDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeEPSA_SB_, .-_ZN2v88internal8compiler21PropertyAccessBuilder18BuildLoadDataFieldERKNS1_7NameRefERKNS1_18PropertyAccessInfoEPNS1_4NodeEPSA_SB_
	.section	.rodata._ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_.str1.1,"aMS",@progbits,1
.LC6:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	.type	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_, @function
_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_:
.LFB23793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r13
	cmpq	24(%rdi), %r13
	je	.L153
	movq	(%rsi), %rax
	movq	%rax, 0(%r13)
	addq	$8, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r13, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L178
	testq	%rax, %rax
	je	.L165
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L179
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L156:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L180
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L159:
	leaq	(%rax,%r15), %rdi
	leaq	8(%rax), %rdx
.L157:
	movq	(%r12), %rsi
	movq	%rsi, (%rax,%rcx)
	cmpq	%r14, %r13
	je	.L160
	leaq	-8(%r13), %rdx
	movq	%rdx, %rcx
	leaq	15(%rax), %rdx
	subq	%r14, %rcx
	subq	%r14, %rdx
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L168
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L168
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L162:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L162
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r14
	addq	%rax, %rsi
	cmpq	%rdx, %r8
	je	.L164
	movq	(%r14), %rdx
	movq	%rdx, (%rsi)
.L164:
	leaq	16(%rax,%rcx), %rdx
.L160:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L181
	movl	$8, %edx
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L165:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L168:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L161:
	movq	(%r14,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %rsi
	jne	.L161
	jmp	.L164
.L180:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L159
.L178:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L181:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L156
	.cfi_endproc
.LFE23793:
	.size	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_, .-_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	.section	.text._ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	.type	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_, @function
_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_:
.LFB23798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L220
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L198
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L221
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L184:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L222
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L187:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L221:
	testq	%rdx, %rdx
	jne	.L223
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L185:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L188
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L201
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L201
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L190:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L190
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L192
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L192:
	leaq	16(%rax,%r8), %rcx
.L188:
	cmpq	%r14, %r12
	je	.L193
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L202
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L202
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L195:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L195
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L197
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L197:
	leaq	8(%rcx,%r9), %rcx
.L193:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L202:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L194:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L194
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L201:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L189:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L189
	jmp	.L192
.L222:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L187
.L220:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L223:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L184
	.cfi_endproc
.LFE23798:
	.size	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_, .-_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	.section	.rodata._ZN2v88internal8compiler21PropertyAccessBuilder14BuildCheckMapsEPNS1_4NodeEPS4_S4_RKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"vector::reserve"
	.section	.text._ZN2v88internal8compiler21PropertyAccessBuilder14BuildCheckMapsEPNS1_4NodeEPS4_S4_RKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21PropertyAccessBuilder14BuildCheckMapsEPNS1_4NodeEPS4_S4_RKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	.type	_ZN2v88internal8compiler21PropertyAccessBuilder14BuildCheckMapsEPNS1_4NodeEPS4_S4_RKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE, @function
_ZN2v88internal8compiler21PropertyAccessBuilder14BuildCheckMapsEPNS1_4NodeEPS4_S4_RKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE:
.LFB20171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%rcx, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpw	$30, 16(%rax)
	je	.L225
	movq	16(%r12), %rax
	movq	8(%r8), %r8
	leaq	-96(%rbp), %r13
	movq	%rax, -144(%rbp)
.L226:
	cmpq	-144(%rbp), %r8
	je	.L266
	xorl	%r15d, %r15d
	movq	%r8, %rbx
	movl	$1, %r12d
	movl	%r15d, -136(%rbp)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L291:
	testq	%rdx, %rdx
	jne	.L235
	cmpq	%r12, %rax
	je	.L261
	movq	16(%r15), %rax
	movq	24(%r15), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L289
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%r15)
.L238:
	movq	%r15, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	cmpq	-120(%rbp), %r12
	jnb	.L239
	leaq	-112(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	movq	%r12, -112(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	movq	-152(%rbp), %rax
	movq	16(%rax), %rsi
	cmpq	24(%rax), %rsi
	je	.L240
	movq	-120(%rbp), %rdx
	movq	%rdx, (%rsi)
	addq	$8, 16(%rax)
.L241:
	orq	$2, %rax
	movq	%rax, %r12
.L261:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler6MapRef19is_migration_targetEv@PLT
	testb	%al, %al
	movl	$1, %eax
	cmove	-136(%rbp), %eax
	addq	$8, %rbx
	movl	%eax, -136(%rbp)
	cmpq	%rbx, -144(%rbp)
	je	.L290
.L263:
	movq	(%rbx), %rdx
	movq	8(%r14), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	(%r14), %rax
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	(%rax), %r15
	call	_ZNK2v88internal8compiler6MapRef6objectEv@PLT
	movq	%r12, %rdx
	andl	$3, %edx
	movq	%rax, -120(%rbp)
	cmpq	$1, %rdx
	jne	.L291
	movq	%rax, %r12
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L235:
	movq	6(%r12), %rsi
	movq	14(%r12), %rcx
	leaq	-2(%r12), %r8
	subq	%rsi, %rcx
	sarq	$3, %rcx
	je	.L242
	xorl	%edx, %edx
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L292:
	addq	$1, %rdx
	cmpq	%rcx, %rdx
	je	.L242
.L243:
	cmpq	(%rsi,%rdx,8), %rax
	je	.L261
	jnb	.L292
.L242:
	movq	16(%r15), %r12
	movq	24(%r15), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	jbe	.L293
	leaq	32(%r12), %rax
	movq	%rax, 16(%r15)
.L245:
	movq	%r15, (%r12)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	8(%r8), %rdx
	movq	16(%r8), %rcx
	subq	%rdx, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	addq	$1, %rax
	cmpq	$268435455, %rax
	ja	.L294
	xorl	%edi, %edi
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L295
.L247:
	xorl	%ecx, %ecx
	movq	%rcx, %r15
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%rax, (%rsi)
	movq	16(%r12), %rax
	addq	$1, %r15
	leaq	8(%rax), %rsi
	movq	%rsi, 16(%r12)
.L288:
	movq	8(%r8), %rdx
	movq	16(%r8), %rax
	movq	24(%r12), %rdi
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r15, %rax
	jbe	.L282
.L254:
	leaq	(%rdx,%r15,8), %rdx
	movq	(%rdx), %rax
	cmpq	-120(%rbp), %rax
	ja	.L282
	cmpq	%rdi, %rsi
	jne	.L296
	movq	%r12, %rdi
	movq	%r8, -152(%rbp)
	addq	$1, %r15
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	16(%r12), %rsi
	movq	-152(%rbp), %r8
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L282:
	movq	%r15, %rcx
.L250:
	cmpq	%rdi, %rsi
	je	.L255
	movq	-120(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L256:
	movq	8(%r8), %rdx
	movq	16(%r8), %rax
	movq	%rcx, %r15
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rcx
	jb	.L257
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L297:
	movq	(%rdx), %rax
	addq	$1, %r15
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L286:
	movq	8(%r8), %rdx
	movq	16(%r8), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r15, %rax
	jbe	.L260
.L257:
	movq	16(%r12), %rsi
	leaq	(%rdx,%r15,8), %rdx
	cmpq	24(%r12), %rsi
	jne	.L297
	movq	%r12, %rdi
	movq	%r8, -152(%rbp)
	addq	$1, %r15
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-152(%rbp), %r8
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L260:
	orq	$2, %r12
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L239:
	movq	%rax, %rdi
	leaq	-120(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, -152(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-152(%rbp), %rax
	leaq	-112(%rbp), %rsi
	movq	%r12, -112(%rbp)
	movq	%rax, %rdi
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE12emplace_backIJS0_EEEvDpOT_
	movq	-152(%rbp), %rax
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L290:
	movl	%eax, %r15d
.L233:
	movq	(%r14), %rax
	movq	-160(%rbp), %rbx
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	%r15d, %esi
	movq	376(%rax), %rdi
	movq	(%rbx), %rbx
	movq	(%rax), %r14
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9CheckMapsENS_4base5FlagsINS1_13CheckMapsFlagEiEENS0_13ZoneHandleSetINS0_3MapEEERKNS1_14FeedbackSourceE@PLT
	movq	%rbx, %xmm1
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-168(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movq	-176(%rbp), %rax
	movl	$3, %edx
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-160(%rbp), %rbx
	movq	%rax, (%rbx)
.L224:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L298
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	movq	16(%r15), %rsi
	movq	24(%r15), %rdx
	leaq	15(%rcx), %r10
	andq	$-8, %r10
	subq	%rsi, %rdx
	cmpq	%rdx, %r10
	ja	.L299
	addq	%rsi, %r10
	movq	%r10, 16(%r15)
.L249:
	leaq	8(%rsi,%rcx), %rdi
	movq	%rsi, 8(%r12)
	movq	%rsi, 16(%r12)
	movq	%rdi, 24(%r12)
	movq	8(%r8), %rdx
	cmpq	%rdx, 16(%r8)
	jne	.L247
	xorl	%ecx, %ecx
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L225:
	movq	8(%rdi), %rsi
	leaq	-96(%rbp), %r13
	movq	48(%rax), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsHeapObjectEv@PLT
	testb	%al, %al
	je	.L300
	movq	%r13, %rdi
	leaq	-112(%rbp), %r15
	call	_ZNK2v88internal8compiler13HeapObjectRef3mapEv@PLT
	movq	%r15, %rdi
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZNK2v88internal8compiler6MapRef9is_stableEv@PLT
	testb	%al, %al
	je	.L228
	movq	8(%r12), %rbx
	movq	16(%r12), %r8
	cmpq	%r8, %rbx
	je	.L265
	movq	%r12, -136(%rbp)
	movq	%r8, %r12
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L230:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	je	.L301
.L232:
	movq	8(%r14), %rsi
	movq	(%rbx), %rdx
	movq	%r13, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler6MapRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef6equalsERKS2_@PLT
	testb	%al, %al
	je	.L230
	movq	16(%r14), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler23CompilationDependencies17DependOnStableMapERKNS1_6MapRefE@PLT
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L255:
	leaq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r8, -184(%rbp)
	movq	%rcx, -152(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-184(%rbp), %r8
	movq	-152(%rbp), %rcx
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L228:
	movq	16(%r12), %rax
	movq	8(%r12), %r8
	movq	%rax, -144(%rbp)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L289:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L240:
	movq	%rax, %rdi
	leaq	-120(%rbp), %rdx
	movq	%rax, -152(%rbp)
	call	_ZNSt6vectorIPmN2v88internal13ZoneAllocatorIS0_EEE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S5_EEDpOT_
	movq	-152(%rbp), %rax
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L293:
	movl	$32, %esi
	movq	%r15, %rdi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-152(%rbp), %r8
	movq	%rax, %r12
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L266:
	movl	$1, %r12d
	xorl	%r15d, %r15d
	jmp	.L233
.L301:
	movq	-136(%rbp), %r12
	movq	16(%r12), %rax
	movq	8(%r12), %r8
	movq	%rax, -144(%rbp)
	jmp	.L226
.L299:
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	%rcx, -184(%rbp)
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-152(%rbp), %r8
	movq	-184(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L249
.L300:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L265:
	movq	%r8, -144(%rbp)
	jmp	.L226
.L294:
	leaq	.LC7(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20171:
	.size	_ZN2v88internal8compiler21PropertyAccessBuilder14BuildCheckMapsEPNS1_4NodeEPS4_S4_RKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE, .-_ZN2v88internal8compiler21PropertyAccessBuilder14BuildCheckMapsEPNS1_4NodeEPS4_S4_RKNS0_10ZoneVectorINS0_6HandleINS0_3MapEEEEE
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal8compiler21PropertyAccessBuilder5graphEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal8compiler21PropertyAccessBuilder5graphEv, @function
_GLOBAL__sub_I__ZNK2v88internal8compiler21PropertyAccessBuilder5graphEv:
.LFB24230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24230:
	.size	_GLOBAL__sub_I__ZNK2v88internal8compiler21PropertyAccessBuilder5graphEv, .-_GLOBAL__sub_I__ZNK2v88internal8compiler21PropertyAccessBuilder5graphEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal8compiler21PropertyAccessBuilder5graphEv
	.section	.rodata.CSWTCH.142,"a"
	.type	CSWTCH.142, @object
	.size	CSWTCH.142, 4
CSWTCH.142:
	.byte	6
	.byte	13
	.byte	7
	.byte	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
