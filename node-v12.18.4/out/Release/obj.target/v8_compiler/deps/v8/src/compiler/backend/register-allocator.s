	.file	"register-allocator.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1554:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1554:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler17TopLevelLiveRangeESt4pairIKS4_ZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS7_17RangeWithRegisterENSB_4HashENSB_6EqualsEEEE4VoteESt10_Select1stISH_EZNS7_32ComputeStateFromManyPredecessorsES9_SF_E27TopLevelLiveRangeComparatorNS1_13ZoneAllocatorISH_EEE8_M_eraseEPSt13_Rb_tree_nodeISH_E,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler17TopLevelLiveRangeESt4pairIKS4_ZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS7_17RangeWithRegisterENSB_4HashENSB_6EqualsEEEE4VoteESt10_Select1stISH_EZNS7_32ComputeStateFromManyPredecessorsES9_SF_E27TopLevelLiveRangeComparatorNS1_13ZoneAllocatorISH_EEE8_M_eraseEPSt13_Rb_tree_nodeISH_E, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler17TopLevelLiveRangeESt4pairIKS4_ZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS7_17RangeWithRegisterENSB_4HashENSB_6EqualsEEEE4VoteESt10_Select1stISH_EZNS7_32ComputeStateFromManyPredecessorsES9_SF_E27TopLevelLiveRangeComparatorNS1_13ZoneAllocatorISH_EEE8_M_eraseEPSt13_Rb_tree_nodeISH_E:
.LFB27756:
	.cfi_startproc
	testq	%rsi, %rsi
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L5:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler17TopLevelLiveRangeESt4pairIKS4_ZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS7_17RangeWithRegisterENSB_4HashENSB_6EqualsEEEE4VoteESt10_Select1stISH_EZNS7_32ComputeStateFromManyPredecessorsES9_SF_E27TopLevelLiveRangeComparatorNS1_13ZoneAllocatorISH_EEE8_M_eraseEPSt13_Rb_tree_nodeISH_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L5
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE27756:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler17TopLevelLiveRangeESt4pairIKS4_ZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS7_17RangeWithRegisterENSB_4HashENSB_6EqualsEEEE4VoteESt10_Select1stISH_EZNS7_32ComputeStateFromManyPredecessorsES9_SF_E27TopLevelLiveRangeComparatorNS1_13ZoneAllocatorISH_EEE8_M_eraseEPSt13_Rb_tree_nodeISH_E, .-_ZNSt8_Rb_treeIPN2v88internal8compiler17TopLevelLiveRangeESt4pairIKS4_ZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS7_17RangeWithRegisterENSB_4HashENSB_6EqualsEEEE4VoteESt10_Select1stISH_EZNS7_32ComputeStateFromManyPredecessorsES9_SF_E27TopLevelLiveRangeComparatorNS1_13ZoneAllocatorISH_EEE8_M_eraseEPSt13_Rb_tree_nodeISH_E
	.section	.text._ZNSt17_Function_handlerIFbPN2v88internal8compiler17TopLevelLiveRangeEEZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS6_17RangeWithRegisterENSA_4HashENSA_6EqualsEEEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFbPN2v88internal8compiler17TopLevelLiveRangeEEZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS6_17RangeWithRegisterENSA_4HashENSA_6EqualsEEEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFbPN2v88internal8compiler17TopLevelLiveRangeEEZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS6_17RangeWithRegisterENSA_4HashENSA_6EqualsEEEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB27790:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movl	88(%rax), %eax
	shrl	$31, %eax
	ret
	.cfi_endproc
.LFE27790:
	.size	_ZNSt17_Function_handlerIFbPN2v88internal8compiler17TopLevelLiveRangeEEZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS6_17RangeWithRegisterENSA_4HashENSA_6EqualsEEEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFbPN2v88internal8compiler17TopLevelLiveRangeEEZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS6_17RangeWithRegisterENSA_4HashENSA_6EqualsEEEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS3_16InstructionBlockEPNS2_16ZoneUnorderedSetINS4_17RangeWithRegisterENS8_4HashENS8_6EqualsEEEEUlPNS3_17TopLevelLiveRangeEE0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS3_16InstructionBlockEPNS2_16ZoneUnorderedSetINS4_17RangeWithRegisterENS8_4HashENS8_6EqualsEEEEUlPNS3_17TopLevelLiveRangeEE0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS3_16InstructionBlockEPNS2_16ZoneUnorderedSetINS4_17RangeWithRegisterENS8_4HashENS8_6EqualsEEEEUlPNS3_17TopLevelLiveRangeEE0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation:
.LFB27791:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L17
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27791:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS3_16InstructionBlockEPNS2_16ZoneUnorderedSetINS4_17RangeWithRegisterENS8_4HashENS8_6EqualsEEEEUlPNS3_17TopLevelLiveRangeEE0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS3_16InstructionBlockEPNS2_16ZoneUnorderedSetINS4_17RangeWithRegisterENS8_4HashENS8_6EqualsEEEEUlPNS3_17TopLevelLiveRangeEE0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFbPN2v88internal8compiler17TopLevelLiveRangeEEZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS6_17RangeWithRegisterENSA_4HashENSA_6EqualsEEEEUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFbPN2v88internal8compiler17TopLevelLiveRangeEEZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS6_17RangeWithRegisterENSA_4HashENSA_6EqualsEEEEUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFbPN2v88internal8compiler17TopLevelLiveRangeEEZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS6_17RangeWithRegisterENSA_4HashENSA_6EqualsEEEEUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB27795:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movl	88(%rax), %eax
	notl	%eax
	shrl	$31, %eax
	ret
	.cfi_endproc
.LFE27795:
	.size	_ZNSt17_Function_handlerIFbPN2v88internal8compiler17TopLevelLiveRangeEEZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS6_17RangeWithRegisterENSA_4HashENSA_6EqualsEEEEUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFbPN2v88internal8compiler17TopLevelLiveRangeEEZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS6_17RangeWithRegisterENSA_4HashENSA_6EqualsEEEEUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS3_16InstructionBlockEPNS2_16ZoneUnorderedSetINS4_17RangeWithRegisterENS8_4HashENS8_6EqualsEEEEUlPNS3_17TopLevelLiveRangeEE1_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS3_16InstructionBlockEPNS2_16ZoneUnorderedSetINS4_17RangeWithRegisterENS8_4HashENS8_6EqualsEEEEUlPNS3_17TopLevelLiveRangeEE1_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS3_16InstructionBlockEPNS2_16ZoneUnorderedSetINS4_17RangeWithRegisterENS8_4HashENS8_6EqualsEEEEUlPNS3_17TopLevelLiveRangeEE1_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation:
.LFB27796:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L21
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27796:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS3_16InstructionBlockEPNS2_16ZoneUnorderedSetINS4_17RangeWithRegisterENS8_4HashENS8_6EqualsEEEEUlPNS3_17TopLevelLiveRangeEE1_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS3_16InstructionBlockEPNS2_16ZoneUnorderedSetINS4_17RangeWithRegisterENS8_4HashENS8_6EqualsEEEEUlPNS3_17TopLevelLiveRangeEE1_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler9LiveRangeEEZZNS2_19LinearScanAllocator25UpdateDeferredFixedRangesENS2_22RegisterAllocationData9SpillModeEPNS2_16InstructionBlockEENKUlS4_E_clES4_EUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler9LiveRangeEEZZNS2_19LinearScanAllocator25UpdateDeferredFixedRangesENS2_22RegisterAllocationData9SpillModeEPNS2_16InstructionBlockEENKUlS4_E_clES4_EUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler9LiveRangeEEZZNS2_19LinearScanAllocator25UpdateDeferredFixedRangesENS2_22RegisterAllocationData9SpillModeEPNS2_16InstructionBlockEENKUlS4_E_clES4_EUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB27799:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	(%rdi), %rdx
	movq	8(%rax), %rcx
	movl	160(%rdx), %eax
	cmpl	%eax, 4(%rcx)
	cmovle	4(%rcx), %eax
	movl	%eax, 160(%rdx)
	ret
	.cfi_endproc
.LFE27799:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler9LiveRangeEEZZNS2_19LinearScanAllocator25UpdateDeferredFixedRangesENS2_22RegisterAllocationData9SpillModeEPNS2_16InstructionBlockEENKUlS4_E_clES4_EUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler9LiveRangeEEZZNS2_19LinearScanAllocator25UpdateDeferredFixedRangesENS2_22RegisterAllocationData9SpillModeEPNS2_16InstructionBlockEENKUlS4_E_clES4_EUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E0_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E0_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E0_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB27800:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L24
	cmpl	$3, %edx
	je	.L25
	cmpl	$1, %edx
	je	.L29
.L25:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27800:
	.size	_ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E0_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E0_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler9LiveRangeEEZZNS2_19LinearScanAllocator25UpdateDeferredFixedRangesENS2_22RegisterAllocationData9SpillModeEPNS2_16InstructionBlockEENKUlS4_E_clES4_EUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler9LiveRangeEEZZNS2_19LinearScanAllocator25UpdateDeferredFixedRangesENS2_22RegisterAllocationData9SpillModeEPNS2_16InstructionBlockEENKUlS4_E_clES4_EUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler9LiveRangeEEZZNS2_19LinearScanAllocator25UpdateDeferredFixedRangesENS2_22RegisterAllocationData9SpillModeEPNS2_16InstructionBlockEENKUlS4_E_clES4_EUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB27804:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	(%rdi), %rdx
	movq	8(%rax), %rcx
	movl	164(%rdx), %eax
	cmpl	%eax, 4(%rcx)
	cmovle	4(%rcx), %eax
	movl	%eax, 164(%rdx)
	ret
	.cfi_endproc
.LFE27804:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler9LiveRangeEEZZNS2_19LinearScanAllocator25UpdateDeferredFixedRangesENS2_22RegisterAllocationData9SpillModeEPNS2_16InstructionBlockEENKUlS4_E_clES4_EUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler9LiveRangeEEZZNS2_19LinearScanAllocator25UpdateDeferredFixedRangesENS2_22RegisterAllocationData9SpillModeEPNS2_16InstructionBlockEENKUlS4_E_clES4_EUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E1_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E1_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E1_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB27805:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L32
	cmpl	$3, %edx
	je	.L33
	cmpl	$1, %edx
	je	.L37
.L33:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27805:
	.size	_ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E1_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E1_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB31199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE31199:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB32004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE32004:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB31200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE31200:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB32005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE32005:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_113PrintBlockRowERSoRKNS0_10ZoneVectorIPNS1_16InstructionBlockEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"(deferred)"
.LC1:
	.string	""
.LC2:
	.string	"     "
.LC3:
	.string	"[-B%d-%s"
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_113PrintBlockRowERSoRKNS0_10ZoneVectorIPNS1_16InstructionBlockEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_113PrintBlockRowERSoRKNS0_10ZoneVectorIPNS1_16InstructionBlockEEE, @function
_ZN2v88internal8compiler12_GLOBAL__N_113PrintBlockRowERSoRKNS0_10ZoneVectorIPNS1_16InstructionBlockEEE:
.LFB23319:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-97(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC2(%rip), %rsi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%rbx), %rsi
	movq	8(%rbx), %rax
	movq	%rsi, -136(%rbp)
	cmpq	%rsi, %rax
	je	.L47
	movq	%rax, -120(%rbp)
	leaq	-96(%rbp), %r14
	leaq	-97(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L53:
	movq	-120(%rbp), %rax
	movl	$32, %ebx
	movq	%r14, %rdi
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %r8
	movl	$32, %ecx
	movq	(%rax), %rax
	movl	116(%rax), %edx
	subl	112(%rax), %edx
	movl	100(%rax), %r9d
	cmpb	$0, 120(%rax)
	leal	0(,%rdx,4), %r15d
	leaq	.LC1(%rip), %rax
	cmovne	%rsi, %rax
	cmpl	$32, %r15d
	movl	$1, %edx
	cmovle	%r15d, %ebx
	subq	$8, %rsp
	pushq	%rax
	xorl	%eax, %eax
	movslq	%ebx, %rsi
	call	__snprintf_chk@PLT
	movq	%r14, %rdx
	movl	%eax, %r8d
.L49:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L49
	movl	%eax, %ecx
	movq	%r14, %rsi
	movl	%r8d, -124(%rbp)
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %edi
	addb	%al, %dil
	movq	%r12, %rdi
	sbbq	$3, %rdx
	subq	%r14, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-124(%rbp), %r8d
	popq	%rax
	popq	%rdx
	cmpl	%ebx, %r8d
	cmovle	%r8d, %ebx
	subl	%ebx, %r15d
	movl	%r15d, %ebx
	cmpl	$1, %r15d
	jle	.L51
	subl	$1, %ebx
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	addl	$1, %r15d
	movb	$45, -97(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, %r15d
	jne	.L52
.L51:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$93, -97(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, -120(%rbp)
	movq	-120(%rbp), %rax
	cmpq	%rax, -136(%rbp)
	jne	.L53
.L47:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$10, -97(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L63:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23319:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_113PrintBlockRowERSoRKNS0_10ZoneVectorIPNS1_16InstructionBlockEEE, .-_ZN2v88internal8compiler12_GLOBAL__N_113PrintBlockRowERSoRKNS0_10ZoneVectorIPNS1_16InstructionBlockEEE
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler17TopLevelLiveRangeESt4pairIKS4_ZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS7_17RangeWithRegisterENSB_4HashENSB_6EqualsEEEE4VoteESt10_Select1stISH_EZNS7_32ComputeStateFromManyPredecessorsES9_SF_E27TopLevelLiveRangeComparatorNS1_13ZoneAllocatorISH_EEE7_M_copyINSN_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISH_EPKSR_PSt18_Rb_tree_node_baseRT_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler17TopLevelLiveRangeESt4pairIKS4_ZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS7_17RangeWithRegisterENSB_4HashENSB_6EqualsEEEE4VoteESt10_Select1stISH_EZNS7_32ComputeStateFromManyPredecessorsES9_SF_E27TopLevelLiveRangeComparatorNS1_13ZoneAllocatorISH_EEE7_M_copyINSN_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISH_EPKSR_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler17TopLevelLiveRangeESt4pairIKS4_ZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS7_17RangeWithRegisterENSB_4HashENSB_6EqualsEEEE4VoteESt10_Select1stISH_EZNS7_32ComputeStateFromManyPredecessorsES9_SF_E27TopLevelLiveRangeComparatorNS1_13ZoneAllocatorISH_EEE7_M_copyINSN_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISH_EPKSR_PSt18_Rb_tree_node_baseRT_:
.LFB29499:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rcx), %rax
	movq	%rdi, -56(%rbp)
	movq	(%rax), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$175, %rax
	jbe	.L87
	leaq	176(%r13), %rax
	movq	%rax, 16(%rdi)
.L66:
	movdqu	32(%rbx), %xmm1
	movups	%xmm1, 32(%r13)
	movdqu	48(%rbx), %xmm2
	movups	%xmm2, 48(%r13)
	movdqu	64(%rbx), %xmm3
	movups	%xmm3, 64(%r13)
	movdqu	80(%rbx), %xmm4
	movups	%xmm4, 80(%r13)
	movdqu	96(%rbx), %xmm5
	movups	%xmm5, 96(%r13)
	movdqu	112(%rbx), %xmm6
	movups	%xmm6, 112(%r13)
	movdqu	128(%rbx), %xmm7
	movups	%xmm7, 128(%r13)
	movdqu	144(%rbx), %xmm1
	movups	%xmm1, 144(%r13)
	movdqu	160(%rbx), %xmm2
	movups	%xmm2, 160(%r13)
	movl	(%rbx), %eax
	movq	$0, 16(%r13)
	movl	%eax, 0(%r13)
	movq	$0, 24(%r13)
	movq	%rdx, 8(%r13)
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L67
	movq	-56(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler17TopLevelLiveRangeESt4pairIKS4_ZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS7_17RangeWithRegisterENSB_4HashENSB_6EqualsEEEE4VoteESt10_Select1stISH_EZNS7_32ComputeStateFromManyPredecessorsES9_SF_E27TopLevelLiveRangeComparatorNS1_13ZoneAllocatorISH_EEE7_M_copyINSN_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISH_EPKSR_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r13)
.L67:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L64
	movq	%r13, %r15
.L73:
	movq	(%r14), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$175, %rax
	jbe	.L88
	leaq	176(%rbx), %rax
	movq	%rax, 16(%rdi)
.L70:
	movdqu	32(%r12), %xmm0
	movups	%xmm0, 32(%rbx)
	movdqu	48(%r12), %xmm1
	movups	%xmm1, 48(%rbx)
	movdqu	64(%r12), %xmm2
	movups	%xmm2, 64(%rbx)
	movdqu	80(%r12), %xmm3
	movups	%xmm3, 80(%rbx)
	movdqu	96(%r12), %xmm4
	movups	%xmm4, 96(%rbx)
	movdqu	112(%r12), %xmm5
	movups	%xmm5, 112(%rbx)
	movdqu	128(%r12), %xmm6
	movups	%xmm6, 128(%rbx)
	movdqu	144(%r12), %xmm7
	movups	%xmm7, 144(%rbx)
	movdqu	160(%r12), %xmm0
	movups	%xmm0, 160(%rbx)
	movl	(%r12), %eax
	movq	$0, 16(%rbx)
	movl	%eax, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbx, 16(%r15)
	movq	%r15, 8(%rbx)
	movq	24(%r12), %rsi
	testq	%rsi, %rsi
	je	.L71
	movq	-56(%rbp), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler17TopLevelLiveRangeESt4pairIKS4_ZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS7_17RangeWithRegisterENSB_4HashENSB_6EqualsEEEE4VoteESt10_Select1stISH_EZNS7_32ComputeStateFromManyPredecessorsES9_SF_E27TopLevelLiveRangeComparatorNS1_13ZoneAllocatorISH_EEE7_M_copyINSN_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISH_EPKSR_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%rbx)
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L64
.L72:
	movq	%rbx, %r15
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L71:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L72
.L64:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movl	$176, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$176, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L66
	.cfi_endproc
.LFE29499:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler17TopLevelLiveRangeESt4pairIKS4_ZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS7_17RangeWithRegisterENSB_4HashENSB_6EqualsEEEE4VoteESt10_Select1stISH_EZNS7_32ComputeStateFromManyPredecessorsES9_SF_E27TopLevelLiveRangeComparatorNS1_13ZoneAllocatorISH_EEE7_M_copyINSN_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISH_EPKSR_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler17TopLevelLiveRangeESt4pairIKS4_ZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS7_17RangeWithRegisterENSB_4HashENSB_6EqualsEEEE4VoteESt10_Select1stISH_EZNS7_32ComputeStateFromManyPredecessorsES9_SF_E27TopLevelLiveRangeComparatorNS1_13ZoneAllocatorISH_EEE7_M_copyINSN_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISH_EPKSR_PSt18_Rb_tree_node_baseRT_
	.section	.text._ZN2v88internal4Zone3NewEm,"axG",@progbits,_ZN2v88internal4Zone3NewEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4Zone3NewEm
	.type	_ZN2v88internal4Zone3NewEm, @function
_ZN2v88internal4Zone3NewEm:
.LFB5934:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	addq	$7, %rsi
	andq	$-8, %rsi
	subq	%r8, %rax
	cmpq	%rsi, %rax
	jb	.L91
	addq	%r8, %rsi
	movq	%r8, %rax
	movq	%rsi, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	jmp	_ZN2v88internal4Zone9NewExpandEm@PLT
	.cfi_endproc
.LFE5934:
	.size	_ZN2v88internal4Zone3NewEm, .-_ZN2v88internal4Zone3NewEm
	.section	.text._ZN2v88internal8compiler11UsePositionC2ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler11UsePositionC2ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE
	.type	_ZN2v88internal8compiler11UsePositionC2ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE, @function
_ZN2v88internal8compiler11UsePositionC2ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE:
.LFB23239:
	.cfi_startproc
	endbr64
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	$0, 16(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movl	%esi, 24(%rdi)
	movups	%xmm0, (%rdi)
	testq	%rdx, %rdx
	je	.L95
	movq	(%rdx), %rcx
	movl	$32, %eax
	xorl	%edx, %edx
	movl	%ecx, %esi
	andl	$7, %esi
	cmpl	$1, %esi
	je	.L102
.L93:
	movzbl	%r8b, %r8d
	orl	%edx, %eax
	sall	$2, %r8d
	orl	%r8d, %eax
	orb	$8, %ah
	movl	%eax, 28(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%rcx, %rdx
	shrq	$35, %rdx
	andl	$1, %edx
	je	.L94
	shrq	$36, %rcx
	andl	$7, %ecx
	cmpl	$5, %ecx
	je	.L98
	cmpl	$6, %ecx
	je	.L99
	xorl	%eax, %eax
	cmpl	$2, %ecx
	je	.L93
	xorl	%eax, %eax
	cmpl	$1, %ecx
	setne	%al
	sall	$5, %eax
.L94:
	xorl	%edx, %edx
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%edx, %edx
	movl	$32, %eax
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L98:
	movl	$2, %edx
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$3, %edx
	xorl	%eax, %eax
	jmp	.L93
	.cfi_endproc
.LFE23239:
	.size	_ZN2v88internal8compiler11UsePositionC2ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE, .-_ZN2v88internal8compiler11UsePositionC2ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE
	.globl	_ZN2v88internal8compiler11UsePositionC1ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE
	.set	_ZN2v88internal8compiler11UsePositionC1ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE,_ZN2v88internal8compiler11UsePositionC2ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE
	.section	.rodata._ZNK2v88internal8compiler11UsePosition7HasHintEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal8compiler11UsePosition7HasHintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler11UsePosition7HasHintEv
	.type	_ZNK2v88internal8compiler11UsePosition7HasHintEv, @function
_ZNK2v88internal8compiler11UsePosition7HasHintEv:
.LFB23241:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L111
	movl	28(%rdi), %eax
	shrl	$2, %eax
	andl	$7, %eax
	cmpb	$4, %al
	ja	.L105
	leaq	.L106(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler11UsePosition7HasHintEv,"a",@progbits
	.align 4
	.align 4
.L106:
	.long	.L111-.L106
	.long	.L109-.L106
	.long	.L108-.L106
	.long	.L107-.L106
	.long	.L111-.L106
	.section	.text._ZNK2v88internal8compiler11UsePosition7HasHintEv
	.p2align 4,,10
	.p2align 3
.L111:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	movl	28(%rcx), %eax
	shrl	$6, %eax
	andl	$63, %eax
	cmpl	$32, %eax
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	cmpl	$32, 48(%rcx)
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	movl	$1, %eax
	ret
.L105:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23241:
	.size	_ZNK2v88internal8compiler11UsePosition7HasHintEv, .-_ZNK2v88internal8compiler11UsePosition7HasHintEv
	.section	.text._ZNK2v88internal8compiler11UsePosition12HintRegisterEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler11UsePosition12HintRegisterEPi
	.type	_ZNK2v88internal8compiler11UsePosition12HintRegisterEPi, @function
_ZNK2v88internal8compiler11UsePosition12HintRegisterEPi:
.LFB23242:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L122
	movl	28(%rdi), %eax
	shrl	$2, %eax
	andl	$7, %eax
	cmpb	$4, %al
	ja	.L116
	leaq	.L117(%rip), %rcx
	movzbl	%al, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler11UsePosition12HintRegisterEPi,"a",@progbits
	.align 4
	.align 4
.L117:
	.long	.L122-.L117
	.long	.L120-.L117
	.long	.L119-.L117
	.long	.L118-.L117
	.long	.L122-.L117
	.section	.text._ZNK2v88internal8compiler11UsePosition12HintRegisterEPi
	.p2align 4,,10
	.p2align 3
.L122:
	xorl	%eax, %eax
.L114:
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	movl	28(%rdx), %edx
	xorl	%eax, %eax
	shrl	$6, %edx
	andl	$63, %edx
	cmpl	$32, %edx
	je	.L114
	movl	%edx, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	movq	(%rdx), %rax
	sarq	$35, %rax
	movl	%eax, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	movl	48(%rdx), %edx
	xorl	%eax, %eax
	cmpl	$32, %edx
	je	.L114
	movl	%edx, (%rsi)
	movl	$1, %eax
	ret
.L116:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23242:
	.size	_ZNK2v88internal8compiler11UsePosition12HintRegisterEPi, .-_ZNK2v88internal8compiler11UsePosition12HintRegisterEPi
	.section	.text._ZN2v88internal8compiler11UsePosition18HintTypeForOperandERKNS1_18InstructionOperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler11UsePosition18HintTypeForOperandERKNS1_18InstructionOperandE
	.type	_ZN2v88internal8compiler11UsePosition18HintTypeForOperandERKNS1_18InstructionOperandE, @function
_ZN2v88internal8compiler11UsePosition18HintTypeForOperandERKNS1_18InstructionOperandE:
.LFB23243:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	%edx, %eax
	andl	$7, %eax
	cmpl	$4, %eax
	ja	.L131
	andl	$6, %edx
	jne	.L134
	cmpl	$1, %eax
	jne	.L133
	movl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	cmpl	$5, %eax
	jne	.L133
	andl	$24, %edx
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	xorl	%eax, %eax
	ret
.L133:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23243:
	.size	_ZN2v88internal8compiler11UsePosition18HintTypeForOperandERKNS1_18InstructionOperandE, .-_ZN2v88internal8compiler11UsePosition18HintTypeForOperandERKNS1_18InstructionOperandE
	.section	.text._ZN2v88internal8compiler11UsePosition7SetHintEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler11UsePosition7SetHintEPS2_
	.type	_ZN2v88internal8compiler11UsePosition7SetHintEPS2_, @function
_ZN2v88internal8compiler11UsePosition7SetHintEPS2_:
.LFB23244:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %eax
	movq	%rsi, 8(%rdi)
	andl	$-29, %eax
	orl	$8, %eax
	movl	%eax, 28(%rdi)
	ret
	.cfi_endproc
.LFE23244:
	.size	_ZN2v88internal8compiler11UsePosition7SetHintEPS2_, .-_ZN2v88internal8compiler11UsePosition7SetHintEPS2_
	.section	.text._ZN2v88internal8compiler11UsePosition11ResolveHintEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler11UsePosition11ResolveHintEPS2_
	.type	_ZN2v88internal8compiler11UsePosition11ResolveHintEPS2_, @function
_ZN2v88internal8compiler11UsePosition11ResolveHintEPS2_:
.LFB23245:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %eax
	movl	%eax, %edx
	shrl	$2, %edx
	andl	$7, %edx
	cmpb	$4, %dl
	jne	.L139
	andl	$-29, %eax
	movq	%rsi, 8(%rdi)
	orl	$8, %eax
	movl	%eax, 28(%rdi)
.L139:
	ret
	.cfi_endproc
.LFE23245:
	.size	_ZN2v88internal8compiler11UsePosition11ResolveHintEPS2_, .-_ZN2v88internal8compiler11UsePosition11ResolveHintEPS2_
	.section	.text._ZN2v88internal8compiler11UsePosition8set_typeENS1_15UsePositionTypeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler11UsePosition8set_typeENS1_15UsePositionTypeEb
	.type	_ZN2v88internal8compiler11UsePosition8set_typeENS1_15UsePositionTypeEb, @function
_ZN2v88internal8compiler11UsePosition8set_typeENS1_15UsePositionTypeEb:
.LFB23246:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %eax
	movzbl	%dl, %edx
	movzbl	%sil, %esi
	sall	$5, %edx
	andl	$28, %eax
	orl	%esi, %edx
	orl	%edx, %eax
	orb	$8, %ah
	movl	%eax, 28(%rdi)
	ret
	.cfi_endproc
.LFE23246:
	.size	_ZN2v88internal8compiler11UsePosition8set_typeENS1_15UsePositionTypeEb, .-_ZN2v88internal8compiler11UsePosition8set_typeENS1_15UsePositionTypeEb
	.section	.text._ZN2v88internal8compiler11UseInterval7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler11UseInterval7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler11UseInterval7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE, @function
_ZN2v88internal8compiler11UseInterval7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE:
.LFB23249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$8, %rsp
	movq	16(%rdx), %rax
	movq	24(%rdx), %rdx
	movl	4(%rbx), %r13d
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L146
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L144:
	movq	$0, 8(%rax)
	movl	%r12d, (%rax)
	movl	%r13d, 4(%rax)
	movq	8(%rbx), %rdx
	movq	%rdx, 8(%rax)
	movl	%r12d, 4(%rbx)
	movq	$0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L144
	.cfi_endproc
.LFE23249:
	.size	_ZN2v88internal8compiler11UseInterval7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE, .-_ZN2v88internal8compiler11UseInterval7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compilerlsERSoNS1_16LifetimePositionE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoNS1_16LifetimePositionE
	.type	_ZN2v88internal8compilerlsERSoNS1_16LifetimePositionE, @function
_ZN2v88internal8compilerlsERSoNS1_16LifetimePositionE:
.LFB23251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-41(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	movq	%r13, %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$64, -41(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testl	%ebx, %ebx
	leal	3(%rbx), %esi
	cmovns	%ebx, %esi
	movq	%rax, %rdi
	sarl	$2, %esi
	call	_ZNSolsEi@PLT
	testb	$2, %bl
	jne	.L148
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$103, -41(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	andl	$1, %ebx
	jne	.L150
.L155:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$115, -41(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L151:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$105, -41(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	andl	$1, %ebx
	je	.L155
.L150:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$101, -41(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L151
.L154:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23251:
	.size	_ZN2v88internal8compilerlsERSoNS1_16LifetimePositionE, .-_ZN2v88internal8compilerlsERSoNS1_16LifetimePositionE
	.section	.text._ZNK2v88internal8compiler16LifetimePosition5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16LifetimePosition5PrintEv
	.type	_ZNK2v88internal8compiler16LifetimePosition5PrintEv, @function
_ZNK2v88internal8compiler16LifetimePosition5PrintEv:
.LFB23250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-304(%rbp), %r14
	leaq	-384(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	subq	$352, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%ax, -80(%rbp)
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	movq	%rbx, -304(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	movl	(%r12), %esi
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -384(%rbp)
	addq	$40, %rax
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compilerlsERSoNS1_16LifetimePositionE
	movq	-384(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-144(%rbp,%rax), %r12
	testq	%r12, %r12
	je	.L163
	cmpb	$0, 56(%r12)
	je	.L158
	movsbl	67(%r12), %esi
.L159:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-320(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rbx, -304(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	addq	$352, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L159
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L159
.L164:
	call	__stack_chk_fail@PLT
.L163:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE23250:
	.size	_ZNK2v88internal8compiler16LifetimePosition5PrintEv, .-_ZNK2v88internal8compiler16LifetimePosition5PrintEv
	.section	.text._ZN2v88internal8compiler9LiveRangeC2EiNS0_21MachineRepresentationEPNS1_17TopLevelLiveRangeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9LiveRangeC2EiNS0_21MachineRepresentationEPNS1_17TopLevelLiveRangeE
	.type	_ZN2v88internal8compiler9LiveRangeC2EiNS0_21MachineRepresentationEPNS1_17TopLevelLiveRangeE, @function
_ZN2v88internal8compiler9LiveRangeC2EiNS0_21MachineRepresentationEPNS1_17TopLevelLiveRangeE:
.LFB23256:
	.cfi_startproc
	endbr64
	movzbl	%dl, %edx
	pxor	%xmm0, %xmm0
	movl	%esi, (%rdi)
	sall	$13, %edx
	movq	$0, 24(%rdi)
	orl	$134221824, %edx
	movq	%rcx, 32(%rdi)
	movl	%edx, 4(%rdi)
	movups	%xmm0, 8(%rdi)
	movups	%xmm0, 40(%rdi)
	movups	%xmm0, 56(%rdi)
	movups	%xmm0, 72(%rdi)
	ret
	.cfi_endproc
.LFE23256:
	.size	_ZN2v88internal8compiler9LiveRangeC2EiNS0_21MachineRepresentationEPNS1_17TopLevelLiveRangeE, .-_ZN2v88internal8compiler9LiveRangeC2EiNS0_21MachineRepresentationEPNS1_17TopLevelLiveRangeE
	.globl	_ZN2v88internal8compiler9LiveRangeC1EiNS0_21MachineRepresentationEPNS1_17TopLevelLiveRangeE
	.set	_ZN2v88internal8compiler9LiveRangeC1EiNS0_21MachineRepresentationEPNS1_17TopLevelLiveRangeE,_ZN2v88internal8compiler9LiveRangeC2EiNS0_21MachineRepresentationEPNS1_17TopLevelLiveRangeE
	.section	.rodata._ZNK2v88internal8compiler9LiveRange15VerifyPositionsEv.str1.1,"aMS",@progbits,1
.LC5:
	.string	"Start() <= pos->pos()"
.LC6:
	.string	"Check failed: %s."
.LC7:
	.string	"pos->pos() <= End()"
.LC8:
	.string	"(interval) != nullptr"
	.section	.text._ZNK2v88internal8compiler9LiveRange15VerifyPositionsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange15VerifyPositionsEv
	.type	_ZNK2v88internal8compiler9LiveRange15VerifyPositionsEv, @function
_ZNK2v88internal8compiler9LiveRange15VerifyPositionsEv:
.LFB23258:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %r8
	movq	16(%rdi), %rax
	testq	%r8, %r8
	je	.L183
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rax), %r9d
	movl	%r9d, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
.L173:
	movl	24(%r8), %edx
	cmpl	%edx, %r9d
	jg	.L186
	movq	8(%rdi), %rcx
	cmpl	4(%rcx), %edx
	jg	.L187
.L169:
	movl	4(%rax), %ecx
	cmpl	%edx, %esi
	jg	.L170
	cmpl	%edx, %ecx
	jle	.L170
.L171:
	movq	16(%r8), %r8
	testq	%r8, %r8
	jne	.L173
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	cmpl	%edx, %ecx
	je	.L171
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L174
	movl	(%rax), %esi
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L174:
	leaq	.LC8(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L186:
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	.LC7(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L183:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE23258:
	.size	_ZNK2v88internal8compiler9LiveRange15VerifyPositionsEv, .-_ZNK2v88internal8compiler9LiveRange15VerifyPositionsEv
	.section	.text._ZNK2v88internal8compiler9LiveRange15VerifyIntervalsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange15VerifyIntervalsEv
	.type	_ZNK2v88internal8compiler9LiveRange15VerifyIntervalsEv, @function
_ZNK2v88internal8compiler9LiveRange15VerifyIntervalsEv:
.LFB23259:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L195:
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L195
	ret
	.cfi_endproc
.LFE23259:
	.size	_ZNK2v88internal8compiler9LiveRange15VerifyIntervalsEv, .-_ZNK2v88internal8compiler9LiveRange15VerifyIntervalsEv
	.section	.text._ZN2v88internal8compiler9LiveRange21set_assigned_registerEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9LiveRange21set_assigned_registerEi
	.type	_ZN2v88internal8compiler9LiveRange21set_assigned_registerEi, @function
_ZN2v88internal8compiler9LiveRange21set_assigned_registerEi:
.LFB23260:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	sall	$7, %esi
	andl	$-8065, %eax
	orl	%esi, %eax
	movl	%eax, 4(%rdi)
	ret
	.cfi_endproc
.LFE23260:
	.size	_ZN2v88internal8compiler9LiveRange21set_assigned_registerEi, .-_ZN2v88internal8compiler9LiveRange21set_assigned_registerEi
	.section	.text._ZN2v88internal8compiler9LiveRange21UnsetAssignedRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9LiveRange21UnsetAssignedRegisterEv
	.type	_ZN2v88internal8compiler9LiveRange21UnsetAssignedRegisterEv, @function
_ZN2v88internal8compiler9LiveRange21UnsetAssignedRegisterEv:
.LFB23261:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	andl	$-8065, %eax
	orb	$16, %ah
	movl	%eax, 4(%rdi)
	ret
	.cfi_endproc
.LFE23261:
	.size	_ZN2v88internal8compiler9LiveRange21UnsetAssignedRegisterEv, .-_ZN2v88internal8compiler9LiveRange21UnsetAssignedRegisterEv
	.section	.text._ZN2v88internal8compiler9LiveRange12AttachToNextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9LiveRange12AttachToNextEv
	.type	_ZN2v88internal8compiler9LiveRange12AttachToNextEv, @function
_ZN2v88internal8compiler9LiveRange12AttachToNextEv:
.LFB23262:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdx
	movq	8(%rdi), %rax
	movq	16(%rdx), %rdx
	movq	%rdx, 8(%rax)
	movq	40(%rdi), %rax
	movq	$0, 16(%rax)
	movq	40(%rdi), %rax
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movq	$0, 8(%rax)
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L203
	.p2align 4,,10
	.p2align 3
.L199:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L199
	movq	40(%rdi), %rax
	movq	24(%rax), %rax
	movq	%rax, 16(%rdx)
	movq	40(%rdi), %rax
.L200:
	movq	$0, 24(%rax)
	movq	40(%rdi), %rax
	movq	40(%rax), %rdx
	movq	%rdx, 40(%rdi)
	movq	$0, 40(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	movq	40(%rdi), %rax
	movq	24(%rax), %rdx
	movq	%rdx, 24(%rdi)
	jmp	.L200
	.cfi_endproc
.LFE23262:
	.size	_ZN2v88internal8compiler9LiveRange12AttachToNextEv, .-_ZN2v88internal8compiler9LiveRange12AttachToNextEv
	.section	.text._ZN2v88internal8compiler9LiveRange7UnspillEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9LiveRange7UnspillEv
	.type	_ZN2v88internal8compiler9LiveRange7UnspillEv, @function
_ZN2v88internal8compiler9LiveRange7UnspillEv:
.LFB23263:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	andl	$-8066, %eax
	orb	$16, %ah
	movl	%eax, 4(%rdi)
	ret
	.cfi_endproc
.LFE23263:
	.size	_ZN2v88internal8compiler9LiveRange7UnspillEv, .-_ZN2v88internal8compiler9LiveRange7UnspillEv
	.section	.text._ZN2v88internal8compiler9LiveRange5SpillEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9LiveRange5SpillEv
	.type	_ZN2v88internal8compiler9LiveRange5SpillEv, @function
_ZN2v88internal8compiler9LiveRange5SpillEv:
.LFB23264:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	andl	$-8066, %eax
	orl	$4097, %eax
	movl	%eax, 4(%rdi)
	ret
	.cfi_endproc
.LFE23264:
	.size	_ZN2v88internal8compiler9LiveRange5SpillEv, .-_ZN2v88internal8compiler9LiveRange5SpillEv
	.section	.text._ZNK2v88internal8compiler9LiveRange4kindEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange4kindEv
	.type	_ZNK2v88internal8compiler9LiveRange4kindEv, @function
_ZNK2v88internal8compiler9LiveRange4kindEv:
.LFB23265:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	shrl	$13, %eax
	cmpb	$11, %al
	seta	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE23265:
	.size	_ZNK2v88internal8compiler9LiveRange4kindEv, .-_ZNK2v88internal8compiler9LiveRange4kindEv
	.section	.text._ZNK2v88internal8compiler9LiveRange17FirstHintPositionEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange17FirstHintPositionEPi
	.type	_ZNK2v88internal8compiler9LiveRange17FirstHintPositionEPi, @function
_ZNK2v88internal8compiler9LiveRange17FirstHintPositionEPi:
.LFB23266:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %r8
	testq	%r8, %r8
	je	.L207
	leaq	.L211(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L215:
	movq	8(%r8), %rdx
	testq	%rdx, %rdx
	je	.L209
	movl	28(%r8), %eax
	shrl	$2, %eax
	andl	$7, %eax
	cmpb	$4, %al
	ja	.L210
	movzbl	%al, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler9LiveRange17FirstHintPositionEPi,"a",@progbits
	.align 4
	.align 4
.L211:
	.long	.L209-.L211
	.long	.L214-.L211
	.long	.L213-.L211
	.long	.L212-.L211
	.long	.L209-.L211
	.section	.text._ZNK2v88internal8compiler9LiveRange17FirstHintPositionEPi
	.p2align 4,,10
	.p2align 3
.L213:
	movl	28(%rdx), %eax
	shrl	$6, %eax
	andl	$63, %eax
	cmpl	$32, %eax
	jne	.L231
	.p2align 4,,10
	.p2align 3
.L209:
	movq	16(%r8), %r8
	testq	%r8, %r8
	jne	.L215
.L233:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	movq	(%rdx), %rax
	sarq	$35, %rax
.L231:
	movl	%eax, (%rsi)
.L207:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	movl	48(%rdx), %eax
	cmpl	$32, %eax
	jne	.L231
	movq	16(%r8), %r8
	testq	%r8, %r8
	jne	.L215
	jmp	.L233
.L210:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23266:
	.size	_ZNK2v88internal8compiler9LiveRange17FirstHintPositionEPi, .-_ZNK2v88internal8compiler9LiveRange17FirstHintPositionEPi
	.section	.text._ZNK2v88internal8compiler9LiveRange15NextUsePositionENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange15NextUsePositionENS1_16LifetimePositionE
	.type	_ZNK2v88internal8compiler9LiveRange15NextUsePositionENS1_16LifetimePositionE, @function
_ZNK2v88internal8compiler9LiveRange15NextUsePositionENS1_16LifetimePositionE:
.LFB23267:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L235
	movl	24(%rax), %edx
	cmpl	%esi, %edx
	jle	.L239
.L235:
	movq	24(%rdi), %rax
	testq	%rax, %rax
	jne	.L250
.L237:
	movq	%rax, 56(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L237
.L250:
	movl	24(%rax), %edx
.L239:
	cmpl	%edx, %esi
	jg	.L238
	movq	%rax, 56(%rdi)
	ret
	.cfi_endproc
.LFE23267:
	.size	_ZNK2v88internal8compiler9LiveRange15NextUsePositionENS1_16LifetimePositionE, .-_ZNK2v88internal8compiler9LiveRange15NextUsePositionENS1_16LifetimePositionE
	.section	.text._ZNK2v88internal8compiler9LiveRange35NextUsePositionRegisterIsBeneficialENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange35NextUsePositionRegisterIsBeneficialENS1_16LifetimePositionE
	.type	_ZNK2v88internal8compiler9LiveRange35NextUsePositionRegisterIsBeneficialENS1_16LifetimePositionE, @function
_ZNK2v88internal8compiler9LiveRange35NextUsePositionRegisterIsBeneficialENS1_16LifetimePositionE:
.LFB23268:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L252
	movl	24(%rax), %edx
	cmpl	%edx, %esi
	jge	.L257
.L252:
	movq	24(%rdi), %rax
	testq	%rax, %rax
	jne	.L269
.L254:
	movq	$0, 56(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L254
.L269:
	movl	24(%rax), %edx
.L257:
	cmpl	%edx, %esi
	jg	.L270
	movq	%rax, 56(%rdi)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L272:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L271
.L258:
	testb	$32, 28(%rax)
	je	.L272
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	ret
	.cfi_endproc
.LFE23268:
	.size	_ZNK2v88internal8compiler9LiveRange35NextUsePositionRegisterIsBeneficialENS1_16LifetimePositionE, .-_ZNK2v88internal8compiler9LiveRange35NextUsePositionRegisterIsBeneficialENS1_16LifetimePositionE
	.section	.text._ZNK2v88internal8compiler9LiveRange40NextLifetimePositionRegisterIsBeneficialERKNS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange40NextLifetimePositionRegisterIsBeneficialERKNS1_16LifetimePositionE
	.type	_ZNK2v88internal8compiler9LiveRange40NextLifetimePositionRegisterIsBeneficialERKNS1_16LifetimePositionE, @function
_ZNK2v88internal8compiler9LiveRange40NextLifetimePositionRegisterIsBeneficialERKNS1_16LifetimePositionE:
.LFB23269:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	movl	(%rsi), %ecx
	testq	%rax, %rax
	je	.L274
	movl	24(%rax), %edx
	cmpl	%ecx, %edx
	jle	.L279
.L274:
	movq	24(%rdi), %rax
	testq	%rax, %rax
	jne	.L294
	.p2align 4,,10
	.p2align 3
.L276:
	movq	$0, 56(%rdi)
.L277:
	movq	8(%rdi), %rax
	movl	4(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L276
.L294:
	movl	24(%rax), %edx
.L279:
	cmpl	%edx, %ecx
	jg	.L295
	movq	%rax, 56(%rdi)
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L296:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L277
.L281:
	testb	$32, 28(%rax)
	je	.L296
	movl	24(%rax), %eax
	ret
	.cfi_endproc
.LFE23269:
	.size	_ZNK2v88internal8compiler9LiveRange40NextLifetimePositionRegisterIsBeneficialERKNS1_16LifetimePositionE, .-_ZNK2v88internal8compiler9LiveRange40NextLifetimePositionRegisterIsBeneficialERKNS1_16LifetimePositionE
	.section	.text._ZNK2v88internal8compiler9LiveRange39PreviousUsePositionRegisterIsBeneficialENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange39PreviousUsePositionRegisterIsBeneficialENS1_16LifetimePositionE
	.type	_ZNK2v88internal8compiler9LiveRange39PreviousUsePositionRegisterIsBeneficialENS1_16LifetimePositionE, @function
_ZNK2v88internal8compiler9LiveRange39PreviousUsePositionRegisterIsBeneficialENS1_16LifetimePositionE:
.LFB23270:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	jne	.L301
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L304:
	testb	$32, 28(%rax)
	cmovne	%rax, %r8
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L297
.L301:
	cmpl	%esi, 24(%rax)
	jl	.L304
.L297:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE23270:
	.size	_ZNK2v88internal8compiler9LiveRange39PreviousUsePositionRegisterIsBeneficialENS1_16LifetimePositionE, .-_ZNK2v88internal8compiler9LiveRange39PreviousUsePositionRegisterIsBeneficialENS1_16LifetimePositionE
	.section	.text._ZNK2v88internal8compiler9LiveRange20NextRegisterPositionENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange20NextRegisterPositionENS1_16LifetimePositionE
	.type	_ZNK2v88internal8compiler9LiveRange20NextRegisterPositionENS1_16LifetimePositionE, @function
_ZNK2v88internal8compiler9LiveRange20NextRegisterPositionENS1_16LifetimePositionE:
.LFB23271:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L306
	movl	24(%rax), %edx
	cmpl	%edx, %esi
	jge	.L311
.L306:
	movq	24(%rdi), %rax
	testq	%rax, %rax
	jne	.L326
.L308:
	movq	$0, 56(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L308
.L326:
	movl	24(%rax), %edx
.L311:
	cmpl	%edx, %esi
	jg	.L327
	movq	%rax, 56(%rdi)
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L329:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L328
.L312:
	movzbl	28(%rax), %edx
	andl	$3, %edx
	cmpb	$2, %dl
	jne	.L329
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	ret
	.cfi_endproc
.LFE23271:
	.size	_ZNK2v88internal8compiler9LiveRange20NextRegisterPositionENS1_16LifetimePositionE, .-_ZNK2v88internal8compiler9LiveRange20NextRegisterPositionENS1_16LifetimePositionE
	.section	.text._ZNK2v88internal8compiler9LiveRange16NextSlotPositionENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange16NextSlotPositionENS1_16LifetimePositionE
	.type	_ZNK2v88internal8compiler9LiveRange16NextSlotPositionENS1_16LifetimePositionE, @function
_ZNK2v88internal8compiler9LiveRange16NextSlotPositionENS1_16LifetimePositionE:
.LFB23272:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L331
	movl	24(%rax), %edx
	cmpl	%edx, %esi
	jge	.L336
.L331:
	movq	24(%rdi), %rax
	testq	%rax, %rax
	jne	.L351
.L333:
	movq	$0, 56(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L333
.L351:
	movl	24(%rax), %edx
.L336:
	cmpl	%edx, %esi
	jg	.L352
	movq	%rax, 56(%rdi)
.L337:
	movzbl	28(%rax), %edx
	andl	$3, %edx
	cmpb	$3, %dl
	je	.L330
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L337
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	ret
	.cfi_endproc
.LFE23272:
	.size	_ZNK2v88internal8compiler9LiveRange16NextSlotPositionENS1_16LifetimePositionE, .-_ZNK2v88internal8compiler9LiveRange16NextSlotPositionENS1_16LifetimePositionE
	.section	.text._ZNK2v88internal8compiler9LiveRange12CanBeSpilledENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange12CanBeSpilledENS1_16LifetimePositionE
	.type	_ZNK2v88internal8compiler9LiveRange12CanBeSpilledENS1_16LifetimePositionE, @function
_ZNK2v88internal8compiler9LiveRange12CanBeSpilledENS1_16LifetimePositionE:
.LFB23273:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L354
	movl	24(%rax), %edx
	cmpl	%esi, %edx
	jle	.L359
.L354:
	movq	24(%rdi), %rax
	testq	%rax, %rax
	jne	.L373
	.p2align 4,,10
	.p2align 3
.L356:
	movq	$0, 56(%rdi)
.L357:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L356
.L373:
	movl	24(%rax), %edx
.L359:
	cmpl	%edx, %esi
	jg	.L374
	movq	%rax, 56(%rdi)
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L375:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L357
.L361:
	movzbl	28(%rax), %edx
	andl	$3, %edx
	cmpb	$2, %dl
	jne	.L375
	andl	$-2, %esi
	addl	$3, %esi
	cmpl	24(%rax), %esi
	setl	%al
	ret
	.cfi_endproc
.LFE23273:
	.size	_ZNK2v88internal8compiler9LiveRange12CanBeSpilledENS1_16LifetimePositionE, .-_ZNK2v88internal8compiler9LiveRange12CanBeSpilledENS1_16LifetimePositionE
	.section	.text._ZNK2v88internal8compiler9LiveRange10IsTopLevelEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange10IsTopLevelEv
	.type	_ZNK2v88internal8compiler9LiveRange10IsTopLevelEv, @function
_ZNK2v88internal8compiler9LiveRange10IsTopLevelEv:
.LFB23274:
	.cfi_startproc
	endbr64
	cmpq	%rdi, 32(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE23274:
	.size	_ZNK2v88internal8compiler9LiveRange10IsTopLevelEv, .-_ZNK2v88internal8compiler9LiveRange10IsTopLevelEv
	.section	.text._ZNK2v88internal8compiler9LiveRange18GetAssignedOperandEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange18GetAssignedOperandEv
	.type	_ZNK2v88internal8compiler9LiveRange18GetAssignedOperandEv, @function
_ZNK2v88internal8compiler9LiveRange18GetAssignedOperandEv:
.LFB23275:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %edx
	movl	%edx, %ecx
	shrl	$7, %ecx
	andl	$63, %ecx
	cmpl	$32, %ecx
	je	.L378
	shrl	$13, %edx
	salq	$35, %rcx
	movzbl	%dl, %eax
	salq	$5, %rax
	orq	%rcx, %rax
	orq	$5, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	movq	32(%rdi), %rdx
	movl	4(%rdx), %eax
	movq	104(%rdx), %rcx
	movl	%eax, %edx
	shrl	$5, %edx
	andl	$3, %edx
	cmpl	$1, %edx
	je	.L382
	movslq	44(%rcx), %rdx
	shrl	$13, %eax
	movzbl	%al, %eax
	salq	$5, %rax
	salq	$35, %rdx
	orq	%rdx, %rax
	orq	$13, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	movq	(%rcx), %rax
	ret
	.cfi_endproc
.LFE23275:
	.size	_ZNK2v88internal8compiler9LiveRange18GetAssignedOperandEv, .-_ZNK2v88internal8compiler9LiveRange18GetAssignedOperandEv
	.section	.text._ZNK2v88internal8compiler9LiveRange30FirstSearchIntervalForPositionENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange30FirstSearchIntervalForPositionENS1_16LifetimePositionE
	.type	_ZNK2v88internal8compiler9LiveRange30FirstSearchIntervalForPositionENS1_16LifetimePositionE, @function
_ZNK2v88internal8compiler9LiveRange30FirstSearchIntervalForPositionENS1_16LifetimePositionE:
.LFB23276:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	testq	%rax, %rax
	je	.L386
	cmpl	(%rax), %esi
	jl	.L387
	ret
	.p2align 4,,10
	.p2align 3
.L387:
	movq	$0, 48(%rdi)
.L386:
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE23276:
	.size	_ZNK2v88internal8compiler9LiveRange30FirstSearchIntervalForPositionENS1_16LifetimePositionE, .-_ZNK2v88internal8compiler9LiveRange30FirstSearchIntervalForPositionENS1_16LifetimePositionE
	.section	.text._ZNK2v88internal8compiler9LiveRange26AdvanceLastProcessedMarkerEPNS1_11UseIntervalENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange26AdvanceLastProcessedMarkerEPNS1_11UseIntervalENS1_16LifetimePositionE
	.type	_ZNK2v88internal8compiler9LiveRange26AdvanceLastProcessedMarkerEPNS1_11UseIntervalENS1_16LifetimePositionE, @function
_ZNK2v88internal8compiler9LiveRange26AdvanceLastProcessedMarkerEPNS1_11UseIntervalENS1_16LifetimePositionE:
.LFB23277:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L388
	movl	(%rsi), %eax
	cmpl	%eax, %edx
	jl	.L388
	movq	48(%rdi), %rcx
	movl	$-1, %edx
	testq	%rcx, %rcx
	je	.L390
	movl	(%rcx), %edx
.L390:
	cmpl	%edx, %eax
	jle	.L388
	movq	%rsi, 48(%rdi)
.L388:
	ret
	.cfi_endproc
.LFE23277:
	.size	_ZNK2v88internal8compiler9LiveRange26AdvanceLastProcessedMarkerEPNS1_11UseIntervalENS1_16LifetimePositionE, .-_ZNK2v88internal8compiler9LiveRange26AdvanceLastProcessedMarkerEPNS1_11UseIntervalENS1_16LifetimePositionE
	.section	.text._ZN2v88internal8compiler9LiveRange8DetachAtENS1_16LifetimePositionEPS2_PNS0_4ZoneENS2_20HintConnectionOptionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9LiveRange8DetachAtENS1_16LifetimePositionEPS2_PNS0_4ZoneENS2_20HintConnectionOptionE
	.type	_ZN2v88internal8compiler9LiveRange8DetachAtENS1_16LifetimePositionEPS2_PNS0_4ZoneENS2_20HintConnectionOptionE, @function
_ZN2v88internal8compiler9LiveRange8DetachAtENS1_16LifetimePositionEPS2_PNS0_4ZoneENS2_20HintConnectionOptionE:
.LFB23279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	48(%rdi), %r12
	testq	%r12, %r12
	je	.L436
	movl	(%r12), %eax
	cmpl	%esi, %eax
	jg	.L438
.L398:
	cmpl	%eax, %ebx
	jne	.L404
	movq	16(%r13), %r12
.L404:
	testq	%r12, %r12
	je	.L418
.L442:
	cmpl	%ebx, (%r12)
	jg	.L401
	movl	4(%r12), %r15d
	cmpl	%ebx, %r15d
	jle	.L401
	movq	16(%rcx), %rax
	movq	24(%rcx), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L439
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
.L403:
	movq	$0, 8(%rax)
	xorl	%ecx, %ecx
	movl	%ebx, (%rax)
	movl	%r15d, 4(%rax)
	movq	8(%r12), %rdx
	movq	%rdx, 8(%rax)
	movq	$0, 8(%r12)
	movl	%ebx, 4(%r12)
.L400:
	movq	8(%r13), %rdx
	movq	%rax, %xmm1
	cmpq	%r12, %rdx
	cmove	%rax, %rdx
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r14)
	movq	72(%r13), %rax
	movq	%r12, 8(%r13)
	testq	%rax, %rax
	je	.L406
	cmpl	24(%rax), %ebx
	jge	.L407
.L406:
	movq	24(%r13), %rax
	testb	%cl, %cl
	je	.L408
	testq	%rax, %rax
	je	.L409
.L416:
	xorl	%esi, %esi
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L440:
	movq	16(%rax), %rdx
	movq	%rax, %rsi
	testq	%rdx, %rdx
	je	.L412
	movq	%rdx, %rax
.L411:
	cmpl	24(%rax), %ebx
	jg	.L440
.L410:
	testq	%rsi, %rsi
	je	.L409
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rsi)
	movq	%rax, 24(%r14)
	movups	%xmm0, 48(%r13)
	testb	%r8b, %r8b
	je	.L437
	movl	28(%rax), %edx
	movq	%rsi, 8(%rax)
	andl	$-29, %edx
	orl	$8, %edx
	movl	%edx, 28(%rax)
.L437:
	addq	$24, %rsp
	movq	%rsi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	movq	8(%r12), %rax
	cmpl	%ebx, (%rax)
	jge	.L441
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.L442
.L418:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L407:
	testb	%cl, %cl
	jne	.L416
.L417:
	xorl	%esi, %esi
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L443:
	movq	16(%rax), %rcx
	movq	%rax, %rsi
	testq	%rcx, %rcx
	je	.L412
	movq	%rcx, %rax
.L413:
	cmpl	24(%rax), %ebx
	jge	.L443
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L408:
	testq	%rax, %rax
	jne	.L417
.L409:
	pxor	%xmm0, %xmm0
	movq	$0, 24(%r13)
	movq	%rax, 24(%r14)
	xorl	%eax, %eax
	movups	%xmm0, 48(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	movq	$0, 48(%rdi)
.L436:
	movq	16(%r13), %r12
	movl	(%r12), %eax
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L412:
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	$0, 24(%r14)
	movups	%xmm0, 48(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	movq	$0, 8(%r12)
	sete	%cl
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L439:
	movl	$16, %esi
	movq	%rcx, %rdi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	jmp	.L403
	.cfi_endproc
.LFE23279:
	.size	_ZN2v88internal8compiler9LiveRange8DetachAtENS1_16LifetimePositionEPS2_PNS0_4ZoneENS2_20HintConnectionOptionE, .-_ZN2v88internal8compiler9LiveRange8DetachAtENS1_16LifetimePositionEPS2_PNS0_4ZoneENS2_20HintConnectionOptionE
	.section	.text._ZN2v88internal8compiler9LiveRange7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9LiveRange7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler9LiveRange7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE, @function
_ZN2v88internal8compiler9LiveRange7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE:
.LFB23278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	32(%rdi), %rax
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%rax, %rsi
	movq	96(%rax), %rax
	testq	%rax, %rax
	jne	.L445
	movl	92(%rsi), %eax
	leal	1(%rax), %r15d
	movl	%r15d, 92(%rsi)
	movq	16(%rcx), %r12
	movq	24(%rcx), %rax
	movl	4(%rbx), %r13d
	movq	32(%rbx), %rdx
	subq	%r12, %rax
	cmpq	$87, %rax
	jbe	.L450
	leaq	88(%r12), %rax
	movq	%rax, 16(%rcx)
.L447:
	andl	$2088960, %r13d
	movl	%r14d, %esi
	movq	%rbx, %rdi
	xorl	%r8d, %r8d
	pxor	%xmm0, %xmm0
	orl	$134221824, %r13d
	movl	%r15d, (%r12)
	movq	%rdx, 32(%r12)
	movq	%r12, %rdx
	movl	%r13d, 4(%r12)
	movups	%xmm0, 72(%r12)
	movq	$0, 24(%r12)
	movups	%xmm0, 8(%r12)
	movups	%xmm0, 40(%r12)
	movups	%xmm0, 56(%r12)
	movq	80(%rbx), %rax
	movq	%rax, 80(%r12)
	call	_ZN2v88internal8compiler9LiveRange8DetachAtENS1_16LifetimePositionEPS2_PNS0_4ZoneENS2_20HintConnectionOptionE
	movq	32(%rbx), %rax
	movq	%rax, 32(%r12)
	movq	40(%rbx), %rax
	movq	%rax, 40(%r12)
	movq	%r12, %rax
	movq	%r12, 40(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	.cfi_restore_state
	movq	%rcx, %rdi
	movl	$88, %esi
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L447
	.cfi_endproc
.LFE23278:
	.size	_ZN2v88internal8compiler9LiveRange7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE, .-_ZN2v88internal8compiler9LiveRange7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler9LiveRange26UpdateParentForAllChildrenEPNS1_17TopLevelLiveRangeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9LiveRange26UpdateParentForAllChildrenEPNS1_17TopLevelLiveRangeE
	.type	_ZN2v88internal8compiler9LiveRange26UpdateParentForAllChildrenEPNS1_17TopLevelLiveRangeE, @function
_ZN2v88internal8compiler9LiveRange26UpdateParentForAllChildrenEPNS1_17TopLevelLiveRangeE:
.LFB23280:
	.cfi_startproc
	endbr64
	.p2align 4,,10
	.p2align 3
.L452:
	movq	%rsi, 32(%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L452
	ret
	.cfi_endproc
.LFE23280:
	.size	_ZN2v88internal8compiler9LiveRange26UpdateParentForAllChildrenEPNS1_17TopLevelLiveRangeE, .-_ZN2v88internal8compiler9LiveRange26UpdateParentForAllChildrenEPNS1_17TopLevelLiveRangeE
	.section	.text._ZN2v88internal8compiler9LiveRange20ConvertUsesToOperandERKNS1_18InstructionOperandES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9LiveRange20ConvertUsesToOperandERKNS1_18InstructionOperandES5_
	.type	_ZN2v88internal8compiler9LiveRange20ConvertUsesToOperandERKNS1_18InstructionOperandES5_, @function
_ZN2v88internal8compiler9LiveRange20ConvertUsesToOperandERKNS1_18InstructionOperandES5_:
.LFB23281:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testq	%rax, %rax
	jne	.L458
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L457:
	movq	(%rsi), %rcx
	movq	%rcx, (%rdi)
.L456:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L454
.L458:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L456
	movzbl	28(%rax), %ecx
	andl	$3, %ecx
	cmpb	$3, %cl
	jne	.L457
	movq	(%rdx), %rcx
	movq	%rcx, (%rdi)
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L458
.L454:
	ret
	.cfi_endproc
.LFE23281:
	.size	_ZN2v88internal8compiler9LiveRange20ConvertUsesToOperandERKNS1_18InstructionOperandES5_, .-_ZN2v88internal8compiler9LiveRange20ConvertUsesToOperandERKNS1_18InstructionOperandES5_
	.section	.text._ZNK2v88internal8compiler9LiveRange23ShouldBeAllocatedBeforeEPKS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange23ShouldBeAllocatedBeforeEPKS2_
	.type	_ZNK2v88internal8compiler9LiveRange23ShouldBeAllocatedBeforeEPKS2_, @function
_ZNK2v88internal8compiler9LiveRange23ShouldBeAllocatedBeforeEPKS2_:
.LFB23282:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	(%rax), %edx
	movq	16(%rsi), %rax
	movl	(%rax), %eax
	cmpl	%eax, %edx
	setl	%r8b
	je	.L476
.L466:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	movl	4(%rdi), %edx
	movl	4(%rsi), %eax
	movl	$1, %r8d
	shrl	$22, %edx
	shrl	$22, %eax
	andl	$63, %edx
	andl	$63, %eax
	cmpl	%eax, %edx
	jl	.L466
	movl	$0, %r8d
	jg	.L466
	movq	24(%rdi), %rax
	movq	24(%rsi), %rdx
	cmpq	%rdx, %rax
	je	.L475
	testq	%rax, %rax
	je	.L466
	testq	%rdx, %rdx
	je	.L474
	movl	24(%rdx), %edx
	movl	24(%rax), %eax
	cmpl	%eax, %edx
	setg	%r8b
	jne	.L466
.L475:
	movq	32(%rdi), %rdx
	movq	32(%rsi), %rax
	movl	88(%rax), %eax
	cmpl	%eax, 88(%rdx)
	setl	%r8b
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L474:
	movl	$1, %r8d
	jmp	.L466
	.cfi_endproc
.LFE23282:
	.size	_ZNK2v88internal8compiler9LiveRange23ShouldBeAllocatedBeforeEPKS2_, .-_ZNK2v88internal8compiler9LiveRange23ShouldBeAllocatedBeforeEPKS2_
	.section	.text._ZN2v88internal8compiler9LiveRange11SetUseHintsEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9LiveRange11SetUseHintsEi
	.type	_ZN2v88internal8compiler9LiveRange11SetUseHintsEi, @function
_ZN2v88internal8compiler9LiveRange11SetUseHintsEi:
.LFB23283:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L477
	sall	$6, %esi
	.p2align 4,,10
	.p2align 3
.L480:
	cmpq	$0, (%rax)
	je	.L479
	movl	28(%rax), %edx
	movl	%edx, %ecx
	andl	$3, %ecx
	cmpb	$3, %cl
	je	.L479
	andl	$-4033, %edx
	orl	%esi, %edx
	movl	%edx, 28(%rax)
.L479:
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L480
.L477:
	ret
	.cfi_endproc
.LFE23283:
	.size	_ZN2v88internal8compiler9LiveRange11SetUseHintsEi, .-_ZN2v88internal8compiler9LiveRange11SetUseHintsEi
	.section	.text._ZNK2v88internal8compiler9LiveRange8CanCoverENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange8CanCoverENS1_16LifetimePositionE
	.type	_ZNK2v88internal8compiler9LiveRange8CanCoverENS1_16LifetimePositionE, @function
_ZNK2v88internal8compiler9LiveRange8CanCoverENS1_16LifetimePositionE:
.LFB23284:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L488
	cmpl	%esi, (%rax)
	jg	.L488
	movq	8(%rdi), %rax
	cmpl	4(%rax), %esi
	setl	%r8b
.L488:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE23284:
	.size	_ZNK2v88internal8compiler9LiveRange8CanCoverENS1_16LifetimePositionE, .-_ZNK2v88internal8compiler9LiveRange8CanCoverENS1_16LifetimePositionE
	.section	.text._ZNK2v88internal8compiler9LiveRange6CoversENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange6CoversENS1_16LifetimePositionE
	.type	_ZNK2v88internal8compiler9LiveRange6CoversENS1_16LifetimePositionE, @function
_ZNK2v88internal8compiler9LiveRange6CoversENS1_16LifetimePositionE:
.LFB23285:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L493
	movl	(%rcx), %eax
	cmpl	%esi, %eax
	jg	.L493
	movq	8(%rdi), %rdx
	cmpl	%esi, 4(%rdx)
	jle	.L493
	movq	48(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L499
	movl	(%rdx), %eax
	cmpl	%esi, %eax
	jle	.L498
	movq	$0, 48(%rdi)
	movl	(%rcx), %eax
	movq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L498:
	cmpl	%eax, %esi
	jl	.L493
	movq	48(%rdi), %r8
	movl	$-1, %ecx
	testq	%r8, %r8
	je	.L495
	movl	(%r8), %ecx
.L495:
	cmpl	%eax, %ecx
	jge	.L496
	movq	%rdx, 48(%rdi)
	cmpl	(%rdx), %esi
	jl	.L493
.L496:
	cmpl	%esi, 4(%rdx)
	jle	.L509
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L509:
	movq	8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L493
	movl	(%rdx), %eax
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L499:
	movq	%rcx, %rdx
	jmp	.L498
	.cfi_endproc
.LFE23285:
	.size	_ZNK2v88internal8compiler9LiveRange6CoversENS1_16LifetimePositionE, .-_ZNK2v88internal8compiler9LiveRange6CoversENS1_16LifetimePositionE
	.section	.text._ZNK2v88internal8compiler9LiveRange12NextEndAfterENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange12NextEndAfterENS1_16LifetimePositionE
	.type	_ZNK2v88internal8compiler9LiveRange12NextEndAfterENS1_16LifetimePositionE, @function
_ZNK2v88internal8compiler9LiveRange12NextEndAfterENS1_16LifetimePositionE:
.LFB23286:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	testq	%rax, %rax
	je	.L516
	cmpl	%esi, (%rax)
	jg	.L517
.L512:
	movl	4(%rax), %r8d
	cmpl	%r8d, %esi
	jle	.L513
	.p2align 4,,10
	.p2align 3
.L514:
	movq	8(%rax), %rax
	movl	4(%rax), %r8d
	cmpl	%esi, %r8d
	jl	.L514
.L513:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	movq	$0, 48(%rdi)
.L516:
	movq	16(%rdi), %rax
	jmp	.L512
	.cfi_endproc
.LFE23286:
	.size	_ZNK2v88internal8compiler9LiveRange12NextEndAfterENS1_16LifetimePositionE, .-_ZNK2v88internal8compiler9LiveRange12NextEndAfterENS1_16LifetimePositionE
	.section	.text._ZNK2v88internal8compiler9LiveRange14NextStartAfterENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange14NextStartAfterENS1_16LifetimePositionE
	.type	_ZNK2v88internal8compiler9LiveRange14NextStartAfterENS1_16LifetimePositionE, @function
_ZNK2v88internal8compiler9LiveRange14NextStartAfterENS1_16LifetimePositionE:
.LFB23287:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	testq	%rax, %rax
	je	.L524
	movl	(%rax), %r8d
	cmpl	%esi, %r8d
	jg	.L525
.L520:
	cmpl	%r8d, %esi
	jle	.L521
	.p2align 4,,10
	.p2align 3
.L522:
	movq	8(%rax), %rax
	movl	(%rax), %r8d
	cmpl	%esi, %r8d
	jl	.L522
.L521:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	movq	$0, 48(%rdi)
.L524:
	movq	16(%rdi), %rax
	movl	(%rax), %r8d
	jmp	.L520
	.cfi_endproc
.LFE23287:
	.size	_ZNK2v88internal8compiler9LiveRange14NextStartAfterENS1_16LifetimePositionE, .-_ZNK2v88internal8compiler9LiveRange14NextStartAfterENS1_16LifetimePositionE
	.section	.text._ZNK2v88internal8compiler9LiveRange17FirstIntersectionEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange17FirstIntersectionEPS2_
	.type	_ZNK2v88internal8compiler9LiveRange17FirstIntersectionEPS2_, @function
_ZNK2v88internal8compiler9LiveRange17FirstIntersectionEPS2_:
.LFB23288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L531
	movq	%rdi, %rbx
	movq	48(%rdi), %rdi
	movl	(%rsi), %r14d
	testq	%rdi, %rdi
	je	.L558
	movl	(%rdi), %r10d
	cmpl	%r14d, %r10d
	jg	.L559
.L532:
	movq	8(%r13), %rax
	movl	4(%rax), %r12d
	.p2align 4,,10
	.p2align 3
.L534:
	cmpl	%r12d, %r10d
	jg	.L531
.L560:
	movq	8(%rbx), %rax
	movl	(%rsi), %r11d
	cmpl	%r11d, 4(%rax)
	jl	.L531
	movl	%r10d, %edx
	movl	%r11d, %eax
	movq	%rsi, %r8
	movq	%rdi, %rcx
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L542:
	movl	%eax, %ecx
	movl	%edx, %eax
	movl	%ecx, %edx
	movq	%r9, %rcx
.L536:
	movq	%r8, %r9
	movq	%rcx, %r8
	cmpl	%edx, %eax
	jl	.L542
	cmpl	%eax, 4(%rcx)
	jle	.L537
	cmpl	$-1, %eax
	jne	.L528
.L537:
	cmpl	%r10d, %r11d
	jle	.L538
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L531
	movl	(%rdi), %r10d
	cmpl	%r12d, %r10d
	jg	.L531
	cmpl	%r10d, %r14d
	jl	.L534
	movq	48(%rbx), %rdx
	movl	$-1, %eax
	testq	%rdx, %rdx
	je	.L540
	movl	(%rdx), %eax
.L540:
	cmpl	%r10d, %eax
	jge	.L534
	movq	%rdi, 48(%rbx)
	movq	8(%r13), %rax
	movl	(%rdi), %r10d
	movl	4(%rax), %r12d
	cmpl	%r12d, %r10d
	jle	.L560
	.p2align 4,,10
	.p2align 3
.L531:
	movl	$-1, %eax
.L528:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L538:
	.cfi_restore_state
	movq	8(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.L534
	jmp	.L531
.L559:
	movq	$0, 48(%rbx)
	movq	16(%rbx), %rdi
.L533:
	testq	%rdi, %rdi
	je	.L531
	movl	(%rdi), %r10d
	jmp	.L532
.L558:
	movq	16(%rbx), %rdi
	jmp	.L533
	.cfi_endproc
.LFE23288:
	.size	_ZNK2v88internal8compiler9LiveRange17FirstIntersectionEPS2_, .-_ZNK2v88internal8compiler9LiveRange17FirstIntersectionEPS2_
	.section	.text._ZNK2v88internal8compiler9LiveRange18RegisterFromBundleEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange18RegisterFromBundleEPi
	.type	_ZNK2v88internal8compiler9LiveRange18RegisterFromBundleEPi, @function
_ZNK2v88internal8compiler9LiveRange18RegisterFromBundleEPi:
.LFB23291:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	testq	%rax, %rax
	je	.L563
	movl	116(%rax), %edx
	xorl	%eax, %eax
	cmpl	$32, %edx
	je	.L561
	movl	%edx, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L563:
	xorl	%eax, %eax
.L561:
	ret
	.cfi_endproc
.LFE23291:
	.size	_ZNK2v88internal8compiler9LiveRange18RegisterFromBundleEPi, .-_ZNK2v88internal8compiler9LiveRange18RegisterFromBundleEPi
	.section	.text._ZNK2v88internal8compiler9LiveRange20UpdateBundleRegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange20UpdateBundleRegisterEi
	.type	_ZNK2v88internal8compiler9LiveRange20UpdateBundleRegisterEi, @function
_ZNK2v88internal8compiler9LiveRange20UpdateBundleRegisterEi:
.LFB23292:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	testq	%rax, %rax
	je	.L566
	cmpl	$32, 116(%rax)
	je	.L571
.L566:
	ret
	.p2align 4,,10
	.p2align 3
.L571:
	movl	%esi, 116(%rax)
	ret
	.cfi_endproc
.LFE23292:
	.size	_ZNK2v88internal8compiler9LiveRange20UpdateBundleRegisterEi, .-_ZNK2v88internal8compiler9LiveRange20UpdateBundleRegisterEi
	.section	.text._ZN2v88internal8compiler17TopLevelLiveRangeC2EiNS0_21MachineRepresentationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TopLevelLiveRangeC2EiNS0_21MachineRepresentationE
	.type	_ZN2v88internal8compiler17TopLevelLiveRangeC2EiNS0_21MachineRepresentationE, @function
_ZN2v88internal8compiler17TopLevelLiveRangeC2EiNS0_21MachineRepresentationE:
.LFB23298:
	.cfi_startproc
	endbr64
	movzbl	%dl, %edx
	pxor	%xmm0, %xmm0
	movq	%rdi, 32(%rdi)
	sall	$13, %edx
	movl	$0, (%rdi)
	orl	$134221824, %edx
	movq	$0, 24(%rdi)
	movl	%edx, 4(%rdi)
	movl	%esi, 88(%rdi)
	movl	$0, 92(%rdi)
	movq	$0, 112(%rdi)
	movb	$0, 120(%rdi)
	movl	$2147483647, 124(%rdi)
	movq	$0, 128(%rdi)
	movq	%rdi, 136(%rdi)
	movq	$0, 144(%rdi)
	movb	$0, 152(%rdi)
	movups	%xmm0, 8(%rdi)
	movups	%xmm0, 40(%rdi)
	movups	%xmm0, 56(%rdi)
	movups	%xmm0, 72(%rdi)
	movups	%xmm0, 96(%rdi)
	ret
	.cfi_endproc
.LFE23298:
	.size	_ZN2v88internal8compiler17TopLevelLiveRangeC2EiNS0_21MachineRepresentationE, .-_ZN2v88internal8compiler17TopLevelLiveRangeC2EiNS0_21MachineRepresentationE
	.globl	_ZN2v88internal8compiler17TopLevelLiveRangeC1EiNS0_21MachineRepresentationE
	.set	_ZN2v88internal8compiler17TopLevelLiveRangeC1EiNS0_21MachineRepresentationE,_ZN2v88internal8compiler17TopLevelLiveRangeC2EiNS0_21MachineRepresentationE
	.section	.text._ZN2v88internal8compiler17TopLevelLiveRange19RecordSpillLocationEPNS0_4ZoneEiPNS1_18InstructionOperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TopLevelLiveRange19RecordSpillLocationEPNS0_4ZoneEiPNS1_18InstructionOperandE
	.type	_ZN2v88internal8compiler17TopLevelLiveRange19RecordSpillLocationEPNS0_4ZoneEiPNS1_18InstructionOperandE, @function
_ZN2v88internal8compiler17TopLevelLiveRange19RecordSpillLocationEPNS0_4ZoneEiPNS1_18InstructionOperandE:
.LFB23300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$16, %rsp
	movq	16(%rsi), %rax
	movq	24(%rsi), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L577
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rsi)
.L575:
	movq	112(%rbx), %rdx
	movl	%r13d, (%rax)
	movq	%rcx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 112(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L577:
	.cfi_restore_state
	movl	$24, %esi
	movq	%rcx, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-24(%rbp), %rcx
	jmp	.L575
	.cfi_endproc
.LFE23300:
	.size	_ZN2v88internal8compiler17TopLevelLiveRange19RecordSpillLocationEPNS0_4ZoneEiPNS1_18InstructionOperandE, .-_ZN2v88internal8compiler17TopLevelLiveRange19RecordSpillLocationEPNS0_4ZoneEiPNS1_18InstructionOperandE
	.section	.text._ZN2v88internal8compiler17TopLevelLiveRange15SetSpillOperandEPNS1_18InstructionOperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TopLevelLiveRange15SetSpillOperandEPNS1_18InstructionOperandE
	.type	_ZN2v88internal8compiler17TopLevelLiveRange15SetSpillOperandEPNS1_18InstructionOperandE, @function
_ZN2v88internal8compiler17TopLevelLiveRange15SetSpillOperandEPNS1_18InstructionOperandE:
.LFB23302:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	movq	%rsi, 104(%rdi)
	andl	$-97, %eax
	orl	$32, %eax
	movl	%eax, 4(%rdi)
	ret
	.cfi_endproc
.LFE23302:
	.size	_ZN2v88internal8compiler17TopLevelLiveRange15SetSpillOperandEPNS1_18InstructionOperandE, .-_ZN2v88internal8compiler17TopLevelLiveRange15SetSpillOperandEPNS1_18InstructionOperandE
	.section	.text._ZN2v88internal8compiler17TopLevelLiveRange13SetSpillRangeEPNS1_10SpillRangeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TopLevelLiveRange13SetSpillRangeEPNS1_10SpillRangeE
	.type	_ZN2v88internal8compiler17TopLevelLiveRange13SetSpillRangeEPNS1_10SpillRangeE, @function
_ZN2v88internal8compiler17TopLevelLiveRange13SetSpillRangeEPNS1_10SpillRangeE:
.LFB23303:
	.cfi_startproc
	endbr64
	movq	%rsi, 104(%rdi)
	ret
	.cfi_endproc
.LFE23303:
	.size	_ZN2v88internal8compiler17TopLevelLiveRange13SetSpillRangeEPNS1_10SpillRangeE, .-_ZN2v88internal8compiler17TopLevelLiveRange13SetSpillRangeEPNS1_10SpillRangeE
	.section	.text._ZNK2v88internal8compiler17TopLevelLiveRange20GetSpillRangeOperandEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17TopLevelLiveRange20GetSpillRangeOperandEv
	.type	_ZNK2v88internal8compiler17TopLevelLiveRange20GetSpillRangeOperandEv, @function
_ZNK2v88internal8compiler17TopLevelLiveRange20GetSpillRangeOperandEv:
.LFB23304:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	movq	104(%rdi), %rdx
	shrl	$13, %eax
	movslq	44(%rdx), %rdx
	movzbl	%al, %eax
	salq	$5, %rax
	salq	$35, %rdx
	orq	%rdx, %rax
	orq	$13, %rax
	ret
	.cfi_endproc
.LFE23304:
	.size	_ZNK2v88internal8compiler17TopLevelLiveRange20GetSpillRangeOperandEv, .-_ZNK2v88internal8compiler17TopLevelLiveRange20GetSpillRangeOperandEv
	.section	.text._ZN2v88internal8compiler17TopLevelLiveRange8SplinterENS1_16LifetimePositionES3_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TopLevelLiveRange8SplinterENS1_16LifetimePositionES3_PNS0_4ZoneE
	.type	_ZN2v88internal8compiler17TopLevelLiveRange8SplinterENS1_16LifetimePositionES3_PNS0_4ZoneE, @function
_ZN2v88internal8compiler17TopLevelLiveRange8SplinterENS1_16LifetimePositionES3_PNS0_4ZoneE:
.LFB23305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-224(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$280, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	4(%rdi), %eax
	movq	%r12, -192(%rbp)
	movl	$0, -224(%rbp)
	andl	$2088960, %eax
	movq	$0, -112(%rbp)
	orl	$134221824, %eax
	movb	$0, -104(%rbp)
	movl	%eax, -220(%rbp)
	movl	$4294967295, %eax
	movq	%rax, -136(%rbp)
	movq	8(%rdi), %rax
	movq	$0, -200(%rbp)
	movl	$2147483647, -100(%rbp)
	movq	$0, -96(%rbp)
	movq	%r12, -88(%rbp)
	movq	$0, -80(%rbp)
	movb	$0, -72(%rbp)
	movups	%xmm0, -216(%rbp)
	movups	%xmm0, -184(%rbp)
	movups	%xmm0, -168(%rbp)
	movups	%xmm0, -152(%rbp)
	movaps	%xmm0, -128(%rbp)
	cmpl	%edx, 4(%rax)
	jg	.L582
	movl	$1, %r8d
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler9LiveRange8DetachAtENS1_16LifetimePositionEPS2_PNS0_4ZoneENS2_20HintConnectionOptionE
	movq	$0, 40(%rbx)
	xorl	%eax, %eax
.L583:
	movq	144(%rbx), %rdx
	movq	-208(%rbp), %rcx
	cmpq	$0, 16(%rdx)
	je	.L611
	movq	8(%rdx), %rdx
	movq	%rcx, 8(%rdx)
	movq	144(%rbx), %rdx
	movq	-216(%rbp), %rcx
	movq	%rcx, 8(%rdx)
.L587:
	movq	144(%rbx), %rdx
	movq	-200(%rbp), %rcx
	cmpq	$0, 24(%rdx)
	je	.L612
	movq	128(%rdx), %rdx
	movq	%rcx, 16(%rdx)
.L589:
	movq	144(%rbx), %rdx
	testq	%rax, %rax
	je	.L590
	movq	%rax, 128(%rdx)
.L581:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L613
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L582:
	.cfi_restore_state
	movl	%edx, %r13d
	movl	$1, %r8d
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler9LiveRange8DetachAtENS1_16LifetimePositionEPS2_PNS0_4ZoneENS2_20HintConnectionOptionE
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %r15
	movl	4(%rbx), %eax
	leaq	-320(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movups	%xmm0, -312(%rbp)
	andl	$2088960, %eax
	movups	%xmm0, -296(%rbp)
	orl	$134221824, %eax
	movups	%xmm0, -280(%rbp)
	movl	$2147483647, -320(%rbp)
	movl	%eax, -316(%rbp)
	movups	%xmm0, -264(%rbp)
	movups	%xmm0, -248(%rbp)
	call	_ZN2v88internal8compiler9LiveRange8DetachAtENS1_16LifetimePositionEPS2_PNS0_4ZoneENS2_20HintConnectionOptionE
	movq	-280(%rbp), %rdx
	movq	-304(%rbp), %rcx
	movq	%rdx, 40(%rbx)
	movq	8(%rbx), %rdx
	movq	%rcx, 8(%rdx)
	movq	8(%rbx), %rdx
	cmpq	$0, 24(%rbx)
	movq	%rdx, 48(%rbx)
	movq	-312(%rbp), %rdx
	movq	%rdx, 8(%rbx)
	je	.L614
	movq	%r15, 72(%rbx)
	testq	%r15, %r15
	je	.L583
	movq	-296(%rbp), %rdx
	movq	%rdx, 16(%r15)
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L590:
	movq	24(%rdx), %rax
	testq	%rax, %rax
	je	.L581
	cmpq	$0, 128(%rdx)
	jne	.L581
	movq	%rax, 128(%rdx)
	movq	144(%rbx), %rdx
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L610
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L593:
	movq	144(%rbx), %rdx
.L610:
	movq	%rax, 128(%rdx)
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L593
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L612:
	movq	%rcx, 24(%rdx)
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L611:
	movq	%rcx, 16(%rdx)
	movq	-216(%rbp), %rcx
	movq	144(%rbx), %rdx
	movq	%rcx, 8(%rdx)
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L614:
	movq	-296(%rbp), %rdx
	movq	%rdx, 24(%rbx)
	jmp	.L583
.L613:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23305:
	.size	_ZN2v88internal8compiler17TopLevelLiveRange8SplinterENS1_16LifetimePositionES3_PNS0_4ZoneE, .-_ZN2v88internal8compiler17TopLevelLiveRange8SplinterENS1_16LifetimePositionES3_PNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler17TopLevelLiveRange17SetSplinteredFromEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TopLevelLiveRange17SetSplinteredFromEPS2_
	.type	_ZN2v88internal8compiler17TopLevelLiveRange17SetSplinteredFromEPS2_, @function
_ZN2v88internal8compiler17TopLevelLiveRange17SetSplinteredFromEPS2_:
.LFB23306:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	movq	%rsi, 96(%rdi)
	shrl	$5, %eax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L615
	movq	104(%rsi), %rax
	testq	%rax, %rax
	je	.L615
	movq	%rax, 104(%rdi)
.L615:
	ret
	.cfi_endproc
.LFE23306:
	.size	_ZN2v88internal8compiler17TopLevelLiveRange17SetSplinteredFromEPS2_, .-_ZN2v88internal8compiler17TopLevelLiveRange17SetSplinteredFromEPS2_
	.section	.text._ZN2v88internal8compiler17TopLevelLiveRange25UpdateSpillRangePostMergeEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TopLevelLiveRange25UpdateSpillRangePostMergeEPS2_
	.type	_ZN2v88internal8compiler17TopLevelLiveRange25UpdateSpillRangePostMergeEPS2_, @function
_ZN2v88internal8compiler17TopLevelLiveRange25UpdateSpillRangePostMergeEPS2_:
.LFB23307:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	testb	$96, %al
	jne	.L623
	movl	4(%rsi), %edx
	testb	$64, %dl
	je	.L623
	andl	$-97, %eax
	andl	$96, %edx
	orl	%edx, %eax
	movl	%eax, 4(%rdi)
	andl	$-97, 4(%rsi)
	movq	$0, 104(%rsi)
.L623:
	ret
	.cfi_endproc
.LFE23307:
	.size	_ZN2v88internal8compiler17TopLevelLiveRange25UpdateSpillRangePostMergeEPS2_, .-_ZN2v88internal8compiler17TopLevelLiveRange25UpdateSpillRangePostMergeEPS2_
	.section	.rodata._ZN2v88internal8compiler17TopLevelLiveRange5MergeEPS2_PNS0_4ZoneE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"temp != first"
	.section	.text._ZN2v88internal8compiler17TopLevelLiveRange5MergeEPS2_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TopLevelLiveRange5MergeEPS2_PNS0_4ZoneE
	.type	_ZN2v88internal8compiler17TopLevelLiveRange5MergeEPS2_PNS0_4ZoneE, @function
_ZN2v88internal8compiler17TopLevelLiveRange5MergeEPS2_PNS0_4ZoneE:
.LFB23309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L631:
	testq	%rbx, %rbx
	je	.L630
.L657:
	testq	%r12, %r12
	je	.L630
	movq	16(%rbx), %rax
	movq	16(%r12), %rcx
	movl	(%rax), %eax
	movl	(%rcx), %esi
	cmpl	%esi, %eax
	jg	.L641
	movq	8(%rbx), %rcx
	cmpl	%esi, 4(%rcx)
	jle	.L655
	movq	8(%r12), %rdx
	cmpl	4(%rdx), %eax
	jge	.L631
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler9LiveRange7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE
	cmpq	%rax, %rbx
	je	.L656
	movl	4(%rbx), %ecx
	movl	4(%rax), %edx
	andl	$1, %ecx
	andl	$-2, %edx
	orl	%edx, %ecx
	movl	%ecx, 4(%rax)
	testb	$1, %cl
	jne	.L636
	movl	4(%rbx), %edx
	andl	$-8065, %ecx
	andl	$8064, %edx
	orl	%edx, %ecx
	movl	%ecx, 4(%rax)
.L636:
	movq	%r12, 40(%rbx)
.L654:
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.L657
	.p2align 4,,10
	.p2align 3
.L630:
	movq	32(%r13), %rdx
	movq	%rdx, %rax
	testq	%rdx, %rdx
	je	.L637
	.p2align 4,,10
	.p2align 3
.L638:
	movq	%rdx, 32(%rax)
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L638
	movq	32(%r13), %rdx
.L637:
	movl	4(%rdx), %ecx
	movl	4(%r14), %eax
	testb	$96, %cl
	jne	.L639
	testb	$64, %al
	je	.L639
	andl	$-97, %ecx
	andl	$96, %eax
	orl	%ecx, %eax
	movl	%eax, 4(%rdx)
	movl	4(%r14), %eax
	movq	$0, 104(%r14)
	andl	$-97, %eax
	movl	%eax, 4(%r14)
	movq	32(%r13), %rdx
	movl	4(%rdx), %ecx
.L639:
	movl	%ecx, %esi
	shrl	%eax
	shrl	%esi
	andl	$3, %eax
	andl	$3, %esi
	cmpl	%eax, %esi
	cmovge	%esi, %eax
	andl	$-7, %ecx
	addl	%eax, %eax
	orl	%ecx, %eax
	movl	%eax, 4(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L641:
	.cfi_restore_state
	movq	%rbx, %rax
	movq	%r12, %rbx
	movq	%rax, %r12
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L655:
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L636
	movq	16(%rax), %rdx
	cmpl	(%rdx), %esi
	jge	.L654
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L656:
	leaq	.LC9(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23309:
	.size	_ZN2v88internal8compiler17TopLevelLiveRange5MergeEPS2_PNS0_4ZoneE, .-_ZN2v88internal8compiler17TopLevelLiveRange5MergeEPS2_PNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler17TopLevelLiveRange21VerifyChildrenInOrderEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17TopLevelLiveRange21VerifyChildrenInOrderEv
	.type	_ZNK2v88internal8compiler17TopLevelLiveRange21VerifyChildrenInOrderEv, @function
_ZNK2v88internal8compiler17TopLevelLiveRange21VerifyChildrenInOrderEv:
.LFB23310:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L658
	.p2align 4,,10
	.p2align 3
.L660:
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L660
.L658:
	ret
	.cfi_endproc
.LFE23310:
	.size	_ZNK2v88internal8compiler17TopLevelLiveRange21VerifyChildrenInOrderEv, .-_ZNK2v88internal8compiler17TopLevelLiveRange21VerifyChildrenInOrderEv
	.section	.text._ZN2v88internal8compiler17TopLevelLiveRange14GetChildCoversENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TopLevelLiveRange14GetChildCoversENS1_16LifetimePositionE
	.type	_ZN2v88internal8compiler17TopLevelLiveRange14GetChildCoversENS1_16LifetimePositionE, @function
_ZN2v88internal8compiler17TopLevelLiveRange14GetChildCoversENS1_16LifetimePositionE:
.LFB23311:
	.cfi_startproc
	endbr64
	movq	136(%rdi), %r9
	testq	%r9, %r9
	jne	.L668
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L678:
	movq	40(%r9), %r9
	testq	%r9, %r9
	je	.L666
.L668:
	movq	8(%r9), %rax
	cmpl	4(%rax), %esi
	jge	.L678
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, 136(%rdi)
	movq	%r9, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal8compiler9LiveRange6CoversENS1_16LifetimePositionE
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r9
	movq	%r9, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L666:
	.cfi_restore 6
	movq	$0, 136(%rdi)
	xorl	%r9d, %r9d
	movq	%r9, %rax
	ret
	.cfi_endproc
.LFE23311:
	.size	_ZN2v88internal8compiler17TopLevelLiveRange14GetChildCoversENS1_16LifetimePositionE, .-_ZN2v88internal8compiler17TopLevelLiveRange14GetChildCoversENS1_16LifetimePositionE
	.section	.text._ZNK2v88internal8compiler17TopLevelLiveRange6VerifyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17TopLevelLiveRange6VerifyEv
	.type	_ZNK2v88internal8compiler17TopLevelLiveRange6VerifyEv, @function
_ZNK2v88internal8compiler17TopLevelLiveRange6VerifyEv:
.LFB23312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L680
	.p2align 4,,10
	.p2align 3
.L681:
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L681
.L680:
	movq	16(%r13), %rax
	movq	%r13, %rbx
	movq	8(%rax), %r12
	.p2align 4,,10
	.p2align 3
.L684:
	testq	%r12, %r12
	je	.L682
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L683:
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L683
.L682:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9LiveRange15VerifyPositionsEv
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L684
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23312:
	.size	_ZNK2v88internal8compiler17TopLevelLiveRange6VerifyEv, .-_ZNK2v88internal8compiler17TopLevelLiveRange6VerifyEv
	.section	.rodata._ZN2v88internal8compiler17TopLevelLiveRange9ShortenToENS1_16LifetimePositionEb.str1.1,"aMS",@progbits,1
.LC10:
	.string	"Shorten live range %d to [%d\n"
	.section	.text._ZN2v88internal8compiler17TopLevelLiveRange9ShortenToENS1_16LifetimePositionEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TopLevelLiveRange9ShortenToENS1_16LifetimePositionEb
	.type	_ZN2v88internal8compiler17TopLevelLiveRange9ShortenToENS1_16LifetimePositionEb, @function
_ZN2v88internal8compiler17TopLevelLiveRange9ShortenToENS1_16LifetimePositionEb:
.LFB23313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	testb	%dl, %dl
	jne	.L698
	movq	16(%r12), %rax
	movl	%ebx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L698:
	.cfi_restore_state
	movl	88(%rdi), %esi
	movl	%ebx, %edx
	xorl	%eax, %eax
	leaq	.LC10(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	16(%r12), %rax
	movl	%ebx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23313:
	.size	_ZN2v88internal8compiler17TopLevelLiveRange9ShortenToENS1_16LifetimePositionEb, .-_ZN2v88internal8compiler17TopLevelLiveRange9ShortenToENS1_16LifetimePositionEb
	.section	.rodata._ZN2v88internal8compiler17TopLevelLiveRange14EnsureIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"Ensure live range %d in interval [%d %d[\n"
	.section	.text._ZN2v88internal8compiler17TopLevelLiveRange14EnsureIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TopLevelLiveRange14EnsureIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb
	.type	_ZN2v88internal8compiler17TopLevelLiveRange14EnsureIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb, @function
_ZN2v88internal8compiler17TopLevelLiveRange14EnsureIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb:
.LFB23314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	testb	%r8b, %r8b
	jne	.L712
.L700:
	movq	16(%r13), %rax
	movl	%ebx, %r12d
	testq	%rax, %rax
	jne	.L708
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L713:
	movl	4(%rax), %edx
	movq	8(%rax), %rax
	cmpl	%ebx, %edx
	movq	%rax, 16(%r13)
	cmovg	%edx, %r12d
	testq	%rax, %rax
	je	.L701
.L708:
	cmpl	%ebx, (%rax)
	jle	.L713
.L701:
	movq	16(%r15), %rax
	movq	24(%r15), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L714
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
.L705:
	movq	$0, 8(%rax)
	movl	%r14d, (%rax)
	movl	%r12d, 4(%rax)
	movq	16(%r13), %rdx
	movq	%rdx, 8(%rax)
	movq	%rax, 16(%r13)
	cmpq	$0, 8(%rax)
	je	.L715
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L712:
	.cfi_restore_state
	movl	88(%rdi), %esi
	movl	%edx, %ecx
	leaq	.LC11(%rip), %rdi
	movl	%r14d, %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L715:
	movq	%rax, 8(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L714:
	.cfi_restore_state
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L705
	.cfi_endproc
.LFE23314:
	.size	_ZN2v88internal8compiler17TopLevelLiveRange14EnsureIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb, .-_ZN2v88internal8compiler17TopLevelLiveRange14EnsureIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb
	.section	.rodata._ZN2v88internal8compiler17TopLevelLiveRange14AddUseIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"Add to live range %d interval [%d %d[\n"
	.section	.text._ZN2v88internal8compiler17TopLevelLiveRange14AddUseIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TopLevelLiveRange14AddUseIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb
	.type	_ZN2v88internal8compiler17TopLevelLiveRange14AddUseIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb, @function
_ZN2v88internal8compiler17TopLevelLiveRange14AddUseIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb:
.LFB23315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	testb	%r8b, %r8b
	jne	.L727
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.L728
.L718:
	movl	(%rax), %esi
	cmpl	%ebx, %esi
	je	.L729
	jg	.L730
	cmpl	%esi, %r12d
	cmovg	%esi, %r12d
	movl	%r12d, (%rax)
	movq	16(%r14), %rax
	cmpl	%ebx, 4(%rax)
	cmovge	4(%rax), %ebx
	movl	%ebx, 4(%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L727:
	.cfi_restore_state
	movl	88(%rdi), %esi
	movl	%edx, %ecx
	xorl	%eax, %eax
	movl	%r12d, %edx
	leaq	.LC12(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	16(%r14), %rax
	testq	%rax, %rax
	jne	.L718
.L728:
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L731
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
.L720:
	movl	%r12d, (%rax)
	movl	%ebx, 4(%rax)
	movq	$0, 8(%rax)
	popq	%rbx
	movq	%rax, 16(%r14)
	popq	%r12
	movq	%rax, 8(%r14)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L729:
	.cfi_restore_state
	movl	%r12d, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L730:
	.cfi_restore_state
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L732
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
.L725:
	movq	$0, 8(%rax)
	movl	%r12d, (%rax)
	movl	%ebx, 4(%rax)
	movq	16(%r14), %rdx
	movq	%rdx, 8(%rax)
	popq	%rbx
	movq	%rax, 16(%r14)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L732:
	.cfi_restore_state
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L731:
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L720
	.cfi_endproc
.LFE23315:
	.size	_ZN2v88internal8compiler17TopLevelLiveRange14AddUseIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb, .-_ZN2v88internal8compiler17TopLevelLiveRange14AddUseIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb
	.section	.rodata._ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"Add to live range %d use position %d\n"
	.section	.text._ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb
	.type	_ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb, @function
_ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb:
.LFB23316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	24(%rsi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.L767
.L734:
	movq	24(%r12), %r8
	testq	%r8, %r8
	je	.L735
	movq	%r8, %rdx
	xorl	%ecx, %ecx
	leaq	.L740(%rip), %rsi
	xorl	%edi, %edi
	cmpl	%r13d, 24(%rdx)
	jge	.L736
	.p2align 4,,10
	.p2align 3
.L768:
	movq	8(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L737
	movl	28(%rdx), %eax
	shrl	$2, %eax
	andl	$7, %eax
	cmpb	$4, %al
	ja	.L738
	movzbl	%al, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb,"a",@progbits
	.align 4
	.align 4
.L740:
	.long	.L737-.L740
	.long	.L752-.L740
	.long	.L742-.L740
	.long	.L741-.L740
	.long	.L737-.L740
	.section	.text._ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb
	.p2align 4,,10
	.p2align 3
.L742:
	movl	28(%rcx), %eax
	shrl	$6, %eax
	andl	$63, %eax
	cmpl	$32, %eax
	cmovne	%rdx, %rdi
.L737:
	movq	16(%rdx), %rax
	movq	%rdx, %rcx
	testq	%rax, %rax
	je	.L746
.L755:
	movq	%rax, %rdx
	cmpl	%r13d, 24(%rdx)
	jl	.L768
.L736:
	testq	%rcx, %rcx
	jne	.L765
	movq	%r8, 16(%rbx)
	movq	%rbx, 24(%r12)
	testq	%rdi, %rdi
	jne	.L733
	.p2align 4,,10
	.p2align 3
.L747:
	leaq	-44(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler11UsePosition12HintRegisterEPi
	testb	%al, %al
	je	.L733
	movq	%rbx, 64(%r12)
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L741:
	movq	16(%rdx), %rax
	cmpl	$32, 48(%rcx)
	movq	%rdx, %rcx
	cmovne	%rdx, %rdi
	testq	%rax, %rax
	jne	.L755
	.p2align 4,,10
	.p2align 3
.L746:
	movq	%rax, 16(%rbx)
	movq	%rbx, 16(%rdx)
	testq	%rdi, %rdi
	je	.L747
.L733:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L769
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L752:
	.cfi_restore_state
	movq	%rdx, %rdi
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L767:
	movl	88(%rdi), %esi
	movl	%r13d, %edx
	leaq	.LC13(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L765:
	movq	16(%rcx), %rax
	movq	%rcx, %rdx
	jmp	.L746
.L735:
	movq	$0, 16(%rbx)
	movq	%rbx, 24(%r12)
	jmp	.L747
.L738:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L769:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23316:
	.size	_ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb, .-_ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_18PrintableLiveRangeE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"Range: "
.LC15:
	.string	":"
.LC16:
	.string	" "
.LC17:
	.string	"phi "
.LC18:
	.string	"nlphi "
.LC19:
	.string	"{"
.LC20:
	.string	"}"
.LC21:
	.string	", "
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_18PrintableLiveRangeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_18PrintableLiveRangeE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_18PrintableLiveRangeE, @function
_ZN2v88internal8compilerlsERSoRKNS1_18PrintableLiveRangeE:
.LFB23318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$7, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r12
	leaq	.LC14(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r12), %rax
	movq	%r13, %rdi
	movl	88(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%r12), %esi
	movq	%r14, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r12), %rax
	movl	4(%rax), %eax
	testb	$8, %al
	jne	.L819
.L771:
	testb	$16, %al
	jne	.L820
.L772:
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r14
	testq	%r14, %r14
	je	.L777
	cmpb	$0, 56(%r14)
	je	.L774
	movsbl	67(%r14), %esi
.L775:
	movq	%r13, %rdi
	leaq	.LC16(%rip), %r14
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	16(%r12), %rbx
	movq	24(%r12), %r12
	testq	%r12, %r12
	je	.L780
	.p2align 4,,10
	.p2align 3
.L776:
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L779
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_18InstructionOperandE@PLT
	movl	24(%r12), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compilerlsERSoNS1_16LifetimePositionE
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L776
.L780:
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r12
	testq	%r12, %r12
	je	.L777
	cmpb	$0, 56(%r12)
	je	.L782
	movsbl	67(%r12), %esi
.L783:
	movq	%r13, %rdi
	leaq	-57(%rbp), %r14
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	testq	%rbx, %rbx
	jne	.L784
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L821:
	movsbl	67(%r15), %esi
.L786:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L787
.L784:
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movb	$91, -57(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compilerlsERSoNS1_16LifetimePositionE
	movl	$2, %edx
	leaq	.LC21(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%rbx), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoNS1_16LifetimePositionE
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$41, -57(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r15
	testq	%r15, %r15
	je	.L777
	cmpb	$0, 56(%r15)
	jne	.L821
	movq	%r15, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L786
	movq	%r15, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L779:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L776
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L787:
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L822
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L774:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L775
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L782:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L783
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L820:
	movl	$6, %edx
	leaq	.LC18(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L819:
	movl	$4, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r12), %rax
	movl	4(%rax), %eax
	jmp	.L771
.L777:
	call	_ZSt16__throw_bad_castv@PLT
.L822:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23318:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_18PrintableLiveRangeE, .-_ZN2v88internal8compilerlsERSoRKNS1_18PrintableLiveRangeE
	.section	.text._ZNK2v88internal8compiler9LiveRange5PrintEPKNS0_21RegisterConfigurationEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange5PrintEPKNS0_21RegisterConfigurationEb
	.type	_ZNK2v88internal8compiler9LiveRange5PrintEPKNS0_21RegisterConfigurationEb, @function
_ZNK2v88internal8compiler9LiveRange5PrintEPKNS0_21RegisterConfigurationEb:
.LFB23289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-400(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$392, %rsp
	movl	%edx, -420(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -432(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%r12, %rdi
	movups	%xmm0, -88(%rbp)
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r13, -416(%rbp)
	leaq	-416(%rbp), %r13
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	movl	-420(%rbp), %eax
	testb	%al, %al
	jne	.L824
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L843:
	movsbl	67(%r14), %esi
.L831:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L828
.L824:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rbx, -408(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_18PrintableLiveRangeE
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %r14
	testq	%r14, %r14
	je	.L825
	cmpb	$0, 56(%r14)
	jne	.L843
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L831
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L826:
	movsbl	67(%r13), %esi
.L827:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
.L828:
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-432(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L844
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L842:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rbx, -408(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_18PrintableLiveRangeE
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r13
	testq	%r13, %r13
	je	.L825
	cmpb	$0, 56(%r13)
	jne	.L826
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L827
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L827
.L825:
	call	_ZSt16__throw_bad_castv@PLT
.L844:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23289:
	.size	_ZNK2v88internal8compiler9LiveRange5PrintEPKNS0_21RegisterConfigurationEb, .-_ZNK2v88internal8compiler9LiveRange5PrintEPKNS0_21RegisterConfigurationEb
	.section	.text._ZNK2v88internal8compiler9LiveRange5PrintEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler9LiveRange5PrintEb
	.type	_ZNK2v88internal8compiler9LiveRange5PrintEb, @function
_ZNK2v88internal8compiler9LiveRange5PrintEb:
.LFB23290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	call	_ZN2v88internal21RegisterConfiguration7DefaultEv@PLT
	movzbl	%bl, %edx
	movq	%r12, %rdi
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v88internal8compiler9LiveRange5PrintEPKNS0_21RegisterConfigurationEb
	.cfi_endproc
.LFE23290:
	.size	_ZNK2v88internal8compiler9LiveRange5PrintEb, .-_ZNK2v88internal8compiler9LiveRange5PrintEb
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator13PrintRangeRowERSoPKNS1_17TopLevelLiveRangeE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"s?"
.LC23:
	.string	"sd"
.LC24:
	.string	"ss"
.LC25:
	.string	"so"
.LC26:
	.string	"unassigned"
.LC27:
	.string	"s:"
.LC28:
	.string	": "
.LC29:
	.string	"start.value() >= position"
.LC30:
	.string	"|%s"
.LC31:
	.string	"end.value() >= position"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator13PrintRangeRowERSoPKNS1_17TopLevelLiveRangeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator13PrintRangeRowERSoPKNS1_17TopLevelLiveRangeE
	.type	_ZN2v88internal8compiler19LinearScanAllocator13PrintRangeRowERSoPKNS1_17TopLevelLiveRangeE, @function
_ZN2v88internal8compiler19LinearScanAllocator13PrintRangeRowERSoPKNS1_17TopLevelLiveRangeE:
.LFB23321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$120, %rsp
	movq	%rdi, -152(%rbp)
	movq	%r12, %rdi
	movq	%rdx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	-24(%rax), %rax
	movq	$3, 16(%rsi,%rax)
	movl	88(%rdx), %esi
	call	_ZNSolsEi@PLT
	cmpq	$0, 96(%rbx)
	movl	$2, %edx
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdi
	leaq	.LC28(%rip), %rax
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%rbx), %eax
	leaq	.LC24(%rip), %rcx
	movq	%rcx, -144(%rbp)
	movl	%eax, -120(%rbp)
	shrl	$5, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	je	.L865
	leaq	.LC23(%rip), %rcx
	movq	%rcx, -144(%rbp)
	cmpl	$3, %eax
	je	.L865
	cmpl	$1, %eax
	leaq	.LC22(%rip), %rdx
	leaq	.LC25(%rip), %rax
	cmovne	%rdx, %rax
	movq	%rax, -144(%rbp)
.L865:
	xorl	%r15d, %r15d
.L864:
	movq	-136(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L849
	leaq	-97(%rbp), %rbx
.L863:
	movq	-120(%rbp), %rax
	movl	(%rax), %r13d
	movl	4(%rax), %r14d
	cmpl	%r15d, %r13d
	jl	.L888
	jle	.L851
	.p2align 4,,10
	.p2align 3
.L852:
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addl	$1, %r15d
	movb	$32, -97(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, %r13d
	jne	.L852
	movl	%r13d, %r15d
.L851:
	movl	%r14d, %eax
	subl	%r13d, %eax
	movl	%eax, %r13d
	movl	$32, %eax
	addl	$1, %r13d
	cmpl	$32, %r13d
	cmovg	%eax, %r13d
	movq	-136(%rbp), %rax
	movl	4(%rax), %eax
	movslq	%r13d, %rsi
	testb	$1, %al
	jne	.L889
	shrl	$7, %eax
	leaq	.LC26(%rip), %r9
	andl	$63, %eax
	cmpl	$32, %eax
	je	.L855
	movq	-152(%rbp), %rcx
	cltq
	movl	8(%rcx), %edx
	testl	%edx, %edx
	jne	.L856
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rcx
	movq	(%rcx,%rax,8), %r9
.L855:
	leaq	-96(%rbp), %r11
	movl	$32, %ecx
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%r11, %rdi
	leaq	.LC30(%rip), %r8
	movq	%r11, -128(%rbp)
	call	__snprintf_chk@PLT
	movq	-128(%rbp), %r11
	movl	%eax, %ecx
.L854:
	movq	%r11, %rdx
.L857:
	movl	(%rdx), %esi
	addq	$4, %rdx
	leal	-16843009(%rsi), %eax
	notl	%esi
	andl	%esi, %eax
	andl	$-2139062144, %eax
	je	.L857
	movl	%eax, %esi
	movl	%ecx, -128(%rbp)
	movq	%r12, %rdi
	shrl	$16, %esi
	testl	$32896, %eax
	cmove	%esi, %eax
	leaq	2(%rdx), %rsi
	cmove	%rsi, %rdx
	movq	%r11, %rsi
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rdx
	subl	$1, %r13d
	subq	%r11, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-128(%rbp), %ecx
	cmpl	%ecx, %r13d
	cmovg	%ecx, %r13d
	addl	%r13d, %r15d
	cmpl	%r15d, %r14d
	jl	.L890
	movq	-136(%rbp), %rax
	movl	4(%rax), %eax
	movl	%eax, -128(%rbp)
	movzbl	-128(%rbp), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%r13d, %r13d
	andl	$16, %r13d
	addl	$45, %r13d
	cmpl	%r15d, %r14d
	jle	.L861
	.p2align 4,,10
	.p2align 3
.L862:
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addl	$1, %r15d
	movb	%r13b, -97(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, %r14d
	jne	.L862
.L861:
	movq	-120(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	jne	.L863
.L849:
	movq	-136(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	jne	.L864
	leaq	-97(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	movb	$10, -97(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L891
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L889:
	.cfi_restore_state
	leaq	-96(%rbp), %r11
	movl	$32, %ecx
	movl	$1, %edx
	xorl	%eax, %eax
	movq	-144(%rbp), %r9
	movq	%r11, %rdi
	leaq	.LC30(%rip), %r8
	movq	%r11, -128(%rbp)
	call	__snprintf_chk@PLT
	movq	-128(%rbp), %r11
	movl	$3, %ecx
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L856:
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rcx
	movq	(%rcx,%rax,8), %r9
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L888:
	leaq	.LC29(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L890:
	leaq	.LC31(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L891:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23321:
	.size	_ZN2v88internal8compiler19LinearScanAllocator13PrintRangeRowERSoPKNS1_17TopLevelLiveRangeE, .-_ZN2v88internal8compiler19LinearScanAllocator13PrintRangeRowERSoPKNS1_17TopLevelLiveRangeE
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator18PrintRangeOverviewERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator18PrintRangeOverviewERSo
	.type	_ZN2v88internal8compiler19LinearScanAllocator18PrintRangeOverviewERSo, @function
_ZN2v88internal8compiler19LinearScanAllocator18PrintRangeOverviewERSo:
.LFB23322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	%r13, %rdi
	movq	16(%rax), %rax
	movq	16(%rax), %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_113PrintBlockRowERSoRKNS0_10ZoneVectorIPNS1_16InstructionBlockEEE
	movq	(%r14), %rax
	movq	200(%rax), %rbx
	movq	208(%rax), %r12
	cmpq	%r12, %rbx
	je	.L893
	.p2align 4,,10
	.p2align 3
.L895:
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L894
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator13PrintRangeRowERSoPKNS1_17TopLevelLiveRangeE
.L894:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L895
	movq	(%r14), %rax
.L893:
	movq	176(%rax), %r15
	movq	168(%rax), %rbx
	xorl	%edx, %edx
	cmpq	%rbx, %r15
	jne	.L899
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L897:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L892
.L899:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L897
	cmpq	$0, 16(%r12)
	je	.L897
	movl	4(%r12), %eax
	shrl	$13, %eax
	cmpb	$11, %al
	seta	%al
	movzbl	%al, %eax
	cmpl	%eax, 8(%r14)
	jne	.L897
	leal	1(%rdx), %eax
	imull	$-858993459, %edx, %edx
	movl	%eax, -52(%rbp)
	rorl	%edx
	cmpl	$429496729, %edx
	jbe	.L911
.L898:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler19LinearScanAllocator13PrintRangeRowERSoPKNS1_17TopLevelLiveRangeE
	movl	-52(%rbp), %edx
	cmpq	%rbx, %r15
	jne	.L899
.L892:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L911:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	%r13, %rdi
	movq	16(%rax), %rax
	movq	16(%rax), %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_113PrintBlockRowERSoRKNS0_10ZoneVectorIPNS1_16InstructionBlockEEE
	jmp	.L898
	.cfi_endproc
.LFE23322:
	.size	_ZN2v88internal8compiler19LinearScanAllocator18PrintRangeOverviewERSo, .-_ZN2v88internal8compiler19LinearScanAllocator18PrintRangeOverviewERSo
	.section	.rodata._ZN2v88internal8compiler10SpillRangeC2EPNS1_17TopLevelLiveRangeEPNS0_4ZoneE.str1.1,"aMS",@progbits,1
.LC32:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal8compiler10SpillRangeC2EPNS1_17TopLevelLiveRangeEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10SpillRangeC2EPNS1_17TopLevelLiveRangeEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler10SpillRangeC2EPNS1_17TopLevelLiveRangeEPNS0_4ZoneE, @function
_ZN2v88internal8compiler10SpillRangeC2EPNS1_17TopLevelLiveRangeEPNS0_4ZoneE:
.LFB23327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rdx, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	$-1, 40(%rdi)
	movl	4(%rsi), %eax
	movq	%rsi, -72(%rbp)
	movl	%eax, -56(%rbp)
	shrl	$13, %eax
	cmpb	$13, %al
	ja	.L913
	testb	%al, %al
	jne	.L956
.L915:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L913:
	cmpb	$14, %al
	jne	.L915
	movl	$16, %eax
	movq	%rsi, %r9
.L914:
	movl	%eax, 48(%r12)
	xorl	%r15d, %r15d
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L922:
	movq	16(%r9), %rbx
	testq	%rbx, %rbx
	jne	.L921
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L959:
	movq	%rax, 8(%r15)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L957
.L920:
	movq	%rax, %r15
.L921:
	movq	16(%r13), %rax
	movq	24(%r13), %rcx
	movq	(%rbx), %r14
	subq	%rax, %rcx
	cmpq	$15, %rcx
	jbe	.L958
	leaq	16(%rax), %rcx
	movq	%rcx, 16(%r13)
.L918:
	movq	%r14, (%rax)
	movq	$0, 8(%rax)
	testq	%rdx, %rdx
	jne	.L959
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L938
	movq	%rax, %rdx
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L938:
	movq	%rax, %r15
	movq	%rax, %rdx
.L916:
	movq	40(%r9), %r9
	testq	%r9, %r9
	jne	.L922
.L960:
	movq	%rdx, 32(%r12)
	movq	16(%r12), %rbx
	cmpq	24(%r12), %rbx
	je	.L923
	movq	-72(%rbp), %rax
	movq	%rax, (%rbx)
	addq	$8, 16(%r12)
.L924:
	movl	4(%r15), %eax
	movl	%eax, 40(%r12)
	movq	-72(%rbp), %rax
	movq	%r12, 104(%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L958:
	.cfi_restore_state
	movl	$16, %esi
	movq	%r13, %rdi
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L957:
	movq	40(%r9), %r9
	movq	%rax, %r15
	testq	%r9, %r9
	jne	.L922
	jmp	.L960
.L956:
	movl	$8, %eax
	movq	%rsi, %r9
	jmp	.L914
.L923:
	movq	8(%r12), %r13
	movq	%rbx, %rcx
	subq	%r13, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L961
	testq	%rax, %rax
	je	.L939
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L962
	movl	$2147483640, %esi
	movl	$2147483640, %r14d
.L926:
	movq	(%r12), %rdi
	movq	16(%rdi), %r9
	movq	24(%rdi), %rax
	subq	%r9, %rax
	cmpq	%rsi, %rax
	jb	.L963
	addq	%r9, %rsi
	movq	%rsi, 16(%rdi)
.L929:
	leaq	(%r9,%r14), %rdx
	leaq	8(%r9), %rax
.L927:
	movq	-72(%rbp), %rdi
	movq	%rdi, (%r9,%rcx)
	cmpq	%r13, %rbx
	je	.L930
	subq	$8, %rbx
	leaq	15(%r9), %rax
	subq	%r13, %rbx
	subq	%r13, %rax
	movq	%rbx, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L942
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L942
	leaq	1(%rcx), %rsi
	xorl	%eax, %eax
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L932:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%r9,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L932
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	addq	%rcx, %r13
	addq	%r9, %rcx
	cmpq	%rsi, %rax
	je	.L934
	movq	0(%r13), %rax
	movq	%rax, (%rcx)
.L934:
	leaq	16(%r9,%rbx), %rax
.L930:
	movq	%r9, %xmm0
	movq	%rax, %xmm2
	movq	%rdx, 24(%r12)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%r12)
	jmp	.L924
.L962:
	testq	%rdx, %rdx
	jne	.L964
	xorl	%edx, %edx
	movl	$8, %eax
	jmp	.L927
.L939:
	movl	$8, %esi
	movl	$8, %r14d
	jmp	.L926
.L942:
	xorl	%eax, %eax
.L931:
	movq	0(%r13,%rax,8), %rsi
	movq	%rsi, (%r9,%rax,8)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rsi, %rcx
	jne	.L931
	jmp	.L934
.L963:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r9
	jmp	.L929
.L961:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L964:
	movl	$268435455, %r14d
	cmpq	$268435455, %rdx
	cmova	%r14, %rdx
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rsi
	jmp	.L926
	.cfi_endproc
.LFE23327:
	.size	_ZN2v88internal8compiler10SpillRangeC2EPNS1_17TopLevelLiveRangeEPNS0_4ZoneE, .-_ZN2v88internal8compiler10SpillRangeC2EPNS1_17TopLevelLiveRangeEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler10SpillRangeC1EPNS1_17TopLevelLiveRangeEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler10SpillRangeC1EPNS1_17TopLevelLiveRangeEPNS0_4ZoneE,_ZN2v88internal8compiler10SpillRangeC2EPNS1_17TopLevelLiveRangeEPNS0_4ZoneE
	.section	.text._ZNK2v88internal8compiler10SpillRange18IsIntersectingWithEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler10SpillRange18IsIntersectingWithEPS2_
	.type	_ZNK2v88internal8compiler10SpillRange18IsIntersectingWithEPS2_, @function
_ZNK2v88internal8compiler10SpillRange18IsIntersectingWithEPS2_:
.LFB23329:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L974
	movq	32(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L974
	movl	(%rdx), %ecx
	xorl	%r8d, %r8d
	cmpl	%ecx, 40(%rdi)
	jle	.L965
	movl	(%rax), %edi
	cmpl	40(%rsi), %edi
	jl	.L967
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L977:
	cmpl	4(%rax), %ecx
	jl	.L976
	movq	8(%rax), %rax
.L969:
	testq	%rax, %rax
	je	.L974
	testq	%rdx, %rdx
	je	.L974
	movl	(%rdx), %ecx
	movl	(%rax), %edi
.L967:
	cmpl	%edi, %ecx
	jg	.L977
	cmpl	%edi, 4(%rdx)
	jg	.L976
	movq	8(%rdx), %rdx
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L976:
	movl	$1, %r8d
.L965:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L974:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE23329:
	.size	_ZNK2v88internal8compiler10SpillRange18IsIntersectingWithEPS2_, .-_ZNK2v88internal8compiler10SpillRange18IsIntersectingWithEPS2_
	.section	.text._ZN2v88internal8compiler10SpillRange22MergeDisjointIntervalsEPNS1_11UseIntervalE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10SpillRange22MergeDisjointIntervalsEPNS1_11UseIntervalE
	.type	_ZN2v88internal8compiler10SpillRange22MergeDisjointIntervalsEPNS1_11UseIntervalE, @function
_ZN2v88internal8compiler10SpillRange22MergeDisjointIntervalsEPNS1_11UseIntervalE:
.LFB23331:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	testq	%rsi, %rsi
	je	.L978
	xorl	%ecx, %ecx
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L992:
	movq	%rax, %rdx
	testq	%rcx, %rcx
	je	.L990
.L983:
	movq	%rdx, 8(%rcx)
	movq	8(%rdx), %rax
.L984:
	movq	%rdx, %rcx
.L985:
	testq	%rax, %rax
	je	.L991
	movl	(%rsi), %edx
	cmpl	%edx, (%rax)
	jle	.L992
	movq	%rsi, %rdx
	movq	%rax, %rsi
	testq	%rcx, %rcx
	jne	.L983
.L990:
	movq	%rdx, 32(%rdi)
	movq	8(%rdx), %rax
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L991:
	testq	%rcx, %rcx
	je	.L993
	movq	%rsi, 8(%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L978:
	ret
	.p2align 4,,10
	.p2align 3
.L993:
	movq	%rsi, 32(%rdi)
	ret
	.cfi_endproc
.LFE23331:
	.size	_ZN2v88internal8compiler10SpillRange22MergeDisjointIntervalsEPNS1_11UseIntervalE, .-_ZN2v88internal8compiler10SpillRange22MergeDisjointIntervalsEPNS1_11UseIntervalE
	.section	.text._ZNK2v88internal8compiler10SpillRange5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler10SpillRange5PrintEv
	.type	_ZNK2v88internal8compiler10SpillRange5PrintEv, @function
_ZNK2v88internal8compiler10SpillRange5PrintEv:
.LFB23332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-400(%rbp), %r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rax, -320(%rbp)
	movq	%r12, %rdi
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, -400(%rbp)
	leaq	.LC19(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-160(%rbp,%rax), %r13
	testq	%r13, %r13
	je	.L999
	cmpb	$0, 56(%r13)
	je	.L996
	movsbl	67(%r13), %esi
.L997:
	movq	%r12, %rdi
	leaq	.LC16(%rip), %r13
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	16(%r14), %rbx
	movq	8(%r14), %r15
	cmpq	%rbx, %r15
	je	.L1002
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	(%r15), %rax
	movq	%r12, %rdi
	addq	$8, %r15
	movl	88(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%r15, %rbx
	jne	.L1001
.L1002:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-160(%rbp,%rax), %r13
	testq	%r13, %r13
	je	.L999
	cmpb	$0, 56(%r13)
	je	.L1003
	movsbl	67(%r13), %esi
.L1004:
	movq	%r12, %rdi
	leaq	-401(%rbp), %r13
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	32(%r14), %rbx
	testq	%rbx, %rbx
	jne	.L1005
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1036:
	movsbl	67(%r14), %esi
.L1008:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1009
.L1005:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$91, -401(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compilerlsERSoNS1_16LifetimePositionE
	movl	$2, %edx
	leaq	.LC21(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%rbx), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compilerlsERSoNS1_16LifetimePositionE
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$41, -401(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %r14
	testq	%r14, %r14
	je	.L999
	cmpb	$0, 56(%r14)
	jne	.L1036
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1008
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1009:
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-160(%rbp,%rax), %r13
	testq	%r13, %r13
	je	.L999
	cmpb	$0, 56(%r13)
	je	.L1010
	movsbl	67(%r13), %esi
.L1011:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-424(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1037
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L996:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L997
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L1003:
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1004
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1011
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1011
.L999:
	call	_ZSt16__throw_bad_castv@PLT
.L1037:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23332:
	.size	_ZNK2v88internal8compiler10SpillRange5PrintEv, .-_ZNK2v88internal8compiler10SpillRange5PrintEv
	.section	.rodata._ZN2v88internal8compiler22RegisterAllocationData11PhiMapValueC2EPNS1_14PhiInstructionEPKNS1_16InstructionBlockEPNS0_4ZoneE.str1.1,"aMS",@progbits,1
.LC33:
	.string	"vector::reserve"
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData11PhiMapValueC2EPNS1_14PhiInstructionEPKNS1_16InstructionBlockEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValueC2EPNS1_14PhiInstructionEPKNS1_16InstructionBlockEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValueC2EPNS1_14PhiInstructionEPKNS1_16InstructionBlockEPNS0_4ZoneE, @function
_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValueC2EPNS1_14PhiInstructionEPKNS1_16InstructionBlockEPNS0_4ZoneE:
.LFB23337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movq	%rcx, 16(%rdi)
	movq	$0, 24(%rdi)
	movups	%xmm0, 32(%rdi)
	movq	32(%rsi), %rax
	subq	24(%rsi), %rax
	movq	%rax, %rdx
	movl	$32, 48(%rdi)
	sarq	$2, %rdx
	cmpq	$1073741823, %rax
	ja	.L1047
	testq	%rdx, %rdx
	jne	.L1048
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1048:
	.cfi_restore_state
	leaq	0(,%rdx,8), %r12
	movq	24(%rcx), %rax
	movq	16(%rcx), %rdx
	movq	%rdi, %rbx
	movq	%r12, %rsi
	subq	%rdx, %rax
	cmpq	%rax, %r12
	ja	.L1049
	addq	%rdx, %rsi
	movq	%rsi, 16(%rcx)
.L1042:
	movq	%rdx, 24(%rbx)
	movq	%rdx, 32(%rbx)
	addq	%r12, %rdx
	movq	%rdx, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1049:
	.cfi_restore_state
	movq	%rcx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1042
.L1047:
	leaq	.LC33(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23337:
	.size	_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValueC2EPNS1_14PhiInstructionEPKNS1_16InstructionBlockEPNS0_4ZoneE, .-_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValueC2EPNS1_14PhiInstructionEPKNS1_16InstructionBlockEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValueC1EPNS1_14PhiInstructionEPKNS1_16InstructionBlockEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValueC1EPNS1_14PhiInstructionEPKNS1_16InstructionBlockEPNS0_4ZoneE,_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValueC2EPNS1_14PhiInstructionEPKNS1_16InstructionBlockEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData11PhiMapValue16CommitAssignmentERKNS1_18InstructionOperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValue16CommitAssignmentERKNS1_18InstructionOperandE
	.type	_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValue16CommitAssignmentERKNS1_18InstructionOperandE, @function
_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValue16CommitAssignmentERKNS1_18InstructionOperandE:
.LFB23340:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	32(%rdi), %rdi
	cmpq	%rdi, %rax
	je	.L1050
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	(%rax), %rdx
	movq	(%rsi), %rcx
	addq	$8, %rax
	movq	%rcx, (%rdx)
	cmpq	%rax, %rdi
	jne	.L1052
.L1050:
	ret
	.cfi_endproc
.LFE23340:
	.size	_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValue16CommitAssignmentERKNS1_18InstructionOperandE, .-_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValue16CommitAssignmentERKNS1_18InstructionOperandE
	.section	.rodata._ZN2v88internal8compiler22RegisterAllocationDataC2EPKNS0_21RegisterConfigurationEPNS0_4ZoneEPNS1_5FrameEPNS1_19InstructionSequenceENS_4base5FlagsINS1_22RegisterAllocationFlagEiEEPNS0_11TickCounterEPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal8compiler22RegisterAllocationDataC2EPKNS0_21RegisterConfigurationEPNS0_4ZoneEPNS1_5FrameEPNS1_19InstructionSequenceENS_4base5FlagsINS1_22RegisterAllocationFlagEiEEPNS0_11TickCounterEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationDataC2EPKNS0_21RegisterConfigurationEPNS0_4ZoneEPNS1_5FrameEPNS1_19InstructionSequenceENS_4base5FlagsINS1_22RegisterAllocationFlagEiEEPNS0_11TickCounterEPKc
	.type	_ZN2v88internal8compiler22RegisterAllocationDataC2EPKNS0_21RegisterConfigurationEPNS0_4ZoneEPNS1_5FrameEPNS1_19InstructionSequenceENS_4base5FlagsINS1_22RegisterAllocationFlagEiEEPNS0_11TickCounterEPKc, @function
_ZN2v88internal8compiler22RegisterAllocationDataC2EPKNS0_21RegisterConfigurationEPNS0_4ZoneEPNS1_5FrameEPNS1_19InstructionSequenceENS_4base5FlagsINS1_22RegisterAllocationFlagEiEEPNS0_11TickCounterEPKc:
.LFB23375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm0
	movq	%rcx, %xmm5
	leaq	56(%rdi), %rax
	punpcklqdq	%xmm5, %xmm0
	movq	%rdx, %xmm6
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movups	%xmm0, (%rdi)
	movq	%r8, %xmm0
	movhps	24(%rbp), %xmm0
	movl	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	%rax, 72(%rdi)
	movq	%rax, 80(%rdi)
	movq	$0, 88(%rdi)
	movq	16(%r8), %rdx
	movups	%xmm0, 16(%rdi)
	movq	%rsi, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 32(%rdi)
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	$3, %rax
	cltq
	cmpq	$268435455, %rax
	ja	.L1059
	movq	%r13, 96(%rdi)
	movq	%rdi, %rbx
	movq	%r8, %r12
	movl	%r9d, %r14d
	movq	$0, 104(%rdi)
	leaq	0(,%rax,8), %rdx
	movq	$0, 112(%rdi)
	movq	$0, 120(%rdi)
	testq	%rax, %rax
	je	.L1056
	movq	16(%r13), %rdi
	movq	24(%r13), %rax
	movq	%rdx, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L1144
	addq	%rdi, %rsi
	movq	%rsi, 16(%r13)
.L1058:
	leaq	(%rdi,%rdx), %r15
	movq	%rdi, 104(%rbx)
	xorl	%esi, %esi
	movq	%r15, 120(%rbx)
	call	memset@PLT
	movq	(%rbx), %rdi
.L1123:
	movq	%r15, 112(%rbx)
	movq	16(%r12), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	$3, %rax
	cltq
	cmpq	$268435455, %rax
	ja	.L1059
	movq	%rdi, 128(%rbx)
	xorl	%r15d, %r15d
	leaq	0(,%rax,8), %rdx
	movq	$0, 136(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 152(%rbx)
	testq	%rax, %rax
	je	.L1122
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	movq	%rdx, %rsi
	subq	%r8, %rax
	cmpq	%rax, %rdx
	ja	.L1145
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L1062:
	leaq	(%r8,%rdx), %r15
	movq	%r8, 136(%rbx)
	movq	%r8, %rdi
	xorl	%esi, %esi
	movq	%r15, 152(%rbx)
	call	memset@PLT
	movq	(%rbx), %rdi
.L1122:
	movq	%r15, 144(%rbx)
	movl	272(%r12), %eax
	addl	%eax, %eax
	cltq
	cmpq	$268435455, %rax
	ja	.L1059
	movq	%rdi, 160(%rbx)
	xorl	%r15d, %r15d
	leaq	0(,%rax,8), %rdx
	movq	$0, 168(%rbx)
	movq	$0, 176(%rbx)
	movq	$0, 184(%rbx)
	testq	%rax, %rax
	je	.L1121
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	movq	%rdx, %rsi
	subq	%r8, %rax
	cmpq	%rax, %rdx
	ja	.L1146
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L1065:
	leaq	(%r8,%rdx), %r15
	movq	%r8, 168(%rbx)
	movq	%r8, %rdi
	xorl	%esi, %esi
	movq	%r15, 184(%rbx)
	call	memset@PLT
	movq	(%rbx), %rdi
.L1121:
	movq	32(%rbx), %rdx
	movq	%r15, 176(%rbx)
	movl	8(%rdx), %eax
	addl	%eax, %eax
	cltq
	cmpq	$268435455, %rax
	ja	.L1059
	movq	%rdi, 192(%rbx)
	xorl	%r15d, %r15d
	leaq	0(,%rax,8), %r9
	movq	$0, 200(%rbx)
	movq	$0, 208(%rbx)
	movq	$0, 216(%rbx)
	testq	%rax, %rax
	je	.L1120
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	movq	%r9, %rsi
	subq	%r8, %rax
	cmpq	%rax, %r9
	ja	.L1147
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L1068:
	leaq	(%r8,%r9), %r15
	movq	%r8, 200(%rbx)
	movq	%r9, %rdx
	xorl	%esi, %esi
	movq	%r15, 216(%rbx)
	movq	%r8, %rdi
	call	memset@PLT
	movq	(%rbx), %rdi
	movq	32(%rbx), %rdx
.L1120:
	movq	%r15, 208(%rbx)
	movq	%rdi, 224(%rbx)
	movq	$0, 232(%rbx)
	movq	$0, 240(%rbx)
	movq	$0, 248(%rbx)
	movl	16(%rdx), %eax
	addl	%eax, %eax
	cltq
	cmpq	$268435455, %rax
	ja	.L1059
	movq	%rdi, 256(%rbx)
	xorl	%r15d, %r15d
	leaq	0(,%rax,8), %rdx
	movq	$0, 264(%rbx)
	movq	$0, 272(%rbx)
	movq	$0, 280(%rbx)
	testq	%rax, %rax
	je	.L1119
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	movq	%rdx, %rsi
	subq	%r8, %rax
	cmpq	%rax, %rdx
	ja	.L1148
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L1071:
	leaq	(%r8,%rdx), %r15
	movq	%r8, 264(%rbx)
	movq	%r8, %rdi
	xorl	%esi, %esi
	movq	%r15, 280(%rbx)
	call	memset@PLT
	movq	(%rbx), %rdi
.L1119:
	movq	%r15, 272(%rbx)
	movq	%rdi, 288(%rbx)
	movq	$0, 296(%rbx)
	movq	$0, 304(%rbx)
	movq	$0, 312(%rbx)
	movslq	272(%r12), %rax
	cmpq	$268435455, %rax
	ja	.L1059
	movq	%rdi, 320(%rbx)
	xorl	%r15d, %r15d
	leaq	0(,%rax,8), %rdx
	movq	$0, 328(%rbx)
	movq	$0, 336(%rbx)
	movq	$0, 344(%rbx)
	testq	%rax, %rax
	je	.L1118
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	movq	%rdx, %rsi
	subq	%r8, %rax
	cmpq	%rax, %rdx
	ja	.L1149
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L1074:
	leaq	(%r8,%rdx), %r15
	movq	%r8, 328(%rbx)
	movq	%r8, %rdi
	xorl	%esi, %esi
	movq	%r15, 344(%rbx)
	call	memset@PLT
	movq	(%rbx), %rdi
.L1118:
	movq	%r15, 336(%rbx)
	movq	%rdi, 352(%rbx)
	movq	$0, 360(%rbx)
	movq	$0, 368(%rbx)
	movq	$0, 376(%rbx)
	movl	272(%r12), %eax
	movq	%r13, 424(%rbx)
	movl	%eax, 416(%rbx)
	movq	$0, 432(%rbx)
	movq	$0, 440(%rbx)
	movq	$0, 448(%rbx)
	movq	16(%r12), %rax
	movq	$0, 384(%rbx)
	movq	$0, 392(%rbx)
	movq	16(%rax), %r12
	subq	8(%rax), %r12
	sarq	$3, %r12
	movslq	%r12d, %r12
	cmpq	$67108863, %r12
	ja	.L1059
	movq	%r12, %r15
	xorl	%esi, %esi
	movq	%r13, 456(%rbx)
	movq	$0, 464(%rbx)
	salq	$5, %r15
	movq	$0, 472(%rbx)
	movq	$0, 480(%rbx)
	testq	%r12, %r12
	je	.L1117
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	movq	%r15, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %r15
	ja	.L1150
	addq	%rax, %rsi
	movq	%rsi, 16(%r13)
.L1077:
	leaq	(%rax,%r15), %rsi
	leaq	-1(%r12), %rcx
	movq	%rax, 464(%rbx)
	movq	%rax, %rdx
	movq	%rsi, 480(%rbx)
	cmpq	$2, %rcx
	jbe	.L1124
	movq	%r13, %xmm0
	pxor	%xmm2, %xmm2
	movq	%r12, %rcx
	punpcklqdq	%xmm0, %xmm0
	shrq	%rcx
	movdqa	%xmm0, %xmm1
	punpcklqdq	%xmm2, %xmm0
	salq	$6, %rcx
	psrldq	$8, %xmm1
	movdqa	%xmm0, %xmm3
	addq	%rax, %rcx
	movdqa	%xmm1, %xmm4
	psrldq	$8, %xmm0
	punpcklqdq	%xmm2, %xmm3
	punpcklqdq	%xmm2, %xmm4
	psrldq	$8, %xmm1
	movdqa	%xmm4, %xmm2
	.p2align 4,,10
	.p2align 3
.L1079:
	movups	%xmm3, (%rax)
	addq	$64, %rax
	movups	%xmm0, -48(%rax)
	movups	%xmm2, -32(%rax)
	movups	%xmm1, -16(%rax)
	cmpq	%rax, %rcx
	jne	.L1079
	movq	%r12, %rax
	movq	%r12, %rcx
	andq	$-2, %rax
	andl	$1, %ecx
	movq	%rax, %rdi
	salq	$5, %rdi
	addq	%rdi, %rdx
	cmpq	%rax, %r12
	je	.L1117
.L1078:
	movq	%r13, (%rdx)
	movq	$0, 8(%rdx)
	movq	$0, 16(%rdx)
	movq	$0, 24(%rdx)
	cmpq	$1, %rcx
	je	.L1117
	movq	%r13, 32(%rdx)
	movq	$0, 40(%rdx)
	movq	$0, 48(%rdx)
	movq	$0, 56(%rdx)
	cmpq	$2, %rcx
	je	.L1117
	movq	%r13, 64(%rdx)
	movq	$0, 72(%rdx)
	movq	$0, 80(%rdx)
	movq	$0, 88(%rdx)
.L1117:
	movq	16(%rbp), %rax
	movq	%rsi, 472(%rbx)
	movl	%r14d, 488(%rbx)
	movq	%rax, 496(%rbx)
	movq	32(%rbx), %rax
	movl	8(%rax), %r12d
	movq	16(%rbx), %rax
	movq	8(%rax), %r14
	movq	16(%r14), %r13
	movq	24(%r14), %rax
	subq	%r13, %rax
	cmpq	$15, %rax
	jbe	.L1151
	leaq	16(%r13), %rax
	movq	%rax, 16(%r14)
.L1082:
	movl	%r12d, 0(%r13)
	cmpl	$64, %r12d
	jle	.L1152
	subl	$1, %r12d
	movq	$0, 8(%r13)
	sarl	$6, %r12d
	addl	$1, %r12d
	movl	%r12d, 4(%r13)
	movq	16(%r14), %rax
	movslq	%r12d, %r12
	movq	24(%r14), %rdx
	leaq	0(,%r12,8), %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1153
	addq	%rax, %rsi
	movq	%rsi, 16(%r14)
.L1086:
	movl	4(%r13), %edx
	movq	%rax, 8(%r13)
	cmpl	$1, %edx
	je	.L1154
	testl	%edx, %edx
	jle	.L1084
	movq	$0, (%rax)
	cmpl	$1, 4(%r13)
	jle	.L1084
	movl	$8, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1089:
	movq	8(%r13), %rcx
	addl	$1, %eax
	movq	$0, (%rcx,%rdx)
	addq	$8, %rdx
	cmpl	%eax, 4(%r13)
	jg	.L1089
.L1084:
	movq	32(%rbx), %rax
	movq	%r13, 384(%rbx)
	movl	16(%rax), %r12d
	movq	16(%rbx), %rax
	movq	8(%rax), %r14
	movq	16(%r14), %r13
	movq	24(%r14), %rax
	subq	%r13, %rax
	cmpq	$15, %rax
	jbe	.L1155
	leaq	16(%r13), %rax
	movq	%rax, 16(%r14)
.L1091:
	movl	%r12d, 0(%r13)
	cmpl	$64, %r12d
	jle	.L1156
	subl	$1, %r12d
	movq	$0, 8(%r13)
	sarl	$6, %r12d
	addl	$1, %r12d
	movl	%r12d, 4(%r13)
	movq	16(%r14), %rax
	movslq	%r12d, %r12
	movq	24(%r14), %rdx
	leaq	0(,%r12,8), %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1157
	addq	%rax, %rsi
	movq	%rsi, 16(%r14)
.L1095:
	movl	4(%r13), %edx
	movq	%rax, 8(%r13)
	cmpl	$1, %edx
	je	.L1158
	testl	%edx, %edx
	jle	.L1093
	movq	$0, (%rax)
	cmpl	$1, 4(%r13)
	jle	.L1093
	movl	$8, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1098:
	movq	8(%r13), %rcx
	addl	$1, %eax
	movq	$0, (%rcx,%rdx)
	addq	$8, %rdx
	cmpl	%eax, 4(%r13)
	jg	.L1098
.L1093:
	movq	32(%rbx), %rax
	movq	%r13, 392(%rbx)
	movl	8(%rax), %r12d
	movq	16(%rbx), %rax
	movq	8(%rax), %r14
	movq	16(%r14), %r13
	movq	24(%r14), %rax
	subq	%r13, %rax
	cmpq	$15, %rax
	jbe	.L1159
	leaq	16(%r13), %rax
	movq	%rax, 16(%r14)
.L1100:
	movl	%r12d, 0(%r13)
	cmpl	$64, %r12d
	jle	.L1160
	subl	$1, %r12d
	movq	$0, 8(%r13)
	sarl	$6, %r12d
	addl	$1, %r12d
	movl	%r12d, 4(%r13)
	movq	16(%r14), %rax
	movslq	%r12d, %r12
	movq	24(%r14), %rdx
	leaq	0(,%r12,8), %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1161
	addq	%rax, %rsi
	movq	%rsi, 16(%r14)
.L1104:
	movl	4(%r13), %edx
	movq	%rax, 8(%r13)
	cmpl	$1, %edx
	je	.L1162
	testl	%edx, %edx
	jle	.L1102
	movq	$0, (%rax)
	cmpl	$1, 4(%r13)
	jle	.L1102
	movl	$8, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1107:
	movq	8(%r13), %rcx
	addl	$1, %eax
	movq	$0, (%rcx,%rdx)
	addq	$8, %rdx
	cmpl	%eax, 4(%r13)
	jg	.L1107
.L1102:
	movq	32(%rbx), %rax
	movq	%r13, 400(%rbx)
	movl	16(%rax), %r12d
	movq	16(%rbx), %rax
	movq	8(%rax), %r14
	movq	16(%r14), %r13
	movq	24(%r14), %rax
	subq	%r13, %rax
	cmpq	$15, %rax
	jbe	.L1163
	leaq	16(%r13), %rax
	movq	%rax, 16(%r14)
.L1109:
	movl	%r12d, 0(%r13)
	cmpl	$64, %r12d
	jle	.L1164
	subl	$1, %r12d
	movq	$0, 8(%r13)
	sarl	$6, %r12d
	addl	$1, %r12d
	movl	%r12d, 4(%r13)
	movq	16(%r14), %rax
	movslq	%r12d, %r12
	movq	24(%r14), %rdx
	leaq	0(,%r12,8), %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1165
	addq	%rax, %rsi
	movq	%rsi, 16(%r14)
.L1113:
	movl	4(%r13), %edx
	movq	%rax, 8(%r13)
	cmpl	$1, %edx
	je	.L1166
	testl	%edx, %edx
	jle	.L1111
	movq	$0, (%rax)
	cmpl	$1, 4(%r13)
	jle	.L1111
	movl	$8, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	8(%r13), %rcx
	addl	$1, %eax
	movq	$0, (%rcx,%rdx)
	addq	$8, %rdx
	cmpl	%eax, 4(%r13)
	jg	.L1116
.L1111:
	movq	8(%rbx), %rax
	movq	384(%rbx), %rdx
	movq	%r13, 408(%rbx)
	movq	%rdx, 16(%rax)
	movq	8(%rbx), %rax
	movq	392(%rbx), %rdx
	movq	%rdx, 24(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1164:
	.cfi_restore_state
	movl	$1, 4(%r13)
	movq	$0, 8(%r13)
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1160:
	movl	$1, 4(%r13)
	movq	$0, 8(%r13)
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1156:
	movl	$1, 4(%r13)
	movq	$0, 8(%r13)
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1152:
	movl	$1, 4(%r13)
	movq	$0, 8(%r13)
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1144:
	movq	%r13, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1166:
	movq	$0, 8(%r13)
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	$0, 8(%r13)
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1158:
	movq	$0, 8(%r13)
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1154:
	movq	$0, 8(%r13)
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1163:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1151:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1159:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1155:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1124:
	movq	%r12, %rcx
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1146:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1145:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1148:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1147:
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r9
	movq	%rax, %r8
	jmp	.L1068
	.p2align 4,,10
	.p2align 3
.L1150:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1149:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1165:
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1153:
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1161:
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1104
.L1059:
	leaq	.LC34(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23375:
	.size	_ZN2v88internal8compiler22RegisterAllocationDataC2EPKNS0_21RegisterConfigurationEPNS0_4ZoneEPNS1_5FrameEPNS1_19InstructionSequenceENS_4base5FlagsINS1_22RegisterAllocationFlagEiEEPNS0_11TickCounterEPKc, .-_ZN2v88internal8compiler22RegisterAllocationDataC2EPKNS0_21RegisterConfigurationEPNS0_4ZoneEPNS1_5FrameEPNS1_19InstructionSequenceENS_4base5FlagsINS1_22RegisterAllocationFlagEiEEPNS0_11TickCounterEPKc
	.globl	_ZN2v88internal8compiler22RegisterAllocationDataC1EPKNS0_21RegisterConfigurationEPNS0_4ZoneEPNS1_5FrameEPNS1_19InstructionSequenceENS_4base5FlagsINS1_22RegisterAllocationFlagEiEEPNS0_11TickCounterEPKc
	.set	_ZN2v88internal8compiler22RegisterAllocationDataC1EPKNS0_21RegisterConfigurationEPNS0_4ZoneEPNS1_5FrameEPNS1_19InstructionSequenceENS_4base5FlagsINS1_22RegisterAllocationFlagEiEEPNS0_11TickCounterEPKc,_ZN2v88internal8compiler22RegisterAllocationDataC2EPKNS0_21RegisterConfigurationEPNS0_4ZoneEPNS1_5FrameEPNS1_19InstructionSequenceENS_4base5FlagsINS1_22RegisterAllocationFlagEiEEPNS0_11TickCounterEPKc
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData17RepresentationForEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData17RepresentationForEi
	.type	_ZN2v88internal8compiler22RegisterAllocationData17RepresentationForEi, @function
_ZN2v88internal8compiler22RegisterAllocationData17RepresentationForEi:
.LFB23378:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	.cfi_endproc
.LFE23378:
	.size	_ZN2v88internal8compiler22RegisterAllocationData17RepresentationForEi, .-_ZN2v88internal8compiler22RegisterAllocationData17RepresentationForEi
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData12NewLiveRangeEiNS0_21MachineRepresentationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData12NewLiveRangeEiNS0_21MachineRepresentationE
	.type	_ZN2v88internal8compiler22RegisterAllocationData12NewLiveRangeEiNS0_21MachineRepresentationE, @function
_ZN2v88internal8compiler22RegisterAllocationData12NewLiveRangeEiNS0_21MachineRepresentationE:
.LFB23380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	movl	%edx, %ebx
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$159, %rdx
	jbe	.L1172
	leaq	160(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1170:
	movzbl	%bl, %ebx
	pxor	%xmm0, %xmm0
	movq	%rax, 32(%rax)
	sall	$13, %ebx
	movl	$0, (%rax)
	orl	$134221824, %ebx
	movq	$0, 24(%rax)
	movl	%ebx, 4(%rax)
	movl	%r12d, 88(%rax)
	movl	$0, 92(%rax)
	movq	$0, 112(%rax)
	movb	$0, 120(%rax)
	movl	$2147483647, 124(%rax)
	movq	$0, 128(%rax)
	movq	%rax, 136(%rax)
	movq	$0, 144(%rax)
	movb	$0, 152(%rax)
	movups	%xmm0, 8(%rax)
	movups	%xmm0, 40(%rax)
	movups	%xmm0, 56(%rax)
	movups	%xmm0, 72(%rax)
	movups	%xmm0, 96(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1172:
	.cfi_restore_state
	movl	$160, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1170
	.cfi_endproc
.LFE23380:
	.size	_ZN2v88internal8compiler22RegisterAllocationData12NewLiveRangeEiNS0_21MachineRepresentationE, .-_ZN2v88internal8compiler22RegisterAllocationData12NewLiveRangeEiNS0_21MachineRepresentationE
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData17GetPhiMapValueForEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData17GetPhiMapValueForEi
	.type	_ZN2v88internal8compiler22RegisterAllocationData17GetPhiMapValueForEi, @function
_ZN2v88internal8compiler22RegisterAllocationData17GetPhiMapValueForEi:
.LFB23392:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	leaq	56(%rdi), %rcx
	testq	%rax, %rax
	je	.L1174
	movq	%rcx, %rdx
	jmp	.L1175
	.p2align 4,,10
	.p2align 3
.L1183:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1176
.L1175:
	cmpl	%esi, 32(%rax)
	jge	.L1183
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1175
.L1176:
	cmpq	%rdx, %rcx
	je	.L1174
	cmpl	%esi, 32(%rdx)
	cmovle	%rdx, %rcx
.L1174:
	movq	40(%rcx), %rax
	ret
	.cfi_endproc
.LFE23392:
	.size	_ZN2v88internal8compiler22RegisterAllocationData17GetPhiMapValueForEi, .-_ZN2v88internal8compiler22RegisterAllocationData17GetPhiMapValueForEi
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData17GetPhiMapValueForEPNS1_17TopLevelLiveRangeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData17GetPhiMapValueForEPNS1_17TopLevelLiveRangeE
	.type	_ZN2v88internal8compiler22RegisterAllocationData17GetPhiMapValueForEPNS1_17TopLevelLiveRangeE, @function
_ZN2v88internal8compiler22RegisterAllocationData17GetPhiMapValueForEPNS1_17TopLevelLiveRangeE:
.LFB23393:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	movl	88(%rsi), %edx
	leaq	56(%rdi), %rsi
	testq	%rax, %rax
	je	.L1185
	movq	%rsi, %rcx
	jmp	.L1186
	.p2align 4,,10
	.p2align 3
.L1194:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1187
.L1186:
	cmpl	32(%rax), %edx
	jle	.L1194
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1186
.L1187:
	cmpq	%rcx, %rsi
	je	.L1185
	cmpl	32(%rcx), %edx
	cmovge	%rcx, %rsi
.L1185:
	movq	40(%rsi), %rax
	ret
	.cfi_endproc
.LFE23393:
	.size	_ZN2v88internal8compiler22RegisterAllocationData17GetPhiMapValueForEPNS1_17TopLevelLiveRangeE, .-_ZN2v88internal8compiler22RegisterAllocationData17GetPhiMapValueForEPNS1_17TopLevelLiveRangeE
	.section	.rodata._ZN2v88internal8compiler22RegisterAllocationData37RangesDefinedInDeferredStayInDeferredEv.str1.8,"aMS",@progbits,1
	.align 8
.LC35:
	.string	"live_ranges_size == live_ranges().size()"
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData37RangesDefinedInDeferredStayInDeferredEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData37RangesDefinedInDeferredStayInDeferredEv
	.type	_ZN2v88internal8compiler22RegisterAllocationData37RangesDefinedInDeferredStayInDeferredEv, @function
_ZN2v88internal8compiler22RegisterAllocationData37RangesDefinedInDeferredStayInDeferredEv:
.LFB23395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	176(%rdi), %r14
	movq	168(%rdi), %r12
	movq	%r14, %rax
	subq	%r12, %rax
	movq	%rax, -56(%rbp)
	cmpq	%r14, %r12
	je	.L1206
	movq	(%r12), %rbx
	movq	%rdi, %r15
	addq	$8, %r12
.L1198:
	testq	%rbx, %rbx
	je	.L1199
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L1199
	movl	(%rax), %eax
	movq	16(%r15), %rdi
	testl	%eax, %eax
	leal	3(%rax), %esi
	cmovns	%eax, %esi
	sarl	$2, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	cmpb	$0, 120(%rax)
	jne	.L1227
.L1199:
	cmpq	%r12, %r14
	je	.L1206
	movq	(%r12), %rbx
	movq	176(%r15), %rax
	addq	$8, %r12
	subq	168(%r15), %rax
	cmpq	-56(%rbp), %rax
	je	.L1198
	leaq	.LC35(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1227:
	movq	16(%rbx), %r13
	testq	%r13, %r13
	je	.L1199
.L1204:
	movl	0(%r13), %eax
	testl	%eax, %eax
	leal	3(%rax), %esi
	cmovns	%eax, %esi
	andl	$2, %eax
	sarl	$2, %esi
	cmpl	$1, %eax
	movl	4(%r13), %eax
	sbbl	$-1, %esi
	leal	3(%rax), %ebx
	testl	%eax, %eax
	cmovns	%eax, %ebx
	sarl	$2, %ebx
	testb	$2, %al
	jne	.L1201
	leal	-1(%rbx), %ecx
	testb	$1, %al
	cmove	%ecx, %ebx
.L1201:
	cmpl	%esi, %ebx
	jge	.L1203
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1228:
	movl	116(%rax), %esi
	cmpl	%ebx, %esi
	jg	.L1202
.L1203:
	movq	16(%r15), %rdi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movzbl	120(%rax), %r8d
	testb	%r8b, %r8b
	jne	.L1228
.L1195:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1202:
	.cfi_restore_state
	movq	8(%r13), %r13
	testq	%r13, %r13
	jne	.L1204
	jmp	.L1199
.L1206:
	movl	$1, %r8d
	jmp	.L1195
	.cfi_endproc
.LFE23395:
	.size	_ZN2v88internal8compiler22RegisterAllocationData37RangesDefinedInDeferredStayInDeferredEv, .-_ZN2v88internal8compiler22RegisterAllocationData37RangesDefinedInDeferredStayInDeferredEv
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData27AssignSpillRangeToLiveRangeEPNS1_17TopLevelLiveRangeENS2_9SpillModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData27AssignSpillRangeToLiveRangeEPNS1_17TopLevelLiveRangeENS2_9SpillModeE
	.type	_ZN2v88internal8compiler22RegisterAllocationData27AssignSpillRangeToLiveRangeEPNS1_17TopLevelLiveRangeENS2_9SpillModeE, @function
_ZN2v88internal8compiler22RegisterAllocationData27AssignSpillRangeToLiveRangeEPNS1_17TopLevelLiveRangeENS2_9SpillModeE:
.LFB23396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	104(%rsi), %rax
	testq	%rax, %rax
	je	.L1241
.L1230:
	movl	4(%rbx), %ecx
	movl	%ecx, %edx
	andl	$-97, %edx
	cmpl	$1, %r13d
	je	.L1242
.L1233:
	orl	$64, %edx
	movl	%edx, 4(%rbx)
	movq	96(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L1235
.L1243:
	movslq	88(%rdx), %rdx
.L1236:
	movq	328(%r12), %rcx
	movq	%rax, (%rcx,%rdx,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1242:
	.cfi_restore_state
	shrl	$5, %ecx
	andl	$3, %ecx
	cmpl	$2, %ecx
	je	.L1233
	orl	$96, %edx
	movl	%edx, 4(%rbx)
	movq	96(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L1243
.L1235:
	movslq	88(%rbx), %rdx
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1241:
	movq	(%rdi), %rdx
	movq	16(%rdx), %rax
	movq	24(%rdx), %rcx
	subq	%rax, %rcx
	cmpq	$55, %rcx
	jbe	.L1244
	leaq	56(%rax), %rcx
	movq	%rcx, 16(%rdx)
.L1232:
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal8compiler10SpillRangeC1EPNS1_17TopLevelLiveRangeEPNS0_4ZoneE
	movq	-40(%rbp), %rax
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	%rdx, %rdi
	movl	$56, %esi
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	jmp	.L1232
	.cfi_endproc
.LFE23396:
	.size	_ZN2v88internal8compiler22RegisterAllocationData27AssignSpillRangeToLiveRangeEPNS1_17TopLevelLiveRangeENS2_9SpillModeE, .-_ZN2v88internal8compiler22RegisterAllocationData27AssignSpillRangeToLiveRangeEPNS1_17TopLevelLiveRangeENS2_9SpillModeE
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData28CreateSpillRangeForLiveRangeEPNS1_17TopLevelLiveRangeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData28CreateSpillRangeForLiveRangeEPNS1_17TopLevelLiveRangeE
	.type	_ZN2v88internal8compiler22RegisterAllocationData28CreateSpillRangeForLiveRangeEPNS1_17TopLevelLiveRangeE, @function
_ZN2v88internal8compiler22RegisterAllocationData28CreateSpillRangeForLiveRangeEPNS1_17TopLevelLiveRangeE:
.LFB23397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	(%rdi), %rdx
	movq	16(%rdx), %r12
	movq	24(%rdx), %rax
	subq	%r12, %rax
	cmpq	$55, %rax
	jbe	.L1249
	leaq	56(%r12), %rax
	movq	%rax, 16(%rdx)
.L1247:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler10SpillRangeC1EPNS1_17TopLevelLiveRangeEPNS0_4ZoneE
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1249:
	.cfi_restore_state
	movq	%rdx, %rdi
	movl	$56, %esi
	movq	%rdx, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-24(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L1247
	.cfi_endproc
.LFE23397:
	.size	_ZN2v88internal8compiler22RegisterAllocationData28CreateSpillRangeForLiveRangeEPNS1_17TopLevelLiveRangeE, .-_ZN2v88internal8compiler22RegisterAllocationData28CreateSpillRangeForLiveRangeEPNS1_17TopLevelLiveRangeE
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData12MarkFixedUseENS0_21MachineRepresentationEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData12MarkFixedUseENS0_21MachineRepresentationEi
	.type	_ZN2v88internal8compiler22RegisterAllocationData12MarkFixedUseENS0_21MachineRepresentationEi, @function
_ZN2v88internal8compiler22RegisterAllocationData12MarkFixedUseENS0_21MachineRepresentationEi:
.LFB23398:
	.cfi_startproc
	endbr64
	cmpb	$13, %sil
	je	.L1251
	andl	$-3, %esi
	cmpb	$12, %sil
	je	.L1251
	movq	400(%rdi), %rax
	cmpl	$1, 4(%rax)
	movq	8(%rax), %rsi
	je	.L1258
.L1256:
	testl	%edx, %edx
	leal	63(%rdx), %eax
	movl	%edx, %edi
	cmovns	%edx, %eax
	sarl	$31, %edi
	shrl	$26, %edi
	leal	(%rdx,%rdi), %ecx
	sarl	$6, %eax
	movl	$1, %edx
	andl	$63, %ecx
	cltq
	subl	%edi, %ecx
	salq	%cl, %rdx
	orq	%rdx, (%rsi,%rax,8)
	ret
	.p2align 4,,10
	.p2align 3
.L1251:
	movq	408(%rdi), %rax
	cmpl	$1, 4(%rax)
	movq	8(%rax), %rsi
	jne	.L1256
.L1258:
	btsq	%rdx, %rsi
	movq	%rsi, 8(%rax)
	ret
	.cfi_endproc
.LFE23398:
	.size	_ZN2v88internal8compiler22RegisterAllocationData12MarkFixedUseENS0_21MachineRepresentationEi, .-_ZN2v88internal8compiler22RegisterAllocationData12MarkFixedUseENS0_21MachineRepresentationEi
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData11HasFixedUseENS0_21MachineRepresentationEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData11HasFixedUseENS0_21MachineRepresentationEi
	.type	_ZN2v88internal8compiler22RegisterAllocationData11HasFixedUseENS0_21MachineRepresentationEi, @function
_ZN2v88internal8compiler22RegisterAllocationData11HasFixedUseENS0_21MachineRepresentationEi:
.LFB23399:
	.cfi_startproc
	endbr64
	cmpb	$13, %sil
	je	.L1261
	andl	$-3, %esi
	cmpb	$12, %sil
	je	.L1261
	movq	400(%rdi), %rcx
	cmpl	$1, 4(%rcx)
	movq	8(%rcx), %rax
	je	.L1269
.L1270:
	testl	%edx, %edx
	leal	63(%rdx), %ecx
	cmovns	%edx, %ecx
	sarl	$6, %ecx
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
.L1269:
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$26, %esi
	leal	(%rdx,%rsi), %ecx
	andl	$63, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1261:
	movq	408(%rdi), %rcx
	cmpl	$1, 4(%rcx)
	movq	8(%rcx), %rax
	jne	.L1270
	jmp	.L1269
	.cfi_endproc
.LFE23399:
	.size	_ZN2v88internal8compiler22RegisterAllocationData11HasFixedUseENS0_21MachineRepresentationEi, .-_ZN2v88internal8compiler22RegisterAllocationData11HasFixedUseENS0_21MachineRepresentationEi
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData13MarkAllocatedENS0_21MachineRepresentationEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData13MarkAllocatedENS0_21MachineRepresentationEi
	.type	_ZN2v88internal8compiler22RegisterAllocationData13MarkAllocatedENS0_21MachineRepresentationEi, @function
_ZN2v88internal8compiler22RegisterAllocationData13MarkAllocatedENS0_21MachineRepresentationEi:
.LFB23400:
	.cfi_startproc
	endbr64
	cmpb	$13, %sil
	je	.L1272
	andl	$-3, %esi
	cmpb	$12, %sil
	je	.L1272
	movq	384(%rdi), %rax
	cmpl	$1, 4(%rax)
	movq	8(%rax), %rsi
	je	.L1279
.L1277:
	testl	%edx, %edx
	leal	63(%rdx), %eax
	movl	%edx, %edi
	cmovns	%edx, %eax
	sarl	$31, %edi
	shrl	$26, %edi
	leal	(%rdx,%rdi), %ecx
	sarl	$6, %eax
	movl	$1, %edx
	andl	$63, %ecx
	cltq
	subl	%edi, %ecx
	salq	%cl, %rdx
	orq	%rdx, (%rsi,%rax,8)
	ret
	.p2align 4,,10
	.p2align 3
.L1272:
	movq	392(%rdi), %rax
	cmpl	$1, 4(%rax)
	movq	8(%rax), %rsi
	jne	.L1277
.L1279:
	btsq	%rdx, %rsi
	movq	%rsi, 8(%rax)
	ret
	.cfi_endproc
.LFE23400:
	.size	_ZN2v88internal8compiler22RegisterAllocationData13MarkAllocatedENS0_21MachineRepresentationEi, .-_ZN2v88internal8compiler22RegisterAllocationData13MarkAllocatedENS0_21MachineRepresentationEi
	.section	.text._ZNK2v88internal8compiler22RegisterAllocationData15IsBlockBoundaryENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler22RegisterAllocationData15IsBlockBoundaryENS1_16LifetimePositionE
	.type	_ZNK2v88internal8compiler22RegisterAllocationData15IsBlockBoundaryENS1_16LifetimePositionE, @function
_ZNK2v88internal8compiler22RegisterAllocationData15IsBlockBoundaryENS1_16LifetimePositionE:
.LFB23401:
	.cfi_startproc
	endbr64
	testb	$3, %sil
	je	.L1289
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1289:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leal	3(%rsi), %ebx
	subq	$8, %rsp
	testl	%esi, %esi
	movq	16(%rdi), %rdi
	cmovns	%esi, %ebx
	sarl	$2, %ebx
	movl	%ebx, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	cmpl	%ebx, 112(%rax)
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23401:
	.size	_ZNK2v88internal8compiler22RegisterAllocationData15IsBlockBoundaryENS1_16LifetimePositionE, .-_ZNK2v88internal8compiler22RegisterAllocationData15IsBlockBoundaryENS1_16LifetimePositionE
	.section	.text._ZN2v88internal8compiler17ConstraintBuilderC2EPNS1_22RegisterAllocationDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17ConstraintBuilderC2EPNS1_22RegisterAllocationDataE
	.type	_ZN2v88internal8compiler17ConstraintBuilderC2EPNS1_22RegisterAllocationDataE, @function
_ZN2v88internal8compiler17ConstraintBuilderC2EPNS1_22RegisterAllocationDataE:
.LFB23403:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE23403:
	.size	_ZN2v88internal8compiler17ConstraintBuilderC2EPNS1_22RegisterAllocationDataE, .-_ZN2v88internal8compiler17ConstraintBuilderC2EPNS1_22RegisterAllocationDataE
	.globl	_ZN2v88internal8compiler17ConstraintBuilderC1EPNS1_22RegisterAllocationDataE
	.set	_ZN2v88internal8compiler17ConstraintBuilderC1EPNS1_22RegisterAllocationDataE,_ZN2v88internal8compiler17ConstraintBuilderC2EPNS1_22RegisterAllocationDataE
	.section	.rodata._ZN2v88internal8compiler17ConstraintBuilder13AllocateFixedEPNS1_18UnallocatedOperandEibb.str1.8,"aMS",@progbits,1
	.align 8
.LC36:
	.string	"Allocating fixed reg for op %d\n"
	.section	.rodata._ZN2v88internal8compiler17ConstraintBuilder13AllocateFixedEPNS1_18UnallocatedOperandEibb.str1.1,"aMS",@progbits,1
.LC37:
	.string	"Fixed reg is tagged at %d\n"
	.section	.text._ZN2v88internal8compiler17ConstraintBuilder13AllocateFixedEPNS1_18UnallocatedOperandEibb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17ConstraintBuilder13AllocateFixedEPNS1_18UnallocatedOperandEibb
	.type	_ZN2v88internal8compiler17ConstraintBuilder13AllocateFixedEPNS1_18UnallocatedOperandEibb, @function
_ZN2v88internal8compiler17ConstraintBuilder13AllocateFixedEPNS1_18UnallocatedOperandEibb:
.LFB23405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%edx, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdx
	movq	(%rdi), %rax
	movq	%rdx, %rsi
	shrq	$3, %rsi
	movl	%esi, %r8d
	testb	$4, 488(%rax)
	jne	.L1325
.L1292:
	movl	$5, %esi
	cmpl	$-1, %r8d
	je	.L1293
	movq	(%r14), %rax
	movl	%r8d, %esi
	movq	16(%rax), %rdi
	call	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	movq	(%r12), %rdx
	movl	%eax, %esi
.L1293:
	movq	%rdx, %rax
	shrq	$35, %rax
	testb	$1, %al
	je	.L1326
	movq	%rdx, %rax
	shrq	$36, %rax
	andl	$7, %eax
	cmpl	$3, %eax
	je	.L1324
	cmpl	$4, %eax
	je	.L1324
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1326:
	movq	%rdx, %r9
	movzbl	%sil, %eax
	sarq	$36, %r9
	salq	$5, %rax
	salq	$35, %r9
	orq	%rax, %r9
	orq	$13, %r9
.L1295:
	testb	%r15b, %r15b
	je	.L1298
	testb	$8, %r9b
	jne	.L1298
	shrq	$41, %rdx
	movq	(%r14), %rdi
	andl	$63, %edx
	call	_ZN2v88internal8compiler22RegisterAllocationData12MarkFixedUseENS0_21MachineRepresentationEi
	.p2align 4,,10
	.p2align 3
.L1298:
	movq	%r9, (%r12)
	testb	%r13b, %r13b
	je	.L1300
	movq	(%r14), %rax
	testb	$4, 488(%rax)
	jne	.L1327
.L1301:
	movq	16(%rax), %rdx
	movq	208(%rdx), %rcx
	movq	%rcx, %rax
	subq	216(%rdx), %rax
	sarq	$3, %rax
	addq	%rbx, %rax
	js	.L1302
	cmpq	$63, %rax
	jg	.L1303
	leaq	(%rcx,%rbx,8), %rax
.L1304:
	movq	(%rax), %rax
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1300
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12ReferenceMap15RecordReferenceERKNS1_16AllocatedOperandE@PLT
.L1300:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1325:
	.cfi_restore_state
	leaq	.LC36(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rdx
	movq	%rdx, %r8
	shrq	$3, %r8
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1302:
	movq	%rax, %rcx
	notq	%rcx
	shrq	$6, %rcx
	notq	%rcx
.L1305:
	movq	232(%rdx), %rdx
	movq	%rcx, %rsi
	salq	$6, %rsi
	movq	(%rdx,%rcx,8), %rdx
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %rax
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1324:
	movq	%rdx, %r9
	movzbl	%sil, %eax
	shrq	$41, %r9
	salq	$5, %rax
	andl	$63, %r9d
	salq	$35, %r9
	orq	%rax, %r9
	orq	$5, %r9
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1303:
	movq	%rax, %rcx
	sarq	$6, %rcx
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1327:
	xorl	%eax, %eax
	movl	%ebx, %esi
	leaq	.LC37(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r14), %rax
	jmp	.L1301
	.cfi_endproc
.LFE23405:
	.size	_ZN2v88internal8compiler17ConstraintBuilder13AllocateFixedEPNS1_18UnallocatedOperandEibb, .-_ZN2v88internal8compiler17ConstraintBuilder13AllocateFixedEPNS1_18UnallocatedOperandEibb
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilderC2EPNS1_22RegisterAllocationDataEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16LiveRangeBuilderC2EPNS1_22RegisterAllocationDataEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler16LiveRangeBuilderC2EPNS1_22RegisterAllocationDataEPNS0_4ZoneE, @function
_ZN2v88internal8compiler16LiveRangeBuilderC2EPNS1_22RegisterAllocationDataEPNS0_4ZoneE:
.LFB23420:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	leaq	24(%rdi), %rax
	movl	$0, 24(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 32(%rdi)
	movq	%rax, 40(%rdi)
	movq	%rax, 48(%rdi)
	movq	$0, 56(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE23420:
	.size	_ZN2v88internal8compiler16LiveRangeBuilderC2EPNS1_22RegisterAllocationDataEPNS0_4ZoneE, .-_ZN2v88internal8compiler16LiveRangeBuilderC2EPNS1_22RegisterAllocationDataEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler16LiveRangeBuilderC1EPNS1_22RegisterAllocationDataEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler16LiveRangeBuilderC1EPNS1_22RegisterAllocationDataEPNS0_4ZoneE,_ZN2v88internal8compiler16LiveRangeBuilderC2EPNS1_22RegisterAllocationDataEPNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler16LiveRangeBuilder14ComputeLiveOutEPKNS1_16InstructionBlockEPNS1_22RegisterAllocationDataE.str1.8,"aMS",@progbits,1
	.align 8
.LC38:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilder14ComputeLiveOutEPKNS1_16InstructionBlockEPNS1_22RegisterAllocationDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16LiveRangeBuilder14ComputeLiveOutEPKNS1_16InstructionBlockEPNS1_22RegisterAllocationDataE
	.type	_ZN2v88internal8compiler16LiveRangeBuilder14ComputeLiveOutEPKNS1_16InstructionBlockEPNS1_22RegisterAllocationDataE, @function
_ZN2v88internal8compiler16LiveRangeBuilder14ComputeLiveOutEPKNS1_16InstructionBlockEPNS1_22RegisterAllocationDataE:
.LFB23422:
	.cfi_startproc
	endbr64
	movslq	100(%rdi), %r9
	movq	136(%rsi), %rax
	movq	(%rax,%r9,8), %r8
	testq	%r8, %r8
	je	.L1367
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1367:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rsi), %rax
	movq	(%rsi), %r15
	movl	272(%rax), %r12d
	movq	16(%r15), %rbx
	movq	%rax, -72(%rbp)
	movq	24(%r15), %rax
	subq	%rbx, %rax
	cmpq	$15, %rax
	jbe	.L1368
	leaq	16(%rbx), %rax
	movq	%rax, 16(%r15)
.L1332:
	movl	%r12d, (%rbx)
	movq	%rbx, %r8
	cmpl	$64, %r12d
	jle	.L1369
	subl	$1, %r12d
	movq	$0, 8(%rbx)
	sarl	$6, %r12d
	leal	1(%r12), %eax
	movl	%eax, 4(%rbx)
	cltq
	movq	24(%r15), %rdx
	leaq	0(,%rax,8), %rsi
	movq	16(%r15), %rax
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1370
	addq	%rax, %rsi
	movq	%rsi, 16(%r15)
.L1336:
	movl	4(%rbx), %edx
	movq	%rax, 8(%rbx)
	cmpl	$1, %edx
	je	.L1371
	testl	%edx, %edx
	jle	.L1334
	movq	$0, (%rax)
	cmpl	$1, 4(%rbx)
	movl	$8, %edx
	movl	$1, %eax
	jle	.L1334
	.p2align 4,,10
	.p2align 3
.L1339:
	movq	8(%rbx), %rcx
	addl	$1, %eax
	movq	$0, (%rcx,%rdx)
	addq	$8, %rdx
	cmpl	%eax, 4(%rbx)
	jg	.L1339
.L1334:
	movq	16(%r13), %rax
	movq	8(%r13), %r12
	movl	$1, %r15d
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r12
	je	.L1344
	movq	%r14, %rax
	movq	%r8, -80(%rbp)
	movq	%r12, %r14
	movq	%r13, %r12
	movq	%r9, -88(%rbp)
	movq	%rax, %r13
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1349:
	addq	$4, %r14
	cmpq	%r14, -56(%rbp)
	je	.L1372
.L1343:
	movslq	(%r14), %rax
	cmpl	%eax, 100(%r12)
	jge	.L1349
	movq	104(%r13), %rdx
	movq	(%rdx,%rax,8), %rdi
	testq	%rdi, %rdi
	je	.L1345
	movl	4(%rbx), %edx
	cmpl	$1, %edx
	je	.L1373
	testl	%edx, %edx
	jle	.L1345
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1347:
	movq	8(%rdi), %rcx
	movq	8(%rbx), %rdx
	movq	(%rcx,%rax,8), %rcx
	orq	%rcx, (%rdx,%rax,8)
	addq	$1, %rax
	cmpl	%eax, 4(%rbx)
	jg	.L1347
	movslq	(%r14), %rax
.L1345:
	movq	-72(%rbp), %rdi
	movq	16(%rdi), %rdx
	movq	8(%rdx), %rcx
	movq	16(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rax, %rdx
	jbe	.L1374
	movq	(%rcx,%rax,8), %rcx
	movl	100(%r12), %esi
	movq	%rcx, %rdi
	movq	%rcx, -64(%rbp)
	call	_ZNK2v88internal8compiler16InstructionBlock18PredecessorIndexOfENS1_9RpoNumberE@PLT
	movq	-64(%rbp), %rcx
	movq	72(%rcx), %rdx
	movq	80(%rcx), %r10
	cmpq	%r10, %rdx
	je	.L1349
	leaq	0(,%rax,4), %rdi
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1350:
	testl	%ecx, %ecx
	leal	63(%rcx), %eax
	movl	%ecx, %r8d
	movq	%r15, %r11
	cmovns	%ecx, %eax
	sarl	$31, %r8d
	shrl	$26, %r8d
	addl	%r8d, %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%r8d, %ecx
	salq	%cl, %r11
	orq	%r11, (%rsi,%rax,8)
.L1351:
	addq	$8, %rdx
	cmpq	%rdx, %r10
	je	.L1349
.L1352:
	movq	(%rdx), %rax
	cmpl	$1, 4(%rbx)
	movq	8(%rbx), %rsi
	movq	24(%rax), %rax
	movl	(%rax,%rdi), %ecx
	jne	.L1350
	btsq	%rcx, %rsi
	movq	%rsi, 8(%rbx)
	jmp	.L1351
	.p2align 4,,10
	.p2align 3
.L1372:
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %r9
	movq	%r13, %r14
.L1344:
	movq	136(%r14), %rax
	movq	%rbx, (%rax,%r9,8)
	addq	$56, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1373:
	.cfi_restore_state
	movq	8(%rdi), %rax
	orq	%rax, 8(%rbx)
	movslq	(%r14), %rax
	jmp	.L1345
	.p2align 4,,10
	.p2align 3
.L1369:
	movl	$1, 4(%rbx)
	movq	$0, 8(%rbx)
	jmp	.L1334
.L1371:
	movq	$0, 8(%rbx)
	jmp	.L1334
.L1368:
	movl	$16, %esi
	movq	%r15, %rdi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r9
	movq	%rax, %rbx
	jmp	.L1332
.L1370:
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	movq	%rbx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	jmp	.L1336
.L1374:
	movq	%rax, %rsi
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE23422:
	.size	_ZN2v88internal8compiler16LiveRangeBuilder14ComputeLiveOutEPKNS1_16InstructionBlockEPNS1_22RegisterAllocationDataE, .-_ZN2v88internal8compiler16LiveRangeBuilder14ComputeLiveOutEPKNS1_16InstructionBlockEPNS1_22RegisterAllocationDataE
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilder18FixedFPLiveRangeIDEiNS0_21MachineRepresentationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16LiveRangeBuilder18FixedFPLiveRangeIDEiNS0_21MachineRepresentationE
	.type	_ZN2v88internal8compiler16LiveRangeBuilder18FixedFPLiveRangeIDEiNS0_21MachineRepresentationE, @function
_ZN2v88internal8compiler16LiveRangeBuilder18FixedFPLiveRangeIDEiNS0_21MachineRepresentationE:
.LFB23424:
	.cfi_startproc
	endbr64
	notl	%esi
	cmpb	$13, %dl
	je	.L1376
	cmpb	$14, %dl
	je	.L1377
	cmpb	$12, %dl
	je	.L1386
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1376:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	movl	8(%rax), %eax
	addl	%eax, %eax
	subl	%eax, %esi
	movl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1386:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
.L1381:
	movl	16(%rax), %ecx
	movl	8(%rax), %eax
	leal	(%rcx,%rcx), %edx
	addl	%eax, %eax
	subl	%edx, %esi
	subl	%eax, %esi
	movl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1377:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	movl	12(%rax), %ecx
	leal	(%rcx,%rcx), %edx
	subl	%edx, %esi
	jmp	.L1381
	.cfi_endproc
.LFE23424:
	.size	_ZN2v88internal8compiler16LiveRangeBuilder18FixedFPLiveRangeIDEiNS0_21MachineRepresentationE, .-_ZN2v88internal8compiler16LiveRangeBuilder18FixedFPLiveRangeIDEiNS0_21MachineRepresentationE
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilder17FixedLiveRangeForEiNS1_22RegisterAllocationData9SpillModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16LiveRangeBuilder17FixedLiveRangeForEiNS1_22RegisterAllocationData9SpillModeE
	.type	_ZN2v88internal8compiler16LiveRangeBuilder17FixedLiveRangeForEiNS1_22RegisterAllocationData9SpillModeE, @function
_ZN2v88internal8compiler16LiveRangeBuilder17FixedLiveRangeForEiNS1_22RegisterAllocationData9SpillModeE:
.LFB23425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	movq	(%rdi), %rdi
	testl	%edx, %edx
	je	.L1388
	movq	32(%rdi), %rax
	addl	8(%rax), %esi
.L1388:
	movq	200(%rdi), %rax
	movslq	%esi, %r14
	movq	(%rax,%r14,8), %rax
	testq	%rax, %rax
	je	.L1396
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1396:
	.cfi_restore_state
	notl	%esi
	movl	$5, %edx
	call	_ZN2v88internal8compiler22RegisterAllocationData12NewLiveRangeEiNS0_21MachineRepresentationE
	movl	%ebx, %ecx
	movl	4(%rax), %edx
	sall	$7, %ecx
	andl	$-8065, %edx
	orl	%ecx, %edx
	movl	%edx, 4(%rax)
	movq	(%r12), %rdx
	movq	384(%rdx), %rsi
	cmpl	$1, 4(%rsi)
	je	.L1397
	testl	%ebx, %ebx
	movq	8(%rsi), %rdi
	leal	63(%rbx), %edx
	movl	%ebx, %esi
	cmovns	%ebx, %edx
	sarl	$31, %esi
	shrl	$26, %esi
	leal	(%rbx,%rsi), %ecx
	sarl	$6, %edx
	andl	$63, %ecx
	movslq	%edx, %rdx
	subl	%esi, %ecx
	movl	$1, %esi
	salq	%cl, %rsi
	orq	%rsi, (%rdi,%rdx,8)
.L1391:
	cmpl	$1, %r13d
	jne	.L1392
	orl	$268435456, 4(%rax)
.L1392:
	movq	(%r12), %rdx
	movq	200(%rdx), %rdx
	movq	%rax, (%rdx,%r14,8)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1397:
	.cfi_restore_state
	movl	$1, %edx
	movl	%ebx, %ecx
	salq	%cl, %rdx
	orq	%rdx, 8(%rsi)
	jmp	.L1391
	.cfi_endproc
.LFE23425:
	.size	_ZN2v88internal8compiler16LiveRangeBuilder17FixedLiveRangeForEiNS1_22RegisterAllocationData9SpillModeE, .-_ZN2v88internal8compiler16LiveRangeBuilder17FixedLiveRangeForEiNS1_22RegisterAllocationData9SpillModeE
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilder19FixedFPLiveRangeForEiNS0_21MachineRepresentationENS1_22RegisterAllocationData9SpillModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16LiveRangeBuilder19FixedFPLiveRangeForEiNS0_21MachineRepresentationENS1_22RegisterAllocationData9SpillModeE
	.type	_ZN2v88internal8compiler16LiveRangeBuilder19FixedFPLiveRangeForEiNS0_21MachineRepresentationENS1_22RegisterAllocationData9SpillModeE, @function
_ZN2v88internal8compiler16LiveRangeBuilder19FixedFPLiveRangeForEiNS0_21MachineRepresentationENS1_22RegisterAllocationData9SpillModeE:
.LFB23426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$24, %rsp
	movq	(%rdi), %r15
	movq	32(%r15), %rdi
	movq	264(%r15), %rax
	movl	16(%rdi), %edx
	addl	%edx, %esi
	testl	%ecx, %ecx
	cmove	%r12d, %esi
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	testq	%rax, %rax
	je	.L1410
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1410:
	.cfi_restore_state
	notl	%esi
	cmpb	$13, %r14b
	je	.L1401
	cmpb	$14, %r14b
	jne	.L1411
	movl	12(%rdi), %eax
	addl	%eax, %eax
	subl	%eax, %esi
.L1403:
	addl	%edx, %edx
	subl	%edx, %esi
.L1401:
	movl	8(%rdi), %eax
	movl	%r14d, %edx
	movq	%r15, %rdi
	movq	%rcx, -64(%rbp)
	addl	%eax, %eax
	subl	%eax, %esi
	call	_ZN2v88internal8compiler22RegisterAllocationData12NewLiveRangeEiNS0_21MachineRepresentationE
	movl	%r12d, %esi
	movl	4(%rax), %edx
	sall	$7, %esi
	movq	%rax, -56(%rbp)
	andl	$-8065, %edx
	orl	%esi, %edx
	movl	%r14d, %esi
	movl	%edx, 4(%rax)
	movq	0(%r13), %rdi
	movl	%r12d, %edx
	call	_ZN2v88internal8compiler22RegisterAllocationData13MarkAllocatedENS0_21MachineRepresentationEi
	cmpl	$1, %ebx
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rcx
	jne	.L1406
	orl	$268435456, 4(%rax)
.L1406:
	movq	264(%r15), %rdx
	movq	%rax, (%rdx,%rcx,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1411:
	.cfi_restore_state
	cmpb	$12, %r14b
	je	.L1403
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23426:
	.size	_ZN2v88internal8compiler16LiveRangeBuilder19FixedFPLiveRangeForEiNS0_21MachineRepresentationENS1_22RegisterAllocationData9SpillModeE, .-_ZN2v88internal8compiler16LiveRangeBuilder19FixedFPLiveRangeForEiNS0_21MachineRepresentationENS1_22RegisterAllocationData9SpillModeE
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilder14NewUsePositionENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16LiveRangeBuilder14NewUsePositionENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE
	.type	_ZN2v88internal8compiler16LiveRangeBuilder14NewUsePositionENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE, @function
_ZN2v88internal8compiler16LiveRangeBuilder14NewUsePositionENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE:
.LFB23428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rax
	movq	%rcx, -40(%rbp)
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L1426
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1414:
	movq	%r12, %xmm0
	movq	$0, 16(%rax)
	movhps	-40(%rbp), %xmm0
	movl	%r13d, 24(%rax)
	movups	%xmm0, (%rax)
	testq	%r12, %r12
	je	.L1417
	movq	(%r12), %rsi
	movl	$32, %edi
	xorl	%edx, %edx
	movl	%esi, %ecx
	andl	$7, %ecx
	cmpl	$1, %ecx
	je	.L1427
.L1415:
	movzbl	%r8b, %r8d
	orl	%edi, %edx
	leal	0(,%r8,4), %ebx
	orl	%edx, %ebx
	orb	$8, %bh
	movl	%ebx, 28(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1427:
	.cfi_restore_state
	movq	%rsi, %rcx
	shrq	$35, %rcx
	movl	%ecx, %edx
	andl	$1, %edx
	je	.L1425
	shrq	$36, %rsi
	andl	$7, %esi
	cmpl	$5, %esi
	je	.L1420
	xorl	%edi, %edi
	cmpl	$6, %esi
	je	.L1421
	cmpl	$2, %esi
	je	.L1415
	xorl	%edx, %edx
	cmpl	$1, %esi
	setne	%dl
	movl	%edx, %edi
	sall	$5, %edi
.L1425:
	xorl	%edx, %edx
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1417:
	movl	$32, %edi
	xorl	%edx, %edx
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1426:
	movl	$32, %esi
	movl	%r8d, -44(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-44(%rbp), %r8d
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1420:
	movl	$2, %edx
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1421:
	movl	$3, %edx
	jmp	.L1415
	.cfi_endproc
.LFE23428:
	.size	_ZN2v88internal8compiler16LiveRangeBuilder14NewUsePositionENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE, .-_ZN2v88internal8compiler16LiveRangeBuilder14NewUsePositionENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilder14ResolvePhiHintEPNS1_18InstructionOperandEPNS1_11UsePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16LiveRangeBuilder14ResolvePhiHintEPNS1_18InstructionOperandEPNS1_11UsePositionE
	.type	_ZN2v88internal8compiler16LiveRangeBuilder14ResolvePhiHintEPNS1_18InstructionOperandEPNS1_11UsePositionE, @function
_ZN2v88internal8compiler16LiveRangeBuilder14ResolvePhiHintEPNS1_18InstructionOperandEPNS1_11UsePositionE:
.LFB23466:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	leaq	24(%rdi), %r8
	testq	%rax, %rax
	je	.L1428
	movq	%r8, %rcx
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1437:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1431
.L1430:
	cmpq	%rsi, 32(%rax)
	jnb	.L1437
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1430
.L1431:
	cmpq	%rcx, %r8
	je	.L1428
	cmpq	%rsi, 32(%rcx)
	jbe	.L1438
.L1428:
	ret
	.p2align 4,,10
	.p2align 3
.L1438:
	movq	40(%rcx), %rsi
	movl	28(%rsi), %eax
	movl	%eax, %ecx
	shrl	$2, %ecx
	andl	$7, %ecx
	cmpb	$4, %cl
	jne	.L1428
	andl	$-29, %eax
	movq	%rdx, 8(%rsi)
	orl	$8, %eax
	movl	%eax, 28(%rsi)
	ret
	.cfi_endproc
.LFE23466:
	.size	_ZN2v88internal8compiler16LiveRangeBuilder14ResolvePhiHintEPNS1_18InstructionOperandEPNS1_11UsePositionE, .-_ZN2v88internal8compiler16LiveRangeBuilder14ResolvePhiHintEPNS1_18InstructionOperandEPNS1_11UsePositionE
	.section	.text._ZNK2v88internal8compiler16LiveRangeBuilder29IntervalStartsAtBlockBoundaryEPKNS1_11UseIntervalE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16LiveRangeBuilder29IntervalStartsAtBlockBoundaryEPKNS1_11UseIntervalE
	.type	_ZNK2v88internal8compiler16LiveRangeBuilder29IntervalStartsAtBlockBoundaryEPKNS1_11UseIntervalE, @function
_ZNK2v88internal8compiler16LiveRangeBuilder29IntervalStartsAtBlockBoundaryEPKNS1_11UseIntervalE:
.LFB23468:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testb	$3, %al
	je	.L1447
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1447:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leal	3(%rax), %ebx
	subq	$8, %rsp
	testl	%eax, %eax
	cmovns	%eax, %ebx
	movq	(%rdi), %rax
	sarl	$2, %ebx
	movq	16(%rax), %rdi
	movl	%ebx, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	cmpl	%ebx, 112(%rax)
	sete	%r8b
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23468:
	.size	_ZNK2v88internal8compiler16LiveRangeBuilder29IntervalStartsAtBlockBoundaryEPKNS1_11UseIntervalE, .-_ZNK2v88internal8compiler16LiveRangeBuilder29IntervalStartsAtBlockBoundaryEPKNS1_11UseIntervalE
	.section	.text._ZNK2v88internal8compiler16LiveRangeBuilder34IntervalPredecessorsCoveredByRangeEPKNS1_11UseIntervalEPKNS1_17TopLevelLiveRangeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16LiveRangeBuilder34IntervalPredecessorsCoveredByRangeEPKNS1_11UseIntervalEPKNS1_17TopLevelLiveRangeE
	.type	_ZNK2v88internal8compiler16LiveRangeBuilder34IntervalPredecessorsCoveredByRangeEPKNS1_11UseIntervalEPKNS1_17TopLevelLiveRangeE, @function
_ZNK2v88internal8compiler16LiveRangeBuilder34IntervalPredecessorsCoveredByRangeEPKNS1_11UseIntervalEPKNS1_17TopLevelLiveRangeE:
.LFB23469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rsi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	leal	3(%rax), %esi
	cmovns	%eax, %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rdi
	sarl	$2, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movq	%rax, %rdx
	movq	40(%rax), %rax
	movq	48(%rdx), %r8
	cmpq	%r8, %rax
	je	.L1449
	.p2align 4,,10
	.p2align 3
.L1458:
	movq	(%rbx), %rdx
	movslq	(%rax), %rsi
	movq	16(%rdx), %rdx
	movq	16(%rdx), %rdx
	movq	8(%rdx), %rcx
	movq	16(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1471
	movq	(%rcx,%rsi,8), %rdx
	movl	116(%rdx), %edx
	leal	-1(,%rdx,4), %ecx
	movq	16(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1451
	movl	(%rdx), %esi
	cmpl	%esi, %ecx
	jl	.L1451
	movq	8(%r12), %rdi
	cmpl	4(%rdi), %ecx
	jge	.L1451
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1459
	movl	(%rdi), %esi
	cmpl	%esi, %ecx
	jl	.L1472
.L1469:
	movq	48(%r12), %r9
	movl	$-1, %edx
	testq	%r9, %r9
	je	.L1453
	movl	(%r9), %edx
.L1453:
	cmpl	%esi, %edx
	jge	.L1454
	movq	%rdi, 48(%r12)
	cmpl	(%rdi), %ecx
	jl	.L1451
.L1454:
	cmpl	4(%rdi), %ecx
	jl	.L1455
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1451
	movl	(%rdi), %esi
.L1456:
	cmpl	%esi, %ecx
	jge	.L1469
.L1451:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1455:
	.cfi_restore_state
	addq	$4, %rax
	cmpq	%rax, %r8
	jne	.L1458
.L1449:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1472:
	.cfi_restore_state
	movq	$0, 48(%r12)
	movq	%rdx, %rdi
	movl	(%rdx), %esi
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1459:
	movq	%rdx, %rdi
	jmp	.L1456
.L1471:
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE23469:
	.size	_ZNK2v88internal8compiler16LiveRangeBuilder34IntervalPredecessorsCoveredByRangeEPKNS1_11UseIntervalEPKNS1_17TopLevelLiveRangeE, .-_ZNK2v88internal8compiler16LiveRangeBuilder34IntervalPredecessorsCoveredByRangeEPKNS1_11UseIntervalEPKNS1_17TopLevelLiveRangeE
	.section	.rodata._ZNK2v88internal8compiler16LiveRangeBuilder6VerifyEv.str1.1,"aMS",@progbits,1
.LC39:
	.string	"hint.second->IsResolved()"
.LC40:
	.string	"(current->next()) == nullptr"
	.section	.rodata._ZNK2v88internal8compiler16LiveRangeBuilder6VerifyEv.str1.8,"aMS",@progbits,1
	.align 8
.LC41:
	.string	"NextIntervalStartsInDifferentBlocks(first)"
	.align 8
.LC42:
	.string	"IntervalStartsAtBlockBoundary(i)"
	.align 8
.LC43:
	.string	"IntervalPredecessorsCoveredByRange(i, current)"
	.align 8
.LC44:
	.string	"NextIntervalStartsInDifferentBlocks(i)"
	.section	.text._ZNK2v88internal8compiler16LiveRangeBuilder6VerifyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16LiveRangeBuilder6VerifyEv
	.type	_ZNK2v88internal8compiler16LiveRangeBuilder6VerifyEv, @function
_ZNK2v88internal8compiler16LiveRangeBuilder6VerifyEv:
.LFB23467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	24(%rbx), %r12
	subq	$24, %rsp
	movq	40(%rdi), %rdi
	cmpq	%r12, %rdi
	jne	.L1476
	jmp	.L1474
	.p2align 4,,10
	.p2align 3
.L1475:
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdi
	cmpq	%rax, %r12
	je	.L1474
.L1476:
	movq	40(%rdi), %rax
	movl	28(%rax), %edx
	shrl	$2, %edx
	andl	$7, %edx
	cmpb	$4, %dl
	jne	.L1475
	leaq	.LC39(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1474:
	movq	(%rbx), %rax
	movq	168(%rax), %r15
	movq	176(%rax), %rax
	movq	%rax, -64(%rbp)
	cmpq	%r15, %rax
	je	.L1473
	.p2align 4,,10
	.p2align 3
.L1479:
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.L1481
	movq	16(%r13), %r12
	testq	%r12, %r12
	je	.L1481
	cmpq	$0, 40(%r13)
	jne	.L1513
	movq	8(%r12), %r14
	testq	%r14, %r14
	je	.L1483
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L1485:
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L1485
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9LiveRange15VerifyPositionsEv
	movl	4(%r12), %eax
	movl	(%r14), %r14d
	movl	%eax, %edx
	notl	%eax
	andl	$1, %eax
	andl	$-2, %edx
	cmpb	$1, %al
	movq	(%rbx), %rax
	adcl	$-1, %edx
	testl	%edx, %edx
	leal	3(%rdx), %esi
	movq	16(%rax), %rdi
	cmovns	%edx, %esi
	sarl	$2, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	testl	%r14d, %r14d
	leal	3(%r14), %esi
	movq	%rax, -56(%rbp)
	movq	(%rbx), %rax
	cmovns	%r14d, %esi
	movq	16(%rax), %rdi
	sarl	$2, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movq	-56(%rbp), %rdx
	movl	100(%rdx), %ecx
	cmpl	%ecx, 100(%rax)
	jle	.L1514
.L1491:
	movq	8(%r12), %r12
	testq	%r12, %r12
	je	.L1481
	movl	(%r12), %eax
	testb	$3, %al
	je	.L1487
.L1488:
	leaq	.LC42(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1483:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler9LiveRange15VerifyPositionsEv
	.p2align 4,,10
	.p2align 3
.L1481:
	addq	$8, %r15
	cmpq	%r15, -64(%rbp)
	jne	.L1479
.L1473:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1487:
	.cfi_restore_state
	testl	%eax, %eax
	leal	3(%rax), %r14d
	cmovns	%eax, %r14d
	movq	(%rbx), %rax
	sarl	$2, %r14d
	movq	16(%rax), %rdi
	movl	%r14d, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	cmpl	112(%rax), %r14d
	jne	.L1488
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler16LiveRangeBuilder34IntervalPredecessorsCoveredByRangeEPKNS1_11UseIntervalEPKNS1_17TopLevelLiveRangeE
	testb	%al, %al
	je	.L1515
	movq	8(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1481
	movl	4(%r12), %eax
	movl	(%rdx), %r14d
	movl	%eax, %edx
	notl	%eax
	andl	$1, %eax
	andl	$-2, %edx
	cmpb	$1, %al
	movq	(%rbx), %rax
	adcl	$-1, %edx
	testl	%edx, %edx
	leal	3(%rdx), %esi
	movq	16(%rax), %rdi
	cmovns	%edx, %esi
	sarl	$2, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	testl	%r14d, %r14d
	leal	3(%r14), %esi
	movq	%rax, -56(%rbp)
	movq	(%rbx), %rax
	cmovns	%r14d, %esi
	movq	16(%rax), %rdi
	sarl	$2, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movq	-56(%rbp), %rdx
	movl	100(%rdx), %ecx
	cmpl	%ecx, 100(%rax)
	jg	.L1491
	leaq	.LC44(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1515:
	leaq	.LC43(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1513:
	leaq	.LC40(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1514:
	leaq	.LC41(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23467:
	.size	_ZNK2v88internal8compiler16LiveRangeBuilder6VerifyEv, .-_ZNK2v88internal8compiler16LiveRangeBuilder6VerifyEv
	.section	.text._ZNK2v88internal8compiler16LiveRangeBuilder35NextIntervalStartsInDifferentBlocksEPKNS1_11UseIntervalE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16LiveRangeBuilder35NextIntervalStartsInDifferentBlocksEPKNS1_11UseIntervalE
	.type	_ZNK2v88internal8compiler16LiveRangeBuilder35NextIntervalStartsInDifferentBlocksEPKNS1_11UseIntervalE, @function
_ZNK2v88internal8compiler16LiveRangeBuilder35NextIntervalStartsInDifferentBlocksEPKNS1_11UseIntervalE:
.LFB23470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	4(%rsi), %eax
	movq	8(%rsi), %rdx
	movl	(%rdx), %r13d
	movl	%eax, %edx
	notl	%eax
	andl	$1, %eax
	andl	$-2, %edx
	cmpb	$1, %al
	movq	(%rdi), %rax
	adcl	$-1, %edx
	testl	%edx, %edx
	leal	3(%rdx), %esi
	movq	16(%rax), %rdi
	cmovns	%edx, %esi
	sarl	$2, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	testl	%r13d, %r13d
	leal	3(%r13), %esi
	movq	%rax, %r12
	movq	(%rbx), %rax
	cmovns	%r13d, %esi
	movq	16(%rax), %rdi
	sarl	$2, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movl	100(%rax), %eax
	cmpl	%eax, 100(%r12)
	setl	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23470:
	.size	_ZNK2v88internal8compiler16LiveRangeBuilder35NextIntervalStartsInDifferentBlocksEPKNS1_11UseIntervalE, .-_ZNK2v88internal8compiler16LiveRangeBuilder35NextIntervalStartsInDifferentBlocksEPKNS1_11UseIntervalE
	.section	.text._ZN2v88internal8compiler17RegisterAllocatorC2EPNS1_22RegisterAllocationDataENS1_12RegisterKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17RegisterAllocatorC2EPNS1_22RegisterAllocationDataENS1_12RegisterKindE
	.type	_ZN2v88internal8compiler17RegisterAllocatorC2EPNS1_22RegisterAllocationDataENS1_12RegisterKindE, @function
_ZN2v88internal8compiler17RegisterAllocatorC2EPNS1_22RegisterAllocationDataENS1_12RegisterKindE:
.LFB23476:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movq	32(%rsi), %rax
	movl	%edx, 8(%rdi)
	cmpl	$1, %edx
	je	.L1524
	movl	8(%rax), %edx
	movb	$0, 32(%rdi)
	movl	%edx, 12(%rdi)
	movl	24(%rax), %edx
	movq	56(%rax), %rax
	movl	%edx, 16(%rdi)
	movq	%rax, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1524:
	movl	16(%rax), %edx
	movb	$0, 32(%rdi)
	movl	%edx, 12(%rdi)
	movl	32(%rax), %edx
	movq	192(%rax), %rax
	movl	%edx, 16(%rdi)
	movq	%rax, 24(%rdi)
	ret
	.cfi_endproc
.LFE23476:
	.size	_ZN2v88internal8compiler17RegisterAllocatorC2EPNS1_22RegisterAllocationDataENS1_12RegisterKindE, .-_ZN2v88internal8compiler17RegisterAllocatorC2EPNS1_22RegisterAllocationDataENS1_12RegisterKindE
	.globl	_ZN2v88internal8compiler17RegisterAllocatorC1EPNS1_22RegisterAllocationDataENS1_12RegisterKindE
	.set	_ZN2v88internal8compiler17RegisterAllocatorC1EPNS1_22RegisterAllocationDataENS1_12RegisterKindE,_ZN2v88internal8compiler17RegisterAllocatorC2EPNS1_22RegisterAllocationDataENS1_12RegisterKindE
	.section	.text._ZN2v88internal8compiler17RegisterAllocator30GetSplitPositionForInstructionEPKNS1_9LiveRangeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17RegisterAllocator30GetSplitPositionForInstructionEPKNS1_9LiveRangeEi
	.type	_ZN2v88internal8compiler17RegisterAllocator30GetSplitPositionForInstructionEPKNS1_9LiveRangeEi, @function
_ZN2v88internal8compiler17RegisterAllocator30GetSplitPositionForInstructionEPKNS1_9LiveRangeEi:
.LFB23478:
	.cfi_startproc
	endbr64
	leal	0(,%rdx,4), %eax
	movq	16(%rsi), %rdx
	cmpl	%eax, (%rdx)
	jge	.L1527
	movq	8(%rsi), %rdx
	cmpl	4(%rdx), %eax
	movl	$-1, %edx
	cmovge	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1527:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE23478:
	.size	_ZN2v88internal8compiler17RegisterAllocator30GetSplitPositionForInstructionEPKNS1_9LiveRangeEi, .-_ZN2v88internal8compiler17RegisterAllocator30GetSplitPositionForInstructionEPKNS1_9LiveRangeEi
	.section	.rodata._ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE.str1.8,"aMS",@progbits,1
	.align 8
.LC45:
	.string	"Splitting live range %d:%d at %d\n"
	.section	.text._ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	.type	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE, @function
_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE:
.LFB23480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	testb	$4, 488(%rax)
	jne	.L1539
.L1529:
	movq	16(%r12), %rax
	cmpl	(%rax), %r13d
	jg	.L1540
.L1530:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1540:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	(%rax), %rcx
	movq	32(%r12), %rax
	.p2align 4,,10
	.p2align 3
.L1531:
	movq	%rax, %rdx
	movq	96(%rax), %rax
	testq	%rax, %rax
	jne	.L1531
	movl	92(%rdx), %eax
	leal	1(%rax), %r15d
	movl	%r15d, 92(%rdx)
	movq	16(%rcx), %rbx
	movq	24(%rcx), %rax
	movl	4(%r12), %r14d
	movq	32(%r12), %rdx
	subq	%rbx, %rax
	cmpq	$87, %rax
	jbe	.L1541
	leaq	88(%rbx), %rax
	movq	%rax, 16(%rcx)
.L1533:
	andl	$2088960, %r14d
	movl	%r15d, (%rbx)
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	pxor	%xmm0, %xmm0
	movq	%rdx, 32(%rbx)
	movl	%r13d, %esi
	orl	$134221824, %r14d
	movups	%xmm0, 72(%rbx)
	movq	%rbx, %rdx
	movq	$0, 24(%rbx)
	movl	%r14d, 4(%rbx)
	movups	%xmm0, 8(%rbx)
	movups	%xmm0, 40(%rbx)
	movups	%xmm0, 56(%rbx)
	movq	80(%r12), %rax
	movq	%rax, 80(%rbx)
	call	_ZN2v88internal8compiler9LiveRange8DetachAtENS1_16LifetimePositionEPS2_PNS0_4ZoneENS2_20HintConnectionOptionE
	movq	32(%r12), %rax
	movq	%rax, 32(%rbx)
	movq	40(%r12), %rax
	movq	%rax, 40(%rbx)
	movq	%rbx, 40(%r12)
	movq	%rbx, %r12
	jmp	.L1530
	.p2align 4,,10
	.p2align 3
.L1539:
	movq	32(%rsi), %rax
	movl	%edx, %ecx
	movl	(%r12), %edx
	leaq	.LC45(%rip), %rdi
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1529
	.p2align 4,,10
	.p2align 3
.L1541:
	movq	%rcx, %rdi
	movl	$88, %esi
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L1533
	.cfi_endproc
.LFE23480:
	.size	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE, .-_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	.section	.rodata._ZN2v88internal8compiler17RegisterAllocator12SplitBetweenEPNS1_9LiveRangeENS1_16LifetimePositionES5_.str1.8,"aMS",@progbits,1
	.align 8
.LC46:
	.string	"Splitting live range %d:%d in position between [%d, %d]\n"
	.section	.text._ZN2v88internal8compiler17RegisterAllocator12SplitBetweenEPNS1_9LiveRangeENS1_16LifetimePositionES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17RegisterAllocator12SplitBetweenEPNS1_9LiveRangeENS1_16LifetimePositionES5_
	.type	_ZN2v88internal8compiler17RegisterAllocator12SplitBetweenEPNS1_9LiveRangeENS1_16LifetimePositionES5_, @function
_ZN2v88internal8compiler17RegisterAllocator12SplitBetweenEPNS1_9LiveRangeENS1_16LifetimePositionES5_:
.LFB23481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	testb	$4, 488(%rax)
	jne	.L1558
.L1543:
	testl	%r13d, %r13d
	leal	3(%r13), %esi
	cmovns	%r13d, %esi
	leal	3(%r12), %r13d
	sarl	$2, %esi
	testl	%r12d, %r12d
	cmovns	%r12d, %r13d
	sarl	$2, %r13d
	cmpl	%r13d, %esi
	jne	.L1559
.L1544:
	addq	$8, %rsp
	movl	%r12d, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	.p2align 4,,10
	.p2align 3
.L1559:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movl	%r13d, %esi
	movq	%rax, %rbx
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	cmpq	%rax, %rbx
	je	.L1544
	movq	%rax, %rcx
	movq	(%r14), %rdx
	movslq	104(%rcx), %rsi
	movq	16(%rdx), %rdi
	testl	%esi, %esi
	js	.L1549
.L1545:
	movq	16(%rdi), %rdx
	movq	8(%rdx), %r8
	movq	16(%rdx), %rdx
	subq	%r8, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1560
	movq	(%r8,%rsi,8), %rdx
	testq	%rdx, %rdx
	je	.L1549
	movl	100(%rbx), %esi
	cmpl	%esi, 100(%rdx)
	jle	.L1549
	movq	%rdx, %rcx
	movslq	104(%rcx), %rsi
	testl	%esi, %esi
	jns	.L1545
.L1549:
	cmpq	%rcx, %rax
	je	.L1561
.L1547:
	movl	112(%rcx), %r12d
	sall	$2, %r12d
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1558:
	movq	32(%rsi), %rax
	movl	%ecx, %r8d
	movl	%edx, %ecx
	movl	(%r15), %edx
	leaq	.LC46(%rip), %rdi
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1561:
	movl	108(%rcx), %eax
	testl	%eax, %eax
	jns	.L1547
	jmp	.L1544
.L1560:
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE23481:
	.size	_ZN2v88internal8compiler17RegisterAllocator12SplitBetweenEPNS1_9LiveRangeENS1_16LifetimePositionES5_, .-_ZN2v88internal8compiler17RegisterAllocator12SplitBetweenEPNS1_9LiveRangeENS1_16LifetimePositionES5_
	.section	.text._ZN2v88internal8compiler17RegisterAllocator19FindOptimalSplitPosENS1_16LifetimePositionES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17RegisterAllocator19FindOptimalSplitPosENS1_16LifetimePositionES3_
	.type	_ZN2v88internal8compiler17RegisterAllocator19FindOptimalSplitPosENS1_16LifetimePositionES3_, @function
_ZN2v88internal8compiler17RegisterAllocator19FindOptimalSplitPosENS1_16LifetimePositionES3_:
.LFB23482:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addl	$3, %esi
	testl	%ecx, %ecx
	cmovns	%ecx, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	sarl	$2, %esi
	pushq	%r13
	testl	%edx, %edx
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leal	3(%rdx), %r12d
	cmovns	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	sarl	$2, %r12d
	cmpl	%r12d, %esi
	jne	.L1563
.L1565:
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1563:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdi, %r13
	movq	16(%rax), %rdi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movl	%r12d, %esi
	movq	%rax, %r14
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	cmpq	%r14, %rax
	je	.L1565
	movq	%rax, %rcx
	movq	0(%r13), %rdx
	movslq	104(%rcx), %rsi
	movq	16(%rdx), %r8
	testl	%esi, %esi
	js	.L1570
.L1566:
	movq	16(%r8), %rdx
	movq	8(%rdx), %rdi
	movq	16(%rdx), %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1576
	movq	(%rdi,%rsi,8), %rdx
	testq	%rdx, %rdx
	je	.L1570
	movl	100(%rdx), %edi
	cmpl	%edi, 100(%r14)
	jge	.L1570
	movq	%rdx, %rcx
	movslq	104(%rcx), %rsi
	testl	%esi, %esi
	jns	.L1566
.L1570:
	cmpq	%rax, %rcx
	je	.L1577
.L1568:
	movl	112(%rcx), %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	sall	$2, %eax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1577:
	.cfi_restore_state
	movl	108(%rcx), %eax
	testl	%eax, %eax
	jns	.L1568
	jmp	.L1565
.L1576:
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE23482:
	.size	_ZN2v88internal8compiler17RegisterAllocator19FindOptimalSplitPosENS1_16LifetimePositionES3_, .-_ZN2v88internal8compiler17RegisterAllocator19FindOptimalSplitPosENS1_16LifetimePositionES3_
	.section	.text._ZN2v88internal8compiler17RegisterAllocator22FindOptimalSpillingPosEPNS1_9LiveRangeENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeEPS4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17RegisterAllocator22FindOptimalSpillingPosEPNS1_9LiveRangeENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeEPS4_
	.type	_ZN2v88internal8compiler17RegisterAllocator22FindOptimalSpillingPosEPNS1_9LiveRangeENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeEPS4_, @function
_ZN2v88internal8compiler17RegisterAllocator22FindOptimalSpillingPosEPNS1_9LiveRangeENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeEPS4_:
.LFB23483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	%rsi, (%r8)
	cmpl	$1, %ecx
	jne	.L1579
.L1584:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1579:
	.cfi_restore_state
	movl	%edx, %eax
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movl	%edx, %r15d
	andl	$-2, %eax
	leal	3(%rax), %esi
	cmovns	%eax, %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rdi
	sarl	$2, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movq	%rax, %r10
	movl	108(%rax), %eax
	testl	%eax, %eax
	js	.L1646
	movq	(%rbx), %rcx
.L1585:
	testb	$1, 488(%rcx)
	je	.L1586
.L1606:
	movl	112(%r10), %eax
	leal	0(,%rax,4), %esi
	movslq	100(%r10), %rax
	salq	$5, %rax
	addq	464(%rcx), %rax
	movq	8(%rax), %r9
	movq	16(%rax), %r11
	cmpq	%r11, %r9
	jne	.L1589
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1591:
	addq	$8, %r9
	cmpq	%r9, %r11
	je	.L1647
.L1589:
	movq	(%r9), %rdi
	movq	32(%r13), %rax
	cmpq	%rax, 32(%rdi)
	jne	.L1591
	call	_ZNK2v88internal8compiler9LiveRange6CoversENS1_16LifetimePositionE
	testb	%al, %al
	je	.L1591
	testb	$1, 4(%rdi)
	jne	.L1591
	movq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L1592:
	movq	16(%rdx), %rax
	cmpl	%r15d, (%rax)
	jge	.L1603
	movq	56(%rdx), %rax
	testq	%rax, %rax
	je	.L1594
	movl	24(%rax), %ecx
	cmpl	%ecx, %esi
	jge	.L1600
.L1594:
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L1643
	jmp	.L1599
	.p2align 4,,10
	.p2align 3
.L1648:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1599
.L1643:
	movl	24(%rax), %ecx
.L1600:
	cmpl	%ecx, %esi
	jg	.L1648
	movq	%rax, 56(%rdx)
	jmp	.L1602
	.p2align 4,,10
	.p2align 3
.L1649:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1613
.L1602:
	testb	$32, 28(%rax)
	je	.L1649
	cmpl	%r15d, 24(%rax)
	jl	.L1604
.L1613:
	movq	40(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1592
.L1603:
	movq	%rdi, (%r12)
	movq	(%rbx), %rcx
	movl	%esi, %r15d
.L1587:
	movslq	104(%r10), %rsi
	testl	%esi, %esi
	js	.L1604
	movq	16(%rcx), %rax
	movq	16(%rax), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1645
	movq	(%rax,%rsi,8), %r10
	testq	%r10, %r10
	jne	.L1606
.L1604:
	addq	$8, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1646:
	.cfi_restore_state
	movslq	104(%r10), %rsi
	testl	%esi, %esi
	js	.L1584
	movq	(%rbx), %rcx
	movq	16(%rcx), %rax
	movq	16(%rax), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1645
	movq	(%rax,%rsi,8), %r10
	testq	%r10, %r10
	je	.L1584
	jmp	.L1585
	.p2align 4,,10
	.p2align 3
.L1586:
	movq	24(%r13), %rax
	xorl	%r9d, %r9d
	testq	%rax, %rax
	jne	.L1609
	jmp	.L1612
	.p2align 4,,10
	.p2align 3
.L1650:
	testb	$32, 28(%rax)
	cmovne	%rax, %r9
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1612
.L1609:
	cmpl	24(%rax), %r14d
	jg	.L1650
	.p2align 4,,10
	.p2align 3
.L1612:
	movl	112(%r10), %eax
	movq	%r13, %rdi
	leal	0(,%rax,4), %esi
	call	_ZNK2v88internal8compiler9LiveRange6CoversENS1_16LifetimePositionE
	testb	%al, %al
	je	.L1610
	testq	%r9, %r9
	je	.L1616
	cmpl	%esi, 24(%r9)
	cmovl	%esi, %r15d
.L1610:
	movslq	104(%r10), %rsi
	testl	%esi, %esi
	js	.L1604
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1645
	movq	(%rax,%rsi,8), %r10
	testq	%r10, %r10
	jne	.L1612
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1599:
	movq	$0, 56(%rdx)
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L1647:
	movq	(%rbx), %rcx
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1616:
	movl	%esi, %r15d
	jmp	.L1610
.L1645:
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE23483:
	.size	_ZN2v88internal8compiler17RegisterAllocator22FindOptimalSpillingPosEPNS1_9LiveRangeENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeEPS4_, .-_ZN2v88internal8compiler17RegisterAllocator22FindOptimalSpillingPosEPNS1_9LiveRangeENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeEPS4_
	.section	.rodata._ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC47:
	.string	"Spilling live range %d:%d mode %d\n"
	.section	.rodata._ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE.str1.1,"aMS",@progbits,1
.LC48:
	.string	"Starting spill type is %d\n"
.LC49:
	.string	"New spill range needed"
.LC50:
	.string	"Upgrading\n"
.LC51:
	.string	"Final spill type is %d\n"
	.section	.text._ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	.type	_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE, @function
_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE:
.LFB23484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rdi
	movq	%rsi, %rbx
	movq	32(%rsi), %r14
	testb	$4, 488(%rdi)
	jne	.L1682
.L1652:
	movl	4(%r14), %eax
	movl	%eax, %edx
	shrl	$5, %edx
	movl	%edx, %ecx
	andl	$3, %ecx
	jne	.L1683
.L1655:
	movl	%r13d, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler22RegisterAllocationData27AssignSpillRangeToLiveRangeEPNS1_17TopLevelLiveRangeENS2_9SpillModeE
.L1681:
	movq	(%r12), %rax
	movl	488(%rax), %eax
	andl	$4, %eax
	testl	%r13d, %r13d
	jne	.L1656
	movl	4(%r14), %edx
	movl	%edx, %ecx
	xorl	$96, %ecx
	andl	$96, %ecx
	je	.L1684
.L1656:
	testl	%eax, %eax
	je	.L1659
	movl	4(%r14), %esi
	leaq	.LC51(%rip), %rdi
	xorl	%eax, %eax
	shrl	$5, %esi
	andl	$3, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L1659:
	movl	4(%rbx), %eax
	andl	$-8066, %eax
	orl	$4097, %eax
	movl	%eax, 4(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1683:
	.cfi_restore_state
	testl	%r13d, %r13d
	jne	.L1659
	movl	%eax, %edx
	cmpl	$3, %ecx
	jne	.L1659
.L1658:
	movl	%edx, %eax
	andl	$-97, %eax
	orl	$64, %eax
	movl	%eax, 4(%r14)
	movq	(%r12), %rax
	movl	488(%rax), %eax
	andl	$4, %eax
	jmp	.L1656
	.p2align 4,,10
	.p2align 3
.L1682:
	movl	%edx, %ecx
	movl	88(%r14), %esi
	movl	(%rbx), %edx
	leaq	.LC47(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rdi
	testb	$4, 488(%rdi)
	je	.L1652
	movl	4(%r14), %esi
	xorl	%eax, %eax
	leaq	.LC48(%rip), %rdi
	shrl	$5, %esi
	andl	$3, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	testb	$96, 4(%r14)
	jne	.L1681
	movq	(%r12), %rdi
	testb	$4, 488(%rdi)
	je	.L1655
	leaq	.LC49(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r12), %rdi
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1684:
	testl	%eax, %eax
	je	.L1658
	leaq	.LC50(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	4(%r14), %edx
	jmp	.L1658
	.cfi_endproc
.LFE23484:
	.size	_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE, .-_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	.section	.rodata._ZN2v88internal8compiler17RegisterAllocator41SplitAndSpillRangesDefinedByMemoryOperandEv.str1.8,"aMS",@progbits,1
	.align 8
.LC52:
	.string	"initial_range_count == data()->live_ranges().size()"
	.align 8
.LC53:
	.string	"Live range %d:%d is defined by a spill operand.\n"
	.section	.text._ZN2v88internal8compiler17RegisterAllocator41SplitAndSpillRangesDefinedByMemoryOperandEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17RegisterAllocator41SplitAndSpillRangesDefinedByMemoryOperandEv
	.type	_ZN2v88internal8compiler17RegisterAllocator41SplitAndSpillRangesDefinedByMemoryOperandEv, @function
_ZN2v88internal8compiler17RegisterAllocator41SplitAndSpillRangesDefinedByMemoryOperandEv:
.LFB23479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	168(%rdx), %rsi
	movq	176(%rdx), %r14
	subq	%rsi, %r14
	movq	%r14, %r15
	sarq	$3, %r15
	je	.L1685
	movq	%rdi, %r13
	xorl	%ebx, %ebx
	leaq	.LC53(%rip), %rcx
	jmp	.L1686
	.p2align 4,,10
	.p2align 3
.L1688:
	addq	$1, %rbx
	cmpq	%rbx, %r15
	je	.L1685
.L1729:
	movq	0(%r13), %rdx
	movq	168(%rdx), %rsi
	movq	176(%rdx), %rax
	subq	%rsi, %rax
	cmpq	%r14, %rax
	jne	.L1734
.L1686:
	movq	(%rsi,%rbx,8), %r12
	testq	%r12, %r12
	je	.L1688
	movq	16(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1688
	movl	4(%r12), %eax
	movl	%eax, %edi
	shrl	$13, %edi
	cmpb	$11, %dil
	seta	%dil
	movzbl	%dil, %edi
	cmpl	%edi, 8(%r13)
	jne	.L1688
	movl	%eax, %edi
	shrl	$5, %edi
	andl	$3, %edi
	je	.L1688
	cmpl	$1, %edi
	je	.L1690
	shrl	%eax
	andl	$3, %eax
	cmpl	$2, %eax
	jne	.L1688
.L1690:
	movl	(%rsi), %r8d
	testb	$4, 488(%rdx)
	jne	.L1735
.L1691:
	movl	%r8d, %eax
	andl	$-2, %eax
	addl	$2, %eax
	testb	$2, %r8b
	cmove	%eax, %r8d
	cmpq	$0, 96(%r12)
	je	.L1693
	movq	56(%r12), %rax
	testq	%rax, %rax
	je	.L1694
	movl	24(%rax), %edx
	cmpl	%edx, %r8d
	jge	.L1700
.L1694:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L1732
	.p2align 4,,10
	.p2align 3
.L1699:
	movq	$0, 56(%r12)
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1734:
	leaq	.LC52(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1736:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1699
.L1732:
	movl	24(%rax), %edx
.L1700:
	cmpl	%edx, %r8d
	jg	.L1736
	movq	%rax, 56(%r12)
	jmp	.L1702
	.p2align 4,,10
	.p2align 3
.L1737:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1733
.L1702:
	movzbl	28(%rax), %edx
	andl	$3, %edx
	cmpb	$2, %dl
	jne	.L1737
.L1703:
	testq	%rax, %rax
	je	.L1733
	movq	16(%r12), %rdx
	movl	24(%rax), %edi
	movl	(%rdx), %esi
	movl	%esi, %eax
	andl	$-2, %eax
	addl	$2, %eax
	cmpl	%eax, %edi
	jle	.L1688
	testl	%edi, %edi
	leal	3(%rdi), %edx
	cmovns	%edi, %edx
	andl	$-4, %edx
	cmpl	%edx, %esi
	jge	.L1688
	movq	8(%r12), %rax
	cmpl	4(%rax), %edx
	jge	.L1688
	andl	$-4, %esi
	movq	%r13, %rdi
	addl	$4, %esi
	call	_ZN2v88internal8compiler17RegisterAllocator19FindOptimalSplitPosENS1_16LifetimePositionES3_
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
.L1733:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	addq	$1, %rbx
	call	_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	leaq	.LC53(%rip), %rcx
	cmpq	%rbx, %r15
	jne	.L1729
.L1685:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1735:
	.cfi_restore_state
	movq	32(%r12), %rax
	movl	(%r12), %edx
	movq	%rcx, %rdi
	movl	%r8d, -52(%rbp)
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	-52(%rbp), %r8d
	leaq	.LC53(%rip), %rcx
	jmp	.L1691
	.p2align 4,,10
	.p2align 3
.L1693:
	movl	%r8d, %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9LiveRange35NextUsePositionRegisterIsBeneficialENS1_16LifetimePositionE
	leaq	.LC53(%rip), %rcx
	jmp	.L1703
	.cfi_endproc
.LFE23479:
	.size	_ZN2v88internal8compiler17RegisterAllocator41SplitAndSpillRangesDefinedByMemoryOperandEv, .-_ZN2v88internal8compiler17RegisterAllocator41SplitAndSpillRangesDefinedByMemoryOperandEv
	.section	.rodata._ZNK2v88internal8compiler17RegisterAllocator12RegisterNameEi.str1.1,"aMS",@progbits,1
.LC54:
	.string	"invalid"
	.section	.text._ZNK2v88internal8compiler17RegisterAllocator12RegisterNameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler17RegisterAllocator12RegisterNameEi
	.type	_ZNK2v88internal8compiler17RegisterAllocator12RegisterNameEi, @function
_ZNK2v88internal8compiler17RegisterAllocator12RegisterNameEi:
.LFB23485:
	.cfi_startproc
	endbr64
	leaq	.LC26(%rip), %rax
	cmpl	$32, %esi
	je	.L1738
	movl	8(%rdi), %edx
	leaq	.LC54(%rip), %rax
	testl	%edx, %edx
	jne	.L1740
	cmpl	$-1, %esi
	je	.L1738
	movslq	%esi, %rsi
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1740:
	cmpl	$-1, %esi
	je	.L1738
	movslq	%esi, %rsi
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rax
	movq	(%rax,%rsi,8), %rax
.L1738:
	ret
	.cfi_endproc
.LFE23485:
	.size	_ZNK2v88internal8compiler17RegisterAllocator12RegisterNameEi, .-_ZNK2v88internal8compiler17RegisterAllocator12RegisterNameEi
	.section	.text._ZN2v88internal8compiler19LinearScanAllocatorC2EPNS1_22RegisterAllocationDataENS1_12RegisterKindEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocatorC2EPNS1_22RegisterAllocationDataENS1_12RegisterKindEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler19LinearScanAllocatorC2EPNS1_22RegisterAllocationDataENS1_12RegisterKindEPNS0_4ZoneE, @function
_ZN2v88internal8compiler19LinearScanAllocatorC2EPNS1_22RegisterAllocationDataENS1_12RegisterKindEPNS0_4ZoneE:
.LFB23493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	32(%rsi), %rax
	movq	%rcx, %rdi
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	cmpl	$1, %edx
	je	.L1772
	movl	8(%rax), %edx
	movl	%edx, 12(%rbx)
	movl	24(%rax), %edx
	movq	56(%rax), %rax
	movl	%edx, 16(%rbx)
.L1749:
	movq	%rax, 24(%rbx)
	leaq	56(%rbx), %rax
	movb	$0, 32(%rbx)
	movq	%rdi, 40(%rbx)
	movl	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	movq	%rax, 72(%rbx)
	movq	%rax, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	%rdi, 96(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	movq	%rdi, 128(%rbx)
	movq	$0, 136(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 152(%rbx)
	movq	16(%rdi), %rdx
	movq	$-1, 160(%rbx)
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	$63, %rax
	jbe	.L1773
	leaq	64(%rdx), %rax
	movq	%rdx, 104(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 16(%rdi)
	movq	%rdx, 112(%rbx)
	movq	%rax, 120(%rbx)
.L1751:
	movq	128(%rbx), %rdi
	movq	144(%rbx), %r13
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	movq	%r13, %r14
	subq	%r12, %r14
	subq	%rax, %rdx
	cmpq	$63, %rdx
	jbe	.L1774
	leaq	64(%rax), %rsi
	movq	%rsi, 16(%rdi)
.L1754:
	cmpq	%r12, %r13
	je	.L1759
	subq	$8, %r13
	leaq	15(%rax), %rdx
	subq	%r12, %r13
	subq	%r12, %rdx
	movq	%r13, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L1756
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rdi
	je	.L1756
	addq	$1, %rdi
	movq	%r12, %r8
	movq	%rax, %rdx
	movq	%rdi, %rcx
	subq	%rax, %r8
	shrq	%rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1757:
	movdqu	(%rdx,%r8), %xmm0
	addq	$16, %rdx
	movups	%xmm0, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L1757
	movq	%rdi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %r12
	addq	%rax, %rcx
	cmpq	%rdx, %rdi
	je	.L1759
	movq	(%r12), %rdx
	movq	%rdx, (%rcx)
.L1759:
	movq	%rax, 136(%rbx)
	addq	%r14, %rax
	movq	%rax, 144(%rbx)
	movq	%rsi, 152(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1773:
	.cfi_restore_state
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	136(%rbx), %r12
	movq	152(%rbx), %rdx
	movq	%rax, 104(%rbx)
	movq	%rax, 112(%rbx)
	subq	%r12, %rdx
	addq	$64, %rax
	movq	%rax, 120(%rbx)
	cmpq	$63, %rdx
	jbe	.L1751
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1772:
	.cfi_restore_state
	movl	16(%rax), %edx
	movl	%edx, 12(%rbx)
	movl	32(%rax), %edx
	movq	192(%rax), %rax
	movl	%edx, 16(%rbx)
	jmp	.L1749
	.p2align 4,,10
	.p2align 3
.L1756:
	leaq	8(%rax,%r13), %rdi
	movq	%rax, %rdx
	subq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L1761:
	movq	(%rdx,%r12), %rcx
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rdi, %rdx
	jne	.L1761
	jmp	.L1759
	.p2align 4,,10
	.p2align 3
.L1774:
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	64(%rax), %rsi
	jmp	.L1754
	.cfi_endproc
.LFE23493:
	.size	_ZN2v88internal8compiler19LinearScanAllocatorC2EPNS1_22RegisterAllocationDataENS1_12RegisterKindEPNS0_4ZoneE, .-_ZN2v88internal8compiler19LinearScanAllocatorC2EPNS1_22RegisterAllocationDataENS1_12RegisterKindEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler19LinearScanAllocatorC1EPNS1_22RegisterAllocationDataENS1_12RegisterKindEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler19LinearScanAllocatorC1EPNS1_22RegisterAllocationDataENS1_12RegisterKindEPNS0_4ZoneE,_ZN2v88internal8compiler19LinearScanAllocatorC2EPNS1_22RegisterAllocationDataENS1_12RegisterKindEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator24MaybeSpillPreviousRangesEPNS1_9LiveRangeENS1_16LifetimePositionES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator24MaybeSpillPreviousRangesEPNS1_9LiveRangeENS1_16LifetimePositionES4_
	.type	_ZN2v88internal8compiler19LinearScanAllocator24MaybeSpillPreviousRangesEPNS1_9LiveRangeENS1_16LifetimePositionES4_, @function
_ZN2v88internal8compiler19LinearScanAllocator24MaybeSpillPreviousRangesEPNS1_9LiveRangeENS1_16LifetimePositionES4_:
.LFB23495:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rsi
	je	.L1782
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	subq	$8, %rsp
	testb	$1, 4(%rsi)
	je	.L1785
.L1777:
	movq	40(%r12), %rax
	cmpq	%rax, %rbx
	je	.L1775
	.p2align 4,,10
	.p2align 3
.L1780:
	movl	4(%rax), %edx
	testb	$1, %dl
	jne	.L1778
	andl	$-8066, %edx
	orl	$4097, %edx
	movl	%edx, 4(%rax)
	movq	40(%rax), %rax
	cmpq	%rax, %rbx
	jne	.L1780
.L1775:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1778:
	.cfi_restore_state
	movq	40(%rax), %rax
	cmpq	%rax, %rbx
	jne	.L1780
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1785:
	.cfi_restore_state
	movq	%rdi, %r13
	call	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	jmp	.L1777
	.p2align 4,,10
	.p2align 3
.L1782:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE23495:
	.size	_ZN2v88internal8compiler19LinearScanAllocator24MaybeSpillPreviousRangesEPNS1_9LiveRangeENS1_16LifetimePositionES4_, .-_ZN2v88internal8compiler19LinearScanAllocator24MaybeSpillPreviousRangesEPNS1_9LiveRangeENS1_16LifetimePositionES4_
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator27ConsiderBlockForControlFlowEPNS1_16InstructionBlockENS1_9RpoNumberE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator27ConsiderBlockForControlFlowEPNS1_16InstructionBlockENS1_9RpoNumberE
	.type	_ZN2v88internal8compiler19LinearScanAllocator27ConsiderBlockForControlFlowEPNS1_16InstructionBlockENS1_9RpoNumberE, @function
_ZN2v88internal8compiler19LinearScanAllocator27ConsiderBlockForControlFlowEPNS1_16InstructionBlockENS1_9RpoNumberE:
.LFB23569:
	.cfi_startproc
	endbr64
	cmpl	%edx, 100(%rsi)
	jle	.L1789
	movzbl	120(%rsi), %eax
	testb	%al, %al
	je	.L1793
	ret
	.p2align 4,,10
	.p2align 3
.L1789:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1793:
	movq	(%rdi), %rax
	movslq	%edx, %rsi
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rcx
	movq	16(%rax), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1794
	movq	(%rcx,%rsi,8), %rax
	movzbl	120(%rax), %eax
	xorl	$1, %eax
	ret
.L1794:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE23569:
	.size	_ZN2v88internal8compiler19LinearScanAllocator27ConsiderBlockForControlFlowEPNS1_16InstructionBlockENS1_9RpoNumberE, .-_ZN2v88internal8compiler19LinearScanAllocator27ConsiderBlockForControlFlowEPNS1_16InstructionBlockENS1_9RpoNumberE
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator50BlockIsDeferredOrImmediatePredecessorIsNotDeferredEPKNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator50BlockIsDeferredOrImmediatePredecessorIsNotDeferredEPKNS1_16InstructionBlockE
	.type	_ZN2v88internal8compiler19LinearScanAllocator50BlockIsDeferredOrImmediatePredecessorIsNotDeferredEPKNS1_16InstructionBlockE, @function
_ZN2v88internal8compiler19LinearScanAllocator50BlockIsDeferredOrImmediatePredecessorIsNotDeferredEPKNS1_16InstructionBlockE:
.LFB23578:
	.cfi_startproc
	endbr64
	movzbl	120(%rsi), %eax
	testb	%al, %al
	jne	.L1795
	movq	40(%rsi), %rax
	movq	48(%rsi), %rcx
	cmpq	%rax, %rcx
	je	.L1801
	movl	100(%rsi), %r8d
	.p2align 4,,10
	.p2align 3
.L1800:
	movslq	(%rax), %rsi
	leal	1(%rsi), %edx
	cmpl	%r8d, %edx
	je	.L1806
	addq	$4, %rax
	cmpq	%rax, %rcx
	jne	.L1800
.L1801:
	movl	$1, %eax
.L1795:
	ret
	.p2align 4,,10
	.p2align 3
.L1806:
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1807
	movq	(%rax,%rsi,8), %rax
	movzbl	120(%rax), %eax
	xorl	$1, %eax
	ret
.L1807:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE23578:
	.size	_ZN2v88internal8compiler19LinearScanAllocator50BlockIsDeferredOrImmediatePredecessorIsNotDeferredEPKNS1_16InstructionBlockE, .-_ZN2v88internal8compiler19LinearScanAllocator50BlockIsDeferredOrImmediatePredecessorIsNotDeferredEPKNS1_16InstructionBlockE
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator25HasNonDeferredPredecessorEPNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator25HasNonDeferredPredecessorEPNS1_16InstructionBlockE
	.type	_ZN2v88internal8compiler19LinearScanAllocator25HasNonDeferredPredecessorEPNS1_16InstructionBlockE, @function
_ZN2v88internal8compiler19LinearScanAllocator25HasNonDeferredPredecessorEPNS1_16InstructionBlockE:
.LFB23579:
	.cfi_startproc
	endbr64
	movq	40(%rsi), %rax
	movq	48(%rsi), %r8
	cmpq	%r8, %rax
	je	.L1812
	movq	(%rdi), %rdx
	movq	16(%rdx), %rdx
	movq	16(%rdx), %rdx
	movq	8(%rdx), %rdi
	movq	16(%rdx), %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	jmp	.L1811
	.p2align 4,,10
	.p2align 3
.L1819:
	addq	$4, %rax
	cmpq	%rax, %r8
	je	.L1812
.L1811:
	movslq	(%rax), %rsi
	cmpq	%rdx, %rsi
	jnb	.L1818
	movq	(%rdi,%rsi,8), %rcx
	cmpb	$0, 120(%rcx)
	jne	.L1819
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1812:
	xorl	%eax, %eax
	ret
.L1818:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE23579:
	.size	_ZN2v88internal8compiler19LinearScanAllocator25HasNonDeferredPredecessorEPNS1_16InstructionBlockE, .-_ZN2v88internal8compiler19LinearScanAllocator25HasNonDeferredPredecessorEPNS1_16InstructionBlockE
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator28SetLiveRangeAssignedRegisterEPNS1_9LiveRangeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator28SetLiveRangeAssignedRegisterEPNS1_9LiveRangeEi
	.type	_ZN2v88internal8compiler19LinearScanAllocator28SetLiveRangeAssignedRegisterEPNS1_9LiveRangeEi, @function
_ZN2v88internal8compiler19LinearScanAllocator28SetLiveRangeAssignedRegisterEPNS1_9LiveRangeEi:
.LFB23589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movl	4(%rsi), %esi
	movq	%rdi, %r10
	movq	(%rdi), %rdi
	movl	%edx, %r9d
	shrl	$13, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler22RegisterAllocationData13MarkAllocatedENS0_21MachineRepresentationEi
	movl	4(%r8), %eax
	movl	%r9d, %edx
	sall	$7, %edx
	andl	$-8065, %eax
	orl	%edx, %eax
	movl	%eax, 4(%r8)
	movq	24(%r8), %rax
	testq	%rax, %rax
	je	.L1821
	movl	%r9d, %edx
	sall	$6, %edx
	.p2align 4,,10
	.p2align 3
.L1823:
	cmpq	$0, (%rax)
	je	.L1822
	movl	28(%rax), %ecx
	movl	%ecx, %esi
	andl	$3, %esi
	cmpb	$3, %sil
	je	.L1822
	andl	$-4033, %ecx
	orl	%edx, %ecx
	movl	%ecx, 28(%rax)
.L1822:
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1823
.L1821:
	movq	80(%r8), %rax
	testq	%rax, %rax
	je	.L1824
	cmpl	$32, 116(%rax)
	je	.L1849
.L1824:
	cmpq	32(%r8), %r8
	je	.L1850
.L1820:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1849:
	.cfi_restore_state
	movl	%r9d, 116(%rax)
	cmpq	32(%r8), %r8
	jne	.L1820
.L1850:
	testb	$8, 4(%r8)
	je	.L1820
	movq	(%r10), %rax
	movl	88(%r8), %edx
	leaq	56(%rax), %rsi
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L1826
	movq	%rsi, %rcx
	jmp	.L1827
	.p2align 4,,10
	.p2align 3
.L1851:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1828
.L1827:
	cmpl	32(%rax), %edx
	jle	.L1851
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1827
.L1828:
	cmpq	%rcx, %rsi
	je	.L1826
	cmpl	32(%rcx), %edx
	cmovge	%rcx, %rsi
.L1826:
	movq	40(%rsi), %rax
	movl	%r9d, 48(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23589:
	.size	_ZN2v88internal8compiler19LinearScanAllocator28SetLiveRangeAssignedRegisterEPNS1_9LiveRangeEi, .-_ZN2v88internal8compiler19LinearScanAllocator28SetLiveRangeAssignedRegisterEPNS1_9LiveRangeEi
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator15ActiveToHandledEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC55:
	.string	"Moving live range %d:%d from active to handled\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator15ActiveToHandledEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator15ActiveToHandledEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEE
	.type	_ZN2v88internal8compiler19LinearScanAllocator15ActiveToHandledEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEE, @function
_ZN2v88internal8compiler19LinearScanAllocator15ActiveToHandledEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEE:
.LFB23593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	testb	$4, 488(%rax)
	jne	.L1859
.L1853:
	movq	112(%rbx), %rdx
	leaq	8(%r12), %rsi
	cmpq	%rsi, %rdx
	je	.L1854
	subq	%rsi, %rdx
	movq	%r12, %rdi
	call	memmove@PLT
	movq	112(%rbx), %rsi
.L1854:
	subq	$8, %rsi
	movq	%r12, %rax
	movq	%rsi, 112(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1859:
	.cfi_restore_state
	movq	(%rsi), %rax
	leaq	.LC55(%rip), %rdi
	movq	32(%rax), %rdx
	movl	88(%rdx), %esi
	movl	(%rax), %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1853
	.cfi_endproc
.LFE23593:
	.size	_ZN2v88internal8compiler19LinearScanAllocator15ActiveToHandledEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEE, .-_ZN2v88internal8compiler19LinearScanAllocator15ActiveToHandledEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEE
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator17InactiveToHandledEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC56:
	.string	"Moving live range %d:%d from inactive to handled\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator17InactiveToHandledEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator17InactiveToHandledEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEE
	.type	_ZN2v88internal8compiler19LinearScanAllocator17InactiveToHandledEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEE, @function
_ZN2v88internal8compiler19LinearScanAllocator17InactiveToHandledEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEE:
.LFB23595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	testb	$4, 488(%rax)
	jne	.L1867
.L1861:
	movq	144(%rbx), %rdx
	leaq	8(%r12), %rsi
	cmpq	%rsi, %rdx
	je	.L1862
	subq	%rsi, %rdx
	movq	%r12, %rdi
	call	memmove@PLT
	movq	144(%rbx), %rsi
.L1862:
	subq	$8, %rsi
	movq	%r12, %rax
	movq	%rsi, 144(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1867:
	.cfi_restore_state
	movq	(%rsi), %rax
	leaq	.LC56(%rip), %rdi
	movq	32(%rax), %rdx
	movl	88(%rdx), %esi
	movl	(%rax), %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1861
	.cfi_endproc
.LFE23595:
	.size	_ZN2v88internal8compiler19LinearScanAllocator17InactiveToHandledEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEE, .-_ZN2v88internal8compiler19LinearScanAllocator17InactiveToHandledEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEE
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator28LastDeferredInstructionIndexEPNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator28LastDeferredInstructionIndexEPNS1_16InstructionBlockE
	.type	_ZN2v88internal8compiler19LinearScanAllocator28LastDeferredInstructionIndexEPNS1_16InstructionBlockE, @function
_ZN2v88internal8compiler19LinearScanAllocator28LastDeferredInstructionIndexEPNS1_16InstructionBlockE:
.LFB23598:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rsi, %rcx
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movq	16(%rax), %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	leal	-1(%rdx), %r8d
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L1877:
	addl	$1, %esi
	movslq	%esi, %rsi
	cmpq	%rdx, %rsi
	jnb	.L1876
	movq	(%rdi,%rsi,8), %rax
	cmpb	$0, 120(%rax)
	je	.L1869
	movq	%rax, %rcx
.L1871:
	movl	100(%rcx), %esi
	cmpl	%r8d, %esi
	jl	.L1877
.L1869:
	movl	116(%rcx), %eax
	subl	$1, %eax
	ret
.L1876:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE23598:
	.size	_ZN2v88internal8compiler19LinearScanAllocator28LastDeferredInstructionIndexEPNS1_16InstructionBlockE, .-_ZN2v88internal8compiler19LinearScanAllocator28LastDeferredInstructionIndexEPNS1_16InstructionBlockE
	.section	.text._ZNK2v88internal8compiler19LinearScanAllocator16GetFPRegisterSetENS0_21MachineRepresentationEPiS4_PPKi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19LinearScanAllocator16GetFPRegisterSetENS0_21MachineRepresentationEPiS4_PPKi
	.type	_ZNK2v88internal8compiler19LinearScanAllocator16GetFPRegisterSetENS0_21MachineRepresentationEPiS4_PPKi, @function
_ZNK2v88internal8compiler19LinearScanAllocator16GetFPRegisterSetENS0_21MachineRepresentationEPiS4_PPKi:
.LFB23599:
	.cfi_startproc
	endbr64
	cmpb	$12, %sil
	je	.L1884
	cmpb	$14, %sil
	jne	.L1881
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	movl	20(%rax), %eax
	movl	%eax, (%rdx)
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	movl	36(%rax), %eax
	movl	%eax, (%rcx)
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$200, %rax
	movq	%rax, (%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L1884:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	movl	12(%rax), %eax
	movl	%eax, (%rdx)
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	movl	28(%rax), %eax
	movl	%eax, (%rcx)
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$64, %rax
	movq	%rax, (%r8)
	ret
.L1881:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23599:
	.size	_ZNK2v88internal8compiler19LinearScanAllocator16GetFPRegisterSetENS0_21MachineRepresentationEPiS4_PPKi, .-_ZNK2v88internal8compiler19LinearScanAllocator16GetFPRegisterSetENS0_21MachineRepresentationEPiS4_PPKi
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator25FindFreeRegistersForRangeEPNS1_9LiveRangeENS0_6VectorINS1_16LifetimePositionEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC58:
	.string	"Register %s is free until pos %d (1) due to %d\n"
	.align 8
.LC59:
	.string	"Register %s is free until pos %d (2)\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator25FindFreeRegistersForRangeEPNS1_9LiveRangeENS0_6VectorINS1_16LifetimePositionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator25FindFreeRegistersForRangeEPNS1_9LiveRangeENS0_6VectorINS1_16LifetimePositionEEE
	.type	_ZN2v88internal8compiler19LinearScanAllocator25FindFreeRegistersForRangeEPNS1_9LiveRangeENS0_6VectorINS1_16LifetimePositionEEE, @function
_ZN2v88internal8compiler19LinearScanAllocator25FindFreeRegistersForRangeEPNS1_9LiveRangeENS0_6VectorINS1_16LifetimePositionEEE:
.LFB23600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	12(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L1886
	leal	-1(%rcx), %eax
	cmpl	$2, %eax
	jbe	.L1903
	movq	%rdx, %rax
	movl	%ecx, %edx
	movdqa	.LC57(%rip), %xmm0
	shrl	$2, %edx
	salq	$4, %rdx
	addq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L1888:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1888
	movl	%ecx, %eax
	andl	$-4, %eax
	testb	$3, %cl
	je	.L1886
.L1887:
	movslq	%eax, %rdx
	movl	$2147483647, 0(%r13,%rdx,4)
	leal	1(%rax), %edx
	cmpl	%edx, %ecx
	jle	.L1886
	movslq	%edx, %rdx
	addl	$2, %eax
	movl	$2147483647, 0(%r13,%rdx,4)
	cmpl	%eax, %ecx
	jle	.L1886
	cltq
	movl	$2147483647, 0(%r13,%rax,4)
.L1886:
	movq	104(%r15), %r14
	movq	112(%r15), %rbx
	cmpq	%rbx, %r14
	jne	.L1894
	jmp	.L1890
	.p2align 4,,10
	.p2align 3
.L1922:
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rdi
	movq	(%rdi,%rax,8), %r11
.L1892:
	xorl	%edx, %edx
	movq	%r11, %rsi
	leaq	.LC58(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L1891:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1890
.L1894:
	movq	(%r14), %rdx
	movl	4(%rdx), %eax
	shrl	$7, %eax
	movl	%eax, %esi
	andl	$63, %eax
	movl	$0, 0(%r13,%rax,4)
	movq	(%r15), %rcx
	andl	$63, %esi
	testb	$4, 488(%rcx)
	je	.L1891
	movq	32(%rdx), %rdx
	leaq	.LC26(%rip), %r11
	movl	88(%rdx), %ecx
	cmpl	$32, %esi
	je	.L1892
	movl	8(%r15), %edx
	testl	%edx, %edx
	je	.L1922
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rdi
	movq	(%rdi,%rax,8), %r11
	jmp	.L1892
	.p2align 4,,10
	.p2align 3
.L1890:
	movq	136(%r15), %rcx
	movq	144(%r15), %rbx
	cmpq	%rcx, %rbx
	je	.L1885
	movq	%rcx, %r14
	jmp	.L1900
	.p2align 4,,10
	.p2align 3
.L1923:
	movq	(%r15), %rax
	testb	$4, 488(%rax)
	je	.L1901
.L1902:
	cmpl	$32, -68(%rbp)
	leaq	.LC26(%rip), %r11
	je	.L1898
	movl	8(%r15), %eax
	testl	%eax, %eax
	jne	.L1899
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rax
	movq	(%rax,%rdx,8), %r11
.L1898:
	movl	%r8d, %edx
	movq	%r11, %rsi
	leaq	.LC59(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1901:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1885
.L1900:
	movq	(%r14), %rdi
	movl	4(%rdi), %eax
	shrl	$7, %eax
	movl	%eax, %edx
	movl	%eax, %ecx
	movq	16(%r12), %rax
	andl	$63, %edx
	andl	$63, %ecx
	leaq	0(%r13,%rdx,4), %r11
	movl	(%rax), %eax
	movl	%ecx, -68(%rbp)
	cmpl	%eax, (%r11)
	movq	%rdx, -64(%rbp)
	movq	%r11, -56(%rbp)
	jl	.L1901
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler9LiveRange17FirstIntersectionEPS2_
	cmpl	$-1, %eax
	je	.L1901
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %rdx
	movl	(%r11), %r8d
	cmpl	%eax, %r8d
	jl	.L1923
	movl	%eax, (%r11)
	movq	(%r15), %rsi
	testb	$4, 488(%rsi)
	je	.L1901
	movl	%eax, %r8d
	jmp	.L1902
	.p2align 4,,10
	.p2align 3
.L1885:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1899:
	.cfi_restore_state
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rax
	movq	(%rax,%rdx,8), %r11
	jmp	.L1898
.L1903:
	xorl	%eax, %eax
	jmp	.L1887
	.cfi_endproc
.LFE23600:
	.size	_ZN2v88internal8compiler19LinearScanAllocator25FindFreeRegistersForRangeEPNS1_9LiveRangeENS0_6VectorINS1_16LifetimePositionEEE, .-_ZN2v88internal8compiler19LinearScanAllocator25FindFreeRegistersForRangeEPNS1_9LiveRangeENS0_6VectorINS1_16LifetimePositionEEE
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator23TryAllocatePreferredRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC60:
	.string	"Found reg hint %s (free until [%d) for live range %d:%d (end %d[).\n"
	.align 8
.LC61:
	.string	"Assigning preferred reg %s to live range %d:%d\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator23TryAllocatePreferredRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator23TryAllocatePreferredRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE
	.type	_ZN2v88internal8compiler19LinearScanAllocator23TryAllocatePreferredRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE, @function
_ZN2v88internal8compiler19LinearScanAllocator23TryAllocatePreferredRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE:
.LFB23602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movl	4(%rsi), %r14d
	shrl	$22, %r14d
	andl	$63, %r14d
	cmpb	$32, %r14b
	jne	.L1925
	movq	24(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L1926
	leaq	.L1929(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L1935:
	movq	8(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L1927
	movl	28(%rdx), %eax
	shrl	$2, %eax
	andl	$7, %eax
	cmpb	$4, %al
	ja	.L1928
	movzbl	%al, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator23TryAllocatePreferredRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE,"a",@progbits
	.align 4
	.align 4
.L1929:
	.long	.L1927-.L1929
	.long	.L1932-.L1929
	.long	.L1931-.L1929
	.long	.L1930-.L1929
	.long	.L1927-.L1929
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator23TryAllocatePreferredRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE
	.p2align 4,,10
	.p2align 3
.L1925:
	movzbl	%r14b, %r14d
.L1934:
	movq	(%rbx), %rax
	movslq	%r14d, %r15
	leaq	0(,%r15,4), %r10
	movl	(%rax,%r15,4), %edx
	movq	8(%r12), %rax
	movl	4(%rax), %r9d
	movq	0(%r13), %rax
	testb	$4, 488(%rax)
	je	.L1937
	movq	32(%r12), %rax
	movl	(%r12), %r8d
	movl	88(%rax), %ecx
	cmpl	$32, %r14d
	jne	.L1945
	leaq	.LC26(%rip), %rsi
	jmp	.L1938
	.p2align 4,,10
	.p2align 3
.L1930:
	movl	48(%rcx), %r14d
	cmpl	$32, %r14d
	jne	.L1933
	.p2align 4,,10
	.p2align 3
.L1927:
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L1935
.L1926:
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L1940
	movl	116(%rax), %r14d
	cmpl	$32, %r14d
	jne	.L1933
.L1940:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1931:
	.cfi_restore_state
	movl	28(%rcx), %r14d
	shrl	$6, %r14d
	andl	$63, %r14d
	cmpl	$32, %r14d
	je	.L1927
.L1933:
	movq	(%rbx), %rax
	movslq	%r14d, %r15
	leaq	0(,%r15,4), %r10
	movl	(%rax,%r15,4), %edx
	movq	8(%r12), %rax
	movl	4(%rax), %r9d
	movq	0(%r13), %rax
	testb	$4, 488(%rax)
	jne	.L1984
.L1937:
	cmpl	%r9d, %edx
	jl	.L1940
.L1941:
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator28SetLiveRangeAssignedRegisterEPNS1_9LiveRangeEi
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1932:
	.cfi_restore_state
	movq	(%rcx), %r14
	sarq	$35, %r14
	jmp	.L1934
	.p2align 4,,10
	.p2align 3
.L1984:
	movq	32(%r12), %rax
	movl	(%r12), %r8d
	movl	88(%rax), %ecx
.L1945:
	movl	8(%r13), %edi
	leaq	.LC54(%rip), %rsi
	testl	%edi, %edi
	je	.L1985
	cmpl	$-1, %r14d
	je	.L1938
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rax
	movq	(%rax,%r15,8), %rsi
.L1938:
	xorl	%eax, %eax
	leaq	.LC60(%rip), %rdi
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	8(%r12), %rax
	movq	(%rbx), %rdx
	movq	-56(%rbp), %r10
	movl	4(%rax), %eax
	cmpl	%eax, (%rdx,%r10)
	jl	.L1940
	movq	0(%r13), %rax
	testb	$4, 488(%rax)
	je	.L1941
	movq	32(%r12), %rax
	movl	(%r12), %ecx
	leaq	.LC26(%rip), %rsi
	movl	88(%rax), %edx
	cmpl	$32, %r14d
	je	.L1942
	movl	8(%r13), %eax
	leaq	.LC54(%rip), %rsi
	testl	%eax, %eax
	jne	.L1943
	cmpl	$-1, %r14d
	je	.L1942
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rax
	movq	(%rax,%r15,8), %rsi
.L1942:
	leaq	.LC61(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L1985:
	cmpl	$-1, %r14d
	je	.L1938
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rax
	movq	(%rax,%r15,8), %rsi
	jmp	.L1938
	.p2align 4,,10
	.p2align 3
.L1943:
	cmpl	$-1, %r14d
	je	.L1942
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rax
	movq	(%rax,%r15,8), %rsi
	jmp	.L1942
.L1928:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23602:
	.size	_ZN2v88internal8compiler19LinearScanAllocator23TryAllocatePreferredRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE, .-_ZN2v88internal8compiler19LinearScanAllocator23TryAllocatePreferredRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator34PickRegisterThatIsAvailableLongestEPNS1_9LiveRangeEiRKNS0_6VectorINS1_16LifetimePositionEEE.str1.1,"aMS",@progbits,1
.LC62:
	.string	"Register %s in free until %d\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator34PickRegisterThatIsAvailableLongestEPNS1_9LiveRangeEiRKNS0_6VectorINS1_16LifetimePositionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator34PickRegisterThatIsAvailableLongestEPNS1_9LiveRangeEiRKNS0_6VectorINS1_16LifetimePositionEEE
	.type	_ZN2v88internal8compiler19LinearScanAllocator34PickRegisterThatIsAvailableLongestEPNS1_9LiveRangeEiRKNS0_6VectorINS1_16LifetimePositionEEE, @function
_ZN2v88internal8compiler19LinearScanAllocator34PickRegisterThatIsAvailableLongestEPNS1_9LiveRangeEiRKNS0_6VectorINS1_16LifetimePositionEEE:
.LFB23603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	16(%rdi), %eax
	movq	24(%rdi), %r12
	movq	%rsi, -80(%rbp)
	movl	%edx, -68(%rbp)
	cmpl	$32, %edx
	jne	.L1987
	movl	(%r12), %r15d
.L1987:
	testl	%eax, %eax
	jle	.L1986
	subl	$1, %eax
	movl	%r15d, -52(%rbp)
	movl	$-1, %r14d
	movq	%r8, %r13
	leaq	4(%r12,%rax,4), %rax
	movq	%rcx, %r15
	movq	%rax, -64(%rbp)
	jmp	.L1993
	.p2align 4,,10
	.p2align 3
.L2019:
	jne	.L1992
	movl	-52(%rbp), %ecx
	cmpl	%ecx, -68(%rbp)
	jne	.L2017
.L1992:
	addq	$4, %r12
	cmpq	%r12, -64(%rbp)
	je	.L2018
.L1993:
	movslq	(%r12), %rdx
	movq	(%r15), %rax
	movl	(%rax,%rdx,4), %eax
	movq	%rdx, %r9
	testl	%eax, %eax
	leal	3(%rax), %ebx
	cmovns	%eax, %ebx
	movq	0(%r13), %rax
	sarl	$2, %ebx
	testb	$4, 488(%rax)
	je	.L1989
	leaq	.LC26(%rip), %rsi
	cmpl	$32, %edx
	je	.L1990
	movl	8(%r13), %eax
	leaq	.LC54(%rip), %rsi
	testl	%eax, %eax
	jne	.L1991
	cmpl	$-1, %edx
	je	.L1990
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rax
	movq	(%rax,%rdx,8), %rsi
.L1990:
	movl	%ebx, %edx
	leaq	.LC62(%rip), %rdi
	xorl	%eax, %eax
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	-56(%rbp), %r9d
.L1989:
	cmpl	%r14d, %ebx
	jle	.L2019
	movl	%r9d, -52(%rbp)
	movl	%ebx, %r14d
	addq	$4, %r12
	cmpq	%r12, -64(%rbp)
	jne	.L1993
.L2018:
	movl	-52(%rbp), %r15d
.L1986:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1991:
	.cfi_restore_state
	cmpl	$-1, %edx
	je	.L1990
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rax
	movq	(%rax,%rdx,8), %rsi
	jmp	.L1990
	.p2align 4,,10
	.p2align 3
.L2017:
	movq	-80(%rbp), %rax
	movq	0(%r13), %rdi
	movl	%ecx, %edx
	movl	%r9d, -72(%rbp)
	movl	%ecx, %r10d
	movl	4(%rax), %eax
	movl	%eax, %r11d
	movl	%eax, -56(%rbp)
	shrl	$13, %r11d
	movl	%r11d, %esi
	call	_ZN2v88internal8compiler22RegisterAllocationData11HasFixedUseENS0_21MachineRepresentationEi
	testb	%al, %al
	je	.L1992
	movl	-72(%rbp), %r9d
	movl	%r11d, %esi
	movl	%r9d, %edx
	movl	%r9d, -52(%rbp)
	call	_ZN2v88internal8compiler22RegisterAllocationData11HasFixedUseENS0_21MachineRepresentationEi
	movl	-52(%rbp), %r9d
	testb	%al, %al
	cmovne	%r10d, %r9d
	cmove	%ebx, %r14d
	movl	%r9d, -52(%rbp)
	jmp	.L1992
	.cfi_endproc
.LFE23603:
	.size	_ZN2v88internal8compiler19LinearScanAllocator34PickRegisterThatIsAvailableLongestEPNS1_9LiveRangeEiRKNS0_6VectorINS1_16LifetimePositionEEE, .-_ZN2v88internal8compiler19LinearScanAllocator34PickRegisterThatIsAvailableLongestEPNS1_9LiveRangeEiRKNS0_6VectorINS1_16LifetimePositionEEE
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator10SpillAfterEPNS1_9LiveRangeENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator10SpillAfterEPNS1_9LiveRangeENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeE
	.type	_ZN2v88internal8compiler19LinearScanAllocator10SpillAfterEPNS1_9LiveRangeENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeE, @function
_ZN2v88internal8compiler19LinearScanAllocator10SpillAfterEPNS1_9LiveRangeENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeE:
.LFB23608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	(%rdi), %rax
	movq	%rsi, %r12
	testb	$4, 488(%rax)
	jne	.L2027
.L2021:
	movq	16(%r12), %rax
	cmpl	(%rax), %r13d
	jle	.L2022
	movq	(%r15), %rax
	movq	%r12, %rdi
	movl	%r13d, %esi
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler9LiveRange7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE
	movq	%rax, %r12
.L2022:
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	.p2align 4,,10
	.p2align 3
.L2027:
	.cfi_restore_state
	movq	32(%rsi), %rax
	movl	%edx, %ecx
	movl	(%r12), %edx
	leaq	.LC45(%rip), %rdi
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2021
	.cfi_endproc
.LFE23608:
	.size	_ZN2v88internal8compiler19LinearScanAllocator10SpillAfterEPNS1_9LiveRangeENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeE, .-_ZN2v88internal8compiler19LinearScanAllocator10SpillAfterEPNS1_9LiveRangeENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeE
	.section	.text._ZN2v88internal8compiler16SpillSlotLocatorC2EPNS1_22RegisterAllocationDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16SpillSlotLocatorC2EPNS1_22RegisterAllocationDataE
	.type	_ZN2v88internal8compiler16SpillSlotLocatorC2EPNS1_22RegisterAllocationDataE, @function
_ZN2v88internal8compiler16SpillSlotLocatorC2EPNS1_22RegisterAllocationDataE:
.LFB23612:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE23612:
	.size	_ZN2v88internal8compiler16SpillSlotLocatorC2EPNS1_22RegisterAllocationDataE, .-_ZN2v88internal8compiler16SpillSlotLocatorC2EPNS1_22RegisterAllocationDataE
	.globl	_ZN2v88internal8compiler16SpillSlotLocatorC1EPNS1_22RegisterAllocationDataE
	.set	_ZN2v88internal8compiler16SpillSlotLocatorC1EPNS1_22RegisterAllocationDataE,_ZN2v88internal8compiler16SpillSlotLocatorC2EPNS1_22RegisterAllocationDataE
	.section	.rodata._ZN2v88internal8compiler16SpillSlotLocator16LocateSpillSlotsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC63:
	.string	"live_ranges_size == data()->live_ranges().size()"
	.section	.text._ZN2v88internal8compiler16SpillSlotLocator16LocateSpillSlotsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16SpillSlotLocator16LocateSpillSlotsEv
	.type	_ZN2v88internal8compiler16SpillSlotLocator16LocateSpillSlotsEv, @function
_ZN2v88internal8compiler16SpillSlotLocator16LocateSpillSlotsEv:
.LFB23614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	176(%rdx), %r14
	movq	168(%rdx), %rbx
	movq	16(%rdx), %r12
	movq	%r14, %rax
	subq	%rbx, %rax
	movq	%rax, -56(%rbp)
	cmpq	%r14, %rbx
	je	.L2029
	movq	(%rbx), %rax
	movq	%rdi, %r13
	addq	$8, %rbx
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2052:
	andl	$3, %esi
	cmpl	$3, %esi
	sete	%dl
	testb	%dl, %dl
	je	.L2050
	.p2align 4,,10
	.p2align 3
.L2035:
	cmpq	%rbx, %r14
	je	.L2029
.L2033:
	movq	0(%r13), %rdx
	movq	(%rbx), %rax
	addq	$8, %rbx
	movq	176(%rdx), %rcx
	subq	168(%rdx), %rcx
	cmpq	%rcx, -56(%rbp)
	jne	.L2051
.L2031:
	testq	%rax, %rax
	je	.L2035
	cmpq	$0, 16(%rax)
	je	.L2035
	movl	4(%rax), %ecx
	movl	%ecx, %esi
	shrl	$5, %esi
	andl	$64, %ecx
	je	.L2035
	testb	$1, 488(%rdx)
	jne	.L2052
	movzbl	120(%rax), %edx
	testb	%dl, %dl
	jne	.L2035
.L2050:
	movq	112(%rax), %r15
	testq	%r15, %r15
	je	.L2035
	.p2align 4,,10
	.p2align 3
.L2038:
	movl	(%r15), %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movb	$1, 124(%rax)
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.L2038
	cmpq	%rbx, %r14
	jne	.L2033
.L2029:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2051:
	.cfi_restore_state
	leaq	.LC63(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23614:
	.size	_ZN2v88internal8compiler16SpillSlotLocator16LocateSpillSlotsEv, .-_ZN2v88internal8compiler16SpillSlotLocator16LocateSpillSlotsEv
	.section	.text._ZN2v88internal8compiler15OperandAssignerC2EPNS1_22RegisterAllocationDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15OperandAssignerC2EPNS1_22RegisterAllocationDataE
	.type	_ZN2v88internal8compiler15OperandAssignerC2EPNS1_22RegisterAllocationDataE, @function
_ZN2v88internal8compiler15OperandAssignerC2EPNS1_22RegisterAllocationDataE:
.LFB23616:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE23616:
	.size	_ZN2v88internal8compiler15OperandAssignerC2EPNS1_22RegisterAllocationDataE, .-_ZN2v88internal8compiler15OperandAssignerC2EPNS1_22RegisterAllocationDataE
	.globl	_ZN2v88internal8compiler15OperandAssignerC1EPNS1_22RegisterAllocationDataE
	.set	_ZN2v88internal8compiler15OperandAssignerC1EPNS1_22RegisterAllocationDataE,_ZN2v88internal8compiler15OperandAssignerC2EPNS1_22RegisterAllocationDataE
	.section	.rodata._ZN2v88internal8compiler15OperandAssigner18DecideSpillingModeEv.str1.8,"aMS",@progbits,1
	.align 8
.LC64:
	.string	"Live range %d is spilled and alive in deferred code only\n"
	.align 8
.LC65:
	.string	"Live range %d is spilled deferred code only but alive outside\n"
	.section	.text._ZN2v88internal8compiler15OperandAssigner18DecideSpillingModeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15OperandAssigner18DecideSpillingModeEv
	.type	_ZN2v88internal8compiler15OperandAssigner18DecideSpillingModeEv, @function
_ZN2v88internal8compiler15OperandAssigner18DecideSpillingModeEv:
.LFB23618:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testb	$1, 488(%rax)
	je	.L2090
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	168(%rax), %rdx
	movq	176(%rax), %r13
	cmpq	%r13, %rdx
	je	.L2054
	movq	%rdi, %rbx
	movq	%rdx, %r15
	jmp	.L2074
	.p2align 4,,10
	.p2align 3
.L2094:
	movl	4(%r14), %esi
	xorl	$96, %esi
	andl	$96, %esi
	sete	%sil
	testb	%sil, %sil
	jne	.L2093
.L2057:
	addq	$8, %r15
	cmpq	%r15, %r13
	je	.L2054
.L2096:
	movq	(%rbx), %rax
.L2074:
	movq	(%r15), %r14
	movq	496(%rax), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	testq	%r14, %r14
	je	.L2057
	movq	(%rbx), %rax
	testb	$1, 488(%rax)
	jne	.L2094
	movzbl	120(%r14), %esi
	testb	%sil, %sil
	je	.L2057
.L2093:
	movq	16(%rax), %rdi
	movq	16(%rdi), %rax
	movq	8(%rax), %rcx
	movq	16(%rax), %r12
	movq	16(%r14), %rax
	movq	%rcx, -56(%rbp)
	movl	(%rax), %eax
	testl	%eax, %eax
	leal	3(%rax), %esi
	cmovns	%eax, %esi
	sarl	$2, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	cmpb	$0, 120(%rax)
	movq	(%rbx), %rax
	je	.L2061
	testb	$4, 488(%rax)
	jne	.L2095
.L2062:
	movl	4(%r14), %eax
	movl	%eax, %esi
	xorl	$96, %esi
	andl	$96, %esi
	jne	.L2057
	andl	$-97, %eax
	addq	$8, %r15
	orl	$64, %eax
	movl	%eax, 4(%r14)
	cmpq	%r15, %r13
	jne	.L2096
.L2054:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2090:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L2061:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	testb	$4, 488(%rax)
	jne	.L2097
.L2063:
	movq	(%rax), %rdi
	movl	$-1, 124(%r14)
	movq	$0, 112(%r14)
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	$15, %rax
	jbe	.L2098
	leaq	16(%r8), %rax
	movq	%rax, 16(%rdi)
.L2065:
	subq	-56(%rbp), %r12
	sarq	$3, %r12
	movl	%r12d, (%r8)
	cmpl	$64, %r12d
	jle	.L2099
	subl	$1, %r12d
	movq	$0, 8(%r8)
	sarl	$6, %r12d
	leal	1(%r12), %esi
	movl	%esi, 4(%r8)
	movq	16(%rdi), %rax
	movslq	%esi, %rsi
	movq	24(%rdi), %r9
	salq	$3, %rsi
	subq	%rax, %r9
	cmpq	%r9, %rsi
	ja	.L2100
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2069:
	movl	4(%r8), %esi
	movq	%rax, 8(%r8)
	cmpl	$1, %esi
	je	.L2101
	testl	%esi, %esi
	jle	.L2067
	movq	$0, (%rax)
	cmpl	$1, 4(%r8)
	movl	$8, %esi
	movl	$1, %eax
	jle	.L2067
	.p2align 4,,10
	.p2align 3
.L2072:
	movq	8(%r8), %rdi
	addl	$1, %eax
	movq	$0, (%rdi,%rsi)
	addq	$8, %rsi
	cmpl	%eax, 4(%r8)
	jg	.L2072
.L2067:
	movq	%r8, 112(%r14)
	jmp	.L2057
	.p2align 4,,10
	.p2align 3
.L2101:
	movq	$0, 8(%r8)
	jmp	.L2067
	.p2align 4,,10
	.p2align 3
.L2099:
	movl	$1, 4(%r8)
	movq	$0, 8(%r8)
	movq	%r8, 112(%r14)
	jmp	.L2057
	.p2align 4,,10
	.p2align 3
.L2095:
	movl	88(%r14), %esi
	leaq	.LC64(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2097:
	movl	88(%r14), %esi
	xorl	%eax, %eax
	leaq	.LC65(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%rbx), %rax
	jmp	.L2063
	.p2align 4,,10
	.p2align 3
.L2098:
	movl	$16, %esi
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rdi
	movq	%rax, %r8
	jmp	.L2065
.L2100:
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	jmp	.L2069
	.cfi_endproc
.LFE23618:
	.size	_ZN2v88internal8compiler15OperandAssigner18DecideSpillingModeEv, .-_ZN2v88internal8compiler15OperandAssigner18DecideSpillingModeEv
	.section	.text._ZN2v88internal8compiler21ReferenceMapPopulatorC2EPNS1_22RegisterAllocationDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21ReferenceMapPopulatorC2EPNS1_22RegisterAllocationDataE
	.type	_ZN2v88internal8compiler21ReferenceMapPopulatorC2EPNS1_22RegisterAllocationDataE, @function
_ZN2v88internal8compiler21ReferenceMapPopulatorC2EPNS1_22RegisterAllocationDataE:
.LFB23622:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE23622:
	.size	_ZN2v88internal8compiler21ReferenceMapPopulatorC2EPNS1_22RegisterAllocationDataE, .-_ZN2v88internal8compiler21ReferenceMapPopulatorC2EPNS1_22RegisterAllocationDataE
	.globl	_ZN2v88internal8compiler21ReferenceMapPopulatorC1EPNS1_22RegisterAllocationDataE
	.set	_ZN2v88internal8compiler21ReferenceMapPopulatorC1EPNS1_22RegisterAllocationDataE,_ZN2v88internal8compiler21ReferenceMapPopulatorC2EPNS1_22RegisterAllocationDataE
	.section	.text._ZNK2v88internal8compiler21ReferenceMapPopulator20SafePointsAreInOrderEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler21ReferenceMapPopulator20SafePointsAreInOrderEv
	.type	_ZNK2v88internal8compiler21ReferenceMapPopulator20SafePointsAreInOrderEv, @function
_ZNK2v88internal8compiler21ReferenceMapPopulator20SafePointsAreInOrderEv:
.LFB23624:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	xorl	%ecx, %ecx
	movq	16(%rax), %rdx
	movq	312(%rdx), %rax
	movq	344(%rdx), %rdi
	movq	328(%rdx), %rsi
	movq	336(%rdx), %r8
	cmpq	%rdi, %rax
	je	.L2105
	movq	(%rax), %rdx
	movl	32(%rdx), %edx
	cmpl	%ecx, %edx
	jl	.L2113
	.p2align 4,,10
	.p2align 3
.L2106:
	addq	$8, %rax
	cmpq	%rsi, %rax
	je	.L2114
	cmpq	%rax, %rdi
	je	.L2105
	movl	%edx, %ecx
.L2115:
	movq	(%rax), %rdx
	movl	32(%rdx), %edx
	cmpl	%ecx, %edx
	jge	.L2106
.L2113:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2105:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2114:
	movq	8(%r8), %rax
	leaq	8(%r8), %rcx
	leaq	512(%rax), %rsi
	cmpq	%rax, %rdi
	je	.L2105
	movq	%rcx, %r8
	movl	%edx, %ecx
	jmp	.L2115
	.cfi_endproc
.LFE23624:
	.size	_ZNK2v88internal8compiler21ReferenceMapPopulator20SafePointsAreInOrderEv, .-_ZNK2v88internal8compiler21ReferenceMapPopulator20SafePointsAreInOrderEv
	.section	.rodata._ZN2v88internal8compiler21ReferenceMapPopulator21PopulateReferenceMapsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC66:
	.string	"Pointer for range %d (spilled at %d) at safe point %d\n"
	.align 8
.LC67:
	.string	"Pointer in register for range %d:%d (start at %d) at safe point %d\n"
	.section	.text._ZN2v88internal8compiler21ReferenceMapPopulator21PopulateReferenceMapsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21ReferenceMapPopulator21PopulateReferenceMapsEv
	.type	_ZN2v88internal8compiler21ReferenceMapPopulator21PopulateReferenceMapsEv, @function
_ZN2v88internal8compiler21ReferenceMapPopulator21PopulateReferenceMapsEv:
.LFB23625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rax), %rax
	movq	%rdi, -80(%rbp)
	movq	360(%rax), %rbx
	movq	368(%rax), %r12
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	cmpq	%r12, %rbx
	je	.L2117
	leaq	-64(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L2118:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	addq	$16, %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler12ReferenceMap15RecordReferenceERKNS1_16AllocatedOperandE@PLT
	cmpq	%rbx, %r12
	jne	.L2118
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
.L2117:
	movq	16(%rax), %rdi
	movq	168(%rax), %rdx
	movq	328(%rdi), %rsi
	movq	%rdi, -96(%rbp)
	movq	312(%rdi), %r14
	movq	336(%rdi), %rdi
	movq	%rsi, -144(%rbp)
	movq	%rdi, -152(%rbp)
	movq	176(%rax), %rdi
	movq	%rdi, %rsi
	movq	%rdi, -104(%rbp)
	subq	%rdx, %rsi
	movq	%rsi, -112(%rbp)
	cmpq	%rdi, %rdx
	je	.L2116
	leaq	8(%rdx), %rdi
	movq	%r14, -136(%rbp)
	movq	(%rdx), %r13
	xorl	%ebx, %ebx
	movq	%rdi, -72(%rbp)
	jmp	.L2120
	.p2align 4,,10
	.p2align 3
.L2204:
	subl	$10, %eax
	cmpb	$1, %al
	jbe	.L2122
.L2121:
	movq	-72(%rbp), %rdi
	cmpq	%rdi, -104(%rbp)
	je	.L2116
.L2154:
	movq	-80(%rbp), %rax
	movq	(%rdi), %r13
	addq	$8, %rdi
	movq	%rdi, -72(%rbp)
	movq	(%rax), %rax
	movq	176(%rax), %rdx
	subq	168(%rax), %rdx
	cmpq	-112(%rbp), %rdx
	jne	.L2203
.L2120:
	testq	%r13, %r13
	je	.L2121
	movl	88(%r13), %esi
	movq	16(%rax), %rdi
	call	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	leal	-7(%rax), %edx
	cmpb	$1, %dl
	ja	.L2204
.L2122:
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.L2121
	cmpb	$0, 152(%r13)
	jne	.L2121
	movl	(%rax), %edx
	testl	%edx, %edx
	leal	3(%rdx), %eax
	cmovns	%edx, %eax
	movq	%r13, %rdx
	xorl	%r15d, %r15d
	sarl	$2, %eax
	movl	%eax, %r9d
	.p2align 4,,10
	.p2align 3
.L2123:
	movq	8(%rdx), %rax
	movq	40(%rdx), %rdx
	movl	4(%rax), %ecx
	testl	%ecx, %ecx
	leal	3(%rcx), %eax
	cmovns	%ecx, %eax
	sarl	$2, %eax
	cmpl	%eax, %r15d
	cmovl	%eax, %r15d
	testq	%rdx, %rdx
	jne	.L2123
	cmpl	%ebx, %r9d
	jl	.L2205
.L2124:
	movq	-96(%rbp), %rax
	movq	-136(%rbp), %r14
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rsi
	movq	344(%rax), %rdx
.L2127:
	movq	%r14, %rax
.L2126:
	movq	%rax, %r14
	cmpq	%rdx, %rax
	je	.L2159
	movq	(%rax), %rax
	cmpl	32(%rax), %r9d
	jle	.L2198
	leaq	8(%r14), %rax
	cmpq	%rax, %rcx
	jne	.L2126
	movq	8(%rsi), %r14
	addq	$8, %rsi
	leaq	512(%r14), %rcx
	jmp	.L2127
.L2203:
	leaq	.LC63(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2205:
	movq	-96(%rbp), %rax
	movq	312(%rax), %rdi
	movq	%rdi, -136(%rbp)
	movq	328(%rax), %rdi
	movq	336(%rax), %rax
	movq	%rdi, -144(%rbp)
	movq	%rax, -152(%rbp)
	jmp	.L2124
.L2159:
	movq	%rcx, -144(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%rdx, -136(%rbp)
.L2125:
	movl	4(%r13), %eax
	movl	%eax, %ecx
	shrl	$5, %ecx
	movl	%ecx, %esi
	andl	$3, %esi
	cmpl	$1, %esi
	je	.L2206
	movq	$0, -160(%rbp)
	andl	$2, %ecx
	je	.L2129
	movq	104(%r13), %rcx
	shrl	$13, %eax
	movzbl	%al, %eax
	movslq	44(%rcx), %rcx
	salq	$5, %rax
	salq	$35, %rcx
	orq	%rcx, %rax
	orq	$13, %rax
	movq	%rax, -160(%rbp)
.L2129:
	cmpq	-136(%rbp), %rdx
	je	.L2131
	movq	-152(%rbp), %rax
	movl	%r9d, -164(%rbp)
	movq	%r13, %rbx
	movq	%r13, %r14
	movq	-136(%rbp), %r12
	movq	%rax, -128(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -88(%rbp)
	movl	-160(%rbp), %eax
	andl	$7, %eax
	movl	%eax, -116(%rbp)
	.p2align 4,,10
	.p2align 3
.L2130:
	movq	(%r12), %r13
	movl	32(%r13), %r8d
	leal	-1(%r8), %eax
	cmpl	%r15d, %eax
	jg	.L2199
	leal	2(,%r8,4), %eax
	.p2align 4,,10
	.p2align 3
.L2141:
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2132
	movl	(%rsi), %edx
	cmpl	%edx, %eax
	jl	.L2132
	movq	8(%rbx), %rcx
	cmpl	4(%rcx), %eax
	jge	.L2132
	movq	48(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L2163
	movl	(%rcx), %edx
	cmpl	%edx, %eax
	jl	.L2207
.L2196:
	movq	48(%rbx), %r9
	movl	$-1, %edi
	testq	%r9, %r9
	je	.L2134
	movl	(%r9), %edi
.L2134:
	cmpl	%edx, %edi
	jge	.L2135
	movq	%rcx, 48(%rbx)
	cmpl	(%rcx), %eax
	jl	.L2132
.L2135:
	cmpl	4(%rcx), %eax
	jge	.L2208
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movl	488(%rax), %eax
	testb	$1, %al
	je	.L2209
	movl	4(%r14), %edx
	xorl	$96, %edx
	andl	$96, %edx
	sete	%dl
.L2142:
	testb	%dl, %dl
	je	.L2143
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	leal	3(%rcx), %edx
	cmovns	%ecx, %edx
	sarl	$2, %edx
.L2144:
	movl	-116(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L2145
	cmpl	%r8d, %edx
	jle	.L2210
.L2145:
	movl	4(%rbx), %eax
	testb	$1, %al
	jne	.L2139
	movq	-80(%rbp), %rdi
	movq	(%rdi), %rdx
	testb	$4, 488(%rdx)
	jne	.L2211
.L2147:
	movl	%eax, %edx
	shrl	$7, %edx
	andl	$63, %edx
	cmpl	$32, %edx
	je	.L2148
	shrl	$13, %eax
	salq	$35, %rdx
	movzbl	%al, %eax
	salq	$5, %rax
	orq	%rdx, %rax
	orq	$5, %rax
.L2149:
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler12ReferenceMap15RecordReferenceERKNS1_16AllocatedOperandE@PLT
.L2139:
	addq	$8, %r12
	cmpq	-88(%rbp), %r12
	je	.L2212
	movq	-96(%rbp), %rax
	cmpq	%r12, 344(%rax)
	jne	.L2130
.L2199:
	movl	-164(%rbp), %r9d
.L2131:
	movq	-72(%rbp), %rdi
	movl	%r9d, %ebx
	cmpq	%rdi, -104(%rbp)
	jne	.L2154
.L2116:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2213
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2208:
	.cfi_restore_state
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2132
	movl	(%rcx), %edx
.L2137:
	cmpl	%edx, %eax
	jge	.L2196
	.p2align 4,,10
	.p2align 3
.L2132:
	movq	40(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L2139
	movq	16(%rdx), %rcx
	cmpl	(%rcx), %eax
	jl	.L2139
	movq	%rdx, %rbx
	jmp	.L2141
	.p2align 4,,10
	.p2align 3
.L2207:
	movq	$0, 48(%rbx)
	movq	%rsi, %rcx
	movl	(%rsi), %edx
	jmp	.L2137
	.p2align 4,,10
	.p2align 3
.L2163:
	movq	%rsi, %rcx
	jmp	.L2137
	.p2align 4,,10
	.p2align 3
.L2143:
	movl	124(%r14), %edx
	jmp	.L2144
	.p2align 4,,10
	.p2align 3
.L2209:
	movzbl	120(%r14), %edx
	jmp	.L2142
	.p2align 4,,10
	.p2align 3
.L2212:
	movq	-128(%rbp), %rdi
	movq	8(%rdi), %r12
	leaq	8(%rdi), %rax
	leaq	512(%r12), %rdi
	movq	%rdi, -88(%rbp)
	movq	-96(%rbp), %rdi
	cmpq	%r12, 344(%rdi)
	je	.L2199
	movq	%rax, -128(%rbp)
	jmp	.L2130
	.p2align 4,,10
	.p2align 3
.L2210:
	testb	$4, %al
	jne	.L2214
.L2146:
	movq	-160(%rbp), %rax
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdi
	movl	%r8d, -120(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler12ReferenceMap15RecordReferenceERKNS1_16AllocatedOperandE@PLT
	movl	-120(%rbp), %r8d
	jmp	.L2145
	.p2align 4,,10
	.p2align 3
.L2148:
	movq	32(%rbx), %rdx
	movl	4(%rdx), %eax
	movq	104(%rdx), %rcx
	movl	%eax, %edx
	shrl	$5, %edx
	andl	$3, %edx
	cmpl	$1, %edx
	je	.L2215
	movslq	44(%rcx), %rdx
	shrl	$13, %eax
	movzbl	%al, %eax
	salq	$5, %rax
	salq	$35, %rdx
	orq	%rdx, %rax
	orq	$13, %rax
	jmp	.L2149
	.p2align 4,,10
	.p2align 3
.L2211:
	movq	16(%rbx), %rax
	movl	(%rbx), %edx
	leaq	.LC67(%rip), %rdi
	movl	88(%r14), %esi
	movl	(%rax), %ecx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	4(%rbx), %eax
	jmp	.L2147
.L2198:
	movq	%r14, -136(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rsi, -152(%rbp)
	jmp	.L2125
.L2214:
	movl	88(%r14), %esi
	movl	%r8d, %ecx
	leaq	.LC66(%rip), %rdi
	xorl	%eax, %eax
	movl	%r8d, -120(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	-120(%rbp), %r8d
	jmp	.L2146
.L2206:
	movq	104(%r13), %rax
	movq	(%rax), %rdi
	movl	%edi, %eax
	andl	$7, %eax
	cmpl	$2, %eax
	movl	$0, %eax
	cmovne	%rdi, %rax
	movq	%rax, -160(%rbp)
	jmp	.L2129
.L2215:
	movq	(%rcx), %rax
	jmp	.L2149
.L2213:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23625:
	.size	_ZN2v88internal8compiler21ReferenceMapPopulator21PopulateReferenceMapsEv, .-_ZN2v88internal8compiler21ReferenceMapPopulator21PopulateReferenceMapsEv
	.section	.text._ZN2v88internal8compiler18LiveRangeConnectorC2EPNS1_22RegisterAllocationDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18LiveRangeConnectorC2EPNS1_22RegisterAllocationDataE
	.type	_ZN2v88internal8compiler18LiveRangeConnectorC2EPNS1_22RegisterAllocationDataE, @function
_ZN2v88internal8compiler18LiveRangeConnectorC2EPNS1_22RegisterAllocationDataE:
.LFB23627:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE23627:
	.size	_ZN2v88internal8compiler18LiveRangeConnectorC2EPNS1_22RegisterAllocationDataE, .-_ZN2v88internal8compiler18LiveRangeConnectorC2EPNS1_22RegisterAllocationDataE
	.globl	_ZN2v88internal8compiler18LiveRangeConnectorC1EPNS1_22RegisterAllocationDataE
	.set	_ZN2v88internal8compiler18LiveRangeConnectorC1EPNS1_22RegisterAllocationDataE,_ZN2v88internal8compiler18LiveRangeConnectorC2EPNS1_22RegisterAllocationDataE
	.section	.text._ZNK2v88internal8compiler18LiveRangeConnector28CanEagerlyResolveControlFlowEPKNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18LiveRangeConnector28CanEagerlyResolveControlFlowEPKNS1_16InstructionBlockE
	.type	_ZNK2v88internal8compiler18LiveRangeConnector28CanEagerlyResolveControlFlowEPKNS1_16InstructionBlockE, @function
_ZNK2v88internal8compiler18LiveRangeConnector28CanEagerlyResolveControlFlowEPKNS1_16InstructionBlockE:
.LFB23629:
	.cfi_startproc
	endbr64
	movq	40(%rsi), %rdx
	movq	48(%rsi), %rax
	xorl	%r8d, %r8d
	subq	%rdx, %rax
	cmpq	$4, %rax
	jne	.L2217
	movl	(%rdx), %eax
	addl	$1, %eax
	cmpl	100(%rsi), %eax
	sete	%r8b
.L2217:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE23629:
	.size	_ZNK2v88internal8compiler18LiveRangeConnector28CanEagerlyResolveControlFlowEPKNS1_16InstructionBlockE, .-_ZNK2v88internal8compiler18LiveRangeConnector28CanEagerlyResolveControlFlowEPKNS1_16InstructionBlockE
	.section	.text._ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB26632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L2258
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L2236
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L2259
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L2222:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L2260
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2225:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L2223
	.p2align 4,,10
	.p2align 3
.L2259:
	testq	%rdx, %rdx
	jne	.L2261
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L2223:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L2226
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L2239
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L2239
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L2228:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L2228
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L2230
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L2230:
	leaq	16(%rax,%r8), %rcx
.L2226:
	cmpq	%r14, %r12
	je	.L2231
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L2240
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L2240
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L2233:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L2233
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L2235
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L2235:
	leaq	8(%rcx,%r9), %rcx
.L2231:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2236:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2240:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2232:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L2232
	jmp	.L2235
	.p2align 4,,10
	.p2align 3
.L2239:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2227:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L2227
	jmp	.L2230
.L2260:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L2225
.L2258:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2261:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L2222
	.cfi_endproc
.LFE26632:
	.size	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal8compiler17TopLevelLiveRange16CommitSpillMovesEPNS1_22RegisterAllocationDataERKNS1_18InstructionOperandEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17TopLevelLiveRange16CommitSpillMovesEPNS1_22RegisterAllocationDataERKNS1_18InstructionOperandEb
	.type	_ZN2v88internal8compiler17TopLevelLiveRange16CommitSpillMovesEPNS1_22RegisterAllocationDataERKNS1_18InstructionOperandEb, @function
_ZN2v88internal8compiler17TopLevelLiveRange16CommitSpillMovesEPNS1_22RegisterAllocationDataERKNS1_18InstructionOperandEb:
.LFB23301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rsi), %r13
	movq	112(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%r13), %r8
	testq	%rbx, %rbx
	je	.L2262
	movq	%rdi, %r15
	movq	%rdx, %r14
	leaq	-64(%rbp), %r9
	jmp	.L2263
	.p2align 4,,10
	.p2align 3
.L2318:
	cmpq	$63, %rax
	jg	.L2265
	leaq	(%rsi,%rdx,8), %rax
	movq	(%rax), %rdx
	movq	8(%rdx), %r12
	testq	%r12, %r12
	je	.L2315
.L2268:
	movzbl	152(%r15), %r10d
	testb	%cl, %cl
	jne	.L2271
.L2320:
	testb	%r10b, %r10b
	jne	.L2316
.L2273:
	movq	8(%rbx), %r10
	movq	(%r10), %rax
	testb	$4, %al
	je	.L2278
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L2279
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L2279:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L2278:
	movq	(%r14), %rdx
	testb	$4, %dl
	je	.L2280
	xorl	%esi, %esi
	testb	$24, %dl
	jne	.L2281
	movq	%rdx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L2281:
	andq	$-8161, %rdx
	orq	%rsi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L2280:
	cmpq	%rax, %rdx
	je	.L2276
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L2317
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2284:
	movq	(%r10), %rdx
	movq	%rdx, (%rax)
	movq	(%r14), %rdx
	movq	%rax, -64(%rbp)
	movq	%rdx, 8(%rax)
	movq	16(%r12), %rsi
	cmpq	8(%r12), %rsi
	movq	24(%r12), %rax
	je	.L2285
.L2286:
	cmpq	%rax, %rsi
	je	.L2289
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
	.p2align 4,,10
	.p2align 3
.L2276:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2262
.L2263:
	movq	208(%r13), %rsi
	movslq	(%rbx), %rdx
	movq	%rsi, %rax
	subq	216(%r13), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	jns	.L2318
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
.L2267:
	movq	232(%r13), %rsi
	movq	%rdx, %rdi
	salq	$6, %rdi
	movq	(%rsi,%rdx,8), %rdx
	subq	%rdi, %rax
	leaq	(%rdx,%rax,8), %rax
	movq	(%rax), %rdx
	movq	8(%rdx), %r12
	testq	%r12, %r12
	jne	.L2268
.L2315:
	movq	16(%r8), %r12
	movq	24(%r8), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	jbe	.L2319
	leaq	32(%r12), %rax
	movq	%rax, 16(%r8)
.L2270:
	movq	%r8, (%r12)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	%r12, 8(%rdx)
	movzbl	152(%r15), %r10d
	testb	%cl, %cl
	je	.L2320
	.p2align 4,,10
	.p2align 3
.L2271:
	movq	8(%r12), %rax
	movq	16(%r12), %rdi
	cmpq	%rax, %rdi
	jne	.L2277
	jmp	.L2274
	.p2align 4,,10
	.p2align 3
.L2275:
	addq	$8, %rax
	cmpq	%rax, %rdi
	je	.L2274
.L2277:
	movq	(%rax), %rsi
	movq	(%rsi), %rdx
	testb	$7, %dl
	je	.L2275
	movq	8(%rbx), %r11
	cmpq	%rdx, (%r11)
	jne	.L2275
	movq	(%r14), %rdx
	cmpq	%rdx, 8(%rsi)
	jne	.L2275
	testb	%r10b, %r10b
	je	.L2276
	movq	$0, 8(%rsi)
	movq	$0, (%rsi)
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2263
	.p2align 4,,10
	.p2align 3
.L2262:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2321
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2265:
	.cfi_restore_state
	movq	%rax, %rdx
	sarq	$6, %rdx
	jmp	.L2267
	.p2align 4,,10
	.p2align 3
.L2274:
	testb	%r10b, %r10b
	jne	.L2276
	jmp	.L2273
	.p2align 4,,10
	.p2align 3
.L2316:
	movq	8(%r12), %rax
	movq	16(%r12), %rdi
	cmpq	%rdi, %rax
	jne	.L2277
	jmp	.L2276
.L2285:
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	$31, %rdx
	ja	.L2286
	movq	(%r12), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$31, %rax
	jbe	.L2322
	leaq	32(%rsi), %rax
	movq	%rax, 16(%rdi)
.L2288:
	movq	%rsi, 8(%r12)
	movq	%rsi, 16(%r12)
	movq	%rax, 24(%r12)
	jmp	.L2286
.L2289:
	movq	%r9, %rdx
	movq	%r12, %rdi
	movb	%cl, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movzbl	-88(%rbp), %ecx
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %r9
	jmp	.L2276
.L2317:
	movl	$16, %esi
	movq	%r9, -96(%rbp)
	movb	%cl, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %r8
	movzbl	-88(%rbp), %ecx
	movq	-96(%rbp), %r9
	jmp	.L2284
.L2319:
	movq	%r8, %rdi
	movl	$32, %esi
	movq	%r9, -96(%rbp)
	movb	%cl, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rdx
	movzbl	-88(%rbp), %ecx
	movq	-96(%rbp), %r9
	movq	%rax, %r12
	jmp	.L2270
.L2322:
	movl	$32, %esi
	movq	%r9, -88(%rbp)
	movb	%cl, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r8
	movzbl	-80(%rbp), %ecx
	movq	%rax, %rsi
	movq	-88(%rbp), %r9
	leaq	32(%rax), %rax
	jmp	.L2288
.L2321:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23301:
	.size	_ZN2v88internal8compiler17TopLevelLiveRange16CommitSpillMovesEPNS1_22RegisterAllocationDataERKNS1_18InstructionOperandEb, .-_ZN2v88internal8compiler17TopLevelLiveRange16CommitSpillMovesEPNS1_22RegisterAllocationDataERKNS1_18InstructionOperandEb
	.section	.text._ZN2v88internal8compiler15OperandAssigner16CommitAssignmentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15OperandAssigner16CommitAssignmentEv
	.type	_ZN2v88internal8compiler15OperandAssigner16CommitAssignmentEv, @function
_ZN2v88internal8compiler15OperandAssigner16CommitAssignmentEv:
.LFB23620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	176(%rax), %rbx
	movq	168(%rax), %r15
	movq	%rbx, %r12
	subq	%r15, %r12
	cmpq	%rbx, %r15
	je	.L2323
	movq	%rdi, %r13
	.p2align 4,,10
	.p2align 3
.L2359:
	movq	496(%rax), %rdi
	movq	(%r15), %r14
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	0(%r13), %rdx
	movq	176(%rdx), %rax
	subq	168(%rdx), %rax
	cmpq	%r12, %rax
	jne	.L2386
	testq	%r14, %r14
	je	.L2327
	cmpq	$0, 16(%r14)
	je	.L2327
	movq	$0, -64(%rbp)
	movl	4(%r14), %eax
	movq	32(%r14), %rdi
	movl	%eax, %ecx
	shrl	$5, %ecx
	andl	$3, %ecx
	cmpl	$1, %ecx
	je	.L2387
	movl	4(%rdi), %ecx
	testb	$64, %cl
	je	.L2332
	movq	104(%rdi), %rax
	shrl	$13, %ecx
	movslq	44(%rax), %rax
	salq	$35, %rax
	movq	%rax, %rsi
	movzbl	%cl, %eax
	salq	$5, %rax
	orq	%rsi, %rax
	orq	$13, %rax
	movq	%rax, -64(%rbp)
	movl	4(%r14), %eax
.L2332:
	testb	$8, %al
	jne	.L2388
.L2333:
	movl	%eax, %edx
	movq	%r14, %rdi
	shrl	$7, %edx
	andl	$63, %edx
	cmpl	$32, %edx
	je	.L2345
	.p2align 4,,10
	.p2align 3
.L2390:
	shrl	$13, %eax
	salq	$35, %rdx
	movzbl	%al, %esi
	salq	$5, %rsi
	orq	%rdx, %rsi
	orq	$5, %rsi
.L2346:
	movq	24(%rdi), %rax
	testq	%rax, %rax
	jne	.L2349
	jmp	.L2354
	.p2align 4,,10
	.p2align 3
.L2353:
	movq	%rsi, (%rcx)
.L2352:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2354
.L2349:
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.L2352
	movzbl	28(%rax), %edx
	andl	$3, %edx
	cmpb	$3, %dl
	jne	.L2353
	movq	-64(%rbp), %rdx
	movq	%rdx, (%rcx)
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L2349
.L2354:
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2389
	movl	4(%rdi), %eax
	movl	%eax, %edx
	shrl	$7, %edx
	andl	$63, %edx
	cmpl	$32, %edx
	jne	.L2390
.L2345:
	movq	32(%rdi), %rdx
	movl	4(%rdx), %eax
	movq	104(%rdx), %rcx
	movl	%eax, %edx
	shrl	$5, %edx
	andl	$3, %edx
	cmpl	$1, %edx
	je	.L2391
	shrl	$13, %eax
	movzbl	%al, %esi
	movslq	44(%rcx), %rax
	salq	$5, %rsi
	salq	$35, %rax
	orq	%rax, %rsi
	orq	$13, %rsi
	jmp	.L2346
	.p2align 4,,10
	.p2align 3
.L2389:
	testb	$7, -64(%rbp)
	je	.L2327
	movq	0(%r13), %rsi
	testb	$1, 488(%rsi)
	je	.L2356
	movl	4(%r14), %eax
	xorl	$96, %eax
	testb	$96, %al
	sete	%al
.L2357:
	testb	%al, %al
	jne	.L2327
	movl	4(%r14), %eax
	leaq	-64(%rbp), %rdx
	movq	%r14, %rdi
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$6, %al
	movl	$1, %eax
	cmovne	%eax, %ecx
	call	_ZN2v88internal8compiler17TopLevelLiveRange16CommitSpillMovesEPNS1_22RegisterAllocationDataERKNS1_18InstructionOperandEb
	.p2align 4,,10
	.p2align 3
.L2327:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L2323
	movq	0(%r13), %rax
	jmp	.L2359
	.p2align 4,,10
	.p2align 3
.L2391:
	movq	(%rcx), %rsi
	jmp	.L2346
.L2387:
	movq	104(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	movl	4(%r14), %eax
	testb	$8, %al
	je	.L2333
.L2388:
	leaq	56(%rdx), %r9
	movq	64(%rdx), %rdx
	movl	88(%r14), %ecx
	testq	%rdx, %rdx
	je	.L2334
	movq	%r9, %rsi
	jmp	.L2335
	.p2align 4,,10
	.p2align 3
.L2392:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L2336
.L2335:
	cmpl	32(%rdx), %ecx
	jle	.L2392
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2335
.L2336:
	cmpq	%r9, %rsi
	je	.L2334
	cmpl	32(%rsi), %ecx
	cmovge	%rsi, %r9
.L2334:
	movl	%eax, %edx
	movq	40(%r9), %rsi
	shrl	$7, %edx
	andl	$63, %edx
	cmpl	$32, %edx
	je	.L2339
	movl	%eax, %ecx
	salq	$35, %rdx
	shrl	$13, %ecx
	movzbl	%cl, %ecx
	salq	$5, %rcx
	orq	%rdx, %rcx
	orq	$5, %rcx
.L2340:
	movq	24(%rsi), %rdx
	movq	32(%rsi), %rsi
	cmpq	%rsi, %rdx
	je	.L2333
	movq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L2344:
	movq	(%rax), %rdx
	addq	$8, %rax
	movq	%rcx, (%rdx)
	cmpq	%rax, %rsi
	jne	.L2344
	movl	4(%r14), %eax
	jmp	.L2333
.L2356:
	movzbl	120(%r14), %eax
	jmp	.L2357
.L2386:
	leaq	.LC63(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2323:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2393
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2339:
	.cfi_restore_state
	movl	4(%rdi), %edx
	movq	104(%rdi), %rdi
	movl	%edx, %ecx
	shrl	$5, %ecx
	andl	$3, %ecx
	cmpl	$1, %ecx
	je	.L2394
	movl	%edx, %ecx
	movslq	44(%rdi), %rdx
	shrl	$13, %ecx
	movzbl	%cl, %ecx
	salq	$35, %rdx
	salq	$5, %rcx
	orq	%rdx, %rcx
	orq	$13, %rcx
	jmp	.L2340
.L2394:
	movq	(%rdi), %rcx
	jmp	.L2340
.L2393:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23620:
	.size	_ZN2v88internal8compiler15OperandAssigner16CommitAssignmentEv, .-_ZN2v88internal8compiler15OperandAssigner16CommitAssignmentEv
	.section	.text._ZN2v88internal8compiler18LiveRangeConnector18ResolveControlFlowEPKNS1_16InstructionBlockERKNS1_18InstructionOperandES5_S8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18LiveRangeConnector18ResolveControlFlowEPKNS1_16InstructionBlockERKNS1_18InstructionOperandES5_S8_
	.type	_ZN2v88internal8compiler18LiveRangeConnector18ResolveControlFlowEPKNS1_16InstructionBlockERKNS1_18InstructionOperandES5_S8_, @function
_ZN2v88internal8compiler18LiveRangeConnector18ResolveControlFlowEPKNS1_16InstructionBlockERKNS1_18InstructionOperandES5_S8_:
.LFB23631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rsi), %rax
	subq	40(%rsi), %rax
	cmpq	$4, %rax
	je	.L2429
	movl	116(%rcx), %r13d
	movl	$1, %edx
	subl	$1, %r13d
.L2397:
	movq	(%rdi), %rax
	movslq	%r13d, %rsi
	movq	16(%rax), %rcx
	movq	208(%rcx), %rdi
	movq	%rdi, %rax
	subq	216(%rcx), %rax
	sarq	$3, %rax
	addq	%rsi, %rax
	js	.L2398
	cmpq	$63, %rax
	jle	.L2430
	movq	%rax, %rsi
	sarq	$6, %rsi
.L2401:
	movq	232(%rcx), %rdi
	movq	%rsi, %r8
	salq	$6, %r8
	movq	(%rdi,%rsi,8), %rsi
	subq	%r8, %rax
	leaq	(%rsi,%rax,8), %rsi
.L2400:
	movslq	%edx, %rax
	movq	(%rsi), %rdx
	leaq	(%rdx,%rax,8), %r15
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2431
.L2402:
	movq	(%r14), %rdx
	testb	$4, %dl
	je	.L2405
	xorl	%eax, %eax
	testb	$24, %dl
	jne	.L2406
	movq	%rdx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L2406:
	andq	$-8161, %rdx
	orq	%rax, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L2405:
	movq	(%rbx), %rax
	testb	$4, %al
	je	.L2407
	xorl	%ecx, %ecx
	testb	$24, %al
	jne	.L2408
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L2408:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L2407:
	cmpq	%rdx, %rax
	je	.L2395
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L2432
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2411:
	movq	(%r14), %rdx
	movq	%rdx, (%rax)
	movq	(%rbx), %rdx
	movq	%rax, -64(%rbp)
	movq	%rdx, 8(%rax)
	movq	16(%r12), %rsi
	cmpq	8(%r12), %rsi
	movq	24(%r12), %rax
	je	.L2412
.L2413:
	cmpq	%rax, %rsi
	je	.L2416
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L2395:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2433
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2430:
	.cfi_restore_state
	leaq	(%rdi,%rsi,8), %rsi
	jmp	.L2400
	.p2align 4,,10
	.p2align 3
.L2429:
	movl	112(%rsi), %r13d
	xorl	%edx, %edx
	jmp	.L2397
	.p2align 4,,10
	.p2align 3
.L2398:
	movq	%rax, %rsi
	notq	%rsi
	shrq	$6, %rsi
	notq	%rsi
	jmp	.L2401
	.p2align 4,,10
	.p2align 3
.L2431:
	movq	8(%rcx), %rdx
	movq	16(%rdx), %r12
	movq	24(%rdx), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	jbe	.L2434
	leaq	32(%r12), %rax
	movq	%rax, 16(%rdx)
.L2404:
	movq	%rdx, (%r12)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	%r12, 8(%r15)
	jmp	.L2402
	.p2align 4,,10
	.p2align 3
.L2416:
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2395
	.p2align 4,,10
	.p2align 3
.L2412:
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	$31, %rdx
	ja	.L2413
	movq	(%r12), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$31, %rax
	jbe	.L2435
	leaq	32(%rsi), %rax
	movq	%rax, 16(%rdi)
.L2415:
	movq	%rsi, 8(%r12)
	movq	%rsi, 16(%r12)
	movq	%rax, 24(%r12)
	jmp	.L2413
	.p2align 4,,10
	.p2align 3
.L2432:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2411
	.p2align 4,,10
	.p2align 3
.L2434:
	movq	%rdx, %rdi
	movl	$32, %esi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L2404
.L2435:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	leaq	32(%rax), %rax
	jmp	.L2415
.L2433:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23631:
	.size	_ZN2v88internal8compiler18LiveRangeConnector18ResolveControlFlowEPKNS1_16InstructionBlockERKNS1_18InstructionOperandES5_S8_, .-_ZN2v88internal8compiler18LiveRangeConnector18ResolveControlFlowEPKNS1_16InstructionBlockERKNS1_18InstructionOperandES5_S8_
	.section	.text._ZN2v88internal8compiler17ConstraintBuilder21MeetConstraintsBeforeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17ConstraintBuilder21MeetConstraintsBeforeEi
	.type	_ZN2v88internal8compiler17ConstraintBuilder21MeetConstraintsBeforeEi, @function
_ZN2v88internal8compiler17ConstraintBuilder21MeetConstraintsBeforeEi:
.LFB23410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	%esi, %rbx
	subq	$88, %rsp
	movl	%esi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rbx, -80(%rbp)
	movq	16(%rax), %rdx
	movq	208(%rdx), %rcx
	movq	%rcx, %rax
	subq	216(%rdx), %rax
	sarq	$3, %rax
	addq	%rbx, %rax
	js	.L2437
	cmpq	$63, %rax
	jle	.L2527
	movq	%rax, %rcx
	sarq	$6, %rcx
.L2440:
	movq	232(%rdx), %rdx
	movq	%rcx, %rsi
	salq	$6, %rsi
	movq	(%rdx,%rcx,8), %rdx
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %rax
.L2439:
	movq	(%rax), %r15
	movq	-80(%rbp), %rax
	xorl	%r12d, %r12d
	movl	4(%r15), %ecx
	salq	$3, %rax
	movq	%rax, -96(%rbp)
	movzbl	%cl, %edx
	testl	$16776960, %ecx
	je	.L2464
	movq	%r15, %r14
	movq	%r11, %r15
	jmp	.L2463
	.p2align 4,,10
	.p2align 3
.L2531:
	cmpq	$63, %rax
	jg	.L2447
	addq	-96(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	16(%rcx), %r8
	testq	%r8, %r8
	je	.L2528
.L2450:
	movq	(%r9), %rax
	testb	$4, %al
	je	.L2453
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L2454
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L2454:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L2453:
	cmpq	%rax, %r13
	je	.L2455
	movq	(%r8), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L2529
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2457:
	movq	%r13, (%rax)
	movq	40(%r14,%rbx,8), %rdx
	movq	%rdx, 8(%rax)
	movq	%rax, -64(%rbp)
	movq	16(%r8), %rax
	cmpq	8(%r8), %rax
	movq	24(%r8), %rsi
	je	.L2458
.L2459:
	cmpq	%rax, %rsi
	je	.L2462
.L2539:
	movq	-64(%rbp), %rdx
	movq	%rdx, (%rax)
	addq	$8, 16(%r8)
.L2455:
	movl	4(%r14), %ecx
	movzbl	%cl, %edx
.L2443:
	movl	%ecx, %eax
	addq	$1, %r12
	shrl	$8, %eax
	movzwl	%ax, %eax
	cmpq	%r12, %rax
	jbe	.L2530
.L2463:
	movl	%edx, %ebx
	addq	%r12, %rbx
	leaq	40(%r14,%rbx,8), %r9
	movq	(%r9), %rsi
	movl	%esi, %eax
	andl	$7, %eax
	subl	$3, %eax
	cmpl	$1, %eax
	jbe	.L2443
	movabsq	$34359738368, %rax
	testq	%rax, %rsi
	je	.L2444
	movq	%rsi, %rax
	shrq	$36, %rax
	andl	$7, %eax
	subl	$3, %eax
	cmpl	$1, %eax
	ja	.L2443
.L2444:
	movabsq	$34359738360, %r13
	movq	%r9, -72(%rbp)
	movabsq	$652835028993, %rax
	andq	%rsi, %r13
	shrq	$3, %rsi
	orq	%rax, %r13
	movq	(%r15), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	movq	-72(%rbp), %r9
	movl	$1, %ecx
	leal	-7(%rax), %edx
	cmpb	$1, %dl
	jbe	.L2445
	subl	$10, %eax
	xorl	%ecx, %ecx
	cmpb	$1, %al
	setbe	%cl
.L2445:
	movl	-88(%rbp), %edx
	movq	%r9, %rsi
	movl	$1, %r8d
	movq	%r15, %rdi
	movq	%r9, -72(%rbp)
	call	_ZN2v88internal8compiler17ConstraintBuilder13AllocateFixedEPNS1_18UnallocatedOperandEibb
	movq	(%r15), %rax
	movq	-72(%rbp), %r9
	movq	16(%rax), %rdx
	movq	208(%rdx), %rcx
	movq	%rcx, %rax
	subq	216(%rdx), %rax
	sarq	$3, %rax
	addq	-80(%rbp), %rax
	jns	.L2531
	movq	%rax, %rcx
	notq	%rcx
	shrq	$6, %rcx
	notq	%rcx
.L2449:
	movq	232(%rdx), %rsi
	movq	%rcx, %rdi
	salq	$6, %rdi
	movq	(%rsi,%rcx,8), %rcx
	subq	%rdi, %rax
	leaq	(%rcx,%rax,8), %rcx
	movq	(%rcx), %rcx
	movq	16(%rcx), %r8
	testq	%r8, %r8
	jne	.L2450
.L2528:
	movq	8(%rdx), %rdx
	movq	16(%rdx), %r8
	movq	24(%rdx), %rax
	subq	%r8, %rax
	cmpq	$31, %rax
	jbe	.L2532
	leaq	32(%r8), %rax
	movq	%rax, 16(%rdx)
.L2452:
	movq	%rdx, (%r8)
	movq	$0, 8(%r8)
	movq	$0, 16(%r8)
	movq	$0, 24(%r8)
	movq	%r8, 16(%rcx)
	jmp	.L2450
	.p2align 4,,10
	.p2align 3
.L2527:
	leaq	(%rcx,%rbx,8), %rax
	jmp	.L2439
	.p2align 4,,10
	.p2align 3
.L2447:
	movq	%rax, %rcx
	sarq	$6, %rcx
	jmp	.L2449
	.p2align 4,,10
	.p2align 3
.L2530:
	movq	%r15, %r11
	movq	%r14, %r15
.L2464:
	movq	-80(%rbp), %rax
	movl	%edx, %r14d
	xorl	%ebx, %ebx
	movabsq	$34359738368, %r9
	salq	$3, %rax
	movq	%rax, -88(%rbp)
	testl	%edx, %edx
	je	.L2436
	.p2align 4,,10
	.p2align 3
.L2492:
	movq	40(%r15,%rbx,8), %r12
	movl	%r12d, %eax
	andl	$7, %eax
	cmpl	$1, %eax
	jne	.L2465
	testq	%r9, %r12
	je	.L2465
	movq	%r12, %rax
	shrq	$36, %rax
	andl	$7, %eax
	cmpl	$7, %eax
	je	.L2533
	.p2align 4,,10
	.p2align 3
.L2465:
	addq	$1, %rbx
	movl	%edx, %r14d
	cmpq	%r14, %rbx
	jb	.L2492
.L2436:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2534
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2533:
	.cfi_restore_state
	movabsq	$34359738360, %rdx
	leaq	40(%r15,%r14,8), %r13
	movabsq	$652835028993, %rdi
	movabsq	$34359738360, %rsi
	movq	0(%r13), %rax
	andq	%r12, %rsi
	andq	%rax, %rdx
	movq	%rax, %rcx
	orq	%rdi, %rdx
	shrq	$3, %rcx
	movabsq	$-34359738361, %rdi
	andq	%rdi, %rax
	orq	%rsi, %rax
	movq	%rax, 0(%r13)
	movq	(%r11), %rsi
	movq	16(%rsi), %rdi
	movq	208(%rdi), %r8
	movq	%r8, %rsi
	subq	216(%rdi), %rsi
	sarq	$3, %rsi
	addq	-80(%rbp), %rsi
	js	.L2466
	cmpq	$63, %rsi
	jg	.L2467
	addq	-88(%rbp), %r8
	movq	(%r8), %r10
	movq	16(%r10), %r8
	testq	%r8, %r8
	je	.L2535
.L2470:
	testb	$4, %al
	je	.L2473
	xorl	%esi, %esi
	testb	$24, %al
	je	.L2536
.L2474:
	andq	$-8161, %rax
	orq	%rsi, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L2473:
	xorl	%r13d, %r13d
	cmpq	%rax, %rdx
	je	.L2475
	movq	(%r8), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rsi
	subq	%rax, %rsi
	cmpq	$15, %rsi
	jbe	.L2537
	leaq	16(%rax), %rsi
	movq	%rsi, 16(%rdi)
.L2477:
	movq	%rdx, (%rax)
	movq	40(%r15,%r14,8), %rdx
	movq	%rdx, 8(%rax)
	movq	%rax, -64(%rbp)
	movq	16(%r8), %rsi
	cmpq	8(%r8), %rsi
	movq	24(%r8), %rax
	je	.L2478
.L2479:
	cmpq	%rax, %rsi
	je	.L2480
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r8)
.L2481:
	movq	-64(%rbp), %r13
.L2475:
	movq	(%r11), %rax
	movl	%ecx, %esi
	movq	%r11, -72(%rbp)
	movq	16(%rax), %rdi
	call	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	movq	-72(%rbp), %r11
	movabsq	$34359738368, %r9
	leal	-7(%rax), %edx
	cmpb	$1, %dl
	jbe	.L2482
	subl	$10, %eax
	cmpb	$1, %al
	jbe	.L2482
.L2483:
	movzbl	4(%r15), %edx
	jmp	.L2465
	.p2align 4,,10
	.p2align 3
.L2458:
	movq	%rsi, %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	ja	.L2459
	movq	(%r8), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L2538
	leaq	32(%rax), %rsi
	movq	%rsi, 16(%rdi)
.L2461:
	movq	%rax, 8(%r8)
	movq	%rax, 16(%r8)
	movq	%rsi, 24(%r8)
	cmpq	%rax, %rsi
	jne	.L2539
	.p2align 4,,10
	.p2align 3
.L2462:
	leaq	-64(%rbp), %rdx
	movq	%r8, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2455
	.p2align 4,,10
	.p2align 3
.L2482:
	movq	(%r11), %rax
	movq	%r12, %rsi
	movq	%r11, -72(%rbp)
	shrq	$3, %rsi
	movq	16(%rax), %rdi
	call	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	movq	-72(%rbp), %r11
	movabsq	$34359738368, %r9
	leal	-7(%rax), %edx
	cmpb	$1, %dl
	jbe	.L2483
	subl	$10, %eax
	cmpb	$1, %al
	jbe	.L2483
	movq	24(%r15), %r12
	testq	%r12, %r12
	je	.L2483
	movq	(%r11), %r14
	movq	368(%r14), %rdx
	cmpq	376(%r14), %rdx
	je	.L2485
	movq	%r12, %xmm0
	movq	%r13, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rdx)
	addq	$16, 368(%r14)
	jmp	.L2483
	.p2align 4,,10
	.p2align 3
.L2467:
	movq	%rsi, %r8
	sarq	$6, %r8
.L2469:
	movq	%r8, %r10
	salq	$6, %r10
	subq	%r10, %rsi
	movq	232(%rdi), %r10
	movq	(%r10,%r8,8), %r8
	leaq	(%r8,%rsi,8), %r8
	movq	(%r8), %r10
	movq	16(%r10), %r8
	testq	%r8, %r8
	jne	.L2470
.L2535:
	movq	8(%rdi), %rdi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	$31, %rax
	jbe	.L2540
	leaq	32(%r8), %rax
	movq	%rax, 16(%rdi)
.L2472:
	movq	%rdi, (%r8)
	movq	$0, 8(%r8)
	movq	$0, 16(%r8)
	movq	$0, 24(%r8)
	movq	%r8, 16(%r10)
	movq	0(%r13), %rax
	jmp	.L2470
	.p2align 4,,10
	.p2align 3
.L2437:
	movq	%rax, %rcx
	notq	%rcx
	shrq	$6, %rcx
	notq	%rcx
	jmp	.L2440
	.p2align 4,,10
	.p2align 3
.L2529:
	movl	$16, %esi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r8
	jmp	.L2457
	.p2align 4,,10
	.p2align 3
.L2466:
	movq	%rsi, %r8
	notq	%r8
	shrq	$6, %r8
	notq	%r8
	jmp	.L2469
	.p2align 4,,10
	.p2align 3
.L2536:
	movq	%rax, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
	jmp	.L2474
.L2480:
	leaq	-64(%rbp), %rdx
	movq	%r8, %rdi
	movq	%r11, -96(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-96(%rbp), %r11
	movq	-72(%rbp), %rcx
	jmp	.L2481
.L2478:
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	$31, %rdx
	ja	.L2479
	movq	(%r8), %rdi
	movl	$32, %esi
	movq	%r11, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal4Zone3NewEm
	movq	-72(%rbp), %r8
	movq	-104(%rbp), %r11
	movq	%rax, %xmm0
	movq	%rax, %rsi
	leaq	32(%rax), %rax
	movq	-96(%rbp), %rcx
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 24(%r8)
	movups	%xmm0, 8(%r8)
	jmp	.L2479
.L2532:
	movq	%rdx, %rdi
	movl	$32, %esi
	movq	%rcx, -112(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %rcx
	movq	%rax, %r8
	jmp	.L2452
.L2537:
	movl	$16, %esi
	movq	%r11, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %r11
	jmp	.L2477
.L2538:
	movl	$32, %esi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r8
	leaq	32(%rax), %rsi
	jmp	.L2461
.L2485:
	movq	360(%r14), %rcx
	movq	%rdx, %r8
	subq	%rcx, %r8
	movq	%r8, %rsi
	sarq	$4, %rsi
	cmpq	$134217727, %rsi
	je	.L2541
	testq	%rsi, %rsi
	je	.L2501
	leaq	(%rsi,%rsi), %rax
	movl	$2147483632, %r10d
	cmpq	%rax, %rsi
	jbe	.L2542
.L2488:
	movq	352(%r14), %rdi
	movq	%r10, %rsi
	movq	%r11, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal4Zone3NewEm
	movq	-72(%rbp), %r10
	movq	-96(%rbp), %rdx
	movabsq	$34359738368, %r9
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %r8
	movq	%rax, %rsi
	movq	-120(%rbp), %r11
	leaq	(%rax,%r10), %rdi
	leaq	16(%rax), %rax
.L2489:
	movq	%r12, %xmm0
	movq	%r13, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%rsi,%r8)
	cmpq	%rcx, %rdx
	je	.L2490
	subq	$16, %rdx
	xorl	%eax, %eax
	subq	%rcx, %rdx
	movq	%rdx, %r10
	movq	%rdx, %r8
	xorl	%edx, %edx
	shrq	$4, %r10
	addq	$1, %r10
	.p2align 4,,10
	.p2align 3
.L2491:
	movdqu	(%rcx,%rax), %xmm1
	addq	$1, %rdx
	movups	%xmm1, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdx, %r10
	ja	.L2491
	leaq	32(%rsi,%r8), %rax
.L2490:
	movq	%rsi, %xmm0
	movq	%rax, %xmm4
	movq	%rdi, 376(%r14)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 360(%r14)
	jmp	.L2483
.L2540:
	movl	$32, %esi
	movq	%r11, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%r10, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdi
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %rcx
	movq	%rax, %r8
	movq	-120(%rbp), %r11
	jmp	.L2472
.L2542:
	testq	%rax, %rax
	jne	.L2543
	movl	$16, %eax
	xorl	%edi, %edi
	xorl	%esi, %esi
	jmp	.L2489
.L2501:
	movl	$16, %r10d
	jmp	.L2488
.L2534:
	call	__stack_chk_fail@PLT
.L2541:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2543:
	cmpq	$134217727, %rax
	movl	$134217727, %r10d
	cmova	%r10, %rax
	salq	$4, %rax
	movq	%rax, %r10
	jmp	.L2488
	.cfi_endproc
.LFE23410:
	.size	_ZN2v88internal8compiler17ConstraintBuilder21MeetConstraintsBeforeEi, .-_ZN2v88internal8compiler17ConstraintBuilder21MeetConstraintsBeforeEi
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData10AddGapMoveEiNS1_11Instruction11GapPositionERKNS1_18InstructionOperandES7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData10AddGapMoveEiNS1_11Instruction11GapPositionERKNS1_18InstructionOperandES7_
	.type	_ZN2v88internal8compiler22RegisterAllocationData10AddGapMoveEiNS1_11Instruction11GapPositionERKNS1_18InstructionOperandES7_, @function
_ZN2v88internal8compiler22RegisterAllocationData10AddGapMoveEiNS1_11Instruction11GapPositionERKNS1_18InstructionOperandES7_:
.LFB23377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	208(%rcx), %rdi
	movq	%rdi, %rax
	subq	216(%rcx), %rax
	sarq	$3, %rax
	addq	%rsi, %rax
	js	.L2545
	cmpq	$63, %rax
	jle	.L2578
	movq	%rax, %rsi
	sarq	$6, %rsi
.L2548:
	movq	232(%rcx), %rdi
	movq	%rsi, %r8
	salq	$6, %r8
	movq	(%rdi,%rsi,8), %rsi
	subq	%r8, %rax
	leaq	(%rsi,%rax,8), %rax
.L2547:
	movq	(%rax), %rax
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,8), %r14
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L2579
.L2549:
	movq	(%rbx), %rdx
	testb	$4, %dl
	je	.L2552
	xorl	%eax, %eax
	testb	$24, %dl
	jne	.L2553
	movq	%rdx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L2553:
	andq	$-8161, %rdx
	orq	%rax, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L2552:
	movq	0(%r13), %rax
	testb	$4, %al
	je	.L2554
	xorl	%ecx, %ecx
	testb	$24, %al
	jne	.L2555
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L2555:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L2554:
	xorl	%r8d, %r8d
	cmpq	%rdx, %rax
	je	.L2544
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L2580
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2558:
	movq	(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	0(%r13), %rdx
	movq	%rax, -64(%rbp)
	movq	%rdx, 8(%rax)
	movq	16(%r12), %rax
	cmpq	8(%r12), %rax
	movq	24(%r12), %rsi
	je	.L2559
.L2560:
	cmpq	%rax, %rsi
	je	.L2563
	movq	-64(%rbp), %rdx
	movq	%rdx, (%rax)
	addq	$8, 16(%r12)
.L2564:
	movq	-64(%rbp), %r8
.L2544:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2581
	addq	$24, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2578:
	.cfi_restore_state
	leaq	(%rdi,%rsi,8), %rax
	jmp	.L2547
	.p2align 4,,10
	.p2align 3
.L2545:
	movq	%rax, %rsi
	notq	%rsi
	shrq	$6, %rsi
	notq	%rsi
	jmp	.L2548
	.p2align 4,,10
	.p2align 3
.L2579:
	movq	8(%rcx), %r15
	movq	16(%r15), %r12
	movq	24(%r15), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	jbe	.L2582
	leaq	32(%r12), %rax
	movq	%rax, 16(%r15)
.L2551:
	movq	%r15, (%r12)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	%r12, 8(%r14)
	jmp	.L2549
	.p2align 4,,10
	.p2align 3
.L2563:
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2564
	.p2align 4,,10
	.p2align 3
.L2559:
	movq	%rsi, %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	ja	.L2560
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L2583
	leaq	32(%rax), %rsi
	movq	%rsi, 16(%rdi)
.L2562:
	movq	%rax, 8(%r12)
	movq	%rax, 16(%r12)
	movq	%rsi, 24(%r12)
	jmp	.L2560
	.p2align 4,,10
	.p2align 3
.L2580:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2558
	.p2align 4,,10
	.p2align 3
.L2582:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L2551
.L2583:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	32(%rax), %rsi
	jmp	.L2562
.L2581:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23377:
	.size	_ZN2v88internal8compiler22RegisterAllocationData10AddGapMoveEiNS1_11Instruction11GapPositionERKNS1_18InstructionOperandES7_, .-_ZN2v88internal8compiler22RegisterAllocationData10AddGapMoveEiNS1_11Instruction11GapPositionERKNS1_18InstructionOperandES7_
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB26771:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L2592
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L2586:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2586
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2592:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE26771:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt6vectorIPN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB27545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L2633
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L2611
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L2634
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L2597:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L2635
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2600:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L2598
	.p2align 4,,10
	.p2align 3
.L2634:
	testq	%rdx, %rdx
	jne	.L2636
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L2598:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L2601
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L2614
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L2614
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L2603:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L2603
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L2605
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L2605:
	leaq	16(%rax,%r8), %rcx
.L2601:
	cmpq	%r14, %r12
	je	.L2606
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L2615
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L2615
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L2608:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L2608
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L2610
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L2610:
	leaq	8(%rcx,%r9), %rcx
.L2606:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2611:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L2597
	.p2align 4,,10
	.p2align 3
.L2615:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2607:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L2607
	jmp	.L2610
	.p2align 4,,10
	.p2align 3
.L2614:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2602:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L2602
	jmp	.L2605
.L2635:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L2600
.L2633:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2636:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L2597
	.cfi_endproc
.LFE27545:
	.size	_ZNSt6vectorIPN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData11PhiMapValue10AddOperandEPNS1_18InstructionOperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValue10AddOperandEPNS1_18InstructionOperandE
	.type	_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValue10AddOperandEPNS1_18InstructionOperandE, @function
_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValue10AddOperandEPNS1_18InstructionOperandE:
.LFB23339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -8(%rbp)
	movq	32(%rdi), %rsi
	cmpq	40(%rdi), %rsi
	je	.L2638
	movq	-8(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 32(%rdi)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2638:
	.cfi_restore_state
	leaq	-8(%rbp), %rdx
	addq	$16, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23339:
	.size	_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValue10AddOperandEPNS1_18InstructionOperandE, .-_ZN2v88internal8compiler22RegisterAllocationData11PhiMapValue10AddOperandEPNS1_18InstructionOperandE
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_.str1.1,"aMS",@progbits,1
.LC68:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	.type	_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_, @function
_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_:
.LFB27627:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L2751
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	24(%r12), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jb	.L2644
	movq	%rdi, %r8
	movq	(%rcx), %xmm0
	subq	%rsi, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L2645
	leaq	0(,%rdx,8), %r14
	movq	%rdi, %rdx
	subq	%r14, %rdx
	cmpq	%rdx, %rdi
	je	.L2692
	leaq	-8(%rdi), %rax
	movl	$16, %ecx
	subq	%rdx, %rax
	subq	%r14, %rcx
	shrq	$3, %rax
	testq	%rcx, %rcx
	leaq	16(%rdi), %rcx
	setle	%sil
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L2693
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L2693
	leaq	1(%rax), %r8
	xorl	%eax, %eax
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L2648:
	movdqu	(%rdx,%rax), %xmm2
	movups	%xmm2, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L2648
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	leaq	(%rdx,%rcx), %rsi
	addq	%rdi, %rcx
	cmpq	%rax, %r8
	je	.L2650
	movq	(%rsi), %rax
	movq	%rax, (%rcx)
.L2650:
	movq	16(%r12), %rax
.L2646:
	addq	%r14, %rax
	movq	%rax, 16(%r12)
	cmpq	%rdx, %rbx
	je	.L2651
	subq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%xmm0, -40(%rbp)
	subq	%rdx, %rdi
	call	memmove@PLT
	movq	-40(%rbp), %xmm0
.L2651:
	leaq	(%rbx,%r14), %rdx
	cmpq	%rdx, %rbx
	je	.L2641
	subq	%rbx, %rdx
	movq	%rbx, %rax
	subq	$8, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rdx, %rdx
	je	.L2691
	movq	%rcx, %rdx
	movdqa	%xmm0, %xmm1
	shrq	%rdx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rdx
	addq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L2655:
	movups	%xmm1, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L2655
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	(%rbx,%rax,8), %r13
	cmpq	%rax, %rcx
	je	.L2641
.L2691:
	movq	%xmm0, 0(%r13)
.L2641:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2751:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L2645:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	subq	%rax, %rdx
	je	.L2694
	cmpq	$1, %rdx
	je	.L2695
	movq	%rdx, %rsi
	movdqa	%xmm0, %xmm1
	xorl	%eax, %eax
	shrq	%rsi
	punpcklqdq	%xmm1, %xmm1
	.p2align 4,,10
	.p2align 3
.L2659:
	movq	%rax, %rcx
	addq	$1, %rax
	salq	$4, %rcx
	movups	%xmm1, (%rdi,%rcx)
	cmpq	%rax, %rsi
	jne	.L2659
	movq	%rdx, %rcx
	andq	$-2, %rcx
	leaq	(%rdi,%rcx,8), %rax
	cmpq	%rcx, %rdx
	je	.L2660
.L2658:
	movq	%xmm0, (%rax)
.L2660:
	leaq	(%rdi,%rdx,8), %rsi
.L2657:
	movq	%rsi, 16(%r12)
	cmpq	%rbx, %rdi
	je	.L2661
	subq	%rbx, %rdi
	leaq	15(%rbx), %rcx
	movq	%rbx, %rdx
	leaq	-8(%rdi), %r9
	subq	%rsi, %rcx
	movq	%r9, %rax
	shrq	$3, %rax
	cmpq	$30, %rcx
	jbe	.L2696
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L2696
	leaq	1(%rax), %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L2663:
	movdqu	(%rbx,%rax), %xmm3
	movups	%xmm3, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L2663
	movq	%rcx, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rax
	leaq	(%rbx,%rax), %rdi
	addq	%rax, %rsi
	cmpq	%r9, %rcx
	je	.L2664
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
.L2664:
	addq	%r8, 16(%r12)
	movq	%rcx, %rax
.L2690:
	movq	%rax, %rcx
	movdqa	%xmm0, %xmm1
	shrq	%rcx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rcx
	addq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L2668:
	movups	%xmm1, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L2668
	movq	%rax, %rdx
	andq	$-2, %rdx
	leaq	(%rbx,%rdx,8), %r13
	cmpq	%rax, %rdx
	jne	.L2691
	jmp	.L2641
	.p2align 4,,10
	.p2align 3
.L2644:
	movq	8(%r12), %rsi
	movl	$268435455, %r8d
	movq	%r8, %rax
	subq	%rsi, %rdi
	sarq	$3, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L2754
	cmpq	%rdi, %rdx
	movq	%rdi, %rax
	cmovnb	%rdx, %rax
	addq	%rax, %rdi
	setc	%al
	subq	%rsi, %r13
	movzbl	%al, %eax
	testq	%rax, %rax
	jne	.L2697
	testq	%rdi, %rdi
	jne	.L2755
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L2673:
	leaq	0(,%rdx,8), %r9
	leaq	(%rax,%r13), %r11
	leaq	0(%r13,%r9), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	leaq	8(%rcx), %rsi
	setnb	%dil
	cmpq	%rsi, %r11
	setnb	%sil
	orb	%sil, %dil
	je	.L2699
	leaq	-1(%rdx), %rsi
	cmpq	$3, %rsi
	jbe	.L2699
	movq	(%rcx), %xmm0
	leaq	-2(%rdx), %rdi
	xorl	%esi, %esi
	shrq	%rdi
	addq	$1, %rdi
	punpcklqdq	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L2677:
	movq	%rsi, %r10
	addq	$1, %rsi
	salq	$4, %r10
	movups	%xmm0, (%r11,%r10)
	cmpq	%rsi, %rdi
	ja	.L2677
	leaq	(%rdi,%rdi), %rsi
	salq	$4, %rdi
	addq	%r11, %rdi
	cmpq	%rsi, %rdx
	je	.L2679
	movq	(%rcx), %rdx
	movq	%rdx, (%rdi)
.L2679:
	movq	8(%r12), %rcx
	cmpq	%rcx, %rbx
	je	.L2700
	leaq	-8(%rbx), %r10
	leaq	15(%rcx), %rdx
	subq	%rcx, %r10
	subq	%rax, %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L2701
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L2701
	leaq	1(%rsi), %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L2682:
	movdqu	(%rcx,%rdx), %xmm4
	movups	%xmm4, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L2682
	movq	%r11, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %rcx
	addq	%rax, %rsi
	cmpq	%rdx, %r11
	je	.L2684
	movq	(%rcx), %rdx
	movq	%rdx, (%rsi)
.L2684:
	leaq	8(%rax,%r10), %rdi
.L2680:
	movq	16(%r12), %rdx
	leaq	(%rdi,%r9), %rcx
	cmpq	%rdx, %rbx
	je	.L2685
	subq	%rbx, %rdx
	leaq	-8(%rdx), %r10
	leaq	16(%rdi,%r9), %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %rbx
	leaq	16(%rbx), %rdx
	setnb	%dil
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %dil
	je	.L2702
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L2702
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L2687:
	movdqu	(%rbx,%rdx), %xmm5
	movups	%xmm5, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L2687
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rdi
	addq	%rdi, %rbx
	addq	%rcx, %rdi
	cmpq	%rdx, %rsi
	je	.L2689
	movq	(%rbx), %rdx
	movq	%rdx, (%rdi)
.L2689:
	leaq	8(%rcx,%r10), %rcx
.L2685:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%r8, 24(%r12)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2697:
	.cfi_restore_state
	movl	$2147483640, %esi
	movl	$2147483640, %r14d
.L2672:
	movq	(%r12), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L2756
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L2675:
	leaq	(%rax,%r14), %r8
	jmp	.L2673
	.p2align 4,,10
	.p2align 3
.L2699:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L2676:
	movq	(%rcx), %rdi
	movq	%rdi, (%r11,%rsi,8)
	addq	$1, %rsi
	cmpq	%rsi, %rdx
	jne	.L2676
	jmp	.L2679
	.p2align 4,,10
	.p2align 3
.L2693:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L2647:
	movq	(%rdx,%rcx,8), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rax, %rsi
	jne	.L2647
	jmp	.L2650
	.p2align 4,,10
	.p2align 3
.L2696:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L2662:
	movq	(%rbx,%rcx,8), %rdi
	movq	%rdi, (%rsi,%rcx,8)
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	%rax, %rdi
	jne	.L2662
	addq	%r8, 16(%r12)
	addq	$1, %rax
	testq	%r9, %r9
	je	.L2691
	jmp	.L2690
	.p2align 4,,10
	.p2align 3
.L2701:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2681:
	movq	(%rcx,%rdx,8), %rdi
	movq	%rdi, (%rax,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L2681
	jmp	.L2684
	.p2align 4,,10
	.p2align 3
.L2702:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2686:
	movq	(%rbx,%rdx,8), %rdi
	movq	%rdi, (%rcx,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L2686
	jmp	.L2689
	.p2align 4,,10
	.p2align 3
.L2694:
	movq	%rdi, %rsi
	jmp	.L2657
	.p2align 4,,10
	.p2align 3
.L2692:
	movq	%rdi, %rax
	jmp	.L2646
	.p2align 4,,10
	.p2align 3
.L2661:
	addq	%r8, %rsi
	movq	%rsi, 16(%r12)
	jmp	.L2641
	.p2align 4,,10
	.p2align 3
.L2700:
	movq	%rax, %rdi
	jmp	.L2680
.L2695:
	movq	%rdi, %rax
	jmp	.L2658
.L2756:
	movq	%r8, %rdi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	jmp	.L2675
.L2755:
	cmpq	$268435455, %rdi
	cmova	%r8, %rdi
	leaq	0(,%rdi,8), %r14
	movq	%r14, %rsi
	jmp	.L2672
.L2754:
	leaq	.LC68(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE27627:
	.size	_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_, .-_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi
	.type	_ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi, @function
_ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi:
.LFB23379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	176(%rdi), %rsi
	movq	168(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpl	%eax, %r12d
	jge	.L2764
.L2758:
	movslq	%r12d, %r13
	movq	(%rcx,%r13,8), %rax
	testq	%rax, %rax
	je	.L2765
.L2757:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2766
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2764:
	.cfi_restore_state
	leal	1(%r12), %edx
	movq	$0, -48(%rbp)
	movslq	%edx, %rdx
	cmpq	%rax, %rdx
	ja	.L2767
	jnb	.L2758
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L2758
	movq	%rax, 176(%rdi)
	jmp	.L2758
	.p2align 4,,10
	.p2align 3
.L2765:
	movq	16(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal8compiler22RegisterAllocationData12NewLiveRangeEiNS0_21MachineRepresentationE
	movq	168(%rbx), %rdx
	movq	%rax, (%rdx,%r13,8)
	jmp	.L2757
	.p2align 4,,10
	.p2align 3
.L2767:
	leaq	-48(%rbp), %rcx
	subq	%rax, %rdx
	leaq	160(%rdi), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	movq	168(%rbx), %rcx
	jmp	.L2758
.L2766:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23379:
	.size	_ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi, .-_ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi
	.section	.text._ZN2v88internal8compiler17ConstraintBuilder48MeetRegisterConstraintsForLastInstructionInBlockEPKNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17ConstraintBuilder48MeetRegisterConstraintsForLastInstructionInBlockEPKNS1_16InstructionBlockE
	.type	_ZN2v88internal8compiler17ConstraintBuilder48MeetRegisterConstraintsForLastInstructionInBlockEPKNS1_16InstructionBlockE, @function
_ZN2v88internal8compiler17ConstraintBuilder48MeetRegisterConstraintsForLastInstructionInBlockEPKNS1_16InstructionBlockE:
.LFB23408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movl	116(%rsi), %eax
	movq	%rsi, -104(%rbp)
	movq	16(%rdi), %rdx
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	leal	-1(%rax), %ebx
	movl	%eax, -72(%rbp)
	movq	208(%rdx), %rsi
	movslq	%ebx, %rcx
	movl	%ebx, -88(%rbp)
	movq	%rsi, %rax
	subq	216(%rdx), %rax
	sarq	$3, %rax
	addq	%rcx, %rax
	js	.L2769
	cmpq	$63, %rax
	jle	.L2828
	movq	%rax, %rcx
	sarq	$6, %rcx
.L2772:
	movq	232(%rdx), %rdx
	movq	%rcx, %rsi
	salq	$6, %rsi
	movq	(%rdx,%rcx,8), %rdx
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %rax
	movq	(%rax), %rax
	cmpb	$0, 4(%rax)
	movq	%rax, -112(%rbp)
	je	.L2768
.L2835:
	movq	$0, -72(%rbp)
	leaq	40(%rax), %r13
	movq	%r14, %r15
	.p2align 4,,10
	.p2align 3
.L2806:
	movq	0(%r13), %r12
	movq	%r12, %rsi
	shrq	$3, %rsi
	call	_ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi
	movabsq	$34359738368, %rcx
	movq	%rax, %rbx
	movq	0(%r13), %rax
	testq	%rcx, %rax
	je	.L2774
	shrq	$36, %rax
	andl	$7, %eax
	subl	$3, %eax
	cmpl	$1, %eax
	ja	.L2801
.L2774:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17ConstraintBuilder13AllocateFixedEPNS1_18UnallocatedOperandEibb
	movq	0(%r13), %rax
	testb	$4, %al
	jne	.L2777
	movq	-104(%rbp), %rax
	movb	$0, -81(%rbp)
	movq	8(%rax), %rcx
	movq	16(%rax), %r8
	cmpq	%r8, %rcx
	je	.L2776
.L2778:
	movabsq	$34359738360, %rax
	movq	%rbx, -96(%rbp)
	movq	%r8, %rbx
	movabsq	$652835028993, %r14
	andq	%rax, %r12
	orq	%r12, %r14
	movq	%r14, %rax
	movq	%rcx, %r14
	movq	%r15, %rcx
	movq	%rax, %r15
	jmp	.L2800
	.p2align 4,,10
	.p2align 3
.L2833:
	cmpq	$63, %rax
	jg	.L2784
	leaq	(%rdi,%rdx,8), %rax
	movq	(%rax), %rdx
	movq	8(%rdx), %r12
	testq	%r12, %r12
	je	.L2829
.L2787:
	movq	0(%r13), %rax
	testb	$4, %al
	je	.L2790
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L2791
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L2791:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L2790:
	cmpq	%rax, %r15
	je	.L2792
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L2830
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2794:
	movq	0(%r13), %rdx
	movq	%r15, 8(%rax)
	movq	%rdx, (%rax)
	movq	16(%r12), %rsi
	cmpq	8(%r12), %rsi
	movq	%rax, -64(%rbp)
	movq	24(%r12), %rax
	je	.L2795
.L2796:
	cmpq	%rax, %rsi
	je	.L2799
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L2792:
	addq	$4, %r14
	cmpq	%r14, %rbx
	je	.L2831
.L2800:
	movq	(%rcx), %rax
	movslq	(%r14), %r12
	movq	16(%rax), %rsi
	movq	16(%rsi), %rax
	movq	8(%rax), %rdi
	movq	16(%rax), %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r12
	jnb	.L2832
	movq	(%rdi,%r12,8), %rdx
	movq	208(%rsi), %rdi
	movslq	112(%rdx), %rdx
	movq	%rdi, %rax
	subq	216(%rsi), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	jns	.L2833
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
.L2786:
	movq	232(%rsi), %rdi
	movq	%rdx, %r9
	salq	$6, %r9
	movq	(%rdi,%rdx,8), %rdx
	subq	%r9, %rax
	leaq	(%rdx,%rax,8), %rax
	movq	(%rax), %rdx
	movq	8(%rdx), %r12
	testq	%r12, %r12
	jne	.L2787
.L2829:
	movq	8(%rsi), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	jbe	.L2834
	leaq	32(%r12), %rax
	movq	%rax, 16(%rdi)
.L2789:
	movq	%rdi, (%r12)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	%r12, 8(%rdx)
	jmp	.L2787
.L2828:
	leaq	(%rsi,%rcx,8), %rax
	movq	(%rax), %rax
	cmpb	$0, 4(%rax)
	movq	%rax, -112(%rbp)
	jne	.L2835
	.p2align 4,,10
	.p2align 3
.L2768:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2836
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2784:
	.cfi_restore_state
	movq	%rax, %rdx
	sarq	$6, %rdx
	jmp	.L2786
	.p2align 4,,10
	.p2align 3
.L2831:
	cmpb	$0, -81(%rbp)
	movq	-96(%rbp), %rbx
	movq	%rcx, %r15
	je	.L2801
.L2776:
	movq	-112(%rbp), %rax
	addq	$1, -72(%rbp)
	addq	$8, %r13
	movq	-72(%rbp), %rbx
	movzbl	4(%rax), %eax
	cmpq	%rbx, %rax
	jbe	.L2768
	movq	(%r15), %rdi
	jmp	.L2806
	.p2align 4,,10
	.p2align 3
.L2799:
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rcx, -80(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-80(%rbp), %rcx
	jmp	.L2792
	.p2align 4,,10
	.p2align 3
.L2795:
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	$31, %rdx
	ja	.L2796
	movq	(%r12), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$31, %rax
	jbe	.L2837
	leaq	32(%rsi), %rax
	movq	%rax, 16(%rdi)
.L2798:
	movq	%rsi, 8(%r12)
	movq	%rsi, 16(%r12)
	movq	%rax, 24(%r12)
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2801:
	movq	-104(%rbp), %rax
	movq	8(%rax), %r12
	movq	16(%rax), %r14
	cmpq	%r14, %r12
	je	.L2776
	.p2align 4,,10
	.p2align 3
.L2805:
	movq	(%r15), %rax
	movslq	(%r12), %rsi
	movq	16(%rax), %rdx
	movq	16(%rdx), %rdx
	movq	8(%rdx), %rcx
	movq	16(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2838
	movq	(%rax), %rdi
	movq	(%rcx,%rsi,8), %rdx
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	movl	112(%rdx), %edx
	subq	%rax, %rcx
	cmpq	$23, %rcx
	jbe	.L2839
	leaq	24(%rax), %rcx
	movq	%rcx, 16(%rdi)
	movq	112(%rbx), %rcx
.L2825:
	movl	%edx, (%rax)
	movq	%r13, 8(%rax)
	movq	%rcx, 16(%rax)
	cmpl	%edx, 124(%rbx)
	cmovle	124(%rbx), %edx
	addq	$4, %r12
	movq	%rax, 112(%rbx)
	movl	%edx, 124(%rbx)
	cmpq	%r12, %r14
	jne	.L2805
	jmp	.L2776
	.p2align 4,,10
	.p2align 3
.L2830:
	movl	$16, %esi
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %rcx
	jmp	.L2794
	.p2align 4,,10
	.p2align 3
.L2777:
	movq	%rax, %rdx
	shrq	$3, %rdx
	andl	$3, %edx
	cmpl	$1, %edx
	je	.L2780
.L2827:
	movq	-104(%rbp), %rax
	movb	$0, -81(%rbp)
	movq	8(%rax), %rcx
	movq	16(%rax), %r8
	cmpq	%rcx, %r8
	jne	.L2778
	jmp	.L2776
	.p2align 4,,10
	.p2align 3
.L2839:
	movl	$24, %esi
	movl	%edx, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	112(%rbx), %rcx
	movl	-80(%rbp), %edx
	jmp	.L2825
	.p2align 4,,10
	.p2align 3
.L2780:
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L2827
	movl	4(%rbx), %eax
	movq	%r13, 104(%rbx)
	andl	$-97, %eax
	orl	$32, %eax
	movl	%eax, 4(%rbx)
	movl	-88(%rbp), %eax
	cmpl	%eax, 124(%rbx)
	cmovle	124(%rbx), %eax
	movl	%eax, 124(%rbx)
	movq	-104(%rbp), %rax
	movq	8(%rax), %rcx
	movq	16(%rax), %r8
	cmpq	%r8, %rcx
	je	.L2776
	movb	$1, -81(%rbp)
	jmp	.L2778
	.p2align 4,,10
	.p2align 3
.L2834:
	movl	$32, %esi
	movq	%rcx, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %rdi
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rcx
	movq	%rax, %r12
	jmp	.L2789
.L2769:
	movq	%rax, %rcx
	notq	%rcx
	shrq	$6, %rcx
	notq	%rcx
	jmp	.L2772
.L2837:
	movl	$32, %esi
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %rcx
	movq	%rax, %rsi
	leaq	32(%rax), %rax
	jmp	.L2798
.L2832:
	movq	%r12, %rsi
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2838:
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2836:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23408:
	.size	_ZN2v88internal8compiler17ConstraintBuilder48MeetRegisterConstraintsForLastInstructionInBlockEPKNS1_16InstructionBlockE, .-_ZN2v88internal8compiler17ConstraintBuilder48MeetRegisterConstraintsForLastInstructionInBlockEPKNS1_16InstructionBlockE
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilder12LiveRangeForEPNS1_18InstructionOperandENS1_22RegisterAllocationData9SpillModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16LiveRangeBuilder12LiveRangeForEPNS1_18InstructionOperandENS1_22RegisterAllocationData9SpillModeE
	.type	_ZN2v88internal8compiler16LiveRangeBuilder12LiveRangeForEPNS1_18InstructionOperandENS1_22RegisterAllocationData9SpillModeE, @function
_ZN2v88internal8compiler16LiveRangeBuilder12LiveRangeForEPNS1_18InstructionOperandENS1_22RegisterAllocationData9SpillModeE:
.LFB23427:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rsi
	movl	%esi, %eax
	andl	$7, %eax
	cmpl	$1, %eax
	je	.L2848
	cmpl	$2, %eax
	je	.L2848
	testb	$4, %sil
	je	.L2840
	testb	$24, %sil
	jne	.L2840
	movq	%rsi, %rax
	sarq	$35, %rsi
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L2849
	movl	%edx, %ecx
	movl	%eax, %edx
	jmp	_ZN2v88internal8compiler16LiveRangeBuilder19FixedFPLiveRangeForEiNS0_21MachineRepresentationENS1_22RegisterAllocationData9SpillModeE
	.p2align 4,,10
	.p2align 3
.L2840:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2848:
	movq	(%rdi), %rdi
	shrq	$3, %rsi
	jmp	_ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi
	.p2align 4,,10
	.p2align 3
.L2849:
	jmp	_ZN2v88internal8compiler16LiveRangeBuilder17FixedLiveRangeForEiNS1_22RegisterAllocationData9SpillModeE
	.cfi_endproc
.LFE23427:
	.size	_ZN2v88internal8compiler16LiveRangeBuilder12LiveRangeForEPNS1_18InstructionOperandENS1_22RegisterAllocationData9SpillModeE, .-_ZN2v88internal8compiler16LiveRangeBuilder12LiveRangeForEPNS1_18InstructionOperandENS1_22RegisterAllocationData9SpillModeE
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE
	.type	_ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE, @function
_ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE:
.LFB23429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movl	%r9d, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movq	%r15, %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal8compiler16LiveRangeBuilder12LiveRangeForEPNS1_18InstructionOperandENS1_22RegisterAllocationData9SpillModeE
	testq	%rax, %rax
	je	.L2859
	movq	(%rbx), %rcx
	movq	16(%rax), %rdx
	movq	%rax, %r12
	movl	488(%rcx), %eax
	andl	$4, %eax
	testq	%rdx, %rdx
	je	.L2853
	cmpl	(%rdx), %r13d
	jl	.L2853
	testl	%eax, %eax
	jne	.L2867
.L2858:
	movl	%r13d, (%rdx)
	movl	(%r15), %eax
	andl	$7, %eax
	cmpl	$1, %eax
	je	.L2868
.L2859:
	xorl	%r13d, %r13d
	jmp	.L2850
	.p2align 4,,10
	.p2align 3
.L2853:
	movl	%r13d, %r9d
	xorl	%r8d, %r8d
	movq	(%rcx), %rcx
	movl	%r13d, %esi
	andl	$-2, %r9d
	movq	%r12, %rdi
	addl	$2, %r9d
	testl	%eax, %eax
	setne	%r8b
	movl	%r9d, %edx
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal8compiler17TopLevelLiveRange14AddUseIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb
	movq	(%rbx), %rax
	movl	-56(%rbp), %r9d
	movq	(%rax), %rdi
	movl	488(%rax), %edx
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	shrl	$2, %edx
	andl	$1, %edx
	subq	%rsi, %rax
	cmpq	$31, %rax
	jbe	.L2869
	leaq	32(%rsi), %rax
	movq	%rax, 16(%rdi)
.L2856:
	movq	$0, 16(%rsi)
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movl	%r9d, 24(%rsi)
	movl	$2080, 28(%rsi)
	movups	%xmm0, (%rsi)
	call	_ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb
	movl	(%r15), %eax
	andl	$7, %eax
	cmpl	$1, %eax
	jne	.L2859
.L2868:
	movzbl	-52(%rbp), %r8d
	movl	%r13d, %esi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%r14, %rcx
	call	_ZN2v88internal8compiler16LiveRangeBuilder14NewUsePositionENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movl	488(%rax), %edx
	shrl	$2, %edx
	andl	$1, %edx
	call	_ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb
.L2850:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2869:
	.cfi_restore_state
	movl	$32, %esi
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %r9d
	movl	-60(%rbp), %edx
	movq	%rax, %rsi
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L2867:
	movl	88(%r12), %esi
	movl	%r13d, %edx
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	16(%r12), %rdx
	jmp	.L2858
	.cfi_endproc
.LFE23429:
	.size	_ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE, .-_ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE.constprop.0, @function
_ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE.constprop.0:
.LFB32009:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	%ecx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movq	%r14, %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal8compiler16LiveRangeBuilder12LiveRangeForEPNS1_18InstructionOperandENS1_22RegisterAllocationData9SpillModeE
	testq	%rax, %rax
	je	.L2879
	movq	(%rbx), %rcx
	movq	16(%rax), %rdx
	movq	%rax, %r12
	movl	488(%rcx), %eax
	andl	$4, %eax
	testq	%rdx, %rdx
	je	.L2873
	cmpl	(%rdx), %r13d
	jl	.L2873
	testl	%eax, %eax
	jne	.L2887
.L2878:
	movl	%r13d, (%rdx)
	movl	(%r14), %eax
	andl	$7, %eax
	cmpl	$1, %eax
	je	.L2888
.L2879:
	xorl	%r13d, %r13d
	jmp	.L2870
	.p2align 4,,10
	.p2align 3
.L2873:
	movl	%r13d, %r15d
	xorl	%r8d, %r8d
	movq	(%rcx), %rcx
	movl	%r13d, %esi
	andl	$-2, %r15d
	movq	%r12, %rdi
	addl	$2, %r15d
	testl	%eax, %eax
	setne	%r8b
	movl	%r15d, %edx
	call	_ZN2v88internal8compiler17TopLevelLiveRange14AddUseIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb
	movq	(%rbx), %rax
	movq	(%rax), %rdi
	movl	488(%rax), %edx
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	shrl	$2, %edx
	andl	$1, %edx
	subq	%rsi, %rax
	cmpq	$31, %rax
	jbe	.L2889
	leaq	32(%rsi), %rax
	movq	%rax, 16(%rdi)
.L2876:
	movq	$0, 16(%rsi)
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movl	%r15d, 24(%rsi)
	movl	$2080, 28(%rsi)
	movups	%xmm0, (%rsi)
	call	_ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb
	movl	(%r14), %eax
	andl	$7, %eax
	cmpl	$1, %eax
	jne	.L2879
.L2888:
	movl	%r13d, %esi
	movq	%r14, %rdx
	movq	%rbx, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler16LiveRangeBuilder14NewUsePositionENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movl	488(%rax), %edx
	shrl	$2, %edx
	andl	$1, %edx
	call	_ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb
.L2870:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2889:
	.cfi_restore_state
	movl	$32, %esi
	movl	%edx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %edx
	movq	%rax, %rsi
	jmp	.L2876
	.p2align 4,,10
	.p2align 3
.L2887:
	movl	88(%r12), %esi
	movl	%r13d, %edx
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	16(%r12), %rdx
	jmp	.L2878
	.cfi_endproc
.LFE32009:
	.size	_ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE.constprop.0, .-_ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE.constprop.0
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilder19ProcessInstructionsEPKNS1_16InstructionBlockEPNS0_9BitVectorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16LiveRangeBuilder19ProcessInstructionsEPKNS1_16InstructionBlockEPNS0_9BitVectorE
	.type	_ZN2v88internal8compiler16LiveRangeBuilder19ProcessInstructionsEPKNS1_16InstructionBlockEPNS0_9BitVectorE, @function
_ZN2v88internal8compiler16LiveRangeBuilder19ProcessInstructionsEPKNS1_16InstructionBlockEPNS0_9BitVectorE:
.LFB23431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -144(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	112(%rsi), %eax
	movl	%eax, -160(%rbp)
	sall	$2, %eax
	movl	%eax, -84(%rbp)
	movq	(%rdi), %rax
	testb	$1, 488(%rax)
	je	.L2891
	movzbl	120(%rsi), %r12d
.L2891:
	movq	-144(%rbp), %rbx
	movl	116(%rbx), %ebx
	movl	%ebx, -68(%rbp)
	subl	$1, %ebx
	movl	%ebx, -156(%rbp)
	cmpl	-160(%rbp), %ebx
	jl	.L2890
	movslq	%ebx, %rdi
	leal	3(,%rbx,4), %ebx
	movl	%r12d, -68(%rbp)
	movq	%r14, %r12
	movl	%ebx, -88(%rbp)
	leaq	-64(%rbp), %rbx
	movq	%rbx, -192(%rbp)
	leaq	-56(%rbp), %rbx
	movq	%rbx, -184(%rbp)
	leaq	24(%r14), %rbx
	movq	%rdi, -176(%rbp)
	movq	%rbx, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L2976:
	movl	-156(%rbp), %ebx
	movq	16(%rax), %rdx
	sall	$2, %ebx
	movq	208(%rdx), %rcx
	movl	%ebx, -72(%rbp)
	movl	-88(%rbp), %ebx
	movq	%rcx, %rax
	subq	216(%rdx), %rax
	subl	$1, %ebx
	sarq	$3, %rax
	movl	%ebx, -96(%rbp)
	movq	-176(%rbp), %rbx
	addq	%rbx, %rax
	js	.L2893
	cmpq	$63, %rax
	jg	.L2894
	leaq	(%rcx,%rbx,8), %rax
.L2895:
	movq	(%rax), %rdi
	movl	4(%rdi), %eax
	movq	%rdi, -80(%rbp)
	testb	%al, %al
	je	.L2897
	movl	-160(%rbp), %ebx
	cmpl	%ebx, -156(%rbp)
	leaq	40(%rdi), %r14
	sete	%bl
	xorl	%r13d, %r13d
	movq	%r14, %r15
	movb	%bl, -104(%rbp)
	movq	%r13, %r14
	movl	-68(%rbp), %ebx
	movl	-96(%rbp), %r13d
	jmp	.L2905
	.p2align 4,,10
	.p2align 3
.L2898:
	cmpl	$2, %eax
	je	.L3050
.L2900:
	movq	-144(%rbp), %rax
	cmpb	$0, 121(%rax)
	je	.L2902
	cmpb	$0, -104(%rbp)
	jne	.L3051
.L2902:
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE.constprop.0
.L2904:
	movq	-80(%rbp), %rax
	addq	$1, %r14
	addq	$8, %r15
	movl	4(%rax), %eax
	movzbl	%al, %ecx
	cmpq	%r14, %rcx
	jbe	.L2897
.L2905:
	movq	(%r15), %rcx
	movl	%ecx, %eax
	andl	$7, %eax
	cmpl	$1, %eax
	jne	.L2898
.L3050:
	movq	-112(%rbp), %rax
	shrq	$3, %rcx
	cmpl	$1, 4(%rax)
	movq	8(%rax), %rsi
	je	.L3052
	testl	%ecx, %ecx
	leal	63(%rcx), %eax
	movl	%ecx, %edi
	cmovns	%ecx, %eax
	sarl	$31, %edi
	shrl	$26, %edi
	addl	%edi, %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%edi, %ecx
	movl	$1, %edi
	salq	%cl, %rdi
	movq	%rdi, %rcx
	notq	%rcx
	andq	%rcx, (%rsi,%rax,8)
	jmp	.L2900
	.p2align 4,,10
	.p2align 3
.L3052:
	movl	$1, %eax
	salq	%cl, %rax
	notq	%rax
	andq	%rax, %rsi
	movq	-112(%rbp), %rax
	movq	%rsi, 8(%rax)
	jmp	.L2900
	.p2align 4,,10
	.p2align 3
.L3051:
	movq	(%r15), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$5, %ecx
	jne	.L2902
	testb	$24, %al
	jne	.L2902
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	ja	.L2902
	shrq	$35, %rax
	jne	.L2902
	movl	-72(%rbp), %esi
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE.constprop.0
	jmp	.L2904
	.p2align 4,,10
	.p2align 3
.L2897:
	testl	$1073741824, %eax
	jne	.L3053
.L2906:
	movq	-80(%rbp), %r13
	xorl	%ebx, %ebx
	testl	$16776960, %eax
	jne	.L2917
	jmp	.L2933
	.p2align 4,,10
	.p2align 3
.L2925:
	movl	-68(%rbp), %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16LiveRangeBuilder12LiveRangeForEPNS1_18InstructionOperandENS1_22RegisterAllocationData9SpillModeE
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L2929
	movl	(%r14), %eax
	movq	(%r12), %rdx
	andl	$7, %eax
	movq	(%rdx), %rcx
	cmpl	$1, %eax
	je	.L3054
.L2930:
	movl	488(%rdx), %r8d
	movl	-84(%rbp), %esi
	movl	%r15d, %edx
	movq	%r10, %rdi
	shrl	$2, %r8d
	andl	$1, %r8d
	call	_ZN2v88internal8compiler17TopLevelLiveRange14AddUseIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb
.L2929:
	movl	4(%r13), %eax
.L2920:
	movl	%eax, %edx
	addq	$1, %rbx
	shrl	$8, %edx
	movzwl	%dx, %edx
	cmpq	%rdx, %rbx
	jnb	.L2933
.L2917:
	movzbl	%al, %edx
	leaq	5(%rbx,%rdx), %rdx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %rsi
	movl	%esi, %edx
	andl	$7, %edx
	leal	-3(%rdx), %ecx
	cmpl	$1, %ecx
	jbe	.L2920
	movl	-88(%rbp), %r15d
	cmpl	$1, %edx
	jne	.L2925
	movq	%rsi, %rax
	movl	-96(%rbp), %r15d
	shrq	$39, %rax
	testb	$1, %al
	movq	-112(%rbp), %rax
	cmovne	-88(%rbp), %r15d
	shrq	$3, %rsi
	cmpl	$1, 4(%rax)
	movq	8(%rax), %rdx
	je	.L3055
	testl	%esi, %esi
	leal	63(%rsi), %eax
	movl	%esi, %edi
	cmovns	%esi, %eax
	sarl	$31, %edi
	shrl	$26, %edi
	leal	(%rdi,%rsi), %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%edi, %ecx
	movl	$1, %edi
	salq	%cl, %rdi
	orq	%rdi, (%rdx,%rax,8)
.L2924:
	movabsq	$34359738368, %rdi
	movq	(%r14), %rax
	testq	%rdi, %rax
	je	.L2925
	shrq	$36, %rax
	andl	$7, %eax
	cmpl	$6, %eax
	jne	.L2925
	movq	(%r12), %rdi
	testb	$1, 488(%rdi)
	je	.L2926
	call	_ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi
	movq	-144(%rbp), %rdi
	movl	4(%rax), %ecx
	cmpb	$1, 120(%rdi)
	sbbl	%edx, %edx
	andl	$2, %edx
	addl	$2, %edx
	cmpb	$1, 120(%rdi)
	movl	%ecx, %edi
	sbbl	%esi, %esi
	shrl	%edi
	notl	%esi
	andl	$3, %edi
	addl	$2, %esi
	leal	(%rdi,%rdi), %r8d
	cmpl	%esi, %edi
	cmovge	%r8d, %edx
	andl	$-7, %ecx
	orl	%ecx, %edx
	movl	%edx, 4(%rax)
	jmp	.L2925
	.p2align 4,,10
	.p2align 3
.L2933:
	movabsq	$34359738368, %r13
	movl	-68(%rbp), %r15d
	xorl	%ebx, %ebx
	testl	$1056964608, %eax
	jne	.L2918
	jmp	.L2941
	.p2align 4,,10
	.p2align 3
.L3057:
	testb	$24, %dl
	jne	.L2934
	shrq	$5, %rdx
	cmpb	$11, %dl
	jbe	.L2936
	.p2align 4,,10
	.p2align 3
.L2934:
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16LiveRangeBuilder12LiveRangeForEPNS1_18InstructionOperandENS1_22RegisterAllocationData9SpillModeE
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L2937
	movl	(%r14), %eax
	movq	(%r12), %rdx
	andl	$7, %eax
	movq	(%rdx), %rcx
	cmpl	$1, %eax
	je	.L3056
.L2938:
	movl	488(%rdx), %r8d
	movl	-84(%rbp), %esi
	movq	%r10, %rdi
	movl	-88(%rbp), %edx
	shrl	$2, %r8d
	andl	$1, %r8d
	call	_ZN2v88internal8compiler17TopLevelLiveRange14AddUseIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb
.L2937:
	movl	-96(%rbp), %esi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE.constprop.0
	movq	-80(%rbp), %rax
	movl	4(%rax), %eax
.L2936:
	movl	%eax, %edx
	addq	$1, %rbx
	shrl	$24, %edx
	andl	$63, %edx
	cmpq	%rdx, %rbx
	jnb	.L2941
.L2918:
	movl	%eax, %ecx
	movzbl	%al, %edx
	movq	-80(%rbp), %rdi
	shrl	$8, %ecx
	leaq	5(%rbx,%rdx), %rdx
	movzwl	%cx, %ecx
	addq	%rcx, %rdx
	leaq	(%rdi,%rdx,8), %r14
	testl	$1073741824, %eax
	je	.L2934
	movq	(%r14), %rdx
	testb	$4, %dl
	jne	.L3057
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpl	$1, %ecx
	jne	.L2934
	testq	%r13, %rdx
	je	.L2936
	shrq	$36, %rdx
	andl	$7, %edx
	subl	$3, %edx
	cmpl	$1, %edx
	jbe	.L2936
	jmp	.L2934
	.p2align 4,,10
	.p2align 3
.L3056:
	movq	16(%rcx), %r11
	movq	24(%rcx), %rax
	subq	%r11, %rax
	cmpq	$31, %rax
	jbe	.L3058
	leaq	32(%r11), %rax
	movq	%rax, 16(%rcx)
.L2940:
	movl	-88(%rbp), %esi
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal8compiler11UsePositionC1ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE
	movq	(%r12), %rax
	movq	-104(%rbp), %r10
	movq	%r11, %rsi
	movl	488(%rax), %edx
	movq	%r10, %rdi
	shrl	$2, %edx
	andl	$1, %edx
	call	_ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb
	movq	(%r12), %rdx
	movq	-104(%rbp), %r10
	movq	(%rdx), %rcx
	jmp	.L2938
	.p2align 4,,10
	.p2align 3
.L2941:
	movq	-192(%rbp), %rax
	movq	$1, -64(%rbp)
	movq	%rax, -136(%rbp)
.L2919:
	movq	-136(%rbp), %rax
	movq	-80(%rbp), %rbx
	movslq	(%rax), %rax
	movq	%rax, %rdx
	movq	8(%rbx,%rax,8), %rax
	testq	%rax, %rax
	je	.L2942
	andl	$-2, -72(%rbp)
	cmpl	$1, %edx
	movl	-72(%rbp), %ebx
	sete	%dl
	movzbl	%dl, %edx
	addl	%edx, %ebx
	movq	8(%rax), %rdx
	movq	16(%rax), %rax
	movl	%ebx, -72(%rbp)
	movq	%rax, -120(%rbp)
	cmpq	%rdx, %rax
	je	.L2942
	movq	%rdx, %rbx
	.p2align 4,,10
	.p2align 3
.L2975:
	movq	(%rbx), %r14
	movq	8(%r14), %r15
	leaq	8(%r14), %rax
	movq	%rax, -104(%rbp)
	movl	%r15d, %eax
	andl	$7, %eax
	cmpl	$4, %eax
	ja	.L2944
	testb	$6, %r15b
	jne	.L2985
	cmpl	$1, %eax
	jne	.L2947
	shrq	$3, %r15
	movq	(%r12), %rdi
	movl	%r15d, %esi
	movl	%r15d, %r13d
	call	_ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi
	movl	4(%rax), %edx
	testb	$8, %dl
	je	.L2948
	andl	$16, %edx
	je	.L2949
	movq	64(%rax), %rax
	movq	$0, -96(%rbp)
	cmpq	$1, %rax
	movq	%rax, -104(%rbp)
	sbbl	%eax, %eax
	notl	%eax
	andl	$2, %eax
	movb	%al, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L2950:
	movl	-68(%rbp), %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16LiveRangeBuilder12LiveRangeForEPNS1_18InstructionOperandENS1_22RegisterAllocationData9SpillModeE
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L2963
	movl	(%r14), %edx
	movq	(%r12), %rsi
	xorl	%r11d, %r11d
	xorl	%r15d, %r15d
	andl	$7, %edx
	movq	(%rsi), %rcx
	cmpl	$1, %edx
	je	.L3059
.L2964:
	movl	488(%rsi), %r8d
	movl	-72(%rbp), %edx
	movq	%r9, %rdi
	movb	%r11b, -104(%rbp)
	movl	-84(%rbp), %esi
	shrl	$2, %r8d
	andl	$1, %r8d
	call	_ZN2v88internal8compiler17TopLevelLiveRange14AddUseIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb
	movq	(%r14), %rdx
	movzbl	-104(%rbp), %r11d
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpl	$1, %ecx
	je	.L2977
.L2967:
	testb	%r11b, %r11b
	je	.L2969
	movq	-96(%rbp), %rax
	movl	28(%rax), %edx
	movl	%edx, %ecx
	shrl	$2, %ecx
	andl	$7, %ecx
	cmpb	$4, %cl
	jne	.L2970
	andl	$-29, %edx
	movq	%r15, 8(%rax)
	orl	$8, %edx
	movl	%edx, 28(%rax)
.L2970:
	movl	28(%r15), %edx
	movl	%edx, %ecx
	shrl	$2, %ecx
	andl	$7, %ecx
	cmpb	$4, %cl
	jne	.L2969
	movq	-96(%rbp), %rax
	andl	$-29, %edx
	orl	$8, %edx
	movq	%rax, 8(%r15)
	movl	%edx, 28(%r15)
.L2969:
	cmpl	$-1, %r13d
	je	.L2962
	movq	32(%r12), %rdx
	testq	%rdx, %rdx
	je	.L2962
	movq	-152(%rbp), %rcx
	jmp	.L2971
	.p2align 4,,10
	.p2align 3
.L3060:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L2972
.L2971:
	cmpq	%r14, 32(%rdx)
	jnb	.L3060
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L2971
.L2972:
	cmpq	-152(%rbp), %rcx
	je	.L2962
	cmpq	%r14, 32(%rcx)
	ja	.L2962
	movq	40(%rcx), %rsi
	movl	28(%rsi), %edx
	movl	%edx, %ecx
	shrl	$2, %ecx
	andl	$7, %ecx
	cmpb	$4, %cl
	jne	.L2962
	andl	$-29, %edx
	movq	%r15, 8(%rsi)
	orl	$8, %edx
	movl	%edx, 28(%rsi)
	.p2align 4,,10
	.p2align 3
.L2962:
	addq	$8, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L2975
.L2942:
	addq	$4, -136(%rbp)
	movq	-136(%rbp), %rax
	cmpq	-184(%rbp), %rax
	jne	.L2919
	subl	$1, -156(%rbp)
	movl	-156(%rbp), %eax
	subq	$1, -176(%rbp)
	subl	$4, -88(%rbp)
	cmpl	-160(%rbp), %eax
	jl	.L2890
	movq	(%r12), %rax
	jmp	.L2976
	.p2align 4,,10
	.p2align 3
.L2944:
	cmpl	$5, %eax
	jne	.L2947
	andl	$24, %r15d
	sete	-128(%rbp)
.L2945:
	movl	-68(%rbp), %ecx
	movl	-72(%rbp), %esi
	leaq	8(%r14), %rdx
	movq	%r12, %rdi
	movl	$-1, %r13d
	call	_ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE.constprop.0
	movq	$0, -96(%rbp)
	jmp	.L2950
	.p2align 4,,10
	.p2align 3
.L2985:
	movb	$0, -128(%rbp)
	jmp	.L2945
	.p2align 4,,10
	.p2align 3
.L2963:
	movq	(%r14), %rdx
	xorl	%r15d, %r15d
	movl	%edx, %eax
	andl	$7, %eax
	cmpl	$1, %eax
	jne	.L2969
	xorl	%r11d, %r11d
	.p2align 4,,10
	.p2align 3
.L2977:
	movq	-112(%rbp), %rax
	shrq	$3, %rdx
	cmpl	$1, 4(%rax)
	movq	8(%rax), %rdi
	je	.L3061
	testl	%edx, %edx
	leal	63(%rdx), %esi
	movl	%edx, %r8d
	cmovns	%edx, %esi
	sarl	$31, %r8d
	shrl	$26, %r8d
	leal	(%r8,%rdx), %ecx
	sarl	$6, %esi
	movl	$1, %edx
	andl	$63, %ecx
	movslq	%esi, %rsi
	subl	%r8d, %ecx
	salq	%cl, %rdx
	orq	%rdx, (%rdi,%rsi,8)
	jmp	.L2967
	.p2align 4,,10
	.p2align 3
.L2948:
	movq	-112(%rbp), %rax
	cmpl	$1, 4(%rax)
	movq	8(%rax), %rdx
	je	.L2957
	testl	%r15d, %r15d
	leal	63(%r15), %eax
	cmovns	%r15d, %eax
	sarl	$6, %eax
	cltq
	movq	(%rdx,%rax,8), %rdx
.L2957:
	movl	%r15d, %eax
	sarl	$31, %eax
	shrl	$26, %eax
	leal	(%rax,%r15), %r13d
	andl	$63, %r13d
	subl	%eax, %r13d
	btq	%r13, %rdx
	jnc	.L2958
	movq	(%r14), %rdx
	movl	%edx, %eax
	andl	$7, %eax
	cmpl	$4, %eax
	ja	.L2959
	andl	$6, %edx
	jne	.L2988
	cmpl	$1, %eax
	jne	.L2947
	movl	$4, %r8d
.L2960:
	movl	-68(%rbp), %r9d
	movl	-72(%rbp), %esi
	movq	%r12, %rdi
	movq	%r14, %rcx
	leaq	8(%r14), %rdx
	call	_ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE
	movq	%rax, -96(%rbp)
	movq	-112(%rbp), %rax
	cmpl	$1, 4(%rax)
	movq	%rax, %rdi
	je	.L3062
	testl	%r15d, %r15d
	leal	63(%r15), %eax
	movl	%r13d, %ecx
	movq	8(%rdi), %rsi
	cmovs	%eax, %r15d
	movl	$1, %edx
	movb	$4, -128(%rbp)
	movl	$-1, %r13d
	salq	%cl, %rdx
	sarl	$6, %r15d
	notq	%rdx
	movslq	%r15d, %rax
	andq	%rdx, (%rsi,%rax,8)
	jmp	.L2950
	.p2align 4,,10
	.p2align 3
.L3059:
	movq	16(%rcx), %rax
	movq	24(%rcx), %rdx
	subq	%rax, %rdx
	cmpq	$31, %rdx
	jbe	.L3063
	leaq	32(%rax), %rdx
	movq	%rdx, 16(%rcx)
.L2966:
	movq	-104(%rbp), %rcx
	movzbl	-128(%rbp), %r8d
	movq	%rax, %rdi
	movq	%r14, %rdx
	movl	-72(%rbp), %esi
	movq	%rax, %r15
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal8compiler11UsePositionC1ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE
	movq	(%r12), %rdx
	movq	-168(%rbp), %r9
	movq	%r15, %rsi
	movl	488(%rdx), %edx
	movq	%r9, %rdi
	movq	%r9, -104(%rbp)
	shrl	$2, %edx
	andl	$1, %edx
	call	_ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb
	testq	%r15, %r15
	movq	(%r12), %rsi
	movq	-104(%rbp), %r9
	setne	%r11b
	cmpq	$0, -96(%rbp)
	setne	%dl
	movq	(%rsi), %rcx
	andl	%edx, %r11d
	jmp	.L2964
	.p2align 4,,10
	.p2align 3
.L3061:
	btsq	%rdx, %rdi
	movq	%rdi, 8(%rax)
	jmp	.L2967
	.p2align 4,,10
	.p2align 3
.L3054:
	movq	16(%rcx), %r11
	movq	24(%rcx), %rax
	subq	%r11, %rax
	cmpq	$31, %rax
	jbe	.L3064
	leaq	32(%r11), %rax
	movq	%rax, 16(%rcx)
.L2932:
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movl	%r15d, %esi
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal8compiler11UsePositionC1ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE
	movq	(%r12), %rax
	movq	-104(%rbp), %r10
	movq	%r11, %rsi
	movl	488(%rax), %edx
	movq	%r10, %rdi
	shrl	$2, %edx
	andl	$1, %edx
	call	_ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb
	movq	(%r12), %rdx
	movq	-104(%rbp), %r10
	movq	(%rdx), %rcx
	jmp	.L2930
	.p2align 4,,10
	.p2align 3
.L2959:
	cmpl	$5, %eax
	jne	.L2947
	andl	$24, %edx
	sete	%r8b
	jmp	.L2960
	.p2align 4,,10
	.p2align 3
.L2958:
	movq	$0, 8(%r14)
	addq	$8, %rbx
	movq	$0, (%r14)
	cmpq	%rbx, -120(%rbp)
	jne	.L2975
	jmp	.L2942
	.p2align 4,,10
	.p2align 3
.L3055:
	btsq	%rsi, %rdx
	movq	%rdx, 8(%rax)
	jmp	.L2924
	.p2align 4,,10
	.p2align 3
.L2949:
	movq	(%r12), %rax
	leaq	56(%rax), %rcx
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L2951
	movq	%rcx, %rdx
	jmp	.L2952
	.p2align 4,,10
	.p2align 3
.L3065:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2953
.L2952:
	cmpl	32(%rax), %r13d
	jle	.L3065
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L2952
.L2953:
	cmpq	%rdx, %rcx
	je	.L2951
	cmpl	%r15d, 32(%rdx)
	cmovle	%rdx, %rcx
.L2951:
	movq	40(%rcx), %rax
	movq	$0, -96(%rbp)
	movb	$3, -128(%rbp)
	movq	%rax, -104(%rbp)
	jmp	.L2950
	.p2align 4,,10
	.p2align 3
.L2894:
	movq	%rax, %rcx
	sarq	$6, %rcx
.L2896:
	movq	232(%rdx), %rdx
	movq	%rcx, %rsi
	salq	$6, %rsi
	movq	(%rdx,%rcx,8), %rdx
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %rax
	jmp	.L2895
	.p2align 4,,10
	.p2align 3
.L3062:
	movl	$1, %eax
	movl	%r15d, %ecx
	movb	$4, -128(%rbp)
	movl	$-1, %r13d
	salq	%cl, %rax
	notq	%rax
	andq	%rax, 8(%rdi)
	jmp	.L2950
	.p2align 4,,10
	.p2align 3
.L2893:
	movq	%rax, %rcx
	notq	%rcx
	shrq	$6, %rcx
	notq	%rcx
	jmp	.L2896
	.p2align 4,,10
	.p2align 3
.L2988:
	xorl	%r8d, %r8d
	jmp	.L2960
	.p2align 4,,10
	.p2align 3
.L3053:
	movq	(%r12), %rbx
	movq	32(%rbx), %rdx
	movl	24(%rdx), %r8d
	testl	%r8d, %r8d
	jle	.L2907
	movl	-68(%rbp), %r14d
	movl	-96(%rbp), %r15d
	xorl	%r13d, %r13d
	movl	-88(%rbp), %ebx
	.p2align 4,,10
	.p2align 3
.L2908:
	movq	56(%rdx), %rax
	movq	%r12, %rdi
	movl	%r14d, %edx
	movl	(%rax,%r13,4), %esi
	addq	$1, %r13
	call	_ZN2v88internal8compiler16LiveRangeBuilder17FixedLiveRangeForEiNS1_22RegisterAllocationData9SpillModeE
	movl	%ebx, %edx
	movl	%r15d, %esi
	movq	%rax, %rdi
	movq	(%r12), %rax
	movl	488(%rax), %r8d
	movq	(%rax), %rcx
	shrl	$2, %r8d
	andl	$1, %r8d
	call	_ZN2v88internal8compiler17TopLevelLiveRange14AddUseIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb
	movq	(%r12), %rax
	movq	32(%rax), %rdx
	cmpl	%r13d, 24(%rdx)
	jg	.L2908
	movq	%rax, %rbx
	movq	-80(%rbp), %rax
	movl	4(%rax), %eax
	testl	$1073741824, %eax
	je	.L2906
.L2907:
	movq	32(%rbx), %rcx
	movl	32(%rcx), %edi
	testl	%edi, %edi
	jle	.L2906
	xorl	%r13d, %r13d
	movl	-96(%rbp), %r15d
	movq	%r13, %r14
	movl	-88(%rbp), %r13d
	jmp	.L2916
	.p2align 4,,10
	.p2align 3
.L2910:
	movl	488(%rbx), %r8d
	movq	(%rbx), %rcx
	movl	%r13d, %edx
	movl	%r15d, %esi
	addq	$1, %r14
	shrl	$2, %r8d
	andl	$1, %r8d
	call	_ZN2v88internal8compiler17TopLevelLiveRange14AddUseIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb
	movq	(%r12), %rbx
	movq	32(%rbx), %rcx
	cmpl	%r14d, 32(%rcx)
	jle	.L3066
.L2916:
	movq	192(%rcx), %rax
	movl	-68(%rbp), %esi
	movl	(%rax,%r14,4), %edx
	movl	16(%rcx), %eax
	addl	%edx, %eax
	testl	%esi, %esi
	movq	264(%rbx), %rsi
	cmove	%edx, %eax
	movslq	%eax, %r8
	movq	(%rsi,%r8,8), %rdi
	testq	%rdi, %rdi
	jne	.L2910
	movl	8(%rcx), %ecx
	movq	(%rbx), %rdi
	notl	%eax
	addl	%ecx, %ecx
	movq	24(%rdi), %rsi
	subl	%ecx, %eax
	movl	%eax, %ecx
	movq	16(%rdi), %rax
	subq	%rax, %rsi
	cmpq	$159, %rsi
	jbe	.L3067
	leaq	160(%rax), %rsi
	movq	%rsi, 16(%rdi)
.L2912:
	movl	%ecx, 88(%rax)
	movl	%edx, %ecx
	pxor	%xmm0, %xmm0
	movq	%rax, %rdi
	sall	$7, %ecx
	movl	$0, (%rax)
	orl	$134324224, %ecx
	movq	$0, 24(%rax)
	movq	%rax, 32(%rax)
	movl	$0, 92(%rax)
	movq	$0, 112(%rax)
	movb	$0, 120(%rax)
	movl	$2147483647, 124(%rax)
	movq	$0, 128(%rax)
	movq	%rax, 136(%rax)
	movq	$0, 144(%rax)
	movb	$0, 152(%rax)
	movl	%ecx, 4(%rax)
	movups	%xmm0, 8(%rax)
	movups	%xmm0, 40(%rax)
	movups	%xmm0, 56(%rax)
	movups	%xmm0, 72(%rax)
	movups	%xmm0, 96(%rax)
	movq	(%r12), %rcx
	movq	392(%rcx), %r10
	cmpl	$1, 4(%r10)
	je	.L3068
	testl	%edx, %edx
	leal	63(%rdx), %esi
	movl	%edx, %r11d
	movq	8(%r10), %r10
	cmovns	%edx, %esi
	sarl	$31, %r11d
	shrl	$26, %r11d
	leal	(%rdx,%r11), %ecx
	sarl	$6, %esi
	movl	$1, %edx
	andl	$63, %ecx
	movslq	%esi, %rsi
	subl	%r11d, %ecx
	salq	%cl, %rdx
	orq	%rdx, (%r10,%rsi,8)
.L2914:
	cmpl	$1, -68(%rbp)
	jne	.L2915
	orl	$268435456, 4(%rax)
.L2915:
	movq	264(%rbx), %rdx
	movq	%rax, (%rdx,%r8,8)
	movq	(%r12), %rbx
	jmp	.L2910
	.p2align 4,,10
	.p2align 3
.L3066:
	movq	-80(%rbp), %rax
	movl	4(%rax), %eax
	jmp	.L2906
	.p2align 4,,10
	.p2align 3
.L3063:
	movl	$32, %esi
	movq	%rcx, %rdi
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-168(%rbp), %r9
	jmp	.L2966
	.p2align 4,,10
	.p2align 3
.L2926:
	call	_ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi
	movl	$2, %esi
	movl	4(%rax), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$3, %edx
	cmpl	$2, %edx
	cmovb	%esi, %edx
	andl	$-7, %ecx
	addl	%edx, %edx
	orl	%ecx, %edx
	movl	%edx, 4(%rax)
	jmp	.L2925
	.p2align 4,,10
	.p2align 3
.L3068:
	movl	$1, %esi
	movl	%edx, %ecx
	salq	%cl, %rsi
	orq	%rsi, 8(%r10)
	jmp	.L2914
	.p2align 4,,10
	.p2align 3
.L2890:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3069
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3058:
	.cfi_restore_state
	movl	$32, %esi
	movq	%rcx, %rdi
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %r10
	movq	%rax, %r11
	jmp	.L2940
.L3064:
	movl	$32, %esi
	movq	%rcx, %rdi
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %r10
	movq	%rax, %r11
	jmp	.L2932
.L3067:
	movl	$160, %esi
	movq	%r8, -128(%rbp)
	movl	%ecx, -120(%rbp)
	movl	%edx, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-104(%rbp), %edx
	movl	-120(%rbp), %ecx
	movq	-128(%rbp), %r8
	jmp	.L2912
.L2947:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3069:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23431:
	.size	_ZN2v88internal8compiler16LiveRangeBuilder19ProcessInstructionsEPKNS1_16InstructionBlockEPNS0_9BitVectorE, .-_ZN2v88internal8compiler16LiveRangeBuilder19ProcessInstructionsEPKNS1_16InstructionBlockEPNS0_9BitVectorE
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilder17ProcessLoopHeaderEPKNS1_16InstructionBlockEPNS0_9BitVectorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16LiveRangeBuilder17ProcessLoopHeaderEPKNS1_16InstructionBlockEPNS0_9BitVectorE
	.type	_ZN2v88internal8compiler16LiveRangeBuilder17ProcessLoopHeaderEPKNS1_16InstructionBlockEPNS0_9BitVectorE, @function
_ZN2v88internal8compiler16LiveRangeBuilder17ProcessLoopHeaderEPKNS1_16InstructionBlockEPNS0_9BitVectorE:
.LFB23433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 4(%rdx)
	movq	8(%rdx), %rax
	movq	%rdx, -96(%rbp)
	movl	$0, -88(%rbp)
	je	.L3072
	movq	(%rax), %rax
.L3072:
	leaq	-96(%rbp), %r15
	movq	%rax, -80(%rbp)
	movq	%r15, %rdi
	movl	$-1, -72(%rbp)
	call	_ZN2v88internal9BitVector8Iterator7AdvanceEv@PLT
	movl	112(%r13), %eax
	movq	(%r14), %rdi
	movslq	108(%r13), %rsi
	sall	$2, %eax
	movl	%eax, -100(%rbp)
	movq	16(%rdi), %rax
	movq	%rsi, %r8
	subq	$1, %rsi
	movq	16(%rax), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L3105
	movq	(%rax,%rsi,8), %rax
	movl	116(%rax), %ebx
	movq	-96(%rbp), %rax
	sall	$2, %ebx
	movl	4(%rax), %eax
	cmpl	%eax, -88(%rbp)
	jge	.L3074
	.p2align 4,,10
	.p2align 3
.L3086:
	movl	-72(%rbp), %esi
	call	_ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi
	movq	%rax, %r8
	movq	(%r14), %rax
	movq	(%rax), %r10
	testb	$4, 488(%rax)
	jne	.L3106
.L3075:
	movq	16(%r8), %rax
	movl	%ebx, %edx
	testq	%rax, %rax
	jne	.L3083
	jmp	.L3076
	.p2align 4,,10
	.p2align 3
.L3107:
	movl	4(%rax), %ecx
	movq	8(%rax), %rax
	cmpl	%ecx, %ebx
	movq	%rax, 16(%r8)
	cmovl	%ecx, %edx
	testq	%rax, %rax
	je	.L3076
.L3083:
	cmpl	(%rax), %ebx
	jge	.L3107
.L3076:
	movq	16(%r10), %rax
	movq	24(%r10), %rcx
	subq	%rax, %rcx
	cmpq	$15, %rcx
	jbe	.L3108
	leaq	16(%rax), %rcx
	movq	%rcx, 16(%r10)
.L3080:
	movl	-100(%rbp), %esi
	movq	$0, 8(%rax)
	movl	%edx, 4(%rax)
	movl	%esi, (%rax)
	movq	16(%r8), %rdx
	movq	%rdx, 8(%rax)
	movq	%rax, 16(%r8)
	cmpq	$0, 8(%rax)
	je	.L3109
.L3102:
	movq	%r15, %rdi
	call	_ZN2v88internal9BitVector8Iterator7AdvanceEv@PLT
	movq	-96(%rbp), %rax
	movl	4(%rax), %eax
	cmpl	%eax, -88(%rbp)
	jge	.L3110
	movq	(%r14), %rdi
	jmp	.L3086
	.p2align 4,,10
	.p2align 3
.L3106:
	movl	88(%r8), %esi
	movl	-100(%rbp), %edx
	movl	%ebx, %ecx
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	movq	%r10, -120(%rbp)
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-120(%rbp), %r10
	movq	-112(%rbp), %r8
	jmp	.L3075
	.p2align 4,,10
	.p2align 3
.L3109:
	movq	%rax, 8(%r8)
	jmp	.L3102
	.p2align 4,,10
	.p2align 3
.L3110:
	movl	108(%r13), %r8d
.L3074:
	movl	100(%r13), %eax
	leal	1(%rax), %ecx
	movslq	%ecx, %rdi
	salq	$3, %rdi
	cmpl	%r8d, %ecx
	jge	.L3070
	.p2align 4,,10
	.p2align 3
.L3091:
	movq	(%r14), %rax
	movq	104(%rax), %rax
	movq	(%rax,%rdi), %rdx
	movl	4(%rdx), %eax
	cmpl	$1, %eax
	je	.L3111
	testl	%eax, %eax
	jle	.L3089
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L3090:
	movq	8(%r12), %r8
	movq	8(%rdx), %rsi
	movq	(%r8,%rax,8), %r8
	orq	%r8, (%rsi,%rax,8)
	addq	$1, %rax
	cmpl	%eax, 4(%rdx)
	jg	.L3090
	movl	108(%r13), %r8d
.L3089:
	addl	$1, %ecx
	addq	$8, %rdi
	cmpl	%r8d, %ecx
	jl	.L3091
.L3070:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3112
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3111:
	.cfi_restore_state
	movq	8(%r12), %rax
	orq	%rax, 8(%rdx)
	movl	108(%r13), %r8d
	jmp	.L3089
	.p2align 4,,10
	.p2align 3
.L3108:
	movl	$16, %esi
	movq	%r10, %rdi
	movl	%edx, -120(%rbp)
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %r8
	movl	-120(%rbp), %edx
	jmp	.L3080
.L3105:
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L3112:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23433:
	.size	_ZN2v88internal8compiler16LiveRangeBuilder17ProcessLoopHeaderEPKNS1_16InstructionBlockEPNS0_9BitVectorE, .-_ZN2v88internal8compiler16LiveRangeBuilder17ProcessLoopHeaderEPKNS1_16InstructionBlockEPNS0_9BitVectorE
	.section	.text._ZN2v88internal8compiler17ConstraintBuilder20MeetConstraintsAfterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17ConstraintBuilder20MeetConstraintsAfterEi
	.type	_ZN2v88internal8compiler17ConstraintBuilder20MeetConstraintsAfterEi, @function
_ZN2v88internal8compiler17ConstraintBuilder20MeetConstraintsAfterEi:
.LFB23409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -76(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	16(%rax), %rdx
	movq	208(%rdx), %rsi
	movq	%rsi, %rax
	subq	216(%rdx), %rax
	sarq	$3, %rax
	addq	%rcx, %rax
	js	.L3114
	cmpq	$63, %rax
	jle	.L3193
	movq	%rax, %rcx
	sarq	$6, %rcx
.L3117:
	movq	232(%rdx), %rdx
	movq	%rcx, %rsi
	salq	$6, %rsi
	movq	(%rdx,%rcx,8), %rdx
	subq	%rsi, %rax
	leaq	(%rdx,%rax,8), %rax
.L3116:
	movq	(%rax), %r14
	movl	-76(%rbp), %r15d
	xorl	%r12d, %r12d
	movabsq	$34359738368, %rbx
	movl	4(%r14), %edx
	movzbl	%dl, %ecx
	testl	$1056964608, %edx
	je	.L3122
	.p2align 4,,10
	.p2align 3
.L3118:
	movl	%edx, %esi
	movl	%ecx, %eax
	shrl	$8, %esi
	leaq	5(%r12,%rax), %rax
	movzwl	%si, %esi
	addq	%rsi, %rax
	leaq	(%r14,%rax,8), %rsi
	movq	(%rsi), %rax
	testq	%rbx, %rax
	je	.L3120
	shrq	$36, %rax
	andl	$7, %eax
	subl	$3, %eax
	cmpl	$1, %eax
	ja	.L3121
.L3120:
	xorl	%ecx, %ecx
	movl	%r15d, %edx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler17ConstraintBuilder13AllocateFixedEPNS1_18UnallocatedOperandEibb
	movl	4(%r14), %edx
	movzbl	%dl, %ecx
.L3121:
	movl	%edx, %eax
	addq	$1, %r12
	shrl	$24, %eax
	andl	$63, %eax
	cmpq	%r12, %rax
	ja	.L3118
.L3122:
	testl	%ecx, %ecx
	je	.L3113
	movl	-76(%rbp), %eax
	leaq	40(%r14), %rbx
	movq	%r14, -72(%rbp)
	xorl	%r12d, %r12d
	movq	%r13, %r14
	movq	%rbx, %r13
	addl	$1, %eax
	movl	%eax, -80(%rbp)
	cltq
	movq	%rax, -88(%rbp)
	salq	$3, %rax
	movq	%rax, -96(%rbp)
	jmp	.L3157
	.p2align 4,,10
	.p2align 3
.L3200:
	cmpq	$63, %rax
	jg	.L3139
	addq	-96(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	8(%rdx), %r8
	testq	%r8, %r8
	je	.L3194
.L3142:
	movq	0(%r13), %rax
	testb	$4, %al
	je	.L3145
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L3146
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L3146:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L3145:
	cmpq	%rax, %r15
	je	.L3147
	movq	(%r8), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L3195
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3149:
	movq	0(%r13), %rdx
	movq	%r15, 8(%rax)
	movq	%rdx, (%rax)
	movq	16(%r8), %rsi
	cmpq	8(%r8), %rsi
	movq	%rax, -64(%rbp)
	movq	24(%r8), %rax
	je	.L3150
.L3151:
	cmpq	%rax, %rsi
	je	.L3154
.L3204:
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r8)
.L3147:
	testb	%cl, %cl
	jne	.L3124
.L3155:
	movq	(%r14), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L3196
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3156:
	movq	112(%rbx), %rdx
	movl	-80(%rbp), %ecx
	movq	%r13, 8(%rax)
	movl	%ecx, (%rax)
	movq	%rdx, 16(%rax)
	cmpl	%ecx, 124(%rbx)
	movq	%rax, 112(%rbx)
	movl	%ecx, %eax
	cmovle	124(%rbx), %eax
	movl	%eax, 124(%rbx)
.L3124:
	movq	-72(%rbp), %rax
	addq	$1, %r12
	addq	$8, %r13
	movzbl	4(%rax), %eax
	cmpq	%rax, %r12
	jnb	.L3113
.L3157:
	movq	0(%r13), %rax
	movq	(%r14), %rdi
	movq	%rax, %rsi
	andl	$7, %eax
	shrq	$3, %rsi
	cmpl	$2, %eax
	je	.L3197
	call	_ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi
	movq	0(%r13), %rsi
	movq	%rax, %rbx
	movabsq	$34359738368, %rax
	testq	%rax, %rsi
	je	.L3125
	movq	%rsi, %rax
	shrq	$36, %rax
	andl	$7, %eax
	subl	$3, %eax
	cmpl	$1, %eax
	ja	.L3155
.L3125:
	movabsq	$34359738360, %r15
	movabsq	$652835028993, %rax
	andq	%rsi, %r15
	shrq	$3, %rsi
	orq	%rax, %r15
	movq	(%r14), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	movl	$1, %ecx
	leal	-7(%rax), %edx
	cmpb	$1, %dl
	jbe	.L3128
	subl	$10, %eax
	cmpb	$1, %al
	setbe	%cl
.L3128:
	movabsq	$34359738368, %rdi
	movq	0(%r13), %rax
	testq	%rdi, %rax
	je	.L3129
	movq	%rax, %rdx
	shrq	$36, %rdx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L3198
.L3129:
	movl	-76(%rbp), %edx
	movzbl	%cl, %ecx
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler17ConstraintBuilder13AllocateFixedEPNS1_18UnallocatedOperandEibb
	movq	0(%r13), %rax
	xorl	%ecx, %ecx
	testb	$4, %al
	je	.L3137
	movq	%rax, %rdx
	shrq	$3, %rdx
	andl	$3, %edx
	cmpl	$1, %edx
	je	.L3199
.L3137:
	movq	(%r14), %rax
	movq	16(%rax), %rsi
	movq	208(%rsi), %rdx
	movq	%rdx, %rax
	subq	216(%rsi), %rax
	sarq	$3, %rax
	addq	-88(%rbp), %rax
	jns	.L3200
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
.L3141:
	movq	232(%rsi), %rdi
	movq	%rdx, %r8
	salq	$6, %r8
	movq	(%rdi,%rdx,8), %rdx
	subq	%r8, %rax
	leaq	(%rdx,%rax,8), %rdx
	movq	(%rdx), %rdx
	movq	8(%rdx), %r8
	testq	%r8, %r8
	jne	.L3142
.L3194:
	movq	8(%rsi), %rdi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	$31, %rax
	jbe	.L3201
	leaq	32(%r8), %rax
	movq	%rax, 16(%rdi)
.L3144:
	movq	%rdi, (%r8)
	movq	$0, 8(%r8)
	movq	$0, 16(%r8)
	movq	$0, 24(%r8)
	movq	%r8, 8(%rdx)
	jmp	.L3142
	.p2align 4,,10
	.p2align 3
.L3193:
	leaq	(%rsi,%rcx,8), %rax
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3139:
	movq	%rax, %rdx
	sarq	$6, %rdx
	jmp	.L3141
	.p2align 4,,10
	.p2align 3
.L3197:
	call	_ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi
	movl	-80(%rbp), %ebx
	movl	124(%rax), %edx
	movq	%r13, 104(%rax)
	cmpl	%edx, %ebx
	cmovle	%ebx, %edx
	movl	%edx, 124(%rax)
	movl	4(%rax), %edx
	andl	$-97, %edx
	orl	$32, %edx
	movl	%edx, 4(%rax)
	jmp	.L3124
	.p2align 4,,10
	.p2align 3
.L3113:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3202
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3198:
	.cfi_restore_state
	movabsq	$1099511627776, %rdi
	testq	%rdi, %rax
	je	.L3129
	movb	$1, 152(%rbx)
	movq	0(%r13), %rax
	movq	(%r14), %r8
	shrq	$47, %rax
	movl	%eax, %r9d
	movq	440(%r8), %rdx
	andl	$7, %r9d
	cmpq	448(%r8), %rdx
	je	.L3130
	movq	%rbx, (%rdx)
	movl	%r9d, 8(%rdx)
	addq	$16, 440(%r8)
	jmp	.L3129
	.p2align 4,,10
	.p2align 3
.L3199:
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L3137
	movl	4(%rbx), %eax
	movl	-80(%rbp), %ecx
	movq	%r13, 104(%rbx)
	andl	$-97, %eax
	orl	$32, %eax
	movl	%eax, 4(%rbx)
	movl	124(%rbx), %eax
	cmpl	%eax, %ecx
	cmovle	%ecx, %eax
	movl	$1, %ecx
	movl	%eax, 124(%rbx)
	jmp	.L3137
	.p2align 4,,10
	.p2align 3
.L3150:
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	$31, %rdx
	ja	.L3151
	movq	(%r8), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$31, %rax
	jbe	.L3203
	leaq	32(%rsi), %rax
	movq	%rax, 16(%rdi)
.L3153:
	movq	%rsi, 8(%r8)
	movq	%rsi, 16(%r8)
	movq	%rax, 24(%r8)
	cmpq	%rax, %rsi
	jne	.L3204
	.p2align 4,,10
	.p2align 3
.L3154:
	leaq	-64(%rbp), %rdx
	movq	%r8, %rdi
	movb	%cl, -104(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movzbl	-104(%rbp), %ecx
	jmp	.L3147
	.p2align 4,,10
	.p2align 3
.L3196:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3156
	.p2align 4,,10
	.p2align 3
.L3195:
	movl	$16, %esi
	movq	%r8, -112(%rbp)
	movb	%cl, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-104(%rbp), %ecx
	movq	-112(%rbp), %r8
	jmp	.L3149
	.p2align 4,,10
	.p2align 3
.L3114:
	movq	%rax, %rcx
	notq	%rcx
	shrq	$6, %rcx
	notq	%rcx
	jmp	.L3117
.L3201:
	movl	$32, %esi
	movq	%rdx, -120(%rbp)
	movb	%cl, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %rdi
	movzbl	-112(%rbp), %ecx
	movq	-120(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L3144
.L3130:
	movq	432(%r8), %r11
	movq	%rdx, %rax
	subq	%r11, %rax
	movq	%rax, -112(%rbp)
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L3205
	testq	%rax, %rax
	je	.L3162
	leaq	(%rax,%rax), %rdi
	movl	$2147483632, %esi
	cmpq	%rdi, %rax
	jbe	.L3206
.L3133:
	movq	424(%r8), %rdi
	movq	%r11, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movl	%r9d, -128(%rbp)
	movb	%cl, -121(%rbp)
	movq	%r8, -120(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal4Zone3NewEm
	movq	-104(%rbp), %rsi
	movq	-120(%rbp), %r8
	movq	%rax, %r10
	movzbl	-121(%rbp), %ecx
	movl	-128(%rbp), %r9d
	leaq	(%rax,%rsi), %rax
	movq	-136(%rbp), %rdx
	leaq	16(%r10), %rsi
	movq	-144(%rbp), %r11
	movq	%rax, -104(%rbp)
.L3134:
	movq	-112(%rbp), %rax
	addq	%r10, %rax
	movq	%rbx, (%rax)
	movl	%r9d, 8(%rax)
	cmpq	%r11, %rdx
	je	.L3135
	movq	%r11, %rax
	movq	%r10, %rsi
	.p2align 4,,10
	.p2align 3
.L3136:
	movq	(%rax), %r9
	movl	8(%rax), %edi
	addq	$16, %rax
	addq	$16, %rsi
	movq	%r9, -16(%rsi)
	movl	%edi, -8(%rsi)
	cmpq	%rax, %rdx
	jne	.L3136
	subq	%r11, %rdx
	leaq	16(%r10,%rdx), %rsi
.L3135:
	movq	-104(%rbp), %rax
	movq	%r10, %xmm0
	movq	%rsi, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 448(%r8)
	movups	%xmm0, 432(%r8)
	jmp	.L3129
.L3162:
	movl	$16, %esi
	jmp	.L3133
.L3203:
	movl	$32, %esi
	movq	%r8, -112(%rbp)
	movb	%cl, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-104(%rbp), %ecx
	movq	-112(%rbp), %r8
	movq	%rax, %rsi
	leaq	32(%rax), %rax
	jmp	.L3153
.L3206:
	testq	%rdi, %rdi
	jne	.L3207
	movq	$0, -104(%rbp)
	movl	$16, %esi
	xorl	%r10d, %r10d
	jmp	.L3134
.L3202:
	call	__stack_chk_fail@PLT
.L3205:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3207:
	cmpq	$134217727, %rdi
	movl	$134217727, %esi
	cmovbe	%rdi, %rsi
	salq	$4, %rsi
	jmp	.L3133
	.cfi_endproc
.LFE23409:
	.size	_ZN2v88internal8compiler17ConstraintBuilder20MeetConstraintsAfterEi, .-_ZN2v88internal8compiler17ConstraintBuilder20MeetConstraintsAfterEi
	.section	.text._ZN2v88internal8compiler17ConstraintBuilder23MeetRegisterConstraintsEPKNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17ConstraintBuilder23MeetRegisterConstraintsEPKNS1_16InstructionBlockE
	.type	_ZN2v88internal8compiler17ConstraintBuilder23MeetRegisterConstraintsEPKNS1_16InstructionBlockE, @function
_ZN2v88internal8compiler17ConstraintBuilder23MeetRegisterConstraintsEPKNS1_16InstructionBlockE:
.LFB23407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	116(%rsi), %r14d
	movl	112(%rsi), %ebx
	leal	-1(%r14), %r13d
	cmpl	%ebx, %r13d
	jl	.L3209
	.p2align 4,,10
	.p2align 3
.L3212:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17ConstraintBuilder21MeetConstraintsBeforeEi
	cmpl	%ebx, %r13d
	je	.L3210
	movl	%ebx, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN2v88internal8compiler17ConstraintBuilder20MeetConstraintsAfterEi
	cmpl	%r14d, %ebx
	jne	.L3212
.L3209:
	addq	$8, %rsp
	movq	%r15, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler17ConstraintBuilder48MeetRegisterConstraintsForLastInstructionInBlockEPKNS1_16InstructionBlockE
	.p2align 4,,10
	.p2align 3
.L3210:
	.cfi_restore_state
	leal	1(%r13), %ebx
	cmpl	%r14d, %ebx
	jne	.L3212
	jmp	.L3209
	.cfi_endproc
.LFE23407:
	.size	_ZN2v88internal8compiler17ConstraintBuilder23MeetRegisterConstraintsEPKNS1_16InstructionBlockE, .-_ZN2v88internal8compiler17ConstraintBuilder23MeetRegisterConstraintsEPKNS1_16InstructionBlockE
	.section	.text._ZN2v88internal8compiler17ConstraintBuilder23MeetRegisterConstraintsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17ConstraintBuilder23MeetRegisterConstraintsEv
	.type	_ZN2v88internal8compiler17ConstraintBuilder23MeetRegisterConstraintsEv, @function
_ZN2v88internal8compiler17ConstraintBuilder23MeetRegisterConstraintsEv:
.LFB23406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	16(%rax), %rdx
	movq	16(%rdx), %rdx
	movq	16(%rdx), %rcx
	movq	8(%rdx), %r12
	movq	%rcx, -64(%rbp)
	cmpq	%rcx, %r12
	je	.L3214
	movq	%r12, -56(%rbp)
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L3221:
	movq	-56(%rbp), %rcx
	movq	496(%rax), %rdi
	movq	(%rcx), %r15
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movl	116(%r15), %r14d
	movl	112(%r15), %r12d
	leal	-1(%r14), %r13d
	cmpl	%r13d, %r12d
	jg	.L3219
	.p2align 4,,10
	.p2align 3
.L3216:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler17ConstraintBuilder21MeetConstraintsBeforeEi
	cmpl	%r12d, %r13d
	je	.L3218
	movl	%r12d, %esi
	movq	%rbx, %rdi
	addl	$1, %r12d
	call	_ZN2v88internal8compiler17ConstraintBuilder20MeetConstraintsAfterEi
	cmpl	%r14d, %r12d
	jne	.L3216
.L3219:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler17ConstraintBuilder48MeetRegisterConstraintsForLastInstructionInBlockEPKNS1_16InstructionBlockE
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -64(%rbp)
	je	.L3214
	movq	(%rbx), %rax
	jmp	.L3221
	.p2align 4,,10
	.p2align 3
.L3218:
	leal	1(%r13), %r12d
	cmpl	%r12d, %r14d
	jne	.L3216
	jmp	.L3219
	.p2align 4,,10
	.p2align 3
.L3214:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23406:
	.size	_ZN2v88internal8compiler17ConstraintBuilder23MeetRegisterConstraintsEv, .-_ZN2v88internal8compiler17ConstraintBuilder23MeetRegisterConstraintsEv
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilder3UseENS1_16LifetimePositionES3_PNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16LiveRangeBuilder3UseENS1_16LifetimePositionES3_PNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE
	.type	_ZN2v88internal8compiler16LiveRangeBuilder3UseENS1_16LifetimePositionES3_PNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE, @function
_ZN2v88internal8compiler16LiveRangeBuilder3UseENS1_16LifetimePositionES3_PNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE:
.LFB23430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rcx), %rsi
	movl	%esi, %eax
	andl	$7, %eax
	cmpl	$1, %eax
	je	.L3238
	cmpl	$2, %eax
	je	.L3238
	testb	$4, %sil
	jne	.L3228
.L3230:
	xorl	%r15d, %r15d
.L3224:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3228:
	.cfi_restore_state
	testb	$24, %sil
	jne	.L3230
	movq	%rsi, %rdx
	movl	%r9d, -60(%rbp)
	sarq	$35, %rsi
	shrq	$5, %rdx
	movq	%r8, -56(%rbp)
	cmpb	$11, %dl
	jbe	.L3239
	movl	16(%rbp), %ecx
	call	_ZN2v88internal8compiler16LiveRangeBuilder19FixedFPLiveRangeForEiNS0_21MachineRepresentationENS1_22RegisterAllocationData9SpillModeE
	movl	-60(%rbp), %r9d
	movq	-56(%rbp), %r11
	movq	%rax, %r10
	jmp	.L3226
	.p2align 4,,10
	.p2align 3
.L3238:
	movq	(%rbx), %rdi
	shrq	$3, %rsi
	movl	%r9d, -60(%rbp)
	movq	%r11, -56(%rbp)
	call	_ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi
	movq	-56(%rbp), %r11
	movl	-60(%rbp), %r9d
	movq	%rax, %r10
.L3226:
	testq	%r10, %r10
	je	.L3230
	movl	(%r12), %edx
	xorl	%r15d, %r15d
	andl	$7, %edx
	cmpl	$1, %edx
	je	.L3240
.L3232:
	movq	(%rbx), %rdx
	movl	%r14d, %esi
	movq	%r10, %rdi
	movl	488(%rdx), %r8d
	movq	(%rdx), %rcx
	movl	%r13d, %edx
	shrl	$2, %r8d
	andl	$1, %r8d
	call	_ZN2v88internal8compiler17TopLevelLiveRange14AddUseIntervalENS1_16LifetimePositionES3_PNS0_4ZoneEb
	jmp	.L3224
	.p2align 4,,10
	.p2align 3
.L3240:
	movl	%r9d, %r8d
	movq	%r11, %rcx
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal8compiler16LiveRangeBuilder14NewUsePositionENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE
	movq	(%rbx), %rdx
	movq	-56(%rbp), %r10
	movq	%rax, %rsi
	movq	%rax, %r15
	movl	488(%rdx), %edx
	movq	%r10, %rdi
	shrl	$2, %edx
	andl	$1, %edx
	call	_ZN2v88internal8compiler17TopLevelLiveRange14AddUsePositionEPNS1_11UsePositionEb
	movq	-56(%rbp), %r10
	jmp	.L3232
	.p2align 4,,10
	.p2align 3
.L3239:
	movl	16(%rbp), %edx
	call	_ZN2v88internal8compiler16LiveRangeBuilder17FixedLiveRangeForEiNS1_22RegisterAllocationData9SpillModeE
	movq	-56(%rbp), %r11
	movl	-60(%rbp), %r9d
	movq	%rax, %r10
	jmp	.L3226
	.cfi_endproc
.LFE23430:
	.size	_ZN2v88internal8compiler16LiveRangeBuilder3UseENS1_16LifetimePositionES3_PNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE, .-_ZN2v88internal8compiler16LiveRangeBuilder3UseENS1_16LifetimePositionES3_PNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData13NextLiveRangeENS0_21MachineRepresentationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData13NextLiveRangeENS0_21MachineRepresentationE
	.type	_ZN2v88internal8compiler22RegisterAllocationData13NextLiveRangeENS0_21MachineRepresentationE, @function
_ZN2v88internal8compiler22RegisterAllocationData13NextLiveRangeENS0_21MachineRepresentationE:
.LFB23382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	176(%rdi), %rsi
	movq	168(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	416(%rdi), %r13d
	movq	%rsi, %rax
	subq	%rcx, %rax
	leal	1(%r13), %edx
	sarq	$3, %rax
	movl	%edx, 416(%rdi)
	cmpl	%eax, %r13d
	jge	.L3249
.L3242:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$159, %rdx
	jbe	.L3250
	leaq	160(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3246:
	movzbl	%bl, %ebx
	movl	$0, (%rax)
	pxor	%xmm0, %xmm0
	sall	$13, %ebx
	movq	$0, 24(%rax)
	orl	$134221824, %ebx
	movq	%rax, 32(%rax)
	movl	%ebx, 4(%rax)
	movl	%r13d, 88(%rax)
	movl	$0, 92(%rax)
	movq	$0, 112(%rax)
	movb	$0, 120(%rax)
	movl	$2147483647, 124(%rax)
	movq	$0, 128(%rax)
	movq	%rax, 136(%rax)
	movq	$0, 144(%rax)
	movb	$0, 152(%rax)
	movups	%xmm0, 8(%rax)
	movups	%xmm0, 40(%rax)
	movups	%xmm0, 56(%rax)
	movups	%xmm0, 72(%rax)
	movups	%xmm0, 96(%rax)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3251
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3249:
	.cfi_restore_state
	movslq	%edx, %rdx
	movq	$0, -48(%rbp)
	cmpq	%rax, %rdx
	ja	.L3252
	jnb	.L3242
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L3242
	movq	%rax, 176(%rdi)
	jmp	.L3242
	.p2align 4,,10
	.p2align 3
.L3252:
	leaq	-48(%rbp), %rcx
	subq	%rax, %rdx
	leaq	160(%rdi), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	jmp	.L3242
	.p2align 4,,10
	.p2align 3
.L3250:
	movl	$160, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3246
.L3251:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23382:
	.size	_ZN2v88internal8compiler22RegisterAllocationData13NextLiveRangeENS0_21MachineRepresentationE, .-_ZN2v88internal8compiler22RegisterAllocationData13NextLiveRangeENS0_21MachineRepresentationE
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData18GetNextLiveRangeIdEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData18GetNextLiveRangeIdEv
	.type	_ZN2v88internal8compiler22RegisterAllocationData18GetNextLiveRangeIdEv, @function
_ZN2v88internal8compiler22RegisterAllocationData18GetNextLiveRangeIdEv:
.LFB23381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	176(%rdi), %rsi
	movq	168(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	416(%rdi), %r12d
	movq	%rsi, %rax
	subq	%rcx, %rax
	leal	1(%r12), %edx
	sarq	$3, %rax
	movl	%edx, 416(%rdi)
	cmpl	%eax, %r12d
	jge	.L3259
.L3253:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3260
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3259:
	.cfi_restore_state
	movslq	%edx, %rdx
	movq	$0, -32(%rbp)
	cmpq	%rax, %rdx
	ja	.L3261
	jnb	.L3253
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L3253
	movq	%rax, 176(%rdi)
	jmp	.L3253
	.p2align 4,,10
	.p2align 3
.L3261:
	leaq	-32(%rbp), %rcx
	subq	%rax, %rdx
	addq	$160, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	jmp	.L3253
.L3260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23381:
	.size	_ZN2v88internal8compiler22RegisterAllocationData18GetNextLiveRangeIdEv, .-_ZN2v88internal8compiler22RegisterAllocationData18GetNextLiveRangeIdEv
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilder19AddInitialIntervalsEPKNS1_16InstructionBlockEPNS0_9BitVectorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16LiveRangeBuilder19AddInitialIntervalsEPKNS1_16InstructionBlockEPNS0_9BitVectorE
	.type	_ZN2v88internal8compiler16LiveRangeBuilder19AddInitialIntervalsEPKNS1_16InstructionBlockEPNS0_9BitVectorE, @function
_ZN2v88internal8compiler16LiveRangeBuilder19AddInitialIntervalsEPKNS1_16InstructionBlockEPNS0_9BitVectorE:
.LFB23423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movl	116(%rsi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	112(%rsi), %eax
	movq	%rdx, -96(%rbp)
	sall	$2, %ebx
	cmpl	$1, 4(%rdx)
	movl	$0, -88(%rbp)
	leal	0(,%rax,4), %r13d
	movq	8(%rdx), %rax
	je	.L3264
	movq	(%rax), %rax
.L3264:
	leaq	-96(%rbp), %r14
	movq	%rax, -80(%rbp)
	movq	%r14, %rdi
	movl	$-1, -72(%rbp)
	call	_ZN2v88internal9BitVector8Iterator7AdvanceEv@PLT
	leaq	-104(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-96(%rbp), %rax
	movl	4(%rax), %eax
	cmpl	%eax, -88(%rbp)
	jl	.L3265
	jmp	.L3262
	.p2align 4,,10
	.p2align 3
.L3266:
	movslq	%r10d, %r11
	movq	(%rcx,%r11,8), %r15
	testq	%r15, %r15
	je	.L3287
.L3269:
	movq	(%r12), %rax
	movq	(%rax), %r9
	testb	$4, 488(%rax)
	jne	.L3288
.L3270:
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.L3289
.L3271:
	movl	(%rax), %edx
	cmpl	%edx, %ebx
	je	.L3290
	jl	.L3291
	cmpl	%edx, %r13d
	cmovle	%r13d, %edx
	movl	%edx, (%rax)
	movq	16(%r15), %rax
	movl	%ebx, %edx
	cmpl	%ebx, 4(%rax)
	cmovge	4(%rax), %edx
	movl	%edx, 4(%rax)
.L3274:
	movq	%r14, %rdi
	call	_ZN2v88internal9BitVector8Iterator7AdvanceEv@PLT
	movq	-96(%rbp), %rax
	movl	4(%rax), %eax
	cmpl	%eax, -88(%rbp)
	jge	.L3262
.L3265:
	movq	(%r12), %r9
	movl	-72(%rbp), %r10d
	movq	176(%r9), %rsi
	movq	168(%r9), %rcx
	movq	%rsi, %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpl	%eax, %r10d
	jl	.L3266
	leal	1(%r10), %edx
	movq	$0, -104(%rbp)
	movslq	%edx, %rdx
	cmpq	%rax, %rdx
	ja	.L3292
	jnb	.L3266
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L3266
	movslq	%r10d, %r11
	movq	%rax, 176(%r9)
	movq	(%rcx,%r11,8), %r15
	testq	%r15, %r15
	jne	.L3269
	.p2align 4,,10
	.p2align 3
.L3287:
	movq	16(%r9), %rdi
	movl	%r10d, %esi
	movq	%r11, -136(%rbp)
	movq	%r9, -128(%rbp)
	movl	%r10d, -120(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	movq	-128(%rbp), %r9
	movl	-120(%rbp), %r10d
	movl	%eax, %edx
	movq	%r9, %rdi
	movl	%r10d, %esi
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal8compiler22RegisterAllocationData12NewLiveRangeEiNS0_21MachineRepresentationE
	movq	-120(%rbp), %r9
	movq	-136(%rbp), %r11
	movq	%rax, %r15
	movq	168(%r9), %rax
	movq	%r15, (%rax,%r11,8)
	movq	(%r12), %rax
	movq	(%rax), %r9
	testb	$4, 488(%rax)
	je	.L3270
	.p2align 4,,10
	.p2align 3
.L3288:
	movl	88(%r15), %esi
	xorl	%eax, %eax
	movl	%ebx, %ecx
	movl	%r13d, %edx
	leaq	.LC12(%rip), %rdi
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	16(%r15), %rax
	movq	-120(%rbp), %r9
	testq	%rax, %rax
	jne	.L3271
.L3289:
	movq	16(%r9), %rax
	movq	24(%r9), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L3293
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r9)
.L3273:
	movl	%r13d, (%rax)
	movl	%ebx, 4(%rax)
	movq	$0, 8(%rax)
	movq	%rax, 16(%r15)
	movq	%rax, 8(%r15)
	jmp	.L3274
	.p2align 4,,10
	.p2align 3
.L3290:
	movl	%r13d, (%rax)
	jmp	.L3274
	.p2align 4,,10
	.p2align 3
.L3291:
	movq	16(%r9), %rax
	movq	24(%r9), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L3294
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r9)
.L3278:
	movq	$0, 8(%rax)
	movl	%r13d, (%rax)
	movl	%ebx, 4(%rax)
	movq	16(%r15), %rdx
	movq	%rdx, 8(%rax)
	movq	%rax, 16(%r15)
	jmp	.L3274
	.p2align 4,,10
	.p2align 3
.L3262:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3295
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3292:
	.cfi_restore_state
	movq	-144(%rbp), %rcx
	leaq	160(%r9), %rdi
	subq	%rax, %rdx
	movl	%r10d, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	movq	-120(%rbp), %r9
	movl	-128(%rbp), %r10d
	movq	168(%r9), %rcx
	jmp	.L3266
	.p2align 4,,10
	.p2align 3
.L3294:
	movl	$16, %esi
	movq	%r9, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3278
	.p2align 4,,10
	.p2align 3
.L3293:
	movl	$16, %esi
	movq	%r9, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3273
.L3295:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23423:
	.size	_ZN2v88internal8compiler16LiveRangeBuilder19AddInitialIntervalsEPKNS1_16InstructionBlockEPNS0_9BitVectorE, .-_ZN2v88internal8compiler16LiveRangeBuilder19AddInitialIntervalsEPKNS1_16InstructionBlockEPNS0_9BitVectorE
	.section	.rodata._ZN2v88internal8compiler22RegisterAllocationData26ExistsUseWithoutDefinitionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC69:
	.string	"Register allocator error: live v%d reached first block.\n"
	.section	.rodata._ZN2v88internal8compiler22RegisterAllocationData26ExistsUseWithoutDefinitionEv.str1.1,"aMS",@progbits,1
.LC70:
	.string	"  (first use is at %d)\n"
.LC71:
	.string	"\n"
.LC72:
	.string	"  (function: %s)\n"
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData26ExistsUseWithoutDefinitionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData26ExistsUseWithoutDefinitionEv
	.type	_ZN2v88internal8compiler22RegisterAllocationData26ExistsUseWithoutDefinitionEv, @function
_ZN2v88internal8compiler22RegisterAllocationData26ExistsUseWithoutDefinitionEv:
.LFB23394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	104(%rdi), %rax
	movq	(%rax), %rax
	movl	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	cmpl	$1, 4(%rax)
	movq	8(%rax), %rax
	je	.L3298
	movq	(%rax), %rax
.L3298:
	leaq	-96(%rbp), %r13
	movq	%rax, -80(%rbp)
	movq	%r13, %rdi
	movl	$-1, -72(%rbp)
	call	_ZN2v88internal9BitVector8Iterator7AdvanceEv@PLT
	movq	-96(%rbp), %rax
	movl	-88(%rbp), %ecx
	cmpl	%ecx, 4(%rax)
	jle	.L3308
	leaq	-104(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L3306:
	movl	-72(%rbp), %r12d
	xorl	%eax, %eax
	leaq	.LC69(%rip), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	176(%rbx), %rsi
	movq	168(%rbx), %rcx
	movq	%rsi, %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpl	%eax, %r12d
	jge	.L3311
.L3300:
	movslq	%r12d, %r15
	movq	(%rcx,%r15,8), %rax
	testq	%rax, %rax
	je	.L3312
.L3303:
	movq	24(%rax), %rax
	leaq	.LC70(%rip), %rdi
	movl	24(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L3313
	xorl	%eax, %eax
	leaq	.LC72(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal9BitVector8Iterator7AdvanceEv@PLT
	movq	-96(%rbp), %rax
	movl	4(%rax), %eax
	cmpl	%eax, -88(%rbp)
	jl	.L3306
.L3309:
	movl	$1, %eax
.L3296:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L3314
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3311:
	.cfi_restore_state
	leal	1(%r12), %edx
	movq	$0, -104(%rbp)
	movslq	%edx, %rdx
	cmpq	%rax, %rdx
	ja	.L3315
	jnb	.L3300
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L3300
	movslq	%r12d, %r15
	movq	%rax, 176(%rbx)
	movq	(%rcx,%r15,8), %rax
	testq	%rax, %rax
	jne	.L3303
	.p2align 4,,10
	.p2align 3
.L3312:
	movq	16(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal8compiler22RegisterAllocationData12NewLiveRangeEiNS0_21MachineRepresentationE
	movq	168(%rbx), %rdx
	movq	%rax, (%rdx,%r15,8)
	jmp	.L3303
	.p2align 4,,10
	.p2align 3
.L3313:
	xorl	%eax, %eax
	leaq	.LC71(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal9BitVector8Iterator7AdvanceEv@PLT
	movq	-96(%rbp), %rax
	movl	-88(%rbp), %edx
	cmpl	%edx, 4(%rax)
	jg	.L3306
	jmp	.L3309
	.p2align 4,,10
	.p2align 3
.L3315:
	movq	%r14, %rcx
	subq	%rax, %rdx
	leaq	160(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	movq	168(%rbx), %rcx
	jmp	.L3300
	.p2align 4,,10
	.p2align 3
.L3308:
	xorl	%eax, %eax
	jmp	.L3296
.L3314:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23394:
	.size	_ZN2v88internal8compiler22RegisterAllocationData26ExistsUseWithoutDefinitionEv, .-_ZN2v88internal8compiler22RegisterAllocationData26ExistsUseWithoutDefinitionEv
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler22RegisterAllocationData11PhiMapValueEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE17_M_emplace_uniqueIJS0_IiS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler22RegisterAllocationData11PhiMapValueEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE17_M_emplace_uniqueIJS0_IiS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler22RegisterAllocationData11PhiMapValueEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE17_M_emplace_uniqueIJS0_IiS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_
	.type	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler22RegisterAllocationData11PhiMapValueEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE17_M_emplace_uniqueIJS0_IiS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_, @function
_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler22RegisterAllocationData11PhiMapValueEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE17_M_emplace_uniqueIJS0_IiS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_:
.LFB27631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L3340
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
.L3318:
	movl	(%r14), %r15d
	movq	8(%r14), %rax
	leaq	16(%r13), %r14
	movl	%r15d, 32(%rbx)
	movq	%rax, 40(%rbx)
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L3320
	jmp	.L3341
	.p2align 4,,10
	.p2align 3
.L3342:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L3321
.L3343:
	movq	%rax, %r12
.L3320:
	movl	32(%r12), %ecx
	cmpl	%ecx, %r15d
	jl	.L3342
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L3343
.L3321:
	testb	%dl, %dl
	jne	.L3344
	cmpl	%ecx, %r15d
	jle	.L3326
.L3329:
	movl	$1, %edi
	cmpq	%r14, %r12
	jne	.L3345
.L3327:
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
	addq	$8, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3344:
	.cfi_restore_state
	cmpq	32(%r13), %r12
	je	.L3329
.L3330:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpl	%r15d, 32(%rax)
	jl	.L3346
	movq	%rax, %r12
.L3326:
	addq	$8, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3346:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L3326
	movl	$1, %edi
	cmpq	%r14, %r12
	je	.L3327
.L3345:
	xorl	%edi, %edi
	cmpl	32(%r12), %r15d
	setl	%dil
	jmp	.L3327
	.p2align 4,,10
	.p2align 3
.L3341:
	movq	%r14, %r12
	cmpq	32(%r13), %r14
	jne	.L3330
	movl	$1, %edi
	jmp	.L3327
	.p2align 4,,10
	.p2align 3
.L3340:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L3318
	.cfi_endproc
.LFE27631:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler22RegisterAllocationData11PhiMapValueEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE17_M_emplace_uniqueIJS0_IiS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_, .-_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler22RegisterAllocationData11PhiMapValueEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE17_M_emplace_uniqueIJS0_IiS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_
	.section	.text._ZN2v88internal8compiler22RegisterAllocationData16InitializePhiMapEPKNS1_16InstructionBlockEPNS1_14PhiInstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22RegisterAllocationData16InitializePhiMapEPKNS1_16InstructionBlockEPNS1_14PhiInstructionE
	.type	_ZN2v88internal8compiler22RegisterAllocationData16InitializePhiMapEPKNS1_16InstructionBlockEPNS1_14PhiInstructionE, @function
_ZN2v88internal8compiler22RegisterAllocationData16InitializePhiMapEPKNS1_16InstructionBlockEPNS1_14PhiInstructionE:
.LFB23383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%rsi, -88(%rbp)
	movq	(%rdi), %r14
	movq	16(%r14), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%r14), %rax
	subq	%r12, %rax
	cmpq	$55, %rax
	jbe	.L3359
	leaq	56(%r12), %rax
	movq	%rax, 16(%r14)
.L3349:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movq	$0, 24(%r12)
	movhps	-88(%rbp), %xmm0
	movups	%xmm0, (%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
	movq	32(%rbx), %rax
	subq	24(%rbx), %rax
	movl	$32, 48(%r12)
	movq	%rax, %rdx
	sarq	$2, %rdx
	cmpq	$1073741823, %rax
	ja	.L3360
	testq	%rdx, %rdx
	jne	.L3361
.L3351:
	movl	(%rbx), %eax
	leaq	-80(%rbp), %rsi
	leaq	40(%r13), %rdi
	movq	%r12, -72(%rbp)
	movl	%eax, -80(%rbp)
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler22RegisterAllocationData11PhiMapValueEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE17_M_emplace_uniqueIJS0_IiS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3362
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3361:
	.cfi_restore_state
	leaq	0(,%rdx,8), %r15
	movq	24(%r14), %rax
	movq	16(%r14), %rdx
	movq	%r15, %rsi
	subq	%rdx, %rax
	cmpq	%rax, %r15
	ja	.L3363
	addq	%rdx, %rsi
	movq	%rsi, 16(%r14)
.L3353:
	movq	%rdx, 24(%r12)
	movq	%rdx, 32(%r12)
	addq	%r15, %rdx
	movq	%rdx, 40(%r12)
	jmp	.L3351
	.p2align 4,,10
	.p2align 3
.L3359:
	movl	$56, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L3349
	.p2align 4,,10
	.p2align 3
.L3363:
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L3353
.L3360:
	leaq	.LC33(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3362:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23383:
	.size	_ZN2v88internal8compiler22RegisterAllocationData16InitializePhiMapEPKNS1_16InstructionBlockEPNS1_14PhiInstructionE, .-_ZN2v88internal8compiler22RegisterAllocationData16InitializePhiMapEPKNS1_16InstructionBlockEPNS1_14PhiInstructionE
	.section	.text._ZN2v88internal8compiler17ConstraintBuilder11ResolvePhisEPKNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17ConstraintBuilder11ResolvePhisEPKNS1_16InstructionBlockE
	.type	_ZN2v88internal8compiler17ConstraintBuilder11ResolvePhisEPKNS1_16InstructionBlockE, @function
_ZN2v88internal8compiler17ConstraintBuilder11ResolvePhisEPKNS1_16InstructionBlockE:
.LFB23412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	72(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	80(%rsi), %rax
	movq	%rcx, -88(%rbp)
	movq	%rax, -144(%rbp)
	cmpq	%rax, %rcx
	je	.L3364
	movq	%rdi, %r15
	movq	%rsi, %r14
	.p2align 4,,10
	.p2align 3
.L3398:
	movq	-88(%rbp), %rax
	movq	(%r15), %rbx
	movq	(%rax), %r9
	movq	(%rbx), %r12
	movl	(%r9), %eax
	movq	16(%r12), %r8
	movl	%eax, -132(%rbp)
	movq	24(%r12), %rax
	subq	%r8, %rax
	cmpq	$55, %rax
	jbe	.L3418
	leaq	56(%r8), %rax
	movq	%rax, 16(%r12)
.L3367:
	movq	%r9, %xmm0
	movq	%r14, %xmm1
	leaq	16(%r8), %rax
	movq	%r12, 16(%r8)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 24(%r8)
	movups	%xmm0, (%r8)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 32(%r8)
	movq	%rax, -120(%rbp)
	movq	32(%r9), %rax
	subq	24(%r9), %rax
	movl	$32, 48(%r8)
	movq	%rax, %r13
	sarq	$2, %r13
	cmpq	$1073741823, %rax
	ja	.L3419
	testq	%r13, %r13
	jne	.L3420
.L3369:
	movl	(%r9), %eax
	leaq	-80(%rbp), %r13
	leaq	40(%rbx), %rdi
	movq	%r9, -104(%rbp)
	movq	%r13, %rsi
	movq	%r8, -72(%rbp)
	movq	%r8, -96(%rbp)
	movl	%eax, -80(%rbp)
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler22RegisterAllocationData11PhiMapValueEESt10_Select1stIS8_ESt4lessIiENS3_13ZoneAllocatorIS8_EEE17_M_emplace_uniqueIJS0_IiS7_EEEES0_ISt17_Rb_tree_iteratorIS8_EbEDpOT_
	movq	-104(%rbp), %r9
	xorl	%ecx, %ecx
	movq	-96(%rbp), %r8
	movq	24(%r9), %rbx
	leaq	8(%r9), %rax
	cmpq	32(%r9), %rbx
	movq	%rax, -128(%rbp)
	je	.L3396
	movq	%r13, %rax
	movq	%rcx, %r13
	movq	%r15, %rcx
	movq	%r8, %r15
	movq	%r14, %r8
	movq	%r9, %r14
	movq	%rax, %r9
	.p2align 4,,10
	.p2align 3
.L3372:
	movq	(%rcx), %rax
	movq	16(%rax), %rsi
	movq	40(%r8), %rax
	movq	16(%rsi), %rdx
	movslq	(%rax,%r13,4), %rax
	movq	8(%rdx), %rdi
	movq	16(%rdx), %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	jnb	.L3421
	movq	(%rdi,%rax,8), %rax
	movl	(%rbx,%r13,4), %ebx
	movabsq	$652835028993, %rdx
	movq	208(%rsi), %rdi
	movl	116(%rax), %eax
	salq	$3, %rbx
	orq	%rdx, %rbx
	movq	%rdi, %rdx
	subq	216(%rsi), %rdx
	subl	$1, %eax
	sarq	$3, %rdx
	cltq
	addq	%rax, %rdx
	js	.L3376
	cmpq	$63, %rdx
	jg	.L3377
	leaq	(%rdi,%rax,8), %rax
	movq	(%rax), %rdx
	movq	16(%rdx), %r12
	testq	%r12, %r12
	je	.L3422
.L3380:
	movq	8(%r14), %rax
	testb	$4, %al
	je	.L3383
	xorl	%edx, %edx
	testb	$24, %al
	jne	.L3384
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L3384:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L3383:
	xorl	%edx, %edx
	cmpq	%rax, %rbx
	je	.L3385
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L3423
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3387:
	movq	%rbx, (%rax)
	movq	8(%r14), %rdx
	movq	%rdx, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	16(%r12), %rsi
	cmpq	8(%r12), %rsi
	movq	24(%r12), %rax
	je	.L3388
.L3389:
	cmpq	%rax, %rsi
	je	.L3392
	movq	-80(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L3393:
	movq	-80(%rbp), %rdx
.L3385:
	leaq	8(%rdx), %rax
	movq	%rax, -80(%rbp)
	movq	32(%r15), %rsi
	cmpq	40(%r15), %rsi
	je	.L3394
	movq	%rax, (%rsi)
	addq	$1, %r13
	addq	$8, 32(%r15)
	movq	24(%r14), %rbx
	movq	32(%r14), %rax
	subq	%rbx, %rax
	sarq	$2, %rax
	cmpq	%rax, %r13
	jb	.L3372
.L3417:
	movq	%rcx, %r15
	movq	%r8, %r14
.L3396:
	movq	(%r15), %rdi
	movl	-132(%rbp), %esi
	call	_ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi
	movl	112(%r14), %r12d
	movq	%rax, %rbx
	movq	(%r15), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L3424
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L3397:
	movq	112(%rbx), %rdx
	movq	-128(%rbp), %rcx
	movl	%r12d, (%rax)
	movq	%rcx, 8(%rax)
	movq	%rdx, 16(%rax)
	movl	4(%rbx), %edx
	cmpl	%r12d, 124(%rbx)
	cmovle	124(%rbx), %r12d
	movq	%rax, 112(%rbx)
	movl	%edx, %eax
	andl	$-25, %edx
	addq	$8, -88(%rbp)
	orl	$8, %eax
	movl	%r12d, 124(%rbx)
	movl	%eax, 4(%rbx)
	movl	108(%r14), %eax
	shrl	$31, %eax
	sall	$4, %eax
	orl	%edx, %eax
	orl	$8, %eax
	movl	%eax, 4(%rbx)
	movq	-88(%rbp), %rax
	cmpq	%rax, -144(%rbp)
	jne	.L3398
.L3364:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3425
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3377:
	.cfi_restore_state
	movq	%rdx, %rax
	sarq	$6, %rax
.L3379:
	movq	232(%rsi), %rdi
	movq	%rax, %r10
	salq	$6, %r10
	movq	(%rdi,%rax,8), %rax
	subq	%r10, %rdx
	leaq	(%rax,%rdx,8), %rax
	movq	(%rax), %rdx
	movq	16(%rdx), %r12
	testq	%r12, %r12
	jne	.L3380
.L3422:
	movq	8(%rsi), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	jbe	.L3426
	leaq	32(%r12), %rax
	movq	%rax, 16(%rdi)
.L3382:
	movq	%rdi, (%r12)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	%r12, 16(%rdx)
	jmp	.L3380
	.p2align 4,,10
	.p2align 3
.L3376:
	movq	%rdx, %rax
	notq	%rax
	shrq	$6, %rax
	notq	%rax
	jmp	.L3379
	.p2align 4,,10
	.p2align 3
.L3394:
	movq	-120(%rbp), %rdi
	movq	%r9, %rdx
	movq	%r8, -112(%rbp)
	addq	$1, %r13
	movq	%rcx, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	24(%r14), %rbx
	movq	32(%r14), %rax
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %rcx
	subq	%rbx, %rax
	movq	-112(%rbp), %r8
	sarq	$2, %rax
	cmpq	%r13, %rax
	ja	.L3372
	jmp	.L3417
	.p2align 4,,10
	.p2align 3
.L3392:
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rcx
	movq	-96(%rbp), %r9
	jmp	.L3393
	.p2align 4,,10
	.p2align 3
.L3388:
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	$31, %rdx
	ja	.L3389
	movq	(%r12), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$31, %rax
	jbe	.L3427
	leaq	32(%rsi), %rax
	movq	%rax, 16(%rdi)
.L3391:
	movq	%rsi, 8(%r12)
	movq	%rsi, 16(%r12)
	movq	%rax, 24(%r12)
	jmp	.L3389
	.p2align 4,,10
	.p2align 3
.L3423:
	movl	$16, %esi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	jmp	.L3387
	.p2align 4,,10
	.p2align 3
.L3420:
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	salq	$3, %r13
	movq	%r13, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %r13
	ja	.L3428
	addq	%rax, %rsi
	movq	%rsi, 16(%r12)
.L3371:
	addq	%rax, %r13
	movq	%rax, 24(%r8)
	movq	%rax, 32(%r8)
	movq	%r13, 40(%r8)
	jmp	.L3369
	.p2align 4,,10
	.p2align 3
.L3426:
	movl	$32, %esi
	movq	%r9, -160(%rbp)
	movq	%r8, -152(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rdi
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rcx
	movq	-152(%rbp), %r8
	movq	%rax, %r12
	movq	-160(%rbp), %r9
	jmp	.L3382
	.p2align 4,,10
	.p2align 3
.L3418:
	movl	$56, %esi
	movq	%r12, %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %r9
	movq	%rax, %r8
	jmp	.L3367
	.p2align 4,,10
	.p2align 3
.L3424:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L3397
.L3427:
	movl	$32, %esi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %r8
	movq	%rax, %rsi
	movq	-112(%rbp), %r9
	leaq	32(%rax), %rax
	jmp	.L3391
.L3428:
	movq	%r12, %rdi
	movq	%r8, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %r8
	jmp	.L3371
.L3421:
	movq	%rax, %rsi
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L3419:
	leaq	.LC33(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3425:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23412:
	.size	_ZN2v88internal8compiler17ConstraintBuilder11ResolvePhisEPKNS1_16InstructionBlockE, .-_ZN2v88internal8compiler17ConstraintBuilder11ResolvePhisEPKNS1_16InstructionBlockE
	.section	.text._ZN2v88internal8compiler17ConstraintBuilder11ResolvePhisEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler17ConstraintBuilder11ResolvePhisEv
	.type	_ZN2v88internal8compiler17ConstraintBuilder11ResolvePhisEv, @function
_ZN2v88internal8compiler17ConstraintBuilder11ResolvePhisEv:
.LFB23411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	16(%rax), %rdx
	movq	16(%rdx), %rdx
	movq	16(%rdx), %rbx
	movq	8(%rdx), %r13
	cmpq	%r13, %rbx
	je	.L3429
	movq	%rdi, %r12
	subq	$8, %rbx
	jmp	.L3431
	.p2align 4,,10
	.p2align 3
.L3433:
	movq	(%r12), %rax
	subq	$8, %rbx
.L3431:
	movq	(%rbx), %r14
	movq	496(%rax), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17ConstraintBuilder11ResolvePhisEPKNS1_16InstructionBlockE
	cmpq	%rbx, %r13
	jne	.L3433
.L3429:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23411:
	.size	_ZN2v88internal8compiler17ConstraintBuilder11ResolvePhisEv, .-_ZN2v88internal8compiler17ConstraintBuilder11ResolvePhisEv
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_:
.LFB27686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L3454
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
.L3436:
	movq	(%r14), %r15
	movq	8(%r14), %rax
	leaq	16(%r13), %r14
	movq	%r15, 32(%rbx)
	movq	%rax, 40(%rbx)
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L3438
	jmp	.L3455
	.p2align 4,,10
	.p2align 3
.L3456:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L3439
.L3457:
	movq	%rax, %r12
.L3438:
	movq	32(%r12), %rcx
	cmpq	%r15, %rcx
	ja	.L3456
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L3457
.L3439:
	testb	%dl, %dl
	jne	.L3458
	cmpq	%r15, %rcx
	jnb	.L3447
.L3446:
	movl	$1, %edi
	cmpq	%r12, %r14
	jne	.L3459
.L3444:
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
	addq	$8, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3458:
	.cfi_restore_state
	cmpq	%r12, 32(%r13)
	je	.L3446
.L3448:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpq	32(%rax), %r15
	ja	.L3446
	movq	%rax, %r12
.L3447:
	addq	$8, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3459:
	.cfi_restore_state
	xorl	%edi, %edi
	cmpq	32(%r12), %r15
	setb	%dil
	jmp	.L3444
	.p2align 4,,10
	.p2align 3
.L3455:
	movq	%r14, %r12
	cmpq	32(%r13), %r14
	jne	.L3448
	movl	$1, %edi
	jmp	.L3444
	.p2align 4,,10
	.p2align 3
.L3454:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L3436
	.cfi_endproc
.LFE27686:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilder10MapPhiHintEPNS1_18InstructionOperandEPNS1_11UsePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16LiveRangeBuilder10MapPhiHintEPNS1_18InstructionOperandEPNS1_11UsePositionE
	.type	_ZN2v88internal8compiler16LiveRangeBuilder10MapPhiHintEPNS1_18InstructionOperandEPNS1_11UsePositionE, @function
_ZN2v88internal8compiler16LiveRangeBuilder10MapPhiHintEPNS1_18InstructionOperandEPNS1_11UsePositionE:
.LFB23439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	addq	$8, %rdi
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rsi
	movaps	%xmm0, -32(%rbp)
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3463
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3463:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23439:
	.size	_ZN2v88internal8compiler16LiveRangeBuilder10MapPhiHintEPNS1_18InstructionOperandEPNS1_11UsePositionE, .-_ZN2v88internal8compiler16LiveRangeBuilder10MapPhiHintEPNS1_18InstructionOperandEPNS1_11UsePositionE
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilder11ProcessPhisEPKNS1_16InstructionBlockEPNS0_9BitVectorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16LiveRangeBuilder11ProcessPhisEPKNS1_16InstructionBlockEPNS0_9BitVectorE
	.type	_ZN2v88internal8compiler16LiveRangeBuilder11ProcessPhisEPKNS1_16InstructionBlockEPNS0_9BitVectorE, @function
_ZN2v88internal8compiler16LiveRangeBuilder11ProcessPhisEPKNS1_16InstructionBlockEPNS0_9BitVectorE:
.LFB23432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	72(%rsi), %rdi
	movq	%rdx, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	80(%rsi), %rax
	movq	%rdi, -112(%rbp)
	movq	%rax, -152(%rbp)
	cmpq	%rax, %rdi
	je	.L3464
	movq	%rsi, %rbx
	.p2align 4,,10
	.p2align 3
.L3488:
	movq	-112(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -128(%rbp)
	movl	(%rax), %r9d
	movq	-144(%rbp), %rax
	cmpl	$1, 4(%rax)
	movq	8(%rax), %rdx
	je	.L3518
	testl	%r9d, %r9d
	leal	63(%r9), %eax
	movl	%r9d, %esi
	movl	$1, %edi
	cmovns	%r9d, %eax
	sarl	$31, %esi
	shrl	$26, %esi
	leal	(%r9,%rsi), %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%esi, %ecx
	salq	%cl, %rdi
	movq	%rdi, %rcx
	notq	%rcx
	andq	%rcx, (%rdx,%rax,8)
.L3467:
	movq	-120(%rbp), %rax
	movq	40(%rbx), %rdi
	movq	48(%rbx), %r12
	movq	(%rax), %rax
	movq	%rax, -136(%rbp)
	cmpq	%r12, %rdi
	je	.L3494
	movq	16(%rax), %r13
	movl	$2, -92(%rbp)
	movl	$0, -100(%rbp)
	movq	16(%r13), %rax
	movq	$0, -88(%rbp)
	movq	8(%rax), %r11
	movq	16(%rax), %rax
	subq	%r11, %rax
	sarq	$3, %rax
	movq	%rax, %r10
	jmp	.L3485
	.p2align 4,,10
	.p2align 3
.L3470:
	addq	$4, %rdi
	cmpq	%rdi, %r12
	je	.L3468
.L3485:
	movslq	(%rdi), %rsi
	cmpq	%r10, %rsi
	jnb	.L3519
	movq	(%r11,%rsi,8), %r8
	cmpl	100(%rbx), %esi
	jge	.L3470
	movl	116(%r8), %eax
	movq	208(%r13), %rcx
	leal	-1(%rax), %edx
	movq	%rcx, %rax
	subq	216(%r13), %rax
	movl	%edx, -96(%rbp)
	sarq	$3, %rax
	movslq	%edx, %rdx
	addq	%rdx, %rax
	js	.L3471
	cmpq	$63, %rax
	jg	.L3472
	leaq	(%rcx,%rdx,8), %rax
.L3473:
	movq	(%rax), %r14
	movq	16(%r14), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %r15
	cmpq	%rax, %r15
	jne	.L3477
	jmp	.L3495
	.p2align 4,,10
	.p2align 3
.L3476:
	addq	$8, %rax
	cmpq	%rax, %r15
	je	.L3495
.L3477:
	movq	(%rax), %rcx
	movq	8(%rcx), %rdx
	movl	%edx, %esi
	andl	$7, %esi
	cmpl	$1, %esi
	jne	.L3476
	shrq	$3, %rdx
	cmpl	%edx, %r9d
	jne	.L3476
.L3475:
	xorl	%esi, %esi
	movq	8(%r14), %rdx
	cmpb	$0, 120(%r8)
	sete	%sil
	sall	$2, %esi
	testq	%rdx, %rdx
	jne	.L3520
.L3479:
	movl	-96(%rbp), %eax
	cmpl	112(%r8), %eax
	jne	.L3482
	orl	$1, %esi
.L3482:
	cmpq	$0, -88(%rbp)
	je	.L3500
	cmpl	-100(%rbp), %esi
	jg	.L3500
.L3483:
	cmpl	$1, -92(%rbp)
	je	.L3468
	addq	$4, %rdi
	movl	$1, -92(%rbp)
	cmpq	%rdi, %r12
	jne	.L3485
	.p2align 4,,10
	.p2align 3
.L3468:
	movl	112(%rbx), %eax
	xorl	%r9d, %r9d
	leal	0(,%rax,4), %esi
	movq	-136(%rbp), %rax
	testb	$1, 488(%rax)
	je	.L3486
	movzbl	120(%rbx), %r9d
.L3486:
	movq	-88(%rbp), %rax
	movq	(%rax), %rdx
	movl	%edx, %eax
	andl	$7, %eax
	cmpl	$4, %eax
	ja	.L3490
	andl	$6, %edx
	jne	.L3498
	cmpl	$1, %eax
	jne	.L3491
	movl	$4, %r8d
.L3487:
	movq	-88(%rbp), %r15
	movq	-120(%rbp), %r14
	movq	-128(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r14, %rdi
	addq	$8, %rdx
	call	_ZN2v88internal8compiler16LiveRangeBuilder6DefineENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeENS1_22RegisterAllocationData9SpillModeE
	movq	%r15, %xmm0
	leaq	-80(%rbp), %rsi
	leaq	8(%r14), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS5_IS4_S8_EEEES5_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
	addq	$8, -112(%rbp)
	movq	-112(%rbp), %rax
	cmpq	%rax, -152(%rbp)
	jne	.L3488
.L3464:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3521
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3495:
	.cfi_restore_state
	xorl	%ecx, %ecx
	jmp	.L3475
	.p2align 4,,10
	.p2align 3
.L3500:
	movl	%esi, -100(%rbp)
	movq	%rcx, -88(%rbp)
	jmp	.L3483
	.p2align 4,,10
	.p2align 3
.L3520:
	movq	8(%rdx), %rax
	movq	16(%rdx), %r14
	cmpq	%rax, %r14
	je	.L3479
	movq	(%rcx), %r15
	jmp	.L3481
	.p2align 4,,10
	.p2align 3
.L3480:
	addq	$8, %rax
	cmpq	%rax, %r14
	je	.L3479
.L3481:
	movq	(%rax), %rdx
	cmpq	8(%rdx), %r15
	jne	.L3480
	movq	(%rdx), %rax
	movl	%esi, %edx
	orl	$2, %edx
	andl	$7, %eax
	subl	$4, %eax
	cmpl	$2, %eax
	cmovb	%edx, %esi
	jmp	.L3479
	.p2align 4,,10
	.p2align 3
.L3472:
	movq	%rax, %rdx
	sarq	$6, %rdx
.L3474:
	movq	232(%r13), %rsi
	movq	%rdx, %rcx
	salq	$6, %rcx
	movq	(%rsi,%rdx,8), %rdx
	subq	%rcx, %rax
	leaq	(%rdx,%rax,8), %rax
	jmp	.L3473
	.p2align 4,,10
	.p2align 3
.L3471:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L3474
.L3490:
	cmpl	$5, %eax
	jne	.L3491
	andl	$24, %edx
	sete	%r8b
	jmp	.L3487
.L3498:
	xorl	%r8d, %r8d
	jmp	.L3487
.L3518:
	movl	$1, %eax
	movl	%r9d, %ecx
	salq	%cl, %rax
	notq	%rax
	andq	%rax, %rdx
	movq	-144(%rbp), %rax
	movq	%rdx, 8(%rax)
	jmp	.L3467
.L3494:
	movq	$0, -88(%rbp)
	jmp	.L3468
.L3519:
	movq	%r10, %rdx
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L3521:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L3491:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23432:
	.size	_ZN2v88internal8compiler16LiveRangeBuilder11ProcessPhisEPKNS1_16InstructionBlockEPNS0_9BitVectorE, .-_ZN2v88internal8compiler16LiveRangeBuilder11ProcessPhisEPKNS1_16InstructionBlockEPNS0_9BitVectorE
	.section	.text._ZN2v88internal8compiler16LiveRangeBuilder15BuildLiveRangesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16LiveRangeBuilder15BuildLiveRangesEv
	.type	_ZN2v88internal8compiler16LiveRangeBuilder15BuildLiveRangesEv, @function
_ZN2v88internal8compiler16LiveRangeBuilder15BuildLiveRangesEv:
.LFB23434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %rdi
	movq	16(%rdi), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %r13
	subq	8(%rax), %r13
	sarq	$3, %r13
	movl	%r13d, %eax
	subl	$1, %eax
	js	.L3523
	movslq	%r13d, %r13
	movslq	%eax, %r15
	movl	%eax, %eax
	subq	$2, %r13
	subq	%rax, %r13
	jmp	.L3528
	.p2align 4,,10
	.p2align 3
.L3525:
	movq	(%rbx), %rax
	movq	104(%rax), %rax
	movq	%r12, (%rax,%r15,8)
	subq	$1, %r15
	cmpq	%r15, %r13
	je	.L3526
.L3527:
	movq	(%rbx), %rdi
.L3528:
	movq	496(%rdi), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	(%rbx), %rsi
	movq	16(%rsi), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rcx
	movq	16(%rax), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%r15, %rdx
	jbe	.L3564
	movq	(%rcx,%r15,8), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler16LiveRangeBuilder14ComputeLiveOutEPKNS1_16InstructionBlockEPNS1_22RegisterAllocationDataE
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r12
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler16LiveRangeBuilder19AddInitialIntervalsEPKNS1_16InstructionBlockEPNS0_9BitVectorE
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler16LiveRangeBuilder19ProcessInstructionsEPKNS1_16InstructionBlockEPNS0_9BitVectorE
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler16LiveRangeBuilder11ProcessPhisEPKNS1_16InstructionBlockEPNS0_9BitVectorE
	movl	108(%r14), %eax
	testl	%eax, %eax
	js	.L3525
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler16LiveRangeBuilder17ProcessLoopHeaderEPKNS1_16InstructionBlockEPNS0_9BitVectorE
	movq	(%rbx), %rax
	movq	104(%rax), %rax
	movq	%r12, (%rax,%r15,8)
	subq	$1, %r15
	cmpq	%r13, %r15
	jne	.L3527
.L3526:
	movq	(%rbx), %rdi
.L3523:
	movq	176(%rdi), %r12
	movq	168(%rdi), %r14
	movq	%r12, %r13
	subq	%r14, %r13
	cmpq	%r12, %r14
	jne	.L3538
	jmp	.L3529
	.p2align 4,,10
	.p2align 3
.L3563:
	movq	(%rbx), %rdi
.L3531:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L3529
.L3538:
	movq	496(%rdi), %rdi
	movq	(%r14), %r15
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	(%rbx), %rdi
	movq	176(%rdi), %rax
	subq	168(%rdi), %rax
	cmpq	%r13, %rax
	jne	.L3565
	testq	%r15, %r15
	je	.L3531
	movl	4(%r15), %eax
	movl	%eax, %edx
	shrl	$5, %eax
	shrl	%edx
	andl	$3, %eax
	andl	$3, %edx
	je	.L3532
	testl	%eax, %eax
	jne	.L3532
	cmpl	$1, %edx
	movq	%r15, %rsi
	sete	%dl
	movzbl	%dl, %edx
	call	_ZN2v88internal8compiler22RegisterAllocationData27AssignSpillRangeToLiveRangeEPNS1_17TopLevelLiveRangeENS2_9SpillModeE
	movl	4(%r15), %eax
	shrl	$5, %eax
	andl	$3, %eax
.L3532:
	cmpl	$1, %eax
	jne	.L3563
	movq	104(%r15), %rax
	movq	(%rax), %rax
	andl	$7, %eax
	cmpl	$2, %eax
	jne	.L3563
	movq	24(%r15), %rdx
	testq	%rdx, %rdx
	je	.L3563
	.p2align 4,,10
	.p2align 3
.L3537:
	movl	28(%rdx), %eax
	testb	$1, %al
	jne	.L3535
	movl	24(%rdx), %ecx
	andl	$2, %ecx
	cmpl	$1, %ecx
	sbbl	%ecx, %ecx
	andl	$28, %eax
	andl	$-2, %ecx
	addl	$34, %ecx
	orl	%ecx, %eax
	orb	$8, %ah
	movl	%eax, 28(%rdx)
.L3535:
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L3537
	jmp	.L3563
	.p2align 4,,10
	.p2align 3
.L3565:
	leaq	.LC63(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3529:
	movq	440(%rdi), %rcx
	movq	432(%rdi), %r12
	cmpq	%r12, %rcx
	jne	.L3546
	jmp	.L3522
	.p2align 4,,10
	.p2align 3
.L3540:
	addq	$16, %r12
	movl	%r15d, 44(%r14)
	cmpq	%r12, %rcx
	je	.L3522
.L3546:
	movq	(%r12), %r13
	movl	8(%r12), %r15d
	movl	4(%r13), %eax
	movq	104(%r13), %r14
	testb	$64, %al
	jne	.L3540
	movq	(%rbx), %r8
	testq	%r14, %r14
	je	.L3566
.L3541:
	andl	$-97, %eax
	orl	$64, %eax
	movl	%eax, 4(%r13)
	movq	96(%r13), %rax
	testq	%rax, %rax
	je	.L3544
	movslq	88(%rax), %rax
.L3545:
	movq	328(%r8), %rdx
	addq	$16, %r12
	movq	%r14, (%rdx,%rax,8)
	movl	%r15d, 44(%r14)
	cmpq	%r12, %rcx
	jne	.L3546
.L3522:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3544:
	.cfi_restore_state
	movslq	88(%r13), %rax
	jmp	.L3545
	.p2align 4,,10
	.p2align 3
.L3566:
	movq	(%r8), %rdx
	movq	16(%rdx), %r14
	movq	24(%rdx), %rax
	subq	%r14, %rax
	cmpq	$55, %rax
	jbe	.L3567
	leaq	56(%r14), %rax
	movq	%rax, 16(%rdx)
.L3543:
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal8compiler10SpillRangeC1EPNS1_17TopLevelLiveRangeEPNS0_4ZoneE
	movl	4(%r13), %eax
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r8
	jmp	.L3541
.L3567:
	movq	%rdx, %rdi
	movl	$56, %esi
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	%rax, %r14
	jmp	.L3543
.L3564:
	movq	%r15, %rsi
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE23434:
	.size	_ZN2v88internal8compiler16LiveRangeBuilder15BuildLiveRangesEv, .-_ZN2v88internal8compiler16LiveRangeBuilder15BuildLiveRangesEv
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB27730:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L3576
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L3570:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L3570
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3576:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE27730:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt6vectorIPN2v88internal8compiler9LiveRangeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler9LiveRangeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler9LiveRangeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler9LiveRangeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler9LiveRangeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB27826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L3617
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L3595
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L3618
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L3581:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L3619
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3584:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L3582
	.p2align 4,,10
	.p2align 3
.L3618:
	testq	%rdx, %rdx
	jne	.L3620
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L3582:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L3585
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L3598
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L3598
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L3587:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L3587
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L3589
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L3589:
	leaq	16(%rax,%r8), %rcx
.L3585:
	cmpq	%r14, %r12
	je	.L3590
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L3599
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L3599
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L3592:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L3592
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L3594
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L3594:
	leaq	8(%rcx,%r9), %rcx
.L3590:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3595:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L3581
	.p2align 4,,10
	.p2align 3
.L3599:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L3591:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L3591
	jmp	.L3594
	.p2align 4,,10
	.p2align 3
.L3598:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L3586:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L3586
	jmp	.L3589
.L3619:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L3584
.L3617:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3620:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L3581
	.cfi_endproc
.LFE27826:
	.size	_ZNSt6vectorIPN2v88internal8compiler9LiveRangeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler9LiveRangeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator11AddToActiveEPNS1_9LiveRangeE.str1.8,"aMS",@progbits,1
	.align 8
.LC73:
	.string	"Add live range %d:%d in %s to active\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator11AddToActiveEPNS1_9LiveRangeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator11AddToActiveEPNS1_9LiveRangeE
	.type	_ZN2v88internal8compiler19LinearScanAllocator11AddToActiveEPNS1_9LiveRangeE, @function
_ZN2v88internal8compiler19LinearScanAllocator11AddToActiveEPNS1_9LiveRangeE:
.LFB23590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	%rsi, -24(%rbp)
	testb	$4, 488(%rax)
	je	.L3622
	movl	4(%rsi), %eax
	movq	%rsi, %rdx
	leaq	.LC26(%rip), %rcx
	shrl	$7, %eax
	andl	$63, %eax
	cmpl	$32, %eax
	je	.L3623
	movl	8(%rdi), %ecx
	cltq
	testl	%ecx, %ecx
	jne	.L3624
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rcx
	movq	(%rcx,%rax,8), %rcx
.L3623:
	movq	32(%rdx), %rax
	movl	(%rdx), %edx
	leaq	.LC73(%rip), %rdi
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L3622:
	movq	112(%rbx), %rsi
	cmpq	120(%rbx), %rsi
	je	.L3625
	movq	-24(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 112(%rbx)
.L3626:
	movq	-24(%rbp), %rdx
	movq	16(%rdx), %rsi
	movq	48(%rdx), %rax
	movl	(%rsi), %ecx
	testq	%rax, %rax
	je	.L3633
	cmpl	%ecx, (%rax)
	jle	.L3640
	movq	%rsi, %rax
	movq	$0, 48(%rdx)
	movl	4(%rax), %edx
	cmpl	%ecx, %edx
	jge	.L3641
	.p2align 4,,10
	.p2align 3
.L3629:
	movq	8(%rax), %rax
.L3640:
	movl	4(%rax), %edx
	cmpl	%ecx, %edx
	jl	.L3629
.L3641:
	cmpl	%edx, 160(%rbx)
	jle	.L3621
	movl	%edx, 160(%rbx)
.L3621:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3624:
	.cfi_restore_state
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rcx
	movq	(%rcx,%rax,8), %rcx
	jmp	.L3623
	.p2align 4,,10
	.p2align 3
.L3633:
	movq	%rsi, %rax
	jmp	.L3640
	.p2align 4,,10
	.p2align 3
.L3625:
	leaq	-24(%rbp), %rdx
	leaq	96(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler9LiveRangeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L3626
	.cfi_endproc
.LFE23590:
	.size	_ZN2v88internal8compiler19LinearScanAllocator11AddToActiveEPNS1_9LiveRangeE, .-_ZN2v88internal8compiler19LinearScanAllocator11AddToActiveEPNS1_9LiveRangeE
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator13AddToInactiveEPNS1_9LiveRangeE.str1.8,"aMS",@progbits,1
	.align 8
.LC74:
	.string	"Add live range %d:%d to inactive\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator13AddToInactiveEPNS1_9LiveRangeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator13AddToInactiveEPNS1_9LiveRangeE
	.type	_ZN2v88internal8compiler19LinearScanAllocator13AddToInactiveEPNS1_9LiveRangeE, @function
_ZN2v88internal8compiler19LinearScanAllocator13AddToInactiveEPNS1_9LiveRangeE:
.LFB23591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	%rsi, -24(%rbp)
	testb	$4, 488(%rax)
	jne	.L3657
	movq	144(%rbx), %rsi
	cmpq	152(%rbx), %rsi
	je	.L3644
.L3658:
	movq	-24(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 144(%rbx)
.L3645:
	movq	-24(%rbp), %rsi
	movq	16(%rsi), %rdi
	movq	48(%rsi), %rax
	movl	(%rdi), %ecx
	testq	%rax, %rax
	je	.L3651
	movl	(%rax), %edx
	cmpl	%ecx, %edx
	jle	.L3647
	movq	$0, 48(%rsi)
	movl	(%rdi), %edx
	movq	%rdi, %rax
.L3647:
	cmpl	%edx, %ecx
	jle	.L3646
	.p2align 4,,10
	.p2align 3
.L3648:
	movq	8(%rax), %rax
	movl	(%rax), %edx
	cmpl	%ecx, %edx
	jl	.L3648
.L3646:
	cmpl	%edx, 164(%rbx)
	jle	.L3642
	movl	%edx, 164(%rbx)
.L3642:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3657:
	.cfi_restore_state
	movq	32(%rsi), %rdx
	movq	%rsi, %rax
	leaq	.LC74(%rip), %rdi
	movl	88(%rdx), %esi
	movl	(%rax), %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	144(%rbx), %rsi
	cmpq	152(%rbx), %rsi
	jne	.L3658
.L3644:
	leaq	-24(%rbp), %rdx
	leaq	128(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler9LiveRangeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L3645
	.p2align 4,,10
	.p2align 3
.L3651:
	movl	%ecx, %edx
	jmp	.L3646
	.cfi_endproc
.LFE23591:
	.size	_ZN2v88internal8compiler19LinearScanAllocator13AddToInactiveEPNS1_9LiveRangeE, .-_ZN2v88internal8compiler19LinearScanAllocator13AddToInactiveEPNS1_9LiveRangeE
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator16ActiveToInactiveEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEENS1_16LifetimePositionE.str1.8,"aMS",@progbits,1
	.align 8
.LC75:
	.string	"Moving live range %d:%d from active to inactive\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator16ActiveToInactiveEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator16ActiveToInactiveEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEENS1_16LifetimePositionE
	.type	_ZN2v88internal8compiler19LinearScanAllocator16ActiveToInactiveEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEENS1_16LifetimePositionE, @function
_ZN2v88internal8compiler19LinearScanAllocator16ActiveToInactiveEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEENS1_16LifetimePositionE:
.LFB23594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	144(%rdi), %rsi
	movq	%rax, -48(%rbp)
	cmpq	152(%rdi), %rsi
	je	.L3660
	movq	%rax, (%rsi)
	addq	$8, 144(%rdi)
.L3661:
	movq	(%r12), %rax
	movq	-48(%rbp), %rcx
	testb	$4, 488(%rax)
	jne	.L3678
.L3662:
	movq	48(%rcx), %rax
	testq	%rax, %rax
	je	.L3676
	movl	(%rax), %edx
	cmpl	%edx, %ebx
	jge	.L3677
	jmp	.L3679
	.p2align 4,,10
	.p2align 3
.L3666:
	movq	8(%rax), %rax
	movl	(%rax), %edx
.L3677:
	cmpl	%edx, %ebx
	jg	.L3666
	cmpl	164(%r12), %edx
	jge	.L3668
	movl	%edx, 164(%r12)
.L3668:
	movq	112(%r12), %rdx
	leaq	8(%r13), %rsi
	cmpq	%rsi, %rdx
	je	.L3669
	subq	%rsi, %rdx
	movq	%r13, %rdi
	call	memmove@PLT
	movq	112(%r12), %rsi
.L3669:
	subq	$8, %rsi
	movq	%rsi, 112(%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3680
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3679:
	.cfi_restore_state
	movq	$0, 48(%rcx)
.L3676:
	movq	16(%rcx), %rax
	movl	(%rax), %edx
	jmp	.L3677
	.p2align 4,,10
	.p2align 3
.L3678:
	movq	32(%rcx), %rax
	movl	(%rcx), %edx
	leaq	.LC75(%rip), %rdi
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-48(%rbp), %rcx
	jmp	.L3662
	.p2align 4,,10
	.p2align 3
.L3660:
	leaq	-48(%rbp), %rdx
	leaq	128(%rdi), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler9LiveRangeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L3661
.L3680:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23594:
	.size	_ZN2v88internal8compiler19LinearScanAllocator16ActiveToInactiveEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEENS1_16LifetimePositionE, .-_ZN2v88internal8compiler19LinearScanAllocator16ActiveToInactiveEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEENS1_16LifetimePositionE
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator16InactiveToActiveEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEENS1_16LifetimePositionE.str1.8,"aMS",@progbits,1
	.align 8
.LC76:
	.string	"Moving live range %d:%d from inactive to active\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator16InactiveToActiveEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator16InactiveToActiveEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEENS1_16LifetimePositionE
	.type	_ZN2v88internal8compiler19LinearScanAllocator16InactiveToActiveEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEENS1_16LifetimePositionE, @function
_ZN2v88internal8compiler19LinearScanAllocator16InactiveToActiveEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEENS1_16LifetimePositionE:
.LFB23596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	112(%rdi), %rsi
	movq	%rax, -48(%rbp)
	cmpq	120(%rdi), %rsi
	je	.L3682
	movq	%rax, (%rsi)
	addq	$8, 112(%rdi)
.L3683:
	movq	(%r12), %rax
	movq	-48(%rbp), %rdx
	testb	$4, 488(%rax)
	jne	.L3699
.L3684:
	movq	48(%rdx), %rax
	testq	%rax, %rax
	je	.L3698
	cmpl	(%rax), %ebx
	jl	.L3700
.L3686:
	movl	4(%rax), %edx
	cmpl	%ebx, %edx
	jge	.L3687
	.p2align 4,,10
	.p2align 3
.L3688:
	movq	8(%rax), %rax
	movl	4(%rax), %edx
	cmpl	%edx, %ebx
	jg	.L3688
.L3687:
	cmpl	160(%r12), %edx
	jge	.L3690
	movl	%edx, 160(%r12)
.L3690:
	movq	144(%r12), %rdx
	leaq	8(%r13), %rsi
	cmpq	%rsi, %rdx
	je	.L3691
	subq	%rsi, %rdx
	movq	%r13, %rdi
	call	memmove@PLT
	movq	144(%r12), %rsi
.L3691:
	subq	$8, %rsi
	movq	%rsi, 144(%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3701
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3700:
	.cfi_restore_state
	movq	$0, 48(%rdx)
.L3698:
	movq	16(%rdx), %rax
	jmp	.L3686
	.p2align 4,,10
	.p2align 3
.L3699:
	movq	32(%rdx), %rax
	movl	(%rdx), %edx
	leaq	.LC76(%rip), %rdi
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-48(%rbp), %rdx
	jmp	.L3684
	.p2align 4,,10
	.p2align 3
.L3682:
	leaq	-48(%rbp), %rdx
	leaq	96(%rdi), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler9LiveRangeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L3683
.L3701:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23596:
	.size	_ZN2v88internal8compiler19LinearScanAllocator16InactiveToActiveEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEENS1_16LifetimePositionE, .-_ZN2v88internal8compiler19LinearScanAllocator16InactiveToActiveEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEENS1_16LifetimePositionE
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator14ForwardStateToENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator14ForwardStateToENS1_16LifetimePositionE
	.type	_ZN2v88internal8compiler19LinearScanAllocator14ForwardStateToENS1_16LifetimePositionE, @function
_ZN2v88internal8compiler19LinearScanAllocator14ForwardStateToENS1_16LifetimePositionE:
.LFB23597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	cmpl	%esi, 160(%rdi)
	jg	.L3703
	movq	104(%rdi), %r14
	movq	112(%rdi), %rdx
	movl	$2147483647, 160(%rdi)
	leaq	.LC55(%rip), %r13
	cmpq	%rdx, %r14
	jne	.L3704
	.p2align 4,,10
	.p2align 3
.L3703:
	cmpl	%ebx, 164(%r12)
	jg	.L3702
	movq	136(%r12), %r14
	movq	144(%r12), %rdx
	movl	$2147483647, 164(%r12)
	leaq	.LC56(%rip), %r13
	cmpq	%r14, %rdx
	jne	.L3717
.L3702:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3744:
	.cfi_restore_state
	movq	(%r12), %rax
	testb	$4, 488(%rax)
	jne	.L3743
.L3706:
	leaq	8(%r14), %rsi
	cmpq	%rdx, %rsi
	je	.L3707
	subq	%rsi, %rdx
	movq	%r14, %rdi
	call	memmove@PLT
	movq	112(%r12), %rdx
.L3707:
	subq	$8, %rdx
	movq	%rdx, 112(%r12)
.L3708:
	cmpq	%rdx, %r14
	je	.L3703
.L3704:
	movq	(%r14), %rdi
	movq	8(%rdi), %rax
	cmpl	%ebx, 4(%rax)
	jle	.L3744
	movl	%ebx, %esi
	call	_ZNK2v88internal8compiler9LiveRange6CoversENS1_16LifetimePositionE
	testb	%al, %al
	jne	.L3709
	movl	%ebx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator16ActiveToInactiveEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEENS1_16LifetimePositionE
	movq	112(%r12), %rdx
	movq	%rax, %r14
	jmp	.L3708
	.p2align 4,,10
	.p2align 3
.L3746:
	movq	(%r12), %rax
	testb	$4, 488(%rax)
	jne	.L3745
.L3719:
	leaq	8(%r14), %rsi
	cmpq	%rdx, %rsi
	je	.L3720
	subq	%rsi, %rdx
	movq	%r14, %rdi
	call	memmove@PLT
	movq	144(%r12), %rdx
.L3720:
	subq	$8, %rdx
	movq	%rdx, 144(%r12)
.L3721:
	cmpq	%rdx, %r14
	je	.L3702
.L3717:
	movq	(%r14), %rdi
	movq	8(%rdi), %rax
	cmpl	%ebx, 4(%rax)
	jle	.L3746
	movl	%ebx, %esi
	call	_ZNK2v88internal8compiler9LiveRange6CoversENS1_16LifetimePositionE
	testb	%al, %al
	je	.L3722
	movl	%ebx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator16InactiveToActiveEN9__gnu_cxx17__normal_iteratorIPPNS1_9LiveRangeESt6vectorIS6_NS0_13ZoneAllocatorIS6_EEEEENS1_16LifetimePositionE
	movq	144(%r12), %rdx
	movq	%rax, %r14
	jmp	.L3721
	.p2align 4,,10
	.p2align 3
.L3722:
	movq	48(%rdi), %rax
	testq	%rax, %rax
	je	.L3741
	movl	(%rax), %edx
	cmpl	%ebx, %edx
	jle	.L3742
	movq	$0, 48(%rdi)
.L3741:
	movq	16(%rdi), %rax
	movl	(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L3747
	.p2align 4,,10
	.p2align 3
.L3726:
	movq	8(%rax), %rax
	movl	(%rax), %edx
.L3742:
	cmpl	%edx, %ebx
	jg	.L3726
.L3747:
	cmpl	%edx, 164(%r12)
	jle	.L3728
	movl	%edx, 164(%r12)
.L3728:
	movq	144(%r12), %rdx
	addq	$8, %r14
	jmp	.L3721
	.p2align 4,,10
	.p2align 3
.L3709:
	movq	48(%rdi), %rax
	testq	%rax, %rax
	je	.L3740
	cmpl	%ebx, (%rax)
	jg	.L3748
.L3711:
	movl	4(%rax), %edx
	cmpl	%edx, %ebx
	jle	.L3712
	.p2align 4,,10
	.p2align 3
.L3713:
	movq	8(%rax), %rax
	movl	4(%rax), %edx
	cmpl	%ebx, %edx
	jl	.L3713
.L3712:
	cmpl	%edx, 160(%r12)
	jle	.L3715
	movl	%edx, 160(%r12)
.L3715:
	movq	112(%r12), %rdx
	addq	$8, %r14
	jmp	.L3708
	.p2align 4,,10
	.p2align 3
.L3745:
	movq	32(%rdi), %rax
	movl	(%rdi), %edx
	movq	%r13, %rdi
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	144(%r12), %rdx
	jmp	.L3719
	.p2align 4,,10
	.p2align 3
.L3743:
	movq	32(%rdi), %rax
	movl	(%rdi), %edx
	movq	%r13, %rdi
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	112(%r12), %rdx
	jmp	.L3706
	.p2align 4,,10
	.p2align 3
.L3748:
	movq	$0, 48(%rdi)
.L3740:
	movq	16(%rdi), %rax
	jmp	.L3711
	.cfi_endproc
.LFE23597:
	.size	_ZN2v88internal8compiler19LinearScanAllocator14ForwardStateToENS1_16LifetimePositionE, .-_ZN2v88internal8compiler19LinearScanAllocator14ForwardStateToENS1_16LifetimePositionE
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE15_M_insert_equalIRKS4_EESt17_Rb_tree_iteratorIS4_EOT_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE15_M_insert_equalIRKS4_EESt17_Rb_tree_iteratorIS4_EOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE15_M_insert_equalIRKS4_EESt17_Rb_tree_iteratorIS4_EOT_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE15_M_insert_equalIRKS4_EESt17_Rb_tree_iteratorIS4_EOT_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE15_M_insert_equalIRKS4_EESt17_Rb_tree_iteratorIS4_EOT_:
.LFB27827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L3750
	movq	(%rsi), %rdi
	movq	16(%rdi), %rax
	movl	(%rax), %ecx
	jmp	.L3751
	.p2align 4,,10
	.p2align 3
.L3755:
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L3752
.L3777:
	movq	%rax, %r12
.L3751:
	movq	32(%r12), %rsi
	movq	16(%rsi), %rax
	movl	(%rax), %edx
	cmpl	%edx, %ecx
	setl	%al
	je	.L3776
.L3757:
	testb	%al, %al
	je	.L3755
.L3754:
	movq	16(%r12), %rax
	testq	%rax, %rax
	jne	.L3777
.L3752:
	movl	$1, %r8d
	cmpq	%r12, %r15
	je	.L3760
	call	_ZNK2v88internal8compiler9LiveRange23ShouldBeAllocatedBeforeEPKS2_
	movzbl	%al, %r8d
.L3760:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$39, %rax
	jbe	.L3778
	leaq	40(%r13), %rax
	movq	%rax, 16(%rdi)
.L3762:
	movq	(%r14), %rax
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3776:
	.cfi_restore_state
	movl	4(%rdi), %edx
	movl	4(%rsi), %eax
	shrl	$22, %edx
	shrl	$22, %eax
	andl	$63, %edx
	andl	$63, %eax
	cmpl	%eax, %edx
	jl	.L3754
	jg	.L3755
	movq	24(%rdi), %rax
	movq	24(%rsi), %rdx
	cmpq	%rdx, %rax
	je	.L3775
	testq	%rax, %rax
	je	.L3755
	testq	%rdx, %rdx
	je	.L3754
	movl	24(%rdx), %r8d
	movl	24(%rax), %edx
	cmpl	%edx, %r8d
	setg	%al
	jne	.L3757
.L3775:
	movq	32(%rdi), %rdx
	movq	32(%rsi), %rax
	movl	88(%rax), %eax
	cmpl	%eax, 88(%rdx)
	setl	%al
	jmp	.L3757
	.p2align 4,,10
	.p2align 3
.L3750:
	movq	%r15, %r12
	movl	$1, %r8d
	jmp	.L3760
	.p2align 4,,10
	.p2align 3
.L3778:
	movl	$40, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L3762
	.cfi_endproc
.LFE27827:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE15_M_insert_equalIRKS4_EESt17_Rb_tree_iteratorIS4_EOT_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE15_M_insert_equalIRKS4_EESt17_Rb_tree_iteratorIS4_EOT_
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE.str1.8,"aMS",@progbits,1
	.align 8
.LC77:
	.string	"Add live range %d:%d to unhandled\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE
	.type	_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE, @function
_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE:
.LFB23592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%rsi, -24(%rbp)
	testq	%rsi, %rsi
	je	.L3779
	cmpq	$0, 16(%rsi)
	movq	%rsi, %rax
	je	.L3779
	movq	(%rdi), %rdx
	movq	%rdi, %rbx
	testb	$4, 488(%rdx)
	jne	.L3789
.L3781:
	leaq	-24(%rbp), %rsi
	leaq	40(%rbx), %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE15_M_insert_equalIRKS4_EESt17_Rb_tree_iteratorIS4_EOT_
.L3779:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3789:
	.cfi_restore_state
	movq	32(%rsi), %rdx
	leaq	.LC77(%rip), %rdi
	movl	88(%rdx), %esi
	movl	(%rax), %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3781
	.cfi_endproc
.LFE23592:
	.size	_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE, .-_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator24TrySplitAndSpillSplinterEPNS1_9LiveRangeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator24TrySplitAndSpillSplinterEPNS1_9LiveRangeE
	.type	_ZN2v88internal8compiler19LinearScanAllocator24TrySplitAndSpillSplinterEPNS1_9LiveRangeE, @function
_ZN2v88internal8compiler19LinearScanAllocator24TrySplitAndSpillSplinterEPNS1_9LiveRangeE:
.LFB23588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	16(%rsi), %r8
	movq	%rsi, %r12
	movq	56(%rsi), %rax
	movl	(%r8), %ecx
	testq	%rax, %rax
	je	.L3791
	movl	24(%rax), %edx
	cmpl	%edx, %ecx
	jge	.L3796
.L3791:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L3827
	.p2align 4,,10
	.p2align 3
.L3793:
	movq	$0, 56(%r12)
.L3794:
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3829:
	.cfi_restore_state
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3793
.L3827:
	movl	24(%rax), %edx
.L3796:
	cmpl	%edx, %ecx
	jg	.L3829
	movq	%rax, 56(%r12)
	jmp	.L3798
	.p2align 4,,10
	.p2align 3
.L3830:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3794
.L3798:
	movzbl	28(%rax), %edx
	andl	$3, %edx
	cmpb	$2, %dl
	jne	.L3830
	movq	24(%r12), %rcx
	leaq	.L3801(%rip), %rdi
	testq	%rcx, %rcx
	je	.L3828
	.p2align 4,,10
	.p2align 3
.L3806:
	movq	8(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L3799
	movl	28(%rcx), %edx
	shrl	$2, %edx
	andl	$7, %edx
	cmpb	$4, %dl
	ja	.L3800
	movzbl	%dl, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator24TrySplitAndSpillSplinterEPNS1_9LiveRangeE,"a",@progbits
	.align 4
	.align 4
.L3801:
	.long	.L3799-.L3801
	.long	.L3804-.L3801
	.long	.L3803-.L3801
	.long	.L3802-.L3801
	.long	.L3799-.L3801
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator24TrySplitAndSpillSplinterEPNS1_9LiveRangeE
	.p2align 4,,10
	.p2align 3
.L3802:
	cmpl	$32, 48(%rsi)
	jne	.L3804
	.p2align 4,,10
	.p2align 3
.L3799:
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L3806
.L3828:
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3803:
	.cfi_restore_state
	movl	28(%rsi), %edx
	shrl	$6, %edx
	andl	$63, %edx
	cmpl	$32, %edx
	je	.L3799
.L3804:
	movl	24(%rax), %edx
	andl	$-2, %edx
	subl	$2, %edx
	cmpl	(%r8), %edx
	jle	.L3828
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE
	jmp	.L3794
.L3800:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23588:
	.size	_ZN2v88internal8compiler19LinearScanAllocator24TrySplitAndSpillSplinterEPNS1_9LiveRangeE, .-_ZN2v88internal8compiler19LinearScanAllocator24TrySplitAndSpillSplinterEPNS1_9LiveRangeE
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator18TryAllocateFreeRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC78:
	.string	"Assigning free reg %s to live range %d:%d\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator18TryAllocateFreeRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator18TryAllocateFreeRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE
	.type	_ZN2v88internal8compiler19LinearScanAllocator18TryAllocateFreeRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE, @function
_ZN2v88internal8compiler19LinearScanAllocator18TryAllocateFreeRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE:
.LFB23604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	4(%rsi), %edx
	shrl	$22, %edx
	andl	$63, %edx
	cmpb	$32, %dl
	je	.L3832
	movzbl	%dl, %edx
	.p2align 4,,10
	.p2align 3
.L3833:
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator34PickRegisterThatIsAvailableLongestEPNS1_9LiveRangeEiRKNS0_6VectorINS1_16LifetimePositionEEE
	movq	16(%r12), %rcx
	movslq	%eax, %rbx
	movq	(%r14), %rax
	movq	%rbx, %r15
	movl	(%rax,%rbx,4), %edx
	xorl	%eax, %eax
	cmpl	%edx, (%rcx)
	jge	.L3831
	movq	8(%r12), %rax
	cmpl	4(%rax), %edx
	jl	.L3843
	movq	0(%r13), %rax
	testb	$4, 488(%rax)
	je	.L3845
.L3844:
	movq	32(%r12), %rax
	movl	(%r12), %ecx
	leaq	.LC26(%rip), %rsi
	movl	88(%rax), %edx
	cmpl	$32, %r15d
	je	.L3847
	movl	8(%r13), %eax
	leaq	.LC54(%rip), %rsi
	testl	%eax, %eax
	jne	.L3848
	cmpl	$-1, %r15d
	je	.L3847
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
.L3847:
	leaq	.LC78(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L3845:
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator28SetLiveRangeAssignedRegisterEPNS1_9LiveRangeEi
	movl	$1, %eax
.L3831:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3832:
	.cfi_restore_state
	movq	24(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L3834
	leaq	.L3837(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L3841:
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L3835
	movl	28(%rcx), %eax
	shrl	$2, %eax
	andl	$7, %eax
	cmpb	$4, %al
	ja	.L3836
	movzbl	%al, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator18TryAllocateFreeRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE,"a",@progbits
	.align 4
	.align 4
.L3837:
	.long	.L3835-.L3837
	.long	.L3840-.L3837
	.long	.L3839-.L3837
	.long	.L3838-.L3837
	.long	.L3835-.L3837
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator18TryAllocateFreeRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE
	.p2align 4,,10
	.p2align 3
.L3839:
	movl	28(%rdx), %edx
	shrl	$6, %edx
	andl	$63, %edx
	cmpl	$32, %edx
	jne	.L3833
	.p2align 4,,10
	.p2align 3
.L3835:
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L3841
.L3834:
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L3876
	movl	116(%rax), %edx
	jmp	.L3833
	.p2align 4,,10
	.p2align 3
.L3840:
	movq	(%rdx), %rdx
	sarq	$35, %rdx
	jmp	.L3833
	.p2align 4,,10
	.p2align 3
.L3838:
	movl	48(%rdx), %edx
	cmpl	$32, %edx
	jne	.L3833
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L3841
	jmp	.L3834
	.p2align 4,,10
	.p2align 3
.L3843:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator23TryAllocatePreferredRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE
	testb	%al, %al
	jne	.L3831
	movq	0(%r13), %rax
	testb	$4, 488(%rax)
	jne	.L3844
	jmp	.L3845
	.p2align 4,,10
	.p2align 3
.L3848:
	cmpl	$-1, %r15d
	je	.L3847
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	jmp	.L3847
.L3876:
	movl	$32, %edx
	jmp	.L3833
.L3836:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23604:
	.size	_ZN2v88internal8compiler19LinearScanAllocator18TryAllocateFreeRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE, .-_ZN2v88internal8compiler19LinearScanAllocator18TryAllocateFreeRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator17SpillBetweenUntilEPNS1_9LiveRangeENS1_16LifetimePositionES5_S5_NS1_22RegisterAllocationData9SpillModeE.str1.1,"aMS",@progbits,1
.LC79:
	.string	"start < end"
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator17SpillBetweenUntilEPNS1_9LiveRangeENS1_16LifetimePositionES5_S5_NS1_22RegisterAllocationData9SpillModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC80:
	.string	"Setting control flow hint for %d:%d to %s\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator17SpillBetweenUntilEPNS1_9LiveRangeENS1_16LifetimePositionES5_S5_NS1_22RegisterAllocationData9SpillModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator17SpillBetweenUntilEPNS1_9LiveRangeENS1_16LifetimePositionES5_S5_NS1_22RegisterAllocationData9SpillModeE
	.type	_ZN2v88internal8compiler19LinearScanAllocator17SpillBetweenUntilEPNS1_9LiveRangeENS1_16LifetimePositionES5_S5_NS1_22RegisterAllocationData9SpillModeE, @function
_ZN2v88internal8compiler19LinearScanAllocator17SpillBetweenUntilEPNS1_9LiveRangeENS1_16LifetimePositionES5_S5_NS1_22RegisterAllocationData9SpillModeE:
.LFB23610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	%edx, %r8d
	jle	.L3899
	movl	%r8d, -52(%rbp)
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movl	%r9d, %r14d
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	movl	-52(%rbp), %r8d
	movq	%rax, %r15
	movq	16(%rax), %rax
	movl	(%rax), %r13d
	cmpl	%r13d, %r8d
	jle	.L3879
	movl	-56(%rbp), %ecx
	orl	$1, %r13d
	movq	(%r12), %rax
	cmpl	%ecx, %r13d
	cmovl	%ecx, %r13d
	movl	%r8d, %ecx
	andl	$-2, %ecx
	leal	-1(%rcx), %edx
	cmpl	%r13d, %edx
	cmovl	%r13d, %edx
	andl	$2, %r8d
	movl	%edx, %r9d
	je	.L3900
	testb	$4, 488(%rax)
	jne	.L3901
	.p2align 4,,10
	.p2align 3
.L3883:
	movl	%r9d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17RegisterAllocator19FindOptimalSplitPosENS1_16LifetimePositionES3_
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	movq	%rax, %r13
	movq	16(%r15), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	leal	3(%rax), %esi
	cmovns	%eax, %esi
	movq	(%r12), %rax
	movq	16(%rax), %rdi
	sarl	$2, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	cmpb	$0, 120(%rax)
	je	.L3884
	movl	4(%rbx), %eax
	movq	(%r12), %rdx
	shrl	$22, %eax
	andl	$63, %eax
	testb	$4, 488(%rdx)
	je	.L3885
	movzbl	%al, %edx
	leaq	.LC26(%rip), %rcx
	cmpb	$32, %al
	je	.L3886
	movl	8(%r12), %eax
	testl	%eax, %eax
	jne	.L3887
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rax
	movq	(%rax,%rdx,8), %rcx
.L3886:
	movq	32(%r13), %rax
	movl	0(%r13), %edx
	leaq	.LC80(%rip), %rdi
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	4(%rbx), %eax
	shrl	$22, %eax
	andl	$63, %eax
.L3885:
	movl	4(%r13), %edx
	movzbl	%al, %eax
	sall	$22, %eax
	andl	$-264241153, %edx
	orl	%edx, %eax
	movl	%eax, 4(%r13)
.L3884:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE
	cmpq	%r13, %r15
	je	.L3877
	addq	$24, %rsp
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	.p2align 4,,10
	.p2align 3
.L3877:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3879:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%r15, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE
	.p2align 4,,10
	.p2align 3
.L3901:
	.cfi_restore_state
	movq	32(%r15), %rax
	movl	(%r15), %edx
	movl	%r9d, %r8d
	movl	%r13d, %ecx
	leaq	.LC46(%rip), %rdi
	movl	%r9d, -52(%rbp)
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	-52(%rbp), %r9d
	jmp	.L3883
	.p2align 4,,10
	.p2align 3
.L3900:
	testl	%ecx, %ecx
	movl	%edx, -60(%rbp)
	leal	3(%rcx), %edx
	movq	16(%rax), %rdi
	cmovns	%ecx, %edx
	movl	%ecx, -56(%rbp)
	sarl	$2, %edx
	movl	%edx, %esi
	movl	%edx, -52(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movl	-52(%rbp), %edx
	movl	-56(%rbp), %ecx
	cmpl	112(%rax), %edx
	je	.L3882
	movq	(%r12), %rax
	movl	-60(%rbp), %r9d
	testb	$4, 488(%rax)
	je	.L3883
	jmp	.L3901
	.p2align 4,,10
	.p2align 3
.L3882:
	cmpl	%r13d, %ecx
	movq	(%r12), %rax
	cmovl	%r13d, %ecx
	movl	%ecx, %r9d
	testb	$4, 488(%rax)
	je	.L3883
	jmp	.L3901
	.p2align 4,,10
	.p2align 3
.L3899:
	leaq	.LC79(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3887:
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rax
	movq	(%rax,%rdx,8), %rcx
	jmp	.L3886
	.cfi_endproc
.LFE23610:
	.size	_ZN2v88internal8compiler19LinearScanAllocator17SpillBetweenUntilEPNS1_9LiveRangeENS1_16LifetimePositionES5_S5_NS1_22RegisterAllocationData9SpillModeE, .-_ZN2v88internal8compiler19LinearScanAllocator17SpillBetweenUntilEPNS1_9LiveRangeENS1_16LifetimePositionES5_S5_NS1_22RegisterAllocationData9SpillModeE
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator12SpillBetweenEPNS1_9LiveRangeENS1_16LifetimePositionES5_NS1_22RegisterAllocationData9SpillModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator12SpillBetweenEPNS1_9LiveRangeENS1_16LifetimePositionES5_NS1_22RegisterAllocationData9SpillModeE
	.type	_ZN2v88internal8compiler19LinearScanAllocator12SpillBetweenEPNS1_9LiveRangeENS1_16LifetimePositionES5_NS1_22RegisterAllocationData9SpillModeE, @function
_ZN2v88internal8compiler19LinearScanAllocator12SpillBetweenEPNS1_9LiveRangeENS1_16LifetimePositionES5_NS1_22RegisterAllocationData9SpillModeE:
.LFB23609:
	.cfi_startproc
	endbr64
	movl	%r8d, %r9d
	movl	%ecx, %r8d
	movl	%edx, %ecx
	jmp	_ZN2v88internal8compiler19LinearScanAllocator17SpillBetweenUntilEPNS1_9LiveRangeENS1_16LifetimePositionES5_S5_NS1_22RegisterAllocationData9SpillModeE
	.cfi_endproc
.LFE23609:
	.size	_ZN2v88internal8compiler19LinearScanAllocator12SpillBetweenEPNS1_9LiveRangeENS1_16LifetimePositionES5_NS1_22RegisterAllocationData9SpillModeE, .-_ZN2v88internal8compiler19LinearScanAllocator12SpillBetweenEPNS1_9LiveRangeENS1_16LifetimePositionES5_NS1_22RegisterAllocationData9SpillModeE
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator19TryReuseSpillForPhiEPNS1_17TopLevelLiveRangeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler19LinearScanAllocator19TryReuseSpillForPhiEPNS1_17TopLevelLiveRangeE.part.0, @function
_ZN2v88internal8compiler19LinearScanAllocator19TryReuseSpillForPhiEPNS1_17TopLevelLiveRangeE.part.0:
.LFB31981:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movl	88(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	80(%rsi), %rax
	leaq	56(%r12), %rsi
	movq	%rax, -104(%rbp)
	movq	64(%r12), %rax
	testq	%rax, %rax
	je	.L3904
	movq	%rsi, %rcx
	jmp	.L3905
	.p2align 4,,10
	.p2align 3
.L3962:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3906
.L3905:
	cmpl	32(%rax), %edx
	jle	.L3962
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L3905
.L3906:
	cmpq	%rcx, %rsi
	je	.L3904
	cmpl	32(%rcx), %edx
	cmovge	%rcx, %rsi
.L3904:
	movq	40(%rsi), %rax
	movq	$0, -72(%rbp)
	xorl	%ebx, %ebx
	movq	(%rax), %r14
	movq	8(%rax), %r15
	leaq	-64(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	24(%r14), %rdx
	cmpq	32(%r14), %rdx
	je	.L3921
	movq	%r11, -120(%rbp)
	jmp	.L3909
	.p2align 4,,10
	.p2align 3
.L3915:
	movq	24(%r14), %rdx
	movq	32(%r14), %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$2, %rax
	cmpq	%rbx, %rax
	jbe	.L3920
	movq	(%r10), %r12
.L3909:
	movq	176(%r12), %rsi
	movq	168(%r12), %rdi
	leaq	0(,%rbx,4), %r9
	movl	(%rdx,%rbx,4), %r13d
	movq	%rsi, %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpl	%eax, %r13d
	jge	.L3963
.L3911:
	movslq	%r13d, %rcx
	movq	(%rdi,%rcx,8), %rax
	testq	%rax, %rax
	je	.L3964
.L3914:
	movq	32(%rax), %rdx
	testb	$64, 4(%rdx)
	je	.L3915
	movq	(%r10), %rdx
	movq	40(%r15), %rcx
	movq	16(%rdx), %rdx
	movslq	(%rcx,%r9), %rsi
	movq	16(%rdx), %rdx
	movq	8(%rdx), %rcx
	movq	16(%rdx), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L3965
	movq	(%rcx,%rsi,8), %rdx
	movl	116(%rdx), %edx
	leal	-2(,%rdx,4), %ecx
.L3919:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L3917
	cmpl	(%rdx), %ecx
	jl	.L3917
	movq	8(%rax), %rdx
	cmpl	4(%rdx), %ecx
	jge	.L3917
	testb	$1, 4(%rax)
	je	.L3915
	movq	-104(%rbp), %rdi
	cmpq	80(%rax), %rdi
	sete	%al
	movzbl	%al, %eax
	addq	%rax, -72(%rbp)
	jmp	.L3915
	.p2align 4,,10
	.p2align 3
.L3917:
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L3919
	jmp	.L3915
	.p2align 4,,10
	.p2align 3
.L3963:
	leal	1(%r13), %edx
	movq	$0, -64(%rbp)
	movslq	%edx, %rdx
	cmpq	%rax, %rdx
	ja	.L3966
	jnb	.L3911
	leaq	(%rdi,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L3911
	movslq	%r13d, %rcx
	movq	%rax, 176(%r12)
	movq	(%rdi,%rcx,8), %rax
	testq	%rax, %rax
	jne	.L3914
	.p2align 4,,10
	.p2align 3
.L3964:
	movq	16(%r12), %rdi
	movl	%r13d, %esi
	movq	%r10, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%r9, -80(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal8compiler22RegisterAllocationData12NewLiveRangeEiNS0_21MachineRepresentationE
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %r10
	movq	168(%r12), %rdx
	movq	-80(%rbp), %r9
	movq	%rax, (%rdx,%rcx,8)
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L3920:
	movq	-72(%rbp), %r15
	movq	-120(%rbp), %r11
	addq	%r15, %r15
	cmpq	%r15, %rax
	jnb	.L3921
	movq	16(%r11), %rsi
	movl	(%rsi), %ecx
	movl	%ecx, %eax
	andl	$-2, %eax
	addl	$2, %eax
	testb	$2, %cl
	cmove	%eax, %ecx
	movq	56(%r11), %rax
	testq	%rax, %rax
	je	.L3923
	movl	24(%rax), %edx
	cmpl	%edx, %ecx
	jge	.L3928
.L3923:
	movq	24(%r11), %rax
	testq	%rax, %rax
	jne	.L3961
	jmp	.L3925
	.p2align 4,,10
	.p2align 3
.L3967:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3925
.L3961:
	movl	24(%rax), %edx
.L3928:
	cmpl	%edx, %ecx
	jg	.L3967
	movq	%rax, 56(%r11)
	jmp	.L3930
	.p2align 4,,10
	.p2align 3
.L3968:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L3926
.L3930:
	testb	$32, 28(%rax)
	je	.L3968
	movl	(%rsi), %edx
	movl	24(%rax), %r8d
	movl	%edx, %eax
	andl	$-2, %eax
	addl	$2, %eax
	cmpl	%r8d, %eax
	jl	.L3969
.L3921:
	xorl	%eax, %eax
.L3903:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L3970
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3966:
	.cfi_restore_state
	movq	-112(%rbp), %rcx
	leaq	160(%r12), %rdi
	subq	%rax, %rdx
	movq	%r10, -88(%rbp)
	movq	%r9, -80(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	movq	-80(%rbp), %r9
	movq	-88(%rbp), %r10
	movq	168(%r12), %rdi
	jmp	.L3911
	.p2align 4,,10
	.p2align 3
.L3969:
	xorl	%r9d, %r9d
	movl	%edx, %ecx
	movq	%r11, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator17SpillBetweenUntilEPNS1_9LiveRangeENS1_16LifetimePositionES5_S5_NS1_22RegisterAllocationData9SpillModeE
	movl	$1, %eax
	jmp	.L3903
	.p2align 4,,10
	.p2align 3
.L3925:
	movq	$0, 56(%r11)
	.p2align 4,,10
	.p2align 3
.L3926:
	xorl	%edx, %edx
	movq	%r11, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	movl	$1, %eax
	jmp	.L3903
.L3965:
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L3970:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE31981:
	.size	_ZN2v88internal8compiler19LinearScanAllocator19TryReuseSpillForPhiEPNS1_17TopLevelLiveRangeE.part.0, .-_ZN2v88internal8compiler19LinearScanAllocator19TryReuseSpillForPhiEPNS1_17TopLevelLiveRangeE.part.0
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator19TryReuseSpillForPhiEPNS1_17TopLevelLiveRangeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator19TryReuseSpillForPhiEPNS1_17TopLevelLiveRangeE
	.type	_ZN2v88internal8compiler19LinearScanAllocator19TryReuseSpillForPhiEPNS1_17TopLevelLiveRangeE, @function
_ZN2v88internal8compiler19LinearScanAllocator19TryReuseSpillForPhiEPNS1_17TopLevelLiveRangeE:
.LFB23607:
	.cfi_startproc
	endbr64
	testb	$8, 4(%rsi)
	je	.L3972
	jmp	_ZN2v88internal8compiler19LinearScanAllocator19TryReuseSpillForPhiEPNS1_17TopLevelLiveRangeE.part.0
	.p2align 4,,10
	.p2align 3
.L3972:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23607:
	.size	_ZN2v88internal8compiler19LinearScanAllocator19TryReuseSpillForPhiEPNS1_17TopLevelLiveRangeE, .-_ZN2v88internal8compiler19LinearScanAllocator19TryReuseSpillForPhiEPNS1_17TopLevelLiveRangeE
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator25SplitAndSpillIntersectingEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator25SplitAndSpillIntersectingEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	.type	_ZN2v88internal8compiler19LinearScanAllocator25SplitAndSpillIntersectingEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE, @function
_ZN2v88internal8compiler19LinearScanAllocator25SplitAndSpillIntersectingEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE:
.LFB23606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -72(%rbp)
	movq	104(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	4(%rsi), %eax
	shrl	$7, %eax
	andl	$63, %eax
	movl	%eax, -68(%rbp)
	movq	16(%rsi), %rax
	movl	(%rax), %eax
	movl	%eax, -84(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -96(%rbp)
	cmpq	112(%rdi), %r14
	je	.L3975
	movq	%rsi, -80(%rbp)
	jmp	.L3974
	.p2align 4,,10
	.p2align 3
.L4047:
	movq	112(%r13), %rdx
	movq	%r15, %r14
	cmpq	%rdx, %r14
	je	.L4039
.L3974:
	movq	(%r14), %r12
	leaq	8(%r14), %r15
	movl	4(%r12), %eax
	shrl	$7, %eax
	andl	$63, %eax
	cmpl	%eax, -68(%rbp)
	jne	.L4047
	movq	-80(%rbp), %rax
	movq	56(%r12), %rbx
	movq	16(%rax), %rax
	movl	(%rax), %edx
	testq	%rbx, %rbx
	je	.L3978
	movl	24(%rbx), %eax
	cmpl	%eax, %edx
	jge	.L3984
.L3978:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	jne	.L4044
	jmp	.L3983
	.p2align 4,,10
	.p2align 3
.L4048:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3983
.L4044:
	movl	24(%rbx), %eax
.L3984:
	cmpl	%eax, %edx
	jg	.L4048
	movq	%rbx, 56(%r12)
	jmp	.L3986
	.p2align 4,,10
	.p2align 3
.L4049:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3981
.L3986:
	movzbl	28(%rbx), %eax
	andl	$3, %eax
	cmpb	$2, %al
	jne	.L4049
	movl	-72(%rbp), %ecx
	movq	-96(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	-84(%rbp), %edx
	movq	$0, -64(%rbp)
	call	_ZN2v88internal8compiler17RegisterAllocator22FindOptimalSpillingPosEPNS1_9LiveRangeENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeEPS4_
	movq	-64(%rbp), %rcx
	movl	%eax, %r11d
	cmpq	%rcx, %r12
	je	.L3990
	testb	$1, 4(%rcx)
	je	.L4050
.L3987:
	movq	40(%rcx), %rax
	cmpq	%rax, %r12
	je	.L3992
	.p2align 4,,10
	.p2align 3
.L3988:
	movl	4(%rax), %edx
	testb	$1, %dl
	jne	.L3991
	andl	$-8066, %edx
	orl	$4097, %edx
	movl	%edx, 4(%rax)
	movq	40(%rax), %rax
	cmpq	%rax, %r12
	jne	.L3988
.L3992:
	testq	%rbx, %rbx
	je	.L3989
.L3990:
	movq	-80(%rbp), %rax
	movl	-72(%rbp), %r9d
	movl	%r11d, %edx
	movq	%r12, %rsi
	movl	24(%rbx), %r8d
	movq	%r13, %rdi
	movq	16(%rax), %rax
	movl	(%rax), %ecx
	call	_ZN2v88internal8compiler19LinearScanAllocator17SpillBetweenUntilEPNS1_9LiveRangeENS1_16LifetimePositionES5_S5_NS1_22RegisterAllocationData9SpillModeE
	movq	0(%r13), %rax
	testb	$4, 488(%rax)
	jne	.L4051
.L3995:
	movq	112(%r13), %rdx
	cmpq	%rdx, %r15
	je	.L3996
	subq	%r15, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	memmove@PLT
	movq	112(%r13), %rdx
.L3996:
	subq	$8, %rdx
	movq	%rdx, 112(%r13)
	cmpq	%rdx, %r14
	jne	.L3974
.L4039:
	movq	-80(%rbp), %r15
.L3975:
	movq	136(%r13), %r12
	movq	144(%r13), %rdx
	cmpq	%rdx, %r12
	jne	.L3997
	jmp	.L3973
	.p2align 4,,10
	.p2align 3
.L4017:
	movq	%rbx, %r12
.L3999:
	cmpq	%rdx, %r12
	je	.L3973
.L3997:
	movq	(%r12), %r14
	leaq	8(%r12), %rbx
	movq	32(%r14), %rax
	movl	88(%rax), %eax
	testl	%eax, %eax
	js	.L4017
	movl	4(%r14), %eax
	shrl	$7, %eax
	andl	$63, %eax
	cmpl	%eax, -68(%rbp)
	jne	.L4017
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler9LiveRange17FirstIntersectionEPS2_
	cmpl	$-1, %eax
	je	.L4000
	movq	16(%r15), %rdx
	movl	(%rdx), %esi
	movq	56(%r14), %rdx
	testq	%rdx, %rdx
	je	.L4001
	movl	24(%rdx), %ecx
	cmpl	%ecx, %esi
	jge	.L4007
.L4001:
	movq	24(%r14), %rdx
	testq	%rdx, %rdx
	jne	.L4045
	.p2align 4,,10
	.p2align 3
.L4006:
	movq	$0, 56(%r14)
	.p2align 4,,10
	.p2align 3
.L4004:
	movl	-84(%rbp), %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	movl	-72(%rbp), %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	movq	0(%r13), %rax
	testb	$4, 488(%rax)
	je	.L4010
	.p2align 4,,10
	.p2align 3
.L4054:
	movq	(%r12), %rax
	leaq	.LC56(%rip), %rdi
	movq	32(%rax), %rdx
	movl	88(%rdx), %esi
	movl	(%rax), %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L4010
	.p2align 4,,10
	.p2align 3
.L3991:
	movq	40(%rax), %rax
	cmpq	%rax, %r12
	jne	.L3988
	testq	%rbx, %rbx
	jne	.L3990
.L3989:
	movl	%r11d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	movl	-72(%rbp), %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	movq	0(%r13), %rax
	testb	$4, 488(%rax)
	je	.L3995
.L4051:
	movq	(%r14), %rax
	leaq	.LC55(%rip), %rdi
	movq	32(%rax), %rdx
	movl	88(%rdx), %esi
	movl	(%rax), %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L3995
	.p2align 4,,10
	.p2align 3
.L3983:
	movq	$0, 56(%r12)
	.p2align 4,,10
	.p2align 3
.L3981:
	movl	-72(%rbp), %ecx
	movq	-96(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	-84(%rbp), %edx
	movq	$0, -64(%rbp)
	xorl	%ebx, %ebx
	call	_ZN2v88internal8compiler17RegisterAllocator22FindOptimalSpillingPosEPNS1_9LiveRangeENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeEPS4_
	movq	-64(%rbp), %rcx
	movl	%eax, %r11d
	cmpq	%r12, %rcx
	je	.L3989
	testb	$1, 4(%rcx)
	jne	.L3987
.L4050:
	movl	%r11d, %edx
	movq	%rcx, %rsi
	movq	%r13, %rdi
	movl	%r11d, -88(%rbp)
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	movl	-88(%rbp), %r11d
	movq	-104(%rbp), %rcx
	jmp	.L3987
	.p2align 4,,10
	.p2align 3
.L4052:
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L4006
.L4045:
	movl	24(%rdx), %ecx
.L4007:
	cmpl	%ecx, %esi
	jg	.L4052
	movq	%rdx, 56(%r14)
	jmp	.L4009
	.p2align 4,,10
	.p2align 3
.L4053:
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L4004
.L4009:
	movzbl	28(%rdx), %ecx
	andl	$3, %ecx
	cmpb	$2, %cl
	jne	.L4053
	cmpl	%eax, 24(%rdx)
	movl	-84(%rbp), %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	cmovle	24(%rdx), %eax
	movl	-72(%rbp), %r9d
	movl	%ecx, %edx
	movl	%eax, %r8d
	call	_ZN2v88internal8compiler19LinearScanAllocator17SpillBetweenUntilEPNS1_9LiveRangeENS1_16LifetimePositionES5_S5_NS1_22RegisterAllocationData9SpillModeE
	movq	0(%r13), %rax
	testb	$4, 488(%rax)
	jne	.L4054
.L4010:
	movq	144(%r13), %rdx
	cmpq	%rbx, %rdx
	je	.L4011
	subq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	memmove@PLT
	movq	144(%r13), %rbx
.L4011:
	leaq	-8(%rbx), %rdx
	movq	%rdx, 144(%r13)
	cmpq	%rdx, %r12
	jne	.L3997
	.p2align 4,,10
	.p2align 3
.L3973:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4055
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4000:
	.cfi_restore_state
	movq	144(%r13), %rdx
	movq	%rbx, %r12
	jmp	.L3999
.L4055:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23606:
	.size	_ZN2v88internal8compiler19LinearScanAllocator25SplitAndSpillIntersectingEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE, .-_ZN2v88internal8compiler19LinearScanAllocator25SplitAndSpillIntersectingEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator18AllocateBlockedRegEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC81:
	.string	"Assigning blocked reg %s to live range %d:%d\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator18AllocateBlockedRegEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator18AllocateBlockedRegEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	.type	_ZN2v88internal8compiler19LinearScanAllocator18AllocateBlockedRegEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE, @function
_ZN2v88internal8compiler19LinearScanAllocator18AllocateBlockedRegEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE:
.LFB23605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -56
	movl	%edx, -388(%rbp)
	movq	56(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movl	(%rax), %edx
	testq	%rbx, %rbx
	je	.L4057
	movl	24(%rbx), %eax
	cmpl	%eax, %edx
	jge	.L4062
.L4057:
	movq	24(%r14), %rbx
	testq	%rbx, %rbx
	jne	.L4184
	.p2align 4,,10
	.p2align 3
.L4059:
	movq	$0, 56(%r14)
	.p2align 4,,10
	.p2align 3
.L4060:
	movl	-388(%rbp), %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	jmp	.L4056
	.p2align 4,,10
	.p2align 3
.L4187:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L4059
.L4184:
	movl	24(%rbx), %eax
.L4062:
	cmpl	%eax, %edx
	jg	.L4187
	movq	%rbx, 56(%r14)
	jmp	.L4064
	.p2align 4,,10
	.p2align 3
.L4188:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L4060
.L4064:
	movzbl	28(%rbx), %eax
	andl	$3, %eax
	cmpb	$2, %al
	jne	.L4188
	movdqa	.LC57(%rip), %xmm0
	leaq	-336(%rbp), %rax
	movq	112(%r12), %r11
	movq	$32, -344(%rbp)
	movq	104(%r12), %r9
	movq	%rax, -352(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	$32, -200(%rbp)
	movaps	%xmm0, -336(%rbp)
	movaps	%xmm0, -320(%rbp)
	movaps	%xmm0, -304(%rbp)
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm0, -272(%rbp)
	movaps	%xmm0, -256(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	%r11, %r9
	jne	.L4086
	jmp	.L4087
	.p2align 4,,10
	.p2align 3
.L4067:
	movl	$0, (%r10)
	movq	-208(%rbp), %rax
	addq	$8, %r9
	movl	$0, (%rax,%rsi,4)
	cmpq	%r9, %r11
	je	.L4087
.L4086:
	movq	(%r9), %r8
	movq	-352(%rbp), %rax
	movl	4(%r8), %esi
	shrl	$7, %esi
	andl	$63, %esi
	leaq	(%rax,%rsi,4), %r10
	movq	32(%r8), %rax
	movl	88(%rax), %edi
	testl	%edi, %edi
	js	.L4067
	movq	16(%r14), %rax
	movl	(%rax), %edi
	movq	56(%r8), %rax
	testq	%rax, %rax
	je	.L4068
	movl	24(%rax), %edx
	cmpl	%edx, %edi
	jge	.L4074
.L4068:
	movq	24(%r8), %rax
	testq	%rax, %rax
	jne	.L4185
	jmp	.L4070
	.p2align 4,,10
	.p2align 3
.L4189:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L4073
.L4185:
	movl	24(%rax), %edx
.L4074:
	cmpl	%edx, %edi
	jg	.L4189
	movq	%rax, 56(%r8)
	movq	%rax, %rdx
	jmp	.L4076
	.p2align 4,,10
	.p2align 3
.L4190:
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L4120
.L4076:
	movzbl	28(%rdx), %ecx
	andl	$3, %ecx
	cmpb	$2, %cl
	jne	.L4190
	andl	$-2, %edi
	addl	$3, %edi
	cmpl	%edi, 24(%rdx)
	jle	.L4067
.L4120:
	movq	16(%r14), %rdx
	movl	(%rdx), %ecx
	movl	24(%rax), %edx
	cmpl	%ecx, %edx
	jle	.L4083
.L4124:
	movq	24(%r8), %rax
	testq	%rax, %rax
	jne	.L4186
	jmp	.L4082
	.p2align 4,,10
	.p2align 3
.L4191:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L4082
.L4186:
	movl	24(%rax), %edx
.L4083:
	cmpl	%edx, %ecx
	jg	.L4191
	movq	%rax, 56(%r8)
	jmp	.L4085
	.p2align 4,,10
	.p2align 3
.L4192:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L4080
.L4085:
	testb	$32, 28(%rax)
	je	.L4192
	movl	24(%rax), %eax
.L4118:
	addq	$8, %r9
	movl	%eax, (%r10)
	cmpq	%r9, %r11
	jne	.L4086
.L4087:
	movq	144(%r12), %rax
	movq	136(%r12), %r13
	movq	%rax, -376(%rbp)
	cmpq	%r13, %rax
	jne	.L4095
	jmp	.L4066
	.p2align 4,,10
	.p2align 3
.L4090:
	movq	-352(%rbp), %rax
	cmpl	%esi, (%rax,%rdx,4)
	jl	.L4094
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler9LiveRange17FirstIntersectionEPS2_
	cmpl	$-1, %eax
	je	.L4094
	addq	-352(%rbp), %r15
	cmpl	%eax, (%r15)
	cmovle	(%r15), %eax
	movl	%eax, (%r15)
.L4094:
	addq	$8, %r13
	cmpq	%r13, -376(%rbp)
	je	.L4066
.L4095:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movl	4(%rdi), %edx
	movl	(%rax), %esi
	movq	32(%rdi), %rax
	shrl	$7, %edx
	andl	$63, %edx
	movl	88(%rax), %ecx
	leaq	0(,%rdx,4), %r15
	testl	%ecx, %ecx
	jns	.L4090
	movq	-208(%rbp), %rax
	cmpl	%esi, (%rax,%rdx,4)
	jl	.L4094
	movq	%r14, %rsi
	movq	%rdx, -384(%rbp)
	call	_ZNK2v88internal8compiler9LiveRange17FirstIntersectionEPS2_
	movq	-384(%rbp), %rdx
	cmpl	$-1, %eax
	je	.L4094
	movq	-208(%rbp), %rsi
	addq	%r15, %rsi
	cmpl	%eax, (%rsi)
	cmovle	(%rsi), %eax
	movl	%eax, (%rsi)
	movq	-208(%rbp), %rsi
	addq	-352(%rbp), %r15
	movl	(%r15), %eax
	cmpl	%eax, (%rsi,%rdx,4)
	cmovle	(%rsi,%rdx,4), %eax
	addq	$8, %r13
	movl	%eax, (%r15)
	cmpq	%r13, -376(%rbp)
	jne	.L4095
	.p2align 4,,10
	.p2align 3
.L4066:
	movl	$32, -356(%rbp)
	movl	4(%r14), %edx
	shrl	$22, %edx
	andl	$63, %edx
	cmpb	$32, %dl
	je	.L4193
	movzbl	%dl, %edx
	movl	%edx, -356(%rbp)
.L4096:
	leaq	-352(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator34PickRegisterThatIsAvailableLongestEPNS1_9LiveRangeEiRKNS0_6VectorINS1_16LifetimePositionEEE
	movl	24(%rbx), %r8d
	movslq	%eax, %r10
	movq	-352(%rbp), %rax
	movq	%r10, %r9
	cmpl	(%rax,%r10,4), %r8d
	movq	16(%r14), %rax
	movl	(%rax), %r11d
	jle	.L4103
	cmpl	%r11d, %r8d
	jge	.L4126
	movl	%r11d, %edx
	movl	%r8d, %eax
.L4100:
	addl	$1, %eax
	testb	$2, %al
	jne	.L4101
	cmpl	%edx, %eax
	setl	%al
.L4102:
	testb	%al, %al
	jne	.L4194
.L4103:
	movq	8(%r14), %rax
	cmpl	$1, -388(%rbp)
	movl	4(%rax), %r15d
	je	.L4195
.L4105:
	movq	-208(%rbp), %rax
	movl	(%rax,%r10,4), %eax
	movl	%eax, %edx
	andl	$-2, %edx
	cmpl	%r15d, %eax
	cmovl	%edx, %r15d
	cmpl	%r11d, %r15d
	je	.L4196
	movq	(%r12), %rax
	movq	8(%r14), %rdx
	movl	488(%rax), %eax
	andl	$4, %eax
	cmpl	4(%rdx), %r15d
	je	.L4112
	testl	%eax, %eax
	jne	.L4197
.L4113:
	movl	%r11d, %esi
	movl	%r15d, %edx
	movq	%r12, %rdi
	movq	%r10, -384(%rbp)
	movl	%r9d, -376(%rbp)
	call	_ZN2v88internal8compiler17RegisterAllocator19FindOptimalSplitPosENS1_16LifetimePositionES3_
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE
	movq	(%r12), %rax
	movq	-384(%rbp), %r10
	movl	-376(%rbp), %r9d
	movl	488(%rax), %eax
	andl	$4, %eax
.L4112:
	testl	%eax, %eax
	je	.L4114
	movq	32(%r14), %rax
	movl	(%r14), %ecx
	leaq	.LC26(%rip), %rsi
	movl	88(%rax), %edx
	cmpl	$32, %r9d
	je	.L4115
	movl	8(%r12), %eax
	leaq	.LC54(%rip), %rsi
	testl	%eax, %eax
	jne	.L4116
	cmpl	$-1, %r9d
	je	.L4115
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rax
	movq	(%rax,%r10,8), %rsi
.L4115:
	leaq	.LC81(%rip), %rdi
	xorl	%eax, %eax
	movl	%r9d, -376(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	-376(%rbp), %r9d
.L4114:
	movl	%r9d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator28SetLiveRangeAssignedRegisterEPNS1_9LiveRangeEi
	movl	-388(%rbp), %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator25SplitAndSpillIntersectingEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
.L4056:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4198
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4070:
	.cfi_restore_state
	movq	$0, 56(%r8)
	.p2align 4,,10
	.p2align 3
.L4082:
	movq	$0, 56(%r8)
	.p2align 4,,10
	.p2align 3
.L4080:
	movq	8(%r8), %rax
	movl	4(%rax), %eax
	jmp	.L4118
	.p2align 4,,10
	.p2align 3
.L4193:
	leaq	-356(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler11UsePosition12HintRegisterEPi
	testb	%al, %al
	jne	.L4098
	movq	80(%r14), %rax
	testq	%rax, %rax
	je	.L4098
	movl	116(%rax), %edx
	cmpl	$32, %edx
	je	.L4098
	movl	%edx, -356(%rbp)
	jmp	.L4096
	.p2align 4,,10
	.p2align 3
.L4098:
	movl	-356(%rbp), %edx
	jmp	.L4096
.L4101:
	andl	$-4, %eax
	addl	$4, %eax
	cmpl	%eax, %edx
	setg	%al
	jmp	.L4102
.L4126:
	movl	%r8d, %edx
	movl	%r11d, %eax
	jmp	.L4100
.L4116:
	cmpl	$-1, %r9d
	je	.L4115
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rax
	movq	(%rax,%r10,8), %rsi
	jmp	.L4115
.L4195:
	testl	%r11d, %r11d
	movq	(%r12), %rax
	leal	3(%r11), %esi
	movq	%r10, -384(%rbp)
	cmovns	%r11d, %esi
	movl	%r9d, -376(%rbp)
	movq	16(%rax), %rdi
	sarl	$2, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movl	-376(%rbp), %r9d
	movq	-384(%rbp), %r10
	movq	%rax, %rdx
	movq	(%r12), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rcx
	movq	16(%rax), %r8
	subq	%rcx, %r8
	sarq	$3, %r8
	leal	-1(%r8), %edi
	jmp	.L4108
	.p2align 4,,10
	.p2align 3
.L4106:
	addl	$1, %eax
	cltq
	cmpq	%r8, %rax
	jnb	.L4199
	movq	(%rcx,%rax,8), %rax
	cmpb	$0, 120(%rax)
	je	.L4109
	movq	%rax, %rdx
.L4108:
	movl	100(%rdx), %eax
	cmpl	%eax, %edi
	jg	.L4106
.L4109:
	movl	116(%rdx), %eax
	leal	-4(,%rax,4), %eax
	cmpl	%eax, %r15d
	cmovg	%eax, %r15d
	movq	16(%r14), %rax
	movl	(%rax), %r11d
	jmp	.L4105
.L4197:
	movq	32(%r14), %rax
	movl	(%r14), %edx
	movl	%r11d, %ecx
	movl	%r15d, %r8d
	leaq	.LC46(%rip), %rdi
	movq	%r10, -400(%rbp)
	movl	88(%rax), %esi
	xorl	%eax, %eax
	movl	%r9d, -384(%rbp)
	movl	%r11d, -376(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-400(%rbp), %r10
	movl	-384(%rbp), %r9d
	movl	-376(%rbp), %r11d
	jmp	.L4113
.L4194:
	movl	-388(%rbp), %r9d
	movl	%r11d, %ecx
	movl	%r11d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator17SpillBetweenUntilEPNS1_9LiveRangeENS1_16LifetimePositionES5_S5_NS1_22RegisterAllocationData9SpillModeE
	jmp	.L4056
.L4196:
	movl	-388(%rbp), %r9d
	movl	%r15d, %ecx
	movl	%r15d, %edx
	movq	%r14, %rsi
	movl	24(%rbx), %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator17SpillBetweenUntilEPNS1_9LiveRangeENS1_16LifetimePositionES5_S5_NS1_22RegisterAllocationData9SpillModeE
	jmp	.L4056
	.p2align 4,,10
	.p2align 3
.L4073:
	movq	$0, 56(%r8)
	movq	16(%r14), %rax
	movl	(%rax), %ecx
	jmp	.L4124
.L4199:
	movq	%rax, %rsi
	movq	%r8, %rdx
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L4198:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23605:
	.size	_ZN2v88internal8compiler19LinearScanAllocator18AllocateBlockedRegEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE, .-_ZN2v88internal8compiler19LinearScanAllocator18AllocateBlockedRegEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator19ProcessCurrentRangeEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator19ProcessCurrentRangeEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	.type	_ZN2v88internal8compiler19LinearScanAllocator19ProcessCurrentRangeEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE, @function
_ZN2v88internal8compiler19LinearScanAllocator19ProcessCurrentRangeEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE:
.LFB23601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pcmpeqd	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	-176(%rbp), %rax
	movaps	%xmm0, -176(%rbp)
	movq	%rax, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	$32, -184(%rbp)
	movq	-184(%rbp), %rcx
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler19LinearScanAllocator25FindFreeRegistersForRangeEPNS1_9LiveRangeENS0_6VectorINS1_16LifetimePositionEEE
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator23TryAllocatePreferredRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE
	testb	%al, %al
	jne	.L4202
	movq	32(%r12), %rax
	cmpq	$0, 96(%rax)
	je	.L4203
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator24TrySplitAndSpillSplinterEPNS1_9LiveRangeE
	testb	%al, %al
	jne	.L4200
.L4203:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator18TryAllocateFreeRegEPNS1_9LiveRangeERKNS0_6VectorINS1_16LifetimePositionEEE
	testb	%al, %al
	je	.L4214
.L4202:
	movl	4(%r12), %eax
	shrl	$7, %eax
	andl	$63, %eax
	cmpl	$32, %eax
	jne	.L4215
.L4200:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4216
	addq	$160, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4215:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator11AddToActiveEPNS1_9LiveRangeE
	jmp	.L4200
.L4214:
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator18AllocateBlockedRegEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	movl	4(%r12), %eax
	shrl	$7, %eax
	andl	$63, %eax
	cmpl	$32, %eax
	je	.L4200
	jmp	.L4215
.L4216:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23601:
	.size	_ZN2v88internal8compiler19LinearScanAllocator19ProcessCurrentRangeEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE, .-_ZN2v88internal8compiler19LinearScanAllocator19ProcessCurrentRangeEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS1_22RegisterAllocationData9SpillModeEPNS1_16InstructionBlockE.str1.8,"aMS",@progbits,1
	.align 8
.LC82:
	.string	"Resolving conflict of %d with deferred fixed for register %s\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS1_22RegisterAllocationData9SpillModeEPNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS1_22RegisterAllocationData9SpillModeEPNS1_16InstructionBlockE
	.type	_ZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS1_22RegisterAllocationData9SpillModeEPNS1_16InstructionBlockE, @function
_ZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS1_22RegisterAllocationData9SpillModeEPNS1_16InstructionBlockE:
.LFB23570:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %esi
	je	.L4364
	movq	136(%rdi), %rcx
	movq	144(%rdi), %rax
	cmpq	%rax, %rcx
	jne	.L4270
	jmp	.L4217
	.p2align 4,,10
	.p2align 3
.L4288:
	movq	%rsi, %rcx
	cmpq	%rax, %rcx
	je	.L4217
.L4270:
	movq	(%rcx), %rdx
	leaq	8(%rcx), %rsi
	movq	32(%rdx), %rdx
	testb	$16, 7(%rdx)
	je	.L4288
	cmpq	%rsi, %rax
	je	.L4273
	subq	%rsi, %rax
	movq	%rcx, %rdi
	movq	%rax, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
	movq	144(%rbx), %rax
.L4273:
	subq	$8, %rax
	movq	%rax, 144(%rbx)
	cmpq	%rax, %rcx
	jne	.L4270
.L4217:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4365
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4364:
	.cfi_restore_state
	movq	(%rdi), %r9
	movq	16(%r9), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rcx
	movq	16(%rax), %r8
	subq	%rcx, %r8
	sarq	$3, %r8
	leal	-1(%r8), %edi
	jmp	.L4223
	.p2align 4,,10
	.p2align 3
.L4219:
	addl	$1, %eax
	cltq
	cmpq	%r8, %rax
	jnb	.L4366
	movq	(%rcx,%rax,8), %rax
	cmpb	$0, 120(%rax)
	je	.L4224
	movq	%rax, %rdx
.L4223:
	movl	100(%rdx), %eax
	cmpl	%eax, %edi
	jg	.L4219
.L4224:
	movl	116(%rdx), %eax
	movl	8(%rbx), %r12d
	leal	-2(,%rax,4), %eax
	movl	%eax, -252(%rbp)
	testl	%r12d, %r12d
	je	.L4220
	movq	272(%r9), %rdi
	movq	264(%r9), %rax
	movq	%rdi, -280(%rbp)
	cmpq	%rdi, %rax
	je	.L4217
	movq	.LC83(%rip), %xmm4
	movq	%rax, %r14
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler9LiveRangeEEZZNS2_19LinearScanAllocator25UpdateDeferredFixedRangesENS2_22RegisterAllocationData9SpillModeEPNS2_16InstructionBlockEENKUlS4_E_clES4_EUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rax, %xmm6
	punpcklqdq	%xmm6, %xmm4
	movaps	%xmm4, -272(%rbp)
	jmp	.L4269
	.p2align 4,,10
	.p2align 3
.L4250:
	addq	$8, %r14
	cmpq	%r14, -280(%rbp)
	je	.L4217
.L4269:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L4250
	testb	$16, 7(%r15)
	je	.L4250
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator13AddToInactiveEPNS1_9LiveRangeE
	movq	112(%rbx), %rax
	movq	104(%rbx), %r13
	cmpq	%rax, %r13
	je	.L4261
	movq	%r14, -232(%rbp)
	movq	%rax, %r12
	jmp	.L4260
	.p2align 4,,10
	.p2align 3
.L4369:
	movl	4(%r15), %edx
	movl	4(%r14), %eax
	leaq	_ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E0_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %r8
	shrl	$7, %edx
	shrl	$7, %eax
	andl	$63, %edx
	andl	$63, %eax
	cmpl	%eax, %edx
	je	.L4367
.L4254:
	leaq	-128(%rbp), %rdi
	addq	$8, %r13
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%r8
	cmpq	%r13, %r12
	je	.L4368
.L4260:
	movq	0(%r13), %r14
	movdqa	-272(%rbp), %xmm2
	movq	%rbx, -128(%rbp)
	movq	32(%r14), %rax
	movaps	%xmm2, -112(%rbp)
	movl	88(%rax), %edi
	testl	%edi, %edi
	jns	.L4369
	leaq	_ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E0_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %r8
	jmp	.L4254
	.p2align 4,,10
	.p2align 3
.L4220:
	movq	.LC83(%rip), %xmm3
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler9LiveRangeEEZZNS2_19LinearScanAllocator25UpdateDeferredFixedRangesENS2_22RegisterAllocationData9SpillModeEPNS2_16InstructionBlockEENKUlS4_E_clES4_EUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	208(%r9), %rcx
	movq	%rax, %xmm5
	movq	200(%r9), %r14
	punpcklqdq	%xmm5, %xmm3
	movq	%rcx, -280(%rbp)
	movaps	%xmm3, -272(%rbp)
	cmpq	%rcx, %r14
	jne	.L4248
	jmp	.L4217
	.p2align 4,,10
	.p2align 3
.L4227:
	addq	$8, %r14
	cmpq	%r14, -280(%rbp)
	je	.L4217
.L4248:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L4227
	testb	$16, 7(%r15)
	je	.L4227
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator13AddToInactiveEPNS1_9LiveRangeE
	movq	112(%rbx), %rax
	movq	104(%rbx), %r13
	cmpq	%rax, %r13
	je	.L4239
	movq	%r14, -232(%rbp)
	movq	%rax, %r12
	jmp	.L4238
	.p2align 4,,10
	.p2align 3
.L4372:
	movl	4(%r15), %edx
	movl	4(%r14), %eax
	leaq	_ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E0_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %r8
	shrl	$7, %edx
	shrl	$7, %eax
	andl	$63, %edx
	andl	$63, %eax
	cmpl	%eax, %edx
	je	.L4370
.L4231:
	leaq	-192(%rbp), %rdi
	addq	$8, %r13
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%r8
	cmpq	%r13, %r12
	je	.L4371
.L4238:
	movq	0(%r13), %r14
	movdqa	-272(%rbp), %xmm1
	movq	%rbx, -192(%rbp)
	movq	32(%r14), %rax
	movaps	%xmm1, -176(%rbp)
	movl	88(%rax), %r11d
	testl	%r11d, %r11d
	jns	.L4372
	leaq	_ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E0_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %r8
	jmp	.L4231
	.p2align 4,,10
	.p2align 3
.L4370:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9LiveRange17FirstIntersectionEPS2_
	movl	%eax, %r8d
	cmpl	$-1, %eax
	je	.L4232
	cmpl	%eax, -252(%rbp)
	jl	.L4232
	movq	(%rbx), %rax
	testb	$4, 488(%rax)
	je	.L4233
	movl	4(%r14), %eax
	leaq	.LC26(%rip), %rdx
	shrl	$7, %eax
	andl	$63, %eax
	cmpl	$32, %eax
	je	.L4234
	movl	8(%rbx), %r10d
	cltq
	testl	%r10d, %r10d
	jne	.L4235
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rcx
	movq	(%rcx,%rax,8), %rdx
.L4234:
	movq	32(%r14), %rax
	leaq	.LC82(%rip), %rdi
	movl	%r8d, -240(%rbp)
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%rbx), %rax
	movl	-240(%rbp), %r8d
.L4233:
	movq	(%rax), %rdx
	movl	%r8d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9LiveRange7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE
	movl	4(%r14), %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movl	4(%rax), %eax
	sall	$15, %edx
	andl	$-264241153, %eax
	andl	$264241152, %edx
	orl	%eax, %edx
	movl	%edx, 4(%rsi)
	call	_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE
	cmpq	$0, -176(%rbp)
	movq	%r14, -224(%rbp)
	je	.L4245
	leaq	-224(%rbp), %rsi
	leaq	-192(%rbp), %rdi
	call	*-168(%rbp)
.L4232:
	movq	-176(%rbp), %r8
	testq	%r8, %r8
	jne	.L4231
	addq	$8, %r13
	cmpq	%r13, %r12
	jne	.L4238
.L4371:
	movq	-232(%rbp), %r14
.L4239:
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler9LiveRangeEEZZNS2_19LinearScanAllocator25UpdateDeferredFixedRangesENS2_22RegisterAllocationData9SpillModeEPNS2_16InstructionBlockEENKUlS4_E_clES4_EUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rdi
	movq	144(%rbx), %rax
	movq	136(%rbx), %r13
	movq	%rdi, -248(%rbp)
	leaq	_ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E1_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rdi
	movq	%rax, -232(%rbp)
	movq	%rdi, -240(%rbp)
	cmpq	%rax, %r13
	je	.L4227
	movq	%r14, -288(%rbp)
	movq	%r13, %r14
	movq	%rdi, %r13
	jmp	.L4247
	.p2align 4,,10
	.p2align 3
.L4375:
	movl	4(%r15), %edx
	movl	4(%r12), %eax
	movq	%r13, %r9
	shrl	$7, %edx
	shrl	$7, %eax
	andl	$63, %edx
	andl	$63, %eax
	cmpl	%eax, %edx
	je	.L4373
.L4240:
	leaq	-160(%rbp), %rdi
	movl	$3, %edx
	addq	$8, %r14
	movq	%rdi, %rsi
	call	*%r9
	cmpq	%r14, -232(%rbp)
	je	.L4374
.L4247:
	movq	-240(%rbp), %xmm0
	movq	(%r14), %r12
	movq	%rbx, -160(%rbp)
	movq	32(%r12), %rax
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movl	88(%rax), %r9d
	testl	%r9d, %r9d
	jns	.L4375
	movq	%r13, %r9
	jmp	.L4240
	.p2align 4,,10
	.p2align 3
.L4367:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9LiveRange17FirstIntersectionEPS2_
	movl	%eax, %r8d
	cmpl	$-1, %eax
	je	.L4255
	cmpl	%eax, -252(%rbp)
	jl	.L4255
	movq	(%rbx), %rax
	testb	$4, 488(%rax)
	je	.L4256
	movl	4(%r14), %eax
	leaq	.LC26(%rip), %rdx
	shrl	$7, %eax
	andl	$63, %eax
	cmpl	$32, %eax
	je	.L4257
	movl	8(%rbx), %esi
	cltq
	testl	%esi, %esi
	jne	.L4258
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rcx
	movq	(%rcx,%rax,8), %rdx
.L4257:
	movq	32(%r14), %rax
	leaq	.LC82(%rip), %rdi
	movl	%r8d, -240(%rbp)
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%rbx), %rax
	movl	-240(%rbp), %r8d
.L4256:
	movq	(%rax), %rdx
	movl	%r8d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9LiveRange7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE
	movl	4(%r14), %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movl	4(%rax), %eax
	sall	$15, %edx
	andl	$-264241153, %eax
	andl	$264241152, %edx
	orl	%eax, %edx
	movl	%edx, 4(%rsi)
	call	_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE
	cmpq	$0, -112(%rbp)
	movq	%r14, -208(%rbp)
	je	.L4245
	leaq	-208(%rbp), %rsi
	leaq	-128(%rbp), %rdi
	call	*-104(%rbp)
.L4255:
	movq	-112(%rbp), %r8
	testq	%r8, %r8
	jne	.L4254
	addq	$8, %r13
	cmpq	%r13, %r12
	jne	.L4260
.L4368:
	movq	-232(%rbp), %r14
.L4261:
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler9LiveRangeEEZZNS2_19LinearScanAllocator25UpdateDeferredFixedRangesENS2_22RegisterAllocationData9SpillModeEPNS2_16InstructionBlockEENKUlS4_E_clES4_EUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rdi
	movq	144(%rbx), %rax
	movq	136(%rbx), %r13
	movq	%rdi, -248(%rbp)
	leaq	_ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E1_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rdi
	movq	%rax, -232(%rbp)
	movq	%rdi, -240(%rbp)
	cmpq	%rax, %r13
	je	.L4250
	movq	%r14, -288(%rbp)
	movq	%r13, %r14
	movq	%rdi, %r13
	jmp	.L4268
	.p2align 4,,10
	.p2align 3
.L4378:
	movl	4(%r15), %edx
	movl	4(%r12), %eax
	movq	%r13, %r9
	shrl	$7, %edx
	shrl	$7, %eax
	andl	$63, %edx
	andl	$63, %eax
	cmpl	%eax, %edx
	je	.L4376
.L4262:
	leaq	-96(%rbp), %rdi
	movl	$3, %edx
	addq	$8, %r14
	movq	%rdi, %rsi
	call	*%r9
	cmpq	%r14, -232(%rbp)
	je	.L4377
.L4268:
	movq	-240(%rbp), %xmm0
	movq	(%r14), %r12
	movq	%rbx, -96(%rbp)
	movq	32(%r12), %rax
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movl	88(%rax), %ecx
	testl	%ecx, %ecx
	jns	.L4378
	movq	%r13, %r9
	jmp	.L4262
	.p2align 4,,10
	.p2align 3
.L4373:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9LiveRange17FirstIntersectionEPS2_
	movl	%eax, %r9d
	cmpl	$-1, %eax
	je	.L4241
	cmpl	%eax, -252(%rbp)
	jl	.L4241
	movq	(%rbx), %rax
	testb	$4, 488(%rax)
	je	.L4242
	movl	4(%r12), %eax
	leaq	.LC26(%rip), %rdx
	shrl	$7, %eax
	andl	$63, %eax
	cmpl	$32, %eax
	je	.L4243
	movl	8(%rbx), %r8d
	cltq
	testl	%r8d, %r8d
	jne	.L4244
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rcx
	movq	(%rcx,%rax,8), %rdx
.L4243:
	movq	32(%r12), %rax
	leaq	.LC82(%rip), %rdi
	movl	%r9d, -256(%rbp)
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%rbx), %rax
	movl	-256(%rbp), %r9d
.L4242:
	movq	(%rax), %rdx
	movl	%r9d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9LiveRange7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE
	movl	4(%r12), %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movl	4(%rax), %eax
	sall	$15, %edx
	andl	$-264241153, %eax
	andl	$264241152, %edx
	orl	%eax, %edx
	movl	%edx, 4(%rsi)
	call	_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE
	cmpq	$0, -144(%rbp)
	movq	%r12, -216(%rbp)
	je	.L4245
	leaq	-216(%rbp), %rsi
	leaq	-160(%rbp), %rdi
	call	*-136(%rbp)
.L4241:
	movq	-144(%rbp), %r9
	testq	%r9, %r9
	jne	.L4240
	addq	$8, %r14
	cmpq	%r14, -232(%rbp)
	jne	.L4247
.L4374:
	movq	-288(%rbp), %r14
	jmp	.L4227
	.p2align 4,,10
	.p2align 3
.L4376:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler9LiveRange17FirstIntersectionEPS2_
	movl	%eax, %r9d
	cmpl	$-1, %eax
	je	.L4263
	cmpl	%eax, -252(%rbp)
	jl	.L4263
	movq	(%rbx), %rax
	testb	$4, 488(%rax)
	je	.L4264
	movl	4(%r12), %eax
	leaq	.LC26(%rip), %rdx
	shrl	$7, %eax
	andl	$63, %eax
	cmpl	$32, %eax
	je	.L4265
	movl	8(%rbx), %edx
	cltq
	testl	%edx, %edx
	jne	.L4266
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rcx
	movq	(%rcx,%rax,8), %rdx
.L4265:
	movq	32(%r12), %rax
	leaq	.LC82(%rip), %rdi
	movl	%r9d, -256(%rbp)
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%rbx), %rax
	movl	-256(%rbp), %r9d
.L4264:
	movq	(%rax), %rdx
	movl	%r9d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9LiveRange7SplitAtENS1_16LifetimePositionEPNS0_4ZoneE
	movl	4(%r12), %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movl	4(%rax), %eax
	sall	$15, %edx
	andl	$-264241153, %eax
	andl	$264241152, %edx
	orl	%eax, %edx
	movl	%edx, 4(%rsi)
	call	_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE
	cmpq	$0, -80(%rbp)
	movq	%r12, -200(%rbp)
	je	.L4245
	leaq	-200(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	call	*-72(%rbp)
.L4263:
	movq	-80(%rbp), %r9
	testq	%r9, %r9
	jne	.L4262
	addq	$8, %r14
	cmpq	%r14, -232(%rbp)
	jne	.L4268
.L4377:
	movq	-288(%rbp), %r14
	jmp	.L4250
.L4266:
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rdi
	movq	(%rdi,%rax,8), %rdx
	jmp	.L4265
.L4244:
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rdi
	movq	(%rdi,%rax,8), %rdx
	jmp	.L4243
.L4258:
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rdi
	movq	(%rdi,%rax,8), %rdx
	jmp	.L4257
.L4235:
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rcx
	movq	(%rcx,%rax,8), %rdx
	jmp	.L4234
.L4365:
	call	__stack_chk_fail@PLT
.L4366:
	movq	%rax, %rsi
	movq	%r8, %rdx
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L4245:
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE23570:
	.size	_ZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS1_22RegisterAllocationData9SpillModeEPNS1_16InstructionBlockE, .-_ZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS1_22RegisterAllocationData9SpillModeEPNS1_16InstructionBlockE
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator22AssignRegisterOnReloadEPNS1_9LiveRangeEi.str1.8,"aMS",@progbits,1
	.align 8
.LC84:
	.string	"Found new end for %d:%d at %d\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator22AssignRegisterOnReloadEPNS1_9LiveRangeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator22AssignRegisterOnReloadEPNS1_9LiveRangeEi
	.type	_ZN2v88internal8compiler19LinearScanAllocator22AssignRegisterOnReloadEPNS1_9LiveRangeEi, @function
_ZN2v88internal8compiler19LinearScanAllocator22AssignRegisterOnReloadEPNS1_9LiveRangeEi:
.LFB23499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	136(%rdi), %rsi
	movq	144(%rdi), %rdi
	movl	4(%rax), %r8d
	cmpq	%rdi, %rsi
	je	.L4380
	movl	%r8d, %r12d
	jmp	.L4384
	.p2align 4,,10
	.p2align 3
.L4381:
	addq	$8, %rsi
	cmpq	%rsi, %rdi
	je	.L4405
.L4384:
	movq	(%rsi), %rdx
	movl	4(%rdx), %eax
	shrl	$7, %eax
	andl	$63, %eax
	cmpl	%eax, %r14d
	jne	.L4381
	movq	16(%rdx), %rax
	testq	%rax, %rax
	jne	.L4383
	jmp	.L4381
	.p2align 4,,10
	.p2align 3
.L4406:
	movq	16(%r13), %rcx
	movl	(%rcx), %ecx
	cmpl	%ecx, 4(%rax)
	movq	8(%rax), %rax
	cmovg	%edx, %r12d
	testq	%rax, %rax
	je	.L4381
.L4383:
	movl	(%rax), %edx
	cmpl	%r12d, %edx
	jle	.L4406
	addq	$8, %rsi
	cmpq	%rsi, %rdi
	jne	.L4384
	.p2align 4,,10
	.p2align 3
.L4405:
	cmpl	%r12d, %r8d
	je	.L4380
	movq	(%r15), %rax
	testb	$4, 488(%rax)
	jne	.L4407
.L4385:
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	movq	%rax, -48(%rbp)
	testq	%rax, %rax
	je	.L4380
	cmpq	$0, 16(%rax)
	je	.L4380
	movq	(%r15), %rdx
	testb	$4, 488(%rdx)
	jne	.L4408
.L4387:
	leaq	-48(%rbp), %rsi
	leaq	40(%r15), %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE15_M_insert_equalIRKS4_EESt17_Rb_tree_iteratorIS4_EOT_
.L4380:
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator28SetLiveRangeAssignedRegisterEPNS1_9LiveRangeEi
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4409
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4407:
	.cfi_restore_state
	movq	32(%r13), %rax
	movl	0(%r13), %edx
	movl	%r12d, %ecx
	leaq	.LC84(%rip), %rdi
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L4385
.L4408:
	movq	32(%rax), %rdx
	leaq	.LC77(%rip), %rdi
	movl	88(%rdx), %esi
	movl	(%rax), %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L4387
.L4409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23499:
	.size	_ZN2v88internal8compiler19LinearScanAllocator22AssignRegisterOnReloadEPNS1_9LiveRangeEi, .-_ZN2v88internal8compiler19LinearScanAllocator22AssignRegisterOnReloadEPNS1_9LiveRangeEi
	.section	.text._ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,"axG",@progbits,_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.type	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, @function
_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E:
.LFB27851:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L4418
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L4412:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L4412
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4418:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE27851:
	.size	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, .-_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.section	.text._ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS0_IS7_S6_EEEES0_ISt17_Rb_tree_iteratorIS9_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS0_IS7_S6_EEEES0_ISt17_Rb_tree_iteratorIS9_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS0_IS7_S6_EEEES0_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
	.type	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS0_IS7_S6_EEEES0_ISt17_Rb_tree_iteratorIS9_EbEDpOT_, @function
_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS0_IS7_S6_EEEES0_ISt17_Rb_tree_iteratorIS9_EbEDpOT_:
.LFB27856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L4451
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdi)
.L4423:
	movdqu	(%r12), %xmm0
	movq	16(%r12), %rax
	leaq	16(%r13), %r15
	movq	%rax, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movq	24(%r13), %r12
	testq	%r12, %r12
	je	.L4424
	movq	32(%rbx), %r14
	jmp	.L4425
	.p2align 4,,10
	.p2align 3
.L4453:
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L4426
.L4454:
	movq	%rax, %r12
.L4425:
	movq	32(%r12), %rdx
	cmpq	%rdx, %r14
	setb	%cl
	je	.L4452
.L4428:
	testb	%cl, %cl
	jne	.L4453
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L4454
.L4426:
	movq	%r12, %r8
	testb	%cl, %cl
	jne	.L4455
	cmpq	%r14, %rdx
	setb	%al
	je	.L4456
.L4434:
	testb	%al, %al
	je	.L4435
	testq	%r8, %r8
	je	.L4457
	movl	$1, %edi
	cmpq	%r15, %r8
	jne	.L4458
.L4436:
	movq	%r15, %rcx
	movq	%r8, %rdx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
	addq	$8, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4452:
	.cfi_restore_state
	movq	40(%r12), %rax
	cmpq	%rax, 40(%rbx)
	setb	%cl
	jmp	.L4428
	.p2align 4,,10
	.p2align 3
.L4457:
	xorl	%r12d, %r12d
.L4435:
	addq	$8, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4455:
	.cfi_restore_state
	cmpq	%r12, 32(%r13)
	je	.L4459
.L4441:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %r8
	movq	32(%rax), %rdx
	movq	%rax, %r12
	cmpq	%r14, %rdx
	setb	%al
	jne	.L4434
.L4456:
	movq	40(%rbx), %rax
	cmpq	%rax, 40(%r12)
	setb	%al
	jmp	.L4434
	.p2align 4,,10
	.p2align 3
.L4459:
	movq	%r12, %r8
	movl	$1, %edi
	cmpq	%r15, %r8
	je	.L4436
.L4458:
	movq	32(%r8), %rax
	cmpq	%rax, %r14
	setb	%dil
	je	.L4460
.L4438:
	movzbl	%dil, %edi
	jmp	.L4436
	.p2align 4,,10
	.p2align 3
.L4424:
	cmpq	32(%r13), %r15
	je	.L4445
	movq	32(%rbx), %r14
	movq	%r15, %r12
	jmp	.L4441
	.p2align 4,,10
	.p2align 3
.L4451:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L4423
	.p2align 4,,10
	.p2align 3
.L4460:
	movq	40(%r8), %rax
	cmpq	%rax, 40(%rbx)
	setb	%dil
	jmp	.L4438
	.p2align 4,,10
	.p2align 3
.L4445:
	movq	%r15, %r8
	movl	$1, %edi
	jmp	.L4436
	.cfi_endproc
.LFE27856:
	.size	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS0_IS7_S6_EEEES0_ISt17_Rb_tree_iteratorIS9_EbEDpOT_, .-_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS0_IS7_S6_EEEES0_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
	.section	.text._ZN2v88internal8compiler18LiveRangeConnector13ConnectRangesEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18LiveRangeConnector13ConnectRangesEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler18LiveRangeConnector13ConnectRangesEPNS0_4ZoneE, @function
_ZN2v88internal8compiler18LiveRangeConnector13ConnectRangesEPNS0_4ZoneE:
.LFB23632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-96(%rbp), %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -256(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	%rax, -224(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	(%r10), %rax
	movq	%rsi, -112(%rbp)
	movq	176(%rax), %rdi
	movq	168(%rax), %r8
	movl	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdi, %rcx
	movq	$0, -64(%rbp)
	subq	%r8, %rcx
	movq	%rdi, -208(%rbp)
	movq	%rcx, -216(%rbp)
	cmpq	%rdi, %r8
	je	.L4461
	leaq	-144(%rbp), %rdi
	movq	(%r8), %r9
	leaq	8(%r8), %r15
	movq	%rdi, -248(%rbp)
	movq	%r15, %r8
	movq	%r9, %r15
	.p2align 4,,10
	.p2align 3
.L4464:
	testq	%r15, %r15
	je	.L4470
	testb	$1, 488(%rax)
	je	.L4468
	movl	4(%r15), %eax
	xorl	$96, %eax
	testb	$96, %al
	sete	-200(%rbp)
.L4469:
	movq	40(%r15), %rbx
	testq	%rbx, %rbx
	je	.L4470
	movq	%r15, -240(%rbp)
	movq	%r15, %r12
	movq	%r8, %r14
	movq	%r10, %r13
	jmp	.L4507
	.p2align 4,,10
	.p2align 3
.L4506:
	movq	40(%rbx), %rax
	movq	%rbx, %r12
	testq	%rax, %rax
	je	.L4605
.L4611:
	movq	%rax, %rbx
.L4507:
	movl	4(%rbx), %eax
	testb	$1, %al
	jne	.L4506
	movq	16(%rbx), %rdx
	movl	(%rdx), %r15d
	movq	8(%r12), %rdx
	cmpl	%r15d, 4(%rdx)
	jne	.L4506
	testb	$3, %r15b
	je	.L4608
.L4473:
	movl	4(%r12), %ecx
	movl	%ecx, %esi
	shrl	$7, %esi
	andl	$63, %esi
	cmpl	$32, %esi
	je	.L4475
	shrl	$13, %ecx
	salq	$35, %rsi
	movzbl	%cl, %edx
	salq	$5, %rdx
	orq	%rsi, %rdx
	orq	$5, %rdx
.L4476:
	movl	%eax, %ecx
	shrl	$7, %ecx
	andl	$63, %ecx
	cmpl	$32, %ecx
	je	.L4479
.L4616:
	shrl	$13, %eax
	salq	$35, %rcx
	movzbl	%al, %r12d
	salq	$5, %r12
	orq	%rcx, %r12
	orq	$5, %r12
.L4480:
	cmpq	%rdx, %r12
	je	.L4506
	testl	%r15d, %r15d
	leal	3(%r15), %esi
	movq	0(%r13), %rax
	cmovns	%r15d, %esi
	movq	16(%rax), %rdi
	sarl	$2, %esi
	cmpb	$0, -200(%rbp)
	movl	%esi, %r9d
	je	.L4483
	testb	$4, %dl
	je	.L4484
	testb	$24, %dl
	jne	.L4484
	.p2align 4,,10
	.p2align 3
.L4483:
	movl	%r15d, %ecx
	notl	%ecx
	andl	$1, %ecx
	andl	$2, %r15d
	jne	.L4486
.L4614:
	xorl	$1, %ecx
	movzbl	%cl, %r8d
	xorl	%ecx, %ecx
.L4487:
	movq	208(%rdi), %rsi
	movslq	%r9d, %r9
	movq	%rsi, %rax
	subq	216(%rdi), %rax
	sarq	$3, %rax
	addq	%r9, %rax
	js	.L4488
	cmpq	$63, %rax
	jg	.L4489
	leaq	(%rsi,%r9,8), %rax
	movq	(%rax), %rax
	leaq	(%rax,%r8,8), %r9
	movq	8(%r9), %r8
	testq	%r8, %r8
	je	.L4609
.L4492:
	testb	%cl, %cl
	jne	.L4495
	movl	%r12d, %esi
	andl	$4, %esi
	testb	$4, %dl
	je	.L4496
	xorl	%eax, %eax
	testb	$24, %dl
	jne	.L4497
	movq	%rdx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L4497:
	movq	%rdx, %rcx
	andq	$-8161, %rcx
	orq	%rax, %rcx
	movq	%r12, %rax
	andq	$-8, %rcx
	orq	$4, %rcx
	testl	%esi, %esi
	jne	.L4542
.L4498:
	cmpq	%rax, %rcx
	je	.L4506
.L4543:
	movq	(%r8), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	$15, %rcx
	jbe	.L4610
	leaq	16(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L4502:
	movq	%rdx, %xmm0
	movq	%r12, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rax)
	movq	%rax, -144(%rbp)
	movq	16(%r8), %rsi
	cmpq	8(%r8), %rsi
	movq	24(%r8), %rax
	je	.L4503
.L4504:
	cmpq	%rax, %rsi
	je	.L4505
	movq	-144(%rbp), %rax
	movq	%rbx, %r12
	movq	%rax, (%rsi)
	addq	$8, 16(%r8)
	movq	40(%rbx), %rax
	testq	%rax, %rax
	jne	.L4611
	.p2align 4,,10
	.p2align 3
.L4605:
	movq	%r14, %r8
	movq	%r13, %r10
.L4470:
	cmpq	%r8, -208(%rbp)
	je	.L4612
	movq	(%r10), %rax
	movq	(%r8), %r15
	addq	$8, %r8
	movq	176(%rax), %rdx
	subq	168(%rax), %rdx
	cmpq	%rdx, -216(%rbp)
	je	.L4464
	leaq	.LC63(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4484:
	testb	$4, %r12b
	je	.L4483
	testb	$24, %r12b
	jne	.L4483
	movq	%rdx, -272(%rbp)
	movl	%r9d, -264(%rbp)
	movl	%esi, -232(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movq	-240(%rbp), %rdi
	movl	-232(%rbp), %esi
	movl	100(%rax), %ecx
	movl	-264(%rbp), %r9d
	movq	112(%rdi), %rdi
	movq	-272(%rbp), %rdx
	cmpl	$1, 4(%rdi)
	je	.L4613
	testl	%ecx, %ecx
	leal	63(%rcx), %eax
	movl	%ecx, %r11d
	movq	8(%rdi), %rdi
	cmovns	%ecx, %eax
	sarl	$31, %r11d
	shrl	$26, %r11d
	addl	%r11d, %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%r11d, %ecx
	movl	$1, %r11d
	salq	%cl, %r11
	movl	%r15d, %ecx
	orq	%r11, (%rdi,%rax,8)
	movq	0(%r13), %rax
	notl	%ecx
	andl	$1, %ecx
	andl	$2, %r15d
	movq	16(%rax), %rdi
	je	.L4614
	.p2align 4,,10
	.p2align 3
.L4486:
	movl	$1, %r8d
	testb	%cl, %cl
	jne	.L4487
	leal	1(%rsi), %r9d
	xorl	%r8d, %r8d
	jmp	.L4487
	.p2align 4,,10
	.p2align 3
.L4496:
	testl	%esi, %esi
	je	.L4543
	movq	%rdx, %rcx
.L4542:
	xorl	%esi, %esi
	testb	$24, %r12b
	jne	.L4499
	movq	%r12, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L4499:
	movq	%r12, %rax
	andq	$-8161, %rax
	orq	%rsi, %rax
	andq	$-8, %rax
	orq	$4, %rax
	jmp	.L4498
	.p2align 4,,10
	.p2align 3
.L4608:
	testl	%r15d, %r15d
	movq	0(%r13), %rax
	leal	3(%r15), %esi
	cmovns	%r15d, %esi
	movq	16(%rax), %rdi
	sarl	$2, %esi
	movl	%esi, -232(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movl	-232(%rbp), %esi
	cmpl	112(%rax), %esi
	je	.L4474
.L4607:
	movl	4(%rbx), %eax
	jmp	.L4473
	.p2align 4,,10
	.p2align 3
.L4474:
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movq	40(%rax), %rcx
	movq	48(%rax), %rdx
	subq	%rcx, %rdx
	cmpq	$4, %rdx
	jne	.L4506
	movl	(%rcx), %edx
	addl	$1, %edx
	cmpl	%edx, 100(%rax)
	je	.L4607
	jmp	.L4506
	.p2align 4,,10
	.p2align 3
.L4468:
	movzbl	120(%r15), %eax
	movb	%al, -200(%rbp)
	jmp	.L4469
	.p2align 4,,10
	.p2align 3
.L4495:
	movq	%r12, %xmm1
	movq	-248(%rbp), %rsi
	movq	%rdx, %xmm0
	leaq	-112(%rbp), %rdi
	punpcklqdq	%xmm1, %xmm0
	movq	%r8, -144(%rbp)
	movups	%xmm0, -136(%rbp)
	call	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS0_IS7_S6_EEEES0_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
	jmp	.L4506
	.p2align 4,,10
	.p2align 3
.L4475:
	movq	32(%r12), %rcx
	movl	4(%rcx), %edx
	movq	104(%rcx), %rsi
	movl	%edx, %ecx
	shrl	$5, %ecx
	andl	$3, %ecx
	cmpl	$1, %ecx
	je	.L4615
	movslq	44(%rsi), %rcx
	shrl	$13, %edx
	movzbl	%dl, %edx
	salq	$35, %rcx
	salq	$5, %rdx
	orq	%rcx, %rdx
	movl	%eax, %ecx
	shrl	$7, %ecx
	orq	$13, %rdx
	andl	$63, %ecx
	cmpl	$32, %ecx
	jne	.L4616
.L4479:
	movq	32(%rbx), %rcx
	movl	4(%rcx), %eax
	movq	104(%rcx), %rsi
	movl	%eax, %ecx
	shrl	$5, %ecx
	andl	$3, %ecx
	cmpl	$1, %ecx
	je	.L4617
	shrl	$13, %eax
	movzbl	%al, %r12d
	movslq	44(%rsi), %rax
	salq	$5, %r12
	salq	$35, %rax
	orq	%rax, %r12
	orq	$13, %r12
	jmp	.L4480
	.p2align 4,,10
	.p2align 3
.L4489:
	movq	%rax, %rsi
	sarq	$6, %rsi
.L4491:
	movq	232(%rdi), %r9
	movq	%rsi, %r11
	salq	$6, %r11
	movq	(%r9,%rsi,8), %rsi
	subq	%r11, %rax
	leaq	(%rsi,%rax,8), %rax
	movq	(%rax), %rax
	leaq	(%rax,%r8,8), %r9
	movq	8(%r9), %r8
	testq	%r8, %r8
	jne	.L4492
.L4609:
	movq	8(%rdi), %rdi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	$31, %rax
	jbe	.L4618
	leaq	32(%r8), %rax
	movq	%rax, 16(%rdi)
.L4494:
	movq	%rdi, (%r8)
	movq	$0, 8(%r8)
	movq	$0, 16(%r8)
	movq	$0, 24(%r8)
	movq	%r8, 8(%r9)
	jmp	.L4492
	.p2align 4,,10
	.p2align 3
.L4488:
	movq	%rax, %rsi
	notq	%rsi
	shrq	$6, %rsi
	notq	%rsi
	jmp	.L4491
	.p2align 4,,10
	.p2align 3
.L4615:
	movq	(%rsi), %rdx
	jmp	.L4476
	.p2align 4,,10
	.p2align 3
.L4617:
	movq	(%rsi), %r12
	jmp	.L4480
.L4612:
	cmpq	$0, -64(%rbp)
	jne	.L4508
	movq	-88(%rbp), %r12
	leaq	-112(%rbp), %r13
	testq	%r12, %r12
	je	.L4461
	.p2align 4,,10
	.p2align 3
.L4509:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L4510
.L4511:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L4511
.L4510:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L4509
.L4461:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4619
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4505:
	.cfi_restore_state
	movq	-248(%rbp), %rdx
	movq	%r8, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L4506
.L4503:
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	$31, %rdx
	ja	.L4504
	movq	(%r8), %rdi
	movl	$32, %esi
	movq	%r8, -232(%rbp)
	call	_ZN2v88internal4Zone3NewEm
	movq	-232(%rbp), %r8
	movq	%rax, %xmm0
	movq	%rax, %rsi
	leaq	32(%rax), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 24(%r8)
	movups	%xmm0, 8(%r8)
	jmp	.L4504
.L4508:
	movq	-256(%rbp), %rax
	movl	$32, %esi
	movq	%r10, -200(%rbp)
	movq	$0, -168(%rbp)
	movq	%rax, %rdi
	movq	%rax, -176(%rbp)
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	%rax, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	call	_ZN2v88internal4Zone3NewEm
	movq	-136(%rbp), %rbx
	movq	-200(%rbp), %r10
	movq	%rax, %xmm0
	addq	$32, %rax
	movq	%rax, -152(%rbp)
	movq	-120(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, -168(%rbp)
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L4620
.L4512:
	movq	-80(%rbp), %r14
	movq	%r10, -200(%rbp)
	leaq	-184(%rbp), %r12
	movq	32(%r14), %r13
	movq	%r14, %r15
	movq	%r13, %r14
	.p2align 4,,10
	.p2align 3
.L4538:
	cmpq	-224(%rbp), %r15
	je	.L4520
	cmpq	%r14, 32(%r15)
	je	.L4521
.L4520:
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %rax
	cmpq	%rax, %rcx
	je	.L4526
	.p2align 4,,10
	.p2align 3
.L4525:
	movq	(%rax), %rdx
	addq	$8, %rax
	movq	$0, 8(%rdx)
	movq	$0, (%rdx)
	cmpq	%rax, %rcx
	jne	.L4525
.L4526:
	movq	-160(%rbp), %r13
	movq	-168(%rbp), %rbx
	cmpq	%rbx, %r13
	jne	.L4531
	jmp	.L4524
	.p2align 4,,10
	.p2align 3
.L4621:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, 16(%r14)
	cmpq	%rbx, %r13
	je	.L4524
.L4531:
	movq	(%rbx), %rax
	movq	%rax, -184(%rbp)
	movq	16(%r14), %rsi
	cmpq	24(%r14), %rsi
	jne	.L4621
	movq	%r12, %rdx
	movq	%r14, %rdi
	addq	$8, %rbx
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	cmpq	%rbx, %r13
	jne	.L4531
	.p2align 4,,10
	.p2align 3
.L4524:
	cmpq	-224(%rbp), %r15
	je	.L4622
	movq	-136(%rbp), %rax
	cmpq	-128(%rbp), %rax
	je	.L4532
	movq	%rax, -128(%rbp)
.L4532:
	movq	-168(%rbp), %rax
	cmpq	-160(%rbp), %rax
	je	.L4533
	movq	%rax, -160(%rbp)
.L4533:
	movq	32(%r15), %r14
.L4521:
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$15, %rax
	jbe	.L4623
	leaq	16(%rsi), %rax
	movq	%rax, 16(%rdi)
.L4535:
	movq	40(%r15), %rax
	leaq	-144(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, (%rsi)
	movq	48(%r15), %rax
	movq	%rax, 8(%rsi)
	movq	%rsi, -184(%rbp)
	call	_ZNK2v88internal8compiler12ParallelMove18PrepareInsertAfterEPNS1_12MoveOperandsEPNS0_10ZoneVectorIS4_EE@PLT
	movq	-160(%rbp), %rsi
	cmpq	-152(%rbp), %rsi
	je	.L4536
	movq	-184(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -160(%rbp)
.L4537:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	jmp	.L4538
	.p2align 4,,10
	.p2align 3
.L4536:
	leaq	-176(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L4537
.L4623:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L4535
.L4622:
	movq	-88(%rbp), %r12
	testq	%r12, %r12
	je	.L4461
	leaq	-112(%rbp), %r13
.L4541:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L4539
.L4540:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeISt4pairIPN2v88internal8compiler12ParallelMoveENS3_18InstructionOperandEES0_IKS7_S6_ESt10_Select1stIS9_ENS3_26DelayedInsertionMapCompareENS2_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L4540
.L4539:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L4541
	jmp	.L4461
.L4610:
	movl	$16, %esi
	movq	%rdx, -264(%rbp)
	movq	%r8, -232(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-232(%rbp), %r8
	movq	-264(%rbp), %rdx
	jmp	.L4502
.L4613:
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, 8(%rdi)
	movq	0(%r13), %rax
	movq	16(%rax), %rdi
	jmp	.L4483
.L4618:
	movl	$32, %esi
	movq	%r9, -280(%rbp)
	movq	%rdx, -272(%rbp)
	movb	%cl, -264(%rbp)
	movq	%rdi, -232(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-232(%rbp), %rdi
	movzbl	-264(%rbp), %ecx
	movq	-272(%rbp), %rdx
	movq	-280(%rbp), %r9
	movq	%rax, %r8
	jmp	.L4494
.L4620:
	movq	-128(%rbp), %r13
	movq	-144(%rbp), %rdi
	movl	$32, %esi
	call	_ZN2v88internal4Zone3NewEm
	movq	%r13, %r12
	movq	-200(%rbp), %r10
	subq	%rbx, %r12
	cmpq	%r13, %rbx
	movq	%rax, %rdx
	je	.L4517
	leaq	-8(%r13), %rax
	leaq	15(%rbx), %rcx
	subq	%rbx, %rax
	subq	%rdx, %rcx
	shrq	$3, %rax
	cmpq	$30, %rcx
	jbe	.L4551
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L4551
	leaq	1(%rax), %rcx
	xorl	%eax, %eax
	movq	%rcx, %rsi
	shrq	%rsi
	salq	$4, %rsi
.L4515:
	movdqu	(%rbx,%rax), %xmm3
	movups	%xmm3, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L4515
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rsi
	addq	%rsi, %rbx
	addq	%rdx, %rsi
	cmpq	%rcx, %rax
	je	.L4517
	movq	(%rbx), %rax
	movq	%rax, (%rsi)
.L4517:
	addq	%rdx, %r12
	movq	%rdx, -136(%rbp)
	addq	$32, %rdx
	movq	%r12, -128(%rbp)
	movq	%rdx, -120(%rbp)
	jmp	.L4512
.L4551:
	xorl	%ecx, %ecx
.L4514:
	movq	(%rbx,%rcx,8), %rsi
	movq	%rsi, (%rdx,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rsi, %rax
	jne	.L4514
	jmp	.L4517
.L4619:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23632:
	.size	_ZN2v88internal8compiler18LiveRangeConnector13ConnectRangesEPNS0_4ZoneE, .-_ZN2v88internal8compiler18LiveRangeConnector13ConnectRangesEPNS0_4ZoneE
	.section	.text._ZNSt8_Rb_treeISt4pairIN2v88internal8compiler9RpoNumberEiES5_St9_IdentityIS5_ESt4lessIS5_ENS2_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeISt4pairIN2v88internal8compiler9RpoNumberEiES5_St9_IdentityIS5_ESt4lessIS5_ENS2_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeISt4pairIN2v88internal8compiler9RpoNumberEiES5_St9_IdentityIS5_ESt4lessIS5_ENS2_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeISt4pairIN2v88internal8compiler9RpoNumberEiES5_St9_IdentityIS5_ESt4lessIS5_ENS2_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeISt4pairIN2v88internal8compiler9RpoNumberEiES5_St9_IdentityIS5_ESt4lessIS5_ENS2_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB27897:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L4632
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L4626:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeISt4pairIN2v88internal8compiler9RpoNumberEiES5_St9_IdentityIS5_ESt4lessIS5_ENS2_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L4626
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4632:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE27897:
	.size	_ZNSt8_Rb_treeISt4pairIN2v88internal8compiler9RpoNumberEiES5_St9_IdentityIS5_ESt4lessIS5_ENS2_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeISt4pairIN2v88internal8compiler9RpoNumberEiES5_St9_IdentityIS5_ESt4lessIS5_ENS2_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag.str1.1,"aMS",@progbits,1
.LC85:
	.string	"vector::_M_range_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag
	.type	_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag, @function
_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag:
.LFB28632:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rcx
	je	.L4737
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	subq	%rdx, %r14
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	movq	%r14, %rax
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	sarq	$3, %rax
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	24(%rbx), %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%rax, %rdx
	jb	.L4638
	movq	%rdi, %r8
	subq	%rsi, %r8
	movq	%r8, %r9
	sarq	$3, %r9
	cmpq	%r9, %rax
	jnb	.L4639
	movl	$16, %ecx
	movq	%rdi, %rdx
	leaq	-8(%r14), %rax
	subq	%r14, %rcx
	subq	%r14, %rdx
	shrq	$3, %rax
	testq	%rcx, %rcx
	leaq	16(%rdi), %rcx
	setle	%sil
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L4677
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L4677
	leaq	1(%rax), %r8
	xorl	%eax, %eax
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L4641:
	movdqu	(%rdx,%rax), %xmm1
	movups	%xmm1, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L4641
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	leaq	(%rdx,%rcx), %rsi
	addq	%rdi, %rcx
	cmpq	%rax, %r8
	je	.L4643
	movq	(%rsi), %rax
	movq	%rax, (%rcx)
.L4643:
	addq	%r14, 16(%rbx)
	cmpq	%r12, %rdx
	je	.L4644
	subq	%r12, %rdx
	movq	%r12, %rsi
	subq	%rdx, %rdi
	call	memmove@PLT
.L4644:
	movq	%r14, %rdx
.L4740:
	addq	$16, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L4638:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	movl	$268435455, %r14d
	movq	%r14, %rdx
	subq	%rsi, %rdi
	sarq	$3, %rdi
	subq	%rdi, %rdx
	cmpq	%rdx, %rax
	ja	.L4741
	cmpq	%rdi, %rax
	cmovb	%rdi, %rax
	addq	%rdi, %rax
	jc	.L4658
	testq	%rax, %rax
	jne	.L4742
	xorl	%edi, %edi
	xorl	%eax, %eax
.L4660:
	cmpq	%rsi, %r12
	je	.L4683
	leaq	-8(%r12), %r10
	leaq	15(%rax), %rdx
	subq	%rsi, %r10
	subq	%rsi, %rdx
	movq	%r10, %r9
	shrq	$3, %r9
	cmpq	$30, %rdx
	jbe	.L4684
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r9
	je	.L4684
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %r8
	shrq	%r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L4665:
	movdqu	(%rsi,%rdx), %xmm5
	movups	%xmm5, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%r8, %rdx
	jne	.L4665
	movq	%r9, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %r8
	addq	%r8, %rsi
	addq	%rax, %r8
	cmpq	%rdx, %r9
	je	.L4667
	movq	(%rsi), %rdx
	movq	%rdx, (%r8)
.L4667:
	leaq	8(%rax,%r10), %rsi
.L4663:
	subq	$8, %rcx
	leaq	15(%r13), %rdx
	subq	%r13, %rcx
	subq	%rsi, %rdx
	movq	%rcx, %r8
	shrq	$3, %r8
	cmpq	$30, %rdx
	jbe	.L4685
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r8
	je	.L4685
	leaq	1(%r8), %r10
	xorl	%edx, %edx
	movq	%r10, %r9
	shrq	%r9
	salq	$4, %r9
	.p2align 4,,10
	.p2align 3
.L4669:
	movdqu	0(%r13,%rdx), %xmm2
	movups	%xmm2, (%rsi,%rdx)
	addq	$16, %rdx
	cmpq	%r9, %rdx
	jne	.L4669
	movq	%r10, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %r8
	addq	%r8, %r13
	addq	%rsi, %r8
	cmpq	%rdx, %r10
	je	.L4671
	movq	0(%r13), %rdx
	movq	%rdx, (%r8)
.L4671:
	movq	16(%rbx), %rdx
	leaq	8(%rsi,%rcx), %r8
	cmpq	%rdx, %r12
	je	.L4672
	subq	%r12, %rdx
	leaq	-8(%rdx), %r10
	leaq	24(%rsi,%rcx), %rdx
	movq	%r10, %r9
	shrq	$3, %r9
	cmpq	%rdx, %r12
	leaq	16(%r12), %rdx
	setnb	%cl
	cmpq	%rdx, %r8
	setnb	%dl
	orb	%dl, %cl
	je	.L4686
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r9
	je	.L4686
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L4674:
	movdqu	(%r12,%rdx), %xmm6
	movups	%xmm6, (%r8,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L4674
	movq	%r9, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %r12
	addq	%r8, %rcx
	cmpq	%rdx, %r9
	je	.L4676
	movq	(%r12), %rdx
	movq	%rdx, (%rcx)
.L4676:
	leaq	8(%r8,%r10), %r8
.L4672:
	movq	%rax, %xmm0
	movq	%r8, %xmm7
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 8(%rbx)
.L4635:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4639:
	.cfi_restore_state
	leaq	0(%r13,%r8), %rsi
	cmpq	%rsi, %rcx
	je	.L4678
	subq	$8, %rcx
	leaq	16(%r8,%r13), %rdx
	subq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	%rdx, %rdi
	leaq	16(%rdi), %rdx
	setnb	%r10b
	cmpq	%rdx, %rsi
	setnb	%dl
	orb	%dl, %r10b
	je	.L4679
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L4679
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %r10
	shrq	%r10
	salq	$4, %r10
	.p2align 4,,10
	.p2align 3
.L4647:
	movdqu	(%rsi,%rdx), %xmm3
	movups	%xmm3, (%rdi,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %r10
	jne	.L4647
	movq	%rcx, %r11
	andq	$-2, %r11
	leaq	0(,%r11,8), %r10
	leaq	(%rsi,%r10), %rdx
	addq	%rdi, %r10
	cmpq	%r11, %rcx
	je	.L4649
	movq	(%rdx), %rdx
	movq	%rdx, (%r10)
.L4649:
	movq	16(%rbx), %r10
.L4645:
	movq	%rax, %rdx
	subq	%r9, %rdx
	leaq	(%r10,%rdx,8), %rdx
	movq	%rdx, 16(%rbx)
	cmpq	%r12, %rdi
	je	.L4650
	subq	%r12, %rdi
	subq	%r9, %rax
	leaq	-8(%rdi), %rcx
	leaq	16(%r10,%rax,8), %rax
	shrq	$3, %rcx
	cmpq	%rax, %r12
	leaq	16(%r12), %rax
	setnb	%dil
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %dil
	je	.L4680
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L4680
	addq	$1, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L4652:
	movdqu	(%r12,%rax), %xmm4
	movups	%xmm4, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L4652
	movq	%rcx, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rax
	leaq	(%r12,%rax), %rdi
	addq	%rax, %rdx
	cmpq	%r9, %rcx
	je	.L4654
	movq	(%rdi), %rax
	movq	%rax, (%rdx)
.L4654:
	movq	16(%rbx), %rdx
.L4650:
	addq	%r8, %rdx
	movq	%rdx, 16(%rbx)
	cmpq	%rsi, %r13
	je	.L4635
	movq	%r8, %rdx
	jmp	.L4740
	.p2align 4,,10
	.p2align 3
.L4737:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L4677:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L4640:
	movq	(%rdx,%rcx,8), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rsi, %rax
	jne	.L4640
	jmp	.L4643
	.p2align 4,,10
	.p2align 3
.L4679:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L4646:
	movq	(%rsi,%rdx,8), %r10
	movq	%r10, (%rdi,%rdx,8)
	movq	%rdx, %r10
	addq	$1, %rdx
	cmpq	%r10, %rcx
	jne	.L4646
	jmp	.L4649
	.p2align 4,,10
	.p2align 3
.L4684:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L4664:
	movq	(%rsi,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %r9
	jne	.L4664
	jmp	.L4667
	.p2align 4,,10
	.p2align 3
.L4685:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L4668:
	movq	0(%r13,%rdx,8), %r9
	movq	%r9, (%rsi,%rdx,8)
	movq	%rdx, %r9
	addq	$1, %rdx
	cmpq	%r9, %r8
	jne	.L4668
	jmp	.L4671
	.p2align 4,,10
	.p2align 3
.L4680:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L4651:
	movq	(%r12,%rax,8), %rdi
	movq	%rdi, (%rdx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rcx, %rdi
	jne	.L4651
	jmp	.L4654
	.p2align 4,,10
	.p2align 3
.L4686:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L4673:
	movq	(%r12,%rdx,8), %rcx
	movq	%rcx, (%r8,%rdx,8)
	movq	%rdx, %rcx
	addq	$1, %rdx
	cmpq	%r9, %rcx
	jne	.L4673
	jmp	.L4676
	.p2align 4,,10
	.p2align 3
.L4678:
	movq	%rdi, %r10
	jmp	.L4645
	.p2align 4,,10
	.p2align 3
.L4683:
	movq	%rax, %rsi
	jmp	.L4663
.L4742:
	cmpq	$268435455, %rax
	cmova	%r14, %rax
	leaq	0(,%rax,8), %r14
	movq	%r14, %rsi
.L4659:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L4743
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L4662:
	movq	8(%rbx), %rsi
	leaq	(%rax,%r14), %rdi
	jmp	.L4660
.L4743:
	movq	%rcx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rcx
	jmp	.L4662
.L4658:
	movl	$2147483640, %esi
	movl	$2147483640, %r14d
	jmp	.L4659
.L4741:
	leaq	.LC85(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE28632:
	.size	_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag, .-_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag
	.section	.text._ZN2v88internal8compiler10SpillRange8TryMergeEPS2_.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler10SpillRange8TryMergeEPS2_.part.0, @function
_ZN2v88internal8compiler10SpillRange8TryMergeEPS2_.part.0:
.LFB31878:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	40(%rsi), %eax
	cmpl	40(%rdi), %eax
	jle	.L4745
	cmpl	$2147483647, %eax
	jne	.L4769
.L4745:
	movq	32(%rbx), %rsi
	movl	$2147483647, 40(%rbx)
	xorl	%ecx, %ecx
	movq	32(%rdi), %rax
	testq	%rsi, %rsi
	jne	.L4746
	jmp	.L4751
	.p2align 4,,10
	.p2align 3
.L4749:
	movl	(%rax), %r8d
	movq	%rax, %rdx
	cmpl	%r8d, (%rsi)
	jge	.L4752
	movq	%rsi, %rdx
	movq	%rax, %rsi
.L4752:
	testq	%rcx, %rcx
	je	.L4770
	movq	%rdx, 8(%rcx)
	movq	8(%rdx), %rax
.L4754:
	movq	%rdx, %rcx
.L4746:
	testq	%rax, %rax
	jne	.L4749
	testq	%rcx, %rcx
	je	.L4771
	movq	%rsi, 8(%rcx)
.L4751:
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rax
	movq	$0, 32(%rbx)
	movq	%rcx, %rdx
	cmpq	%rcx, %rax
	je	.L4748
	.p2align 4,,10
	.p2align 3
.L4755:
	movq	(%rax), %rdx
	addq	$8, %rax
	movq	%rdi, 104(%rdx)
	cmpq	%rax, %rcx
	jne	.L4755
	movq	16(%rbx), %rcx
	movq	8(%rbx), %rdx
.L4748:
	movq	16(%rdi), %rsi
	call	_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPS4_S7_EEEEvSC_T_SD_St20forward_iterator_tag
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	je	.L4756
	movq	%rax, 16(%rbx)
.L4756:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4770:
	.cfi_restore_state
	movq	%rdx, 32(%rdi)
	movq	8(%rdx), %rax
	jmp	.L4754
	.p2align 4,,10
	.p2align 3
.L4771:
	movq	%rsi, 32(%rdi)
	jmp	.L4751
	.p2align 4,,10
	.p2align 3
.L4769:
	movl	%eax, 40(%rdi)
	jmp	.L4745
	.cfi_endproc
.LFE31878:
	.size	_ZN2v88internal8compiler10SpillRange8TryMergeEPS2_.part.0, .-_ZN2v88internal8compiler10SpillRange8TryMergeEPS2_.part.0
	.section	.text._ZN2v88internal8compiler10SpillRange8TryMergeEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler10SpillRange8TryMergeEPS2_
	.type	_ZN2v88internal8compiler10SpillRange8TryMergeEPS2_, @function
_ZN2v88internal8compiler10SpillRange8TryMergeEPS2_:
.LFB23330:
	.cfi_startproc
	endbr64
	cmpl	$-1, 44(%rdi)
	jne	.L4772
	cmpl	$-1, 44(%rsi)
	je	.L4789
.L4772:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4789:
	movl	48(%rsi), %eax
	cmpl	%eax, 48(%rdi)
	jne	.L4772
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L4774
	movq	32(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L4774
	movl	(%rdx), %ecx
	cmpl	40(%rdi), %ecx
	jge	.L4774
	movl	(%rax), %r8d
	cmpl	40(%rsi), %r8d
	jl	.L4775
	jmp	.L4774
	.p2align 4,,10
	.p2align 3
.L4790:
	cmpl	4(%rax), %ecx
	jl	.L4772
	movq	8(%rax), %rax
.L4777:
	testq	%rax, %rax
	je	.L4774
	testq	%rdx, %rdx
	je	.L4774
	movl	(%rdx), %ecx
	movl	(%rax), %r8d
.L4775:
	cmpl	%r8d, %ecx
	jg	.L4790
	cmpl	%r8d, 4(%rdx)
	jg	.L4772
	movq	8(%rdx), %rdx
	jmp	.L4777
	.p2align 4,,10
	.p2align 3
.L4774:
	jmp	_ZN2v88internal8compiler10SpillRange8TryMergeEPS2_.part.0
	.cfi_endproc
.LFE23330:
	.size	_ZN2v88internal8compiler10SpillRange8TryMergeEPS2_, .-_ZN2v88internal8compiler10SpillRange8TryMergeEPS2_
	.section	.text._ZN2v88internal8compiler15LiveRangeBundle16MergeSpillRangesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LiveRangeBundle16MergeSpillRangesEv
	.type	_ZN2v88internal8compiler15LiveRangeBundle16MergeSpillRangesEv, @function
_ZN2v88internal8compiler15LiveRangeBundle16MergeSpillRangesEv:
.LFB23474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	leaq	16(%rdi), %rbx
	subq	$8, %rsp
	movq	32(%rdi), %r12
	cmpq	%rbx, %r12
	je	.L4791
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L4792:
	movq	32(%r12), %rax
	movq	32(%rax), %rax
	testb	$64, 4(%rax)
	je	.L4793
	movq	104(%rax), %rsi
	testq	%r13, %r13
	je	.L4800
	cmpq	%r13, %rsi
	je	.L4793
	cmpl	$-1, 44(%r13)
	jne	.L4793
	cmpl	$-1, 44(%rsi)
	je	.L4813
	.p2align 4,,10
	.p2align 3
.L4793:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L4792
.L4791:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4800:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rsi, %r13
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	jne	.L4792
	jmp	.L4791
	.p2align 4,,10
	.p2align 3
.L4813:
	movl	48(%rsi), %eax
	cmpl	%eax, 48(%r13)
	jne	.L4793
	movq	32(%r13), %rax
	testq	%rax, %rax
	je	.L4794
	movq	32(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L4794
	movl	(%rdx), %ecx
	cmpl	40(%r13), %ecx
	jge	.L4794
	movl	(%rax), %edi
	cmpl	40(%rsi), %edi
	jl	.L4795
	.p2align 4,,10
	.p2align 3
.L4794:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler10SpillRange8TryMergeEPS2_.part.0
	jmp	.L4793
	.p2align 4,,10
	.p2align 3
.L4814:
	cmpl	%ecx, 4(%rax)
	jg	.L4793
	movq	8(%rax), %rax
.L4797:
	testq	%rax, %rax
	je	.L4794
	testq	%rdx, %rdx
	je	.L4794
	movl	(%rdx), %ecx
	movl	(%rax), %edi
.L4795:
	cmpl	%ecx, %edi
	jl	.L4814
	cmpl	%edi, 4(%rdx)
	jg	.L4793
	movq	8(%rdx), %rdx
	jmp	.L4797
	.cfi_endproc
.LFE23474:
	.size	_ZN2v88internal8compiler15LiveRangeBundle16MergeSpillRangesEv, .-_ZN2v88internal8compiler15LiveRangeBundle16MergeSpillRangesEv
	.section	.text._ZN2v88internal8compiler15OperandAssigner16AssignSpillSlotsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15OperandAssigner16AssignSpillSlotsEv
	.type	_ZN2v88internal8compiler15OperandAssigner16AssignSpillSlotsEv, @function
_ZN2v88internal8compiler15OperandAssigner16AssignSpillSlotsEv:
.LFB23619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	168(%r13), %rbx
	movq	176(%r13), %r14
	cmpq	%r14, %rbx
	je	.L4816
	.p2align 4,,10
	.p2align 3
.L4818:
	movq	(%rbx), %r15
	movq	496(%r13), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	testq	%r15, %r15
	je	.L4817
	movq	80(%r15), %rdi
	testq	%rdi, %rdi
	je	.L4817
	call	_ZN2v88internal8compiler15LiveRangeBundle16MergeSpillRangesEv
.L4817:
	addq	$8, %rbx
	movq	(%r12), %r13
	cmpq	%rbx, %r14
	jne	.L4818
.L4816:
	movq	328(%r13), %rdx
	movq	%r13, %rax
	xorl	%ecx, %ecx
	cmpq	%rdx, 336(%r13)
	je	.L4815
	.p2align 4,,10
	.p2align 3
.L4819:
	movq	496(%rax), %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	-56(%rbp), %rcx
	movq	328(%r13), %rdx
	movq	336(%r13), %r14
	movq	(%rdx,%rcx,8), %r15
	addq	$1, %rcx
	testq	%r15, %r15
	je	.L4822
	movq	%r14, %rsi
	movq	8(%r15), %rdi
	subq	%rdx, %rsi
	sarq	$3, %rsi
	movq	%rsi, %rax
	cmpq	%rdi, 16(%r15)
	je	.L4823
	cmpq	%rsi, %rcx
	jnb	.L4824
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L4830:
	movq	(%rdx,%rbx,8), %rsi
	testq	%rsi, %rsi
	je	.L4825
	movq	8(%rsi), %rax
	cmpq	%rax, 16(%rsi)
	je	.L4825
	cmpl	$-1, 44(%r15)
	jne	.L4825
	cmpl	$-1, 44(%rsi)
	jne	.L4825
	movl	48(%rsi), %eax
	cmpl	%eax, 48(%r15)
	je	.L4859
	.p2align 4,,10
	.p2align 3
.L4825:
	movq	%r14, %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L4830
.L4823:
	cmpq	%rax, %rcx
	jnb	.L4824
.L4860:
	movq	(%r12), %rax
	jmp	.L4819
	.p2align 4,,10
	.p2align 3
.L4822:
	movq	%r14, %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rcx
	jb	.L4860
	.p2align 4,,10
	.p2align 3
.L4824:
	cmpq	%r14, %rdx
	je	.L4815
	movq	%rdx, %r13
	movl	$-2, %r15d
	.p2align 4,,10
	.p2align 3
.L4833:
	movq	(%r12), %rax
	movq	0(%r13), %rbx
	movq	496(%rax), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	testq	%rbx, %rbx
	je	.L4831
	movq	8(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	je	.L4831
	movl	44(%rbx), %edx
	cmpl	$-1, %edx
	jne	.L4831
	movq	(%r12), %rax
	movq	8(%rax), %rcx
	movl	48(%rbx), %eax
	movl	%eax, %r8d
	leal	14(%rax), %esi
	movl	4(%rcx), %edi
	addl	$7, %r8d
	cmovns	%r8d, %esi
	andl	$15, %eax
	sarl	$3, %esi
	cmpl	$1, %eax
	sbbl	%r8d, %r8d
	testl	%eax, %eax
	movl	%edi, %eax
	cmove	%r15d, %edx
	subl	%r8d, %eax
	addl	%esi, %eax
	andl	%edx, %eax
	movl	%eax, 4(%rcx)
	movl	%eax, %edx
	subl	12(%rcx), %eax
	subl	$1, %eax
	subl	%edi, %edx
	addl	%edx, 8(%rcx)
	movl	%eax, 44(%rbx)
.L4831:
	addq	$8, %r13
	cmpq	%r13, %r14
	jne	.L4833
.L4815:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4859:
	.cfi_restore_state
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.L4826
	movq	32(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L4826
	movl	(%rdi), %r8d
	cmpl	40(%r15), %r8d
	jge	.L4826
	movl	(%rax), %r9d
	cmpl	40(%rsi), %r9d
	jl	.L4827
	jmp	.L4826
	.p2align 4,,10
	.p2align 3
.L4861:
	cmpl	%r8d, 4(%rax)
	jg	.L4825
	movq	8(%rax), %rax
.L4829:
	testq	%rax, %rax
	je	.L4826
	testq	%rdi, %rdi
	je	.L4826
	movl	(%rdi), %r8d
	movl	(%rax), %r9d
.L4827:
	cmpl	%r9d, %r8d
	jg	.L4861
	cmpl	%r9d, 4(%rdi)
	jg	.L4825
	movq	8(%rdi), %rdi
	jmp	.L4829
	.p2align 4,,10
	.p2align 3
.L4826:
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal8compiler10SpillRange8TryMergeEPS2_.part.0
	movq	328(%r13), %rdx
	movq	336(%r13), %r14
	movq	-56(%rbp), %rcx
	jmp	.L4825
	.cfi_endproc
.LFE23619:
	.size	_ZN2v88internal8compiler15OperandAssigner16AssignSpillSlotsEv, .-_ZN2v88internal8compiler15OperandAssigner16AssignSpillSlotsEv
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_:
.LFB28769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	24(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L4876
	movq	(%rsi), %rax
	movq	16(%rax), %rax
	movl	(%rax), %r8d
	jmp	.L4865
	.p2align 4,,10
	.p2align 3
.L4877:
	movq	16(%rbx), %rax
	movl	$1, %r9d
	testq	%rax, %rax
	je	.L4866
.L4878:
	movq	%rax, %rbx
.L4865:
	movq	32(%rbx), %rax
	movq	16(%rax), %rax
	movl	(%rax), %ecx
	cmpl	%ecx, %r8d
	jl	.L4877
	movq	24(%rbx), %rax
	xorl	%r9d, %r9d
	testq	%rax, %rax
	jne	.L4878
.L4866:
	movq	%rbx, %rsi
	testb	%r9b, %r9b
	jne	.L4864
.L4869:
	xorl	%edx, %edx
	movq	%rbx, %rax
	cmpl	%r8d, %ecx
	cmovl	%rdx, %rax
	cmovl	%rsi, %rdx
.L4871:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4876:
	.cfi_restore_state
	leaq	16(%rdi), %rbx
.L4864:
	xorl	%eax, %eax
	movq	%rbx, %rdx
	cmpq	%rbx, 32(%rdi)
	je	.L4871
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rsi
	movq	32(%rax), %rdx
	movq	%rax, %rbx
	movq	16(%rdx), %rdx
	movl	(%rdx), %ecx
	movq	(%r12), %rdx
	movq	16(%rdx), %rdx
	movl	(%rdx), %r8d
	jmp	.L4869
	.cfi_endproc
.LFE28769:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_
	.section	.text._ZN2v88internal8compiler15LiveRangeBundle11TryAddRangeEPNS1_9LiveRangeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LiveRangeBundle11TryAddRangeEPNS1_9LiveRangeE
	.type	_ZN2v88internal8compiler15LiveRangeBundle11TryAddRangeEPNS1_9LiveRangeE, @function
_ZN2v88internal8compiler15LiveRangeBundle11TryAddRangeEPNS1_9LiveRangeE:
.LFB23472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%rsi), %r12
	movq	88(%rdi), %rdi
	movq	%rsi, -56(%rbp)
	testq	%r12, %r12
	je	.L4880
	leaq	72(%rbx), %r13
	cmpq	%r13, %rdi
	jne	.L4881
	jmp	.L4880
	.p2align 4,,10
	.p2align 3
.L4921:
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdi
.L4883:
	cmpq	%r13, %rdi
	je	.L4880
.L4881:
	movl	(%r12), %eax
	cmpl	%eax, 36(%rdi)
	jle	.L4921
	movl	32(%rdi), %eax
	cmpl	%eax, 4(%r12)
	jg	.L4905
	movq	8(%r12), %r12
	testq	%r12, %r12
	jne	.L4883
.L4880:
	leaq	-56(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_
	testq	%rdx, %rdx
	je	.L4902
	leaq	16(%rbx), %rcx
	movl	$1, %r12d
	testq	%rax, %rax
	je	.L4922
.L4885:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$39, %rax
	jbe	.L4923
	leaq	40(%rsi), %rax
	movq	%rax, 16(%rdi)
.L4887:
	movq	-56(%rbp), %rax
	movl	%r12d, %edi
	movq	%rax, 32(%rsi)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
.L4902:
	movq	-56(%rbp), %rax
	leaq	72(%rbx), %r14
	movq	16(%rax), %r13
	movq	%rbx, 80(%rax)
	testq	%r13, %r13
	je	.L4899
	.p2align 4,,10
	.p2align 3
.L4900:
	movq	80(%rbx), %r12
	movl	0(%r13), %edx
	movq	0(%r13), %r15
	testq	%r12, %r12
	jne	.L4890
	jmp	.L4924
	.p2align 4,,10
	.p2align 3
.L4925:
	movq	16(%r12), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L4891
.L4926:
	movq	%rax, %r12
.L4890:
	movl	32(%r12), %ecx
	cmpl	%ecx, %edx
	jl	.L4925
	movq	24(%r12), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L4926
.L4891:
	testb	%sil, %sil
	jne	.L4889
	cmpl	%ecx, %edx
	jle	.L4895
.L4903:
	movl	$1, %r8d
	cmpq	%r12, %r14
	jne	.L4927
.L4896:
	movq	56(%rbx), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$39, %rax
	jbe	.L4928
	leaq	40(%rsi), %rax
	movq	%rax, 16(%rdi)
.L4898:
	movq	%r15, 32(%rsi)
	movq	%r14, %rcx
	movq	%r12, %rdx
	movl	%r8d, %edi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 104(%rbx)
.L4895:
	movq	8(%r13), %r13
	testq	%r13, %r13
	jne	.L4900
.L4899:
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4924:
	.cfi_restore_state
	movq	%r14, %r12
.L4889:
	cmpq	%r12, 88(%rbx)
	je	.L4903
	movq	%r12, %rdi
	movl	%edx, -64(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	-64(%rbp), %edx
	cmpl	32(%rax), %edx
	jle	.L4895
	movl	$1, %r8d
	cmpq	%r12, %r14
	je	.L4896
.L4927:
	xorl	%r8d, %r8d
	cmpl	32(%r12), %edx
	setl	%r8b
	jmp	.L4896
	.p2align 4,,10
	.p2align 3
.L4928:
	movl	$40, %esi
	movl	%r8d, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-64(%rbp), %r8d
	movq	%rax, %rsi
	jmp	.L4898
	.p2align 4,,10
	.p2align 3
.L4922:
	cmpq	%rdx, %rcx
	je	.L4885
	movq	32(%rdx), %rax
	xorl	%r12d, %r12d
	movq	16(%rax), %rsi
	movq	-56(%rbp), %rax
	movq	16(%rax), %rax
	movl	(%rax), %eax
	cmpl	%eax, (%rsi)
	setg	%r12b
	jmp	.L4885
	.p2align 4,,10
	.p2align 3
.L4905:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4923:
	.cfi_restore_state
	movl	$40, %esi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L4887
	.cfi_endproc
.LFE23472:
	.size	_ZN2v88internal8compiler15LiveRangeBundle11TryAddRangeEPNS1_9LiveRangeE, .-_ZN2v88internal8compiler15LiveRangeBundle11TryAddRangeEPNS1_9LiveRangeE
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE11equal_rangeERKS4_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE11equal_rangeERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE11equal_rangeERKS4_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE11equal_rangeERKS4_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE11equal_rangeERKS4_:
.LFB28805:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	leaq	16(%rdi), %r8
	testq	%rax, %rax
	je	.L5003
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	(%rsi), %r9
	movq	16(%r9), %rdx
	movl	(%rdx), %edi
	jmp	.L4931
	.p2align 4,,10
	.p2align 3
.L4932:
	jl	.L4933
	setg	%cl
.L4943:
	testb	%cl, %cl
	je	.L4944
.L4934:
	movq	%rax, %r8
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L4930
.L4931:
	movq	32(%rax), %rdx
	movq	16(%rdx), %rcx
	cmpl	%edi, (%rcx)
	jne	.L4932
	movl	4(%rdx), %esi
	movl	4(%r9), %ecx
	shrl	$22, %esi
	shrl	$22, %ecx
	andl	$63, %esi
	andl	$63, %ecx
	cmpl	%ecx, %esi
	jl	.L4933
	jg	.L4934
	movq	24(%rdx), %rcx
	movq	24(%r9), %rsi
	cmpq	%rsi, %rcx
	je	.L5009
	testq	%rcx, %rcx
	je	.L4937
	testq	%rsi, %rsi
	je	.L4933
	movl	24(%rsi), %r10d
	movl	24(%rcx), %esi
	cmpl	%esi, %r10d
	setg	%cl
	jne	.L4939
	movq	32(%r9), %rcx
	movq	32(%rdx), %r11
	movl	88(%rcx), %ebx
	cmpl	%ebx, 88(%r11)
	setl	%cl
.L4939:
	testb	%cl, %cl
	je	.L5010
	.p2align 4,,10
	.p2align 3
.L4933:
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L4931
.L4930:
	movq	%r8, %rax
.L5000:
	movq	%r8, %rdx
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5009:
	.cfi_restore_state
	movq	32(%r9), %rcx
	movq	32(%rdx), %rdx
	movl	88(%rcx), %ebx
	cmpl	%ebx, 88(%rdx)
	jl	.L4933
.L5006:
	movl	88(%rdx), %ebx
	cmpl	%ebx, 88(%rcx)
	setl	%cl
	jmp	.L4943
	.p2align 4,,10
	.p2align 3
.L5010:
	cmpl	%r10d, %esi
	setg	%cl
	jne	.L4943
	movq	32(%r9), %rcx
	movq	32(%rdx), %rdx
	movl	88(%rdx), %ebx
	cmpl	%ebx, 88(%rcx)
	setl	%cl
	jmp	.L4943
.L4944:
	movq	24(%rax), %rcx
	movq	16(%rax), %rdx
	testq	%rcx, %rcx
	jne	.L4947
	jmp	.L4963
	.p2align 4,,10
	.p2align 3
.L4952:
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L4963
.L4947:
	movq	32(%rcx), %r10
	movq	16(%r10), %rsi
	movl	(%rsi), %esi
	cmpl	%esi, %edi
	setl	%r11b
	je	.L5011
.L4954:
	testb	%r11b, %r11b
	je	.L4952
.L4951:
	movq	%rcx, %r8
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L4947
	.p2align 4,,10
	.p2align 3
.L4963:
	testq	%rdx, %rdx
	je	.L5000
.L5013:
	movq	32(%rdx), %rsi
	movq	16(%rsi), %rcx
	movl	(%rcx), %ecx
	cmpl	%ecx, %edi
	setg	%r10b
	je	.L5012
.L4961:
	testb	%r10b, %r10b
	jne	.L4958
.L4959:
	movq	%rdx, %rax
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L5013
	jmp	.L5000
	.p2align 4,,10
	.p2align 3
.L5012:
	movl	4(%rsi), %r10d
	movl	4(%r9), %ecx
	shrl	$22, %r10d
	shrl	$22, %ecx
	andl	$63, %r10d
	andl	$63, %ecx
	cmpl	%ecx, %r10d
	jl	.L4958
	jg	.L4959
	movq	24(%rsi), %rcx
	movq	24(%r9), %r10
	cmpq	%r10, %rcx
	je	.L5008
	testq	%rcx, %rcx
	je	.L4959
	testq	%r10, %r10
	jne	.L5014
	.p2align 4,,10
	.p2align 3
.L4958:
	movq	24(%rdx), %rdx
	jmp	.L4963
	.p2align 4,,10
	.p2align 3
.L5011:
	movl	4(%r9), %r11d
	movl	4(%r10), %esi
	shrl	$22, %r11d
	shrl	$22, %esi
	andl	$63, %r11d
	andl	$63, %esi
	cmpl	%esi, %r11d
	jl	.L4951
	jg	.L4952
	movq	24(%r9), %rsi
	movq	24(%r10), %r11
	cmpq	%r11, %rsi
	je	.L5007
	testq	%rsi, %rsi
	je	.L4952
	testq	%r11, %r11
	je	.L4951
	movl	24(%r11), %ebx
	movl	24(%rsi), %esi
	cmpl	%esi, %ebx
	setg	%r11b
	jne	.L4954
.L5007:
	movq	32(%r9), %r11
	movq	32(%r10), %rsi
	movl	88(%rsi), %ebx
	cmpl	%ebx, 88(%r11)
	setl	%r11b
	jmp	.L4954
	.p2align 4,,10
	.p2align 3
.L5014:
	movl	24(%r10), %r11d
	movl	24(%rcx), %ecx
	cmpl	%ecx, %r11d
	setg	%r10b
	jne	.L4961
.L5008:
	movq	32(%rsi), %rsi
	movq	32(%r9), %rcx
	movl	88(%rcx), %ebx
	cmpl	%ebx, 88(%rsi)
	setl	%r10b
	jmp	.L4961
	.p2align 4,,10
	.p2align 3
.L4937:
	testq	%rsi, %rsi
	jne	.L4934
	movq	32(%rdx), %rdx
	movq	32(%r9), %rcx
	jmp	.L5006
	.p2align 4,,10
	.p2align 3
.L5003:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movq	%r8, %rax
	movq	%r8, %rdx
	ret
	.cfi_endproc
.LFE28805:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE11equal_rangeERKS4_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE11equal_rangeERKS4_
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE5eraseERKS4_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE5eraseERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE5eraseERKS4_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE5eraseERKS4_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE5eraseERKS4_:
.LFB27737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE11equal_rangeERKS4_
	movq	48(%rbx), %r15
	movq	%rdx, %r14
	movq	%rax, %r12
	cmpq	32(%rbx), %rax
	je	.L5036
.L5016:
	leaq	16(%rbx), %r13
	cmpq	%r14, %rax
	je	.L5037
	.p2align 4,,10
	.p2align 3
.L5024:
	movq	%r12, %rdi
	movq	%r12, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	48(%rbx), %rax
	subq	$1, %rax
	movq	%rax, 48(%rbx)
	cmpq	%r12, %r14
	jne	.L5024
	subq	%rax, %r15
.L5019:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5036:
	.cfi_restore_state
	leaq	16(%rbx), %r13
	cmpq	%r13, %rdx
	jne	.L5016
	movq	24(%rbx), %r14
	testq	%r14, %r14
	je	.L5020
.L5023:
	movq	24(%r14), %r12
	testq	%r12, %r12
	je	.L5021
.L5022:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L5022
.L5021:
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L5023
.L5020:
	movq	$0, 24(%rbx)
	movq	%r13, 32(%rbx)
	movq	%r13, 40(%rbx)
	movq	$0, 48(%rbx)
	jmp	.L5019
	.p2align 4,,10
	.p2align 3
.L5037:
	xorl	%r15d, %r15d
	jmp	.L5019
	.cfi_endproc
.LFE27737:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE5eraseERKS4_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE5eraseERKS4_
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator22MaybeUndoPreviousSplitEPNS1_9LiveRangeE.str1.1,"aMS",@progbits,1
.LC86:
	.string	"Recombining %d:%d with %d\n"
.LC87:
	.string	"No recombine for %d:%d to %d\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator22MaybeUndoPreviousSplitEPNS1_9LiveRangeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator22MaybeUndoPreviousSplitEPNS1_9LiveRangeE
	.type	_ZN2v88internal8compiler19LinearScanAllocator22MaybeUndoPreviousSplitEPNS1_9LiveRangeE, @function
_ZN2v88internal8compiler19LinearScanAllocator22MaybeUndoPreviousSplitEPNS1_9LiveRangeE:
.LFB23496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	40(%rsi), %rax
	testq	%rax, %rax
	je	.L5038
	movq	(%rdi), %rdx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movl	488(%rdx), %edx
	andl	$4, %edx
	testb	$32, 6(%rax)
	je	.L5040
	movq	%rax, -32(%rbp)
	testl	%edx, %edx
	jne	.L5057
.L5041:
	leaq	-32(%rbp), %rsi
	leaq	40(%r12), %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE5eraseERKS4_
	movq	40(%rbx), %rdx
	movq	8(%rbx), %rax
	movq	16(%rdx), %rdx
	movq	%rdx, 8(%rax)
	movq	40(%rbx), %rax
	movq	$0, 16(%rax)
	movq	40(%rbx), %rax
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rbx)
	movq	$0, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L5058
	.p2align 4,,10
	.p2align 3
.L5042:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L5042
	movq	40(%rbx), %rax
	movq	24(%rax), %rax
	movq	%rax, 16(%rdx)
	movq	40(%rbx), %rax
.L5043:
	movq	$0, 24(%rax)
	movq	40(%rbx), %rax
	movq	40(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movq	$0, 40(%rax)
.L5038:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5059
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5040:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L5038
	movq	32(%rsi), %rdx
	movl	(%rax), %ecx
	leaq	.LC87(%rip), %rdi
	xorl	%eax, %eax
	movl	88(%rdx), %esi
	movl	(%rbx), %edx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5038
	.p2align 4,,10
	.p2align 3
.L5057:
	movq	32(%rsi), %rdx
	movl	(%rax), %ecx
	leaq	.LC86(%rip), %rdi
	xorl	%eax, %eax
	movl	88(%rdx), %esi
	movl	(%rbx), %edx
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5041
	.p2align 4,,10
	.p2align 3
.L5058:
	movq	40(%rbx), %rax
	movq	24(%rax), %rdx
	movq	%rdx, 24(%rbx)
	jmp	.L5043
.L5059:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23496:
	.size	_ZN2v88internal8compiler19LinearScanAllocator22MaybeUndoPreviousSplitEPNS1_9LiveRangeE, .-_ZN2v88internal8compiler19LinearScanAllocator22MaybeUndoPreviousSplitEPNS1_9LiveRangeE
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator18SpillNotLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC88:
	.string	"Keeping reactivated fixed range for %s\n"
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator18SpillNotLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeE.str1.1,"aMS",@progbits,1
.LC89:
	.string	"Putting back %d:%d\n"
.LC90:
	.string	"Next use at %d\n"
.LC91:
	.string	"Marking %d:%d to recombine\n"
.LC92:
	.string	"Keeping %d:%d in %s\n"
.LC93:
	.string	"Scheduling %d:%d\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator18SpillNotLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator18SpillNotLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeE
	.type	_ZN2v88internal8compiler19LinearScanAllocator18SpillNotLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeE, @function
_ZN2v88internal8compiler19LinearScanAllocator18SpillNotLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeE:
.LFB23497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	%edx, -76(%rbp)
	movq	104(%rdi), %r15
	movl	%ecx, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-64(%rbp), %rax
	movq	%rax, -96(%rbp)
	cmpq	112(%rdi), %r15
	je	.L5060
	.p2align 4,,10
	.p2align 3
.L5061:
	movq	(%r15), %r13
	movq	16(%r14), %rsi
	xorl	%edx, %edx
	movq	8(%r14), %r8
	movq	32(%r13), %r12
	movslq	88(%r12), %r11
	movq	%r11, %rax
	movq	%r11, %r10
	divq	%rsi
	movq	(%r8,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L5063
	movq	(%rax), %rcx
	movq	24(%rcx), %rdi
	jmp	.L5066
	.p2align 4,,10
	.p2align 3
.L5064:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L5063
	movq	24(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L5063
.L5066:
	cmpq	%rdi, %r11
	jne	.L5064
	cmpq	8(%rcx), %r12
	jne	.L5064
	movl	16(%rcx), %eax
	xorl	%edx, %edx
	movl	%eax, -72(%rbp)
	movq	%r11, %rax
	divq	%rsi
	leaq	0(,%rdx,8), %rax
	movq	%rdx, %r11
	movq	%rax, -88(%rbp)
	addq	%r8, %rax
	movq	(%rax), %rdx
	movq	%rdx, %rdi
	.p2align 4,,10
	.p2align 3
.L5089:
	movq	%rdi, %r9
	movq	(%rdi), %rdi
	cmpq	%rdi, %rcx
	jne	.L5089
	movq	(%rdi), %r10
	cmpq	%r9, %rdx
	je	.L5161
	testq	%r10, %r10
	je	.L5092
	movq	24(%r10), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r11
	je	.L5092
	movq	%r9, (%r8,%rdx,8)
	movq	(%rcx), %r10
.L5092:
	movq	%r10, (%r9)
	subq	$1, 32(%r14)
	movl	4(%r13), %eax
	shrl	$7, %eax
	andl	$63, %eax
	cmpl	%eax, -72(%rbp)
	movq	(%rbx), %rax
	je	.L5162
	testb	$4, 488(%rax)
	jne	.L5163
.L5098:
	movl	-76(%rbp), %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	movl	-72(%rbp), %ecx
	movl	4(%rax), %edx
	sall	$22, %ecx
	andl	$-264241153, %edx
	andl	$1069547520, %ecx
	orl	%ecx, %edx
	cmpq	$0, 16(%rax)
	movl	%edx, 4(%rax)
	movq	%rax, -64(%rbp)
	je	.L5104
	movq	(%rbx), %rdx
	testb	$4, 488(%rdx)
	jne	.L5164
.L5099:
	movq	-96(%rbp), %rsi
	leaq	40(%rbx), %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE15_M_insert_equalIRKS4_EESt17_Rb_tree_iteratorIS4_EOT_
.L5104:
	movq	(%rbx), %rax
	testb	$4, 488(%rax)
	jne	.L5165
.L5100:
	movq	112(%rbx), %rdx
	leaq	8(%r15), %rsi
	cmpq	%rsi, %rdx
	je	.L5101
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
	movq	112(%rbx), %rsi
.L5101:
	subq	$8, %rsi
	movq	%rsi, 112(%rbx)
.L5071:
	cmpq	%rsi, %r15
	jne	.L5061
.L5060:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5166
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5063:
	.cfi_restore_state
	testl	%r10d, %r10d
	jns	.L5067
	movq	(%rbx), %rax
	testb	$4, 488(%rax)
	je	.L5095
	movl	4(%r12), %eax
	leaq	.LC26(%rip), %rsi
	shrl	$7, %eax
	andl	$63, %eax
	cmpl	$32, %eax
	je	.L5069
	movl	8(%rbx), %ecx
	cltq
	testl	%ecx, %ecx
	jne	.L5070
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rdi
	movq	(%rdi,%rax,8), %rsi
.L5069:
	leaq	.LC88(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L5095:
	movq	112(%rbx), %rsi
	addq	$8, %r15
	jmp	.L5071
	.p2align 4,,10
	.p2align 3
.L5067:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator22MaybeUndoPreviousSplitEPNS1_9LiveRangeE
	movq	(%rbx), %rax
	testb	$4, 488(%rax)
	jne	.L5167
.L5072:
	movl	-76(%rbp), %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	movq	%rax, %r13
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L5073
	movl	24(%rax), %edx
	cmpl	%edx, -76(%rbp)
	jl	.L5073
	movl	-76(%rbp), %ecx
	jmp	.L5079
	.p2align 4,,10
	.p2align 3
.L5168:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L5078
	movl	24(%rax), %edx
.L5079:
	cmpl	%edx, %ecx
	jg	.L5168
	movq	%rax, 56(%r13)
	jmp	.L5081
	.p2align 4,,10
	.p2align 3
.L5169:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L5076
.L5081:
	movzbl	28(%rax), %edx
	andl	$3, %edx
	cmpb	$2, %dl
	jne	.L5169
	movl	24(%rax), %ecx
	movq	(%rbx), %rax
	movl	%ecx, %edx
	andl	$-4, %edx
	testb	$4, 488(%rax)
	jne	.L5170
.L5103:
	sarl	$2, %ecx
	movq	16(%rax), %rdi
	movl	%edx, -88(%rbp)
	movl	%ecx, %esi
	movl	%ecx, -72(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movl	-72(%rbp), %ecx
	movl	-88(%rbp), %edx
	cmpl	112(%rax), %ecx
	je	.L5083
	subl	$4, %edx
.L5083:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	cmpl	%edx, -76(%rbp)
	jge	.L5084
	call	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	movl	-80(%rbp), %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	movq	(%rbx), %rax
	movq	-72(%rbp), %r8
	testb	$4, 488(%rax)
	jne	.L5171
.L5085:
	orl	$2097152, 4(%r8)
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE
	movq	(%rbx), %rax
	testb	$4, 488(%rax)
	je	.L5100
	.p2align 4,,10
	.p2align 3
.L5165:
	movq	(%r15), %rax
	leaq	.LC55(%rip), %rdi
	movq	32(%rax), %rdx
	movl	88(%rdx), %esi
	movl	(%rax), %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5100
	.p2align 4,,10
	.p2align 3
.L5162:
	testb	$4, 488(%rax)
	je	.L5095
	movslq	-72(%rbp), %rax
	leaq	.LC26(%rip), %rcx
	cmpl	$32, %eax
	je	.L5096
	movl	8(%rbx), %edx
	testl	%edx, %edx
	jne	.L5097
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rdi
	movq	(%rdi,%rax,8), %rcx
.L5096:
	movl	88(%r12), %esi
	movl	0(%r13), %edx
	xorl	%eax, %eax
	addq	$8, %r15
	leaq	.LC92(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	112(%rbx), %rsi
	jmp	.L5071
	.p2align 4,,10
	.p2align 3
.L5161:
	testq	%r10, %r10
	je	.L5108
	movq	24(%r10), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r11
	je	.L5092
	movq	%r9, (%r8,%rdx,8)
	movq	-88(%rbp), %rax
	leaq	24(%r14), %rcx
	addq	8(%r14), %rax
	movq	(%rax), %rdx
	cmpq	%rcx, %rdx
	je	.L5172
.L5093:
	movq	$0, (%rax)
	movq	(%rdi), %r10
	jmp	.L5092
	.p2align 4,,10
	.p2align 3
.L5073:
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L5078
	movl	24(%rax), %edx
	movl	-76(%rbp), %ecx
	jmp	.L5079
	.p2align 4,,10
	.p2align 3
.L5163:
	movl	88(%r12), %esi
	movl	0(%r13), %edx
	leaq	.LC93(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5098
	.p2align 4,,10
	.p2align 3
.L5084:
	call	_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE
	jmp	.L5104
	.p2align 4,,10
	.p2align 3
.L5164:
	movq	32(%rax), %rdx
	leaq	.LC77(%rip), %rdi
	movl	88(%rdx), %esi
	movl	(%rax), %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5099
	.p2align 4,,10
	.p2align 3
.L5167:
	movl	88(%r12), %esi
	movl	0(%r13), %edx
	leaq	.LC89(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5072
	.p2align 4,,10
	.p2align 3
.L5070:
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rdi
	movq	(%rdi,%rax,8), %rsi
	leaq	.LC88(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5095
	.p2align 4,,10
	.p2align 3
.L5097:
	movslq	-72(%rbp), %rax
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rdi
	movq	(%rdi,%rax,8), %rcx
	jmp	.L5096
	.p2align 4,,10
	.p2align 3
.L5171:
	movl	(%r8), %edx
	movl	88(%r12), %esi
	leaq	.LC91(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-72(%rbp), %r8
	jmp	.L5085
	.p2align 4,,10
	.p2align 3
.L5170:
	movl	%edx, %esi
	xorl	%eax, %eax
	leaq	.LC90(%rip), %rdi
	movl	%ecx, -88(%rbp)
	movl	%edx, -72(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%rbx), %rax
	movl	-88(%rbp), %ecx
	movl	-72(%rbp), %edx
	jmp	.L5103
	.p2align 4,,10
	.p2align 3
.L5108:
	movq	%r9, %rdx
	leaq	24(%r14), %rcx
	cmpq	%rcx, %rdx
	jne	.L5093
.L5172:
	movq	%r10, 24(%r14)
	jmp	.L5093
	.p2align 4,,10
	.p2align 3
.L5078:
	movq	$0, 56(%r13)
	.p2align 4,,10
	.p2align 3
.L5076:
	movl	-80(%rbp), %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler17RegisterAllocator5SpillEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
	jmp	.L5104
.L5166:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23497:
	.size	_ZN2v88internal8compiler19LinearScanAllocator18SpillNotLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeE, .-_ZN2v88internal8compiler19LinearScanAllocator18SpillNotLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeE
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator16ReloadLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionE.str1.1,"aMS",@progbits,1
.LC94:
	.string	"No candidate for %d at %d\n"
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator16ReloadLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionE.str1.8,"aMS",@progbits,1
	.align 8
.LC95:
	.string	"Reload %d:%d starting at %d itself\n"
	.align 8
.LC96:
	.string	"Reload %d:%d starting at %d as %d\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator16ReloadLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator16ReloadLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionE
	.type	_ZN2v88internal8compiler19LinearScanAllocator16ReloadLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionE, @function
_ZN2v88internal8compiler19LinearScanAllocator16ReloadLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionE:
.LFB23500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-64(%rbp), %rax
	movq	%rax, -72(%rbp)
	testq	%r15, %r15
	je	.L5173
	movq	%rdi, %r12
	movl	%edx, %ebx
	.p2align 4,,10
	.p2align 3
.L5174:
	movq	8(%r15), %r14
	movl	16(%r15), %r13d
	movq	136(%r14), %r9
	testq	%r9, %r9
	jne	.L5177
	jmp	.L5175
	.p2align 4,,10
	.p2align 3
.L5211:
	movq	40(%r9), %r9
	testq	%r9, %r9
	je	.L5175
.L5177:
	movq	8(%r9), %rax
	cmpl	4(%rax), %ebx
	jge	.L5211
	movq	%r9, 136(%r14)
	movl	%ebx, %esi
	movq	%r9, %rdi
	call	_ZNK2v88internal8compiler9LiveRange6CoversENS1_16LifetimePositionE
	testb	%al, %al
	je	.L5210
	movq	%r9, %rsi
	movq	%r12, %rdi
	movq	%r9, -64(%rbp)
	call	_ZN2v88internal8compiler19LinearScanAllocator22MaybeUndoPreviousSplitEPNS1_9LiveRangeE
	movq	-64(%rbp), %rsi
	movq	16(%rsi), %rax
	cmpl	(%rax), %ebx
	jne	.L5212
	movq	(%r12), %rax
	testb	$4, 488(%rax)
	jne	.L5213
.L5183:
	movl	4(%rsi), %eax
	testb	$1, %al
	jne	.L5214
	cmpl	$32, %r13d
	je	.L5182
	movq	-72(%rbp), %rsi
	leaq	40(%r12), %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE5eraseERKS4_
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movl	%r13d, %edx
	call	_ZN2v88internal8compiler19LinearScanAllocator22AssignRegisterOnReloadEPNS1_9LiveRangeEi
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler19LinearScanAllocator11AddToActiveEPNS1_9LiveRangeE
	.p2align 4,,10
	.p2align 3
.L5182:
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L5174
.L5173:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5215
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5175:
	.cfi_restore_state
	movq	$0, 136(%r14)
.L5210:
	movq	(%r12), %rax
	movq	$0, -64(%rbp)
	testb	$4, 488(%rax)
	je	.L5182
	movl	88(%r14), %esi
	movl	%ebx, %edx
	leaq	.LC94(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5182
	.p2align 4,,10
	.p2align 3
.L5212:
	movl	%ebx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17RegisterAllocator12SplitRangeAtEPNS1_9LiveRangeENS1_16LifetimePositionE
	movq	%rax, %r9
	movq	(%r12), %rax
	testb	$4, 488(%rax)
	jne	.L5216
.L5186:
	cmpl	$32, %r13d
	je	.L5187
	movq	%r9, %rsi
	movq	%r12, %rdi
	movl	%r13d, %edx
	call	_ZN2v88internal8compiler19LinearScanAllocator22AssignRegisterOnReloadEPNS1_9LiveRangeEi
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler19LinearScanAllocator11AddToActiveEPNS1_9LiveRangeE
	jmp	.L5182
	.p2align 4,,10
	.p2align 3
.L5214:
	andl	$-8066, %eax
	sall	$22, %r13d
	movq	%r12, %rdi
	orb	$16, %ah
	andl	$1069547520, %r13d
	movl	%eax, 4(%rsi)
	movq	-64(%rbp), %rdx
	movl	4(%rdx), %eax
	andl	$-264241153, %eax
	orl	%eax, %r13d
	movl	%r13d, 4(%rdx)
	movq	-64(%rbp), %rsi
	call	_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE
	jmp	.L5182
	.p2align 4,,10
	.p2align 3
.L5213:
	movl	88(%r14), %r8d
	movl	(%rsi), %edx
	movl	%ebx, %ecx
	leaq	.LC95(%rip), %rdi
	xorl	%eax, %eax
	movl	%r8d, %esi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-64(%rbp), %rsi
	jmp	.L5183
	.p2align 4,,10
	.p2align 3
.L5187:
	movl	4(%r9), %eax
	movq	%r9, %rsi
	movq	%r12, %rdi
	andl	$-264241153, %eax
	orl	$134217728, %eax
	movl	%eax, 4(%r9)
	call	_ZN2v88internal8compiler19LinearScanAllocator14AddToUnhandledEPNS1_9LiveRangeE
	jmp	.L5182
	.p2align 4,,10
	.p2align 3
.L5216:
	movq	16(%r9), %rax
	movl	(%r9), %r8d
	movq	%r9, -80(%rbp)
	leaq	.LC96(%rip), %rdi
	movl	88(%r14), %esi
	movl	(%rax), %ecx
	movq	-64(%rbp), %rax
	movl	(%rax), %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-80(%rbp), %r9
	jmp	.L5186
.L5215:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23500:
	.size	_ZN2v88internal8compiler19LinearScanAllocator16ReloadLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionE, .-_ZN2v88internal8compiler19LinearScanAllocator16ReloadLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionE
	.section	.text._ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEm,"axG",@progbits,_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEm
	.type	_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEm, @function
_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEm:
.LFB28830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rdi), %r12
	movq	16(%rdi), %rdi
	subq	%rax, %r12
	subq	%rax, %rdi
	sarq	$3, %rdi
	addq	%rdi, %rdi
	cmpq	%rsi, %rdi
	cmovb	%rsi, %rdi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	leaq	0(,%rax,8), %r14
	movq	%r14, %rdi
	call	malloc@PLT
	movq	(%rbx), %r15
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	movq	%r15, %rsi
	call	memcpy@PLT
	leaq	24(%rbx), %rax
	cmpq	%rax, %r15
	je	.L5218
	movq	%r15, %rdi
	call	free@PLT
.L5218:
	leaq	0(%r13,%r12), %rax
	movq	%r13, (%rbx)
	addq	%r14, %r13
	movq	%r13, 16(%rbx)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28830:
	.size	_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEm, .-_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEm
	.section	.text._ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEv,"axG",@progbits,_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEv
	.type	_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEv, @function
_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEv:
.LFB27749:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEm
	.cfi_endproc
.LFE27749:
	.size	_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEv, .-_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEv
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator31ChooseOneOfTwoPredecessorStatesEPNS1_16InstructionBlockENS1_16LifetimePositionE.str1.1,"aMS",@progbits,1
.LC97:
	.string	"Looking at only uses\n"
.LC98:
	.string	"Vote went %zu vs %zu\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator31ChooseOneOfTwoPredecessorStatesEPNS1_16InstructionBlockENS1_16LifetimePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator31ChooseOneOfTwoPredecessorStatesEPNS1_16InstructionBlockENS1_16LifetimePositionE
	.type	_ZN2v88internal8compiler19LinearScanAllocator31ChooseOneOfTwoPredecessorStatesEPNS1_16InstructionBlockENS1_16LifetimePositionE, @function
_ZN2v88internal8compiler19LinearScanAllocator31ChooseOneOfTwoPredecessorStatesEPNS1_16InstructionBlockENS1_16LifetimePositionE:
.LFB23501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-600(%rbp), %rbx
	movq	%rbx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	subq	$632, %rsp
	movq	%rsi, -640(%rbp)
	movq	40(%rsi), %rcx
	movslq	(%rcx), %r12
	movslq	4(%rcx), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rbx, -648(%rbp)
	salq	$5, %r12
	salq	$5, %r13
	movaps	%xmm0, -624(%rbp)
	movq	464(%rax), %rax
	leaq	(%rax,%r12), %rdx
	addq	%rax, %r13
	leaq	-344(%rbp), %rax
	movq	%rax, -608(%rbp)
	movq	16(%rdx), %rbx
	leaq	-624(%rbp), %rax
	movq	8(%rdx), %r12
	movq	%rdx, -664(%rbp)
	movq	%rax, -632(%rbp)
	cmpq	%rbx, %r12
	je	.L5238
	.p2align 4,,10
	.p2align 3
.L5237:
	movq	(%r12), %r9
	movq	32(%r9), %rax
	movq	136(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L5227
	jmp	.L5225
	.p2align 4,,10
	.p2align 3
.L5364:
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5225
.L5227:
	movq	8(%rdi), %rsi
	cmpl	4(%rsi), %r15d
	jge	.L5364
	movq	%rdi, 136(%rax)
	movl	%r15d, %esi
	call	_ZNK2v88internal8compiler9LiveRange6CoversENS1_16LifetimePositionE
	testb	%al, %al
	je	.L5282
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L5228
	movl	24(%rax), %esi
	cmpl	%esi, %r15d
	jge	.L5234
.L5228:
	movq	24(%rdi), %rax
	testq	%rax, %rax
	jne	.L5359
	jmp	.L5233
	.p2align 4,,10
	.p2align 3
.L5365:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L5233
.L5359:
	movl	24(%rax), %esi
.L5234:
	cmpl	%esi, %r15d
	jg	.L5365
	movq	%rax, 56(%rdi)
	jmp	.L5236
	.p2align 4,,10
	.p2align 3
.L5366:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L5282
.L5236:
	testb	$32, 28(%rax)
	je	.L5366
	movq	32(%r9), %rsi
	movq	-616(%rbp), %rax
	cmpq	%rax, -608(%rbp)
	je	.L5367
.L5284:
	movq	%rsi, (%rax)
	addq	$8, %rax
	movq	%rax, -616(%rbp)
.L5282:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L5237
.L5238:
	leaq	-312(%rbp), %rax
	movq	%rax, %xmm0
	movq	%rax, -632(%rbp)
	leaq	-56(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	movq	16(%r13), %rbx
	leaq	-336(%rbp), %rax
	movaps	%xmm0, -336(%rbp)
	movq	8(%r13), %r12
	movq	%rax, -656(%rbp)
	cmpq	%r12, %rbx
	je	.L5368
	.p2align 4,,10
	.p2align 3
.L5251:
	movq	(%r12), %r9
	movq	32(%r9), %rax
	movq	136(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L5241
	jmp	.L5239
	.p2align 4,,10
	.p2align 3
.L5369:
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5239
.L5241:
	movq	8(%rdi), %rsi
	cmpl	4(%rsi), %r15d
	jge	.L5369
	movq	%rdi, 136(%rax)
	movl	%r15d, %esi
	call	_ZNK2v88internal8compiler9LiveRange6CoversENS1_16LifetimePositionE
	testb	%al, %al
	je	.L5288
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L5242
	movl	24(%rax), %esi
	cmpl	%esi, %r15d
	jge	.L5248
.L5242:
	movq	24(%rdi), %rax
	testq	%rax, %rax
	jne	.L5360
	jmp	.L5247
	.p2align 4,,10
	.p2align 3
.L5370:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L5247
.L5360:
	movl	24(%rax), %esi
.L5248:
	cmpl	%esi, %r15d
	jg	.L5370
	movq	%rax, 56(%rdi)
	jmp	.L5250
	.p2align 4,,10
	.p2align 3
.L5371:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L5288
.L5250:
	testb	$32, 28(%rax)
	je	.L5371
	movq	32(%r9), %rsi
	movq	-328(%rbp), %rax
	cmpq	-320(%rbp), %rax
	je	.L5372
.L5287:
	movq	%rsi, (%rax)
	addq	$8, %rax
	movq	%rax, -328(%rbp)
.L5288:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L5251
	movq	-328(%rbp), %rax
	movq	-336(%rbp), %r9
	movq	-624(%rbp), %rdi
	movq	-616(%rbp), %rsi
	movq	%rax, %r8
	subq	%r9, %r8
	sarq	$3, %r8
	cmpq	%rsi, %rdi
	je	.L5373
.L5254:
	movq	(%r14), %rax
	subq	%rdi, %rsi
	sarq	$3, %rsi
	testb	$4, 488(%rax)
	jne	.L5374
.L5278:
	movq	-640(%rbp), %rax
	cmpq	%rsi, %r8
	movq	40(%rax), %rax
	leaq	4(%rax), %rdx
	cmovnb	%rdx, %rax
	movl	(%rax), %r12d
	cmpq	-632(%rbp), %r9
	je	.L5280
	movq	%r9, %rdi
	call	free@PLT
	movq	-624(%rbp), %rdi
.L5280:
	cmpq	-648(%rbp), %rdi
	je	.L5281
	call	free@PLT
.L5281:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5375
	addq	$632, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5239:
	.cfi_restore_state
	movq	$0, 136(%rax)
	jmp	.L5288
	.p2align 4,,10
	.p2align 3
.L5225:
	movq	$0, 136(%rax)
	jmp	.L5282
	.p2align 4,,10
	.p2align 3
.L5374:
	movq	%r8, %rdx
	leaq	.LC98(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-624(%rbp), %rdi
	movq	-336(%rbp), %r9
	movq	-328(%rbp), %r8
	movq	-616(%rbp), %rsi
	subq	%r9, %r8
	subq	%rdi, %rsi
	sarq	$3, %r8
	sarq	$3, %rsi
	jmp	.L5278
	.p2align 4,,10
	.p2align 3
.L5372:
	movq	-656(%rbp), %rdi
	movq	%rsi, -672(%rbp)
	call	_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEv
	movq	-672(%rbp), %rsi
	jmp	.L5287
	.p2align 4,,10
	.p2align 3
.L5367:
	movq	-632(%rbp), %rdi
	movq	%rsi, -656(%rbp)
	call	_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEv
	movq	-656(%rbp), %rsi
	jmp	.L5284
	.p2align 4,,10
	.p2align 3
.L5247:
	movq	$0, 56(%rdi)
	jmp	.L5288
	.p2align 4,,10
	.p2align 3
.L5233:
	movq	$0, 56(%rdi)
	jmp	.L5282
	.p2align 4,,10
	.p2align 3
.L5373:
	cmpq	%rax, %r9
	je	.L5253
	subq	%r9, %rax
	movq	%rax, %r8
	sarq	$3, %r8
	jmp	.L5254
	.p2align 4,,10
	.p2align 3
.L5368:
	movq	-624(%rbp), %rdi
	movq	-616(%rbp), %rsi
	cmpq	%rdi, %rsi
	je	.L5253
	movq	-632(%rbp), %r9
	xorl	%r8d, %r8d
	jmp	.L5254
.L5253:
	movq	(%r14), %rax
	testb	$4, 488(%rax)
	jne	.L5376
.L5255:
	movq	-664(%rbp), %rax
	movq	16(%rax), %rbx
	movq	8(%rax), %r12
	leaq	-624(%rbp), %rax
	movq	%rax, -656(%rbp)
	cmpq	%r12, %rbx
	je	.L5268
	.p2align 4,,10
	.p2align 3
.L5267:
	movq	(%r12), %r9
	movq	32(%r9), %rsi
	movq	136(%rsi), %rdi
	testq	%rdi, %rdi
	jne	.L5261
	jmp	.L5259
	.p2align 4,,10
	.p2align 3
.L5377:
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5259
.L5261:
	movq	8(%rdi), %rax
	cmpl	4(%rax), %r15d
	jge	.L5377
	movq	%rdi, 136(%rsi)
	movl	%r15d, %esi
	call	_ZNK2v88internal8compiler9LiveRange6CoversENS1_16LifetimePositionE
	testb	%al, %al
	je	.L5292
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L5262
	movl	24(%rax), %esi
	cmpl	%r15d, %esi
	jle	.L5266
.L5262:
	movq	24(%rdi), %rax
	testq	%rax, %rax
	jne	.L5361
	jmp	.L5264
	.p2align 4,,10
	.p2align 3
.L5378:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L5264
.L5361:
	movl	24(%rax), %esi
.L5266:
	cmpl	%esi, %r15d
	jg	.L5378
	movq	%rax, 56(%rdi)
	movq	-616(%rbp), %rax
	movq	32(%r9), %rsi
	cmpq	-608(%rbp), %rax
	je	.L5379
.L5291:
	movq	%rsi, (%rax)
	addq	$8, %rax
	movq	%rax, -616(%rbp)
.L5292:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L5267
.L5268:
	movq	8(%r13), %rax
	movq	16(%r13), %rbx
	leaq	-336(%rbp), %r12
	movq	%rax, %r13
	cmpq	%rax, %rbx
	je	.L5363
	.p2align 4,,10
	.p2align 3
.L5277:
	movq	0(%r13), %r9
	movq	32(%r9), %rcx
	movq	136(%rcx), %rdi
	testq	%rdi, %rdi
	jne	.L5271
	jmp	.L5269
	.p2align 4,,10
	.p2align 3
.L5380:
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5269
.L5271:
	movq	8(%rdi), %rax
	cmpl	4(%rax), %r15d
	jge	.L5380
	movq	%rdi, 136(%rcx)
	movl	%r15d, %esi
	call	_ZNK2v88internal8compiler9LiveRange6CoversENS1_16LifetimePositionE
	testb	%al, %al
	je	.L5294
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L5272
	movl	24(%rax), %ecx
	cmpl	%ecx, %r15d
	jge	.L5276
.L5272:
	movq	24(%rdi), %rax
	testq	%rax, %rax
	jne	.L5362
	jmp	.L5274
	.p2align 4,,10
	.p2align 3
.L5381:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L5274
.L5362:
	movl	24(%rax), %ecx
.L5276:
	cmpl	%ecx, %r15d
	jg	.L5381
	movq	%rax, 56(%rdi)
	movq	-328(%rbp), %rax
	movq	32(%r9), %rcx
	cmpq	-320(%rbp), %rax
	je	.L5382
.L5296:
	movq	%rcx, (%rax)
	addq	$8, %rax
	movq	%rax, -328(%rbp)
.L5294:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L5277
.L5363:
	movq	-336(%rbp), %r9
	movq	-328(%rbp), %r8
	movq	-624(%rbp), %rdi
	movq	-616(%rbp), %rsi
	subq	%r9, %r8
	sarq	$3, %r8
	jmp	.L5254
	.p2align 4,,10
	.p2align 3
.L5259:
	movq	$0, 136(%rsi)
	jmp	.L5292
	.p2align 4,,10
	.p2align 3
.L5269:
	movq	$0, 136(%rcx)
	jmp	.L5294
	.p2align 4,,10
	.p2align 3
.L5274:
	movq	$0, 56(%rdi)
	jmp	.L5294
	.p2align 4,,10
	.p2align 3
.L5264:
	movq	$0, 56(%rdi)
	jmp	.L5292
.L5376:
	leaq	.LC97(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5255
.L5382:
	movq	%r12, %rdi
	movq	%rcx, -656(%rbp)
	call	_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEv
	movq	-656(%rbp), %rcx
	jmp	.L5296
.L5379:
	movq	-656(%rbp), %rdi
	movq	%rsi, -664(%rbp)
	call	_ZN2v84base11SmallVectorIPNS_8internal8compiler17TopLevelLiveRangeELm32EE4GrowEv
	movq	-664(%rbp), %rsi
	jmp	.L5291
.L5375:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23501:
	.size	_ZN2v88internal8compiler19LinearScanAllocator31ChooseOneOfTwoPredecessorStatesEPNS1_16InstructionBlockENS1_16LifetimePositionE, .-_ZN2v88internal8compiler19LinearScanAllocator31ChooseOneOfTwoPredecessorStatesEPNS1_16InstructionBlockENS1_16LifetimePositionE
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERKS4_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERKS4_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERKS4_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERKS4_:
.LFB29414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	%rax, %rsi
	je	.L5394
	movq	32(%rsi), %rax
	movq	(%rdx), %rdx
	movq	%rsi, %rbx
	movq	16(%rdx), %rdx
	movq	16(%rax), %rax
	movl	(%rdx), %r14d
	cmpl	%r14d, (%rax)
	jle	.L5387
	movq	32(%rdi), %r8
	movq	%r8, %rdx
	cmpq	%rsi, %r8
	je	.L5386
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	32(%rax), %rdx
	movq	16(%rdx), %rdx
	cmpl	(%rdx), %r14d
	jle	.L5385
	cmpq	$0, 24(%rax)
	movl	$0, %r8d
	cmovne	%rbx, %r8
	cmove	%rax, %rbx
	movq	%rbx, %rdx
.L5386:
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5387:
	.cfi_restore_state
	jge	.L5390
	movq	40(%rdi), %rax
	xorl	%r8d, %r8d
	movq	%rax, %rdx
	cmpq	%rsi, %rax
	je	.L5386
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	32(%rax), %rdx
	movq	16(%rdx), %rdx
	cmpl	(%rdx), %r14d
	jge	.L5385
	cmpq	$0, 24(%rbx)
	movl	$0, %r8d
	cmovne	%rax, %rbx
	cmovne	%rax, %r8
	movq	%rbx, %rdx
	jmp	.L5386
	.p2align 4,,10
	.p2align 3
.L5394:
	cmpq	$0, 48(%rdi)
	je	.L5385
	movq	40(%rdi), %rbx
	movq	32(%rbx), %rax
	movq	16(%rax), %rdx
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	movl	(%rax), %eax
	cmpl	%eax, (%rdx)
	jl	.L5395
.L5385:
	popq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE24_M_get_insert_unique_posERKS4_
	.p2align 4,,10
	.p2align 3
.L5390:
	.cfi_restore_state
	movq	%rsi, %r8
	popq	%rbx
	xorl	%edx, %edx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5395:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE29414:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERKS4_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERKS4_
	.section	.rodata._ZN2v88internal8compiler15LiveRangeBundle8TryMergeEPS2_b.str1.1,"aMS",@progbits,1
.LC99:
	.string	"No merge %d:%d %d:%d\n"
	.section	.text._ZN2v88internal8compiler15LiveRangeBundle8TryMergeEPS2_b,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15LiveRangeBundle8TryMergeEPS2_b
	.type	_ZN2v88internal8compiler15LiveRangeBundle8TryMergeEPS2_b, @function
_ZN2v88internal8compiler15LiveRangeBundle8TryMergeEPS2_b:
.LFB23473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -64(%rbp)
	cmpq	%rdi, %rsi
	je	.L5430
	movq	88(%rdi), %r12
	leaq	72(%rdi), %r13
	movq	88(%rsi), %r15
	movq	%rdi, %rbx
	movq	%rsi, %r14
	cmpq	%r13, %r12
	je	.L5398
	leaq	72(%rsi), %rax
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r15
	jne	.L5399
	jmp	.L5398
	.p2align 4,,10
	.p2align 3
.L5400:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%r13, %r12
	je	.L5398
.L5459:
	cmpq	-56(%rbp), %r15
	je	.L5398
.L5399:
	movl	32(%r12), %esi
	movl	36(%r15), %r8d
	cmpl	%r8d, %esi
	jg	.L5400
	movl	32(%r15), %ecx
	movl	36(%r12), %edx
	cmpl	%edx, %ecx
	jle	.L5427
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%r13, %r12
	jne	.L5459
.L5398:
	movq	32(%r14), %r12
	leaq	16(%r14), %rax
	movq	%rax, -80(%rbp)
	cmpq	%rax, %r12
	je	.L5418
	.p2align 4,,10
	.p2align 3
.L5402:
	movq	32(%r12), %rax
	movq	%rbx, 80(%rax)
	movq	32(%r12), %rax
	movq	16(%rax), %r15
	testq	%r15, %r15
	je	.L5417
	.p2align 4,,10
	.p2align 3
.L5405:
	movq	(%r15), %rax
	movq	80(%rbx), %rdx
	movl	(%r15), %ecx
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jne	.L5408
	jmp	.L5460
	.p2align 4,,10
	.p2align 3
.L5461:
	movq	16(%rdx), %rax
	movl	$1, %edi
	testq	%rax, %rax
	je	.L5409
.L5462:
	movq	%rax, %rdx
.L5408:
	movl	32(%rdx), %esi
	cmpl	%esi, %ecx
	jl	.L5461
	movq	24(%rdx), %rax
	xorl	%edi, %edi
	testq	%rax, %rax
	jne	.L5462
.L5409:
	testb	%dil, %dil
	jne	.L5407
	cmpl	%esi, %ecx
	jle	.L5413
.L5428:
	movl	$1, %r9d
	cmpq	%r13, %rdx
	jne	.L5463
.L5414:
	movq	56(%rbx), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$39, %rax
	jbe	.L5464
	leaq	40(%rsi), %rax
	movq	%rax, 16(%rdi)
.L5416:
	movq	-56(%rbp), %rax
	movq	%r13, %rcx
	movl	%r9d, %edi
	movq	%rax, 32(%rsi)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 104(%rbx)
.L5413:
	movq	8(%r15), %r15
	testq	%r15, %r15
	jne	.L5405
.L5417:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	-80(%rbp), %rax
	jne	.L5402
	movq	32(%r14), %r12
	cmpq	-80(%rbp), %r12
	je	.L5418
	leaq	16(%rbx), %r13
	jmp	.L5423
	.p2align 4,,10
	.p2align 3
.L5433:
	movl	$1, %r15d
.L5420:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$39, %rax
	jbe	.L5465
	leaq	40(%rsi), %rax
	movq	%rax, 16(%rdi)
.L5422:
	movq	32(%r12), %rax
	movq	%r13, %rcx
	movl	%r15d, %edi
	movq	%rax, 32(%rsi)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
.L5419:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	-80(%rbp), %rax
	je	.L5418
.L5423:
	leaq	32(%r12), %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS4_ERKS4_
	testq	%rdx, %rdx
	je	.L5419
	testq	%rax, %rax
	jne	.L5433
	cmpq	%rdx, %r13
	je	.L5433
	movq	32(%rdx), %rax
	xorl	%r15d, %r15d
	movq	16(%rax), %rcx
	movq	32(%r12), %rax
	movq	16(%rax), %rax
	movl	(%rax), %eax
	cmpl	%eax, (%rcx)
	setg	%r15b
	jmp	.L5420
	.p2align 4,,10
	.p2align 3
.L5460:
	movq	%r13, %rdx
.L5407:
	cmpq	%rdx, 88(%rbx)
	je	.L5428
	movq	%rdx, %rdi
	movl	%ecx, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	-72(%rbp), %ecx
	movq	-64(%rbp), %rdx
	cmpl	%ecx, 32(%rax)
	jge	.L5413
	movl	$1, %r9d
	cmpq	%r13, %rdx
	je	.L5414
.L5463:
	xorl	%r9d, %r9d
	cmpl	32(%rdx), %ecx
	setl	%r9b
	jmp	.L5414
	.p2align 4,,10
	.p2align 3
.L5464:
	movl	$40, %esi
	movq	%rdx, -72(%rbp)
	movl	%r9d, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-64(%rbp), %r9d
	movq	-72(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L5416
	.p2align 4,,10
	.p2align 3
.L5418:
	movq	24(%r14), %r12
	testq	%r12, %r12
	je	.L5403
.L5404:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L5424
.L5425:
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L5425
.L5424:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L5404
.L5403:
	movq	-80(%rbp), %rax
	movq	$0, 24(%r14)
	movl	$1, %r12d
	movq	$0, 48(%r14)
	movq	%rax, 32(%r14)
	movq	%rax, 40(%r14)
.L5396:
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5465:
	.cfi_restore_state
	movl	$40, %esi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L5422
	.p2align 4,,10
	.p2align 3
.L5430:
	movl	$1, %r12d
	jmp	.L5396
	.p2align 4,,10
	.p2align 3
.L5427:
	xorl	%r12d, %r12d
	cmpb	$0, -64(%rbp)
	je	.L5396
	leaq	.LC99(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5396
	.cfi_endproc
.LFE23473:
	.size	_ZN2v88internal8compiler15LiveRangeBundle8TryMergeEPS2_b, .-_ZN2v88internal8compiler15LiveRangeBundle8TryMergeEPS2_b
	.section	.rodata._ZN2v88internal8compiler13BundleBuilder12BuildBundlesEv.str1.1,"aMS",@progbits,1
.LC100:
	.string	"Build bundles\n"
.LC101:
	.string	"Block B%d\n"
	.section	.rodata._ZN2v88internal8compiler13BundleBuilder12BuildBundlesEv.str1.8,"aMS",@progbits,1
	.align 8
.LC102:
	.string	"Processing phi for v%d with %d:%d\n"
	.align 8
.LC103:
	.string	"Input value v%d with range %d:%d\n"
	.section	.rodata._ZN2v88internal8compiler13BundleBuilder12BuildBundlesEv.str1.1
.LC104:
	.string	"Merge\n"
.LC105:
	.string	"Merged %d and %d to %d\n"
.LC106:
	.string	"Add\n"
.LC107:
	.string	"Added %d and %d to %d\n"
.LC108:
	.string	"Done block B%d\n"
	.section	.text._ZN2v88internal8compiler13BundleBuilder12BuildBundlesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13BundleBuilder12BuildBundlesEv
	.type	_ZN2v88internal8compiler13BundleBuilder12BuildBundlesEv, @function
_ZN2v88internal8compiler13BundleBuilder12BuildBundlesEv:
.LFB23471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$4, 488(%r15)
	jne	.L5523
.L5467:
	movq	16(%r15), %rax
	movq	16(%rax), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	%rdx, %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	movl	%ecx, %edi
	subl	$1, %edi
	movl	%edi, -124(%rbp)
	js	.L5466
	movslq	%edi, %rdi
	movq	%r15, %r10
	movq	%rdi, -136(%rbp)
	leaq	-64(%rbp), %rdi
	movq	%rdi, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L5495:
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	-136(%rbp), %rdx
	jbe	.L5524
	movq	-136(%rbp), %rdi
	movq	(%rax,%rdi,8), %rbx
	testb	$4, 488(%r10)
	jne	.L5525
	movq	80(%rbx), %rdi
	movq	72(%rbx), %rax
	movq	%rdi, -112(%rbp)
	cmpq	%rax, %rdi
	je	.L5494
.L5496:
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L5491:
	movq	-80(%rbp), %rax
	movq	%r10, %rdi
	movq	(%rax), %rax
	movl	(%rax), %esi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler22RegisterAllocationData23GetOrCreateLiveRangeForEi
	movq	80(%rax), %r12
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.L5526
.L5472:
	movq	(%r14), %r10
	testb	$4, 488(%r10)
	jne	.L5527
.L5475:
	movq	-96(%rbp), %rax
	movq	24(%rax), %rbx
	movq	32(%rax), %r13
	cmpq	%r13, %rbx
	je	.L5476
	movq	%r13, -72(%rbp)
	movq	%r14, %r13
	jmp	.L5490
	.p2align 4,,10
	.p2align 3
.L5477:
	movslq	%r14d, %rdx
	movq	(%rcx,%rdx,8), %r15
	testq	%r15, %r15
	je	.L5528
.L5480:
	movq	0(%r13), %rax
	movl	488(%rax), %eax
	andl	$4, %eax
	jne	.L5529
.L5483:
	movq	80(%r15), %rsi
	testq	%rsi, %rsi
	je	.L5488
.L5485:
	xorl	%edx, %edx
	testl	%eax, %eax
	movq	%r12, %rdi
	setne	%dl
	call	_ZN2v88internal8compiler15LiveRangeBundle8TryMergeEPS2_b
	movq	0(%r13), %r10
	testb	%al, %al
	je	.L5487
	testb	$4, 488(%r10)
	jne	.L5530
.L5487:
	addq	$4, %rbx
	cmpq	%rbx, -72(%rbp)
	je	.L5531
.L5490:
	movq	176(%r10), %rsi
	movq	168(%r10), %rcx
	movl	(%rbx), %r14d
	movq	%rsi, %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpl	%eax, %r14d
	jl	.L5477
	leal	1(%r14), %edx
	movq	$0, -64(%rbp)
	movslq	%edx, %rdx
	cmpq	%rax, %rdx
	ja	.L5532
	jnb	.L5477
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L5477
	movslq	%r14d, %rdx
	movq	%rax, 176(%r10)
	movq	(%rcx,%rdx,8), %r15
	testq	%r15, %r15
	jne	.L5480
	.p2align 4,,10
	.p2align 3
.L5528:
	movq	16(%r10), %rdi
	movl	%r14d, %esi
	movq	%rdx, -104(%rbp)
	movq	%r10, -88(%rbp)
	call	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	movq	-88(%rbp), %r10
	movq	-104(%rbp), %rdx
	movzbl	%al, %ecx
	movq	(%r10), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$159, %rax
	jbe	.L5533
	leaq	160(%r15), %rax
	movq	%rax, 16(%rdi)
.L5482:
	sall	$13, %ecx
	pxor	%xmm0, %xmm0
	movq	%r15, 32(%r15)
	orl	$134221824, %ecx
	movl	$0, (%r15)
	movq	$0, 24(%r15)
	movl	%ecx, 4(%r15)
	movl	%r14d, 88(%r15)
	movl	$0, 92(%r15)
	movq	$0, 112(%r15)
	movb	$0, 120(%r15)
	movl	$2147483647, 124(%r15)
	movq	$0, 128(%r15)
	movq	%r15, 136(%r15)
	movq	$0, 144(%r15)
	movb	$0, 152(%r15)
	movups	%xmm0, 8(%r15)
	movups	%xmm0, 40(%r15)
	movups	%xmm0, 56(%r15)
	movups	%xmm0, 72(%r15)
	movups	%xmm0, 96(%r15)
	movq	168(%r10), %rax
	movq	%r15, (%rax,%rdx,8)
	movq	0(%r13), %rax
	movl	488(%rax), %eax
	andl	$4, %eax
	je	.L5483
	.p2align 4,,10
	.p2align 3
.L5529:
	movq	32(%r15), %rax
	movl	(%r15), %ecx
	movl	%r14d, %esi
	leaq	.LC103(%rip), %rdi
	movl	88(%rax), %edx
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	80(%r15), %rsi
	movq	0(%r13), %rax
	testq	%rsi, %rsi
	je	.L5484
	movl	488(%rax), %eax
	andl	$4, %eax
	je	.L5485
	xorl	%eax, %eax
	leaq	.LC104(%rip), %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	0(%r13), %rax
	movq	-88(%rbp), %rsi
	movl	488(%rax), %eax
	andl	$4, %eax
	jmp	.L5485
	.p2align 4,,10
	.p2align 3
.L5484:
	testb	$4, 488(%rax)
	je	.L5488
	leaq	.LC106(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L5488:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler15LiveRangeBundle11TryAddRangeEPNS1_9LiveRangeE
	movq	0(%r13), %r10
	testb	%al, %al
	je	.L5487
	testb	$4, 488(%r10)
	je	.L5487
	movq	-96(%rbp), %rax
	movl	112(%r12), %ecx
	movl	%r14d, %edx
	addq	$4, %rbx
	leaq	.LC107(%rip), %rdi
	movl	(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	0(%r13), %r10
	cmpq	%rbx, -72(%rbp)
	jne	.L5490
	.p2align 4,,10
	.p2align 3
.L5531:
	movq	%r13, %r14
.L5476:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	jne	.L5491
.L5492:
	testb	$4, 488(%r10)
	je	.L5494
	movl	-124(%rbp), %esi
	leaq	.LC108(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L5494:
	subl	$1, -124(%rbp)
	movl	-124(%rbp), %eax
	subq	$1, -136(%rbp)
	cmpl	$-1, %eax
	je	.L5466
	movq	(%r14), %r10
	movq	16(%r10), %rax
	movq	16(%rax), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	jmp	.L5495
	.p2align 4,,10
	.p2align 3
.L5530:
	movq	-96(%rbp), %rax
	movl	112(%r12), %ecx
	movl	%r14d, %edx
	leaq	.LC105(%rip), %rdi
	movl	(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	0(%r13), %r10
	jmp	.L5487
	.p2align 4,,10
	.p2align 3
.L5532:
	movq	-120(%rbp), %rcx
	leaq	160(%r10), %rdi
	subq	%rax, %rdx
	movq	%r10, -88(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler17TopLevelLiveRangeENS1_13ZoneAllocatorIS4_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS4_S7_EEmRKS4_
	movq	-88(%rbp), %r10
	movq	168(%r10), %rcx
	jmp	.L5477
	.p2align 4,,10
	.p2align 3
.L5527:
	movq	32(%rbx), %rax
	movl	(%rbx), %ecx
	leaq	.LC102(%rip), %rdi
	movl	88(%rax), %edx
	movq	-96(%rbp), %rax
	movl	(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r14), %r10
	jmp	.L5475
	.p2align 4,,10
	.p2align 3
.L5526:
	movl	8(%r14), %r13d
	movq	(%r14), %rax
	leal	1(%r13), %ecx
	movq	(%rax), %rdx
	movl	%ecx, 8(%r14)
	movq	(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$119, %rax
	jbe	.L5534
	leaq	120(%r12), %rax
	movq	%rax, 16(%rdi)
.L5474:
	leaq	16(%r12), %rax
	movq	%rdx, (%r12)
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, 32(%r12)
	movq	%rax, 40(%r12)
	leaq	72(%r12), %rax
	movl	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 48(%r12)
	movq	%rdx, 56(%r12)
	movl	$0, 72(%r12)
	movq	$0, 80(%r12)
	movq	%rax, 88(%r12)
	movq	%rax, 96(%r12)
	movq	$0, 104(%r12)
	movl	%r13d, 112(%r12)
	movl	$32, 116(%r12)
	call	_ZN2v88internal8compiler15LiveRangeBundle11TryAddRangeEPNS1_9LiveRangeE
	jmp	.L5472
	.p2align 4,,10
	.p2align 3
.L5533:
	movl	$160, %esi
	movq	%r10, -144(%rbp)
	movb	%cl, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movzbl	-88(%rbp), %ecx
	movq	-104(%rbp), %rdx
	movq	-144(%rbp), %r10
	movq	%rax, %r15
	jmp	.L5482
.L5525:
	movl	-124(%rbp), %esi
	xorl	%eax, %eax
	leaq	.LC101(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	72(%rbx), %rax
	movq	80(%rbx), %rbx
	movq	(%r14), %r10
	movq	%rbx, -112(%rbp)
	cmpq	%rbx, %rax
	jne	.L5496
	jmp	.L5492
.L5534:
	movl	$120, %esi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L5474
.L5466:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5535
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5523:
	.cfi_restore_state
	leaq	.LC100(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r14), %r15
	jmp	.L5467
.L5535:
	call	__stack_chk_fail@PLT
.L5524:
	movq	-136(%rbp), %rsi
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE23471:
	.size	_ZN2v88internal8compiler13BundleBuilder12BuildBundlesEv, .-_ZN2v88internal8compiler13BundleBuilder12BuildBundlesEv
	.section	.text._ZNSt10_HashtableIN2v88internal8compiler19LinearScanAllocator17RangeWithRegisterES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS4_6EqualsENS4_4HashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal8compiler19LinearScanAllocator17RangeWithRegisterES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS4_6EqualsENS4_4HashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal8compiler19LinearScanAllocator17RangeWithRegisterES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS4_6EqualsENS4_4HashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm
	.type	_ZNSt10_HashtableIN2v88internal8compiler19LinearScanAllocator17RangeWithRegisterES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS4_6EqualsENS4_4HashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm, @function
_ZNSt10_HashtableIN2v88internal8compiler19LinearScanAllocator17RangeWithRegisterES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS4_6EqualsENS4_4HashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm:
.LFB29496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L5560
.L5537:
	movq	%r14, 24(%r12)
	movq	8(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L5546
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L5547:
	addq	$1, 32(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5560:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L5561
	movq	(%rbx), %r8
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L5562
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L5541:
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %rdi
.L5539:
	movq	24(%rbx), %rsi
	movq	$0, 24(%rbx)
	testq	%rsi, %rsi
	je	.L5542
	xorl	%r9d, %r9d
	leaq	24(%rbx), %r10
	jmp	.L5543
	.p2align 4,,10
	.p2align 3
.L5544:
	movq	(%r8), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L5545:
	testq	%rsi, %rsi
	je	.L5542
.L5543:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	24(%rcx), %rax
	divq	%r13
	leaq	(%rdi,%rdx,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L5544
	movq	24(%rbx), %r8
	movq	%r8, (%rcx)
	movq	%rcx, 24(%rbx)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L5549
	movq	%rcx, (%rdi,%r9,8)
	movq	%rdx, %r9
	testq	%rsi, %rsi
	jne	.L5543
	.p2align 4,,10
	.p2align 3
.L5542:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r13, 16(%rbx)
	divq	%r13
	movq	%rdi, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L5537
	.p2align 4,,10
	.p2align 3
.L5546:
	movq	24(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 24(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L5548
	movq	24(%rax), %rax
	xorl	%edx, %edx
	divq	16(%rbx)
	movq	8(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L5548:
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L5547
	.p2align 4,,10
	.p2align 3
.L5549:
	movq	%rdx, %r9
	jmp	.L5545
	.p2align 4,,10
	.p2align 3
.L5561:
	movq	$0, 56(%rbx)
	leaq	56(%rbx), %rdi
	jmp	.L5539
	.p2align 4,,10
	.p2align 3
.L5562:
	movq	%r8, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L5541
	.cfi_endproc
.LFE29496:
	.size	_ZNSt10_HashtableIN2v88internal8compiler19LinearScanAllocator17RangeWithRegisterES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS4_6EqualsENS4_4HashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm, .-_ZNSt10_HashtableIN2v88internal8compiler19LinearScanAllocator17RangeWithRegisterES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS4_6EqualsENS4_4HashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS1_16InstructionBlockEPNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS6_4HashENS6_6EqualsEEE.str1.1,"aMS",@progbits,1
.LC109:
	.string	"result.second"
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS1_16InstructionBlockEPNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS6_4HashENS6_6EqualsEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC110:
	.string	"Reset %d as live due vote %zu in %s\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS1_16InstructionBlockEPNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS6_4HashENS6_6EqualsEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS1_16InstructionBlockEPNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS6_4HashENS6_6EqualsEEE
	.type	_ZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS1_16InstructionBlockEPNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS6_4HashENS6_6EqualsEEE, @function
_ZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS1_16InstructionBlockEPNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS6_4HashENS6_6EqualsEEE:
.LFB23505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-288(%rbp), %rcx
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -344(%rbp)
	movq	40(%rsi), %r9
	movq	%rdi, -368(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	(%rax), %rdx
	movq	48(%rsi), %rax
	movl	$0, -288(%rbp)
	movq	$0, -280(%rbp)
	movq	%rdx, -304(%rbp)
	movq	%rcx, -272(%rbp)
	movq	%rcx, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -336(%rbp)
	cmpq	%r9, %rax
	je	.L5643
	movl	$0, -348(%rbp)
	movq	%r9, %r14
	pxor	%xmm0, %xmm0
	movq	%rcx, %r13
	movq	%rsi, -328(%rbp)
	.p2align 4,,10
	.p2align 3
.L5587:
	movq	-328(%rbp), %rdi
	movslq	(%r14), %rax
	cmpl	100(%rdi), %eax
	jge	.L5569
	movq	-368(%rbp), %rdi
	movq	(%rdi), %rsi
	movq	-328(%rbp), %rdi
	cmpb	$0, 120(%rdi)
	je	.L5731
.L5567:
	salq	$5, %rax
	addq	464(%rsi), %rax
	movq	8(%rax), %rbx
	movq	16(%rax), %r15
	cmpq	%rbx, %r15
	je	.L5566
	movq	%r14, -360(%rbp)
	.p2align 4,,10
	.p2align 3
.L5586:
	movq	(%rbx), %r12
	movl	4(%r12), %eax
	shrl	$7, %eax
	andl	$63, %eax
	cmpl	$32, %eax
	je	.L5570
	movq	-280(%rbp), %rax
	movq	32(%r12), %r8
	testq	%rax, %rax
	je	.L5571
	movl	88(%r8), %esi
	movq	%r13, %rdx
	jmp	.L5572
	.p2align 4,,10
	.p2align 3
.L5732:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L5573
.L5572:
	movq	32(%rax), %rcx
	cmpl	%esi, 88(%rcx)
	jge	.L5732
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L5572
.L5573:
	cmpq	%r13, %rdx
	je	.L5571
	movq	32(%rdx), %rax
	cmpl	88(%rax), %esi
	jge	.L5733
.L5571:
	movq	-304(%rbp), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$175, %rax
	jbe	.L5734
	leaq	176(%r14), %rax
	movq	%rax, 16(%rdi)
.L5577:
	movq	-280(%rbp), %rdx
	movq	%r8, 32(%r14)
	movq	$1, 40(%r14)
	movups	%xmm0, 48(%r14)
	movups	%xmm0, 64(%r14)
	movups	%xmm0, 80(%r14)
	movups	%xmm0, 96(%r14)
	movups	%xmm0, 112(%r14)
	movups	%xmm0, 128(%r14)
	movups	%xmm0, 144(%r14)
	movups	%xmm0, 160(%r14)
	testq	%rdx, %rdx
	je	.L5578
	movl	88(%r8), %r8d
	jmp	.L5579
	.p2align 4,,10
	.p2align 3
.L5735:
	movq	16(%rdx), %rax
	movl	$1, %edi
	testq	%rax, %rax
	je	.L5580
.L5736:
	movq	%rax, %rdx
.L5579:
	movq	32(%rdx), %rax
	movl	88(%rax), %ecx
	cmpl	%ecx, %r8d
	jl	.L5735
	movq	24(%rdx), %rax
	xorl	%edi, %edi
	testq	%rax, %rax
	jne	.L5736
.L5580:
	testb	%dil, %dil
	jne	.L5737
	cmpl	%ecx, %r8d
	jle	.L5640
.L5639:
	movl	$1, %edi
	cmpq	%r13, %rdx
	jne	.L5738
.L5585:
	movq	%r13, %rcx
	movq	%r14, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -256(%rbp)
	movl	4(%r12), %eax
	pxor	%xmm0, %xmm0
	shrl	$7, %eax
	andl	$63, %eax
	addl	$1, 48(%r14,%rax,4)
.L5570:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.L5586
.L5743:
	movq	-360(%rbp), %r14
.L5566:
	addq	$4, %r14
	cmpq	%r14, -336(%rbp)
	jne	.L5587
	movq	-328(%rbp), %r15
	movslq	-348(%rbp), %r14
	movq	-304(%rbp), %rdx
	movq	-280(%rbp), %rsi
	movq	40(%r15), %r9
	movq	48(%r15), %rax
.L5564:
	pxor	%xmm0, %xmm0
	subq	%r9, %rax
	movq	%rdx, %xmm1
	movl	$0, -216(%rbp)
	sarq	$2, %rax
	movaps	%xmm0, -160(%rbp)
	leaq	-216(%rbp), %r15
	movaps	%xmm0, -144(%rbp)
	subq	%r14, %rax
	movq	-368(%rbp), %xmm0
	leaq	2(%rax), %rbx
	movq	%r15, -200(%rbp)
	punpcklqdq	%xmm1, %xmm0
	shrq	%rbx
	movq	$0, -208(%rbp)
	movq	%r15, -192(%rbp)
	movq	$0, -184(%rbp)
	movaps	%xmm0, -240(%rbp)
	testq	%rsi, %rsi
	je	.L5588
	leaq	-232(%rbp), %rdi
	leaq	-312(%rbp), %rcx
	movq	%r15, %rdx
	movq	%rdi, -312(%rbp)
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler17TopLevelLiveRangeESt4pairIKS4_ZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS7_17RangeWithRegisterENSB_4HashENSB_6EqualsEEEE4VoteESt10_Select1stISH_EZNS7_32ComputeStateFromManyPredecessorsES9_SF_E27TopLevelLiveRangeComparatorNS1_13ZoneAllocatorISH_EEE7_M_copyINSN_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISH_EPKSR_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L5589:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L5589
	movq	%r12, -200(%rbp)
	movq	%rcx, %rax
	.p2align 4,,10
	.p2align 3
.L5590:
	movq	%rax, %rdx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L5590
	movq	-256(%rbp), %rax
	movq	%rdx, -192(%rbp)
	leaq	_ZNSt17_Function_handlerIFbPN2v88internal8compiler17TopLevelLiveRangeEEZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS6_17RangeWithRegisterENSA_4HashENSA_6EqualsEEEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rdx
	movq	%rdx, %xmm2
	movq	%rcx, -208(%rbp)
	movq	%rax, -184(%rbp)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS3_16InstructionBlockEPNS2_16ZoneUnorderedSetINS4_17RangeWithRegisterENS8_4HashENS8_6EqualsEEEEUlPNS3_17TopLevelLiveRangeEE0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation(%rip), %rax
	movq	%rax, %xmm0
	movq	%rbx, -176(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -112(%rbp)
	cmpq	%r15, %r12
	je	.L5645
	leaq	-320(%rbp), %r14
	jmp	.L5608
	.p2align 4,,10
	.p2align 3
.L5594:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	movq	-112(%rbp), %rax
	cmpq	%r15, %r12
	je	.L5739
.L5608:
	movq	32(%r12), %rdx
	movq	%rdx, -320(%rbp)
	testq	%rax, %rax
	je	.L5611
	leaq	-128(%rbp), %r13
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*-104(%rbp)
	testb	%al, %al
	je	.L5594
	movq	-176(%rbp), %rax
	cmpq	%rax, 40(%r12)
	jb	.L5594
	xorl	%eax, %eax
	movl	$32, %ebx
	xorl	%ecx, %ecx
	jmp	.L5598
	.p2align 4,,10
	.p2align 3
.L5741:
	movslq	%ebx, %rsi
	cmpb	$0, -160(%rbp,%rsi)
	je	.L5596
	cmpl	%ecx, %edx
	cmove	%eax, %ebx
.L5596:
	addq	$1, %rax
	cmpq	$32, %rax
	je	.L5740
.L5598:
	movl	48(%r12,%rax,4), %edx
	testl	%edx, %edx
	je	.L5596
	cmpl	%ecx, %edx
	jle	.L5741
	movl	%eax, %ebx
	addq	$1, %rax
	movl	%edx, %ecx
	cmpq	$32, %rax
	jne	.L5598
.L5740:
	movslq	%ebx, %rax
	leaq	-160(%rbp,%rax), %rax
	cmpb	$0, (%rax)
	jne	.L5648
	movb	$1, (%rax)
.L5599:
	movq	-344(%rbp), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r11
	movq	24(%rdi), %rax
	subq	%r11, %rax
	cmpq	$31, %rax
	jbe	.L5742
	leaq	32(%r11), %rax
	movq	%rax, 16(%rdi)
.L5601:
	movq	$0, (%r11)
	movq	32(%r12), %r8
	xorl	%edx, %edx
	movq	-344(%rbp), %rsi
	movl	%ebx, 16(%r11)
	movq	%r8, 8(%r11)
	movslq	88(%r8), %r9
	movq	16(%rsi), %rdi
	movq	%r9, %rax
	divq	%rdi
	movq	8(%rsi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L5602
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L5605
	.p2align 4,,10
	.p2align 3
.L5603:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L5602
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L5602
.L5605:
	cmpq	%rsi, %r9
	jne	.L5603
	cmpq	8(%rcx), %r8
	jne	.L5603
.L5604:
	movq	-240(%rbp), %rax
	movq	(%rax), %rdx
	testb	$4, 488(%rdx)
	je	.L5594
	leaq	.LC26(%rip), %rcx
	cmpl	$32, %ebx
	je	.L5606
	movl	8(%rax), %edx
	movslq	%ebx, %rbx
	testl	%edx, %edx
	jne	.L5607
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rax
	movq	(%rax,%rbx,8), %rcx
.L5606:
	movq	32(%r12), %rax
	movq	40(%r12), %rdx
	leaq	.LC110(%rip), %rdi
	movq	32(%rax), %rax
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5594
	.p2align 4,,10
	.p2align 3
.L5733:
	addq	$1, 40(%rdx)
	movl	4(%r12), %eax
	addq	$8, %rbx
	shrl	$7, %eax
	andl	$63, %eax
	addl	$1, 48(%rdx,%rax,4)
	cmpq	%rbx, %r15
	jne	.L5586
	jmp	.L5743
	.p2align 4,,10
	.p2align 3
.L5731:
	movq	16(%rsi), %rdx
	movq	16(%rdx), %rdx
	movq	8(%rdx), %rdi
	movq	16(%rdx), %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%rax, %rdx
	jbe	.L5744
	movq	(%rdi,%rax,8), %rdx
	cmpb	$0, 120(%rdx)
	je	.L5567
	.p2align 4,,10
	.p2align 3
.L5569:
	addl	$1, -348(%rbp)
	jmp	.L5566
	.p2align 4,,10
	.p2align 3
.L5737:
	cmpq	-272(%rbp), %rdx
	je	.L5639
.L5641:
	movq	%rdx, %rdi
	movl	%r8d, -352(%rbp)
	movq	%rdx, -376(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	-352(%rbp), %r8d
	movq	-376(%rbp), %rdx
	movq	32(%rax), %rax
	cmpl	88(%rax), %r8d
	jg	.L5639
.L5640:
	leaq	.LC109(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5738:
	movq	32(%rdx), %rax
	xorl	%edi, %edi
	cmpl	88(%rax), %r8d
	setl	%dil
	jmp	.L5585
	.p2align 4,,10
	.p2align 3
.L5578:
	movq	%r13, %rdx
	cmpq	%r13, -272(%rbp)
	je	.L5655
	movl	88(%r8), %r8d
	jmp	.L5641
	.p2align 4,,10
	.p2align 3
.L5734:
	movl	$176, %esi
	movq	%r8, -376(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-376(%rbp), %r8
	pxor	%xmm0, %xmm0
	movq	%rax, %r14
	jmp	.L5577
	.p2align 4,,10
	.p2align 3
.L5648:
	movl	$32, %ebx
	jmp	.L5599
	.p2align 4,,10
	.p2align 3
.L5602:
	movq	-344(%rbp), %rdi
	movq	%r11, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	call	_ZNSt10_HashtableIN2v88internal8compiler19LinearScanAllocator17RangeWithRegisterES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS4_6EqualsENS4_4HashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm
	jmp	.L5604
.L5739:
	testq	%rax, %rax
	je	.L5609
.L5591:
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L5609:
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS3_16InstructionBlockEPNS2_16ZoneUnorderedSetINS4_17RangeWithRegisterENS8_4HashENS8_6EqualsEEEEUlPNS3_17TopLevelLiveRangeEE1_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation(%rip), %rax
	leaq	_ZNSt17_Function_handlerIFbPN2v88internal8compiler17TopLevelLiveRangeEEZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS6_17RangeWithRegisterENSA_4HashENSA_6EqualsEEEEUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rdx
	movq	-200(%rbp), %r12
	movq	%rax, %xmm0
	movq	%rdx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	cmpq	%r15, %r12
	je	.L5650
	leaq	-312(%rbp), %r14
	jmp	.L5627
	.p2align 4,,10
	.p2align 3
.L5613:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	movq	-80(%rbp), %rax
	cmpq	%r15, %r12
	je	.L5745
.L5627:
	movq	32(%r12), %rdx
	movq	%rdx, -312(%rbp)
	testq	%rax, %rax
	je	.L5611
	leaq	-96(%rbp), %r13
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*-72(%rbp)
	testb	%al, %al
	je	.L5613
	movq	-176(%rbp), %rax
	cmpq	%rax, 40(%r12)
	jb	.L5613
	xorl	%eax, %eax
	movl	$32, %ebx
	xorl	%ecx, %ecx
	jmp	.L5617
	.p2align 4,,10
	.p2align 3
.L5747:
	movslq	%ebx, %rsi
	cmpb	$0, -160(%rbp,%rsi)
	je	.L5615
	cmpl	%ecx, %edx
	cmove	%eax, %ebx
.L5615:
	addq	$1, %rax
	cmpq	$32, %rax
	je	.L5746
.L5617:
	movl	48(%r12,%rax,4), %edx
	testl	%edx, %edx
	je	.L5615
	cmpl	%ecx, %edx
	jle	.L5747
	movl	%eax, %ebx
	addq	$1, %rax
	movl	%edx, %ecx
	cmpq	$32, %rax
	jne	.L5617
.L5746:
	movslq	%ebx, %rax
	leaq	-160(%rbp,%rax), %rax
	cmpb	$0, (%rax)
	jne	.L5653
	movb	$1, (%rax)
.L5618:
	movq	-344(%rbp), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %r11
	movq	24(%rdi), %rax
	subq	%r11, %rax
	cmpq	$31, %rax
	jbe	.L5748
	leaq	32(%r11), %rax
	movq	%rax, 16(%rdi)
.L5620:
	movq	$0, (%r11)
	movq	32(%r12), %r8
	xorl	%edx, %edx
	movq	-344(%rbp), %rsi
	movl	%ebx, 16(%r11)
	movq	%r8, 8(%r11)
	movslq	88(%r8), %r9
	movq	16(%rsi), %rdi
	movq	%r9, %rax
	divq	%rdi
	movq	8(%rsi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L5621
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L5624
	.p2align 4,,10
	.p2align 3
.L5622:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L5621
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L5621
.L5624:
	cmpq	%r9, %rsi
	jne	.L5622
	cmpq	8(%rcx), %r8
	jne	.L5622
.L5623:
	movq	-240(%rbp), %rax
	movq	(%rax), %rdx
	testb	$4, 488(%rdx)
	je	.L5613
	leaq	.LC26(%rip), %rcx
	cmpl	$32, %ebx
	je	.L5625
	movl	8(%rax), %eax
	movslq	%ebx, %rbx
	testl	%eax, %eax
	jne	.L5626
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %rax
	movq	(%rax,%rbx,8), %rcx
.L5625:
	movq	32(%r12), %rax
	movq	40(%r12), %rdx
	leaq	.LC110(%rip), %rdi
	movq	32(%rax), %rax
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5613
	.p2align 4,,10
	.p2align 3
.L5653:
	movl	$32, %ebx
	jmp	.L5618
	.p2align 4,,10
	.p2align 3
.L5621:
	movq	-344(%rbp), %rdi
	movq	%r11, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	movl	$1, %r8d
	call	_ZNSt10_HashtableIN2v88internal8compiler19LinearScanAllocator17RangeWithRegisterES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS4_6EqualsENS4_4HashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm
	jmp	.L5623
.L5745:
	testq	%rax, %rax
	je	.L5628
.L5610:
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L5628:
	movq	-208(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L5629
	leaq	-232(%rbp), %rcx
.L5632:
	movq	24(%rdx), %rax
	testq	%rax, %rax
	je	.L5630
.L5631:
	movq	24(%rax), %rsi
	movq	%rcx, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler17TopLevelLiveRangeESt4pairIKS4_ZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS7_17RangeWithRegisterENSB_4HashENSB_6EqualsEEEE4VoteESt10_Select1stISH_EZNS7_32ComputeStateFromManyPredecessorsES9_SF_E27TopLevelLiveRangeComparatorNS1_13ZoneAllocatorISH_EEE8_M_eraseEPSt13_Rb_tree_nodeISH_E
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L5631
.L5630:
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L5632
.L5629:
	movq	-280(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L5563
	leaq	-304(%rbp), %rcx
.L5636:
	movq	24(%rdx), %rax
	testq	%rax, %rax
	je	.L5634
.L5635:
	movq	24(%rax), %rsi
	movq	%rcx, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler17TopLevelLiveRangeESt4pairIKS4_ZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS7_17RangeWithRegisterENSB_4HashENSB_6EqualsEEEE4VoteESt10_Select1stISH_EZNS7_32ComputeStateFromManyPredecessorsES9_SF_E27TopLevelLiveRangeComparatorNS1_13ZoneAllocatorISH_EEE8_M_eraseEPSt13_Rb_tree_nodeISH_E
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L5635
.L5634:
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L5636
.L5563:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5749
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5655:
	.cfi_restore_state
	movl	$1, %edi
	jmp	.L5585
.L5607:
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rax
	movq	(%rax,%rbx,8), %rcx
	jmp	.L5606
.L5626:
	leaq	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names(%rip), %rax
	movq	(%rax,%rbx,8), %rcx
	jmp	.L5625
.L5742:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r11
	jmp	.L5601
.L5748:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r11
	jmp	.L5620
.L5643:
	movq	%r9, %rax
	xorl	%esi, %esi
	xorl	%r14d, %r14d
	jmp	.L5564
.L5650:
	leaq	-96(%rbp), %r13
	jmp	.L5610
.L5588:
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS3_16InstructionBlockEPNS2_16ZoneUnorderedSetINS4_17RangeWithRegisterENS8_4HashENS8_6EqualsEEEEUlPNS3_17TopLevelLiveRangeEE0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation(%rip), %rax
	movq	%rbx, -176(%rbp)
	leaq	_ZNSt17_Function_handlerIFbPN2v88internal8compiler17TopLevelLiveRangeEEZNS2_19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS2_16InstructionBlockEPNS1_16ZoneUnorderedSetINS6_17RangeWithRegisterENSA_4HashENSA_6EqualsEEEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rdx
	movq	%rax, %xmm0
	movq	%rdx, %xmm4
	leaq	-128(%rbp), %r13
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -112(%rbp)
	jmp	.L5591
.L5611:
	call	_ZSt25__throw_bad_function_callv@PLT
.L5744:
	movq	%rax, %rsi
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L5645:
	leaq	-128(%rbp), %r13
	jmp	.L5591
.L5749:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23505:
	.size	_ZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS1_16InstructionBlockEPNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS6_4HashENS6_6EqualsEEE, .-_ZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS1_16InstructionBlockEPNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS6_4HashENS6_6EqualsEEE
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator17AllocateRegistersEv.str1.8,"aMS",@progbits,1
	.align 8
.LC111:
	.string	"!data()->is_turbo_preprocess_ranges()"
	.align 8
.LC112:
	.string	"Processing boundary at %d leaving %d\n"
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator17AllocateRegistersEv.str1.1,"aMS",@progbits,1
.LC114:
	.string	"Single predecessor for B%d\n"
.LC115:
	.string	"Using information from B%d\n"
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator17AllocateRegistersEv.str1.8
	.align 8
.LC116:
	.string	"Not a fallthrough. Adding %zu elements...\n"
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator17AllocateRegistersEv.str1.1
.LC117:
	.string	"Two predecessors for B%d\n"
	.section	.rodata._ZN2v88internal8compiler19LinearScanAllocator17AllocateRegistersEv.str1.8
	.align 8
.LC118:
	.string	"Processing interval %d:%d start=%d\n"
	.section	.text._ZN2v88internal8compiler19LinearScanAllocator17AllocateRegistersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19LinearScanAllocator17AllocateRegistersEv
	.type	_ZN2v88internal8compiler19LinearScanAllocator17AllocateRegistersEv, @function
_ZN2v88internal8compiler19LinearScanAllocator17AllocateRegistersEv:
.LFB23580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler17RegisterAllocator41SplitAndSpillRangesDefinedByMemoryOperandEv
	movq	(%r12), %rdx
	movq	464(%rdx), %rax
	movq	472(%rdx), %rcx
	cmpq	%rax, %rcx
	je	.L5751
	.p2align 4,,10
	.p2align 3
.L5753:
	movq	8(%rax), %rdx
	cmpq	%rdx, 16(%rax)
	je	.L5752
	movq	%rdx, 16(%rax)
.L5752:
	addq	$32, %rax
	cmpq	%rcx, %rax
	jne	.L5753
	movq	(%r12), %rdx
.L5751:
	testb	$4, 488(%rdx)
	jne	.L5961
.L5754:
	movq	176(%rdx), %r15
	movq	168(%rdx), %r13
	movq	%r15, %r14
	subq	%r13, %r14
	cmpq	%r15, %r13
	je	.L5756
	leaq	-128(%rbp), %rax
	movq	0(%r13), %rbx
	addq	$8, %r13
	movq	%rax, -136(%rbp)
	jmp	.L5757
	.p2align 4,,10
	.p2align 3
.L5758:
	cmpq	%r13, %r15
	je	.L5756
.L5763:
	movq	176(%rdx), %rax
	movq	0(%r13), %rbx
	addq	$8, %r13
	subq	168(%rdx), %rax
	cmpq	%rax, %r14
	jne	.L5962
.L5757:
	testq	%rbx, %rbx
	je	.L5758
	cmpq	$0, 16(%rbx)
	je	.L5758
	movl	4(%rbx), %eax
	movl	%eax, %esi
	shrl	$13, %esi
	cmpb	$11, %sil
	seta	%sil
	movzbl	%sil, %esi
	cmpl	8(%r12), %esi
	je	.L5762
	cmpq	%r13, %r15
	jne	.L5763
.L5756:
	movl	8(%r12), %eax
	testl	%eax, %eax
	jne	.L5764
	movq	200(%rdx), %rbx
	movq	208(%rdx), %r13
	cmpq	%r13, %rbx
	je	.L5765
	.p2align 4,,10
	.p2align 3
.L5767:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L5766
	testb	$16, 7(%rsi)
	jne	.L5766
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator13AddToInactiveEPNS1_9LiveRangeE
.L5766:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L5767
.L5958:
	movq	(%r12), %rdx
.L5765:
	movq	16(%rdx), %rax
	movq	16(%rax), %rcx
	movq	16(%rcx), %rsi
	movq	8(%rcx), %rax
	movq	%rsi, %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	subl	$1, %ecx
	movl	%ecx, -148(%rbp)
	cmpq	%rax, %rsi
	je	.L5963
	movq	(%rax), %rax
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	movl	%r13d, -136(%rbp)
	movl	116(%rax), %eax
	leal	0(,%rax,4), %ebx
	leaq	56(%r12), %rax
	movl	%ebx, %r14d
	movq	%rax, -144(%rbp)
	movl	%ecx, %ebx
	jmp	.L5771
	.p2align 4,,10
	.p2align 3
.L5855:
	movl	%r14d, %r13d
	xorl	%r15d, %r15d
.L5774:
	movq	(%r12), %rax
	movl	488(%rax), %eax
	testb	$1, %al
	je	.L5778
.L5967:
	testb	$2, %al
	jne	.L5964
	andl	$4, %eax
	cmpl	%r14d, %r13d
	jge	.L5965
	testl	%eax, %eax
	jne	.L5966
.L5844:
	movq	72(%r12), %rdi
	movq	-144(%rbp), %rsi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	subq	$1, 88(%r12)
	cmpq	32(%r15), %r15
	je	.L5845
.L5846:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator14ForwardStateToENS1_16LifetimePositionE
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator19ProcessCurrentRangeEPNS1_9LiveRangeENS1_22RegisterAllocationData9SpillModeE
.L5843:
	movq	(%r12), %rdx
.L5771:
	cmpq	$0, 88(%r12)
	jne	.L5772
	movl	488(%rdx), %eax
	movl	-136(%rbp), %edi
	cmpl	%edi, -148(%rbp)
	jle	.L5773
	testb	$1, %al
	je	.L5773
.L5772:
	movq	496(%rdx), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	cmpq	$0, 88(%r12)
	je	.L5855
	movq	72(%r12), %rax
	movl	%r14d, %r13d
	movq	32(%rax), %r15
	testq	%r15, %r15
	je	.L5774
	movq	16(%r15), %rax
	movl	(%rax), %r13d
	movq	(%r12), %rax
	movl	488(%rax), %eax
	testb	$1, %al
	jne	.L5967
.L5778:
	andl	$4, %eax
	testl	%eax, %eax
	je	.L5844
.L5966:
	movq	32(%r15), %rax
	movl	(%r15), %edx
	movl	%r13d, %ecx
	leaq	.LC118(%rip), %rdi
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5844
	.p2align 4,,10
	.p2align 3
.L5969:
	movq	%rbx, -128(%rbp)
	cmpq	$0, 16(%rbx)
	je	.L5759
	testb	$4, 488(%rdx)
	jne	.L5968
.L5761:
	movq	-136(%rbp), %rsi
	leaq	40(%r12), %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE15_M_insert_equalIRKS4_EESt17_Rb_tree_iteratorIS4_EOT_
	movq	(%r12), %rdx
.L5759:
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L5758
	movl	4(%rbx), %eax
.L5762:
	testb	$1, %al
	jne	.L5759
	jmp	.L5969
	.p2align 4,,10
	.p2align 3
.L5962:
	leaq	.LC63(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5968:
	movq	32(%rbx), %rax
	movl	(%rbx), %edx
	leaq	.LC77(%rip), %rdi
	movl	88(%rax), %esi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5761
	.p2align 4,,10
	.p2align 3
.L5961:
	leaq	_ZSt4cout(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator18PrintRangeOverviewERSo
	movq	(%r12), %rdx
	jmp	.L5754
	.p2align 4,,10
	.p2align 3
.L5764:
	movq	264(%rdx), %rbx
	movq	272(%rdx), %r13
	cmpq	%r13, %rbx
	je	.L5765
	.p2align 4,,10
	.p2align 3
.L5769:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L5768
	testb	$16, 7(%rsi)
	jne	.L5768
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator13AddToInactiveEPNS1_9LiveRangeE
.L5768:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L5769
	jmp	.L5958
	.p2align 4,,10
	.p2align 3
.L5845:
	testb	$8, 4(%r15)
	je	.L5846
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator19TryReuseSpillForPhiEPNS1_17TopLevelLiveRangeE.part.0
	testb	%al, %al
	je	.L5846
	jmp	.L5843
	.p2align 4,,10
	.p2align 3
.L5965:
	testl	%eax, %eax
	jne	.L5970
.L5781:
	leal	-1(%r14), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator14ForwardStateToENS1_16LifetimePositionE
	testl	%r14d, %r14d
	movq	(%r12), %rax
	leal	3(%r14), %esi
	cmovns	%r14d, %esi
	movq	16(%rax), %rdi
	sarl	$2, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movslq	-136(%rbp), %r13
	movq	%rax, %r15
	movq	(%r12), %rax
	salq	$5, %r13
	addq	464(%rax), %r13
	leaq	96(%r12), %rax
	cmpq	%rax, %r13
	je	.L5782
	movq	112(%r12), %rcx
	movq	104(%r12), %r8
	movq	8(%r13), %rdi
	movq	24(%r13), %rax
	movq	%rcx, %r9
	subq	%r8, %r9
	subq	%rdi, %rax
	movq	%r9, %rsi
	sarq	$3, %rax
	sarq	$3, %rsi
	cmpq	%rax, %rsi
	ja	.L5971
	movq	16(%r13), %rax
	movq	%rax, %rdx
	subq	%rdi, %rdx
	movq	%rdx, %r10
	sarq	$3, %r10
	cmpq	%r10, %rsi
	ja	.L5793
	leaq	(%rdi,%r9), %rdx
	cmpq	%rcx, %r8
	je	.L5786
	movq	%r9, %rdx
	movq	%r8, %rsi
	movq	%r9, -136(%rbp)
	call	memmove@PLT
	movq	-136(%rbp), %r9
	movq	8(%r13), %rdx
	addq	%r9, %rdx
	.p2align 4,,10
	.p2align 3
.L5786:
	movq	%rdx, 16(%r13)
.L5782:
	movq	40(%r15), %rdx
	movq	48(%r15), %rax
	movl	%ebx, %esi
	movzbl	120(%r15), %ecx
	andl	$1, %esi
	subq	%rdx, %rax
	cmpq	$4, %rax
	je	.L5972
.L5801:
	cmpb	%cl, %sil
	je	.L5850
	movzbl	%cl, %ebx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator14ForwardStateToENS1_16LifetimePositionE
	movq	%r15, %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS1_22RegisterAllocationData9SpillModeEPNS1_16InstructionBlockE
	.p2align 4,,10
	.p2align 3
.L5850:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator14ForwardStateToENS1_16LifetimePositionE
	movq	(%r12), %rax
	leaq	-72(%rbp), %rcx
	leaq	-88(%rbp), %rdi
	movl	$100, %esi
	movq	%rcx, -136(%rbp)
	movq	(%rax), %rax
	movq	%rcx, -120(%rbp)
	movq	$1, -112(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movl	$0x3f800000, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	movq	%rax, %r13
	cmpq	-112(%rbp), %rax
	jbe	.L5803
	cmpq	$1, %rax
	movq	-136(%rbp), %rcx
	je	.L5973
	leaq	0(,%rax,8), %rdx
	movq	-128(%rbp), %rdi
	movq	%rdx, %rsi
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal4Zone3NewEm
	movq	-136(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	%rax, %rcx
.L5805:
	movq	%rcx, -120(%rbp)
	movq	%r13, -112(%rbp)
.L5803:
	movq	40(%r15), %r13
	movq	48(%r15), %rax
	subq	%r13, %rax
	cmpq	$4, %rax
	je	.L5974
	cmpq	$8, %rax
	jne	.L5820
	movq	(%r12), %rax
	movl	100(%r15), %esi
	testb	$4, 488(%rax)
	jne	.L5975
.L5821:
	movl	0(%r13), %eax
	cmpl	%esi, %eax
	jl	.L5822
	movq	(%r12), %rdx
.L5823:
	movslq	4(%r13), %r13
.L5826:
	leal	1(%r13), %ecx
	testb	$4, 488(%rdx)
	jne	.L5976
	movl	100(%r15), %eax
	cmpl	%ecx, %eax
	sete	-136(%rbp)
	je	.L5819
	salq	$5, %r13
	addq	464(%rdx), %r13
.L5830:
	movq	8(%r13), %rax
	movq	16(%r13), %rcx
	cmpq	%rcx, %rax
	je	.L5819
	movq	%rax, %r13
	leaq	-128(%rbp), %rax
	movl	%ebx, -160(%rbp)
	movq	%rcx, %rbx
	movq	%r12, -168(%rbp)
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L5838:
	movq	0(%r13), %rdx
	movl	4(%rdx), %eax
	shrl	$7, %eax
	andl	$63, %eax
	cmpl	$32, %eax
	je	.L5831
	movq	-128(%rbp), %rdi
	movq	16(%rdi), %r11
	movq	24(%rdi), %rax
	subq	%r11, %rax
	cmpq	$31, %rax
	jbe	.L5977
	leaq	32(%r11), %rax
	movq	%rax, 16(%rdi)
.L5833:
	movq	$0, (%r11)
	movl	4(%rdx), %eax
	movq	32(%rdx), %r8
	xorl	%edx, %edx
	shrl	$7, %eax
	andl	$63, %eax
	movq	%r8, 8(%r11)
	movl	%eax, 16(%r11)
	movslq	88(%r8), %r9
	movq	-112(%rbp), %rdi
	movq	%r9, %rax
	divq	%rdi
	movq	-120(%rbp), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L5834
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L5837
	.p2align 4,,10
	.p2align 3
.L5835:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L5834
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L5834
.L5837:
	cmpq	%rsi, %r9
	jne	.L5835
	cmpq	%r8, 8(%rcx)
	jne	.L5835
.L5831:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L5838
	jmp	.L5959
	.p2align 4,,10
	.p2align 3
.L5972:
	movl	(%rdx), %eax
	movl	100(%r15), %edi
	addl	$1, %eax
	movl	%edi, -136(%rbp)
	cmpl	%eax, %edi
	jne	.L5801
	cmpb	%cl, %sil
	je	.L5848
	movzbl	%cl, %ebx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator14ForwardStateToENS1_16LifetimePositionE
	movq	%r15, %rdx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS1_22RegisterAllocationData9SpillModeEPNS1_16InstructionBlockE
	movl	100(%r15), %eax
	movl	%eax, -136(%rbp)
.L5848:
	movl	116(%r15), %r14d
	sall	$2, %r14d
	jmp	.L5843
	.p2align 4,,10
	.p2align 3
.L5773:
	testb	$4, %al
	jne	.L5978
.L5750:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5979
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5964:
	.cfi_restore_state
	leaq	.LC111(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5970:
	movl	-136(%rbp), %edx
	movl	%r14d, %esi
	leaq	.LC112(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5781
	.p2align 4,,10
	.p2align 3
.L5793:
	testq	%rdx, %rdx
	je	.L5795
	movq	%r8, %rsi
	movq	%r9, -136(%rbp)
	call	memmove@PLT
	movq	16(%r13), %rax
	movq	8(%r13), %rdi
	movq	112(%r12), %rcx
	movq	104(%r12), %r8
	movq	%rax, %rdx
	movq	-136(%rbp), %r9
	subq	%rdi, %rdx
.L5795:
	leaq	(%r8,%rdx), %rsi
	cmpq	%rcx, %rsi
	je	.L5980
	leaq	-8(%rcx), %rdi
	leaq	16(%r8,%rdx), %rdx
	subq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	%rdx, %rax
	leaq	16(%rax), %rdx
	setnb	%r8b
	cmpq	%rdx, %rsi
	setnb	%dl
	orb	%dl, %r8b
	je	.L5921
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rdi
	je	.L5921
	leaq	1(%rdi), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L5798:
	movdqu	(%rsi,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L5798
	movq	%rcx, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rdi
	addq	%rdi, %rsi
	addq	%rdi, %rax
	cmpq	%rcx, %rdx
	je	.L5800
	movq	(%rsi), %rdx
	movq	%rdx, (%rax)
.L5800:
	movq	8(%r13), %rdx
	addq	%r9, %rdx
	jmp	.L5786
	.p2align 4,,10
	.p2align 3
.L5921:
	movq	(%rsi), %rdx
	addq	$8, %rsi
	addq	$8, %rax
	movq	%rdx, -8(%rax)
	cmpq	%rcx, %rsi
	jne	.L5921
	jmp	.L5800
	.p2align 4,,10
	.p2align 3
.L5971:
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.L5784
	movq	0(%r13), %rdi
	movq	%r9, %rsi
	movq	%rcx, -168(%rbp)
	movq	%r8, -160(%rbp)
	movq	%r9, -136(%rbp)
	call	_ZN2v88internal4Zone3NewEm
	movq	-168(%rbp), %rcx
	movq	-160(%rbp), %r8
	movq	-136(%rbp), %r9
	movq	%rax, %rdx
.L5784:
	cmpq	%rcx, %r8
	je	.L5790
	subq	$8, %rcx
	leaq	15(%r8), %rax
	subq	%r8, %rcx
	subq	%rdx, %rax
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L5858
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L5858
	addq	$1, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L5788:
	movdqu	(%r8,%rax), %xmm1
	movups	%xmm1, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L5788
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rsi
	addq	%rsi, %r8
	addq	%rdx, %rsi
	cmpq	%rcx, %rax
	je	.L5790
	movq	(%r8), %rax
	movq	%rax, (%rsi)
.L5790:
	movq	%rdx, 8(%r13)
	addq	%r9, %rdx
	movq	%rdx, 24(%r13)
	jmp	.L5786
.L5978:
	leaq	_ZSt4cout(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator18PrintRangeOverviewERSo
	jmp	.L5750
.L5974:
	movq	(%r12), %rax
	movl	100(%r15), %esi
	testb	$4, 488(%rax)
	jne	.L5981
	movslq	0(%r13), %r13
.L5852:
	leal	1(%r13), %edx
	cmpl	%esi, %edx
	sete	-136(%rbp)
	je	.L5819
	salq	$5, %r13
	addq	464(%rax), %r13
.L5810:
	movq	8(%r13), %rax
	movq	16(%r13), %rcx
	cmpq	%rcx, %rax
	je	.L5819
	movq	%rax, %r13
	leaq	-128(%rbp), %rax
	movl	%ebx, -160(%rbp)
	movq	%rcx, %rbx
	movq	%r12, -168(%rbp)
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L5818:
	movq	0(%r13), %rdx
	movl	4(%rdx), %eax
	shrl	$7, %eax
	andl	$63, %eax
	cmpl	$32, %eax
	je	.L5811
	movq	-128(%rbp), %rdi
	movq	16(%rdi), %r11
	movq	24(%rdi), %rax
	subq	%r11, %rax
	cmpq	$31, %rax
	jbe	.L5982
	leaq	32(%r11), %rax
	movq	%rax, 16(%rdi)
.L5813:
	movq	$0, (%r11)
	movl	4(%rdx), %eax
	movq	32(%rdx), %r8
	xorl	%edx, %edx
	shrl	$7, %eax
	andl	$63, %eax
	movq	%r8, 8(%r11)
	movl	%eax, 16(%r11)
	movslq	88(%r8), %r9
	movq	-112(%rbp), %rdi
	movq	%r9, %rax
	divq	%rdi
	movq	-120(%rbp), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L5814
	movq	(%rax), %rcx
	movq	24(%rcx), %rsi
	jmp	.L5817
	.p2align 4,,10
	.p2align 3
.L5815:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L5814
	movq	24(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L5814
.L5817:
	cmpq	%rsi, %r9
	jne	.L5815
	cmpq	%r8, 8(%rcx)
	jne	.L5815
.L5811:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L5818
.L5959:
	movl	-160(%rbp), %ebx
	movq	-168(%rbp), %r12
.L5819:
	cmpb	$0, -136(%rbp)
	jne	.L5840
	leaq	-128(%rbp), %r13
	jmp	.L5839
.L5820:
	leaq	-128(%rbp), %r13
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler19LinearScanAllocator32ComputeStateFromManyPredecessorsEPNS1_16InstructionBlockEPNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS6_4HashENS6_6EqualsEEE
.L5839:
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%ebx, %ecx
	call	_ZN2v88internal8compiler19LinearScanAllocator18SpillNotLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionENS1_22RegisterAllocationData9SpillModeE
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator16ReloadLiveRangesERNS0_16ZoneUnorderedSetINS2_17RangeWithRegisterENS4_4HashENS4_6EqualsEEENS1_16LifetimePositionE
.L5840:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator14ForwardStateToENS1_16LifetimePositionE
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L5841
	.p2align 4,,10
	.p2align 3
.L5842:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L5842
.L5841:
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movl	100(%r15), %eax
	movl	116(%r15), %r14d
	movl	%eax, -136(%rbp)
	sall	$2, %r14d
	jmp	.L5843
.L5822:
	cmpb	$0, 120(%r15)
	je	.L5983
.L5824:
	movl	4(%r13), %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator27ConsiderBlockForControlFlowEPNS1_16InstructionBlockENS1_9RpoNumberE
	testb	%al, %al
	jne	.L5827
	movslq	0(%r13), %r13
	movq	(%r12), %rdx
	jmp	.L5826
.L5975:
	leaq	.LC117(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	40(%r15), %r13
	movl	100(%r15), %esi
	jmp	.L5821
.L5981:
	xorl	%eax, %eax
	leaq	.LC114(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	40(%r15), %rax
	movslq	(%rax), %r13
	movq	(%r12), %rax
	testb	$4, 488(%rax)
	je	.L5808
	xorl	%eax, %eax
	movl	%r13d, %esi
	leaq	.LC115(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	100(%r15), %edx
	leal	1(%r13), %eax
	cmpl	%eax, %edx
	sete	-136(%rbp)
	je	.L5819
	movq	(%r12), %rax
	salq	$5, %r13
	addq	464(%rax), %r13
	movq	8(%r13), %rdx
	movq	16(%r13), %rsi
	testb	$4, 488(%rax)
	je	.L5810
	subq	%rdx, %rsi
	leaq	.LC116(%rip), %rdi
	xorl	%eax, %eax
	sarq	$3, %rsi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5810
	.p2align 4,,10
	.p2align 3
.L5814:
	movl	$1, %r8d
	movq	%r11, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZNSt10_HashtableIN2v88internal8compiler19LinearScanAllocator17RangeWithRegisterES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS4_6EqualsENS4_4HashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm
	jmp	.L5811
	.p2align 4,,10
	.p2align 3
.L5834:
	movl	$1, %r8d
	movq	%r11, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZNSt10_HashtableIN2v88internal8compiler19LinearScanAllocator17RangeWithRegisterES4_NS1_13ZoneAllocatorIS4_EENSt8__detail9_IdentityENS4_6EqualsENS4_4HashENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS4_Lb1EEEm
	jmp	.L5831
.L5858:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L5787:
	movq	(%r8,%rax,8), %rsi
	movq	%rsi, (%rdx,%rax,8)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rcx, %rsi
	jne	.L5787
	jmp	.L5790
.L5982:
	movl	$32, %esi
	movq	%rdx, -176(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-176(%rbp), %rdx
	movq	%rax, %r11
	jmp	.L5813
.L5977:
	movl	$32, %esi
	movq	%rdx, -176(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-176(%rbp), %rdx
	movq	%rax, %r11
	jmp	.L5833
.L5976:
	xorl	%eax, %eax
	movl	%r13d, %esi
	leaq	.LC115(%rip), %rdi
	movl	%ecx, -136(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	-136(%rbp), %ecx
	movl	100(%r15), %eax
	cmpl	%ecx, %eax
	sete	-136(%rbp)
	je	.L5819
	movq	(%r12), %rax
	salq	$5, %r13
	addq	464(%rax), %r13
	movq	8(%r13), %rdx
	movq	16(%r13), %rsi
	testb	$4, 488(%rax)
	je	.L5830
	subq	%rdx, %rsi
	leaq	.LC116(%rip), %rdi
	xorl	%eax, %eax
	sarq	$3, %rsi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5830
.L5980:
	leaq	(%rdi,%r9), %rdx
	jmp	.L5786
.L5983:
	movq	(%r12), %rdx
	movslq	%eax, %rsi
	movq	16(%rdx), %rcx
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rax
	movq	16(%rcx), %r8
	subq	%rax, %r8
	sarq	$3, %r8
	cmpq	%r8, %rsi
	jnb	.L5984
	movq	(%rax,%rsi,8), %rax
	cmpb	$0, 120(%rax)
	je	.L5824
	jmp	.L5823
.L5973:
	movq	$0, -72(%rbp)
	jmp	.L5805
.L5827:
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator31ChooseOneOfTwoPredecessorStatesEPNS1_16InstructionBlockENS1_16LifetimePositionE
	movq	(%r12), %rdx
	movslq	%eax, %r13
	jmp	.L5826
.L5963:
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L5979:
	call	__stack_chk_fail@PLT
.L5808:
	movl	100(%r15), %esi
	jmp	.L5852
.L5984:
	movq	%r8, %rdx
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE23580:
	.size	_ZN2v88internal8compiler19LinearScanAllocator17AllocateRegistersEv, .-_ZN2v88internal8compiler19LinearScanAllocator17AllocateRegistersEv
	.section	.rodata._ZNSt5dequeIiN2v88internal22RecyclingZoneAllocatorIiEEE16_M_push_back_auxIJiEEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC119:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIiN2v88internal22RecyclingZoneAllocatorIiEEE16_M_push_back_auxIJiEEEvDpOT_,"axG",@progbits,_ZNSt5dequeIiN2v88internal22RecyclingZoneAllocatorIiEEE16_M_push_back_auxIJiEEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIiN2v88internal22RecyclingZoneAllocatorIiEEE16_M_push_back_auxIJiEEEvDpOT_
	.type	_ZNSt5dequeIiN2v88internal22RecyclingZoneAllocatorIiEEE16_M_push_back_auxIJiEEEvDpOT_, @function
_ZNSt5dequeIiN2v88internal22RecyclingZoneAllocatorIiEEE16_M_push_back_auxIJiEEEvDpOT_:
.LFB29565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$2, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$7, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$2, %rax
	addq	%rdx, %rax
	cmpq	$536870911, %rax
	je	.L6003
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L6004
.L5987:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L5995
	cmpq	$127, 8(%rax)
	ja	.L6005
.L5995:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L6006
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L5996:
	movq	%rax, 8(%r13)
	movl	(%r12), %edx
	movq	64(%rbx), %rax
	movl	%edx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6004:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L6007
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L6008
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L5992:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L5993
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L5993:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L5994
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L5994:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L5990:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L5987
	.p2align 4,,10
	.p2align 3
.L6005:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L5996
	.p2align 4,,10
	.p2align 3
.L6007:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L5989
	cmpq	%r13, %rsi
	je	.L5990
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L5990
	.p2align 4,,10
	.p2align 3
.L5989:
	cmpq	%r13, %rsi
	je	.L5990
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L5990
	.p2align 4,,10
	.p2align 3
.L6006:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L5996
	.p2align 4,,10
	.p2align 3
.L6008:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L5992
.L6003:
	leaq	.LC119(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29565:
	.size	_ZNSt5dequeIiN2v88internal22RecyclingZoneAllocatorIiEEE16_M_push_back_auxIJiEEEvDpOT_, .-_ZNSt5dequeIiN2v88internal22RecyclingZoneAllocatorIiEEE16_M_push_back_auxIJiEEEvDpOT_
	.section	.text._ZNSt11_Deque_baseIiN2v88internal22RecyclingZoneAllocatorIiEEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIiN2v88internal22RecyclingZoneAllocatorIiEEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIiN2v88internal22RecyclingZoneAllocatorIiEEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIiN2v88internal22RecyclingZoneAllocatorIiEEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIiN2v88internal22RecyclingZoneAllocatorIiEEE17_M_initialize_mapEm:
.LFB29968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$7, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L6023
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L6011:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L6012
	movq	%r15, %rbx
	jmp	.L6017
	.p2align 4,,10
	.p2align 3
.L6013:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L6024
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L6014:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L6012
.L6017:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L6013
	cmpq	$127, 8(%rax)
	jbe	.L6013
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L6017
.L6012:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$127, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,4), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6024:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L6014
	.p2align 4,,10
	.p2align 3
.L6023:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L6011
	.cfi_endproc
.LFE29968:
	.size	_ZNSt11_Deque_baseIiN2v88internal22RecyclingZoneAllocatorIiEEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIiN2v88internal22RecyclingZoneAllocatorIiEEE17_M_initialize_mapEm
	.section	.rodata._ZN2v88internal8compiler18LiveRangeConnector28CommitSpillsInDeferredBlocksEPNS1_17TopLevelLiveRangeEPNS1_19LiveRangeBoundArrayEPNS0_4ZoneE.str1.8,"aMS",@progbits,1
	.align 8
.LC120:
	.string	"Live Range %d will be spilled only in deferred blocks.\n"
	.align 8
.LC121:
	.string	"Spilling deferred spill for range %d at B%d\n"
	.section	.text._ZN2v88internal8compiler18LiveRangeConnector28CommitSpillsInDeferredBlocksEPNS1_17TopLevelLiveRangeEPNS1_19LiveRangeBoundArrayEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18LiveRangeConnector28CommitSpillsInDeferredBlocksEPNS1_17TopLevelLiveRangeEPNS1_19LiveRangeBoundArrayEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler18LiveRangeConnector28CommitSpillsInDeferredBlocksEPNS1_17TopLevelLiveRangeEPNS1_19LiveRangeBoundArrayEPNS0_4ZoneE, @function
_ZN2v88internal8compiler18LiveRangeConnector28CommitSpillsInDeferredBlocksEPNS1_17TopLevelLiveRangeEPNS1_19LiveRangeBoundArrayEPNS0_4ZoneE:
.LFB23658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -376(%rbp)
	movq	%rcx, -368(%rbp)
	movq	(%rdi), %rcx
	movq	%rdi, -400(%rbp)
	movq	16(%rcx), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	104(%rsi), %rax
	movslq	44(%rax), %rdx
	movl	4(%rsi), %eax
	shrl	$13, %eax
	salq	$35, %rdx
	movzbl	%al, %eax
	salq	$5, %rax
	orq	%rax, %rdx
	movq	%rdx, -392(%rbp)
	orq	$13, %rdx
	movq	%rdx, -432(%rbp)
	testb	$4, 488(%rcx)
	jne	.L6207
.L6026:
	movq	%r14, %rbx
	movl	$1, %r12d
.L6029:
	movq	24(%rbx), %r15
	testq	%r15, %r15
	jne	.L6027
	jmp	.L6034
	.p2align 4,,10
	.p2align 3
.L6032:
	testl	%ecx, %ecx
	movq	8(%rdx), %rsi
	leal	63(%rcx), %eax
	movl	%ecx, %edx
	cmovns	%ecx, %eax
	sarl	$31, %edx
	shrl	$26, %edx
	addl	%edx, %ecx
	sarl	$6, %eax
	andl	$63, %ecx
	cltq
	subl	%edx, %ecx
	movq	%r12, %rdx
	salq	%cl, %rdx
	orq	%rdx, (%rsi,%rax,8)
.L6031:
	movq	16(%r15), %r15
	testq	%r15, %r15
	je	.L6034
.L6027:
	movzbl	28(%r15), %eax
	andl	$3, %eax
	cmpb	$3, %al
	je	.L6030
	testb	$1, 4(%rbx)
	je	.L6031
.L6030:
	movl	24(%r15), %eax
	movq	%r13, %rdi
	testl	%eax, %eax
	leal	3(%rax), %esi
	cmovns	%eax, %esi
	sarl	$2, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence19GetInstructionBlockEi@PLT
	movq	112(%r14), %rdx
	movl	100(%rax), %ecx
	cmpl	$1, 4(%rdx)
	jne	.L6032
	movq	%r12, %rax
	salq	%cl, %rax
	orq	%rax, 8(%rdx)
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.L6027
.L6034:
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L6029
	movq	-368(%rbp), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	$0, -248(%rbp)
	movaps	%xmm0, -224(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-256(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	%rax, -408(%rbp)
	call	_ZNSt11_Deque_baseIiN2v88internal22RecyclingZoneAllocatorIiEEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	pxor	%xmm0, %xmm0
	je	.L6208
	movdqa	-256(%rbp), %xmm7
	leaq	-160(%rbp), %r15
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movq	%r15, %rdi
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIiN2v88internal22RecyclingZoneAllocatorIiEEE17_M_initialize_mapEm
	movdqa	-96(%rbp), %xmm7
	movq	-216(%rbp), %r9
	movq	-224(%rbp), %xmm3
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %xmm1
	movq	-184(%rbp), %rcx
	movaps	%xmm7, -192(%rbp)
	movq	%r9, %xmm7
	punpcklqdq	%xmm7, %xmm3
	movdqa	-128(%rbp), %xmm5
	movq	-208(%rbp), %xmm2
	movaps	%xmm3, -320(%rbp)
	movq	%r8, %xmm3
	movq	-168(%rbp), %rdx
	movdqa	-112(%rbp), %xmm6
	punpcklqdq	%xmm3, %xmm2
	movq	-248(%rbp), %rax
	movq	-176(%rbp), %xmm0
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm2, -304(%rbp)
	movq	%rcx, %xmm2
	movq	-256(%rbp), %xmm4
	movq	-240(%rbp), %r11
	punpcklqdq	%xmm2, %xmm1
	movq	-232(%rbp), %r10
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm6, -208(%rbp)
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	%rax, %xmm6
	movaps	%xmm1, -288(%rbp)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm6, %xmm4
	movq	%r11, -336(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -240(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%r10, -328(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -352(%rbp)
	movaps	%xmm0, -272(%rbp)
	testq	%rsi, %rsi
	je	.L6036
	movq	-168(%rbp), %rbx
	movq	-200(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L6037
.L6040:
	testq	%rax, %rax
	je	.L6038
	cmpq	$128, 8(%rax)
	ja	.L6039
.L6038:
	movq	(%rdx), %rax
	movq	$128, 8(%rax)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -248(%rbp)
.L6039:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L6040
	movq	-232(%rbp), %rdi
	movq	-240(%rbp), %rsi
.L6037:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L6036
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L6036:
	movl	$0, -152(%rbp)
	movq	112(%r14), %rax
	movq	%rax, -160(%rbp)
	cmpl	$1, 4(%rax)
	movq	8(%rax), %rax
	je	.L6043
	movq	(%rax), %rax
.L6043:
	movq	%r15, %rdi
	movq	%rax, -144(%rbp)
	leaq	-352(%rbp), %rbx
	movl	$-1, -136(%rbp)
	call	_ZN2v88internal9BitVector8Iterator7AdvanceEv@PLT
	movq	-160(%rbp), %rax
	movl	4(%rax), %eax
	cmpl	%eax, -152(%rbp)
	jl	.L6044
	jmp	.L6049
	.p2align 4,,10
	.p2align 3
.L6209:
	movl	%ecx, (%rdx)
	addq	$4, -288(%rbp)
.L6206:
	movq	%r15, %rdi
	call	_ZN2v88internal9BitVector8Iterator7AdvanceEv@PLT
	movq	-160(%rbp), %rax
	movl	-152(%rbp), %edi
	cmpl	%edi, 4(%rax)
	jle	.L6049
.L6044:
	movq	-272(%rbp), %rax
	movl	-136(%rbp), %ecx
	movq	-288(%rbp), %rdx
	subq	$4, %rax
	movl	%ecx, -256(%rbp)
	cmpq	%rax, %rdx
	jne	.L6209
	movq	-408(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNSt5dequeIiN2v88internal22RecyclingZoneAllocatorIiEEE16_M_push_back_auxIJiEEEvDpOT_
	jmp	.L6206
.L6049:
	movq	-368(%rbp), %rax
	movq	$0, -112(%rbp)
	movl	$0, -144(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%rax, -120(%rbp)
	movq	%rax, -360(%rbp)
	movq	112(%r14), %rax
	movq	$0, -136(%rbp)
	movl	(%rax), %eax
	cmpl	$64, %eax
	jle	.L6210
	subl	$1, %eax
	sarl	$6, %eax
	movslq	%eax, %rbx
	leal	1(%rbx), %eax
	movslq	%eax, %rsi
	movl	%eax, -380(%rbp)
	movq	-368(%rbp), %rax
	salq	$3, %rsi
	movq	16(%rax), %r11
	movq	24(%rax), %rax
	movq	%rax, -416(%rbp)
	subq	%r11, %rax
	cmpq	%rax, %rsi
	ja	.L6211
	movq	-368(%rbp), %rax
	addq	%r11, %rsi
	movq	%rsi, 16(%rax)
.L6051:
	movq	%r11, %rdi
	leaq	8(,%rbx,8), %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %r11
.L6045:
	movq	-392(%rbp), %rax
	movq	%r15, -440(%rbp)
	movq	%r14, -368(%rbp)
	movq	%r11, %r14
	andq	$-8161, %rax
	orq	$12, %rax
	movq	%rax, -416(%rbp)
.L6069:
	cmpl	$1, -380(%rbp)
	movq	-320(%rbp), %rax
	je	.L6052
.L6059:
	cmpq	%rax, -288(%rbp)
	je	.L6201
	movq	-304(%rbp), %rdi
	movslq	(%rax), %rsi
	leaq	-4(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L6212
	addq	$4, %rax
	movq	%rax, -320(%rbp)
.L6056:
	movl	%esi, %edx
	sarl	$31, %edx
	shrl	$26, %edx
	leal	(%rsi,%rdx), %ecx
	andl	$63, %ecx
	subl	%edx, %ecx
	testl	%esi, %esi
	leal	63(%rsi), %edx
	cmovns	%esi, %edx
	sarl	$6, %edx
	movslq	%edx, %rdx
	movq	(%r14,%rdx,8), %rdi
	btq	%rcx, %rdi
	jc	.L6059
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, (%r14,%rdx,8)
.L6131:
	movq	16(%r13), %rax
	movq	8(%rax), %rdx
	movq	16(%rax), %rax
	movq	%rax, %r8
	subq	%rdx, %r8
	sarq	$3, %r8
	cmpq	%r8, %rsi
	jnb	.L6213
	movq	(%rdx,%rsi,8), %r10
	movq	40(%r10), %r15
	movq	48(%r10), %r8
	cmpq	%r15, %r8
	je	.L6069
	movq	%r14, -392(%rbp)
	movq	%r15, %r9
	movq	%r13, %r14
	movq	%r8, %r15
	movq	%r10, %r13
	jmp	.L6068
	.p2align 4,,10
	.p2align 3
.L6215:
	movl	100(%rax), %ecx
	movq	-272(%rbp), %rax
	movq	-288(%rbp), %rdx
	subq	$4, %rax
	movl	%ecx, -256(%rbp)
	cmpq	%rax, %rdx
	je	.L6072
	movl	%ecx, (%rdx)
	addq	$4, -288(%rbp)
.L6074:
	addq	$4, %r9
	cmpq	%r9, %r15
	je	.L6203
.L6226:
	movq	16(%r14), %rax
	movq	8(%rax), %rdx
	movq	16(%rax), %rax
.L6068:
	subq	%rdx, %rax
	movslq	(%r9), %rsi
	sarq	$3, %rax
	cmpq	%rax, %rsi
	jnb	.L6214
	movq	(%rdx,%rsi,8), %rax
	cmpb	$0, 120(%rax)
	jne	.L6215
	movl	116(%rax), %eax
	xorl	%ecx, %ecx
	leal	-2(,%rax,4), %esi
	movq	-376(%rbp), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rdi
	.p2align 4,,10
	.p2align 3
.L6076:
	movq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L6075:
	movq	%rax, %rdx
	subq	%rcx, %rax
	shrq	%rax
	addq	%rcx, %rax
	leaq	(%rax,%rax,2), %r8
	leaq	(%rdi,%r8,8), %rbx
	cmpl	%esi, 8(%rbx)
	jg	.L6075
	cmpl	%esi, 12(%rbx)
	jg	.L6216
	movq	%rax, %rcx
	jmp	.L6076
.L6052:
	cmpq	%rax, -288(%rbp)
	je	.L6201
	movq	-304(%rbp), %rdi
	movslq	(%rax), %rsi
	leaq	-4(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L6061
	addq	$4, %rax
	movq	%rax, -320(%rbp)
.L6062:
	movl	%esi, %ecx
	sarl	$31, %ecx
	shrl	$26, %ecx
	leal	(%rsi,%rcx), %edx
	andl	$63, %edx
	subl	%ecx, %edx
	btq	%rdx, %r14
	jc	.L6052
	movl	$1, %eax
	movl	%esi, %ecx
	salq	%cl, %rax
	orq	%rax, %r14
	jmp	.L6131
	.p2align 4,,10
	.p2align 3
.L6216:
	movq	(%rbx), %rcx
	movl	4(%rcx), %eax
	movl	%eax, %edx
	shrl	$7, %edx
	andl	$63, %edx
	cmpl	$32, %edx
	je	.L6077
	shrl	$13, %eax
	salq	$35, %rdx
	movzbl	%al, %r12d
	salq	$5, %r12
	orq	%rdx, %r12
	orq	$5, %r12
.L6078:
	movq	-368(%rbp), %rax
	movl	100(%r13), %ebx
	movl	88(%rax), %esi
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L6081
	movq	-360(%rbp), %rdx
	jmp	.L6082
	.p2align 4,,10
	.p2align 3
.L6217:
	jne	.L6085
	cmpl	36(%rax), %esi
	jg	.L6084
.L6085:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L6083
.L6082:
	cmpl	32(%rax), %ebx
	jle	.L6217
.L6084:
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L6082
.L6083:
	cmpq	-360(%rbp), %rdx
	je	.L6081
	cmpl	32(%rdx), %ebx
	jge	.L6218
.L6081:
	movq	-400(%rbp), %rax
	movq	(%rax), %rax
	testb	$4, 488(%rax)
	jne	.L6219
.L6126:
	movq	16(%rax), %rsi
	movslq	112(%r13), %rdx
	movq	208(%rsi), %rcx
	movq	%rcx, %rax
	subq	216(%rsi), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	js	.L6088
	cmpq	$63, %rax
	jg	.L6089
	leaq	(%rcx,%rdx,8), %rax
	movq	(%rax), %rdx
	movq	8(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L6220
.L6092:
	movq	%r12, %rax
	testb	$4, %r12b
	je	.L6095
	xorl	%edx, %edx
	testb	$24, %r12b
	jne	.L6096
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
.L6096:
	movq	%r12, %rax
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L6095:
	cmpq	%rax, -416(%rbp)
	je	.L6097
	movq	(%rcx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L6221
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L6099:
	movq	%r12, %xmm0
	movhps	-432(%rbp), %xmm0
	movups	%xmm0, (%rax)
	movq	%rax, -256(%rbp)
	movq	16(%rcx), %rsi
	cmpq	8(%rcx), %rsi
	movq	24(%rcx), %rax
	je	.L6100
.L6101:
	cmpq	%rax, %rsi
	je	.L6102
	movq	-256(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%rcx)
.L6097:
	movq	-368(%rbp), %rax
	movq	-136(%rbp), %rdx
	movl	88(%rax), %ecx
	testq	%rdx, %rdx
	jne	.L6104
	jmp	.L6222
	.p2align 4,,10
	.p2align 3
.L6223:
	jne	.L6107
	cmpl	36(%rdx), %ecx
	jl	.L6106
.L6107:
	movq	24(%rdx), %rax
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L6105
.L6224:
	movq	%rax, %rdx
.L6104:
	movl	32(%rdx), %esi
	cmpl	%esi, %ebx
	jge	.L6223
.L6106:
	movq	16(%rdx), %rax
	movl	$1, %edi
	testq	%rax, %rax
	jne	.L6224
.L6105:
	testb	%dil, %dil
	jne	.L6103
	movq	%rdx, %rdi
	cmpl	%esi, %ebx
	jg	.L6128
.L6129:
	cmpl	%esi, %ebx
	jne	.L6111
	cmpl	36(%rdx), %ecx
	jle	.L6111
	testq	%rdi, %rdi
	jne	.L6225
.L6111:
	addq	$4, %r9
	movb	$1, 124(%r13)
	cmpq	%r9, %r15
	jne	.L6226
.L6203:
	movq	%r14, %r13
	movq	-392(%rbp), %r14
	jmp	.L6069
.L6218:
	jne	.L6074
	cmpl	36(%rdx), %esi
	jl	.L6081
	jmp	.L6074
.L6077:
	movq	32(%rcx), %rdx
	movl	4(%rdx), %eax
	movq	104(%rdx), %rcx
	movl	%eax, %edx
	shrl	$5, %edx
	andl	$3, %edx
	cmpl	$1, %edx
	je	.L6227
	shrl	$13, %eax
	movzbl	%al, %r12d
	movslq	44(%rcx), %rax
	salq	$5, %r12
	salq	$35, %rax
	orq	%rax, %r12
	orq	$13, %r12
	jmp	.L6078
.L6072:
	movq	-408(%rbp), %rsi
	leaq	-352(%rbp), %rdi
	movq	%r9, -424(%rbp)
	call	_ZNSt5dequeIiN2v88internal22RecyclingZoneAllocatorIiEEE16_M_push_back_auxIJiEEEvDpOT_
	movq	-424(%rbp), %r9
	jmp	.L6074
.L6225:
	movq	%rdi, %rdx
.L6128:
	movl	$1, %r12d
	cmpq	-360(%rbp), %rdx
	jne	.L6228
.L6112:
	movq	-160(%rbp), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$39, %rax
	jbe	.L6229
	leaq	40(%rsi), %rax
	movq	%rax, 16(%rdi)
.L6114:
	movl	%ecx, 36(%rsi)
	movq	-360(%rbp), %rcx
	movl	%r12d, %edi
	movl	%ebx, 32(%rsi)
	movq	%r9, -424(%rbp)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -112(%rbp)
	movq	-424(%rbp), %r9
	jmp	.L6111
.L6222:
	movq	-360(%rbp), %rdx
.L6103:
	cmpq	-128(%rbp), %rdx
	je	.L6128
	movq	%rdx, %rdi
	movq	%r9, -456(%rbp)
	movl	%ecx, -448(%rbp)
	movq	%rdx, -424(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-424(%rbp), %rdx
	movl	-448(%rbp), %ecx
	movl	32(%rax), %esi
	movq	-456(%rbp), %r9
	cmpl	%esi, %ebx
	jg	.L6128
	movq	%rdx, %rdi
	movq	%rax, %rdx
	jmp	.L6129
.L6089:
	movq	%rax, %rdx
	sarq	$6, %rdx
.L6091:
	movq	232(%rsi), %rdi
	movq	%rdx, %rcx
	salq	$6, %rcx
	movq	(%rdi,%rdx,8), %rdx
	subq	%rcx, %rax
	leaq	(%rdx,%rax,8), %rax
	movq	(%rax), %rdx
	movq	8(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L6092
.L6220:
	movq	8(%rsi), %rdi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	$31, %rax
	jbe	.L6230
	leaq	32(%rcx), %rax
	movq	%rax, 16(%rdi)
.L6094:
	movq	%rdi, (%rcx)
	movq	$0, 8(%rcx)
	movq	$0, 16(%rcx)
	movq	$0, 24(%rcx)
	movq	%rcx, 8(%rdx)
	jmp	.L6092
.L6227:
	movq	(%rcx), %r12
	jmp	.L6078
.L6219:
	xorl	%eax, %eax
	movl	%ebx, %edx
	leaq	.LC121(%rip), %rdi
	movq	%r9, -424(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-400(%rbp), %rax
	movq	-424(%rbp), %r9
	movq	(%rax), %rax
	jmp	.L6126
.L6088:
	movq	%rax, %rdx
	notq	%rdx
	shrq	$6, %rdx
	notq	%rdx
	jmp	.L6091
.L6201:
	movq	-136(%rbp), %r12
	movq	-440(%rbp), %r15
	testq	%r12, %r12
	je	.L6115
.L6118:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L6116
.L6117:
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeISt4pairIN2v88internal8compiler9RpoNumberEiES5_St9_IdentityIS5_ESt4lessIS5_ENS2_13ZoneAllocatorIS5_EEE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L6117
.L6116:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L6118
.L6115:
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L6025
	movq	-264(%rbp), %rdi
	movq	-296(%rbp), %rdx
	leaq	8(%rdi), %rcx
	cmpq	%rdx, %rcx
	jbe	.L6120
	movq	-344(%rbp), %rax
.L6123:
	testq	%rax, %rax
	je	.L6121
	cmpq	$128, 8(%rax)
	ja	.L6122
.L6121:
	movq	(%rdx), %rax
	movq	$128, 8(%rax)
	movq	-344(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -344(%rbp)
.L6122:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L6123
	movq	-336(%rbp), %rax
.L6120:
	movq	-328(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L6025
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L6025:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6231
	addq	$424, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6212:
	.cfi_restore_state
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L6057
	cmpq	$128, 8(%rax)
	ja	.L6058
.L6057:
	movq	-312(%rbp), %rax
	movq	$128, 8(%rax)
	movq	-344(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -344(%rbp)
.L6058:
	movq	-296(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -296(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -312(%rbp)
	movq	%rdx, -304(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L6056
.L6061:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L6063
	cmpq	$128, 8(%rax)
	ja	.L6064
.L6063:
	movq	-312(%rbp), %rax
	movq	$128, 8(%rax)
	movq	-344(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -344(%rbp)
.L6064:
	movq	-296(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -296(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -312(%rbp)
	movq	%rdx, -304(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L6062
.L6208:
	movdqa	-256(%rbp), %xmm5
	movdqa	-224(%rbp), %xmm6
	movq	$0, -336(%rbp)
	leaq	-160(%rbp), %r15
	movq	-232(%rbp), %rax
	movdqa	-208(%rbp), %xmm7
	movaps	%xmm5, -352(%rbp)
	movdqa	-192(%rbp), %xmm5
	movaps	%xmm6, -320(%rbp)
	movdqa	-176(%rbp), %xmm6
	movq	%rax, -328(%rbp)
	movaps	%xmm7, -304(%rbp)
	movaps	%xmm5, -288(%rbp)
	movaps	%xmm6, -272(%rbp)
	jmp	.L6036
.L6102:
	movq	-408(%rbp), %rdx
	movq	%rcx, %rdi
	movq	%r9, -424(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-424(%rbp), %r9
	jmp	.L6097
.L6228:
	cmpl	32(%rdx), %ebx
	jl	.L6112
	movl	$0, %r12d
	jne	.L6112
	xorl	%r12d, %r12d
	cmpl	36(%rdx), %ecx
	setl	%r12b
	jmp	.L6112
.L6100:
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	$31, %rdx
	ja	.L6101
	movq	(%rcx), %rdi
	movl	$32, %esi
	movq	%r9, -448(%rbp)
	movq	%rcx, -424(%rbp)
	call	_ZN2v88internal4Zone3NewEm
	movq	-424(%rbp), %rcx
	movq	-448(%rbp), %r9
	movq	%rax, %xmm0
	movq	%rax, %rsi
	leaq	32(%rax), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 24(%rcx)
	movups	%xmm0, 8(%rcx)
	jmp	.L6101
.L6207:
	movl	88(%rsi), %esi
	leaq	.LC120(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L6026
.L6221:
	movl	$16, %esi
	movq	%r9, -448(%rbp)
	movq	%rcx, -424(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-424(%rbp), %rcx
	movq	-448(%rbp), %r9
	jmp	.L6099
.L6210:
	movl	$1, -380(%rbp)
	xorl	%r11d, %r11d
	jmp	.L6045
.L6229:
	movl	$40, %esi
	movq	%r9, -456(%rbp)
	movq	%rdx, -448(%rbp)
	movl	%ecx, -424(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-424(%rbp), %ecx
	movq	-448(%rbp), %rdx
	movq	-456(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L6114
.L6230:
	movl	$32, %esi
	movq	%r9, -456(%rbp)
	movq	%rdx, -448(%rbp)
	movq	%rdi, -424(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-424(%rbp), %rdi
	movq	-448(%rbp), %rdx
	movq	-456(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L6094
.L6231:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L6211:
	movq	-368(%rbp), %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r11
	jmp	.L6051
.L6213:
	movq	%r8, %rdx
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L6214:
	movq	%rax, %rdx
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE23658:
	.size	_ZN2v88internal8compiler18LiveRangeConnector28CommitSpillsInDeferredBlocksEPNS1_17TopLevelLiveRangeEPNS1_19LiveRangeBoundArrayEPNS0_4ZoneE, .-_ZN2v88internal8compiler18LiveRangeConnector28CommitSpillsInDeferredBlocksEPNS1_17TopLevelLiveRangeEPNS1_19LiveRangeBoundArrayEPNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler18LiveRangeConnector18ResolveControlFlowEPNS0_4ZoneE.str1.8,"aMS",@progbits,1
	.align 8
.LC122:
	.string	"Adding B%d to list of spill blocks for %d\n"
	.section	.text._ZN2v88internal8compiler18LiveRangeConnector18ResolveControlFlowEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18LiveRangeConnector18ResolveControlFlowEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler18LiveRangeConnector18ResolveControlFlowEPNS0_4ZoneE, @function
_ZN2v88internal8compiler18LiveRangeConnector18ResolveControlFlowEPNS0_4ZoneE:
.LFB23630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%rsi, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	24(%rcx), %rdi
	movq	176(%rax), %rbx
	subq	168(%rax), %rbx
	movq	%rdi, %rdx
	movq	%rdi, -128(%rbp)
	movq	%rax, -184(%rbp)
	sarq	$3, %rbx
	movq	16(%rcx), %rax
	movq	%rcx, %rdi
	movslq	%ebx, %rsi
	salq	$4, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L6394
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx)
.L6234:
	movq	%rax, -144(%rbp)
	testl	%ebx, %ebx
	jle	.L6238
	leal	-1(%rbx), %edx
	addq	$1, %rdx
	salq	$4, %rdx
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L6239:
	movq	$0, (%rax)
	addq	$16, %rax
	movq	$0, -8(%rax)
	cmpq	%rax, %rdx
	jne	.L6239
.L6238:
	movq	-120(%rbp), %rax
	leaq	-96(%rbp), %rbx
	movq	%rbx, -136(%rbp)
	leaq	-104(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rbx, -168(%rbp)
	movq	%rax, -200(%rbp)
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rdi
	movq	8(%rax), %rax
	movq	%rdi, -192(%rbp)
	movq	%rax, -160(%rbp)
	cmpq	%rdi, %rax
	jne	.L6241
	jmp	.L6395
	.p2align 4,,10
	.p2align 3
.L6397:
	movl	(%rcx), %eax
	addl	$1, %eax
	cmpl	%edx, %eax
	jne	.L6240
.L6246:
	addq	$8, -160(%rbp)
	movq	-160(%rbp), %rax
	cmpq	%rax, -192(%rbp)
	je	.L6396
.L6241:
	movq	-160(%rbp), %rax
	movq	(%rax), %r8
	movq	40(%r8), %rcx
	movq	48(%r8), %rax
	movslq	100(%r8), %rdx
	subq	%rcx, %rax
	cmpq	$4, %rax
	je	.L6397
.L6240:
	movq	-200(%rbp), %rax
	movq	104(%rax), %rax
	movq	(%rax,%rdx,8), %rax
	movl	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	cmpl	$1, 4(%rax)
	movq	8(%rax), %rax
	je	.L6244
	movq	(%rax), %rax
.L6244:
	movq	-136(%rbp), %rdi
	movq	%rax, -80(%rbp)
	movq	%r8, -128(%rbp)
	movl	$-1, -72(%rbp)
	call	_ZN2v88internal9BitVector8Iterator7AdvanceEv@PLT
	movq	-96(%rbp), %rax
	movl	4(%rax), %eax
	cmpl	%eax, -88(%rbp)
	jge	.L6246
	movq	-128(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L6245:
	movq	-120(%rbp), %rax
	movq	%r8, -128(%rbp)
	movq	(%rax), %rax
	movq	496(%rax), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movslq	-72(%rbp), %rax
	movq	-128(%rbp), %r8
	movq	%rax, %r14
	salq	$4, %r14
	addq	-144(%rbp), %r14
	cmpq	$0, 8(%r14)
	je	.L6247
.L6252:
	movq	48(%r8), %rax
	movq	40(%r8), %r13
	movq	%rax, -128(%rbp)
	cmpq	%rax, %r13
	je	.L6249
	.p2align 4,,10
	.p2align 3
.L6311:
	movq	-120(%rbp), %rax
	movslq	0(%r13), %rsi
	movq	(%rax), %r11
	movq	16(%r11), %rdi
	movq	16(%rdi), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L6398
	movq	(%rax,%rsi,8), %r15
	movq	(%r14), %rdx
	xorl	%r10d, %r10d
	movq	8(%r14), %r9
	movl	116(%r15), %eax
	movq	%rdx, %rcx
	leal	-2(,%rax,4), %r12d
	.p2align 4,,10
	.p2align 3
.L6257:
	movq	%rcx, %rax
	.p2align 4,,10
	.p2align 3
.L6256:
	movq	%rax, %rcx
	subq	%r10, %rax
	shrq	%rax
	addq	%r10, %rax
	leaq	(%rax,%rax,2), %rsi
	leaq	(%r9,%rsi,8), %rsi
	movl	8(%rsi), %ebx
	cmpl	%ebx, %r12d
	jl	.L6256
	movl	12(%rsi), %r10d
	cmpl	%r10d, %r12d
	jl	.L6399
	movq	%rax, %r10
	jmp	.L6257
	.p2align 4,,10
	.p2align 3
.L6399:
	movl	112(%r8), %eax
	sall	$2, %eax
	cmpl	%eax, %r10d
	jle	.L6335
	cmpl	%eax, %ebx
	jle	.L6310
.L6335:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L6261:
	movq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L6260:
	movq	%rcx, %rdx
	subq	%rbx, %rcx
	shrq	%rcx
	addq	%rbx, %rcx
	leaq	(%rcx,%rcx,2), %r10
	leaq	(%r9,%r10,8), %r10
	cmpl	8(%r10), %eax
	jl	.L6260
	cmpl	12(%r10), %eax
	jl	.L6400
	movq	%rcx, %rbx
	jmp	.L6261
	.p2align 4,,10
	.p2align 3
.L6400:
	cmpb	$0, 16(%r10)
	jne	.L6310
	movq	(%rsi), %r9
	movq	(%r10), %rcx
	cmpq	%rcx, %r9
	je	.L6310
	movl	4(%r9), %edx
	movl	%edx, %esi
	shrl	$7, %esi
	andl	$63, %esi
	cmpl	$32, %esi
	je	.L6262
	shrl	$13, %edx
	salq	$35, %rsi
	movzbl	%dl, %ebx
	salq	$5, %rbx
	orq	%rsi, %rbx
	orq	$5, %rbx
.L6263:
	movl	4(%rcx), %edx
	movl	%edx, %esi
	shrl	$7, %esi
	andl	$63, %esi
	cmpl	$32, %esi
	je	.L6266
.L6407:
	shrl	$13, %edx
	salq	$35, %rsi
	movzbl	%dl, %r12d
	salq	$5, %r12
	orq	%rsi, %r12
	orq	$5, %r12
.L6267:
	cmpq	%rbx, %r12
	je	.L6310
	movl	%r12d, %r9d
	movl	%ebx, %r10d
	andl	$4, %r9d
	andl	$4, %r10d
	je	.L6270
	testb	$24, %bl
	je	.L6271
.L6270:
	testl	%r9d, %r9d
	je	.L6271
	testb	$24, %r12b
	jne	.L6271
	movl	116(%r8), %edi
	movq	8(%rcx), %rsi
	leal	0(,%rdi,4), %edx
	cmpl	%edx, 4(%rsi)
	jge	.L6272
	movq	40(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L6273
	testb	$1, 4(%rdx)
	jne	.L6273
.L6272:
	movl	488(%r11), %edx
	movq	32(%rcx), %rax
	testb	$1, %dl
	je	.L6283
	movl	4(%rax), %esi
	xorl	$96, %esi
	andl	$96, %esi
	sete	%sil
.L6284:
	testb	%sil, %sil
	je	.L6393
	cmpb	$0, 120(%r15)
	je	.L6393
	andl	$4, %edx
	movl	100(%r15), %esi
	jne	.L6401
.L6287:
	movq	112(%rax), %rax
	cmpl	$1, 4(%rax)
	movq	8(%rax), %rdx
	je	.L6402
	testl	%esi, %esi
	leal	63(%rsi), %eax
	movl	%esi, %edi
	cmovns	%esi, %eax
	sarl	$31, %edi
	shrl	$26, %edi
	leal	(%rsi,%rdi), %ecx
	sarl	$6, %eax
	movl	$1, %esi
	andl	$63, %ecx
	cltq
	subl	%edi, %ecx
	salq	%cl, %rsi
	orq	%rsi, (%rdx,%rax,8)
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rdi
	.p2align 4,,10
	.p2align 3
.L6271:
	movq	48(%r8), %rax
	subq	40(%r8), %rax
	cmpq	$4, %rax
	je	.L6403
	movl	116(%r15), %ecx
	movl	$1, %edx
	subl	$1, %ecx
.L6290:
	movq	208(%rdi), %rsi
	movslq	%ecx, %rcx
	movq	%rsi, %rax
	subq	216(%rdi), %rax
	sarq	$3, %rax
	addq	%rcx, %rax
	js	.L6291
	cmpq	$63, %rax
	jg	.L6292
	leaq	(%rsi,%rcx,8), %rax
	movq	(%rax), %rax
	leaq	(%rax,%rdx,8), %rdx
	movq	8(%rdx), %r15
	testq	%r15, %r15
	je	.L6404
.L6295:
	testl	%r10d, %r10d
	je	.L6298
	xorl	%eax, %eax
	testb	$24, %bl
	jne	.L6299
	movq	%rbx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L6299:
	movq	%rbx, %rdx
	andq	$-8161, %rdx
	orq	%rax, %rdx
	movq	%r12, %rax
	andq	$-8, %rdx
	orq	$4, %rdx
	testl	%r9d, %r9d
	je	.L6300
.L6324:
	xorl	%ecx, %ecx
	testb	$24, %r12b
	jne	.L6301
	movq	%r12, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L6301:
	movq	%r12, %rax
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L6300:
	cmpq	%rdx, %rax
	je	.L6310
.L6325:
	movq	(%r15), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L6405
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L6304:
	movq	%rbx, %xmm0
	movq	%r12, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r15), %rsi
	cmpq	8(%r15), %rsi
	movq	%rax, -104(%rbp)
	movq	24(%r15), %rax
	je	.L6305
.L6306:
	cmpq	%rax, %rsi
	je	.L6309
.L6411:
	movq	-104(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%r15)
.L6310:
	addq	$4, %r13
	cmpq	%r13, -128(%rbp)
	jne	.L6311
.L6249:
	movq	-136(%rbp), %rdi
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal9BitVector8Iterator7AdvanceEv@PLT
	movq	-96(%rbp), %rax
	movl	-88(%rbp), %edi
	movq	-128(%rbp), %r8
	cmpl	%edi, 4(%rax)
	jg	.L6245
	jmp	.L6246
	.p2align 4,,10
	.p2align 3
.L6298:
	testl	%r9d, %r9d
	je	.L6325
	movq	%rbx, %rdx
	jmp	.L6324
	.p2align 4,,10
	.p2align 3
.L6262:
	movq	32(%r9), %rdx
	movl	4(%rdx), %ebx
	movq	104(%rdx), %rdx
	movl	%ebx, %esi
	shrl	$5, %esi
	andl	$3, %esi
	cmpl	$1, %esi
	je	.L6406
	movslq	44(%rdx), %rdx
	shrl	$13, %ebx
	movzbl	%bl, %ebx
	salq	$35, %rdx
	salq	$5, %rbx
	orq	%rdx, %rbx
	movl	4(%rcx), %edx
	orq	$13, %rbx
	movl	%edx, %esi
	shrl	$7, %esi
	andl	$63, %esi
	cmpl	$32, %esi
	jne	.L6407
.L6266:
	movq	32(%rcx), %rsi
	movl	4(%rsi), %edx
	movq	104(%rsi), %rsi
	movl	%edx, %r9d
	shrl	$5, %r9d
	andl	$3, %r9d
	cmpl	$1, %r9d
	je	.L6408
	shrl	$13, %edx
	movzbl	%dl, %r12d
	movslq	44(%rsi), %rdx
	salq	$5, %r12
	salq	$35, %rdx
	orq	%rdx, %r12
	orq	$13, %r12
	jmp	.L6267
	.p2align 4,,10
	.p2align 3
.L6292:
	movq	%rax, %rcx
	sarq	$6, %rcx
.L6294:
	movq	232(%rdi), %r11
	movq	%rcx, %rsi
	salq	$6, %rsi
	movq	(%r11,%rcx,8), %rcx
	subq	%rsi, %rax
	leaq	(%rcx,%rax,8), %rax
	movq	(%rax), %rax
	leaq	(%rax,%rdx,8), %rdx
	movq	8(%rdx), %r15
	testq	%r15, %r15
	jne	.L6295
.L6404:
	movq	8(%rdi), %rcx
	movq	16(%rcx), %r15
	movq	24(%rcx), %rax
	subq	%r15, %rax
	cmpq	$31, %rax
	jbe	.L6409
	leaq	32(%r15), %rax
	movq	%rax, 16(%rcx)
.L6297:
	movq	%rcx, (%r15)
	movq	$0, 8(%r15)
	movq	$0, 16(%r15)
	movq	$0, 24(%r15)
	movq	%r15, 8(%rdx)
	jmp	.L6295
	.p2align 4,,10
	.p2align 3
.L6403:
	movl	112(%r8), %ecx
	xorl	%edx, %edx
	jmp	.L6290
	.p2align 4,,10
	.p2align 3
.L6291:
	movq	%rax, %rcx
	notq	%rcx
	shrq	$6, %rcx
	notq	%rcx
	jmp	.L6294
	.p2align 4,,10
	.p2align 3
.L6408:
	movq	(%rsi), %r12
	jmp	.L6267
	.p2align 4,,10
	.p2align 3
.L6406:
	movq	(%rdx), %rbx
	jmp	.L6263
	.p2align 4,,10
	.p2align 3
.L6393:
	movq	16(%r11), %rdi
	jmp	.L6271
	.p2align 4,,10
	.p2align 3
.L6305:
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	$31, %rdx
	ja	.L6306
	movq	(%r15), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$31, %rax
	jbe	.L6410
	leaq	32(%rsi), %rax
	movq	%rax, 16(%rdi)
.L6308:
	movq	%rsi, 8(%r15)
	movq	%rsi, 16(%r15)
	movq	%rax, 24(%r15)
	cmpq	%rax, %rsi
	jne	.L6411
	.p2align 4,,10
	.p2align 3
.L6309:
	movq	-168(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r8, -152(%rbp)
	addq	$4, %r13
	call	_ZNSt6vectorIPN2v88internal8compiler12MoveOperandsENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-152(%rbp), %r8
	cmpq	%r13, -128(%rbp)
	jne	.L6311
	jmp	.L6249
	.p2align 4,,10
	.p2align 3
.L6283:
	movzbl	120(%rax), %esi
	jmp	.L6284
	.p2align 4,,10
	.p2align 3
.L6405:
	movl	$16, %esi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-152(%rbp), %r8
	jmp	.L6304
	.p2align 4,,10
	.p2align 3
.L6247:
	movq	-184(%rbp), %rdi
	movq	168(%rdi), %rdx
	movq	-176(%rbp), %rdi
	movq	(%rdx,%rax,8), %rbx
	movl	92(%rbx), %eax
	addl	$1, %eax
	cltq
	leaq	(%rax,%rax,2), %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdi
	salq	$3, %rsi
	movq	%rdi, %rdx
	movq	%rdi, -128(%rbp)
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L6412
	movq	-176(%rbp), %rdi
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L6251:
	movq	%rax, 8(%r14)
	movq	$0, (%r14)
	.p2align 4,,10
	.p2align 3
.L6253:
	movq	16(%rbx), %rcx
	movzbl	4(%rbx), %edx
	movq	%rbx, (%rax)
	addq	$24, %rax
	movl	(%rcx), %ecx
	andl	$1, %edx
	movb	%dl, -8(%rax)
	movl	%ecx, -16(%rax)
	movq	8(%rbx), %rcx
	movl	4(%rcx), %ecx
	movl	%ecx, -12(%rax)
	movq	40(%rbx), %rbx
	addq	$1, (%r14)
	testq	%rbx, %rbx
	jne	.L6253
	jmp	.L6252
	.p2align 4,,10
	.p2align 3
.L6273:
	movq	56(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L6274
	movl	24(%rdx), %esi
	cmpl	%eax, %esi
	jle	.L6280
.L6274:
	movq	24(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L6392
	jmp	.L6279
	.p2align 4,,10
	.p2align 3
.L6413:
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L6279
.L6392:
	movl	24(%rdx), %esi
.L6280:
	cmpl	%esi, %eax
	jg	.L6413
	movq	%rdx, 56(%rcx)
.L6282:
	movq	(%rdx), %rax
	movq	(%rax), %rax
	testb	$4, %al
	je	.L6281
	testb	$24, %al
	jne	.L6281
	movq	-120(%rbp), %rax
	movq	(%rax), %r11
	jmp	.L6272
	.p2align 4,,10
	.p2align 3
.L6281:
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L6282
	addq	$4, %r13
	cmpq	%r13, -128(%rbp)
	jne	.L6311
	jmp	.L6249
	.p2align 4,,10
	.p2align 3
.L6409:
	movq	%rcx, %rdi
	movl	$32, %esi
	movq	%r8, -232(%rbp)
	movl	%r9d, -224(%rbp)
	movl	%r10d, -212(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%rcx, -152(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-152(%rbp), %rcx
	movq	-208(%rbp), %rdx
	movl	-212(%rbp), %r10d
	movl	-224(%rbp), %r9d
	movq	%rax, %r15
	movq	-232(%rbp), %r8
	jmp	.L6297
.L6402:
	btsq	%rsi, %rdx
	movq	%rdx, 8(%rax)
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rdi
	jmp	.L6271
.L6401:
	movl	88(%rax), %edx
	leaq	.LC122(%rip), %rdi
	xorl	%eax, %eax
	movq	%r8, -224(%rbp)
	movl	%r9d, -212(%rbp)
	movl	%r10d, -208(%rbp)
	movq	%rcx, -152(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-152(%rbp), %rcx
	movl	100(%r15), %esi
	movq	-224(%rbp), %r8
	movl	-212(%rbp), %r9d
	movq	32(%rcx), %rax
	movl	-208(%rbp), %r10d
	jmp	.L6287
.L6396:
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -200(%rbp)
.L6237:
	movq	176(%rax), %r13
	movq	168(%rax), %rbx
	movq	%r13, %r14
	subq	%rbx, %r14
	cmpq	%r13, %rbx
	je	.L6232
	movq	(%rbx), %r12
	movq	%r14, %r15
	movq	-176(%rbp), %rcx
	movq	%r13, %r14
	movq	%rax, %rdx
	movq	-120(%rbp), %r13
	addq	$8, %rbx
	jmp	.L6313
.L6415:
	movl	4(%r12), %eax
	xorl	$96, %eax
	testb	$96, %al
	sete	%al
.L6316:
	testb	%al, %al
	je	.L6314
	movslq	88(%r12), %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	-144(%rbp), %rdx
	cmpq	$0, 8(%rdx)
	je	.L6317
.L6320:
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal8compiler18LiveRangeConnector28CommitSpillsInDeferredBlocksEPNS1_17TopLevelLiveRangeEPNS1_19LiveRangeBoundArrayEPNS0_4ZoneE
	movq	-120(%rbp), %rcx
.L6314:
	cmpq	%rbx, %r14
	je	.L6232
	movq	0(%r13), %rdx
	movq	(%rbx), %r12
	addq	$8, %rbx
	movq	176(%rdx), %rax
	subq	168(%rdx), %rax
	cmpq	%rax, %r15
	jne	.L6414
.L6313:
	testq	%r12, %r12
	je	.L6314
	cmpq	$0, 16(%r12)
	je	.L6314
	testb	$1, 488(%rdx)
	jne	.L6415
	movzbl	120(%r12), %eax
	jmp	.L6316
.L6412:
	movq	-176(%rbp), %rdi
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %r8
	jmp	.L6251
.L6414:
	leaq	.LC63(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L6410:
	movl	$32, %esi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-152(%rbp), %r8
	movq	%rax, %rsi
	leaq	32(%rax), %rax
	jmp	.L6308
.L6232:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6416
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6279:
	.cfi_restore_state
	movq	$0, 56(%rcx)
	addq	$4, %r13
	cmpq	%r13, -128(%rbp)
	jne	.L6311
	jmp	.L6249
.L6317:
	movq	-184(%rbp), %rdi
	movq	168(%rdi), %rsi
	movq	24(%rcx), %rdi
	movq	(%rsi,%rax,8), %r9
	movl	92(%r9), %eax
	addl	$1, %eax
	cltq
	leaq	(%rax,%rax,2), %rsi
	movq	16(%rcx), %rax
	salq	$3, %rsi
	subq	%rax, %rdi
	cmpq	%rdi, %rsi
	ja	.L6417
	addq	%rax, %rsi
	movq	%rsi, 16(%rcx)
.L6319:
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L6321:
	movq	16(%r9), %rdi
	movzbl	4(%r9), %esi
	movq	%r9, (%rax)
	addq	$24, %rax
	movl	(%rdi), %edi
	andl	$1, %esi
	movb	%sil, -8(%rax)
	movl	%edi, -16(%rax)
	movq	8(%r9), %rdi
	movl	4(%rdi), %edi
	movl	%edi, -12(%rax)
	movq	40(%r9), %r9
	addq	$1, (%rdx)
	testq	%r9, %r9
	jne	.L6321
	jmp	.L6320
.L6398:
	leaq	.LC38(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L6395:
	movq	-200(%rbp), %rax
	jmp	.L6237
.L6394:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L6234
.L6417:
	movq	%rcx, %rdi
	movq	%r9, -136(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movq	-136(%rbp), %r9
	jmp	.L6319
.L6416:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23630:
	.size	_ZN2v88internal8compiler18LiveRangeConnector18ResolveControlFlowEPNS0_4ZoneE, .-_ZN2v88internal8compiler18LiveRangeConnector18ResolveControlFlowEPNS0_4ZoneE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler11UsePositionC2ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler11UsePositionC2ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler11UsePositionC2ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE:
.LFB31234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE31234:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler11UsePositionC2ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE, .-_GLOBAL__sub_I__ZN2v88internal8compiler11UsePositionC2ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler11UsePositionC2ENS1_16LifetimePositionEPNS1_18InstructionOperandEPvNS1_19UsePositionHintTypeE
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.weak	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC123:
	.string	"xmm0"
.LC124:
	.string	"xmm1"
.LC125:
	.string	"xmm2"
.LC126:
	.string	"xmm3"
.LC127:
	.string	"xmm4"
.LC128:
	.string	"xmm5"
.LC129:
	.string	"xmm6"
.LC130:
	.string	"xmm7"
.LC131:
	.string	"xmm8"
.LC132:
	.string	"xmm9"
.LC133:
	.string	"xmm10"
.LC134:
	.string	"xmm11"
.LC135:
	.string	"xmm12"
.LC136:
	.string	"xmm13"
.LC137:
	.string	"xmm14"
.LC138:
	.string	"xmm15"
	.section	.data.rel.ro.local._ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names,"awG",@progbits,_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names,comdat
	.align 32
	.type	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names, @gnu_unique_object
	.size	_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names, 128
_ZZN2v88internal12RegisterNameENS0_11XMMRegisterEE5Names:
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.quad	.LC126
	.quad	.LC127
	.quad	.LC128
	.quad	.LC129
	.quad	.LC130
	.quad	.LC131
	.quad	.LC132
	.quad	.LC133
	.quad	.LC134
	.quad	.LC135
	.quad	.LC136
	.quad	.LC137
	.quad	.LC138
	.weak	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names
	.section	.rodata.str1.1
.LC139:
	.string	"rax"
.LC140:
	.string	"rcx"
.LC141:
	.string	"rdx"
.LC142:
	.string	"rbx"
.LC143:
	.string	"rsp"
.LC144:
	.string	"rbp"
.LC145:
	.string	"rsi"
.LC146:
	.string	"rdi"
.LC147:
	.string	"r8"
.LC148:
	.string	"r9"
.LC149:
	.string	"r10"
.LC150:
	.string	"r11"
.LC151:
	.string	"r12"
.LC152:
	.string	"r13"
.LC153:
	.string	"r14"
.LC154:
	.string	"r15"
	.section	.data.rel.ro.local._ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names,"awG",@progbits,_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names,comdat
	.align 32
	.type	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names, @gnu_unique_object
	.size	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names, 128
_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names:
	.quad	.LC139
	.quad	.LC140
	.quad	.LC141
	.quad	.LC142
	.quad	.LC143
	.quad	.LC144
	.quad	.LC145
	.quad	.LC146
	.quad	.LC147
	.quad	.LC148
	.quad	.LC149
	.quad	.LC150
	.quad	.LC151
	.quad	.LC152
	.quad	.LC153
	.quad	.LC154
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC57:
	.long	2147483647
	.long	2147483647
	.long	2147483647
	.long	2147483647
	.section	.data.rel.ro.local,"aw"
	.align 8
.LC83:
	.quad	_ZNSt14_Function_base13_Base_managerIZZN2v88internal8compiler19LinearScanAllocator25UpdateDeferredFixedRangesENS3_22RegisterAllocationData9SpillModeEPNS3_16InstructionBlockEENKUlPNS3_9LiveRangeEE_clESA_EUlSA_E0_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
