	.file	"node-cache.cc"
	.text
	.section	.text._ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEEC2Ej,"axG",@progbits,_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEEC5Ej,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEEC2Ej
	.type	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEEC2Ej, @function
_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEEC2Ej:
.LFB5806:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	movq	%rax, 16(%rdi)
	ret
	.cfi_endproc
.LFE5806:
	.size	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEEC2Ej, .-_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEEC2Ej
	.weak	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEEC1Ej
	.set	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEEC1Ej,_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEEC2Ej
	.section	.text._ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE6ResizeEPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE6ResizeEPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE6ResizeEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE6ResizeEPNS0_4ZoneE, @function
_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE6ResizeEPNS0_4ZoneE:
.LFB5810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	cmpq	16(%rdi), %r13
	jb	.L16
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	%rsi, %rdi
	leaq	0(,%r13,4), %rdx
	movq	(%r12), %rbx
	movq	%rdx, 8(%r12)
	movq	16(%rdi), %r8
	addq	$5, %rdx
	movq	24(%rdi), %rax
	salq	$4, %rdx
	movq	%rdx, %rsi
	subq	%r8, %rax
	cmpq	%rax, %rdx
	ja	.L17
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L6:
	movq	%r8, (%r12)
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	memset@PLT
	cmpq	$-5, %r13
	je	.L11
	salq	$4, %r13
	leaq	80(%rbx,%r13), %r13
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L10:
	addq	$16, %rbx
	cmpq	%r13, %rbx
	je	.L11
.L12:
	cmpq	$0, 8(%rbx)
	je	.L10
	movl	(%rbx), %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movq	%rax, %r8
	movq	8(%r12), %rax
	subq	$1, %rax
	andq	%r8, %rax
	cmpq	$-6, %rax
	ja	.L10
	movq	(%r12), %rcx
	salq	$4, %rax
	leaq	(%rcx,%rax), %rdx
	cmpq	$0, 8(%rdx)
	je	.L9
	leaq	16(%rcx,%rax), %rdx
	cmpq	$0, 8(%rdx)
	je	.L9
	leaq	32(%rcx,%rax), %rdx
	cmpq	$0, 8(%rdx)
	je	.L9
	leaq	48(%rcx,%rax), %rdx
	cmpq	$0, 8(%rdx)
	je	.L9
	leaq	64(%rcx,%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L10
	.p2align 4,,10
	.p2align 3
.L9:
	movl	(%rbx), %eax
	addq	$16, %rbx
	movl	%eax, (%rdx)
	movq	-8(%rbx), %rax
	movq	%rax, 8(%rdx)
	cmpq	%r13, %rbx
	jne	.L12
.L11:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L17:
	.cfi_restore_state
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L6
	.cfi_endproc
.LFE5810:
	.size	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE6ResizeEPNS0_4ZoneE, .-_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE6ResizeEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE4FindEPNS0_4ZoneEi,"axG",@progbits,_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE4FindEPNS0_4ZoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE4FindEPNS0_4ZoneEi
	.type	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE4FindEPNS0_4ZoneEi, @function
_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE4FindEPNS0_4ZoneEi:
.LFB5808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	%edx, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	call	_ZN2v84base10hash_valueEj@PLT
	cmpq	$0, (%r12)
	movq	%rax, %r13
	je	.L31
.L19:
	movq	8(%r12), %rax
	leaq	-1(%rax), %rdx
	andq	%r13, %rdx
	cmpq	$-6, %rdx
	ja	.L23
	movq	(%r12), %rcx
	salq	$4, %rdx
	leaq	(%rcx,%rdx), %rax
	cmpl	%ebx, (%rax)
	je	.L30
	cmpq	$0, 8(%rax)
	je	.L25
	leaq	16(%rcx,%rdx), %rax
	cmpl	(%rax), %ebx
	je	.L30
	cmpq	$0, 8(%rax)
	je	.L25
	leaq	32(%rcx,%rdx), %rax
	cmpl	(%rax), %ebx
	je	.L30
	cmpq	$0, 8(%rax)
	je	.L25
	leaq	48(%rcx,%rdx), %rax
	cmpl	(%rax), %ebx
	je	.L30
	cmpq	$0, 8(%rax)
	je	.L25
	leaq	64(%rcx,%rdx), %rax
	cmpl	%ebx, (%rax)
	je	.L30
	cmpq	$0, 8(%rax)
	je	.L25
.L23:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE6ResizeEPNS0_4ZoneE
	testb	%al, %al
	jne	.L19
	movq	8(%r12), %rax
	subq	$1, %rax
	andq	%r13, %rax
	salq	$4, %rax
	addq	(%r12), %rax
	movl	%ebx, (%rax)
	movq	$0, 8(%rax)
	.p2align 4,,10
	.p2align 3
.L30:
	addq	$8, %rax
.L18:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movl	%ebx, (%rax)
	addq	$8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L31:
	.cfi_restore_state
	movq	16(%r14), %rax
	movq	24(%r14), %rdx
	subq	%rax, %rdx
	cmpq	$335, %rdx
	jbe	.L32
	leaq	336(%rax), %rdx
	movq	%rdx, 16(%r14)
.L21:
	leaq	8(%rax), %rdi
	movq	%rax, (%r12)
	movq	$16, 8(%r12)
	andq	$-8, %rdi
	movq	$0, (%rax)
	movq	$0, 328(%rax)
	subq	%rdi, %rax
	leal	336(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movq	%r13, %rax
	andl	$15, %eax
	salq	$4, %rax
	addq	(%r12), %rax
	movl	%ebx, (%rax)
	addq	$8, %rax
	jmp	.L18
.L32:
	movl	$336, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L21
	.cfi_endproc
.LFE5808:
	.size	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE4FindEPNS0_4ZoneEi, .-_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE4FindEPNS0_4ZoneEi
	.section	.text._ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEEC2Ej,"axG",@progbits,_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEEC5Ej,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEEC2Ej
	.type	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEEC2Ej, @function
_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEEC2Ej:
.LFB5812:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	movq	%rax, 16(%rdi)
	ret
	.cfi_endproc
.LFE5812:
	.size	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEEC2Ej, .-_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEEC2Ej
	.weak	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEEC1Ej
	.set	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEEC1Ej,_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEEC2Ej
	.section	.text._ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE6ResizeEPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE6ResizeEPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE6ResizeEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE6ResizeEPNS0_4ZoneE, @function
_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE6ResizeEPNS0_4ZoneE:
.LFB5816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	cmpq	16(%rdi), %r13
	jb	.L47
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	%rsi, %rdi
	leaq	0(,%r13,4), %rdx
	movq	(%r12), %rbx
	movq	%rdx, 8(%r12)
	movq	16(%rdi), %r8
	addq	$5, %rdx
	movq	24(%rdi), %rax
	salq	$4, %rdx
	movq	%rdx, %rsi
	subq	%r8, %rax
	cmpq	%rax, %rdx
	ja	.L48
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L37:
	movq	%r8, (%r12)
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	memset@PLT
	cmpq	$-5, %r13
	je	.L42
	salq	$4, %r13
	leaq	80(%rbx,%r13), %r13
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L41:
	addq	$16, %rbx
	cmpq	%r13, %rbx
	je	.L42
.L43:
	cmpq	$0, 8(%rbx)
	je	.L41
	movq	(%rbx), %rdi
	call	_ZN2v84base10hash_valueEm@PLT
	movq	%rax, %r8
	movq	8(%r12), %rax
	subq	$1, %rax
	andq	%r8, %rax
	cmpq	$-6, %rax
	ja	.L41
	movq	(%r12), %rcx
	salq	$4, %rax
	leaq	(%rcx,%rax), %rdx
	cmpq	$0, 8(%rdx)
	je	.L40
	leaq	16(%rcx,%rax), %rdx
	cmpq	$0, 8(%rdx)
	je	.L40
	leaq	32(%rcx,%rax), %rdx
	cmpq	$0, 8(%rdx)
	je	.L40
	leaq	48(%rcx,%rax), %rdx
	cmpq	$0, 8(%rdx)
	je	.L40
	leaq	64(%rcx,%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L41
	.p2align 4,,10
	.p2align 3
.L40:
	movq	(%rbx), %rax
	addq	$16, %rbx
	movq	%rax, (%rdx)
	movq	-8(%rbx), %rax
	movq	%rax, 8(%rdx)
	cmpq	%r13, %rbx
	jne	.L43
.L42:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L48:
	.cfi_restore_state
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L37
	.cfi_endproc
.LFE5816:
	.size	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE6ResizeEPNS0_4ZoneE, .-_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE6ResizeEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl,"axG",@progbits,_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl
	.type	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl, @function
_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl:
.LFB5814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	call	_ZN2v84base10hash_valueEm@PLT
	cmpq	$0, (%r12)
	movq	%rax, %r13
	je	.L62
.L50:
	movq	8(%r12), %rax
	leaq	-1(%rax), %rdx
	andq	%r13, %rdx
	cmpq	$-6, %rdx
	ja	.L54
	movq	(%r12), %rcx
	salq	$4, %rdx
	leaq	(%rcx,%rdx), %rax
	cmpq	%rbx, (%rax)
	je	.L61
	cmpq	$0, 8(%rax)
	je	.L56
	leaq	16(%rcx,%rdx), %rax
	cmpq	(%rax), %rbx
	je	.L61
	cmpq	$0, 8(%rax)
	je	.L56
	leaq	32(%rcx,%rdx), %rax
	cmpq	(%rax), %rbx
	je	.L61
	cmpq	$0, 8(%rax)
	je	.L56
	leaq	48(%rcx,%rdx), %rax
	cmpq	(%rax), %rbx
	je	.L61
	cmpq	$0, 8(%rax)
	je	.L56
	leaq	64(%rcx,%rdx), %rax
	cmpq	%rbx, (%rax)
	je	.L61
	cmpq	$0, 8(%rax)
	je	.L56
.L54:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE6ResizeEPNS0_4ZoneE
	testb	%al, %al
	jne	.L50
	movq	8(%r12), %rax
	subq	$1, %rax
	andq	%r13, %rax
	salq	$4, %rax
	addq	(%r12), %rax
	movq	%rbx, (%rax)
	movq	$0, 8(%rax)
	.p2align 4,,10
	.p2align 3
.L61:
	addq	$8, %rax
.L49:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movq	%rbx, (%rax)
	addq	$8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L62:
	.cfi_restore_state
	movq	16(%r14), %rax
	movq	24(%r14), %rdx
	subq	%rax, %rdx
	cmpq	$335, %rdx
	jbe	.L63
	leaq	336(%rax), %rdx
	movq	%rdx, 16(%r14)
.L52:
	leaq	8(%rax), %rdi
	movq	%rax, (%r12)
	movq	$16, 8(%r12)
	andq	$-8, %rdi
	movq	$0, (%rax)
	movq	$0, 328(%rax)
	subq	%rdi, %rax
	leal	336(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movq	%r13, %rax
	andl	$15, %eax
	salq	$4, %rax
	addq	(%r12), %rax
	movq	%rbx, (%rax)
	addq	$8, %rax
	jmp	.L49
.L63:
	movl	$336, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L52
	.cfi_endproc
.LFE5814:
	.size	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl, .-_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE4FindEPNS0_4ZoneEl
	.section	.text._ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EEC2Ej,"axG",@progbits,_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EEC5Ej,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EEC2Ej
	.type	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EEC2Ej, @function
_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EEC2Ej:
.LFB5818:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	movq	%rax, 16(%rdi)
	ret
	.cfi_endproc
.LFE5818:
	.size	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EEC2Ej, .-_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EEC2Ej
	.weak	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EEC1Ej
	.set	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EEC1Ej,_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EEC2Ej
	.section	.text._ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE6ResizeEPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE6ResizeEPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE6ResizeEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE6ResizeEPNS0_4ZoneE, @function
_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE6ResizeEPNS0_4ZoneE:
.LFB5822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	8(%rdi), %r13
	cmpq	16(%rdi), %r13
	jb	.L78
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movq	%rsi, %rdi
	leaq	0(,%r13,4), %rdx
	movq	(%r12), %rbx
	movq	%rdx, 8(%r12)
	movq	16(%rdi), %r8
	addq	$5, %rdx
	movq	24(%rdi), %rax
	salq	$4, %rdx
	movq	%rdx, %rsi
	subq	%r8, %rax
	cmpq	%rax, %rdx
	ja	.L79
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L68:
	movq	%r8, (%r12)
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	memset@PLT
	cmpq	$-5, %r13
	je	.L73
	salq	$4, %r13
	leaq	80(%rbx,%r13), %r14
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L72:
	addq	$16, %rbx
	cmpq	%r14, %rbx
	je	.L73
.L74:
	cmpq	$0, 8(%rbx)
	je	.L72
	movl	(%rbx), %edi
	call	_ZN2v84base10hash_valueEj@PLT
	movsbl	4(%rbx), %edi
	movq	%rax, %r13
	call	_ZN2v84base10hash_valueEj@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %r8
	movq	8(%r12), %rax
	subq	$1, %rax
	andq	%r8, %rax
	cmpq	$-6, %rax
	ja	.L72
	movq	(%r12), %rcx
	salq	$4, %rax
	leaq	(%rcx,%rax), %rdx
	cmpq	$0, 8(%rdx)
	je	.L71
	leaq	16(%rcx,%rax), %rdx
	cmpq	$0, 8(%rdx)
	je	.L71
	leaq	32(%rcx,%rax), %rdx
	cmpq	$0, 8(%rdx)
	je	.L71
	leaq	48(%rcx,%rax), %rdx
	cmpq	$0, 8(%rdx)
	je	.L71
	leaq	64(%rcx,%rax), %rdx
	cmpq	$0, 8(%rdx)
	jne	.L72
	.p2align 4,,10
	.p2align 3
.L71:
	movl	(%rbx), %eax
	addq	$16, %rbx
	movl	%eax, (%rdx)
	movzbl	-12(%rbx), %eax
	movb	%al, 4(%rdx)
	movq	-8(%rbx), %rax
	movq	%rax, 8(%rdx)
	cmpq	%r14, %rbx
	jne	.L74
.L73:
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L79:
	.cfi_restore_state
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L68
	.cfi_endproc
.LFE5822:
	.size	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE6ResizeEPNS0_4ZoneE, .-_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE6ResizeEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_,"axG",@progbits,_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_
	.type	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_, @function
_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_:
.LFB5820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	%edx, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	salq	$24, %r12
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	sarq	$56, %r12
	subq	$24, %rsp
	movb	%r12b, -56(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	movsbl	%r12b, %edi
	movq	%rax, -64(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-64(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	cmpq	$0, (%r15)
	movq	%rax, %r8
	je	.L97
.L81:
	movq	8(%r15), %rax
	leaq	-1(%rax), %rdx
	andq	%r8, %rdx
	cmpq	$-6, %rdx
	ja	.L85
	movq	(%r15), %rsi
	salq	$4, %rdx
	leaq	(%rsi,%rdx), %rax
	cmpl	%r14d, (%rax)
	je	.L98
.L86:
	cmpq	$0, 8(%rax)
	je	.L88
	leaq	16(%rsi,%rdx), %rax
	cmpl	(%rax), %r14d
	je	.L99
.L89:
	cmpq	$0, 8(%rax)
	je	.L88
	leaq	32(%rsi,%rdx), %rax
	cmpl	(%rax), %r14d
	je	.L100
.L90:
	cmpq	$0, 8(%rax)
	je	.L88
	leaq	48(%rsi,%rdx), %rax
	cmpl	(%rax), %r14d
	je	.L101
.L91:
	cmpq	$0, 8(%rax)
	je	.L88
	leaq	64(%rsi,%rdx), %rax
	cmpl	%r14d, (%rax)
	je	.L102
.L92:
	cmpq	$0, 8(%rax)
	je	.L88
.L85:
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r8, -64(%rbp)
	call	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE6ResizeEPNS0_4ZoneE
	movq	-64(%rbp), %r8
	testb	%al, %al
	jne	.L81
	movq	8(%r15), %rax
	leaq	-1(%rax), %rdx
	movq	%rdx, %rax
	andq	%r8, %rax
	salq	$4, %rax
	addq	(%r15), %rax
	movl	%ebx, (%rax)
	movb	%r12b, 4(%rax)
	movq	$0, 8(%rax)
	.p2align 4,,10
	.p2align 3
.L96:
	addq	$8, %rax
.L80:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movzbl	-56(%rbp), %ecx
	cmpb	4(%rax), %cl
	jne	.L86
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L99:
	movzbl	-56(%rbp), %ecx
	cmpb	4(%rax), %cl
	jne	.L89
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L100:
	movzbl	-56(%rbp), %ecx
	cmpb	4(%rax), %cl
	jne	.L90
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L101:
	movzbl	-56(%rbp), %ecx
	cmpb	4(%rax), %cl
	jne	.L91
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L102:
	movzbl	-56(%rbp), %ecx
	cmpb	4(%rax), %cl
	jne	.L92
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L88:
	movl	%ebx, (%rax)
	addq	$8, %rax
	movb	%r12b, -4(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$335, %rdx
	jbe	.L103
	leaq	336(%rax), %rdx
	movq	%rdx, 16(%r13)
.L83:
	leaq	8(%rax), %rdi
	movq	%rax, (%r15)
	andq	$-8, %rdi
	movq	$16, 8(%r15)
	movq	$0, (%rax)
	movq	$0, 328(%rax)
	subq	%rdi, %rax
	leal	336(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movq	%r8, %rax
	andl	$15, %eax
	salq	$4, %rax
	addq	(%r15), %rax
	movl	%ebx, (%rax)
	addq	$8, %rax
	movb	%r12b, -4(%rax)
	jmp	.L80
.L103:
	movl	$336, %esi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	jmp	.L83
	.cfi_endproc
.LFE5820:
	.size	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_, .-_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_
	.section	.text._ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EEC2Ej,"axG",@progbits,_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EEC5Ej,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EEC2Ej
	.type	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EEC2Ej, @function
_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EEC2Ej:
.LFB5824:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	movq	%rax, 16(%rdi)
	ret
	.cfi_endproc
.LFE5824:
	.size	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EEC2Ej, .-_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EEC2Ej
	.weak	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EEC1Ej
	.set	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EEC1Ej,_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EEC2Ej
	.section	.text._ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE6ResizeEPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE6ResizeEPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE6ResizeEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE6ResizeEPNS0_4ZoneE, @function
_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE6ResizeEPNS0_4ZoneE:
.LFB5828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	8(%rdi), %r13
	cmpq	16(%rdi), %r13
	jb	.L118
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	%rsi, %rdi
	leaq	0(,%r13,4), %rax
	movq	(%r12), %rbx
	movq	%rax, 8(%r12)
	leaq	15(%rax,%rax,2), %rdx
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	salq	$3, %rdx
	movq	%rdx, %rsi
	subq	%r8, %rax
	cmpq	%rax, %rdx
	ja	.L119
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L108:
	movq	%r8, (%r12)
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	memset@PLT
	cmpq	$-5, %r13
	je	.L113
	leaq	0(%r13,%r13,2), %rax
	leaq	120(%rbx,%rax,8), %r13
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L112:
	addq	$24, %rbx
	cmpq	%r13, %rbx
	je	.L113
.L114:
	cmpq	$0, 16(%rbx)
	je	.L112
	movq	(%rbx), %rdi
	call	_ZN2v84base10hash_valueEm@PLT
	movsbl	8(%rbx), %edi
	movq	%rax, %r14
	call	_ZN2v84base10hash_valueEj@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %r8
	movq	8(%r12), %rax
	subq	$1, %rax
	andq	%r8, %rax
	cmpq	$-6, %rax
	ja	.L112
	leaq	(%rax,%rax,2), %rdx
	movq	(%r12), %rcx
	salq	$3, %rdx
	leaq	(%rcx,%rdx), %rax
	cmpq	$0, 16(%rax)
	je	.L111
	leaq	24(%rcx,%rdx), %rax
	cmpq	$0, 16(%rax)
	je	.L111
	leaq	48(%rcx,%rdx), %rax
	cmpq	$0, 16(%rax)
	je	.L111
	leaq	72(%rcx,%rdx), %rax
	cmpq	$0, 16(%rax)
	je	.L111
	leaq	96(%rcx,%rdx), %rax
	cmpq	$0, 16(%rax)
	jne	.L112
	.p2align 4,,10
	.p2align 3
.L111:
	movq	(%rbx), %rdx
	addq	$24, %rbx
	movq	%rdx, (%rax)
	movzbl	-16(%rbx), %edx
	movb	%dl, 8(%rax)
	movq	-8(%rbx), %rdx
	movq	%rdx, 16(%rax)
	cmpq	%r13, %rbx
	jne	.L114
.L113:
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L119:
	.cfi_restore_state
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L108
	.cfi_endproc
.LFE5828:
	.size	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE6ResizeEPNS0_4ZoneE, .-_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE6ResizeEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_,"axG",@progbits,_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_
	.type	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_, @function
_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_:
.LFB5826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rdx, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	call	_ZN2v84base10hash_valueEm@PLT
	movsbl	%bl, %edi
	movq	%rax, -56(%rbp)
	call	_ZN2v84base10hash_valueEj@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	cmpq	$0, (%r15)
	movq	%rax, %rdx
	je	.L137
.L121:
	movq	8(%r15), %rax
	subq	$1, %rax
	andq	%rdx, %rax
	cmpq	$-6, %rax
	ja	.L125
	leaq	(%rax,%rax,2), %rcx
	movq	(%r15), %rsi
	salq	$3, %rcx
	leaq	(%rsi,%rcx), %rax
	cmpq	%r14, (%rax)
	je	.L138
.L126:
	cmpq	$0, 16(%rax)
	je	.L128
	leaq	24(%rsi,%rcx), %rax
	cmpq	(%rax), %r14
	je	.L139
.L129:
	cmpq	$0, 16(%rax)
	je	.L128
	leaq	48(%rsi,%rcx), %rax
	cmpq	(%rax), %r14
	je	.L140
.L130:
	cmpq	$0, 16(%rax)
	je	.L128
	leaq	72(%rsi,%rcx), %rax
	cmpq	(%rax), %r14
	je	.L141
.L131:
	cmpq	$0, 16(%rax)
	je	.L128
	leaq	96(%rsi,%rcx), %rax
	cmpq	%r14, (%rax)
	je	.L142
.L132:
	cmpq	$0, 16(%rax)
	je	.L128
.L125:
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE6ResizeEPNS0_4ZoneE
	movq	-56(%rbp), %rdx
	testb	%al, %al
	jne	.L121
	movq	8(%r15), %rax
	subq	$1, %rax
	andq	%rax, %rdx
	movq	(%r15), %rax
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rax
	movq	%r14, (%rax)
	movb	%bl, 8(%rax)
	movq	$0, 16(%rax)
	.p2align 4,,10
	.p2align 3
.L136:
	addq	$16, %rax
.L120:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	cmpb	8(%rax), %r13b
	jne	.L126
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L139:
	cmpb	8(%rax), %r13b
	jne	.L129
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L140:
	cmpb	8(%rax), %r13b
	jne	.L130
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L141:
	cmpb	8(%rax), %r13b
	jne	.L131
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L142:
	cmpb	8(%rax), %r13b
	jne	.L132
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r14, (%rax)
	addq	$16, %rax
	movb	%bl, -8(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	subq	%rax, %rcx
	cmpq	$503, %rcx
	jbe	.L143
	leaq	504(%rax), %rcx
	movq	%rcx, 16(%r12)
.L123:
	leaq	8(%rax), %rdi
	movq	%rax, (%r15)
	andl	$15, %edx
	andq	$-8, %rdi
	movq	$16, 8(%r15)
	leaq	(%rdx,%rdx,2), %rdx
	movq	$0, (%rax)
	movq	$0, 496(%rax)
	subq	%rdi, %rax
	leal	504(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movq	(%r15), %rax
	leaq	(%rax,%rdx,8), %rax
	movq	%r14, (%rax)
	addq	$16, %rax
	movb	%bl, -8(%rax)
	jmp	.L120
.L143:
	movl	$504, %esi
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	jmp	.L123
	.cfi_endproc
.LFE5826:
	.size	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_, .-_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE4FindEPNS0_4ZoneES4_
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB5999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L182
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L160
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L183
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L146:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L184
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L149:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L183:
	testq	%rdx, %rdx
	jne	.L185
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L147:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L150
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L163
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L163
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L152:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L152
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L154
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L154:
	leaq	16(%rax,%r8), %rcx
.L150:
	cmpq	%r14, %r12
	je	.L155
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L164
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L164
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L157:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L157
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L159
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L159:
	leaq	8(%rcx,%r9), %rcx
.L155:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L164:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L156:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L156
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L163:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L151:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L151
	jmp	.L154
.L184:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L149
.L182:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L185:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L146
	.cfi_endproc
.LFE5999:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE,"axG",@progbits,_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE
	.type	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE, @function
_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE:
.LFB5827:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L201
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rcx
	cmpq	$-5, %rcx
	je	.L186
	movq	%rsi, %r12
	xorl	%ebx, %ebx
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L204:
	movq	0(%r13), %rax
.L190:
	leaq	(%rbx,%rbx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	movq	16(%rdx), %rax
	testq	%rax, %rax
	je	.L188
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L189
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
	movq	8(%r13), %rcx
.L188:
	addq	$1, %rbx
	leaq	5(%rcx), %rax
	cmpq	%rbx, %rax
	ja	.L204
.L186:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	addq	$16, %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	8(%r13), %rcx
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE5827:
	.size	_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE, .-_ZN2v88internal8compiler9NodeCacheISt4pairIlcENS_4base4hashIS4_EESt8equal_toIS4_EE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE
	.section	.text._ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE,"axG",@progbits,_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE
	.type	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE, @function
_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE:
.LFB5809:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L220
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rcx
	cmpq	$-5, %rcx
	je	.L205
	movq	%rsi, %r12
	xorl	%ebx, %ebx
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L223:
	movq	0(%r13), %rdx
.L209:
	movq	%rbx, %rax
	salq	$4, %rax
	addq	%rax, %rdx
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L207
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L208
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
	movq	8(%r13), %rcx
.L207:
	addq	$1, %rbx
	leaq	5(%rcx), %rax
	cmpq	%rbx, %rax
	ja	.L223
.L205:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	addq	$8, %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	8(%r13), %rcx
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE5809:
	.size	_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE, .-_ZN2v88internal8compiler9NodeCacheIiNS_4base4hashIiEESt8equal_toIiEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE
	.section	.text._ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE,"axG",@progbits,_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE
	.type	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE, @function
_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE:
.LFB5815:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L239
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rcx
	cmpq	$-5, %rcx
	je	.L224
	movq	%rsi, %r12
	xorl	%ebx, %ebx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L242:
	movq	0(%r13), %rdx
.L228:
	movq	%rbx, %rax
	salq	$4, %rax
	addq	%rax, %rdx
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L226
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L227
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
	movq	8(%r13), %rcx
.L226:
	addq	$1, %rbx
	leaq	5(%rcx), %rax
	cmpq	%rbx, %rax
	ja	.L242
.L224:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	addq	$8, %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	8(%r13), %rcx
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE5815:
	.size	_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE, .-_ZN2v88internal8compiler9NodeCacheIlNS_4base4hashIlEESt8equal_toIlEE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE
	.section	.text._ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE,"axG",@progbits,_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE
	.type	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE, @function
_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE:
.LFB5821:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L258
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rcx
	cmpq	$-5, %rcx
	je	.L243
	movq	%rsi, %r12
	xorl	%ebx, %ebx
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L261:
	movq	0(%r13), %rdx
.L247:
	movq	%rbx, %rax
	salq	$4, %rax
	addq	%rax, %rdx
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L245
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	je	.L246
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
	movq	8(%r13), %rcx
.L245:
	addq	$1, %rbx
	leaq	5(%rcx), %rax
	cmpq	%rbx, %rax
	ja	.L261
.L243:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore_state
	addq	$8, %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	8(%r13), %rcx
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE5821:
	.size	_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE, .-_ZN2v88internal8compiler9NodeCacheISt4pairIicENS_4base4hashIS4_EESt8equal_toIS4_EE14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
