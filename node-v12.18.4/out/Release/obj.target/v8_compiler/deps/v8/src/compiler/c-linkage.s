	.file	"c-linkage.cc"
	.text
	.section	.rodata._ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"MachineRepresentation::kFloat32 != rep"
	.section	.rodata._ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE.str1.8
	.align 8
.LC2:
	.string	"MachineRepresentation::kFloat64 != rep"
	.section	.rodata._ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE.str1.1
.LC3:
	.string	"2 >= locations.return_count_"
.LC5:
	.string	"c-call"
	.section	.text._ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	.type	_ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE, @function
_ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE:
.LFB22008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	8(%rsi), %r15
	movq	(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	leal	(%r15,%r14), %esi
	movslq	%esi, %rsi
	subq	%r8, %rax
	salq	$3, %rsi
	cmpq	%rax, %rsi
	ja	.L43
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L3:
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L10
	movq	16(%rbx), %rsi
	xorl	%eax, %eax
.L9:
	movzbl	(%rsi,%rax,2), %ecx
	cmpb	$12, %cl
	je	.L11
	cmpb	$13, %cl
	je	.L12
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L9
.L10:
	movq	8(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L6
	movq	16(%rbx), %rax
	leaq	(%rax,%rdx,2), %rsi
	xorl	%eax, %eax
.L13:
	movzbl	(%rsi,%rax,2), %edx
	cmpb	$12, %dl
	je	.L11
	cmpb	$13, %dl
	je	.L12
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L13
.L6:
	cmpq	$2, %r14
	jbe	.L44
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r8
	jmp	.L3
.L44:
	testq	%r14, %r14
	je	.L16
	movq	16(%rbx), %rax
	movzwl	(%rax), %eax
	movl	$0, (%r8)
	movw	%ax, 4(%r8)
	cmpq	$2, %r14
	jne	.L41
	movq	16(%rbx), %rax
	movzwl	2(%rax), %eax
	movl	$4, 8(%r8)
	movw	%ax, 12(%r8)
.L41:
	movq	8(%rbx), %rcx
.L16:
	movdqa	.LC4(%rip), %xmm0
	movl	%ecx, %r9d
	movabsq	$38654705672, %rax
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	testl	%ecx, %ecx
	jle	.L23
	subl	$1, %ecx
	leaq	(%r8,%r14,8), %rsi
	leaq	-80(%rbp), %r10
	movl	$5, %edi
	cmpl	$5, %ecx
	cmovle	%ecx, %edi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L21:
	movq	16(%rbx), %r11
	movq	%rax, %rdx
	addq	$1, %rax
	addq	(%rbx), %rdx
	addq	$8, %rsi
	movzwl	(%r11,%rdx,2), %r11d
	movl	-4(%r10,%rax,4), %edx
	addl	%edx, %edx
	movw	%r11w, -4(%rsi)
	movl	%edx, -8(%rsi)
	movslq	%eax, %rdx
	cmpl	%eax, %edi
	jge	.L21
	cmpl	%edx, %r9d
	jle	.L23
	subl	%eax, %ecx
	leaq	(%r14,%rax), %rsi
	leaq	1(%rdx), %rax
	leaq	(%rcx,%rax), %r9
	leaq	(%r8,%rsi,8), %rsi
	movl	$-2, %ecx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L45:
	addq	$1, %rax
.L25:
	movq	16(%rbx), %rdi
	addq	(%rbx), %rdx
	addq	$8, %rsi
	movzwl	(%rdi,%rdx,2), %edx
	movl	%ecx, %edi
	subl	$2, %ecx
	orl	$1, %edi
	movw	%dx, -4(%rsi)
	movq	%rax, %rdx
	movl	%edi, -8(%rsi)
	cmpq	%rax, %r9
	jne	.L45
.L23:
	movq	16(%r12), %rbx
	movq	24(%r12), %rax
	orl	$16, %r13d
	subq	%rbx, %rax
	cmpq	$23, %rax
	jbe	.L46
	leaq	24(%rbx), %rax
	movq	%rax, 16(%r12)
.L26:
	movq	%r14, %xmm0
	movq	%r15, %xmm1
	movq	%r8, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	subq	%rax, %rcx
	cmpq	$79, %rcx
	jbe	.L47
	leaq	80(%rax), %rdx
	movq	%rdx, 16(%r12)
.L28:
	leaq	.LC5(%rip), %rdi
	movl	$5, %edx
	movl	$5, %ecx
	movl	$1, (%rax)
	movb	$1, 4(%rax)
	pxor	%xmm0, %xmm0
	movl	$2, 8(%rax)
	movw	%dx, 12(%rax)
	movl	$-2, 16(%rax)
	movw	%cx, 20(%rax)
	movq	%rbx, 24(%rax)
	movb	$32, 48(%rax)
	movl	$61448, 52(%rax)
	movq	$0, 56(%rax)
	movl	%r13d, 64(%rax)
	movq	%rdi, 72(%rax)
	movups	%xmm0, 32(%rax)
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L48
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L46:
	.cfi_restore_state
	movl	$24, %esi
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %rbx
	jmp	.L26
.L47:
	movl	$80, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L28
.L48:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22008:
	.size	_ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE, .-_ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE:
.LFB26721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26721:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE, .-_GLOBAL__sub_I__ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler7Linkage24GetSimplifiedCDescriptorEPNS0_4ZoneEPKNS0_9SignatureINS0_11MachineTypeEEENS_4base5FlagsINS1_14CallDescriptor4FlagEiEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	7
	.long	6
	.long	2
	.long	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
