	.file	"compiler-source-position-table.cc"
	.text
	.section	.text._ZN2v88internal8compiler19SourcePositionTable9DecoratorD2Ev,"axG",@progbits,_ZN2v88internal8compiler19SourcePositionTable9DecoratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19SourcePositionTable9DecoratorD2Ev
	.type	_ZN2v88internal8compiler19SourcePositionTable9DecoratorD2Ev, @function
_ZN2v88internal8compiler19SourcePositionTable9DecoratorD2Ev:
.LFB11890:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11890:
	.size	_ZN2v88internal8compiler19SourcePositionTable9DecoratorD2Ev, .-_ZN2v88internal8compiler19SourcePositionTable9DecoratorD2Ev
	.weak	_ZN2v88internal8compiler19SourcePositionTable9DecoratorD1Ev
	.set	_ZN2v88internal8compiler19SourcePositionTable9DecoratorD1Ev,_ZN2v88internal8compiler19SourcePositionTable9DecoratorD2Ev
	.section	.rodata._ZN2v88internal8compiler19SourcePositionTable9DecoratorD0Ev.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler19SourcePositionTable9DecoratorD0Ev,"axG",@progbits,_ZN2v88internal8compiler19SourcePositionTable9DecoratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19SourcePositionTable9DecoratorD0Ev
	.type	_ZN2v88internal8compiler19SourcePositionTable9DecoratorD0Ev, @function
_ZN2v88internal8compiler19SourcePositionTable9DecoratorD0Ev:
.LFB11892:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE11892:
	.size	_ZN2v88internal8compiler19SourcePositionTable9DecoratorD0Ev, .-_ZN2v88internal8compiler19SourcePositionTable9DecoratorD0Ev
	.section	.text._ZN2v88internal8compiler19SourcePositionTableC2EPNS1_5GraphE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19SourcePositionTableC2EPNS1_5GraphE
	.type	_ZN2v88internal8compiler19SourcePositionTableC2EPNS1_5GraphE, @function
_ZN2v88internal8compiler19SourcePositionTableC2EPNS1_5GraphE:
.LFB10188:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	%rax, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	ret
	.cfi_endproc
.LFE10188:
	.size	_ZN2v88internal8compiler19SourcePositionTableC2EPNS1_5GraphE, .-_ZN2v88internal8compiler19SourcePositionTableC2EPNS1_5GraphE
	.globl	_ZN2v88internal8compiler19SourcePositionTableC1EPNS1_5GraphE
	.set	_ZN2v88internal8compiler19SourcePositionTableC1EPNS1_5GraphE,_ZN2v88internal8compiler19SourcePositionTableC2EPNS1_5GraphE
	.section	.text._ZN2v88internal8compiler19SourcePositionTable12AddDecoratorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19SourcePositionTable12AddDecoratorEv
	.type	_ZN2v88internal8compiler19SourcePositionTable12AddDecoratorEv, @function
_ZN2v88internal8compiler19SourcePositionTable12AddDecoratorEv:
.LFB10190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$15, %rax
	jbe	.L10
	leaq	16(%rsi), %rax
	movq	%rax, 16(%rdi)
.L8:
	leaq	16+_ZTVN2v88internal8compiler19SourcePositionTable9DecoratorE(%rip), %rax
	movq	%rbx, 8(%rsi)
	movq	%rax, (%rsi)
	movq	(%rbx), %rdi
	movq	%rsi, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler5Graph12AddDecoratorEPNS1_14GraphDecoratorE@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L8
	.cfi_endproc
.LFE10190:
	.size	_ZN2v88internal8compiler19SourcePositionTable12AddDecoratorEv, .-_ZN2v88internal8compiler19SourcePositionTable12AddDecoratorEv
	.section	.text._ZN2v88internal8compiler19SourcePositionTable15RemoveDecoratorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19SourcePositionTable15RemoveDecoratorEv
	.type	_ZN2v88internal8compiler19SourcePositionTable15RemoveDecoratorEv, @function
_ZN2v88internal8compiler19SourcePositionTable15RemoveDecoratorEv:
.LFB10191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rsi
	movq	(%rdi), %rdi
	call	_ZN2v88internal8compiler5Graph15RemoveDecoratorEPNS1_14GraphDecoratorE@PLT
	movq	$0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10191:
	.size	_ZN2v88internal8compiler19SourcePositionTable15RemoveDecoratorEv, .-_ZN2v88internal8compiler19SourcePositionTable15RemoveDecoratorEv
	.section	.text._ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE
	.type	_ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE, @function
_ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE:
.LFB10192:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rcx
	movq	40(%rdi), %rax
	xorl	%r8d, %r8d
	movl	20(%rsi), %edx
	subq	%rcx, %rax
	andl	$16777215, %edx
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L14
	movq	(%rcx,%rdx,8), %r8
.L14:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE10192:
	.size	_ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE, .-_ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE
	.section	.rodata._ZNK2v88internal8compiler19SourcePositionTable9PrintJsonERSo.str1.1,"aMS",@progbits,1
.LC1:
	.string	"{"
.LC2:
	.string	"}"
.LC3:
	.string	","
.LC4:
	.string	"\""
.LC5:
	.string	"\" : "
	.section	.text._ZNK2v88internal8compiler19SourcePositionTable9PrintJsonERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19SourcePositionTable9PrintJsonERSo
	.type	_ZNK2v88internal8compiler19SourcePositionTable9PrintJsonERSo, @function
_ZNK2v88internal8compiler19SourcePositionTable9PrintJsonERSo:
.LFB10194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	.LC1(%rip), %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%r12), %rax
	movq	40(%r12), %rbx
	subq	%rax, %rbx
	sarq	$3, %rbx
	je	.L21
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
	leaq	-64(%rbp), %r13
	movq	(%rax,%r15,8), %rax
	movq	%rax, -64(%rbp)
	testb	$1, %al
	jne	.L18
.L36:
	movq	%rax, %rdx
	shrq	$31, %rax
	shrq	%rdx
	movzwl	%ax, %eax
	andl	$1073741823, %edx
	orl	%eax, %edx
	je	.L19
.L18:
	testb	%cl, %cl
	jne	.L35
.L20:
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$4, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal14SourcePosition9PrintJsonERSo@PLT
	movl	$1, %ecx
.L19:
	addq	$1, %r15
	cmpq	%r15, %rbx
	je	.L21
	movq	32(%r12), %rax
	movq	(%rax,%r15,8), %rax
	movq	%rax, -64(%rbp)
	testb	$1, %al
	je	.L36
	testb	%cl, %cl
	je	.L20
.L35:
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L37:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10194:
	.size	_ZNK2v88internal8compiler19SourcePositionTable9PrintJsonERSo, .-_ZNK2v88internal8compiler19SourcePositionTable9PrintJsonERSo
	.section	.rodata._ZNSt6vectorIN2v88internal14SourcePositionENS1_13ZoneAllocatorIS2_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S5_EEmRKS2_.str1.1,"aMS",@progbits,1
.LC6:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIN2v88internal14SourcePositionENS1_13ZoneAllocatorIS2_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S5_EEmRKS2_,"axG",@progbits,_ZNSt6vectorIN2v88internal14SourcePositionENS1_13ZoneAllocatorIS2_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S5_EEmRKS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal14SourcePositionENS1_13ZoneAllocatorIS2_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S5_EEmRKS2_
	.type	_ZNSt6vectorIN2v88internal14SourcePositionENS1_13ZoneAllocatorIS2_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S5_EEmRKS2_, @function
_ZNSt6vectorIN2v88internal14SourcePositionENS1_13ZoneAllocatorIS2_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S5_EEmRKS2_:
.LFB11589:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L148
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	24(%r12), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jb	.L41
	movq	%rdi, %r8
	movq	(%rcx), %xmm0
	subq	%rsi, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L42
	leaq	0(,%rdx,8), %r14
	movq	%rdi, %rdx
	subq	%r14, %rdx
	cmpq	%rdx, %rdi
	je	.L89
	leaq	-8(%rdi), %rax
	movl	$16, %ecx
	subq	%rdx, %rax
	subq	%r14, %rcx
	shrq	$3, %rax
	testq	%rcx, %rcx
	leaq	16(%rdi), %rcx
	setle	%sil
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L90
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L90
	leaq	1(%rax), %r8
	xorl	%eax, %eax
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L45:
	movdqu	(%rdx,%rax), %xmm2
	movups	%xmm2, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L45
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	leaq	(%rdx,%rcx), %rsi
	addq	%rdi, %rcx
	cmpq	%rax, %r8
	je	.L47
	movq	(%rsi), %rax
	movq	%rax, (%rcx)
.L47:
	movq	16(%r12), %rax
.L43:
	addq	%r14, %rax
	movq	%rax, 16(%r12)
	cmpq	%rdx, %rbx
	je	.L48
	subq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%xmm0, -40(%rbp)
	subq	%rdx, %rdi
	call	memmove@PLT
	movq	-40(%rbp), %xmm0
.L48:
	leaq	(%rbx,%r14), %rdx
	cmpq	%rdx, %rbx
	je	.L38
	subq	%rbx, %rdx
	movq	%rbx, %rax
	subq	$8, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rdx, %rdx
	je	.L88
	movq	%rcx, %rdx
	movdqa	%xmm0, %xmm1
	shrq	%rdx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rdx
	addq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L52:
	movups	%xmm1, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L52
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	(%rbx,%rax,8), %r13
	cmpq	%rax, %rcx
	je	.L38
.L88:
	movq	%xmm0, 0(%r13)
.L38:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	subq	%rax, %rdx
	je	.L91
	cmpq	$1, %rdx
	je	.L92
	movq	%rdx, %rsi
	movdqa	%xmm0, %xmm1
	xorl	%eax, %eax
	shrq	%rsi
	punpcklqdq	%xmm1, %xmm1
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%rax, %rcx
	addq	$1, %rax
	salq	$4, %rcx
	movups	%xmm1, (%rdi,%rcx)
	cmpq	%rax, %rsi
	jne	.L56
	movq	%rdx, %rcx
	andq	$-2, %rcx
	leaq	(%rdi,%rcx,8), %rax
	cmpq	%rcx, %rdx
	je	.L57
.L55:
	movq	%xmm0, (%rax)
.L57:
	leaq	(%rdi,%rdx,8), %rsi
.L54:
	movq	%rsi, 16(%r12)
	cmpq	%rbx, %rdi
	je	.L58
	subq	%rbx, %rdi
	leaq	15(%rbx), %rcx
	movq	%rbx, %rdx
	leaq	-8(%rdi), %r9
	subq	%rsi, %rcx
	movq	%r9, %rax
	shrq	$3, %rax
	cmpq	$30, %rcx
	jbe	.L93
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L93
	leaq	1(%rax), %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L60:
	movdqu	(%rbx,%rax), %xmm3
	movups	%xmm3, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L60
	movq	%rcx, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rax
	leaq	(%rbx,%rax), %rdi
	addq	%rax, %rsi
	cmpq	%r9, %rcx
	je	.L61
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
.L61:
	addq	%r8, 16(%r12)
	movq	%rcx, %rax
.L87:
	movq	%rax, %rcx
	movdqa	%xmm0, %xmm1
	shrq	%rcx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rcx
	addq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L65:
	movups	%xmm1, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L65
	movq	%rax, %rdx
	andq	$-2, %rdx
	leaq	(%rbx,%rdx,8), %r13
	cmpq	%rax, %rdx
	jne	.L88
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L41:
	movq	8(%r12), %rsi
	movl	$268435455, %r8d
	movq	%r8, %rax
	subq	%rsi, %rdi
	sarq	$3, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L151
	cmpq	%rdi, %rdx
	movq	%rdi, %rax
	cmovnb	%rdx, %rax
	addq	%rax, %rdi
	setc	%al
	subq	%rsi, %r13
	movzbl	%al, %eax
	testq	%rax, %rax
	jne	.L94
	testq	%rdi, %rdi
	jne	.L152
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L70:
	leaq	0(,%rdx,8), %r9
	leaq	(%rax,%r13), %r11
	leaq	0(%r13,%r9), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	leaq	8(%rcx), %rsi
	setnb	%dil
	cmpq	%rsi, %r11
	setnb	%sil
	orb	%sil, %dil
	je	.L96
	leaq	-1(%rdx), %rsi
	cmpq	$3, %rsi
	jbe	.L96
	movq	(%rcx), %xmm0
	leaq	-2(%rdx), %rdi
	xorl	%esi, %esi
	shrq	%rdi
	addq	$1, %rdi
	punpcklqdq	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L74:
	movq	%rsi, %r10
	addq	$1, %rsi
	salq	$4, %r10
	movups	%xmm0, (%r11,%r10)
	cmpq	%rsi, %rdi
	ja	.L74
	leaq	(%rdi,%rdi), %rsi
	salq	$4, %rdi
	addq	%r11, %rdi
	cmpq	%rsi, %rdx
	je	.L76
	movq	(%rcx), %rdx
	movq	%rdx, (%rdi)
.L76:
	movq	8(%r12), %rcx
	cmpq	%rcx, %rbx
	je	.L97
	leaq	-8(%rbx), %r10
	leaq	15(%rcx), %rdx
	subq	%rcx, %r10
	subq	%rax, %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L98
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L98
	leaq	1(%rsi), %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L79:
	movdqu	(%rcx,%rdx), %xmm4
	movups	%xmm4, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L79
	movq	%r11, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %rcx
	addq	%rax, %rsi
	cmpq	%rdx, %r11
	je	.L81
	movq	(%rcx), %rdx
	movq	%rdx, (%rsi)
.L81:
	leaq	8(%rax,%r10), %rdi
.L77:
	movq	16(%r12), %rdx
	leaq	(%rdi,%r9), %rcx
	cmpq	%rdx, %rbx
	je	.L82
	subq	%rbx, %rdx
	leaq	-8(%rdx), %r10
	leaq	16(%rdi,%r9), %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %rbx
	leaq	16(%rbx), %rdx
	setnb	%dil
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %dil
	je	.L99
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L99
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L84:
	movdqu	(%rbx,%rdx), %xmm5
	movups	%xmm5, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L84
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rdi
	addq	%rdi, %rbx
	addq	%rcx, %rdi
	cmpq	%rdx, %rsi
	je	.L86
	movq	(%rbx), %rdx
	movq	%rdx, (%rdi)
.L86:
	leaq	8(%rcx,%r10), %rcx
.L82:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%r8, 24(%r12)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movl	$2147483640, %esi
	movl	$2147483640, %r14d
.L69:
	movq	(%r12), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L153
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L72:
	leaq	(%rax,%r14), %r8
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L96:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%rcx), %rdi
	movq	%rdi, (%r11,%rsi,8)
	addq	$1, %rsi
	cmpq	%rsi, %rdx
	jne	.L73
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L44:
	movq	(%rdx,%rcx,8), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rax, %rsi
	jne	.L44
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L59:
	movq	(%rbx,%rcx,8), %rdi
	movq	%rdi, (%rsi,%rcx,8)
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	%rax, %rdi
	jne	.L59
	addq	%r8, 16(%r12)
	addq	$1, %rax
	testq	%r9, %r9
	je	.L88
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L98:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L78:
	movq	(%rcx,%rdx,8), %rdi
	movq	%rdi, (%rax,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L78
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L99:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L83:
	movq	(%rbx,%rdx,8), %rdi
	movq	%rdi, (%rcx,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L83
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%rdi, %rsi
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%rdi, %rax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L58:
	addq	%r8, %rsi
	movq	%rsi, 16(%r12)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%rax, %rdi
	jmp	.L77
.L92:
	movq	%rdi, %rax
	jmp	.L55
.L153:
	movq	%r8, %rdi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	jmp	.L72
.L152:
	cmpq	$268435455, %rdi
	cmova	%r8, %rdi
	leaq	0(,%rdi,8), %r14
	movq	%r14, %rsi
	jmp	.L69
.L151:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11589:
	.size	_ZNSt6vectorIN2v88internal14SourcePositionENS1_13ZoneAllocatorIS2_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S5_EEmRKS2_, .-_ZNSt6vectorIN2v88internal14SourcePositionENS1_13ZoneAllocatorIS2_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S5_EEmRKS2_
	.section	.text._ZN2v88internal8compiler19SourcePositionTable9Decorator8DecorateEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler19SourcePositionTable9Decorator8DecorateEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19SourcePositionTable9Decorator8DecorateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19SourcePositionTable9Decorator8DecorateEPNS1_4NodeE, @function
_ZN2v88internal8compiler19SourcePositionTable9Decorator8DecorateEPNS1_4NodeE:
.LFB10180:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %r12
	movl	20(%rsi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%r12), %rsi
	movq	32(%r12), %rcx
	andl	$16777215, %ebx
	movq	16(%r12), %r13
	movq	%rsi, %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L161
.L155:
	leaq	(%rcx,%rbx,8), %rax
	cmpq	%r13, (%rax)
	je	.L154
	movq	%r13, (%rax)
.L154:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L162
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	leaq	1(%rbx), %rdx
	movq	$0, -48(%rbp)
	cmpq	%rdx, %rax
	jb	.L163
	jbe	.L155
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L155
	movq	%rax, 40(%r12)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L163:
	leaq	-48(%rbp), %rcx
	subq	%rax, %rdx
	leaq	24(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal14SourcePositionENS1_13ZoneAllocatorIS2_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S5_EEmRKS2_
	movq	32(%r12), %rcx
	jmp	.L155
.L162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10180:
	.size	_ZN2v88internal8compiler19SourcePositionTable9Decorator8DecorateEPNS1_4NodeE, .-_ZN2v88internal8compiler19SourcePositionTable9Decorator8DecorateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19SourcePositionTable17SetSourcePositionEPNS1_4NodeENS0_14SourcePositionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19SourcePositionTable17SetSourcePositionEPNS1_4NodeENS0_14SourcePositionE
	.type	_ZN2v88internal8compiler19SourcePositionTable17SetSourcePositionEPNS1_4NodeENS0_14SourcePositionE, @function
_ZN2v88internal8compiler19SourcePositionTable17SetSourcePositionEPNS1_4NodeENS0_14SourcePositionE:
.LFB10193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	20(%rsi), %ebx
	movq	40(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rcx
	movq	%rsi, %rax
	andl	$16777215, %ebx
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L171
.L165:
	leaq	(%rcx,%rbx,8), %rax
	cmpq	%r13, (%rax)
	je	.L164
	movq	%r13, (%rax)
.L164:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L172
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	leaq	1(%rbx), %rdx
	movq	$0, -48(%rbp)
	movq	%rdi, %r12
	cmpq	%rdx, %rax
	jb	.L173
	jbe	.L165
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rax, %rsi
	je	.L165
	movq	%rax, 40(%rdi)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L173:
	leaq	-48(%rbp), %rcx
	subq	%rax, %rdx
	leaq	24(%rdi), %rdi
	call	_ZNSt6vectorIN2v88internal14SourcePositionENS1_13ZoneAllocatorIS2_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S5_EEmRKS2_
	movq	32(%r12), %rcx
	jmp	.L165
.L172:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10193:
	.size	_ZN2v88internal8compiler19SourcePositionTable17SetSourcePositionEPNS1_4NodeENS0_14SourcePositionE, .-_ZN2v88internal8compiler19SourcePositionTable17SetSourcePositionEPNS1_4NodeENS0_14SourcePositionE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler19SourcePositionTableC2EPNS1_5GraphE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler19SourcePositionTableC2EPNS1_5GraphE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler19SourcePositionTableC2EPNS1_5GraphE:
.LFB11930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11930:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler19SourcePositionTableC2EPNS1_5GraphE, .-_GLOBAL__sub_I__ZN2v88internal8compiler19SourcePositionTableC2EPNS1_5GraphE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler19SourcePositionTableC2EPNS1_5GraphE
	.weak	_ZTVN2v88internal8compiler19SourcePositionTable9DecoratorE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler19SourcePositionTable9DecoratorE,"awG",@progbits,_ZTVN2v88internal8compiler19SourcePositionTable9DecoratorE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler19SourcePositionTable9DecoratorE, @object
	.size	_ZTVN2v88internal8compiler19SourcePositionTable9DecoratorE, 40
_ZTVN2v88internal8compiler19SourcePositionTable9DecoratorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler19SourcePositionTable9DecoratorD1Ev
	.quad	_ZN2v88internal8compiler19SourcePositionTable9DecoratorD0Ev
	.quad	_ZN2v88internal8compiler19SourcePositionTable9Decorator8DecorateEPNS1_4NodeE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
