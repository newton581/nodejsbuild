	.file	"value-numbering-reducer.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler21ValueNumberingReducer12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ValueNumberingReducer"
	.section	.text._ZNK2v88internal8compiler21ValueNumberingReducer12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler21ValueNumberingReducer12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler21ValueNumberingReducer12reducer_nameEv
	.type	_ZNK2v88internal8compiler21ValueNumberingReducer12reducer_nameEv, @function
_ZNK2v88internal8compiler21ValueNumberingReducer12reducer_nameEv:
.LFB10163:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10163:
	.size	_ZNK2v88internal8compiler21ValueNumberingReducer12reducer_nameEv, .-_ZNK2v88internal8compiler21ValueNumberingReducer12reducer_nameEv
	.section	.text._ZN2v88internal8compiler21ValueNumberingReducerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21ValueNumberingReducerD2Ev
	.type	_ZN2v88internal8compiler21ValueNumberingReducerD2Ev, @function
_ZN2v88internal8compiler21ValueNumberingReducerD2Ev:
.LFB10207:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10207:
	.size	_ZN2v88internal8compiler21ValueNumberingReducerD2Ev, .-_ZN2v88internal8compiler21ValueNumberingReducerD2Ev
	.globl	_ZN2v88internal8compiler21ValueNumberingReducerD1Ev
	.set	_ZN2v88internal8compiler21ValueNumberingReducerD1Ev,_ZN2v88internal8compiler21ValueNumberingReducerD2Ev
	.section	.text._ZN2v88internal8compiler21ValueNumberingReducerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21ValueNumberingReducerD0Ev
	.type	_ZN2v88internal8compiler21ValueNumberingReducerD0Ev, @function
_ZN2v88internal8compiler21ValueNumberingReducerD0Ev:
.LFB10209:
	.cfi_startproc
	endbr64
	movl	$48, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10209:
	.size	_ZN2v88internal8compiler21ValueNumberingReducerD0Ev, .-_ZN2v88internal8compiler21ValueNumberingReducerD0Ev
	.section	.text._ZN2v88internal8compiler21ValueNumberingReducerC2EPNS0_4ZoneES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21ValueNumberingReducerC2EPNS0_4ZoneES4_
	.type	_ZN2v88internal8compiler21ValueNumberingReducerC2EPNS0_4ZoneES4_, @function
_ZN2v88internal8compiler21ValueNumberingReducerC2EPNS0_4ZoneES4_:
.LFB10204:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	%rdx, %xmm1
	leaq	16+_ZTVN2v88internal8compiler21ValueNumberingReducerE(%rip), %rax
	movq	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, (%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE10204:
	.size	_ZN2v88internal8compiler21ValueNumberingReducerC2EPNS0_4ZoneES4_, .-_ZN2v88internal8compiler21ValueNumberingReducerC2EPNS0_4ZoneES4_
	.globl	_ZN2v88internal8compiler21ValueNumberingReducerC1EPNS0_4ZoneES4_
	.set	_ZN2v88internal8compiler21ValueNumberingReducerC1EPNS0_4ZoneES4_,_ZN2v88internal8compiler21ValueNumberingReducerC2EPNS0_4ZoneES4_
	.section	.text._ZN2v88internal8compiler21ValueNumberingReducer19ReplaceIfTypesMatchEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21ValueNumberingReducer19ReplaceIfTypesMatchEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler21ValueNumberingReducer19ReplaceIfTypesMatchEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler21ValueNumberingReducer19ReplaceIfTypesMatchEPNS1_4NodeES4_:
.LFB10211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L7
	movq	8(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.L23
.L7:
	movq	%rdx, %rax
.L13:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L24
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movq	%rax, -24(%rbp)
	movq	%rsi, -16(%rbp)
	cmpq	%rsi, %rax
	je	.L7
	leaq	-24(%rbp), %rdi
	movq	%rdx, -40(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-40(%rbp), %rdx
	testb	%al, %al
	jne	.L7
	movq	-24(%rbp), %rsi
	movq	-16(%rbp), %rax
	cmpq	%rax, %rsi
	jne	.L25
.L11:
	movq	%rax, 8(%rdx)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L25:
	leaq	-16(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L13
	movq	-16(%rbp), %rax
	movq	-40(%rbp), %rdx
	jmp	.L11
.L24:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10211:
	.size	_ZN2v88internal8compiler21ValueNumberingReducer19ReplaceIfTypesMatchEPNS1_4NodeES4_, .-_ZN2v88internal8compiler21ValueNumberingReducer19ReplaceIfTypesMatchEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler21ValueNumberingReducer4GrowEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21ValueNumberingReducer4GrowEv
	.type	_ZN2v88internal8compiler21ValueNumberingReducer4GrowEv, @function
_ZN2v88internal8compiler21ValueNumberingReducer4GrowEv:
.LFB10212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	32(%rdi), %r8
	movq	8(%rdi), %r15
	leaq	(%r12,%r12), %rax
	movq	%r12, %rsi
	movq	%rax, 16(%rdi)
	movq	16(%r8), %rdi
	salq	$4, %rsi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L42
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L28:
	movq	16(%r14), %rax
	movq	%rdi, 8(%r14)
	xorl	%esi, %esi
	leaq	(%r15,%r12,8), %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	16(%r14), %rax
	movq	$0, 24(%r14)
	leaq	-1(%rax), %r13
	testq	%r12, %r12
	je	.L26
	.p2align 4,,10
	.p2align 3
.L39:
	movq	(%r15), %r12
	testq	%r12, %r12
	je	.L31
	movzbl	23(%r12), %eax
	leaq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L33
	movq	32(%r12), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
.L33:
	testl	%eax, %eax
	jle	.L34
	cmpq	$0, (%rdx)
	je	.L31
.L34:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8HashCodeEPNS1_4NodeE@PLT
	movq	8(%r14), %rsi
	andq	%r13, %rax
	leaq	(%rsi,%rax,8), %rcx
	movq	(%rcx), %rdx
	cmpq	%rdx, %r12
	jne	.L37
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L36:
	addq	$1, %rax
	andq	%r13, %rax
	leaq	(%rsi,%rax,8), %rcx
	movq	(%rcx), %rdx
	cmpq	%rdx, %r12
	je	.L31
.L37:
	testq	%rdx, %rdx
	jne	.L36
	movq	%r12, (%rcx)
	addq	$1, 24(%r14)
.L31:
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L39
.L26:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L42:
	.cfi_restore_state
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L28
	.cfi_endproc
.LFE10212:
	.size	_ZN2v88internal8compiler21ValueNumberingReducer4GrowEv, .-_ZN2v88internal8compiler21ValueNumberingReducer4GrowEv
	.section	.text._ZN2v88internal8compiler21ValueNumberingReducer6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21ValueNumberingReducer6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21ValueNumberingReducer6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler21ValueNumberingReducer6ReduceEPNS1_4NodeE:
.LFB10210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$4, 18(%rax)
	jne	.L44
	xorl	%eax, %eax
.L45:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L110
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	%rdi, %r13
	movq	%rsi, %rdi
	movq	%rsi, %r15
	call	_ZN2v88internal8compiler14NodeProperties8HashCodeEPNS1_4NodeE@PLT
	movq	8(%r13), %rsi
	movq	%rax, %r12
	testq	%rsi, %rsi
	je	.L111
	movq	16(%r13), %rax
	leaq	-1(%rax), %r14
	movq	%rax, -88(%rbp)
	andq	%r14, %r12
	leaq	0(,%r12,8), %rcx
	leaq	(%rsi,%rcx), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	jne	.L51
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties6EqualsEPNS1_4NodeES4_@PLT
	testb	%al, %al
	jne	.L75
	movq	8(%r13), %rsi
.L74:
	addq	$1, %r12
	andq	%r14, %r12
	leaq	0(,%r12,8), %rcx
	leaq	(%rsi,%rcx), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L112
.L51:
	cmpq	%rbx, %r15
	je	.L59
	movzbl	23(%rbx), %eax
	leaq	32(%rbx), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L72
	movq	32(%rbx), %rcx
	movl	8(%rcx), %eax
	addq	$16, %rcx
.L72:
	testl	%eax, %eax
	jle	.L73
	cmpq	$0, (%rcx)
	jne	.L73
	movq	%r12, -88(%rbp)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L59:
	addq	$1, %r12
	andq	%r14, %r12
	leaq	0(,%r12,8), %rdx
	leaq	(%rsi,%rdx), %r8
	movq	(%r8), %rbx
	testq	%rbx, %rbx
	je	.L81
.L70:
	movzbl	23(%rbx), %eax
	leaq	32(%rbx), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L57
	movq	32(%rbx), %rdi
	movl	8(%rdi), %eax
	addq	$16, %rdi
.L57:
	testl	%eax, %eax
	jle	.L58
	cmpq	$0, (%rdi)
	je	.L59
.L58:
	cmpq	%rbx, %r15
	je	.L113
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties6EqualsEPNS1_4NodeES4_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdx
	testb	%al, %al
	jne	.L62
	addq	$1, %r12
	movq	8(%r13), %rsi
	andq	%r14, %r12
	leaq	0(,%r12,8), %rdx
	leaq	(%rsi,%rdx), %r8
	movq	(%r8), %rbx
	testq	%rbx, %rbx
	jne	.L70
	.p2align 4,,10
	.p2align 3
.L81:
	xorl	%ebx, %ebx
.L49:
	movq	%rbx, %rax
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L113:
	addq	$1, %r12
	andq	%r14, %r12
	movq	(%rsi,%r12,8), %rbx
	leaq	0(,%r12,8), %rdx
	testq	%rbx, %rbx
	je	.L114
	leaq	(%rsi,%rdx), %r8
	movq	(%r8), %rbx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L112:
	movq	-88(%rbp), %rdx
	cmpq	%rdx, 16(%r13)
	je	.L50
	movq	%r15, (%rsi,%rdx,8)
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L75:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L49
	movq	8(%r15), %rsi
	testq	%rsi, %rsi
	je	.L49
	movq	%rax, -72(%rbp)
	movq	%rsi, -64(%rbp)
	cmpq	%rsi, %rax
	je	.L49
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L49
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rax
	cmpq	%rax, %rsi
	je	.L80
	leaq	-64(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L81
	movq	-64(%rbp), %rax
.L80:
	movq	%rax, 8(%rbx)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L114:
	movq	$0, (%r8)
	subq	$1, 24(%r13)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L111:
	movq	32(%r13), %rdi
	movq	$256, 16(%r13)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	$2047, %rcx
	jbe	.L115
	leaq	2048(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L48:
	leaq	8(%rax), %rdi
	movq	%rax, 8(%r13)
	movzbl	%r12b, %r12d
	xorl	%ebx, %ebx
	andq	$-8, %rdi
	movq	$0, (%rax)
	movq	$0, 2040(%rax)
	subq	%rdi, %rax
	leal	2048(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movq	8(%r13), %rcx
	movq	%r15, (%rcx,%r12,8)
	movq	$1, 24(%r13)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%r15, (%rax)
	movq	24(%r13), %rax
	addq	$1, %rax
	movq	%rax, %rdx
	movq	%rax, 24(%r13)
	shrq	$2, %rdx
	addq	%rdx, %rax
	cmpq	16(%r13), %rax
	jb	.L81
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21ValueNumberingReducer4GrowEv
	jmp	.L81
.L62:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L64
	movq	8(%r15), %rsi
	testq	%rsi, %rsi
	je	.L64
	movq	%rax, -72(%rbp)
	movq	%rsi, -64(%rbp)
	cmpq	%rsi, %rax
	je	.L64
	leaq	-72(%rbp), %rdi
	movq	%rdx, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdx
	testb	%al, %al
	jne	.L64
	movq	-72(%rbp), %rsi
	cmpq	-64(%rbp), %rsi
	je	.L68
	leaq	-64(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdx
	testb	%al, %al
	je	.L81
.L68:
	movq	-64(%rbp), %rax
	movq	%rax, 8(%rbx)
.L64:
	movq	8(%r13), %rax
	addq	$1, %r12
	andq	%r12, %r14
	movq	%rbx, (%rax,%rcx)
	movq	8(%r13), %rax
	cmpq	$0, (%rax,%r14,8)
	jne	.L49
	movq	$0, (%rax,%rdx)
	subq	$1, 24(%r13)
	jmp	.L49
.L115:
	movl	$2048, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L48
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10210:
	.size	_ZN2v88internal8compiler21ValueNumberingReducer6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler21ValueNumberingReducer6ReduceEPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler21ValueNumberingReducerC2EPNS0_4ZoneES4_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler21ValueNumberingReducerC2EPNS0_4ZoneES4_, @function
_GLOBAL__sub_I__ZN2v88internal8compiler21ValueNumberingReducerC2EPNS0_4ZoneES4_:
.LFB11816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11816:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler21ValueNumberingReducerC2EPNS0_4ZoneES4_, .-_GLOBAL__sub_I__ZN2v88internal8compiler21ValueNumberingReducerC2EPNS0_4ZoneES4_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler21ValueNumberingReducerC2EPNS0_4ZoneES4_
	.weak	_ZTVN2v88internal8compiler21ValueNumberingReducerE
	.section	.data.rel.ro._ZTVN2v88internal8compiler21ValueNumberingReducerE,"awG",@progbits,_ZTVN2v88internal8compiler21ValueNumberingReducerE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler21ValueNumberingReducerE, @object
	.size	_ZTVN2v88internal8compiler21ValueNumberingReducerE, 56
_ZTVN2v88internal8compiler21ValueNumberingReducerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler21ValueNumberingReducerD1Ev
	.quad	_ZN2v88internal8compiler21ValueNumberingReducerD0Ev
	.quad	_ZNK2v88internal8compiler21ValueNumberingReducer12reducer_nameEv
	.quad	_ZN2v88internal8compiler21ValueNumberingReducer6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
