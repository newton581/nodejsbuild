	.file	"type-cache.cc"
	.text
	.section	.rodata._ZN2v88internal8compiler9TypeCacheC2Ev.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../deps/v8/src/compiler/type-cache.h:27"
	.section	.text._ZN2v88internal8compiler9TypeCacheC2Ev,"axG",@progbits,_ZN2v88internal8compiler9TypeCacheC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9TypeCacheC2Ev
	.type	_ZN2v88internal8compiler9TypeCacheC2Ev, @function
_ZN2v88internal8compiler9TypeCacheC2Ev:
.LFB10008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal19AccountingAllocatorE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rdi, %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	24(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movups	%xmm0, 8(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movsd	.LC1(%rip), %xmm1
	movsd	.LC2(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movsd	.LC3(%rip), %xmm1
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, 88(%rbx)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$6145, %esi
	movq	%rax, 96(%rbx)
	movq	%rax, %rdi
	movq	%rax, 104(%rbx)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movsd	.LC5(%rip), %xmm1
	movsd	.LC6(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, 112(%rbx)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movsd	.LC7(%rip), %xmm1
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, 120(%rbx)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movsd	.LC8(%rip), %xmm1
	movsd	.LC9(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, 128(%rbx)
	movq	$1099, 136(%rbx)
	movq	$1031, 144(%rbx)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movsd	.LC10(%rip), %xmm1
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, 152(%rbx)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	$7263, 168(%rbx)
	movq	%rax, 160(%rbx)
	movq	$7263, 176(%rbx)
	movq	$134217729, 184(%rbx)
	movq	$134217729, 192(%rbx)
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	%r12, %rdx
	movl	$8388609, %esi
	movl	%eax, %edi
	orl	$1, %edi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	pxor	%xmm1, %xmm1
	movq	%r12, %rdi
	movq	%rax, 200(%rbx)
	movapd	%xmm1, %xmm0
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movsd	.LC11(%rip), %xmm2
	movq	%r12, %rdi
	movq	%rax, 208(%rbx)
	movapd	%xmm2, %xmm1
	movapd	%xmm2, %xmm0
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movsd	.LC12(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, 216(%rbx)
	movapd	%xmm0, %xmm1
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movsd	.LC13(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, 224(%rbx)
	movapd	%xmm0, %xmm1
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	208(%rbx), %rdi
	movq	%r12, %rdx
	movl	$2049, %esi
	movq	%rax, 232(%rbx)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	208(%rbx), %rdi
	movq	%r12, %rdx
	movl	$257, %esi
	movq	%rax, 240(%rbx)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	224(%rbx), %rdi
	movq	%r12, %rdx
	movl	$257, %esi
	movq	%rax, 248(%rbx)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdi
	pxor	%xmm1, %xmm1
	movq	%rax, 256(%rbx)
	movq	.LC13(%rip), %rax
	movq	%rax, %xmm0
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%r12, %rdi
	movq	%rax, 264(%rbx)
	movq	.LC11(%rip), %rax
	movq	%rax, %xmm1
	movq	.LC13(%rip), %rax
	movq	%rax, %xmm0
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$2049, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$4097, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, 272(%rbx)
	movq	.LC11(%rip), %rax
	movq	%rax, %xmm1
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$4097, %esi
	movq	%rax, 280(%rbx)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movsd	.LC14(%rip), %xmm5
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, 288(%rbx)
	movapd	%xmm5, %xmm1
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movsd	.LC15(%rip), %xmm1
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, 296(%rbx)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	208(%rbx), %rdi
	movq	%r12, %rdx
	movl	$6145, %esi
	movq	%rax, 304(%rbx)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movsd	.LC16(%rip), %xmm1
	movsd	.LC17(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, 312(%rbx)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$2049, %esi
	movq	%rax, 320(%rbx)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$4097, %esi
	movq	%rax, 328(%rbx)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, 336(%rbx)
	movq	.LC16(%rip), %rax
	movq	%rax, %xmm1
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$2049, %esi
	movq	%rax, 344(%rbx)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	344(%rbx), %rdi
	movq	%r12, %rdx
	movl	$4097, %esi
	movq	%rax, 352(%rbx)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rdx
	movl	$4097, %esi
	movq	%rax, 360(%rbx)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movsd	.LC18(%rip), %xmm1
	movsd	.LC19(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, 368(%rbx)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movsd	.LC20(%rip), %xmm4
	movsd	.LC21(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, 376(%rbx)
	movapd	%xmm4, %xmm1
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	376(%rbx), %rdi
	movq	%r12, %rdx
	movl	$2049, %esi
	movq	%rax, 384(%rbx)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	384(%rbx), %rdi
	movq	%r12, %rdx
	movl	$2049, %esi
	movq	%rax, 392(%rbx)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, 400(%rbx)
	movq	.LC20(%rip), %rax
	movq	%rax, %xmm1
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movsd	.LC22(%rip), %xmm3
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, 408(%rbx)
	movapd	%xmm3, %xmm1
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, 416(%rbx)
	movq	.LC22(%rip), %rax
	movq	%rax, %xmm1
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	$1031, 432(%rbx)
	movq	%rax, 424(%rbx)
	movq	.LC20(%rip), %rax
	movq	%rax, %xmm1
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movsd	.LC23(%rip), %xmm1
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, 440(%rbx)
	movq	%rax, 448(%rbx)
	movq	%rax, 456(%rbx)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movsd	.LC24(%rip), %xmm1
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, 464(%rbx)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movsd	.LC25(%rip), %xmm1
	movsd	.LC26(%rip), %xmm0
	movq	%r12, %rdi
	movq	%rax, 472(%rbx)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%r12, %rdi
	movq	%rax, 480(%rbx)
	movq	.LC14(%rip), %rax
	movq	%rax, %xmm1
	movq	.LC11(%rip), %rax
	movq	%rax, %xmm0
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$4097, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movsd	.LC27(%rip), %xmm1
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, 488(%rbx)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$4097, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movsd	.LC28(%rip), %xmm1
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, 496(%rbx)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$4097, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movsd	.LC29(%rip), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, 504(%rbx)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$4097, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	480(%rbx), %rdi
	movq	%r12, %rdx
	movl	$4097, %esi
	movq	%rax, 512(%rbx)
	movq	504(%rbx), %rax
	movq	%rax, 520(%rbx)
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movsd	.LC30(%rip), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, 528(%rbx)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%r12, %rdx
	movl	$4097, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, 536(%rbx)
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	%r12, %rdx
	movl	$4097, %esi
	movl	%eax, %edi
	orl	$1, %edi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, 544(%rbx)
	movq	.LC22(%rip), %rax
	movq	%rax, %xmm1
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movsd	.LC31(%rip), %xmm1
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	movq	%rax, 552(%rbx)
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%rax, 560(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10008:
	.size	_ZN2v88internal8compiler9TypeCacheC2Ev, .-_ZN2v88internal8compiler9TypeCacheC2Ev
	.weak	_ZN2v88internal8compiler9TypeCacheC1Ev
	.set	_ZN2v88internal8compiler9TypeCacheC1Ev,_ZN2v88internal8compiler9TypeCacheC2Ev
	.section	.text._ZN2v88internal8compiler9TypeCache3GetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler9TypeCache3GetEv
	.type	_ZN2v88internal8compiler9TypeCache3GetEv, @function
_ZN2v88internal8compiler9TypeCache3GetEv:
.LFB10013:
	.cfi_startproc
	endbr64
	movzbl	_ZGVZN2v88internal8compiler9TypeCache3GetEvE6object(%rip), %eax
	testb	%al, %al
	je	.L15
	leaq	_ZZN2v88internal8compiler9TypeCache3GetEvE6object(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZGVZN2v88internal8compiler9TypeCache3GetEvE6object(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	jne	.L16
	leaq	_ZZN2v88internal8compiler9TypeCache3GetEvE6object(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	leaq	_ZZN2v88internal8compiler9TypeCache3GetEvE6object(%rip), %rdi
	call	_ZN2v88internal8compiler9TypeCacheC1Ev
	leaq	_ZGVZN2v88internal8compiler9TypeCache3GetEvE6object(%rip), %rdi
	call	__cxa_guard_release@PLT
	leaq	_ZZN2v88internal8compiler9TypeCache3GetEvE6object(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10013:
	.size	_ZN2v88internal8compiler9TypeCache3GetEv, .-_ZN2v88internal8compiler9TypeCache3GetEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler9TypeCache3GetEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler9TypeCache3GetEv, @function
_GLOBAL__sub_I__ZN2v88internal8compiler9TypeCache3GetEv:
.LFB11631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11631:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler9TypeCache3GetEv, .-_GLOBAL__sub_I__ZN2v88internal8compiler9TypeCache3GetEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler9TypeCache3GetEv
	.section	.bss._ZGVZN2v88internal8compiler9TypeCache3GetEvE6object,"aw",@nobits
	.align 8
	.type	_ZGVZN2v88internal8compiler9TypeCache3GetEvE6object, @object
	.size	_ZGVZN2v88internal8compiler9TypeCache3GetEvE6object, 8
_ZGVZN2v88internal8compiler9TypeCache3GetEvE6object:
	.zero	8
	.section	.bss._ZZN2v88internal8compiler9TypeCache3GetEvE6object,"aw",@nobits
	.align 32
	.type	_ZZN2v88internal8compiler9TypeCache3GetEvE6object, @object
	.size	_ZZN2v88internal8compiler9TypeCache3GetEvE6object, 568
_ZZN2v88internal8compiler9TypeCache3GetEvE6object:
	.zero	568
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1080016896
	.align 8
.LC2:
	.long	0
	.long	-1067450368
	.align 8
.LC3:
	.long	0
	.long	1081073664
	.align 8
.LC5:
	.long	0
	.long	1088421824
	.align 8
.LC6:
	.long	0
	.long	-1059061760
	.align 8
.LC7:
	.long	0
	.long	1089470432
	.align 8
.LC8:
	.long	0
	.long	1138753536
	.align 8
.LC9:
	.long	0
	.long	-1008730112
	.align 8
.LC10:
	.long	0
	.long	1139802112
	.align 8
.LC11:
	.long	0
	.long	1072693248
	.align 8
.LC12:
	.long	0
	.long	1076101120
	.align 8
.LC13:
	.long	0
	.long	-1074790400
	.align 8
.LC14:
	.long	0
	.long	1077870592
	.align 8
.LC15:
	.long	0
	.long	1077936128
	.align 8
.LC16:
	.long	0
	.long	2146435072
	.align 8
.LC17:
	.long	0
	.long	-1048576
	.align 8
.LC18:
	.long	0
	.long	1127219200
	.align 8
.LC19:
	.long	0
	.long	-1020264448
	.align 8
.LC20:
	.long	4294967295
	.long	1128267775
	.align 8
.LC21:
	.long	4294967295
	.long	-1019215873
	.align 8
.LC22:
	.long	4160749568
	.long	1101004799
	.align 8
.LC23:
	.long	4290772992
	.long	1105199103
	.align 8
.LC24:
	.long	4085252096
	.long	1104150527
	.align 8
.LC25:
	.long	3269197824
	.long	1128182280
	.align 8
.LC26:
	.long	3269197824
	.long	-1019301368
	.align 8
.LC27:
	.long	0
	.long	1077346304
	.align 8
.LC28:
	.long	0
	.long	1078820864
	.align 8
.LC29:
	.long	0
	.long	1076232192
	.align 8
.LC30:
	.long	0
	.long	1075314688
	.align 8
.LC31:
	.long	0
	.long	1073741824
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
