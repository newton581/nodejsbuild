	.file	"checkpoint-elimination.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler21CheckpointElimination12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"CheckpointElimination"
	.section	.text._ZNK2v88internal8compiler21CheckpointElimination12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler21CheckpointElimination12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler21CheckpointElimination12reducer_nameEv
	.type	_ZNK2v88internal8compiler21CheckpointElimination12reducer_nameEv, @function
_ZNK2v88internal8compiler21CheckpointElimination12reducer_nameEv:
.LFB10163:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10163:
	.size	_ZNK2v88internal8compiler21CheckpointElimination12reducer_nameEv, .-_ZNK2v88internal8compiler21CheckpointElimination12reducer_nameEv
	.section	.text._ZN2v88internal8compiler21CheckpointEliminationD2Ev,"axG",@progbits,_ZN2v88internal8compiler21CheckpointEliminationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler21CheckpointEliminationD2Ev
	.type	_ZN2v88internal8compiler21CheckpointEliminationD2Ev, @function
_ZN2v88internal8compiler21CheckpointEliminationD2Ev:
.LFB11777:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11777:
	.size	_ZN2v88internal8compiler21CheckpointEliminationD2Ev, .-_ZN2v88internal8compiler21CheckpointEliminationD2Ev
	.weak	_ZN2v88internal8compiler21CheckpointEliminationD1Ev
	.set	_ZN2v88internal8compiler21CheckpointEliminationD1Ev,_ZN2v88internal8compiler21CheckpointEliminationD2Ev
	.section	.text._ZN2v88internal8compiler21CheckpointEliminationD0Ev,"axG",@progbits,_ZN2v88internal8compiler21CheckpointEliminationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler21CheckpointEliminationD0Ev
	.type	_ZN2v88internal8compiler21CheckpointEliminationD0Ev, @function
_ZN2v88internal8compiler21CheckpointEliminationD0Ev:
.LFB11779:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11779:
	.size	_ZN2v88internal8compiler21CheckpointEliminationD0Ev, .-_ZN2v88internal8compiler21CheckpointEliminationD0Ev
	.section	.text._ZN2v88internal8compiler21CheckpointElimination6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21CheckpointElimination6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21CheckpointElimination6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler21CheckpointElimination6ReduceEPNS1_4NodeE:
.LFB10210:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	xorl	%eax, %eax
	cmpw	$38, 16(%rdx)
	je	.L22
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	subq	$8, %rsp
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L23:
	cmpl	$1, 24(%rax)
	jne	.L7
	xorl	%esi, %esi
	cmpw	$38, 16(%rax)
	je	.L8
.L21:
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	testb	$16, 18(%rax)
	jne	.L23
.L7:
	xorl	%eax, %eax
.L14:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	jmp	.L14
	.cfi_endproc
.LFE10210:
	.size	_ZN2v88internal8compiler21CheckpointElimination6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler21CheckpointElimination6ReduceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler21CheckpointEliminationC2EPNS1_15AdvancedReducer6EditorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21CheckpointEliminationC2EPNS1_15AdvancedReducer6EditorE
	.type	_ZN2v88internal8compiler21CheckpointEliminationC2EPNS1_15AdvancedReducer6EditorE, @function
_ZN2v88internal8compiler21CheckpointEliminationC2EPNS1_15AdvancedReducer6EditorE:
.LFB10206:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler21CheckpointEliminationE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE10206:
	.size	_ZN2v88internal8compiler21CheckpointEliminationC2EPNS1_15AdvancedReducer6EditorE, .-_ZN2v88internal8compiler21CheckpointEliminationC2EPNS1_15AdvancedReducer6EditorE
	.globl	_ZN2v88internal8compiler21CheckpointEliminationC1EPNS1_15AdvancedReducer6EditorE
	.set	_ZN2v88internal8compiler21CheckpointEliminationC1EPNS1_15AdvancedReducer6EditorE,_ZN2v88internal8compiler21CheckpointEliminationC2EPNS1_15AdvancedReducer6EditorE
	.section	.text._ZN2v88internal8compiler21CheckpointElimination16ReduceCheckpointEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler21CheckpointElimination16ReduceCheckpointEPNS1_4NodeE
	.type	_ZN2v88internal8compiler21CheckpointElimination16ReduceCheckpointEPNS1_4NodeE, @function
_ZN2v88internal8compiler21CheckpointElimination16ReduceCheckpointEPNS1_4NodeE:
.LFB10209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	testb	$16, 18(%rax)
	jne	.L28
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%esi, %esi
	cmpw	$38, 16(%rax)
	je	.L27
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	testb	$16, 18(%rax)
	je	.L26
.L28:
	cmpl	$1, 24(%rax)
	je	.L36
.L26:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10209:
	.size	_ZN2v88internal8compiler21CheckpointElimination16ReduceCheckpointEPNS1_4NodeE, .-_ZN2v88internal8compiler21CheckpointElimination16ReduceCheckpointEPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler21CheckpointEliminationC2EPNS1_15AdvancedReducer6EditorE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler21CheckpointEliminationC2EPNS1_15AdvancedReducer6EditorE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler21CheckpointEliminationC2EPNS1_15AdvancedReducer6EditorE:
.LFB11817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11817:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler21CheckpointEliminationC2EPNS1_15AdvancedReducer6EditorE, .-_GLOBAL__sub_I__ZN2v88internal8compiler21CheckpointEliminationC2EPNS1_15AdvancedReducer6EditorE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler21CheckpointEliminationC2EPNS1_15AdvancedReducer6EditorE
	.weak	_ZTVN2v88internal8compiler21CheckpointEliminationE
	.section	.data.rel.ro._ZTVN2v88internal8compiler21CheckpointEliminationE,"awG",@progbits,_ZTVN2v88internal8compiler21CheckpointEliminationE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler21CheckpointEliminationE, @object
	.size	_ZTVN2v88internal8compiler21CheckpointEliminationE, 56
_ZTVN2v88internal8compiler21CheckpointEliminationE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler21CheckpointEliminationD1Ev
	.quad	_ZN2v88internal8compiler21CheckpointEliminationD0Ev
	.quad	_ZNK2v88internal8compiler21CheckpointElimination12reducer_nameEv
	.quad	_ZN2v88internal8compiler21CheckpointElimination6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
