	.file	"js-type-hint-lowering.cc"
	.text
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0, @function
_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0:
.LFB12889:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	%rsi, -104(%rbp)
	movq	%rdx, -112(%rbp)
	movq	352(%rbx), %r12
	movq	(%rbx), %r14
	movq	8(%rbx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L19
.L2:
	leaq	-96(%rbp), %rcx
	movl	%r15d, %edx
	movl	$1, %esi
	movq	$0, -96(%rbp)
	movl	$-1, -88(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10DeoptimizeENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	-112(%rbp), %rax
	movhps	-104(%rbp), %xmm0
	leaq	-80(%rbp), %rcx
	movl	$3, %edx
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r13), %rbx
	movq	%rax, %r12
	movq	352(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L20
.L3:
	movq	%r12, %rdi
	leaq	32(%r12), %rbx
	call	_ZN2v88internal8compiler14NodeProperties20FindFrameStateBeforeEPNS1_4NodeES4_@PLT
	movq	%rax, %r13
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L4
	movq	32(%r12), %rdi
	movq	%r12, %rsi
	cmpq	%rdi, %r13
	je	.L1
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	je	.L8
.L22:
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L8:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L1
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movq	32(%r12), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rbx
	cmpq	%rdi, %r13
	je	.L1
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	jne	.L22
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L20:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%rbx)
	movq	%rax, %rsi
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L19:
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%rbx)
	movq	%rax, %r12
	movq	8(%r13), %rax
	movq	8(%rax), %rdi
	jmp	.L2
.L21:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12889:
	.size	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0, .-_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0
	.section	.text._ZN2v88internal8compiler18JSTypeHintLoweringC2EPNS1_12JSHeapBrokerEPNS1_7JSGraphENS1_17FeedbackVectorRefENS_4base5FlagsINS2_4FlagEiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18JSTypeHintLoweringC2EPNS1_12JSHeapBrokerEPNS1_7JSGraphENS1_17FeedbackVectorRefENS_4base5FlagsINS2_4FlagEiEE
	.type	_ZN2v88internal8compiler18JSTypeHintLoweringC2EPNS1_12JSHeapBrokerEPNS1_7JSGraphENS1_17FeedbackVectorRefENS_4base5FlagsINS2_4FlagEiEE, @function
_ZN2v88internal8compiler18JSTypeHintLoweringC2EPNS1_12JSHeapBrokerEPNS1_7JSGraphENS1_17FeedbackVectorRefENS_4base5FlagsINS2_4FlagEiEE:
.LFB11108:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	movl	%r9d, 16(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movq	%rcx, 24(%rdi)
	movq	%r8, 32(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE11108:
	.size	_ZN2v88internal8compiler18JSTypeHintLoweringC2EPNS1_12JSHeapBrokerEPNS1_7JSGraphENS1_17FeedbackVectorRefENS_4base5FlagsINS2_4FlagEiEE, .-_ZN2v88internal8compiler18JSTypeHintLoweringC2EPNS1_12JSHeapBrokerEPNS1_7JSGraphENS1_17FeedbackVectorRefENS_4base5FlagsINS2_4FlagEiEE
	.globl	_ZN2v88internal8compiler18JSTypeHintLoweringC1EPNS1_12JSHeapBrokerEPNS1_7JSGraphENS1_17FeedbackVectorRefENS_4base5FlagsINS2_4FlagEiEE
	.set	_ZN2v88internal8compiler18JSTypeHintLoweringC1EPNS1_12JSHeapBrokerEPNS1_7JSGraphENS1_17FeedbackVectorRefENS_4base5FlagsINS2_4FlagEiEE,_ZN2v88internal8compiler18JSTypeHintLoweringC2EPNS1_12JSHeapBrokerEPNS1_7JSGraphENS1_17FeedbackVectorRefENS_4base5FlagsINS2_4FlagEiEE
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18JSTypeHintLowering7isolateEv
	.type	_ZNK2v88internal8compiler18JSTypeHintLowering7isolateEv, @function
_ZNK2v88internal8compiler18JSTypeHintLowering7isolateEv:
.LFB11110:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE11110:
	.size	_ZNK2v88internal8compiler18JSTypeHintLowering7isolateEv, .-_ZNK2v88internal8compiler18JSTypeHintLowering7isolateEv
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering22GetBinaryOperationHintENS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18JSTypeHintLowering22GetBinaryOperationHintENS0_12FeedbackSlotE
	.type	_ZNK2v88internal8compiler18JSTypeHintLowering22GetBinaryOperationHintENS0_12FeedbackSlotE, @function
_ZNK2v88internal8compiler18JSTypeHintLowering22GetBinaryOperationHintENS0_12FeedbackSlotE:
.LFB11111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-48(%rbp), %r12
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	32(%rdi), %rdx
	movq	24(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker29GetFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L28
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L28:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11111:
	.size	_ZNK2v88internal8compiler18JSTypeHintLowering22GetBinaryOperationHintENS0_12FeedbackSlotE, .-_ZNK2v88internal8compiler18JSTypeHintLowering22GetBinaryOperationHintENS0_12FeedbackSlotE
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering23GetCompareOperationHintENS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18JSTypeHintLowering23GetCompareOperationHintENS0_12FeedbackSlotE
	.type	_ZNK2v88internal8compiler18JSTypeHintLowering23GetCompareOperationHintENS0_12FeedbackSlotE, @function
_ZNK2v88internal8compiler18JSTypeHintLowering23GetCompareOperationHintENS0_12FeedbackSlotE:
.LFB11112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-48(%rbp), %r12
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	32(%rdi), %rdx
	movq	24(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker30GetFeedbackForCompareOperationERKNS1_14FeedbackSourceE@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L32
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L32:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11112:
	.size	_ZNK2v88internal8compiler18JSTypeHintLowering23GetCompareOperationHintENS0_12FeedbackSlotE, .-_ZNK2v88internal8compiler18JSTypeHintLowering23GetCompareOperationHintENS0_12FeedbackSlotE
	.section	.rodata._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.type	_ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE, @function
_ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE:
.LFB11113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	%rcx, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$2, 16(%rsi)
	je	.L35
	movq	24(%rsi), %rsi
	leaq	-112(%rbp), %r15
	movq	32(%r12), %rdx
	movl	16(%rbp), %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%r12), %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker22FeedbackIsInsufficientERKNS1_14FeedbackSourceE@PLT
	testb	%al, %al
	je	.L35
	movq	-128(%rbp), %rsi
	movl	$15, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0
	testq	%rax, %rax
	je	.L35
	movl	$2, 0(%r13)
	pxor	%xmm0, %xmm0
	movq	%rax, 24(%r13)
	movups	%xmm0, 8(%r13)
.L33:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movzwl	16(%r14), %eax
	cmpw	$713, %ax
	je	.L38
	ja	.L39
	cmpw	$711, %ax
	je	.L40
	cmpw	$712, %ax
	jne	.L42
	movsd	.LC2(%rip), %xmm0
	movq	8(%r12), %rdi
	leaq	-112(%rbp), %r15
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -144(%rbp)
	movq	8(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8SubtractEv@PLT
	movq	24(%r12), %rsi
	movl	16(%rbp), %ecx
	movq	%r15, %rdi
	movq	32(%r12), %rdx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker29GetFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	cmpb	$5, %al
	ja	.L51
	leaq	.L70(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE,"a",@progbits
	.align 4
	.align 4
.L70:
	.long	.L51-.L70
	.long	.L74-.L70
	.long	.L73-.L70
	.long	.L138-.L70
	.long	.L71-.L70
	.long	.L69-.L70
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.p2align 4,,10
	.p2align 3
.L39:
	cmpw	$714, %ax
	jne	.L42
	movq	8(%r12), %rdi
	movsd	.LC0(%rip), %xmm0
	leaq	24(%r12), %r15
	leaq	-112(%rbp), %r14
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -152(%rbp)
	movq	8(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder8MultiplyEv@PLT
	movq	24(%r12), %rsi
	movl	16(%rbp), %ecx
	movq	%r14, %rdi
	movq	8(%r15), %rdx
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%r12), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker29GetFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	cmpb	$5, %al
	ja	.L112
	leaq	.L114(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.align 4
	.align 4
.L114:
	.long	.L112-.L114
	.long	.L118-.L114
	.long	.L117-.L114
	.long	.L116-.L114
	.long	.L115-.L114
	.long	.L140-.L114
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.p2align 4,,10
	.p2align 3
.L40:
	movsd	.LC0(%rip), %xmm0
	movq	8(%r12), %rdi
	leaq	-112(%rbp), %r15
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, -144(%rbp)
	movq	8(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder10BitwiseXorEv@PLT
	movq	24(%r12), %rsi
	movl	16(%rbp), %ecx
	movq	%r15, %rdi
	movq	32(%r12), %rdx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker29GetFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	cmpb	$5, %al
	ja	.L51
	leaq	.L46(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.align 4
	.align 4
.L46:
	.long	.L51-.L46
	.long	.L50-.L46
	.long	.L49-.L46
	.long	.L137-.L46
	.long	.L47-.L46
	.long	.L45-.L46
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.p2align 4,,10
	.p2align 3
.L51:
	pxor	%xmm0, %xmm0
	movl	$0, 0(%r13)
	movq	$0, 24(%r13)
	movups	%xmm0, 8(%r13)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L38:
	movsd	.LC2(%rip), %xmm0
	movq	8(%r12), %rdi
	leaq	-112(%rbp), %r15
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movl	$8, %esi
	movq	%rax, -144(%rbp)
	movq	8(%r12), %rax
	movq	368(%rax), %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilder3AddENS0_19BinaryOperationHintE@PLT
	movq	24(%r12), %rsi
	movl	16(%rbp), %ecx
	movq	%r15, %rdi
	movq	32(%r12), %rdx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker29GetFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	cmpb	$5, %al
	ja	.L51
	leaq	.L92(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.align 4
	.align 4
.L92:
	.long	.L51-.L92
	.long	.L96-.L92
	.long	.L95-.L92
	.long	.L139-.L92
	.long	.L93-.L92
	.long	.L91-.L92
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.p2align 4,,10
	.p2align 3
.L93:
	movl	$1, %ecx
	movl	$3, %esi
.L94:
	movzwl	16(%r14), %eax
	subw	$688, %ax
	cmpw	$10, %ax
	ja	.L42
	leaq	.L98(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.align 4
	.align 4
.L98:
	.long	.L108-.L98
	.long	.L107-.L98
	.long	.L106-.L98
	.long	.L105-.L98
	.long	.L104-.L98
	.long	.L103-.L98
	.long	.L102-.L98
	.long	.L101-.L98
	.long	.L100-.L98
	.long	.L99-.L98
	.long	.L97-.L98
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.p2align 4,,10
	.p2align 3
.L139:
	xorl	%ecx, %ecx
	movl	$2, %esi
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$1, %ecx
	movl	$3, %esi
.L72:
	movzwl	16(%r14), %eax
	subw	$688, %ax
	cmpw	$10, %ax
	ja	.L42
	leaq	.L76(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.align 4
	.align 4
.L76:
	.long	.L86-.L76
	.long	.L85-.L76
	.long	.L84-.L76
	.long	.L83-.L76
	.long	.L82-.L76
	.long	.L81-.L76
	.long	.L80-.L76
	.long	.L79-.L76
	.long	.L78-.L76
	.long	.L77-.L76
	.long	.L75-.L76
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.p2align 4,,10
	.p2align 3
.L138:
	xorl	%ecx, %ecx
	movl	$2, %esi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L140:
	movl	$4, %edx
	movl	$4, %esi
.L113:
	movq	-144(%rbp), %rax
	movzwl	16(%rax), %eax
	subw	$688, %ax
	cmpw	$10, %ax
	ja	.L42
	leaq	.L120(%rip), %rcx
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.align 4
	.align 4
.L120:
	.long	.L130-.L120
	.long	.L129-.L120
	.long	.L128-.L120
	.long	.L127-.L120
	.long	.L126-.L120
	.long	.L125-.L120
	.long	.L124-.L120
	.long	.L123-.L120
	.long	.L122-.L120
	.long	.L121-.L120
	.long	.L119-.L120
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.p2align 4,,10
	.p2align 3
.L115:
	movl	$1, %edx
	movl	$3, %esi
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L47:
	movl	$1, %ecx
	movl	$3, %esi
.L48:
	movzwl	16(%r14), %eax
	subw	$688, %ax
	cmpw	$10, %ax
	ja	.L42
	leaq	.L53(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.align 4
	.align 4
.L53:
	.long	.L63-.L53
	.long	.L62-.L53
	.long	.L61-.L53
	.long	.L60-.L53
	.long	.L59-.L53
	.long	.L58-.L53
	.long	.L57-.L53
	.long	.L56-.L53
	.long	.L55-.L53
	.long	.L54-.L53
	.long	.L52-.L53
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.p2align 4,,10
	.p2align 3
.L137:
	xorl	%ecx, %ecx
	movl	$2, %esi
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$1, %ecx
	movl	$1, %esi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L74:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$1, %ecx
	movl	$1, %esi
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L50:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L91:
	movl	$4, %ecx
	movl	$4, %esi
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L96:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L69:
	movl	$4, %ecx
	movl	$4, %esi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$1, %ecx
	movl	$1, %esi
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L116:
	xorl	%edx, %edx
	movl	$2, %esi
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L117:
	movl	$1, %edx
	movl	$1, %esi
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L118:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$4, %ecx
	movl	$4, %esi
	jmp	.L48
.L99:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23SpeculativeNumberDivideENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L110:
	movq	-136(%rbp), %xmm0
	movq	8(%r12), %rax
	movq	%rbx, %xmm3
	movhps	-144(%rbp), %xmm0
	movq	(%rax), %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-128(%rbp), %xmm0
	punpcklqdq	%xmm3, %xmm0
.L152:
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$4, %edx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %xmm0
.L67:
	movq	%xmm0, %rax
	testq	%rax, %rax
	je	.L51
.L141:
	punpcklqdq	%xmm0, %xmm0
	movl	$1, 0(%r13)
	movq	%rbx, 24(%r13)
	movups	%xmm0, 8(%r13)
	jmp	.L33
.L100:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeNumberMultiplyENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L110
.L101:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	testb	%cl, %cl
	jne	.L111
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder30SpeculativeSafeIntegerSubtractENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L110
.L77:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23SpeculativeNumberDivideENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L88:
	movq	-136(%rbp), %xmm0
	movq	8(%r12), %rax
	movq	%rbx, %xmm2
	movhps	-144(%rbp), %xmm0
	movq	(%rax), %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-128(%rbp), %xmm0
	punpcklqdq	%xmm2, %xmm0
	jmp	.L152
.L122:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeNumberMultiplyENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L132:
	movq	-136(%rbp), %xmm0
	movq	8(%r12), %rax
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rbx, %xmm4
	movl	$4, %edx
	movhps	-152(%rbp), %xmm0
	movq	(%rax), %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-128(%rbp), %xmm0
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %xmm0
	testq	%rax, %rax
	jne	.L141
.L112:
	movq	24(%r12), %rsi
	movq	8(%r15), %rdx
	movq	%r14, %rdi
	movl	16(%rbp), %ecx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%r12), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker29GetFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	cmpb	$7, %al
	jne	.L51
	movq	-136(%rbp), %xmm0
	movq	8(%r12), %rax
	xorl	%esi, %esi
	movq	376(%rax), %rdi
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23SpeculativeBigIntNegateENS1_19BigIntOperationHintE@PLT
	movdqa	-128(%rbp), %xmm0
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	8(%r12), %rax
	movl	$3, %edx
	movq	(%rax), %rdi
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %xmm0
	jmp	.L67
.L123:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	testb	%dl, %dl
	jne	.L133
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder30SpeculativeSafeIntegerSubtractENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L132
.L121:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23SpeculativeNumberDivideENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L132
.L124:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	testb	%dl, %dl
	jne	.L131
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeSafeIntegerAddENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L132
.L125:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder34SpeculativeNumberShiftRightLogicalENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L132
.L126:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27SpeculativeNumberShiftRightENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L132
.L127:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26SpeculativeNumberShiftLeftENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L132
.L128:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27SpeculativeNumberBitwiseAndENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L132
.L129:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27SpeculativeNumberBitwiseXorENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L132
.L130:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26SpeculativeNumberBitwiseOrENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L132
.L119:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder24SpeculativeNumberModulusENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L132
.L57:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	testb	%cl, %cl
	jne	.L64
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeSafeIntegerAddENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L65:
	movq	-136(%rbp), %xmm0
	movq	8(%r12), %rax
	movq	%rbx, %xmm1
	movhps	-144(%rbp), %xmm0
	movq	(%rax), %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-128(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm0
	jmp	.L152
.L58:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder34SpeculativeNumberShiftRightLogicalENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L65
.L61:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27SpeculativeNumberBitwiseAndENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L65
.L62:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27SpeculativeNumberBitwiseXorENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L65
.L63:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26SpeculativeNumberBitwiseOrENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L65
.L52:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder24SpeculativeNumberModulusENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L65
.L59:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27SpeculativeNumberShiftRightENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L65
.L60:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26SpeculativeNumberShiftLeftENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L65
.L54:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23SpeculativeNumberDivideENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L65
.L102:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	testb	%cl, %cl
	jne	.L109
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeSafeIntegerAddENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L110
.L103:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder34SpeculativeNumberShiftRightLogicalENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L110
.L104:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27SpeculativeNumberShiftRightENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L110
.L105:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26SpeculativeNumberShiftLeftENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L110
.L78:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeNumberMultiplyENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L88
.L79:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	testb	%cl, %cl
	jne	.L89
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder30SpeculativeSafeIntegerSubtractENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L88
.L80:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	testb	%cl, %cl
	jne	.L87
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeSafeIntegerAddENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L88
.L55:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeNumberMultiplyENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L65
.L82:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27SpeculativeNumberShiftRightENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L88
.L83:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26SpeculativeNumberShiftLeftENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L88
.L84:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27SpeculativeNumberBitwiseAndENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L88
.L85:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27SpeculativeNumberBitwiseXorENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L88
.L81:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder34SpeculativeNumberShiftRightLogicalENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L88
.L106:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27SpeculativeNumberBitwiseAndENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L110
.L107:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27SpeculativeNumberBitwiseXorENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L110
.L108:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26SpeculativeNumberBitwiseOrENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L110
.L97:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder24SpeculativeNumberModulusENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L110
.L86:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26SpeculativeNumberBitwiseOrENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L88
.L75:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder24SpeculativeNumberModulusENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L88
.L56:
	movq	8(%r12), %rax
	movq	376(%rax), %rdi
	testb	%cl, %cl
	jne	.L66
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder30SpeculativeSafeIntegerSubtractENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L65
.L87:
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20SpeculativeNumberAddENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L88
.L66:
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeNumberSubtractENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L65
.L109:
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20SpeculativeNumberAddENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L110
.L89:
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeNumberSubtractENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L88
.L111:
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeNumberSubtractENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L110
.L133:
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeNumberSubtractENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L132
.L131:
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20SpeculativeNumberAddENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L132
.L64:
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20SpeculativeNumberAddENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L65
.L153:
	call	__stack_chk_fail@PLT
.L42:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE11113:
	.size	_ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE, .-_ZNK2v88internal8compiler18JSTypeHintLowering20ReduceUnaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE
	.type	_ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE, @function
_ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE:
.LFB11114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -128(%rbp)
	movq	16(%rbp), %r14
	movq	%r8, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdx), %eax
	subw	$682, %ax
	cmpw	$19, %ax
	ja	.L211
	movq	%rdx, %r11
	movzwl	%ax, %eax
	leaq	.L157(%rip), %rdx
	movq	%rdi, %r12
	movslq	(%rdx,%rax,4), %rax
	movq	%rsi, %rbx
	movq	%r9, %r13
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE,"a",@progbits
	.align 4
	.align 4
.L157:
	.long	.L160-.L157
	.long	.L156-.L157
	.long	.L160-.L157
	.long	.L160-.L157
	.long	.L160-.L157
	.long	.L160-.L157
	.long	.L159-.L157
	.long	.L159-.L157
	.long	.L159-.L157
	.long	.L159-.L157
	.long	.L159-.L157
	.long	.L159-.L157
	.long	.L159-.L157
	.long	.L159-.L157
	.long	.L159-.L157
	.long	.L159-.L157
	.long	.L159-.L157
	.long	.L158-.L157
	.long	.L211-.L157
	.long	.L156-.L157
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE
	.p2align 4,,10
	.p2align 3
.L156:
	testb	$2, 16(%rsi)
	je	.L163
	movq	24(%rsi), %rsi
	leaq	-112(%rbp), %r15
	movq	32(%rbx), %rdx
	movl	24(%rbp), %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker22FeedbackIsInsufficientERKNS1_14FeedbackSourceE@PLT
	testb	%al, %al
	je	.L163
	movl	$12, %ecx
.L264:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0
	testq	%rax, %rax
	je	.L163
.L261:
	movl	$2, (%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, 24(%r12)
	movups	%xmm0, 8(%r12)
.L154:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L270
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	leaq	24(%rsi), %rax
	leaq	-112(%rbp), %r15
	movq	%rax, -152(%rbp)
	testb	$2, 16(%rsi)
	je	.L183
	movq	24(%rsi), %rsi
	movq	32(%rbx), %rdx
	leaq	-112(%rbp), %r15
	movq	%r11, -152(%rbp)
	movl	24(%rbp), %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker22FeedbackIsInsufficientERKNS1_14FeedbackSourceE@PLT
	movq	-152(%rbp), %r11
	testb	%al, %al
	je	.L183
	movl	$11, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0
	movq	-152(%rbp), %r11
	testq	%rax, %rax
	jne	.L261
	.p2align 4,,10
	.p2align 3
.L183:
	movq	24(%rbx), %rsi
	movl	24(%rbp), %ecx
	movq	%r15, %rdi
	movq	%r11, -152(%rbp)
	movq	32(%rbx), %rdx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker29GetFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	-152(%rbp), %r11
	cmpb	$5, %al
	ja	.L186
	leaq	.L188(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE
	.align 4
	.align 4
.L188:
	.long	.L186-.L188
	.long	.L192-.L188
	.long	.L191-.L188
	.long	.L190-.L188
	.long	.L189-.L188
	.long	.L218-.L188
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE
	.p2align 4,,10
	.p2align 3
.L160:
	leaq	24(%rsi), %r8
	leaq	-112(%rbp), %r15
	testb	$2, 16(%rsi)
	je	.L168
	movq	8(%r8), %rdx
	movq	24(%rsi), %rsi
	leaq	-112(%rbp), %r15
	movq	%r11, -160(%rbp)
	movl	24(%rbp), %ecx
	movq	%r15, %rdi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker22FeedbackIsInsufficientERKNS1_14FeedbackSourceE@PLT
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %r11
	testb	%al, %al
	je	.L168
	movl	$12, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %r11
	testq	%rax, %rax
	jne	.L261
	.p2align 4,,10
	.p2align 3
.L168:
	movq	24(%rbx), %rsi
	movl	24(%rbp), %ecx
	movq	%r15, %rdi
	movq	%r11, -152(%rbp)
	movq	8(%r8), %rdx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker30GetFeedbackForCompareOperationERKNS1_14FeedbackSourceE@PLT
	movq	-152(%rbp), %r11
	cmpb	$2, %al
	je	.L216
	cmpb	$3, %al
	je	.L217
	xorl	%esi, %esi
	cmpb	$1, %al
	jne	.L163
.L171:
	movzwl	16(%r11), %eax
	subw	$682, %ax
	cmpw	$5, %ax
	ja	.L211
	leaq	.L174(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE
	.align 4
	.align 4
.L174:
	.long	.L178-.L174
	.long	.L211-.L174
	.long	.L177-.L174
	.long	.L176-.L174
	.long	.L175-.L174
	.long	.L173-.L174
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE
	.p2align 4,,10
	.p2align 3
.L158:
	testb	$2, 16(%rsi)
	je	.L163
	movq	24(%rsi), %rsi
	leaq	-112(%rbp), %r15
	movq	32(%rbx), %rdx
	movl	24(%rbp), %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker22FeedbackIsInsufficientERKNS1_14FeedbackSourceE@PLT
	testb	%al, %al
	je	.L163
	movl	$11, %ecx
	jmp	.L264
.L271:
	jbe	.L163
	cmpb	$8, %al
	jne	.L211
	.p2align 4,,10
	.p2align 3
.L163:
	pxor	%xmm0, %xmm0
	movl	$0, (%r12)
	movq	$0, 24(%r12)
	movups	%xmm0, 8(%r12)
	jmp	.L154
.L211:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L173:
	movq	8(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder32SpeculativeNumberLessThanOrEqualENS1_19NumberOperationHintE@PLT
.L259:
	movq	-144(%rbp), %rcx
	movq	%rax, %rsi
	movq	-128(%rbp), %rax
	movq	%rcx, -128(%rbp)
	movq	%rax, -144(%rbp)
.L179:
	movq	8(%rbx), %rax
	movq	%r14, %xmm2
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-128(%rbp), %xmm0
	movl	$4, %edx
	movq	(%rax), %rdi
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r13, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %xmm0
	testq	%rax, %rax
	je	.L163
.L262:
	punpcklqdq	%xmm0, %xmm0
	movl	$1, (%r12)
	movq	%r14, 24(%r12)
	movups	%xmm0, 8(%r12)
	jmp	.L154
.L175:
	movq	8(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder32SpeculativeNumberLessThanOrEqualENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L179
.L176:
	movq	8(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeNumberLessThanENS1_19NumberOperationHintE@PLT
	jmp	.L259
.L177:
	movq	8(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeNumberLessThanENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L179
.L178:
	movq	8(%rbx), %rax
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder22SpeculativeNumberEqualENS1_19NumberOperationHintE@PLT
	movq	%rax, %rsi
	jmp	.L179
.L218:
	movl	$4, %ecx
	movl	$4, %esi
.L187:
	movzwl	16(%r11), %eax
	subw	$688, %ax
	cmpw	$10, %ax
	ja	.L211
	leaq	.L194(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE
	.align 4
	.align 4
.L194:
	.long	.L204-.L194
	.long	.L203-.L194
	.long	.L202-.L194
	.long	.L201-.L194
	.long	.L200-.L194
	.long	.L199-.L194
	.long	.L198-.L194
	.long	.L197-.L194
	.long	.L196-.L194
	.long	.L195-.L194
	.long	.L193-.L194
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE
.L189:
	movl	$1, %ecx
	movl	$3, %esi
	jmp	.L187
.L191:
	movl	$1, %ecx
	movl	$1, %esi
	jmp	.L187
.L192:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	jmp	.L187
.L190:
	xorl	%ecx, %ecx
	movl	$2, %esi
	jmp	.L187
.L195:
	movq	8(%rbx), %rax
	movq	%r11, -152(%rbp)
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder23SpeculativeNumberDivideENS1_19NumberOperationHintE@PLT
	movq	-152(%rbp), %r11
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L206:
	movq	8(%rbx), %rax
	movq	%r14, %xmm3
	xorl	%r8d, %r8d
	leaq	-96(%rbp), %rcx
	movq	-128(%rbp), %xmm0
	movl	$4, %edx
	movq	%r11, -152(%rbp)
	movq	(%rax), %rdi
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r13, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-152(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %xmm0
	jne	.L262
.L186:
	cmpw	$694, 16(%r11)
	jne	.L163
	movq	24(%rbx), %rsi
	movl	24(%rbp), %ecx
	movq	%r15, %rdi
	movq	%r11, -152(%rbp)
	movq	32(%rbx), %rdx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker29GetFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	movq	-152(%rbp), %r11
	cmpb	$7, %al
	jne	.L271
	cmpw	$694, 16(%r11)
	jne	.L211
	movq	8(%rbx), %rax
	movq	%r14, %xmm4
	movq	%r13, %xmm1
	xorl	%esi, %esi
	movq	-128(%rbp), %xmm0
	punpcklqdq	%xmm4, %xmm1
	movq	376(%rax), %rdi
	movaps	%xmm1, -128(%rbp)
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20SpeculativeBigIntAddENS1_19BigIntOperationHintE@PLT
	movdqa	-144(%rbp), %xmm0
	xorl	%r8d, %r8d
	leaq	-96(%rbp), %rcx
	movq	%rax, %rsi
	movq	8(%rbx), %rax
	movdqa	-128(%rbp), %xmm1
	movl	$4, %edx
	movq	(%rax), %rdi
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	testq	%rax, %rax
	je	.L163
	movq	%rax, %xmm0
	movl	$1, (%r12)
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, 24(%r12)
	movups	%xmm0, 8(%r12)
	jmp	.L154
.L196:
	movq	8(%rbx), %rax
	movq	%r11, -152(%rbp)
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeNumberMultiplyENS1_19NumberOperationHintE@PLT
	movq	-152(%rbp), %r11
	movq	%rax, %rsi
	jmp	.L206
.L197:
	movq	8(%rbx), %rax
	movq	%r11, -152(%rbp)
	movq	376(%rax), %rdi
	testb	%cl, %cl
	jne	.L207
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder30SpeculativeSafeIntegerSubtractENS1_19NumberOperationHintE@PLT
	movq	-152(%rbp), %r11
	movq	%rax, %rsi
	jmp	.L206
.L198:
	movq	8(%rbx), %rax
	movq	%r11, -152(%rbp)
	movq	376(%rax), %rdi
	testb	%cl, %cl
	jne	.L205
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeSafeIntegerAddENS1_19NumberOperationHintE@PLT
	movq	-152(%rbp), %r11
	movq	%rax, %rsi
	jmp	.L206
.L202:
	movq	8(%rbx), %rax
	movq	%r11, -152(%rbp)
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27SpeculativeNumberBitwiseAndENS1_19NumberOperationHintE@PLT
	movq	-152(%rbp), %r11
	movq	%rax, %rsi
	jmp	.L206
.L200:
	movq	8(%rbx), %rax
	movq	%r11, -152(%rbp)
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27SpeculativeNumberShiftRightENS1_19NumberOperationHintE@PLT
	movq	-152(%rbp), %r11
	movq	%rax, %rsi
	jmp	.L206
.L201:
	movq	8(%rbx), %rax
	movq	%r11, -152(%rbp)
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26SpeculativeNumberShiftLeftENS1_19NumberOperationHintE@PLT
	movq	-152(%rbp), %r11
	movq	%rax, %rsi
	jmp	.L206
.L199:
	movq	8(%rbx), %rax
	movq	%r11, -152(%rbp)
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder34SpeculativeNumberShiftRightLogicalENS1_19NumberOperationHintE@PLT
	movq	-152(%rbp), %r11
	movq	%rax, %rsi
	jmp	.L206
.L204:
	movq	8(%rbx), %rax
	movq	%r11, -152(%rbp)
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder26SpeculativeNumberBitwiseOrENS1_19NumberOperationHintE@PLT
	movq	-152(%rbp), %r11
	movq	%rax, %rsi
	jmp	.L206
.L193:
	movq	8(%rbx), %rax
	movq	%r11, -152(%rbp)
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder24SpeculativeNumberModulusENS1_19NumberOperationHintE@PLT
	movq	-152(%rbp), %r11
	movq	%rax, %rsi
	jmp	.L206
.L203:
	movq	8(%rbx), %rax
	movq	%r11, -152(%rbp)
	movq	376(%rax), %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder27SpeculativeNumberBitwiseXorENS1_19NumberOperationHintE@PLT
	movq	-152(%rbp), %r11
	movq	%rax, %rsi
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L217:
	movl	$4, %esi
	jmp	.L171
.L216:
	movl	$3, %esi
	jmp	.L171
.L207:
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder25SpeculativeNumberSubtractENS1_19NumberOperationHintE@PLT
	movq	-152(%rbp), %r11
	movq	%rax, %rsi
	jmp	.L206
.L205:
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder20SpeculativeNumberAddENS1_19NumberOperationHintE@PLT
	movq	-152(%rbp), %r11
	movq	%rax, %rsi
	jmp	.L206
.L270:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11114:
	.size	_ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE, .-_ZNK2v88internal8compiler18JSTypeHintLowering21ReduceBinaryOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering24ReduceForInNextOperationEPNS1_4NodeES4_S4_S4_S4_S4_NS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceForInNextOperationEPNS1_4NodeES4_S4_S4_S4_S4_NS0_12FeedbackSlotE
	.type	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceForInNextOperationEPNS1_4NodeES4_S4_S4_S4_S4_NS0_12FeedbackSlotE, @function
_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceForInNextOperationEPNS1_4NodeES4_S4_S4_S4_S4_NS0_12FeedbackSlotE:
.LFB11115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %r15
	movq	24(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$2, 16(%rsi)
	je	.L274
	movq	%rsi, %r13
	leaq	-80(%rbp), %r14
	movq	24(%rsi), %rsi
	movl	32(%rbp), %ecx
	movq	32(%r13), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	0(%r13), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker22FeedbackIsInsufficientERKNS1_14FeedbackSourceE@PLT
	testb	%al, %al
	je	.L274
	movl	$10, %ecx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0
	testq	%rax, %rax
	je	.L274
	movl	$2, (%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, 24(%r12)
	movups	%xmm0, 8(%r12)
.L272:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L285
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movl	$0, (%r12)
	movq	$0, 24(%r12)
	movups	%xmm0, 8(%r12)
	jmp	.L272
.L285:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11115:
	.size	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceForInNextOperationEPNS1_4NodeES4_S4_S4_S4_S4_NS0_12FeedbackSlotE, .-_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceForInNextOperationEPNS1_4NodeES4_S4_S4_S4_S4_NS0_12FeedbackSlotE
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering27ReduceForInPrepareOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18JSTypeHintLowering27ReduceForInPrepareOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE
	.type	_ZNK2v88internal8compiler18JSTypeHintLowering27ReduceForInPrepareOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE, @function
_ZNK2v88internal8compiler18JSTypeHintLowering27ReduceForInPrepareOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE:
.LFB11116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$2, 16(%rsi)
	je	.L288
	movq	%rsi, %r13
	leaq	-80(%rbp), %r15
	movq	24(%rsi), %rsi
	movq	%rcx, %r14
	movq	32(%r13), %rdx
	movq	%r15, %rdi
	movl	%r9d, %ecx
	movq	%r8, %rbx
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	0(%r13), %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker22FeedbackIsInsufficientERKNS1_14FeedbackSourceE@PLT
	testb	%al, %al
	je	.L288
	movl	$10, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0
	testq	%rax, %rax
	je	.L288
	movl	$2, (%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, 24(%r12)
	movups	%xmm0, 8(%r12)
.L286:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L299
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movl	$0, (%r12)
	movq	$0, 24(%r12)
	movups	%xmm0, 8(%r12)
	jmp	.L286
.L299:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11116:
	.size	_ZNK2v88internal8compiler18JSTypeHintLowering27ReduceForInPrepareOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE, .-_ZNK2v88internal8compiler18JSTypeHintLowering27ReduceForInPrepareOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering23ReduceToNumberOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18JSTypeHintLowering23ReduceToNumberOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE
	.type	_ZNK2v88internal8compiler18JSTypeHintLowering23ReduceToNumberOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE, @function
_ZNK2v88internal8compiler18JSTypeHintLowering23ReduceToNumberOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE:
.LFB11117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%rdx, -112(%rbp)
	movq	24(%rsi), %rsi
	movq	%rcx, -120(%rbp)
	movq	32(%rbx), %rdx
	movl	%r9d, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12JSHeapBroker29GetFeedbackForBinaryOperationERKNS1_14FeedbackSourceE@PLT
	leal	-1(%rax), %esi
	cmpb	$4, %sil
	ja	.L301
	movq	8(%rbx), %rax
	movq	-112(%rbp), %xmm0
	movq	%r14, %rdx
	movq	376(%rax), %rdi
	movq	(%rax), %r15
	movhps	-120(%rbp), %xmm0
	movq	$0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	$-1, -88(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder19SpeculativeToNumberENS1_19NumberOperationHintERKNS1_14FeedbackSourceE@PLT
	movdqa	-112(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movl	$3, %edx
	movq	%r13, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, (%r12)
	movq	%rax, %xmm0
	movq	%r13, 24(%r12)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
.L300:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L305
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movl	$0, (%r12)
	movq	$0, 24(%r12)
	movups	%xmm0, 8(%r12)
	jmp	.L300
.L305:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11117:
	.size	_ZNK2v88internal8compiler18JSTypeHintLowering23ReduceToNumberOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE, .-_ZNK2v88internal8compiler18JSTypeHintLowering23ReduceToNumberOperationEPNS1_4NodeES4_S4_NS0_12FeedbackSlotE
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering19ReduceCallOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18JSTypeHintLowering19ReduceCallOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE
	.type	_ZNK2v88internal8compiler18JSTypeHintLowering19ReduceCallOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE, @function
_ZNK2v88internal8compiler18JSTypeHintLowering19ReduceCallOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE:
.LFB11118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$2, 16(%rsi)
	je	.L308
	movq	%rsi, %r13
	leaq	-80(%rbp), %r15
	movq	24(%rsi), %rsi
	movl	24(%rbp), %ecx
	movq	32(%r13), %rdx
	movq	%r15, %rdi
	movq	%r9, %r14
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	0(%r13), %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker22FeedbackIsInsufficientERKNS1_14FeedbackSourceE@PLT
	testb	%al, %al
	je	.L308
	movl	$8, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0
	testq	%rax, %rax
	je	.L308
	movl	$2, (%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, 24(%r12)
	movups	%xmm0, 8(%r12)
.L306:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L319
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movl	$0, (%r12)
	movq	$0, 24(%r12)
	movups	%xmm0, 8(%r12)
	jmp	.L306
.L319:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11118:
	.size	_ZNK2v88internal8compiler18JSTypeHintLowering19ReduceCallOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE, .-_ZNK2v88internal8compiler18JSTypeHintLowering19ReduceCallOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering24ReduceConstructOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceConstructOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE
	.type	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceConstructOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE, @function
_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceConstructOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE:
.LFB11119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$2, 16(%rsi)
	je	.L322
	movq	%rsi, %r13
	leaq	-80(%rbp), %r15
	movq	24(%rsi), %rsi
	movl	24(%rbp), %ecx
	movq	32(%r13), %rdx
	movq	%r15, %rdi
	movq	%r9, %r14
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	0(%r13), %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker22FeedbackIsInsufficientERKNS1_14FeedbackSourceE@PLT
	testb	%al, %al
	je	.L322
	movl	$9, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0
	testq	%rax, %rax
	je	.L322
	movl	$2, (%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, 24(%r12)
	movups	%xmm0, 8(%r12)
.L320:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L333
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movl	$0, (%r12)
	movq	$0, 24(%r12)
	movups	%xmm0, 8(%r12)
	jmp	.L320
.L333:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11119:
	.size	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceConstructOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE, .-_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceConstructOperationEPKNS1_8OperatorEPKPNS1_4NodeEiS7_S7_NS0_12FeedbackSlotE
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.type	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE, @function
_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE:
.LFB11120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$2, 16(%rsi)
	je	.L336
	movq	%rsi, %r13
	leaq	-80(%rbp), %r15
	movq	24(%rsi), %rsi
	movl	16(%rbp), %ecx
	movq	32(%r13), %rdx
	movq	%r15, %rdi
	movq	%r8, %rbx
	movq	%r9, %r14
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	0(%r13), %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker22FeedbackIsInsufficientERKNS1_14FeedbackSourceE@PLT
	testb	%al, %al
	je	.L336
	movl	$13, %ecx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0
	testq	%rax, %rax
	je	.L336
	movl	$2, (%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, 24(%r12)
	movups	%xmm0, 8(%r12)
.L334:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L347
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movl	$0, (%r12)
	movq	$0, 24(%r12)
	movups	%xmm0, 8(%r12)
	jmp	.L334
.L347:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11120:
	.size	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE, .-_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_NS0_12FeedbackSlotE
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE
	.type	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE, @function
_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE:
.LFB11121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$2, 16(%rsi)
	je	.L350
	movq	%rsi, %r13
	leaq	-80(%rbp), %r15
	movq	24(%rsi), %rsi
	movl	24(%rbp), %ecx
	movq	32(%r13), %rdx
	movq	%r15, %rdi
	movq	%r9, %r14
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	0(%r13), %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker22FeedbackIsInsufficientERKNS1_14FeedbackSourceE@PLT
	testb	%al, %al
	je	.L350
	movl	$14, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0
	testq	%rax, %rax
	je	.L350
	movl	$2, (%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, 24(%r12)
	movups	%xmm0, 8(%r12)
.L348:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L361
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movl	$0, (%r12)
	movq	$0, 24(%r12)
	movups	%xmm0, 8(%r12)
	jmp	.L348
.L361:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11121:
	.size	_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE, .-_ZNK2v88internal8compiler18JSTypeHintLowering24ReduceLoadKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE
	.type	_ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE, @function
_ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE:
.LFB11122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$2, 16(%rsi)
	je	.L364
	movq	%rsi, %r13
	leaq	-80(%rbp), %r15
	movq	24(%rsi), %rsi
	movl	24(%rbp), %ecx
	movq	32(%r13), %rdx
	movq	%r15, %rdi
	movq	%r9, %r14
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	0(%r13), %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker22FeedbackIsInsufficientERKNS1_14FeedbackSourceE@PLT
	testb	%al, %al
	je	.L364
	movl	$13, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0
	testq	%rax, %rax
	je	.L364
	movl	$2, (%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, 24(%r12)
	movups	%xmm0, 8(%r12)
.L362:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L375
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movl	$0, (%r12)
	movq	$0, 24(%r12)
	movups	%xmm0, 8(%r12)
	jmp	.L362
.L375:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11122:
	.size	_ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE, .-_ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreNamedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_NS0_12FeedbackSlotE
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_S7_NS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_S7_NS0_12FeedbackSlotE
	.type	_ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_S7_NS0_12FeedbackSlotE, @function
_ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_S7_NS0_12FeedbackSlotE:
.LFB11123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %r15
	movq	24(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$2, 16(%rsi)
	je	.L378
	movq	%rsi, %r13
	leaq	-80(%rbp), %r14
	movq	24(%rsi), %rsi
	movl	32(%rbp), %ecx
	movq	32(%r13), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	0(%r13), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker22FeedbackIsInsufficientERKNS1_14FeedbackSourceE@PLT
	testb	%al, %al
	je	.L378
	movl	$14, %ecx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0
	testq	%rax, %rax
	je	.L378
	movl	$2, (%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, 24(%r12)
	movups	%xmm0, 8(%r12)
.L376:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L389
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movl	$0, (%r12)
	movq	$0, 24(%r12)
	movups	%xmm0, 8(%r12)
	jmp	.L376
.L389:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11123:
	.size	_ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_S7_NS0_12FeedbackSlotE, .-_ZNK2v88internal8compiler18JSTypeHintLowering25ReduceStoreKeyedOperationEPKNS1_8OperatorEPNS1_4NodeES7_S7_S7_S7_NS0_12FeedbackSlotE
	.section	.text._ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE
	.type	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE, @function
_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE:
.LFB11124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$2, 16(%rdi)
	je	.L393
	movl	%r8d, %ebx
	movq	24(%rdi), %r8
	leaq	-80(%rbp), %r14
	movq	%rdx, %r13
	movq	32(%rdi), %rdx
	movq	%rdi, %r12
	movq	%rcx, %r15
	movq	%r14, %rdi
	movl	%esi, %ecx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE@PLT
	movq	(%r12), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler12JSHeapBroker22FeedbackIsInsufficientERKNS1_14FeedbackSourceE@PLT
	testb	%al, %al
	je	.L393
	movzbl	%bl, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE.part.0
.L390:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L399
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L390
.L399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11124:
	.size	_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE, .-_ZNK2v88internal8compiler18JSTypeHintLowering17TryBuildSoftDeoptENS0_12FeedbackSlotEPNS1_4NodeES5_NS0_16DeoptimizeReasonE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler18JSTypeHintLoweringC2EPNS1_12JSHeapBrokerEPNS1_7JSGraphENS1_17FeedbackVectorRefENS_4base5FlagsINS2_4FlagEiEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler18JSTypeHintLoweringC2EPNS1_12JSHeapBrokerEPNS1_7JSGraphENS1_17FeedbackVectorRefENS_4base5FlagsINS2_4FlagEiEE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler18JSTypeHintLoweringC2EPNS1_12JSHeapBrokerEPNS1_7JSGraphENS1_17FeedbackVectorRefENS_4base5FlagsINS2_4FlagEiEE:
.LFB12868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12868:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler18JSTypeHintLoweringC2EPNS1_12JSHeapBrokerEPNS1_7JSGraphENS1_17FeedbackVectorRefENS_4base5FlagsINS2_4FlagEiEE, .-_GLOBAL__sub_I__ZN2v88internal8compiler18JSTypeHintLoweringC2EPNS1_12JSHeapBrokerEPNS1_7JSGraphENS1_17FeedbackVectorRefENS_4base5FlagsINS2_4FlagEiEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler18JSTypeHintLoweringC2EPNS1_12JSHeapBrokerEPNS1_7JSGraphENS1_17FeedbackVectorRefENS_4base5FlagsINS2_4FlagEiEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-1074790400
	.align 8
.LC2:
	.long	0
	.long	1072693248
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
