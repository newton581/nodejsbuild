	.file	"graph.cc"
	.text
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB12198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE12198:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB12267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12267:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB12199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12199:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB12268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE12268:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal8compiler5GraphC2EPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5GraphC2EPNS0_4ZoneE
	.type	_ZN2v88internal8compiler5GraphC2EPNS0_4ZoneE, @function
_ZN2v88internal8compiler5GraphC2EPNS0_4ZoneE:
.LFB10357:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	%rsi, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	ret
	.cfi_endproc
.LFE10357:
	.size	_ZN2v88internal8compiler5GraphC2EPNS0_4ZoneE, .-_ZN2v88internal8compiler5GraphC2EPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler5GraphC1EPNS0_4ZoneE
	.set	_ZN2v88internal8compiler5GraphC1EPNS0_4ZoneE,_ZN2v88internal8compiler5GraphC2EPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler5Graph8DecorateEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5Graph8DecorateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler5Graph8DecorateEPNS1_4NodeE, @function
_ZN2v88internal8compiler5Graph8DecorateEPNS1_4NodeE:
.LFB10359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	40(%rdi), %rbx
	movq	48(%rdi), %r13
	cmpq	%r13, %rbx
	je	.L11
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L13:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	cmpq	%rbx, %r13
	jne	.L13
.L11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10359:
	.size	_ZN2v88internal8compiler5Graph8DecorateEPNS1_4NodeE, .-_ZN2v88internal8compiler5Graph8DecorateEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler5Graph12AddDecoratorEPNS1_14GraphDecoratorE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal8compiler5Graph12AddDecoratorEPNS1_14GraphDecoratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5Graph12AddDecoratorEPNS1_14GraphDecoratorE
	.type	_ZN2v88internal8compiler5Graph12AddDecoratorEPNS1_14GraphDecoratorE, @function
_ZN2v88internal8compiler5Graph12AddDecoratorEPNS1_14GraphDecoratorE:
.LFB10360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	48(%rdi), %r12
	cmpq	56(%rdi), %r12
	je	.L17
	movq	%rsi, (%r12)
	addq	$8, 48(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	40(%rdi), %r14
	movq	%r12, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L42
	testq	%rax, %rax
	je	.L29
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L43
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L20:
	movq	32(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L44
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L23:
	leaq	(%rax,%r15), %rsi
	leaq	8(%rax), %rdx
.L21:
	movq	%r13, (%rax,%rcx)
	cmpq	%r14, %r12
	je	.L24
	subq	$8, %r12
	leaq	15(%rax), %rdx
	subq	%r14, %r12
	subq	%r14, %rdx
	movq	%r12, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L32
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L32
	leaq	1(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L26:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L26
	movq	%rdi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %r14
	addq	%rax, %rcx
	cmpq	%rdx, %rdi
	je	.L28
	movq	(%r14), %rdx
	movq	%rdx, (%rcx)
.L28:
	leaq	16(%rax,%r12), %rdx
.L24:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rsi, 56(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 40(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L45
	movl	$8, %edx
	xorl	%esi, %esi
	xorl	%eax, %eax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L32:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%r14,%rdx,8), %rdi
	movq	%rdi, (%rax,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rdi, %rcx
	jne	.L25
	jmp	.L28
.L44:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L23
.L42:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L45:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L20
	.cfi_endproc
.LFE10360:
	.size	_ZN2v88internal8compiler5Graph12AddDecoratorEPNS1_14GraphDecoratorE, .-_ZN2v88internal8compiler5Graph12AddDecoratorEPNS1_14GraphDecoratorE
	.section	.text._ZN2v88internal8compiler5Graph15RemoveDecoratorEPNS1_14GraphDecoratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5Graph15RemoveDecoratorEPNS1_14GraphDecoratorE
	.type	_ZN2v88internal8compiler5Graph15RemoveDecoratorEPNS1_14GraphDecoratorE, @function
_ZN2v88internal8compiler5Graph15RemoveDecoratorEPNS1_14GraphDecoratorE:
.LFB10361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %rdx
	movq	40(%rdi), %rdi
	movq	%rdx, %rax
	subq	%rdi, %rax
	movq	%rax, %rsi
	sarq	$5, %rax
	sarq	$3, %rsi
	testq	%rax, %rax
	jle	.L47
	salq	$5, %rax
	addq	%rdi, %rax
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L69:
	cmpq	8(%rdi), %rcx
	je	.L65
	cmpq	16(%rdi), %rcx
	je	.L66
	cmpq	24(%rdi), %rcx
	je	.L67
	addq	$32, %rdi
	cmpq	%rax, %rdi
	je	.L68
.L52:
	leaq	8(%rdi), %rsi
	cmpq	(%rdi), %rcx
	jne	.L69
.L48:
	cmpq	%rsi, %rdx
	je	.L57
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	48(%rbx), %rsi
.L57:
	subq	$8, %rsi
	movq	%rsi, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	%rdx, %rsi
	subq	%rdi, %rsi
	sarq	$3, %rsi
.L47:
	cmpq	$2, %rsi
	je	.L58
	cmpq	$3, %rsi
	je	.L54
	cmpq	$1, %rsi
	je	.L55
.L56:
	leaq	8(%rdx), %rsi
	movq	%rdx, %rdi
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rdi, %rsi
.L53:
	leaq	8(%rsi), %rdi
	cmpq	(%rsi), %rcx
	je	.L59
.L55:
	leaq	8(%rdi), %rsi
	cmpq	(%rdi), %rcx
	jne	.L56
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L54:
	leaq	8(%rdi), %rsi
	cmpq	(%rdi), %rcx
	jne	.L53
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L65:
	leaq	16(%rdi), %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	24(%rdi), %rsi
	addq	$16, %rdi
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	32(%rdi), %rsi
	addq	$24, %rdi
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%rdi, %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	.L48
	.cfi_endproc
.LFE10361:
	.size	_ZN2v88internal8compiler5Graph15RemoveDecoratorEPNS1_14GraphDecoratorE, .-_ZN2v88internal8compiler5Graph15RemoveDecoratorEPNS1_14GraphDecoratorE
	.section	.text._ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	.type	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb, @function
_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb:
.LFB10362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movzbl	%r8b, %r9d
	movq	%rcx, %r8
	movl	%edx, %ecx
	movq	%r10, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	28(%rdi), %esi
	leal	1(%rsi), %eax
	movl	%eax, 28(%rdi)
	movq	(%rdi), %rdi
	call	_ZN2v88internal8compiler4Node3NewEPNS0_4ZoneEjPKNS1_8OperatorEiPKPS2_b@PLT
	movq	40(%rbx), %rdx
	movq	48(%rbx), %r13
	movq	%rax, %r12
	cmpq	%r13, %rdx
	je	.L70
	movq	%rdx, %rbx
	.p2align 4,,10
	.p2align 3
.L72:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	cmpq	%rbx, %r13
	jne	.L72
.L70:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10362:
	.size	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb, .-_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb
	.section	.text._ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb
	.type	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb, @function
_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb:
.LFB10363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movzbl	%r8b, %r9d
	movq	%rcx, %r8
	movl	%edx, %ecx
	movq	%r10, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	28(%rdi), %esi
	leal	1(%rsi), %eax
	movl	%eax, 28(%rdi)
	movq	(%rdi), %rdi
	call	_ZN2v88internal8compiler4Node3NewEPNS0_4ZoneEjPKNS1_8OperatorEiPKPS2_b@PLT
	movq	40(%r12), %rbx
	movq	48(%r12), %r12
	movq	%rax, %r13
	cmpq	%r12, %rbx
	je	.L75
	.p2align 4,,10
	.p2align 3
.L77:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	movq	%r13, %rsi
	movq	(%rdi), %rdx
	call	*16(%rdx)
	cmpq	%rbx, %r12
	jne	.L77
.L75:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10363:
	.size	_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb, .-_ZN2v88internal8compiler5Graph16NewNodeUncheckedEPKNS1_8OperatorEiPKPNS1_4NodeEb
	.section	.text._ZN2v88internal8compiler5Graph9CloneNodeEPKNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler5Graph9CloneNodeEPKNS1_4NodeE
	.type	_ZN2v88internal8compiler5Graph9CloneNodeEPKNS1_4NodeE, @function
_ZN2v88internal8compiler5Graph9CloneNodeEPKNS1_4NodeE:
.LFB10364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	28(%rdi), %esi
	leal	1(%rsi), %eax
	movl	%eax, 28(%rdi)
	movq	(%rdi), %rdi
	call	_ZN2v88internal8compiler4Node5CloneEPNS0_4ZoneEjPKS2_@PLT
	movq	40(%r12), %rbx
	movq	48(%r12), %r12
	movq	%rax, %r13
	cmpq	%r12, %rbx
	je	.L80
	.p2align 4,,10
	.p2align 3
.L82:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	movq	%r13, %rsi
	movq	(%rdi), %rdx
	call	*16(%rdx)
	cmpq	%rbx, %r12
	jne	.L82
.L80:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10364:
	.size	_ZN2v88internal8compiler5Graph9CloneNodeEPKNS1_4NodeE, .-_ZN2v88internal8compiler5Graph9CloneNodeEPKNS1_4NodeE
	.section	.text._ZNK2v88internal8compiler5Graph5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler5Graph5PrintEv
	.type	_ZNK2v88internal8compiler5Graph5PrintEv, @function
_ZNK2v88internal8compiler5Graph5PrintEv:
.LFB10366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-384(%rbp), %r14
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-304(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$368, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	movw	%ax, -80(%rbp)
	movq	%r13, -304(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r14, %rdi
	leaq	-392(%rbp), %rsi
	movq	%rax, -384(%rbp)
	addq	$40, %rax
	movq	%rax, -304(%rbp)
	movq	%rbx, -392(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_5AsRPOE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-320(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r12, %rdi
	movq	%r13, -304(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	addq	$368, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L88:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10366:
	.size	_ZNK2v88internal8compiler5Graph5PrintEv, .-_ZNK2v88internal8compiler5Graph5PrintEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler5GraphC2EPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler5GraphC2EPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler5GraphC2EPNS0_4ZoneE:
.LFB12233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12233:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler5GraphC2EPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler5GraphC2EPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler5GraphC2EPNS0_4ZoneE
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
