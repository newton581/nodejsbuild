	.file	"type-narrowing-reducer.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler20TypeNarrowingReducer12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"TypeNarrowingReducer"
	.section	.text._ZNK2v88internal8compiler20TypeNarrowingReducer12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler20TypeNarrowingReducer12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler20TypeNarrowingReducer12reducer_nameEv
	.type	_ZNK2v88internal8compiler20TypeNarrowingReducer12reducer_nameEv, @function
_ZNK2v88internal8compiler20TypeNarrowingReducer12reducer_nameEv:
.LFB10167:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10167:
	.size	_ZNK2v88internal8compiler20TypeNarrowingReducer12reducer_nameEv, .-_ZNK2v88internal8compiler20TypeNarrowingReducer12reducer_nameEv
	.section	.text._ZN2v88internal8compiler20TypeNarrowingReducerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20TypeNarrowingReducerD2Ev
	.type	_ZN2v88internal8compiler20TypeNarrowingReducerD2Ev, @function
_ZN2v88internal8compiler20TypeNarrowingReducerD2Ev:
.LFB10805:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10805:
	.size	_ZN2v88internal8compiler20TypeNarrowingReducerD2Ev, .-_ZN2v88internal8compiler20TypeNarrowingReducerD2Ev
	.globl	_ZN2v88internal8compiler20TypeNarrowingReducerD1Ev
	.set	_ZN2v88internal8compiler20TypeNarrowingReducerD1Ev,_ZN2v88internal8compiler20TypeNarrowingReducerD2Ev
	.section	.text._ZN2v88internal8compiler20TypeNarrowingReducerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20TypeNarrowingReducerD0Ev
	.type	_ZN2v88internal8compiler20TypeNarrowingReducerD0Ev, @function
_ZN2v88internal8compiler20TypeNarrowingReducerD0Ev:
.LFB10807:
	.cfi_startproc
	endbr64
	movl	$136, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10807:
	.size	_ZN2v88internal8compiler20TypeNarrowingReducerD0Ev, .-_ZN2v88internal8compiler20TypeNarrowingReducerD0Ev
	.section	.text._ZN2v88internal8compiler20TypeNarrowingReducer6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20TypeNarrowingReducer6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler20TypeNarrowingReducer6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler20TypeNarrowingReducer6ReduceEPNS1_4NodeE:
.LFB10808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rsi), %eax
	subl	$58, %eax
	cmpw	$196, %ax
	ja	.L145
	leaq	.L8(%rip), %rdx
	movzwl	%ax, %eax
	movq	%rdi, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler20TypeNarrowingReducer6ReduceEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L8:
	.long	.L61-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L60-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L59-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L58-.L8
	.long	.L57-.L8
	.long	.L56-.L8
	.long	.L55-.L8
	.long	.L54-.L8
	.long	.L53-.L8
	.long	.L52-.L8
	.long	.L51-.L8
	.long	.L50-.L8
	.long	.L49-.L8
	.long	.L48-.L8
	.long	.L47-.L8
	.long	.L46-.L8
	.long	.L45-.L8
	.long	.L44-.L8
	.long	.L43-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L42-.L8
	.long	.L41-.L8
	.long	.L40-.L8
	.long	.L39-.L8
	.long	.L38-.L8
	.long	.L37-.L8
	.long	.L36-.L8
	.long	.L35-.L8
	.long	.L34-.L8
	.long	.L33-.L8
	.long	.L32-.L8
	.long	.L31-.L8
	.long	.L30-.L8
	.long	.L29-.L8
	.long	.L28-.L8
	.long	.L27-.L8
	.long	.L26-.L8
	.long	.L25-.L8
	.long	.L24-.L8
	.long	.L23-.L8
	.long	.L22-.L8
	.long	.L21-.L8
	.long	.L20-.L8
	.long	.L19-.L8
	.long	.L18-.L8
	.long	.L17-.L8
	.long	.L16-.L8
	.long	.L15-.L8
	.long	.L14-.L8
	.long	.L13-.L8
	.long	.L12-.L8
	.long	.L11-.L8
	.long	.L10-.L8
	.long	.L9-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L145-.L8
	.long	.L7-.L8
	.section	.text._ZN2v88internal8compiler20TypeNarrowingReducer6ReduceEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L145:
	xorl	%eax, %eax
.L144:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L153
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L7:
	.cfi_restore_state
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L143
	movq	16(%rdx), %rdx
.L143:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper9ToBooleanENS1_4TypeE@PLT
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L73:
	movq	16(%r12), %rax
	movq	8(%rbx), %rsi
	movq	%r13, %rdi
	movq	%rsi, -64(%rbp)
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler4Type9IntersectES2_S2_PNS0_4ZoneE@PLT
	movq	%rax, %r12
	cmpq	-64(%rbp), %rax
	je	.L145
	leaq	-64(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L145
	movq	%r12, 8(%rbx)
	movq	%rbx, %rax
	jmp	.L144
.L9:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L142
	movq	16(%rdx), %rdx
.L142:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper16NumberSilenceNaNENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L10:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L141
	movq	16(%rdx), %rdx
.L141:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper20NumberToUint8ClampedENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L11:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L140
	movq	16(%rdx), %rdx
.L140:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper14NumberToUint32ENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L12:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L139
	movq	16(%rdx), %rdx
.L139:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper14NumberToStringENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L13:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L138
	movq	16(%rdx), %rdx
.L138:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper13NumberToInt32ENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L14:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L137
	movq	16(%rdx), %rdx
.L137:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper15NumberToBooleanENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L15:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L136
	movq	16(%rdx), %rdx
.L136:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper11NumberTruncENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L16:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L135
	movq	16(%rdx), %rdx
.L135:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper10NumberTanhENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L17:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L134
	movq	16(%rdx), %rdx
.L134:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper9NumberTanENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L18:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L133
	movq	16(%rdx), %rdx
.L133:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper10NumberSqrtENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L19:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L132
	movq	16(%rdx), %rdx
.L132:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper10NumberSinhENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L20:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L131
	movq	16(%rdx), %rdx
.L131:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper9NumberSinENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L21:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L130
	movq	16(%rdx), %rdx
.L130:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper10NumberSignENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L22:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L129
	movq	16(%rdx), %rdx
.L129:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper11NumberRoundENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L23:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L128
	movq	16(%rdx), %rdx
.L128:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper11NumberLog10ENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L24:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L127
	movq	16(%rdx), %rdx
.L127:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper10NumberLog2ENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L25:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L126
	movq	16(%rdx), %rdx
.L126:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper11NumberLog1pENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L26:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L125
	movq	16(%rdx), %rdx
.L125:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper9NumberLogENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L27:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L124
	movq	16(%rdx), %rdx
.L124:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper12NumberFroundENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L28:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L123
	movq	16(%rdx), %rdx
.L123:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper11NumberFloorENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L29:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L122
	movq	16(%rdx), %rdx
.L122:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper11NumberExpm1ENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L30:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L121
	movq	16(%rdx), %rdx
.L121:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper9NumberExpENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L31:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L120
	movq	16(%rdx), %rdx
.L120:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper10NumberCoshENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L32:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L119
	movq	16(%rdx), %rdx
.L119:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper9NumberCosENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L33:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L118
	movq	16(%rdx), %rdx
.L118:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper11NumberClz32ENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L34:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L117
	movq	16(%rdx), %rdx
.L117:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper10NumberCeilENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L35:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L116
	movq	16(%rdx), %rdx
.L116:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper10NumberCbrtENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L36:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L115
	movq	16(%rdx), %rdx
.L115:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper11NumberAtanhENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L37:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L114
	movq	16(%rdx), %rdx
.L114:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper10NumberAtanENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L38:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L113
	movq	16(%rdx), %rdx
.L113:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper11NumberAsinhENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L39:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L112
	movq	16(%rdx), %rdx
.L112:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper10NumberAsinENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L40:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L111
	movq	16(%rdx), %rdx
.L111:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper11NumberAcoshENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L41:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L110
	movq	16(%rdx), %rdx
.L110:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper10NumberAcosENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L42:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L109
	movq	16(%rdx), %rdx
.L109:
	movq	8(%rdx), %rsi
	call	_ZN2v88internal8compiler14OperationTyper9NumberAbsENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L43:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L105
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L106:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper9NumberPowENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L44:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L103
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L104:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper9NumberMinENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L45:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L101
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L102:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper9NumberMaxENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L46:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L99
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L100:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper10NumberImulENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L54:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L83
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L84:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper13NumberModulusENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L50:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L91
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L92:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper15NumberShiftLeftENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L58:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L75
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L76:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper9NumberAddENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L48:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L95
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L96:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper23NumberShiftRightLogicalENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L56:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L79
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L80:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper14NumberMultiplyENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L52:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L87
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L88:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper16NumberBitwiseXorENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L60:
	movzbl	23(%rbx), %edx
	movq	32(%rbx), %rax
	movl	$4294967295, %r13d
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L62
	movq	8(%rax), %rdx
	leaq	40(%rbx), %rax
.L63:
	movq	%rdx, -72(%rbp)
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	cmpq	$1119, %rdx
	je	.L64
	leaq	-72(%rbp), %rdi
	movl	$1119, %esi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L73
	movq	-64(%rbp), %rax
.L64:
	leaq	-64(%rbp), %r14
	cmpq	$1119, %rax
	je	.L70
	movl	$1119, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L73
.L70:
	leaq	-72(%rbp), %r15
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	comisd	-88(%rbp), %xmm0
	jbe	.L154
	movq	80(%r12), %r13
	jmp	.L73
.L47:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L97
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L98:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper11NumberAtan2ENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L55:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L81
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L82:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper12NumberDivideENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L51:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L89
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L90:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper16NumberBitwiseAndENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L59:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L107
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L108:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper9SameValueENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L49:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L93
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L94:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper16NumberShiftRightENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L57:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L77
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L78:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper14NumberSubtractENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L53:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L85
	movq	40(%rbx), %rax
	movq	8(%rax), %r8
.L86:
	movq	8(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14OperationTyper15NumberBitwiseOrENS1_4TypeES3_@PLT
	movq	%rax, %r13
	jmp	.L73
.L61:
	movzbl	23(%rbx), %eax
	movq	32(%rbx), %rdx
	leaq	24(%rdi), %rdi
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L74
	movq	16(%rdx), %rdx
.L74:
	movq	8(%rdx), %rdx
	call	_ZN2v88internal8compiler14OperationTyper13TypeTypeGuardEPKNS1_8OperatorENS1_4TypeE@PLT
	movq	%rax, %r13
	jmp	.L73
.L62:
	movq	16(%rax), %rdx
	addq	$24, %rax
	movq	8(%rdx), %rdx
	jmp	.L63
.L105:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L106
.L103:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L104
.L101:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L102
.L75:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L76
.L107:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L108
.L83:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L84
.L81:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L82
.L79:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L80
.L77:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L78
.L99:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L100
.L97:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L98
.L95:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L96
.L93:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L94
.L91:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L92
.L89:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L90
.L87:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L88
.L85:
	movq	24(%rdx), %rax
	movq	16(%rdx), %rdx
	movq	8(%rax), %r8
	jmp	.L86
.L154:
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movsd	-88(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L73
	movq	72(%r12), %r13
	jmp	.L73
.L153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10808:
	.size	_ZN2v88internal8compiler20TypeNarrowingReducer6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler20TypeNarrowingReducer6ReduceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler20TypeNarrowingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler20TypeNarrowingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.type	_ZN2v88internal8compiler20TypeNarrowingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, @function
_ZN2v88internal8compiler20TypeNarrowingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE:
.LFB10802:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movq	%rdx, 16(%rdi)
	movq	%rcx, %rsi
	addq	$24, %rdi
	leaq	16+_ZTVN2v88internal8compiler20TypeNarrowingReducerE(%rip), %rax
	movq	%r8, -16(%rdi)
	movq	%rax, -24(%rdi)
	movq	(%rdx), %rax
	movq	(%rax), %rdx
	jmp	_ZN2v88internal8compiler14OperationTyperC1EPNS1_12JSHeapBrokerEPNS0_4ZoneE@PLT
	.cfi_endproc
.LFE10802:
	.size	_ZN2v88internal8compiler20TypeNarrowingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, .-_ZN2v88internal8compiler20TypeNarrowingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.globl	_ZN2v88internal8compiler20TypeNarrowingReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.set	_ZN2v88internal8compiler20TypeNarrowingReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE,_ZN2v88internal8compiler20TypeNarrowingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.section	.text._ZNK2v88internal8compiler20TypeNarrowingReducer5graphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler20TypeNarrowingReducer5graphEv
	.type	_ZNK2v88internal8compiler20TypeNarrowingReducer5graphEv, @function
_ZNK2v88internal8compiler20TypeNarrowingReducer5graphEv:
.LFB10812:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE10812:
	.size	_ZNK2v88internal8compiler20TypeNarrowingReducer5graphEv, .-_ZNK2v88internal8compiler20TypeNarrowingReducer5graphEv
	.section	.text._ZNK2v88internal8compiler20TypeNarrowingReducer4zoneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler20TypeNarrowingReducer4zoneEv
	.type	_ZNK2v88internal8compiler20TypeNarrowingReducer4zoneEv, @function
_ZNK2v88internal8compiler20TypeNarrowingReducer4zoneEv:
.LFB10813:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE10813:
	.size	_ZNK2v88internal8compiler20TypeNarrowingReducer4zoneEv, .-_ZNK2v88internal8compiler20TypeNarrowingReducer4zoneEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler20TypeNarrowingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler20TypeNarrowingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler20TypeNarrowingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE:
.LFB12461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12461:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler20TypeNarrowingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, .-_GLOBAL__sub_I__ZN2v88internal8compiler20TypeNarrowingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler20TypeNarrowingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.weak	_ZTVN2v88internal8compiler20TypeNarrowingReducerE
	.section	.data.rel.ro._ZTVN2v88internal8compiler20TypeNarrowingReducerE,"awG",@progbits,_ZTVN2v88internal8compiler20TypeNarrowingReducerE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler20TypeNarrowingReducerE, @object
	.size	_ZTVN2v88internal8compiler20TypeNarrowingReducerE, 56
_ZTVN2v88internal8compiler20TypeNarrowingReducerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler20TypeNarrowingReducerD1Ev
	.quad	_ZN2v88internal8compiler20TypeNarrowingReducerD0Ev
	.quad	_ZNK2v88internal8compiler20TypeNarrowingReducer12reducer_nameEv
	.quad	_ZN2v88internal8compiler20TypeNarrowingReducer6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
