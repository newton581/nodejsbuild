	.file	"basic-block-instrumentor.cc"
	.text
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIPS4_EEvN9__gnu_cxx17__normal_iteratorIS9_S7_EET_SD_St20forward_iterator_tag.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_range_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIPS4_EEvN9__gnu_cxx17__normal_iteratorIS9_S7_EET_SD_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIPS4_EEvN9__gnu_cxx17__normal_iteratorIS9_S7_EET_SD_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIPS4_EEvN9__gnu_cxx17__normal_iteratorIS9_S7_EET_SD_St20forward_iterator_tag
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIPS4_EEvN9__gnu_cxx17__normal_iteratorIS9_S7_EET_SD_St20forward_iterator_tag, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIPS4_EEvN9__gnu_cxx17__normal_iteratorIS9_S7_EET_SD_St20forward_iterator_tag:
.LFB23001:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rdx
	je	.L101
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	subq	%rdx, %r14
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	movq	%r14, %rax
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	sarq	$3, %rax
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	24(%rbx), %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%rax, %rdx
	jb	.L4
	movq	%rdi, %r8
	subq	%rsi, %r8
	movq	%r8, %r9
	sarq	$3, %r9
	cmpq	%r9, %rax
	jnb	.L5
	movl	$16, %ecx
	movq	%rdi, %rdx
	leaq	-8(%r14), %rax
	subq	%r14, %rcx
	subq	%r14, %rdx
	shrq	$3, %rax
	testq	%rcx, %rcx
	leaq	16(%rdi), %rcx
	setle	%sil
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L43
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L43
	leaq	1(%rax), %r8
	xorl	%eax, %eax
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L7:
	movdqu	(%rdx,%rax), %xmm1
	movups	%xmm1, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L7
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	leaq	(%rdx,%rcx), %rsi
	addq	%rdi, %rcx
	cmpq	%rax, %r8
	je	.L9
	movq	(%rsi), %rax
	movq	%rax, (%rcx)
.L9:
	addq	%r14, 16(%rbx)
	cmpq	%rdx, %r12
	je	.L10
	subq	%r12, %rdx
	movq	%r12, %rsi
	subq	%rdx, %rdi
	call	memmove@PLT
.L10:
	movq	%r14, %rdx
.L104:
	addq	$16, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	movl	$268435455, %r14d
	movq	%r14, %rdx
	subq	%rsi, %rdi
	sarq	$3, %rdi
	subq	%rdi, %rdx
	cmpq	%rdx, %rax
	ja	.L106
	cmpq	%rdi, %rax
	cmovb	%rdi, %rax
	addq	%rdi, %rax
	jc	.L24
	testq	%rax, %rax
	jne	.L107
	xorl	%edi, %edi
	xorl	%eax, %eax
.L26:
	cmpq	%rsi, %r12
	je	.L49
	leaq	-8(%r12), %r10
	leaq	15(%rax), %rdx
	subq	%rsi, %r10
	subq	%rsi, %rdx
	movq	%r10, %r9
	shrq	$3, %r9
	cmpq	$30, %rdx
	jbe	.L50
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r9
	je	.L50
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %r8
	shrq	%r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L31:
	movdqu	(%rsi,%rdx), %xmm5
	movups	%xmm5, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%r8, %rdx
	jne	.L31
	movq	%r9, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %r8
	addq	%r8, %rsi
	addq	%rax, %r8
	cmpq	%rdx, %r9
	je	.L33
	movq	(%rsi), %rdx
	movq	%rdx, (%r8)
.L33:
	leaq	8(%rax,%r10), %rsi
.L29:
	leaq	-8(%rcx), %r8
	leaq	15(%r13), %rdx
	subq	%r13, %r8
	subq	%rsi, %rdx
	movq	%r8, %r9
	shrq	$3, %r9
	cmpq	$30, %rdx
	jbe	.L51
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r9
	je	.L51
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L35:
	movdqu	0(%r13,%rdx), %xmm2
	movups	%xmm2, (%rsi,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L35
	movq	%r9, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %r13
	addq	%rsi, %rcx
	cmpq	%rdx, %r9
	je	.L37
	movq	0(%r13), %rdx
	movq	%rdx, (%rcx)
.L37:
	movq	16(%rbx), %rdx
	leaq	8(%rsi,%r8), %rcx
	cmpq	%rdx, %r12
	je	.L38
	subq	%r12, %rdx
	leaq	-8(%rdx), %r10
	leaq	24(%rsi,%r8), %rdx
	movq	%r10, %r9
	shrq	$3, %r9
	cmpq	%rdx, %r12
	leaq	16(%r12), %rdx
	setnb	%sil
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %sil
	je	.L52
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r9
	je	.L52
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L40:
	movdqu	(%r12,%rdx), %xmm6
	movups	%xmm6, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L40
	movq	%r9, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %r8
	addq	%r8, %r12
	addq	%rcx, %r8
	cmpq	%rdx, %r9
	je	.L42
	movq	(%r12), %rdx
	movq	%rdx, (%r8)
.L42:
	leaq	8(%rcx,%r10), %rcx
.L38:
	movq	%rax, %xmm0
	movq	%rcx, %xmm7
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 8(%rbx)
.L1:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	leaq	0(%r13,%r8), %rsi
	cmpq	%rsi, %rcx
	je	.L44
	leaq	-8(%rcx), %rdx
	leaq	16(%r13,%r8), %r10
	subq	%rsi, %rdx
	shrq	$3, %rdx
	cmpq	%r10, %rdi
	leaq	16(%rdi), %r10
	setnb	%r11b
	cmpq	%r10, %rsi
	setnb	%r10b
	orb	%r10b, %r11b
	je	.L45
	movabsq	$2305843009213693948, %r10
	testq	%r10, %rdx
	je	.L45
	leaq	1(%rdx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %r10
	shrq	%r10
	salq	$4, %r10
	.p2align 4,,10
	.p2align 3
.L13:
	movdqu	(%rsi,%rdx), %xmm3
	movups	%xmm3, (%rdi,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %r10
	jne	.L13
	movq	%rcx, %r11
	andq	$-2, %r11
	leaq	0(,%r11,8), %r10
	leaq	(%rsi,%r10), %rdx
	addq	%rdi, %r10
	cmpq	%r11, %rcx
	je	.L15
	movq	(%rdx), %rdx
	movq	%rdx, (%r10)
.L15:
	movq	16(%rbx), %r10
.L11:
	movq	%rax, %rdx
	subq	%r9, %rdx
	leaq	(%r10,%rdx,8), %rdx
	movq	%rdx, 16(%rbx)
	cmpq	%r12, %rdi
	je	.L16
	subq	%r12, %rdi
	subq	%r9, %rax
	leaq	-8(%rdi), %rcx
	leaq	16(%r10,%rax,8), %rax
	shrq	$3, %rcx
	cmpq	%rax, %r12
	leaq	16(%r12), %rax
	setnb	%dil
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %dil
	je	.L46
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L46
	addq	$1, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L18:
	movdqu	(%r12,%rax), %xmm4
	movups	%xmm4, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L18
	movq	%rcx, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rax
	leaq	(%r12,%rax), %rdi
	addq	%rax, %rdx
	cmpq	%r9, %rcx
	je	.L20
	movq	(%rdi), %rax
	movq	%rax, (%rdx)
.L20:
	movq	16(%rbx), %rdx
.L16:
	addq	%r8, %rdx
	movq	%rdx, 16(%rbx)
	cmpq	%rsi, %r13
	je	.L1
	movq	%r8, %rdx
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L6:
	movq	(%rdx,%rcx,8), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rsi, %rax
	jne	.L6
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%rdi, %r10
	movq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L12:
	movq	(%rdx), %r11
	addq	$8, %rdx
	addq	$8, %r10
	movq	%r11, -8(%r10)
	cmpq	%rdx, %rcx
	jne	.L12
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L50:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L30:
	movq	(%rsi,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r9, %r8
	jne	.L30
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L34:
	movq	0(%r13), %r9
	addq	$8, %r13
	addq	$8, %rdx
	movq	%r9, -8(%rdx)
	cmpq	%r13, %rcx
	jne	.L34
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L46:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L17:
	movq	(%r12,%rax,8), %rdi
	movq	%rdi, (%rdx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rcx, %rdi
	jne	.L17
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L39:
	movq	(%r12,%rdx,8), %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%r9, %rsi
	jne	.L39
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%rdi, %r10
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%rax, %rsi
	jmp	.L29
.L107:
	cmpq	$268435455, %rax
	cmova	%r14, %rax
	leaq	0(,%rax,8), %r14
	movq	%r14, %rsi
.L25:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L108
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L28:
	movq	8(%rbx), %rsi
	leaq	(%rax,%r14), %rdi
	jmp	.L26
.L108:
	movq	%rcx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rcx
	jmp	.L28
.L24:
	movl	$2147483640, %esi
	movl	$2147483640, %r14d
	jmp	.L25
.L106:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23001:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIPS4_EEvN9__gnu_cxx17__normal_iteratorIS9_S7_EET_SD_St20forward_iterator_tag, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIPS4_EEvN9__gnu_cxx17__normal_iteratorIS9_S7_EET_SD_St20forward_iterator_tag
	.section	.text._ZN2v88internal8compiler22BasicBlockInstrumentor10InstrumentEPNS0_24OptimizedCompilationInfoEPNS1_5GraphEPNS1_8ScheduleEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22BasicBlockInstrumentor10InstrumentEPNS0_24OptimizedCompilationInfoEPNS1_5GraphEPNS1_8ScheduleEPNS0_7IsolateE
	.type	_ZN2v88internal8compiler22BasicBlockInstrumentor10InstrumentEPNS0_24OptimizedCompilationInfoEPNS1_5GraphEPNS1_8ScheduleEPNS0_7IsolateE, @function
_ZN2v88internal8compiler22BasicBlockInstrumentor10InstrumentEPNS0_24OptimizedCompilationInfoEPNS1_5GraphEPNS1_8ScheduleEPNS0_7IsolateE:
.LFB19229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$632, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	88(%rdx), %rax
	subq	80(%rdx), %rax
	sarq	$3, %rax
	leaq	-1(%rax), %rbx
	movq	%rbx, -600(%rbp)
	call	_ZN2v88internal18BasicBlockProfiler3GetEv@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal18BasicBlockProfiler7NewDataEm@PLT
	leaq	-480(%rbp), %rcx
	movq	%r12, %rsi
	movq	%rax, %r13
	movq	%rcx, %rbx
	movq	%rcx, %rdi
	movq	%rax, -576(%rbp)
	movq	%rcx, -536(%rbp)
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal18BasicBlockProfiler4Data15SetFunctionNameESt10unique_ptrIA_cSt14default_deleteIS4_EE@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L110
	call	_ZdaPv@PLT
.L110:
	leaq	24+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, %xmm7
	movq	%rax, %xmm2
	movq	%rax, %xmm1
	leaq	-320(%rbp), %r12
	punpcklqdq	%xmm7, %xmm2
	movhps	.LC1(%rip), %xmm1
	movq	%r12, %rdi
	movaps	%xmm2, -560(%rbp)
	leaq	-432(%rbp), %r13
	movaps	%xmm1, -528(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	%rax, -432(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	-368(%rbp), %r8
	pxor	%xmm0, %xmm0
	movdqa	-528(%rbp), %xmm1
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r8, %rdi
	movq	%r8, -528(%rbp)
	movq	%rdx, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	%rbx, -424(%rbp)
	movq	%r12, %rdi
	leaq	-336(%rbp), %rbx
	leaq	-424(%rbp), %rsi
	movl	$16, -360(%rbp)
	movq	%rbx, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE@PLT
	movq	-576(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal18BasicBlockProfiler4Data11SetScheduleEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE@PLT
	movq	-352(%rbp), %rdi
	movdqa	-560(%rbp), %xmm2
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rdx, -320(%rbp)
	movq	-528(%rbp), %r8
	cmpq	%rbx, %rdi
	movaps	%xmm2, -432(%rbp)
	je	.L111
	call	_ZdlPv@PLT
	movq	-528(%rbp), %r8
.L111:
	movq	%r8, %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	-496(%rbp), %rbx
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	(%r15), %rsi
	movq	%rbx, %rdi
	movq	%rbx, -632(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilderC1EPNS0_4ZoneE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, -608(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	(%r15), %rsi
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	-536(%rbp), %rdi
	movl	$2, %r8d
	movl	$5, %edx
	movq	%rax, -616(%rbp)
	movq	$2, -508(%rbp)
	movl	$0, -500(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilderC1EPNS0_4ZoneENS0_21MachineRepresentationENS_4base5FlagsINS2_4FlagEjEENS2_21AlignmentRequirementsE@PLT
	movq	80(%r14), %rax
	movq	-608(%rbp), %xmm6
	movq	$0, -528(%rbp)
	cmpq	$0, -600(%rbp)
	movq	%rax, -624(%rbp)
	leaq	-384(%rbp), %rax
	movhps	-616(%rbp), %xmm6
	movq	%rax, -640(%rbp)
	leaq	-416(%rbp), %rax
	movq	%rax, -664(%rbp)
	movaps	%xmm6, -656(%rbp)
	je	.L109
	.p2align 4,,10
	.p2align 3
.L112:
	movq	-528(%rbp), %r12
	movq	-624(%rbp), %rcx
	movq	-576(%rbp), %rdi
	movq	(%rcx,%r12,8), %rbx
	movq	%r12, %rsi
	movl	4(%rbx), %edx
	call	_ZN2v88internal18BasicBlockProfiler4Data17SetBlockRpoNumberEmi@PLT
	movq	-576(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r12, -528(%rbp)
	call	_ZN2v88internal18BasicBlockProfiler4Data17GetCounterAddressEm@PLT
	movq	-632(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-536(%rbp), %rdi
	movq	8(%r15), %r12
	movl	$772, %esi
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder4LoadENS0_11MachineTypeE@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, %rsi
	movl	$4, %edx
	movq	%r15, %rdi
	movq	-568(%rbp), %xmm1
	movaps	%xmm0, -416(%rbp)
	movhps	-608(%rbp), %xmm1
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm1, -592(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-536(%rbp), %rdi
	movq	%rax, -544(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$2, %edx
	movq	-544(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-616(%rbp), %xmm0
	movaps	%xmm0, -432(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-536(%rbp), %rdi
	movq	8(%r15), %r12
	movl	$4, %esi
	movq	%rax, -560(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder5StoreENS1_19StoreRepresentationE@PLT
	movq	%r12, %xmm3
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-560(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$5, %edx
	movq	%r15, %rdi
	movdqa	-592(%rbp), %xmm1
	movq	%r12, -400(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movdqa	-656(%rbp), %xmm4
	movq	-568(%rbp), %xmm0
	movq	%rax, %xmm5
	cmpq	$0, -528(%rbp)
	movhps	-544(%rbp), %xmm0
	movaps	%xmm4, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movq	-560(%rbp), %xmm0
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -400(%rbp)
	je	.L122
	movq	-664(%rbp), %rax
	movl	$2, -544(%rbp)
	movq	%rax, -568(%rbp)
.L113:
	movq	72(%rbx), %r12
	cmpq	%r12, 80(%rbx)
	je	.L114
	movq	%r13, -560(%rbp)
	movq	%rbx, %r13
.L117:
	movq	(%r12), %rax
	movq	(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18OperatorProperties17IsBasicBlockBeginEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L115
	movzwl	16(%rbx), %eax
	cmpw	$36, %ax
	ja	.L116
	cmpw	$34, %ax
	ja	.L115
.L130:
	movq	%r13, %rbx
	movq	-560(%rbp), %r13
.L114:
	movq	-640(%rbp), %rcx
	movq	-568(%rbp), %rdx
	movq	%r12, %rsi
	leaq	64(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE15_M_range_insertIPS4_EEvN9__gnu_cxx17__normal_iteratorIS9_S7_EET_SD_St20forward_iterator_tag
	movslq	-544(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L118:
	movq	0(%r13,%r12,8), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	addq	$1, %r12
	call	_ZN2v88internal8compiler8Schedule15SetBlockForNodeEPNS1_10BasicBlockEPNS1_4NodeE@PLT
	cmpl	$6, %r12d
	jne	.L118
	addq	$1, -528(%rbp)
	movq	-528(%rbp), %rax
	cmpq	%rax, -600(%rbp)
	jne	.L112
.L109:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L132
	movq	-576(%rbp), %rax
	addq	$632, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	cmpw	$50, %ax
	jne	.L130
.L115:
	addq	$8, %r12
	cmpq	%r12, 80(%r13)
	jne	.L117
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L122:
	movq	%r13, -568(%rbp)
	movl	$0, -544(%rbp)
	jmp	.L113
.L132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19229:
	.size	_ZN2v88internal8compiler22BasicBlockInstrumentor10InstrumentEPNS0_24OptimizedCompilationInfoEPNS1_5GraphEPNS1_8ScheduleEPNS0_7IsolateE, .-_ZN2v88internal8compiler22BasicBlockInstrumentor10InstrumentEPNS0_24OptimizedCompilationInfoEPNS1_5GraphEPNS1_8ScheduleEPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler22BasicBlockInstrumentor10InstrumentEPNS0_24OptimizedCompilationInfoEPNS1_5GraphEPNS1_8ScheduleEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler22BasicBlockInstrumentor10InstrumentEPNS0_24OptimizedCompilationInfoEPNS1_5GraphEPNS1_8ScheduleEPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler22BasicBlockInstrumentor10InstrumentEPNS0_24OptimizedCompilationInfoEPNS1_5GraphEPNS1_8ScheduleEPNS0_7IsolateE:
.LFB23255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23255:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler22BasicBlockInstrumentor10InstrumentEPNS0_24OptimizedCompilationInfoEPNS1_5GraphEPNS1_8ScheduleEPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal8compiler22BasicBlockInstrumentor10InstrumentEPNS0_24OptimizedCompilationInfoEPNS1_5GraphEPNS1_8ScheduleEPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler22BasicBlockInstrumentor10InstrumentEPNS0_24OptimizedCompilationInfoEPNS1_5GraphEPNS1_8ScheduleEPNS0_7IsolateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC1:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
