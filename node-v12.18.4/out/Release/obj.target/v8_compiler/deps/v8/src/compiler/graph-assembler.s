	.file	"graph-assembler.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB11041:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11041:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB11034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE11034:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB11035:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE11035:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB11043:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11043:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal8compiler14GraphAssemblerC2EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssemblerC2EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE
	.type	_ZN2v88internal8compiler14GraphAssemblerC2EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE, @function
_ZN2v88internal8compiler14GraphAssemblerC2EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE:
.LFB13600:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movq	%r8, 8(%rdi)
	movq	%rsi, 16(%rdi)
	movq	%rdx, 24(%rdi)
	movq	%rcx, 32(%rdi)
	ret
	.cfi_endproc
.LFE13600:
	.size	_ZN2v88internal8compiler14GraphAssemblerC2EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE, .-_ZN2v88internal8compiler14GraphAssemblerC2EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE
	.globl	_ZN2v88internal8compiler14GraphAssemblerC1EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE
	.set	_ZN2v88internal8compiler14GraphAssemblerC1EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE,_ZN2v88internal8compiler14GraphAssemblerC2EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl
	.type	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl, @function
_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl:
.LFB13602:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler12MachineGraph14IntPtrConstantEl@PLT
	.cfi_endproc
.LFE13602:
	.size	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl, .-_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl
	.section	.text._ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi
	.type	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi, @function
_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi:
.LFB16254:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	.cfi_endproc
.LFE16254:
	.size	_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi, .-_ZN2v88internal8compiler14GraphAssembler13Int32ConstantEi
	.section	.text._ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl
	.type	_ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl, @function
_ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl:
.LFB13604:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler12MachineGraph13Int64ConstantEl@PLT
	.cfi_endproc
.LFE13604:
	.size	_ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl, .-_ZN2v88internal8compiler14GraphAssembler13Int64ConstantEl
	.section	.text._ZN2v88internal8compiler14GraphAssembler20UniqueIntPtrConstantEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler20UniqueIntPtrConstantEl
	.type	_ZN2v88internal8compiler14GraphAssembler20UniqueIntPtrConstantEl, @function
_ZN2v88internal8compiler14GraphAssembler20UniqueIntPtrConstantEl:
.LFB13605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %rax
	movq	(%rax), %r12
	movq	8(%rax), %rdi
	movq	16(%rax), %rax
	cmpb	$5, 16(%rax)
	je	.L15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	movq	%rax, %rsi
.L13:
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	popq	%r12
	xorl	%edx, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	movq	%rax, %rsi
	jmp	.L13
	.cfi_endproc
.LFE13605:
	.size	_ZN2v88internal8compiler14GraphAssembler20UniqueIntPtrConstantEl, .-_ZN2v88internal8compiler14GraphAssembler20UniqueIntPtrConstantEl
	.section	.text._ZN2v88internal8compiler14GraphAssembler11SmiConstantEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler11SmiConstantEi
	.type	_ZN2v88internal8compiler14GraphAssembler11SmiConstantEi, @function
_ZN2v88internal8compiler14GraphAssembler11SmiConstantEi:
.LFB13606:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	16(%rdi), %rdi
	cvtsi2sdl	%esi, %xmm0
	jmp	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	.cfi_endproc
.LFE13606:
	.size	_ZN2v88internal8compiler14GraphAssembler11SmiConstantEi, .-_ZN2v88internal8compiler14GraphAssembler11SmiConstantEi
	.section	.text._ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi
	.type	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi, @function
_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi:
.LFB13607:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler12MachineGraph13Int32ConstantEi@PLT
	.cfi_endproc
.LFE13607:
	.size	_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi, .-_ZN2v88internal8compiler14GraphAssembler14Uint32ConstantEi
	.section	.text._ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd
	.type	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd, @function
_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd:
.LFB13608:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler12MachineGraph15Float64ConstantEd@PLT
	.cfi_endproc
.LFE13608:
	.size	_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd, .-_ZN2v88internal8compiler14GraphAssembler15Float64ConstantEd
	.section	.text._ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE
	.type	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE, @function
_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE:
.LFB13609:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph12HeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	.cfi_endproc
.LFE13609:
	.size	_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE, .-_ZN2v88internal8compiler14GraphAssembler12HeapConstantENS0_6HandleINS0_10HeapObjectEEE
	.section	.text._ZN2v88internal8compiler14GraphAssembler14NumberConstantEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler14NumberConstantEd
	.type	_ZN2v88internal8compiler14GraphAssembler14NumberConstantEd, @function
_ZN2v88internal8compiler14GraphAssembler14NumberConstantEd:
.LFB13610:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	.cfi_endproc
.LFE13610:
	.size	_ZN2v88internal8compiler14GraphAssembler14NumberConstantEd, .-_ZN2v88internal8compiler14GraphAssembler14NumberConstantEd
	.section	.text._ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE
	.type	_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE, @function
_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE:
.LFB13611:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler12MachineGraph16ExternalConstantENS0_17ExternalReferenceE@PLT
	.cfi_endproc
.LFE13611:
	.size	_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE, .-_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE
	.section	.text._ZN2v88internal8compiler14GraphAssembler18CEntryStubConstantEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler18CEntryStubConstantEi
	.type	_ZN2v88internal8compiler14GraphAssembler18CEntryStubConstantEi, @function
_ZN2v88internal8compiler14GraphAssembler18CEntryStubConstantEi:
.LFB13612:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	_ZN2v88internal8compiler7JSGraph18CEntryStubConstantEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	.cfi_endproc
.LFE13612:
	.size	_ZN2v88internal8compiler14GraphAssembler18CEntryStubConstantEi, .-_ZN2v88internal8compiler14GraphAssembler18CEntryStubConstantEi
	.section	.text._ZN2v88internal8compiler14GraphAssembler16LoadFramePointerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler16LoadFramePointerEv
	.type	_ZN2v88internal8compiler14GraphAssembler16LoadFramePointerEv, @function
_ZN2v88internal8compiler14GraphAssembler16LoadFramePointerEv:
.LFB13613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder16LoadFramePointerEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	popq	%r12
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	.cfi_endproc
.LFE13613:
	.size	_ZN2v88internal8compiler14GraphAssembler16LoadFramePointerEv, .-_ZN2v88internal8compiler14GraphAssembler16LoadFramePointerEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler12TrueConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler12TrueConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler12TrueConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler12TrueConstantEv:
.LFB13614:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	.cfi_endproc
.LFE13614:
	.size	_ZN2v88internal8compiler14GraphAssembler12TrueConstantEv, .-_ZN2v88internal8compiler14GraphAssembler12TrueConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler13FalseConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler13FalseConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler13FalseConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler13FalseConstantEv:
.LFB13615:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	.cfi_endproc
.LFE13615:
	.size	_ZN2v88internal8compiler14GraphAssembler13FalseConstantEv, .-_ZN2v88internal8compiler14GraphAssembler13FalseConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler12NullConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler12NullConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler12NullConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler12NullConstantEv:
.LFB13616:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph12NullConstantEv@PLT
	.cfi_endproc
.LFE13616:
	.size	_ZN2v88internal8compiler14GraphAssembler12NullConstantEv, .-_ZN2v88internal8compiler14GraphAssembler12NullConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler17BigIntMapConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler17BigIntMapConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler17BigIntMapConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler17BigIntMapConstantEv:
.LFB13617:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph17BigIntMapConstantEv@PLT
	.cfi_endproc
.LFE13617:
	.size	_ZN2v88internal8compiler14GraphAssembler17BigIntMapConstantEv, .-_ZN2v88internal8compiler14GraphAssembler17BigIntMapConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler18BooleanMapConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler18BooleanMapConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler18BooleanMapConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler18BooleanMapConstantEv:
.LFB13618:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph18BooleanMapConstantEv@PLT
	.cfi_endproc
.LFE13618:
	.size	_ZN2v88internal8compiler14GraphAssembler18BooleanMapConstantEv, .-_ZN2v88internal8compiler14GraphAssembler18BooleanMapConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv:
.LFB13619:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph21HeapNumberMapConstantEv@PLT
	.cfi_endproc
.LFE13619:
	.size	_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv, .-_ZN2v88internal8compiler14GraphAssembler21HeapNumberMapConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv:
.LFB13620:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	.cfi_endproc
.LFE13620:
	.size	_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv, .-_ZN2v88internal8compiler14GraphAssembler17NoContextConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler19EmptyStringConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler19EmptyStringConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler19EmptyStringConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler19EmptyStringConstantEv:
.LFB13621:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph19EmptyStringConstantEv@PLT
	.cfi_endproc
.LFE13621:
	.size	_ZN2v88internal8compiler14GraphAssembler19EmptyStringConstantEv, .-_ZN2v88internal8compiler14GraphAssembler19EmptyStringConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler17UndefinedConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler17UndefinedConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler17UndefinedConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler17UndefinedConstantEv:
.LFB13622:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	.cfi_endproc
.LFE13622:
	.size	_ZN2v88internal8compiler14GraphAssembler17UndefinedConstantEv, .-_ZN2v88internal8compiler14GraphAssembler17UndefinedConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler15TheHoleConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler15TheHoleConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler15TheHoleConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler15TheHoleConstantEv:
.LFB13623:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph15TheHoleConstantEv@PLT
	.cfi_endproc
.LFE13623:
	.size	_ZN2v88internal8compiler14GraphAssembler15TheHoleConstantEv, .-_ZN2v88internal8compiler14GraphAssembler15TheHoleConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler21FixedArrayMapConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler21FixedArrayMapConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler21FixedArrayMapConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler21FixedArrayMapConstantEv:
.LFB13624:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph21FixedArrayMapConstantEv@PLT
	.cfi_endproc
.LFE13624:
	.size	_ZN2v88internal8compiler14GraphAssembler21FixedArrayMapConstantEv, .-_ZN2v88internal8compiler14GraphAssembler21FixedArrayMapConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler27FixedDoubleArrayMapConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler27FixedDoubleArrayMapConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler27FixedDoubleArrayMapConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler27FixedDoubleArrayMapConstantEv:
.LFB13625:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph27FixedDoubleArrayMapConstantEv@PLT
	.cfi_endproc
.LFE13625:
	.size	_ZN2v88internal8compiler14GraphAssembler27FixedDoubleArrayMapConstantEv, .-_ZN2v88internal8compiler14GraphAssembler27FixedDoubleArrayMapConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler23ToNumberBuiltinConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler23ToNumberBuiltinConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler23ToNumberBuiltinConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler23ToNumberBuiltinConstantEv:
.LFB13626:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph23ToNumberBuiltinConstantEv@PLT
	.cfi_endproc
.LFE13626:
	.size	_ZN2v88internal8compiler14GraphAssembler23ToNumberBuiltinConstantEv, .-_ZN2v88internal8compiler14GraphAssembler23ToNumberBuiltinConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler37AllocateInYoungGenerationStubConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler37AllocateInYoungGenerationStubConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler37AllocateInYoungGenerationStubConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler37AllocateInYoungGenerationStubConstantEv:
.LFB13627:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph37AllocateInYoungGenerationStubConstantEv@PLT
	.cfi_endproc
.LFE13627:
	.size	_ZN2v88internal8compiler14GraphAssembler37AllocateInYoungGenerationStubConstantEv, .-_ZN2v88internal8compiler14GraphAssembler37AllocateInYoungGenerationStubConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler44AllocateRegularInYoungGenerationStubConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler44AllocateRegularInYoungGenerationStubConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler44AllocateRegularInYoungGenerationStubConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler44AllocateRegularInYoungGenerationStubConstantEv:
.LFB13628:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph44AllocateRegularInYoungGenerationStubConstantEv@PLT
	.cfi_endproc
.LFE13628:
	.size	_ZN2v88internal8compiler14GraphAssembler44AllocateRegularInYoungGenerationStubConstantEv, .-_ZN2v88internal8compiler14GraphAssembler44AllocateRegularInYoungGenerationStubConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler35AllocateInOldGenerationStubConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler35AllocateInOldGenerationStubConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler35AllocateInOldGenerationStubConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler35AllocateInOldGenerationStubConstantEv:
.LFB13629:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph35AllocateInOldGenerationStubConstantEv@PLT
	.cfi_endproc
.LFE13629:
	.size	_ZN2v88internal8compiler14GraphAssembler35AllocateInOldGenerationStubConstantEv, .-_ZN2v88internal8compiler14GraphAssembler35AllocateInOldGenerationStubConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler42AllocateRegularInOldGenerationStubConstantEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler42AllocateRegularInOldGenerationStubConstantEv
	.type	_ZN2v88internal8compiler14GraphAssembler42AllocateRegularInOldGenerationStubConstantEv, @function
_ZN2v88internal8compiler14GraphAssembler42AllocateRegularInOldGenerationStubConstantEv:
.LFB13630:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN2v88internal8compiler7JSGraph42AllocateRegularInOldGenerationStubConstantEv@PLT
	.cfi_endproc
.LFE13630:
	.size	_ZN2v88internal8compiler14GraphAssembler42AllocateRegularInOldGenerationStubConstantEv, .-_ZN2v88internal8compiler14GraphAssembler42AllocateRegularInOldGenerationStubConstantEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE:
.LFB13631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder18ChangeInt32ToInt64Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L45
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L45:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13631:
	.size	_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler18ChangeInt32ToInt64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler20ChangeInt32ToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler20ChangeInt32ToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler20ChangeInt32ToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler20ChangeInt32ToFloat64EPNS1_4NodeE:
.LFB13632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeInt32ToFloat64Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L49
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L49:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13632:
	.size	_ZN2v88internal8compiler14GraphAssembler20ChangeInt32ToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler20ChangeInt32ToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler20ChangeInt64ToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler20ChangeInt64ToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler20ChangeInt64ToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler20ChangeInt64ToFloat64EPNS1_4NodeE:
.LFB13633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeInt64ToFloat64Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L53
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L53:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13633:
	.size	_ZN2v88internal8compiler14GraphAssembler20ChangeInt64ToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler20ChangeInt64ToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler21ChangeUint32ToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler21ChangeUint32ToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler21ChangeUint32ToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler21ChangeUint32ToFloat64EPNS1_4NodeE:
.LFB13634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21ChangeUint32ToFloat64Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L57
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L57:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13634:
	.size	_ZN2v88internal8compiler14GraphAssembler21ChangeUint32ToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler21ChangeUint32ToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler20ChangeUint32ToUint64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler20ChangeUint32ToUint64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler20ChangeUint32ToUint64EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler20ChangeUint32ToUint64EPNS1_4NodeE:
.LFB13635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeUint32ToUint64Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L61
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L61:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13635:
	.size	_ZN2v88internal8compiler14GraphAssembler20ChangeUint32ToUint64EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler20ChangeUint32ToUint64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt32EPNS1_4NodeE:
.LFB13636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeFloat64ToInt32Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L65
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L65:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13636:
	.size	_ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt64EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt64EPNS1_4NodeE:
.LFB13637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20ChangeFloat64ToInt64Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L69
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L69:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13637:
	.size	_ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt64EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler20ChangeFloat64ToInt64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler21ChangeFloat64ToUint32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler21ChangeFloat64ToUint32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler21ChangeFloat64ToUint32EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler21ChangeFloat64ToUint32EPNS1_4NodeE:
.LFB13638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21ChangeFloat64ToUint32Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L73
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L73:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13638:
	.size	_ZN2v88internal8compiler14GraphAssembler21ChangeFloat64ToUint32EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler21ChangeFloat64ToUint32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE:
.LFB13639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20TruncateInt64ToInt32Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L77
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L77:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13639:
	.size	_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler20TruncateInt64ToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler19RoundFloat64ToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler19RoundFloat64ToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler19RoundFloat64ToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler19RoundFloat64ToInt32EPNS1_4NodeE:
.LFB13640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder19RoundFloat64ToInt32Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L81
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13640:
	.size	_ZN2v88internal8compiler14GraphAssembler19RoundFloat64ToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler19RoundFloat64ToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler22TruncateFloat64ToInt64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler22TruncateFloat64ToInt64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler22TruncateFloat64ToInt64EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler22TruncateFloat64ToInt64EPNS1_4NodeE:
.LFB13641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22TruncateFloat64ToInt64Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L85
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L85:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13641:
	.size	_ZN2v88internal8compiler14GraphAssembler22TruncateFloat64ToInt64EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler22TruncateFloat64ToInt64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler23TruncateFloat64ToWord32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler23TruncateFloat64ToWord32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler23TruncateFloat64ToWord32EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler23TruncateFloat64ToWord32EPNS1_4NodeE:
.LFB13642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder23TruncateFloat64ToWord32Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L89
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L89:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13642:
	.size	_ZN2v88internal8compiler14GraphAssembler23TruncateFloat64ToWord32EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler23TruncateFloat64ToWord32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler23Float64ExtractLowWord32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler23Float64ExtractLowWord32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler23Float64ExtractLowWord32EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler23Float64ExtractLowWord32EPNS1_4NodeE:
.LFB13643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder23Float64ExtractLowWord32Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L93
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L93:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13643:
	.size	_ZN2v88internal8compiler14GraphAssembler23Float64ExtractLowWord32EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler23Float64ExtractLowWord32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE:
.LFB13644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder24Float64ExtractHighWord32Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L97
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L97:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13644:
	.size	_ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler24Float64ExtractHighWord32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler21BitcastInt32ToFloat32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler21BitcastInt32ToFloat32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler21BitcastInt32ToFloat32EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler21BitcastInt32ToFloat32EPNS1_4NodeE:
.LFB13645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21BitcastInt32ToFloat32Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L101
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L101:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13645:
	.size	_ZN2v88internal8compiler14GraphAssembler21BitcastInt32ToFloat32EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler21BitcastInt32ToFloat32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler21BitcastInt64ToFloat64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler21BitcastInt64ToFloat64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler21BitcastInt64ToFloat64EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler21BitcastInt64ToFloat64EPNS1_4NodeE:
.LFB13646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21BitcastInt64ToFloat64Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L105
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L105:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13646:
	.size	_ZN2v88internal8compiler14GraphAssembler21BitcastInt64ToFloat64EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler21BitcastInt64ToFloat64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler21BitcastFloat32ToInt32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler21BitcastFloat32ToInt32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler21BitcastFloat32ToInt32EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler21BitcastFloat32ToInt32EPNS1_4NodeE:
.LFB13647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21BitcastFloat32ToInt32Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L109
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L109:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13647:
	.size	_ZN2v88internal8compiler14GraphAssembler21BitcastFloat32ToInt32EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler21BitcastFloat32ToInt32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler21BitcastFloat64ToInt64EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler21BitcastFloat64ToInt64EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler21BitcastFloat64ToInt64EPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler21BitcastFloat64ToInt64EPNS1_4NodeE:
.LFB13648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21BitcastFloat64ToInt64Ev@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L113
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L113:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13648:
	.size	_ZN2v88internal8compiler14GraphAssembler21BitcastFloat64ToInt64EPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler21BitcastFloat64ToInt64EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler10Float64AbsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler10Float64AbsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler10Float64AbsEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler10Float64AbsEPNS1_4NodeE:
.LFB13649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64AbsEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L117
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L117:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13649:
	.size	_ZN2v88internal8compiler14GraphAssembler10Float64AbsEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler10Float64AbsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler18Word32ReverseBytesEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler18Word32ReverseBytesEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler18Word32ReverseBytesEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler18Word32ReverseBytesEPNS1_4NodeE:
.LFB13650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder18Word32ReverseBytesEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L121
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L121:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13650:
	.size	_ZN2v88internal8compiler14GraphAssembler18Word32ReverseBytesEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler18Word32ReverseBytesEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler18Word64ReverseBytesEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler18Word64ReverseBytesEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler18Word64ReverseBytesEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler18Word64ReverseBytesEPNS1_4NodeE:
.LFB13651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder18Word64ReverseBytesEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L125
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L125:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13651:
	.size	_ZN2v88internal8compiler14GraphAssembler18Word64ReverseBytesEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler18Word64ReverseBytesEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler17Float64SilenceNaNEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler17Float64SilenceNaNEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler17Float64SilenceNaNEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler17Float64SilenceNaNEPNS1_4NodeE:
.LFB13652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder17Float64SilenceNaNEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L129
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L129:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13652:
	.size	_ZN2v88internal8compiler14GraphAssembler17Float64SilenceNaNEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler17Float64SilenceNaNEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler24ChangeCompressedToTaggedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler24ChangeCompressedToTaggedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler24ChangeCompressedToTaggedEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler24ChangeCompressedToTaggedEPNS1_4NodeE:
.LFB13653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder24ChangeCompressedToTaggedEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L133
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L133:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13653:
	.size	_ZN2v88internal8compiler14GraphAssembler24ChangeCompressedToTaggedEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler24ChangeCompressedToTaggedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler24ChangeTaggedToCompressedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler24ChangeTaggedToCompressedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler24ChangeTaggedToCompressedEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler24ChangeTaggedToCompressedEPNS1_4NodeE:
.LFB13654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder24ChangeTaggedToCompressedEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L137
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L137:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13654:
	.size	_ZN2v88internal8compiler14GraphAssembler24ChangeTaggedToCompressedEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler24ChangeTaggedToCompressedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler36ChangeTaggedSignedToCompressedSignedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler36ChangeTaggedSignedToCompressedSignedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler36ChangeTaggedSignedToCompressedSignedEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler36ChangeTaggedSignedToCompressedSignedEPNS1_4NodeE:
.LFB13655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder36ChangeTaggedSignedToCompressedSignedEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L141
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L141:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13655:
	.size	_ZN2v88internal8compiler14GraphAssembler36ChangeTaggedSignedToCompressedSignedEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler36ChangeTaggedSignedToCompressedSignedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler36ChangeCompressedSignedToTaggedSignedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler36ChangeCompressedSignedToTaggedSignedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler36ChangeCompressedSignedToTaggedSignedEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler36ChangeCompressedSignedToTaggedSignedEPNS1_4NodeE:
.LFB13656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder36ChangeCompressedSignedToTaggedSignedEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L145
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L145:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13656:
	.size	_ZN2v88internal8compiler14GraphAssembler36ChangeCompressedSignedToTaggedSignedEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler36ChangeCompressedSignedToTaggedSignedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler38ChangeTaggedPointerToCompressedPointerEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler38ChangeTaggedPointerToCompressedPointerEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler38ChangeTaggedPointerToCompressedPointerEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler38ChangeTaggedPointerToCompressedPointerEPNS1_4NodeE:
.LFB13657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder38ChangeTaggedPointerToCompressedPointerEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L149
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L149:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13657:
	.size	_ZN2v88internal8compiler14GraphAssembler38ChangeTaggedPointerToCompressedPointerEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler38ChangeTaggedPointerToCompressedPointerEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler38ChangeCompressedPointerToTaggedPointerEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler38ChangeCompressedPointerToTaggedPointerEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler38ChangeCompressedPointerToTaggedPointerEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler38ChangeCompressedPointerToTaggedPointerEPNS1_4NodeE:
.LFB13658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder38ChangeCompressedPointerToTaggedPointerEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-32(%rbp), %rcx
	movq	%rbx, -32(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L153
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L153:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13658:
	.size	_ZN2v88internal8compiler14GraphAssembler38ChangeCompressedPointerToTaggedPointerEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler38ChangeCompressedPointerToTaggedPointerEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_:
.LFB13659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	cmpb	$4, 16(%rdi)
	je	.L159
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word64ShlEv@PLT
	movq	%rax, %rsi
.L156:
	movq	-56(%rbp), %xmm0
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$2, %edx
	movhps	-64(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L160
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShlEv@PLT
	movq	%rax, %rsi
	jmp	.L156
.L160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13659:
	.size	_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler7WordShlEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_:
.LFB13660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	cmpb	$4, 16(%rdi)
	je	.L166
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word64SarEv@PLT
	movq	%rax, %rsi
.L163:
	movq	-56(%rbp), %xmm0
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$2, %edx
	movhps	-64(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L167
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
	movq	%rax, %rsi
	jmp	.L163
.L167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13660:
	.size	_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler7WordSarEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_:
.LFB13661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	cmpb	$4, 16(%rdi)
	je	.L173
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word64AndEv@PLT
	movq	%rax, %rsi
.L170:
	movq	-56(%rbp), %xmm0
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$2, %edx
	movhps	-64(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L174
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movq	%rax, %rsi
	jmp	.L170
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13661:
	.size	_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler7WordAndEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler8Word32OrEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler8Word32OrEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler8Word32OrEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler8Word32OrEPNS1_4NodeES4_:
.LFB13662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Word32OrEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L178
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L178:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13662:
	.size	_ZN2v88internal8compiler14GraphAssembler8Word32OrEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler8Word32OrEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_:
.LFB13663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L182
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L182:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13663:
	.size	_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler9Word32AndEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler9Word32XorEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler9Word32XorEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler9Word32XorEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler9Word32XorEPNS1_4NodeES4_:
.LFB13664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32XorEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L186
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L186:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13664:
	.size	_ZN2v88internal8compiler14GraphAssembler9Word32XorEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler9Word32XorEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler9Word32ShrEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler9Word32ShrEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler9Word32ShrEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler9Word32ShrEPNS1_4NodeES4_:
.LFB13665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShrEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L190
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L190:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13665:
	.size	_ZN2v88internal8compiler14GraphAssembler9Word32ShrEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler9Word32ShrEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler9Word32ShlEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler9Word32ShlEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler9Word32ShlEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler9Word32ShlEPNS1_4NodeES4_:
.LFB13666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShlEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L194
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L194:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13666:
	.size	_ZN2v88internal8compiler14GraphAssembler9Word32ShlEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler9Word32ShlEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler9Word32SarEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler9Word32SarEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler9Word32SarEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler9Word32SarEPNS1_4NodeES4_:
.LFB13667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L198
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L198:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13667:
	.size	_ZN2v88internal8compiler14GraphAssembler9Word32SarEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler9Word32SarEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler9Word64AndEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler9Word64AndEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler9Word64AndEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler9Word64AndEPNS1_4NodeES4_:
.LFB13668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word64AndEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L202
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L202:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13668:
	.size	_ZN2v88internal8compiler14GraphAssembler9Word64AndEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler9Word64AndEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_:
.LFB13669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	cmpb	$4, 16(%rdi)
	je	.L208
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int64AddEv@PLT
	movq	%rax, %rsi
.L205:
	movq	-56(%rbp), %xmm0
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$2, %edx
	movhps	-64(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L209
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	movq	%rax, %rsi
	jmp	.L205
.L209:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13669:
	.size	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler6IntSubEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler6IntSubEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler6IntSubEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler6IntSubEPNS1_4NodeES4_:
.LFB13670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	cmpb	$4, 16(%rdi)
	je	.L215
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int64SubEv@PLT
	movq	%rax, %rsi
.L212:
	movq	-56(%rbp), %xmm0
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$2, %edx
	movhps	-64(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L216
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movq	%rax, %rsi
	jmp	.L212
.L216:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13670:
	.size	_ZN2v88internal8compiler14GraphAssembler6IntSubEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler6IntSubEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler6IntMulEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler6IntMulEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler6IntMulEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler6IntMulEPNS1_4NodeES4_:
.LFB13671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	cmpb	$4, 16(%rdi)
	je	.L222
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int64MulEv@PLT
	movq	%rax, %rsi
.L219:
	movq	-56(%rbp), %xmm0
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$2, %edx
	movhps	-64(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L223
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32MulEv@PLT
	movq	%rax, %rsi
	jmp	.L219
.L223:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13671:
	.size	_ZN2v88internal8compiler14GraphAssembler6IntMulEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler6IntMulEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler11IntLessThanEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler11IntLessThanEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler11IntLessThanEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler11IntLessThanEPNS1_4NodeES4_:
.LFB13672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	cmpb	$4, 16(%rdi)
	je	.L229
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int64LessThanEv@PLT
	movq	%rax, %rsi
.L226:
	movq	-56(%rbp), %xmm0
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$2, %edx
	movhps	-64(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L230
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movq	%rax, %rsi
	jmp	.L226
.L230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13672:
	.size	_ZN2v88internal8compiler14GraphAssembler11IntLessThanEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler11IntLessThanEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler12UintLessThanEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler12UintLessThanEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler12UintLessThanEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler12UintLessThanEPNS1_4NodeES4_:
.LFB13673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	cmpb	$4, 16(%rdi)
	je	.L236
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint64LessThanEv@PLT
	movq	%rax, %rsi
.L233:
	movq	-56(%rbp), %xmm0
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$2, %edx
	movhps	-64(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L237
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	movq	%rax, %rsi
	jmp	.L233
.L237:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13673:
	.size	_ZN2v88internal8compiler14GraphAssembler12UintLessThanEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler12UintLessThanEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler8Int32AddEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler8Int32AddEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler8Int32AddEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler8Int32AddEPNS1_4NodeES4_:
.LFB13674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L241
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L241:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13674:
	.size	_ZN2v88internal8compiler14GraphAssembler8Int32AddEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler8Int32AddEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler8Int32SubEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler8Int32SubEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler8Int32SubEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler8Int32SubEPNS1_4NodeES4_:
.LFB13675:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32SubEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L245
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L245:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13675:
	.size	_ZN2v88internal8compiler14GraphAssembler8Int32SubEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler8Int32SubEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler8Int32MulEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler8Int32MulEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler8Int32MulEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler8Int32MulEPNS1_4NodeES4_:
.LFB13676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32MulEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L249
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L249:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13676:
	.size	_ZN2v88internal8compiler14GraphAssembler8Int32MulEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler8Int32MulEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler20Int32LessThanOrEqualEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler20Int32LessThanOrEqualEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler20Int32LessThanOrEqualEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler20Int32LessThanOrEqualEPNS1_4NodeES4_:
.LFB13677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Int32LessThanOrEqualEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L253
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L253:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13677:
	.size	_ZN2v88internal8compiler14GraphAssembler20Int32LessThanOrEqualEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler20Int32LessThanOrEqualEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler14Uint32LessThanEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler14Uint32LessThanEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler14Uint32LessThanEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler14Uint32LessThanEPNS1_4NodeES4_:
.LFB13678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L257
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L257:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13678:
	.size	_ZN2v88internal8compiler14GraphAssembler14Uint32LessThanEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler14Uint32LessThanEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler21Uint32LessThanOrEqualEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler21Uint32LessThanOrEqualEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler21Uint32LessThanOrEqualEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler21Uint32LessThanOrEqualEPNS1_4NodeES4_:
.LFB13679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21Uint32LessThanOrEqualEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L261
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L261:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13679:
	.size	_ZN2v88internal8compiler14GraphAssembler21Uint32LessThanOrEqualEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler21Uint32LessThanOrEqualEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler14Uint64LessThanEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler14Uint64LessThanEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler14Uint64LessThanEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler14Uint64LessThanEPNS1_4NodeES4_:
.LFB13680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint64LessThanEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L265
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L265:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13680:
	.size	_ZN2v88internal8compiler14GraphAssembler14Uint64LessThanEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler14Uint64LessThanEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler21Uint64LessThanOrEqualEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler21Uint64LessThanOrEqualEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler21Uint64LessThanOrEqualEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler21Uint64LessThanOrEqualEPNS1_4NodeES4_:
.LFB13681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21Uint64LessThanOrEqualEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L269
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L269:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13681:
	.size	_ZN2v88internal8compiler14GraphAssembler21Uint64LessThanOrEqualEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler21Uint64LessThanOrEqualEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_:
.LFB13682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L273
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L273:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13682:
	.size	_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler13Int32LessThanEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler8Int64SubEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler8Int64SubEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler8Int64SubEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler8Int64SubEPNS1_4NodeES4_:
.LFB13683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int64SubEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L277
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L277:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13683:
	.size	_ZN2v88internal8compiler14GraphAssembler8Int64SubEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler8Int64SubEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler10Float64AddEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler10Float64AddEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler10Float64AddEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler10Float64AddEPNS1_4NodeES4_:
.LFB13684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64AddEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L281
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L281:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13684:
	.size	_ZN2v88internal8compiler14GraphAssembler10Float64AddEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler10Float64AddEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_:
.LFB13685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64SubEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L285
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L285:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13685:
	.size	_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler10Float64SubEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler10Float64DivEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler10Float64DivEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler10Float64DivEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler10Float64DivEPNS1_4NodeES4_:
.LFB13686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64DivEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L289
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L289:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13686:
	.size	_ZN2v88internal8compiler14GraphAssembler10Float64DivEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler10Float64DivEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler10Float64ModEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler10Float64ModEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler10Float64ModEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler10Float64ModEPNS1_4NodeES4_:
.LFB13687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10Float64ModEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L293
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L293:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13687:
	.size	_ZN2v88internal8compiler14GraphAssembler10Float64ModEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler10Float64ModEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_:
.LFB13688:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder12Float64EqualEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L297
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L297:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13688:
	.size	_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler12Float64EqualEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_:
.LFB13689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Float64LessThanEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L301
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L301:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13689:
	.size	_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler15Float64LessThanEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler22Float64LessThanOrEqualEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler22Float64LessThanOrEqualEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler22Float64LessThanOrEqualEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler22Float64LessThanOrEqualEPNS1_4NodeES4_:
.LFB13690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22Float64LessThanOrEqualEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L305
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L305:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13690:
	.size	_ZN2v88internal8compiler14GraphAssembler22Float64LessThanOrEqualEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler22Float64LessThanOrEqualEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler22Float64InsertLowWord32EPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler22Float64InsertLowWord32EPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler22Float64InsertLowWord32EPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler22Float64InsertLowWord32EPNS1_4NodeES4_:
.LFB13691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22Float64InsertLowWord32Ev@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L309
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L309:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13691:
	.size	_ZN2v88internal8compiler14GraphAssembler22Float64InsertLowWord32EPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler22Float64InsertLowWord32EPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler23Float64InsertHighWord32EPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler23Float64InsertHighWord32EPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler23Float64InsertHighWord32EPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler23Float64InsertHighWord32EPNS1_4NodeES4_:
.LFB13692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder23Float64InsertHighWord32Ev@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L313
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L313:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13692:
	.size	_ZN2v88internal8compiler14GraphAssembler23Float64InsertHighWord32EPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler23Float64InsertHighWord32EPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_:
.LFB13693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L317
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L317:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13693:
	.size	_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler11Word32EqualEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler11Word64EqualEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler11Word64EqualEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler11Word64EqualEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler11Word64EqualEPNS1_4NodeES4_:
.LFB13694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word64EqualEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	-48(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L321
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L321:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13694:
	.size	_ZN2v88internal8compiler14GraphAssembler11Word64EqualEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler11Word64EqualEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler9WordEqualEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler9WordEqualEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler9WordEqualEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler9WordEqualEPNS1_4NodeES4_:
.LFB13695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	cmpb	$4, 16(%rdi)
	je	.L327
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word64EqualEv@PLT
	movq	%rax, %rsi
.L324:
	movq	-56(%rbp), %xmm0
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$2, %edx
	movhps	-64(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L328
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	%rax, %rsi
	jmp	.L324
.L328:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13695:
	.size	_ZN2v88internal8compiler14GraphAssembler9WordEqualEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler9WordEqualEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler20Int32AddWithOverflowEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler20Int32AddWithOverflowEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler20Int32AddWithOverflowEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler20Int32AddWithOverflowEPNS1_4NodeES4_:
.LFB13696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Int32AddWithOverflowEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$3, %edx
	movq	%rbx, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L332
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L332:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13696:
	.size	_ZN2v88internal8compiler14GraphAssembler20Int32AddWithOverflowEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler20Int32AddWithOverflowEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler20Int32SubWithOverflowEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler20Int32SubWithOverflowEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler20Int32SubWithOverflowEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler20Int32SubWithOverflowEPNS1_4NodeES4_:
.LFB13697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Int32SubWithOverflowEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$3, %edx
	movq	%rbx, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L336
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L336:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13697:
	.size	_ZN2v88internal8compiler14GraphAssembler20Int32SubWithOverflowEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler20Int32SubWithOverflowEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler20Int32MulWithOverflowEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler20Int32MulWithOverflowEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler20Int32MulWithOverflowEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler20Int32MulWithOverflowEPNS1_4NodeES4_:
.LFB13698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Int32MulWithOverflowEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$3, %edx
	movq	%rbx, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L340
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L340:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13698:
	.size	_ZN2v88internal8compiler14GraphAssembler20Int32MulWithOverflowEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler20Int32MulWithOverflowEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler8Int32ModEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler8Int32ModEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler8Int32ModEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler8Int32ModEPNS1_4NodeES4_:
.LFB13699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32ModEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$3, %edx
	movq	%rbx, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L344
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L344:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13699:
	.size	_ZN2v88internal8compiler14GraphAssembler8Int32ModEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler8Int32ModEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler8Int32DivEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler8Int32DivEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler8Int32DivEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler8Int32DivEPNS1_4NodeES4_:
.LFB13700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32DivEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$3, %edx
	movq	%rbx, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L348
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L348:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13700:
	.size	_ZN2v88internal8compiler14GraphAssembler8Int32DivEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler8Int32DivEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler9Uint32ModEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler9Uint32ModEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler9Uint32ModEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler9Uint32ModEPNS1_4NodeES4_:
.LFB13701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Uint32ModEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$3, %edx
	movq	%rbx, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L352
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L352:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13701:
	.size	_ZN2v88internal8compiler14GraphAssembler9Uint32ModEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler9Uint32ModEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler9Uint32DivEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler9Uint32DivEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler9Uint32DivEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler9Uint32DivEPNS1_4NodeES4_:
.LFB13702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -64(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Uint32DivEv@PLT
	movdqa	-64(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$3, %edx
	movq	%rbx, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L356
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L356:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13702:
	.size	_ZN2v88internal8compiler14GraphAssembler9Uint32DivEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler9Uint32DivEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_:
.LFB13703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	cmpb	$4, 16(%rdi)
	je	.L362
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word64EqualEv@PLT
	movq	%rax, %rsi
.L359:
	movq	-56(%rbp), %xmm0
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$2, %edx
	movhps	-64(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L363
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	%rax, %rsi
	jmp	.L359
.L363:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13703:
	.size	_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler11IntPtrEqualEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_:
.LFB16256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r12
	cmpb	$4, 16(%rdi)
	je	.L369
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word64EqualEv@PLT
	movq	%rax, %rsi
.L366:
	movq	-56(%rbp), %xmm0
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$2, %edx
	movhps	-64(%rbp), %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L370
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	.cfi_restore_state
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	%rax, %rsi
	jmp	.L366
.L370:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16256:
	.size	_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler11TaggedEqualEPNS1_4NodeES4_
	.section	.rodata._ZN2v88internal8compiler14GraphAssembler16Float64RoundDownEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"machine()->Float64RoundDown().IsSupported()"
	.section	.rodata._ZN2v88internal8compiler14GraphAssembler16Float64RoundDownEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler14GraphAssembler16Float64RoundDownEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler16Float64RoundDownEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler16Float64RoundDownEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler16Float64RoundDownEPNS1_4NodeE:
.LFB13705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder16Float64RoundDownEv@PLT
	testb	%al, %al
	je	.L375
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder16Float64RoundDownEv@PLT
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rdx, %rsi
	movl	$1, %edx
	movq	%r12, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L376
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L376:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13705:
	.size	_ZN2v88internal8compiler14GraphAssembler16Float64RoundDownEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler16Float64RoundDownEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler14GraphAssembler20Float64RoundTruncateEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"machine()->Float64RoundTruncate().IsSupported()"
	.section	.text._ZN2v88internal8compiler14GraphAssembler20Float64RoundTruncateEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler20Float64RoundTruncateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler20Float64RoundTruncateEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler20Float64RoundTruncateEPNS1_4NodeE:
.LFB13706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Float64RoundTruncateEv@PLT
	testb	%al, %al
	je	.L381
	movq	16(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Float64RoundTruncateEv@PLT
	leaq	-48(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%rdx, %rsi
	movl	$1, %edx
	movq	%r12, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L382
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L382:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13706:
	.size	_ZN2v88internal8compiler14GraphAssembler20Float64RoundTruncateEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler20Float64RoundTruncateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler10ProjectionEiPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler10ProjectionEiPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler10ProjectionEiPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler10ProjectionEiPNS1_4NodeE:
.LFB13707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdx, -56(%rbp)
	movq	32(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10ProjectionEm@PLT
	movq	%rbx, %xmm1
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	-56(%rbp), %xmm0
	movq	%rax, %rsi
	leaq	-48(%rbp), %rcx
	movl	$2, %edx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L386
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L386:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13707:
	.size	_ZN2v88internal8compiler14GraphAssembler10ProjectionEiPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler10ProjectionEiPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE:
.LFB13708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	32(%rdi), %r14
	movq	24(%rdi), %r12
	movq	%rdx, -72(%rbp)
	movl	%esi, %edx
	movl	$4294967295, %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11AllocateRawENS1_4TypeENS0_14AllocationTypeENS0_17AllowLargeObjectsE@PLT
	movq	%r12, %xmm1
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r14, -48(%rbp)
	movq	-72(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L390
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L390:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13708:
	.size	_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler8AllocateENS0_14AllocationTypeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE:
.LFB13709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	32(%rdi), %r14
	movq	24(%rdi), %r12
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder9LoadFieldERKNS1_11FieldAccessE@PLT
	movq	%r12, %xmm1
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r14, -48(%rbp)
	movq	-72(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L394
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L394:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13709:
	.size	_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler9LoadFieldERKNS1_11FieldAccessEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler11LoadElementERKNS1_13ElementAccessEPNS1_4NodeES7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler11LoadElementERKNS1_13ElementAccessEPNS1_4NodeES7_
	.type	_ZN2v88internal8compiler14GraphAssembler11LoadElementERKNS1_13ElementAccessEPNS1_4NodeES7_, @function
_ZN2v88internal8compiler14GraphAssembler11LoadElementERKNS1_13ElementAccessEPNS1_4NodeES7_:
.LFB13710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	32(%rdi), %r13
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -96(%rbp)
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder11LoadElementERKNS1_13ElementAccessE@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r13, %xmm2
	movl	$4, %edx
	leaq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movaps	%xmm0, -80(%rbp)
	movq	%r12, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L398
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L398:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13710:
	.size	_ZN2v88internal8compiler14GraphAssembler11LoadElementERKNS1_13ElementAccessEPNS1_4NodeES7_, .-_ZN2v88internal8compiler14GraphAssembler11LoadElementERKNS1_13ElementAccessEPNS1_4NodeES7_
	.section	.text._ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_
	.type	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_, @function
_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_:
.LFB13711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	32(%rdi), %r13
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -96(%rbp)
	movq	376(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder10StoreFieldERKNS1_11FieldAccessE@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r13, %xmm2
	movl	$4, %edx
	leaq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movaps	%xmm0, -80(%rbp)
	movq	%r12, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L402
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L402:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13711:
	.size	_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_, .-_ZN2v88internal8compiler14GraphAssembler10StoreFieldERKNS1_11FieldAccessEPNS1_4NodeES7_
	.section	.text._ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_
	.type	_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_, @function
_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_:
.LFB13712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	32(%rdi), %r14
	movq	24(%rdi), %r12
	movq	%r8, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -112(%rbp)
	movq	376(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilder12StoreElementERKNS1_13ElementAccessE@PLT
	movdqa	-112(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%r12, %xmm2
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movq	%r14, -48(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	-88(%rbp), %xmm0
	movl	$5, %edx
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L406
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L406:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13712:
	.size	_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_, .-_ZN2v88internal8compiler14GraphAssembler12StoreElementERKNS1_13ElementAccessEPNS1_4NodeES7_S7_
	.section	.text._ZN2v88internal8compiler14GraphAssembler10DebugBreakEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler10DebugBreakEv
	.type	_ZN2v88internal8compiler14GraphAssembler10DebugBreakEv, @function
_ZN2v88internal8compiler14GraphAssembler10DebugBreakEv:
.LFB13713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	32(%rdi), %r13
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder10DebugBreakEv@PLT
	movq	%r13, %xmm1
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r12, %xmm0
	movl	$2, %edx
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L410
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L410:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13713:
	.size	_ZN2v88internal8compiler14GraphAssembler10DebugBreakEv, .-_ZN2v88internal8compiler14GraphAssembler10DebugBreakEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler11UnreachableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler11UnreachableEv
	.type	_ZN2v88internal8compiler14GraphAssembler11UnreachableEv, @function
_ZN2v88internal8compiler14GraphAssembler11UnreachableEv:
.LFB13714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	32(%rdi), %r13
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder11UnreachableEv@PLT
	movq	%r13, %xmm1
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r12, %xmm0
	movl	$2, %edx
	leaq	-64(%rbp), %rcx
	movq	%rax, %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L414
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L414:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13714:
	.size	_ZN2v88internal8compiler14GraphAssembler11UnreachableEv, .-_ZN2v88internal8compiler14GraphAssembler11UnreachableEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler5StoreENS1_19StoreRepresentationEPNS1_4NodeES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler5StoreENS1_19StoreRepresentationEPNS1_4NodeES5_S5_
	.type	_ZN2v88internal8compiler14GraphAssembler5StoreENS1_19StoreRepresentationEPNS1_4NodeES5_S5_, @function
_ZN2v88internal8compiler14GraphAssembler5StoreENS1_19StoreRepresentationEPNS1_4NodeES5_S5_:
.LFB13715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	32(%rdi), %r14
	movq	24(%rdi), %r12
	movq	%r8, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -112(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder5StoreENS1_19StoreRepresentationE@PLT
	movdqa	-112(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	%r12, %xmm2
	movq	%rax, %rsi
	leaq	-80(%rbp), %rcx
	movq	%r14, -48(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	-88(%rbp), %xmm0
	movl	$5, %edx
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L418
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L418:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13715:
	.size	_ZN2v88internal8compiler14GraphAssembler5StoreENS1_19StoreRepresentationEPNS1_4NodeES5_S5_, .-_ZN2v88internal8compiler14GraphAssembler5StoreENS1_19StoreRepresentationEPNS1_4NodeES5_S5_
	.section	.text._ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_
	.type	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_, @function
_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_:
.LFB13716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	32(%rdi), %r13
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -96(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder4LoadENS0_11MachineTypeE@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r13, %xmm2
	movl	$4, %edx
	leaq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movaps	%xmm0, -80(%rbp)
	movq	%r12, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L422
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L422:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13716:
	.size	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_, .-_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_
	.section	.text._ZN2v88internal8compiler14GraphAssembler14StoreUnalignedENS0_21MachineRepresentationEPNS1_4NodeES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler14StoreUnalignedENS0_21MachineRepresentationEPNS1_4NodeES5_S5_
	.type	_ZN2v88internal8compiler14GraphAssembler14StoreUnalignedENS0_21MachineRepresentationEPNS1_4NodeES5_S5_, @function
_ZN2v88internal8compiler14GraphAssembler14StoreUnalignedENS0_21MachineRepresentationEPNS1_4NodeES5_S5_:
.LFB13717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%rdx, -72(%rbp)
	movl	%esi, %edx
	movq	%rcx, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	cmpb	$2, %sil
	je	.L424
	movl	24(%rdi), %eax
	movl	32(%rdi), %r8d
	cmpl	$1, %eax
	je	.L425
	cmpl	$2, %eax
	je	.L424
	testl	%eax, %eax
	je	.L426
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L425:
	movl	%esi, %ecx
	sall	%cl, %eax
	testl	%r8d, %eax
	jne	.L426
	.p2align 4,,10
	.p2align 3
.L424:
	xorl	%eax, %eax
	movb	%dl, %al
	movb	$0, %ah
	movl	%eax, %esi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder5StoreENS1_19StoreRepresentationE@PLT
	movq	%rax, %rsi
.L428:
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdx
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %rcx
	movq	-72(%rbp), %xmm0
	movq	(%rax), %rdi
	movq	32(%rbx), %rax
	movq	%rdx, -40(%rbp)
	movl	$5, %edx
	movhps	-80(%rbp), %xmm0
	movq	%r12, -48(%rbp)
	movq	%rax, -32(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L441
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14UnalignedStoreENS0_21MachineRepresentationE@PLT
	movq	%rax, %rsi
	jmp	.L428
.L441:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13717:
	.size	_ZN2v88internal8compiler14GraphAssembler14StoreUnalignedENS0_21MachineRepresentationEPNS1_4NodeES5_S5_, .-_ZN2v88internal8compiler14GraphAssembler14StoreUnalignedENS0_21MachineRepresentationEPNS1_4NodeES5_S5_
	.section	.text._ZN2v88internal8compiler14GraphAssembler13LoadUnalignedENS0_11MachineTypeEPNS1_4NodeES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler13LoadUnalignedENS0_11MachineTypeEPNS1_4NodeES5_
	.type	_ZN2v88internal8compiler14GraphAssembler13LoadUnalignedENS0_11MachineTypeEPNS1_4NodeES5_, @function
_ZN2v88internal8compiler14GraphAssembler13LoadUnalignedENS0_11MachineTypeEPNS1_4NodeES5_:
.LFB13718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rdx, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	cmpb	$2, %sil
	je	.L443
	movl	24(%rdi), %eax
	movl	28(%rdi), %edx
	cmpl	$1, %eax
	je	.L444
	cmpl	$2, %eax
	je	.L443
	testl	%eax, %eax
	je	.L445
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L444:
	movl	%esi, %ecx
	sall	%cl, %eax
	testl	%edx, %eax
	jne	.L445
	.p2align 4,,10
	.p2align 3
.L443:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder4LoadENS0_11MachineTypeE@PLT
	movq	%rax, %rsi
.L447:
	movq	16(%rbx), %rax
	movdqu	24(%rbx), %xmm1
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %rcx
	movq	-72(%rbp), %xmm0
	movl	$4, %edx
	movq	(%rax), %rdi
	movaps	%xmm1, -48(%rbp)
	movhps	-80(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L463
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13UnalignedLoadENS0_11MachineTypeE@PLT
	movq	%rax, %rsi
	jmp	.L447
.L463:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13718:
	.size	_ZN2v88internal8compiler14GraphAssembler13LoadUnalignedENS0_11MachineTypeEPNS1_4NodeES5_, .-_ZN2v88internal8compiler14GraphAssembler13LoadUnalignedENS0_11MachineTypeEPNS1_4NodeES5_
	.section	.text._ZN2v88internal8compiler14GraphAssembler6RetainEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler6RetainEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler6RetainEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler6RetainEPNS1_4NodeE:
.LFB13719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rsi, -72(%rbp)
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6RetainEv@PLT
	movq	%r12, %xmm1
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	-72(%rbp), %xmm0
	movl	$2, %edx
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L467
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L467:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13719:
	.size	_ZN2v88internal8compiler14GraphAssembler6RetainEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler6RetainEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler16UnsafePointerAddEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler16UnsafePointerAddEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler16UnsafePointerAddEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler16UnsafePointerAddEPNS1_4NodeES4_:
.LFB13720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	32(%rdi), %r13
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movaps	%xmm0, -96(%rbp)
	movq	16(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder16UnsafePointerAddEv@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r13, %xmm2
	leaq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movl	$4, %edx
	movaps	%xmm0, -80(%rbp)
	movq	%r12, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L471
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L471:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13720:
	.size	_ZN2v88internal8compiler14GraphAssembler16UnsafePointerAddEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler16UnsafePointerAddEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler8ToNumberEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler8ToNumberEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler8ToNumberEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler8ToNumberEPNS1_4NodeE:
.LFB13721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	16(%rdi), %rdi
	movq	24(%rbx), %r12
	movq	%rsi, -152(%rbp)
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler7JSGraph12ZeroConstantEv@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler7JSGraph23ToNumberBuiltinConstantEv@PLT
	movq	(%rbx), %rsi
	movq	%rax, -136(%rbp)
	testq	%rsi, %rsi
	je	.L476
.L473:
	movq	%r12, %xmm1
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r13, %rdi
	movq	-136(%rbp), %xmm0
	leaq	-80(%rbp), %rcx
	movhps	-152(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	-144(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L477
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	.cfi_restore_state
	movq	16(%rbx), %rax
	leaq	-112(%rbp), %rdi
	movl	$102, %edx
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rax
	leaq	-128(%rbp), %rsi
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$112, %r8d
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -120(%rbp)
	movq	16(%rbx), %rax
	movq	%rcx, -128(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rsi
	jmp	.L473
.L477:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13721:
	.size	_ZN2v88internal8compiler14GraphAssembler8ToNumberEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler8ToNumberEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler19BitcastWordToTaggedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler19BitcastWordToTaggedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler19BitcastWordToTaggedEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler19BitcastWordToTaggedEPNS1_4NodeE:
.LFB13722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	32(%rdi), %r14
	movq	24(%rdi), %r12
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder19BitcastWordToTaggedEv@PLT
	movq	%r12, %xmm1
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	$3, %edx
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	movq	%r14, -48(%rbp)
	movq	-72(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L481
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L481:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13722:
	.size	_ZN2v88internal8compiler14GraphAssembler19BitcastWordToTaggedEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler19BitcastWordToTaggedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler19BitcastTaggedToWordEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler19BitcastTaggedToWordEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler19BitcastTaggedToWordEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler19BitcastTaggedToWordEPNS1_4NodeE:
.LFB13723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	32(%rdi), %r14
	movq	24(%rdi), %r12
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder19BitcastTaggedToWordEv@PLT
	movq	%r12, %xmm1
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	$3, %edx
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	movq	%r14, -48(%rbp)
	movq	-72(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L485
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L485:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13723:
	.size	_ZN2v88internal8compiler14GraphAssembler19BitcastTaggedToWordEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler19BitcastTaggedToWordEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler25BitcastTaggedSignedToWordEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler25BitcastTaggedSignedToWordEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler25BitcastTaggedSignedToWordEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler25BitcastTaggedSignedToWordEPNS1_4NodeE:
.LFB13724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	32(%rdi), %r14
	movq	24(%rdi), %r12
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder25BitcastTaggedSignedToWordEv@PLT
	movq	%r12, %xmm1
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	$3, %edx
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	movq	%r14, -48(%rbp)
	movq	-72(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L489
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L489:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13724:
	.size	_ZN2v88internal8compiler14GraphAssembler25BitcastTaggedSignedToWordEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler25BitcastTaggedSignedToWordEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler25Word32PoisonOnSpeculationEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler25Word32PoisonOnSpeculationEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler25Word32PoisonOnSpeculationEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler25Word32PoisonOnSpeculationEPNS1_4NodeE:
.LFB13725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	32(%rdi), %r14
	movq	24(%rdi), %r12
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	16(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler22MachineOperatorBuilder25Word32PoisonOnSpeculationEv@PLT
	movq	%r12, %xmm1
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	$3, %edx
	movq	%rax, %rsi
	leaq	-64(%rbp), %rcx
	movq	%r14, -48(%rbp)
	movq	-72(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 24(%rbx)
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L493
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L493:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13725:
	.size	_ZN2v88internal8compiler14GraphAssembler25Word32PoisonOnSpeculationEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler25Word32PoisonOnSpeculationEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE
	.type	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE, @function
_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE:
.LFB13726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm0
	movq	%rdx, %rcx
	movl	%esi, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	32(%rdi), %r13
	movq	24(%rdi), %r12
	movq	%r8, -96(%rbp)
	movl	%r9d, %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder12DeoptimizeIfENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceENS1_13IsSafetyCheckE@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r13, %xmm1
	movl	$4, %edx
	leaq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movaps	%xmm0, -80(%rbp)
	movq	%r12, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L497
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L497:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13726:
	.size	_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE, .-_ZN2v88internal8compiler14GraphAssembler12DeoptimizeIfENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE
	.section	.text._ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE
	.type	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE, @function
_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE:
.LFB13727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm0
	movq	%rdx, %rcx
	movl	%esi, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	32(%rdi), %r13
	movq	24(%rdi), %r12
	movq	%r8, -96(%rbp)
	movl	%r9d, %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder16DeoptimizeUnlessENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceENS1_13IsSafetyCheckE@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r13, %xmm1
	movl	$4, %edx
	leaq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movaps	%xmm0, -80(%rbp)
	movq	%r12, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L501
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L501:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13727:
	.size	_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE, .-_ZN2v88internal8compiler14GraphAssembler15DeoptimizeIfNotENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeES8_NS1_13IsSafetyCheckE
	.section	.text._ZN2v88internal8compiler14GraphAssembler21ExtractCurrentControlEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler21ExtractCurrentControlEv
	.type	_ZN2v88internal8compiler14GraphAssembler21ExtractCurrentControlEv, @function
_ZN2v88internal8compiler14GraphAssembler21ExtractCurrentControlEv:
.LFB13729:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	$0, 32(%rdi)
	ret
	.cfi_endproc
.LFE13729:
	.size	_ZN2v88internal8compiler14GraphAssembler21ExtractCurrentControlEv, .-_ZN2v88internal8compiler14GraphAssembler21ExtractCurrentControlEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler20ExtractCurrentEffectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler20ExtractCurrentEffectEv
	.type	_ZN2v88internal8compiler14GraphAssembler20ExtractCurrentEffectEv, @function
_ZN2v88internal8compiler14GraphAssembler20ExtractCurrentEffectEv:
.LFB13730:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE13730:
	.size	_ZN2v88internal8compiler14GraphAssembler20ExtractCurrentEffectEv, .-_ZN2v88internal8compiler14GraphAssembler20ExtractCurrentEffectEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler27InsertDecompressionIfNeededENS0_21MachineRepresentationEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler27InsertDecompressionIfNeededENS0_21MachineRepresentationEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler27InsertDecompressionIfNeededENS0_21MachineRepresentationEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler27InsertDecompressionIfNeededENS0_21MachineRepresentationEPNS1_4NodeE:
.LFB13731:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	ret
	.cfi_endproc
.LFE13731:
	.size	_ZN2v88internal8compiler14GraphAssembler27InsertDecompressionIfNeededENS0_21MachineRepresentationEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler27InsertDecompressionIfNeededENS0_21MachineRepresentationEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler25InsertCompressionIfNeededENS0_21MachineRepresentationEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler25InsertCompressionIfNeededENS0_21MachineRepresentationEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14GraphAssembler25InsertCompressionIfNeededENS0_21MachineRepresentationEPNS1_4NodeE, @function
_ZN2v88internal8compiler14GraphAssembler25InsertCompressionIfNeededENS0_21MachineRepresentationEPNS1_4NodeE:
.LFB16258:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	ret
	.cfi_endproc
.LFE16258:
	.size	_ZN2v88internal8compiler14GraphAssembler25InsertCompressionIfNeededENS0_21MachineRepresentationEPNS1_4NodeE, .-_ZN2v88internal8compiler14GraphAssembler25InsertCompressionIfNeededENS0_21MachineRepresentationEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14GraphAssembler5ResetEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler5ResetEPNS1_4NodeES4_
	.type	_ZN2v88internal8compiler14GraphAssembler5ResetEPNS1_4NodeES4_, @function
_ZN2v88internal8compiler14GraphAssembler5ResetEPNS1_4NodeES4_:
.LFB13733:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rdi)
	ret
	.cfi_endproc
.LFE13733:
	.size	_ZN2v88internal8compiler14GraphAssembler5ResetEPNS1_4NodeES4_, .-_ZN2v88internal8compiler14GraphAssembler5ResetEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler14GraphAssembler16ToNumberOperatorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler16ToNumberOperatorEv
	.type	_ZN2v88internal8compiler14GraphAssembler16ToNumberOperatorEv, @function
_ZN2v88internal8compiler14GraphAssembler16ToNumberOperatorEv:
.LFB13734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L511
.L507:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L512
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L511:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	$102, %edx
	leaq	-48(%rbp), %rdi
	movq	16(%rbx), %rax
	movq	360(%rax), %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-32(%rbp), %rax
	leaq	-64(%rbp), %rsi
	xorl	%r9d, %r9d
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$112, %r8d
	movl	8(%rax), %edx
	subl	(%rax), %edx
	movq	%rax, -56(%rbp)
	movq	16(%rbx), %rax
	movq	%rcx, -64(%rbp)
	xorl	%ecx, %ecx
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%rax, (%rbx)
	jmp	.L507
.L512:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13734:
	.size	_ZN2v88internal8compiler14GraphAssembler16ToNumberOperatorEv, .-_ZN2v88internal8compiler14GraphAssembler16ToNumberOperatorEv
	.section	.text._ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,"axG",@progbits,_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.type	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, @function
_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_:
.LFB14585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	32(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 4(%rsi)
	movq	8(%rsi), %rax
	je	.L550
	testl	%eax, %eax
	jne	.L527
	movq	%r14, 24(%rsi)
	movq	24(%rdi), %rdx
	movq	%rdx, 16(%rsi)
.L516:
	addq	$1, %rax
	movq	%rax, 8(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L551
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	.cfi_restore_state
	movl	%eax, %r13d
	movq	16(%r12), %rax
	movq	24(%rsi), %rdi
	movq	(%rax), %r15
	cmpl	$1, %r13d
	je	.L552
	movq	(%r15), %rsi
	movq	%r14, %rdx
	leal	1(%r13), %r14d
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%r12), %rax
	movl	%r14d, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	24(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%rbx), %rdi
	movslq	%r13d, %rax
	movq	24(%r12), %r15
	salq	$3, %rax
	movzbl	23(%rdi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L529
	leaq	32(%rdi,%rax), %rax
	movq	(%rax), %r8
	cmpq	%r8, %r15
	je	.L531
.L530:
	notl	%r13d
	movslq	%r13d, %r13
	leaq	0(%r13,%r13,2), %rdx
	leaq	(%rdi,%rdx,8), %r13
	testq	%r8, %r8
	je	.L532
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %rax
.L532:
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L548
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L548:
	movq	16(%rbx), %rdi
.L531:
	movq	16(%r12), %rax
	movq	24(%rbx), %rdx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%r12), %rax
	movl	%r14d, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L549:
	movq	8(%rbx), %rax
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L550:
	testl	%eax, %eax
	je	.L553
	movq	24(%rsi), %rsi
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L517
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %r15
	cmpq	%rdi, %r14
	je	.L520
.L519:
	leaq	-48(%rsi), %r13
	testq	%rdi, %rdi
	je	.L521
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L521:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L520
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L520:
	movq	16(%rbx), %rsi
	movq	24(%r12), %r13
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L522
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %r14
	cmpq	%rdi, %r13
	je	.L549
.L524:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L525
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L525:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L549
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	8(%rbx), %rax
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L553:
	movq	16(%rdi), %rax
	movq	%r14, %xmm0
	movl	$2, %esi
	leaq	-80(%rbp), %r14
	punpcklqdq	%xmm0, %xmm0
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4LoopEi@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r14, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, 24(%rbx)
	movq	%rax, %r13
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$3, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r13, -64(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%rbx), %r13
	movq	%rax, 16(%rbx)
	movq	%rax, -96(%rbp)
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TerminateEv@PLT
	movq	%r13, %xmm2
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rdx
	movq	16(%r12), %rax
	movq	8(%rax), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	8(%rbx), %rax
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L529:
	movq	32(%rdi), %rdx
	leaq	16(%rdx,%rax), %rax
	movq	(%rax), %r8
	cmpq	%r8, %r15
	je	.L531
	movq	%rdx, %rdi
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L552:
	movq	%r14, %xmm3
	movq	%rdi, %xmm0
	movq	8(%rax), %rdi
	movl	$2, %esi
	punpcklqdq	%xmm3, %xmm0
	leaq	-80(%rbp), %r14
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r14, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, 24(%rbx)
	movq	%rax, %r13
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	movq	16(%rbx), %r12
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movhps	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r15, %rdi
	movq	%r13, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 16(%rbx)
	movq	8(%rbx), %rax
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L517:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L520
	leaq	24(%rsi), %r15
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L522:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L549
	leaq	24(%rsi), %r14
	jmp	.L524
.L551:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14585:
	.size	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, .-_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.section	.text._ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE
	.type	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE, @function
_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE:
.LFB13728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	%r8d, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	4(%rcx), %eax
	movl	4(%r14), %ecx
	testl	%ecx, %ecx
	sete	%dil
	testl	%eax, %eax
	sete	%cl
	xorl	%esi, %esi
	cmpb	%cl, %dil
	je	.L555
	testl	%eax, %eax
	setne	%sil
	addl	$1, %esi
.L555:
	movq	16(%rbx), %rax
	movq	32(%rbx), %r15
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%r15, %xmm1
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	-88(%rbp), %xmm0
	leaq	-80(%rbp), %r15
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rcx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r13
	movq	16(%rbx), %rax
	movq	(%rax), %r9
	movq	8(%rax), %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	movq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, -80(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, 32(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r13, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, 32(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	pxor	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L561
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L561:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE13728:
	.size	_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE, .-_ZN2v88internal8compiler14GraphAssembler6BranchEPNS1_4NodeEPNS1_19GraphAssemblerLabelILm0EEES7_NS1_13IsSafetyCheckE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler14GraphAssemblerC2EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler14GraphAssemblerC2EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler14GraphAssemblerC2EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE:
.LFB16226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE16226:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler14GraphAssemblerC2EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler14GraphAssemblerC2EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler14GraphAssemblerC2EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
