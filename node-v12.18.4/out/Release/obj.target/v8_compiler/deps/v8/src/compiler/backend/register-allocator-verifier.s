	.file	"register-allocator-verifier.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB2487:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE2487:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB14336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE14336:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB14665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14665:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB14337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14337:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB14666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE14666:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier11VerifyInputERKNS2_17OperandConstraintE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"kSameAsFirst != constraint.type_"
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier11VerifyInputERKNS2_17OperandConstraintE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier11VerifyInputERKNS2_17OperandConstraintE.str1.8
	.align 8
.LC2:
	.string	"InstructionOperand::kInvalidVirtualRegister != constraint.virtual_register_"
	.section	.text._ZN2v88internal8compiler25RegisterAllocatorVerifier11VerifyInputERKNS2_17OperandConstraintE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25RegisterAllocatorVerifier11VerifyInputERKNS2_17OperandConstraintE
	.type	_ZN2v88internal8compiler25RegisterAllocatorVerifier11VerifyInputERKNS2_17OperandConstraintE, @function
_ZN2v88internal8compiler25RegisterAllocatorVerifier11VerifyInputERKNS2_17OperandConstraintE:
.LFB10838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$12, %eax
	je	.L21
	cmpl	$1, %eax
	je	.L11
	cmpl	$11, %eax
	je	.L11
	cmpl	$-1, 12(%rdi)
	je	.L22
.L11:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10838:
	.size	_ZN2v88internal8compiler25RegisterAllocatorVerifier11VerifyInputERKNS2_17OperandConstraintE, .-_ZN2v88internal8compiler25RegisterAllocatorVerifier11VerifyInputERKNS2_17OperandConstraintE
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier10VerifyTempERKNS2_17OperandConstraintE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"kImmediate != constraint.type_"
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier10VerifyTempERKNS2_17OperandConstraintE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"kExplicit != constraint.type_"
.LC5:
	.string	"kConstant != constraint.type_"
	.section	.text._ZN2v88internal8compiler25RegisterAllocatorVerifier10VerifyTempERKNS2_17OperandConstraintE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25RegisterAllocatorVerifier10VerifyTempERKNS2_17OperandConstraintE
	.type	_ZN2v88internal8compiler25RegisterAllocatorVerifier10VerifyTempERKNS2_17OperandConstraintE, @function
_ZN2v88internal8compiler25RegisterAllocatorVerifier10VerifyTempERKNS2_17OperandConstraintE:
.LFB10839:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$12, %eax
	je	.L29
	cmpl	$1, %eax
	je	.L30
	cmpl	$11, %eax
	je	.L31
	testl	%eax, %eax
	je	.L32
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10839:
	.size	_ZN2v88internal8compiler25RegisterAllocatorVerifier10VerifyTempERKNS2_17OperandConstraintE, .-_ZN2v88internal8compiler25RegisterAllocatorVerifier10VerifyTempERKNS2_17OperandConstraintE
	.section	.text._ZN2v88internal8compiler25RegisterAllocatorVerifier12VerifyOutputERKNS2_17OperandConstraintE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25RegisterAllocatorVerifier12VerifyOutputERKNS2_17OperandConstraintE
	.type	_ZN2v88internal8compiler25RegisterAllocatorVerifier12VerifyOutputERKNS2_17OperandConstraintE, @function
_ZN2v88internal8compiler25RegisterAllocatorVerifier12VerifyOutputERKNS2_17OperandConstraintE:
.LFB10840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$1, %eax
	je	.L38
	cmpl	$11, %eax
	je	.L39
	cmpl	$-1, 12(%rdi)
	je	.L40
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10840:
	.size	_ZN2v88internal8compiler25RegisterAllocatorVerifier12VerifyOutputERKNS2_17OperandConstraintE, .-_ZN2v88internal8compiler25RegisterAllocatorVerifier12VerifyOutputERKNS2_17OperandConstraintE
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier15BuildConstraintEPKNS1_18InstructionOperandEPNS2_17OperandConstraintE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"op->IsUnallocated()"
.LC7:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler25RegisterAllocatorVerifier15BuildConstraintEPKNS1_18InstructionOperandEPNS2_17OperandConstraintE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25RegisterAllocatorVerifier15BuildConstraintEPKNS1_18InstructionOperandEPNS2_17OperandConstraintE
	.type	_ZN2v88internal8compiler25RegisterAllocatorVerifier15BuildConstraintEPKNS1_18InstructionOperandEPNS2_17OperandConstraintE, @function
_ZN2v88internal8compiler25RegisterAllocatorVerifier15BuildConstraintEPKNS1_18InstructionOperandEPNS2_17OperandConstraintE:
.LFB10842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	movl	$-2147483648, 4(%rdx)
	movl	$-1, 12(%rdx)
	movq	(%rsi), %rdx
	movl	%edx, %eax
	andl	$7, %eax
	cmpl	$2, %eax
	je	.L63
	cmpl	$4, %eax
	je	.L64
	cmpl	$3, %eax
	je	.L65
	cmpl	$1, %eax
	jne	.L66
	shrq	$3, %rdx
	movl	%edx, 12(%rbx)
	movq	(%rsi), %rax
	btq	$35, %rax
	jc	.L47
	movl	$7, (%rbx)
	movq	(%rsi), %rax
	sarq	$36, %rax
	movl	%eax, 4(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movl	$11, (%rbx)
.L41:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movl	$0, (%rbx)
	movq	(%rsi), %rax
	shrq	$3, %rax
	movl	%eax, 4(%rbx)
	movl	%eax, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	%rax, %r8
	leaq	.L50(%rip), %r9
	shrq	$36, %r8
	andl	$7, %r8d
	movslq	(%r9,%r8,4), %rcx
	addq	%r9, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier15BuildConstraintEPKNS1_18InstructionOperandEPNS2_17OperandConstraintE,"a",@progbits
	.align 4
	.align 4
.L50:
	.long	.L48-.L50
	.long	.L48-.L50
	.long	.L55-.L50
	.long	.L54-.L50
	.long	.L53-.L50
	.long	.L52-.L50
	.long	.L51-.L50
	.long	.L49-.L50
	.section	.text._ZN2v88internal8compiler25RegisterAllocatorVerifier15BuildConstraintEPKNS1_18InstructionOperandEPNS2_17OperandConstraintE
	.p2align 4,,10
	.p2align 3
.L65:
	sarq	$32, %rdx
	movl	$1, (%rbx)
	movl	%edx, 4(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movl	$12, (%rbx)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$6, (%rbx)
	movq	16(%rdi), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	subl	$1, %eax
	cmpb	$13, %al
	ja	.L60
	movzbl	%al, %eax
	leaq	CSWTCH.268(%rip), %rdx
	movl	(%rdx,%rax,4), %eax
	movl	%eax, 4(%rbx)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L52:
	movq	16(%rdi), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	cmpb	$12, %al
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
	movl	%eax, (%rbx)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$5, (%rbx)
.L62:
	movq	(%rsi), %rax
	shrq	$41, %rax
	andl	$63, %eax
	movl	%eax, 4(%rbx)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$10, (%rbx)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L54:
	btq	$40, %rax
	jnc	.L57
	movl	$13, (%rbx)
	movq	(%rsi), %rax
	shrq	$47, %rax
	andl	$7, %eax
	movl	%eax, 8(%rbx)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L48:
	movq	16(%rdi), %rdi
	movl	%edx, %esi
	call	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	cmpb	$11, %al
	seta	%al
	movzbl	%al, %eax
	addl	$8, %eax
	movl	%eax, (%rbx)
	jmp	.L41
.L57:
	movl	$3, (%rbx)
	jmp	.L62
.L60:
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10842:
	.size	_ZN2v88internal8compiler25RegisterAllocatorVerifier15BuildConstraintEPKNS1_18InstructionOperandEPNS2_17OperandConstraintE, .-_ZN2v88internal8compiler25RegisterAllocatorVerifier15BuildConstraintEPKNS1_18InstructionOperandEPNS2_17OperandConstraintE
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier15CheckConstraintEPKNS1_18InstructionOperandEPKNS2_17OperandConstraintE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"ConstantOperand::cast(op)->virtual_register() == constraint->value_"
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier15CheckConstraintEPKNS1_18InstructionOperandEPKNS2_17OperandConstraintE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"value == constraint->value_"
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier15CheckConstraintEPKNS1_18InstructionOperandEPKNS2_17OperandConstraintE.str1.8
	.align 8
.LC10:
	.string	"LocationOperand::cast(op)->register_code() == constraint->value_"
	.align 8
.LC11:
	.string	"LocationOperand::cast(op)->index() == constraint->value_"
	.align 8
.LC12:
	.string	"ElementSizeLog2Of(LocationOperand::cast(op)->representation()) == constraint->value_"
	.section	.text._ZN2v88internal8compiler25RegisterAllocatorVerifier15CheckConstraintEPKNS1_18InstructionOperandEPKNS2_17OperandConstraintE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25RegisterAllocatorVerifier15CheckConstraintEPKNS1_18InstructionOperandEPKNS2_17OperandConstraintE
	.type	_ZN2v88internal8compiler25RegisterAllocatorVerifier15CheckConstraintEPKNS1_18InstructionOperandEPKNS2_17OperandConstraintE, @function
_ZN2v88internal8compiler25RegisterAllocatorVerifier15CheckConstraintEPKNS1_18InstructionOperandEPKNS2_17OperandConstraintE:
.LFB10843:
	.cfi_startproc
	endbr64
	cmpl	$13, (%rdx)
	ja	.L117
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rdx), %eax
	leaq	.L70(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier15CheckConstraintEPKNS1_18InstructionOperandEPKNS2_17OperandConstraintE,"a",@progbits
	.align 4
	.align 4
.L70:
	.long	.L82-.L70
	.long	.L81-.L70
	.long	.L80-.L70
	.long	.L69-.L70
	.long	.L79-.L70
	.long	.L78-.L70
	.long	.L77-.L70
	.long	.L76-.L70
	.long	.L75-.L70
	.long	.L74-.L70
	.long	.L73-.L70
	.long	.L72-.L70
	.long	.L71-.L70
	.long	.L69-.L70
	.section	.text._ZN2v88internal8compiler25RegisterAllocatorVerifier15CheckConstraintEPKNS1_18InstructionOperandEPKNS2_17OperandConstraintE
	.p2align 4,,10
	.p2align 3
.L69:
	movq	(%rsi), %rax
	testb	$4, %al
	je	.L84
	testb	$24, %al
	jne	.L84
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	jbe	.L130
	.p2align 4,,10
	.p2align 3
.L84:
	movq	168(%rdi), %rsi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L79:
	movq	(%rsi), %rax
	testb	$4, %al
	je	.L84
	testb	$24, %al
	jne	.L84
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L78:
	movq	(%rsi), %rax
	testb	$4, %al
	je	.L84
	testb	$24, %al
	jne	.L84
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$11, %cl
	jbe	.L84
.L130:
	sarq	$35, %rax
	cmpl	%eax, 4(%rdx)
	je	.L67
	leaq	.LC10(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	movq	(%rsi), %rax
	testb	$4, %al
	je	.L84
	movq	%rax, %rcx
	shrq	$3, %rcx
	andl	$3, %ecx
	cmpl	$1, %ecx
	jne	.L84
	shrq	$5, %rax
	movl	4(%rdx), %ecx
	subl	$1, %eax
	cmpb	$13, %al
	ja	.L86
	movzbl	%al, %eax
	leaq	CSWTCH.268(%rip), %rdx
	cmpl	(%rdx,%rax,4), %ecx
	je	.L67
	leaq	.LC12(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L76:
	movq	(%rsi), %rax
	testb	$4, %al
	je	.L84
	movq	%rax, %rcx
	shrq	$3, %rcx
	andl	$3, %ecx
	cmpl	$1, %ecx
	jne	.L84
	sarq	$35, %rax
	cmpl	%eax, 4(%rdx)
	je	.L67
	leaq	.LC11(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L75:
	movq	(%rsi), %rax
	testb	$4, %al
	je	.L84
.L133:
	movq	%rax, %rdx
	shrq	$3, %rdx
	andl	$3, %edx
	jne	.L91
.L127:
	shrq	$5, %rax
	cmpb	$11, %al
	ja	.L84
.L67:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	(%rsi), %rax
	testb	$4, %al
	je	.L84
	movq	%rax, %rdx
	shrq	$3, %rdx
	andl	$3, %edx
	je	.L123
	cmpl	$1, %edx
	jne	.L84
	.p2align 4,,10
	.p2align 3
.L123:
	shrq	$5, %rax
	cmpb	$11, %al
	jbe	.L84
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movq	(%rsi), %rax
	andl	$7, %eax
	cmpl	$4, %eax
	jne	.L84
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$2, %ecx
	jne	.L84
	shrq	$3, %rax
	cmpl	%eax, 4(%rdx)
	je	.L67
	leaq	.LC8(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L81:
	movq	(%rsi), %rax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	jne	.L84
	sarq	$32, %rax
	cmpl	%eax, 4(%rdx)
	je	.L67
	leaq	.LC9(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L80:
	movq	(%rsi), %rax
	testb	$4, %al
	je	.L84
	testb	$24, %al
	jne	.L84
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%rsi), %rax
	testb	$4, %al
	jne	.L133
	andl	$7, %eax
	cmpl	$2, %eax
	jne	.L84
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L71:
	.cfi_restore_state
	movq	168(%rdi), %rsi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L91:
	cmpl	$1, %edx
	je	.L127
	jmp	.L84
.L117:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	ret
.L86:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10843:
	.size	_ZN2v88internal8compiler25RegisterAllocatorVerifier15CheckConstraintEPKNS1_18InstructionOperandEPKNS2_17OperandConstraintE, .-_ZN2v88internal8compiler25RegisterAllocatorVerifier15CheckConstraintEPKNS1_18InstructionOperandEPKNS2_17OperandConstraintE
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier16VerifyAssignmentEPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"sequence()->instructions().size() == constraints()->size()"
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier16VerifyAssignmentEPKc.str1.1,"aMS",@progbits,1
.LC14:
	.string	"instr == *instr_it"
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier16VerifyAssignmentEPKc.str1.8
	.align 8
.LC15:
	.string	"operand_count == OperandCount(instr)"
	.section	.text._ZN2v88internal8compiler25RegisterAllocatorVerifier16VerifyAssignmentEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25RegisterAllocatorVerifier16VerifyAssignmentEPKc
	.type	_ZN2v88internal8compiler25RegisterAllocatorVerifier16VerifyAssignmentEPKc, @function
_ZN2v88internal8compiler25RegisterAllocatorVerifier16VerifyAssignmentEPKc:
.LFB10841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%rsi, 168(%rdi)
	movq	40(%rdi), %rax
	movq	%rsi, -144(%rbp)
	movq	16(%rdi), %rsi
	movq	32(%rdi), %r8
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	movabsq	$-6148914691236517205, %rax
	movq	232(%rsi), %rcx
	movq	264(%rsi), %rdx
	subq	%r8, %rdi
	movq	224(%rsi), %rbx
	movq	208(%rsi), %r11
	subq	%rcx, %rdx
	sarq	$3, %rdi
	movq	%rcx, -152(%rbp)
	sarq	$3, %rdx
	imulq	%rax, %rdi
	movq	240(%rsi), %rax
	movq	%rbx, -104(%rbp)
	subq	$1, %rdx
	subq	248(%rsi), %rax
	movq	%r11, -64(%rbp)
	movq	%rdx, %rcx
	sarq	$3, %rax
	salq	$6, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	%rbx, %rax
	subq	%r11, %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rax, %rdi
	jne	.L198
	movq	-88(%rbp), %rbx
	movq	%r8, -56(%rbp)
	cmpq	%rbx, %r8
	je	.L137
.L165:
	movq	-56(%rbp), %rax
	movl	$1, %r8d
	movq	(%rax), %rbx
.L149:
	movq	(%rbx,%r8,8), %rax
	testq	%rax, %rax
	je	.L140
	movq	8(%rax), %rcx
	movq	16(%rax), %rdi
	cmpq	%rdi, %rcx
	je	.L140
	.p2align 4,,10
	.p2align 3
.L148:
	movq	(%rcx), %rdx
	movq	(%rdx), %rax
	movl	%eax, %esi
	andl	$7, %esi
	je	.L141
	testb	$4, %al
	je	.L142
	xorl	%r9d, %r9d
	testb	$24, %al
	jne	.L143
	movq	%rax, %r9
	shrq	$5, %r9
	cmpb	$12, %r9b
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
.L143:
	andq	$-8161, %rax
	orq	%r9, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L142:
	movq	8(%rdx), %rdx
	movl	%edx, %r9d
	testb	$4, %dl
	je	.L144
	xorl	%r10d, %r10d
	testb	$24, %dl
	jne	.L145
	movq	%rdx, %r10
	shrq	$5, %r10
	cmpb	$12, %r10b
	sbbq	%r10, %r10
	notq	%r10
	andl	$416, %r10d
.L145:
	andq	$-8161, %rdx
	orq	%r10, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L144:
	cmpq	%rax, %rdx
	je	.L141
	cmpl	$2, %esi
	je	.L172
	cmpl	$5, %esi
	jne	.L146
.L172:
	andl	$7, %r9d
	cmpl	$5, %r9d
	jne	.L146
.L141:
	addq	$8, %rcx
	cmpq	%rcx, %rdi
	jne	.L148
.L140:
	cmpq	$2, %r8
	jne	.L170
	movq	-56(%rbp), %rax
	movq	8(%rax), %rcx
	movq	-64(%rbp), %rax
	cmpq	%rbx, (%rax)
	jne	.L199
	movl	4(%rbx), %eax
	movl	%eax, %edx
	movzbl	%al, %esi
	shrl	$24, %eax
	shrl	$8, %edx
	andl	$63, %eax
	movl	%esi, -108(%rbp)
	movzwl	%dx, %edi
	movq	%rsi, -96(%rbp)
	movq	%rdi, -72(%rbp)
	movq	%rdi, %rdx
	addq	%rsi, %rdi
	movq	%rdi, -120(%rbp)
	addq	%rax, %rdi
	movl	%eax, -112(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rdi, -136(%rbp)
	cmpq	%rdi, %rcx
	jne	.L200
	movq	-56(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, -80(%rbp)
	testl	%edx, %edx
	je	.L201
	movq	-72(%rbp), %r13
	movq	-80(%rbp), %rax
	leaq	40(%rbx,%rsi,8), %r14
	salq	$4, %r13
	movq	%rax, %r15
	addq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$16, %r15
	call	_ZN2v88internal8compiler25RegisterAllocatorVerifier15CheckConstraintEPKNS1_18InstructionOperandEPKNS2_17OperandConstraintE
	addq	$8, %r14
	cmpq	%r15, %r13
	jne	.L155
.L156:
	movl	-112(%rbp), %edx
	testl	%edx, %edx
	je	.L154
	movq	-120(%rbp), %rax
	movq	-72(%rbp), %rdx
	leaq	40(%rbx,%rax,8), %r14
	movq	-80(%rbp), %rax
	salq	$4, %rdx
	leaq	(%rax,%rdx), %r15
	movq	-136(%rbp), %rax
	leaq	40(%rbx,%rax,8), %r13
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$8, %r14
	call	_ZN2v88internal8compiler25RegisterAllocatorVerifier15CheckConstraintEPKNS1_18InstructionOperandEPKNS2_17OperandConstraintE
	addq	$16, %r15
	cmpq	%r14, %r13
	jne	.L159
	movq	-128(%rbp), %rdi
	addq	%rdi, -72(%rbp)
.L154:
	movl	-108(%rbp), %eax
	testl	%eax, %eax
	je	.L158
	movq	-72(%rbp), %rax
	movq	-96(%rbp), %r14
	addq	$40, %rbx
	movq	-80(%rbp), %rdi
	movq	%rax, %r13
	addq	%rax, %r14
	salq	$4, %r13
	salq	$4, %r14
	addq	%rdi, %r13
	leaq	(%rdi,%r14), %r15
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addq	$16, %r13
	call	_ZN2v88internal8compiler25RegisterAllocatorVerifier15CheckConstraintEPKNS1_18InstructionOperandEPKNS2_17OperandConstraintE
	addq	$8, %rbx
	cmpq	%r13, %r15
	jne	.L161
.L158:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	je	.L202
	addq	$24, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -88(%rbp)
	jne	.L165
.L137:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movq	-144(%rbp), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L170:
	movl	$2, %r8d
	jmp	.L149
.L202:
	movq	-152(%rbp), %rbx
	addq	$24, -56(%rbp)
	leaq	8(%rbx), %rax
	movq	8(%rbx), %rbx
	movq	%rbx, -64(%rbp)
	addq	$512, %rbx
	movq	%rbx, -104(%rbp)
	movq	-56(%rbp), %rbx
	cmpq	%rbx, -88(%rbp)
	je	.L137
	movq	%rax, -152(%rbp)
	jmp	.L165
.L199:
	leaq	.LC14(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L200:
	leaq	.LC15(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L201:
	movq	$0, -72(%rbp)
	jmp	.L156
.L198:
	leaq	.LC13(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10841:
	.size	_ZN2v88internal8compiler25RegisterAllocatorVerifier16VerifyAssignmentEPKc, .-_ZN2v88internal8compiler25RegisterAllocatorVerifier16VerifyAssignmentEPKc
	.section	.text._ZN2v88internal8compiler16BlockAssessments13DropRegistersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16BlockAssessments13DropRegistersEv
	.type	_ZN2v88internal8compiler16BlockAssessments13DropRegistersEv, @function
_ZN2v88internal8compiler16BlockAssessments13DropRegistersEv:
.LFB10850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	16(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	32(%rdi), %rbx
	cmpq	%r13, %rbx
	je	.L203
	.p2align 4,,10
	.p2align 3
.L204:
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rbx
	movq	32(%r12), %rax
	testb	$4, %al
	je	.L206
	testb	$24, %al
	jne	.L206
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	subq	$1, 48(%r14)
.L206:
	cmpq	%rbx, %r13
	jne	.L204
.L203:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10850:
	.size	_ZN2v88internal8compiler16BlockAssessments13DropRegistersEv, .-_ZN2v88internal8compiler16BlockAssessments13DropRegistersEv
	.section	.rodata._ZNK2v88internal8compiler16BlockAssessments5PrintEv.str1.1,"aMS",@progbits,1
.LC16:
	.string	" : "
.LC17:
	.string	"v"
.LC18:
	.string	"assessment->kind() == Final"
.LC19:
	.string	"P"
	.section	.text._ZNK2v88internal8compiler16BlockAssessments5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler16BlockAssessments5PrintEv
	.type	_ZNK2v88internal8compiler16BlockAssessments5PrintEv, @function
_ZNK2v88internal8compiler16BlockAssessments5PrintEv:
.LFB10851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-400(%rbp), %r13
	addq	$16, %r14
	pushq	%r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%si, -96(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movq	%rax, -320(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	16(%r14), %r12
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	cmpq	%r14, %r12
	je	.L226
	leaq	-408(%rbp), %r15
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L236:
	leaq	.LC17(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L234
	movl	4(%rbx), %esi
	movq	%r13, %rdi
	call	_ZNSolsEi@PLT
.L217:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-160(%rbp,%rax), %rbx
	testq	%rbx, %rbx
	je	.L222
	cmpb	$0, 56(%rbx)
	je	.L219
	movsbl	67(%rbx), %esi
.L220:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r14
	je	.L235
.L221:
	movq	32(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	40(%r12), %rbx
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_18InstructionOperandE@PLT
	movl	$3, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%rbx), %ecx
	movl	$1, %edx
	testl	%ecx, %ecx
	je	.L236
	leaq	.LC19(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L219:
	movq	%rbx, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%rbx), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L220
	movq	%rbx, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L235:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
.L214:
	movq	-160(%rbp,%rax), %r12
	testq	%r12, %r12
	je	.L222
	cmpb	$0, 56(%r12)
	je	.L223
	movsbl	67(%r12), %esi
.L224:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	-424(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L237
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	leaq	.LC18(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L224
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L224
.L226:
	movl	$80, %eax
	jmp	.L214
.L222:
	call	_ZSt16__throw_bad_castv@PLT
.L237:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10851:
	.size	_ZNK2v88internal8compiler16BlockAssessments5PrintEv, .-_ZNK2v88internal8compiler16BlockAssessments5PrintEv
	.section	.text._ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_
	.type	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_, @function
_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_:
.LFB12537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L252
	movl	(%rsi), %esi
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L259:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L241
.L260:
	movq	%rax, %r12
.L240:
	movl	32(%r12), %ecx
	cmpl	%ecx, %esi
	jl	.L259
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L260
.L241:
	testb	%dl, %dl
	jne	.L239
	cmpl	%ecx, %esi
	jle	.L245
.L250:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L261
.L246:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$39, %rax
	jbe	.L262
	leaq	40(%r13), %rax
	movq	%rax, 16(%rdi)
.L248:
	movl	(%r14), %eax
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	%r8d, %edi
	movl	%eax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movq	%r15, %r12
.L239:
	cmpq	%r12, 32(%rbx)
	je	.L250
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%r14), %ecx
	cmpl	%ecx, 32(%rax)
	jl	.L250
	movq	%rax, %r12
.L245:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movl	32(%r12), %eax
	cmpl	%eax, (%r14)
	setl	%r8b
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L262:
	movl	$40, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L248
	.cfi_endproc
.LFE12537:
	.size	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_, .-_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB12558:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L271
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L265:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L265
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE12558:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE4findERS5_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE4findERS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE4findERS5_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE4findERS5_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE4findERS5_:
.LFB12565:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	leaq	16(%rdi), %r8
	testq	%rax, %rax
	je	.L324
	movq	(%rsi), %rcx
	movq	%rcx, %rdx
	movq	%rcx, %rdi
	movq	%rcx, %rsi
	shrq	$3, %rdx
	shrq	$5, %rdi
	andq	$-8161, %rsi
	andl	$3, %edx
	testb	$4, %cl
	jne	.L339
	movq	%r8, %rdi
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L341:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L340
.L276:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L292
	testb	$24, %dl
	je	.L293
	xorl	%esi, %esi
.L294:
	andq	$-8161, %rdx
	orq	%rsi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L292:
	cmpq	%rdx, %rcx
	ja	.L341
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L276
.L340:
	cmpq	%rdi, %r8
	je	.L324
.L307:
	movq	32(%rdi), %rax
	testb	$4, %al
	je	.L303
	xorl	%edx, %edx
	testb	$24, %al
	je	.L342
.L304:
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L303:
	cmpq	%rcx, %rax
	cmovbe	%rdi, %r8
.L324:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	testl	%edx, %edx
	jne	.L309
	andq	$-8168, %rcx
	cmpb	$11, %dil
	movq	%r8, %rdi
	ja	.L310
	orq	$4, %rcx
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L343:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L291
.L285:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L279
	testb	$24, %dl
	je	.L280
	xorl	%r9d, %r9d
.L281:
	andq	$-8161, %rdx
	orq	%r9, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L279:
	cmpq	%rcx, %rdx
	jb	.L343
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L285
	.p2align 4,,10
	.p2align 3
.L291:
	cmpq	%rdi, %r8
	je	.L324
.L305:
	movq	%rsi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L293:
	movq	%rdx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L309:
	andq	$-8168, %rcx
	movq	%r8, %rdi
	orq	$4, %rcx
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L344:
	movq	24(%rax), %rax
.L290:
	testq	%rax, %rax
	je	.L291
.L277:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L286
	testb	$24, %dl
	je	.L287
	xorl	%r9d, %r9d
.L288:
	andq	$-8161, %rdx
	orq	%r9, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L286:
	cmpq	%rcx, %rdx
	jb	.L344
	movq	%rax, %rdi
	movq	16(%rax), %rax
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L287:
	movq	%rdx, %r9
	shrq	$5, %r9
	cmpb	$12, %r9b
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L342:
	movq	%rax, %rdx
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L310:
	orq	$420, %rcx
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L346:
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L345
.L278:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L299
	xorl	%r9d, %r9d
	testb	$24, %dl
	jne	.L300
	movq	%rdx, %r9
	shrq	$5, %r9
	cmpb	$12, %r9b
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
.L300:
	andq	$-8161, %rdx
	orq	%r9, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L299:
	cmpq	%rdx, %rcx
	jbe	.L346
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L278
.L345:
	orq	$416, %rsi
	cmpq	%r8, %rdi
	jne	.L305
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L280:
	movq	%rdx, %r9
	shrq	$5, %r9
	cmpb	$12, %r9b
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
	jmp	.L281
	.cfi_endproc
.LFE12565:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE4findERS5_, .-_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE4findERS5_
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_iESt10_Select1stIS6_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS4_IS3_iEEEES4_ISt17_Rb_tree_iteratorIS6_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_iESt10_Select1stIS6_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS4_IS3_iEEEES4_ISt17_Rb_tree_iteratorIS6_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_iESt10_Select1stIS6_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS4_IS3_iEEEES4_ISt17_Rb_tree_iteratorIS6_EbEDpOT_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_iESt10_Select1stIS6_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS4_IS3_iEEEES4_ISt17_Rb_tree_iteratorIS6_EbEDpOT_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_iESt10_Select1stIS6_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS4_IS3_iEEEES4_ISt17_Rb_tree_iteratorIS6_EbEDpOT_:
.LFB12594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$47, %rax
	jbe	.L447
	leaq	48(%r13), %rax
	movq	%rax, 16(%rdi)
.L349:
	movq	(%r14), %r9
	movl	8(%r14), %eax
	leaq	16(%r12), %r14
	movq	%r9, 32(%r13)
	movl	%eax, 40(%r13)
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L350
	movq	%r9, %rax
	movq	%r9, %rdx
	movl	%r9d, %r8d
	movq	%r9, %rcx
	shrq	$3, %rax
	shrq	$5, %rdx
	andl	$3, %eax
	andl	$4, %r8d
	je	.L351
	testl	%eax, %eax
	je	.L443
	andq	$-8168, %r9
	orq	$4, %r9
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L448:
	movq	24(%rbx), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L359
.L449:
	movq	%rsi, %rbx
.L352:
	movq	32(%rbx), %rdx
	movl	%edx, %edi
	movq	%rdx, %rsi
	andl	$4, %edi
	je	.L361
	testb	$24, %dl
	je	.L362
	xorl	%eax, %eax
.L363:
	movq	%rdx, %rsi
	andq	$-8161, %rsi
	orq	%rax, %rsi
	andq	$-8, %rsi
	orq	$4, %rsi
.L361:
	cmpq	%r9, %rsi
	jbe	.L448
	movq	16(%rbx), %rsi
	movl	$1, %eax
	testq	%rsi, %rsi
	jne	.L449
	.p2align 4,,10
	.p2align 3
.L359:
	movq	%rbx, %r9
	testb	%al, %al
	jne	.L450
.L375:
	testl	%edi, %edi
	je	.L377
	xorl	%eax, %eax
	testb	$24, %dl
	jne	.L378
	movq	%rdx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L378:
	andq	$-8161, %rdx
	orq	%rax, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L377:
	testl	%r8d, %r8d
	je	.L379
	xorl	%eax, %eax
	testb	$24, %cl
	jne	.L380
	movq	%rcx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L380:
	andq	$-8161, %rcx
	orq	%rax, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L379:
	cmpq	%rdx, %rcx
	jbe	.L381
	testq	%r9, %r9
	je	.L404
.L376:
	movl	$1, %edi
	cmpq	%r9, %r14
	jne	.L451
.L382:
	movq	%r14, %rcx
	movq	%r9, %rdx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	popq	%rbx
	movq	%r13, %rax
	addq	$1, 48(%r12)
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L452:
	.cfi_restore_state
	movq	24(%rbx), %rsi
	xorl	%eax, %eax
.L370:
	testq	%rsi, %rsi
	je	.L359
	movq	%rsi, %rbx
.L351:
	movq	32(%rbx), %rdx
	movl	%edx, %edi
	movq	%rdx, %rsi
	andl	$4, %edi
	je	.L366
	testb	$24, %dl
	je	.L367
	xorl	%eax, %eax
.L368:
	movq	%rdx, %rsi
	andq	$-8161, %rsi
	orq	%rax, %rsi
	andq	$-8, %rsi
	orq	$4, %rsi
.L366:
	cmpq	%rsi, %r9
	jnb	.L452
	movq	16(%rbx), %rsi
	movl	$1, %eax
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L367:
	movq	%rdx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L404:
	xorl	%ebx, %ebx
.L381:
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movq	%rdx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L450:
	cmpq	%rbx, 32(%r12)
	je	.L376
.L388:
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	32(%r13), %rcx
	movq	%rbx, %r9
	movq	32(%rax), %rdx
	movq	%rax, %rbx
	movl	%ecx, %r8d
	movl	%edx, %edi
	andl	$4, %r8d
	andl	$4, %edi
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L443:
	movq	%r9, %rsi
	andq	$-8168, %rsi
	cmpb	$11, %dl
	ja	.L453
	orq	$4, %rsi
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L454:
	movq	24(%rbx), %r9
	xorl	%eax, %eax
.L358:
	testq	%r9, %r9
	je	.L359
	movq	%r9, %rbx
.L360:
	movq	32(%rbx), %rdx
	movl	%edx, %edi
	movq	%rdx, %rax
	andl	$4, %edi
	je	.L354
	testb	$24, %dl
	je	.L355
	xorl	%r9d, %r9d
.L356:
	movq	%rdx, %rax
	andq	$-8161, %rax
	orq	%r9, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L354:
	cmpq	%rsi, %rax
	jbe	.L454
	movq	16(%rbx), %r9
	movl	$1, %eax
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L453:
	orq	$420, %rsi
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L455:
	movq	16(%rbx), %r9
	movl	$1, %eax
.L374:
	testq	%r9, %r9
	je	.L359
	movq	%r9, %rbx
.L353:
	movq	32(%rbx), %rdx
	movl	%edx, %edi
	movq	%rdx, %rax
	andl	$4, %edi
	je	.L371
	xorl	%r9d, %r9d
	testb	$24, %dl
	jne	.L372
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
.L372:
	movq	%rdx, %rax
	andq	$-8161, %rax
	orq	%r9, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L371:
	cmpq	%rax, %rsi
	jb	.L455
	movq	24(%rbx), %r9
	xorl	%eax, %eax
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L355:
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L451:
	movq	32(%r13), %rdx
	testb	$4, %dl
	je	.L383
	xorl	%eax, %eax
	testb	$24, %dl
	je	.L456
.L384:
	andq	$-8161, %rdx
	orq	%rax, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L383:
	movq	32(%r9), %rax
	testb	$4, %al
	je	.L385
	xorl	%ecx, %ecx
	testb	$24, %al
	je	.L457
.L386:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L385:
	xorl	%edi, %edi
	cmpq	%rdx, %rax
	seta	%dil
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L350:
	movq	%r14, %rbx
	cmpq	32(%r12), %r14
	jne	.L388
	movq	%r14, %r9
	movl	$1, %edi
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L447:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L457:
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L456:
	movq	%rdx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
	jmp	.L384
	.cfi_endproc
.LFE12594:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_iESt10_Select1stIS6_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS4_IS3_iEEEES4_ISt17_Rb_tree_iteratorIS6_EbEDpOT_, .-_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_iESt10_Select1stIS6_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS4_IS3_iEEEES4_ISt17_Rb_tree_iteratorIS6_EbEDpOT_
	.section	.rodata._ZNSt6vectorIN2v88internal8compiler25RegisterAllocatorVerifier21InstructionConstraintENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC20:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal8compiler25RegisterAllocatorVerifier21InstructionConstraintENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler25RegisterAllocatorVerifier21InstructionConstraintENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler25RegisterAllocatorVerifier21InstructionConstraintENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal8compiler25RegisterAllocatorVerifier21InstructionConstraintENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIN2v88internal8compiler25RegisterAllocatorVerifier21InstructionConstraintENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB12645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$-6148914691236517205, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	8(%rdi), %r14
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	$89478485, %rax
	je	.L474
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L468
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L475
	movl	$2147483640, %esi
	movl	$2147483640, %r8d
.L460:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L476
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L463:
	addq	%rax, %r8
	leaq	24(%rax), %rdi
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L475:
	testq	%rcx, %rcx
	jne	.L477
	movl	$24, %edi
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L461:
	movdqu	(%r15), %xmm3
	movups	%xmm3, (%rax,%rdx)
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax,%rdx)
	cmpq	%r14, %rbx
	je	.L464
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L465:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L465
	leaq	-24(%rbx), %rdx
	subq	%r14, %rdx
	shrq	$3, %rdx
	leaq	48(%rax,%rdx,8), %rdi
.L464:
	cmpq	%r12, %rbx
	je	.L466
	movq	%rbx, %rdx
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L467:
	movdqu	(%rdx), %xmm2
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm2, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r12
	jne	.L467
	subq	%rbx, %r12
	leaq	-24(%r12), %rdx
	shrq	$3, %rdx
	leaq	24(%rdi,%rdx,8), %rdi
.L466:
	movq	%rax, %xmm0
	movq	%rdi, %xmm4
	movq	%r8, 24(%r13)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_restore_state
	movl	$24, %esi
	movl	$24, %r8d
	jmp	.L460
.L476:
	movq	%rdx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rdx
	jmp	.L463
.L477:
	cmpq	$89478485, %rcx
	movl	$89478485, %r8d
	cmovbe	%rcx, %r8
	imulq	$24, %r8, %r8
	movq	%r8, %rsi
	jmp	.L460
.L474:
	leaq	.LC20(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12645:
	.size	_ZNSt6vectorIN2v88internal8compiler25RegisterAllocatorVerifier21InstructionConstraintENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIN2v88internal8compiler25RegisterAllocatorVerifier21InstructionConstraintENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifierC2EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE.str1.1,"aMS",@progbits,1
.LC21:
	.string	"vector::reserve"
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifierC2EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"(instr->GetParallelMove(inner_pos)) == nullptr"
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifierC2EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE.str1.1
.LC23:
	.string	"0 < instr->InputCount()"
	.section	.text._ZN2v88internal8compiler25RegisterAllocatorVerifierC2EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25RegisterAllocatorVerifierC2EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE
	.type	_ZN2v88internal8compiler25RegisterAllocatorVerifierC2EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE, @function
_ZN2v88internal8compiler25RegisterAllocatorVerifierC2EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE:
.LFB10836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	movq	%rsi, %xmm2
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	movq	%rcx, %xmm0
	leaq	72(%rdi), %rax
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 88(%rdi)
	movq	%rax, 96(%rdi)
	leaq	128(%rdi), %rax
	movups	%xmm0, 16(%rdi)
	pxor	%xmm0, %xmm0
	movq	%rsi, 56(%rdi)
	movq	%rsi, 112(%rdi)
	movq	%rax, 144(%rdi)
	movq	%rax, 152(%rdi)
	leaq	24(%rdi), %rax
	movq	$0, 48(%rdi)
	movl	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 104(%rdi)
	movl	$0, 128(%rdi)
	movq	$0, 136(%rdi)
	movq	$0, 160(%rdi)
	movups	%xmm0, 32(%rdi)
	movq	264(%rcx), %rdx
	subq	232(%rcx), %rdx
	sarq	$3, %rdx
	movq	%rax, -128(%rbp)
	movq	240(%rcx), %rax
	subq	$1, %rdx
	subq	248(%rcx), %rax
	movq	$0, 168(%rdi)
	movq	%rdx, %rsi
	sarq	$3, %rax
	salq	$6, %rsi
	leaq	(%rsi,%rax), %rdx
	movq	224(%rcx), %rax
	subq	208(%rcx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$89478485, %rax
	ja	.L543
	testq	%rax, %rax
	jne	.L544
.L480:
	movq	224(%rcx), %rax
	movq	208(%rcx), %r13
	movq	%rax, -112(%rbp)
	movq	232(%rcx), %rax
	movq	%rax, -120(%rbp)
	movq	240(%rcx), %rax
	movq	%rax, -104(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -136(%rbp)
.L512:
	movq	%r13, -96(%rbp)
	movq	%r13, %rax
.L511:
	cmpq	%rax, -104(%rbp)
	je	.L478
	movq	(%rax), %r14
	cmpq	$0, 8(%r14)
	jne	.L484
	cmpq	$0, 16(%r14)
	jne	.L484
	movl	4(%r14), %eax
	movl	%eax, %r12d
	shrl	$8, %r12d
	movzwl	%r12w, %edx
	movl	%eax, %r12d
	movzbl	%al, %eax
	shrl	$24, %r12d
	andl	$63, %r12d
	addq	%rdx, %r12
	addq	%r12, %rax
	movq	%rax, %rsi
	movq	%rax, -144(%rbp)
	movq	16(%rbx), %rax
	salq	$4, %rsi
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	movq	24(%rbx), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L545
	addq	-88(%rbp), %rsi
	movq	%rdi, %r13
	movq	%rsi, 16(%rbx)
.L487:
	movl	4(%r14), %eax
	xorl	%r12d, %r12d
	testl	$16776960, %eax
	je	.L493
.L488:
	movzbl	%al, %eax
	movq	%r13, %rdx
	movq	%r15, %rdi
	leaq	5(%r12,%rax), %rax
	leaq	(%r14,%rax,8), %rsi
	call	_ZN2v88internal8compiler25RegisterAllocatorVerifier15BuildConstraintEPKNS1_18InstructionOperandEPNS2_17OperandConstraintE
	movl	0(%r13), %eax
	cmpl	$12, %eax
	je	.L497
	cmpl	$1, %eax
	je	.L492
	cmpl	$11, %eax
	je	.L492
	cmpl	$-1, 12(%r13)
	je	.L506
.L492:
	movl	4(%r14), %eax
	addq	$1, %r12
	addq	$16, %r13
	movl	%eax, %edx
	shrl	$8, %edx
	movzwl	%dx, %edx
	cmpq	%rdx, %r12
	jb	.L488
.L493:
	movq	%r12, %rdx
	xorl	%r9d, %r9d
	movq	%r12, %rsi
	salq	$4, %rdx
	addq	-88(%rbp), %rdx
	testl	$1056964608, %eax
	je	.L490
	movq	%rbx, -152(%rbp)
	movq	%r9, %r13
	movq	%r15, %rbx
	movq	%r14, %r15
	movq	%rdx, %r14
.L501:
	movzbl	%al, %edx
	shrl	$8, %eax
	movq	%rbx, %rdi
	leaq	5(%r13,%rdx), %rcx
	movzwl	%ax, %eax
	movq	%r14, %rdx
	addq	%rcx, %rax
	leaq	(%r15,%rax,8), %rsi
	call	_ZN2v88internal8compiler25RegisterAllocatorVerifier15BuildConstraintEPKNS1_18InstructionOperandEPNS2_17OperandConstraintE
	movl	(%r14), %eax
	cmpl	$12, %eax
	je	.L497
	cmpl	$1, %eax
	je	.L504
	cmpl	$11, %eax
	je	.L505
	testl	%eax, %eax
	je	.L546
	movl	4(%r15), %eax
	addq	$1, %r13
	addq	$16, %r14
	leaq	0(%r13,%r12), %rsi
	movl	%eax, %edx
	shrl	$24, %edx
	andl	$63, %edx
	cmpq	%rdx, %r13
	jb	.L501
	movq	%r15, %r14
	movq	%rbx, %r15
	movq	-152(%rbp), %rbx
.L490:
	testb	%al, %al
	je	.L508
	movq	-88(%rbp), %rax
	salq	$4, %rsi
	leaq	40(%r14), %rcx
	xorl	%r9d, %r9d
	movq	%rbx, -152(%rbp)
	movq	%r15, %r13
	movq	%r9, %rbx
	movq	%rcx, %r15
	addq	%rsi, %rax
	movq	%rax, %r12
.L507:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler25RegisterAllocatorVerifier15BuildConstraintEPKNS1_18InstructionOperandEPNS2_17OperandConstraintE
	movl	(%r12), %eax
	cmpl	$12, %eax
	jne	.L502
	cmpw	$0, 5(%r14)
	jne	.L503
	leaq	.LC23(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L497:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L544:
	leaq	(%rax,%rax,2), %rax
	movq	24(%rbx), %rdx
	leaq	0(,%rax,8), %r12
	movq	16(%rbx), %rax
	movq	%r12, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %r12
	ja	.L547
	addq	%rax, %rsi
	movq	%rsi, 16(%rbx)
.L482:
	movq	%rax, 32(%r15)
	movq	%rax, 40(%r15)
	addq	%r12, %rax
	movq	%rax, 48(%r15)
	jmp	.L480
.L503:
	movq	-88(%rbp), %rdi
	movl	(%rdi), %eax
	movl	%eax, (%r12)
	movl	4(%rdi), %edx
	movl	%edx, 4(%r12)
.L502:
	cmpl	$1, %eax
	je	.L504
	cmpl	$11, %eax
	je	.L505
	cmpl	$-1, 12(%r12)
	je	.L506
	movzbl	4(%r14), %eax
	addq	$1, %rbx
	addq	$8, %r15
	addq	$16, %r12
	cmpq	%rax, %rbx
	jb	.L507
	movq	-152(%rbp), %rbx
	movq	%r13, %r15
.L508:
	movq	-144(%rbp), %rax
	movq	40(%r15), %rsi
	movq	%r14, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -64(%rbp)
	cmpq	48(%r15), %rsi
	je	.L548
	movdqa	-80(%rbp), %xmm3
	addq	$8, -96(%rbp)
	movups	%xmm3, (%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	-96(%rbp), %rax
	addq	$24, 40(%r15)
	cmpq	%rax, -112(%rbp)
	jne	.L511
.L509:
	addq	$8, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	(%rax), %r13
	leaq	512(%r13), %rax
	movq	%rax, -112(%rbp)
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L484:
	leaq	.LC22(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L506:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L504:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L505:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L545:
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %r13
	jmp	.L487
.L546:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L478:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L549
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L547:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rcx
	jmp	.L482
.L548:
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler25RegisterAllocatorVerifier21InstructionConstraintENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	addq	$8, -96(%rbp)
	movq	-96(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	jne	.L511
	jmp	.L509
.L543:
	leaq	.LC21(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L549:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10836:
	.size	_ZN2v88internal8compiler25RegisterAllocatorVerifierC2EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE, .-_ZN2v88internal8compiler25RegisterAllocatorVerifierC2EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE
	.globl	_ZN2v88internal8compiler25RegisterAllocatorVerifierC1EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE
	.set	_ZN2v88internal8compiler25RegisterAllocatorVerifierC1EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE,_ZN2v88internal8compiler25RegisterAllocatorVerifierC2EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E, @function
_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E:
.LFB12729:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L558
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L552:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L552
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L558:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE12729:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E, .-_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_:
.LFB12732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L575
	movl	(%rsi), %esi
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L582:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L564
.L583:
	movq	%rax, %r12
.L563:
	movl	32(%r12), %ecx
	cmpl	%ecx, %esi
	jl	.L582
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L583
.L564:
	testb	%dl, %dl
	jne	.L562
	cmpl	%ecx, %esi
	jle	.L568
.L573:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L584
.L569:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$39, %rax
	jbe	.L585
	leaq	40(%r13), %rax
	movq	%rax, 16(%rdi)
.L571:
	movl	(%r14), %eax
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	%r8d, %edi
	movl	%eax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L575:
	.cfi_restore_state
	movq	%r15, %r12
.L562:
	cmpq	%r12, 32(%rbx)
	je	.L573
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%r14), %ecx
	cmpl	%ecx, 32(%rax)
	jl	.L573
	movq	%rax, %r12
.L568:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L584:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movl	32(%r12), %eax
	cmpl	%eax, (%r14)
	setl	%r8b
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L585:
	movl	$40, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L571
	.cfi_endproc
.LFE12732:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_, .-_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_25RegisterAllocatorVerifier18DelayedAssessmentsEESt10_Select1stIS9_ESt4lessIS3_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS4_IS3_S8_EEEES4_ISt17_Rb_tree_iteratorIS9_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_25RegisterAllocatorVerifier18DelayedAssessmentsEESt10_Select1stIS9_ESt4lessIS3_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS4_IS3_S8_EEEES4_ISt17_Rb_tree_iteratorIS9_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_25RegisterAllocatorVerifier18DelayedAssessmentsEESt10_Select1stIS9_ESt4lessIS3_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS4_IS3_S8_EEEES4_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_25RegisterAllocatorVerifier18DelayedAssessmentsEESt10_Select1stIS9_ESt4lessIS3_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS4_IS3_S8_EEEES4_ISt17_Rb_tree_iteratorIS9_EbEDpOT_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_25RegisterAllocatorVerifier18DelayedAssessmentsEESt10_Select1stIS9_ESt4lessIS3_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS4_IS3_S8_EEEES4_ISt17_Rb_tree_iteratorIS9_EbEDpOT_:
.LFB12755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L606
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
.L588:
	movl	(%r12), %edx
	movq	8(%r12), %rax
	leaq	16(%r13), %r14
	movl	%edx, 32(%rbx)
	movq	%rax, 40(%rbx)
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L590
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L608:
	movq	16(%r12), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L591
.L609:
	movq	%rax, %r12
.L590:
	movl	32(%r12), %esi
	cmpl	%esi, %edx
	jl	.L608
	movq	24(%r12), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L609
.L591:
	testb	%cl, %cl
	jne	.L610
	cmpl	%esi, %edx
	jle	.L599
.L598:
	movl	$1, %edi
	cmpq	%r14, %r12
	jne	.L611
.L596:
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	movq	%rbx, %rax
	addq	$1, 48(%r13)
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L610:
	.cfi_restore_state
	cmpq	32(%r13), %r12
	je	.L598
.L600:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	32(%rax), %ecx
	cmpl	%ecx, 32(%rbx)
	jg	.L598
	movq	%rax, %r12
.L599:
	popq	%rbx
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L611:
	.cfi_restore_state
	xorl	%edi, %edi
	movl	32(%r12), %eax
	cmpl	%eax, 32(%rbx)
	setl	%dil
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L607:
	movq	%r14, %r12
	cmpq	32(%r13), %r14
	jne	.L600
	movl	$1, %edi
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L606:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L588
	.cfi_endproc
.LFE12755:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_25RegisterAllocatorVerifier18DelayedAssessmentsEESt10_Select1stIS9_ESt4lessIS3_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS4_IS3_S8_EEEES4_ISt17_Rb_tree_iteratorIS9_EbEDpOT_, .-_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_25RegisterAllocatorVerifier18DelayedAssessmentsEESt10_Select1stIS9_ESt4lessIS3_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS4_IS3_S8_EEEES4_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE11equal_rangeERS5_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE11equal_rangeERS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE11equal_rangeERS5_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE11equal_rangeERS5_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE11equal_rangeERS5_:
.LFB13214:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	leaq	16(%rdi), %r8
	testq	%rax, %rax
	je	.L835
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rsi), %rsi
	movq	%rsi, %rbx
	movq	%rsi, %r10
	movq	%rsi, %r11
	shrq	$3, %r10
	andq	$-8168, %rbx
	shrq	$5, %r11
	movq	%rbx, %r9
	andl	$3, %r10d
	orq	$4, %r9
	cmpb	$12, %r11b
	sbbl	%edi, %edi
	notl	%edi
	andl	$13, %edi
	salq	$5, %rdi
	orq	%rbx, %rdi
	orq	$4, %rdi
	testb	$4, %sil
	je	.L614
	cmpb	$11, %r11b
	jbe	.L615
	orq	$420, %rbx
	testl	%r10d, %r10d
	jne	.L627
	.p2align 4,,10
	.p2align 3
.L616:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L653
	xorl	%r12d, %r12d
	testb	$24, %dl
	jne	.L654
	movq	%rdx, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%r12, %r12
	notq	%r12
	andl	$416, %r12d
.L654:
	movq	%rdx, %rcx
	andq	$-8161, %rcx
	orq	%r12, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
	cmpq	%rcx, %rdi
	ja	.L721
	xorl	%ecx, %ecx
	testb	$24, %dl
	je	.L842
.L657:
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L720:
	movq	16(%rax), %rcx
	cmpq	%rbx, %rdx
	jbe	.L623
	movq	%rax, %r8
	movq	%rcx, %rax
.L656:
	testq	%rax, %rax
	jne	.L616
	.p2align 4,,10
	.p2align 3
.L613:
	movq	%r8, %rax
.L833:
	popq	%rbx
	movq	%r8, %rdx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L843:
	.cfi_restore_state
	testb	$24, %dl
	je	.L645
	xorl	%edi, %edi
.L648:
	movq	%rdx, %rcx
	andq	$-8161, %rcx
	orq	%rdi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
	cmpq	%rcx, %rsi
	ja	.L646
	testb	$24, %dl
	je	.L650
	xorl	%ecx, %ecx
.L652:
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L651:
	movq	16(%rax), %rcx
	cmpq	%rdx, %rsi
	jnb	.L623
	movq	%rax, %r8
	movq	%rcx, %rax
.L649:
	testq	%rax, %rax
	je	.L613
.L614:
	movq	32(%rax), %rdx
	testb	$4, %dl
	jne	.L843
	cmpq	%rdx, %rsi
	jbe	.L651
.L646:
	movq	24(%rax), %rax
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L645:
	movq	%rdx, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L650:
	movq	%rdx, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L619:
	movq	24(%rax), %rax
.L622:
	testq	%rax, %rax
	je	.L613
.L627:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L617
	testb	$24, %dl
	je	.L618
	xorl	%edi, %edi
.L621:
	movq	%rdx, %rcx
	andq	$-8161, %rcx
	orq	%rdi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
	cmpq	%rcx, %r9
	ja	.L619
	testb	$24, %dl
	je	.L624
	xorl	%ecx, %ecx
.L626:
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L653:
	cmpq	%rdx, %rdi
	jbe	.L720
.L721:
	movq	24(%rax), %rax
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L617:
	cmpq	%rdx, %r9
	ja	.L619
.L625:
	movq	16(%rax), %rcx
	cmpq	%rdx, %r9
	jnb	.L623
	movq	%rax, %r8
	movq	%rcx, %rax
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L618:
	movq	%rdx, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L623:
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.L844
	testb	$4, %sil
	je	.L691
	testl	%r10d, %r10d
	jne	.L699
	cmpb	$11, %r11b
	jbe	.L706
	movq	%rsi, %r10
	andq	$-8168, %r10
	orq	$420, %r10
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L846:
	movq	%rdi, %r8
	movq	16(%rdi), %rdi
.L710:
	testq	%rdi, %rdi
	je	.L845
.L661:
	movq	32(%rdi), %rdx
	testb	$4, %dl
	je	.L707
	xorl	%r9d, %r9d
	testb	$24, %dl
	jne	.L708
	movq	%rdx, %r9
	shrq	$5, %r9
	cmpb	$12, %r9b
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
.L708:
	andq	$-8161, %rdx
	orq	%r9, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L707:
	cmpq	%r10, %rdx
	ja	.L846
	movq	24(%rdi), %rdi
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L847:
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L690
.L691:
	movq	32(%rdi), %rdx
	testb	$4, %dl
	je	.L685
	testb	$24, %dl
	je	.L686
	xorl	%r9d, %r9d
.L687:
	andq	$-8161, %rdx
	orq	%r9, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L685:
	cmpq	%rdx, %rsi
	jnb	.L847
	movq	%rdi, %r8
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L691
	.p2align 4,,10
	.p2align 3
.L690:
	testq	%rcx, %rcx
	je	.L833
.L683:
	movq	32(%rcx), %rdx
	testb	$4, %dl
	je	.L678
	testb	$24, %dl
	je	.L679
	xorl	%edi, %edi
.L680:
	andq	$-8161, %rdx
	orq	%rdi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L678:
	cmpq	%rdx, %rsi
	jbe	.L681
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L683
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L681:
	movq	%rcx, %rax
	movq	16(%rcx), %rcx
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L686:
	movq	%rdx, %r9
	shrq	$5, %r9
	cmpb	$12, %r9b
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L679:
	movq	%rdx, %rdi
	shrq	$5, %rdi
	cmpb	$12, %dil
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L615:
	testl	%r10d, %r10d
	je	.L628
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L629
	.p2align 4,,10
	.p2align 3
.L849:
	xorl	%edi, %edi
	testb	$24, %dl
	jne	.L630
	movq	%rdx, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
.L630:
	movq	%rdx, %rcx
	andq	$-8161, %rcx
	orq	%rdi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
	cmpq	%rcx, %r9
	ja	.L631
	xorl	%ecx, %ecx
	testb	$24, %dl
	je	.L848
.L632:
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L718:
	movq	16(%rax), %rcx
	cmpq	%rdx, %r9
	jnb	.L623
	movq	%rax, %r8
	movq	%rcx, %rax
.L633:
	testq	%rax, %rax
	je	.L613
	movq	32(%rax), %rdx
	testb	$4, %dl
	jne	.L849
.L629:
	cmpq	%rdx, %r9
	jbe	.L718
.L631:
	movq	24(%rax), %rax
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L637:
	movq	24(%rax), %rax
.L640:
	testq	%rax, %rax
	je	.L613
.L628:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L635
	testb	$24, %dl
	je	.L636
	xorl	%ebx, %ebx
.L639:
	movq	%rdx, %rcx
	andq	$-8161, %rcx
	orq	%rbx, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
	cmpq	%rcx, %rdi
	ja	.L637
	testb	$24, %dl
	je	.L641
	xorl	%ecx, %ecx
.L643:
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L635:
	cmpq	%rdx, %rdi
	ja	.L637
.L642:
	movq	16(%rax), %rcx
	cmpq	%rdx, %r9
	jnb	.L623
	movq	%rax, %r8
	movq	%rcx, %rax
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L636:
	movq	%rdx, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rbx, %rbx
	notq	%rbx
	andl	$416, %ebx
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L641:
	movq	%rdx, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L848:
	movq	%rdx, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L624:
	movq	%rdx, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L842:
	movq	%rdx, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L850:
	movq	24(%rdi), %rdi
.L697:
	testq	%rdi, %rdi
	je	.L698
.L699:
	movq	32(%rdi), %rdx
	testb	$4, %dl
	je	.L693
	testb	$24, %dl
	je	.L694
	xorl	%esi, %esi
.L695:
	andq	$-8161, %rdx
	orq	%rsi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L693:
	cmpq	%rdx, %r9
	jnb	.L850
	movq	%rdi, %r8
	movq	16(%rdi), %rdi
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L851:
	movq	24(%rdi), %rdi
.L704:
	testq	%rdi, %rdi
	je	.L705
.L706:
	movq	32(%rdi), %rdx
	testb	$4, %dl
	je	.L700
	testb	$24, %dl
	je	.L701
	xorl	%esi, %esi
.L702:
	andq	$-8161, %rdx
	orq	%rsi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L700:
	cmpq	%rdx, %r9
	jnb	.L851
	movq	%rdi, %r8
	movq	16(%rdi), %rdi
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L675:
	movq	%rcx, %rax
	movq	16(%rcx), %rcx
.L698:
	testq	%rcx, %rcx
	je	.L833
.L677:
	movq	32(%rcx), %rdx
	testb	$4, %dl
	je	.L672
	testb	$24, %dl
	je	.L673
	xorl	%esi, %esi
.L674:
	andq	$-8161, %rdx
	orq	%rsi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L672:
	cmpq	%r9, %rdx
	jnb	.L675
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L677
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L669:
	movq	%rcx, %rax
	movq	16(%rcx), %rcx
.L705:
	testq	%rcx, %rcx
	je	.L833
.L671:
	movq	32(%rcx), %rdx
	testb	$4, %dl
	je	.L666
	testb	$24, %dl
	je	.L667
	xorl	%esi, %esi
.L668:
	andq	$-8161, %rdx
	orq	%rsi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L666:
	cmpq	%rdx, %r9
	jbe	.L669
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L671
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L694:
	movq	%rdx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L673:
	movq	%rdx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
	jmp	.L674
.L667:
	movq	%rdx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
	jmp	.L668
.L701:
	movq	%rdx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
	jmp	.L702
.L835:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movq	%r8, %rax
	movq	%r8, %rdx
	ret
.L845:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	testq	%rcx, %rcx
	je	.L833
.L665:
	andq	$-8168, %rsi
	orq	$420, %rsi
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L852:
	movq	%rcx, %rax
	movq	16(%rcx), %rcx
.L715:
	testq	%rcx, %rcx
	je	.L833
.L711:
	movq	32(%rcx), %rdx
	testb	$4, %dl
	je	.L712
	xorl	%edi, %edi
	testb	$24, %dl
	jne	.L713
	movq	%rdx, %rdi
	shrq	$5, %rdi
	cmpb	$12, %dil
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
.L713:
	andq	$-8161, %rdx
	orq	%rdi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L712:
	cmpq	%rsi, %rdx
	jnb	.L852
	movq	24(%rcx), %rcx
	jmp	.L715
.L844:
	testq	%rcx, %rcx
	je	.L833
	testb	$4, %sil
	je	.L683
	testl	%r10d, %r10d
	jne	.L677
	cmpb	$11, %r11b
	ja	.L665
	jmp	.L671
	.cfi_endproc
.LFE13214:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE11equal_rangeERS5_, .-_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE11equal_rangeERS5_
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE24_M_get_insert_unique_posERS5_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE24_M_get_insert_unique_posERS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE24_M_get_insert_unique_posERS5_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE24_M_get_insert_unique_posERS5_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE24_M_get_insert_unique_posERS5_:
.LFB13233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	24(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L931
	movq	(%rsi), %r8
	movq	%r8, %rax
	movq	%r8, %rdx
	movl	%r8d, %esi
	shrq	$3, %rax
	shrq	$5, %rdx
	andl	$3, %eax
	andl	$4, %esi
	je	.L856
	testl	%eax, %eax
	je	.L928
	movq	%r8, %rdx
	andq	$-8168, %rdx
	orq	$4, %rdx
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L932:
	movq	24(%rbx), %rax
	xorl	%r10d, %r10d
	testq	%rax, %rax
	je	.L864
.L933:
	movq	%rax, %rbx
.L857:
	movq	32(%rbx), %rcx
	movl	%ecx, %r9d
	movq	%rcx, %rax
	andl	$4, %r9d
	je	.L866
	testb	$24, %cl
	je	.L867
	xorl	%r10d, %r10d
.L868:
	movq	%rcx, %rax
	andq	$-8161, %rax
	orq	%r10, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L866:
	cmpq	%rdx, %rax
	jbe	.L932
	movq	16(%rbx), %rax
	movl	$1, %r10d
	testq	%rax, %rax
	jne	.L933
	.p2align 4,,10
	.p2align 3
.L864:
	movq	%rbx, %r11
	testb	%r10b, %r10b
	jne	.L855
.L880:
	testl	%r9d, %r9d
	je	.L883
	xorl	%eax, %eax
	testb	$24, %cl
	jne	.L884
	movq	%rcx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L884:
	andq	$-8161, %rcx
	orq	%rax, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L883:
	testl	%esi, %esi
	je	.L885
	xorl	%eax, %eax
	testb	$24, %r8b
	jne	.L886
	movq	%r8, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L886:
	andq	$-8161, %r8
	orq	%rax, %r8
	andq	$-8, %r8
	orq	$4, %r8
.L885:
	xorl	%edx, %edx
	movq	%rbx, %rax
	cmpq	%rcx, %r8
	cmova	%rdx, %rax
	cmova	%r11, %rdx
.L882:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L934:
	.cfi_restore_state
	movq	24(%rbx), %rax
	xorl	%r10d, %r10d
.L875:
	testq	%rax, %rax
	je	.L864
	movq	%rax, %rbx
.L856:
	movq	32(%rbx), %rcx
	movl	%ecx, %r9d
	movq	%rcx, %rax
	andl	$4, %r9d
	je	.L871
	testb	$24, %cl
	je	.L872
	xorl	%edx, %edx
.L873:
	movq	%rcx, %rax
	andq	$-8161, %rax
	orq	%rdx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L871:
	cmpq	%rax, %r8
	jnb	.L934
	movq	16(%rbx), %rax
	movl	$1, %r10d
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L872:
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$416, %edx
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L931:
	leaq	16(%rdi), %rbx
.L855:
	xorl	%eax, %eax
	movq	%rbx, %rdx
	cmpq	32(%rdi), %rbx
	je	.L882
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r12), %r8
	movq	%rbx, %r11
	movq	32(%rax), %rcx
	movq	%rax, %rbx
	movl	%r8d, %esi
	movl	%ecx, %r9d
	andl	$4, %esi
	andl	$4, %r9d
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L867:
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%r10, %r10
	notq	%r10
	andl	$416, %r10d
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L928:
	cmpb	$11, %dl
	movq	%r8, %rdx
	ja	.L935
	andq	$-8168, %rdx
	orq	$4, %rdx
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L936:
	movq	24(%rbx), %rax
	xorl	%r10d, %r10d
.L863:
	testq	%rax, %rax
	je	.L864
	movq	%rax, %rbx
.L865:
	movq	32(%rbx), %rcx
	movl	%ecx, %r9d
	movq	%rcx, %rax
	andl	$4, %r9d
	je	.L859
	testb	$24, %cl
	je	.L860
	xorl	%r10d, %r10d
.L861:
	movq	%rcx, %rax
	andq	$-8161, %rax
	orq	%r10, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L859:
	cmpq	%rax, %rdx
	jnb	.L936
	movq	16(%rbx), %rax
	movl	$1, %r10d
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L935:
	andq	$-8168, %rdx
	orq	$420, %rdx
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L937:
	movq	16(%rbx), %rax
	movl	$1, %r10d
.L879:
	testq	%rax, %rax
	je	.L864
	movq	%rax, %rbx
.L858:
	movq	32(%rbx), %rcx
	movl	%ecx, %r9d
	movq	%rcx, %rax
	andl	$4, %r9d
	je	.L876
	xorl	%r10d, %r10d
	testb	$24, %cl
	jne	.L877
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%r10, %r10
	notq	%r10
	andl	$416, %r10d
.L877:
	movq	%rcx, %rax
	andq	$-8161, %rax
	orq	%r10, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L876:
	cmpq	%rax, %rdx
	jb	.L937
	movq	24(%rbx), %rax
	xorl	%r10d, %r10d
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L860:
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%r10, %r10
	notq	%r10
	andl	$416, %r10d
	jmp	.L861
	.cfi_endproc
.LFE13233:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE24_M_get_insert_unique_posERS5_, .-_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE24_M_get_insert_unique_posERS5_
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_:
.LFB13309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	je	.L1014
	movq	(%rdx), %r12
	movq	%rsi, %rbx
	movl	%r12d, %r15d
	movq	%r12, %rdx
	andl	$4, %r15d
	je	.L946
	xorl	%eax, %eax
	testb	$24, %r12b
	jne	.L947
	movq	%r12, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L947:
	movq	%r12, %rdx
	andq	$-8161, %rdx
	orq	%rax, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L946:
	movq	32(%rbx), %rax
	testb	$4, %al
	jne	.L1015
.L1012:
	cmpq	%rdx, %rax
	jbe	.L968
.L967:
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rbx, 32(%r13)
	je	.L1001
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	32(%rax), %rcx
	movq	%rax, %rdx
	testb	$4, %cl
	je	.L953
	xorl	%eax, %eax
	testb	$24, %cl
	jne	.L954
	movq	%rcx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L954:
	andq	$-8161, %rcx
	orq	%rax, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L953:
	testl	%r15d, %r15d
	je	.L955
	xorl	%eax, %eax
	testb	$24, %r12b
	jne	.L956
	movq	%r12, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L956:
	andq	$-8161, %r12
	orq	%rax, %r12
	andq	$-8, %r12
	orq	$4, %r12
.L955:
	cmpq	%rcx, %r12
	jbe	.L940
	cmpq	$0, 24(%rdx)
	movl	$0, %eax
	cmovne	%rbx, %rax
	cmovne	%rbx, %rdx
.L1001:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1015:
	.cfi_restore_state
	testb	$24, %al
	jne	.L949
	movq	%rax, %rcx
	andq	$-8168, %rax
	shrq	$5, %rcx
	cmpb	$11, %cl
	jbe	.L950
	orq	$420, %rax
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1014:
	cmpq	$0, 48(%rdi)
	je	.L940
	movq	40(%rdi), %rbx
	movq	32(%rbx), %rdx
	testb	$4, %dl
	je	.L941
	xorl	%eax, %eax
	testb	$24, %dl
	jne	.L942
	movq	%rdx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L942:
	andq	$-8161, %rdx
	orq	%rax, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L941:
	movq	(%r14), %rax
	testb	$4, %al
	je	.L943
	xorl	%ecx, %ecx
	testb	$24, %al
	jne	.L944
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L944:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L943:
	cmpq	%rdx, %rax
	ja	.L1013
.L940:
	addq	$8, %rsp
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE24_M_get_insert_unique_posERS5_
	.p2align 4,,10
	.p2align 3
.L950:
	.cfi_restore_state
	orq	$4, %rax
	cmpq	%rdx, %rax
	ja	.L967
	.p2align 4,,10
	.p2align 3
.L968:
	movq	%r12, %rdx
	testl	%r15d, %r15d
	je	.L958
	xorl	%ecx, %ecx
	testb	$24, %r12b
	jne	.L959
	shrq	$5, %rdx
	cmpb	$12, %dl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L959:
	movq	%r12, %rdx
	andq	$-8161, %rdx
	orq	%rcx, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L958:
	cmpq	%rax, %rdx
	jbe	.L960
	cmpq	%rbx, 40(%r13)
	je	.L1013
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	testl	%r15d, %r15d
	je	.L962
	xorl	%eax, %eax
	testb	$24, %r12b
	je	.L1016
.L963:
	andq	$-8161, %r12
	orq	%rax, %r12
	andq	$-8, %r12
	orq	$4, %r12
.L962:
	movq	32(%rdx), %rcx
	testb	$4, %cl
	je	.L964
	xorl	%eax, %eax
	testb	$24, %cl
	je	.L1017
.L965:
	andq	$-8161, %rcx
	orq	%rax, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L964:
	cmpq	%r12, %rcx
	jbe	.L940
	cmpq	$0, 24(%rbx)
	movl	$0, %eax
	cmovne	%rdx, %rax
	cmove	%rbx, %rdx
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L960:
	addq	$8, %rsp
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L949:
	.cfi_restore_state
	andq	$-8168, %rax
	orq	$4, %rax
	cmpq	%rax, %rdx
	jb	.L967
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L1013:
	addq	$8, %rsp
	movq	%rbx, %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1017:
	.cfi_restore_state
	movq	%rcx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	%r12, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
	jmp	.L963
	.cfi_endproc
.LFE13309:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_, .-_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_
	.section	.rodata._ZN2v88internal8compiler16BlockAssessments20PerformParallelMovesEPKNS1_12ParallelMoveE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"map_for_moves_.empty()"
.LC25:
	.string	"it != map_.end()"
	.section	.rodata._ZN2v88internal8compiler16BlockAssessments20PerformParallelMovesEPKNS1_12ParallelMoveE.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"map_for_moves_.find(move->destination()) == map_for_moves_.end()"
	.section	.text._ZN2v88internal8compiler16BlockAssessments20PerformParallelMovesEPKNS1_12ParallelMoveE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16BlockAssessments20PerformParallelMovesEPKNS1_12ParallelMoveE
	.type	_ZN2v88internal8compiler16BlockAssessments20PerformParallelMovesEPKNS1_12ParallelMoveE, @function
_ZN2v88internal8compiler16BlockAssessments20PerformParallelMovesEPKNS1_12ParallelMoveE:
.LFB10845:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1252
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	cmpq	$0, 104(%rdi)
	jne	.L1257
	movq	16(%rsi), %rax
	movq	8(%rsi), %r12
	leaq	16(%rdi), %rdi
	movq	%rdi, -80(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r12
	je	.L1071
	.p2align 4,,10
	.p2align 3
.L1070:
	movq	(%r12), %r14
	movq	(%r14), %rax
	testb	$7, %al
	je	.L1024
	testb	$4, %al
	je	.L1025
	xorl	%ecx, %ecx
	testb	$24, %al
	jne	.L1026
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L1026:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1025:
	movq	8(%r14), %rcx
	testb	$4, %cl
	je	.L1027
	xorl	%esi, %esi
	testb	$24, %cl
	jne	.L1028
	movq	%rcx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L1028:
	andq	$-8161, %rcx
	orq	%rsi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1027:
	cmpq	%rax, %rcx
	je	.L1024
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE4findERS5_
	movq	%rax, %r13
	cmpq	-80(%rbp), %rax
	je	.L1258
	leaq	56(%rbx), %r10
	leaq	72(%rbx), %rcx
	movq	%r10, %rdi
	leaq	8(%r14), %rsi
	movq	%rcx, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE4findERS5_
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %r10
	cmpq	%rax, %rcx
	jne	.L1259
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.L1131
	movq	8(%r14), %rdi
	movq	%rcx, %r15
	movq	%rdi, %rsi
	movq	%rdi, %r11
	movq	%rdi, %r9
	shrq	$3, %rsi
	shrq	$5, %r11
	andq	$-8161, %r9
	andl	$3, %esi
	testb	$4, %dil
	je	.L1032
	andq	$-8168, %rdi
	testl	%esi, %esi
	jne	.L1133
	cmpb	$11, %r11b
	ja	.L1134
	orq	$4, %rdi
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1261:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1260
.L1041:
	movq	32(%rax), %rsi
	testb	$4, %sil
	je	.L1035
	testb	$24, %sil
	je	.L1036
	xorl	%r11d, %r11d
.L1037:
	andq	$-8161, %rsi
	orq	%r11, %rsi
	andq	$-8, %rsi
	orq	$4, %rsi
.L1035:
	cmpq	%rsi, %rdi
	ja	.L1261
	movq	%rax, %r15
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1041
.L1260:
	cmpq	%r15, %rcx
	jne	.L1125
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1263:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1262
.L1032:
	movq	32(%rax), %rsi
	testb	$4, %sil
	je	.L1048
	testb	$24, %sil
	je	.L1049
	xorl	%r9d, %r9d
.L1050:
	andq	$-8161, %rsi
	orq	%r9, %rsi
	andq	$-8, %rsi
	orq	$4, %rsi
.L1048:
	cmpq	%rsi, %rdi
	ja	.L1263
	movq	%rax, %r15
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1032
.L1262:
	cmpq	%r15, %rcx
	je	.L1031
.L1124:
	movq	32(%r15), %rax
	testb	$4, %al
	je	.L1059
	xorl	%esi, %esi
	testb	$24, %al
	je	.L1264
.L1060:
	andq	$-8161, %rax
	orq	%rsi, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1059:
	cmpq	%rdi, %rax
	jbe	.L1061
.L1031:
	movq	56(%rbx), %rdi
	movq	16(%rdi), %r9
	movq	24(%rdi), %rax
	subq	%r9, %rax
	cmpq	$47, %rax
	jbe	.L1265
	leaq	48(%r9), %rax
	movq	%rax, 16(%rdi)
.L1063:
	movq	8(%r14), %rax
	movq	%r15, %rsi
	leaq	32(%r9), %rdx
	movq	%r10, %rdi
	movq	$0, 40(%r9)
	movq	%rax, 32(%r9)
	movq	%rcx, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_
	movq	%rax, %r15
	testq	%rdx, %rdx
	je	.L1061
	testq	%rax, %rax
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %rcx
	jne	.L1142
	cmpq	%rdx, %rcx
	jne	.L1266
.L1142:
	movl	$1, %edi
.L1065:
	movq	%r9, %rsi
	movq	%r9, -64(%rbp)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 104(%rbx)
	movq	-64(%rbp), %r9
	movq	%r9, %r15
.L1061:
	movq	40(%r13), %rax
	movq	%rax, 40(%r15)
.L1024:
	addq	$8, %r12
	cmpq	%r12, -56(%rbp)
	jne	.L1070
.L1071:
	movq	88(%rbx), %r14
	leaq	72(%rbx), %rax
	leaq	16(%rbx), %r12
	movq	%rax, -56(%rbp)
	cmpq	%r14, %rax
	je	.L1023
	.p2align 4,,10
	.p2align 3
.L1119:
	movq	24(%rbx), %rax
	movq	32(%r14), %rcx
	movq	40(%r14), %r15
	testq	%rax, %rax
	je	.L1167
	movq	%rcx, %rdx
	movq	%rcx, %rsi
	movq	%r12, %r8
	shrq	$3, %rdx
	shrq	$5, %rsi
	andl	$3, %edx
	testb	$4, %cl
	je	.L1075
	movq	%rcx, %rdi
	andq	$-8168, %rdi
	cmpb	$11, %sil
	jbe	.L1076
	testl	%edx, %edx
	je	.L1149
	orq	$4, %rdi
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1267:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1083
.L1084:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L1078
	testb	$24, %dl
	je	.L1079
	xorl	%esi, %esi
.L1080:
	andq	$-8161, %rdx
	orq	%rsi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1078:
	cmpq	%rdx, %rdi
	ja	.L1267
	movq	%rax, %r8
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1084
	.p2align 4,,10
	.p2align 3
.L1083:
	cmpq	%r8, %r12
	je	.L1167
	xorl	%eax, %eax
.L1123:
	movq	%rcx, %rdx
	andq	$-8168, %rdx
	orq	%rax, %rdx
	orq	$4, %rdx
	jmp	.L1122
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	%rsi, %r9
	shrq	$5, %r9
	cmpb	$12, %r9b
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1133:
	orq	$4, %rdi
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1269:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1268
.L1033:
	movq	32(%rax), %rsi
	testb	$4, %sil
	je	.L1042
	testb	$24, %sil
	je	.L1043
	xorl	%r11d, %r11d
.L1044:
	andq	$-8161, %rsi
	orq	%r11, %rsi
	andq	$-8, %rsi
	orq	$4, %rsi
.L1042:
	cmpq	%rsi, %rdi
	ja	.L1269
	movq	%rax, %r15
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1033
.L1268:
	cmpq	%rcx, %r15
	je	.L1031
.L1125:
	movq	%r9, %rdi
	andq	$-8, %rdi
	orq	$4, %rdi
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1264:
	movq	%rax, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1271:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1270
.L1075:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L1097
	testb	$24, %dl
	je	.L1098
	xorl	%esi, %esi
.L1099:
	andq	$-8161, %rdx
	orq	%rsi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1097:
	cmpq	%rcx, %rdx
	jb	.L1271
	movq	%rax, %r8
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1075
.L1270:
	cmpq	%r12, %r8
	je	.L1074
	movq	%rcx, %rdx
.L1122:
	movq	32(%r8), %rax
	testb	$4, %al
	je	.L1108
	xorl	%esi, %esi
	testb	$24, %al
	jne	.L1109
	movq	%rax, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L1109:
	andq	$-8161, %rax
	orq	%rsi, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1108:
	cmpq	%rdx, %rax
	jbe	.L1110
.L1074:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$47, %rax
	jbe	.L1272
	leaq	48(%r13), %rax
	movq	%rax, 16(%rdi)
.L1112:
	movq	%rcx, 32(%r13)
	movq	%r8, %rsi
	leaq	32(%r13), %rdx
	movq	%rbx, %rdi
	movq	$0, 40(%r13)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_
	movq	%rax, %r8
	testq	%rdx, %rdx
	je	.L1110
	cmpq	%rdx, %r12
	je	.L1160
	testq	%rax, %rax
	je	.L1273
.L1160:
	movl	$1, %edi
.L1114:
	movq	%r12, %rcx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	movq	%r13, %r8
.L1110:
	movq	%r15, 40(%r8)
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, -56(%rbp)
	jne	.L1119
.L1023:
	movq	80(%rbx), %r14
	leaq	56(%rbx), %r15
	testq	%r14, %r14
	je	.L1072
.L1073:
	movq	24(%r14), %r12
	testq	%r12, %r12
	je	.L1120
.L1121:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1121
.L1120:
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L1073
.L1072:
	movq	-56(%rbp), %rax
	movq	$0, 80(%rbx)
	movq	$0, 104(%rbx)
	movq	%rax, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1098:
	.cfi_restore_state
	movq	%rdx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	%rsi, %r11
	shrq	$5, %r11
	cmpb	$12, %r11b
	sbbq	%r11, %r11
	notq	%r11
	andl	$416, %r11d
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1079:
	movq	%rdx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1036:
	movq	%rsi, %r11
	shrq	$5, %r11
	cmpb	$12, %r11b
	sbbq	%r11, %r11
	notq	%r11
	andl	$416, %r11d
	jmp	.L1037
	.p2align 4,,10
	.p2align 3
.L1076:
	orq	$4, %rdi
	testl	%edx, %edx
	je	.L1085
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1274:
	movq	%rax, %r8
	movq	16(%rax), %rax
.L1089:
	testq	%rax, %rax
	je	.L1083
.L1090:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L1086
	xorl	%esi, %esi
	testb	$24, %dl
	jne	.L1087
	movq	%rdx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L1087:
	andq	$-8161, %rdx
	orq	%rsi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1086:
	cmpq	%rdx, %rdi
	jbe	.L1274
	movq	24(%rax), %rax
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1275:
	movq	24(%rax), %rax
.L1095:
	testq	%rax, %rax
	je	.L1083
.L1085:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L1091
	testb	$24, %dl
	je	.L1092
	xorl	%esi, %esi
.L1093:
	andq	$-8161, %rdx
	orq	%rsi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1091:
	cmpq	%rdx, %rdi
	ja	.L1275
	movq	%rax, %r8
	movq	16(%rax), %rax
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1092:
	movq	%rdx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1167:
	movq	%r12, %r8
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1149:
	orq	$420, %rdi
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1277:
	movq	%rax, %r8
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1276
.L1077:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L1104
	xorl	%esi, %esi
	testb	$24, %dl
	jne	.L1105
	movq	%rdx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L1105:
	andq	$-8161, %rdx
	orq	%rsi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1104:
	cmpq	%rdx, %rdi
	jbe	.L1277
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1077
.L1276:
	cmpq	%r12, %r8
	je	.L1074
	movl	$416, %eax
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1258:
	leaq	.LC25(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1134:
	orq	$420, %rdi
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1279:
	movq	%rax, %r15
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1278
.L1034:
	movq	32(%rax), %rsi
	testb	$4, %sil
	je	.L1055
	xorl	%r11d, %r11d
	testb	$24, %sil
	jne	.L1056
	movq	%rsi, %r11
	shrq	$5, %r11
	cmpb	$12, %r11b
	sbbq	%r11, %r11
	notq	%r11
	andl	$416, %r11d
.L1056:
	andq	$-8161, %rsi
	orq	%r11, %rsi
	andq	$-8, %rsi
	orq	$4, %rsi
.L1055:
	cmpq	%rsi, %rdi
	jbe	.L1279
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1034
.L1278:
	orq	$416, %r9
	cmpq	%r15, %rcx
	jne	.L1125
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1259:
	leaq	.LC26(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1131:
	movq	%rcx, %r15
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1273:
	movq	32(%r13), %rax
	testb	$4, %al
	je	.L1115
	xorl	%ecx, %ecx
	testb	$24, %al
	je	.L1280
.L1116:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1115:
	movq	32(%rdx), %rcx
	testb	$4, %cl
	je	.L1117
	xorl	%esi, %esi
	testb	$24, %cl
	je	.L1281
.L1118:
	andq	$-8161, %rcx
	orq	%rsi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1117:
	xorl	%edi, %edi
	cmpq	%rax, %rcx
	seta	%dil
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1272:
	movl	$48, %esi
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1266:
	movq	32(%r9), %rsi
	testb	$4, %sil
	je	.L1066
	xorl	%eax, %eax
	testb	$24, %sil
	je	.L1282
.L1067:
	andq	$-8161, %rsi
	orq	%rax, %rsi
	andq	$-8, %rsi
	orq	$4, %rsi
.L1066:
	movq	32(%rdx), %rax
	testb	$4, %al
	je	.L1068
	xorl	%edi, %edi
	testb	$24, %al
	je	.L1283
.L1069:
	andq	$-8161, %rax
	orq	%rdi, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1068:
	xorl	%edi, %edi
	cmpq	%rsi, %rax
	seta	%dil
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1265:
	movl	$48, %esi
	movq	%r10, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r10
	movq	%rax, %r9
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1252:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1257:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	.LC24(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1281:
	movq	%rcx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
	jmp	.L1118
.L1280:
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
	jmp	.L1116
.L1283:
	movq	%rax, %rdi
	shrq	$5, %rdi
	cmpb	$12, %dil
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
	jmp	.L1069
.L1282:
	movq	%rsi, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
	jmp	.L1067
	.cfi_endproc
.LFE10845:
	.size	_ZN2v88internal8compiler16BlockAssessments20PerformParallelMovesEPKNS1_12ParallelMoveE, .-_ZN2v88internal8compiler16BlockAssessments20PerformParallelMovesEPKNS1_12ParallelMoveE
	.section	.text._ZN2v88internal8compiler16BlockAssessments12PerformMovesEPKNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16BlockAssessments12PerformMovesEPKNS1_11InstructionE
	.type	_ZN2v88internal8compiler16BlockAssessments12PerformMovesEPKNS1_11InstructionE, @function
_ZN2v88internal8compiler16BlockAssessments12PerformMovesEPKNS1_11InstructionE:
.LFB10844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	8(%rsi), %rsi
	call	_ZN2v88internal8compiler16BlockAssessments20PerformParallelMovesEPKNS1_12ParallelMoveE
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler16BlockAssessments20PerformParallelMovesEPKNS1_12ParallelMoveE
	.cfi_endproc
.LFE10844:
	.size	_ZN2v88internal8compiler16BlockAssessments12PerformMovesEPKNS1_11InstructionE, .-_ZN2v88internal8compiler16BlockAssessments12PerformMovesEPKNS1_11InstructionE
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_16BlockAssessmentsEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_16BlockAssessmentsEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_16BlockAssessmentsEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_16BlockAssessmentsEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_, @function
_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_16BlockAssessmentsEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_:
.LFB13322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r15
	je	.L1336
	movl	(%rdx), %r14d
	cmpl	%r14d, 32(%rsi)
	jle	.L1297
	movq	32(%rdi), %rbx
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rsi, %rbx
	je	.L1289
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	%r14d, 32(%rax)
	jge	.L1299
	movl	$0, %ebx
	cmpq	$0, 24(%rax)
	movq	%rbx, %rax
	cmovne	%r12, %rdx
	cmovne	%r12, %rax
.L1289:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1297:
	.cfi_restore_state
	jge	.L1308
	movq	40(%rdi), %rdx
	cmpq	%rsi, %rdx
	je	.L1335
	movq	%rsi, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %rdx
	cmpl	32(%rax), %r14d
	jge	.L1310
	movl	$0, %ebx
	cmpq	$0, 24(%r12)
	movq	%rbx, %rax
	cmovne	%rdx, %rax
	cmove	%r12, %rdx
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1336:
	cmpq	$0, 48(%rdi)
	je	.L1288
	movq	40(%rdi), %rdx
	movl	32(%rdx), %eax
	cmpl	%eax, (%r14)
	jg	.L1335
.L1288:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.L1319
	movl	(%r14), %esi
	jmp	.L1291
	.p2align 4,,10
	.p2align 3
.L1337:
	movq	16(%rbx), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	je	.L1292
.L1338:
	movq	%rax, %rbx
.L1291:
	movl	32(%rbx), %edx
	cmpl	%edx, %esi
	jl	.L1337
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L1338
.L1292:
	movq	%rbx, %r12
	testb	%cl, %cl
	jne	.L1290
.L1295:
	xorl	%eax, %eax
	cmpl	%esi, %edx
	cmovl	%rax, %rbx
	cmovge	%rax, %r12
.L1296:
	addq	$8, %rsp
	movq	%rbx, %rax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1308:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rsi, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1335:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1299:
	.cfi_restore_state
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L1302
	jmp	.L1339
	.p2align 4,,10
	.p2align 3
.L1340:
	movq	16(%r12), %rax
	movl	$1, %esi
.L1305:
	testq	%rax, %rax
	je	.L1303
	movq	%rax, %r12
.L1302:
	movl	32(%r12), %ecx
	cmpl	%ecx, %r14d
	jl	.L1340
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1319:
	movq	%r12, %rbx
.L1290:
	cmpq	%rbx, 32(%r13)
	je	.L1321
	movq	%rbx, %rdi
	movq	%rbx, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%r14), %esi
	movl	32(%rax), %edx
	movq	%rax, %rbx
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1303:
	movq	%r12, %rdx
	testb	%sil, %sil
	jne	.L1301
.L1306:
	xorl	%eax, %eax
	cmpl	%ecx, %r14d
	cmovg	%rax, %r12
	cmovle	%rax, %rdx
.L1307:
	movq	%r12, %rax
	jmp	.L1289
.L1339:
	movq	%r15, %r12
.L1301:
	cmpq	%r12, %rbx
	je	.L1325
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r12, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %r12
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1310:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L1313
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1342:
	movq	16(%rbx), %rax
	movl	$1, %esi
.L1316:
	testq	%rax, %rax
	je	.L1314
	movq	%rax, %rbx
.L1313:
	movl	32(%rbx), %ecx
	cmpl	%ecx, %r14d
	jl	.L1342
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	jmp	.L1316
	.p2align 4,,10
	.p2align 3
.L1314:
	movq	%rbx, %rdx
	testb	%sil, %sil
	jne	.L1312
.L1317:
	xorl	%eax, %eax
	cmpl	%ecx, %r14d
	cmovg	%rax, %rbx
	cmovle	%rax, %rdx
.L1318:
	movq	%rbx, %rax
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1321:
	movq	%rbx, %r12
	xorl	%ebx, %ebx
	jmp	.L1296
.L1341:
	movq	%r15, %rbx
.L1312:
	cmpq	%rbx, 32(%r13)
	je	.L1329
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rbx, %rdx
	movl	32(%rax), %ecx
	movq	%rax, %rbx
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1325:
	movq	%r12, %rdx
	xorl	%r12d, %r12d
	jmp	.L1307
.L1329:
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	jmp	.L1318
	.cfi_endproc
.LFE13322:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_16BlockAssessmentsEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_, .-_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_16BlockAssessmentsEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier14CreateForBlockEPKNS1_16InstructionBlockE.str1.1,"aMS",@progbits,1
.LC27:
	.string	"map_.empty()"
.LC28:
	.string	"(other) != nullptr"
.LC29:
	.string	"pred_id >= current_block_id"
.LC30:
	.string	"block->IsLoopHeader()"
.LC31:
	.string	"(pred_assessments) != nullptr"
	.section	.text._ZN2v88internal8compiler25RegisterAllocatorVerifier14CreateForBlockEPKNS1_16InstructionBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25RegisterAllocatorVerifier14CreateForBlockEPKNS1_16InstructionBlockE
	.type	_ZN2v88internal8compiler25RegisterAllocatorVerifier14CreateForBlockEPKNS1_16InstructionBlockE, @function
_ZN2v88internal8compiler25RegisterAllocatorVerifier14CreateForBlockEPKNS1_16InstructionBlockE:
.LFB10860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -88(%rbp)
	movl	100(%rsi), %eax
	movq	%rsi, -96(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	(%rdi), %rbx
	movl	%eax, -116(%rbp)
	movq	16(%rbx), %r12
	movq	24(%rbx), %rax
	subq	%r12, %rax
	cmpq	$119, %rax
	jbe	.L1436
	leaq	120(%r12), %rax
	movq	%rax, 16(%rbx)
.L1345:
	leaq	72(%r12), %rax
	leaq	16(%r12), %r13
	movq	%rbx, (%r12)
	movq	%rax, 88(%r12)
	movq	%rax, 96(%r12)
	movq	-96(%rbp), %rax
	movl	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	%r13, 32(%r12)
	movq	%r13, 40(%r12)
	movq	$0, 48(%r12)
	movq	%rbx, 56(%r12)
	movl	$0, 72(%r12)
	movq	$0, 80(%r12)
	movq	$0, 104(%r12)
	movq	%rbx, 112(%r12)
	movq	48(%rax), %rsi
	movq	40(%rax), %r14
	movq	%rsi, -104(%rbp)
	cmpq	%r14, %rsi
	je	.L1343
	movq	-88(%rbp), %rax
	subq	%r14, %rsi
	leaq	72(%rax), %rbx
	movq	80(%rax), %rax
	movq	%rbx, -80(%rbp)
	cmpq	$4, %rsi
	je	.L1437
.L1347:
	movq	%r14, -72(%rbp)
	movq	-72(%rbp), %rsi
	leaq	-64(%rbp), %r15
	movl	(%rsi), %edx
	testq	%rax, %rax
	je	.L1372
	.p2align 4,,10
	.p2align 3
.L1443:
	movq	-80(%rbp), %rcx
	jmp	.L1373
	.p2align 4,,10
	.p2align 3
.L1438:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1374
.L1373:
	cmpl	32(%rax), %edx
	jle	.L1438
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1373
.L1374:
	cmpq	-80(%rbp), %rcx
	je	.L1372
	cmpl	32(%rcx), %edx
	jl	.L1372
	movq	40(%rcx), %rbx
	testq	%rbx, %rbx
	je	.L1439
	movq	32(%rbx), %r14
	addq	$16, %rbx
	cmpq	%r14, %rbx
	jne	.L1394
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1383:
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, %rbx
	je	.L1382
.L1394:
	movq	32(%r14), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE4findERS5_
	cmpq	%rax, %r13
	jne	.L1383
	movq	-88(%rbp), %rax
	movq	-64(%rbp), %r8
	movq	(%rax), %rcx
	movq	16(%rcx), %rdx
	movq	24(%rcx), %rax
	subq	%rdx, %rax
	cmpq	$79, %rax
	jbe	.L1440
	leaq	80(%rdx), %rax
	movq	%rax, 16(%rcx)
.L1385:
	movq	-96(%rbp), %rax
	movl	$1, (%rdx)
	movq	%rcx, 24(%rdx)
	movq	-64(%rbp), %rcx
	movq	%rax, 8(%rdx)
	leaq	40(%rdx), %rax
	movq	%r8, 16(%rdx)
	movl	$0, 40(%rdx)
	movq	$0, 48(%rdx)
	movq	%rax, 56(%rdx)
	movq	%rax, 64(%rdx)
	movq	$0, 72(%rdx)
	movq	(%r12), %rdi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	$47, %rax
	jbe	.L1441
	leaq	48(%r8), %rax
	movq	%rax, 16(%rdi)
.L1387:
	movq	%rcx, 32(%r8)
	leaq	32(%r8), %rsi
	movq	%r12, %rdi
	movq	%rdx, 40(%r8)
	movq	%r8, -112(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE24_M_get_insert_unique_posERS5_
	testq	%rdx, %rdx
	je	.L1383
	testq	%rax, %rax
	movq	-112(%rbp), %r8
	jne	.L1403
	cmpq	%rdx, %r13
	jne	.L1442
.L1403:
	movl	$1, %edi
.L1389:
	movq	%r13, %rcx
	movq	%r8, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r12)
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, %rbx
	jne	.L1394
	.p2align 4,,10
	.p2align 3
.L1382:
	addq	$4, -72(%rbp)
	movq	-72(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	je	.L1343
	movq	-88(%rbp), %rax
	movq	-72(%rbp), %rsi
	movq	80(%rax), %rax
	movl	(%rsi), %edx
	testq	%rax, %rax
	jne	.L1443
	.p2align 4,,10
	.p2align 3
.L1372:
	cmpl	-116(%rbp), %edx
	jl	.L1444
	movq	-96(%rbp), %rax
	movl	108(%rax), %eax
	testl	%eax, %eax
	jns	.L1382
	leaq	.LC30(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1442:
	movq	32(%r8), %rax
	testb	$4, %al
	je	.L1390
	xorl	%ecx, %ecx
	testb	$24, %al
	jne	.L1391
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L1391:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1390:
	movq	32(%rdx), %rcx
	testb	$4, %cl
	je	.L1392
	xorl	%esi, %esi
	testb	$24, %cl
	jne	.L1393
	movq	%rcx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L1393:
	andq	$-8161, %rcx
	orq	%rsi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1392:
	xorl	%edi, %edi
	cmpq	%rax, %rcx
	seta	%dil
	jmp	.L1389
.L1437:
	movq	-96(%rbp), %rbx
	movq	80(%rbx), %rsi
	cmpq	%rsi, 72(%rbx)
	jne	.L1347
	testq	%rax, %rax
	je	.L1396
	movl	(%r14), %edx
	movq	-80(%rbp), %r15
	jmp	.L1351
.L1445:
	movq	%rax, %r15
	movq	16(%rax), %rax
.L1354:
	testq	%rax, %rax
	je	.L1352
.L1351:
	cmpl	%edx, 32(%rax)
	jge	.L1445
	movq	24(%rax), %rax
	jmp	.L1354
	.p2align 4,,10
	.p2align 3
.L1343:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1446
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1441:
	.cfi_restore_state
	movl	$48, %esi
	movq	%rdx, -128(%rbp)
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L1387
.L1440:
	movq	%rcx, %rdi
	movl	$80, %esi
	movq	%r8, -128(%rbp)
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %rcx
	movq	-128(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L1385
.L1444:
	leaq	.LC29(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1439:
	leaq	.LC31(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1436:
	movl	$120, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1345
.L1352:
	cmpq	-80(%rbp), %r15
	je	.L1350
	cmpl	32(%r15), %edx
	jl	.L1350
	movq	40(%r15), %rax
.L1355:
	testq	%rax, %rax
	je	.L1447
	movq	32(%rax), %r14
	leaq	16(%rax), %rbx
	cmpq	%r14, %rbx
	jne	.L1361
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1398:
	movl	$1, %r15d
.L1364:
	movq	(%r12), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$47, %rax
	jbe	.L1448
	leaq	48(%rsi), %rax
	movq	%rax, 16(%rdi)
.L1370:
	movq	32(%r14), %rcx
	movq	40(%r14), %rax
	movl	%r15d, %edi
	movq	%rcx, 32(%rsi)
	movq	%r13, %rcx
	movq	%rax, 40(%rsi)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r12)
.L1363:
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	%rax, %rbx
	je	.L1343
.L1361:
	leaq	32(%r14), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_
	testq	%rdx, %rdx
	je	.L1363
	testq	%rax, %rax
	jne	.L1398
	cmpq	%rdx, %r13
	je	.L1398
	movq	32(%r14), %rcx
	testb	$4, %cl
	je	.L1365
	testb	$24, %cl
	jne	.L1366
	movq	%rcx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L1366:
	andq	$-8161, %rcx
	orq	%rax, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1365:
	movq	32(%rdx), %rax
	testb	$4, %al
	je	.L1367
	xorl	%esi, %esi
	testb	$24, %al
	jne	.L1368
	movq	%rax, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L1368:
	andq	$-8161, %rax
	orq	%rsi, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1367:
	xorl	%r15d, %r15d
	cmpq	%rcx, %rax
	seta	%r15b
	jmp	.L1364
.L1396:
	movq	-80(%rbp), %r15
.L1350:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	leaq	56(%rax), %r8
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$47, %rax
	jbe	.L1449
	leaq	48(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1357:
	movl	(%r14), %eax
	leaq	32(%rbx), %rdx
	movq	%r15, %rsi
	movq	%r8, %rdi
	movq	$0, 40(%rbx)
	movl	%eax, 32(%rbx)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_16BlockAssessmentsEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_
	movq	%rax, %rcx
	testq	%rdx, %rdx
	je	.L1358
	testq	%rax, %rax
	jne	.L1397
	cmpq	%rdx, -80(%rbp)
	jne	.L1450
.L1397:
	movl	$1, %edi
.L1359:
	movq	-80(%rbp), %rcx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	movq	-88(%rbp), %rax
	movq	%rbx, %rcx
	addq	$1, 104(%rax)
.L1358:
	cmpq	$0, 48(%r12)
	movq	40(%rcx), %rax
	je	.L1355
	leaq	.LC27(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1448:
	movl	$48, %esi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L1370
.L1447:
	leaq	.LC28(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1449:
	movl	$48, %esi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	jmp	.L1357
.L1450:
	xorl	%edi, %edi
	movl	32(%rdx), %eax
	cmpl	%eax, 32(%rbx)
	setl	%dil
	jmp	.L1359
.L1446:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10860:
	.size	_ZN2v88internal8compiler25RegisterAllocatorVerifier14CreateForBlockEPKNS1_16InstructionBlockE, .-_ZN2v88internal8compiler25RegisterAllocatorVerifier14CreateForBlockEPKNS1_16InstructionBlockE
	.section	.rodata._ZNSt5dequeISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE12emplace_backIJS7_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE12emplace_backIJS7_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE12emplace_backIJS7_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE12emplace_backIJS7_EEEvDpOT_
	.type	_ZNSt5dequeISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE12emplace_backIJS7_EEEvDpOT_, @function
_ZNSt5dequeISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE12emplace_backIJS7_EEEvDpOT_:
.LFB13352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	80(%rdi), %rcx
	movq	64(%rdi), %rax
	leaq	-16(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L1452
	movq	(%rsi), %rcx
	movl	8(%rsi), %edx
	movq	%rcx, (%rax)
	movl	%edx, 8(%rax)
	addq	$16, 64(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1452:
	.cfi_restore_state
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	subq	72(%rdi), %rax
	movq	%r13, %r14
	sarq	$4, %rax
	subq	%rsi, %r14
	movq	%r14, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$5, %rdx
	addq	%rax, %rdx
	movq	48(%rdi), %rax
	subq	32(%rdi), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$134217727, %rax
	je	.L1471
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rdi
	movq	%r13, %rax
	subq	%rdi, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L1472
.L1455:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L1463
	cmpq	$31, 8(%rax)
	ja	.L1473
.L1463:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1474
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1464:
	movq	%rax, 8(%r13)
	movl	8(%r12), %edx
	movq	(%r12), %rcx
	movq	64(%rbx), %rax
	movl	%edx, 8(%rax)
	movq	%rcx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1472:
	.cfi_restore_state
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L1475
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L1476
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1460:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1461
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L1461:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L1462
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L1462:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L1458:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1475:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rdi,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L1457
	cmpq	%r13, %rsi
	je	.L1458
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1458
	.p2align 4,,10
	.p2align 3
.L1473:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L1464
	.p2align 4,,10
	.p2align 3
.L1457:
	cmpq	%r13, %rsi
	je	.L1458
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1458
	.p2align 4,,10
	.p2align 3
.L1474:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1464
.L1476:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L1460
.L1471:
	leaq	.LC32(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13352:
	.size	_ZNSt5dequeISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE12emplace_backIJS7_EEEvDpOT_, .-_ZNSt5dequeISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE12emplace_backIJS7_EEEvDpOT_
	.section	.text._ZNSt11_Deque_baseISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE17_M_initialize_mapEm:
.LFB14002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$5, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L1491
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L1479:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L1480
	movq	%r15, %rbx
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1481:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L1492
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1482:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L1480
.L1485:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L1481
	cmpq	$31, 8(%rax)
	jbe	.L1481
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L1485
.L1480:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$31, %r13d
	salq	$4, %r13
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	addq	%rax, %r13
	leaq	512(%rax), %rcx
	movq	%rax, 72(%r12)
	movq	%r13, 64(%r12)
	movq	%rcx, 80(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1492:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1491:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1479
	.cfi_endproc
.LFE14002:
	.size	_ZNSt11_Deque_baseISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE17_M_initialize_mapEm
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier25ValidatePendingAssessmentENS1_9RpoNumberENS1_18InstructionOperandEPKNS1_16BlockAssessmentsEPNS1_17PendingAssessmentEi.str1.8,"aMS",@progbits,1
	.align 8
.LC33:
	.string	"origin->PredecessorCount() > 1 || origin->phis().size() > 0"
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier25ValidatePendingAssessmentENS1_9RpoNumberENS1_18InstructionOperandEPKNS1_16BlockAssessmentsEPNS1_17PendingAssessmentEi.str1.1,"aMS",@progbits,1
.LC34:
	.string	"origin->IsLoopHeader()"
.LC35:
	.string	"it->second == vreg"
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier25ValidatePendingAssessmentENS1_9RpoNumberENS1_18InstructionOperandEPKNS1_16BlockAssessmentsEPNS1_17PendingAssessmentEi.str1.8
	.align 8
.LC36:
	.string	"FinalAssessment::cast(contribution)->virtual_register() == expected"
	.align 8
.LC37:
	.string	"../deps/v8/src/compiler/backend/register-allocator-verifier.cc:385"
	.align 8
.LC38:
	.string	"found_contribution != pred_assessments->map().end()"
	.section	.text._ZN2v88internal8compiler25RegisterAllocatorVerifier25ValidatePendingAssessmentENS1_9RpoNumberENS1_18InstructionOperandEPKNS1_16BlockAssessmentsEPNS1_17PendingAssessmentEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25RegisterAllocatorVerifier25ValidatePendingAssessmentENS1_9RpoNumberENS1_18InstructionOperandEPKNS1_16BlockAssessmentsEPNS1_17PendingAssessmentEi
	.type	_ZN2v88internal8compiler25RegisterAllocatorVerifier25ValidatePendingAssessmentENS1_9RpoNumberENS1_18InstructionOperandEPKNS1_16BlockAssessmentsEPNS1_17PendingAssessmentEi, @function
_ZN2v88internal8compiler25RegisterAllocatorVerifier25ValidatePendingAssessmentENS1_9RpoNumberENS1_18InstructionOperandEPKNS1_16BlockAssessmentsEPNS1_17PendingAssessmentEi:
.LFB10865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -448(%rbp)
	movq	48(%r8), %rax
	movl	%esi, -436(%rbp)
	movq	%r8, -480(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.L1494
	leaq	40(%r8), %rcx
	movq	%rcx, %rdx
	jmp	.L1495
	.p2align 4,,10
	.p2align 3
.L1768:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1496
.L1495:
	cmpl	%r9d, 32(%rax)
	jge	.L1768
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1495
.L1496:
	cmpq	%rcx, %rdx
	je	.L1494
	cmpl	%r9d, 32(%rdx)
	jle	.L1493
.L1494:
	movq	-448(%rbp), %rax
	leaq	.LC37(%rip), %rdx
	movl	%r9d, -456(%rbp)
	movq	(%rax), %rax
	movq	32(%rax), %rsi
	leaq	-416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%rax, -528(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	leaq	-256(%rbp), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, %rdi
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	%rbx, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZNSt11_Deque_baseISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE17_M_initialize_mapEm
	movl	-456(%rbp), %r9d
	pxor	%xmm0, %xmm0
	cmpq	$0, -240(%rbp)
	je	.L1769
	leaq	-160(%rbp), %rax
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movdqa	-256(%rbp), %xmm5
	movq	%rax, %rdi
	movl	%r9d, -456(%rbp)
	movq	%rax, -520(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE17_M_initialize_mapEm
	movdqa	-96(%rbp), %xmm5
	movq	-216(%rbp), %r8
	movq	-224(%rbp), %xmm3
	movq	-200(%rbp), %rdi
	movq	-192(%rbp), %xmm1
	movq	-184(%rbp), %rsi
	movaps	%xmm5, -192(%rbp)
	movq	%r8, %xmm5
	punpcklqdq	%xmm5, %xmm3
	movdqa	-128(%rbp), %xmm6
	movq	-208(%rbp), %xmm2
	movaps	%xmm3, -320(%rbp)
	movq	%rdi, %xmm3
	movq	-168(%rbp), %r11
	movdqa	-112(%rbp), %xmm7
	punpcklqdq	%xmm3, %xmm2
	movq	-248(%rbp), %rax
	movq	-176(%rbp), %xmm0
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm2, -304(%rbp)
	movq	%rsi, %xmm2
	movq	-144(%rbp), %rdx
	movq	-256(%rbp), %xmm4
	punpcklqdq	%xmm2, %xmm1
	movq	-240(%rbp), %r10
	movq	-232(%rbp), %rbx
	movaps	%xmm7, -208(%rbp)
	movdqa	-80(%rbp), %xmm6
	movq	-136(%rbp), %rcx
	movq	%rax, %xmm7
	testq	%rdx, %rdx
	movaps	%xmm1, -288(%rbp)
	movq	%r11, %xmm1
	punpcklqdq	%xmm7, %xmm4
	movl	-456(%rbp), %r9d
	punpcklqdq	%xmm1, %xmm0
	movq	%rdx, -240(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%r10, -336(%rbp)
	movq	%rbx, -328(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm4, -352(%rbp)
	movaps	%xmm0, -272(%rbp)
	je	.L1500
	movq	-168(%rbp), %rbx
	movq	-200(%rbp), %rsi
	leaq	8(%rbx), %rdi
	cmpq	%rsi, %rdi
	jbe	.L1501
	.p2align 4,,10
	.p2align 3
.L1504:
	testq	%rax, %rax
	je	.L1502
	cmpq	$32, 8(%rax)
	ja	.L1503
.L1502:
	movq	(%rsi), %rax
	movq	$32, 8(%rax)
	movq	-248(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -248(%rbp)
.L1503:
	addq	$8, %rsi
	cmpq	%rsi, %rdi
	ja	.L1504
	movq	-232(%rbp), %rcx
	movq	-240(%rbp), %rdx
.L1501:
	leaq	0(,%rcx,8), %rax
	cmpq	$15, %rax
	jbe	.L1500
	movq	%rcx, 8(%rdx)
	movq	$0, (%rdx)
.L1500:
	movq	-528(%rbp), %rax
	movq	-504(%rbp), %rsi
	movl	%r9d, -248(%rbp)
	movl	%r9d, -456(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, -496(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rax, -120(%rbp)
	movq	-480(%rbp), %rax
	movl	$0, -144(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-352(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -544(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -112(%rbp)
	call	_ZNSt5dequeISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE12emplace_backIJS7_EEEvDpOT_
	movq	-520(%rbp), %rdi
	leaq	-436(%rbp), %rsi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	movq	-320(%rbp), %rsi
	cmpq	%rsi, -288(%rbp)
	movl	-456(%rbp), %r9d
	je	.L1617
	movq	-448(%rbp), %rax
	movl	%r9d, -472(%rbp)
	leaq	72(%rax), %r11
	subq	$-128, %rax
	movq	%rax, -488(%rbp)
.L1506:
	movq	-304(%rbp), %rax
	movq	(%rsi), %rdx
	movl	8(%rsi), %r15d
	subq	$16, %rax
	movq	16(%rdx), %rbx
	cmpq	%rax, %rsi
	je	.L1509
	addq	$16, %rsi
	movq	%rsi, -320(%rbp)
.L1510:
	movq	8(%rdx), %rax
	movq	48(%rax), %r10
	movq	%rax, %rdi
	movq	40(%rax), %rcx
	movq	%rax, -512(%rbp)
	movq	80(%rdi), %rdx
	movq	72(%rax), %rax
	movq	%r10, %rdi
	movq	%r10, -456(%rbp)
	subq	%rcx, %rdi
	cmpq	$7, %rdi
	jbe	.L1770
	cmpq	%rdx, %rax
	jne	.L1516
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1771:
	addq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1639
.L1516:
	movq	(%rax), %r12
	cmpl	(%r12), %r15d
	jne	.L1771
.L1515:
	cmpq	%rcx, -456(%rbp)
	je	.L1517
	movl	%ebx, %eax
	movl	%r15d, -440(%rbp)
	xorl	%r13d, %r13d
	andl	$4, %eax
	movl	%eax, -460(%rbp)
	movq	%rbx, %rax
	shrq	$3, %rax
	andl	$3, %eax
	movl	%eax, -468(%rbp)
	movq	%rbx, %rax
	shrq	$5, %rax
	movb	%al, -461(%rbp)
	movq	%rbx, %rax
	andq	$-8168, %rax
	movq	%rax, %r14
	movq	%rax, -536(%rbp)
	movq	%rcx, %rax
	movq	%r12, %rcx
	orq	$4, %r14
	movq	%rax, %r12
.L1616:
	movl	(%r12,%r13), %eax
	movl	-440(%rbp), %r15d
	movl	%eax, -420(%rbp)
	testq	%rcx, %rcx
	je	.L1518
	movq	24(%rcx), %rax
	movl	(%rax,%r13), %r15d
.L1518:
	movq	-448(%rbp), %rax
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L1519
	movl	-420(%rbp), %edx
	movq	%r11, %rsi
	jmp	.L1520
	.p2align 4,,10
	.p2align 3
.L1772:
	movq	%rax, %rsi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1521
.L1520:
	cmpl	%edx, 32(%rax)
	jge	.L1772
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1520
.L1521:
	cmpq	%r11, %rsi
	je	.L1519
	cmpl	32(%rsi), %edx
	jge	.L1524
.L1519:
	movq	-512(%rbp), %rax
	movl	108(%rax), %r10d
	testl	%r10d, %r10d
	js	.L1773
	movq	-448(%rbp), %rax
	movq	136(%rax), %rax
	testq	%rax, %rax
	je	.L1526
	movl	-420(%rbp), %edx
	movq	-488(%rbp), %rsi
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1774:
	movq	%rax, %rsi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1528
.L1527:
	cmpl	%edx, 32(%rax)
	jge	.L1774
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1527
.L1528:
	cmpq	%rsi, -488(%rbp)
	je	.L1526
	cmpl	32(%rsi), %edx
	jl	.L1526
	movq	40(%rsi), %r8
	leaq	16(%r8), %r10
.L1534:
	movq	24(%r8), %rax
	testq	%rax, %rax
	je	.L1535
	movl	-460(%rbp), %r9d
	movq	%r10, %rsi
	testl	%r9d, %r9d
	je	.L1536
	cmpb	$11, -461(%rbp)
	jbe	.L1537
	movl	-468(%rbp), %edi
	testl	%edi, %edi
	jne	.L1545
	movq	-536(%rbp), %r9
	orq	$420, %r9
	jmp	.L1538
	.p2align 4,,10
	.p2align 3
.L1776:
	movq	%rax, %rsi
	movq	16(%rax), %rax
.L1568:
	testq	%rax, %rax
	je	.L1775
.L1538:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L1565
	xorl	%edi, %edi
	testb	$24, %dl
	jne	.L1566
	movq	%rdx, %rdi
	shrq	$5, %rdi
	cmpb	$12, %dil
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
.L1566:
	andq	$-8161, %rdx
	orq	%rdi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1565:
	cmpq	%rdx, %r9
	jbe	.L1776
	movq	24(%rax), %rax
	jmp	.L1568
	.p2align 4,,10
	.p2align 3
.L1613:
	cmpq	-496(%rbp), %rdi
	je	.L1611
	cmpl	32(%rdi), %edx
	jge	.L1572
.L1611:
	movq	%rsi, -256(%rbp)
	movq	-544(%rbp), %rdi
	movq	-504(%rbp), %rsi
	movq	%r11, -560(%rbp)
	movq	%rcx, -552(%rbp)
	movl	%r15d, -248(%rbp)
	call	_ZNSt5dequeISt4pairIPKN2v88internal8compiler17PendingAssessmentEiENS2_22RecyclingZoneAllocatorIS7_EEE12emplace_backIJS7_EEEvDpOT_
	movq	-520(%rbp), %rdi
	leaq	-420(%rbp), %rsi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	movq	-552(%rbp), %rcx
	movq	-560(%rbp), %r11
	.p2align 4,,10
	.p2align 3
.L1572:
	addq	$4, %r13
	leaq	(%r12,%r13), %rax
	cmpq	%rax, -456(%rbp)
	jne	.L1616
	movq	-320(%rbp), %rsi
.L1517:
	cmpq	%rsi, -288(%rbp)
	jne	.L1506
	movl	-472(%rbp), %r9d
.L1617:
	movq	-480(%rbp), %rdi
	movq	-504(%rbp), %rsi
	movl	%r9d, -256(%rbp)
	addq	$24, %rdi
	call	_ZNSt8_Rb_treeIiiSt9_IdentityIiESt4lessIiEN2v88internal13ZoneAllocatorIiEEE16_M_insert_uniqueIRKiEESt4pairISt17_Rb_tree_iteratorIiEbEOT_
	movq	-136(%rbp), %r12
	movq	-520(%rbp), %r13
	testq	%r12, %r12
	je	.L1507
.L1508:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1618
.L1619:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberES3_St9_IdentityIS3_ESt4lessIS3_ENS1_13ZoneAllocatorIS3_EEE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1619
.L1618:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1508
.L1507:
	cmpq	$0, -336(%rbp)
	je	.L1620
	movq	-264(%rbp), %rax
	movq	-344(%rbp), %rdx
	leaq	8(%rax), %rcx
	movq	-296(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L1626
.L1627:
	testq	%rdx, %rdx
	je	.L1624
	cmpq	$32, 8(%rdx)
	ja	.L1625
.L1624:
	movq	(%rax), %rdx
	movq	$32, 8(%rdx)
	movq	-344(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	%rdx, -344(%rbp)
.L1625:
	addq	$8, %rax
	cmpq	%rax, %rcx
	ja	.L1627
.L1626:
	movq	-328(%rbp), %rdx
	leaq	0(,%rdx,8), %rax
	cmpq	$15, %rax
	jbe	.L1620
	movq	-336(%rbp), %rax
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L1620:
	movq	-528(%rbp), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1493:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1777
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1524:
	.cfi_restore_state
	movq	40(%rsi), %rax
	leaq	16(%rax), %r8
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1573
	movl	-460(%rbp), %r10d
	movq	%r8, %rdi
	testl	%r10d, %r10d
	je	.L1574
	cmpb	$11, -461(%rbp)
	jbe	.L1575
	movl	-468(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L1583
	movq	-536(%rbp), %r10
	orq	$420, %r10
	jmp	.L1576
	.p2align 4,,10
	.p2align 3
.L1779:
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1778
.L1576:
	movq	32(%rax), %rsi
	testb	$4, %sil
	je	.L1603
	xorl	%r9d, %r9d
	testb	$24, %sil
	jne	.L1604
	movq	%rsi, %r9
	shrq	$5, %r9
	cmpb	$12, %r9b
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
.L1604:
	andq	$-8161, %rsi
	orq	%r9, %rsi
	andq	$-8, %rsi
	orq	$4, %rsi
.L1603:
	cmpq	%rsi, %r10
	jbe	.L1779
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1576
.L1778:
	cmpq	%r8, %rdi
	je	.L1573
	movl	$416, %eax
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1781:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1780
.L1574:
	movq	32(%rax), %rsi
	testb	$4, %sil
	je	.L1596
	testb	$24, %sil
	je	.L1597
	xorl	%r9d, %r9d
.L1598:
	andq	$-8161, %rsi
	orq	%r9, %rsi
	andq	$-8, %rsi
	orq	$4, %rsi
.L1596:
	cmpq	%rsi, %rbx
	ja	.L1781
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1574
.L1780:
	cmpq	%r8, %rdi
	je	.L1573
	movq	%rbx, %rsi
.L1637:
	movq	32(%rdi), %rax
	testb	$4, %al
	je	.L1607
	xorl	%r8d, %r8d
	testb	$24, %al
	jne	.L1608
	movq	%rax, %r8
	shrq	$5, %r8
	cmpb	$12, %r8b
	sbbq	%r8, %r8
	notq	%r8
	andl	$416, %r8d
.L1608:
	andq	$-8161, %rax
	orq	%r8, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1607:
	cmpq	%rsi, %rax
	jbe	.L1782
.L1573:
	leaq	.LC38(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1783:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1582
.L1583:
	movq	32(%rax), %rsi
	testb	$4, %sil
	je	.L1577
	testb	$24, %sil
	je	.L1578
	xorl	%r9d, %r9d
.L1579:
	andq	$-8161, %rsi
	orq	%r9, %rsi
	andq	$-8, %rsi
	orq	$4, %rsi
.L1577:
	cmpq	%rsi, %r14
	ja	.L1783
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1583
.L1582:
	cmpq	%rdi, %r8
	je	.L1573
.L1767:
	xorl	%eax, %eax
.L1636:
	movq	%rbx, %rsi
	andq	$-8168, %rsi
	orq	%rax, %rsi
	orq	$4, %rsi
	jmp	.L1637
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	%rsi, %r9
	shrq	$5, %r9
	cmpb	$12, %r9b
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
	jmp	.L1598
	.p2align 4,,10
	.p2align 3
.L1782:
	movq	40(%rdi), %rsi
	movl	(%rsi), %eax
	testl	%eax, %eax
	je	.L1632
	cmpl	$1, %eax
	jne	.L1572
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L1611
	movq	-496(%rbp), %rdi
	jmp	.L1612
	.p2align 4,,10
	.p2align 3
.L1784:
	movq	%rax, %rdi
	movq	16(%rax), %rax
.L1615:
	testq	%rax, %rax
	je	.L1613
.L1612:
	cmpl	32(%rax), %edx
	jle	.L1784
	movq	24(%rax), %rax
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1632:
	cmpl	4(%rsi), %r15d
	je	.L1572
	leaq	.LC36(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1786:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1785
.L1536:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L1558
	testb	$24, %dl
	je	.L1559
	xorl	%edi, %edi
.L1560:
	andq	$-8161, %rdx
	orq	%rdi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1558:
	cmpq	%rdx, %rbx
	ja	.L1786
	movq	%rax, %rsi
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1536
.L1785:
	movq	%rbx, %rdx
	cmpq	%r10, %rsi
	je	.L1535
	movq	32(%rsi), %rax
	testb	$4, %al
	je	.L1569
.L1795:
	xorl	%edi, %edi
	testb	$24, %al
	jne	.L1570
	movq	%rax, %rdi
	shrq	$5, %rdi
	cmpb	$12, %dil
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
.L1570:
	andq	$-8161, %rax
	orq	%rdi, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1569:
	cmpq	%rdx, %rax
	jbe	.L1787
.L1535:
	movq	-504(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r11, -560(%rbp)
	movq	%rcx, -552(%rbp)
	movq	%rbx, -256(%rbp)
	movl	%r15d, -248(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_iESt10_Select1stIS6_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS4_IS3_iEEEES4_ISt17_Rb_tree_iteratorIS6_EbEDpOT_
	movq	-552(%rbp), %rcx
	movq	-560(%rbp), %r11
	jmp	.L1572
	.p2align 4,,10
	.p2align 3
.L1559:
	movq	%rdx, %rdi
	shrq	$5, %rdi
	cmpb	$12, %dil
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
	jmp	.L1560
	.p2align 4,,10
	.p2align 3
.L1578:
	movq	%rsi, %r9
	shrq	$5, %r9
	cmpb	$12, %r9b
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1575:
	movl	-468(%rbp), %esi
	testl	%esi, %esi
	je	.L1584
	jmp	.L1589
	.p2align 4,,10
	.p2align 3
.L1788:
	movq	%rax, %rdi
	movq	16(%rax), %rax
.L1588:
	testq	%rax, %rax
	je	.L1582
.L1589:
	movq	32(%rax), %rsi
	testb	$4, %sil
	je	.L1585
	xorl	%r9d, %r9d
	testb	$24, %sil
	jne	.L1586
	movq	%rsi, %r9
	shrq	$5, %r9
	cmpb	$12, %r9b
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
.L1586:
	andq	$-8161, %rsi
	orq	%r9, %rsi
	andq	$-8, %rsi
	orq	$4, %rsi
.L1585:
	cmpq	%r14, %rsi
	jnb	.L1788
	movq	24(%rax), %rax
	jmp	.L1588
	.p2align 4,,10
	.p2align 3
.L1790:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1789
.L1584:
	movq	32(%rax), %rsi
	testb	$4, %sil
	je	.L1590
	testb	$24, %sil
	je	.L1591
	xorl	%r9d, %r9d
.L1592:
	andq	$-8161, %rsi
	orq	%r9, %rsi
	andq	$-8, %rsi
	orq	$4, %rsi
.L1590:
	cmpq	%rsi, %r14
	ja	.L1790
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1584
.L1789:
	cmpq	%r8, %rdi
	jne	.L1767
	jmp	.L1573
	.p2align 4,,10
	.p2align 3
.L1591:
	movq	%rsi, %r9
	shrq	$5, %r9
	cmpb	$12, %r9b
	sbbq	%r9, %r9
	notq	%r9
	andl	$416, %r9d
	jmp	.L1592
	.p2align 4,,10
	.p2align 3
.L1639:
	xorl	%r12d, %r12d
	jmp	.L1515
.L1787:
	cmpl	40(%rsi), %r15d
	je	.L1572
	leaq	.LC35(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1769:
	movdqa	-256(%rbp), %xmm6
	movdqa	-224(%rbp), %xmm7
	movq	$0, -336(%rbp)
	movq	-232(%rbp), %rax
	movdqa	-176(%rbp), %xmm1
	movaps	%xmm6, -352(%rbp)
	movdqa	-208(%rbp), %xmm6
	movaps	%xmm7, -320(%rbp)
	movdqa	-192(%rbp), %xmm7
	movq	%rax, -328(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, -520(%rbp)
	movaps	%xmm6, -304(%rbp)
	movaps	%xmm7, -288(%rbp)
	movaps	%xmm1, -272(%rbp)
	jmp	.L1500
	.p2align 4,,10
	.p2align 3
.L1526:
	movq	-448(%rbp), %rax
	movq	(%rax), %rdx
	movq	16(%rdx), %r8
	movq	24(%rdx), %rax
	subq	%r8, %rax
	cmpq	$55, %rax
	jbe	.L1791
	leaq	56(%r8), %rax
	movq	%rax, 16(%rdx)
.L1533:
	movq	%rdx, (%r8)
	movl	-420(%rbp), %eax
	leaq	16(%r8), %r10
	movq	%r10, 32(%r8)
	movq	-504(%rbp), %rsi
	movq	%r10, 40(%r8)
	movl	$0, 16(%r8)
	movq	$0, 24(%r8)
	movq	$0, 48(%r8)
	movl	%eax, -256(%rbp)
	movq	-448(%rbp), %rax
	movq	%r11, -576(%rbp)
	leaq	112(%rax), %rdi
	movq	%rcx, -568(%rbp)
	movq	%r10, -560(%rbp)
	movq	%r8, -248(%rbp)
	movq	%r8, -552(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_25RegisterAllocatorVerifier18DelayedAssessmentsEESt10_Select1stIS9_ESt4lessIS3_ENS1_13ZoneAllocatorIS9_EEE17_M_emplace_uniqueIJS4_IS3_S8_EEEES4_ISt17_Rb_tree_iteratorIS9_EbEDpOT_
	movq	-552(%rbp), %r8
	movq	-560(%rbp), %r10
	movq	-568(%rbp), %rcx
	movq	-576(%rbp), %r11
	jmp	.L1534
	.p2align 4,,10
	.p2align 3
.L1792:
	movq	24(%rax), %rax
.L1543:
	testq	%rax, %rax
	je	.L1557
.L1545:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L1539
	testb	$24, %dl
	je	.L1540
	xorl	%edi, %edi
.L1541:
	andq	$-8161, %rdx
	orq	%rdi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1539:
	cmpq	%rdx, %r14
	ja	.L1792
	movq	%rax, %rsi
	movq	16(%rax), %rax
	jmp	.L1543
.L1537:
	movl	-468(%rbp), %edx
	testl	%edx, %edx
	je	.L1546
	jmp	.L1551
	.p2align 4,,10
	.p2align 3
.L1793:
	movq	%rax, %rsi
	movq	16(%rax), %rax
.L1550:
	testq	%rax, %rax
	je	.L1557
.L1551:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L1547
	xorl	%edi, %edi
	testb	$24, %dl
	jne	.L1548
	movq	%rdx, %rdi
	shrq	$5, %rdi
	cmpb	$12, %dil
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
.L1548:
	andq	$-8161, %rdx
	orq	%rdi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1547:
	cmpq	%rdx, %r14
	jbe	.L1793
	movq	24(%rax), %rax
	jmp	.L1550
	.p2align 4,,10
	.p2align 3
.L1794:
	movq	24(%rax), %rax
.L1556:
	testq	%rax, %rax
	je	.L1557
.L1546:
	movq	32(%rax), %rdx
	testb	$4, %dl
	je	.L1552
	testb	$24, %dl
	je	.L1553
	xorl	%edi, %edi
.L1554:
	andq	$-8161, %rdx
	orq	%rdi, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1552:
	cmpq	%rdx, %r14
	ja	.L1794
	movq	%rax, %rsi
	movq	16(%rax), %rax
	jmp	.L1556
.L1557:
	xorl	%eax, %eax
	cmpq	%r10, %rsi
	je	.L1535
.L1634:
	movq	%rbx, %rdx
	andq	$-8168, %rdx
	orq	%rax, %rdx
	movq	32(%rsi), %rax
	orq	$4, %rdx
	testb	$4, %al
	je	.L1569
	jmp	.L1795
.L1540:
	movq	%rdx, %rdi
	shrq	$5, %rdi
	cmpb	$12, %dil
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
	jmp	.L1541
.L1553:
	movq	%rdx, %rdi
	shrq	$5, %rdi
	cmpb	$12, %dil
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
	jmp	.L1554
.L1509:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L1511
	cmpq	$32, 8(%rax)
	ja	.L1512
.L1511:
	movq	-312(%rbp), %rax
	movq	$32, 8(%rax)
	movq	-344(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	%rax, -344(%rbp)
.L1512:
	movq	-296(%rbp), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, -296(%rbp)
	movq	8(%rax), %rsi
	leaq	512(%rsi), %rax
	movq	%rsi, -312(%rbp)
	movq	%rax, -304(%rbp)
	movq	%rsi, -320(%rbp)
	jmp	.L1510
.L1773:
	leaq	.LC34(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1775:
	movl	$416, %eax
	cmpq	%r10, %rsi
	jne	.L1634
	jmp	.L1535
.L1770:
	cmpq	%rdx, %rax
	jne	.L1516
	leaq	.LC33(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1791:
	movq	%rdx, %rdi
	movl	$56, %esi
	movq	%r11, -568(%rbp)
	movq	%rcx, -560(%rbp)
	movq	%rdx, -552(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-552(%rbp), %rdx
	movq	-560(%rbp), %rcx
	movq	-568(%rbp), %r11
	movq	%rax, %r8
	jmp	.L1533
.L1777:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10865:
	.size	_ZN2v88internal8compiler25RegisterAllocatorVerifier25ValidatePendingAssessmentENS1_9RpoNumberENS1_18InstructionOperandEPKNS1_16BlockAssessmentsEPNS1_17PendingAssessmentEi, .-_ZN2v88internal8compiler25RegisterAllocatorVerifier25ValidatePendingAssessmentENS1_9RpoNumberENS1_18InstructionOperandEPKNS1_16BlockAssessmentsEPNS1_17PendingAssessmentEi
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier11ValidateUseENS1_9RpoNumberEPNS1_16BlockAssessmentsENS1_18InstructionOperandEi.str1.8,"aMS",@progbits,1
	.align 8
.LC39:
	.string	"iterator != current_assessments->map().end()"
	.align 8
.LC40:
	.string	"FinalAssessment::cast(assessment)->virtual_register() == virtual_register"
	.section	.text._ZN2v88internal8compiler25RegisterAllocatorVerifier11ValidateUseENS1_9RpoNumberEPNS1_16BlockAssessmentsENS1_18InstructionOperandEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25RegisterAllocatorVerifier11ValidateUseENS1_9RpoNumberEPNS1_16BlockAssessmentsENS1_18InstructionOperandEi
	.type	_ZN2v88internal8compiler25RegisterAllocatorVerifier11ValidateUseENS1_9RpoNumberEPNS1_16BlockAssessmentsENS1_18InstructionOperandEi, @function
_ZN2v88internal8compiler25RegisterAllocatorVerifier11ValidateUseENS1_9RpoNumberEPNS1_16BlockAssessmentsENS1_18InstructionOperandEi:
.LFB10933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	leaq	-40(%rbp), %rsi
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%r8d, %ebx
	subq	$16, %rsp
	movq	%rcx, -40(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE4findERS5_
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L1802
	movq	40(%rax), %r8
	movl	(%r8), %eax
	testl	%eax, %eax
	je	.L1798
	cmpl	$1, %eax
	je	.L1799
.L1796:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1799:
	.cfi_restore_state
	movq	-40(%rbp), %rdx
	movl	%ebx, %r9d
	movq	%r12, %rcx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler25RegisterAllocatorVerifier25ValidatePendingAssessmentENS1_9RpoNumberENS1_18InstructionOperandEPKNS1_16BlockAssessmentsEPNS1_17PendingAssessmentEi
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1798:
	.cfi_restore_state
	cmpl	4(%r8), %ebx
	je	.L1796
	leaq	.LC40(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1802:
	leaq	.LC39(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10933:
	.size	_ZN2v88internal8compiler25RegisterAllocatorVerifier11ValidateUseENS1_9RpoNumberEPNS1_16BlockAssessmentsENS1_18InstructionOperandEi, .-_ZN2v88internal8compiler25RegisterAllocatorVerifier11ValidateUseENS1_9RpoNumberEPNS1_16BlockAssessmentsENS1_18InstructionOperandEi
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier14VerifyGapMovesEv.str1.1,"aMS",@progbits,1
.LC41:
	.string	"assessments_.empty()"
	.section	.rodata._ZN2v88internal8compiler25RegisterAllocatorVerifier14VerifyGapMovesEv.str1.8,"aMS",@progbits,1
	.align 8
.LC42:
	.string	"outstanding_assessments_.empty()"
	.align 8
.LC43:
	.string	"found_op != block_assessments->map().end()"
	.align 8
.LC44:
	.string	"FinalAssessment::cast(found_op->second)->virtual_register() == vreg"
	.section	.text._ZN2v88internal8compiler25RegisterAllocatorVerifier14VerifyGapMovesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler25RegisterAllocatorVerifier14VerifyGapMovesEv
	.type	_ZN2v88internal8compiler25RegisterAllocatorVerifier14VerifyGapMovesEv, @function
_ZN2v88internal8compiler25RegisterAllocatorVerifier14VerifyGapMovesEv:
.LFB10934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	cmpq	$0, 104(%rdi)
	jne	.L2121
	cmpq	$0, 160(%rdi)
	jne	.L2122
	movq	16(%rdi), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdx
	movq	16(%rax), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	movq	%rax, -176(%rbp)
	je	.L1803
	leaq	128(%rdi), %rax
	movq	$0, -152(%rbp)
	movq	%rax, -168(%rbp)
	leaq	72(%rdi), %rax
	movq	%rax, -160(%rbp)
.L1953:
	movq	-152(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	(%rdx,%rax,8), %r14
	movq	%r14, %rsi
	movq	%r14, -120(%rbp)
	call	_ZN2v88internal8compiler25RegisterAllocatorVerifier14CreateForBlockEPKNS1_16InstructionBlockE
	movl	112(%r14), %esi
	movq	%rax, %rbx
	movl	%esi, -132(%rbp)
	cmpl	116(%r14), %esi
	jge	.L1833
	movslq	-132(%rbp), %rax
	movq	%rbx, %r15
	leaq	(%rax,%rax,2), %rax
	salq	$3, %rax
	movq	%rax, -144(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L1832:
	movq	-104(%rbp), %rax
	movq	-144(%rbp), %rbx
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	addq	32(%rax), %rbx
	movq	(%rbx), %r12
	movq	8(%r12), %rsi
	call	_ZN2v88internal8compiler16BlockAssessments20PerformParallelMovesEPKNS1_12ParallelMoveE
	movq	16(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler16BlockAssessments20PerformParallelMovesEPKNS1_12ParallelMoveE
	movq	16(%rbx), %rax
	movl	4(%r12), %edx
	movq	%rax, -128(%rbp)
	movq	%rax, %rbx
	movl	%edx, %eax
	testl	$16776960, %edx
	je	.L1816
	.p2align 4,,10
	.p2align 3
.L1817:
	movl	(%rbx), %eax
	cmpl	$1, %eax
	je	.L2001
	cmpl	$11, %eax
	je	.L2001
	movzbl	%dl, %edx
	movl	12(%rbx), %r8d
	movq	-104(%rbp), %rdi
	addq	$16, %rbx
	leaq	4(%r14,%rdx), %rax
	movq	%r15, %rdx
	addq	$1, %r14
	movq	8(%r12,%rax,8), %rcx
	movq	-120(%rbp), %rax
	movl	100(%rax), %esi
	call	_ZN2v88internal8compiler25RegisterAllocatorVerifier11ValidateUseENS1_9RpoNumberEPNS1_16BlockAssessmentsENS1_18InstructionOperandEi
	movl	4(%r12), %edx
	movl	%edx, %ecx
	movl	%edx, %eax
	shrl	$8, %ecx
	movzwl	%cx, %ecx
	cmpq	%rcx, %r14
	jb	.L1817
.L1816:
	leaq	16(%r15), %rbx
	xorl	%r13d, %r13d
	andl	$1056964608, %edx
	movq	%rbx, %rsi
	movq	%r14, %rbx
	je	.L1812
	movq	%r14, -96(%rbp)
	movq	%r15, %rbx
	movq	%rsi, %r14
	.p2align 4,,10
	.p2align 3
.L1827:
	movl	%eax, %edx
	movzbl	%al, %eax
	movq	-88(%rbp), %rsi
	movq	%rbx, %rdi
	shrl	$8, %edx
	movzwl	%dx, %edx
	addq	%r13, %rdx
	leaq	4(%rax,%rdx), %rax
	movq	8(%r12,%rax,8), %rax
	movq	%rax, -64(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE11equal_rangeERS5_
	movq	%rax, %r15
	cmpq	32(%rbx), %rax
	je	.L2123
.L1818:
	cmpq	%rax, %rdx
	je	.L1823
	movq	%r12, -80(%rbp)
	movq	%rbx, %r12
	movq	%rdx, %rbx
	.p2align 4,,10
	.p2align 3
.L1825:
	movq	%r15, %rdi
	movq	%r15, -72(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-72(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, %r15
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	subq	$1, 48(%r12)
	cmpq	%r15, %rbx
	jne	.L1825
	movq	%r12, %rbx
	movq	-80(%rbp), %r12
.L1823:
	movq	-96(%rbp), %rax
	addq	$1, %r13
	leaq	0(%r13,%rax), %rsi
	movl	4(%r12), %eax
	movl	%eax, %edx
	shrl	$24, %edx
	andl	$63, %edx
	cmpq	%r13, %rdx
	ja	.L1827
	movq	%rbx, %r15
	movq	%rsi, %rbx
.L1812:
	testl	$1073741824, %eax
	jne	.L2124
.L1828:
	testb	%al, %al
	je	.L1933
	movq	%r12, -72(%rbp)
	salq	$4, %rbx
	leaq	16(%r15), %r14
	addq	-128(%rbp), %rbx
	xorl	%r13d, %r13d
	movq	%r15, %r12
	.p2align 4,,10
	.p2align 3
.L1932:
	movl	12(%rbx), %eax
	movl	%eax, -80(%rbp)
	movq	-72(%rbp), %rax
	movq	40(%rax,%r13,8), %rdx
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L1834
	movq	%rdx, %rcx
	movq	%rdx, %rsi
	movq	%r14, %rdi
	shrq	$3, %rcx
	shrq	$5, %rsi
	andl	$3, %ecx
	testb	$4, %dl
	je	.L1835
	cmpb	$11, %sil
	movq	%rdx, %rsi
	jbe	.L1836
	andq	$-8168, %rsi
	testl	%ecx, %ecx
	je	.L1960
	orq	$4, %rsi
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L2125:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1843
.L1844:
	movq	32(%rax), %rcx
	testb	$4, %cl
	je	.L1838
	testb	$24, %cl
	je	.L1839
	xorl	%r8d, %r8d
.L1840:
	andq	$-8161, %rcx
	orq	%r8, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1838:
	cmpq	%rcx, %rsi
	ja	.L2125
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1844
	.p2align 4,,10
	.p2align 3
.L1843:
	xorl	%eax, %eax
	cmpq	%r14, %rdi
	je	.L1834
.L1955:
	movq	%rdx, %rcx
	andq	$-8168, %rcx
	orq	%rax, %rcx
	orq	$4, %rcx
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L2127:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L2126
.L1835:
	movq	32(%rax), %rcx
	testb	$4, %cl
	je	.L1857
	testb	$24, %cl
	je	.L1858
	xorl	%esi, %esi
.L1859:
	andq	$-8161, %rcx
	orq	%rsi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1857:
	cmpq	%rcx, %rdx
	ja	.L2127
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1835
.L2126:
	movq	%rdx, %rcx
	cmpq	%r14, %rdi
	je	.L1834
.L1956:
	movq	32(%rdi), %rax
	testb	$4, %al
	je	.L1868
	xorl	%esi, %esi
	testb	$24, %al
	jne	.L1869
	movq	%rax, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L1869:
	andq	$-8161, %rax
	orq	%rsi, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1868:
	cmpq	%rcx, %rax
	ja	.L1834
	movq	%r14, %rsi
	movq	%rdx, -96(%rbp)
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	subq	$1, 48(%r12)
	movq	-96(%rbp), %rdx
.L1834:
	movq	112(%r12), %rdi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$7, %rax
	jbe	.L2128
	leaq	8(%r15), %rax
	movq	%rax, 16(%rdi)
.L1872:
	movl	-80(%rbp), %eax
	movl	$0, (%r15)
	movl	%eax, 4(%r15)
	movq	(%r12), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$47, %rax
	jbe	.L2129
	leaq	48(%rsi), %rax
	movq	%rax, 16(%rdi)
.L1874:
	movq	%rdx, 32(%rsi)
	movq	%r15, 40(%rsi)
	movq	24(%r12), %r15
	testq	%r15, %r15
	je	.L1875
	movq	%rdx, %rax
	movq	%rdx, %rcx
	movl	%edx, %r9d
	shrq	$3, %rax
	shrq	$5, %rcx
	andl	$3, %eax
	andl	$4, %r9d
	je	.L1876
	movq	%rdx, %r10
	andq	$-8168, %r10
	cmpb	$11, %cl
	jbe	.L1877
	testl	%eax, %eax
	je	.L2017
	orq	$4, %r10
	jmp	.L1885
	.p2align 4,,10
	.p2align 3
.L2130:
	movq	24(%r15), %r8
	xorl	%ecx, %ecx
	testq	%r8, %r8
	je	.L1884
.L2131:
	movq	%r8, %r15
.L1885:
	movq	32(%r15), %rax
	movl	%eax, %edi
	movq	%rax, %rcx
	andl	$4, %edi
	je	.L1879
	testb	$24, %al
	je	.L1880
	xorl	%r8d, %r8d
.L1881:
	movq	%rax, %rcx
	andq	$-8161, %rcx
	orq	%r8, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1879:
	cmpq	%r10, %rcx
	jbe	.L2130
	movq	16(%r15), %r8
	movl	$1, %ecx
	testq	%r8, %r8
	jne	.L2131
	.p2align 4,,10
	.p2align 3
.L1884:
	testb	%cl, %cl
	jne	.L2132
.L1906:
	testl	%edi, %edi
	je	.L1908
	xorl	%ecx, %ecx
	testb	$24, %al
	jne	.L1909
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L1909:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1908:
	movq	%rdx, %rcx
	testl	%r9d, %r9d
	je	.L1910
	xorl	%edi, %edi
	testb	$24, %dl
	jne	.L1911
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rdi, %rdi
	notq	%rdi
	andl	$416, %edi
.L1911:
	movq	%rdx, %rcx
	andq	$-8161, %rcx
	orq	%rdi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1910:
	cmpq	%rax, %rcx
	jbe	.L1912
	testq	%r15, %r15
	je	.L1912
.L1907:
	movl	$1, %edi
	cmpq	%r15, %r14
	jne	.L2133
.L1913:
	movq	%r14, %rcx
	movq	%r15, %rdx
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r12)
.L1912:
	cmpl	$13, (%rbx)
	je	.L2134
.L1918:
	movq	-72(%rbp), %rax
	addq	$1, %r13
	addq	$16, %rbx
	movzbl	4(%rax), %eax
	cmpq	%rax, %r13
	jb	.L1932
	movq	%r12, %r15
.L1933:
	movq	-120(%rbp), %rbx
	addl	$1, -132(%rbp)
	addq	$24, -144(%rbp)
	movl	-132(%rbp), %eax
	cmpl	116(%rbx), %eax
	jl	.L1832
	movq	%r15, %rbx
.L1833:
	movq	-120(%rbp), %rax
	movq	-160(%rbp), %r12
	movl	100(%rax), %r15d
	movq	-104(%rbp), %rax
	movq	80(%rax), %rax
	testq	%rax, %rax
	jne	.L1809
	jmp	.L1808
	.p2align 4,,10
	.p2align 3
.L2135:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1934
.L1809:
	cmpl	32(%rax), %r15d
	jle	.L2135
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1809
.L1934:
	cmpq	%r12, -160(%rbp)
	je	.L1808
	cmpl	32(%r12), %r15d
	jge	.L1937
.L1808:
	movq	-104(%rbp), %rax
	movq	56(%rax), %rdi
	leaq	56(%rax), %r14
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$47, %rax
	jbe	.L2136
	leaq	48(%r13), %rax
	movq	%rax, 16(%rdi)
.L1939:
	movl	%r15d, 32(%r13)
	movq	%r12, %rsi
	leaq	32(%r13), %rdx
	movq	%r14, %rdi
	movq	$0, 40(%r13)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler9RpoNumberESt4pairIKS3_PNS2_16BlockAssessmentsEESt10_Select1stIS8_ESt4lessIS3_ENS1_13ZoneAllocatorIS8_EEE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS5_
	movq	%rax, %r12
	testq	%rdx, %rdx
	je	.L1937
	testq	%rax, %rax
	jne	.L1998
	cmpq	%rdx, -160(%rbp)
	jne	.L2137
.L1998:
	movl	$1, %edi
.L1941:
	movq	-160(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r13, %r12
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	movq	-104(%rbp), %rax
	addq	$1, 104(%rax)
.L1937:
	movq	-120(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%rbx, 40(%r12)
	movl	100(%rax), %edx
	movq	-104(%rbp), %rax
	movq	136(%rax), %rax
	testq	%rax, %rax
	jne	.L1942
	jmp	.L1947
	.p2align 4,,10
	.p2align 3
.L2138:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1944
.L1942:
	cmpl	32(%rax), %edx
	jle	.L2138
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1942
.L1944:
	cmpq	%rcx, -168(%rbp)
	je	.L1947
	cmpl	32(%rcx), %edx
	jl	.L1947
	movq	40(%rcx), %rax
	movq	32(%rax), %r15
	addq	$16, %rax
	movq	%rax, -72(%rbp)
	cmpq	%r15, %rax
	je	.L1947
	leaq	16(%rbx), %r14
	leaq	-64(%rbp), %r13
.L1952:
	movq	32(%r15), %rax
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	40(%r15), %r12d
	movq	%rax, -64(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE4findERS5_
	cmpq	%r14, %rax
	je	.L2139
	movq	40(%rax), %r8
	movl	(%r8), %eax
	testl	%eax, %eax
	je	.L1949
	cmpl	$1, %eax
	jne	.L1951
	movq	-120(%rbp), %rax
	movq	-64(%rbp), %rdx
	movl	%r12d, %r9d
	movq	%rbx, %rcx
	movq	-104(%rbp), %rdi
	movl	100(%rax), %esi
	call	_ZN2v88internal8compiler25RegisterAllocatorVerifier25ValidatePendingAssessmentENS1_9RpoNumberENS1_18InstructionOperandEPKNS1_16BlockAssessmentsEPNS1_17PendingAssessmentEi
.L1951:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, -72(%rbp)
	jne	.L1952
.L1947:
	addq	$1, -152(%rbp)
	movq	-152(%rbp), %rax
	cmpq	%rax, -176(%rbp)
	je	.L1803
	movq	-104(%rbp), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdx
	jmp	.L1953
	.p2align 4,,10
	.p2align 3
.L1858:
	movq	%rcx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
	jmp	.L1859
	.p2align 4,,10
	.p2align 3
.L2140:
	movq	24(%r15), %r8
	xorl	%ecx, %ecx
.L1901:
	testq	%r8, %r8
	je	.L1884
	movq	%r8, %r15
.L1876:
	movq	32(%r15), %rax
	movl	%eax, %edi
	movq	%rax, %rcx
	andl	$4, %edi
	je	.L1897
	testb	$24, %al
	je	.L1898
	xorl	%r8d, %r8d
.L1899:
	movq	%rax, %rcx
	andq	$-8161, %rcx
	orq	%r8, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1897:
	cmpq	%rcx, %rdx
	jnb	.L2140
	movq	16(%r15), %r8
	movl	$1, %ecx
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1898:
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%r8, %r8
	notq	%r8
	andl	$416, %r8d
	jmp	.L1899
	.p2align 4,,10
	.p2align 3
.L2001:
	movl	%edx, %ecx
	addq	$1, %r14
	movl	%edx, %eax
	addq	$16, %rbx
	shrl	$8, %ecx
	movzwl	%cx, %ecx
	cmpq	%r14, %rcx
	ja	.L1817
	jmp	.L1816
	.p2align 4,,10
	.p2align 3
.L2132:
	cmpq	32(%r12), %r15
	je	.L1907
.L1954:
	movq	%r15, %rdi
	movq	%rsi, -112(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-96(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	32(%rax), %rax
	movl	%edx, %r9d
	movl	%eax, %edi
	andl	$4, %r9d
	andl	$4, %edi
	jmp	.L1906
	.p2align 4,,10
	.p2align 3
.L2123:
	cmpq	%r14, %rdx
	jne	.L1818
	movq	24(%rbx), %r15
	testq	%r15, %r15
	je	.L1819
	movq	%r12, -72(%rbp)
.L1822:
	movq	24(%r15), %r12
	testq	%r12, %r12
	je	.L1820
.L1821:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L1821
.L1820:
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.L1822
	movq	-72(%rbp), %r12
.L1819:
	movq	$0, 24(%rbx)
	movq	%r14, 32(%rbx)
	movq	%r14, 40(%rbx)
	movq	$0, 48(%rbx)
	jmp	.L1823
	.p2align 4,,10
	.p2align 3
.L1839:
	movq	%rcx, %r8
	shrq	$5, %r8
	cmpb	$12, %r8b
	sbbq	%r8, %r8
	notq	%r8
	andl	$416, %r8d
	jmp	.L1840
	.p2align 4,,10
	.p2align 3
.L1880:
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%r8, %r8
	notq	%r8
	andl	$416, %r8d
	jmp	.L1881
	.p2align 4,,10
	.p2align 3
.L1877:
	orq	$4, %r10
	testl	%eax, %eax
	je	.L1886
	jmp	.L1891
	.p2align 4,,10
	.p2align 3
.L2141:
	movq	16(%r15), %r8
	movl	$1, %ecx
.L1890:
	testq	%r8, %r8
	je	.L1884
	movq	%r8, %r15
.L1891:
	movq	32(%r15), %rax
	movl	%eax, %edi
	movq	%rax, %rcx
	andl	$4, %edi
	je	.L1887
	xorl	%r8d, %r8d
	testb	$24, %al
	jne	.L1888
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%r8, %r8
	notq	%r8
	andl	$416, %r8d
.L1888:
	movq	%rax, %rcx
	andq	$-8161, %rcx
	orq	%r8, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1887:
	cmpq	%r10, %rcx
	ja	.L2141
	movq	24(%r15), %r8
	xorl	%ecx, %ecx
	jmp	.L1890
	.p2align 4,,10
	.p2align 3
.L2142:
	movq	24(%r15), %r8
	xorl	%ecx, %ecx
.L1896:
	testq	%r8, %r8
	je	.L1884
	movq	%r8, %r15
.L1886:
	movq	32(%r15), %rax
	movl	%eax, %edi
	movq	%rax, %rcx
	andl	$4, %edi
	je	.L1892
	testb	$24, %al
	je	.L1893
	xorl	%r8d, %r8d
.L1894:
	movq	%rax, %rcx
	andq	$-8161, %rcx
	orq	%r8, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1892:
	cmpq	%r10, %rcx
	jbe	.L2142
	movq	16(%r15), %r8
	movl	$1, %ecx
	jmp	.L1896
	.p2align 4,,10
	.p2align 3
.L1893:
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%r8, %r8
	notq	%r8
	andl	$416, %r8d
	jmp	.L1894
	.p2align 4,,10
	.p2align 3
.L1836:
	andq	$-8168, %rsi
	orq	$4, %rsi
	testl	%ecx, %ecx
	je	.L1845
	jmp	.L1850
	.p2align 4,,10
	.p2align 3
.L2143:
	movq	%rax, %rdi
	movq	16(%rax), %rax
.L1849:
	testq	%rax, %rax
	je	.L1843
.L1850:
	movq	32(%rax), %rcx
	testb	$4, %cl
	je	.L1846
	xorl	%r8d, %r8d
	testb	$24, %cl
	jne	.L1847
	movq	%rcx, %r8
	shrq	$5, %r8
	cmpb	$12, %r8b
	sbbq	%r8, %r8
	notq	%r8
	andl	$416, %r8d
.L1847:
	andq	$-8161, %rcx
	orq	%r8, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1846:
	cmpq	%rcx, %rsi
	jbe	.L2143
	movq	24(%rax), %rax
	jmp	.L1849
	.p2align 4,,10
	.p2align 3
.L2144:
	movq	24(%rax), %rax
.L1855:
	testq	%rax, %rax
	je	.L1843
.L1845:
	movq	32(%rax), %rcx
	testb	$4, %cl
	je	.L1851
	testb	$24, %cl
	je	.L1852
	xorl	%r8d, %r8d
.L1853:
	andq	$-8161, %rcx
	orq	%r8, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1851:
	cmpq	%rcx, %rsi
	ja	.L2144
	movq	%rax, %rdi
	movq	16(%rax), %rax
	jmp	.L1855
	.p2align 4,,10
	.p2align 3
.L1852:
	movq	%rcx, %r8
	shrq	$5, %r8
	cmpb	$12, %r8b
	sbbq	%r8, %r8
	notq	%r8
	andl	$416, %r8d
	jmp	.L1853
	.p2align 4,,10
	.p2align 3
.L2134:
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rsi
	movq	(%rax), %rdi
	movq	%r13, %rax
	salq	$4, %rax
	movslq	8(%rsi,%rax), %r15
	movq	-72(%rbp), %rax
	movq	24(%rdi), %rdx
	movq	40(%rax,%r13,8), %rax
	salq	$35, %r15
	movq	%rax, -96(%rbp)
	andl	$8160, %eax
	orq	%rax, %r15
	movq	16(%rdi), %rax
	orq	$13, %r15
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L2145
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1920:
	movq	%r15, (%rax)
	movq	-88(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r15, -64(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE4findERS5_
	movq	%rax, %rdi
	cmpq	%r14, %rax
	je	.L1921
	movq	%r14, %rsi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	subq	$1, 48(%r12)
.L1921:
	movq	112(%r12), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	$7, %rax
	jbe	.L2146
	leaq	8(%rdx), %rax
	movq	%rax, 16(%rdi)
.L1923:
	movl	-80(%rbp), %eax
	movl	$0, (%rdx)
	movl	%eax, 4(%rdx)
	movq	(%r12), %rdi
	movq	-64(%rbp), %rcx
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	subq	%r15, %rax
	cmpq	$47, %rax
	jbe	.L2147
	leaq	48(%r15), %rax
	movq	%rax, 16(%rdi)
.L1925:
	movq	%rcx, 32(%r15)
	leaq	32(%r15), %rsi
	movq	%r12, %rdi
	movq	%rdx, 40(%r15)
	call	_ZNSt8_Rb_treeIN2v88internal8compiler18InstructionOperandESt4pairIKS3_PNS2_10AssessmentEESt10_Select1stIS8_ENS2_16OperandAsKeyLessENS1_13ZoneAllocatorIS8_EEE24_M_get_insert_unique_posERS5_
	testq	%rdx, %rdx
	je	.L1918
	testq	%rax, %rax
	jne	.L1993
	cmpq	%r14, %rdx
	jne	.L2148
.L1993:
	movl	$1, %edi
.L1927:
	movq	%r14, %rcx
	movq	%r15, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r12)
	jmp	.L1918
	.p2align 4,,10
	.p2align 3
.L2133:
	testb	$4, %dl
	je	.L1914
	xorl	%eax, %eax
	testb	$24, %dl
	jne	.L1915
	movq	%rdx, %rax
	shrq	$5, %rax
	cmpb	$12, %al
	sbbq	%rax, %rax
	notq	%rax
	andl	$416, %eax
.L1915:
	andq	$-8161, %rdx
	orq	%rax, %rdx
	andq	$-8, %rdx
	orq	$4, %rdx
.L1914:
	movq	32(%r15), %rax
	testb	$4, %al
	je	.L1916
	xorl	%ecx, %ecx
	testb	$24, %al
	jne	.L1917
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L1917:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1916:
	xorl	%edi, %edi
	cmpq	%rdx, %rax
	seta	%dil
	jmp	.L1913
	.p2align 4,,10
	.p2align 3
.L1960:
	orq	$420, %rsi
	jmp	.L1837
	.p2align 4,,10
	.p2align 3
.L2150:
	movq	%rax, %rdi
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L2149
.L1837:
	movq	32(%rax), %rcx
	testb	$4, %cl
	je	.L1864
	xorl	%r8d, %r8d
	testb	$24, %cl
	jne	.L1865
	movq	%rcx, %r8
	shrq	$5, %r8
	cmpb	$12, %r8b
	sbbq	%r8, %r8
	notq	%r8
	andl	$416, %r8d
.L1865:
	andq	$-8161, %rcx
	orq	%r8, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1864:
	cmpq	%rcx, %rsi
	jbe	.L2150
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L1837
.L2149:
	movl	$416, %eax
	cmpq	%r14, %rdi
	jne	.L1955
	jmp	.L1834
	.p2align 4,,10
	.p2align 3
.L2017:
	orq	$420, %r10
	jmp	.L1878
	.p2align 4,,10
	.p2align 3
.L2151:
	movq	16(%r15), %r8
	movl	$1, %ecx
.L1905:
	testq	%r8, %r8
	je	.L1884
	movq	%r8, %r15
.L1878:
	movq	32(%r15), %rax
	movl	%eax, %edi
	movq	%rax, %rcx
	andl	$4, %edi
	je	.L1902
	xorl	%r8d, %r8d
	testb	$24, %al
	jne	.L1903
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%r8, %r8
	notq	%r8
	andl	$416, %r8d
.L1903:
	movq	%rax, %rcx
	andq	$-8161, %rcx
	orq	%r8, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1902:
	cmpq	%rcx, %r10
	jb	.L2151
	movq	24(%r15), %r8
	xorl	%ecx, %ecx
	jmp	.L1905
	.p2align 4,,10
	.p2align 3
.L1875:
	movq	%r14, %r15
	cmpq	%r14, 32(%r12)
	jne	.L1954
	movl	$1, %edi
	jmp	.L1913
	.p2align 4,,10
	.p2align 3
.L2129:
	movl	$48, %esi
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L1874
	.p2align 4,,10
	.p2align 3
.L2128:
	movl	$8, %esi
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L1872
	.p2align 4,,10
	.p2align 3
.L2124:
	movq	32(%r15), %r13
	leaq	16(%r15), %rsi
	cmpq	%rsi, %r13
	je	.L1828
	movq	%r12, -72(%rbp)
	movq	%rsi, %r14
	.p2align 4,,10
	.p2align 3
.L1829:
	movq	%r13, %rdi
	movq	%r13, %r12
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	movq	32(%r12), %rax
	testb	$4, %al
	je	.L1830
	testb	$24, %al
	jne	.L1830
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	subq	$1, 48(%r15)
.L1830:
	cmpq	%r13, %r14
	jne	.L1829
	movq	-72(%rbp), %r12
	movl	4(%r12), %eax
	jmp	.L1828
	.p2align 4,,10
	.p2align 3
.L2148:
	movq	32(%r15), %rax
	testb	$4, %al
	je	.L1928
	xorl	%ecx, %ecx
	testb	$24, %al
	jne	.L1929
	movq	%rax, %rcx
	shrq	$5, %rcx
	cmpb	$12, %cl
	sbbq	%rcx, %rcx
	notq	%rcx
	andl	$416, %ecx
.L1929:
	andq	$-8161, %rax
	orq	%rcx, %rax
	andq	$-8, %rax
	orq	$4, %rax
.L1928:
	movq	32(%rdx), %rcx
	testb	$4, %cl
	je	.L1930
	xorl	%esi, %esi
	testb	$24, %cl
	jne	.L1931
	movq	%rcx, %rsi
	shrq	$5, %rsi
	cmpb	$12, %sil
	sbbq	%rsi, %rsi
	notq	%rsi
	andl	$416, %esi
.L1931:
	andq	$-8161, %rcx
	orq	%rsi, %rcx
	andq	$-8, %rcx
	orq	$4, %rcx
.L1930:
	xorl	%edi, %edi
	cmpq	%rax, %rcx
	seta	%dil
	jmp	.L1927
	.p2align 4,,10
	.p2align 3
.L2146:
	movl	$8, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L1923
	.p2align 4,,10
	.p2align 3
.L2147:
	movl	$48, %esi
	movq	%rdx, -96(%rbp)
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L1925
	.p2align 4,,10
	.p2align 3
.L2145:
	movl	$8, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1920
	.p2align 4,,10
	.p2align 3
.L1949:
	cmpl	4(%r8), %r12d
	je	.L1951
	leaq	.LC44(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2139:
	leaq	.LC43(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2137:
	xorl	%edi, %edi
	movl	32(%rdx), %eax
	cmpl	%eax, 32(%r13)
	setl	%dil
	jmp	.L1941
.L2121:
	leaq	.LC41(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2122:
	leaq	.LC42(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2136:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L1939
.L1803:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2152
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2152:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10934:
	.size	_ZN2v88internal8compiler25RegisterAllocatorVerifier14VerifyGapMovesEv, .-_ZN2v88internal8compiler25RegisterAllocatorVerifier14VerifyGapMovesEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler25RegisterAllocatorVerifierC2EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler25RegisterAllocatorVerifierC2EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler25RegisterAllocatorVerifierC2EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE:
.LFB14371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE14371:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler25RegisterAllocatorVerifierC2EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE, .-_GLOBAL__sub_I__ZN2v88internal8compiler25RegisterAllocatorVerifierC2EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler25RegisterAllocatorVerifierC2EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE
	.section	.rodata.CSWTCH.268,"a"
	.align 32
	.type	CSWTCH.268, @object
	.size	CSWTCH.268, 56
CSWTCH.268:
	.long	0
	.long	0
	.long	1
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	2
	.long	3
	.long	4
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
