	.file	"feedback-source.cc"
	.text
	.section	.text._ZN2v88internal8compiler14FeedbackSourceC2ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14FeedbackSourceC2ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE
	.type	_ZN2v88internal8compiler14FeedbackSourceC2ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE, @function
_ZN2v88internal8compiler14FeedbackSourceC2ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE:
.LFB9765:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movl	%edx, 8(%rdi)
	ret
	.cfi_endproc
.LFE9765:
	.size	_ZN2v88internal8compiler14FeedbackSourceC2ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE, .-_ZN2v88internal8compiler14FeedbackSourceC2ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE
	.globl	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE
	.set	_ZN2v88internal8compiler14FeedbackSourceC1ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE,_ZN2v88internal8compiler14FeedbackSourceC2ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE
	.section	.text._ZN2v88internal8compiler14FeedbackSourceC2ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14FeedbackSourceC2ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE
	.type	_ZN2v88internal8compiler14FeedbackSourceC2ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE, @function
_ZN2v88internal8compiler14FeedbackSourceC2ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE:
.LFB9768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	-32(%rbp), %rdi
	subq	$16, %rsp
	movq	%rsi, -32(%rbp)
	movq	%rdx, -24(%rbp)
	call	_ZNK2v88internal8compiler17FeedbackVectorRef6objectEv@PLT
	movl	%r12d, 8(%rbx)
	movq	%rax, (%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9768:
	.size	_ZN2v88internal8compiler14FeedbackSourceC2ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE, .-_ZN2v88internal8compiler14FeedbackSourceC2ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE
	.globl	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE
	.set	_ZN2v88internal8compiler14FeedbackSourceC1ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE,_ZN2v88internal8compiler14FeedbackSourceC2ENS1_17FeedbackVectorRefENS0_12FeedbackSlotE
	.section	.text._ZN2v88internal8compiler14FeedbackSourceC2ERKNS0_13FeedbackNexusE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14FeedbackSourceC2ERKNS0_13FeedbackNexusE
	.type	_ZN2v88internal8compiler14FeedbackSourceC2ERKNS0_13FeedbackNexusE, @function
_ZN2v88internal8compiler14FeedbackSourceC2ERKNS0_13FeedbackNexusE:
.LFB9771:
	.cfi_startproc
	endbr64
	movl	16(%rsi), %eax
	movq	(%rsi), %rdx
	movl	%eax, 8(%rdi)
	movq	%rdx, (%rdi)
	ret
	.cfi_endproc
.LFE9771:
	.size	_ZN2v88internal8compiler14FeedbackSourceC2ERKNS0_13FeedbackNexusE, .-_ZN2v88internal8compiler14FeedbackSourceC2ERKNS0_13FeedbackNexusE
	.globl	_ZN2v88internal8compiler14FeedbackSourceC1ERKNS0_13FeedbackNexusE
	.set	_ZN2v88internal8compiler14FeedbackSourceC1ERKNS0_13FeedbackNexusE,_ZN2v88internal8compiler14FeedbackSourceC2ERKNS0_13FeedbackNexusE
	.section	.rodata._ZNK2v88internal8compiler14FeedbackSource5indexEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"IsValid()"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZNK2v88internal8compiler14FeedbackSource5indexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler14FeedbackSource5indexEv
	.type	_ZNK2v88internal8compiler14FeedbackSource5indexEv, @function
_ZNK2v88internal8compiler14FeedbackSource5indexEv:
.LFB9773:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L7
	movl	8(%rdi), %eax
	cmpl	$-1, %eax
	je	.L7
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE9773:
	.size	_ZNK2v88internal8compiler14FeedbackSource5indexEv, .-_ZNK2v88internal8compiler14FeedbackSource5indexEv
	.section	.text._ZN2v88internal8compilereqERKNS1_14FeedbackSourceES4_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilereqERKNS1_14FeedbackSourceES4_
	.type	_ZN2v88internal8compilereqERKNS1_14FeedbackSourceES4_, @function
_ZN2v88internal8compilereqERKNS1_14FeedbackSourceES4_:
.LFB9774:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	cmpq	%rdx, (%rsi)
	je	.L17
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movl	8(%rsi), %eax
	cmpl	%eax, 8(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE9774:
	.size	_ZN2v88internal8compilereqERKNS1_14FeedbackSourceES4_, .-_ZN2v88internal8compilereqERKNS1_14FeedbackSourceES4_
	.section	.text._ZN2v88internal8compilerneERKNS1_14FeedbackSourceES4_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerneERKNS1_14FeedbackSourceES4_
	.type	_ZN2v88internal8compilerneERKNS1_14FeedbackSourceES4_, @function
_ZN2v88internal8compilerneERKNS1_14FeedbackSourceES4_:
.LFB9775:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	$1, %eax
	cmpq	%rdx, (%rsi)
	je	.L21
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	8(%rsi), %eax
	cmpl	%eax, 8(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE9775:
	.size	_ZN2v88internal8compilerneERKNS1_14FeedbackSourceES4_, .-_ZN2v88internal8compilerneERKNS1_14FeedbackSourceES4_
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_14FeedbackSourceE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"FeedbackSource("
.LC3:
	.string	")"
.LC4:
	.string	"FeedbackSource(INVALID)"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_14FeedbackSourceE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_14FeedbackSourceE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_14FeedbackSourceE, @function
_ZN2v88internal8compilerlsERSoRKNS1_14FeedbackSourceE:
.LFB9776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	$0, (%rsi)
	je	.L23
	cmpl	$-1, 8(%rsi)
	movq	%rsi, %rbx
	je	.L23
	movl	$15, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%rbx), %esi
	movq	%r12, %rdi
	call	_ZN2v88internallsERSoNS0_12FeedbackSlotE@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$23, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9776:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_14FeedbackSourceE, .-_ZN2v88internal8compilerlsERSoRKNS1_14FeedbackSourceE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler14FeedbackSourceC2ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler14FeedbackSourceC2ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler14FeedbackSourceC2ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE:
.LFB11356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11356:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler14FeedbackSourceC2ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE, .-_GLOBAL__sub_I__ZN2v88internal8compiler14FeedbackSourceC2ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler14FeedbackSourceC2ENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
