	.file	"memory-optimizer.cc"
	.text
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_111CanAllocateEPKNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_111CanAllocateEPKNS1_4NodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_111CanAllocateEPKNS1_4NodeE:
.LFB15721:
	.cfi_startproc
	movq	(%rdi), %rdi
	movzwl	16(%rdi), %ecx
	cmpw	$62, %cx
	ja	.L2
	cmpw	$6, %cx
	jbe	.L15
	subl	$7, %ecx
	cmpw	$55, %cx
	ja	.L15
	leaq	.L8(%rip), %rdx
	movzwl	%cx, %ecx
	movslq	(%rdx,%rcx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_111CanAllocateEPKNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L8:
	.long	.L7-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L7-.L8
	.long	.L7-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L7-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L9-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L7-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L7-.L8
	.long	.L15-.L8
	.long	.L15-.L8
	.long	.L7-.L8
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_111CanAllocateEPKNS1_4NodeE
.L7:
	xorl	%eax, %eax
	ret
.L15:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$1, %eax
	cmpw	$510, %cx
	ja	.L19
	cmpw	$439, %cx
	ja	.L10
	cmpw	$248, %cx
	ja	.L11
	cmpw	$239, %cx
	jbe	.L22
	subw	$240, %cx
	movl	$1, %eax
	salq	%cl, %rax
	testl	$363, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	subw	$440, %cx
	cmpw	$70, %cx
	ja	.L15
	leaq	.L6(%rip), %rdx
	movzwl	%cx, %ecx
	movslq	(%rdx,%rcx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_111CanAllocateEPKNS1_4NodeE
	.align 4
	.align 4
.L6:
	.long	.L7-.L6
	.long	.L15-.L6
	.long	.L7-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L7-.L6
	.long	.L7-.L6
	.long	.L7-.L6
	.long	.L7-.L6
	.long	.L7-.L6
	.long	.L7-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L7-.L6
	.long	.L7-.L6
	.long	.L7-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L7-.L6
	.long	.L7-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L7-.L6
	.long	.L7-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L15-.L6
	.long	.L7-.L6
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_111CanAllocateEPKNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L11:
	subw	$399, %cx
	cmpw	$32, %cx
	seta	%al
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	ret
.L9:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler16CallDescriptorOfEPKNS1_8OperatorE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	64(%rax), %eax
	shrl	$4, %eax
	xorl	$1, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE15721:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_111CanAllocateEPKNS1_4NodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_111CanAllocateEPKNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC2EPNS1_4NodeENS0_14AllocationTypeEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC2EPNS1_4NodeENS0_14AllocationTypeEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC2EPNS1_4NodeENS0_14AllocationTypeEPNS0_4ZoneE, @function
_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC2EPNS1_4NodeENS0_14AllocationTypeEPNS0_4ZoneE:
.LFB15704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%rcx, (%rdi)
	movl	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	%rdx, 32(%rdi)
	movq	%rdx, 40(%rdi)
	movq	$0, 48(%rdi)
	movl	20(%rsi), %r12d
	movq	24(%rcx), %rax
	movq	16(%rcx), %rsi
	movb	%r8b, 56(%rdi)
	movq	$0, 64(%rdi)
	andl	$16777215, %r12d
	subq	%rsi, %rax
	cmpq	$39, %rax
	jbe	.L27
	leaq	40(%rsi), %rax
	movq	%rax, 16(%rcx)
.L25:
	movl	%r12d, 32(%rsi)
	movq	%rdx, %rcx
	movl	$1, %edi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movl	$40, %esi
	movq	%rcx, %rdi
	movq	%rdx, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-24(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L25
	.cfi_endproc
.LFE15704:
	.size	_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC2EPNS1_4NodeENS0_14AllocationTypeEPNS0_4ZoneE, .-_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC2EPNS1_4NodeENS0_14AllocationTypeEPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC1EPNS1_4NodeENS0_14AllocationTypeEPNS0_4ZoneE
	.set	_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC1EPNS1_4NodeENS0_14AllocationTypeEPNS0_4ZoneE,_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC2EPNS1_4NodeENS0_14AllocationTypeEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC2EPNS1_4NodeENS0_14AllocationTypeES5_PNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC2EPNS1_4NodeENS0_14AllocationTypeES5_PNS0_4ZoneE
	.type	_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC2EPNS1_4NodeENS0_14AllocationTypeES5_PNS0_4ZoneE, @function
_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC2EPNS1_4NodeENS0_14AllocationTypeES5_PNS0_4ZoneE:
.LFB15707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r9d
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%r8, (%rdi)
	movl	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	%rdx, 32(%rdi)
	movq	%rdx, 40(%rdi)
	movq	$0, 48(%rdi)
	movl	20(%rsi), %r12d
	movq	24(%r8), %rax
	movq	16(%r8), %rsi
	movb	%r9b, 56(%rdi)
	movq	%rcx, 64(%rdi)
	andl	$16777215, %r12d
	subq	%rsi, %rax
	cmpq	$39, %rax
	jbe	.L32
	leaq	40(%rsi), %rax
	movq	%rax, 16(%r8)
.L30:
	movl	%r12d, 32(%rsi)
	movq	%rdx, %rcx
	movl	$1, %edi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movl	$40, %esi
	movq	%r8, %rdi
	movq	%rdx, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-24(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L30
	.cfi_endproc
.LFE15707:
	.size	_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC2EPNS1_4NodeENS0_14AllocationTypeES5_PNS0_4ZoneE, .-_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC2EPNS1_4NodeENS0_14AllocationTypeES5_PNS0_4ZoneE
	.globl	_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC1EPNS1_4NodeENS0_14AllocationTypeES5_PNS0_4ZoneE
	.set	_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC1EPNS1_4NodeENS0_14AllocationTypeES5_PNS0_4ZoneE,_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroupC2EPNS1_4NodeENS0_14AllocationTypeES5_PNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer15AllocationGroup3AddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroup3AddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroup3AddEPNS1_4NodeE, @function
_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroup3AddEPNS1_4NodeE:
.LFB15709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	20(%rsi), %r13d
	movq	24(%rdi), %r12
	andl	$16777215, %r13d
	testq	%r12, %r12
	jne	.L35
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L52:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L36
.L53:
	movq	%rax, %r12
.L35:
	movl	32(%r12), %ecx
	cmpl	%ecx, %r13d
	jb	.L52
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L53
.L36:
	testb	%dl, %dl
	jne	.L34
	cmpl	%ecx, %r13d
	jbe	.L33
.L44:
	movl	$1, %r15d
	cmpq	%r12, %r14
	jne	.L54
.L41:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$39, %rax
	jbe	.L55
	leaq	40(%rsi), %rax
	movq	%rax, 16(%rdi)
.L43:
	movl	%r13d, 32(%rsi)
	movq	%r14, %rcx
	movq	%r12, %rdx
	movl	%r15d, %edi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
.L33:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	%r14, %r12
.L34:
	cmpq	32(%rbx), %r12
	je	.L44
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpl	32(%rax), %r13d
	jbe	.L33
	movl	$1, %r15d
	cmpq	%r12, %r14
	je	.L41
.L54:
	xorl	%r15d, %r15d
	cmpl	32(%r12), %r13d
	setb	%r15b
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$40, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L43
	.cfi_endproc
.LFE15709:
	.size	_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroup3AddEPNS1_4NodeE, .-_ZN2v88internal8compiler15MemoryOptimizer15AllocationGroup3AddEPNS1_4NodeE
	.section	.text._ZNK2v88internal8compiler15MemoryOptimizer15AllocationGroup8ContainsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15MemoryOptimizer15AllocationGroup8ContainsEPNS1_4NodeE
	.type	_ZNK2v88internal8compiler15MemoryOptimizer15AllocationGroup8ContainsEPNS1_4NodeE, @function
_ZNK2v88internal8compiler15MemoryOptimizer15AllocationGroup8ContainsEPNS1_4NodeE:
.LFB15710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	16(%r12), %rbx
	.p2align 4,,10
	.p2align 3
.L65:
	movl	20(%rdi), %ecx
	movq	24(%r12), %rdx
	andl	$16777215, %ecx
	testq	%rdx, %rdx
	je	.L57
	movq	%rbx, %rsi
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L59
.L58:
	cmpl	%ecx, 32(%rdx)
	jnb	.L81
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L58
.L59:
	cmpq	%rbx, %rsi
	je	.L57
	cmpl	%ecx, 32(%rsi)
	jbe	.L66
.L57:
	movq	(%rdi), %rax
	movzwl	16(%rax), %eax
	cmpw	$440, %ax
	je	.L63
	ja	.L64
	cmpw	$306, %ax
	je	.L63
	cmpw	$325, %ax
	jne	.L67
.L63:
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rdi
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L64:
	cmpw	$442, %ax
	je	.L63
.L67:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L66:
	.cfi_restore_state
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15710:
	.size	_ZNK2v88internal8compiler15MemoryOptimizer15AllocationGroup8ContainsEPNS1_4NodeE, .-_ZNK2v88internal8compiler15MemoryOptimizer15AllocationGroup8ContainsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2Ev
	.type	_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2Ev, @function
_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2Ev:
.LFB15712:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movq	$2147483647, 8(%rdi)
	movq	$0, 16(%rdi)
	ret
	.cfi_endproc
.LFE15712:
	.size	_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2Ev, .-_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2Ev
	.globl	_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC1Ev
	.set	_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC1Ev,_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2Ev
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2EPNS2_15AllocationGroupE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2EPNS2_15AllocationGroupE
	.type	_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2EPNS2_15AllocationGroupE, @function
_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2EPNS2_15AllocationGroupE:
.LFB15715:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movq	$2147483647, 8(%rdi)
	movq	$0, 16(%rdi)
	ret
	.cfi_endproc
.LFE15715:
	.size	_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2EPNS2_15AllocationGroupE, .-_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2EPNS2_15AllocationGroupE
	.globl	_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC1EPNS2_15AllocationGroupE
	.set	_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC1EPNS2_15AllocationGroupE,_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2EPNS2_15AllocationGroupE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2EPNS2_15AllocationGroupElPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2EPNS2_15AllocationGroupElPNS1_4NodeE
	.type	_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2EPNS2_15AllocationGroupElPNS1_4NodeE, @function
_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2EPNS2_15AllocationGroupElPNS1_4NodeE:
.LFB15718:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movq	%rdx, 8(%rdi)
	movq	%rcx, 16(%rdi)
	ret
	.cfi_endproc
.LFE15718:
	.size	_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2EPNS2_15AllocationGroupElPNS1_4NodeE, .-_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2EPNS2_15AllocationGroupElPNS1_4NodeE
	.globl	_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC1EPNS2_15AllocationGroupElPNS1_4NodeE
	.set	_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC1EPNS2_15AllocationGroupElPNS1_4NodeE,_ZN2v88internal8compiler15MemoryOptimizer15AllocationStateC2EPNS2_15AllocationGroupElPNS1_4NodeE
	.section	.text._ZNK2v88internal8compiler15MemoryOptimizer15AllocationState27IsYoungGenerationAllocationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15MemoryOptimizer15AllocationState27IsYoungGenerationAllocationEv
	.type	_ZNK2v88internal8compiler15MemoryOptimizer15AllocationState27IsYoungGenerationAllocationEv, @function
_ZNK2v88internal8compiler15MemoryOptimizer15AllocationState27IsYoungGenerationAllocationEv:
.LFB15720:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L85
	cmpb	$0, 56(%rdx)
	sete	%al
.L85:
	ret
	.cfi_endproc
.LFE15720:
	.size	_ZNK2v88internal8compiler15MemoryOptimizer15AllocationState27IsYoungGenerationAllocationEv, .-_ZNK2v88internal8compiler15MemoryOptimizer15AllocationState27IsYoungGenerationAllocationEv
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer30AllocationTypeNeedsUpdateToOldEPNS1_4NodeENS1_4EdgeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer30AllocationTypeNeedsUpdateToOldEPNS1_4NodeENS1_4EdgeE
	.type	_ZN2v88internal8compiler15MemoryOptimizer30AllocationTypeNeedsUpdateToOldEPNS1_4NodeENS1_4EdgeE, @function
_ZN2v88internal8compiler15MemoryOptimizer30AllocationTypeNeedsUpdateToOldEPNS1_4NodeENS1_4EdgeE:
.LFB15741:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rcx
	xorl	%eax, %eax
	cmpw	$245, 16(%rcx)
	je	.L99
.L96:
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	movl	16(%rdx), %edx
	shrl	%edx
	cmpl	$1, %edx
	jne	.L96
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L91
	movq	16(%rdx), %rdx
.L91:
	movq	(%rdx), %rdi
	xorl	%eax, %eax
	cmpw	$238, 16(%rdi)
	jne	.L96
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler16AllocationTypeOfEPKNS1_8OperatorE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpb	$1, %al
	sete	%al
	ret
	.cfi_endproc
.LFE15741:
	.size	_ZN2v88internal8compiler15MemoryOptimizer30AllocationTypeNeedsUpdateToOldEPNS1_4NodeENS1_4EdgeE, .-_ZN2v88internal8compiler15MemoryOptimizer30AllocationTypeNeedsUpdateToOldEPNS1_4NodeENS1_4EdgeE
	.section	.rodata._ZN2v88internal8compiler15MemoryOptimizer12ComputeIndexERKNS1_13ElementAccessEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer12ComputeIndexERKNS1_13ElementAccessEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer12ComputeIndexERKNS1_13ElementAccessEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15MemoryOptimizer12ComputeIndexERKNS1_13ElementAccessEPNS1_4NodeE, @function
_ZN2v88internal8compiler15MemoryOptimizer12ComputeIndexERKNS1_13ElementAccessEPNS1_4NodeE:
.LFB15758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	16(%rsi), %eax
	subl	$1, %eax
	cmpb	$13, %al
	ja	.L101
	movq	%rdx, %r12
	movzbl	%al, %eax
	leaq	CSWTCH.227(%rip), %rdx
	movq	%rsi, %rbx
	movslq	(%rdx,%rax,4), %rsi
	movq	%rdi, %r13
	testl	%esi, %esi
	jne	.L114
	xorl	%eax, %eax
	movl	4(%rbx), %esi
	cmpb	$1, (%rbx)
	sete	%al
	subl	%eax, %esi
	jne	.L115
.L106:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L116
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler12MachineGraph14IntPtrConstantEl@PLT
	movq	%rax, -72(%rbp)
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	cmpb	$4, 16(%rdi)
	je	.L117
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word64ShlEv@PLT
	movq	%rax, %rsi
.L105:
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %rcx
	movq	%r14, %rdi
	movhps	-72(%rbp), %xmm0
	movl	$2, %edx
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	4(%rbx), %esi
	movq	%rax, %r12
	xorl	%eax, %eax
	cmpb	$1, (%rbx)
	sete	%al
	subl	%eax, %esi
	je	.L106
.L115:
	movq	8(%r13), %rdi
	movslq	%esi, %rsi
	movq	(%rdi), %r14
	call	_ZN2v88internal8compiler12MachineGraph14IntPtrConstantEl@PLT
	movq	%rax, -72(%rbp)
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	cmpb	$4, 16(%rdi)
	je	.L118
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int64AddEv@PLT
	movq	%rax, %rsi
.L108:
	movq	%r12, %xmm0
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movhps	-72(%rbp), %xmm0
	movl	$2, %edx
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L117:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShlEv@PLT
	movq	%rax, %rsi
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L118:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	movq	%rax, %rsi
	jmp	.L108
.L116:
	call	__stack_chk_fail@PLT
.L101:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE15758:
	.size	_ZN2v88internal8compiler15MemoryOptimizer12ComputeIndexERKNS1_13ElementAccessEPNS1_4NodeE, .-_ZN2v88internal8compiler15MemoryOptimizer12ComputeIndexERKNS1_13ElementAccessEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer11MergeStatesERKNS0_10ZoneVectorIPKNS2_15AllocationStateEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer11MergeStatesERKNS0_10ZoneVectorIPKNS2_15AllocationStateEEE
	.type	_ZN2v88internal8compiler15MemoryOptimizer11MergeStatesERKNS0_10ZoneVectorIPKNS2_15AllocationStateEEE, @function
_ZN2v88internal8compiler15MemoryOptimizer11MergeStatesERKNS0_10ZoneVectorIPKNS2_15AllocationStateEEE:
.LFB15762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	8(%rsi), %rdx
	movq	16(%rsi), %rcx
	movq	(%rdx), %r8
	subq	%rdx, %rcx
	movq	(%r8), %rbx
	cmpq	$15, %rcx
	jbe	.L119
	leaq	8(%rdx), %rax
	addq	%rdx, %rcx
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L126:
	movq	(%rax), %rdx
	cmpq	%rdx, %r8
	cmovne	%rsi, %r8
	addq	$8, %rax
	cmpq	%rbx, (%rdx)
	je	.L139
	cmpq	%rax, %rcx
	je	.L125
	xorl	%ebx, %ebx
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L139:
	cmpq	%rcx, %rax
	jne	.L126
	testq	%r8, %r8
	jne	.L119
	testq	%rbx, %rbx
	je	.L127
	movq	176(%rdi), %rdi
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpq	$23, %rax
	jbe	.L140
	leaq	24(%r8), %rax
	movq	%rax, 16(%rdi)
.L129:
	movq	%rbx, (%r8)
	movq	$2147483647, 8(%r8)
	movq	$0, 16(%r8)
.L119:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L119
.L127:
	movq	16(%rdi), %r8
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%r8, %rax
	ret
.L140:
	.cfi_restore_state
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r8
	jmp	.L129
	.cfi_endproc
.LFE15762:
	.size	_ZN2v88internal8compiler15MemoryOptimizer11MergeStatesERKNS0_10ZoneVectorIPKNS2_15AllocationStateEEE, .-_ZN2v88internal8compiler15MemoryOptimizer11MergeStatesERKNS0_10ZoneVectorIPKNS2_15AllocationStateEEE
	.section	.text._ZNK2v88internal8compiler15MemoryOptimizer5graphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15MemoryOptimizer5graphEv
	.type	_ZNK2v88internal8compiler15MemoryOptimizer5graphEv, @function
_ZNK2v88internal8compiler15MemoryOptimizer5graphEv:
.LFB15779:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE15779:
	.size	_ZNK2v88internal8compiler15MemoryOptimizer5graphEv, .-_ZNK2v88internal8compiler15MemoryOptimizer5graphEv
	.section	.text._ZNK2v88internal8compiler15MemoryOptimizer7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15MemoryOptimizer7isolateEv
	.type	_ZNK2v88internal8compiler15MemoryOptimizer7isolateEv, @function
_ZNK2v88internal8compiler15MemoryOptimizer7isolateEv:
.LFB15780:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	360(%rax), %rax
	ret
	.cfi_endproc
.LFE15780:
	.size	_ZNK2v88internal8compiler15MemoryOptimizer7isolateEv, .-_ZNK2v88internal8compiler15MemoryOptimizer7isolateEv
	.section	.text._ZNK2v88internal8compiler15MemoryOptimizer6commonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15MemoryOptimizer6commonEv
	.type	_ZNK2v88internal8compiler15MemoryOptimizer6commonEv, @function
_ZNK2v88internal8compiler15MemoryOptimizer6commonEv:
.LFB15781:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE15781:
	.size	_ZNK2v88internal8compiler15MemoryOptimizer6commonEv, .-_ZNK2v88internal8compiler15MemoryOptimizer6commonEv
	.section	.text._ZNK2v88internal8compiler15MemoryOptimizer7machineEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15MemoryOptimizer7machineEv
	.type	_ZNK2v88internal8compiler15MemoryOptimizer7machineEv, @function
_ZNK2v88internal8compiler15MemoryOptimizer7machineEv:
.LFB15782:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	16(%rax), %rax
	ret
	.cfi_endproc
.LFE15782:
	.size	_ZNK2v88internal8compiler15MemoryOptimizer7machineEv, .-_ZNK2v88internal8compiler15MemoryOptimizer7machineEv
	.section	.text._ZNK2v88internal8compiler15MemoryOptimizer14NeedsPoisoningENS0_15LoadSensitivityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15MemoryOptimizer14NeedsPoisoningENS0_15LoadSensitivityE
	.type	_ZNK2v88internal8compiler15MemoryOptimizer14NeedsPoisoningENS0_15LoadSensitivityE, @function
_ZNK2v88internal8compiler15MemoryOptimizer14NeedsPoisoningENS0_15LoadSensitivityE:
.LFB15783:
	.cfi_startproc
	endbr64
	cmpl	$2, %esi
	je	.L150
	movl	224(%rdi), %eax
	cmpl	$1, %eax
	je	.L150
	cmpl	$2, %eax
	je	.L147
	testl	%eax, %eax
	je	.L155
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	testl	%esi, %esi
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE15783:
	.size	_ZNK2v88internal8compiler15MemoryOptimizer14NeedsPoisoningENS0_15LoadSensitivityE, .-_ZNK2v88internal8compiler15MemoryOptimizer14NeedsPoisoningENS0_15LoadSensitivityE
	.section	.text._ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_
	.type	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_, @function
_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_:
.LFB17305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L170
	movl	(%rsi), %esi
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L177:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L159
.L178:
	movq	%rax, %r12
.L158:
	movl	32(%r12), %ecx
	cmpl	%ecx, %esi
	jb	.L177
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L178
.L159:
	testb	%dl, %dl
	jne	.L157
	cmpl	%ecx, %esi
	jbe	.L163
.L168:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L179
.L164:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$39, %rax
	jbe	.L180
	leaq	40(%r13), %rax
	movq	%rax, 16(%rdi)
.L166:
	movl	(%r14), %eax
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	%r8d, %edi
	movl	%eax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movq	%r15, %r12
.L157:
	cmpq	%r12, 32(%rbx)
	je	.L168
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	(%r14), %ecx
	cmpl	%ecx, 32(%rax)
	jb	.L168
	movq	%rax, %r12
.L163:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movl	32(%r12), %eax
	cmpl	%eax, (%r14)
	setb	%r8b
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L180:
	movl	$40, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L166
	.cfi_endproc
.LFE17305:
	.size	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_, .-_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB17352:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L189
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L183:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L183
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE17352:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_:
.LFB17354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L206
	movq	(%rsi), %rsi
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L213:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L195
.L214:
	movq	%rax, %r12
.L194:
	movq	32(%r12), %rcx
	cmpq	%rcx, %rsi
	jb	.L213
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L214
.L195:
	testb	%dl, %dl
	jne	.L193
	cmpq	%rcx, %rsi
	jbe	.L199
.L204:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L215
.L200:
	movq	(%rbx), %rdi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	$39, %rax
	jbe	.L216
	leaq	40(%r13), %rax
	movq	%rax, 16(%rdi)
.L202:
	movq	(%r14), %rax
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	movq	%r15, %r12
.L193:
	cmpq	%r12, 32(%rbx)
	je	.L204
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	(%r14), %rcx
	cmpq	%rcx, 32(%rax)
	jb	.L204
	movq	%rax, %r12
.L199:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movq	32(%r12), %rax
	cmpq	%rax, (%r14)
	setb	%r8b
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L216:
	movl	$40, %esi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L202
	.cfi_endproc
.LFE17354:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	.section	.text._ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,"axG",@progbits,_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.type	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, @function
_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_:
.LFB17384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	32(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 4(%rsi)
	movq	8(%rsi), %rax
	je	.L254
	testl	%eax, %eax
	jne	.L231
	movq	%r14, 24(%rsi)
	movq	24(%rdi), %rdx
	movq	%rdx, 16(%rsi)
.L220:
	addq	$1, %rax
	movq	%rax, 8(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L255
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	movl	%eax, %r13d
	movq	16(%r12), %rax
	movq	24(%rsi), %rdi
	movq	(%rax), %r15
	cmpl	$1, %r13d
	je	.L256
	movq	(%r15), %rsi
	movq	%r14, %rdx
	leal	1(%r13), %r14d
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%r12), %rax
	movl	%r14d, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	24(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%rbx), %rdi
	movslq	%r13d, %rax
	movq	24(%r12), %r15
	salq	$3, %rax
	movzbl	23(%rdi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L233
	leaq	32(%rdi,%rax), %rax
	movq	(%rax), %r8
	cmpq	%r8, %r15
	je	.L235
.L234:
	notl	%r13d
	movslq	%r13d, %r13
	leaq	0(%r13,%r13,2), %rdx
	leaq	(%rdi,%rdx,8), %r13
	testq	%r8, %r8
	je	.L236
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %rax
.L236:
	movq	%r15, (%rax)
	testq	%r15, %r15
	je	.L252
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L252:
	movq	16(%rbx), %rdi
.L235:
	movq	16(%r12), %rax
	movq	24(%rbx), %rdx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%r12), %rax
	movl	%r14d, %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L253:
	movq	8(%rbx), %rax
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L254:
	testl	%eax, %eax
	je	.L257
	movq	24(%rsi), %rsi
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L221
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %r15
	cmpq	%rdi, %r14
	je	.L224
.L223:
	leaq	-48(%rsi), %r13
	testq	%rdi, %rdi
	je	.L225
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L225:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L224
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L224:
	movq	16(%rbx), %rsi
	movq	24(%r12), %r13
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L226
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %r14
	cmpq	%rdi, %r13
	je	.L253
.L228:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L229
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L229:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L253
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	8(%rbx), %rax
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L257:
	movq	16(%rdi), %rax
	movq	%r14, %xmm0
	movl	$2, %esi
	leaq	-80(%rbp), %r14
	punpcklqdq	%xmm0, %xmm0
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4LoopEi@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r14, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, 24(%rbx)
	movq	%rax, %r13
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$3, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r13, -64(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%rbx), %r13
	movq	%rax, 16(%rbx)
	movq	%rax, -96(%rbp)
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TerminateEv@PLT
	movq	%r13, %xmm2
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rdx
	movq	16(%r12), %rax
	movq	8(%rax), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	8(%rbx), %rax
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L233:
	movq	32(%rdi), %rdx
	leaq	16(%rdx,%rax), %rax
	movq	(%rax), %r8
	cmpq	%r8, %r15
	je	.L235
	movq	%rdx, %rdi
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L256:
	movq	%r14, %xmm3
	movq	%rdi, %xmm0
	movq	8(%rax), %rdi
	movl	$2, %esi
	punpcklqdq	%xmm3, %xmm0
	leaq	-80(%rbp), %r14
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r14, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, 24(%rbx)
	movq	%rax, %r13
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	movq	16(%rbx), %r12
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movhps	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r15, %rdi
	movq	%r13, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 16(%rbx)
	movq	8(%rbx), %rax
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L221:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L224
	leaq	24(%rsi), %r15
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L226:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L253
	leaq	24(%rsi), %r14
	jmp	.L228
.L255:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17384:
	.size	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, .-_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.section	.text._ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,"axG",@progbits,_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.type	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, @function
_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_:
.LFB16814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	4(%rdx), %edx
	movq	32(%rdi), %r12
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	testl	%edx, %edx
	movl	$1, %edx
	sete	%sil
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%r12, %xmm1
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, 32(%rbx)
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	movq	%r12, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 32(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L261
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L261:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE16814:
	.size	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, .-_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.section	.text._ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,"axG",@progbits,_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.type	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, @function
_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_:
.LFB17385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	32(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 4(%rsi)
	movq	8(%rsi), %rax
	je	.L319
	testl	%eax, %eax
	je	.L320
	movl	%eax, %r15d
	movq	16(%r12), %rax
	movq	24(%rsi), %rdi
	movq	(%rax), %r14
	cmpl	$1, %r15d
	je	.L321
	movq	(%r14), %rsi
	movslq	%r15d, %r14
	salq	$3, %r14
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%r12), %rax
	leal	1(%r15), %esi
	movl	%esi, -96(%rbp)
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movq	24(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	16(%rbx), %rdi
	movq	24(%r12), %r8
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L283
	leaq	32(%rdi,%r14), %rdx
	movq	(%rdx), %r9
	cmpq	%r9, %r8
	je	.L285
.L284:
	movl	%r15d, %eax
	notl	%eax
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %rsi
	testq	%r9, %r9
	je	.L286
	movq	%r9, %rdi
	movq	%rdx, -128(%rbp)
	movq	%r8, -120(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %r8
	movq	-112(%rbp), %rsi
.L286:
	movq	%r8, (%rdx)
	testq	%r8, %r8
	je	.L316
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L316:
	movq	16(%rbx), %rdi
.L285:
	movq	16(%r12), %rax
	movq	24(%rbx), %rdx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%r12), %rax
	movl	-96(%rbp), %esi
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	32(%rbx), %rdi
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L288
	leaq	32(%rdi,%r14), %r14
	movq	(%r14), %r8
	cmpq	%r8, %r13
	je	.L290
.L289:
	movl	%r15d, %ecx
	notl	%ecx
	movslq	%ecx, %rcx
	leaq	(%rcx,%rcx,2), %rax
	leaq	(%rdi,%rax,8), %rsi
	testq	%r8, %r8
	je	.L291
	movq	%r8, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-112(%rbp), %rsi
.L291:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L317
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L317:
	movq	32(%rbx), %rdi
.L290:
	movq	16(%r12), %rax
	movq	24(%rbx), %rdx
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movq	16(%r12), %rax
	movzbl	48(%rbx), %esi
	movl	-96(%rbp), %edx
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	32(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L318:
	movq	8(%rbx), %rax
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L320:
	movq	%rdx, 24(%rsi)
	movq	24(%rdi), %rdx
	movq	%r13, 32(%rsi)
	movq	%rdx, 16(%rsi)
.L265:
	addq	$1, %rax
	movq	%rax, 8(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L322
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L323
	movq	24(%rsi), %rsi
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L266
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %r15
	cmpq	%rdi, %rdx
	je	.L269
.L268:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L270
	movq	%r14, %rsi
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-96(%rbp), %rdx
.L270:
	movq	%rdx, (%r15)
	testq	%rdx, %rdx
	je	.L269
	movq	%r14, %rsi
	movq	%rdx, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L269:
	movq	16(%rbx), %rsi
	movq	24(%r12), %r14
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L271
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %r15
	cmpq	%rdi, %r14
	je	.L274
.L273:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L275
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L275:
	movq	%r14, (%r15)
	testq	%r14, %r14
	je	.L274
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L274:
	movq	32(%rbx), %rsi
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L276
	movq	40(%rsi), %rdi
	leaq	40(%rsi), %r14
	cmpq	%rdi, %r13
	je	.L318
.L278:
	leaq	-48(%rsi), %r12
	testq	%rdi, %rdi
	je	.L279
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L279:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L318
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	8(%rbx), %rax
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L323:
	movq	16(%rdi), %rax
	movq	%rdx, %xmm0
	movq	%r13, %xmm1
	movl	$2, %esi
	punpcklqdq	%xmm1, %xmm1
	punpcklqdq	%xmm0, %xmm0
	leaq	-80(%rbp), %r13
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4LoopEi@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r13, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, 24(%rbx)
	movq	%rax, %r14
	movq	16(%r12), %rax
	movq	24(%r12), %rcx
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	movq	%rcx, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$3, %edx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%r14, -64(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%rbx), %r14
	movq	%rax, 16(%rbx)
	movq	%rax, -96(%rbp)
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9TerminateEv@PLT
	movq	%r14, %xmm3
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %rdx
	movq	16(%r12), %rax
	movq	8(%rax), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler14NodeProperties17MergeControlToEndEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_4NodeE@PLT
	movq	16(%r12), %rax
	movzbl	48(%rbx), %esi
	movl	$2, %edx
	movq	24(%rbx), %r14
	movq	8(%rax), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movdqa	-112(%rbp), %xmm1
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rsi
	movl	$3, %edx
	movq	%r12, %rdi
	movq	%r14, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 32(%rbx)
	movq	8(%rbx), %rax
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L283:
	movq	32(%rdi), %rax
	leaq	16(%rax,%r14), %rdx
	movq	(%rdx), %r9
	cmpq	%r9, %r8
	je	.L285
	movq	%rax, %rdi
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L321:
	movq	%rdx, %xmm4
	movq	%rdi, %xmm0
	movq	8(%rax), %rdi
	leaq	-80(%rbp), %r15
	punpcklqdq	%xmm4, %xmm0
	movl	$2, %esi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r15, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%rax, 24(%rbx)
	movq	24(%r12), %rcx
	movq	%rax, -120(%rbp)
	movq	16(%r12), %rax
	movq	%rcx, -96(%rbp)
	movq	16(%rbx), %rcx
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	movq	%rcx, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9EffectPhiEi@PLT
	movq	-120(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rcx
	movq	%rdx, -64(%rbp)
	movl	$3, %edx
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	32(%rbx), %rcx
	movzbl	48(%rbx), %esi
	movl	$2, %edx
	movq	%rax, 16(%rbx)
	movq	16(%r12), %rax
	movq	%rcx, -96(%rbp)
	movq	24(%rbx), %r12
	movq	8(%rax), %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r13, %xmm5
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-96(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r12, -64(%rbp)
	movl	$3, %edx
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 32(%rbx)
	movq	8(%rbx), %rax
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L288:
	movq	32(%rdi), %rax
	leaq	16(%rax,%r14), %r14
	movq	(%r14), %r8
	cmpq	%r8, %r13
	je	.L290
	movq	%rax, %rdi
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L276:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r13
	je	.L318
	leaq	24(%rsi), %r14
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L271:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L274
	leaq	24(%rsi), %r15
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L266:
	movq	32(%rsi), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %rdx
	je	.L269
	leaq	24(%rsi), %r15
	jmp	.L268
.L322:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17385:
	.size	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_, .-_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	.section	.text._ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS0_IjSA_EEEES0_ISt17_Rb_tree_iteratorISB_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS0_IjSA_EEEES0_ISt17_Rb_tree_iteratorISB_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS0_IjSA_EEEES0_ISt17_Rb_tree_iteratorISB_EbEDpOT_
	.type	_ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS0_IjSA_EEEES0_ISt17_Rb_tree_iteratorISB_EbEDpOT_, @function
_ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS0_IjSA_EEEES0_ISt17_Rb_tree_iteratorISB_EbEDpOT_:
.LFB17427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$71, %rax
	jbe	.L344
	leaq	72(%rbx), %rax
	movq	%rax, 16(%rdi)
.L326:
	movl	(%r12), %eax
	pxor	%xmm0, %xmm0
	leaq	16(%r13), %r15
	movl	%eax, 32(%rbx)
	movq	8(%r12), %rax
	movq	%rax, 40(%rbx)
	movdqu	16(%r12), %xmm1
	movq	32(%r12), %rax
	movups	%xmm0, 16(%r12)
	movq	$0, 32(%r12)
	movq	%rax, 64(%rbx)
	movups	%xmm1, 48(%rbx)
	movq	24(%r13), %r12
	testq	%r12, %r12
	je	.L327
	movl	32(%rbx), %r14d
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L345:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L329
.L346:
	movq	%rax, %r12
.L328:
	movl	32(%r12), %ecx
	cmpl	%ecx, %r14d
	jb	.L345
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L346
.L329:
	testb	%dl, %dl
	jne	.L347
	cmpl	%ecx, %r14d
	jbe	.L337
.L336:
	movl	$1, %edi
	cmpq	%r15, %r12
	jne	.L348
.L334:
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
	addq	$8, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	cmpq	%r12, 32(%r13)
	je	.L336
.L338:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpl	32(%rax), %r14d
	ja	.L336
	movq	%rax, %r12
.L337:
	addq	$8, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	xorl	%edi, %edi
	cmpl	32(%r12), %r14d
	setb	%dil
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%r15, %r12
	cmpq	32(%r13), %r15
	je	.L340
	movl	32(%rbx), %r14d
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L344:
	movl	$72, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L340:
	movl	$1, %edi
	jmp	.L334
	.cfi_endproc
.LFE17427:
	.size	_ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS0_IjSA_EEEES0_ISt17_Rb_tree_iteratorISB_EbEDpOT_, .-_ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS0_IjSA_EEEES0_ISt17_Rb_tree_iteratorISB_EbEDpOT_
	.section	.text._ZNSt11_Deque_baseIN2v88internal8compiler15MemoryOptimizer5TokenENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIN2v88internal8compiler15MemoryOptimizer5TokenENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIN2v88internal8compiler15MemoryOptimizer5TokenENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIN2v88internal8compiler15MemoryOptimizer5TokenENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIN2v88internal8compiler15MemoryOptimizer5TokenENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB18211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$5, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L363
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L351:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L352
	movq	%r15, %rbx
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L353:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L364
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L354:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L352
.L357:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L353
	cmpq	$31, 8(%rax)
	jbe	.L353
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L357
.L352:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$31, %r13d
	salq	$4, %r13
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	addq	%rax, %r13
	leaq	512(%rax), %rcx
	movq	%rax, 72(%r12)
	movq	%r13, 64(%r12)
	movq	%rcx, 80(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L363:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L351
	.cfi_endproc
.LFE18211:
	.size	_ZNSt11_Deque_baseIN2v88internal8compiler15MemoryOptimizer5TokenENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIN2v88internal8compiler15MemoryOptimizer5TokenENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZN2v88internal8compiler15MemoryOptimizerC2EPNS1_7JSGraphEPNS0_4ZoneENS0_24PoisoningMitigationLevelENS2_17AllocationFoldingEPKcPNS0_11TickCounterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizerC2EPNS1_7JSGraphEPNS0_4ZoneENS0_24PoisoningMitigationLevelENS2_17AllocationFoldingEPKcPNS0_11TickCounterE
	.type	_ZN2v88internal8compiler15MemoryOptimizerC2EPNS1_7JSGraphEPNS0_4ZoneENS0_24PoisoningMitigationLevelENS2_17AllocationFoldingEPKcPNS0_11TickCounterE, @function
_ZN2v88internal8compiler15MemoryOptimizerC2EPNS1_7JSGraphEPNS0_4ZoneENS0_24PoisoningMitigationLevelENS2_17AllocationFoldingEPKcPNS0_11TickCounterE:
.LFB15694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$248, %rsp
	movq	16(%rbp), %rax
	movq	%r9, -272(%rbp)
	movq	%rax, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rdi)
	movq	16(%rdx), %rax
	movq	24(%rdx), %rdx
	movq	%rsi, 8(%rdi)
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L384
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%r12)
.L367:
	movq	$0, (%rax)
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	-256(%rbp), %rdi
	movq	$2147483647, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rax, 16(%rbx)
	leaq	40(%rbx), %rax
	movq	%r12, 24(%rbx)
	movl	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	%rax, 56(%rbx)
	movq	%rax, 64(%rbx)
	movq	$0, 72(%rbx)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	%r12, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler15MemoryOptimizer5TokenENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	pxor	%xmm0, %xmm0
	je	.L385
	movdqa	-256(%rbp), %xmm5
	leaq	-160(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIN2v88internal8compiler15MemoryOptimizer5TokenENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-96(%rbp), %xmm5
	movq	-216(%rbp), %r8
	movq	-224(%rbp), %xmm3
	movq	-200(%rbp), %rdi
	movq	-192(%rbp), %xmm1
	movq	-184(%rbp), %rsi
	movaps	%xmm5, -192(%rbp)
	movq	%r8, %xmm5
	punpcklqdq	%xmm5, %xmm3
	movdqa	-128(%rbp), %xmm6
	movq	-208(%rbp), %xmm2
	movups	%xmm3, 112(%rbx)
	movq	%rdi, %xmm3
	movq	-168(%rbp), %r10
	movdqa	-112(%rbp), %xmm7
	punpcklqdq	%xmm3, %xmm2
	movq	-248(%rbp), %rax
	movq	-176(%rbp), %xmm0
	movaps	%xmm6, -224(%rbp)
	movups	%xmm2, 128(%rbx)
	movq	%rsi, %xmm2
	movq	-256(%rbp), %xmm4
	movq	-240(%rbp), %r11
	punpcklqdq	%xmm2, %xmm1
	movq	-232(%rbp), %r9
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm7, -208(%rbp)
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %rcx
	movq	%rax, %xmm7
	movups	%xmm1, 144(%rbx)
	movq	%r10, %xmm1
	punpcklqdq	%xmm7, %xmm4
	movq	%r11, 96(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movq	%rdx, -240(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%r9, 104(%rbx)
	movaps	%xmm6, -176(%rbp)
	movups	%xmm4, 80(%rbx)
	movups	%xmm0, 160(%rbx)
	testq	%rdx, %rdx
	je	.L369
	movq	-168(%rbp), %rsi
	leaq	8(%rsi), %rdi
	movq	-200(%rbp), %rsi
	cmpq	%rsi, %rdi
	jbe	.L370
	.p2align 4,,10
	.p2align 3
.L373:
	testq	%rax, %rax
	je	.L371
	cmpq	$32, 8(%rax)
	ja	.L372
.L371:
	movq	(%rsi), %rax
	movq	$32, 8(%rax)
	movq	-248(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -248(%rbp)
.L372:
	addq	$8, %rsi
	cmpq	%rsi, %rdi
	ja	.L373
	movq	-232(%rbp), %rcx
	movq	-240(%rbp), %rdx
.L370:
	leaq	0(,%rcx,8), %rax
	cmpq	$15, %rax
	jbe	.L369
	movq	%rcx, 8(%rdx)
	movq	$0, (%rdx)
.L369:
	movq	-272(%rbp), %xmm0
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %r8
	movq	%r12, 176(%rbx)
	leaq	184(%rbx), %rdi
	movq	%r15, %rsi
	movhps	-280(%rbp), %xmm0
	movaps	%xmm0, -272(%rbp)
	call	_ZN2v88internal8compiler14GraphAssemblerC1EPNS1_7JSGraphEPNS1_4NodeES6_PNS0_4ZoneE@PLT
	movl	%r14d, 224(%rbx)
	movdqa	-272(%rbp), %xmm0
	movl	%r13d, 228(%rbx)
	movups	%xmm0, 232(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L386
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	movdqa	-256(%rbp), %xmm6
	movdqa	-224(%rbp), %xmm7
	movq	$0, 96(%rbx)
	movq	-232(%rbp), %rax
	movdqa	-176(%rbp), %xmm1
	movups	%xmm6, 80(%rbx)
	movdqa	-208(%rbp), %xmm6
	movups	%xmm7, 112(%rbx)
	movdqa	-192(%rbp), %xmm7
	movq	%rax, 104(%rbx)
	movups	%xmm6, 128(%rbx)
	movups	%xmm7, 144(%rbx)
	movups	%xmm1, 160(%rbx)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L384:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L367
.L386:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15694:
	.size	_ZN2v88internal8compiler15MemoryOptimizerC2EPNS1_7JSGraphEPNS0_4ZoneENS0_24PoisoningMitigationLevelENS2_17AllocationFoldingEPKcPNS0_11TickCounterE, .-_ZN2v88internal8compiler15MemoryOptimizerC2EPNS1_7JSGraphEPNS0_4ZoneENS0_24PoisoningMitigationLevelENS2_17AllocationFoldingEPKcPNS0_11TickCounterE
	.globl	_ZN2v88internal8compiler15MemoryOptimizerC1EPNS1_7JSGraphEPNS0_4ZoneENS0_24PoisoningMitigationLevelENS2_17AllocationFoldingEPKcPNS0_11TickCounterE
	.set	_ZN2v88internal8compiler15MemoryOptimizerC1EPNS1_7JSGraphEPNS0_4ZoneENS0_24PoisoningMitigationLevelENS2_17AllocationFoldingEPKcPNS0_11TickCounterE,_ZN2v88internal8compiler15MemoryOptimizerC2EPNS1_7JSGraphEPNS0_4ZoneENS0_24PoisoningMitigationLevelENS2_17AllocationFoldingEPKcPNS0_11TickCounterE
	.section	.text._ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB18239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$6, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L401
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L389:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L390
	movq	%r15, %rbx
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L391:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L402
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L392:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L390
.L395:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L391
	cmpq	$63, 8(%rax)
	jbe	.L391
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L395
.L390:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$63, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,8), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L401:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L389
	.cfi_endproc
.LFE18239:
	.size	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	.type	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb, @function
_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb:
.LFB18267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	88(%rdi), %rax
	movq	56(%rdi), %rsi
	movq	24(%rdi), %rbx
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	1(%r14,%rcx), %r8
	leaq	(%r8,%r8), %rcx
	cmpq	%rcx, %rbx
	jbe	.L404
	subq	%r8, %rbx
	shrq	%rbx
	salq	$3, %rbx
	testb	%dl, %dl
	leaq	(%rbx,%r14,8), %rcx
	cmovne	%rcx, %rbx
	addq	$8, %rax
	addq	16(%rdi), %rbx
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	%rbx, %rsi
	jbe	.L406
	cmpq	%rax, %rsi
	je	.L407
	movq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L404:
	cmpq	%r14, %rbx
	movq	%r14, %rax
	movq	(%rdi), %rdi
	cmovnb	%rbx, %rax
	movq	16(%rdi), %r15
	leaq	2(%rbx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L414
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L409:
	movq	%rcx, %rax
	subq	%r8, %rax
	shrq	%rax
	salq	$3, %rax
	testb	%dl, %dl
	leaq	(%rax,%r14,8), %rsi
	cmovne	%rsi, %rax
	movq	56(%r12), %rsi
	leaq	(%r15,%rax), %rbx
	movq	88(%r12), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L411
	subq	%rsi, %rdx
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L411:
	movq	24(%r12), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L412
	movq	16(%r12), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L412:
	movq	%r15, 16(%r12)
	movq	%rcx, 24(%r12)
.L407:
	movq	%rbx, 56(%r12)
	movq	(%rbx), %rax
	movq	(%rbx), %xmm0
	addq	%r13, %rbx
	addq	$512, %rax
	movq	%rbx, 88(%r12)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%r12)
	movq	(%rbx), %rax
	movq	%rax, 72(%r12)
	addq	$512, %rax
	movq	%rax, 80(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	cmpq	%rax, %rsi
	je	.L407
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L414:
	movl	%edx, -68(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	movl	-68(%rbp), %edx
	movq	%rax, %r15
	jmp	.L409
	.cfi_endproc
.LFE18267:
	.size	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb, .-_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_120SearchAllocatingNodeEPNS1_4NodeES4_PNS0_4ZoneE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_120SearchAllocatingNodeEPNS1_4NodeES4_PNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_120SearchAllocatingNodeEPNS1_4NodeES4_PNS0_4ZoneE, @function
_ZN2v88internal8compiler12_GLOBAL__N_120SearchAllocatingNodeEPNS1_4NodeES4_PNS0_4ZoneE:
.LFB15722:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-256(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$344, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -360(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	je	.L513
	movdqa	-256(%rbp), %xmm5
	leaq	-160(%rbp), %rbx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-96(%rbp), %xmm5
	movq	-216(%rbp), %r9
	movq	-224(%rbp), %xmm3
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %xmm1
	movq	-184(%rbp), %rcx
	movaps	%xmm5, -192(%rbp)
	movq	%r9, %xmm5
	punpcklqdq	%xmm5, %xmm3
	movdqa	-128(%rbp), %xmm6
	movq	-208(%rbp), %xmm2
	movaps	%xmm3, -320(%rbp)
	movq	%r8, %xmm3
	movq	-168(%rbp), %rdx
	movdqa	-112(%rbp), %xmm7
	punpcklqdq	%xmm3, %xmm2
	movq	-248(%rbp), %rax
	movq	-176(%rbp), %xmm0
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm2, -304(%rbp)
	movq	%rcx, %xmm2
	movq	-256(%rbp), %xmm4
	movq	-240(%rbp), %r11
	punpcklqdq	%xmm2, %xmm1
	movq	-232(%rbp), %r10
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm7, -208(%rbp)
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	%rax, %xmm7
	movaps	%xmm1, -288(%rbp)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm7, %xmm4
	movq	%r11, -336(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -240(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%r10, -328(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm4, -352(%rbp)
	movaps	%xmm0, -272(%rbp)
	testq	%rsi, %rsi
	je	.L417
	movq	-168(%rbp), %rcx
	movq	-200(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L418
	.p2align 4,,10
	.p2align 3
.L421:
	testq	%rax, %rax
	je	.L419
	cmpq	$64, 8(%rax)
	ja	.L420
.L419:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -248(%rbp)
.L420:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L421
	movq	-232(%rbp), %rdi
	movq	-240(%rbp), %rsi
.L418:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L417
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L417:
	movq	%r14, -160(%rbp)
	movq	%rbx, %rdi
	leaq	-144(%rbp), %r14
	leaq	-360(%rbp), %rsi
	movl	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	%r14, -128(%rbp)
	movq	%r14, -120(%rbp)
	movq	$0, -112(%rbp)
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-272(%rbp), %rax
	movq	-288(%rbp), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L423
	movq	%r13, (%rdx)
	movq	-288(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -288(%rbp)
.L424:
	movq	-320(%rbp), %rax
	leaq	-352(%rbp), %r13
	cmpq	%rax, %rdx
	je	.L453
	.p2align 4,,10
	.p2align 3
.L431:
	movq	(%rax), %rdx
	movq	-304(%rbp), %rdi
	movq	%rdx, -256(%rbp)
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L433
	addq	$8, %rax
	movq	%rax, -320(%rbp)
.L434:
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L437
	movq	-256(%rbp), %rdx
	movq	%r14, %rcx
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L514:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L439
.L438:
	cmpq	%rdx, 32(%rax)
	jnb	.L514
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L438
.L439:
	cmpq	%r14, %rcx
	je	.L437
	cmpq	%rdx, 32(%rcx)
	jbe	.L509
.L437:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE16_M_insert_uniqueIRKS4_EESt4pairISt17_Rb_tree_iteratorIS4_EbEOT_
	movq	-256(%rbp), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_111CanAllocateEPKNS1_4NodeE
	testb	%al, %al
	jne	.L515
	movq	-256(%rbp), %rdi
	xorl	%r15d, %r15d
	movq	(%rdi), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jg	.L443
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L516:
	movq	%rcx, (%rdx)
	movq	-288(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -288(%rbp)
.L446:
	movq	-256(%rbp), %rdi
	addl	$1, %r15d
	movq	(%rdi), %rax
	cmpl	24(%rax), %r15d
	jge	.L444
.L443:
	movl	%r15d, %esi
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	-288(%rbp), %rdx
	movq	%rax, %rcx
	movq	-272(%rbp), %rax
	subq	$8, %rax
	cmpq	%rax, %rdx
	jne	.L516
	movq	-264(%rbp), %r8
	subq	-280(%rbp), %rdx
	sarq	$3, %rdx
	movq	%r8, %rax
	subq	-296(%rbp), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$6, %rax
	addq	%rax, %rdx
	movq	-304(%rbp), %rax
	subq	-320(%rbp), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L447
	movq	-328(%rbp), %rsi
	movq	%r8, %rax
	subq	-336(%rbp), %rax
	sarq	$3, %rax
	subq	%rax, %rsi
	cmpq	$1, %rsi
	jbe	.L517
.L448:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L449
	cmpq	$63, 8(%rax)
	ja	.L518
.L449:
	movq	-352(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L519
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L450:
	movq	%rax, 8(%r8)
	movq	-288(%rbp), %rax
	movq	%rcx, (%rax)
	movq	-264(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -264(%rbp)
	movq	8(%rax), %rdx
	leaq	512(%rdx), %rax
	movq	%rdx, -280(%rbp)
	movq	%rax, -272(%rbp)
	movq	%rdx, -288(%rbp)
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L509:
	movq	-288(%rbp), %rdx
.L444:
	movq	-320(%rbp), %rax
	cmpq	%rdx, %rax
	jne	.L431
.L453:
	xorl	%r13d, %r13d
.L432:
	movq	-136(%rbp), %r14
	testq	%r14, %r14
	je	.L454
.L457:
	movq	24(%r14), %r12
	testq	%r12, %r12
	je	.L455
.L456:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L456
.L455:
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L457
.L454:
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L415
	movq	-264(%rbp), %rbx
	movq	-296(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L459
	movq	-344(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L462:
	testq	%rax, %rax
	je	.L460
	cmpq	$64, 8(%rax)
	ja	.L461
.L460:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-344(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -344(%rbp)
.L461:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L462
	movq	-336(%rbp), %rax
.L459:
	movq	-328(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L415
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L415:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L520
	addq	$344, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L435
	cmpq	$64, 8(%rax)
	ja	.L436
.L435:
	movq	-312(%rbp), %rax
	movq	$64, 8(%rax)
	movq	-344(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -344(%rbp)
.L436:
	movq	-296(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -296(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -312(%rbp)
	movq	%rdx, -304(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L434
.L517:
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rcx, -368(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	movq	-264(%rbp), %r8
	movq	-368(%rbp), %rcx
	jmp	.L448
.L513:
	movdqa	-256(%rbp), %xmm6
	movdqa	-224(%rbp), %xmm7
	movq	$0, -336(%rbp)
	leaq	-160(%rbp), %rbx
	movq	-232(%rbp), %rax
	movdqa	-176(%rbp), %xmm1
	movaps	%xmm6, -352(%rbp)
	movdqa	-208(%rbp), %xmm6
	movaps	%xmm7, -320(%rbp)
	movdqa	-192(%rbp), %xmm7
	movq	%rax, -328(%rbp)
	movaps	%xmm6, -304(%rbp)
	movaps	%xmm7, -288(%rbp)
	movaps	%xmm1, -272(%rbp)
	jmp	.L417
.L518:
	movq	(%rax), %rdx
	movq	%rdx, -344(%rbp)
	jmp	.L450
.L423:
	movq	-264(%rbp), %r15
	movq	%r15, %rax
	subq	-296(%rbp), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$6, %rax
	movq	%rax, %rcx
	movq	%rdx, %rax
	subq	-280(%rbp), %rax
	movq	-304(%rbp), %rdx
	sarq	$3, %rax
	subq	-320(%rbp), %rdx
	addq	%rcx, %rax
	sarq	$3, %rdx
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L447
	movq	-328(%rbp), %rsi
	movq	%r15, %rax
	subq	-336(%rbp), %rax
	sarq	$3, %rax
	subq	%rax, %rsi
	cmpq	$1, %rsi
	jbe	.L521
.L426:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L427
	cmpq	$63, 8(%rax)
	ja	.L522
.L427:
	movq	-352(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L523
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L428:
	movq	%rax, 8(%r15)
	movq	-288(%rbp), %rax
	movq	%r13, (%rax)
	movq	-264(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -264(%rbp)
	movq	8(%rax), %rdx
	leaq	512(%rdx), %rax
	movq	%rdx, -280(%rbp)
	movq	%rax, -272(%rbp)
	movq	%rdx, -288(%rbp)
	jmp	.L424
.L519:
	movl	$512, %esi
	movq	%r8, -376(%rbp)
	movq	%rcx, -368(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-368(%rbp), %rcx
	movq	-376(%rbp), %r8
	jmp	.L450
.L521:
	leaq	-352(%rbp), %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIPN2v88internal8compiler4NodeENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	movq	-264(%rbp), %r15
	jmp	.L426
.L515:
	movq	-256(%rbp), %r13
	jmp	.L432
.L522:
	movq	(%rax), %rdx
	movq	%rdx, -344(%rbp)
	jmp	.L428
.L523:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L428
.L520:
	call	__stack_chk_fail@PLT
.L447:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE15722:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_120SearchAllocatingNodeEPNS1_4NodeES4_PNS0_4ZoneE, .-_ZN2v88internal8compiler12_GLOBAL__N_120SearchAllocatingNodeEPNS1_4NodeES4_PNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_124WriteBarrierAssertFailedEPNS1_4NodeES4_PKcPNS0_4ZoneE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"MemoryOptimizer could not remove write barrier for node #"
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_124WriteBarrierAssertFailedEPNS1_4NodeES4_PKcPNS0_4ZoneE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"\n"
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_124WriteBarrierAssertFailedEPNS1_4NodeES4_PKcPNS0_4ZoneE.str1.8
	.align 8
.LC4:
	.string	"  Run mksnapshot with --csa-trap-on-node="
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_124WriteBarrierAssertFailedEPNS1_4NodeES4_PKcPNS0_4ZoneE.str1.1
.LC5:
	.string	","
.LC6:
	.string	" to break in CSA code.\n"
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_124WriteBarrierAssertFailedEPNS1_4NodeES4_PKcPNS0_4ZoneE.str1.8
	.align 8
.LC7:
	.string	"\n  There is a potentially allocating node in between:\n"
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_124WriteBarrierAssertFailedEPNS1_4NodeES4_PKcPNS0_4ZoneE.str1.1
.LC8:
	.string	"    "
.LC9:
	.string	" to break there.\n"
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_124WriteBarrierAssertFailedEPNS1_4NodeES4_PKcPNS0_4ZoneE.str1.8
	.align 8
.LC10:
	.string	"  If this is a never-allocating runtime call, you can add an exception to Runtime::MayAllocate.\n"
	.align 8
.LC11:
	.string	"\n  It seems the store happened to something different than a direct allocation:\n"
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_124WriteBarrierAssertFailedEPNS1_4NodeES4_PKcPNS0_4ZoneE.str1.1
.LC12:
	.string	"%s"
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_124WriteBarrierAssertFailedEPNS1_4NodeES4_PKcPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_124WriteBarrierAssertFailedEPNS1_4NodeES4_PKcPNS0_4ZoneE, @function
_ZN2v88internal8compiler12_GLOBAL__N_124WriteBarrierAssertFailedEPNS1_4NodeES4_PKcPNS0_4ZoneE:
.LFB15760:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-448(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$456, %rsp
	movq	%rcx, -488(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$57, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	$41, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r15, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	(%rbx), %rax
	movq	%rbx, %rsi
	cmpw	$35, 16(%rax)
	je	.L542
.L525:
	cmpb	$0, 36(%rax)
	jne	.L543
.L526:
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rsi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	$4, %edx
	movq	%r12, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compilerlsERSoRKNS1_4NodeE@PLT
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	$41, %edx
	movq	%r12, %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%rbx), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
.L529:
	leaq	-480(%rbp), %rdi
	leaq	-424(%rbp), %rsi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	-480(%rbp), %rsi
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L543:
	movq	-488(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_120SearchAllocatingNodeEPNS1_4NodeES4_PNS0_4ZoneE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L526
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	$4, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_4NodeE@PLT
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	$41, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r13), %esi
	movq	%r14, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	0(%r13), %rax
	cmpw	$49, 16(%rax)
	jne	.L529
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	jmp	.L529
.L542:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L528
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L544:
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L526
.L528:
	movl	16(%rdx), %ecx
	movl	%ecx, %eax
	shrl	%eax
	andl	$1, %ecx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,8), %rsi
	movq	(%rsi), %rax
	jne	.L527
	movq	%rax, %rsi
	movq	(%rax), %rax
.L527:
	cmpw	$36, 16(%rax)
	jne	.L544
	jmp	.L525
	.cfi_endproc
.LFE15760:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_124WriteBarrierAssertFailedEPNS1_4NodeES4_PKcPNS0_4ZoneE, .-_ZN2v88internal8compiler12_GLOBAL__N_124WriteBarrierAssertFailedEPNS1_4NodeES4_PKcPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer23ComputeWriteBarrierKindEPNS1_4NodeES4_S4_PKNS2_15AllocationStateENS1_16WriteBarrierKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer23ComputeWriteBarrierKindEPNS1_4NodeES4_S4_PKNS2_15AllocationStateENS1_16WriteBarrierKindE
	.type	_ZN2v88internal8compiler15MemoryOptimizer23ComputeWriteBarrierKindEPNS1_4NodeES4_S4_PKNS2_15AllocationStateENS1_16WriteBarrierKindE, @function
_ZN2v88internal8compiler15MemoryOptimizer23ComputeWriteBarrierKindEPNS1_4NodeES4_S4_PKNS2_15AllocationStateENS1_16WriteBarrierKindE:
.LFB15761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$24, %rsp
	movq	(%r8), %r13
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	testq	%r13, %r13
	je	.L546
	cmpb	$0, 56(%r13)
	jne	.L546
	leaq	16(%r13), %r14
	movq	%rdx, %rdi
	.p2align 4,,10
	.p2align 3
.L554:
	movl	20(%rdi), %eax
	movq	24(%r13), %rdx
	andl	$16777215, %eax
	testq	%rdx, %rdx
	je	.L547
	movq	%r14, %rsi
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L587:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L549
.L548:
	cmpl	32(%rdx), %eax
	jbe	.L587
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L548
.L549:
	cmpq	%r14, %rsi
	je	.L547
	cmpl	32(%rsi), %eax
	jnb	.L562
.L547:
	movq	(%rdi), %rax
	movzwl	16(%rax), %eax
	cmpw	$440, %ax
	je	.L552
	ja	.L553
	cmpw	$306, %ax
	je	.L552
	cmpw	$325, %ax
	jne	.L546
.L552:
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %rdi
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L562:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L546:
	movq	(%r15), %rdi
	movq	8(%r12), %rax
	movzwl	16(%rdi), %edx
	movq	360(%rax), %r14
	cmpw	$443, %dx
	je	.L566
.L590:
	ja	.L556
	cmpw	$30, %dx
	jne	.L588
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	leaq	4856(%r14), %r8
	leaq	56(%r14), %rdx
	cmpq	%rax, %r8
	jbe	.L558
	cmpq	%rax, %rdx
	ja	.L558
	subq	%rdx, %rax
	shrq	$3, %rax
	cmpw	$573, %ax
	jbe	.L566
	.p2align 4,,10
	.p2align 3
.L558:
	cmpb	$1, %bl
	je	.L589
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L588:
	.cfi_restore_state
	xorl	%eax, %eax
	cmpw	$72, %dx
	jne	.L558
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L556:
	.cfi_restore_state
	cmpw	$467, %dx
	ja	.L559
	cmpw	$465, %dx
	jbe	.L558
	movq	%r15, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%rax, %r15
	movq	(%r15), %rdi
	movzwl	16(%rdi), %edx
	cmpw	$443, %dx
	jne	.L590
.L566:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L559:
	.cfi_restore_state
	xorl	%eax, %eax
	cmpw	$468, %dx
	jne	.L558
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L553:
	.cfi_restore_state
	cmpw	$442, %ax
	je	.L552
	jmp	.L546
.L589:
	movq	176(%r12), %rcx
	movq	232(%r12), %rdx
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_124WriteBarrierAssertFailedEPNS1_4NodeES4_PKcPNS0_4ZoneE
	.cfi_endproc
.LFE15761:
	.size	_ZN2v88internal8compiler15MemoryOptimizer23ComputeWriteBarrierKindEPNS1_4NodeES4_S4_PKNS2_15AllocationStateENS1_16WriteBarrierKindE, .-_ZN2v88internal8compiler15MemoryOptimizer23ComputeWriteBarrierKindEPNS1_4NodeES4_S4_PKNS2_15AllocationStateENS1_16WriteBarrierKindE
	.section	.rodata._ZN2v88internal8compiler15MemoryOptimizer12EnqueueMergeEPNS1_4NodeEiPKNS2_15AllocationStateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer12EnqueueMergeEPNS1_4NodeEiPKNS2_15AllocationStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer12EnqueueMergeEPNS1_4NodeEiPKNS2_15AllocationStateE
	.type	_ZN2v88internal8compiler15MemoryOptimizer12EnqueueMergeEPNS1_4NodeEiPKNS2_15AllocationStateE, @function
_ZN2v88internal8compiler15MemoryOptimizer12EnqueueMergeEPNS1_4NodeEiPKNS2_15AllocationStateE:
.LFB15763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	32(%rsi), %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movl	20(%rsi), %edx
	movq	%rcx, -104(%rbp)
	movl	%edx, %ebx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	shrl	$24, %ebx
	andl	$15, %ebx
	cmpl	$15, %ebx
	je	.L592
	subl	$1, %ebx
	movslq	%ebx, %rbx
	leaq	(%r12,%rbx,8), %rsi
.L593:
	movq	(%rsi), %rsi
	movq	(%rsi), %rsi
	cmpw	$1, 16(%rsi)
	je	.L643
	movq	48(%r15), %rax
	andl	$16777215, %edx
	leaq	40(%r15), %r8
	testq	%rax, %rax
	je	.L604
	movq	%r8, %r14
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L644:
	movq	%rax, %r14
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L606
.L605:
	cmpl	%edx, 32(%rax)
	jnb	.L644
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L605
.L606:
	cmpq	%r14, %r8
	je	.L604
	cmpl	%edx, 32(%r14)
	jbe	.L609
.L604:
	movq	176(%r15), %rax
	leaq	-96(%rbp), %rsi
	leaq	24(%r15), %rdi
	movq	%r8, -112(%rbp)
	movl	%edx, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	_ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE17_M_emplace_uniqueIJS0_IjSA_EEEES0_ISt17_Rb_tree_iteratorISB_EbEDpOT_
	movq	-112(%rbp), %r8
	movq	%rax, %r14
.L609:
	movq	56(%r14), %rdx
	cmpq	64(%r14), %rdx
	je	.L610
	movq	-104(%rbp), %rax
	movq	%rax, (%rdx)
	movq	56(%r14), %rax
	addq	$8, %rax
	movq	%rax, 56(%r14)
	subq	48(%r14), %rax
	sarq	$3, %rax
.L611:
	cmpq	%rax, %rbx
	je	.L645
.L591:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L646
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L643:
	.cfi_restore_state
	testl	%eax, %eax
	jne	.L591
	movq	176(%r15), %rax
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	xorl	%edx, %edx
	movq	%rax, %rbx
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L648:
	movq	32(%r13,%rdx), %rdi
.L642:
	movq	-112(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_120SearchAllocatingNodeEPNS1_4NodeES4_PNS0_4ZoneE
	movq	%r14, %rdx
	testq	%rax, %rax
	jne	.L647
.L603:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L598
	movq	32(%rbx), %rax
	movl	8(%rax), %eax
.L598:
	leal	1(%rdx), %esi
	cmpl	%eax, %esi
	jge	.L599
	movzbl	23(%r13), %eax
	leaq	1(%rdx), %r14
	leaq	0(,%r14,8), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L648
	movq	(%r12), %rax
	movq	16(%rax,%rdx), %rdi
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L592:
	movq	32(%rsi), %rsi
	movl	8(%rsi), %ecx
	leal	-1(%rcx), %ebx
	movslq	%ebx, %rbx
	leaq	16(%rsi,%rbx,8), %rsi
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L645:
	leaq	40(%r14), %rsi
	movq	%r15, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal8compiler15MemoryOptimizer11MergeStatesERKNS0_10ZoneVectorIPKNS2_15AllocationStateEEE
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
	movq	-104(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	subq	$1, 72(%r15)
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L610:
	movq	48(%r14), %r9
	movq	%rdx, %r10
	subq	%r9, %r10
	movq	%r10, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L649
	testq	%rax, %rax
	je	.L624
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L650
	movl	$2147483640, %esi
	movl	$2147483640, %r11d
.L613:
	movq	40(%r14), %rdi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rsi, %rax
	jb	.L651
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L616:
	leaq	(%rcx,%r11), %rsi
	leaq	8(%rcx), %rdi
	jmp	.L614
.L650:
	testq	%rcx, %rcx
	jne	.L652
	movl	$8, %edi
	xorl	%esi, %esi
	xorl	%ecx, %ecx
.L614:
	movq	-104(%rbp), %rax
	movq	%rax, (%rcx,%r10)
	cmpq	%r9, %rdx
	je	.L627
	leaq	-8(%rdx), %rax
	leaq	15(%rcx), %rdx
	subq	%r9, %rax
	subq	%r9, %rdx
	movq	%rax, %r10
	shrq	$3, %r10
	cmpq	$30, %rdx
	jbe	.L628
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %r10
	je	.L628
	addq	$1, %r10
	xorl	%edx, %edx
	movq	%r10, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L619:
	movdqu	(%r9,%rdx), %xmm1
	movups	%xmm1, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L619
	movq	%r10, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rdi
	addq	%rdi, %r9
	addq	%rcx, %rdi
	cmpq	%r10, %rdx
	je	.L621
	movq	(%r9), %rdx
	movq	%rdx, (%rdi)
.L621:
	addq	$16, %rax
	leaq	(%rcx,%rax), %rdi
	sarq	$3, %rax
.L617:
	movq	%rcx, %xmm0
	movq	%rdi, %xmm2
	movq	%rsi, 64(%r14)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 48(%r14)
	jmp	.L611
.L599:
	movq	-104(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
	jmp	.L591
.L647:
	movq	16(%r15), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
	jmp	.L591
.L624:
	movl	$8, %esi
	movl	$8, %r11d
	jmp	.L613
.L628:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L618:
	movq	(%r9,%rdx,8), %rdi
	movq	%rdi, (%rcx,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%r10, %rdi
	jne	.L618
	jmp	.L621
.L627:
	movl	$1, %eax
	jmp	.L617
.L651:
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r11, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %r8
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %r11
	movq	-136(%rbp), %r9
	movq	%rax, %rcx
	movq	-144(%rbp), %r10
	jmp	.L616
.L646:
	call	__stack_chk_fail@PLT
.L649:
	leaq	.LC13(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L652:
	movl	$268435455, %r11d
	cmpq	$268435455, %rcx
	cmova	%r11, %rcx
	leaq	0(,%rcx,8), %r11
	movq	%r11, %rsi
	jmp	.L613
	.cfi_endproc
.LFE15763:
	.size	_ZN2v88internal8compiler15MemoryOptimizer12EnqueueMergeEPNS1_4NodeEiPKNS2_15AllocationStateE, .-_ZN2v88internal8compiler15MemoryOptimizer12EnqueueMergeEPNS1_4NodeEiPKNS2_15AllocationStateE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer10EnqueueUseEPNS1_4NodeEiPKNS2_15AllocationStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer10EnqueueUseEPNS1_4NodeEiPKNS2_15AllocationStateE
	.type	_ZN2v88internal8compiler15MemoryOptimizer10EnqueueUseEPNS1_4NodeEiPKNS2_15AllocationStateE, @function
_ZN2v88internal8compiler15MemoryOptimizer10EnqueueUseEPNS1_4NodeEiPKNS2_15AllocationStateE:
.LFB15778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	cmpw	$36, 16(%rax)
	je	.L674
	movq	160(%rdi), %rbx
	movq	144(%rdi), %rax
	leaq	-16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L655
	movq	%rsi, %xmm0
	movq	%rcx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	addq	$16, 144(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer12EnqueueMergeEPNS1_4NodeEiPKNS2_15AllocationStateE
	.p2align 4,,10
	.p2align 3
.L655:
	.cfi_restore_state
	movq	168(%rdi), %rbx
	movq	136(%rdi), %rsi
	subq	152(%r12), %rax
	movq	%rbx, %r14
	sarq	$4, %rax
	subq	%rsi, %r14
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rdx
	salq	$5, %rdx
	addq	%rax, %rdx
	movq	128(%r12), %rax
	subq	112(%r12), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$134217727, %rax
	je	.L675
	movq	96(%r12), %r8
	movq	104(%r12), %rdx
	movq	%rbx, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L676
.L658:
	movq	88(%r12), %rax
	testq	%rax, %rax
	je	.L666
	cmpq	$31, 8(%rax)
	ja	.L677
.L666:
	movq	80(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L678
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L667:
	movq	%rax, 8(%rbx)
	movq	%r13, %xmm0
	movq	144(%r12), %rax
	movq	%rcx, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rax)
	movq	168(%r12), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 168(%r12)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 152(%r12)
	movq	%rdx, 160(%r12)
	movq	%rax, 144(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L676:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	jbe	.L659
	subq	%r15, %rdx
	addq	$8, %rbx
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%rbx, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L660
	cmpq	%rbx, %rsi
	je	.L661
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L661:
	movq	%r15, 136(%r12)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %rbx
	movq	(%r15), %xmm0
	movq	%rbx, 168(%r12)
	addq	$512, %rax
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 120(%r12)
	movq	(%rbx), %rax
	movq	%rax, 152(%r12)
	addq	$512, %rax
	movq	%rax, 160(%r12)
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L677:
	movq	(%rax), %rdx
	movq	%rdx, 88(%r12)
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L659:
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	80(%r12), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %rbx
	leaq	2(%rdx,%rax), %r8
	movq	24(%rdi), %rax
	leaq	0(,%r8,8), %rsi
	subq	%rbx, %rax
	cmpq	%rax, %rsi
	ja	.L679
	addq	%rbx, %rsi
	movq	%rsi, 16(%rdi)
.L663:
	movq	%r8, %rax
	movq	136(%r12), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rbx,%rax,8), %r15
	movq	168(%r12), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L664
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r8
.L664:
	movq	104(%r12), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L665
	movq	96(%r12), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L665:
	movq	%rbx, 96(%r12)
	movq	%r8, 104(%r12)
	jmp	.L661
.L660:
	cmpq	%rbx, %rsi
	je	.L661
	leaq	8(%r14), %rdi
	movq	%rcx, -56(%rbp)
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	movq	-56(%rbp), %rcx
	jmp	.L661
.L678:
	movl	$512, %esi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L667
.L679:
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	%rax, %rbx
	jmp	.L663
.L675:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE15778:
	.size	_ZN2v88internal8compiler15MemoryOptimizer10EnqueueUseEPNS1_4NodeEiPKNS2_15AllocationStateE, .-_ZN2v88internal8compiler15MemoryOptimizer10EnqueueUseEPNS1_4NodeEiPKNS2_15AllocationStateE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer16VisitAllocateRawEPNS1_4NodeEPKNS2_15AllocationStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer16VisitAllocateRawEPNS1_4NodeEPKNS2_15AllocationStateE
	.type	_ZN2v88internal8compiler15MemoryOptimizer16VisitAllocateRawEPNS1_4NodeEPKNS2_15AllocationStateE, @function
_ZN2v88internal8compiler15MemoryOptimizer16VisitAllocateRawEPNS1_4NodeEPKNS2_15AllocationStateE:
.LFB15742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -216(%rbp)
	movq	%rdx, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rsi), %rax
	movq	%rax, -232(%rbp)
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L681
	movq	%rsi, %rbx
	movq	40(%rsi), %rsi
	leaq	48(%rbx), %rax
.L682:
	movq	(%rax), %rdx
	leaq	184(%r12), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler5ResetEPNS1_4NodeES4_@PLT
	movq	-216(%rbp), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler20AllocateParametersOfEPKNS1_8OperatorE@PLT
	movq	%rax, %r13
	movzbl	8(%rax), %eax
	cmpb	$1, %al
	movb	%al, -224(%rbp)
	movq	-216(%rbp), %rax
	je	.L781
	movq	24(%rax), %rsi
	testq	%rsi, %rsi
	je	.L692
	movq	(%rsi), %r14
	.p2align 4,,10
	.p2align 3
.L700:
	movl	16(%rsi), %edx
	movl	%edx, %edi
	shrl	%edi
	andl	$1, %edx
	movl	%edi, %eax
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rsi,%rax,8), %rcx
	movq	(%rcx), %rax
	jne	.L693
	movq	%rax, %rcx
	movq	(%rax), %rax
.L693:
	cmpw	$245, 16(%rax)
	jne	.L698
	cmpl	$1, %edi
	je	.L694
.L698:
	testq	%r14, %r14
	je	.L692
	movq	%r14, %rsi
	movq	(%r14), %r14
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L692:
	cmpb	$0, -224(%rbp)
	movl	12(%r13), %eax
	jne	.L689
	movq	%rbx, %rdi
	cmpl	$1, %eax
	je	.L782
	call	_ZN2v88internal8compiler14GraphAssembler44AllocateRegularInYoungGenerationStubConstantEv@PLT
	movq	%rax, -240(%rbp)
.L702:
	movq	8(%r12), %rax
	movq	360(%rax), %rdi
	call	_ZN2v88internal17ExternalReference32new_space_allocation_top_addressEPNS0_7IsolateE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%rax, %r14
	movq	8(%r12), %rax
	movq	360(%rax), %rdi
	call	_ZN2v88internal17ExternalReference34new_space_allocation_limit_addressEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
.L743:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%rax, %r8
	movq	-232(%rbp), %rax
	movq	(%rax), %rdx
	movzwl	16(%rdx), %eax
	cmpl	$23, %eax
	je	.L783
	cmpl	$24, %eax
	je	.L784
.L707:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movb	$0, -112(%rbp)
	movq	%r8, -248(%rbp)
	movb	$0, -160(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$7, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-248(%rbp), %r8
	movl	$5, %esi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	-232(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movq	-248(%rbp), %r9
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, -256(%rbp)
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12UintLessThanEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rdx
	movq	%rax, %rsi
	movq	%r9, -248(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	cmpl	$1, 12(%r13)
	movq	-248(%rbp), %r9
	movq	-256(%rbp), %r8
	je	.L785
.L710:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r8, -248(%rbp)
	leaq	-112(%rbp), %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%rbx, %rdi
	movq	-248(%rbp), %r8
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler5StoreENS1_19StoreRepresentationEPNS1_4NodeES5_S5_@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	leaq	-208(%rbp), %r15
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler19BitcastWordToTaggedEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%r12), %rsi
	movdqa	-144(%rbp), %xmm1
	movb	$1, -160(%rbp)
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rax
	movups	%xmm1, 208(%r12)
	testq	%rsi, %rsi
	je	.L786
.L724:
	movq	%rdx, %xmm2
	leaq	-192(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-240(%rbp), %xmm0
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -176(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	2(%rdx,%rax), %edx
	movq	200(%r12), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, 208(%r12)
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-80(%rbp), %rax
	movdqa	-96(%rbp), %xmm3
	movb	$1, -112(%rbp)
	movq	176(%r12), %r14
	movups	%xmm3, 208(%r12)
	movq	%rax, -232(%rbp)
	movq	16(%r14), %r13
	movq	24(%r14), %rax
	subq	%r13, %rax
	cmpq	$71, %rax
	jbe	.L787
	leaq	72(%r13), %rax
	movq	%rax, 16(%r14)
.L726:
	leaq	16(%r13), %rax
	movq	%r14, 0(%r13)
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rax, 32(%r13)
	movq	%rax, 40(%r13)
	movzbl	-224(%rbp), %eax
	movl	$0, 16(%r13)
	movb	%al, 56(%r13)
	movq	-232(%rbp), %rax
	movq	$0, 24(%r13)
	movq	$0, 48(%r13)
	movl	20(%rax), %eax
	movq	$0, 64(%r13)
	movl	%eax, -224(%rbp)
	andl	$16777215, %eax
	movl	%eax, -208(%rbp)
	call	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_
	movq	176(%r12), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$23, %rax
	jbe	.L788
	leaq	24(%r14), %rax
	movq	%rax, 16(%rdi)
.L728:
	movq	%r14, -224(%rbp)
	movq	%r13, (%r14)
	movq	$2147483647, 8(%r14)
	movq	$0, 16(%r14)
.L719:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler20ExtractCurrentEffectEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler21ExtractCurrentControlEv@PLT
	movq	%rax, -240(%rbp)
	movq	-216(%rbp), %rax
	movq	24(%rax), %r15
	testq	%r15, %r15
	je	.L729
	movq	(%r15), %rbx
	.p2align 4,,10
	.p2align 3
.L741:
	movl	16(%r15), %ecx
	movq	%r15, %rdi
	movl	%ecx, %edx
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rax
	leaq	(%r15,%rax,8), %rax
	leaq	32(%rax), %rsi
	addq	$16, %rax
	andl	$1, %ecx
	cmovne	%rsi, %rax
	leaq	(%rax,%rdx,8), %r14
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L732
	movl	16(%r15), %eax
	movl	%eax, %edx
	shrl	%edx
	movl	%edx, %ecx
	leaq	3(%rcx,%rcx,2), %rcx
	leaq	(%r15,%rcx,8), %rsi
	testb	$1, %al
	jne	.L733
	movq	(%rsi), %rsi
.L733:
	movq	-224(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler15MemoryOptimizer10EnqueueUseEPNS1_4NodeEiPKNS2_15AllocationStateE
	movq	(%r14), %rdi
	cmpq	%rdi, %r13
	je	.L735
	testq	%rdi, %rdi
	je	.L736
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L736:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L735
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L735:
	testq	%rbx, %rbx
	je	.L729
	movq	%rbx, %r15
	movq	(%rbx), %rbx
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L729:
	movq	-216(%rbp), %rdi
	call	_ZN2v88internal8compiler4Node4KillEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L789
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L732:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler14NodeProperties11IsValueEdgeENS1_4EdgeE@PLT
	movq	(%r14), %rdi
	testb	%al, %al
	je	.L738
	cmpq	%rdi, -232(%rbp)
	je	.L735
	testq	%rdi, %rdi
	je	.L739
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L739:
	movq	-232(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, (%r14)
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L738:
	cmpq	%rdi, -240(%rbp)
	je	.L735
	testq	%rdi, %rdi
	je	.L740
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L740:
	movq	-240(%rbp), %rdi
	movq	%rdi, (%r14)
	testq	%rdi, %rdi
	je	.L735
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L694:
	movzbl	23(%rcx), %eax
	movq	32(%rcx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L697
	movq	16(%rdx), %rdx
.L697:
	movq	(%rdx), %rdi
	cmpw	$238, 16(%rdi)
	jne	.L698
	call	_ZN2v88internal8compiler16AllocationTypeOfEPKNS1_8OperatorE@PLT
	cmpb	$1, %al
	jne	.L698
	movb	$1, -224(%rbp)
	movl	12(%r13), %eax
	.p2align 4,,10
	.p2align 3
.L689:
	movq	%rbx, %rdi
	cmpl	$1, %eax
	je	.L790
.L703:
	call	_ZN2v88internal8compiler14GraphAssembler42AllocateRegularInOldGenerationStubConstantEv@PLT
	movq	%rax, -240(%rbp)
.L704:
	movq	8(%r12), %rax
	movq	360(%rax), %rdi
	call	_ZN2v88internal17ExternalReference32old_space_allocation_top_addressEPNS0_7IsolateE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%rax, %r14
	movq	8(%r12), %rax
	movq	360(%rax), %rdi
	call	_ZN2v88internal17ExternalReference34old_space_allocation_limit_addressEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L781:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L684
	movq	(%rdx), %r15
	.p2align 4,,10
	.p2align 3
.L691:
	movl	16(%rdx), %ecx
	movl	%ecx, %esi
	shrl	%esi
	andl	$1, %ecx
	movl	%esi, %eax
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,8), %rdx
	movq	(%rdx), %rax
	jne	.L685
	movq	%rax, %rdx
	movq	(%rax), %rax
.L685:
	cmpw	$245, 16(%rax)
	jne	.L686
	testl	%esi, %esi
	je	.L791
.L686:
	testq	%r15, %r15
	je	.L684
	movq	%r15, %rdx
	movq	(%r15), %r15
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L791:
	movzbl	23(%rdx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L687
	addq	$40, %rdx
.L688:
	movq	(%rdx), %r14
	movq	(%r14), %rdi
	cmpw	$238, 16(%rdi)
	jne	.L686
	call	_ZN2v88internal8compiler16AllocationTypeOfEPKNS1_8OperatorE@PLT
	testb	%al, %al
	jne	.L686
	movq	-216(%rbp), %rax
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movl	12(%r13), %eax
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L783:
	movslq	44(%rdx), %r11
.L706:
	cmpq	$131072, %r11
	ja	.L707
	cmpb	$0, _ZN2v88internal15FLAG_inline_newE(%rip)
	je	.L707
	movl	228(%r12), %eax
	testl	%eax, %eax
	jne	.L711
	movq	-248(%rbp), %rcx
	movl	$131072, %eax
	subq	%r11, %rax
	movq	8(%rcx), %rdx
	cmpq	%rdx, %rax
	jl	.L711
	movq	(%rcx), %r13
	movzbl	-224(%rbp), %eax
	cmpb	%al, 56(%r13)
	jne	.L711
	movq	8(%r12), %rax
	leaq	(%rdx,%r11), %r15
	movq	64(%r13), %rdx
	movq	16(%rax), %rcx
	movq	(%rdx), %rdx
	cmpb	$5, 16(%rcx)
	je	.L792
	movslq	44(%rdx), %rdx
	cmpq	%r15, %rdx
	jl	.L793
.L716:
	movq	-248(%rbp), %rax
	movq	-232(%rbp), %rdx
	movq	%rbx, %rdi
	movq	16(%rax), %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%rbx, %rdi
	movq	-240(%rbp), %r8
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler5StoreENS1_19StoreRepresentationEPNS1_4NodeES5_S5_@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movq	-248(%rbp), %rax
	movq	16(%rax), %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler19BitcastWordToTaggedEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	leaq	-208(%rbp), %rsi
	movq	%rax, -232(%rbp)
	movl	20(%rax), %eax
	movl	%eax, -224(%rbp)
	andl	$16777215, %eax
	movl	%eax, -208(%rbp)
	call	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_
	movq	176(%r12), %rdi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$23, %rax
	jbe	.L794
	leaq	24(%r14), %rax
	movq	%rax, 16(%rdi)
.L718:
	movq	-240(%rbp), %rcx
	movq	%r14, -224(%rbp)
	movq	%r13, (%r14)
	movq	%r15, 8(%r14)
	movq	%rcx, 16(%r14)
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L681:
	movq	-232(%rbp), %rbx
	leaq	16(%rbx), %rax
	movq	16(%rbx), %rbx
	movq	8(%rax), %rsi
	addq	$16, %rax
	movq	%rbx, -232(%rbp)
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L684:
	movl	12(%r13), %eax
	movq	%rbx, %rdi
	cmpl	$1, %eax
	jne	.L703
.L790:
	call	_ZN2v88internal8compiler14GraphAssembler35AllocateInOldGenerationStubConstantEv@PLT
	movq	%rax, -240(%rbp)
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L687:
	movq	32(%rdx), %rdx
	addq	$24, %rdx
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L784:
	movq	48(%rdx), %r11
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L782:
	call	_ZN2v88internal8compiler14GraphAssembler37AllocateInYoungGenerationStubConstantEv@PLT
	movq	%rax, -240(%rbp)
	jmp	.L702
.L711:
	movq	%r11, %rsi
	movq	%rbx, %rdi
	movq	%r11, -232(%rbp)
	movq	%r8, -256(%rbp)
	movb	$0, -160(%rbp)
	movl	$0, -156(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -112(%rbp)
	movl	$1, -108(%rbp)
	movq	$0, -104(%rbp)
	movb	$5, -64(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler20UniqueIntPtrConstantEl@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-256(%rbp), %r8
	movl	$5, %esi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_@PLT
	movq	-248(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	-112(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler12UintLessThanEPNS1_4NodeES4_@PLT
	leaq	-160(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	(%r12), %rsi
	movdqa	-144(%rbp), %xmm5
	movb	$1, -160(%rbp)
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rax
	testq	%rsi, %rsi
	movq	-232(%rbp), %r11
	movups	%xmm5, 208(%r12)
	je	.L713
	leaq	-208(%rbp), %r15
.L714:
	movq	%rdx, %xmm6
	leaq	-192(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-240(%rbp), %xmm0
	movq	%r11, -256(%rbp)
	movhps	-248(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	%rax, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -176(%rbp)
	movl	28(%rsi), %eax
	movl	24(%rsi), %edx
	leal	2(%rdx,%rax), %edx
	movq	200(%r12), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, %rdi
	movq	%rax, 208(%r12)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler19BitcastTaggedToWordEPNS1_4NodeE@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-232(%rbp), %r8
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler6IntSubEPNS1_4NodeES4_@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler10MergeStateIJPNS1_4NodeEEEEvPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-256(%rbp), %r11
	movq	%rbx, %rdi
	movb	$1, -112(%rbp)
	movdqa	-96(%rbp), %xmm7
	movq	%r11, %rsi
	movq	%r11, -240(%rbp)
	movups	%xmm7, 208(%r12)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	%r13, %r8
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%rax, %rcx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler5StoreENS1_19StoreRepresentationEPNS1_4NodeES5_S5_@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler6IntAddEPNS1_4NodeES4_@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14GraphAssembler19BitcastWordToTaggedEPNS1_4NodeE@PLT
	movq	176(%r12), %r14
	movq	-240(%rbp), %r11
	movq	%rax, -232(%rbp)
	movq	16(%r14), %rcx
	movq	24(%r14), %rax
	subq	%rcx, %rax
	cmpq	$71, %rax
	jbe	.L795
	leaq	72(%rcx), %rax
	movq	%rax, 16(%r14)
.L721:
	leaq	16(%rcx), %rax
	movq	%r14, (%rcx)
	movq	%rcx, %rdi
	movq	%r15, %rsi
	movq	%rax, 32(%rcx)
	movq	%rax, 40(%rcx)
	movzbl	-224(%rbp), %eax
	movl	$0, 16(%rcx)
	movb	%al, 56(%rcx)
	movq	-248(%rbp), %rax
	movq	$0, 24(%rcx)
	movq	%rax, 64(%rcx)
	movq	-232(%rbp), %rax
	movq	$0, 48(%rcx)
	movl	20(%rax), %eax
	movq	%r11, -240(%rbp)
	movq	%rcx, -224(%rbp)
	andl	$16777215, %eax
	movl	%eax, -208(%rbp)
	call	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE16_M_insert_uniqueIjEESt4pairISt17_Rb_tree_iteratorIjEbEOT_
	movq	176(%r12), %rdi
	movq	-224(%rbp), %rcx
	movq	-240(%rbp), %r11
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	$23, %rax
	jbe	.L796
	leaq	24(%r14), %rax
	movq	%rax, 16(%rdi)
.L723:
	movq	%r14, -224(%rbp)
	movq	%rcx, (%r14)
	movq	%r11, 8(%r14)
	movq	%r13, 16(%r14)
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L785:
	movl	$131072, %esi
	movq	%rbx, %rdi
	movq	%r8, -248(%rbp)
	movq	%r9, -256(%rbp)
	call	_ZN2v88internal8compiler14GraphAssembler14IntPtrConstantEl@PLT
	movq	-232(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler12UintLessThanEPNS1_4NodeES4_@PLT
	movq	-256(%rbp), %r9
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler14GraphAssembler9GotoIfNotIJEEEvPNS1_4NodeEPNS1_19GraphAssemblerLabelIXsZT_EEEDpT_
	movq	-248(%rbp), %r8
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L786:
	leaq	40+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	leaq	16+_ZTVN2v88internal18AllocateDescriptorE(%rip), %rcx
	xorl	%r9d, %r9d
	movq	%r15, %rsi
	movq	%rax, %xmm4
	movq	%rcx, %xmm0
	movq	8(%r12), %rax
	movl	48+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %edx
	punpcklqdq	%xmm4, %xmm0
	subl	40+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %edx
	movl	$4, %ecx
	movl	$32, %r8d
	movaps	%xmm0, -208(%rbp)
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%rax, %rsi
	movq	8(%r12), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	216(%r12), %rdx
	movq	%rax, (%r12)
	movq	%rax, %rsi
	movq	208(%r12), %rax
	jmp	.L724
.L787:
	movl	$72, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L726
.L788:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L728
.L713:
	leaq	40+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	leaq	16+_ZTVN2v88internal18AllocateDescriptorE(%rip), %rcx
	movl	48+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %edx
	xorl	%r9d, %r9d
	movq	%rax, %xmm5
	movq	%rcx, %xmm0
	movq	8(%r12), %rax
	leaq	-208(%rbp), %r15
	punpcklqdq	%xmm5, %xmm0
	subl	40+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %edx
	movl	$4, %ecx
	movq	%r15, %rsi
	movaps	%xmm0, -208(%rbp)
	movq	(%rax), %rax
	movl	$32, %r8d
	movq	%r11, -232(%rbp)
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler7Linkage21GetStubCallDescriptorEPNS0_4ZoneERKNS0_23CallInterfaceDescriptorEiNS_4base5FlagsINS1_14CallDescriptor4FlagEiEENS9_INS1_8Operator8PropertyEhEENS0_12StubCallModeE@PLT
	movq	%rax, %rsi
	movq	8(%r12), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	216(%r12), %rdx
	movq	-232(%rbp), %r11
	movq	%rax, (%r12)
	movq	%rax, %rsi
	movq	208(%r12), %rax
	jmp	.L714
.L796:
	movl	$24, %esi
	movq	%rcx, -240(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-224(%rbp), %r11
	movq	-240(%rbp), %rcx
	movq	%rax, %r14
	jmp	.L723
.L795:
	movl	$72, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-240(%rbp), %r11
	movq	%rax, %rcx
	jmp	.L721
.L792:
	cmpq	%r15, 48(%rdx)
	jge	.L716
	movq	8(%rax), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int64ConstantEl@PLT
	movq	64(%r13), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L716
.L793:
	movq	8(%rax), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	movq	64(%r13), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L716
.L794:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L718
.L789:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15742:
	.size	_ZN2v88internal8compiler15MemoryOptimizer16VisitAllocateRawEPNS1_4NodeEPKNS2_15AllocationStateE, .-_ZN2v88internal8compiler15MemoryOptimizer16VisitAllocateRawEPNS1_4NodeEPKNS2_15AllocationStateE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
	.type	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE, @function
_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE:
.LFB15777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rsi), %r12
	movq	%rdx, -56(%rbp)
	testq	%r12, %r12
	je	.L798
	movq	(%r12), %r13
	movq	%rdi, %r14
	movl	$1, %ebx
	.p2align 4,,10
	.p2align 3
.L819:
	movl	16(%r12), %ecx
	movq	%r12, %rdi
	movl	%ecx, %edx
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rax
	leaq	(%r12,%rax,8), %rax
	leaq	32(%rax), %rsi
	addq	$16, %rax
	andl	$1, %ecx
	cmovne	%rsi, %rax
	leaq	(%rax,%rdx,8), %rsi
	call	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L801
	movl	16(%r12), %edx
	movl	%edx, %r8d
	shrl	%r8d
	andl	$1, %edx
	movl	%r8d, %eax
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r12,%rax,8), %r12
	movq	(%r12), %rax
	jne	.L802
	movq	%rax, %r12
	movq	(%rax), %rax
.L802:
	cmpw	$36, 16(%rax)
	je	.L833
	movq	160(%r14), %rdi
	movq	144(%r14), %rax
	leaq	-16(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L804
	movq	%r12, %xmm0
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%rax)
	addq	$16, 144(%r14)
.L801:
	testq	%r13, %r13
	je	.L798
	movq	%r13, %r12
	movq	0(%r13), %r13
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L798:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L833:
	.cfi_restore_state
	movq	-56(%rbp), %rcx
	movl	%r8d, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler15MemoryOptimizer12EnqueueMergeEPNS1_4NodeEiPKNS2_15AllocationStateE
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L804:
	movq	168(%r14), %rcx
	movq	136(%r14), %rsi
	subq	152(%r14), %rax
	movq	%rcx, %r15
	sarq	$4, %rax
	subq	%rsi, %r15
	movq	%r15, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rdx
	salq	$5, %rdx
	addq	%rax, %rdx
	movq	128(%r14), %rax
	subq	112(%r14), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$134217727, %rax
	je	.L834
	movq	96(%r14), %r8
	movq	104(%r14), %rdx
	movq	%rcx, %rax
	subq	%r8, %rax
	movq	%rdx, %r11
	sarq	$3, %rax
	subq	%rax, %r11
	cmpq	$1, %r11
	jbe	.L835
.L807:
	movq	88(%r14), %rax
	testq	%rax, %rax
	je	.L815
	cmpq	$31, 8(%rax)
	ja	.L836
.L815:
	movq	80(%r14), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L837
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L816:
	movq	%rax, 8(%rcx)
	movq	%r12, %xmm0
	movq	144(%r14), %rax
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%rax)
	movq	168(%r14), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 168(%r14)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 152(%r14)
	movq	%rdx, 160(%r14)
	movq	%rax, 144(%r14)
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L835:
	leaq	2(%rdi), %r9
	leaq	(%r9,%r9), %rax
	cmpq	%rax, %rdx
	jbe	.L808
	subq	%r9, %rdx
	addq	$8, %rcx
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r8
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	cmpq	%r8, %rsi
	jbe	.L809
	cmpq	%rsi, %rcx
	je	.L810
	movq	%r8, %rdi
	call	memmove@PLT
	movq	%rax, %r8
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L836:
	movq	(%rax), %rdx
	movq	%rdx, 88(%r14)
	jmp	.L816
.L808:
	testq	%rdx, %rdx
	movq	%rbx, %rax
	movq	80(%r14), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %rcx
	leaq	2(%rdx,%rax), %r10
	movq	24(%rdi), %rax
	leaq	0(,%r10,8), %rsi
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L838
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L812:
	movq	%r10, %rax
	movq	136(%r14), %rsi
	subq	%r9, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r8
	movq	168(%r14), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L813
	movq	%r8, %rdi
	subq	%rsi, %rdx
	movq	%rcx, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %r10
	movq	%rax, %r8
.L813:
	movq	104(%r14), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L814
	movq	96(%r14), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L814:
	movq	%rcx, 96(%r14)
	movq	%r10, 104(%r14)
.L810:
	movq	%r8, 136(%r14)
	movq	(%r8), %rax
	leaq	(%r8,%r15), %rcx
	movq	(%r8), %xmm0
	movq	%rcx, 168(%r14)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 120(%r14)
	movq	(%rcx), %rax
	movq	%rax, 152(%r14)
	addq	$512, %rax
	movq	%rax, 160(%r14)
	jmp	.L807
.L809:
	cmpq	%rsi, %rcx
	je	.L810
	leaq	8(%r15), %rdi
	movq	%r8, -64(%rbp)
	subq	%rdx, %rdi
	addq	%r8, %rdi
	call	memmove@PLT
	movq	-64(%rbp), %r8
	jmp	.L810
.L837:
	movl	$512, %esi
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rcx
	jmp	.L816
.L838:
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r10
	movq	%rax, %rcx
	jmp	.L812
.L834:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE15777:
	.size	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE, .-_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer19VisitLoadFromObjectEPNS1_4NodeEPKNS2_15AllocationStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer19VisitLoadFromObjectEPNS1_4NodeEPKNS2_15AllocationStateE
	.type	_ZN2v88internal8compiler15MemoryOptimizer19VisitLoadFromObjectEPNS1_4NodeEPKNS2_15AllocationStateE, @function
_ZN2v88internal8compiler15MemoryOptimizer19VisitLoadFromObjectEPNS1_4NodeEPKNS2_15AllocationStateE:
.LFB15749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler14ObjectAccessOfEPKNS1_8OperatorE@PLT
	movq	8(%r13), %rdx
	movzwl	(%rax), %esi
	movq	16(%rdx), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder4LoadENS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
	.cfi_endproc
.LFE15749:
	.size	_ZN2v88internal8compiler15MemoryOptimizer19VisitLoadFromObjectEPNS1_4NodeEPKNS2_15AllocationStateE, .-_ZN2v88internal8compiler15MemoryOptimizer19VisitLoadFromObjectEPNS1_4NodeEPKNS2_15AllocationStateE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer18VisitStoreToObjectEPNS1_4NodeEPKNS2_15AllocationStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer18VisitStoreToObjectEPNS1_4NodeEPKNS2_15AllocationStateE
	.type	_ZN2v88internal8compiler15MemoryOptimizer18VisitStoreToObjectEPNS1_4NodeEPKNS2_15AllocationStateE, @function
_ZN2v88internal8compiler15MemoryOptimizer18VisitStoreToObjectEPNS1_4NodeEPKNS2_15AllocationStateE:
.LFB15750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler14ObjectAccessOfEPKNS1_8OperatorE@PLT
	movq	%rax, %rbx
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L842
	movq	32(%r12), %rdx
	leaq	48(%r12), %rax
.L843:
	movzbl	2(%rbx), %r9d
	movq	(%rax), %rcx
	movq	%r14, %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler15MemoryOptimizer23ComputeWriteBarrierKindEPNS1_4NodeES4_S4_PKNS2_15AllocationStateENS1_16WriteBarrierKindE
	movl	%eax, %edx
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	xorl	%eax, %eax
	movb	(%rbx), %al
	movb	%dl, %ah
	movl	%eax, %esi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder5StoreENS1_19StoreRepresentationE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	popq	%rbx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
	.p2align 4,,10
	.p2align 3
.L842:
	.cfi_restore_state
	movq	32(%r12), %rax
	movq	16(%rax), %rdx
	addq	$32, %rax
	jmp	.L843
	.cfi_endproc
.LFE15750:
	.size	_ZN2v88internal8compiler15MemoryOptimizer18VisitStoreToObjectEPNS1_4NodeEPKNS2_15AllocationStateE, .-_ZN2v88internal8compiler15MemoryOptimizer18VisitStoreToObjectEPNS1_4NodeEPKNS2_15AllocationStateE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer9VisitCallEPNS1_4NodeEPKNS2_15AllocationStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer9VisitCallEPNS1_4NodeEPKNS2_15AllocationStateE
	.type	_ZN2v88internal8compiler15MemoryOptimizer9VisitCallEPNS1_4NodeEPKNS2_15AllocationStateE, @function
_ZN2v88internal8compiler15MemoryOptimizer9VisitCallEPNS1_4NodeEPKNS2_15AllocationStateE:
.LFB15751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler16CallDescriptorOfEPKNS1_8OperatorE@PLT
	testb	$16, 64(%rax)
	jne	.L846
	movq	16(%r13), %r14
.L846:
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
	.cfi_endproc
.LFE15751:
	.size	_ZN2v88internal8compiler15MemoryOptimizer9VisitCallEPNS1_4NodeEPKNS2_15AllocationStateE, .-_ZN2v88internal8compiler15MemoryOptimizer9VisitCallEPNS1_4NodeEPKNS2_15AllocationStateE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer16VisitLoadElementEPNS1_4NodeEPKNS2_15AllocationStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer16VisitLoadElementEPNS1_4NodeEPKNS2_15AllocationStateE
	.type	_ZN2v88internal8compiler15MemoryOptimizer16VisitLoadElementEPNS1_4NodeEPKNS2_15AllocationStateE, @function
_ZN2v88internal8compiler15MemoryOptimizer16VisitLoadElementEPNS1_4NodeEPKNS2_15AllocationStateE:
.LFB15752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler15ElementAccessOfEPKNS1_8OperatorE@PLT
	leaq	32(%r12), %rcx
	movq	%rax, %rbx
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L849
	leaq	40(%r12), %rax
.L850:
	movq	(%rax), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal8compiler15MemoryOptimizer12ComputeIndexERKNS1_13ElementAccessEPNS1_4NodeE
	movq	%rax, %r14
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L851
	movq	-56(%rbp), %rcx
	movq	8(%rcx), %rdi
	cmpq	%rdi, %r14
	je	.L854
	addq	$8, %rcx
	movq	%r12, %rsi
.L853:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L855
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rsi
.L855:
	movq	%r14, (%rcx)
	testq	%r14, %r14
	je	.L854
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L854:
	movl	20(%rbx), %edx
	movzwl	16(%rbx), %esi
	cmpl	$2, %edx
	je	.L856
	movl	224(%r13), %eax
	cmpl	$1, %eax
	je	.L856
	cmpl	$2, %eax
	je	.L857
	testl	%eax, %eax
	je	.L858
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L857:
	testl	%edx, %edx
	jne	.L856
.L858:
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder12PoisonedLoadENS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L860:
	addq	$24, %rsp
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
	.p2align 4,,10
	.p2align 3
.L856:
	.cfi_restore_state
	movq	8(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder4LoadENS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L851:
	movq	32(%r12), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r14
	je	.L854
	leaq	24(%rsi), %rcx
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L849:
	movq	32(%r12), %rax
	addq	$24, %rax
	jmp	.L850
	.cfi_endproc
.LFE15752:
	.size	_ZN2v88internal8compiler15MemoryOptimizer16VisitLoadElementEPNS1_4NodeEPKNS2_15AllocationStateE, .-_ZN2v88internal8compiler15MemoryOptimizer16VisitLoadElementEPNS1_4NodeEPKNS2_15AllocationStateE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer14VisitLoadFieldEPNS1_4NodeEPKNS2_15AllocationStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer14VisitLoadFieldEPNS1_4NodeEPKNS2_15AllocationStateE
	.type	_ZN2v88internal8compiler15MemoryOptimizer14VisitLoadFieldEPNS1_4NodeEPKNS2_15AllocationStateE, @function
_ZN2v88internal8compiler15MemoryOptimizer14VisitLoadFieldEPNS1_4NodeEPKNS2_15AllocationStateE:
.LFB15753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler13FieldAccessOfEPKNS1_8OperatorE@PLT
	movq	8(%r12), %rdi
	movq	%rax, %rbx
	cmpb	$1, (%rax)
	sete	%al
	movl	4(%rbx), %esi
	movzbl	%al, %eax
	subl	%eax, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler12MachineGraph14IntPtrConstantEl@PLT
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	8(%r12), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movl	36(%rbx), %edx
	movzwl	32(%rbx), %esi
	cmpl	$2, %edx
	je	.L882
	movl	224(%r12), %eax
	cmpl	$1, %eax
	je	.L882
	cmpl	$2, %eax
	je	.L883
	testl	%eax, %eax
	je	.L884
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L883:
	testl	%edx, %edx
	jne	.L882
.L884:
	movq	8(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder12PoisonedLoadENS0_11MachineTypeE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L886:
	popq	%rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
	.p2align 4,,10
	.p2align 3
.L882:
	.cfi_restore_state
	movq	8(%r12), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder4LoadENS0_11MachineTypeE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L886
	.cfi_endproc
.LFE15753:
	.size	_ZN2v88internal8compiler15MemoryOptimizer14VisitLoadFieldEPNS1_4NodeEPKNS2_15AllocationStateE, .-_ZN2v88internal8compiler15MemoryOptimizer14VisitLoadFieldEPNS1_4NodeEPKNS2_15AllocationStateE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer17VisitStoreElementEPNS1_4NodeEPKNS2_15AllocationStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer17VisitStoreElementEPNS1_4NodeEPKNS2_15AllocationStateE
	.type	_ZN2v88internal8compiler15MemoryOptimizer17VisitStoreElementEPNS1_4NodeEPKNS2_15AllocationStateE, @function
_ZN2v88internal8compiler15MemoryOptimizer17VisitStoreElementEPNS1_4NodeEPKNS2_15AllocationStateE:
.LFB15754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	leaq	32(%r12), %r15
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler15ElementAccessOfEPKNS1_8OperatorE@PLT
	movq	%rax, %rbx
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L902
	movq	32(%r12), %rdx
	movq	8(%r15), %r11
	leaq	48(%r12), %rax
.L903:
	movzbl	18(%rbx), %r9d
	movq	(%rax), %rcx
	movq	%r14, %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r11, -64(%rbp)
	call	_ZN2v88internal8compiler15MemoryOptimizer23ComputeWriteBarrierKindEPNS1_4NodeES4_S4_PKNS2_15AllocationStateENS1_16WriteBarrierKindE
	movq	-64(%rbp), %r11
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movb	%al, -50(%rbp)
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler15MemoryOptimizer12ComputeIndexERKNS1_13ElementAccessEPNS1_4NodeE
	movq	%rax, %r8
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L904
	movq	8(%r15), %rdi
	cmpq	%rdi, %r8
	je	.L907
	leaq	8(%r15), %r10
	movq	%r12, %rsi
.L906:
	subq	$48, %rsi
	testq	%rdi, %rdi
	je	.L908
	movq	%r10, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rsi
.L908:
	movq	%r8, (%r10)
	testq	%r8, %r8
	je	.L907
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L907:
	movq	8(%r13), %rax
	movzwl	-50(%rbp), %ecx
	movq	16(%rax), %rdi
	xorl	%eax, %eax
	movb	16(%rbx), %al
	movb	%cl, %ah
	movl	%eax, %esi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder5StoreENS1_19StoreRepresentationE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$40, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
	.p2align 4,,10
	.p2align 3
.L904:
	.cfi_restore_state
	movq	32(%r12), %rsi
	movq	24(%rsi), %rdi
	cmpq	%rdi, %r8
	je	.L907
	leaq	24(%rsi), %r10
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L902:
	movq	32(%r12), %rax
	movq	16(%rax), %rdx
	movq	24(%rax), %r11
	addq	$32, %rax
	jmp	.L903
	.cfi_endproc
.LFE15754:
	.size	_ZN2v88internal8compiler15MemoryOptimizer17VisitStoreElementEPNS1_4NodeEPKNS2_15AllocationStateE, .-_ZN2v88internal8compiler15MemoryOptimizer17VisitStoreElementEPNS1_4NodeEPKNS2_15AllocationStateE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer15VisitStoreFieldEPNS1_4NodeEPKNS2_15AllocationStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer15VisitStoreFieldEPNS1_4NodeEPKNS2_15AllocationStateE
	.type	_ZN2v88internal8compiler15MemoryOptimizer15VisitStoreFieldEPNS1_4NodeEPKNS2_15AllocationStateE, @function
_ZN2v88internal8compiler15MemoryOptimizer15VisitStoreFieldEPNS1_4NodeEPKNS2_15AllocationStateE:
.LFB15755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler13FieldAccessOfEPKNS1_8OperatorE@PLT
	movq	%rax, %rbx
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L917
	movq	32(%r12), %rdx
	leaq	40(%r12), %rax
.L918:
	movzbl	34(%rbx), %r9d
	movq	(%rax), %rcx
	movq	%r15, %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler15MemoryOptimizer23ComputeWriteBarrierKindEPNS1_4NodeES4_S4_PKNS2_15AllocationStateENS1_16WriteBarrierKindE
	movl	4(%rbx), %esi
	movq	8(%r13), %rdi
	movl	%eax, %r14d
	xorl	%eax, %eax
	cmpb	$1, (%rbx)
	sete	%al
	subl	%eax, %esi
	movslq	%esi, %rsi
	call	_ZN2v88internal8compiler12MachineGraph14IntPtrConstantEl@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	8(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	8(%r13), %rax
	movl	%r14d, %ecx
	movq	16(%rax), %rdi
	xorl	%eax, %eax
	movb	32(%rbx), %al
	movb	%cl, %ah
	movl	%eax, %esi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder5StoreENS1_19StoreRepresentationE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$8, %rsp
	movq	%r15, %rdx
	movq	%r12, %rsi
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
	.p2align 4,,10
	.p2align 3
.L917:
	.cfi_restore_state
	movq	32(%r12), %rax
	movq	16(%rax), %rdx
	addq	$24, %rax
	jmp	.L918
	.cfi_endproc
.LFE15755:
	.size	_ZN2v88internal8compiler15MemoryOptimizer15VisitStoreFieldEPNS1_4NodeEPKNS2_15AllocationStateE, .-_ZN2v88internal8compiler15MemoryOptimizer15VisitStoreFieldEPNS1_4NodeEPKNS2_15AllocationStateE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer10VisitStoreEPNS1_4NodeEPKNS2_15AllocationStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer10VisitStoreEPNS1_4NodeEPKNS2_15AllocationStateE
	.type	_ZN2v88internal8compiler15MemoryOptimizer10VisitStoreEPNS1_4NodeEPKNS2_15AllocationStateE, @function
_ZN2v88internal8compiler15MemoryOptimizer10VisitStoreEPNS1_4NodeEPKNS2_15AllocationStateE:
.LFB15756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler21StoreRepresentationOfEPKNS1_8OperatorE@PLT
	movzbl	(%rax), %ebx
	movzbl	1(%rax), %r15d
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L921
	movq	32(%r12), %rdx
	leaq	48(%r12), %rax
.L922:
	movq	(%rax), %rcx
	movzbl	%r15b, %r9d
	movq	%r14, %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler15MemoryOptimizer23ComputeWriteBarrierKindEPNS1_4NodeES4_S4_PKNS2_15AllocationStateENS1_16WriteBarrierKindE
	cmpb	%r15b, %al
	je	.L923
	movq	8(%r13), %rdx
	movq	16(%rdx), %rdi
	xorl	%edx, %edx
	movb	%bl, %dl
	movb	%al, %dh
	movl	%edx, %esi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder5StoreENS1_19StoreRepresentationE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L923:
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
	.p2align 4,,10
	.p2align 3
.L921:
	.cfi_restore_state
	movq	32(%r12), %rax
	movq	16(%rax), %rdx
	addq	$32, %rax
	jmp	.L922
	.cfi_endproc
.LFE15756:
	.size	_ZN2v88internal8compiler15MemoryOptimizer10VisitStoreEPNS1_4NodeEPKNS2_15AllocationStateE, .-_ZN2v88internal8compiler15MemoryOptimizer10VisitStoreEPNS1_4NodeEPKNS2_15AllocationStateE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer16VisitOtherEffectEPNS1_4NodeEPKNS2_15AllocationStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer16VisitOtherEffectEPNS1_4NodeEPKNS2_15AllocationStateE
	.type	_ZN2v88internal8compiler15MemoryOptimizer16VisitOtherEffectEPNS1_4NodeEPKNS2_15AllocationStateE, @function
_ZN2v88internal8compiler15MemoryOptimizer16VisitOtherEffectEPNS1_4NodeEPKNS2_15AllocationStateE:
.LFB15757:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
	.cfi_endproc
.LFE15757:
	.size	_ZN2v88internal8compiler15MemoryOptimizer16VisitOtherEffectEPNS1_4NodeEPKNS2_15AllocationStateE, .-_ZN2v88internal8compiler15MemoryOptimizer16VisitOtherEffectEPNS1_4NodeEPKNS2_15AllocationStateE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer9VisitNodeEPNS1_4NodeEPKNS2_15AllocationStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer9VisitNodeEPNS1_4NodeEPKNS2_15AllocationStateE
	.type	_ZN2v88internal8compiler15MemoryOptimizer9VisitNodeEPNS1_4NodeEPKNS2_15AllocationStateE, @function
_ZN2v88internal8compiler15MemoryOptimizer9VisitNodeEPNS1_4NodeEPKNS2_15AllocationStateE:
.LFB15740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	240(%rdi), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	(%r12), %rdi
	movzwl	16(%rdi), %eax
	cmpw	$248, %ax
	ja	.L927
	cmpw	$236, %ax
	jbe	.L945
	subw	$237, %ax
	cmpw	$11, %ax
	ja	.L930
	leaq	.L932(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler15MemoryOptimizer9VisitNodeEPNS1_4NodeEPKNS2_15AllocationStateE,"a",@progbits
	.align 4
	.align 4
.L932:
	.long	.L939-.L932
	.long	.L938-.L932
	.long	.L930-.L932
	.long	.L937-.L932
	.long	.L936-.L932
	.long	.L930-.L932
	.long	.L935-.L932
	.long	.L930-.L932
	.long	.L934-.L932
	.long	.L933-.L932
	.long	.L930-.L932
	.long	.L931-.L932
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer9VisitNodeEPNS1_4NodeEPKNS2_15AllocationStateE
	.p2align 4,,10
	.p2align 3
.L927:
	cmpw	$431, %ax
	jne	.L930
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer10VisitStoreEPNS1_4NodeEPKNS2_15AllocationStateE
	.p2align 4,,10
	.p2align 3
.L945:
	.cfi_restore_state
	cmpw	$49, %ax
	jne	.L930
	call	_ZN2v88internal8compiler16CallDescriptorOfEPKNS1_8OperatorE@PLT
	testb	$16, 64(%rax)
	jne	.L944
	movq	16(%r13), %r14
.L944:
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
.L931:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer18VisitStoreToObjectEPNS1_4NodeEPKNS2_15AllocationStateE
.L930:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_111CanAllocateEPKNS1_4NodeE
	testb	%al, %al
	je	.L944
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L938:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer16VisitAllocateRawEPNS1_4NodeEPKNS2_15AllocationStateE
.L937:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer14VisitLoadFieldEPNS1_4NodeEPKNS2_15AllocationStateE
.L936:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer16VisitLoadElementEPNS1_4NodeEPKNS2_15AllocationStateE
.L935:
	.cfi_restore_state
	call	_ZN2v88internal8compiler14ObjectAccessOfEPKNS1_8OperatorE@PLT
	movq	8(%r13), %rdx
	movzwl	(%rax), %esi
	movq	16(%rdx), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder4LoadENS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L944
.L934:
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer15VisitStoreFieldEPNS1_4NodeEPKNS2_15AllocationStateE
.L933:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler15MemoryOptimizer17VisitStoreElementEPNS1_4NodeEPKNS2_15AllocationStateE
.L939:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE15740:
	.size	_ZN2v88internal8compiler15MemoryOptimizer9VisitNodeEPNS1_4NodeEPKNS2_15AllocationStateE, .-_ZN2v88internal8compiler15MemoryOptimizer9VisitNodeEPNS1_4NodeEPKNS2_15AllocationStateE
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer8OptimizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15MemoryOptimizer8OptimizeEv
	.type	_ZN2v88internal8compiler15MemoryOptimizer8OptimizeEv, @function
_ZN2v88internal8compiler15MemoryOptimizer8OptimizeEv:
.LFB15696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	.L958(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %rax
	movq	%rdi, %rbx
	movq	16(%rdi), %rdx
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	call	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
	movq	112(%rbx), %rax
	cmpq	%rax, 144(%rbx)
	je	.L946
	.p2align 4,,10
	.p2align 3
.L947:
	movq	128(%rbx), %rcx
	movq	(%rax), %r14
	movq	8(%rax), %r13
	leaq	-16(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L949
	addq	$16, %rax
	movq	%rax, 112(%rbx)
.L950:
	movq	240(%rbx), %rdi
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	(%r14), %rdi
	movzwl	16(%rdi), %eax
	cmpw	$248, %ax
	ja	.L953
	cmpw	$236, %ax
	jbe	.L976
	subw	$237, %ax
	cmpw	$11, %ax
	ja	.L956
	movzwl	%ax, %eax
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler15MemoryOptimizer8OptimizeEv,"a",@progbits
	.align 4
	.align 4
.L958:
	.long	.L965-.L958
	.long	.L964-.L958
	.long	.L956-.L958
	.long	.L963-.L958
	.long	.L962-.L958
	.long	.L956-.L958
	.long	.L961-.L958
	.long	.L956-.L958
	.long	.L960-.L958
	.long	.L959-.L958
	.long	.L956-.L958
	.long	.L957-.L958
	.section	.text._ZN2v88internal8compiler15MemoryOptimizer8OptimizeEv
.L957:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler15MemoryOptimizer18VisitStoreToObjectEPNS1_4NodeEPKNS2_15AllocationStateE
	.p2align 4,,10
	.p2align 3
.L967:
	movq	112(%rbx), %rax
	cmpq	%rax, 144(%rbx)
	jne	.L947
.L946:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L959:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler15MemoryOptimizer17VisitStoreElementEPNS1_4NodeEPKNS2_15AllocationStateE
	jmp	.L967
.L960:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler15MemoryOptimizer15VisitStoreFieldEPNS1_4NodeEPKNS2_15AllocationStateE
	jmp	.L967
.L961:
	call	_ZN2v88internal8compiler14ObjectAccessOfEPKNS1_8OperatorE@PLT
	movq	8(%rbx), %rdx
	movzwl	(%rax), %esi
	movq	16(%rdx), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder4LoadENS0_11MachineTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L975:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler15MemoryOptimizer11EnqueueUsesEPNS1_4NodeEPKNS2_15AllocationStateE
	jmp	.L967
.L962:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler15MemoryOptimizer16VisitLoadElementEPNS1_4NodeEPKNS2_15AllocationStateE
	jmp	.L967
.L963:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler15MemoryOptimizer14VisitLoadFieldEPNS1_4NodeEPKNS2_15AllocationStateE
	jmp	.L967
.L964:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler15MemoryOptimizer16VisitAllocateRawEPNS1_4NodeEPKNS2_15AllocationStateE
	jmp	.L967
.L956:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_111CanAllocateEPKNS1_4NodeE
	testb	%al, %al
	jne	.L967
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L976:
	cmpw	$49, %ax
	jne	.L956
	call	_ZN2v88internal8compiler16CallDescriptorOfEPKNS1_8OperatorE@PLT
	testb	$16, 64(%rax)
	jne	.L975
	movq	16(%rbx), %r13
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L953:
	cmpw	$431, %ax
	jne	.L956
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler15MemoryOptimizer10VisitStoreEPNS1_4NodeEPKNS2_15AllocationStateE
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L949:
	movq	88(%rbx), %rax
	testq	%rax, %rax
	je	.L951
	cmpq	$32, 8(%rax)
	ja	.L952
.L951:
	movq	120(%rbx), %rax
	movq	$32, 8(%rax)
	movq	88(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 88(%rbx)
.L952:
	movq	136(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 136(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 120(%rbx)
	movq	%rdx, 128(%rbx)
	movq	%rax, 112(%rbx)
	jmp	.L950
.L965:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE15696:
	.size	_ZN2v88internal8compiler15MemoryOptimizer8OptimizeEv, .-_ZN2v88internal8compiler15MemoryOptimizer8OptimizeEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler15MemoryOptimizerC2EPNS1_7JSGraphEPNS0_4ZoneENS0_24PoisoningMitigationLevelENS2_17AllocationFoldingEPKcPNS0_11TickCounterE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler15MemoryOptimizerC2EPNS1_7JSGraphEPNS0_4ZoneENS0_24PoisoningMitigationLevelENS2_17AllocationFoldingEPKcPNS0_11TickCounterE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler15MemoryOptimizerC2EPNS1_7JSGraphEPNS0_4ZoneENS0_24PoisoningMitigationLevelENS2_17AllocationFoldingEPKcPNS0_11TickCounterE:
.LFB19321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE19321:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler15MemoryOptimizerC2EPNS1_7JSGraphEPNS0_4ZoneENS0_24PoisoningMitigationLevelENS2_17AllocationFoldingEPKcPNS0_11TickCounterE, .-_GLOBAL__sub_I__ZN2v88internal8compiler15MemoryOptimizerC2EPNS1_7JSGraphEPNS0_4ZoneENS0_24PoisoningMitigationLevelENS2_17AllocationFoldingEPKcPNS0_11TickCounterE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler15MemoryOptimizerC2EPNS1_7JSGraphEPNS0_4ZoneENS0_24PoisoningMitigationLevelENS2_17AllocationFoldingEPKcPNS0_11TickCounterE
	.section	.rodata.CSWTCH.227,"a"
	.align 32
	.type	CSWTCH.227, @object
	.size	CSWTCH.227, 56
CSWTCH.227:
	.long	0
	.long	0
	.long	1
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	2
	.long	3
	.long	4
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
