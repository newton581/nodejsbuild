	.file	"node.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1527:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1527:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB11746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE11746:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB11807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11807:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB11747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11747:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB11808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE11808:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal8compiler4Node15OutOfLineInputs3NewEPNS0_4ZoneEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node15OutOfLineInputs3NewEPNS0_4ZoneEi
	.type	_ZN2v88internal8compiler4Node15OutOfLineInputs3NewEPNS0_4ZoneEi, @function
_ZN2v88internal8compiler4Node15OutOfLineInputs3NewEPNS0_4ZoneEi:
.LFB10133:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subq	%rax, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movslq	%esi, %r12
	movq	%r12, %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%r12, %rbx
	salq	$5, %rsi
	addq	$16, %rsi
	cmpq	%rdx, %rsi
	ja	.L15
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L13:
	leaq	(%r12,%r12,2), %rdx
	leaq	(%rax,%rdx,8), %rax
	movl	%ebx, 12(%rax)
	movl	$0, 8(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L13
	.cfi_endproc
.LFE10133:
	.size	_ZN2v88internal8compiler4Node15OutOfLineInputs3NewEPNS0_4ZoneEi, .-_ZN2v88internal8compiler4Node15OutOfLineInputs3NewEPNS0_4ZoneEi
	.section	.text._ZN2v88internal8compiler4Node15OutOfLineInputs11ExtractFromEPNS2_3UseEPPS2_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node15OutOfLineInputs11ExtractFromEPNS2_3UseEPPS2_i
	.type	_ZN2v88internal8compiler4Node15OutOfLineInputs11ExtractFromEPNS2_3UseEPPS2_i, @function
_ZN2v88internal8compiler4Node15OutOfLineInputs11ExtractFromEPNS2_3UseEPPS2_i:
.LFB10134:
	.cfi_startproc
	endbr64
	testl	%ecx, %ecx
	jg	.L36
	movl	%ecx, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-24(%rdi), %r8
	leal	-1(%rcx), %r11d
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L38:
	movq	$0, (%rdx,%rax,8)
	movq	8(%rsi), %r10
	testq	%r10, %r10
	je	.L19
	movq	(%rsi), %rbx
	movq	%rbx, (%r10)
.L20:
	movq	(%rsi), %r10
	testq	%r10, %r10
	je	.L21
	movq	8(%rsi), %rbx
	movq	%rbx, 8(%r10)
.L21:
	movq	%r9, 16(%rdi,%rax,8)
	movq	24(%r9), %r10
	movq	$0, 8(%r8)
	movq	%r10, (%r8)
	movq	24(%r9), %r10
	testq	%r10, %r10
	je	.L22
	movq	%r8, 8(%r10)
.L22:
	movq	%r8, 24(%r9)
.L23:
	subq	$24, %rsi
	subq	$24, %r8
	leaq	1(%rax), %r9
	cmpq	%rax, %r11
	je	.L37
	movq	%r9, %rax
.L24:
	leal	(%rax,%rax), %r9d
	movl	%r9d, 16(%r8)
	movq	(%rdx,%rax,8), %r9
	testq	%r9, %r9
	jne	.L38
	movq	$0, 16(%rdi,%rax,8)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L19:
	movq	(%rsi), %r10
	movq	%r10, 24(%r9)
	jmp	.L20
.L37:
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%ecx, 8(%rdi)
	ret
	.cfi_endproc
.LFE10134:
	.size	_ZN2v88internal8compiler4Node15OutOfLineInputs11ExtractFromEPNS2_3UseEPPS2_i, .-_ZN2v88internal8compiler4Node15OutOfLineInputs11ExtractFromEPNS2_3UseEPPS2_i
	.section	.rodata._ZN2v88internal8compiler4Node3NewEPNS0_4ZoneEjPKNS1_8OperatorEiPKPS2_b.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Node::New() Error: #%d:%s[%d] is nullptr"
	.section	.rodata._ZN2v88internal8compiler4Node3NewEPNS0_4ZoneEjPKNS1_8OperatorEiPKPS2_b.str1.1,"aMS",@progbits,1
.LC1:
	.string	"IdField::is_valid(id)"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler4Node3NewEPNS0_4ZoneEjPKNS1_8OperatorEiPKPS2_b,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node3NewEPNS0_4ZoneEjPKNS1_8OperatorEiPKPS2_b
	.type	_ZN2v88internal8compiler4Node3NewEPNS0_4ZoneEjPKNS1_8OperatorEiPKPS2_b, @function
_ZN2v88internal8compiler4Node3NewEPNS0_4ZoneEjPKNS1_8OperatorEiPKPS2_b:
.LFB10137:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testl	%ecx, %ecx
	jle	.L40
	leal	-1(%rcx), %edx
	xorl	%ecx, %ecx
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%rsi, %rcx
.L42:
	cmpq	$0, 0(%r13,%rcx,8)
	je	.L64
	leaq	1(%rcx), %rsi
	cmpq	%rcx, %rdx
	jne	.L60
	cmpl	$14, %r12d
	jle	.L40
	testb	%r9b, %r9b
	leal	14(%r12), %edx
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	cmove	%r12d, %edx
	subq	%rax, %rcx
	movslq	%edx, %rbx
	movq	%rbx, %rsi
	salq	$5, %rsi
	addq	$16, %rsi
	cmpq	%rcx, %rsi
	ja	.L65
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L45:
	leaq	(%rbx,%rbx,2), %rcx
	leaq	(%rax,%rcx,8), %r8
	movl	%edx, 12(%r8)
	movq	%r8, %rbx
	movl	$0, 8(%r8)
	movq	16(%rdi), %r9
	movq	24(%rdi), %rax
	subq	%r9, %rax
	cmpq	$39, %rax
	jbe	.L66
	leaq	40(%r9), %rax
	movq	%rax, 16(%rdi)
.L47:
	movl	%r14d, %eax
	movq	%r15, (%r9)
	orl	$251658240, %eax
	movq	$0, 8(%r9)
	movl	$0, 16(%r9)
	movl	%eax, 20(%r9)
	movq	$0, 24(%r9)
	andl	$-16777216, %r14d
	jne	.L54
	movq	%r8, 32(%r9)
	addq	$16, %r8
	movq	%r9, -16(%r8)
	movl	%r12d, -8(%r8)
.L49:
	leaq	-24(%rbx), %rax
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L59:
	movq	0(%r13,%rdx,8), %rsi
	leal	(%rdx,%rdx), %edi
	orl	%r14d, %edi
	movq	%rsi, (%r8,%rdx,8)
	movl	%edi, 16(%rax)
	movq	24(%rsi), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	movq	24(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L57
	movq	%rax, 8(%rcx)
.L57:
	addq	$1, %rdx
	movq	%rax, 24(%rsi)
	subq	$24, %rax
	cmpl	%edx, %r12d
	jg	.L59
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L40:
	testb	%r9b, %r9b
	jne	.L50
	testl	%r12d, %r12d
	movl	$1, %ebx
	cmovg	%r12d, %ebx
.L51:
	movslq	%ebx, %rdx
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	movq	%rdx, %rsi
	salq	$5, %rsi
	subq	%rax, %rcx
	addq	$32, %rsi
	cmpq	%rcx, %rsi
	ja	.L67
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L53:
	leaq	(%rdx,%rdx,2), %rdx
	sall	$28, %ebx
	leaq	(%rax,%rdx,8), %r9
	movl	%r12d, %eax
	sall	$24, %eax
	movq	%r15, (%r9)
	orl	%r14d, %eax
	movq	$0, 8(%r9)
	orl	%eax, %ebx
	andl	$-16777216, %r14d
	movl	$0, 16(%r9)
	movl	%ebx, 20(%r9)
	movq	$0, 24(%r9)
	jne	.L54
	testl	%r12d, %r12d
	jg	.L68
.L39:
	addq	$24, %rsp
	movq	%r9, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	leal	3(%r12), %ebx
	movl	$14, %eax
	cmpl	$14, %ebx
	cmovg	%eax, %ebx
	jmp	.L51
.L54:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L66:
	movl	$40, %esi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %r9
	jmp	.L47
.L67:
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	jmp	.L53
.L65:
	movl	%edx, -60(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdi
	movl	-60(%rbp), %edx
	jmp	.L45
.L64:
	movq	8(%r15), %rdx
	movl	%r14d, %esi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L68:
	leaq	32(%r9), %r8
	movq	%r9, %rbx
	movl	$1, %r14d
	jmp	.L49
	.cfi_endproc
.LFE10137:
	.size	_ZN2v88internal8compiler4Node3NewEPNS0_4ZoneEjPKNS1_8OperatorEiPKPS2_b, .-_ZN2v88internal8compiler4Node3NewEPNS0_4ZoneEjPKNS1_8OperatorEiPKPS2_b
	.section	.text._ZN2v88internal8compiler4Node5CloneEPNS0_4ZoneEjPKS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node5CloneEPNS0_4ZoneEjPKS2_
	.type	_ZN2v88internal8compiler4Node5CloneEPNS0_4ZoneEjPKS2_, @function
_ZN2v88internal8compiler4Node5CloneEPNS0_4ZoneEjPKS2_:
.LFB10140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rdx), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	movzbl	23(%rdx), %ecx
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L71
	movq	32(%rdx), %r8
	movl	8(%r8), %ecx
	addq	$16, %r8
.L71:
	movq	(%rbx), %rdx
	xorl	%r9d, %r9d
	call	_ZN2v88internal8compiler4Node3NewEPNS0_4ZoneEjPKNS1_8OperatorEiPKPS2_b
	movq	8(%rbx), %rdx
	movq	%rdx, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10140:
	.size	_ZN2v88internal8compiler4Node5CloneEPNS0_4ZoneEjPKS2_, .-_ZN2v88internal8compiler4Node5CloneEPNS0_4ZoneEjPKS2_
	.section	.rodata._ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"Use::InputIndexField::is_valid(input_count)"
	.section	.text._ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_
	.type	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_, @function
_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_:
.LFB10142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	20(%rdi), %edx
	movl	%edx, %ebx
	movl	%edx, %eax
	shrl	$24, %ebx
	shrl	$28, %eax
	andl	$15, %ebx
	cmpl	%eax, %ebx
	jge	.L75
	leal	1(%rbx), %eax
	andl	$-251658241, %edx
	sall	$24, %eax
	orl	%edx, %eax
	movl	%ebx, %edx
	movl	%eax, 20(%rdi)
	xorl	$251658240, %eax
	salq	$3, %rdx
	testl	$251658240, %eax
	je	.L76
	leaq	(%r14,%rdx), %rax
.L77:
	movq	%r13, (%rax)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L78
	movq	(%r14), %r12
.L78:
	movl	%ebx, %eax
	leal	1(%rbx,%rbx), %ebx
	notl	%eax
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%r12,%rax,8), %rax
.L106:
	movl	%ebx, 16(%rax)
	movq	24(%r13), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movq	24(%r13), %rdx
	testq	%rdx, %rdx
	je	.L94
	movq	%rax, 8(%rdx)
.L94:
	movq	%rax, 24(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	%rsi, %rdi
	cmpl	$15, %ebx
	je	.L107
	leal	3(%rbx,%rbx), %edx
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	movl	%ebx, %r8d
	movslq	%edx, %r15
	movq	%r15, %rsi
	subq	%rax, %rcx
	salq	$5, %rsi
	addq	$16, %rsi
	cmpq	%rcx, %rsi
	ja	.L108
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L85:
	leaq	(%r15,%r15,2), %rcx
	movq	%r12, %rsi
	leaq	(%rax,%rcx,8), %r15
	movl	%edx, 12(%r15)
	movq	%r14, %rdx
	movl	$0, 8(%r15)
	movq	%r12, (%r15)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L109
.L86:
	subq	$24, %rsi
	movl	%ebx, %ecx
	movq	%r15, %rdi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal8compiler4Node15OutOfLineInputs11ExtractFromEPNS2_3UseEPPS2_i
	movl	-52(%rbp), %r8d
	orl	$251658240, 20(%r12)
	movq	%r15, (%r14)
.L83:
	addl	$1, 8(%r15)
	movzbl	23(%r12), %edx
	movslq	%r8d, %rax
	salq	$3, %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L90
	addq	%r14, %rax
.L91:
	movq	%r13, (%rax)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L92
	movq	(%r14), %r12
.L92:
	movl	%r8d, %eax
	addl	%ebx, %ebx
	notl	%eax
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%r12,%rax,8), %rax
	testl	%r8d, %r8d
	jns	.L106
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L107:
	movq	(%r14), %r15
	movl	8(%r15), %r8d
	movl	%r8d, %ebx
	cmpl	%r8d, 12(%r15)
	jg	.L83
	leal	3(%r8,%r8), %edx
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	movslq	%edx, %r15
	movq	%r15, %rsi
	subq	%rax, %rcx
	salq	$5, %rsi
	addq	$16, %rsi
	cmpq	%rcx, %rsi
	ja	.L110
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L88:
	leaq	(%r15,%r15,2), %rcx
	movq	%r12, %rsi
	leaq	(%rax,%rcx,8), %r15
	movl	%edx, 12(%r15)
	movq	%r14, %rdx
	movl	$0, 8(%r15)
	movq	%r12, (%r15)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L89
	movq	(%r14), %rsi
	leaq	16(%rsi), %rdx
.L89:
	movl	%r8d, %ecx
	subq	$24, %rsi
	movq	%r15, %rdi
	movl	%r8d, -52(%rbp)
	call	_ZN2v88internal8compiler4Node15OutOfLineInputs11ExtractFromEPNS2_3UseEPPS2_i
	movq	%r15, (%r14)
	movl	-52(%rbp), %r8d
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L76:
	movq	(%r14), %rax
	leaq	16(%rax,%rdx), %rax
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L109:
	movq	(%r14), %rsi
	leaq	16(%rsi), %rdx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L90:
	movq	(%r14), %rdx
	leaq	16(%rdx,%rax), %rax
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L108:
	movl	%edx, -52(%rbp)
	movl	%ebx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %edx
	movl	-56(%rbp), %r8d
	jmp	.L85
.L110:
	movl	%r8d, -56(%rbp)
	movl	%edx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %edx
	movl	-56(%rbp), %r8d
	jmp	.L88
	.cfi_endproc
.LFE10142:
	.size	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_, .-_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_
	.section	.text._ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_
	.type	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_, @function
_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_:
.LFB10144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	32(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movzbl	23(%rdi), %eax
	movq	%rdi, %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L112
	subl	$1, %eax
	cltq
	leaq	(%r14,%rax,8), %rax
.L113:
	movq	(%rax), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_
	movl	20(%rbx), %r9d
	movl	%r9d, %ecx
	shrl	$24, %ecx
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L114
	subl	$1, %ecx
	cmpl	%ecx, %r12d
	jge	.L115
.L116:
	movslq	%ecx, %rax
	leaq	40(%rbx), %r8
	leaq	-8(,%rax,8), %rax
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L159:
	movq	32(%rbx,%rax), %rsi
	movq	40(%rbx,%rax), %rdi
	leaq	(%r8,%rax), %r11
	movq	%rbx, %r10
	cmpq	%rsi, %rdi
	je	.L122
.L121:
	leaq	0(,%rax,4), %rdx
	movq	%rax, %r9
	subq	%rdx, %r9
	leaq	-48(%r10,%r9), %rdx
	testq	%rdi, %rdi
	je	.L124
	movq	8(%rdx), %r9
	movq	(%rdx), %r10
	testq	%r9, %r9
	je	.L125
	movq	%r10, (%r9)
.L126:
	movq	(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L124
	movq	8(%rdx), %r9
	movq	%r9, 8(%rdi)
.L124:
	movq	%rsi, (%r11)
	testq	%rsi, %rsi
	je	.L157
	movq	24(%rsi), %rdi
	movq	$0, 8(%rdx)
	movq	%rdi, (%rdx)
	movq	24(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L129
	movq	%rdx, 8(%rdi)
.L129:
	movq	%rdx, 24(%rsi)
.L157:
	movl	20(%rbx), %r9d
	movl	%r9d, %edx
	shrl	$24, %edx
	andl	$15, %edx
.L122:
	subq	$8, %rax
	cmpl	%r12d, %ecx
	jle	.L119
.L118:
	movl	%r9d, %edx
	subl	$1, %ecx
	leaq	8(%rax), %rdi
	shrl	$24, %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L159
	movq	(%r14), %r10
	leaq	16(%r10,%rdi), %r11
	movq	16(%r10,%rax), %rsi
	movq	(%r11), %rdi
	cmpq	%rsi, %rdi
	jne	.L121
	subq	$8, %rax
	cmpl	%r12d, %ecx
	jg	.L118
	.p2align 4,,10
	.p2align 3
.L119:
	cmpl	$15, %edx
	je	.L160
.L115:
	movslq	%r12d, %rax
	leaq	(%r14,%rax,8), %rcx
	movq	(%rcx), %rdx
	cmpq	%rdx, %r13
	je	.L111
.L130:
	notl	%r12d
	movslq	%r12d, %r12
	leaq	(%r12,%r12,2), %rax
	leaq	(%rbx,%rax,8), %rax
	testq	%rdx, %rdx
	je	.L134
	movq	8(%rax), %rsi
	movq	(%rax), %rdi
	testq	%rsi, %rsi
	je	.L135
	movq	%rdi, (%rsi)
.L136:
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L134
	movq	8(%rax), %rsi
	movq	%rsi, 8(%rdx)
.L134:
	movq	%r13, (%rcx)
	testq	%r13, %r13
	je	.L111
	movq	24(%r13), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movq	24(%r13), %rdx
	testq	%rdx, %rdx
	je	.L138
	movq	%rax, 8(%rdx)
.L138:
	movq	%rax, 24(%r13)
.L111:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	%r10, 24(%rdi)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L114:
	movq	(%r14), %rax
	movl	8(%rax), %ecx
	subl	$1, %ecx
	cmpl	%ecx, %r12d
	jl	.L116
	movq	%rax, %rbx
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L112:
	movq	32(%rdi), %rdx
	movl	8(%rdx), %eax
	subl	$1, %eax
	cltq
	leaq	16(%rdx,%rax,8), %rax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L160:
	movq	(%r14), %rbx
.L117:
	movslq	%r12d, %rax
	leaq	16(%rbx,%rax,8), %rcx
	movq	(%rcx), %rdx
	cmpq	%rdx, %r13
	jne	.L130
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%rdi, 24(%rdx)
	jmp	.L136
	.cfi_endproc
.LFE10144:
	.size	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_, .-_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_
	.section	.text._ZN2v88internal8compiler4Node12InsertInputsEPNS0_4ZoneEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node12InsertInputsEPNS0_4ZoneEii
	.type	_ZN2v88internal8compiler4Node12InsertInputsEPNS0_4ZoneEii, @function
_ZN2v88internal8compiler4Node12InsertInputsEPNS0_4ZoneEii:
.LFB10145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	%edx, -64(%rbp)
	testl	%ecx, %ecx
	jg	.L187
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L250:
	movl	%r13d, %eax
	movl	$0, %ecx
	movl	%r13d, %r10d
	subl	%r12d, %eax
	cmovs	%ecx, %eax
	cltq
	movq	(%r15,%rax,8), %r14
	movl	%edx, %eax
	shrl	$28, %eax
	cmpl	%eax, %r13d
	jge	.L249
	leal	1(%r13), %eax
	andl	$-251658241, %edx
	sall	$24, %eax
	orl	%edx, %eax
	movl	%r13d, %edx
	movl	%eax, 20(%rbx)
	xorl	$251658240, %eax
	salq	$3, %rdx
	testl	$251658240, %eax
	je	.L170
	leaq	(%r15,%rdx), %rax
.L171:
	movq	%r14, (%rax)
	movzbl	23(%rbx), %eax
	movq	%rbx, %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L172
	movq	(%r15), %rdx
.L172:
	movl	%r13d, %eax
	leal	1(%r13,%r13), %r13d
	notl	%eax
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,8), %rax
.L247:
	movl	%r13d, 16(%rax)
	movq	24(%r14), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movq	24(%r14), %rdx
	testq	%rdx, %rdx
	je	.L185
	movq	%rax, 8(%rdx)
.L185:
	addl	$1, %r8d
	movq	%rax, 24(%r14)
	cmpl	%r8d, %r12d
	je	.L186
.L187:
	movl	20(%rbx), %edx
	movl	%edx, %r13d
	shrl	$24, %r13d
	andl	$15, %r13d
	cmpl	$15, %r13d
	jne	.L250
	movq	(%r15), %rdi
	movl	$0, %edx
	movl	8(%rdi), %r10d
	movl	%r10d, %eax
	movl	%r10d, %r13d
	subl	%r12d, %eax
	cmovs	%edx, %eax
	cltq
	movq	16(%rdi,%rax,8), %r14
	movl	%r10d, %eax
	cmpl	12(%rdi), %r10d
	jge	.L251
.L169:
	addl	$1, %eax
	movl	%eax, 8(%rdi)
	movzbl	23(%rbx), %edx
	movslq	%r10d, %rax
	salq	$3, %rax
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L181
	addq	%r15, %rax
.L182:
	movq	%r14, (%rax)
	movzbl	23(%rbx), %eax
	movq	%rbx, %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L183
	movq	(%r15), %rdx
.L183:
	movl	%r10d, %eax
	notl	%eax
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,8), %rax
	testl	%r10d, %r10d
	js	.L252
	addl	%r13d, %r13d
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L249:
	leal	3(%r13,%r13), %edx
	movq	16(%r9), %rax
	movq	24(%r9), %rdi
	movslq	%edx, %rcx
	movq	%rcx, %rsi
	subq	%rax, %rdi
	salq	$5, %rsi
	addq	$16, %rsi
	cmpq	%rdi, %rsi
	ja	.L253
	addq	%rax, %rsi
	movq	%rsi, 16(%r9)
.L176:
	leaq	(%rcx,%rcx,2), %rcx
	movq	%rbx, %rsi
	leaq	(%rax,%rcx,8), %rdi
	movl	%edx, 12(%rdi)
	movq	%r15, %rdx
	movl	$0, 8(%rdi)
	movq	%rbx, (%rdi)
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L177
	movq	(%r15), %rsi
	leaq	16(%rsi), %rdx
.L177:
	subq	$24, %rsi
	movl	%r13d, %ecx
	movq	%r9, -72(%rbp)
	movl	%r8d, -60(%rbp)
	movl	%r10d, -56(%rbp)
	call	_ZN2v88internal8compiler4Node15OutOfLineInputs11ExtractFromEPNS2_3UseEPPS2_i
	orl	$251658240, 20(%rbx)
	movl	-56(%rbp), %r10d
	movl	-60(%rbp), %r8d
	movq	-72(%rbp), %r9
	movq	%rdi, (%r15)
	movl	8(%rdi), %eax
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L170:
	movq	(%r15), %rax
	leaq	16(%rax,%rdx), %rax
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L181:
	movq	(%r15), %rdx
	leaq	16(%rdx,%rax), %rax
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L186:
	movl	20(%rbx), %edx
	movl	%edx, %ecx
	shrl	$24, %ecx
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L254
.L188:
	movl	-64(%rbp), %esi
	movl	%ecx, %r8d
	subl	%r12d, %r8d
	movl	%esi, %edi
	cmpl	%esi, %r12d
	leal	-1(%r8), %eax
	cmovge	%r12d, %edi
	cmpl	%edi, %eax
	jl	.L201
	movl	%r12d, %esi
	notl	%edi
	movslq	%r12d, %r9
	cltq
	subl	%ecx, %esi
	addl	%r8d, %edi
	negq	%r9
	salq	$3, %rax
	movslq	%esi, %rcx
	salq	$3, %r9
	leaq	32(%rbx), %r13
	leaq	(%rcx,%rcx,2), %rsi
	movslq	%r8d, %rcx
	leaq	(%rbx,%r9), %r15
	subq	%rdi, %rcx
	salq	$3, %rsi
	leaq	-16(,%rcx,8), %r11
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L255:
	movq	32(%r15,%rax), %rdi
	movq	32(%rbx,%rax), %rcx
	leaq	(%rax,%r13), %r8
	movq	%rbx, %rdx
	cmpq	%rdi, %rcx
	je	.L193
.L192:
	addq	%rsi, %rdx
	testq	%rcx, %rcx
	je	.L196
	movq	8(%rdx), %r10
	movq	(%rdx), %r14
	testq	%r10, %r10
	je	.L197
	movq	%r14, (%r10)
.L198:
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L196
	movq	8(%rdx), %r10
	movq	%r10, 8(%rcx)
.L196:
	movq	%rdi, (%r8)
	testq	%rdi, %rdi
	je	.L193
	movq	24(%rdi), %rcx
	movq	$0, 8(%rdx)
	movq	%rcx, (%rdx)
	movq	24(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L200
	movq	%rdx, 8(%rcx)
.L200:
	movq	%rdx, 24(%rdi)
.L193:
	subq	$8, %rax
	addq	$24, %rsi
	cmpq	%rax, %r11
	je	.L201
.L256:
	movl	20(%rbx), %edx
.L202:
	xorl	$251658240, %edx
	leaq	(%r9,%rax), %rcx
	andl	$251658240, %edx
	jne	.L255
	movq	0(%r13), %rdx
	leaq	16(%rdx,%rax), %r8
	movq	16(%rdx,%rcx), %rdi
	movq	(%r8), %rcx
	cmpq	%rdi, %rcx
	jne	.L192
	subq	$8, %rax
	addq	$24, %rsi
	cmpq	%rax, %r11
	jne	.L256
	.p2align 4,,10
	.p2align 3
.L201:
	testl	%r12d, %r12d
	jle	.L161
	movslq	-64(%rbp), %rdx
	leal	-1(%r12), %ecx
	leaq	32(%rbx), %r8
	leaq	1(%rdx,%rcx), %r9
	leaq	0(,%rdx,8), %rax
	salq	$3, %r9
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L257:
	movq	32(%rbx,%rax), %rcx
	leaq	(%rax,%r8), %rsi
	movq	%rbx, %rdx
	testq	%rcx, %rcx
	je	.L205
.L204:
	leaq	0(,%rax,4), %rdi
	movq	%rax, %r11
	subq	%rdi, %r11
	leaq	-24(%rdx,%r11), %rdx
	movq	8(%rdx), %rdi
	movq	(%rdx), %r10
	testq	%rdi, %rdi
	je	.L206
	movq	%r10, (%rdi)
.L207:
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L208
	movq	8(%rdx), %rdx
	movq	%rdx, 8(%rcx)
.L208:
	movq	$0, (%rsi)
.L205:
	addq	$8, %rax
	cmpq	%r9, %rax
	je	.L161
.L210:
	movzbl	23(%rbx), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L257
	movq	(%r8), %rdx
	leaq	16(%rdx,%rax), %rsi
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L204
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L197:
	movq	%r14, 24(%rcx)
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L206:
	movq	%r10, 24(%rcx)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L161:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	movq	32(%rbx), %rax
	movl	8(%rax), %ecx
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L251:
	leal	3(%r10,%r10), %edx
	movq	16(%r9), %rax
	movq	24(%r9), %rdi
	movslq	%edx, %rcx
	movq	%rcx, %rsi
	subq	%rax, %rdi
	salq	$5, %rsi
	addq	$16, %rsi
	cmpq	%rdi, %rsi
	ja	.L258
	addq	%rax, %rsi
	movq	%rsi, 16(%r9)
.L179:
	leaq	(%rcx,%rcx,2), %rcx
	movq	%rbx, %rsi
	leaq	(%rax,%rcx,8), %rdi
	movl	%edx, 12(%rdi)
	movq	%r15, %rdx
	movl	$0, 8(%rdi)
	movq	%rbx, (%rdi)
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L180
	movq	(%r15), %rsi
	leaq	16(%rsi), %rdx
.L180:
	movl	%r10d, %ecx
	subq	$24, %rsi
	movq	%r9, -72(%rbp)
	movl	%r8d, -60(%rbp)
	movl	%r10d, -56(%rbp)
	call	_ZN2v88internal8compiler4Node15OutOfLineInputs11ExtractFromEPNS2_3UseEPPS2_i
	movq	-72(%rbp), %r9
	movl	-60(%rbp), %r8d
	movq	%rdi, (%r15)
	movl	-56(%rbp), %r10d
	movl	8(%rdi), %eax
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L252:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L253:
	movq	%r9, %rdi
	movl	%r8d, -84(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%edx, -72(%rbp)
	movq	%r9, -56(%rbp)
	movl	%r13d, -60(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r9
	movl	-60(%rbp), %r10d
	movl	-72(%rbp), %edx
	movq	-80(%rbp), %rcx
	movl	-84(%rbp), %r8d
	jmp	.L176
.L258:
	movq	%r9, %rdi
	movl	%r8d, -84(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%edx, -72(%rbp)
	movl	%r10d, -60(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r9
	movl	-60(%rbp), %r10d
	movl	-72(%rbp), %edx
	movq	-80(%rbp), %rcx
	movl	-84(%rbp), %r8d
	jmp	.L179
	.cfi_endproc
.LFE10145:
	.size	_ZN2v88internal8compiler4Node12InsertInputsEPNS0_4ZoneEii, .-_ZN2v88internal8compiler4Node12InsertInputsEPNS0_4ZoneEii
	.section	.text._ZN2v88internal8compiler4Node11ClearInputsEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node11ClearInputsEii
	.type	_ZN2v88internal8compiler4Node11ClearInputsEii, @function
_ZN2v88internal8compiler4Node11ClearInputsEii:
.LFB10147:
	.cfi_startproc
	endbr64
	movslq	%esi, %rcx
	leaq	32(%rdi), %r8
	leaq	0(,%rcx,8), %rax
	movzbl	23(%rdi), %ecx
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.L260
	addq	%r8, %rax
.L261:
	notl	%esi
	movslq	%esi, %rsi
	leaq	(%rsi,%rsi,2), %rcx
	leaq	(%rdi,%rcx,8), %rcx
	testl	%edx, %edx
	jle	.L259
	subl	$1, %edx
	leaq	8(%rax,%rdx,8), %r8
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%rdi, (%rsi)
.L266:
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L264
	movq	8(%rcx), %rsi
	movq	%rsi, 8(%rdx)
.L264:
	addq	$8, %rax
	subq	$24, %rcx
	cmpq	%r8, %rax
	je	.L259
.L268:
	movq	(%rax), %rdx
	movq	$0, (%rax)
	testq	%rdx, %rdx
	je	.L264
	movq	8(%rcx), %rsi
	movq	(%rcx), %rdi
	testq	%rsi, %rsi
	jne	.L274
	movq	%rdi, 24(%rdx)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L259:
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	movq	32(%rdi), %rdi
	leaq	16(%rdi,%rax), %rax
	jmp	.L261
	.cfi_endproc
.LFE10147:
	.size	_ZN2v88internal8compiler4Node11ClearInputsEii, .-_ZN2v88internal8compiler4Node11ClearInputsEii
	.section	.text._ZN2v88internal8compiler4Node4KillEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node4KillEv
	.type	_ZN2v88internal8compiler4Node4KillEv, @function
_ZN2v88internal8compiler4Node4KillEv:
.LFB10141:
	.cfi_startproc
	endbr64
	movzbl	23(%rdi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L277
	movq	32(%rdi), %rax
	movl	8(%rax), %edx
.L277:
	xorl	%esi, %esi
	jmp	_ZN2v88internal8compiler4Node11ClearInputsEii
	.cfi_endproc
.LFE10141:
	.size	_ZN2v88internal8compiler4Node4KillEv, .-_ZN2v88internal8compiler4Node4KillEv
	.section	.text._ZN2v88internal8compiler4Node11RemoveInputEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node11RemoveInputEi
	.type	_ZN2v88internal8compiler4Node11RemoveInputEi, @function
_ZN2v88internal8compiler4Node11RemoveInputEi:
.LFB10146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	1(%rsi), %r8d
	movslq	%esi, %rsi
	movq	%rdi, %r10
	leaq	0(,%rsi,8), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	32(%rdi), %rbx
	subq	$8, %rsp
	movl	20(%rdi), %r11d
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L303:
	leal	-1(%rax), %r9d
	leal	-1(%r8), %eax
	cmpl	%r9d, %eax
	jge	.L282
	movslq	%r8d, %rax
	movq	32(%r10,%rdx), %rdi
	leaq	(%rbx,%rdx), %rsi
	movq	%r10, %rcx
	movq	(%rbx,%rax,8), %rax
	cmpq	%rax, %rdi
	je	.L291
.L294:
	leaq	0(,%rdx,4), %r9
	movq	%rdx, %r11
	subq	%r9, %r11
	leaq	-24(%rcx,%r11), %rcx
	testq	%rdi, %rdi
	je	.L285
	movq	8(%rcx), %r9
	movq	(%rcx), %r11
	testq	%r9, %r9
	je	.L286
	movq	%r11, (%r9)
.L287:
	movq	(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L285
	movq	8(%rcx), %r9
	movq	%r9, 8(%rdi)
.L285:
	movq	%rax, (%rsi)
	testq	%rax, %rax
	je	.L289
	movq	24(%rax), %rsi
	movq	$0, 8(%rcx)
	movq	%rsi, (%rcx)
	movq	24(%rax), %rsi
	testq	%rsi, %rsi
	je	.L290
	movq	%rcx, 8(%rsi)
.L290:
	movq	%rcx, 24(%rax)
	movl	20(%r10), %r11d
.L291:
	addl	$1, %r8d
	addq	$8, %rdx
.L279:
	movl	%r11d, %eax
	shrl	$24, %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L303
	movq	32(%r10), %rcx
	movl	8(%rcx), %eax
	leal	-1(%rax), %r9d
	leal	-1(%r8), %eax
	cmpl	%r9d, %eax
	jge	.L282
	leaq	16(%rcx), %rsi
	movslq	%r8d, %rax
	movq	(%rsi,%rax,8), %rax
	addq	%rdx, %rsi
	movq	(%rsi), %rdi
	cmpq	%rax, %rdi
	jne	.L294
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L289:
	movl	20(%r10), %r11d
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L286:
	movq	%r11, 24(%rdi)
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L282:
	movl	$1, %edx
	movl	%r9d, %esi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler4Node11ClearInputsEii
	movl	20(%r10), %eax
	movl	%eax, %edx
	xorl	$251658240, %edx
	andl	$251658240, %edx
	je	.L292
	andl	$-251658241, %eax
	sall	$24, %r9d
	orl	%r9d, %eax
	movl	%eax, 20(%r10)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	movq	32(%r10), %rax
	movl	%r9d, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10146:
	.size	_ZN2v88internal8compiler4Node11RemoveInputEi, .-_ZN2v88internal8compiler4Node11RemoveInputEi
	.section	.text._ZN2v88internal8compiler4Node13NullAllInputsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node13NullAllInputsEv
	.type	_ZN2v88internal8compiler4Node13NullAllInputsEv, @function
_ZN2v88internal8compiler4Node13NullAllInputsEv:
.LFB10148:
	.cfi_startproc
	endbr64
	movzbl	23(%rdi), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L306
	movq	32(%rdi), %rax
	movl	8(%rax), %edx
.L306:
	xorl	%esi, %esi
	jmp	_ZN2v88internal8compiler4Node11ClearInputsEii
	.cfi_endproc
.LFE10148:
	.size	_ZN2v88internal8compiler4Node13NullAllInputsEv, .-_ZN2v88internal8compiler4Node13NullAllInputsEv
	.section	.text._ZN2v88internal8compiler4Node14TrimInputCountEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node14TrimInputCountEi
	.type	_ZN2v88internal8compiler4Node14TrimInputCountEi, @function
_ZN2v88internal8compiler4Node14TrimInputCountEi:
.LFB10149:
	.cfi_startproc
	endbr64
	movzbl	23(%rdi), %edx
	movq	%rdi, %r10
	movl	%esi, %r9d
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L309
	movq	32(%rdi), %rax
	movl	8(%rax), %edx
.L309:
	cmpl	%edx, %r9d
	je	.L313
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r9d, %esi
	subl	%r9d, %edx
	movq	%r10, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler4Node11ClearInputsEii
	movl	20(%r10), %esi
	movl	%esi, %eax
	xorl	$251658240, %eax
	testl	$251658240, %eax
	je	.L311
	andl	$-251658241, %esi
	sall	$24, %r9d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	orl	%esi, %r9d
	movl	%r9d, 20(%r10)
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	movq	32(%r10), %rax
	movl	%r9d, 8(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE10149:
	.size	_ZN2v88internal8compiler4Node14TrimInputCountEi, .-_ZN2v88internal8compiler4Node14TrimInputCountEi
	.section	.text._ZNK2v88internal8compiler4Node8UseCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Node8UseCountEv
	.type	_ZNK2v88internal8compiler4Node8UseCountEv, @function
_ZNK2v88internal8compiler4Node8UseCountEv:
.LFB10150:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L316
	.p2align 4,,10
	.p2align 3
.L318:
	movq	(%rax), %rax
	addl	$1, %r8d
	testq	%rax, %rax
	jne	.L318
.L316:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE10150:
	.size	_ZNK2v88internal8compiler4Node8UseCountEv, .-_ZNK2v88internal8compiler4Node8UseCountEv
	.section	.text._ZN2v88internal8compiler4Node11ReplaceUsesEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node11ReplaceUsesEPS2_
	.type	_ZN2v88internal8compiler4Node11ReplaceUsesEPS2_, @function
_ZN2v88internal8compiler4Node11ReplaceUsesEPS2_:
.LFB10151:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	jne	.L326
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L341:
	movq	%rsi, 32(%r8,%rax,8)
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L324
.L340:
	movq	%rax, %rdx
.L326:
	movl	16(%rdx), %ecx
	movl	%ecx, %eax
	shrl	%eax
	andl	$1, %ecx
	leaq	3(%rax,%rax,2), %r8
	leaq	(%rdx,%r8,8), %r8
	jne	.L341
	movq	%rsi, 16(%r8,%rax,8)
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L340
.L324:
	movq	24(%rsi), %rax
	movq	%rax, (%rdx)
	testq	%rax, %rax
	je	.L327
	movq	%rdx, 8(%rax)
.L327:
	movq	24(%rdi), %rax
	movq	%rax, 24(%rsi)
.L322:
	movq	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE10151:
	.size	_ZN2v88internal8compiler4Node11ReplaceUsesEPS2_, .-_ZN2v88internal8compiler4Node11ReplaceUsesEPS2_
	.section	.text._ZNK2v88internal8compiler4Node7OwnedByEPKS2_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Node7OwnedByEPKS2_S4_
	.type	_ZNK2v88internal8compiler4Node7OwnedByEPKS2_S4_, @function
_ZNK2v88internal8compiler4Node7OwnedByEPKS2_S4_:
.LFB10152:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L350
	xorl	%r8d, %r8d
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L345:
	cmpq	%rcx, %rdx
	jne	.L350
	movq	(%rax), %rax
	orl	$2, %r8d
	testq	%rax, %rax
	je	.L352
.L348:
	movl	16(%rax), %edi
	movl	%edi, %ecx
	shrl	%ecx
	andl	$1, %edi
	leaq	3(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rcx
	jne	.L344
	movq	(%rcx), %rcx
.L344:
	cmpq	%rcx, %rsi
	jne	.L345
	movq	(%rax), %rax
	orl	$1, %r8d
	testq	%rax, %rax
	jne	.L348
.L352:
	cmpl	$3, %r8d
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE10152:
	.size	_ZNK2v88internal8compiler4Node7OwnedByEPKS2_S4_, .-_ZNK2v88internal8compiler4Node7OwnedByEPKS2_S4_
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC4:
	.string	": "
.LC5:
	.string	"("
.LC6:
	.string	", "
.LC7:
	.string	"null"
.LC8:
	.string	")"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_4NodeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_4NodeE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_4NodeE, @function
_ZN2v88internal8compilerlsERSoRKNS1_4NodeE:
.LFB10155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movl	20(%rsi), %esi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L355
	movq	32(%rbx), %rax
	movl	8(%rax), %eax
.L355:
	testl	%eax, %eax
	jg	.L367
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	movl	$1, %edx
	movq	%r14, %rdi
	leaq	32(%rbx), %r12
	xorl	%r13d, %r13d
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L369:
	leaq	(%rdx,%r12), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L363
.L370:
	movl	20(%rax), %esi
	movq	%r14, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
.L364:
	addq	$1, %r13
.L365:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	movl	%eax, %edx
	cmpl	$15, %eax
	jne	.L358
	movq	32(%rbx), %rdx
	movl	8(%rdx), %edx
.L358:
	cmpl	%r13d, %edx
	jle	.L359
	testl	%r13d, %r13d
	jne	.L368
.L360:
	leaq	0(,%r13,8), %rdx
	cmpl	$15, %eax
	jne	.L369
	movq	(%r12), %rax
	leaq	16(%rax,%rdx), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L370
.L363:
	movl	$4, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L368:
	movl	$2, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L359:
	movq	%r14, %rdi
	movl	$1, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10155:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_4NodeE, .-_ZN2v88internal8compilerlsERSoRKNS1_4NodeE
	.section	.rodata._ZNK2v88internal8compiler4Node5PrintERSo.str1.1,"aMS",@progbits,1
.LC9:
	.string	"  "
.LC10:
	.string	"(NULL)"
	.section	.text._ZNK2v88internal8compiler4Node5PrintERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Node5PrintERSo
	.type	_ZNK2v88internal8compiler4Node5PrintERSo, @function
_ZNK2v88internal8compiler4Node5PrintERSo:
.LFB10154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	%rbx, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compilerlsERSoRKNS1_4NodeE
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r14
	testq	%r14, %r14
	je	.L380
	cmpb	$0, 56(%r14)
	je	.L373
	movsbl	67(%r14), %esi
.L374:
	movq	%r12, %rdi
	leaq	32(%r13), %r12
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L376
	movq	32(%r13), %r12
	movl	8(%r12), %eax
	addq	$16, %r12
.L376:
	cltq
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %r15
	leaq	(%r12,%rax,8), %r14
	cmpq	%r12, %r14
	jne	.L383
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L393:
	movsbl	67(%r13), %esi
.L382:
	movq	%rbx, %rdi
	addq	$8, %r12
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	cmpq	%r12, %r14
	je	.L371
.L383:
	movq	(%r12), %r13
	movl	$2, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r13, %r13
	je	.L378
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_4NodeE
.L379:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	movq	240(%rbx,%rax), %r13
	testq	%r13, %r13
	je	.L380
	cmpb	$0, 56(%r13)
	jne	.L393
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	movq	48(%rax), %rax
	cmpq	%r15, %rax
	je	.L382
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L378:
	movl	$6, %edx
	leaq	.LC10(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L371:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L373:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L374
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L374
.L380:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE10154:
	.size	_ZNK2v88internal8compiler4Node5PrintERSo, .-_ZNK2v88internal8compiler4Node5PrintERSo
	.section	.text._ZNK2v88internal8compiler4Node5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Node5PrintEv
	.type	_ZNK2v88internal8compiler4Node5PrintEv, @function
_ZNK2v88internal8compiler4Node5PrintEv:
.LFB10153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-304(%rbp), %r13
	leaq	-384(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	subq	$352, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -72(%rbp)
	movups	%xmm0, -56(%rbp)
	movw	%ax, -80(%rbp)
	movq	%rbx, -304(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -384(%rbp)
	addq	$40, %rax
	movq	%rax, -304(%rbp)
	call	_ZNK2v88internal8compiler4Node5PrintERSo
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-320(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r13, %rdi
	movq	%rbx, -304(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L397
	addq	$352, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L397:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10153:
	.size	_ZNK2v88internal8compiler4Node5PrintEv, .-_ZNK2v88internal8compiler4Node5PrintEv
	.section	.text._ZN2v88internal8compiler4NodeC2EjPKNS1_8OperatorEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4NodeC2EjPKNS1_8OperatorEii
	.type	_ZN2v88internal8compiler4NodeC2EjPKNS1_8OperatorEii, @function
_ZN2v88internal8compiler4NodeC2EjPKNS1_8OperatorEii:
.LFB10161:
	.cfi_startproc
	endbr64
	sall	$28, %r8d
	sall	$24, %ecx
	movq	%rdx, (%rdi)
	orl	%esi, %r8d
	movq	$0, 8(%rdi)
	orl	%ecx, %r8d
	andl	$-16777216, %esi
	movl	$0, 16(%rdi)
	movl	%r8d, 20(%rdi)
	movq	$0, 24(%rdi)
	jne	.L403
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10161:
	.size	_ZN2v88internal8compiler4NodeC2EjPKNS1_8OperatorEii, .-_ZN2v88internal8compiler4NodeC2EjPKNS1_8OperatorEii
	.globl	_ZN2v88internal8compiler4NodeC1EjPKNS1_8OperatorEii
	.set	_ZN2v88internal8compiler4NodeC1EjPKNS1_8OperatorEii,_ZN2v88internal8compiler4NodeC2EjPKNS1_8OperatorEii
	.section	.text._ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE
	.type	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE, @function
_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE:
.LFB10163:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	$0, 8(%rsi)
	movq	%rax, (%rsi)
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L405
	movq	%rsi, 8(%rax)
.L405:
	movq	%rsi, 24(%rdi)
	ret
	.cfi_endproc
.LFE10163:
	.size	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE, .-_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE
	.section	.text._ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE
	.type	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE, @function
_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE:
.LFB10164:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rax
	movq	(%rsi), %rdx
	testq	%rax, %rax
	je	.L410
	movq	%rdx, (%rax)
.L411:
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L409
	movq	8(%rsi), %rdx
	movq	%rdx, 8(%rax)
.L409:
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	movq	%rdx, 24(%rdi)
	jmp	.L411
	.cfi_endproc
.LFE10164:
	.size	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE, .-_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE
	.section	.text._ZN2v88internal8compiler4Node10InputEdges8iteratorppEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node10InputEdges8iteratorppEi
	.type	_ZN2v88internal8compiler4Node10InputEdges8iteratorppEi, @function
_ZN2v88internal8compiler4Node10InputEdges8iteratorppEi:
.LFB10165:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	movq	(%rdi), %rax
	leaq	8(%rdx), %rcx
	movq	%rcx, 8(%rdi)
	leaq	-24(%rax), %rcx
	movq	%rcx, (%rdi)
	ret
	.cfi_endproc
.LFE10165:
	.size	_ZN2v88internal8compiler4Node10InputEdges8iteratorppEi, .-_ZN2v88internal8compiler4Node10InputEdges8iteratorppEi
	.section	.text._ZN2v88internal8compiler4Node6Inputs14const_iteratorppEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node6Inputs14const_iteratorppEi
	.type	_ZN2v88internal8compiler4Node6Inputs14const_iteratorppEi, @function
_ZN2v88internal8compiler4Node6Inputs14const_iteratorppEi:
.LFB10166:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, (%rdi)
	ret
	.cfi_endproc
.LFE10166:
	.size	_ZN2v88internal8compiler4Node6Inputs14const_iteratorppEi, .-_ZN2v88internal8compiler4Node6Inputs14const_iteratorppEi
	.section	.text._ZN2v88internal8compiler4Node8UseEdges8iteratorppEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node8UseEdges8iteratorppEi
	.type	_ZN2v88internal8compiler4Node8UseEdges8iteratorppEi, @function
_ZN2v88internal8compiler4Node8UseEdges8iteratorppEi:
.LFB10167:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	movq	(%rdi), %r8
	xorl	%eax, %eax
	movq	%rdx, (%rdi)
	testq	%rdx, %rdx
	je	.L419
	movq	(%rdx), %rax
.L419:
	movq	%rax, 8(%rdi)
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE10167:
	.size	_ZN2v88internal8compiler4Node8UseEdges8iteratorppEi, .-_ZN2v88internal8compiler4Node8UseEdges8iteratorppEi
	.section	.text._ZNK2v88internal8compiler4Node8UseEdges5emptyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Node8UseEdges5emptyEv
	.type	_ZNK2v88internal8compiler4Node8UseEdges5emptyEv, @function
_ZNK2v88internal8compiler4Node8UseEdges5emptyEv:
.LFB10168:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	cmpq	$0, 24(%rax)
	sete	%al
	ret
	.cfi_endproc
.LFE10168:
	.size	_ZNK2v88internal8compiler4Node8UseEdges5emptyEv, .-_ZNK2v88internal8compiler4Node8UseEdges5emptyEv
	.section	.text._ZN2v88internal8compiler4Node4Uses14const_iteratorppEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler4Node4Uses14const_iteratorppEi
	.type	_ZN2v88internal8compiler4Node4Uses14const_iteratorppEi, @function
_ZN2v88internal8compiler4Node4Uses14const_iteratorppEi:
.LFB10169:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rdx
	movq	%rdx, (%rdi)
	ret
	.cfi_endproc
.LFE10169:
	.size	_ZN2v88internal8compiler4Node4Uses14const_iteratorppEi, .-_ZN2v88internal8compiler4Node4Uses14const_iteratorppEi
	.section	.text._ZNK2v88internal8compiler4Node4Uses5emptyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler4Node4Uses5emptyEv
	.type	_ZNK2v88internal8compiler4Node4Uses5emptyEv, @function
_ZNK2v88internal8compiler4Node4Uses5emptyEv:
.LFB10170:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	cmpq	$0, 24(%rax)
	sete	%al
	ret
	.cfi_endproc
.LFE10170:
	.size	_ZNK2v88internal8compiler4Node4Uses5emptyEv, .-_ZNK2v88internal8compiler4Node4Uses5emptyEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler4Node15OutOfLineInputs3NewEPNS0_4ZoneEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler4Node15OutOfLineInputs3NewEPNS0_4ZoneEi, @function
_GLOBAL__sub_I__ZN2v88internal8compiler4Node15OutOfLineInputs3NewEPNS0_4ZoneEi:
.LFB11781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11781:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler4Node15OutOfLineInputs3NewEPNS0_4ZoneEi, .-_GLOBAL__sub_I__ZN2v88internal8compiler4Node15OutOfLineInputs3NewEPNS0_4ZoneEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler4Node15OutOfLineInputs3NewEPNS0_4ZoneEi
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
