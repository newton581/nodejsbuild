	.file	"node-matchers.cc"
	.text
	.section	.text._ZNK2v88internal8compiler11NodeMatcher12IsComparisonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler11NodeMatcher12IsComparisonEv
	.type	_ZNK2v88internal8compiler11NodeMatcher12IsComparisonEv, @function
_ZNK2v88internal8compiler11NodeMatcher12IsComparisonEv:
.LFB11078:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rax
	movzwl	16(%rax), %edx
	leal	-682(%rdx), %eax
	leal	-119(%rdx), %ecx
	cmpl	$5, %eax
	setbe	%al
	cmpl	$12, %ecx
	setbe	%cl
	orb	%cl, %al
	jne	.L1
	subl	$334, %edx
	cmpl	$15, %edx
	setbe	%al
.L1:
	ret
	.cfi_endproc
.LFE11078:
	.size	_ZNK2v88internal8compiler11NodeMatcher12IsComparisonEv, .-_ZNK2v88internal8compiler11NodeMatcher12IsComparisonEv
	.section	.text._ZN2v88internal8compiler13BranchMatcherC2EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13BranchMatcherC2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler13BranchMatcherC2EPNS1_4NodeE, @function
_ZN2v88internal8compiler13BranchMatcherC2EPNS1_4NodeE:
.LFB11080:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movq	(%rsi), %rax
	movq	$0, 8(%rdi)
	cmpw	$2, 16(%rax)
	movq	$0, 16(%rdi)
	jne	.L4
	movq	24(%rsi), %rax
	testq	%rax, %rax
	jne	.L10
.L4:
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$5, %edx
	jne	.L9
	movq	%rcx, 16(%rdi)
.L9:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L4
.L10:
	movl	16(%rax), %esi
	movl	%esi, %edx
	shrl	%edx
	andl	$1, %esi
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rcx
	movq	(%rcx), %rdx
	jne	.L7
	movq	%rdx, %rcx
	movq	(%rdx), %rdx
.L7:
	movzwl	16(%rdx), %edx
	cmpl	$4, %edx
	jne	.L8
	movq	%rcx, 8(%rdi)
	jmp	.L9
	.cfi_endproc
.LFE11080:
	.size	_ZN2v88internal8compiler13BranchMatcherC2EPNS1_4NodeE, .-_ZN2v88internal8compiler13BranchMatcherC2EPNS1_4NodeE
	.globl	_ZN2v88internal8compiler13BranchMatcherC1EPNS1_4NodeE
	.set	_ZN2v88internal8compiler13BranchMatcherC1EPNS1_4NodeE,_ZN2v88internal8compiler13BranchMatcherC2EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14DiamondMatcherC2EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14DiamondMatcherC2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler14DiamondMatcherC2EPNS1_4NodeE, @function
_ZN2v88internal8compiler14DiamondMatcherC2EPNS1_4NodeE:
.LFB11083:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movzbl	23(%rsi), %eax
	movq	$0, 8(%rdi)
	andl	$15, %eax
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movl	%eax, %edx
	cmpl	$15, %eax
	jne	.L17
	movq	32(%rsi), %rdx
	movl	8(%rdx), %edx
.L17:
	cmpl	$2, %edx
	jne	.L15
	movq	(%rsi), %rdx
	cmpw	$10, 16(%rdx)
	je	.L30
.L15:
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	32(%rsi), %rcx
	leaq	32(%rsi), %r8
	movq	%rcx, %r9
	cmpl	$15, %eax
	je	.L31
.L19:
	movzbl	23(%r9), %edx
	andl	$15, %edx
	movl	%edx, %esi
	cmpl	$15, %edx
	je	.L32
.L21:
	cmpl	$1, %esi
	jne	.L15
	addq	$8, %r8
	addq	$24, %rcx
	cmpl	$15, %eax
	cmovne	%r8, %rcx
	movq	(%rcx), %rcx
	movzbl	23(%rcx), %eax
	andl	$15, %eax
	movl	%eax, %esi
	cmpl	$15, %eax
	jne	.L25
	movq	32(%rcx), %rsi
	movl	8(%rsi), %esi
.L25:
	cmpl	$1, %esi
	jne	.L15
	movq	32(%r9), %rsi
	cmpl	$15, %edx
	je	.L33
.L26:
	movq	32(%rcx), %rdx
	cmpl	$15, %eax
	je	.L34
.L27:
	cmpq	%rdx, %rsi
	jne	.L15
	movq	(%rsi), %rax
	cmpw	$2, 16(%rax)
	jne	.L15
	movq	(%r9), %rax
	movzwl	16(%rax), %eax
	cmpl	$4, %eax
	je	.L35
	cmpl	$5, %eax
	jne	.L15
	movq	(%rcx), %rax
	cmpw	$4, 16(%rax)
	jne	.L15
	movq	%rsi, %xmm0
	movq	%rcx, %xmm2
	movq	%r9, 24(%rdi)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	32(%r9), %rsi
	movl	8(%rsi), %esi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L31:
	movq	16(%rcx), %r9
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L34:
	movq	16(%rdx), %rdx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L33:
	movq	16(%rsi), %rsi
	jmp	.L26
.L35:
	movq	(%rcx), %rax
	cmpw	$5, 16(%rax)
	jne	.L15
	movq	%rsi, %xmm0
	movq	%r9, %xmm1
	movq	%rcx, 24(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE11083:
	.size	_ZN2v88internal8compiler14DiamondMatcherC2EPNS1_4NodeE, .-_ZN2v88internal8compiler14DiamondMatcherC2EPNS1_4NodeE
	.globl	_ZN2v88internal8compiler14DiamondMatcherC1EPNS1_4NodeE
	.set	_ZN2v88internal8compiler14DiamondMatcherC1EPNS1_4NodeE,_ZN2v88internal8compiler14DiamondMatcherC2EPNS1_4NodeE
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal8compiler11NodeMatcher12IsComparisonEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal8compiler11NodeMatcher12IsComparisonEv, @function
_GLOBAL__sub_I__ZNK2v88internal8compiler11NodeMatcher12IsComparisonEv:
.LFB12714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12714:
	.size	_GLOBAL__sub_I__ZNK2v88internal8compiler11NodeMatcher12IsComparisonEv, .-_GLOBAL__sub_I__ZNK2v88internal8compiler11NodeMatcher12IsComparisonEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal8compiler11NodeMatcher12IsComparisonEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
