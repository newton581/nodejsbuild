	.file	"bytecode-liveness-map.cc"
	.text
	.section	.text._ZN2v88internal8compiler16BytecodeLivenessC2EiPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16BytecodeLivenessC2EiPNS0_4ZoneE
	.type	_ZN2v88internal8compiler16BytecodeLivenessC2EiPNS0_4ZoneE, @function
_ZN2v88internal8compiler16BytecodeLivenessC2EiPNS0_4ZoneE:
.LFB4099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	movq	16(%rdx), %r12
	movq	24(%rdx), %rax
	subq	%r12, %rax
	cmpq	$23, %rax
	jbe	.L29
	leaq	24(%r12), %rax
	movq	%rax, 16(%rdx)
.L3:
	leal	1(%rbx), %r15d
	movl	%r15d, 8(%r12)
	cmpl	$64, %r15d
	jle	.L30
	movq	$0, 16(%r12)
	sarl	$6, %ebx
	addl	$1, %ebx
	movl	%ebx, 12(%r12)
	movq	16(%r13), %rax
	movslq	%ebx, %rsi
	movq	24(%r13), %rdx
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L31
	addq	%rax, %rsi
	movq	%rsi, 16(%r13)
.L7:
	movl	12(%r12), %edx
	movq	%rax, 16(%r12)
	cmpl	$1, %edx
	je	.L32
	testl	%edx, %edx
	jle	.L5
	movq	$0, (%rax)
	cmpl	$1, 12(%r12)
	movl	$8, %edx
	movl	$1, %eax
	jle	.L5
	.p2align 4,,10
	.p2align 3
.L9:
	movq	16(%r12), %rcx
	addl	$1, %eax
	movq	$0, (%rcx,%rdx)
	addq	$8, %rdx
	cmpl	%eax, 12(%r12)
	jg	.L9
.L5:
	movq	%r12, (%r14)
	movq	24(%r13), %rax
	movq	16(%r13), %r12
	subq	%r12, %rax
	cmpq	$23, %rax
	jbe	.L33
	leaq	24(%r12), %rax
	movq	%rax, 16(%r13)
.L12:
	movl	%r15d, 8(%r12)
	movl	%ebx, 12(%r12)
	movq	$0, 16(%r12)
	cmpl	$1, %ebx
	je	.L17
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	movslq	%ebx, %rsi
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L34
	addq	%rax, %rsi
	movq	%rsi, 16(%r13)
.L15:
	movl	12(%r12), %edx
	movq	%rax, 16(%r12)
	cmpl	$1, %edx
	je	.L35
	testl	%edx, %edx
	jle	.L17
	movq	$0, (%rax)
	cmpl	$1, 12(%r12)
	movl	$8, %edx
	movl	$1, %eax
	jle	.L17
	.p2align 4,,10
	.p2align 3
.L18:
	movq	16(%r12), %rcx
	addl	$1, %eax
	movq	$0, (%rcx,%rdx)
	addq	$8, %rdx
	cmpl	%eax, 12(%r12)
	jg	.L18
.L17:
	movq	%r12, 8(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	$1, 12(%r12)
	movl	$1, %ebx
	movq	$0, 16(%r12)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L32:
	movq	$0, 16(%r12)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L35:
	movq	$0, 16(%r12)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$24, %esi
	movq	%rdx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L15
	.cfi_endproc
.LFE4099:
	.size	_ZN2v88internal8compiler16BytecodeLivenessC2EiPNS0_4ZoneE, .-_ZN2v88internal8compiler16BytecodeLivenessC2EiPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler16BytecodeLivenessC1EiPNS0_4ZoneE
	.set	_ZN2v88internal8compiler16BytecodeLivenessC1EiPNS0_4ZoneE,_ZN2v88internal8compiler16BytecodeLivenessC2EiPNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler19BytecodeLivenessMapC2EiPNS0_4ZoneE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v88internal8compiler19BytecodeLivenessMapC2EiPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19BytecodeLivenessMapC2EiPNS0_4ZoneE
	.type	_ZN2v88internal8compiler19BytecodeLivenessMapC2EiPNS0_4ZoneE, @function
_ZN2v88internal8compiler19BytecodeLivenessMapC2EiPNS0_4ZoneE:
.LFB4102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leal	3(%rsi), %edi
	subq	$8, %rsp
	testl	%esi, %esi
	cmovns	%esi, %edi
	sarl	$2, %edi
	addl	$1, %edi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej@PLT
	movq	24(%r12), %rdx
	movl	%eax, %r15d
	movq	16(%r12), %rax
	movq	%r15, %r14
	movq	%r15, %r13
	salq	$5, %r14
	subq	%rax, %rdx
	movq	%r14, %rsi
	cmpq	%rdx, %r14
	ja	.L47
	addq	%rax, %rsi
	movq	%rsi, 16(%r12)
.L38:
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L48
	movl	%r13d, 8(%rbx)
	testq	%r15, %r15
	je	.L40
	addq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L41:
	movb	$0, 28(%rax)
	addq	$32, %rax
	cmpq	%rax, %r14
	jne	.L41
.L40:
	movl	$0, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L38
.L48:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4102:
	.size	_ZN2v88internal8compiler19BytecodeLivenessMapC2EiPNS0_4ZoneE, .-_ZN2v88internal8compiler19BytecodeLivenessMapC2EiPNS0_4ZoneE
	.globl	_ZN2v88internal8compiler19BytecodeLivenessMapC1EiPNS0_4ZoneE
	.set	_ZN2v88internal8compiler19BytecodeLivenessMapC1EiPNS0_4ZoneE,_ZN2v88internal8compiler19BytecodeLivenessMapC2EiPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler10OffsetHashEi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compiler10OffsetHashEi
	.type	_ZN2v88internal8compiler10OffsetHashEi, @function
_ZN2v88internal8compiler10OffsetHashEi:
.LFB4104:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	ret
	.cfi_endproc
.LFE4104:
	.size	_ZN2v88internal8compiler10OffsetHashEi, .-_ZN2v88internal8compiler10OffsetHashEi
	.section	.text._ZN2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi
	.type	_ZN2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi, @function
_ZN2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi:
.LFB4107:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %ecx
	movl	%esi, %edx
	movq	(%rdi), %r8
	subl	$1, %ecx
	andl	%ecx, %edx
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L56:
	cmpl	%esi, (%rax)
	je	.L51
	addq	$1, %rdx
	andq	%rcx, %rdx
.L55:
	movq	%rdx, %rax
	salq	$5, %rax
	addq	%r8, %rax
	cmpb	$0, 28(%rax)
	jne	.L56
	xorl	%eax, %eax
.L51:
	addq	$8, %rax
	ret
	.cfi_endproc
.LFE4107:
	.size	_ZN2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi, .-_ZN2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi
	.section	.text._ZNK2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi
	.type	_ZNK2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi, @function
_ZNK2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi:
.LFB4730:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %ecx
	movl	%esi, %edx
	movq	(%rdi), %r8
	subl	$1, %ecx
	andl	%ecx, %edx
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L63:
	cmpl	(%rax), %esi
	je	.L58
	addq	$1, %rdx
	andq	%rcx, %rdx
.L62:
	movq	%rdx, %rax
	salq	$5, %rax
	addq	%r8, %rax
	cmpb	$0, 28(%rax)
	jne	.L63
	xorl	%eax, %eax
.L58:
	addq	$8, %rax
	ret
	.cfi_endproc
.LFE4730:
	.size	_ZNK2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi, .-_ZNK2v88internal8compiler19BytecodeLivenessMap11GetLivenessEi
	.section	.text._ZN2v84base19TemplateHashMapImplIiNS_8internal8compiler16BytecodeLivenessENS0_18KeyEqualityMatcherIiEENS2_20ZoneAllocationPolicyEE6ResizeES7_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplIiNS_8internal8compiler16BytecodeLivenessENS0_18KeyEqualityMatcherIiEENS2_20ZoneAllocationPolicyEE6ResizeES7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplIiNS_8internal8compiler16BytecodeLivenessENS0_18KeyEqualityMatcherIiEENS2_20ZoneAllocationPolicyEE6ResizeES7_
	.type	_ZN2v84base19TemplateHashMapImplIiNS_8internal8compiler16BytecodeLivenessENS0_18KeyEqualityMatcherIiEENS2_20ZoneAllocationPolicyEE6ResizeES7_, @function
_ZN2v84base19TemplateHashMapImplIiNS_8internal8compiler16BytecodeLivenessENS0_18KeyEqualityMatcherIiEENS2_20ZoneAllocationPolicyEE6ResizeES7_:
.LFB4605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	8(%rdi), %eax
	movq	24(%r14), %rdx
	movq	(%rdi), %r12
	movl	12(%rdi), %r13d
	leal	(%rax,%rax), %esi
	movq	16(%r14), %rax
	movq	%rsi, %r15
	salq	$5, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L92
	addq	%rax, %rsi
	movq	%rsi, 16(%r14)
.L66:
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L93
	movl	%r15d, 8(%rbx)
	testl	%r15d, %r15d
	je	.L68
	movb	$0, 28(%rax)
	cmpl	$1, 8(%rbx)
	movl	$1, %eax
	jbe	.L68
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%rax, %rdx
	addq	$1, %rax
	salq	$5, %rdx
	addq	(%rbx), %rdx
	movb	$0, 28(%rdx)
	movl	8(%rbx), %edx
	cmpq	%rax, %rdx
	ja	.L69
.L68:
	movl	$0, 12(%rbx)
	testl	%r13d, %r13d
	je	.L64
.L71:
	cmpb	$0, 28(%r12)
	jne	.L94
.L72:
	addq	$32, %r12
	cmpb	$0, 28(%r12)
	je	.L72
.L94:
	movl	8(%rbx), %eax
	movl	24(%r12), %r15d
	movq	(%rbx), %rdi
	movl	(%r12), %esi
	leal	-1(%rax), %ecx
	movl	%r15d, %edx
	andl	%ecx, %edx
	movq	%rdx, %rax
	salq	$5, %rax
	addq	%rdi, %rax
	cmpb	$0, 28(%rax)
	jne	.L74
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L95:
	addq	$1, %rdx
	andq	%rcx, %rdx
	movq	%rdx, %rax
	salq	$5, %rax
	addq	%rdi, %rax
	cmpb	$0, 28(%rax)
	je	.L73
.L74:
	cmpl	%esi, (%rax)
	jne	.L95
.L73:
	movdqu	8(%r12), %xmm0
	movl	%esi, (%rax)
	movl	%r15d, 24(%rax)
	movb	$1, 28(%rax)
	movups	%xmm0, 8(%rax)
	movl	12(%rbx), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%rbx)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%rbx), %eax
	jnb	.L75
.L78:
	addq	$32, %r12
	subl	$1, %r13d
	jne	.L71
.L64:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v84base19TemplateHashMapImplIiNS_8internal8compiler16BytecodeLivenessENS0_18KeyEqualityMatcherIiEENS2_20ZoneAllocationPolicyEE6ResizeES7_
	movl	8(%rbx), %eax
	movq	(%rbx), %rsi
	leal	-1(%rax), %ecx
	movl	%r15d, %eax
	andl	%ecx, %eax
	movq	%rax, %rdx
	salq	$5, %rdx
	addq	%rsi, %rdx
	cmpb	$0, 28(%rdx)
	je	.L78
	movl	(%r12), %edi
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L96:
	addq	$1, %rax
	andq	%rcx, %rax
	movq	%rax, %rdx
	salq	$5, %rdx
	addq	%rsi, %rdx
	cmpb	$0, 28(%rdx)
	je	.L78
.L79:
	cmpl	(%rdx), %edi
	jne	.L96
	jmp	.L78
.L92:
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L66
.L93:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4605:
	.size	_ZN2v84base19TemplateHashMapImplIiNS_8internal8compiler16BytecodeLivenessENS0_18KeyEqualityMatcherIiEENS2_20ZoneAllocationPolicyEE6ResizeES7_, .-_ZN2v84base19TemplateHashMapImplIiNS_8internal8compiler16BytecodeLivenessENS0_18KeyEqualityMatcherIiEENS2_20ZoneAllocationPolicyEE6ResizeES7_
	.section	.text._ZN2v88internal8compiler19BytecodeLivenessMap18InitializeLivenessEiiPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19BytecodeLivenessMap18InitializeLivenessEiiPNS0_4ZoneE
	.type	_ZN2v88internal8compiler19BytecodeLivenessMap18InitializeLivenessEiiPNS0_4ZoneE, @function
_ZN2v88internal8compiler19BytecodeLivenessMap18InitializeLivenessEiiPNS0_4ZoneE:
.LFB4105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	movl	%edx, %esi
	pushq	%rbx
	movl	%r12d, %edx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	0(%r13), %r8
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	8(%rdi), %eax
	leal	-1(%rax), %edi
	andl	%edi, %edx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L111:
	cmpl	%r12d, (%rbx)
	je	.L99
	addq	$1, %rdx
	andq	%rdi, %rdx
.L109:
	movq	%rdx, %rbx
	salq	$5, %rbx
	addq	%r8, %rbx
	cmpb	$0, 28(%rbx)
	jne	.L111
	movq	%r14, %rdx
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal8compiler16BytecodeLivenessC1EiPNS0_4ZoneE
	movdqa	-64(%rbp), %xmm0
	movl	%r12d, 24(%rbx)
	movl	%r12d, (%rbx)
	movb	$1, 28(%rbx)
	movups	%xmm0, 8(%rbx)
	movl	12(%r13), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r13)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r13), %eax
	jnb	.L112
.L99:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	leaq	8(%rbx), %rax
	jne	.L113
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v84base19TemplateHashMapImplIiNS_8internal8compiler16BytecodeLivenessENS0_18KeyEqualityMatcherIiEENS2_20ZoneAllocationPolicyEE6ResizeES7_
	movl	8(%r13), %eax
	movl	%r12d, %edx
	movq	0(%r13), %rcx
	subl	$1, %eax
	andl	%eax, %edx
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L114:
	cmpl	%r12d, (%rbx)
	je	.L99
	addq	$1, %rdx
	andq	%rax, %rdx
.L110:
	movq	%rdx, %rbx
	salq	$5, %rbx
	addq	%rcx, %rbx
	cmpb	$0, 28(%rbx)
	jne	.L114
	jmp	.L99
.L113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4105:
	.size	_ZN2v88internal8compiler19BytecodeLivenessMap18InitializeLivenessEiiPNS0_4ZoneE, .-_ZN2v88internal8compiler19BytecodeLivenessMap18InitializeLivenessEiiPNS0_4ZoneE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler16BytecodeLivenessC2EiPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler16BytecodeLivenessC2EiPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler16BytecodeLivenessC2EiPNS0_4ZoneE:
.LFB4717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE4717:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler16BytecodeLivenessC2EiPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal8compiler16BytecodeLivenessC2EiPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler16BytecodeLivenessC2EiPNS0_4ZoneE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
