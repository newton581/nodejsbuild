	.file	"operator.cc"
	.text
	.section	.text._ZNK2v88internal8compiler8Operator6EqualsEPKS2_,"axG",@progbits,_ZNK2v88internal8compiler8Operator6EqualsEPKS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler8Operator6EqualsEPKS2_
	.type	_ZNK2v88internal8compiler8Operator6EqualsEPKS2_, @function
_ZNK2v88internal8compiler8Operator6EqualsEPKS2_:
.LFB4460:
	.cfi_startproc
	endbr64
	movzwl	16(%rsi), %eax
	cmpw	%ax, 16(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE4460:
	.size	_ZNK2v88internal8compiler8Operator6EqualsEPKS2_, .-_ZNK2v88internal8compiler8Operator6EqualsEPKS2_
	.section	.text._ZNK2v88internal8compiler8Operator8HashCodeEv,"axG",@progbits,_ZNK2v88internal8compiler8Operator8HashCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler8Operator8HashCodeEv
	.type	_ZNK2v88internal8compiler8Operator8HashCodeEv, @function
_ZNK2v88internal8compiler8Operator8HashCodeEv:
.LFB4461:
	.cfi_startproc
	endbr64
	movzwl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE4461:
	.size	_ZNK2v88internal8compiler8Operator8HashCodeEv, .-_ZNK2v88internal8compiler8Operator8HashCodeEv
	.section	.text._ZN2v88internal8compiler8OperatorD2Ev,"axG",@progbits,_ZN2v88internal8compiler8OperatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler8OperatorD2Ev
	.type	_ZN2v88internal8compiler8OperatorD2Ev, @function
_ZN2v88internal8compiler8OperatorD2Ev:
.LFB5214:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5214:
	.size	_ZN2v88internal8compiler8OperatorD2Ev, .-_ZN2v88internal8compiler8OperatorD2Ev
	.weak	_ZN2v88internal8compiler8OperatorD1Ev
	.set	_ZN2v88internal8compiler8OperatorD1Ev,_ZN2v88internal8compiler8OperatorD2Ev
	.section	.rodata._ZN2v88internal8compiler8OperatorD0Ev.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler8OperatorD0Ev,"axG",@progbits,_ZN2v88internal8compiler8OperatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler8OperatorD0Ev
	.type	_ZN2v88internal8compiler8OperatorD0Ev, @function
_ZN2v88internal8compiler8OperatorD0Ev:
.LFB5216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5216:
	.size	_ZN2v88internal8compiler8OperatorD0Ev, .-_ZN2v88internal8compiler8OperatorD0Ev
	.section	.text._ZNK2v88internal8compiler8Operator11PrintToImplERSoNS2_14PrintVerbosityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler8Operator11PrintToImplERSoNS2_14PrintVerbosityE
	.type	_ZNK2v88internal8compiler8Operator11PrintToImplERSoNS2_14PrintVerbosityE, @function
_ZNK2v88internal8compiler8Operator11PrintToImplERSoNS2_14PrintVerbosityE:
.LFB4510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rsi, %r12
	testq	%r13, %r13
	je	.L10
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	(%rsi), %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	-24(%rax), %rdi
	addq	%rsi, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	jmp	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	.cfi_endproc
.LFE4510:
	.size	_ZNK2v88internal8compiler8Operator11PrintToImplERSoNS2_14PrintVerbosityE, .-_ZNK2v88internal8compiler8Operator11PrintToImplERSoNS2_14PrintVerbosityE
	.section	.rodata._ZN2v88internal8compiler8OperatorC2EtNS_4base5FlagsINS2_8PropertyEhEEPKcmmmmmm.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"val <= std::min(static_cast<size_t>(std::numeric_limits<N>::max()), static_cast<size_t>(kMaxInt))"
	.section	.rodata._ZN2v88internal8compiler8OperatorC2EtNS_4base5FlagsINS2_8PropertyEhEEPKcmmmmmm.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler8OperatorC2EtNS_4base5FlagsINS2_8PropertyEhEEPKcmmmmmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8OperatorC2EtNS_4base5FlagsINS2_8PropertyEhEEPKcmmmmmm
	.type	_ZN2v88internal8compiler8OperatorC2EtNS_4base5FlagsINS2_8PropertyEhEEPKcmmmmmm, @function
_ZN2v88internal8compiler8OperatorC2EtNS_4base5FlagsINS2_8PropertyEhEEPKcmmmmmm:
.LFB4507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler8OperatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	movq	32(%rbp), %r11
	.cfi_offset 13, -24
	movq	16(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rax, (%rdi)
	movl	$2147483648, %eax
	movq	24(%rbp), %r12
	movq	%rcx, 8(%rdi)
	movq	40(%rbp), %r10
	movw	%si, 16(%rdi)
	movb	%dl, 18(%rdi)
	cmpq	%rax, %r8
	jnb	.L13
	movl	%r8d, 20(%rdi)
	cmpq	%rax, %r9
	jnb	.L13
	movl	%r9d, 24(%rdi)
	cmpq	%rax, %r13
	jnb	.L13
	movl	%r13d, 28(%rdi)
	cmpq	%rax, %r12
	jnb	.L13
	movl	%r12d, 32(%rdi)
	cmpq	$255, %r11
	ja	.L13
	movb	%r11b, 36(%rdi)
	cmpq	%rax, %r10
	jnb	.L13
	movl	%r10d, 40(%rdi)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4507:
	.size	_ZN2v88internal8compiler8OperatorC2EtNS_4base5FlagsINS2_8PropertyEhEEPKcmmmmmm, .-_ZN2v88internal8compiler8OperatorC2EtNS_4base5FlagsINS2_8PropertyEhEEPKcmmmmmm
	.globl	_ZN2v88internal8compiler8OperatorC1EtNS_4base5FlagsINS2_8PropertyEhEEPKcmmmmmm
	.set	_ZN2v88internal8compiler8OperatorC1EtNS_4base5FlagsINS2_8PropertyEhEEPKcmmmmmm,_ZN2v88internal8compiler8OperatorC2EtNS_4base5FlagsINS2_8PropertyEhEEPKcmmmmmm
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_8OperatorE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE, @function
_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE:
.LFB4509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal8compiler8Operator11PrintToImplERSoNS2_14PrintVerbosityE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rsi), %rax
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L16
	movq	8(%rsi), %r13
	testq	%r13, %r13
	je	.L20
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	%r12, %rsi
	xorl	%edx, %edx
	call	*%rax
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4509:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE, .-_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE
	.section	.rodata._ZNK2v88internal8compiler8Operator12PrintPropsToERSo.str1.1,"aMS",@progbits,1
.LC3:
	.string	"Commutative"
.LC4:
	.string	", "
.LC5:
	.string	"Associative"
.LC6:
	.string	"Idempotent"
.LC7:
	.string	"NoRead"
.LC8:
	.string	"NoWrite"
.LC9:
	.string	"NoThrow"
.LC10:
	.string	"NoDeopt"
	.section	.text._ZNK2v88internal8compiler8Operator12PrintPropsToERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler8Operator12PrintPropsToERSo
	.type	_ZNK2v88internal8compiler8Operator12PrintPropsToERSo, @function
_ZNK2v88internal8compiler8Operator12PrintPropsToERSo:
.LFB4511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	-64(%rbp), %rbx
	movq	%rbx, %rsi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	18(%rdi), %eax
	movq	%rbx, -80(%rbp)
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	testb	$1, %al
	jne	.L52
	testb	$2, %al
	jne	.L53
.L23:
	testb	$4, %al
	jne	.L54
.L24:
	testb	$8, %al
	jne	.L55
.L25:
	testb	$16, %al
	jne	.L56
.L26:
	testb	$32, %al
	jne	.L57
.L27:
	testb	$64, %al
	jne	.L58
.L28:
	cmpq	%rbx, %rsi
	je	.L21
	movq	%rsi, %rdi
	call	_ZdlPv@PLT
.L21:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$11, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$2, %r8d
	leaq	.LC4(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movzbl	18(%r14), %eax
	movq	-80(%rbp), %rsi
	testb	$2, %al
	je	.L23
.L53:
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$11, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$2, %r8d
	leaq	.LC4(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movzbl	18(%r14), %eax
	movq	-80(%rbp), %rsi
	testb	$4, %al
	je	.L24
.L54:
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$10, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$2, %r8d
	leaq	.LC4(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movzbl	18(%r14), %eax
	movq	-80(%rbp), %rsi
	testb	$8, %al
	je	.L25
.L55:
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$6, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$2, %r8d
	leaq	.LC4(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movzbl	18(%r14), %eax
	movq	-80(%rbp), %rsi
	testb	$16, %al
	je	.L26
.L56:
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$7, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$2, %r8d
	leaq	.LC4(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movzbl	18(%r14), %eax
	movq	-80(%rbp), %rsi
	testb	$32, %al
	je	.L27
.L57:
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$7, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$2, %r8d
	leaq	.LC4(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movzbl	18(%r14), %eax
	movq	-80(%rbp), %rsi
	testb	$64, %al
	je	.L28
.L58:
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$7, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$2, %r8d
	leaq	.LC4(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-80(%rbp), %rsi
	jmp	.L28
.L59:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4511:
	.size	_ZNK2v88internal8compiler8Operator12PrintPropsToERSo, .-_ZNK2v88internal8compiler8Operator12PrintPropsToERSo
	.weak	_ZTVN2v88internal8compiler8OperatorE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler8OperatorE,"awG",@progbits,_ZTVN2v88internal8compiler8OperatorE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler8OperatorE, @object
	.size	_ZTVN2v88internal8compiler8OperatorE, 56
_ZTVN2v88internal8compiler8OperatorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler8OperatorD1Ev
	.quad	_ZN2v88internal8compiler8OperatorD0Ev
	.quad	_ZNK2v88internal8compiler8Operator6EqualsEPKS2_
	.quad	_ZNK2v88internal8compiler8Operator8HashCodeEv
	.quad	_ZNK2v88internal8compiler8Operator11PrintToImplERSoNS2_14PrintVerbosityE
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
