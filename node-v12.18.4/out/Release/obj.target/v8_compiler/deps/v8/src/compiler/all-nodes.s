	.file	"all-nodes.cc"
	.text
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB11303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L40
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L17
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L41
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L3:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L42
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L6:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L41:
	testq	%rdx, %rdx
	jne	.L43
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L4:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L7
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L20
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L20
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L9:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L9
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L11
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L11:
	leaq	16(%rax,%r8), %rcx
.L7:
	cmpq	%r14, %r12
	je	.L12
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L21
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L21
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L14:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L14
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L16
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L16:
	leaq	8(%rcx,%r9), %rcx
.L12:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L13:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L13
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L20:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L8:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L8
	jmp	.L11
.L42:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L6
.L40:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L43:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L3
	.cfi_endproc
.LFE11303:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal8compiler8AllNodes4MarkEPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8AllNodes4MarkEPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphE
	.type	_ZN2v88internal8compiler8AllNodes4MarkEPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphE, @function
_ZN2v88internal8compiler8AllNodes4MarkEPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphE:
.LFB10160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	20(%rdx), %ecx
	movq	%rdx, -72(%rbp)
	movl	$1, %edx
	movq	%rcx, %rax
	salq	%cl, %rdx
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	shrq	$3, %rax
	andl	$2097144, %eax
	addq	40(%rdi), %rax
	orq	%rdx, (%rax)
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L45
	movq	-72(%rbp), %rax
	movq	%rax, (%rsi)
	movq	16(%rdi), %rax
	addq	$8, %rax
	movq	%rax, 16(%rdi)
.L46:
	movq	8(%rdi), %rdx
	xorl	%r14d, %r14d
	movl	$1, %r13d
	cmpq	%rax, %rdx
	je	.L44
	.p2align 4,,10
	.p2align 3
.L47:
	movq	(%rdx,%r14,8), %rcx
	leaq	0(,%r14,8), %r8
	movzbl	23(%rcx), %eax
	leaq	32(%rcx), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L49
	movq	32(%rcx), %rbx
	movl	8(%rbx), %eax
	addq	$16, %rbx
.L49:
	cltq
	leaq	(%rbx,%rax,8), %r12
	cmpq	%r12, %rbx
	jne	.L55
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L53:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	je	.L80
.L55:
	movq	(%rbx), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L53
	movl	20(%rax), %ecx
	movq	%r13, %rsi
	movq	%rcx, %rax
	salq	%cl, %rsi
	shrq	$3, %rax
	andl	$2097144, %eax
	addq	40(%rdi), %rax
	movq	(%rax), %rdx
	testq	%rsi, %rdx
	jne	.L53
	orq	%rsi, %rdx
	movq	%rdx, (%rax)
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L54
	movq	-64(%rbp), %rax
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, 16(%rdi)
	cmpq	%rbx, %r12
	jne	.L55
.L80:
	movq	8(%rdi), %rdx
.L56:
	cmpb	$0, 80(%rdi)
	je	.L81
.L51:
	movq	16(%rdi), %rax
	addq	$1, %r14
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r14
	jb	.L47
.L44:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	leaq	-64(%rbp), %rdx
	movq	%r8, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rdi
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L81:
	movq	(%rdx,%r8), %rax
	movq	24(%rax), %rbx
	testq	%rbx, %rbx
	jne	.L63
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L85:
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L60
.L59:
	movl	20(%rax), %ecx
	movl	%ecx, %eax
	andl	$16777215, %eax
	cmpl	%eax, 28(%r15)
	jbe	.L60
	shrq	$3, %rax
	movq	%r13, %rsi
	andl	$2097144, %eax
	addq	40(%rdi), %rax
	salq	%cl, %rsi
	movq	(%rax), %rdx
	testq	%rsi, %rdx
	je	.L83
.L60:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L84
.L63:
	movl	16(%rbx), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rax
	je	.L85
	movq	%rax, -64(%rbp)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L83:
	orq	%rsi, %rdx
	movq	%rdx, (%rax)
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L62
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 16(%rdi)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L63
	.p2align 4,,10
	.p2align 3
.L84:
	movq	8(%rdi), %rdx
	jmp	.L51
.L62:
	leaq	-64(%rbp), %rdx
	movq	%rdi, -80(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-80(%rbp), %rdi
	jmp	.L60
.L45:
	leaq	-72(%rbp), %rdx
	movq	%rdi, -80(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-80(%rbp), %rdi
	movq	16(%rdi), %rax
	jmp	.L46
.L82:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10160:
	.size	_ZN2v88internal8compiler8AllNodes4MarkEPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphE, .-_ZN2v88internal8compiler8AllNodes4MarkEPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphE
	.section	.text._ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPKNS1_5GraphEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPKNS1_5GraphEb
	.type	_ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPKNS1_5GraphEb, @function
_ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPKNS1_5GraphEb:
.LFB10155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movl	28(%rdx), %ebx
	movq	%rsi, 32(%rdi)
	movq	$0, 40(%rdi)
	movl	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movl	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	testq	%rbx, %rbx
	jne	.L98
.L90:
	movb	%cl, 80(%r12)
	movq	16(%r14), %rdx
	addq	$24, %rsp
	movq	%r14, %rcx
	popq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler8AllNodes4MarkEPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphE
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	16(%r13), %rdi
	movq	24(%r13), %rax
	leaq	63(%rbx), %rdx
	movq	%rbx, %r15
	shrq	$6, %rdx
	salq	$3, %rdx
	subq	%rdi, %rax
	movq	%rdx, %rsi
	cmpq	%rax, %rdx
	ja	.L99
	addq	%rdi, %rsi
	movq	%rsi, 16(%r13)
.L89:
	leaq	(%rdi,%rdx), %rax
	sarq	$6, %rbx
	andl	$63, %r15d
	movq	%rdi, 40(%r12)
	movq	%rax, 72(%r12)
	leaq	(%rdi,%rbx,8), %rax
	movl	$0, 48(%r12)
	movq	%rax, 56(%r12)
	movl	%r15d, 64(%r12)
	testq	%rdi, %rdi
	je	.L90
	xorl	%esi, %esi
	movl	%ecx, -56(%rbp)
	call	memset@PLT
	movl	-56(%rbp), %ecx
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%r13, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movq	%rax, %rdi
	jmp	.L89
	.cfi_endproc
.LFE10155:
	.size	_ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPKNS1_5GraphEb, .-_ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPKNS1_5GraphEb
	.globl	_ZN2v88internal8compiler8AllNodesC1EPNS0_4ZoneEPKNS1_5GraphEb
	.set	_ZN2v88internal8compiler8AllNodesC1EPNS0_4ZoneEPKNS1_5GraphEb,_ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPKNS1_5GraphEb
	.section	.text._ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphEb
	.type	_ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphEb, @function
_ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphEb:
.LFB10158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movl	28(%rcx), %ebx
	movq	%rsi, 32(%rdi)
	movq	$0, 40(%rdi)
	movl	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movl	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	testq	%rbx, %rbx
	jne	.L112
.L104:
	movb	%r8b, 80(%r12)
	addq	$40, %rsp
	movq	%r14, %rcx
	movq	%r13, %rsi
	popq	%rbx
	movq	%r12, %rdi
	movq	%r9, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler8AllNodes4MarkEPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphE
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	16(%r13), %rdi
	movq	24(%r13), %rax
	leaq	63(%rbx), %rdx
	movq	%rbx, %r15
	shrq	$6, %rdx
	salq	$3, %rdx
	subq	%rdi, %rax
	movq	%rdx, %rsi
	cmpq	%rax, %rdx
	ja	.L113
	addq	%rdi, %rsi
	movq	%rsi, 16(%r13)
.L103:
	leaq	(%rdi,%rdx), %rax
	sarq	$6, %rbx
	andl	$63, %r15d
	movq	%rdi, 40(%r12)
	movq	%rax, 72(%r12)
	leaq	(%rdi,%rbx,8), %rax
	movl	$0, 48(%r12)
	movq	%rax, 56(%r12)
	movl	%r15d, 64(%r12)
	testq	%rdi, %rdi
	je	.L104
	xorl	%esi, %esi
	movl	%r8d, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	memset@PLT
	movl	-64(%rbp), %r8d
	movq	-56(%rbp), %r9
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r13, %rdi
	movl	%r8d, -68(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r9
	movl	-68(%rbp), %r8d
	movq	%rax, %rdi
	jmp	.L103
	.cfi_endproc
.LFE10158:
	.size	_ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphEb, .-_ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphEb
	.globl	_ZN2v88internal8compiler8AllNodesC1EPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphEb
	.set	_ZN2v88internal8compiler8AllNodesC1EPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphEb,_ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPNS1_4NodeEPKNS1_5GraphEb
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPKNS1_5GraphEb,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPKNS1_5GraphEb, @function
_GLOBAL__sub_I__ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPKNS1_5GraphEb:
.LFB11889:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11889:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPKNS1_5GraphEb, .-_GLOBAL__sub_I__ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPKNS1_5GraphEb
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler8AllNodesC2EPNS0_4ZoneEPKNS1_5GraphEb
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
