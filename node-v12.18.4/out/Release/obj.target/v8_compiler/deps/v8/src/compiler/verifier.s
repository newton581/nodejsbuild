	.file	"verifier.cc"
	.text
	.section	.rodata._ZN2v88internal8compilerL19CheckInputsDominateEPNS1_8ScheduleEPNS1_10BasicBlockEPNS1_4NodeEi.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Node #%d:%s in B%d is not dominated by control input #%d:%s"
	.align 8
.LC1:
	.string	"Node #%d:%s in B%d is not dominated by input@%d #%d:%s"
	.section	.text._ZN2v88internal8compilerL19CheckInputsDominateEPNS1_8ScheduleEPNS1_10BasicBlockEPNS1_4NodeEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compilerL19CheckInputsDominateEPNS1_8ScheduleEPNS1_10BasicBlockEPNS1_4NodeEi, @function
_ZN2v88internal8compilerL19CheckInputsDominateEPNS1_8ScheduleEPNS1_10BasicBlockEPNS1_4NodeEi:
.LFB10902:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rdx), %r14
	movq	%rdi, -64(%rbp)
	movl	20(%r14), %r8d
	subl	$1, %r8d
	js	.L2
	movl	20(%r12), %r15d
	movslq	%r8d, %r9
	movzwl	16(%r14), %edx
	leaq	32(%r12), %r13
	salq	$3, %r9
	movl	%r15d, %r11d
	shrl	$24, %r11d
	andl	$15, %r11d
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rbx, %rsi
	cmpw	$35, %dx
	jne	.L3
	movq	136(%rbx), %rax
	movq	(%rax,%r9), %rsi
	movq	80(%rsi), %rcx
	subq	72(%rsi), %rcx
	sarq	$3, %rcx
	subl	$1, %ecx
.L3:
	leaq	(%r9,%r13), %rax
	cmpl	$15, %r11d
	jne	.L5
	movq	0(%r13), %rax
	leaq	16(%rax,%r9), %rax
.L5:
	movw	%dx, -50(%rbp)
	movq	(%rax), %r10
	movl	%ecx, %eax
	.p2align 4,,10
	.p2align 3
.L6:
	testl	%eax, %eax
	js	.L7
.L31:
	movq	72(%rsi), %rdx
	movslq	%eax, %rdi
	cmpq	(%rdx,%rdi,8), %r10
	je	.L27
	subl	$1, %eax
	testl	%eax, %eax
	jns	.L31
.L7:
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L10
	movq	80(%rsi), %rax
	subq	72(%rsi), %rax
	sarq	$3, %rax
	subl	$1, %eax
	cmpq	56(%rsi), %r10
	jne	.L6
.L27:
	subl	$1, %r8d
	movzwl	-50(%rbp), %edx
	subq	$8, %r9
	cmpl	$-1, %r8d
	jne	.L15
.L2:
	cmpl	$1, 28(%r14)
	je	.L32
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	cmpw	$22, 16(%r14)
	je	.L1
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-64(%rbp), %r15
	movq	%rax, %rsi
	movq	%rax, %r14
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	testq	%rax, %rax
	je	.L12
	.p2align 4,,10
	.p2align 3
.L26:
	cmpq	%rax, %r13
	je	.L1
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L26
.L12:
	movq	(%r14), %rax
	movq	(%r12), %rdx
	leaq	.LC0(%rip), %rdi
	movl	20(%r14), %r8d
	movl	20(%r12), %esi
	movq	8(%rax), %r9
	movl	4(%rbx), %ecx
	xorl	%eax, %eax
	movq	8(%rdx), %rdx
	andl	$16777215, %esi
	andl	$16777215, %r8d
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	movq	(%r10), %rax
	movl	20(%r10), %r9d
	subq	$8, %rsp
	movl	%r15d, %esi
	movq	8(%r14), %rdx
	movl	4(%rbx), %ecx
	andl	$16777215, %esi
	leaq	.LC1(%rip), %rdi
	pushq	8(%rax)
	andl	$16777215, %r9d
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10902:
	.size	_ZN2v88internal8compilerL19CheckInputsDominateEPNS1_8ScheduleEPNS1_10BasicBlockEPNS1_4NodeEi, .-_ZN2v88internal8compilerL19CheckInputsDominateEPNS1_8ScheduleEPNS1_10BasicBlockEPNS1_4NodeEi
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor13CheckNotTypedEPNS1_4NodeE.part.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"TypeError: node #"
.LC3:
	.string	":"
.LC4:
	.string	" should never have a type"
.LC5:
	.string	"%s"
	.section	.text._ZN2v88internal8compiler8Verifier7Visitor13CheckNotTypedEPNS1_4NodeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler8Verifier7Visitor13CheckNotTypedEPNS1_4NodeE.part.0, @function
_ZN2v88internal8compiler8Verifier7Visitor13CheckNotTypedEPNS1_4NodeE.part.0:
.LFB13120:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-304(%rbp), %r13
	leaq	-416(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$424, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -304(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movw	%ax, -80(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -72(%rbp)
	movq	%rax, -416(%rbp)
	movups	%xmm0, -56(%rbp)
	movq	$0, -88(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -416(%rbp,%rax)
	movq	-416(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	leaq	24+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm1
	leaq	-352(%rbp), %rdi
	movq	%rax, %xmm2
	movaps	%xmm0, -400(%rbp)
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm0, -384(%rbp)
	movaps	%xmm1, -416(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-408(%rbp), %rsi
	movq	%rax, -408(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, -336(%rbp)
	movl	$16, -344(%rbp)
	movq	$0, -328(%rbp)
	movb	$0, -320(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%rbx), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$25, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-432(%rbp), %rax
	movq	$0, -440(%rbp)
	leaq	-448(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-368(%rbp), %rax
	movb	$0, -432(%rbp)
	testq	%rax, %rax
	je	.L34
	movq	-384(%rbp), %r8
	movq	-376(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L39
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L36:
	movq	-448(%rbp), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L39:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L36
.L34:
	leaq	-336(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L36
	.cfi_endproc
.LFE13120:
	.size	_ZN2v88internal8compiler8Verifier7Visitor13CheckNotTypedEPNS1_4NodeE.part.0, .-_ZN2v88internal8compiler8Verifier7Visitor13CheckNotTypedEPNS1_4NodeE.part.0
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE.str1.1,"aMS",@progbits,1
.LC6:
	.string	" type "
.LC7:
	.string	" is not "
	.section	.text._ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE,"axG",@progbits,_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	.type	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE, @function
_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE:
.LFB10885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$432, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	8(%rdi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.L40
	movq	8(%rsi), %rax
	movq	%rsi, %rbx
	movq	%rdx, %r12
	movq	%rax, -456(%rbp)
	cmpq	%rax, %rdx
	jne	.L47
.L40:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L48
	addq	$432, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	leaq	-456(%rbp), %rdi
	movq	%rdx, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L40
	leaq	-304(%rbp), %r14
	leaq	-416(%rbp), %r13
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -304(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movw	%ax, -80(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -72(%rbp)
	movq	%rax, -416(%rbp)
	movups	%xmm0, -56(%rbp)
	movq	$0, -88(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -416(%rbp,%rax)
	movq	-416(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	leaq	24+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm1
	leaq	-352(%rbp), %rdi
	movq	%rax, %xmm2
	movaps	%xmm0, -400(%rbp)
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm0, -384(%rbp)
	movaps	%xmm1, -416(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	leaq	-408(%rbp), %rsi
	movq	%rax, -408(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, -336(%rbp)
	movl	$16, -344(%rbp)
	movq	$0, -328(%rbp)
	movb	$0, -320(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$17, %edx
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%rbx), %esi
	movq	%r13, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$6, %edx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoNS1_4TypeE@PLT
	movl	$8, %edx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compilerlsERSoNS1_4TypeE@PLT
	leaq	-432(%rbp), %rax
	movq	$0, -440(%rbp)
	leaq	-448(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-368(%rbp), %rax
	movb	$0, -432(%rbp)
	testq	%rax, %rax
	je	.L42
	movq	-384(%rbp), %r8
	movq	-376(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L43
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L44:
	movq	-448(%rbp), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	-336(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L44
.L48:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10885:
	.size	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE, .-_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor14CheckTypeMaybeEPNS1_4NodeENS1_4TypeE.str1.1,"aMS",@progbits,1
.LC8:
	.string	" must intersect "
	.section	.text._ZN2v88internal8compiler8Verifier7Visitor14CheckTypeMaybeEPNS1_4NodeENS1_4TypeE,"axG",@progbits,_ZN2v88internal8compiler8Verifier7Visitor14CheckTypeMaybeEPNS1_4NodeENS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler8Verifier7Visitor14CheckTypeMaybeEPNS1_4NodeENS1_4TypeE
	.type	_ZN2v88internal8compiler8Verifier7Visitor14CheckTypeMaybeEPNS1_4NodeENS1_4TypeE, @function
_ZN2v88internal8compiler8Verifier7Visitor14CheckTypeMaybeEPNS1_4NodeENS1_4TypeE:
.LFB10886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	8(%rdi), %eax
	testl	%eax, %eax
	je	.L53
.L49:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	addq	$440, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	8(%rsi), %rax
	movq	%rsi, %rbx
	leaq	-456(%rbp), %rdi
	movq	%rdx, %rsi
	movq	%rdx, %r12
	movq	%rax, -456(%rbp)
	call	_ZNK2v88internal8compiler4Type5MaybeES2_@PLT
	testb	%al, %al
	jne	.L49
	leaq	-416(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$17, %edx
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%rbx), %esi
	movq	%r13, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$6, %edx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoNS1_4TypeE@PLT
	movl	$16, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoNS1_4TypeE@PLT
	leaq	-448(%rbp), %rdi
	leaq	-408(%rbp), %rsi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	-448(%rbp), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10886:
	.size	_ZN2v88internal8compiler8Verifier7Visitor14CheckTypeMaybeEPNS1_4NodeENS1_4TypeE, .-_ZN2v88internal8compiler8Verifier7Visitor14CheckTypeMaybeEPNS1_4NodeENS1_4TypeE
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"(input @"
.LC10:
	.string	" = "
.LC11:
	.string	") type "
	.section	.text._ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE,"axG",@progbits,_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	.type	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE, @function
_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE:
.LFB10887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	%edx, %esi
	movq	%rbx, %rdi
	subq	$456, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	8(%r12), %edx
	testl	%edx, %edx
	jne	.L55
	movq	%rax, %r15
	movq	8(%rax), %rax
	movq	%rax, -472(%rbp)
	cmpq	%rax, %r14
	jne	.L64
.L55:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	leaq	-472(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L55
	leaq	-320(%rbp), %r8
	leaq	-432(%rbp), %r12
	movq	%r8, %rdi
	movq	%r8, -488(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	%rax, -432(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	leaq	24+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm1
	leaq	-368(%rbp), %rdi
	movq	%rax, %xmm2
	movaps	%xmm0, -416(%rbp)
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-488(%rbp), %r8
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%r8, %rdi
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%rbx), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$8, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$3, %edx
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movq	%r12, %rdi
	movzwl	16(%rax), %esi
	call	_ZN2v88internal8compilerlsERSoNS1_8IrOpcode5ValueE@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movq	8(%rax), %r13
	testq	%r13, %r13
	je	.L66
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L58:
	movl	$7, %edx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoNS1_4TypeE@PLT
	movl	$8, %edx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compilerlsERSoNS1_4TypeE@PLT
	leaq	-448(%rbp), %rax
	movq	$0, -456(%rbp)
	leaq	-464(%rbp), %rdi
	movq	%rax, -464(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L59
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L60
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L61:
	movq	-464(%rbp), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L60:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L66:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L61
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10887:
	.size	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE, .-_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor11CheckOutputEPNS1_4NodeES5_iPKc.str1.1,"aMS",@progbits,1
.LC12:
	.string	"GraphError: node #"
.LC13:
	.string	" does not produce "
.LC14:
	.string	" output used by node #"
	.section	.text._ZN2v88internal8compiler8Verifier7Visitor11CheckOutputEPNS1_4NodeES5_iPKc,"axG",@progbits,_ZN2v88internal8compiler8Verifier7Visitor11CheckOutputEPNS1_4NodeES5_iPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler8Verifier7Visitor11CheckOutputEPNS1_4NodeES5_iPKc
	.type	_ZN2v88internal8compiler8Verifier7Visitor11CheckOutputEPNS1_4NodeES5_iPKc, @function
_ZN2v88internal8compiler8Verifier7Visitor11CheckOutputEPNS1_4NodeES5_iPKc:
.LFB10888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L71
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L72
	addq	$440, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L71:
	.cfi_restore_state
	leaq	-416(%rbp), %r13
	movq	%r8, -456(%rbp)
	movq	%rsi, %r12
	movq	%rdx, %rbx
	movq	%r13, %rdi
	call	_ZNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
	movl	$18, %edx
	movq	%r13, %rdi
	leaq	.LC12(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%r12), %esi
	movq	%r13, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	movl	$18, %edx
	leaq	.LC13(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-456(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	$22, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	20(%rbx), %esi
	movq	%r12, %rdi
	andl	$16777215, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8OperatorE@PLT
	leaq	-448(%rbp), %rdi
	leaq	-408(%rbp), %rsi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	-448(%rbp), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L72:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10888:
	.size	_ZN2v88internal8compiler8Verifier7Visitor11CheckOutputEPNS1_4NodeES5_iPKc, .-_ZN2v88internal8compiler8Verifier7Visitor11CheckOutputEPNS1_4NodeES5_iPKc
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"input_count == node->InputCount()"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"Check failed: %s."
.LC17:
	.string	"only_inputs_"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.8
	.align 8
.LC18:
	.string	"frame_state->opcode() == IrOpcode::kFrameState || (node->opcode() == IrOpcode::kFrameState && frame_state->opcode() == IrOpcode::kStart)"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.1
.LC19:
	.string	"value"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.8
	.align 8
.LC20:
	.string	"node->opcode() == IrOpcode::kParameter || node->opcode() == IrOpcode::kProjection || value->op()->ValueOutputCount() <= 1"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.1
.LC21:
	.string	"context"
.LC22:
	.string	"effect"
.LC23:
	.string	"control"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.8
	.align 8
.LC24:
	.string	"(discovered_if_success) == nullptr"
	.align 8
.LC25:
	.string	"(discovered_if_exception) == nullptr"
	.align 8
.LC26:
	.string	"#%d:%s should be followed by IfSuccess/IfException, but is only followed by single #%d:%s"
	.align 8
.LC27:
	.string	"#%d:%s if followed by IfSuccess/IfException, there should be no direct control uses, but direct use #%d:%s was found"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.1
.LC28:
	.string	"0 == input_count"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.8
	.align 8
.LC29:
	.string	"0 == node->op()->ValueOutputCount()"
	.align 8
.LC30:
	.string	"0 == node->op()->EffectOutputCount()"
	.align 8
.LC31:
	.string	"0 == node->op()->ControlOutputCount()"
	.align 8
.LC32:
	.string	"IrOpcode::IsGraphTerminator(input->opcode())"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.1
.LC33:
	.string	"unreachable code"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.8
	.align 8
.LC34:
	.string	"IrOpcode::kDeadValue == use->opcode()"
	.align 8
.LC35:
	.string	"all.IsLive(use) && (use->opcode() == IrOpcode::kIfTrue || use->opcode() == IrOpcode::kIfFalse)"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.1
.LC36:
	.string	"1 == count_true"
.LC37:
	.string	"1 == count_false"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.8
	.align 8
.LC38:
	.string	"IrOpcode::kBranch == control->opcode()"
	.align 8
.LC39:
	.string	"!input->op()->HasProperty(Operator::kNoThrow)"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.1
.LC40:
	.string	"1 == count_default"
.LC41:
	.string	"all.IsLive(use)"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.8
	.align 8
.LC42:
	.string	"IfValueParametersOf(use->op()).value() != IfValueParametersOf(user->op()).value()"
	.align 8
.LC43:
	.string	"Switch #%d illegally used by #%d:%s"
	.align 8
.LC44:
	.string	"node->op()->ControlOutputCount() == count_case + count_default"
	.align 8
.LC45:
	.string	"IrOpcode::kSwitch == NodeProperties::GetControlInput(node)->opcode()"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.1
.LC46:
	.string	"control_count == input_count"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.8
	.align 8
.LC47:
	.string	"IrOpcode::kEnd == use->opcode()"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.1
.LC48:
	.string	"1 == control_count"
.LC49:
	.string	"1 == effect_count"
.LC50:
	.string	"2 == input_count"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.8
	.align 8
.LC51:
	.string	"IrOpcode::kLoop == NodeProperties::GetControlInput(node)->opcode()"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.1
.LC52:
	.string	"1 == input_count"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.8
	.align 8
.LC53:
	.string	"IrOpcode::kStart == start->opcode()"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.1
.LC54:
	.string	"-1 <= index"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.8
	.align 8
.LC55:
	.string	"index + 1 < start->op()->ValueOutputCount()"
	.align 8
.LC56:
	.string	"input->op()->ValueOutputCount() > index"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.1
.LC57:
	.string	"0 == effect_count"
.LC58:
	.string	"0 == control_count"
.LC59:
	.string	"3 == value_count"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.8
	.align 8
.LC60:
	.string	"value_count == control->op()->ControlInputCount()"
	.align 8
.LC61:
	.string	"input_count == 1 + value_count"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.1
.LC62:
	.string	"0 == value_count"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.8
	.align 8
.LC63:
	.string	"effect_count == control->op()->ControlInputCount()"
	.align 8
.LC64:
	.string	"input_count == 1 + effect_count"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.1
.LC65:
	.string	"non_phi_use_found"
.LC66:
	.string	"2 == control_count"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.8
	.align 8
.LC67:
	.string	"IrOpcode::kLoop == loop->opcode()"
	.align 8
.LC68:
	.string	"IrOpcode::kLoopExit == loop_exit->opcode()"
	.align 8
.LC69:
	.string	"NodeProperties::GetType(val).Is(NodeProperties::GetType(node))"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.1
.LC70:
	.string	"5 == value_count"
.LC71:
	.string	"6 == input_count"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.8
	.align 8
.LC72:
	.string	"NodeProperties::GetValueInput(node, i)->opcode() == IrOpcode::kStateValues || NodeProperties::GetValueInput(node, i)->opcode() == IrOpcode::kTypedStateValues"
	.align 8
.LC75:
	.string	"PropertyAccessOf(node->op()).feedback().IsValid()"
	.align 8
.LC76:
	.string	"LoadGlobalParametersOf(node->op()).feedback().IsValid()"
	.align 8
.LC77:
	.string	"StoreGlobalParametersOf(node->op()).feedback().IsValid()"
	.align 8
.LC78:
	.string	"StoreNamedOwnParametersOf(node->op()).feedback().IsValid()"
	.align 8
.LC79:
	.string	"FeedbackParameterOf(node->op()).feedback().IsValid()"
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE.str1.1
.LC81:
	.string	"has_terminate"
	.section	.text._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE
	.type	_ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE, @function
_ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE:
.LFB10889:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -104(%rbp)
	movq	(%rsi), %rdi
	movl	20(%rdi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18OperatorProperties15HasContextInputEPKNS1_8OperatorE@PLT
	movq	(%r14), %rdi
	movl	%eax, %r15d
	movb	%al, -96(%rbp)
	call	_ZN2v88internal8compiler18OperatorProperties18HasFrameStateInputEPKNS1_8OperatorE@PLT
	movq	(%r14), %rdx
	movzbl	%al, %ecx
	movl	24(%rdx), %eax
	movl	%ecx, %r13d
	movl	%eax, -80(%rbp)
	movl	28(%rdx), %eax
	movl	%eax, -88(%rbp)
	movzbl	%r15b, %eax
	addl	%ebx, %eax
	addl	%ecx, %eax
	cmpl	$1, 12(%r12)
	movl	%eax, -76(%rbp)
	jne	.L74
	movl	-88(%rbp), %eax
	addl	-80(%rbp), %eax
	addl	%eax, -76(%rbp)
.L74:
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L76
	movq	32(%r14), %rax
	movl	8(%rax), %eax
.L76:
	cmpl	%eax, -76(%rbp)
	jne	.L571
	cmpl	$1, 16(%r12)
	je	.L78
	cmpb	$0, 36(%rdx)
	je	.L78
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.L78
	movq	(%rdi), %r15
	movl	%ebx, -72(%rbp)
	movq	%r15, %rbx
	movq	-104(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L87:
	movl	16(%rdi), %ecx
	movzbl	80(%r15), %r9d
	movl	%ecx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rdx
	salq	$3, %rax
	andl	$1, %ecx
	leaq	(%rdi,%rdx,8), %rdx
	je	.L80
	leaq	32(%rdx,%rax), %rsi
	testb	%r9b, %r9b
	je	.L82
.L81:
	movl	20(%rdx), %ecx
	movq	40(%r15), %r9
	movq	56(%r15), %rax
	movl	64(%r15), %r10d
	movl	%ecx, %edx
	subq	%r9, %rax
	andl	$16777215, %edx
	leaq	(%r10,%rax,8), %rax
	cmpq	%rax, %rdx
	jnb	.L85
	movl	$1, %eax
	shrq	$6, %rdx
	salq	%cl, %rax
	testq	%rax, (%r9,%rdx,8)
	je	.L85
	call	_ZN2v88internal8compiler14NodeProperties12IsEffectEdgeENS1_4EdgeE@PLT
.L85:
	testq	%rbx, %rbx
	je	.L546
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L546:
	movl	-72(%rbp), %ebx
.L78:
	testb	%r13b, %r13b
	je	.L91
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movq	(%rax), %rax
	movzwl	16(%rax), %eax
	cmpl	$41, %eax
	jne	.L572
.L91:
	testl	%ebx, %ebx
	jle	.L89
	xorl	%r15d, %r15d
	movl	%ebx, -72(%rbp)
	leaq	.LC19(%rip), %r13
	movl	%r15d, %ebx
.L90:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	%r13, %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	%r15, %rsi
	movl	32(%rax), %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckOutputEPNS1_4NodeES5_iPKc
	movq	(%r14), %rax
	movzwl	16(%rax), %eax
	cmpl	$55, %eax
	je	.L95
	cmpl	$50, %eax
	jne	.L573
.L95:
	addl	$1, %ebx
	cmpl	-72(%rbp), %ebx
	jne	.L90
	movl	-72(%rbp), %ebx
.L89:
	cmpb	$0, -96(%rbp)
	je	.L94
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	leaq	.LC21(%rip), %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	(%rax), %rax
	movl	32(%rax), %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckOutputEPNS1_4NodeES5_iPKc
.L94:
	cmpl	$1, 12(%r12)
	je	.L574
	movq	(%r14), %r8
.L101:
	cmpw	$788, 16(%r8)
	ja	.L73
	movzwl	16(%r8), %eax
	leaq	.L115(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE,"a",@progbits
	.align 4
	.align 4
.L115:
	.long	.L302-.L115
	.long	.L301-.L115
	.long	.L300-.L115
	.long	.L299-.L115
	.long	.L298-.L115
	.long	.L298-.L115
	.long	.L297-.L115
	.long	.L296-.L115
	.long	.L295-.L115
	.long	.L295-.L115
	.long	.L294-.L115
	.long	.L291-.L115
	.long	.L309-.L115
	.long	.L309-.L115
	.long	.L309-.L115
	.long	.L309-.L115
	.long	.L291-.L115
	.long	.L73-.L115
	.long	.L293-.L115
	.long	.L292-.L115
	.long	.L292-.L115
	.long	.L291-.L115
	.long	.L290-.L115
	.long	.L286-.L115
	.long	.L286-.L115
	.long	.L286-.L115
	.long	.L286-.L115
	.long	.L288-.L115
	.long	.L289-.L115
	.long	.L288-.L115
	.long	.L287-.L115
	.long	.L287-.L115
	.long	.L286-.L115
	.long	.L286-.L115
	.long	.L285-.L115
	.long	.L284-.L115
	.long	.L283-.L115
	.long	.L270-.L115
	.long	.L309-.L115
	.long	.L73-.L115
	.long	.L282-.L115
	.long	.L281-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L280-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L279-.L115
	.long	.L278-.L115
	.long	.L277-.L115
	.long	.L276-.L115
	.long	.L276-.L115
	.long	.L274-.L115
	.long	.L309-.L115
	.long	.L309-.L115
	.long	.L273-.L115
	.long	.L272-.L115
	.long	.L271-.L115
	.long	.L270-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L269-.L115
	.long	.L268-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L267-.L115
	.long	.L266-.L115
	.long	.L266-.L115
	.long	.L265-.L115
	.long	.L265-.L115
	.long	.L265-.L115
	.long	.L264-.L115
	.long	.L263-.L115
	.long	.L263-.L115
	.long	.L262-.L115
	.long	.L261-.L115
	.long	.L261-.L115
	.long	.L261-.L115
	.long	.L260-.L115
	.long	.L260-.L115
	.long	.L260-.L115
	.long	.L260-.L115
	.long	.L259-.L115
	.long	.L258-.L115
	.long	.L258-.L115
	.long	.L258-.L115
	.long	.L257-.L115
	.long	.L257-.L115
	.long	.L256-.L115
	.long	.L254-.L115
	.long	.L255-.L115
	.long	.L254-.L115
	.long	.L254-.L115
	.long	.L254-.L115
	.long	.L253-.L115
	.long	.L249-.L115
	.long	.L249-.L115
	.long	.L249-.L115
	.long	.L249-.L115
	.long	.L249-.L115
	.long	.L252-.L115
	.long	.L252-.L115
	.long	.L252-.L115
	.long	.L251-.L115
	.long	.L251-.L115
	.long	.L250-.L115
	.long	.L249-.L115
	.long	.L249-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L248-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L247-.L115
	.long	.L246-.L115
	.long	.L245-.L115
	.long	.L244-.L115
	.long	.L243-.L115
	.long	.L243-.L115
	.long	.L242-.L115
	.long	.L241-.L115
	.long	.L240-.L115
	.long	.L239-.L115
	.long	.L238-.L115
	.long	.L237-.L115
	.long	.L236-.L115
	.long	.L235-.L115
	.long	.L234-.L115
	.long	.L233-.L115
	.long	.L232-.L115
	.long	.L231-.L115
	.long	.L230-.L115
	.long	.L229-.L115
	.long	.L228-.L115
	.long	.L227-.L115
	.long	.L226-.L115
	.long	.L225-.L115
	.long	.L224-.L115
	.long	.L223-.L115
	.long	.L222-.L115
	.long	.L222-.L115
	.long	.L221-.L115
	.long	.L220-.L115
	.long	.L219-.L115
	.long	.L218-.L115
	.long	.L217-.L115
	.long	.L216-.L115
	.long	.L215-.L115
	.long	.L214-.L115
	.long	.L213-.L115
	.long	.L212-.L115
	.long	.L211-.L115
	.long	.L210-.L115
	.long	.L209-.L115
	.long	.L208-.L115
	.long	.L207-.L115
	.long	.L206-.L115
	.long	.L205-.L115
	.long	.L204-.L115
	.long	.L203-.L115
	.long	.L202-.L115
	.long	.L201-.L115
	.long	.L73-.L115
	.long	.L200-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L309-.L115
	.long	.L309-.L115
	.long	.L309-.L115
	.long	.L73-.L115
	.long	.L309-.L115
	.long	.L309-.L115
	.long	.L309-.L115
	.long	.L309-.L115
	.long	.L309-.L115
	.long	.L199-.L115
	.long	.L198-.L115
	.long	.L197-.L115
	.long	.L196-.L115
	.long	.L195-.L115
	.long	.L194-.L115
	.long	.L193-.L115
	.long	.L192-.L115
	.long	.L190-.L115
	.long	.L190-.L115
	.long	.L190-.L115
	.long	.L190-.L115
	.long	.L190-.L115
	.long	.L190-.L115
	.long	.L191-.L115
	.long	.L190-.L115
	.long	.L191-.L115
	.long	.L190-.L115
	.long	.L190-.L115
	.long	.L190-.L115
	.long	.L190-.L115
	.long	.L190-.L115
	.long	.L190-.L115
	.long	.L190-.L115
	.long	.L189-.L115
	.long	.L188-.L115
	.long	.L187-.L115
	.long	.L187-.L115
	.long	.L186-.L115
	.long	.L185-.L115
	.long	.L184-.L115
	.long	.L183-.L115
	.long	.L182-.L115
	.long	.L218-.L115
	.long	.L180-.L115
	.long	.L179-.L115
	.long	.L178-.L115
	.long	.L309-.L115
	.long	.L73-.L115
	.long	.L177-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L309-.L115
	.long	.L309-.L115
	.long	.L309-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L309-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L176-.L115
	.long	.L176-.L115
	.long	.L176-.L115
	.long	.L176-.L115
	.long	.L176-.L115
	.long	.L176-.L115
	.long	.L167-.L115
	.long	.L167-.L115
	.long	.L167-.L115
	.long	.L167-.L115
	.long	.L167-.L115
	.long	.L167-.L115
	.long	.L175-.L115
	.long	.L167-.L115
	.long	.L167-.L115
	.long	.L167-.L115
	.long	.L167-.L115
	.long	.L167-.L115
	.long	.L140-.L115
	.long	.L140-.L115
	.long	.L140-.L115
	.long	.L174-.L115
	.long	.L173-.L115
	.long	.L172-.L115
	.long	.L172-.L115
	.long	.L171-.L115
	.long	.L170-.L115
	.long	.L169-.L115
	.long	.L168-.L115
	.long	.L167-.L115
	.long	.L167-.L115
	.long	.L167-.L115
	.long	.L167-.L115
	.long	.L152-.L115
	.long	.L166-.L115
	.long	.L165-.L115
	.long	.L164-.L115
	.long	.L163-.L115
	.long	.L162-.L115
	.long	.L161-.L115
	.long	.L160-.L115
	.long	.L159-.L115
	.long	.L158-.L115
	.long	.L157-.L115
	.long	.L152-.L115
	.long	.L156-.L115
	.long	.L155-.L115
	.long	.L154-.L115
	.long	.L153-.L115
	.long	.L152-.L115
	.long	.L152-.L115
	.long	.L151-.L115
	.long	.L150-.L115
	.long	.L149-.L115
	.long	.L148-.L115
	.long	.L147-.L115
	.long	.L146-.L115
	.long	.L145-.L115
	.long	.L144-.L115
	.long	.L309-.L115
	.long	.L143-.L115
	.long	.L142-.L115
	.long	.L141-.L115
	.long	.L141-.L115
	.long	.L140-.L115
	.long	.L140-.L115
	.long	.L139-.L115
	.long	.L138-.L115
	.long	.L309-.L115
	.long	.L137-.L115
	.long	.L137-.L115
	.long	.L137-.L115
	.long	.L137-.L115
	.long	.L132-.L115
	.long	.L132-.L115
	.long	.L132-.L115
	.long	.L132-.L115
	.long	.L136-.L115
	.long	.L136-.L115
	.long	.L136-.L115
	.long	.L136-.L115
	.long	.L135-.L115
	.long	.L134-.L115
	.long	.L133-.L115
	.long	.L132-.L115
	.long	.L131-.L115
	.long	.L130-.L115
	.long	.L129-.L115
	.long	.L128-.L115
	.long	.L73-.L115
	.long	.L73-.L115
	.long	.L127-.L115
	.long	.L309-.L115
	.long	.L309-.L115
	.long	.L126-.L115
	.long	.L125-.L115
	.long	.L124-.L115
	.long	.L123-.L115
	.long	.L122-.L115
	.long	.L121-.L115
	.long	.L120-.L115
	.long	.L119-.L115
	.long	.L118-.L115
	.long	.L309-.L115
	.long	.L117-.L115
	.long	.L116-.L115
	.long	.L309-.L115
	.section	.text._ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE
	.p2align 4,,10
	.p2align 3
.L572:
	movq	(%r14), %rdx
	cmpw	$41, 16(%rdx)
	jne	.L416
	testl	%eax, %eax
	je	.L91
.L416:
	leaq	.LC18(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L279:
	cmpl	$1, -76(%rbp)
	jne	.L372
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler16ParameterIndexOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpw	$0, 16(%rax)
	jne	.L575
	cmpl	$-1, %ebx
	jl	.L576
	addl	$1, %ebx
	cmpl	32(%rax), %ebx
	jge	.L577
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	.p2align 4,,10
	.p2align 3
.L73:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L578
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	leaq	16(%rdx,%rax), %rsi
	movq	(%rdx), %rdx
	testb	%r9b, %r9b
	je	.L82
	testq	%rdx, %rdx
	jne	.L81
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L574:
	movl	-80(%rbp), %eax
	xorl	%r15d, %r15d
	leaq	.LC22(%rip), %r13
	testl	%eax, %eax
	jle	.L102
	movl	%ebx, -72(%rbp)
	movl	%r15d, %ebx
	movl	-80(%rbp), %r15d
	.p2align 4,,10
	.p2align 3
.L98:
	xorl	%esi, %esi
	movq	%r14, %rdi
	addl	$1, %ebx
	call	_ZN2v88internal8compiler14NodeProperties14GetEffectInputEPNS1_4NodeEi@PLT
	movq	%r13, %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	(%rax), %rax
	movzbl	36(%rax), %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckOutputEPNS1_4NodeES5_iPKc
	cmpl	%r15d, %ebx
	jne	.L98
	movl	-72(%rbp), %ebx
.L102:
	movl	-88(%rbp), %eax
	xorl	%r15d, %r15d
	leaq	.LC23(%rip), %r13
	testl	%eax, %eax
	jle	.L103
	movl	%ebx, -72(%rbp)
	movl	%r15d, %ebx
	movl	-88(%rbp), %r15d
	.p2align 4,,10
	.p2align 3
.L99:
	movl	%ebx, %esi
	movq	%r14, %rdi
	addl	$1, %ebx
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%r13, %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	(%rax), %rax
	movl	40(%rax), %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckOutputEPNS1_4NodeES5_iPKc
	cmpl	%r15d, %ebx
	jne	.L99
	movl	-72(%rbp), %ebx
.L103:
	movq	(%r14), %r8
	testb	$32, 18(%r8)
	jne	.L101
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L101
	movq	$0, -112(%rbp)
	xorl	%eax, %eax
	movq	0(%r13), %r15
	movq	$0, -72(%rbp)
	movq	$0, -96(%rbp)
	movl	%ebx, -116(%rbp)
	movq	%r13, %rbx
	movl	%eax, %r13d
	.p2align 4,,10
	.p2align 3
.L111:
	movl	16(%rbx), %esi
	movq	%rbx, %rdi
	movl	%esi, %edx
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rax
	leaq	(%rbx,%rax,8), %rax
	leaq	32(%rax), %r8
	addq	$16, %rax
	andl	$1, %esi
	cmovne	%r8, %rax
	leaq	(%rax,%rdx,8), %rsi
	call	_ZN2v88internal8compiler14NodeProperties13IsControlEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L107
	movl	16(%rbx), %esi
	addl	$1, %r13d
	movl	%esi, %eax
	shrl	%eax
	andl	$1, %esi
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rdx
	movq	(%rdx), %rax
	jne	.L108
	movq	%rax, %rdx
	movq	(%rax), %rax
.L108:
	movzwl	16(%rax), %eax
	cmpl	$6, %eax
	je	.L579
	cmpl	$7, %eax
	je	.L580
	movq	%rdx, -112(%rbp)
.L107:
	testq	%r15, %r15
	je	.L110
.L582:
	movq	%r15, %rbx
	movq	(%r15), %r15
	jmp	.L111
.L291:
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L309
	movq	-104(%rbp), %r8
	cmpb	$0, 80(%r8)
	je	.L82
	movl	$1, %r10d
.L354:
	movl	16(%rax), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %ecx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	jne	.L352
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L353
.L352:
	movl	20(%rdx), %ecx
	movq	40(%r8), %r9
	movq	56(%r8), %rdi
	movl	64(%r8), %r11d
	movl	%ecx, %esi
	subq	%r9, %rdi
	andl	$16777215, %esi
	leaq	(%r11,%rdi,8), %rdi
	cmpq	%rdi, %rsi
	jnb	.L353
	movq	%r10, %rdi
	shrq	$6, %rsi
	salq	%cl, %rdi
	testq	%rdi, (%r9,%rsi,8)
	je	.L353
	movq	(%rdx), %rdx
	cmpw	$22, 16(%rdx)
	jne	.L362
.L353:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L354
.L309:
	cmpq	$0, 8(%r14)
	je	.L73
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L579:
	cmpq	$0, -72(%rbp)
	jne	.L581
	movq	%rdx, -72(%rbp)
	testq	%r15, %r15
	jne	.L582
.L110:
	cmpq	$0, -72(%rbp)
	movl	-116(%rbp), %ebx
	movl	%r13d, %esi
	setne	%al
	cmpq	$0, -96(%rbp)
	movq	(%r14), %r8
	jne	.L407
	testb	%al, %al
	jne	.L583
.L407:
	cmpq	$0, -96(%rbp)
	setne	%dl
	cmpq	$0, -72(%rbp)
	jne	.L112
	testb	%dl, %dl
	jne	.L584
.L112:
	orb	%dl, %al
	je	.L101
	cmpl	$2, %esi
	je	.L101
	movq	-112(%rbp), %rcx
	movl	20(%r14), %esi
	leaq	.LC27(%rip), %rdi
	movq	8(%r8), %rdx
	movq	(%rcx), %rax
	movl	20(%rcx), %ecx
	andl	$16777215, %esi
	movq	8(%rax), %r8
	andl	$16777215, %ecx
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L580:
	cmpq	$0, -96(%rbp)
	jne	.L585
	movq	%rdx, -96(%rbp)
	jmp	.L107
.L283:
	testl	%ebx, %ebx
	jne	.L401
	cmpl	$1, -88(%rbp)
	jne	.L364
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	-80(%rbp), %ecx
	movq	(%rax), %rdx
	cmpl	28(%rdx), %ecx
	jne	.L586
	addl	$1, %ecx
	cmpl	-76(%rbp), %ecx
	jne	.L587
	cmpw	$10, 16(%rdx)
	jne	.L73
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L382
	movq	-104(%rbp), %rsi
	xorl	%edi, %edi
	movl	$1, %r10d
	movl	$1, %r9d
	jmp	.L385
.L589:
	cmpb	$0, 80(%rsi)
	movq	(%rdx), %rdx
	je	.L82
	testq	%rdx, %rdx
	je	.L384
.L404:
	movl	20(%rdx), %ecx
	movq	40(%rsi), %rbx
	movq	56(%rsi), %r11
	movl	64(%rsi), %r12d
	movl	%ecx, %r8d
	subq	%rbx, %r11
	andl	$16777215, %r8d
	leaq	(%r12,%r11,8), %r11
	cmpq	%r11, %r8
	jnb	.L384
	movq	%r10, %r11
	shrq	$6, %r8
	salq	%cl, %r11
	testq	%r11, (%rbx,%r8,8)
	je	.L384
	movq	(%rdx), %rdx
	movzwl	16(%rdx), %edx
	subl	$35, %edx
	cmpl	$2, %edx
	cmovnb	%r9d, %edi
.L384:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L588
.L385:
	movl	16(%rax), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %ecx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	je	.L589
	cmpb	$0, 80(%rsi)
	jne	.L404
.L82:
	leaq	.LC17(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L571:
	leaq	.LC15(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L573:
	movq	(%r15), %rax
	cmpl	$1, 32(%rax)
	jle	.L95
	leaq	.LC20(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L247:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L190:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L167:
	movl	$134224991, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L286:
	movl	-76(%rbp), %ebx
	testl	%ebx, %ebx
	je	.L309
.L371:
	leaq	.LC28(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L132:
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L140:
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L176:
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L249:
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L218:
	movl	$4294967295, %ecx
	xorl	%edx, %edx
.L554:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	cmpq	$0, 8(%r14)
	je	.L73
.L313:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor13CheckNotTypedEPNS1_4NodeE.part.0
	.p2align 4,,10
	.p2align 3
.L276:
	cmpl	$1, -88(%rbp)
	jne	.L364
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpw	$52, 16(%rax)
	je	.L73
	leaq	.LC68(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L288:
	movl	-76(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L371
	movl	$33554433, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L298:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpw	$2, 16(%rax)
	je	.L309
	leaq	.LC38(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L257:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1099, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1031, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$1099, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L254:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L295:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpw	$3, 16(%rax)
	je	.L309
	leaq	.LC45(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L287:
	movl	-76(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L371
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L252:
	movl	$1099, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L141:
	cmpq	$0, 8(%r14)
	jne	.L313
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler19FeedbackParameterOfEPKNS1_8OperatorE@PLT
	cmpq	$0, (%rax)
	je	.L400
	cmpl	$-1, 8(%rax)
	jne	.L73
.L400:
	leaq	.LC79(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetContextInputEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movl	$16777217, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler8Verifier7Visitor14CheckTypeMaybeEPNS1_4NodeENS1_4TypeE
	movl	$16777217, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L136:
	movl	$75431937, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L191:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L187:
	movq	(%r12), %rdi
	movsd	.LC80(%rip), %xmm1
	pxor	%xmm0, %xmm0
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$16777217, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L292:
	cmpl	$1, -88(%rbp)
	jne	.L364
	cmpl	$1, -80(%rbp)
	jne	.L365
	cmpl	$2, -76(%rbp)
	je	.L309
.L366:
	leaq	.LC50(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L222:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16417, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$16417, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L266:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L265:
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L263:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L261:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16417, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16417, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L260:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L251:
	movl	$1099, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L243:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$1031, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L258:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1099, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1099, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$1099, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L172:
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L152:
	movl	$131073, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L270:
	leaq	.LC33(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L581:
	leaq	.LC24(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L585:
	leaq	.LC25(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L219:
	movl	$513, %ecx
	xorl	%edx, %edx
	jmp	.L554
.L364:
	leaq	.LC48(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L225:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16417, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1031, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$16417, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L224:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16417, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16417, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$2, %edx
	movl	%eax, %ecx
	orl	$1, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	orl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L223:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16417, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	472(%rax), %rdx
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L147:
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	movq	(%r14), %rdi
	call	_ZN2v88internal8compiler16PropertyAccessOfEPKNS1_8OperatorE@PLT
	cmpq	$0, (%rax)
	je	.L396
.L568:
	cmpl	$-1, 8(%rax)
	jne	.L73
.L396:
	leaq	.LC75(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L146:
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L145:
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	movq	(%r14), %rdi
	call	_ZN2v88internal8compiler22LoadGlobalParametersOfEPKNS1_8OperatorE@PLT
	cmpq	$0, 8(%rax)
	je	.L397
	cmpl	$-1, 16(%rax)
	jne	.L73
.L397:
	leaq	.LC76(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L144:
	cmpq	$0, 8(%r14)
	jne	.L313
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler16PropertyAccessOfEPKNS1_8OperatorE@PLT
	cmpq	$0, (%rax)
	jne	.L568
	jmp	.L396
.L143:
	cmpq	$0, 8(%r14)
	jne	.L313
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler25StoreNamedOwnParametersOfEPKNS1_8OperatorE@PLT
	cmpq	$0, 8(%rax)
	je	.L399
	cmpl	$-1, 16(%rax)
	jne	.L73
.L399:
	leaq	.LC78(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L142:
	cmpq	$0, 8(%r14)
	jne	.L313
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler23StoreGlobalParametersOfEPKNS1_8OperatorE@PLT
	cmpq	$0, 16(%rax)
	je	.L398
	cmpl	$-1, 24(%rax)
	jne	.L73
.L398:
	leaq	.LC77(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L138:
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L175:
	movl	$134241407, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L130:
	movq	(%r12), %rdx
	movl	$257, %esi
	movl	$24609, %edi
	call	_ZN2v88internal8compiler4Type5UnionES2_S2_PNS0_4ZoneE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L129:
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L128:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L127:
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L126:
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	orl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L125:
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L124:
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L123:
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L122:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$257, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L121:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	movl	$2, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	movl	$3, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$75431937, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L120:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$75431937, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L119:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	movl	$2, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$257, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L174:
	movq	(%r12), %rdi
	movsd	.LC73(%rip), %xmm1
	pxor	%xmm0, %xmm0
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L220:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	movq	408(%rax), %rcx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	408(%rax), %rdx
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L278:
	cmpl	$1, -88(%rbp)
	jne	.L364
	cmpl	$1, -76(%rbp)
	jne	.L372
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L149:
	movl	$131073, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L148:
	movl	$131073, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L301:
	movl	-88(%rbp), %ecx
	cmpl	%ecx, -76(%rbp)
	jne	.L351
	cmpq	$0, 8(%r14)
	jne	.L313
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L347
	movq	-104(%rbp), %r8
	cmpb	$0, 80(%r8)
	je	.L82
	movl	$1, %r9d
.L350:
	movl	16(%rax), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %ecx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	jne	.L348
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L349
.L348:
	movl	20(%rdx), %ecx
	movq	40(%r8), %r10
	movq	56(%r8), %rdi
	movl	64(%r8), %r11d
	movl	%ecx, %esi
	subq	%r10, %rdi
	andl	$16777215, %esi
	leaq	(%r11,%rdi,8), %rdi
	cmpq	%rdi, %rsi
	jnb	.L349
	movq	%r9, %rdi
	shrq	$6, %rsi
	salq	%cl, %rdi
	testq	%rdi, (%r10,%rsi,8)
	je	.L349
	movq	(%rdx), %rdx
	cmpw	$18, 16(%rdx)
	je	.L73
.L349:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L350
.L347:
	leaq	.LC81(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L300:
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L319
	movq	-104(%rbp), %r10
	cmpb	$0, 80(%r10)
	je	.L82
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	movl	$1, %r11d
.L325:
	movl	16(%rax), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %ecx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	jne	.L320
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L321
.L320:
	movl	20(%rdx), %ecx
	movq	40(%r10), %rbx
	movq	56(%r10), %r9
	movl	64(%r10), %r13d
	movl	%ecx, %esi
	subq	%rbx, %r9
	andl	$16777215, %esi
	leaq	0(%r13,%r9,8), %r9
	cmpq	%r9, %rsi
	jb	.L590
.L321:
	leaq	.LC35(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L590:
	movq	%r11, %r9
	shrq	$6, %rsi
	salq	%cl, %r9
	testq	%r9, (%rbx,%rsi,8)
	je	.L321
	movq	(%rdx), %rdx
	movzwl	16(%rdx), %edx
	cmpl	$4, %edx
	jne	.L591
	addl	$1, %r8d
.L324:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L325
	subl	$1, %r8d
	jne	.L319
	subl	$1, %edi
	je	.L219
	leaq	.LC37(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L135:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$131073, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L169:
	movl	$16417, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L178:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1031, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$1031, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L177:
	testl	%ebx, %ebx
	jne	.L401
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L242:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L241:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$134217729, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$134217729, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L240:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$134217729, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$134217729, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L239:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$134217729, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L238:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L237:
	movl	$134217729, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L236:
	movl	$134217729, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L235:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$24575, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L234:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$24575, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$1103, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L233:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$24575, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L232:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$513, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L231:
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	472(%rax), %rcx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16417, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16417, %ecx
	movl	$2, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$16417, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L230:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16417, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L229:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16417, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1031, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	call	_ZN2v88internal8compiler10BitsetType13UnsignedSmallEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	orl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L228:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16417, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1031, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	call	_ZN2v88internal8compiler10BitsetType13UnsignedSmallEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	orl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L227:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$16417, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L214:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$75432321, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L213:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$16417, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L212:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$8193, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L211:
	movl	$4294967295, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	jmp	.L73
.L216:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$16385, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L215:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$75431937, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L217:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L277:
	cmpl	$2, -88(%rbp)
	jne	.L592
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpw	$1, 16(%rax)
	je	.L73
	leaq	.LC67(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L246:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L245:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$1099, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L244:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$16417, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L256:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1031, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1031, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$1031, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L284:
	movl	-80(%rbp), %esi
	testl	%esi, %esi
	jne	.L377
	cmpl	$1, -88(%rbp)
	jne	.L364
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpl	28(%rax), %ebx
	jne	.L593
	addl	$1, %ebx
	cmpl	-76(%rbp), %ebx
	je	.L73
	leaq	.LC61(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L248:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1031, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$1031, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L197:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L196:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L195:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L186:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movl	$33554433, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	(%r12), %rdi
	pxor	%xmm0, %xmm0
	movsd	.LC80(%rip), %xmm1
	call	_ZN2v88internal8compiler4Type5RangeEddPNS0_4ZoneE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$16777217, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L185:
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	472(%rax), %rcx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16417, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16417, %ecx
	movl	$2, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$16417, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L184:
	movl	$16417, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L183:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$58720257, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$58720257, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L182:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$58720257, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1027, %ecx
	movl	$2, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1027, %ecx
	movl	$3, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$58720257, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L116:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16417, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L180:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	orl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L179:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1099, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	orl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L194:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L193:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L293:
	cmpl	$1, -88(%rbp)
	jne	.L364
	cmpl	$1, -80(%rbp)
	jne	.L365
	cmpl	$2, -76(%rbp)
	jne	.L366
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpw	$1, 16(%rax)
	jne	.L594
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L309
	movq	-104(%rbp), %rsi
	movl	$1, %r10d
	jmp	.L363
.L595:
	cmpb	$0, 80(%rsi)
	movq	(%rdx), %rdx
	je	.L82
	testq	%rdx, %rdx
	je	.L361
.L403:
	movl	20(%rdx), %ecx
	movq	40(%rsi), %r9
	movq	56(%rsi), %r8
	movl	64(%rsi), %r11d
	movl	%ecx, %edi
	subq	%r9, %r8
	andl	$16777215, %edi
	leaq	(%r11,%r8,8), %r8
	cmpq	%r8, %rdi
	jnb	.L361
	movq	%r10, %rbx
	shrq	$6, %rdi
	salq	%cl, %rbx
	testq	%rbx, (%r9,%rdi,8)
	je	.L361
	movq	(%rdx), %rdx
	cmpw	$22, 16(%rdx)
	jne	.L362
.L361:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L309
.L363:
	movl	16(%rax), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %ecx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	je	.L595
	cmpb	$0, 80(%rsi)
	jne	.L403
	jmp	.L82
.L221:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16417, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	movl	%eax, %ecx
	orl	$1, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$2, %edx
	movl	%eax, %ecx
	orl	$1, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$16417, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L189:
	movl	$33554433, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L205:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L204:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$75431937, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L203:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$209682431, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L202:
	movl	$16385, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L201:
	movl	$1119, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	jmp	.L73
.L200:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	call	_ZN2v88internal8compiler10BitsetType11SignedSmallEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	movl	%eax, %ecx
	orl	$1, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$209682431, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L199:
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L198:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$8395871, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L209:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$8395871, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$7519, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L208:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$209682431, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L207:
	movl	$16385, %ecx
.L556:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$4294967295, %ecx
	movl	$1, %edx
	jmp	.L554
.L206:
	movl	$8193, %ecx
	jmp	.L556
.L302:
	movl	-76(%rbp), %r15d
	testl	%r15d, %r15d
	jne	.L371
	movl	$58720257, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L210:
	movl	$4294967295, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	jmp	.L73
.L173:
	movl	$24609, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L255:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1031, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1031, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$1099, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L253:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$134217729, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$134217729, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$134217729, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L117:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L274:
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler17ProjectionIndexOfEPKNS1_8OperatorE@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpl	%ebx, 32(%rax)
	jle	.L596
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L273:
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler15TypeGuardTypeOfEPKNS1_8OperatorE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L272:
	movq	%r12, %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.L73
	movq	(%rdi), %r13
	movq	-104(%rbp), %r14
	movl	$1, %ebx
.L318:
	movl	16(%rdi), %edx
	movl	%edx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rcx
	salq	$3, %rax
	andl	$1, %edx
	leaq	(%rdi,%rcx,8), %r12
	leaq	32(%r12,%rax), %rsi
	jne	.L316
	leaq	16(%r12,%rax), %rsi
	movq	(%r12), %r12
.L316:
	call	_ZN2v88internal8compiler14NodeProperties11IsValueEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	jne	.L597
.L317:
	testq	%r13, %r13
	je	.L73
	movq	%r13, %rdi
	movq	0(%r13), %r13
	jmp	.L318
.L597:
	cmpb	$0, 80(%r14)
	je	.L82
	testq	%r12, %r12
	je	.L317
	movl	20(%r12), %ecx
	movq	40(%r14), %rsi
	movq	56(%r14), %rdx
	movl	64(%r14), %edi
	movl	%ecx, %eax
	subq	%rsi, %rdx
	andl	$16777215, %eax
	leaq	(%rdi,%rdx,8), %rdx
	cmpq	%rdx, %rax
	jnb	.L317
	movq	%rbx, %rdx
	shrq	$6, %rax
	salq	%cl, %rdx
	testq	%rdx, (%rsi,%rax,8)
	je	.L317
	movq	(%r12), %rax
	cmpw	$60, 16(%rax)
	je	.L317
	leaq	.LC34(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L271:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L269:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$134217729, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$134217729, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L268:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$134217729, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$134217729, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L267:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L297:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	testb	$32, 18(%rax)
	je	.L309
.L331:
	leaq	.LC39(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L296:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	testb	$32, 18(%rax)
	jne	.L331
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L294:
	movl	-88(%rbp), %ecx
	cmpl	%ecx, -76(%rbp)
	je	.L309
.L351:
	leaq	.LC46(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L285:
	movl	-80(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L377
	movl	-88(%rbp), %edi
	testl	%edi, %edi
	jne	.L391
	cmpl	$3, %ebx
	jne	.L598
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$513, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$4294967295, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L264:
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L262:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L259:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L250:
	movl	$1031, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L290:
	movl	32(%r8), %r13d
	testl	%r13d, %r13d
	jne	.L599
	cmpb	$0, 36(%r8)
	jne	.L600
	movl	40(%r8), %r12d
	testl	%r12d, %r12d
	jne	.L601
	movzbl	23(%r14), %edx
	leaq	32(%r14), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L308
	movq	32(%r14), %rax
	movl	8(%rax), %edx
	addq	$16, %rax
.L308:
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,8), %rdx
	cmpq	%rdx, %rax
	je	.L309
	movl	$2557952, %esi
	jmp	.L312
.L602:
	btq	%rcx, %rsi
	jnc	.L310
	addq	$8, %rax
	cmpq	%rax, %rdx
	je	.L309
.L312:
	movq	(%rax), %rcx
	movq	(%rcx), %rcx
	movzwl	16(%rcx), %ecx
	cmpw	$21, %cx
	jbe	.L602
.L310:
	leaq	.LC32(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L192:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$513, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L289:
	movl	-76(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L371
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L299:
	movq	24(%r14), %r15
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	testq	%r15, %r15
	je	.L343
	movq	%r14, %rsi
	movl	%edx, %r14d
	movl	%r12d, %edx
	movq	-104(%rbp), %r12
.L332:
	movl	16(%r15), %ecx
	movl	%ecx, %eax
	shrl	%eax
	andl	$1, %ecx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r15,%rax,8), %r13
	movzbl	80(%r12), %eax
	jne	.L333
	movq	0(%r13), %r13
	testb	%al, %al
	je	.L82
	testq	%r13, %r13
	je	.L334
.L402:
	movl	20(%r13), %ecx
	movq	40(%r12), %r9
	movq	56(%r12), %rdi
	movl	64(%r12), %r10d
	movl	%ecx, %r8d
	andl	$16777215, %r8d
	subq	%r9, %rdi
	movl	%r8d, %eax
	leaq	(%r10,%rdi,8), %rdi
	cmpq	%rdi, %rax
	jb	.L603
.L334:
	leaq	.LC41(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L333:
	testb	%al, %al
	jne	.L402
	jmp	.L82
.L603:
	movl	$1, %edi
	shrq	$6, %rax
	salq	%cl, %rdi
	testq	%rdi, (%r9,%rax,8)
	je	.L334
	movq	0(%r13), %rax
	movzwl	16(%rax), %ecx
	cmpw	$8, %cx
	jne	.L604
	movq	24(%rsi), %rbx
	testq	%rbx, %rbx
	jne	.L338
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L341:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L342
.L338:
	movl	16(%rbx), %ecx
	movl	%ecx, %eax
	shrl	%eax
	andl	$1, %ecx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rax
	jne	.L340
	movq	(%rax), %rax
.L340:
	cmpq	%r13, %rax
	je	.L341
	movq	(%rax), %rdi
	cmpw	$8, 16(%rdi)
	jne	.L341
	movq	%rsi, -88(%rbp)
	movl	%edx, -76(%rbp)
	call	_ZN2v88internal8compiler19IfValueParametersOfEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movl	(%rax), %ecx
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal8compiler19IfValueParametersOfEPKNS1_8OperatorE@PLT
	movl	-72(%rbp), %ecx
	movl	-76(%rbp), %edx
	cmpl	(%rax), %ecx
	movq	-88(%rbp), %rsi
	jne	.L341
	leaq	.LC42(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L604:
	cmpw	$9, %cx
	jne	.L605
	addl	$1, %edx
.L339:
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L332
	movl	%edx, %r12d
	movl	%r14d, %edx
	movq	%rsi, %r14
	subl	$1, %r12d
	jne	.L343
	movq	(%rsi), %rax
	addl	$1, %edx
	cmpl	40(%rax), %edx
	je	.L309
	leaq	.LC44(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L342:
	addl	$1, %r14d
	jmp	.L339
.L281:
	cmpl	$5, %ebx
	jne	.L606
	movl	-88(%rbp), %edx
	testl	%edx, %edx
	jne	.L391
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	jne	.L377
	cmpl	$6, -76(%rbp)
	movl	-80(%rbp), %ebx
	jne	.L607
.L392:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpw	$42, 16(%rax)
	jne	.L393
.L394:
	cmpl	$1, %ebx
	je	.L73
	movl	$1, %ebx
	jmp	.L392
.L280:
	movl	$73859073, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L171:
	movl	$134224991, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L166:
	movl	$73859073, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L165:
	movl	$67239937, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L168:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$7263, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L139:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$7143425, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L170:
	movl	$75431937, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L282:
	movl	8(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L73
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	8(%r14), %rsi
	movq	8(%rax), %rax
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rsi
	je	.L73
	leaq	-64(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L73
	leaq	.LC69(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L188:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$33554433, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	call	_ZN2v88internal8compiler9TypeCache3GetEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	552(%rax), %rdx
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L118:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$257, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L164:
	movl	$67108865, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L163:
	movl	$67108865, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L162:
	movl	$131073, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L161:
	movl	$131073, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L160:
	movl	$4194305, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L159:
	movl	$2097153, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L158:
	movl	$131073, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L157:
	movl	$67108865, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L156:
	movl	$131073, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L155:
	movl	$131073, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L154:
	movl	$131073, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L153:
	movl	$67108865, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L151:
	movl	$131073, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L150:
	movl	$131073, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L134:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$513, %ecx
	movl	$2, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$131073, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L133:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$513, %ecx
	movl	$2, %edx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$131073, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L131:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$4294967295, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$16777217, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L226:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$7263, %ecx
	call	_ZN2v88internal8compiler8Verifier7Visitor17CheckValueInputIsEPNS1_4NodeEiNS1_4TypeE
	movl	$16417, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Verifier7Visitor11CheckTypeIsEPNS1_4NodeENS1_4TypeE
	jmp	.L73
.L362:
	leaq	.LC47(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L605:
	movl	20(%rsi), %esi
	movq	8(%rax), %rcx
	xorl	%eax, %eax
	movl	%r8d, %edx
	leaq	.LC43(%rip), %rdi
	andl	$16777215, %esi
	call	_Z8V8_FatalPKcz@PLT
.L343:
	leaq	.LC40(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L607:
	leaq	.LC71(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L606:
	leaq	.LC70(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L393:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movq	(%rax), %rax
	cmpw	$43, 16(%rax)
	je	.L394
	leaq	.LC72(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L365:
	leaq	.LC49(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L578:
	call	__stack_chk_fail@PLT
.L591:
	cmpl	$5, %edx
	jne	.L321
	addl	$1, %edi
	jmp	.L324
.L577:
	leaq	.LC55(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L576:
	leaq	.LC54(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L575:
	leaq	.LC53(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L372:
	leaq	.LC52(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L583:
	movq	-72(%rbp), %rcx
.L569:
	movq	(%rcx), %rax
	movl	20(%r14), %esi
	leaq	.LC26(%rip), %rdi
	movl	20(%rcx), %ecx
	movq	8(%r8), %rdx
	movq	8(%rax), %r8
	andl	$16777215, %esi
	xorl	%eax, %eax
	andl	$16777215, %ecx
	call	_Z8V8_FatalPKcz@PLT
.L584:
	movq	-96(%rbp), %rcx
	jmp	.L569
.L594:
	leaq	.LC51(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L592:
	leaq	.LC66(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L319:
	leaq	.LC36(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L401:
	leaq	.LC62(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L587:
	leaq	.LC64(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L586:
	leaq	.LC63(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L588:
	testb	%dil, %dil
	jne	.L73
.L382:
	leaq	.LC65(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L601:
	leaq	.LC31(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L600:
	leaq	.LC30(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L599:
	leaq	.LC29(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L596:
	leaq	.LC56(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L593:
	leaq	.LC60(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L377:
	leaq	.LC57(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L598:
	leaq	.LC59(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L391:
	leaq	.LC58(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE10889:
	.size	_ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE, .-_ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE
	.section	.rodata._ZN2v88internal8compiler8Verifier3RunEPNS1_5GraphENS2_6TypingENS2_11CheckInputsENS2_8CodeTypeE.str1.1,"aMS",@progbits,1
.LC82:
	.string	"(graph->start()) != nullptr"
.LC83:
	.string	"(graph->end()) != nullptr"
	.section	.rodata._ZN2v88internal8compiler8Verifier3RunEPNS1_5GraphENS2_6TypingENS2_11CheckInputsENS2_8CodeTypeE.str1.8,"aMS",@progbits,1
	.align 8
.LC84:
	.string	"../deps/v8/src/compiler/verifier.cc:1900"
	.align 8
.LC85:
	.string	"Node #%d:%s has duplicate projections #%d and #%d"
	.section	.text._ZN2v88internal8compiler8Verifier3RunEPNS1_5GraphENS2_6TypingENS2_11CheckInputsENS2_8CodeTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Verifier3RunEPNS1_5GraphENS2_6TypingENS2_11CheckInputsENS2_8CodeTypeE
	.type	_ZN2v88internal8compiler8Verifier3RunEPNS1_5GraphENS2_6TypingENS2_11CheckInputsENS2_8CodeTypeE, @function
_ZN2v88internal8compiler8Verifier3RunEPNS1_5GraphENS2_6TypingENS2_11CheckInputsENS2_8CodeTypeE:
.LFB10890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 8(%rdi)
	je	.L644
	cmpq	$0, 16(%rdi)
	movq	%rdi, %r13
	je	.L645
	movq	(%rdi), %rax
	movl	%esi, %r15d
	leaq	-208(%rbp), %r12
	movl	%edx, %r14d
	movq	%r12, %rdi
	leaq	.LC84(%rip), %rdx
	movl	%ecx, %ebx
	movq	32(%rax), %rsi
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	%r13, %rdx
	movl	$1, %ecx
	movq	%r12, %rsi
	movl	%r14d, -228(%rbp)
	leaq	-144(%rbp), %r14
	movq	%r14, %rdi
	movl	%r15d, -232(%rbp)
	leaq	-240(%rbp), %r15
	movl	%ebx, -224(%rbp)
	movq	%r12, -240(%rbp)
	call	_ZN2v88internal8compiler8AllNodesC1EPNS0_4ZoneEPKNS1_5GraphEb@PLT
	movq	-128(%rbp), %r13
	movq	-136(%rbp), %rbx
	cmpq	%r13, %rbx
	je	.L613
	.p2align 4,,10
	.p2align 3
.L612:
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	addq	$8, %rbx
	call	_ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE
	cmpq	%rbx, %r13
	jne	.L612
	movq	-136(%rbp), %r14
	movq	-128(%rbp), %rsi
	cmpq	%rsi, %r14
	je	.L613
	movl	$1, %r8d
.L615:
	movq	(%r14), %rdx
	movq	(%rdx), %rax
	cmpw	$55, 16(%rax)
	je	.L614
.L617:
	addq	$8, %r14
	cmpq	%r14, %rsi
	jne	.L615
.L613:
	movq	%r12, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L646
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	.cfi_restore_state
	movzbl	23(%rdx), %eax
	movq	32(%rdx), %r15
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L616
	movq	16(%r15), %r15
.L616:
	movq	24(%r15), %rbx
	testq	%rbx, %rbx
	jne	.L622
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L648:
	movq	0(%r13), %r13
	testb	%al, %al
	je	.L624
	testq	%r13, %r13
	je	.L620
.L623:
	movl	20(%r13), %ecx
	movq	-104(%rbp), %r10
	movq	-88(%rbp), %rax
	movl	-80(%rbp), %r11d
	movl	%ecx, %edi
	subq	%r10, %rax
	andl	$16777215, %edi
	leaq	(%r11,%rax,8), %rax
	cmpq	%rax, %rdi
	jnb	.L620
	movq	%r8, %rax
	shrq	$6, %rdi
	salq	%cl, %rax
	testq	%rax, (%r10,%rdi,8)
	je	.L620
	cmpq	%r13, %rdx
	je	.L620
	movq	0(%r13), %rdi
	cmpw	$55, 16(%rdi)
	je	.L647
	.p2align 4,,10
	.p2align 3
.L620:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L617
.L622:
	movl	16(%rbx), %ecx
	movl	%ecx, %eax
	shrl	%eax
	andl	$1, %ecx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %r13
	movzbl	-64(%rbp), %eax
	je	.L648
	testb	%al, %al
	jne	.L623
.L624:
	leaq	.LC17(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L647:
	xorl	$251658240, %ecx
	movq	32(%r13), %rax
	andl	$251658240, %ecx
	je	.L649
	cmpq	%rax, %r15
	jne	.L620
.L650:
	movq	%rsi, -264(%rbp)
	movq	%rdx, -256(%rbp)
	call	_ZN2v88internal8compiler17ProjectionIndexOfEPKNS1_8OperatorE@PLT
	movq	-256(%rbp), %rdx
	movq	%rax, -248(%rbp)
	movq	(%rdx), %rdi
	call	_ZN2v88internal8compiler17ProjectionIndexOfEPKNS1_8OperatorE@PLT
	cmpq	%rax, -248(%rbp)
	movq	-256(%rbp), %rdx
	movl	$1, %r8d
	movq	-264(%rbp), %rsi
	jne	.L620
	movq	(%r15), %rax
	movl	20(%rdx), %ecx
	leaq	.LC85(%rip), %rdi
	movl	20(%r13), %r8d
	movl	20(%r15), %esi
	movq	8(%rax), %rdx
	andl	$16777215, %ecx
	xorl	%eax, %eax
	andl	$16777215, %esi
	andl	$16777215, %r8d
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L649:
	movq	16(%rax), %rax
	cmpq	%rax, %r15
	jne	.L620
	jmp	.L650
.L644:
	leaq	.LC82(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L645:
	leaq	.LC83(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L646:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10890:
	.size	_ZN2v88internal8compiler8Verifier3RunEPNS1_5GraphENS2_6TypingENS2_11CheckInputsENS2_8CodeTypeE, .-_ZN2v88internal8compiler8Verifier3RunEPNS1_5GraphENS2_6TypingENS2_11CheckInputsENS2_8CodeTypeE
	.section	.rodata._ZNSt6vectorIPN2v88internal9BitVectorENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_.str1.1,"aMS",@progbits,1
.LC86:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIPN2v88internal9BitVectorENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_,"axG",@progbits,_ZNSt6vectorIPN2v88internal9BitVectorENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal9BitVectorENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	.type	_ZNSt6vectorIPN2v88internal9BitVectorENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_, @function
_ZNSt6vectorIPN2v88internal9BitVectorENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_:
.LFB12226:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L761
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rdi
	movq	24(%r12), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	jb	.L654
	movq	%rdi, %r8
	movq	(%rcx), %xmm0
	subq	%rsi, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L655
	leaq	0(,%rdx,8), %r14
	movq	%rdi, %rdx
	subq	%r14, %rdx
	cmpq	%rdx, %rdi
	je	.L702
	leaq	-8(%rdi), %rax
	movl	$16, %ecx
	subq	%rdx, %rax
	subq	%r14, %rcx
	shrq	$3, %rax
	testq	%rcx, %rcx
	leaq	16(%rdi), %rcx
	setle	%sil
	cmpq	%rcx, %rdx
	setnb	%cl
	orb	%cl, %sil
	je	.L703
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L703
	leaq	1(%rax), %r8
	xorl	%eax, %eax
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L658:
	movdqu	(%rdx,%rax), %xmm2
	movups	%xmm2, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L658
	movq	%r8, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	leaq	(%rdx,%rcx), %rsi
	addq	%rdi, %rcx
	cmpq	%rax, %r8
	je	.L660
	movq	(%rsi), %rax
	movq	%rax, (%rcx)
.L660:
	movq	16(%r12), %rax
.L656:
	addq	%r14, %rax
	movq	%rax, 16(%r12)
	cmpq	%rdx, %rbx
	je	.L661
	subq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%xmm0, -40(%rbp)
	subq	%rdx, %rdi
	call	memmove@PLT
	movq	-40(%rbp), %xmm0
.L661:
	leaq	(%rbx,%r14), %rdx
	cmpq	%rdx, %rbx
	je	.L651
	subq	%rbx, %rdx
	movq	%rbx, %rax
	subq	$8, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rdx, %rdx
	je	.L701
	movq	%rcx, %rdx
	movdqa	%xmm0, %xmm1
	shrq	%rdx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rdx
	addq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L665:
	movups	%xmm1, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L665
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	(%rbx,%rax,8), %r13
	cmpq	%rax, %rcx
	je	.L651
.L701:
	movq	%xmm0, 0(%r13)
.L651:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L761:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L655:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	subq	%rax, %rdx
	je	.L704
	cmpq	$1, %rdx
	je	.L705
	movq	%rdx, %rsi
	movdqa	%xmm0, %xmm1
	xorl	%eax, %eax
	shrq	%rsi
	punpcklqdq	%xmm1, %xmm1
	.p2align 4,,10
	.p2align 3
.L669:
	movq	%rax, %rcx
	addq	$1, %rax
	salq	$4, %rcx
	movups	%xmm1, (%rdi,%rcx)
	cmpq	%rax, %rsi
	jne	.L669
	movq	%rdx, %rcx
	andq	$-2, %rcx
	leaq	(%rdi,%rcx,8), %rax
	cmpq	%rcx, %rdx
	je	.L670
.L668:
	movq	%xmm0, (%rax)
.L670:
	leaq	(%rdi,%rdx,8), %rsi
.L667:
	movq	%rsi, 16(%r12)
	cmpq	%rbx, %rdi
	je	.L671
	subq	%rbx, %rdi
	leaq	15(%rbx), %rcx
	movq	%rbx, %rdx
	leaq	-8(%rdi), %r9
	subq	%rsi, %rcx
	movq	%r9, %rax
	shrq	$3, %rax
	cmpq	$30, %rcx
	jbe	.L706
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L706
	leaq	1(%rax), %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L673:
	movdqu	(%rbx,%rax), %xmm3
	movups	%xmm3, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L673
	movq	%rcx, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rax
	leaq	(%rbx,%rax), %rdi
	addq	%rax, %rsi
	cmpq	%r9, %rcx
	je	.L674
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
.L674:
	addq	%r8, 16(%r12)
	movq	%rcx, %rax
.L700:
	movq	%rax, %rcx
	movdqa	%xmm0, %xmm1
	shrq	%rcx
	punpcklqdq	%xmm1, %xmm1
	salq	$4, %rcx
	addq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L678:
	movups	%xmm1, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L678
	movq	%rax, %rdx
	andq	$-2, %rdx
	leaq	(%rbx,%rdx,8), %r13
	cmpq	%rax, %rdx
	jne	.L701
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L654:
	movq	8(%r12), %rsi
	movl	$268435455, %r8d
	movq	%r8, %rax
	subq	%rsi, %rdi
	sarq	$3, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L764
	cmpq	%rdi, %rdx
	movq	%rdi, %rax
	cmovnb	%rdx, %rax
	addq	%rax, %rdi
	setc	%al
	subq	%rsi, %r13
	movzbl	%al, %eax
	testq	%rax, %rax
	jne	.L707
	testq	%rdi, %rdi
	jne	.L765
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L683:
	leaq	0(,%rdx,8), %r9
	leaq	(%rax,%r13), %r11
	leaq	0(%r13,%r9), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	leaq	8(%rcx), %rsi
	setnb	%dil
	cmpq	%rsi, %r11
	setnb	%sil
	orb	%sil, %dil
	je	.L709
	leaq	-1(%rdx), %rsi
	cmpq	$3, %rsi
	jbe	.L709
	movq	(%rcx), %xmm0
	leaq	-2(%rdx), %rdi
	xorl	%esi, %esi
	shrq	%rdi
	addq	$1, %rdi
	punpcklqdq	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L687:
	movq	%rsi, %r10
	addq	$1, %rsi
	salq	$4, %r10
	movups	%xmm0, (%r11,%r10)
	cmpq	%rsi, %rdi
	ja	.L687
	leaq	(%rdi,%rdi), %rsi
	salq	$4, %rdi
	addq	%r11, %rdi
	cmpq	%rsi, %rdx
	je	.L689
	movq	(%rcx), %rdx
	movq	%rdx, (%rdi)
.L689:
	movq	8(%r12), %rcx
	cmpq	%rcx, %rbx
	je	.L710
	leaq	-8(%rbx), %r10
	leaq	15(%rcx), %rdx
	subq	%rcx, %r10
	subq	%rax, %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L711
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L711
	leaq	1(%rsi), %r11
	xorl	%edx, %edx
	movq	%r11, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L692:
	movdqu	(%rcx,%rdx), %xmm4
	movups	%xmm4, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L692
	movq	%r11, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %rcx
	addq	%rax, %rsi
	cmpq	%rdx, %r11
	je	.L694
	movq	(%rcx), %rdx
	movq	%rdx, (%rsi)
.L694:
	leaq	8(%rax,%r10), %rdi
.L690:
	movq	16(%r12), %rdx
	leaq	(%rdi,%r9), %rcx
	cmpq	%rdx, %rbx
	je	.L695
	subq	%rbx, %rdx
	leaq	-8(%rdx), %r10
	leaq	16(%rdi,%r9), %rdx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	%rdx, %rbx
	leaq	16(%rbx), %rdx
	setnb	%dil
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %dil
	je	.L712
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L712
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L697:
	movdqu	(%rbx,%rdx), %xmm5
	movups	%xmm5, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L697
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rdi
	addq	%rdi, %rbx
	addq	%rcx, %rdi
	cmpq	%rdx, %rsi
	je	.L699
	movq	(%rbx), %rdx
	movq	%rdx, (%rdi)
.L699:
	leaq	8(%rcx,%r10), %rcx
.L695:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%r8, 24(%r12)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L707:
	.cfi_restore_state
	movl	$2147483640, %esi
	movl	$2147483640, %r14d
.L682:
	movq	(%r12), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L766
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L685:
	leaq	(%rax,%r14), %r8
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L709:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L686:
	movq	(%rcx), %rdi
	movq	%rdi, (%r11,%rsi,8)
	addq	$1, %rsi
	cmpq	%rsi, %rdx
	jne	.L686
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L703:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L657:
	movq	(%rdx,%rcx,8), %rsi
	movq	%rsi, (%rdi,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rax, %rsi
	jne	.L657
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L706:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L672:
	movq	(%rbx,%rcx,8), %rdi
	movq	%rdi, (%rsi,%rcx,8)
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	%rax, %rdi
	jne	.L672
	addq	%r8, 16(%r12)
	addq	$1, %rax
	testq	%r9, %r9
	je	.L701
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L711:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L691:
	movq	(%rcx,%rdx,8), %rdi
	movq	%rdi, (%rax,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L691
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L712:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L696:
	movq	(%rbx,%rdx,8), %rdi
	movq	%rdi, (%rcx,%rdx,8)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rsi, %rdi
	jne	.L696
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L704:
	movq	%rdi, %rsi
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L702:
	movq	%rdi, %rax
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L671:
	addq	%r8, %rsi
	movq	%rsi, 16(%r12)
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L710:
	movq	%rax, %rdi
	jmp	.L690
.L705:
	movq	%rdi, %rax
	jmp	.L668
.L766:
	movq	%r8, %rdi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rcx
	jmp	.L685
.L765:
	cmpq	$268435455, %rdi
	cmova	%r8, %rdi
	leaq	0(,%rdi,8), %r14
	movq	%r14, %rsi
	jmp	.L682
.L764:
	leaq	.LC86(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12226:
	.size	_ZNSt6vectorIPN2v88internal9BitVectorENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_, .-_ZNSt6vectorIPN2v88internal9BitVectorENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	.section	.rodata._ZNSt5dequeIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC87:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.type	_ZNSt5dequeIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, @function
_ZNSt5dequeIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_:
.LFB12515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r13
	movq	56(%rdi), %rsi
	movq	64(%rbx), %rdx
	subq	72(%rbx), %rdx
	movq	%r13, %r14
	movq	%rdx, %rcx
	subq	%rsi, %r14
	sarq	$3, %rcx
	movq	%r14, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	48(%rbx), %rax
	subq	32(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$268435455, %rax
	je	.L785
	movq	16(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	%r13, %rax
	subq	%rcx, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L786
.L769:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L777
	cmpq	$63, 8(%rax)
	ja	.L787
.L777:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L788
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L778:
	movq	%rax, 8(%r13)
	movq	(%r12), %rdx
	movq	64(%rbx), %rax
	movq	%rdx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L786:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L789
	testq	%rdx, %rdx
	movl	$1, %eax
	movq	(%rbx), %rdi
	cmovne	%rdx, %rax
	movq	16(%rdi), %r13
	leaq	2(%rdx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L790
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L774:
	movq	%rcx, %rax
	movq	56(%rbx), %rsi
	subq	%r15, %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r15
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L775
	subq	%rsi, %rdx
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L775:
	movq	24(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L776
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L776:
	movq	%r13, 16(%rbx)
	movq	%rcx, 24(%rbx)
.L772:
	movq	%r15, 56(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r14), %r13
	movq	(%r15), %xmm0
	movq	%r13, 88(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 72(%rbx)
	addq	$512, %rax
	movq	%rax, 80(%rbx)
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L787:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L789:
	subq	%r15, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%rcx,%rdx,8), %r15
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L771
	cmpq	%r13, %rsi
	je	.L772
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L771:
	cmpq	%r13, %rsi
	je	.L772
	leaq	8(%r14), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L788:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L790:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L774
.L785:
	leaq	.LC87(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12515:
	.size	_ZNSt5dequeIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_, .-_ZNSt5dequeIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	.section	.text._ZNSt11_Deque_baseIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm:
.LFB12805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$6, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	1(%rsi), %rbx
	addq	$3, %rsi
	subq	$8, %rsp
	cmpq	$8, %rsi
	cmovb	%rax, %rsi
	movq	%rsi, 24(%rdi)
	movq	(%rdi), %rdi
	salq	$3, %rsi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L805
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L793:
	movq	24(%r12), %rax
	movq	%rdx, 16(%r12)
	subq	%rbx, %rax
	shrq	%rax
	leaq	(%rdx,%rax,8), %r15
	leaq	(%r15,%rbx,8), %r14
	cmpq	%r14, %r15
	jnb	.L794
	movq	%r15, %rbx
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L795:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L806
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L796:
	movq	%rax, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jbe	.L794
.L799:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L795
	cmpq	$63, 8(%rax)
	jbe	.L795
	movq	(%rax), %rdx
	addq	$8, %rbx
	movq	%rdx, 8(%r12)
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r14
	ja	.L799
.L794:
	movq	%r15, 56(%r12)
	movq	(%r15), %rdx
	andl	$63, %r13d
	leaq	512(%rdx), %rax
	movq	%rdx, 40(%r12)
	movq	%rax, 48(%r12)
	leaq	-8(%r14), %rax
	movq	%rax, 88(%r12)
	movq	-8(%r14), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 72(%r12)
	leaq	512(%rax), %rcx
	leaq	(%rax,%r13,8), %rax
	movq	%rcx, 80(%r12)
	movq	%rax, 64(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L806:
	.cfi_restore_state
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L805:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L793
	.cfi_endproc
.LFE12805:
	.size	_ZNSt11_Deque_baseIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	.section	.text._ZN2v88internal9ZoneQueueIPNS0_8compiler10BasicBlockEEC2EPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal9ZoneQueueIPNS0_8compiler10BasicBlockEEC5EPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9ZoneQueueIPNS0_8compiler10BasicBlockEEC2EPNS0_4ZoneE
	.type	_ZN2v88internal9ZoneQueueIPNS0_8compiler10BasicBlockEEC2EPNS0_4ZoneE, @function
_ZN2v88internal9ZoneQueueIPNS0_8compiler10BasicBlockEEC2EPNS0_4ZoneE:
.LFB11783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-224(%rbp), %rdi
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -224(%rbp)
	xorl	%esi, %esi
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -208(%rbp)
	je	.L824
	movdqa	-224(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rdi
	xorl	%esi, %esi
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	movaps	%xmm0, -48(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movq	-184(%rbp), %r9
	movq	-112(%rbp), %rsi
	movdqa	-64(%rbp), %xmm5
	movq	-168(%rbp), %r8
	movq	-192(%rbp), %xmm3
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %xmm1
	movaps	%xmm5, -160(%rbp)
	movq	%r9, %xmm5
	movq	-176(%rbp), %xmm2
	punpcklqdq	%xmm5, %xmm3
	movq	-136(%rbp), %rdx
	movq	-104(%rbp), %rdi
	movups	%xmm3, 32(%rbx)
	movq	%r8, %xmm3
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	punpcklqdq	%xmm3, %xmm2
	movq	-216(%rbp), %rax
	movq	-144(%rbp), %xmm0
	movups	%xmm2, 48(%rbx)
	movq	%rcx, %xmm2
	movq	-224(%rbp), %xmm4
	movq	-208(%rbp), %r11
	punpcklqdq	%xmm2, %xmm1
	movq	-200(%rbp), %r10
	movaps	%xmm6, -192(%rbp)
	movdqa	-48(%rbp), %xmm6
	movaps	%xmm7, -176(%rbp)
	movq	%rax, %xmm7
	movups	%xmm1, 64(%rbx)
	movq	%rdx, %xmm1
	punpcklqdq	%xmm7, %xmm4
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, -208(%rbp)
	movq	%rdi, -200(%rbp)
	movq	%r11, 16(%rbx)
	movq	%r10, 24(%rbx)
	movaps	%xmm6, -144(%rbp)
	movups	%xmm4, (%rbx)
	movups	%xmm0, 80(%rbx)
	testq	%rsi, %rsi
	je	.L807
	movq	-136(%rbp), %rcx
	movq	-168(%rbp), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L810
	.p2align 4,,10
	.p2align 3
.L813:
	testq	%rax, %rax
	je	.L811
	cmpq	$64, 8(%rax)
	ja	.L812
.L811:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-216(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -216(%rbp)
.L812:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L813
	movq	-200(%rbp), %rdi
	movq	-208(%rbp), %rsi
.L810:
	leaq	0(,%rdi,8), %rax
	cmpq	$15, %rax
	jbe	.L807
	movq	%rdi, 8(%rsi)
	movq	$0, (%rsi)
.L807:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L825
	addq	$216, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L824:
	.cfi_restore_state
	movdqa	-224(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm7
	movq	$0, 16(%rbx)
	movq	-200(%rbp), %rax
	movdqa	-144(%rbp), %xmm1
	movups	%xmm6, (%rbx)
	movdqa	-176(%rbp), %xmm6
	movups	%xmm7, 32(%rbx)
	movdqa	-160(%rbp), %xmm7
	movq	%rax, 24(%rbx)
	movups	%xmm6, 48(%rbx)
	movups	%xmm7, 64(%rbx)
	movups	%xmm1, 80(%rbx)
	jmp	.L807
.L825:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11783:
	.size	_ZN2v88internal9ZoneQueueIPNS0_8compiler10BasicBlockEEC2EPNS0_4ZoneE, .-_ZN2v88internal9ZoneQueueIPNS0_8compiler10BasicBlockEEC2EPNS0_4ZoneE
	.weak	_ZN2v88internal9ZoneQueueIPNS0_8compiler10BasicBlockEEC1EPNS0_4ZoneE
	.set	_ZN2v88internal9ZoneQueueIPNS0_8compiler10BasicBlockEEC1EPNS0_4ZoneE,_ZN2v88internal9ZoneQueueIPNS0_8compiler10BasicBlockEEC2EPNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE.str1.8,"aMS",@progbits,1
	.align 8
.LC88:
	.string	"../deps/v8/src/compiler/verifier.cc:1988"
	.section	.rodata._ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE.str1.1,"aMS",@progbits,1
.LC89:
	.string	"count >= rpo_order->size()"
	.section	.rodata._ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE.str1.8
	.align 8
.LC90:
	.string	"(*b) == schedule->GetBlockById((*b)->id())"
	.align 8
.LC91:
	.string	"predecessor->rpo_number() >= 0"
	.align 8
.LC92:
	.string	"predecessor == schedule->GetBlockById(predecessor->id())"
	.section	.rodata._ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE.str1.1
.LC93:
	.string	"successor->rpo_number() >= 0"
	.section	.rodata._ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE.str1.8
	.align 8
.LC94:
	.string	"successor == schedule->GetBlockById(successor->id())"
	.align 8
.LC95:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.rodata._ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE.str1.1
.LC96:
	.string	"start == rpo_order->at(0)"
	.section	.rodata._ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE.str1.8
	.align 8
.LC97:
	.string	"static_cast<int>(b) == block->rpo_number()"
	.section	.rodata._ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE.str1.1
.LC98:
	.string	"(dom) == nullptr"
.LC99:
	.string	"(dom) != nullptr"
	.section	.rodata._ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE.str1.8
	.align 8
.LC100:
	.string	"dom->rpo_number() < block->rpo_number()"
	.section	.rodata._ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE.str1.1
.LC101:
	.string	"block->rpo_number() >= 0"
	.section	.rodata._ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE.str1.8
	.align 8
.LC102:
	.string	"block == rpo_order->at(block->rpo_number())"
	.align 8
.LC103:
	.string	"marked[rpo_order->at(b)->id().ToSize()]"
	.align 8
.LC104:
	.string	"Block B%d is not dominated by B%d"
	.align 8
.LC105:
	.string	"Block B%d is not immediately dominated by B%d"
	.align 8
.LC106:
	.string	"control->opcode() == IrOpcode::kMerge || control->opcode() == IrOpcode::kLoop"
	.align 8
.LC107:
	.string	"(*b) == schedule->block(control)"
	.align 8
.LC108:
	.string	"block == schedule->block(control)"
	.section	.text._ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE
	.type	_ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE, @function
_ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE:
.LFB10903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC88(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %rbx
	subq	16(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	sarq	$3, %rbx
	movq	%rbx, -448(%rbp)
	movq	32(%rax), %rsi
	leaq	-416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -440(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	104(%r12), %rax
	movq	88(%r12), %rdx
	movq	80(%r12), %r13
	movq	%rax, -424(%rbp)
	movq	%rdx, %rax
	subq	%r13, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	ja	.L1089
	cmpq	%rdx, %r13
	je	.L828
.L835:
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	160(%rax), %rsi
	call	_ZN2v88internal8compiler8Schedule12GetBlockByIdENS1_10BasicBlock2IdE@PLT
	movq	0(%r13), %rdx
	cmpq	%rdx, %rax
	jne	.L1090
	movq	136(%rax), %rbx
	movq	144(%rax), %r14
	cmpq	%r14, %rbx
	je	.L830
.L833:
	movq	(%rbx), %r15
	movl	4(%r15), %edx
	testl	%edx, %edx
	js	.L1091
	movq	160(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Schedule12GetBlockByIdENS1_10BasicBlock2IdE@PLT
	cmpq	%rax, %r15
	jne	.L1092
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L833
	movq	0(%r13), %rdx
.L830:
	movq	112(%rdx), %r15
	movq	104(%rdx), %rbx
	cmpq	%r15, %rbx
	je	.L840
.L839:
	movq	(%rbx), %r14
	movl	4(%r14), %eax
	testl	%eax, %eax
	js	.L1093
	movq	160(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Schedule12GetBlockByIdENS1_10BasicBlock2IdE@PLT
	cmpq	%rax, %r14
	jne	.L1094
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.L839
.L840:
	addq	$8, %r13
	cmpq	%r13, 88(%r12)
	jne	.L835
	movq	80(%r12), %rdx
	movq	%r13, %rax
	subq	%rdx, %rax
	sarq	$3, %rax
.L828:
	testq	%rax, %rax
	je	.L1095
	movq	-424(%rbp), %rdi
	xorl	%ecx, %ecx
	cmpq	%rdi, (%rdx)
	jne	.L1096
.L842:
	movq	(%rdx,%rcx,8), %rsi
	movl	4(%rsi), %edi
	cmpl	%ecx, %edi
	jne	.L1097
	movq	16(%rsi), %rsi
	testq	%rcx, %rcx
	je	.L1098
	testq	%rsi, %rsi
	je	.L1099
	cmpl	4(%rsi), %edi
	jle	.L1100
.L845:
	addq	$1, %rcx
	cmpq	%rax, %rcx
	jne	.L842
	movq	-448(%rbp), %rax
	xorl	%r15d, %r15d
	movl	%eax, -468(%rbp)
	cltq
	testq	%rax, %rax
	jne	.L1101
.L850:
	movq	-440(%rbp), %rax
	leaq	-256(%rbp), %r13
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r13, %rdi
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	%rax, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	cmpq	$0, -240(%rbp)
	pxor	%xmm0, %xmm0
	je	.L1102
	movdqa	-256(%rbp), %xmm7
	leaq	-160(%rbp), %rax
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movq	%rax, %rdi
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE17_M_initialize_mapEm
	movdqa	-112(%rbp), %xmm6
	movq	-216(%rbp), %rsi
	movq	-224(%rbp), %xmm3
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %xmm2
	movaps	%xmm6, -208(%rbp)
	movq	%rsi, %xmm6
	movq	-184(%rbp), %rdx
	punpcklqdq	%xmm6, %xmm3
	movq	-144(%rbp), %r10
	movq	-192(%rbp), %xmm1
	movaps	%xmm3, -320(%rbp)
	movq	%rcx, %xmm3
	movq	-168(%rbp), %rax
	movq	-240(%rbp), %r9
	punpcklqdq	%xmm3, %xmm2
	movq	-232(%rbp), %rdi
	movdqa	-128(%rbp), %xmm5
	movq	%r10, -240(%rbp)
	movaps	%xmm2, -304(%rbp)
	movq	-136(%rbp), %r10
	movq	%rdx, %xmm2
	movq	-176(%rbp), %xmm0
	punpcklqdq	%xmm2, %xmm1
	movdqa	-96(%rbp), %xmm7
	movq	-256(%rbp), %xmm4
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm1, -288(%rbp)
	movdqa	-80(%rbp), %xmm5
	movq	%rax, %xmm1
	movq	%r10, -232(%rbp)
	movhps	-248(%rbp), %xmm4
	punpcklqdq	%xmm1, %xmm0
	movq	%r9, -336(%rbp)
	movq	%rdi, -328(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -352(%rbp)
	movaps	%xmm0, -272(%rbp)
.L852:
	cmpq	$0, -240(%rbp)
	je	.L853
	movq	-168(%rbp), %rax
	movq	-248(%rbp), %rdx
	leaq	8(%rax), %rcx
	movq	-200(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L859
.L860:
	testq	%rdx, %rdx
	je	.L857
	cmpq	$64, 8(%rdx)
	ja	.L858
.L857:
	movq	(%rax), %rdx
	movq	$64, 8(%rdx)
	movq	-248(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	%rdx, -248(%rbp)
.L858:
	addq	$8, %rax
	cmpq	%rax, %rcx
	ja	.L860
.L859:
	movq	-232(%rbp), %rdx
	leaq	0(,%rdx,8), %rax
	cmpq	$15, %rax
	jbe	.L853
	movq	-240(%rbp), %rax
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L853:
	movq	-272(%rbp), %rax
	movq	-288(%rbp), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L861
	movq	-424(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, -288(%rbp)
.L862:
	movq	-424(%rbp), %rax
	movq	160(%rax), %rcx
	movl	$1, %eax
	movq	%rcx, %rdx
	salq	%cl, %rax
	shrq	$6, %rdx
	orq	%rax, (%r15,%rdx,8)
	movq	-320(%rbp), %rax
	cmpq	%rax, -288(%rbp)
	je	.L863
	movl	$1, %edx
.L869:
	movq	-304(%rbp), %rcx
	movq	(%rax), %r14
	subq	$8, %rcx
	cmpq	%rcx, %rax
	je	.L864
	addq	$8, %rax
	movq	%rax, -320(%rbp)
.L865:
	movq	104(%r14), %rsi
	movq	112(%r14), %rdi
	xorl	%ebx, %ebx
	cmpq	%rdi, %rsi
	jne	.L868
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L870:
	movq	%rdi, %rax
	addq	$1, %rbx
	subq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L1103
.L868:
	movq	(%rsi,%rbx,8), %rax
	movq	%rax, -160(%rbp)
	movq	160(%rax), %rcx
	movq	%rcx, %rax
	shrq	$6, %rax
	leaq	(%r15,%rax,8), %r8
	movq	%rdx, %rax
	salq	%cl, %rax
	movq	%rax, %rcx
	movq	(%r8), %rax
	testq	%rcx, %rax
	jne	.L870
	orq	%rcx, %rax
	movq	%rax, (%r8)
	movq	-272(%rbp), %rax
	movq	-288(%rbp), %rcx
	subq	$8, %rax
	cmpq	%rax, %rcx
	je	.L871
	movq	-160(%rbp), %rax
	movq	%rax, (%rcx)
	addq	$8, -288(%rbp)
	movq	104(%r14), %rsi
	movq	112(%r14), %rdi
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L1091:
	leaq	.LC91(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1092:
	leaq	.LC92(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1089:
	leaq	.LC89(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1090:
	leaq	.LC90(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1093:
	leaq	.LC93(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1094:
	leaq	.LC94(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1098:
	testq	%rsi, %rsi
	je	.L845
	leaq	.LC98(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1103:
	movq	-320(%rbp), %rax
.L872:
	cmpq	%rax, -288(%rbp)
	jne	.L869
.L863:
	cmpq	$0, -336(%rbp)
	je	.L873
	movq	-264(%rbp), %rax
	movq	-344(%rbp), %rdx
	leaq	8(%rax), %rcx
	movq	-296(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L879
.L880:
	testq	%rdx, %rdx
	je	.L877
	cmpq	$64, 8(%rdx)
	ja	.L878
.L877:
	movq	(%rax), %rdx
	movq	$64, 8(%rdx)
	movq	-344(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	%rdx, -344(%rbp)
.L878:
	addq	$8, %rax
	cmpq	%rax, %rcx
	ja	.L880
.L879:
	movq	-328(%rbp), %rdx
	leaq	0(,%rdx,8), %rax
	cmpq	$15, %rax
	jbe	.L873
	movq	-336(%rbp), %rax
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L873:
	cmpq	$0, -448(%rbp)
	je	.L881
	xorl	%r14d, %r14d
	movl	$1, %ebx
	jmp	.L886
.L883:
	movq	80(%r12), %rcx
	movq	88(%r12), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1104
	cmpq	%rax, (%rcx,%rsi,8)
	jne	.L1105
.L882:
	addq	$1, %r14
	cmpq	-448(%rbp), %r14
	je	.L1106
.L886:
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler8Schedule12GetBlockByIdENS1_10BasicBlock2IdE@PLT
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movl	%r14d, %ecx
	shrq	$6, %rdx
	salq	%cl, %rdi
	testq	%rdi, (%r15,%rdx,8)
	je	.L882
	movslq	4(%rax), %rsi
	testl	%esi, %esi
	jns	.L883
	leaq	.LC101(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1097:
	leaq	.LC97(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L871:
	leaq	-160(%rbp), %rax
	leaq	-352(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZNSt5dequeIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movq	104(%r14), %rsi
	movq	112(%r14), %rdi
	movl	$1, %edx
	jmp	.L870
.L1099:
	leaq	.LC99(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1100:
	leaq	.LC100(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L864:
	movq	-344(%rbp), %rax
	testq	%rax, %rax
	je	.L866
	cmpq	$64, 8(%rax)
	ja	.L867
.L866:
	movq	-312(%rbp), %rax
	movq	$64, 8(%rax)
	movq	-344(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	%rax, -344(%rbp)
.L867:
	movq	-296(%rbp), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, -296(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rcx
	movq	%rax, -312(%rbp)
	movq	%rcx, -304(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L865
.L1096:
	leaq	.LC96(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1102:
	movdqa	-256(%rbp), %xmm5
	movdqa	-224(%rbp), %xmm6
	movq	$0, -336(%rbp)
	movq	-232(%rbp), %rax
	movdqa	-208(%rbp), %xmm7
	movaps	%xmm5, -352(%rbp)
	movdqa	-192(%rbp), %xmm5
	movaps	%xmm6, -320(%rbp)
	movdqa	-176(%rbp), %xmm6
	movq	%rax, -328(%rbp)
	movaps	%xmm7, -304(%rbp)
	movaps	%xmm5, -288(%rbp)
	movaps	%xmm6, -272(%rbp)
	jmp	.L852
.L1101:
	leaq	63(%rax), %r13
	movq	-400(%rbp), %r15
	movq	-392(%rbp), %rax
	shrq	$6, %r13
	salq	$3, %r13
	subq	%r15, %rax
	movq	%r13, %rsi
	cmpq	%rax, %r13
	ja	.L1107
	addq	%r15, %rsi
	movq	%rsi, -400(%rbp)
.L849:
	testq	%r15, %r15
	je	.L850
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	memset@PLT
	jmp	.L850
.L861:
	leaq	-424(%rbp), %rsi
	leaq	-352(%rbp), %rdi
	call	_ZNSt5dequeIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L862
.L1106:
	movq	80(%r12), %rdx
	movq	88(%r12), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	je	.L1108
.L987:
	xorl	%esi, %esi
	movl	$1, %edi
	jmp	.L888
.L887:
	addq	$1, %rsi
	cmpq	%rax, %rsi
	je	.L1109
.L888:
	movq	(%rdx,%rsi,8), %rcx
	movq	%rdi, %rbx
	movq	160(%rcx), %rcx
	movq	%rcx, %r8
	salq	%cl, %rbx
	shrq	$6, %r8
	testq	%rbx, (%r15,%r8,8)
	jne	.L887
	leaq	.LC103(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1105:
	leaq	.LC102(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1104:
	leaq	.LC95(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1109:
	movq	-440(%rbp), %rax
	cmpq	$0, -448(%rbp)
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	movq	%rax, -352(%rbp)
	leaq	-160(%rbp), %rax
	movq	$0, -328(%rbp)
	movq	$0, -160(%rbp)
	movq	%rax, -456(%rbp)
	jne	.L986
.L889:
	movq	-440(%rbp), %rsi
	movq	-456(%rbp), %rdi
	call	_ZN2v88internal9ZoneQueueIPNS0_8compiler10BasicBlockEEC1EPNS0_4ZoneE
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L890
	movq	-424(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, -96(%rbp)
.L891:
	movq	-400(%rbp), %r14
	movq	-392(%rbp), %rax
	subq	%r14, %rax
	cmpq	$15, %rax
	jbe	.L1110
	leaq	16(%r14), %rax
	movq	%rax, -400(%rbp)
.L893:
	movq	-448(%rbp), %rax
	movl	%eax, (%r14)
	cmpl	$64, %eax
	jle	.L894
	subl	$1, %eax
	movq	$0, 8(%r14)
	sarl	$6, %eax
	addl	$1, %eax
	movl	%eax, 4(%r14)
	movslq	%eax, %rsi
	movq	-392(%rbp), %rdx
	movl	%eax, -448(%rbp)
	movq	-400(%rbp), %rax
	salq	$3, %rsi
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1111
	addq	%rax, %rsi
	movq	%rsi, -400(%rbp)
.L896:
	movq	%rax, 8(%r14)
	xorl	%eax, %eax
	cmpl	$1, 4(%r14)
	jne	.L897
	jmp	.L1112
.L1113:
	movq	8(%r14), %rdx
	movq	$0, (%rdx,%rax,8)
	addq	$1, %rax
.L897:
	cmpl	%eax, 4(%r14)
	jg	.L1113
.L898:
	movq	-424(%rbp), %rax
	movq	160(%rax), %rdx
	movq	-344(%rbp), %rax
	movq	%r14, (%rax,%rdx,8)
	movq	-128(%rbp), %rax
	cmpq	-96(%rbp), %rax
	je	.L899
	movslq	-448(%rbp), %rdx
	movq	%r12, -480(%rbp)
	movl	$1, %r9d
	leaq	0(,%rdx,8), %rcx
	movq	%rcx, -464(%rbp)
.L908:
	movq	-112(%rbp), %rcx
	movq	(%rax), %rbx
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L900
	addq	$8, %rax
	movq	%rax, -128(%rbp)
.L901:
	movq	-344(%rbp), %rcx
	movq	160(%rbx), %rax
	movq	16(%rbx), %r10
	movq	(%rcx,%rax,8), %r15
	testq	%r10, %r10
	je	.L904
	movq	160(%r10), %rsi
	cmpl	$1, 4(%r15)
	movq	8(%r15), %rdi
	movl	%esi, %eax
	je	.L906
	movl	$64, %r8d
	cltd
	idivl	%r8d
	cltq
	movq	(%rdi,%rax,8), %rdi
.L906:
	movl	%esi, %edx
	sarl	$31, %edx
	shrl	$26, %edx
	leal	(%rdx,%rsi), %eax
	andl	$63, %eax
	subl	%edx, %eax
	btq	%rax, %rdi
	jnc	.L1114
.L904:
	xorl	%r12d, %r12d
	movq	104(%rbx), %rsi
	movq	%r12, %r14
	cmpq	112(%rbx), %rsi
	je	.L947
.L907:
	movq	(%rsi,%r14,8), %rax
	movq	%rax, -256(%rbp)
	movq	160(%rax), %rax
	movq	(%rcx,%rax,8), %r12
	testq	%r12, %r12
	je	.L1115
	movq	160(%rbx), %rdi
	movl	4(%r12), %esi
	movq	8(%r12), %rax
	movl	%edi, %r10d
	sarl	$31, %r10d
	shrl	$26, %r10d
	leal	(%r10,%rdi), %ecx
	andl	$63, %ecx
	subl	%r10d, %ecx
	cmpl	$1, %esi
	je	.L1116
	testl	%edi, %edi
	leal	63(%rdi), %r10d
	cmovs	%r10d, %edi
	sarl	$6, %edi
	movslq	%edi, %rdi
	leaq	(%rax,%rdi,8), %rdi
	movq	(%rdi), %r11
	movq	%r11, %r10
	shrq	%cl, %r10
	andl	$1, %r10d
	je	.L1117
	movl	$1, %eax
	salq	%cl, %rax
	notq	%rax
	andq	%rax, %r11
	movq	%r11, (%rdi)
	movl	4(%r12), %eax
	cmpl	$1, %eax
	je	.L984
	testl	%eax, %eax
	jle	.L941
	movq	8(%r12), %rax
.L940:
	movq	%rax, %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L943:
	leaq	(%rsi,%rax,8), %rcx
	movq	8(%r15), %rsi
	movq	(%rcx), %rdx
	movq	(%rsi,%rax,8), %r11
	andq	%rdx, %r11
	movq	%r11, (%rcx)
	movq	8(%r12), %rsi
	cmpq	(%rsi,%rax,8), %rdx
	cmovne	%r9d, %edi
	addq	$1, %rax
	cmpl	%eax, 4(%r12)
	jg	.L943
.L938:
	testb	%dil, %dil
	jne	.L1118
.L944:
	testb	%r10b, %r10b
	je	.L931
	movq	160(%rbx), %rcx
	cmpl	$1, 4(%r12)
	movl	%ecx, %eax
	je	.L1119
.L946:
	testl	%eax, %eax
	leal	63(%rax), %esi
	movl	%eax, %r10d
	movq	8(%r12), %rdi
	cmovns	%eax, %esi
	sarl	$31, %r10d
	shrl	$26, %r10d
	leal	(%rax,%r10), %ecx
	sarl	$6, %esi
	movl	$1, %eax
	andl	$63, %ecx
	movslq	%esi, %rsi
	subl	%r10d, %ecx
	salq	%cl, %rax
	orq	%rax, (%rdi,%rsi,8)
.L931:
	movq	104(%rbx), %rsi
	movq	112(%rbx), %rax
	addq	$1, %r14
	subq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rax, %r14
	jnb	.L947
	movq	-344(%rbp), %rcx
	jmp	.L907
.L1108:
	movq	-440(%rbp), %rax
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -328(%rbp)
	movq	$0, -160(%rbp)
.L986:
	leaq	-160(%rbp), %rax
	movq	-448(%rbp), %rdx
	leaq	-352(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, %rcx
	movq	%rax, -456(%rbp)
	call	_ZNSt6vectorIPN2v88internal9BitVectorENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	jmp	.L889
.L1095:
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC95(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1107:
	movq	-440(%rbp), %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L849
.L881:
	movq	80(%r12), %rdx
	movq	88(%r12), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	jne	.L987
	movq	-440(%rbp), %rax
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	movq	%rax, -352(%rbp)
	leaq	-160(%rbp), %rax
	movq	$0, -328(%rbp)
	movq	%rax, -456(%rbp)
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L1118:
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rcx
	subq	$8, %rax
	cmpq	%rax, %rcx
	je	.L945
	movq	-256(%rbp), %rax
	movq	%rax, (%rcx)
	addq	$8, -96(%rbp)
	jmp	.L944
.L1116:
	movq	%rax, %r10
	shrq	%cl, %r10
	andl	$1, %r10d
	je	.L984
	movl	%edi, %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	movq	%rdx, %rcx
	notq	%rcx
	andq	%rcx, %rax
	movq	%rax, 8(%r12)
.L984:
	movq	8(%r12), %rcx
	movq	8(%r15), %rax
	andq	%rcx, %rax
	cmpq	%rax, %rcx
	movq	%rax, 8(%r12)
	setne	%dil
	jmp	.L938
.L1117:
	testl	%esi, %esi
	jg	.L940
	jmp	.L931
.L1115:
	movq	-400(%rbp), %r12
	movq	-392(%rbp), %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L1120
	leaq	16(%r12), %rax
	movq	%rax, -400(%rbp)
.L911:
	movq	$0, 8(%r12)
	movl	-468(%rbp), %eax
	movl	%eax, (%r12)
	movl	-448(%rbp), %eax
	movl	%eax, 4(%r12)
	cmpl	$1, %eax
	jne	.L1121
.L912:
	movq	8(%r15), %rcx
.L985:
	movq	%rcx, 8(%r12)
	movl	160(%rbx), %r10d
.L919:
	btsq	%r10, %rcx
	movq	%rcx, 8(%r12)
.L929:
	movq	-256(%rbp), %rax
	movq	160(%rax), %rcx
	movq	-344(%rbp), %rax
	movq	%r12, (%rax,%rcx,8)
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rcx
	subq	$8, %rax
	cmpq	%rax, %rcx
	je	.L930
	movq	-256(%rbp), %rax
	movq	%rax, (%rcx)
	addq	$8, -96(%rbp)
	jmp	.L931
.L1119:
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, 8(%r12)
	jmp	.L931
.L947:
	movq	-128(%rbp), %rax
	cmpq	%rax, -96(%rbp)
	jne	.L908
	movq	-480(%rbp), %r12
.L899:
	movq	80(%r12), %r14
	cmpq	%r14, 88(%r12)
	je	.L961
.L960:
	movq	(%r14), %r15
	movq	16(%r15), %rbx
	testq	%rbx, %rbx
	je	.L951
	movq	160(%r15), %rdx
	movq	-344(%rbp), %rax
	movq	(%rax,%rdx,8), %rax
	movl	$0, -248(%rbp)
	movq	%rax, -256(%rbp)
	cmpl	$1, 4(%rax)
	movq	8(%rax), %rax
	je	.L953
	movq	(%rax), %rax
.L953:
	movq	%rax, -240(%rbp)
	movl	$-1, -232(%rbp)
.L1088:
	movq	%r13, %rdi
	call	_ZN2v88internal9BitVector8Iterator7AdvanceEv@PLT
	movq	-256(%rbp), %rax
	movl	4(%rax), %eax
	cmpl	%eax, -248(%rbp)
	jge	.L951
	movslq	-232(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler8Schedule12GetBlockByIdENS1_10BasicBlock2IdE@PLT
	cmpq	%rbx, %rax
	je	.L1088
	movq	160(%rbx), %rcx
	movq	-344(%rbp), %rdx
	movq	160(%rax), %rax
	movq	(%rdx,%rcx,8), %rdx
	cmpl	$1, 4(%rdx)
	movq	8(%rdx), %rcx
	je	.L957
	testl	%eax, %eax
	leal	63(%rax), %edx
	cmovns	%eax, %edx
	sarl	$6, %edx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
.L957:
	cltd
	shrl	$26, %edx
	addl	%edx, %eax
	andl	$63, %eax
	subl	%edx, %eax
	btq	%rax, %rcx
	jc	.L1088
	movl	4(%rbx), %edx
	movl	4(%r15), %esi
	leaq	.LC105(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L951:
	addq	$8, %r14
	cmpq	%r14, 88(%r12)
	jne	.L960
.L961:
	cmpq	$0, -144(%rbp)
	je	.L950
	movq	-72(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-104(%rbp), %rax
	jmp	.L965
.L1122:
	movq	-152(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L963
	cmpq	$64, 8(%rcx)
	ja	.L964
.L963:
	movq	(%rax), %rcx
	movq	$64, 8(%rcx)
	movq	-152(%rbp), %rsi
	movq	%rsi, (%rcx)
	movq	%rcx, -152(%rbp)
.L964:
	addq	$8, %rax
.L965:
	cmpq	%rax, %rdx
	ja	.L1122
	movq	-136(%rbp), %rdx
	leaq	0(,%rdx,8), %rax
	cmpq	$15, %rax
	jbe	.L950
	movq	-144(%rbp), %rax
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L950:
	movq	80(%r12), %rbx
.L969:
	cmpq	%rbx, 88(%r12)
	je	.L967
	movq	(%rbx), %rax
	movq	80(%rax), %rdx
	movq	72(%rax), %r13
	cmpq	%rdx, %r13
	jne	.L976
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L970:
	addq	$8, %r13
	cmpq	%r13, %rdx
	je	.L977
.L976:
	movq	0(%r13), %rdi
	movq	(%rdi), %rcx
	cmpw	$35, 16(%rcx)
	jne	.L970
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L972
	movq	32(%rdi), %rax
	movl	8(%rax), %eax
.L972:
	cmpl	%eax, 20(%rcx)
	jge	.L970
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	%rax, %rsi
	movq	(%rax), %rax
	movzwl	16(%rax), %eax
	cmpl	$1, %eax
	je	.L996
	cmpl	$10, %eax
	jne	.L973
.L996:
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	cmpq	(%rbx), %rax
	jne	.L975
	movq	80(%rax), %rdx
	jmp	.L970
.L1121:
	movq	-400(%rbp), %rax
	movq	-392(%rbp), %rcx
	subq	%rax, %rcx
	cmpq	%rcx, -464(%rbp)
	ja	.L1123
	movq	-464(%rbp), %rcx
	addq	%rax, %rcx
	movq	%rcx, -400(%rbp)
.L914:
	movq	%rax, 8(%r12)
	movl	4(%r12), %eax
	cmpl	$1, %eax
	je	.L1124
	testl	%eax, %eax
	jle	.L916
	xorl	%eax, %eax
.L917:
	movq	8(%r12), %rdx
	movq	$0, (%rdx,%rax,8)
	movl	4(%r12), %esi
	addq	$1, %rax
	cmpl	%eax, %esi
	jg	.L917
	movl	4(%r15), %eax
	movq	8(%r15), %rcx
	cmpl	$1, %esi
	je	.L985
.L918:
	movq	8(%r12), %rsi
	cmpl	$1, %eax
	je	.L1125
	testl	%eax, %eax
	jle	.L925
	movq	(%rcx), %rdi
	movq	%rdi, (%rsi)
	leal	-2(%rax), %esi
	leaq	16(,%rsi,8), %r11
	movl	$8, %esi
.L926:
	movq	(%rsi,%rcx), %rdi
	movq	8(%r12), %rdx
	movq	%rdi, (%rdx,%rsi)
	addq	$8, %rsi
	cmpq	%r11, %rsi
	jne	.L926
	movq	8(%r12), %rsi
.L925:
	movl	4(%r12), %edi
	movslq	%eax, %rcx
	salq	$3, %rcx
	cmpl	%eax, %edi
	jle	.L921
.L927:
	movq	$0, (%rsi,%rcx)
	movl	4(%r12), %edi
	addl	$1, %eax
	addq	$8, %rcx
	movq	8(%r12), %rsi
	cmpl	%eax, %edi
	jg	.L927
.L921:
	movq	160(%rbx), %rax
	movq	%rsi, %rcx
	movl	%eax, %r10d
	cmpl	$1, %edi
	je	.L919
	testl	%eax, %eax
	leal	63(%rax), %edi
	cmovns	%eax, %edi
	sarl	$31, %r10d
	shrl	$26, %r10d
	leal	(%r10,%rax), %ecx
	sarl	$6, %edi
	movl	$1, %eax
	andl	$63, %ecx
	movslq	%edi, %rdi
	subl	%r10d, %ecx
	salq	%cl, %rax
	orq	%rax, (%rsi,%rdi,8)
	jmp	.L929
.L1125:
	movq	%rcx, (%rsi)
	movl	4(%r12), %edi
	movl	$8, %ecx
	cmpl	$1, %edi
	jle	.L1087
.L922:
	movq	8(%r12), %rdx
	addl	$1, %eax
	movq	$0, (%rdx,%rcx)
	movl	4(%r12), %edi
	addq	$8, %rcx
	cmpl	%eax, %edi
	jg	.L922
.L1087:
	movq	8(%r12), %rsi
	jmp	.L921
.L945:
	movq	-456(%rbp), %rdi
	movq	%r13, %rsi
	movb	%r10b, -469(%rbp)
	call	_ZNSt5dequeIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movzbl	-469(%rbp), %r10d
	movl	$1, %r9d
	jmp	.L944
.L1124:
	movq	$0, 8(%r12)
	jmp	.L912
.L1120:
	movq	-440(%rbp), %rdi
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	$1, %r9d
	movq	%rax, %r12
	jmp	.L911
.L941:
	movl	160(%rbx), %eax
	jmp	.L946
.L930:
	movq	-456(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNSt5dequeIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	movl	$1, %r9d
	jmp	.L931
.L900:
	movq	-152(%rbp), %rax
	testq	%rax, %rax
	je	.L902
	cmpq	$64, 8(%rax)
	ja	.L903
.L902:
	movq	-120(%rbp), %rax
	movq	$64, 8(%rax)
	movq	-152(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, -152(%rbp)
.L903:
	movq	-104(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -104(%rbp)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rax, -128(%rbp)
	jmp	.L901
.L1123:
	movq	-464(%rbp), %rsi
	movq	-440(%rbp), %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	$1, %r9d
	jmp	.L914
.L1112:
	movq	$0, 8(%r14)
	jmp	.L898
.L1111:
	movq	-440(%rbp), %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L896
.L916:
	movl	4(%r15), %eax
	movq	8(%r15), %rcx
	jmp	.L918
.L1114:
	movl	4(%r10), %edx
	movl	4(%rbx), %esi
	leaq	.LC104(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L977:
	addq	$8, %rbx
	jmp	.L969
.L973:
	leaq	.LC106(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L967:
	movq	80(%r12), %rbx
.L983:
	cmpq	%rbx, 88(%r12)
	je	.L978
	movq	(%rbx), %r13
	movq	56(%r13), %r14
	testq	%r14, %r14
	je	.L979
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	cmpq	%rax, %r13
	jne	.L1126
	movq	80(%r13), %rcx
	subq	72(%r13), %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	sarq	$3, %rcx
	movq	%r12, %rdi
	subl	$1, %ecx
	call	_ZN2v88internal8compilerL19CheckInputsDominateEPNS1_8ScheduleEPNS1_10BasicBlockEPNS1_4NodeEi
.L979:
	xorl	%r14d, %r14d
	jmp	.L982
.L1127:
	movq	(%rdx,%r14,8), %rdx
	leal	-1(%r14), %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	addq	$1, %r14
	call	_ZN2v88internal8compilerL19CheckInputsDominateEPNS1_8ScheduleEPNS1_10BasicBlockEPNS1_4NodeEi
.L982:
	movq	72(%r13), %rdx
	movq	80(%r13), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r14
	jb	.L1127
	addq	$8, %rbx
	jmp	.L983
.L978:
	movq	-440(%rbp), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1128
	addq	$440, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1128:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L975:
	leaq	.LC107(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1126:
	leaq	.LC108(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L894:
	movl	$1, 4(%r14)
	movq	$0, 8(%r14)
	movl	$1, -448(%rbp)
	jmp	.L898
.L890:
	movq	-456(%rbp), %rdi
	leaq	-424(%rbp), %rsi
	call	_ZNSt5dequeIPN2v88internal8compiler10BasicBlockENS1_22RecyclingZoneAllocatorIS4_EEE16_M_push_back_auxIJRKS4_EEEvDpOT_
	jmp	.L891
.L1110:
	movq	-440(%rbp), %rdi
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L893
	.cfi_endproc
.LFE10903:
	.size	_ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE, .-_ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE:
.LFB12994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12994:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE, .-_GLOBAL__sub_I__ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler8Verifier7Visitor5CheckEPNS1_4NodeERKNS1_8AllNodesE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC73:
	.long	4294967295
	.long	1128267775
	.align 8
.LC80:
	.long	4160749568
	.long	1101004799
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
