	.file	"pipeline.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1527:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1527:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4467:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4467:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4468:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4468:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4470:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4470:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,"axG",@progbits,_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.type	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, @function
_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv:
.LFB7014:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7014:
	.size	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, .-_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.section	.text._ZN2v88internal14CompilationJobD2Ev,"axG",@progbits,_ZN2v88internal14CompilationJobD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CompilationJobD2Ev
	.type	_ZN2v88internal14CompilationJobD2Ev, @function
_ZN2v88internal14CompilationJobD2Ev:
.LFB19212:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE19212:
	.size	_ZN2v88internal14CompilationJobD2Ev, .-_ZN2v88internal14CompilationJobD2Ev
	.weak	_ZN2v88internal14CompilationJobD1Ev
	.set	_ZN2v88internal14CompilationJobD1Ev,_ZN2v88internal14CompilationJobD2Ev
	.section	.rodata._ZNK2v88internal8compiler9JSInliner12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"JSInliner"
	.section	.text._ZNK2v88internal8compiler9JSInliner12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler9JSInliner12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler9JSInliner12reducer_nameEv
	.type	_ZNK2v88internal8compiler9JSInliner12reducer_nameEv, @function
_ZNK2v88internal8compiler9JSInliner12reducer_nameEv:
.LFB25186:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE25186:
	.size	_ZNK2v88internal8compiler9JSInliner12reducer_nameEv, .-_ZNK2v88internal8compiler9JSInliner12reducer_nameEv
	.section	.text._ZNK2v88internal29NativesExternalStringResource4dataEv,"axG",@progbits,_ZNK2v88internal29NativesExternalStringResource4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal29NativesExternalStringResource4dataEv
	.type	_ZNK2v88internal29NativesExternalStringResource4dataEv, @function
_ZNK2v88internal29NativesExternalStringResource4dataEv:
.LFB25907:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE25907:
	.size	_ZNK2v88internal29NativesExternalStringResource4dataEv, .-_ZNK2v88internal29NativesExternalStringResource4dataEv
	.section	.text._ZNK2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper12reducer_nameEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper12reducer_nameEv, @function
_ZNK2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper12reducer_nameEv:
.LFB26432:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*16(%rax)
	.cfi_endproc
.LFE26432:
	.size	_ZNK2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper12reducer_nameEv, .-_ZNK2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper12reducer_nameEv
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper8FinalizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper8FinalizeEv, @function
_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper8FinalizeEv:
.LFB26434:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.cfi_endproc
.LFE26434:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper8FinalizeEv, .-_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper8FinalizeEv
	.section	.text._ZNK2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper12reducer_nameEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper12reducer_nameEv, @function
_ZNK2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper12reducer_nameEv:
.LFB26438:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*16(%rax)
	.cfi_endproc
.LFE26438:
	.size	_ZNK2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper12reducer_nameEv, .-_ZNK2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper12reducer_nameEv
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper6ReduceEPNS1_4NodeE:
.LFB26439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	je	.L14
	movq	24(%rbx), %rdx
	movq	48(%rbx), %rsi
	movl	32(%rbx), %r14d
	movq	40(%rbx), %r15
	movq	%rdx, -64(%rbp)
	movl	20(%r13), %edx
	movq	16(%rbx), %rcx
	movq	%rax, 24(%rbx)
	andl	$16777215, %edx
	movq	%rsi, 16(%rbx)
	movq	%r13, %rsi
	movl	$1, 32(%rbx)
	movq	%rdx, 40(%rbx)
	movq	8(%r12), %rdi
	movq	%rcx, -56(%rbp)
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-56(%rbp), %xmm0
	movl	%r14d, 32(%rbx)
	movq	%r15, 40(%rbx)
	movhps	-64(%rbp), %xmm0
	movups	%xmm0, 16(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	8(%r12), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE26439:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper6ReduceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper8FinalizeEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper8FinalizeEv, @function
_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper8FinalizeEv:
.LFB26440:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.cfi_endproc
.LFE26440:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper8FinalizeEv, .-_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper8FinalizeEv
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperD2Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperD2Ev:
.LFB36232:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE36232:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperD2Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperD2Ev
	.set	_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperD1Ev,_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperD2Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperD2Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperD2Ev:
.LFB36236:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE36236:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperD2Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperD2Ev
	.set	_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperD1Ev,_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperD2Ev
	.section	.text._ZN2v88internal8compiler9JSInlinerD2Ev,"axG",@progbits,_ZN2v88internal8compiler9JSInlinerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9JSInlinerD2Ev
	.type	_ZN2v88internal8compiler9JSInlinerD2Ev, @function
_ZN2v88internal8compiler9JSInlinerD2Ev:
.LFB36244:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE36244:
	.size	_ZN2v88internal8compiler9JSInlinerD2Ev, .-_ZN2v88internal8compiler9JSInlinerD2Ev
	.weak	_ZN2v88internal8compiler9JSInlinerD1Ev
	.set	_ZN2v88internal8compiler9JSInlinerD1Ev,_ZN2v88internal8compiler9JSInlinerD2Ev
	.section	.text._ZN2v88internal14CompilationJobD0Ev,"axG",@progbits,_ZN2v88internal14CompilationJobD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CompilationJobD0Ev
	.type	_ZN2v88internal14CompilationJobD0Ev, @function
_ZN2v88internal14CompilationJobD0Ev:
.LFB19214:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE19214:
	.size	_ZN2v88internal14CompilationJobD0Ev, .-_ZN2v88internal14CompilationJobD0Ev
	.section	.text._ZN2v88internal21RegisterConfigurationD0Ev,"axG",@progbits,_ZN2v88internal21RegisterConfigurationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21RegisterConfigurationD0Ev
	.type	_ZN2v88internal21RegisterConfigurationD0Ev, @function
_ZN2v88internal21RegisterConfigurationD0Ev:
.LFB32214:
	.cfi_startproc
	endbr64
	movl	$336, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE32214:
	.size	_ZN2v88internal21RegisterConfigurationD0Ev, .-_ZN2v88internal21RegisterConfigurationD0Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperD0Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperD0Ev:
.LFB36238:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE36238:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperD0Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperD0Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperD0Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperD0Ev:
.LFB36234:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE36234:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperD0Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperD0Ev
	.section	.text._ZN2v88internal8compiler9JSInlinerD0Ev,"axG",@progbits,_ZN2v88internal8compiler9JSInlinerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9JSInlinerD0Ev
	.type	_ZN2v88internal8compiler9JSInlinerD0Ev, @function
_ZN2v88internal8compiler9JSInlinerD0Ev:
.LFB36246:
	.cfi_startproc
	endbr64
	movl	$56, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE36246:
	.size	_ZN2v88internal8compiler9JSInlinerD0Ev, .-_ZN2v88internal8compiler9JSInlinerD0Ev
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_116PipelineRunScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_116PipelineRunScopeD2Ev, @function
_ZN2v88internal8compiler12_GLOBAL__N_116PipelineRunScopeD2Ev:
.LFB26825:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L27
	movq	40(%rdi), %rdx
	movq	%rdx, 48(%rax)
.L27:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L28
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L28:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L26
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26825:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_116PipelineRunScopeD2Ev, .-_ZN2v88internal8compiler12_GLOBAL__N_116PipelineRunScopeD2Ev
	.set	_ZN2v88internal8compiler12_GLOBAL__N_116PipelineRunScopeD1Ev,_ZN2v88internal8compiler12_GLOBAL__N_116PipelineRunScopeD2Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB37029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE37029:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD1Ev
	.type	_ZN2v88internal8OFStreamD1Ev, @function
_ZN2v88internal8OFStreamD1Ev:
.LFB20004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE20004:
	.size	_ZN2v88internal8OFStreamD1Ev, .-_ZN2v88internal8OFStreamD1Ev
	.section	.rodata._ZN2v88internal8compiler26WasmHeapStubCompilationJob14PrepareJobImplEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler26WasmHeapStubCompilationJob14PrepareJobImplEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler26WasmHeapStubCompilationJob14PrepareJobImplEPNS0_7IsolateE
	.type	_ZN2v88internal8compiler26WasmHeapStubCompilationJob14PrepareJobImplEPNS0_7IsolateE, @function
_ZN2v88internal8compiler26WasmHeapStubCompilationJob14PrepareJobImplEPNS0_7IsolateE:
.LFB26484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE26484:
	.size	_ZN2v88internal8compiler26WasmHeapStubCompilationJob14PrepareJobImplEPNS0_7IsolateE, .-_ZN2v88internal8compiler26WasmHeapStubCompilationJob14PrepareJobImplEPNS0_7IsolateE
	.section	.text._ZN2v88internal8compiler9JSInliner6ReduceEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler9JSInliner6ReduceEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler9JSInliner6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler9JSInliner6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler9JSInliner6ReduceEPNS1_4NodeE:
.LFB25187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25187:
	.size	_ZN2v88internal8compiler9JSInliner6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler9JSInliner6ReduceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE, @function
_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE:
.LFB26441:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rax
	movl	(%rax), %edx
	testb	$32, %dl
	je	.L46
	movq	152(%rdi), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L59
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L48:
	movq	168(%rbx), %rdx
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperE(%rip), %rcx
	movq	%r12, 8(%rax)
	movq	%rax, %r12
	movq	%rcx, (%rax)
	movq	%rdx, 16(%rax)
	movq	24(%rbx), %rdx
	movl	(%rdx), %edx
.L46:
	andb	$64, %dh
	je	.L49
	movq	152(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L60
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L51:
	movq	176(%rbx), %rdx
	leaq	16+_ZTVN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperE(%rip), %rcx
	movq	%r12, 8(%rax)
	movq	%rax, %r12
	movq	%rcx, (%rax)
	movq	%rdx, 16(%rax)
.L49:
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12GraphReducer10AddReducerEPNS1_7ReducerE@PLT
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L48
	.cfi_endproc
.LFE26441:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE, .-_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper6ReduceEPNS1_4NodeE:
.LFB26433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rdi
	call	_ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE@PLT
	movq	16(%rbx), %r13
	movq	16(%r13), %rdx
	movq	%rdx, -40(%rbp)
	testb	$1, %al
	jne	.L62
	movq	%rax, %rcx
	movq	%rax, %rdx
	shrq	%rcx
	shrq	$31, %rdx
	andl	$1073741823, %ecx
	movzwl	%dx, %edx
	orl	%edx, %ecx
	je	.L63
.L62:
	movq	%rax, 16(%r13)
.L63:
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-40(%rbp), %rdx
	movq	%rdx, 16(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26433:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper6ReduceEPNS1_4NodeE
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB38199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE38199:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB37030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE37030:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB38201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE38201:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internal8OFStreamD0Ev,"axG",@progbits,_ZN2v88internal8OFStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD0Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD0Ev:
.LFB38200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE38200:
	.size	_ZTv0_n24_N2v88internal8OFStreamD0Ev, .-_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD0Ev
	.type	_ZN2v88internal8OFStreamD0Ev, @function
_ZN2v88internal8OFStreamD0Ev:
.LFB20005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE20005:
	.size	_ZN2v88internal8OFStreamD0Ev, .-_ZN2v88internal8OFStreamD0Ev
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD1Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD1Ev:
.LFB38202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE38202:
	.size	_ZTv0_n24_N2v88internal8OFStreamD1Ev, .-_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl3RunINS1_29HeapBrokerInitializationPhaseEJEEEvDpOT0_.isra.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"V8.TFHeapBrokerInitialization"
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl3RunINS1_29HeapBrokerInitializationPhaseEJEEEvDpOT0_.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../deps/v8/src/compiler/pipeline.cc:803"
	.section	.text._ZN2v88internal8compiler12PipelineImpl3RunINS1_29HeapBrokerInitializationPhaseEJEEEvDpOT0_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12PipelineImpl3RunINS1_29HeapBrokerInitializationPhaseEJEEEvDpOT0_.isra.0, @function
_ZN2v88internal8compiler12PipelineImpl3RunINS1_29HeapBrokerInitializationPhaseEJEEEvDpOT0_.isra.0:
.LFB37592:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	56(%rbx), %r14
	testq	%r14, %r14
	je	.L81
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L81:
	movq	48(%rbx), %r15
	movq	176(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L82
	movq	48(%rbx), %rax
	movq	%rax, -64(%rbp)
	leaq	.LC2(%rip), %rax
	movq	%rax, 48(%rbx)
.L82:
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%rax, %r13
	movq	(%r12), %rax
	movq	(%rax), %rdx
	movq	24(%rax), %rdi
	movq	320(%rax), %r12
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal24OptimizedCompilationInfo14native_contextEv@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r8
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L83
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L84:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12JSHeapBroker29InitializeAndStartSerializingENS0_6HandleINS0_13NativeContextEEE@PLT
	testq	%rbx, %rbx
	je	.L86
	movq	-64(%rbp), %rax
	movq	%rax, 48(%rbx)
.L86:
	testq	%r13, %r13
	je	.L87
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L87:
	testq	%r14, %r14
	je	.L80
	addq	$40, %rsp
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movq	41088(%rdx), %rsi
	cmpq	41096(%rdx), %rsi
	je	.L102
.L85:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rdx)
	movq	%r8, (%rsi)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L80:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r8
	movq	-56(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L85
	.cfi_endproc
.LFE37592:
	.size	_ZN2v88internal8compiler12PipelineImpl3RunINS1_29HeapBrokerInitializationPhaseEJEEEvDpOT0_.isra.0, .-_ZN2v88internal8compiler12PipelineImpl3RunINS1_29HeapBrokerInitializationPhaseEJEEEvDpOT0_.isra.0
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl3RunINS1_16VerifyGraphPhaseEJRbEEEvDpOT0_.isra.0.str1.1,"aMS",@progbits,1
.LC4:
	.string	"unnamed"
	.section	.text._ZN2v88internal8compiler12PipelineImpl3RunINS1_16VerifyGraphPhaseEJRbEEEvDpOT0_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12PipelineImpl3RunINS1_16VerifyGraphPhaseEJRbEEEvDpOT0_.isra.0, @function
_ZN2v88internal8compiler12PipelineImpl3RunINS1_16VerifyGraphPhaseEJRbEEEvDpOT0_.isra.0:
.LFB37565:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r15
	movq	56(%r15), %r12
	testq	%r12, %r12
	je	.L104
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L104:
	movq	176(%r15), %r9
	movq	48(%r15), %r14
	testq	%r9, %r9
	je	.L105
	movq	48(%r9), %rax
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	movq	%r9, -64(%rbp)
	movq	%rax, -56(%rbp)
	leaq	.LC4(%rip), %rax
	movq	%rax, 48(%r9)
	movzbl	0(%r13), %r13d
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%rbx), %rdx
	movq	-64(%rbp), %r9
	movq	%rax, %r15
	movq	24(%rdx), %rax
	movl	8(%rax), %eax
	cmpl	$8, %eax
	ja	.L106
	cmpl	$4, %eax
	ja	.L107
.L108:
	movq	160(%rdx), %rdi
	movzbl	%r13b, %esi
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r9, -64(%rbp)
	call	_ZN2v88internal8compiler8Verifier3RunEPNS1_5GraphENS2_6TypingENS2_11CheckInputsENS2_8CodeTypeE@PLT
	movq	-64(%rbp), %r9
.L111:
	movq	-56(%rbp), %rax
	movq	%rax, 48(%r9)
.L112:
	testq	%r15, %r15
	je	.L109
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L109:
	testq	%r12, %r12
	je	.L103
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	subl	$10, %eax
	cmpl	$1, %eax
	ja	.L108
.L107:
	movq	160(%rdx), %rdi
	movzbl	%r13b, %esi
	movl	$1, %ecx
	movq	%r9, -64(%rbp)
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier3RunEPNS1_5GraphENS2_6TypingENS2_11CheckInputsENS2_8CodeTypeE@PLT
	movq	-64(%rbp), %r9
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L103:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	movzbl	0(%r13), %r13d
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%rbx), %rdx
	movq	%rax, %r15
	movzbl	%r13b, %esi
	movq	24(%rdx), %rax
	movl	8(%rax), %eax
	cmpl	$8, %eax
	ja	.L113
	cmpl	$4, %eax
	ja	.L114
.L115:
	movq	160(%rdx), %rdi
	xorl	%ecx, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier3RunEPNS1_5GraphENS2_6TypingENS2_11CheckInputsENS2_8CodeTypeE@PLT
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L113:
	subl	$10, %eax
	cmpl	$1, %eax
	ja	.L115
.L114:
	movq	160(%rdx), %rdi
	movl	$1, %ecx
	movl	$1, %edx
	call	_ZN2v88internal8compiler8Verifier3RunEPNS1_5GraphENS2_6TypingENS2_11CheckInputsENS2_8CodeTypeE@PLT
	jmp	.L112
	.cfi_endproc
.LFE37565:
	.size	_ZN2v88internal8compiler12PipelineImpl3RunINS1_16VerifyGraphPhaseEJRbEEEvDpOT0_.isra.0, .-_ZN2v88internal8compiler12PipelineImpl3RunINS1_16VerifyGraphPhaseEJRbEEEvDpOT0_.isra.0
	.set	_ZN2v88internal8compiler12PipelineImpl3RunINS1_16VerifyGraphPhaseEJbEEEvDpOT0_.isra.0,_ZN2v88internal8compiler12PipelineImpl3RunINS1_16VerifyGraphPhaseEJRbEEEvDpOT0_.isra.0
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_119PrintFunctionSourceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEiNS0_6HandleINS0_18SharedFunctionInfoEEE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"ab"
.LC6:
	.string	"--- FUNCTION SOURCE ("
.LC7:
	.string	":"
.LC8:
	.string	") id{"
.LC9:
	.string	","
.LC10:
	.string	"} start{"
.LC11:
	.string	"} ---\n"
.LC12:
	.string	"\n--- END ---\n"
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_119PrintFunctionSourceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEiNS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_119PrintFunctionSourceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEiNS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal8compiler12_GLOBAL__N_119PrintFunctionSourceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEiNS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB26421:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$472, %rsp
	movl	%edx, -496(%rbp)
	movq	(%rcx), %rdx
	movq	%rdi, -488(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	31(%rdx), %rax
	testb	$1, %al
	jne	.L163
.L124:
	cmpq	%rax, 88(%r12)
	jne	.L164
.L123:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	addq	$472, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movq	31(%rdx), %rsi
	testb	$1, %sil
	jne	.L166
.L126:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L127
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L128:
	movq	7(%rsi), %rax
	cmpq	%rax, 88(%r12)
	je	.L123
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	%rax, -504(%rbp)
	movq	144(%rax), %rsi
	je	.L131
	testq	%rsi, %rsi
	je	.L167
.L132:
	movq	-504(%rbp), %rax
	addl	$1, 152(%rax)
.L131:
	movq	(%r14), %rax
	leaq	-400(%rbp), %r12
	movq	%r12, %rdi
	movq	15(%rax), %r13
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movl	$21, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testb	$1, %r13b
	jne	.L133
.L162:
	leaq	-432(%rbp), %r15
	leaq	-464(%rbp), %r9
.L134:
	movq	(%rbx), %rax
	leaq	-472(%rbp), %r13
	movq	%r9, -512(%rbp)
	movq	%r13, %rdi
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	-512(%rbp), %r9
	movl	$1, %edx
	movq	%rax, -464(%rbp)
	movq	%r9, %rsi
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-432(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L168
	movq	%rsi, %rdi
	movq	%rsi, -512(%rbp)
	call	strlen@PLT
	movq	-512(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L138:
	movq	%r12, %rdi
	movl	$5, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L139
	call	_ZdaPv@PLT
.L139:
	movq	-488(%rbp), %rax
	movq	%r12, %rdi
	movl	120(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -488(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-496(%rbp), %esi
	movq	-488(%rbp), %rdi
	call	_ZNSolsEi@PLT
	movl	$8, %edx
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	%rax, -432(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNSolsEi@PLT
	movl	$6, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	%rax, -432(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%r15, %rdi
	movl	%eax, -496(%rbp)
	movq	(%rbx), %rax
	movq	%rax, -432(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	movl	%eax, %ebx
	movq	(%r14), %rax
	movq	7(%rax), %rcx
	movl	%ebx, %eax
	subl	-496(%rbp), %eax
	cmpl	$-1, %eax
	jne	.L140
	movl	-496(%rbp), %ebx
	addl	11(%rcx), %ebx
.L140:
	leaq	-473(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rcx, -432(%rbp)
	movq	%rcx, -512(%rbp)
	movq	%rsi, -488(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	-512(%rbp), %rcx
	movq	-488(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rdx, %r9
	movq	%rax, %r14
	movq	%rax, -464(%rbp)
	movq	%r9, -456(%rbp)
	movq	%rcx, -472(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	cmpq	%rax, %r14
	movq	%rax, -432(%rbp)
	setne	-488(%rbp)
	movzbl	-488(%rbp), %eax
	movq	%rdx, -424(%rbp)
	cmpl	%ebx, -496(%rbp)
	jne	.L141
	testb	%al, %al
	je	.L148
.L141:
	movl	-496(%rbp), %edx
	addl	$1, %edx
	cmpl	$1, -452(%rbp)
	je	.L146
	movslq	%edx, %r15
	.p2align 4,,10
	.p2align 3
.L147:
	movzwl	-2(%r14,%r15,2), %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movw	%ax, -472(%rbp)
	call	_ZN2v88internallsERSoRKNS0_23AsReversiblyEscapedUC16E@PLT
	cmpl	%r15d, %ebx
	setne	%al
	addq	$1, %r15
	orb	-488(%rbp), %al
	jne	.L147
.L148:
	movl	$13, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rbx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rbx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-320(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L123
	movq	-504(%rbp), %rbx
	movl	152(%rbx), %eax
	movl	%eax, -488(%rbp)
	subl	$1, %eax
	movl	%eax, 152(%rbx)
	jne	.L123
	movq	144(%rbx), %rdi
	call	fclose@PLT
	movq	$0, 144(%rbx)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L163:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L124
	movq	23(%rax), %rax
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L127:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L169
.L129:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L133:
	movq	-1(%r13), %rax
	cmpw	$63, 11(%rax)
	ja	.L162
	leaq	-464(%rbp), %r9
	leaq	-432(%rbp), %r15
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r9, %rsi
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%r9, -512(%rbp)
	movq	%r13, -464(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	-432(%rbp), %rdi
	movq	-512(%rbp), %r9
	testq	%rdi, %rdi
	je	.L134
	call	_ZdaPv@PLT
	movq	-512(%rbp), %r9
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L166:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L126
	movq	23(%rsi), %rsi
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L168:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%r12, %rdi
	movq	%rsi, -504(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-504(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L146:
	movslq	%edx, %rdx
	.p2align 4,,10
	.p2align 3
.L149:
	movzbl	-1(%r14,%rdx), %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rdx, -496(%rbp)
	movw	%ax, -472(%rbp)
	call	_ZN2v88internallsERSoRKNS0_23AsReversiblyEscapedUC16E@PLT
	movq	-496(%rbp), %rdx
	cmpl	%edx, %ebx
	setne	%al
	addq	$1, %rdx
	orb	-488(%rbp), %al
	jne	.L149
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L167:
	movq	(%rax), %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%r15)
	movq	%rax, %rsi
	jmp	.L132
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26421:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_119PrintFunctionSourceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEiNS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal8compiler12_GLOBAL__N_119PrintFunctionSourceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEiNS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZNK2v88internal18SharedFunctionInfo6scriptEv,"axG",@progbits,_ZNK2v88internal18SharedFunctionInfo6scriptEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal18SharedFunctionInfo6scriptEv
	.type	_ZNK2v88internal18SharedFunctionInfo6scriptEv, @function
_ZNK2v88internal18SharedFunctionInfo6scriptEv:
.LFB16837:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L172
.L171:
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L171
	movq	23(%rax), %rax
	ret
	.cfi_endproc
.LFE16837:
	.size	_ZNK2v88internal18SharedFunctionInfo6scriptEv, .-_ZNK2v88internal18SharedFunctionInfo6scriptEv
	.section	.rodata._ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC13:
	.string	"IsJSReceiver()"
.LC14:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler13JSReceiverRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB19783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler9ObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	testb	%bl, %bl
	jne	.L176
.L173:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSReceiverEv@PLT
	testb	%al, %al
	jne	.L173
	leaq	.LC13(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19783:
	.size	_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler13JSReceiverRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler13JSReceiverRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC15:
	.string	"IsJSObject()"
	.section	.text._ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler11JSObjectRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB19789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler13JSReceiverRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	testb	%bl, %bl
	jne	.L180
.L177:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef10IsJSObjectEv@PLT
	testb	%al, %al
	jne	.L177
	leaq	.LC15(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19789:
	.size	_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler11JSObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.rodata._ZN2v88internal8compiler13JSFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb.str1.1,"aMS",@progbits,1
.LC16:
	.string	"IsJSFunction()"
	.section	.text._ZN2v88internal8compiler13JSFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,"axG",@progbits,_ZN2v88internal8compiler13JSFunctionRefC5EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler13JSFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal8compiler13JSFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal8compiler13JSFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb:
.LFB19807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler11JSObjectRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	testb	%bl, %bl
	jne	.L184
.L181:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler9ObjectRef12IsJSFunctionEv@PLT
	testb	%al, %al
	jne	.L181
	leaq	.LC16(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19807:
	.size	_ZN2v88internal8compiler13JSFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal8compiler13JSFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.weak	_ZN2v88internal8compiler13JSFunctionRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.set	_ZN2v88internal8compiler13JSFunctionRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb,_ZN2v88internal8compiler13JSFunctionRefC2EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB26278:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L191
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L194
.L185:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L185
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE26278:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_7IsolateEPNS0_24OptimizedCompilationInfoEPNS1_18PipelineStatisticsE.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"../deps/v8/src/compiler/pipeline.cc:116"
	.align 8
.LC18:
	.string	"../deps/v8/src/compiler/pipeline.cc:118"
	.align 8
.LC19:
	.string	"../deps/v8/src/compiler/pipeline.cc:120"
	.align 8
.LC20:
	.string	"../deps/v8/src/compiler/pipeline.cc:124"
	.section	.rodata._ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_7IsolateEPNS0_24OptimizedCompilationInfoEPNS1_18PipelineStatisticsE.str1.1,"aMS",@progbits,1
.LC21:
	.string	"V8.TFInitPipelineData"
	.section	.text._ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_7IsolateEPNS0_24OptimizedCompilationInfoEPNS1_18PipelineStatisticsE,"axG",@progbits,_ZN2v88internal8compiler12PipelineDataC5EPNS1_9ZoneStatsEPNS0_7IsolateEPNS0_24OptimizedCompilationInfoEPNS1_18PipelineStatisticsE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_7IsolateEPNS0_24OptimizedCompilationInfoEPNS1_18PipelineStatisticsE
	.type	_ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_7IsolateEPNS0_24OptimizedCompilationInfoEPNS1_18PipelineStatisticsE, @function
_ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_7IsolateEPNS0_24OptimizedCompilationInfoEPNS1_18PipelineStatisticsE:
.LFB26309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%r8, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movq	%rcx, %rsi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41136(%rdx), %rax
	movq	%rdx, -32(%rdi)
	movq	$0, -24(%rdi)
	movq	%rax, -16(%rdi)
	movq	%rcx, -8(%rdi)
	movaps	%xmm0, -96(%rbp)
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movdqa	-96(%rbp), %xmm0
	xorl	%eax, %eax
	movb	$0, 72(%rbx)
	movw	%ax, 64(%rbx)
	xorl	%eax, %eax
	movups	%xmm0, 48(%rbx)
	pxor	%xmm0, %xmm0
	movb	$0, 80(%rbx)
	movl	$0, 120(%rbx)
	movups	%xmm0, 96(%rbx)
	cmpb	$0, 41456(%r12)
	movb	$0, 40(%rbx)
	movl	$-1, 68(%rbx)
	movq	$0, 112(%rbx)
	jne	.L196
	cmpq	$0, 45520(%r12)
	sete	%al
.L196:
	leaq	.LC17(%rip), %rsi
	movb	%al, 124(%rbx)
	movq	%r15, %rdi
	movq	%rsi, 128(%rbx)
	movq	%r15, 136(%rbx)
	movq	$0, 144(%rbx)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	48(%rbx), %rdi
	leaq	.LC18(%rip), %rsi
	movq	$0, 256(%rbx)
	movq	%rax, %xmm0
	movq	%rsi, 240(%rbx)
	movq	%rdi, 248(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 160(%rbx)
	movups	%xmm0, 176(%rbx)
	movups	%xmm0, 192(%rbx)
	movups	%xmm0, 208(%rbx)
	movups	%xmm0, 224(%rbx)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	48(%rbx), %rdi
	leaq	.LC19(%rip), %rsi
	movq	$0, 272(%rbx)
	movq	%rsi, 280(%rbx)
	movq	%rax, %xmm1
	movq	%rdi, 288(%rbx)
	punpcklqdq	%xmm1, %xmm1
	movq	$0, 296(%rbx)
	movups	%xmm1, 256(%rbx)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movl	$712, %edi
	movq	$0, 312(%rbx)
	movq	%rax, %xmm1
	movq	24(%rbx), %rax
	punpcklqdq	%xmm1, %xmm1
	movups	%xmm1, 296(%rbx)
	movl	(%rax), %r15d
	movq	64(%rax), %rdx
	shrl	$18, %r15d
	movq	%rdx, -96(%rbp)
	call	_Znwm@PLT
	andl	$1, %r15d
	movq	-96(%rbp), %rdx
	movq	(%rbx), %rsi
	movl	%r15d, %ecx
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler12JSHeapBrokerC1EPNS0_7IsolateEPNS0_4ZoneEb@PLT
	movq	48(%rbx), %rdi
	movq	-96(%rbp), %rax
	leaq	.LC20(%rip), %rsi
	movq	%rsi, 336(%rbx)
	movq	%rdi, 344(%rbx)
	movq	%rax, 320(%rbx)
	movq	$0, 328(%rbx)
	movq	$0, 352(%rbx)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rax, %xmm1
	leaq	400(%rbx), %rax
	movq	$0, 392(%rbx)
	leaq	424(%rbx), %rdi
	movq	%rax, 384(%rbx)
	punpcklqdq	%xmm1, %xmm1
	movb	$0, 400(%rbx)
	movq	$0, 416(%rbx)
	movups	%xmm1, 352(%rbx)
	movups	%xmm0, 368(%rbx)
	call	_ZN2v88internal16AssemblerOptions7DefaultEPNS0_7IsolateEb@PLT
	movq	$0, 448(%rbx)
	testq	%r13, %r13
	je	.L197
	leaq	.LC21(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L197:
	movq	152(%rbx), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$63, %rax
	jbe	.L229
	leaq	64(%r12), %rax
	movq	%rax, 16(%rdi)
.L199:
	movq	152(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5GraphC1EPNS0_4ZoneE@PLT
	movq	152(%rbx), %rdi
	movq	%r12, 160(%rbx)
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$55, %rax
	jbe	.L230
	leaq	56(%r12), %rax
	movq	%rax, 16(%rdi)
.L201:
	movq	160(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19SourcePositionTableC1EPNS1_5GraphE@PLT
	movl	(%r14), %eax
	movq	%r12, 168(%rbx)
	xorl	%r12d, %r12d
	testb	$64, %ah
	jne	.L231
.L202:
	movq	152(%rbx), %rdi
	movq	%r12, 176(%rbx)
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L232
	leaq	16(%r12), %rax
	movq	%rax, 16(%rdi)
.L206:
	movq	152(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilderC1EPNS0_4ZoneE@PLT
	movq	%r12, 184(%rbx)
	call	_ZN2v88internal8compiler19InstructionSelector29SupportedMachineOperatorFlagsEv@PLT
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector21AlignmentRequirementsEv@PLT
	movq	152(%rbx), %rdi
	movq	%rax, -68(%rbp)
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	movl	%edx, -60(%rbp)
	subq	%r12, %rax
	cmpq	$39, %rax
	jbe	.L233
	leaq	40(%r12), %rax
	movq	%rax, 16(%rdi)
.L208:
	movq	-68(%rbp), %r8
	movl	-60(%rbp), %r9d
	movq	%r12, %rdi
	movl	%r14d, %ecx
	movq	152(%rbx), %rsi
	movl	$5, %edx
	call	_ZN2v88internal8compiler22MachineOperatorBuilderC1EPNS0_4ZoneENS0_21MachineRepresentationENS_4base5FlagsINS2_4FlagEjEENS2_21AlignmentRequirementsE@PLT
	movq	152(%rbx), %rdi
	movq	%r12, 192(%rbx)
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L234
	leaq	16(%r12), %rax
	movq	%rax, 16(%rdi)
.L210:
	movq	152(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilderC1EPNS0_4ZoneE@PLT
	movq	152(%rbx), %rdi
	movq	%r12, 200(%rbx)
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L235
	leaq	16(%r12), %rax
	movq	%rax, 16(%rdi)
.L212:
	movq	152(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilderC1EPNS0_4ZoneE@PLT
	movq	152(%rbx), %rdi
	movq	%r12, 208(%rbx)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$631, %rdx
	jbe	.L236
	leaq	632(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L214:
	movq	160(%rbx), %rdx
	movq	184(%rbx), %rcx
	movq	208(%rbx), %rsi
	movq	(%rbx), %rdi
	movq	192(%rbx), %r8
	movq	200(%rbx), %r9
	movq	%rdx, (%rax)
	movq	(%rdx), %rdx
	movq	$0, 24(%rax)
	movq	%r9, 8(%rax)
	movq	%r8, 16(%rax)
	movq	$0, 32(%rax)
	movq	$256, 40(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	$256, 72(%rax)
	movq	$0, 88(%rax)
	movq	$0, 96(%rax)
	movq	$256, 104(%rax)
	movq	$0, 120(%rax)
	movq	$0, 128(%rax)
	movq	$256, 136(%rax)
	movq	$0, 152(%rax)
	movq	$0, 160(%rax)
	movq	$256, 168(%rax)
	movq	$0, 184(%rax)
	movq	$0, 192(%rax)
	movq	$256, 200(%rax)
	movq	$0, 216(%rax)
	movq	$0, 224(%rax)
	movq	$256, 232(%rax)
	movq	$0, 248(%rax)
	movq	$0, 256(%rax)
	movq	%rdx, 344(%rax)
	movq	$256, 264(%rax)
	movq	$0, 280(%rax)
	movq	$0, 288(%rax)
	movq	$256, 296(%rax)
	movq	$0, 312(%rax)
	movq	$0, 320(%rax)
	movq	$256, 328(%rax)
	movq	$0, 352(%rax)
	movq	%rdi, 360(%rax)
	movq	%rsi, 368(%rax)
	movq	%rcx, 376(%rax)
	movq	$0, 384(%rax)
	movq	$0, 392(%rax)
	movq	$0, 400(%rax)
	movq	$0, 408(%rax)
	movq	$0, 416(%rax)
	movq	$0, 424(%rax)
	movq	$0, 432(%rax)
	movq	$0, 440(%rax)
	movq	$0, 448(%rax)
	movq	$0, 456(%rax)
	movq	$0, 464(%rax)
	movq	$0, 472(%rax)
	movq	$0, 480(%rax)
	movq	$0, 488(%rax)
	movq	$0, 496(%rax)
	movq	$0, 504(%rax)
	movq	$0, 512(%rax)
	movq	$0, 520(%rax)
	movq	$0, 528(%rax)
	movq	$0, 536(%rax)
	movq	$0, 544(%rax)
	movq	$0, 552(%rax)
	movq	$0, 560(%rax)
	movq	$0, 568(%rax)
	movq	$0, 576(%rax)
	movq	$0, 584(%rax)
	movq	$0, 592(%rax)
	movq	$0, 600(%rax)
	movq	$0, 608(%rax)
	movq	$0, 616(%rax)
	movq	$0, 624(%rax)
	movq	%rax, 216(%rbx)
	movq	24(%rbx), %rax
	movq	64(%rax), %rdx
	movq	16(%rdx), %r12
	movq	24(%rdx), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	jbe	.L237
	leaq	32(%r12), %rax
	movq	%rax, 16(%rdx)
.L216:
	movq	320(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler23CompilationDependenciesC1EPNS1_12JSHeapBrokerEPNS0_4ZoneE@PLT
	movq	%r12, 312(%rbx)
	testq	%r13, %r13
	je	.L195
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L228
	addq	$56, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L228
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	movq	152(%rbx), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$87, %rax
	jbe	.L238
	leaq	88(%r12), %rax
	movq	%rax, 16(%rdi)
.L204:
	movq	160(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler15NodeOriginTableC1EPNS1_5GraphE@PLT
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L230:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L232:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L233:
	movl	$40, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L235:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L229:
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L236:
	movl	$632, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L237:
	movq	%rdx, %rdi
	movl	$32, %esi
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L238:
	movl	$88, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L204
.L228:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26309:
	.size	_ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_7IsolateEPNS0_24OptimizedCompilationInfoEPNS1_18PipelineStatisticsE, .-_ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_7IsolateEPNS0_24OptimizedCompilationInfoEPNS1_18PipelineStatisticsE
	.weak	_ZN2v88internal8compiler12PipelineDataC1EPNS1_9ZoneStatsEPNS0_7IsolateEPNS0_24OptimizedCompilationInfoEPNS1_18PipelineStatisticsE
	.set	_ZN2v88internal8compiler12PipelineDataC1EPNS1_9ZoneStatsEPNS0_7IsolateEPNS0_24OptimizedCompilationInfoEPNS1_18PipelineStatisticsE,_ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_7IsolateEPNS0_24OptimizedCompilationInfoEPNS1_18PipelineStatisticsE
	.section	.rodata._ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS0_19AccountingAllocatorEPNS1_5GraphEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableEPNS0_20JumpOptimizationInfoERKNS0_16AssemblerOptionsE.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"../deps/v8/src/compiler/pipeline.cc:188"
	.align 8
.LC23:
	.string	"../deps/v8/src/compiler/pipeline.cc:194"
	.align 8
.LC24:
	.string	"../deps/v8/src/compiler/pipeline.cc:196"
	.align 8
.LC25:
	.string	"../deps/v8/src/compiler/pipeline.cc:198"
	.section	.text._ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS0_19AccountingAllocatorEPNS1_5GraphEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableEPNS0_20JumpOptimizationInfoERKNS0_16AssemblerOptionsE,"axG",@progbits,_ZN2v88internal8compiler12PipelineDataC5EPNS1_9ZoneStatsEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS0_19AccountingAllocatorEPNS1_5GraphEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableEPNS0_20JumpOptimizationInfoERKNS0_16AssemblerOptionsE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS0_19AccountingAllocatorEPNS1_5GraphEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableEPNS0_20JumpOptimizationInfoERKNS0_16AssemblerOptionsE
	.type	_ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS0_19AccountingAllocatorEPNS1_5GraphEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableEPNS0_20JumpOptimizationInfoERKNS0_16AssemblerOptionsE, @function
_ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS0_19AccountingAllocatorEPNS1_5GraphEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableEPNS0_20JumpOptimizationInfoERKNS0_16AssemblerOptionsE:
.LFB26315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$72, %rsp
	movq	32(%rbp), %rdx
	movq	48(%rbp), %r13
	movhps	24(%rbp), %xmm1
	movq	16(%rbp), %r15
	movq	40(%rbp), %r14
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -32(%rdi)
	movq	%r8, -16(%rdi)
	movq	%rsi, -8(%rdi)
	movq	$0, -24(%rdi)
	movaps	%xmm1, -96(%rbp)
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	xorl	%eax, %eax
	movq	%r12, 48(%rbx)
	pxor	%xmm0, %xmm0
	leaq	.LC22(%rip), %rsi
	movb	$1, 40(%rbx)
	movq	%r12, %rdi
	movq	%rsi, 128(%rbx)
	movq	%r12, 136(%rbx)
	movq	$0, 56(%rbx)
	movw	%ax, 64(%rbx)
	movl	$-1, 68(%rbx)
	movb	$0, 72(%rbx)
	movb	$0, 80(%rbx)
	movq	$0, 112(%rbx)
	movl	$0, 120(%rbx)
	movb	$0, 124(%rbx)
	movq	$0, 144(%rbx)
	movups	%xmm0, 96(%rbx)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-104(%rbp), %rdx
	movq	48(%rbx), %rdi
	leaq	.LC23(%rip), %rsi
	movdqa	-96(%rbp), %xmm1
	movq	%rax, %xmm2
	movq	%rsi, 240(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rdx, 176(%rbx)
	punpcklqdq	%xmm2, %xmm2
	movq	%rdi, 248(%rbx)
	movq	$0, 184(%rbx)
	movq	$0, 224(%rbx)
	movq	%r15, 232(%rbx)
	movq	$0, 256(%rbx)
	movups	%xmm2, 144(%rbx)
	movups	%xmm0, 192(%rbx)
	movups	%xmm0, 208(%rbx)
	movups	%xmm1, 160(%rbx)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	48(%rbx), %rdi
	leaq	.LC24(%rip), %rsi
	movq	$0, 272(%rbx)
	movq	%rsi, 280(%rbx)
	movq	%rax, %xmm1
	movq	%rdi, 288(%rbx)
	punpcklqdq	%xmm1, %xmm1
	movq	$0, 296(%rbx)
	movups	%xmm1, 256(%rbx)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	48(%rbx), %rdi
	leaq	.LC25(%rip), %rsi
	movq	$0, 328(%rbx)
	movq	%rax, %xmm1
	movq	%rsi, 336(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rdi, 344(%rbx)
	punpcklqdq	%xmm1, %xmm1
	movq	$0, 352(%rbx)
	movups	%xmm1, 296(%rbx)
	movups	%xmm0, 312(%rbx)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	152(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movq	$0, 392(%rbx)
	movq	%rax, %xmm1
	leaq	400(%rbx), %rax
	movb	$0, 400(%rbx)
	movq	%rax, 384(%rbx)
	movq	16(%r13), %rax
	punpcklqdq	%xmm1, %xmm1
	movdqu	0(%r13), %xmm3
	movq	%r14, 416(%rbx)
	movq	%rax, 440(%rbx)
	movq	$0, 448(%rbx)
	movups	%xmm1, 352(%rbx)
	movups	%xmm0, 368(%rbx)
	movups	%xmm3, 424(%rbx)
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L252
	leaq	16(%r12), %rax
	movq	%rax, 16(%rdi)
.L241:
	movq	152(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilderC1EPNS0_4ZoneE@PLT
	movq	%r12, 184(%rbx)
	call	_ZN2v88internal8compiler19InstructionSelector29SupportedMachineOperatorFlagsEv@PLT
	movl	%eax, %r13d
	call	_ZN2v88internal8compiler19InstructionSelector21AlignmentRequirementsEv@PLT
	movq	152(%rbx), %rdi
	movq	%rax, -68(%rbp)
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	movl	%edx, -60(%rbp)
	subq	%r12, %rax
	cmpq	$39, %rax
	jbe	.L253
	leaq	40(%r12), %rax
	movq	%rax, 16(%rdi)
.L243:
	movq	-68(%rbp), %r8
	movl	-60(%rbp), %r9d
	movq	%r12, %rdi
	movl	%r13d, %ecx
	movq	152(%rbx), %rsi
	movl	$5, %edx
	call	_ZN2v88internal8compiler22MachineOperatorBuilderC1EPNS0_4ZoneENS0_21MachineRepresentationENS_4base5FlagsINS2_4FlagEjEENS2_21AlignmentRequirementsE@PLT
	movq	152(%rbx), %rdi
	movq	%r12, 192(%rbx)
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L254
	leaq	16(%r12), %rax
	movq	%rax, 16(%rdi)
.L245:
	movq	152(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilderC1EPNS0_4ZoneE@PLT
	movq	152(%rbx), %rdi
	movq	%r12, 200(%rbx)
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L255
	leaq	16(%r12), %rax
	movq	%rax, 16(%rdi)
.L247:
	movq	152(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilderC1EPNS0_4ZoneE@PLT
	movq	152(%rbx), %rdi
	movq	%r12, 208(%rbx)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$631, %rdx
	jbe	.L256
	leaq	632(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L249:
	movq	160(%rbx), %rdx
	movq	184(%rbx), %rcx
	movq	208(%rbx), %rsi
	movq	(%rbx), %rdi
	movq	192(%rbx), %r8
	movq	200(%rbx), %r9
	movq	%rdx, (%rax)
	movq	(%rdx), %rdx
	movq	$0, 24(%rax)
	movq	%r9, 8(%rax)
	movq	%r8, 16(%rax)
	movq	$0, 32(%rax)
	movq	$256, 40(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	$256, 72(%rax)
	movq	$0, 88(%rax)
	movq	$0, 96(%rax)
	movq	$256, 104(%rax)
	movq	$0, 120(%rax)
	movq	$0, 128(%rax)
	movq	$256, 136(%rax)
	movq	$0, 152(%rax)
	movq	$0, 160(%rax)
	movq	$256, 168(%rax)
	movq	$0, 184(%rax)
	movq	$0, 192(%rax)
	movq	$256, 200(%rax)
	movq	$0, 216(%rax)
	movq	$0, 224(%rax)
	movq	$256, 232(%rax)
	movq	$0, 248(%rax)
	movq	$0, 256(%rax)
	movq	$256, 264(%rax)
	movq	$0, 280(%rax)
	movq	$0, 288(%rax)
	movq	$256, 296(%rax)
	movq	$0, 312(%rax)
	movq	$0, 320(%rax)
	movq	$256, 328(%rax)
	movq	%rdx, 344(%rax)
	movq	$0, 352(%rax)
	movq	%rdi, 360(%rax)
	movq	%rsi, 368(%rax)
	movq	%rcx, 376(%rax)
	movq	$0, 384(%rax)
	movq	$0, 392(%rax)
	movq	$0, 400(%rax)
	movq	$0, 408(%rax)
	movq	$0, 416(%rax)
	movq	$0, 424(%rax)
	movq	$0, 432(%rax)
	movq	$0, 440(%rax)
	movq	$0, 448(%rax)
	movq	$0, 456(%rax)
	movq	$0, 464(%rax)
	movq	$0, 472(%rax)
	movq	$0, 480(%rax)
	movq	$0, 488(%rax)
	movq	$0, 496(%rax)
	movq	$0, 504(%rax)
	movq	$0, 512(%rax)
	movq	$0, 520(%rax)
	movq	$0, 528(%rax)
	movq	$0, 536(%rax)
	movq	$0, 544(%rax)
	movq	$0, 552(%rax)
	movq	$0, 560(%rax)
	movq	$0, 568(%rax)
	movq	$0, 576(%rax)
	movq	$0, 584(%rax)
	movq	$0, 592(%rax)
	movq	$0, 600(%rax)
	movq	$0, 608(%rax)
	movq	$0, 616(%rax)
	movq	$0, 624(%rax)
	movq	%rax, 216(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L257
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L253:
	movl	$40, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L254:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L255:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L256:
	movl	$632, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L249
.L257:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26315:
	.size	_ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS0_19AccountingAllocatorEPNS1_5GraphEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableEPNS0_20JumpOptimizationInfoERKNS0_16AssemblerOptionsE, .-_ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS0_19AccountingAllocatorEPNS1_5GraphEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableEPNS0_20JumpOptimizationInfoERKNS0_16AssemblerOptionsE
	.weak	_ZN2v88internal8compiler12PipelineDataC1EPNS1_9ZoneStatsEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS0_19AccountingAllocatorEPNS1_5GraphEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableEPNS0_20JumpOptimizationInfoERKNS0_16AssemblerOptionsE
	.set	_ZN2v88internal8compiler12PipelineDataC1EPNS1_9ZoneStatsEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS0_19AccountingAllocatorEPNS1_5GraphEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableEPNS0_20JumpOptimizationInfoERKNS0_16AssemblerOptionsE,_ZN2v88internal8compiler12PipelineDataC2EPNS1_9ZoneStatsEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS0_19AccountingAllocatorEPNS1_5GraphEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableEPNS0_20JumpOptimizationInfoERKNS0_16AssemblerOptionsE
	.section	.rodata._ZN2v88internal8compiler22PipelineCompilationJob34RegisterWeakObjectsInOptimizedCodeENS0_6HandleINS0_4CodeEEEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC26:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal8compiler22PipelineCompilationJob34RegisterWeakObjectsInOptimizedCodeENS0_6HandleINS0_4CodeEEEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22PipelineCompilationJob34RegisterWeakObjectsInOptimizedCodeENS0_6HandleINS0_4CodeEEEPNS0_7IsolateE
	.type	_ZN2v88internal8compiler22PipelineCompilationJob34RegisterWeakObjectsInOptimizedCodeENS0_6HandleINS0_4CodeEEEPNS0_7IsolateE, @function
_ZN2v88internal8compiler22PipelineCompilationJob34RegisterWeakObjectsInOptimizedCodeENS0_6HandleINS0_4CodeEEEPNS0_7IsolateE:
.LFB26467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-128(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$12, %edx
	subq	$120, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	cmpb	$0, -72(%rbp)
	jne	.L259
	movq	$0, -136(%rbp)
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L260:
	cmpw	$159, %ax
	je	.L261
	cmpw	$1023, %ax
	ja	.L261
	subw	$138, %ax
	cmpw	$9, %ax
	ja	.L262
	.p2align 4,,10
	.p2align 3
.L261:
	movq	-112(%rbp), %rax
	movq	41112(%rbx), %rdi
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L265
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	je	.L310
.L262:
	movq	%r13, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	jne	.L311
.L280:
	movq	-112(%rbp), %rax
	movq	(%rax), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	cmpw	$68, %ax
	jne	.L260
	cmpw	$1024, 11(%rdx)
	ja	.L261
	movq	%r13, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	je	.L280
.L311:
	cmpq	%r14, %r12
	je	.L281
	addq	$37592, %rbx
	movq	%r14, %r13
	.p2align 4,,10
	.p2align 3
.L282:
	movq	0(%r13), %rsi
	movq	%rbx, %rdi
	addq	$8, %r13
	call	_ZN2v88internal4Heap14AddRetainedMapENS0_6HandleINS0_3MapEEE@PLT
	cmpq	%r13, %r12
	jne	.L282
.L281:
	movq	(%r15), %rdx
	movq	31(%rdx), %rax
	movl	15(%rax), %eax
	movq	31(%rdx), %rdx
	orl	$8, %eax
	movl	%eax, 15(%rdx)
	testq	%r14, %r14
	je	.L258
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L258:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L312
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L313
.L267:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	movq	-1(%rsi), %rax
	cmpw	$68, 11(%rax)
	jne	.L262
.L310:
	cmpq	%r12, -136(%rbp)
	je	.L268
	movq	%rdx, (%r12)
	addq	$8, %r12
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L313:
	movq	%rbx, %rdi
	movq	%rsi, -144(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-144(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L268:
	movq	-136(%rbp), %rsi
	movabsq	$1152921504606846975, %rcx
	subq	%r14, %rsi
	movq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L314
	testq	%rax, %rax
	je	.L315
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	ja	.L316
	testq	%rcx, %rcx
	jne	.L317
	movq	$0, -136(%rbp)
	movl	$8, %ecx
	xorl	%eax, %eax
.L273:
	movq	%rdx, (%rax,%rsi)
	cmpq	%r14, %r12
	je	.L288
	leaq	-8(%r12), %rsi
	leaq	15(%rax), %rdx
	subq	%r14, %rsi
	subq	%r14, %rdx
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L289
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L289
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L276:
	movdqu	(%r14,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L276
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rcx
	leaq	(%r14,%rcx), %rdx
	addq	%rax, %rcx
	cmpq	%r8, %rdi
	je	.L278
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L278:
	leaq	16(%rax,%rsi), %r12
.L274:
	testq	%r14, %r14
	je	.L279
	movq	%r14, %rdi
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rax
.L279:
	movq	%rax, %r14
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L259:
	xorl	%r14d, %r14d
	jmp	.L281
.L317:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	salq	$3, %rcx
	.p2align 4,,10
	.p2align 3
.L271:
	movq	%rcx, %rdi
	movq	%rsi, -152(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rcx, -136(%rbp)
	call	_Znwm@PLT
	movq	-136(%rbp), %rcx
	movq	-144(%rbp), %rdx
	movq	-152(%rbp), %rsi
	addq	%rax, %rcx
	movq	%rcx, -136(%rbp)
	leaq	8(%rax), %rcx
	jmp	.L273
.L315:
	movl	$8, %ecx
	jmp	.L271
.L316:
	movabsq	$9223372036854775800, %rcx
	jmp	.L271
.L289:
	movq	%rax, %rcx
	movq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L275:
	movq	(%rdx), %rdi
	addq	$8, %rdx
	addq	$8, %rcx
	movq	%rdi, -8(%rcx)
	cmpq	%r12, %rdx
	jne	.L275
	jmp	.L278
.L288:
	movq	%rcx, %r12
	jmp	.L274
.L314:
	leaq	.LC26(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L312:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26467:
	.size	_ZN2v88internal8compiler22PipelineCompilationJob34RegisterWeakObjectsInOptimizedCodeENS0_6HandleINS0_4CodeEEEPNS0_7IsolateE, .-_ZN2v88internal8compiler22PipelineCompilationJob34RegisterWeakObjectsInOptimizedCodeENS0_6HandleINS0_4CodeEEEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal8compiler8Pipeline29NewWasmHeapStubCompilationJobEPNS0_7IsolateEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorESt10unique_ptrINS0_4ZoneESt14default_deleteISB_EEPNS1_5GraphENS0_4Code4KindESA_IA_cSC_ISJ_EERKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE.str1.1,"aMS",@progbits,1
.LC27:
	.string	"TurboFan"
	.section	.text._ZN2v88internal8compiler8Pipeline29NewWasmHeapStubCompilationJobEPNS0_7IsolateEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorESt10unique_ptrINS0_4ZoneESt14default_deleteISB_EEPNS1_5GraphENS0_4Code4KindESA_IA_cSC_ISJ_EERKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Pipeline29NewWasmHeapStubCompilationJobEPNS0_7IsolateEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorESt10unique_ptrINS0_4ZoneESt14default_deleteISB_EEPNS1_5GraphENS0_4Code4KindESA_IA_cSC_ISJ_EERKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE
	.type	_ZN2v88internal8compiler8Pipeline29NewWasmHeapStubCompilationJobEPNS0_7IsolateEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorESt10unique_ptrINS0_4ZoneESt14default_deleteISB_EEPNS1_5GraphENS0_4Code4KindESA_IA_cSC_ISJ_EERKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE, @function
_ZN2v88internal8compiler8Pipeline29NewWasmHeapStubCompilationJobEPNS0_7IsolateEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorESt10unique_ptrINS0_4ZoneESt14default_deleteISB_EEPNS1_5GraphENS0_4Code4KindESA_IA_cSC_ISJ_EERKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE:
.LFB26481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rbp), %rcx
	movq	%rsi, -120(%rbp)
	movq	24(%rbp), %rax
	movq	32(%rbp), %r12
	movq	%rdx, -96(%rbp)
	movq	%rdi, -104(%rbp)
	movl	$808, %edi
	movq	%r9, -88(%rbp)
	movq	(%rax), %r15
	movq	%rcx, -128(%rbp)
	movq	(%r8), %r9
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	$0, (%r8)
	movq	$0, (%rax)
	movq	%r9, -136(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal14CompilationJobE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%rax, %rbx
	leaq	72(%rax), %r14
	movl	$1, 8(%rax)
	movq	$0, 16(%rax)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	%r15, 64(%rbx)
	movq	%rax, 16(%rbx)
	leaq	.LC27(%rip), %rcx
	movq	-88(%rbp), %rax
	movq	%rcx, 56(%rbx)
	leaq	16+_ZTVN2v88internal8compiler26WasmHeapStubCompilationJobE(%rip), %rcx
	movq	%rcx, (%rbx)
	movq	(%rax), %rcx
	movups	%xmm0, 32(%rbx)
	movq	%rcx, -112(%rbp)
	movq	%r14, 24(%rbx)
	movq	$0, 48(%rbx)
	call	strlen@PLT
	movl	16(%rbp), %r8d
	movq	-112(%rbp), %rcx
	movq	%r15, %rsi
	movq	%rax, %rdx
	movq	%r14, %rdi
	leaq	248(%rbx), %r15
	call	_ZN2v88internal24OptimizedCompilationInfoC1ENS0_6VectorIKcEEPNS0_4ZoneENS0_4Code4KindE@PLT
	movq	-136(%rbp), %r9
	movq	%r15, %rdi
	movq	%r13, 240(%rbx)
	movq	32(%r9), %rsi
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal8compiler9ZoneStatsC1EPNS0_19AccountingAllocatorE@PLT
	movq	-112(%rbp), %r9
	leaq	336(%rbx), %rax
	movq	%rax, -112(%rbp)
	movq	16(%r9), %r13
	movq	24(%r9), %rax
	movq	%r9, %xmm0
	movhps	-88(%rbp), %xmm0
	subq	%r13, %rax
	movups	%xmm0, 320(%rbx)
	cmpq	$87, %rax
	jbe	.L333
	leaq	88(%r13), %rax
	movq	%rax, 16(%r9)
.L320:
	movq	-88(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler15NodeOriginTableC1EPNS1_5GraphE@PLT
	movq	-96(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	%r14, %rsi
	movq	328(%rbx), %rax
	leaq	368(%rbx), %rdi
	movq	$0, 344(%rbx)
	movq	%rdx, 336(%rbx)
	movq	%rax, -88(%rbp)
	leaq	408(%rcx), %rax
	movq	%rax, 352(%rbx)
	movq	%r14, 360(%rbx)
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%r15, %rdi
	leaq	.LC22(%rip), %rsi
	movups	%xmm0, 432(%rbx)
	movq	%rsi, 464(%rbx)
	movb	$1, 376(%rbx)
	movq	%r15, 384(%rbx)
	movq	$0, 392(%rbx)
	movw	%ax, 400(%rbx)
	movl	$-1, 404(%rbx)
	movb	$0, 408(%rbx)
	movb	$0, 416(%rbx)
	movq	$0, 448(%rbx)
	movl	$0, 456(%rbx)
	movb	$0, 460(%rbx)
	movq	%r15, 472(%rbx)
	movq	$0, 480(%rbx)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	384(%rbx), %rdi
	pxor	%xmm0, %xmm0
	leaq	.LC23(%rip), %rsi
	movq	%rax, %xmm1
	movups	%xmm0, 528(%rbx)
	punpcklqdq	%xmm1, %xmm1
	movups	%xmm0, 544(%rbx)
	movups	%xmm1, 480(%rbx)
	movq	-88(%rbp), %xmm1
	movups	%xmm0, 560(%rbx)
	movhps	-128(%rbp), %xmm1
	movq	%rsi, 576(%rbx)
	movq	%rdi, 584(%rbx)
	movups	%xmm1, 496(%rbx)
	movq	%r13, 512(%rbx)
	movq	$0, 520(%rbx)
	movq	$0, 592(%rbx)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	384(%rbx), %rdi
	leaq	.LC24(%rip), %rsi
	movq	$0, 608(%rbx)
	movq	%rax, %xmm1
	movq	%rsi, 616(%rbx)
	punpcklqdq	%xmm1, %xmm1
	movq	%rdi, 624(%rbx)
	movups	%xmm1, 592(%rbx)
	movq	$0, 632(%rbx)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	384(%rbx), %rdi
	pxor	%xmm0, %xmm0
	leaq	.LC25(%rip), %rsi
	movq	%rax, %xmm1
	movups	%xmm0, 648(%rbx)
	punpcklqdq	%xmm1, %xmm1
	movq	%rdi, 680(%rbx)
	movups	%xmm1, 632(%rbx)
	movq	$0, 664(%rbx)
	movq	%rsi, 672(%rbx)
	movq	$0, 688(%rbx)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movdqu	(%r12), %xmm2
	pxor	%xmm0, %xmm0
	movq	488(%rbx), %rdi
	movq	%rax, %xmm1
	leaq	736(%rbx), %rax
	movq	$0, 728(%rbx)
	movq	%rax, 720(%rbx)
	movq	16(%r12), %rax
	punpcklqdq	%xmm1, %xmm1
	movb	$0, 736(%rbx)
	movq	$0, 752(%rbx)
	movq	%rax, 776(%rbx)
	movq	$0, 784(%rbx)
	movups	%xmm1, 688(%rbx)
	movups	%xmm0, 704(%rbx)
	movups	%xmm2, 760(%rbx)
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L334
	leaq	16(%r12), %rax
	movq	%rax, 16(%rdi)
.L322:
	movq	488(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorBuilderC1EPNS0_4ZoneE@PLT
	movq	%r12, 520(%rbx)
	call	_ZN2v88internal8compiler19InstructionSelector29SupportedMachineOperatorFlagsEv@PLT
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler19InstructionSelector21AlignmentRequirementsEv@PLT
	movq	488(%rbx), %rdi
	movq	%rax, -68(%rbp)
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	movl	%edx, -60(%rbp)
	subq	%r12, %rax
	cmpq	$39, %rax
	jbe	.L335
	leaq	40(%r12), %rax
	movq	%rax, 16(%rdi)
.L324:
	movq	-68(%rbp), %r8
	movl	-60(%rbp), %r9d
	movq	%r12, %rdi
	movl	%r14d, %ecx
	movq	488(%rbx), %rsi
	movl	$5, %edx
	call	_ZN2v88internal8compiler22MachineOperatorBuilderC1EPNS0_4ZoneENS0_21MachineRepresentationENS_4base5FlagsINS2_4FlagEjEENS2_21AlignmentRequirementsE@PLT
	movq	488(%rbx), %rdi
	movq	%r12, 528(%rbx)
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L336
	leaq	16(%r12), %rax
	movq	%rax, 16(%rdi)
.L326:
	movq	488(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilderC1EPNS0_4ZoneE@PLT
	movq	488(%rbx), %rdi
	movq	%r12, 536(%rbx)
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L337
	leaq	16(%r12), %rax
	movq	%rax, 16(%rdi)
.L328:
	movq	488(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17JSOperatorBuilderC1EPNS0_4ZoneE@PLT
	movq	488(%rbx), %rdi
	movq	%r12, 544(%rbx)
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$631, %rdx
	jbe	.L338
	leaq	632(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L330:
	movq	496(%rbx), %rdi
	movdqu	496(%rbx), %xmm0
	movdqu	528(%rbx), %xmm3
	movq	520(%rbx), %rdx
	movq	544(%rbx), %rcx
	movq	336(%rbx), %rsi
	movq	528(%rbx), %r8
	movq	(%rdi), %rdi
	shufpd	$2, %xmm3, %xmm0
	movups	%xmm0, (%rax)
	movdqa	.LC28(%rip), %xmm0
	movq	$0, 24(%rax)
	movq	%r8, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 320(%rax)
	movq	$0, 56(%rax)
	movq	$0, 88(%rax)
	movq	$0, 120(%rax)
	movq	$0, 152(%rax)
	movq	$0, 184(%rax)
	movq	$0, 216(%rax)
	movq	$0, 248(%rax)
	movq	$0, 280(%rax)
	movq	$0, 312(%rax)
	movq	%rdi, 344(%rax)
	movq	$0, 352(%rax)
	movq	%rsi, 360(%rax)
	movq	%rcx, 368(%rax)
	movq	%rdx, 376(%rax)
	movq	$0, 384(%rax)
	movq	$0, 392(%rax)
	movq	$0, 400(%rax)
	movq	$0, 408(%rax)
	movq	$0, 416(%rax)
	movq	$0, 424(%rax)
	movq	$0, 432(%rax)
	movq	$0, 440(%rax)
	movq	$0, 448(%rax)
	movq	$0, 456(%rax)
	movq	$0, 464(%rax)
	movq	$0, 472(%rax)
	movq	$0, 480(%rax)
	movq	$0, 488(%rax)
	movq	$0, 496(%rax)
	movq	$0, 504(%rax)
	movq	$0, 512(%rax)
	movq	$0, 520(%rax)
	movq	$0, 528(%rax)
	movq	$0, 536(%rax)
	movq	$0, 544(%rax)
	movq	$0, 552(%rax)
	movq	$0, 560(%rax)
	movq	$0, 568(%rax)
	movq	$0, 576(%rax)
	movq	$0, 584(%rax)
	movq	$0, 592(%rax)
	movq	$0, 600(%rax)
	movq	$0, 608(%rax)
	movq	$0, 616(%rax)
	movq	-112(%rbp), %xmm0
	movq	$0, 624(%rax)
	movq	%rax, 552(%rbx)
	movq	-104(%rbp), %rax
	movhps	-96(%rbp), %xmm0
	movups	%xmm0, 792(%rbx)
	movq	%rbx, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L339
	movq	-104(%rbp), %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	movl	$88, %esi
	movq	%r9, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	movq	328(%rbx), %rax
	movq	%rax, -88(%rbp)
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L334:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L335:
	movl	$40, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L336:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L337:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L338:
	movl	$632, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L330
.L339:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26481:
	.size	_ZN2v88internal8compiler8Pipeline29NewWasmHeapStubCompilationJobEPNS0_7IsolateEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorESt10unique_ptrINS0_4ZoneESt14default_deleteISB_EEPNS1_5GraphENS0_4Code4KindESA_IA_cSC_ISJ_EERKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE, .-_ZN2v88internal8compiler8Pipeline29NewWasmHeapStubCompilationJobEPNS0_7IsolateEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorESt10unique_ptrINS0_4ZoneESt14default_deleteISB_EEPNS1_5GraphENS0_4Code4KindESA_IA_cSC_ISJ_EERKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_23InstructionRangesAsJSONE.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	", \"nodeIdToInstructionRange\": {"
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_23InstructionRangesAsJSONE.str1.1,"aMS",@progbits,1
.LC30:
	.string	"}"
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_23InstructionRangesAsJSONE.str1.8
	.align 8
.LC31:
	.string	", \"blockIdtoInstructionRange\": {"
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_23InstructionRangesAsJSONE.str1.1
.LC32:
	.string	", "
.LC33:
	.string	"\""
.LC34:
	.string	"\": ["
.LC35:
	.string	"]"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_23InstructionRangesAsJSONE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_23InstructionRangesAsJSONE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_23InstructionRangesAsJSONE, @function
_ZN2v88internal8compilerlsERSoRKNS1_23InstructionRangesAsJSONE:
.LFB26646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rcx
	leaq	.LC29(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	264(%rcx), %rax
	subq	232(%rcx), %rax
	sarq	$3, %rax
	subq	$1, %rax
	salq	$6, %rax
	movq	%rax, %rdx
	movq	240(%rcx), %rax
	subq	248(%rcx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	movq	224(%rcx), %rdx
	subq	208(%rcx), %rdx
	sarq	$3, %rdx
	leal	-1(%rax,%rdx), %eax
	movl	$31, %edx
	movl	%eax, -80(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rax
	xorl	%edi, %edi
	movq	8(%rax), %rdx
	movq	16(%rax), %rsi
	cmpq	%rdx, %rsi
	jne	.L341
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L345:
	movl	$1, %edx
	leaq	.LC33(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$4, %edx
	leaq	.LC34(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC35(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rax
	movl	$1, %edi
	movq	8(%rax), %rdx
	movq	16(%rax), %rsi
.L344:
	movq	%rsi, %rax
	addq	$1, %r15
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r15
	jnb	.L346
.L341:
	leaq	(%rdx,%r15,8), %rax
	movl	(%rax), %ecx
	cmpl	$-1, %ecx
	je	.L344
	movl	-80(%rbp), %edx
	movl	%edx, %r13d
	subl	4(%rax), %edx
	subl	%ecx, %r13d
	movl	%edx, %r12d
	addl	$1, %r13d
	addl	$1, %r12d
	testb	%dil, %dil
	je	.L345
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L346:
	movl	$1, %edx
	leaq	.LC30(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$32, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rbx
	movq	16(%rax), %rax
	movq	%rax, -80(%rbp)
	cmpq	%rax, %rbx
	je	.L343
	movq	(%rbx), %r12
	leaq	-60(%rbp), %r15
	addq	$8, %rbx
	leaq	.LC33(%rip), %r13
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L360:
	movl	$2, %edx
	movq	%r14, %rdi
	movq	(%rbx), %r12
	addq	$8, %rbx
	leaq	.LC32(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L347:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	100(%r12), %eax
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%eax, -60(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_9RpoNumberE@PLT
	movl	$4, %edx
	leaq	.LC34(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	112(%r12), %esi
	movq	-72(%rbp), %rdi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	116(%r12), %esi
	movq	-72(%rbp), %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC35(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%rbx, -80(%rbp)
	jne	.L360
.L343:
	movl	$1, %edx
	leaq	.LC30(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L361
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L361:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26646:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_23InstructionRangesAsJSONE, .-_ZN2v88internal8compilerlsERSoRKNS1_23InstructionRangesAsJSONE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_17BlockStartsAsJSONE.str1.1,"aMS",@progbits,1
.LC36:
	.string	", \"blockIdToOffset\": {"
.LC37:
	.string	"\":"
.LC38:
	.string	"},"
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_17BlockStartsAsJSONE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_17BlockStartsAsJSONE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_17BlockStartsAsJSONE, @function
_ZN2v88internal8compilerlsERSoRKNS1_17BlockStartsAsJSONE:
.LFB26738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$22, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	leaq	.LC36(%rip), %rsi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdx
	movq	8(%rdx), %rax
	cmpq	16(%rdx), %rax
	je	.L363
	xorl	%ebx, %ebx
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L366:
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movq	8(%rax), %rax
.L364:
	movl	$1, %edx
	leaq	.LC33(%rip), %rsi
	movq	%r14, %rdi
	movl	(%rax,%rbx,4), %r12d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	addq	$1, %rbx
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC37(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNSolsEi@PLT
	movq	(%r15), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	$2, %rax
	cmpq	%rax, %rbx
	jb	.L366
.L363:
	movq	%r14, %rdi
	movl	$2, %edx
	leaq	.LC38(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26738:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_17BlockStartsAsJSONE, .-_ZN2v88internal8compilerlsERSoRKNS1_17BlockStartsAsJSONE
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl31VerifyGeneratedCodeIsIdempotentEv.str1.8,"aMS",@progbits,1
	.align 8
.LC39:
	.string	"hash_code == jump_opt->hash_code()"
	.section	.text._ZN2v88internal8compiler12PipelineImpl31VerifyGeneratedCodeIsIdempotentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12PipelineImpl31VerifyGeneratedCodeIsIdempotentEv
	.type	_ZN2v88internal8compiler12PipelineImpl31VerifyGeneratedCodeIsIdempotentEv, @function
_ZN2v88internal8compiler12PipelineImpl31VerifyGeneratedCodeIsIdempotentEv:
.LFB26862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	416(%rax), %rcx
	movq	%rcx, -88(%rbp)
	testq	%rcx, %rcx
	je	.L367
	movq	272(%rax), %rbx
	movq	16(%rbx), %rax
	movl	272(%rbx), %r14d
	movq	16(%rax), %rdi
	subq	8(%rax), %rdi
	movl	%r14d, -60(%rbp)
	sarq	$3, %rdi
	call	_ZN2v84base10hash_valueEj@PLT
	movl	%r14d, %edi
	movq	%rax, %r12
	call	_ZN2v84base10hash_valueEj@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	208(%rbx), %r14
	movq	%rax, %r15
	movq	224(%rbx), %rax
	movq	%rax, -56(%rbp)
	movq	232(%rbx), %rax
	movq	%rax, -80(%rbp)
	movq	240(%rbx), %rax
	movq	%rax, -72(%rbp)
	cmpq	%r14, %rax
	je	.L370
	.p2align 4,,10
	.p2align 3
.L369:
	movq	(%r14), %rax
	movq	%r15, %rdi
	addq	$8, %r14
	movl	(%rax), %r12d
	movl	4(%rax), %r13d
	call	_ZN2v84base10hash_valueEm@PLT
	movl	%r12d, %edi
	movq	%rax, %r15
	call	_ZN2v84base10hash_valueEj@PLT
	movl	%r13d, %edi
	movzbl	%r13b, %esi
	shrl	$8, %edi
	movq	%rax, %r12
	movzwl	%di, %edi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %r15
	cmpq	%r14, -56(%rbp)
	je	.L384
	cmpq	%r14, -72(%rbp)
	jne	.L369
.L370:
	movl	-60(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L373
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L374:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	addl	$1, %r14d
	call	_ZNK2v88internal8compiler19InstructionSequence17GetRepresentationEi@PLT
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	_ZN2v84base10hash_valueEm@PLT
	movzbl	%r12b, %esi
	xorl	%edi, %edi
	movq	%rax, %r15
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %r15
	cmpl	-60(%rbp), %r14d
	jne	.L374
.L373:
	movq	-88(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L375
	movq	%r15, 32(%rax)
.L367:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	movq	-80(%rbp), %rdx
	movq	8(%rdx), %r14
	leaq	8(%rdx), %rax
	leaq	512(%r14), %rcx
	movq	%rcx, -56(%rbp)
	cmpq	%r14, -72(%rbp)
	je	.L370
	movq	%rax, -80(%rbp)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L375:
	cmpq	%r15, 32(%rax)
	je	.L367
	leaq	.LC39(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE26862:
	.size	_ZN2v88internal8compiler12PipelineImpl31VerifyGeneratedCodeIsIdempotentEv, .-_ZN2v88internal8compiler12PipelineImpl31VerifyGeneratedCodeIsIdempotentEv
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_23InstructionStartsAsJSONE.str1.8,"aMS",@progbits,1
	.align 8
.LC40:
	.string	", \"instructionOffsetToPCOffset\": {"
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_23InstructionStartsAsJSONE.str1.1,"aMS",@progbits,1
.LC41:
	.string	"\": {"
.LC42:
	.string	"\"gap\": "
.LC43:
	.string	", \"arch\": "
.LC44:
	.string	", \"condition\": "
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_23InstructionStartsAsJSONE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_23InstructionStartsAsJSONE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_23InstructionStartsAsJSONE, @function
_ZN2v88internal8compilerlsERSoRKNS1_23InstructionStartsAsJSONE:
.LFB26863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$34, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	leaq	.LC40(%rip), %rsi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdx
	movq	8(%rdx), %rax
	cmpq	16(%rdx), %rax
	je	.L386
	movabsq	$-6148914691236517205, %rbx
	xorl	%r13d, %r13d
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L389:
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movq	8(%rax), %rax
.L387:
	leaq	0(%r13,%r13,2), %rdx
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	leaq	(%rax,%rdx,4), %r14
	movl	$1, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	addq	$1, %r13
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$4, %edx
	leaq	.LC41(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$7, %edx
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%r14), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$10, %edx
	leaq	.LC43(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	4(%r14), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$15, %edx
	leaq	.LC44(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	8(%r14), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	$2, %rax
	imulq	%rbx, %rax
	cmpq	%rax, %r13
	jb	.L389
.L386:
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC30(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26863:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_23InstructionStartsAsJSONE, .-_ZN2v88internal8compilerlsERSoRKNS1_23InstructionStartsAsJSONE
	.section	.rodata._ZN2v88internal8compilerlsERSoRKNS1_31TurbolizerCodeOffsetsInfoAsJSONE.str1.1,"aMS",@progbits,1
.LC45:
	.string	", \"codeOffsetsInfo\": {"
.LC46:
	.string	"\"codeStartRegisterCheck\": "
.LC47:
	.string	"\"deoptCheck\": "
.LC48:
	.string	"\"initPoison\": "
.LC49:
	.string	"\"blocksStart\": "
.LC50:
	.string	"\"outOfLineCode\": "
.LC51:
	.string	"\"deoptimizationExits\": "
.LC52:
	.string	"\"pools\": "
.LC53:
	.string	"\"jumpTables\": "
	.section	.text._ZN2v88internal8compilerlsERSoRKNS1_31TurbolizerCodeOffsetsInfoAsJSONE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8compilerlsERSoRKNS1_31TurbolizerCodeOffsetsInfoAsJSONE
	.type	_ZN2v88internal8compilerlsERSoRKNS1_31TurbolizerCodeOffsetsInfoAsJSONE, @function
_ZN2v88internal8compilerlsERSoRKNS1_31TurbolizerCodeOffsetsInfoAsJSONE:
.LFB26864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$22, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	.LC45(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$26, %edx
	movq	%r12, %rdi
	leaq	.LC46(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$14, %edx
	movq	%r12, %rdi
	leaq	.LC47(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	4(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$14, %edx
	movq	%r12, %rdi
	leaq	.LC48(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	8(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$15, %edx
	movq	%r12, %rdi
	leaq	.LC49(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	12(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$17, %edx
	movq	%r12, %rdi
	leaq	.LC50(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	16(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$23, %edx
	movq	%r12, %rdi
	leaq	.LC51(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	20(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$9, %edx
	movq	%r12, %rdi
	leaq	.LC52(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	24(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$14, %edx
	movq	%r12, %rdi
	leaq	.LC53(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	28(%rax), %esi
	call	_ZNSolsEi@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC30(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26864:
	.size	_ZN2v88internal8compilerlsERSoRKNS1_31TurbolizerCodeOffsetsInfoAsJSONE, .-_ZN2v88internal8compilerlsERSoRKNS1_31TurbolizerCodeOffsetsInfoAsJSONE
	.section	.text._ZN2v88internal8compiler12PipelineImpl18CommitDependenciesENS0_6HandleINS0_4CodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12PipelineImpl18CommitDependenciesENS0_6HandleINS0_4CodeEEE
	.type	_ZN2v88internal8compiler12PipelineImpl18CommitDependenciesENS0_6HandleINS0_4CodeEEE, @function
_ZN2v88internal8compiler12PipelineImpl18CommitDependenciesENS0_6HandleINS0_4CodeEEE:
.LFB26869:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	312(%rax), %rdi
	testq	%rdi, %rdi
	je	.L393
	jmp	_ZN2v88internal8compiler23CompilationDependencies6CommitENS0_6HandleINS0_4CodeEEE@PLT
	.p2align 4,,10
	.p2align 3
.L393:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE26869:
	.size	_ZN2v88internal8compiler12PipelineImpl18CommitDependenciesENS0_6HandleINS0_4CodeEEE, .-_ZN2v88internal8compiler12PipelineImpl18CommitDependenciesENS0_6HandleINS0_4CodeEEE
	.section	.text._ZNK2v88internal8compiler12PipelineImpl4infoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler12PipelineImpl4infoEv
	.type	_ZNK2v88internal8compiler12PipelineImpl4infoEv, @function
_ZNK2v88internal8compiler12PipelineImpl4infoEv:
.LFB26900:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	ret
	.cfi_endproc
.LFE26900:
	.size	_ZNK2v88internal8compiler12PipelineImpl4infoEv, .-_ZNK2v88internal8compiler12PipelineImpl4infoEv
	.section	.text._ZNK2v88internal8compiler12PipelineImpl7isolateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler12PipelineImpl7isolateEv
	.type	_ZNK2v88internal8compiler12PipelineImpl7isolateEv, @function
_ZNK2v88internal8compiler12PipelineImpl7isolateEv:
.LFB26901:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE26901:
	.size	_ZNK2v88internal8compiler12PipelineImpl7isolateEv, .-_ZNK2v88internal8compiler12PipelineImpl7isolateEv
	.section	.text._ZNK2v88internal8compiler12PipelineImpl14code_generatorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler12PipelineImpl14code_generatorEv
	.type	_ZNK2v88internal8compiler12PipelineImpl14code_generatorEv, @function
_ZNK2v88internal8compiler12PipelineImpl14code_generatorEv:
.LFB26902:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	104(%rax), %rax
	ret
	.cfi_endproc
.LFE26902:
	.size	_ZNK2v88internal8compiler12PipelineImpl14code_generatorEv, .-_ZNK2v88internal8compiler12PipelineImpl14code_generatorEv
	.section	.text._ZN2v88internal6String9VisitFlatINS0_21StringCharacterStreamEEENS0_10ConsStringEPT_S1_i,"axG",@progbits,_ZN2v88internal6String9VisitFlatINS0_21StringCharacterStreamEEENS0_10ConsStringEPT_S1_i,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6String9VisitFlatINS0_21StringCharacterStreamEEENS0_10ConsStringEPT_S1_i
	.type	_ZN2v88internal6String9VisitFlatINS0_21StringCharacterStreamEEENS0_10ConsStringEPT_S1_i, @function
_ZN2v88internal6String9VisitFlatINS0_21StringCharacterStreamEEENS0_10ConsStringEPT_S1_i:
.LFB28704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L401(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	11(%rsi), %edi
.L398:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L399
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6String9VisitFlatINS0_21StringCharacterStreamEEENS0_10ConsStringEPT_S1_i,"aG",@progbits,_ZN2v88internal6String9VisitFlatINS0_21StringCharacterStreamEEENS0_10ConsStringEPT_S1_i,comdat
	.align 4
	.align 4
.L401:
	.long	.L407-.L401
	.long	.L404-.L401
	.long	.L406-.L401
	.long	.L402-.L401
	.long	.L399-.L401
	.long	.L400-.L401
	.long	.L399-.L401
	.long	.L399-.L401
	.long	.L405-.L401
	.long	.L404-.L401
	.long	.L403-.L401
	.long	.L402-.L401
	.long	.L399-.L401
	.long	.L400-.L401
	.section	.text._ZN2v88internal6String9VisitFlatINS0_21StringCharacterStreamEEENS0_10ConsStringEPT_S1_i,"axG",@progbits,_ZN2v88internal6String9VisitFlatINS0_21StringCharacterStreamEEENS0_10ConsStringEPT_S1_i,comdat
	.p2align 4,,10
	.p2align 3
.L400:
	movq	15(%rsi), %rsi
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L404:
	addq	$8, %rsp
	movq	%rsi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	addl	27(%rsi), %r12d
	movq	15(%rsi), %rsi
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L405:
	movslq	%r12d, %rax
	subl	%edx, %edi
	movb	$1, 280(%rbx)
	leaq	15(%rsi,%rax), %rax
	movslq	%edi, %rdi
	movq	%rax, 288(%rbx)
	addq	%rdi, %rax
	movq	%rax, 296(%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	subl	%edx, %edi
	movl	%edi, %r13d
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r13d, %rdi
	movb	$0, 280(%rbx)
	movq	%rax, %r8
	movslq	%r12d, %rax
	leaq	(%r8,%rax,2), %rax
	movq	%rax, 288(%rbx)
	leaq	(%rax,%rdi,2), %rax
	movq	%rax, 296(%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	subl	%edx, %edi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movl	%edi, %r13d
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L409
	movq	8(%rdi), %rax
.L410:
	movslq	%r12d, %r12
	movslq	%r13d, %rdi
	movb	$1, 280(%rbx)
	addq	%r12, %rax
	movq	%rax, 288(%rbx)
	addq	%rdi, %rax
	movq	%rax, 296(%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_restore_state
	movslq	%r12d, %rax
	subl	%edx, %edi
	movb	$0, 280(%rbx)
	leaq	15(%rsi,%rax,2), %rax
	movslq	%edi, %rdi
	movq	%rax, 288(%rbx)
	leaq	(%rax,%rdi,2), %rax
	movq	%rax, 296(%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L399:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L409:
	call	*%rax
	jmp	.L410
	.cfi_endproc
.LFE28704:
	.size	_ZN2v88internal6String9VisitFlatINS0_21StringCharacterStreamEEENS0_10ConsStringEPT_S1_i, .-_ZN2v88internal6String9VisitFlatINS0_21StringCharacterStreamEEENS0_10ConsStringEPT_S1_i
	.section	.text._ZN2v88internal8compiler12GraphTrimmer9TrimGraphIN9__gnu_cxx17__normal_iteratorIPPNS1_4NodeESt6vectorIS7_NS0_13ZoneAllocatorIS7_EEEEEEEvT_SE_,"axG",@progbits,_ZN2v88internal8compiler12GraphTrimmer9TrimGraphIN9__gnu_cxx17__normal_iteratorIPPNS1_4NodeESt6vectorIS7_NS0_13ZoneAllocatorIS7_EEEEEEEvT_SE_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12GraphTrimmer9TrimGraphIN9__gnu_cxx17__normal_iteratorIPPNS1_4NodeESt6vectorIS7_NS0_13ZoneAllocatorIS7_EEEEEEEvT_SE_
	.type	_ZN2v88internal8compiler12GraphTrimmer9TrimGraphIN9__gnu_cxx17__normal_iteratorIPPNS1_4NodeESt6vectorIS7_NS0_13ZoneAllocatorIS7_EEEEEEEvT_SE_, @function
_ZN2v88internal8compiler12GraphTrimmer9TrimGraphIN9__gnu_cxx17__normal_iteratorIPPNS1_4NodeESt6vectorIS7_NS0_13ZoneAllocatorIS7_EEEEEEEvT_SE_:
.LFB29887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rdx, %rsi
	je	.L414
	movabsq	$2305843009213693948, %r14
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L415:
	movq	(%r12), %rbx
	movzbl	23(%rbx), %eax
	leaq	32(%rbx), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L417
	movq	32(%rbx), %rcx
	movl	8(%rcx), %eax
	addq	$16, %rcx
.L417:
	testl	%eax, %eax
	jle	.L418
	cmpq	$0, (%rcx)
	je	.L419
.L418:
	movl	8(%r13), %eax
	cmpl	%eax, 16(%rbx)
	ja	.L419
	addl	$1, %eax
	movl	%eax, 16(%rbx)
	movq	32(%r13), %r15
	cmpq	40(%r13), %r15
	je	.L420
	movq	%rbx, (%r15)
	addq	$8, 32(%r13)
.L419:
	addq	$8, %r12
	cmpq	%r12, %rdx
	jne	.L415
.L414:
	addq	$40, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler12GraphTrimmer9TrimGraphEv@PLT
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore_state
	movq	24(%r13), %r8
	movq	%r15, %rcx
	subq	%r8, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L444
	testq	%rax, %rax
	je	.L431
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L445
	movl	$2147483640, %esi
	movl	$2147483640, %r9d
.L422:
	movq	16(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r10
	subq	%rax, %r10
	cmpq	%rsi, %r10
	jb	.L446
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L425:
	leaq	(%rax,%r9), %rsi
	leaq	8(%rax), %rdi
.L423:
	movq	%rbx, (%rax,%rcx)
	cmpq	%r8, %r15
	je	.L426
	subq	$8, %r15
	leaq	15(%rax), %rcx
	subq	%r8, %r15
	subq	%r8, %rcx
	movq	%r15, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rcx
	jbe	.L434
	testq	%r14, %rdi
	je	.L434
	leaq	1(%rdi), %r9
	xorl	%ecx, %ecx
	movq	%r9, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L428:
	movdqu	(%r8,%rcx), %xmm1
	movups	%xmm1, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %rdi
	jne	.L428
	movq	%r9, %rcx
	andq	$-2, %rcx
	leaq	0(,%rcx,8), %rdi
	addq	%rdi, %r8
	addq	%rax, %rdi
	cmpq	%rcx, %r9
	je	.L430
	movq	(%r8), %rcx
	movq	%rcx, (%rdi)
.L430:
	leaq	16(%rax,%r15), %rdi
.L426:
	movq	%rax, %xmm0
	movq	%rdi, %xmm2
	movq	%rsi, 40(%r13)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 24(%r13)
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L445:
	testq	%rsi, %rsi
	jne	.L447
	movl	$8, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L431:
	movl	$8, %esi
	movl	$8, %r9d
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L434:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L427:
	movq	(%r8,%rcx,8), %r9
	movq	%r9, (%rax,%rcx,8)
	movq	%rcx, %r9
	addq	$1, %rcx
	cmpq	%r9, %rdi
	jne	.L427
	jmp	.L430
.L446:
	movq	%rdx, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
	jmp	.L425
.L447:
	movl	$268435455, %r9d
	cmpq	$268435455, %rsi
	cmova	%r9, %rsi
	leaq	0(,%rsi,8), %r9
	movq	%r9, %rsi
	jmp	.L422
.L444:
	leaq	.LC26(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29887:
	.size	_ZN2v88internal8compiler12GraphTrimmer9TrimGraphIN9__gnu_cxx17__normal_iteratorIPPNS1_4NodeESt6vectorIS7_NS0_13ZoneAllocatorIS7_EEEEEEEvT_SE_, .-_ZN2v88internal8compiler12GraphTrimmer9TrimGraphIN9__gnu_cxx17__normal_iteratorIPPNS1_4NodeESt6vectorIS7_NS0_13ZoneAllocatorIS7_EEEEEEEvT_SE_
	.section	.text._ZN2v84base11make_uniqueINS_8internal4wasm21WasmCompilationResultEJEEESt10unique_ptrIT_St14default_deleteIS6_EEDpOT0_,"axG",@progbits,_ZN2v84base11make_uniqueINS_8internal4wasm21WasmCompilationResultEJEEESt10unique_ptrIT_St14default_deleteIS6_EEDpOT0_,comdat
	.p2align 4
	.weak	_ZN2v84base11make_uniqueINS_8internal4wasm21WasmCompilationResultEJEEESt10unique_ptrIT_St14default_deleteIS6_EEDpOT0_
	.type	_ZN2v84base11make_uniqueINS_8internal4wasm21WasmCompilationResultEJEEESt10unique_ptrIT_St14default_deleteIS6_EEDpOT0_, @function
_ZN2v84base11make_uniqueINS_8internal4wasm21WasmCompilationResultEJEEESt10unique_ptrIT_St14default_deleteIS6_EEDpOT0_:
.LFB29992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$136, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movl	$17, %ecx
	movq	%rax, %rdx
	xorl	%eax, %eax
	movq	%rdx, %rdi
	movq	%rdx, (%r12)
	rep stosq
	movq	%r12, %rax
	movl	$-1, 128(%rdx)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE29992:
	.size	_ZN2v84base11make_uniqueINS_8internal4wasm21WasmCompilationResultEJEEESt10unique_ptrIT_St14default_deleteIS6_EEDpOT0_, .-_ZN2v84base11make_uniqueINS_8internal4wasm21WasmCompilationResultEJEEESt10unique_ptrIT_St14default_deleteIS6_EEDpOT0_
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.type	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, @function
_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E:
.LFB30222:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L458
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L452:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L452
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE30222:
	.size	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, .-_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB31613:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L469
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L463:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L463
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE31613:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB31621:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L480
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L474:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L474
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L480:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE31621:
	.size	_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE,"axG",@progbits,_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE
	.type	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE, @function
_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE:
.LFB31629:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L491
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L485:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L485
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L491:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE31629:
	.size	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE, .-_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB31840:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L502
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L496:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L496
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L502:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE31840:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_:
.LFB32080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r13
	cmpq	24(%rdi), %r13
	je	.L506
	movq	(%rsi), %rax
	movq	%rax, 0(%r13)
	addq	$8, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L506:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r13, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L531
	testq	%rax, %rax
	je	.L518
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L532
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L509:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L533
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L512:
	leaq	(%rax,%r15), %rdi
	leaq	8(%rax), %rdx
.L510:
	movq	(%r12), %rsi
	movq	%rsi, (%rax,%rcx)
	cmpq	%r14, %r13
	je	.L513
	leaq	-8(%r13), %rdx
	movq	%rdx, %rcx
	leaq	15(%rax), %rdx
	subq	%r14, %rcx
	subq	%r14, %rdx
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L521
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L521
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L515:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L515
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r14
	addq	%rax, %rsi
	cmpq	%rdx, %r8
	je	.L517
	movq	(%r14), %rdx
	movq	%rdx, (%rsi)
.L517:
	leaq	16(%rax,%rcx), %rdx
.L513:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L532:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L534
	movl	$8, %edx
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L518:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L521:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L514:
	movq	(%r14,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %rsi
	jne	.L514
	jmp	.L517
.L533:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L512
.L531:
	leaq	.LC26(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L534:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L509
	.cfi_endproc
.LFE32080:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	.type	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E, @function
_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E:
.LFB32088:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L543
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L537:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L537
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE32088:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E, .-_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	.section	.text._ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E,"axG",@progbits,_ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.type	_ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E, @function
_ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E:
.LFB32105:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L554
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L548:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L548
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE32105:
	.size	_ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E, .-_ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl3RunINS1_23MemoryOptimizationPhaseEJEEEvDpOT0_.isra.0.str1.1,"aMS",@progbits,1
.LC54:
	.string	"V8.TFMemoryOptimization"
	.section	.text._ZN2v88internal8compiler12PipelineImpl3RunINS1_23MemoryOptimizationPhaseEJEEEvDpOT0_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12PipelineImpl3RunINS1_23MemoryOptimizationPhaseEJEEEvDpOT0_.isra.0, @function
_ZN2v88internal8compiler12PipelineImpl3RunINS1_23MemoryOptimizationPhaseEJEEEvDpOT0_.isra.0:
.LFB38111:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	56(%rbx), %rax
	movq	%rax, -400(%rbp)
	testq	%rax, %rax
	je	.L558
	leaq	.LC54(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L558:
	movq	48(%rbx), %rax
	movq	176(%rbx), %rbx
	movq	%rax, -392(%rbp)
	testq	%rbx, %rbx
	je	.L559
	movq	48(%rbx), %rax
	movq	%rax, -416(%rbp)
	leaq	.LC54(%rip), %rax
	movq	%rax, 48(%rbx)
.L559:
	movq	-392(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%r14), %r13
	leaq	-352(%rbp), %r14
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	160(%r13), %rdx
	call	_ZN2v88internal8compiler12GraphTrimmerC1EPNS0_4ZoneEPNS1_5GraphE@PLT
	movq	216(%r13), %rdi
	leaq	-384(%rbp), %rsi
	movq	%r12, -384(%rbp)
	movq	$0, -376(%rbp)
	movq	$0, -368(%rbp)
	movq	$0, -360(%rbp)
	call	_ZN2v88internal8compiler7JSGraph14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	movq	-368(%rbp), %r10
	movq	-376(%rbp), %rax
	cmpq	%rax, %r10
	je	.L560
	movq	%rax, %r11
	.p2align 4,,10
	.p2align 3
.L561:
	movq	(%r11), %rcx
	movzbl	23(%rcx), %eax
	leaq	32(%rcx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L563
	movq	32(%rcx), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
.L563:
	testl	%eax, %eax
	jle	.L564
	cmpq	$0, (%rdx)
	je	.L565
.L564:
	movl	-344(%rbp), %eax
	cmpl	%eax, 16(%rcx)
	ja	.L565
	addl	$1, %eax
	movl	%eax, 16(%rcx)
	movq	-320(%rbp), %r15
	cmpq	-312(%rbp), %r15
	je	.L566
	movq	%rcx, (%r15)
	addq	$8, -320(%rbp)
.L565:
	addq	$8, %r11
	cmpq	%r11, %r10
	jne	.L561
.L560:
	movq	%r14, %rdi
	leaq	-304(%rbp), %r15
	call	_ZN2v88internal8compiler12GraphTrimmer9TrimGraphEv@PLT
	movq	24(%r13), %rax
	subq	$8, %rsp
	movq	%r12, %rdx
	movq	32(%r13), %r9
	movq	216(%r13), %rsi
	movq	%r15, %rdi
	movl	(%rax), %r8d
	movl	4(%rax), %ecx
	addq	$160, %rax
	pushq	%rax
	shrl	$12, %r8d
	xorl	$1, %r8d
	andl	$1, %r8d
	call	_ZN2v88internal8compiler15MemoryOptimizerC1EPNS1_7JSGraphEPNS0_4ZoneENS0_24PoisoningMitigationLevelENS2_17AllocationFoldingEPKcPNS0_11TickCounterE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler15MemoryOptimizer8OptimizeEv@PLT
	movq	-208(%rbp), %rax
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L577
	movq	-136(%rbp), %rdi
	movq	-168(%rbp), %rdx
	leaq	8(%rdi), %rcx
	cmpq	%rdx, %rcx
	jbe	.L578
	movq	-216(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L581:
	testq	%rax, %rax
	je	.L579
	cmpq	$32, 8(%rax)
	ja	.L580
.L579:
	movq	(%rdx), %rax
	movq	$32, 8(%rax)
	movq	-216(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -216(%rbp)
.L580:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L581
	movq	-208(%rbp), %rax
.L578:
	movq	-200(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L577
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L577:
	movq	-256(%rbp), %rax
	testq	%rax, %rax
	je	.L583
	leaq	-280(%rbp), %rdx
	movq	%rbx, -408(%rbp)
	movq	%rax, %r15
	movq	%rdx, %rbx
.L586:
	movq	24(%r15), %r13
	testq	%r13, %r13
	je	.L584
.L585:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIjSt4pairIKjN2v88internal10ZoneVectorIPKNS3_8compiler15MemoryOptimizer15AllocationStateEEEESt10_Select1stISB_ESt4lessIjENS3_13ZoneAllocatorISB_EEE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L585
.L584:
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.L586
	movq	-408(%rbp), %rbx
.L583:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12GraphTrimmerD1Ev@PLT
	testq	%rbx, %rbx
	je	.L587
	movq	-416(%rbp), %rax
	movq	%rax, 48(%rbx)
.L587:
	testq	%r12, %r12
	je	.L588
	movq	-392(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L588:
	movq	-400(%rbp), %rax
	testq	%rax, %rax
	je	.L557
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L557:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L635
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L566:
	.cfi_restore_state
	movq	-328(%rbp), %rdx
	movq	%r15, %r9
	subq	%rdx, %r9
	movq	%r9, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L636
	testq	%rax, %rax
	je	.L591
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L637
	movq	$2147483640, -408(%rbp)
	movl	$2147483640, %esi
.L568:
	movq	-336(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%r8, %rsi
	ja	.L638
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L571:
	movq	-408(%rbp), %r8
	leaq	8(%rax), %rsi
	addq	%rax, %r8
.L569:
	movq	%rcx, (%rax,%r9)
	cmpq	%rdx, %r15
	je	.L572
	subq	$8, %r15
	leaq	15(%rax), %rcx
	subq	%rdx, %r15
	subq	%rdx, %rcx
	movq	%r15, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rcx
	jbe	.L594
	movabsq	$2305843009213693948, %rdi
	testq	%rdi, %rsi
	je	.L594
	addq	$1, %rsi
	xorl	%ecx, %ecx
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L574:
	movdqu	(%rdx,%rcx), %xmm1
	movups	%xmm1, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rdi, %rcx
	jne	.L574
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rcx
	addq	%rcx, %rdx
	addq	%rax, %rcx
	cmpq	%rsi, %rdi
	je	.L576
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L576:
	leaq	16(%rax,%r15), %rsi
.L572:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%r8, -312(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -328(%rbp)
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L637:
	testq	%rsi, %rsi
	jne	.L639
	movl	$8, %esi
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L591:
	movq	$8, -408(%rbp)
	movl	$8, %esi
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L594:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L573:
	movq	(%rdx,%rcx,8), %rdi
	movq	%rdi, (%rax,%rcx,8)
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	%rdi, %rsi
	jne	.L573
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L638:
	movq	%r11, -456(%rbp)
	movq	%r10, -448(%rbp)
	movq	%r9, -440(%rbp)
	movq	%rdx, -432(%rbp)
	movq	%rcx, -424(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-424(%rbp), %rcx
	movq	-432(%rbp), %rdx
	movq	-440(%rbp), %r9
	movq	-448(%rbp), %r10
	movq	-456(%rbp), %r11
	jmp	.L571
.L635:
	call	__stack_chk_fail@PLT
.L636:
	leaq	.LC26(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L639:
	cmpq	$268435455, %rsi
	movl	$268435455, %r8d
	cmova	%r8, %rsi
	leaq	0(,%rsi,8), %rax
	movq	%rax, -408(%rbp)
	movq	%rax, %rsi
	jmp	.L568
	.cfi_endproc
.LFE38111:
	.size	_ZN2v88internal8compiler12PipelineImpl3RunINS1_23MemoryOptimizationPhaseEJEEEvDpOT0_.isra.0, .-_ZN2v88internal8compiler12PipelineImpl3RunINS1_23MemoryOptimizationPhaseEJEEEvDpOT0_.isra.0
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E:
.LFB32133:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L648
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L642:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L642
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L648:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE32133:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, .-_ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.section	.text._ZN2v88internal21RegisterConfigurationD2Ev,"axG",@progbits,_ZN2v88internal21RegisterConfigurationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21RegisterConfigurationD2Ev
	.type	_ZN2v88internal21RegisterConfigurationD2Ev, @function
_ZN2v88internal21RegisterConfigurationD2Ev:
.LFB32212:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE32212:
	.size	_ZN2v88internal21RegisterConfigurationD2Ev, .-_ZN2v88internal21RegisterConfigurationD2Ev
	.weak	_ZN2v88internal21RegisterConfigurationD1Ev
	.set	_ZN2v88internal21RegisterConfigurationD1Ev,_ZN2v88internal21RegisterConfigurationD2Ev
	.section	.text._ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.type	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, @function
_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E:
.LFB34844:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L660
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L654:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L654
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L660:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE34844:
	.size	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E, .-_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	.section	.text._ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.type	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, @function
_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E:
.LFB37209:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L678
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L667:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L665
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L663
.L666:
	movq	%rbx, %r12
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L665:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L666
.L663:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L678:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE37209:
	.size	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, .-_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.section	.text._ZN2v88internal9AssemblerD0Ev,"axG",@progbits,_ZN2v88internal9AssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9AssemblerD0Ev
	.type	_ZN2v88internal9AssemblerD0Ev, @function
_ZN2v88internal9AssemblerD0Ev:
.LFB37062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L685
.L682:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L682
.L685:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L683
.L684:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L688
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L683
.L689:
	movq	%rbx, %r13
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L688:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L689
.L683:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L687
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L690
	.p2align 4,,10
	.p2align 3
.L691:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L691
	movq	328(%r12), %rdi
.L690:
	call	_ZdlPv@PLT
.L687:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L692
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L693
	.p2align 4,,10
	.p2align 3
.L694:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L694
	movq	240(%r12), %rdi
.L693:
	call	_ZdlPv@PLT
.L692:
	movq	%r12, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE37062:
	.size	_ZN2v88internal9AssemblerD0Ev, .-_ZN2v88internal9AssemblerD0Ev
	.section	.text._ZN2v88internal9AssemblerD2Ev,"axG",@progbits,_ZN2v88internal9AssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9AssemblerD2Ev
	.type	_ZN2v88internal9AssemblerD2Ev, @function
_ZN2v88internal9AssemblerD2Ev:
.LFB37060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L719
.L716:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L716
.L719:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L717
.L718:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L722
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L717
.L723:
	movq	%rbx, %r13
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L722:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L723
.L717:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L721
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L724
	.p2align 4,,10
	.p2align 3
.L725:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L725
	movq	328(%r12), %rdi
.L724:
	call	_ZdlPv@PLT
.L721:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L726
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L727
	.p2align 4,,10
	.p2align 3
.L728:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L728
	movq	240(%r12), %rdi
.L727:
	call	_ZdlPv@PLT
.L726:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	.cfi_endproc
.LFE37060:
	.size	_ZN2v88internal9AssemblerD2Ev, .-_ZN2v88internal9AssemblerD2Ev
	.weak	_ZN2v88internal9AssemblerD1Ev
	.set	_ZN2v88internal9AssemblerD1Ev,_ZN2v88internal9AssemblerD2Ev
	.section	.text._ZN2v88internal8compiler12PipelineDataD2Ev,"axG",@progbits,_ZN2v88internal8compiler12PipelineDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler12PipelineDataD2Ev
	.type	_ZN2v88internal8compiler12PipelineDataD2Ev, @function
_ZN2v88internal8compiler12PipelineDataD2Ev:
.LFB26321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	104(%rdi), %r12
	testq	%r12, %r12
	je	.L750
	movq	1160(%r12), %rdi
	leaq	16+_ZTVN2v88internal8compiler13CodeGeneratorE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L751
	call	_ZdlPv@PLT
.L751:
	movq	960(%r12), %rax
	testq	%rax, %rax
	je	.L752
	movq	1032(%r12), %rcx
	movq	1000(%r12), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L753
	movq	952(%r12), %rax
	.p2align 4,,10
	.p2align 3
.L756:
	testq	%rax, %rax
	je	.L754
	cmpq	$16, 8(%rax)
	ja	.L755
.L754:
	movq	(%rdx), %rax
	movq	$16, 8(%rax)
	movq	952(%r12), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 952(%r12)
.L755:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L756
	movq	960(%r12), %rax
.L753:
	movq	968(%r12), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L752
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L752:
	movq	864(%r12), %rax
	testq	%rax, %rax
	je	.L758
	movq	936(%r12), %rcx
	movq	904(%r12), %rdx
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jbe	.L759
	movq	856(%r12), %rax
	.p2align 4,,10
	.p2align 3
.L762:
	testq	%rax, %rax
	je	.L760
	cmpq	$64, 8(%rax)
	ja	.L761
.L760:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	856(%r12), %rsi
	movq	%rsi, (%rax)
	movq	%rax, 856(%r12)
.L761:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L762
	movq	864(%r12), %rax
.L759:
	movq	872(%r12), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L758
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L758:
	leaq	200(%r12), %rax
	movq	680(%r12), %r13
	leaq	664(%r12), %r14
	movq	%rax, -56(%rbp)
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rax, 200(%r12)
	testq	%r13, %r13
	je	.L767
.L764:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L764
.L767:
	movq	624(%r12), %r15
	leaq	608(%r12), %r14
	testq	%r15, %r15
	je	.L765
.L766:
	movq	24(%r15), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r15), %rdi
	movq	16(%r15), %r13
	testq	%rdi, %rdi
	je	.L770
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L765
.L771:
	movq	%r13, %r15
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L770:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L771
.L765:
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L769
	movq	600(%r12), %rax
	movq	568(%r12), %r13
	leaq	8(%rax), %r14
	cmpq	%r13, %r14
	jbe	.L772
	.p2align 4,,10
	.p2align 3
.L773:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %r14
	ja	.L773
	movq	528(%r12), %rdi
.L772:
	call	_ZdlPv@PLT
.L769:
	movq	440(%r12), %rdi
	testq	%rdi, %rdi
	je	.L774
	movq	512(%r12), %rax
	movq	480(%r12), %r13
	leaq	8(%rax), %r14
	cmpq	%r13, %r14
	jbe	.L775
	.p2align 4,,10
	.p2align 3
.L776:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %r14
	ja	.L776
	movq	440(%r12), %rdi
.L775:
	call	_ZdlPv@PLT
.L774:
	movq	-56(%rbp), %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	movl	$1344, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L750:
	movq	112(%rbx), %r12
	movq	$0, 104(%rbx)
	testq	%r12, %r12
	je	.L777
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler5TyperD1Ev@PLT
	movl	$176, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L777:
	cmpq	$0, 360(%rbx)
	movq	$0, 112(%rbx)
	je	.L778
	movq	352(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L779
	movq	344(%rbx), %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L779:
	movq	$0, 368(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 352(%rbx)
.L778:
	cmpq	$0, 264(%rbx)
	je	.L780
	movq	256(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L781
	movq	248(%rbx), %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L781:
	movq	$0, 272(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 256(%rbx)
.L780:
	cmpq	$0, 304(%rbx)
	je	.L782
	movq	296(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L783
	movq	288(%rbx), %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L783:
	movq	320(%rbx), %r12
	pxor	%xmm0, %xmm0
	movq	$0, 312(%rbx)
	movups	%xmm0, 296(%rbx)
	testq	%r12, %r12
	je	.L784
	movq	640(%r12), %rax
	testq	%rax, %rax
	je	.L788
	.p2align 4,,10
	.p2align 3
.L785:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L785
.L788:
	movq	632(%r12), %rax
	movq	624(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	576(%r12), %rax
	testq	%rax, %rax
	je	.L786
	.p2align 4,,10
	.p2align 3
.L787:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L787
.L786:
	movq	568(%r12), %rax
	movq	560(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	512(%r12), %rax
	testq	%rax, %rax
	je	.L789
	.p2align 4,,10
	.p2align 3
.L790:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L790
.L789:
	movq	504(%r12), %rax
	movq	496(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, 208(%r12)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	192(%r12), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 128(%r12)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	208(%r12), %rdi
	movq	%rax, 128(%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 208(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L791
	.p2align 4,,10
	.p2align 3
.L792:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L792
.L791:
	movq	72(%r12), %rax
	movq	64(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movl	$712, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L784:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 320(%rbx)
.L782:
	cmpq	$0, 152(%rbx)
	je	.L793
	movq	144(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L794
	movq	136(%rbx), %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L794:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 160(%rbx)
	movups	%xmm0, 176(%rbx)
	movups	%xmm0, 192(%rbx)
	movups	%xmm0, 208(%rbx)
	movups	%xmm0, 224(%rbx)
.L793:
	movq	384(%rbx), %rdi
	leaq	400(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L795
	call	_ZdlPv@PLT
.L795:
	movq	352(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L796
	movq	344(%rbx), %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L796:
	movq	296(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L797
	movq	288(%rbx), %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L797:
	movq	256(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L798
	movq	248(%rbx), %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L798:
	movq	144(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L799
	movq	136(%rbx), %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L799:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L749
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L749:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26321:
	.size	_ZN2v88internal8compiler12PipelineDataD2Ev, .-_ZN2v88internal8compiler12PipelineDataD2Ev
	.weak	_ZN2v88internal8compiler12PipelineDataD1Ev
	.set	_ZN2v88internal8compiler12PipelineDataD1Ev,_ZN2v88internal8compiler12PipelineDataD2Ev
	.section	.rodata._ZN2v88internal8compiler22PipelineCompilationJobD2Ev.str1.8,"aMS",@progbits,1
	.align 8
.LC55:
	.string	"disabled-by-default-v8.compile"
	.section	.rodata._ZN2v88internal8compiler22PipelineCompilationJobD2Ev.str1.1,"aMS",@progbits,1
.LC56:
	.string	"compilationInfo"
.LC57:
	.string	"v8.optimizingCompile.end"
	.section	.text._ZN2v88internal8compiler22PipelineCompilationJobD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22PipelineCompilationJobD2Ev
	.type	_ZN2v88internal8compiler22PipelineCompilationJobD2Ev, @function
_ZN2v88internal8compiler22PipelineCompilationJobD2Ev:
.LFB26461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal8compiler22PipelineCompilationJobE(%rip), %rax
	movq	%rax, (%rdi)
	movq	_ZZN2v88internal8compiler22PipelineCompilationJobD4EvE28trace_event_unique_atomic934(%rip), %r12
	testq	%r12, %r12
	je	.L915
.L888:
	movq	$0, -96(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L916
.L890:
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	leaq	376(%rbx), %rdi
	call	_ZN2v88internal8compiler12PipelineDataD1Ev
	movq	368(%rbx), %r12
	testq	%r12, %r12
	je	.L895
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18PipelineStatisticsD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L895:
	leaq	200(%rbx), %rdi
	call	_ZN2v88internal24OptimizedCompilationInfoD1Ev@PLT
	leaq	128(%rbx), %rdi
	call	_ZN2v88internal8compiler9ZoneStatsD1Ev@PLT
	leaq	64(%rbx), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L917
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L915:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L918
.L889:
	movq	%r12, _ZZN2v88internal8compiler22PipelineCompilationJobD4EvE28trace_event_unique_atomic934(%rip)
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L916:
	movq	24(%rbx), %rsi
	leaq	-120(%rbp), %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal24OptimizedCompilationInfo13ToTracedValueEv@PLT
	leaq	.LC56(%rip), %rax
	movb	$8, -121(%rbp)
	movq	%rax, -112(%rbp)
	movq	-120(%rbp), %rax
	movq	$0, -56(%rbp)
	movq	$0, -120(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L919
.L891:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L892
	movq	(%rdi), %rax
	call	*8(%rax)
.L892:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L893
	movq	(%rdi), %rax
	call	*8(%rax)
.L893:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L894
	movq	(%rdi), %rax
	call	*8(%rax)
.L894:
	leaq	.LC57(%rip), %rax
	movq	%r12, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r13, -72(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L918:
	leaq	.LC55(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L919:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$260
	leaq	.LC57(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	leaq	-104(%rbp), %rdx
	pushq	%rdx
	leaq	-121(%rbp), %rdx
	pushq	%rdx
	leaq	-112(%rbp), %rdx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$1
	pushq	%rbx
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L891
.L917:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26461:
	.size	_ZN2v88internal8compiler22PipelineCompilationJobD2Ev, .-_ZN2v88internal8compiler22PipelineCompilationJobD2Ev
	.globl	_ZN2v88internal8compiler22PipelineCompilationJobD1Ev
	.set	_ZN2v88internal8compiler22PipelineCompilationJobD1Ev,_ZN2v88internal8compiler22PipelineCompilationJobD2Ev
	.section	.text._ZN2v88internal8compiler22PipelineCompilationJobD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22PipelineCompilationJobD0Ev
	.type	_ZN2v88internal8compiler22PipelineCompilationJobD0Ev, @function
_ZN2v88internal8compiler22PipelineCompilationJobD0Ev:
.LFB26463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler22PipelineCompilationJobD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$848, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE26463:
	.size	_ZN2v88internal8compiler22PipelineCompilationJobD0Ev, .-_ZN2v88internal8compiler22PipelineCompilationJobD0Ev
	.section	.text._ZN2v88internal8compiler26WasmHeapStubCompilationJobD2Ev,"axG",@progbits,_ZN2v88internal8compiler26WasmHeapStubCompilationJobD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler26WasmHeapStubCompilationJobD2Ev
	.type	_ZN2v88internal8compiler26WasmHeapStubCompilationJobD2Ev, @function
_ZN2v88internal8compiler26WasmHeapStubCompilationJobD2Ev:
.LFB32045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler26WasmHeapStubCompilationJobE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$336, %rdi
	movq	%rax, -336(%rdi)
	call	_ZN2v88internal8compiler12PipelineDataD1Ev
	movq	320(%rbx), %r12
	testq	%r12, %r12
	je	.L923
	movq	%r12, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L923:
	leaq	248(%rbx), %rdi
	call	_ZN2v88internal8compiler9ZoneStatsD1Ev@PLT
	leaq	72(%rbx), %rdi
	call	_ZN2v88internal24OptimizedCompilationInfoD1Ev@PLT
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L922
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L922:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE32045:
	.size	_ZN2v88internal8compiler26WasmHeapStubCompilationJobD2Ev, .-_ZN2v88internal8compiler26WasmHeapStubCompilationJobD2Ev
	.weak	_ZN2v88internal8compiler26WasmHeapStubCompilationJobD1Ev
	.set	_ZN2v88internal8compiler26WasmHeapStubCompilationJobD1Ev,_ZN2v88internal8compiler26WasmHeapStubCompilationJobD2Ev
	.section	.text._ZN2v88internal8compiler26WasmHeapStubCompilationJobD0Ev,"axG",@progbits,_ZN2v88internal8compiler26WasmHeapStubCompilationJobD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler26WasmHeapStubCompilationJobD0Ev
	.type	_ZN2v88internal8compiler26WasmHeapStubCompilationJobD0Ev, @function
_ZN2v88internal8compiler26WasmHeapStubCompilationJobD0Ev:
.LFB32047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal8compiler26WasmHeapStubCompilationJobE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$336, %rdi
	movq	%rax, -336(%rdi)
	call	_ZN2v88internal8compiler12PipelineDataD1Ev
	movq	320(%r12), %r13
	testq	%r13, %r13
	je	.L930
	movq	%r13, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L930:
	leaq	248(%r12), %rdi
	call	_ZN2v88internal8compiler9ZoneStatsD1Ev@PLT
	leaq	72(%r12), %rdi
	call	_ZN2v88internal24OptimizedCompilationInfoD1Ev@PLT
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L931
	call	_ZdaPv@PLT
.L931:
	movq	%r12, %rdi
	movl	$808, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE32047:
	.size	_ZN2v88internal8compiler26WasmHeapStubCompilationJobD0Ev, .-_ZN2v88internal8compiler26WasmHeapStubCompilationJobD0Ev
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler22PipelineCompilationJobC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler22PipelineCompilationJobC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler22PipelineCompilationJobC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE:
.LFB37382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE37382:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler22PipelineCompilationJobC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE, .-_GLOBAL__sub_I__ZN2v88internal8compiler22PipelineCompilationJobC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler22PipelineCompilationJobC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsENS0_6HandleINS0_6ScriptEEEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_9ZoneStatsE.str1.8,"aMS",@progbits,1
	.align 8
.LC58:
	.string	"disabled-by-default-v8.turbofan"
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsENS0_6HandleINS0_6ScriptEEEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_9ZoneStatsE.str1.1,"aMS",@progbits,1
.LC59:
	.string	"V8.TFInitializing"
.LC60:
	.string	"{\"function\" : "
.LC61:
	.string	",\n\"phases\":["
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsENS0_6HandleINS0_6ScriptEEEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_9ZoneStatsE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsENS0_6HandleINS0_6ScriptEEEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_9ZoneStatsE, @function
_ZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsENS0_6HandleINS0_6ScriptEEEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_9ZoneStatsE:
.LFB26446:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$568, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsENS0_6HandleINS0_6ScriptEEEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_9ZoneStatsEE28trace_event_unique_atomic821(%rip), %rax
	testq	%rax, %rax
	je	.L959
.L943:
	movzbl	(%rax), %eax
	testb	$5, %al
	jne	.L945
	cmpb	$0, _ZN2v88internal16FLAG_turbo_statsE(%rip)
	je	.L960
.L945:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate18GetTurboStatisticsEv@PLT
	movl	$176, %edi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8MallocednwEm@PLT
	movq	-600(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rcx
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatisticsC1EPNS0_24OptimizedCompilationInfoEPNS0_21CompilationStatisticsEPNS1_9ZoneStatsE@PLT
	leaq	.LC59(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc@PLT
.L946:
	testl	$16384, (%r12)
	jne	.L961
.L941:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L962
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L960:
	.cfi_restore_state
	xorl	%r13d, %r13d
	cmpb	$0, _ZN2v88internal20FLAG_turbo_stats_nvpE(%rip)
	jne	.L945
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L959:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L963
.L944:
	movq	%rax, _ZZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsENS0_6HandleINS0_6ScriptEEEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_9ZoneStatsEE28trace_event_unique_atomic821(%rip)
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L961:
	leaq	-576(%rbp), %r15
	movl	$32, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode@PLT
	movl	$14, %edx
	leaq	.LC60(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-584(%rbp), %rdx
	movq	24(%r12), %r9
	movq	%r12, %rsi
	movq	%rdx, %rdi
	movq	%rdx, -600(%rbp)
	movq	%r9, -608(%rbp)
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	subq	$8, %rsp
	movq	%r15, %rdi
	movq	%rbx, %r8
	pushq	$0
	movq	-600(%rbp), %rdx
	movq	%r14, %rcx
	movl	$-1, %esi
	movq	-608(%rbp), %r9
	call	_ZN2v88internal8compiler23JsonPrintFunctionSourceERSoiSt10unique_ptrIA_cSt14default_deleteIS4_EENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateENS8_INS0_18SharedFunctionInfoEEEb@PLT
	movq	-584(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L948
	call	_ZdaPv@PLT
.L948:
	movq	%r15, %rdi
	movl	$12, %edx
	leaq	.LC61(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileD1Ev@PLT
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L963:
	leaq	.LC58(%rip), %rsi
	call	*%rdx
	jmp	.L944
.L962:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26446:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsENS0_6HandleINS0_6ScriptEEEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_9ZoneStatsE, .-_ZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsENS0_6HandleINS0_6ScriptEEEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_9ZoneStatsE
	.section	.rodata._ZN2v88internal8compiler22PipelineCompilationJobC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC62:
	.string	"../deps/v8/src/compiler/pipeline.cc:918"
	.section	.rodata._ZN2v88internal8compiler22PipelineCompilationJobC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE.str1.1,"aMS",@progbits,1
.LC63:
	.string	"function"
.LC64:
	.string	"v8.optimizingCompile.start"
	.section	.text._ZN2v88internal8compiler22PipelineCompilationJobC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22PipelineCompilationJobC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE
	.type	_ZN2v88internal8compiler22PipelineCompilationJobC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE, @function
_ZN2v88internal8compiler22PipelineCompilationJobC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE:
.LFB26458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	200(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal14CompilationJobE(%rip), %rax
	movl	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 16(%rdi)
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, 24(%rbx)
	leaq	64(%rbx), %r10
	movq	%rax, 16(%rbx)
	leaq	.LC27(%rip), %rax
	movq	%r10, %rdi
	leaq	.LC62(%rip), %rdx
	movq	%rax, 56(%rbx)
	leaq	16+_ZTVN2v88internal8compiler22PipelineCompilationJobE(%rip), %rax
	movups	%xmm0, 32(%rbx)
	movq	$0, 48(%rbx)
	movq	%rax, (%rbx)
	movq	(%r12), %rax
	movq	%r10, -176(%rbp)
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	3544(%rax), %rsi
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	(%r12), %rax
	leaq	128(%rbx), %r9
	movq	%r9, %rdi
	movq	%r9, -168(%rbp)
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	3544(%rax), %rsi
	call	_ZN2v88internal8compiler9ZoneStatsC1EPNS0_19AccountingAllocatorE@PLT
	movq	(%r12), %rax
	movq	%r15, %rdi
	movq	%r12, %r8
	movq	-176(%rbp), %r10
	movq	%r14, %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	movq	%r10, %rsi
	subq	$37592, %rdx
	call	_ZN2v88internal24OptimizedCompilationInfoC1EPNS0_4ZoneEPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS6_INS0_10JSFunctionEEE@PLT
	movq	(%r12), %rax
	movq	24(%rbx), %r15
	movq	-168(%rbp), %r9
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	movq	(%r14), %rax
	movq	31(%rax), %rsi
	subq	$37592, %rdx
	testb	$1, %sil
	jne	.L993
.L965:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L966
	movq	%rdx, -176(%rbp)
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %r9
	movq	-176(%rbp), %rdx
	movq	%rax, %rdi
.L967:
	movq	%r9, %rcx
	movq	%r15, %rsi
	movq	%r9, -168(%rbp)
	leaq	376(%rbx), %r13
	call	_ZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsENS0_6HandleINS0_6ScriptEEEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_9ZoneStatsE
	movq	-168(%rbp), %r9
	movq	24(%rbx), %rcx
	movq	%r13, %rdi
	movq	%rax, 368(%rbx)
	movq	%rax, %r8
	movq	(%r12), %rax
	movq	%r9, %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	subq	$37592, %rdx
	call	_ZN2v88internal8compiler12PipelineDataC1EPNS1_9ZoneStatsEPNS0_7IsolateEPNS0_24OptimizedCompilationInfoEPNS1_18PipelineStatisticsE
	movq	%r13, 832(%rbx)
	movq	$0, 840(%rbx)
	movq	_ZZN2v88internal8compiler22PipelineCompilationJobC4EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEEE28trace_event_unique_atomic928(%rip), %r12
	testq	%r12, %r12
	je	.L994
.L970:
	movq	$0, -112(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L995
.L972:
	leaq	-112(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L996
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L966:
	.cfi_restore_state
	movq	41088(%r13), %rdi
	cmpq	%rdi, 41096(%r13)
	je	.L997
.L968:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdi)
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L993:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L965
	movq	23(%rsi), %rsi
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L995:
	movq	(%r14), %rax
	leaq	-144(%rbp), %rdi
	leaq	-136(%rbp), %rsi
	xorl	%r13d, %r13d
	movq	%rax, -136(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo10TraceIDRefEv@PLT
	leaq	.LC63(%rip), %rax
	movb	$8, -145(%rbp)
	movq	%rax, -128(%rbp)
	movq	-144(%rbp), %rax
	movq	$0, -72(%rbp)
	movq	$0, -144(%rbp)
	movq	%rax, -120(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L998
.L973:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L974
	movq	(%rdi), %rax
	call	*8(%rax)
.L974:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L975
	movq	(%rdi), %rax
	call	*8(%rax)
.L975:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L976
	movq	(%rdi), %rax
	call	*8(%rax)
.L976:
	leaq	.LC64(%rip), %rax
	movq	%r12, -104(%rbp)
	movq	%rax, -96(%rbp)
	leaq	-104(%rbp), %rax
	movq	%r13, -88(%rbp)
	movq	%rax, -112(%rbp)
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L994:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L999
.L971:
	movq	%r12, _ZZN2v88internal8compiler22PipelineCompilationJobC4EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEEE28trace_event_unique_atomic928(%rip)
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L997:
	movq	%r13, %rdi
	movq	%rsi, -184(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	-176(%rbp), %rdx
	movq	-168(%rbp), %r9
	movq	%rax, %rdi
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L999:
	leaq	.LC55(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L998:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$516
	leaq	.LC64(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	leaq	-120(%rbp), %rdx
	pushq	%rdx
	leaq	-145(%rbp), %rdx
	pushq	%rdx
	leaq	-128(%rbp), %rdx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$1
	pushq	%rbx
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L973
.L996:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26458:
	.size	_ZN2v88internal8compiler22PipelineCompilationJobC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE, .-_ZN2v88internal8compiler22PipelineCompilationJobC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE
	.globl	_ZN2v88internal8compiler22PipelineCompilationJobC1EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE
	.set	_ZN2v88internal8compiler22PipelineCompilationJobC1EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE,_ZN2v88internal8compiler22PipelineCompilationJobC2EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE
	.section	.text._ZN2v88internal8compiler8Pipeline17NewCompilationJobEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Pipeline17NewCompilationJobEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb
	.type	_ZN2v88internal8compiler8Pipeline17NewCompilationJobEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb, @function
_ZN2v88internal8compiler8Pipeline17NewCompilationJobEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb:
.LFB26820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdx), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	23(%rax), %rsi
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L1001
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L1002:
	movl	$848, %edi
	call	_Znwm@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler22PipelineCompilationJobC1EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEE
	movq	%rbx, (%r12)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1001:
	.cfi_restore_state
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L1005
.L1003:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L1003
	.cfi_endproc
.LFE26820:
	.size	_ZN2v88internal8compiler8Pipeline17NewCompilationJobEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb, .-_ZN2v88internal8compiler8Pipeline17NewCompilationJobEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb
	.section	.rodata._ZN2v88internal8compiler15PrintGraphPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneEPKc.str1.1,"aMS",@progbits,1
.LC65:
	.string	"{\"name\":\""
.LC66:
	.string	"\",\"type\":\"graph\",\"data\":"
.LC67:
	.string	"},\n"
.LC68:
	.string	"-- Graph after "
.LC69:
	.string	" -- "
	.section	.text._ZN2v88internal8compiler15PrintGraphPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneEPKc,"axG",@progbits,_ZN2v88internal8compiler15PrintGraphPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler15PrintGraphPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneEPKc
	.type	_ZN2v88internal8compiler15PrintGraphPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneEPKc, @function
_ZN2v88internal8compiler15PrintGraphPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneEPKc:
.LFB26716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$952, %rsp
	movq	%rdx, -984(%rbp)
	movq	24(%rsi), %r14
	movq	160(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r14), %eax
	testb	$64, %ah
	jne	.L1050
.L1007:
	testl	$65536, %eax
	je	.L1010
	movq	232(%rbx), %r13
	leaq	16+_ZTVN2v88internal19AccountingAllocatorE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, -960(%rbp)
	movups	%xmm0, -952(%rbp)
	testq	%r13, %r13
	je	.L1051
.L1011:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1052
	call	_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv@PLT
	movq	%rax, %rbx
.L1013:
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%rbx), %rsi
	je	.L1014
	testq	%rsi, %rsi
	je	.L1053
.L1015:
	addl	$1, 152(%rbx)
.L1014:
	leaq	-928(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movl	$15, %edx
	leaq	.LC68(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r12, %r12
	je	.L1054
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1017:
	movl	$4, %edx
	leaq	.LC69(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-928(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-688(%rbp,%rax), %r12
	testq	%r12, %r12
	je	.L1031
	cmpb	$0, 56(%r12)
	je	.L1019
	movsbl	67(%r12), %esi
.L1020:
	movq	%r14, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	-968(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r13, -968(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_16AsScheduledGraphE@PLT
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, -848(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-864(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -928(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-848(%rbp), %rdi
	movq	%rax, -928(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -848(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L1022
	subl	$1, 152(%rbx)
	je	.L1055
.L1022:
	leaq	-960(%rbp), %rdi
	call	_ZN2v88internal19AccountingAllocatorD1Ev@PLT
.L1006:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1056
	addq	$952, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1010:
	.cfi_restore_state
	testb	$-128, %ah
	je	.L1006
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1057
	call	_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv@PLT
	movq	%rax, %rbx
.L1026:
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%rbx), %rsi
	je	.L1027
	testq	%rsi, %rsi
	je	.L1058
.L1028:
	addl	$1, 152(%rbx)
.L1027:
	leaq	-576(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movl	$15, %edx
	leaq	.LC68(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r12, %r12
	je	.L1059
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1030:
	movl	$4, %edx
	leaq	.LC69(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-336(%rbp,%rax), %r12
	testq	%r12, %r12
	je	.L1031
	cmpb	$0, 56(%r12)
	je	.L1032
	movsbl	67(%r12), %esi
.L1033:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	-960(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r15, -960(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_5AsRPOE@PLT
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, -496(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-512(%rbp), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -576(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-496(%rbp), %rdi
	movq	%rax, -576(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -496(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L1006
	subl	$1, 152(%rbx)
	jne	.L1006
	movq	144(%rbx), %rdi
	call	fclose@PLT
	movq	$0, 144(%rbx)
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1050:
	leaq	-576(%rbp), %r13
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode@PLT
	movl	$9, %edx
	leaq	.LC65(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r12, %r12
	je	.L1060
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1009:
	movl	$24, %edx
	leaq	.LC66(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	168(%rbx), %rdx
	movq	176(%rbx), %rax
	leaq	-960(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r15, -960(%rbp)
	movq	%rdx, -952(%rbp)
	movq	%rax, -944(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_11GraphAsJSONE@PLT
	movl	$3, %edx
	leaq	.LC67(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileD1Ev@PLT
	movl	(%r14), %eax
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1019:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1020
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	movq	%rax, %rbx
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1051:
	movq	160(%rbx), %rsi
	movq	-984(%rbp), %rdi
	leaq	160(%r14), %rcx
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler9Scheduler15ComputeScheduleEPNS0_4ZoneEPNS1_5GraphENS_4base5FlagsINS2_4FlagEiEEPNS0_11TickCounterE@PLT
	movq	%rax, %r13
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1057:
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	movq	%rax, %rbx
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1054:
	movq	-928(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1032:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1033
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1055:
	movq	144(%rbx), %rdi
	call	fclose@PLT
	movq	$0, 144(%rbx)
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1060:
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1053:
	movq	(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %rsi
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1058:
	movq	(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %rsi
	jmp	.L1028
.L1056:
	call	__stack_chk_fail@PLT
.L1031:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE26716:
	.size	_ZN2v88internal8compiler15PrintGraphPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneEPKc, .-_ZN2v88internal8compiler15PrintGraphPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneEPKc
	.section	.text._ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb
	.type	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb, @function
_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb:
.LFB26719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movb	%dl, -68(%rbp)
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rbx), %rax
	testl	$49152, (%rax)
	je	.L1062
	movq	56(%rbx), %r14
	movq	%rsi, %r15
	testq	%r14, %r14
	je	.L1063
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1063:
	movq	48(%rbx), %r8
	movq	176(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1064
	leaq	.LC4(%rip), %rax
	movq	48(%rbx), %r9
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, 48(%rbx)
	movq	%r9, -88(%rbp)
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%r12), %rsi
	leaq	-57(%rbp), %rdi
	movq	%r15, %rcx
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZN2v88internal8compiler15PrintGraphPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneEPKc
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %r8
	movq	%r9, 48(%rbx)
.L1068:
	testq	%r13, %r13
	je	.L1065
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1065:
	testq	%r14, %r14
	je	.L1062
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1062:
	cmpb	$0, _ZN2v88internal17FLAG_turbo_verifyE(%rip)
	jne	.L1083
.L1061:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1084
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1083:
	.cfi_restore_state
	leaq	-68(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl3RunINS1_16VerifyGraphPhaseEJRbEEEvDpOT0_.isra.0
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1064:
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%r12), %rsi
	leaq	-57(%rbp), %rdi
	movq	%r15, %rcx
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZN2v88internal8compiler15PrintGraphPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneEPKc
	movq	-80(%rbp), %r8
	jmp	.L1068
.L1084:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26719:
	.size	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb, .-_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb
	.section	.text._ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0, @function
_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0:
.LFB38209:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$1, -58(%rbp)
	movq	24(%rbx), %rax
	testl	$49152, (%rax)
	je	.L1086
	movq	56(%rbx), %r14
	movq	%rsi, %r15
	testq	%r14, %r14
	je	.L1087
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1087:
	movq	48(%rbx), %r8
	movq	176(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1088
	leaq	.LC4(%rip), %rax
	movq	48(%rbx), %r9
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, 48(%rbx)
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%r12), %rsi
	leaq	-57(%rbp), %rdi
	movq	%r15, %rcx
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZN2v88internal8compiler15PrintGraphPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneEPKc
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r8
	movq	%r9, 48(%rbx)
.L1092:
	testq	%r13, %r13
	je	.L1089
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1089:
	testq	%r14, %r14
	je	.L1086
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1086:
	cmpb	$0, _ZN2v88internal17FLAG_turbo_verifyE(%rip)
	jne	.L1107
.L1085:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1108
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1107:
	.cfi_restore_state
	leaq	-58(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl3RunINS1_16VerifyGraphPhaseEJRbEEEvDpOT0_.isra.0
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1088:
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%r12), %rsi
	leaq	-57(%rbp), %rdi
	movq	%r15, %rcx
	movq	%rax, %rdx
	movq	%rax, %r13
	call	_ZN2v88internal8compiler15PrintGraphPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneEPKc
	movq	-72(%rbp), %r8
	jmp	.L1092
.L1108:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE38209:
	.size	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0, .-_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_113TraceScheduleEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPNS1_8ScheduleEPKc.str1.1,"aMS",@progbits,1
.LC70:
	.string	"\",\"type\":\"schedule\""
.LC71:
	.string	",\"data\":\""
.LC72:
	.string	"\"},\n"
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_113TraceScheduleEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPNS1_8ScheduleEPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC73:
	.string	"-- Schedule --------------------------------------\n"
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_113TraceScheduleEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPNS1_8ScheduleEPKc,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_113TraceScheduleEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPNS1_8ScheduleEPKc, @function
_ZN2v88internal8compiler12_GLOBAL__N_113TraceScheduleEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPNS1_8ScheduleEPKc:
.LFB26428:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1048, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -1040(%rbp)
	movq	%rdx, -1032(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	testb	$64, %ah
	jne	.L1137
.L1110:
	testb	$-128, %ah
	jne	.L1120
	cmpb	$0, _ZN2v88internal26FLAG_trace_turbo_schedulerE(%rip)
	je	.L1109
.L1120:
	movq	-1040(%rbp), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1138
	call	_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv@PLT
	movq	%rax, %rbx
.L1123:
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%rbx), %rsi
	je	.L1124
	testq	%rsi, %rsi
	je	.L1139
.L1125:
	addl	$1, 152(%rbx)
.L1124:
	leaq	-576(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movl	$51, %edx
	leaq	.LC73(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1032(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE@PLT
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, -496(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-512(%rbp), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -576(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-496(%rbp), %rdi
	movq	%rax, -576(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -496(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L1109
	subl	$1, 152(%rbx)
	je	.L1140
.L1109:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1141
	addq	$1048, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1137:
	.cfi_restore_state
	leaq	-576(%rbp), %r12
	movq	%rdi, %rsi
	movq	%rdi, %rbx
	movq	%rcx, %r14
	movl	$1, %edx
	movq	%r12, %rdi
	movq	.LC74(%rip), %xmm1
	leaq	-848(%rbp), %r15
	movhps	.LC75(%rip), %xmm1
	movaps	%xmm1, -1056(%rbp)
	call	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode@PLT
	movl	$9, %edx
	leaq	.LC65(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-976(%rbp), %r14
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$19, %edx
	leaq	.LC70(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$9, %edx
	leaq	.LC71(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	movq	%r15, -1064(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -848(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -624(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -616(%rbp)
	movups	%xmm0, -600(%rbp)
	movq	%rax, -976(%rbp)
	movq	$0, -632(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -976(%rbp,%rax)
	movq	-976(%rbp), %rax
	movq	$0, -968(%rbp)
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	leaq	-960(%rbp), %r14
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rax, -960(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -960(%rbp,%rax)
	movq	-960(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-1056(%rbp), %xmm1
	movq	%rax, -976(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -976(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -976(%rbp)
	addq	$80, %rax
	movq	%rax, -848(%rbp)
	leaq	-896(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -960(%rbp)
	movaps	%xmm0, -944(%rbp)
	movaps	%xmm0, -928(%rbp)
	movaps	%xmm0, -912(%rbp)
	movq	%rax, -1080(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rdi
	leaq	-952(%rbp), %rsi
	movq	%rax, -952(%rbp)
	leaq	-864(%rbp), %rax
	movq	%rax, -1056(%rbp)
	movq	%rax, -880(%rbp)
	movl	$24, -888(%rbp)
	movq	$0, -872(%rbp)
	movb	$0, -864(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-1032(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE@PLT
	leaq	-992(%rbp), %rax
	movq	$0, -1000(%rbp)
	leaq	-1008(%rbp), %rdi
	movq	%rax, -1072(%rbp)
	movq	%rax, -1008(%rbp)
	movq	-912(%rbp), %rax
	movb	$0, -992(%rbp)
	testq	%rax, %rax
	je	.L1111
	movq	-928(%rbp), %r8
	movq	-920(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1142
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1113:
	movq	-1008(%rbp), %rdx
	movq	-1000(%rbp), %r14
	leaq	-1010(%rbp), %r15
	addq	%rdx, %r14
	movq	%rdx, %r13
	cmpq	%r14, %rdx
	je	.L1118
	.p2align 4,,10
	.p2align 3
.L1117:
	movsbw	0(%r13), %ax
	movq	%r15, %rsi
	movq	%r12, %rdi
	addq	$1, %r13
	movw	%ax, -1010(%rbp)
	call	_ZN2v88internallsERSoRKNS0_20AsEscapedUC16ForJSONE@PLT
	cmpq	%r13, %r14
	jne	.L1117
.L1118:
	movq	%r12, %rdi
	movl	$4, %edx
	leaq	.LC72(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1008(%rbp), %rdi
	cmpq	-1072(%rbp), %rdi
	je	.L1116
	call	_ZdlPv@PLT
.L1116:
	movq	.LC74(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-880(%rbp), %rdi
	movq	%rax, -976(%rbp)
	addq	$80, %rax
	movhps	.LC76(%rip), %xmm0
	movq	%rax, -848(%rbp)
	movaps	%xmm0, -960(%rbp)
	cmpq	-1056(%rbp), %rdi
	je	.L1119
	call	_ZdlPv@PLT
.L1119:
	movq	-1080(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -952(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-1064(%rbp), %rdi
	movq	%rax, -976(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -976(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -960(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -960(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -976(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -976(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -848(%rbp)
	movq	$0, -968(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileD1Ev@PLT
	movl	(%rbx), %eax
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1138:
	movq	(%rax), %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	movq	%rax, %rbx
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1140:
	movq	144(%rbx), %rdi
	call	fclose@PLT
	movq	$0, 144(%rbx)
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1142:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1139:
	movq	(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %rsi
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1111:
	leaq	-880(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1113
.L1141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26428:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_113TraceScheduleEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPNS1_8ScheduleEPKc, .-_ZN2v88internal8compiler12_GLOBAL__N_113TraceScheduleEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPNS1_8ScheduleEPKc
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl21ComputeScheduledGraphEv.str1.1,"aMS",@progbits,1
.LC77:
	.string	"V8.TFLateGraphTrimming"
.LC78:
	.string	"V8.TFScheduling"
.LC79:
	.string	"schedule"
	.section	.text._ZN2v88internal8compiler12PipelineImpl21ComputeScheduledGraphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12PipelineImpl21ComputeScheduledGraphEv
	.type	_ZN2v88internal8compiler12PipelineImpl21ComputeScheduledGraphEv, @function
_ZN2v88internal8compiler12PipelineImpl21ComputeScheduledGraphEv:
.LFB26833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	56(%r12), %rax
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L1144
	leaq	.LC77(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1144:
	movq	48(%r12), %rax
	movq	%rax, -152(%rbp)
	movq	176(%r12), %rax
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L1145
	movq	48(%rax), %rcx
	leaq	.LC77(%rip), %rdx
	movq	%rdx, 48(%rax)
	movq	%rcx, -176(%rbp)
.L1145:
	movq	-152(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	leaq	-112(%rbp), %r14
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%rbx), %r15
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	movq	160(%r15), %rdx
	call	_ZN2v88internal8compiler12GraphTrimmerC1EPNS0_4ZoneEPNS1_5GraphE@PLT
	movq	216(%r15), %rdi
	movq	%r13, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	testq	%rdi, %rdi
	je	.L1146
	leaq	-144(%rbp), %rsi
	call	_ZN2v88internal8compiler7JSGraph14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	movq	-128(%rbp), %r8
	movq	-136(%rbp), %rax
	cmpq	%rax, %r8
	je	.L1146
	movq	%rax, %r11
	movq	%r8, %r10
	.p2align 4,,10
	.p2align 3
.L1147:
	movq	(%r11), %rcx
	movzbl	23(%rcx), %eax
	leaq	32(%rcx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1149
	movq	32(%rcx), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
.L1149:
	testl	%eax, %eax
	jle	.L1150
	cmpq	$0, (%rdx)
	je	.L1151
.L1150:
	movl	-104(%rbp), %eax
	cmpl	%eax, 16(%rcx)
	ja	.L1151
	addl	$1, %eax
	movl	%eax, 16(%rcx)
	movq	-80(%rbp), %r15
	cmpq	-72(%rbp), %r15
	je	.L1152
	movq	%rcx, (%r15)
	addq	$8, -80(%rbp)
.L1151:
	addq	$8, %r11
	cmpq	%r11, %r10
	jne	.L1147
.L1146:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12GraphTrimmer9TrimGraphEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12GraphTrimmerD1Ev@PLT
	movq	-160(%rbp), %rax
	testq	%rax, %rax
	je	.L1163
	movq	-176(%rbp), %rsi
	movq	%rsi, 48(%rax)
.L1163:
	testq	%r13, %r13
	je	.L1164
	movq	-152(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1164:
	movq	-168(%rbp), %rax
	testq	%rax, %rax
	je	.L1165
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1165:
	leaq	.LC77(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	(%rbx), %r13
	movq	56(%r13), %r14
	testq	%r14, %r14
	je	.L1166
	leaq	.LC78(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1166:
	movq	176(%r13), %r15
	movq	48(%r13), %r8
	testq	%r15, %r15
	je	.L1167
	movq	48(%r15), %rax
	movq	%rax, -184(%rbp)
	leaq	.LC78(%rip), %rax
	movq	%rax, 48(%r15)
.L1167:
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%rbx), %rbx
	xorl	%edx, %edx
	movq	%rax, %r13
	movq	24(%rbx), %rax
	movq	160(%rbx), %rsi
	movq	%r13, %rdi
	testb	$16, (%rax)
	leaq	160(%rax), %rcx
	setne	%dl
	addl	%edx, %edx
	call	_ZN2v88internal8compiler9Scheduler15ComputeScheduleEPNS0_4ZoneEPNS1_5GraphENS_4base5FlagsINS2_4FlagEiEEPNS0_11TickCounterE@PLT
	cmpb	$0, _ZN2v88internal17FLAG_turbo_verifyE(%rip)
	movq	-152(%rbp), %r8
	jne	.L1222
.L1169:
	movq	%rax, 232(%rbx)
	testq	%r15, %r15
	je	.L1170
	movq	-184(%rbp), %rax
	movq	%rax, 48(%r15)
.L1170:
	testq	%r13, %r13
	je	.L1171
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1171:
	testq	%r14, %r14
	je	.L1172
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1172:
	movq	232(%r12), %rdx
	movq	24(%r12), %rdi
	leaq	.LC79(%rip), %rcx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_113TraceScheduleEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPNS1_8ScheduleEPKc
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1223
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1152:
	.cfi_restore_state
	movq	-88(%rbp), %rdx
	movq	%r15, %r9
	subq	%rdx, %r9
	movq	%r9, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L1224
	testq	%rax, %rax
	je	.L1174
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1225
	movq	$2147483640, -192(%rbp)
	movl	$2147483640, %esi
.L1154:
	movq	-96(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%r8, %rsi
	ja	.L1226
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1157:
	movq	-192(%rbp), %r8
	leaq	8(%rax), %rsi
	addq	%rax, %r8
.L1155:
	movq	%rcx, (%rax,%r9)
	cmpq	%rdx, %r15
	je	.L1158
	subq	$8, %r15
	leaq	15(%rax), %rcx
	subq	%rdx, %r15
	subq	%rdx, %rcx
	movq	%r15, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rcx
	jbe	.L1177
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rsi
	je	.L1177
	addq	$1, %rsi
	xorl	%ecx, %ecx
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1160:
	movdqu	(%rdx,%rcx), %xmm1
	movups	%xmm1, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %rdi
	jne	.L1160
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rcx
	addq	%rcx, %rdx
	addq	%rax, %rcx
	cmpq	%rdi, %rsi
	je	.L1162
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L1162:
	leaq	16(%rax,%r15), %rsi
.L1158:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%r8, -72(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -88(%rbp)
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1225:
	testq	%rsi, %rsi
	jne	.L1227
	movl	$8, %esi
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1222:
	movq	%rax, %rdi
	movq	%r8, -160(%rbp)
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE@PLT
	movq	-160(%rbp), %r8
	movq	-152(%rbp), %rax
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1174:
	movq	$8, -192(%rbp)
	movl	$8, %esi
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1177:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L1159:
	movq	(%rdx,%rcx,8), %rdi
	movq	%rdi, (%rax,%rcx,8)
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	%rdi, %rsi
	jne	.L1159
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1226:
	movq	%r11, -232(%rbp)
	movq	%r10, -224(%rbp)
	movq	%r9, -216(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdx
	movq	-216(%rbp), %r9
	movq	-224(%rbp), %r10
	movq	-232(%rbp), %r11
	jmp	.L1157
.L1223:
	call	__stack_chk_fail@PLT
.L1224:
	leaq	.LC26(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1227:
	cmpq	$268435455, %rsi
	movl	$268435455, %r8d
	cmova	%r8, %rsi
	leaq	0(,%rsi,8), %rax
	movq	%rax, -192(%rbp)
	movq	%rax, %rsi
	jmp	.L1154
	.cfi_endproc
.LFE26833:
	.size	_ZN2v88internal8compiler12PipelineImpl21ComputeScheduledGraphEv, .-_ZN2v88internal8compiler12PipelineImpl21ComputeScheduledGraphEv
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE.str1.1,"aMS",@progbits,1
.LC80:
	.string	"V8.TFCodeGeneration"
.LC81:
	.string	"V8.TFAssembleCode"
.LC82:
	.string	"{\"name\":\"code generation\""
.LC83:
	.string	", \"type\":\"instructions\""
	.section	.text._ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE
	.type	_ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE, @function
_ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE:
.LFB26865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$648, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1229
	leaq	.LC80(%rip), %rsi
	call	_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc@PLT
.L1229:
	movq	(%r14), %rax
	movq	$0, (%r14)
	movdqu	72(%rbx), %xmm1
	movq	24(%rbx), %r14
	movq	304(%rbx), %rsi
	movq	328(%rbx), %rdx
	movq	272(%rbx), %r8
	movq	(%rbx), %rcx
	movaps	%xmm1, -608(%rbp)
	movq	88(%rbx), %rdi
	movq	%rsi, -680(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdi, -592(%rbp)
	movl	4(%r14), %r9d
	movl	$1344, %edi
	movl	12(%r14), %r10d
	movq	448(%rbx), %r11
	movq	%r8, -664(%rbp)
	movq	%rcx, -656(%rbp)
	movl	%r9d, -644(%rbp)
	movl	%r10d, -640(%rbp)
	movq	%r11, -632(%rbp)
	movq	%rax, -616(%rbp)
	call	_Znwm@PLT
	movl	-644(%rbp), %r9d
	movq	-632(%rbp), %r11
	subq	$8, %rsp
	movl	-640(%rbp), %r10d
	movq	%rax, %r15
	leaq	424(%rbx), %rdi
	leaq	-616(%rbp), %rax
	movq	-656(%rbp), %rcx
	movq	-664(%rbp), %r8
	pushq	%rax
	movq	-672(%rbp), %rdx
	movq	-680(%rbp), %rsi
	pushq	%r11
	pushq	%r10
	pushq	%rdi
	pushq	%r9
	movq	%r14, %r9
	pushq	416(%rbx)
	movl	68(%rbx), %edi
	movq	%rax, -688(%rbp)
	pushq	%rdi
	movq	%r15, %rdi
	pushq	-592(%rbp)
	pushq	-600(%rbp)
	pushq	-608(%rbp)
	pushq	%rcx
	movq	%r13, %rcx
	call	_ZN2v88internal8compiler13CodeGeneratorC1EPNS0_4ZoneEPNS1_5FrameEPNS1_7LinkageEPNS1_19InstructionSequenceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateENS_4base8OptionalINS1_9OsrHelperEEEiPNS0_20JumpOptimizationInfoENS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsEimSt10unique_ptrINS0_15AssemblerBufferESt14default_deleteISQ_EE@PLT
	movq	%r15, 104(%rbx)
	movq	-616(%rbp), %rdi
	addq	$96, %rsp
	testq	%rdi, %rdi
	je	.L1230
	movq	(%rdi), %rax
	call	*8(%rax)
.L1230:
	movq	(%r12), %r13
	movq	56(%r13), %r15
	testq	%r15, %r15
	je	.L1231
	leaq	.LC81(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1231:
	movq	176(%r13), %r14
	movq	48(%r13), %r9
	testq	%r14, %r14
	je	.L1232
	leaq	.LC81(%rip), %rax
	movq	48(%r14), %rdx
	movq	%r9, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, 48(%r14)
	movq	%rdx, -640(%rbp)
	movq	%r9, -632(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%rax, %r13
	movq	(%r12), %rax
	movq	104(%rax), %rdi
	call	_ZN2v88internal8compiler13CodeGenerator12AssembleCodeEv@PLT
	movq	-640(%rbp), %rdx
	movq	-632(%rbp), %r9
	movq	%rdx, 48(%r14)
.L1239:
	testq	%r13, %r13
	je	.L1233
	movq	%r13, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1233:
	testq	%r15, %r15
	je	.L1234
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1234:
	movq	24(%rbx), %rsi
	testl	$16384, (%rsi)
	jne	.L1266
.L1235:
	cmpq	$0, 264(%rbx)
	je	.L1236
	movq	256(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1237
	movq	248(%rbx), %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1237:
	movq	$0, 272(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 256(%rbx)
.L1236:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1228
	call	_ZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEv@PLT
.L1228:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1267
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1266:
	.cfi_restore_state
	leaq	-576(%rbp), %r12
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode@PLT
	movq	%r12, %rdi
	movl	$25, %edx
	leaq	.LC82(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$23, %edx
	movq	%r12, %rdi
	leaq	.LC83(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	104(%rbx), %rax
	movq	-688(%rbp), %rsi
	movq	%r12, %rdi
	addq	$1312, %rax
	movq	%rax, -616(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_23InstructionStartsAsJSONE
	leaq	-608(%rbp), %rsi
	movq	%rax, %rdi
	movq	104(%rbx), %rax
	addq	$1280, %rax
	movq	%rax, -608(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_31TurbolizerCodeOffsetsInfoAsJSONE
	movq	%r12, %rdi
	movl	$3, %edx
	leaq	.LC67(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileD1Ev@PLT
	jmp	.L1235
	.p2align 4,,10
	.p2align 3
.L1232:
	movq	%r9, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r9, -632(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%rax, %r13
	movq	(%r12), %rax
	movq	104(%rax), %rdi
	call	_ZN2v88internal8compiler13CodeGenerator12AssembleCodeEv@PLT
	movq	-632(%rbp), %r9
	jmp	.L1239
.L1267:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26865:
	.size	_ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE, .-_ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb.str1.1,"aMS",@progbits,1
.LC84:
	.string	"V8.TFFinalizeCode"
.LC85:
	.string	"vector::reserve"
.LC86:
	.string	"INLINE ("
.LC87:
	.string	"} AS "
.LC88:
	.string	" AT "
.LC89:
	.string	"<?>"
.LC90:
	.string	"<"
.LC91:
	.string	">"
.LC92:
	.string	"--- Raw source ---\n"
.LC93:
	.string	"\n\n"
.LC94:
	.string	"--- Optimized code ---\n"
.LC95:
	.string	"optimization_id = "
.LC96:
	.string	"\n"
.LC97:
	.string	"--- Code ---\n"
.LC98:
	.string	"source_position = "
.LC99:
	.string	"--- End code ---\n"
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb.str1.8,"aMS",@progbits,1
	.align 8
.LC100:
	.string	"{\"name\":\"disassembly\",\"type\":\"disassembly\""
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb.str1.1
.LC101:
	.string	"\"data\":\""
.LC102:
	.string	"\"}\n],\n"
.LC103:
	.string	"\"nodePositions\":"
.LC104:
	.string	",\n"
.LC105:
	.string	"\n}"
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb.str1.8
	.align 8
.LC106:
	.string	"---------------------------------------------------\n"
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb.str1.1
.LC107:
	.string	"Finished compiling method "
.LC108:
	.string	" using TurboFan"
	.section	.text._ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb
	.type	_ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb, @function
_ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb:
.LFB26866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1176, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -1104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	320(%rax), %rdi
	movq	%rax, -1096(%rbp)
	testq	%rdi, %rdi
	je	.L1375
	testb	%sil, %sil
	jne	.L1442
.L1375:
	movq	-1096(%rbp), %rbx
.L1269:
	movq	56(%rbx), %r13
	testq	%r13, %r13
	je	.L1270
	leaq	.LC84(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1270:
	movq	48(%rbx), %r14
	movq	176(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1271
	leaq	.LC84(%rip), %rax
	movq	48(%rbx), %rdx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, 48(%rbx)
	movq	%rdx, -1120(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%rax, %r12
	movq	-1104(%rbp), %rax
	movq	(%rax), %r15
	movq	104(%r15), %rdi
	call	_ZN2v88internal8compiler13CodeGenerator12FinalizeCodeEv@PLT
	movq	-1120(%rbp), %rdx
	movq	%rax, 96(%r15)
	movq	%rdx, 48(%rbx)
.L1371:
	testq	%r12, %r12
	je	.L1272
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1272:
	testq	%r13, %r13
	je	.L1273
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1273:
	movq	-1096(%rbp), %rax
	movq	96(%rax), %rax
	movq	%rax, -1128(%rbp)
	testq	%rax, %rax
	je	.L1275
	movq	-1096(%rbp), %rax
	cmpq	$0, 376(%rax)
	je	.L1276
	leaq	24+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %r15
	movq	%rcx, %xmm0
	movq	%rcx, %xmm2
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rcx
	movq	%rcx, -1120(%rbp)
	movq	%r15, %xmm5
	movdqa	%xmm0, %xmm1
	leaq	-464(%rbp), %r13
	punpcklqdq	%xmm5, %xmm2
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r14
	movaps	%xmm2, -1152(%rbp)
	leaq	-576(%rbp), %r12
	movhps	-1120(%rbp), %xmm1
	movaps	%xmm1, -1120(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%r9d, %r9d
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movw	%r9w, -240(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -232(%rbp)
	movq	%rbx, -576(%rbp)
	movups	%xmm0, -216(%rbp)
	movq	%r14, -464(%rbp)
	movq	$0, -248(%rbp)
	movq	-24(%rbx), %rax
	movq	%rcx, -576(%rbp,%rax)
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	-512(%rbp), %r8
	pxor	%xmm0, %xmm0
	movdqa	-1120(%rbp), %xmm1
	movq	%r8, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r8, -1120(%rbp)
	movq	%rax, -464(%rbp)
	movaps	%xmm1, -576(%rbp)
	movaps	%xmm0, -560(%rbp)
	movaps	%xmm0, -544(%rbp)
	movaps	%xmm0, -528(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	%r15, -568(%rbp)
	movq	%r13, %rdi
	leaq	-480(%rbp), %r15
	leaq	-568(%rbp), %rsi
	movl	$16, -504(%rbp)
	movq	%r15, -496(%rbp)
	movq	$0, -488(%rbp)
	movb	$0, -480(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-1128(%rbp), %rcx
	xorl	%esi, %esi
	movq	(%rcx), %rdx
	leaq	-1056(%rbp), %rcx
	movq	%rcx, %rdi
	xorl	%ecx, %ecx
	movq	%rdx, -1056(%rbp)
	movq	%r12, %rdx
	call	_ZN2v88internal4Code11DisassembleEPKcRSom@PLT
	movq	-1096(%rbp), %rax
	movq	%r12, %rsi
	movq	376(%rax), %rdi
	call	_ZN2v88internal18BasicBlockProfiler4Data7SetCodeEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE@PLT
	movq	-496(%rbp), %rdi
	movdqa	-1152(%rbp), %xmm2
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -464(%rbp)
	movq	-1120(%rbp), %r8
	cmpq	%r15, %rdi
	movaps	%xmm2, -576(%rbp)
	je	.L1277
	call	_ZdlPv@PLT
	movq	-1120(%rbp), %r8
.L1277:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r8, %rdi
	movq	%rax, -568(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%rbx, -576(%rbp)
	movq	-24(%rbx), %rax
	movq	%r13, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -576(%rbp,%rax)
	movq	%r14, -464(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
.L1276:
	movq	-1104(%rbp), %rcx
	movq	-1128(%rbp), %rdx
	cmpb	$0, _ZN2v88internal21FLAG_print_opt_sourceE(%rip)
	movq	(%rcx), %rax
	movq	24(%rax), %rax
	movq	%rdx, 40(%rax)
	movq	(%rcx), %rax
	movq	24(%rax), %r13
	movq	(%rax), %rax
	movq	%rax, -1168(%rbp)
	je	.L1278
	movl	8(%r13), %eax
	movl	%eax, -1120(%rbp)
	testl	%eax, %eax
	je	.L1443
	cmpb	$0, _ZN2v88internal15FLAG_print_codeE(%rip)
	jne	.L1440
	movl	0(%r13), %eax
	testb	$64, %ah
	jne	.L1444
	.p2align 4,,10
	.p2align 3
.L1347:
	testb	$-64, %ah
	je	.L1357
	movq	-1096(%rbp), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1445
	call	_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv@PLT
	movq	%rax, %rbx
.L1359:
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%rbx), %rsi
	je	.L1360
	testq	%rsi, %rsi
	je	.L1446
.L1361:
	addl	$1, 152(%rbx)
.L1360:
	leaq	-976(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movl	$52, %edx
	leaq	.LC106(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$26, %edx
	leaq	.LC107(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1104(%rbp), %rax
	leaq	-1064(%rbp), %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rsi
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movq	-1064(%rbp), %r12
	testq	%r12, %r12
	je	.L1447
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1363:
	movl	$15, %edx
	leaq	.LC108(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-976(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-736(%rbp,%rax), %r12
	testq	%r12, %r12
	je	.L1364
	cmpb	$0, 56(%r12)
	je	.L1365
	movsbl	67(%r12), %esi
.L1366:
	movq	%r14, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-1064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1367
	call	_ZdaPv@PLT
.L1367:
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, -896(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-912(%rbp), %rdi
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -976(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-896(%rbp), %rdi
	movq	%rax, -976(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -896(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L1357
	subl	$1, 152(%rbx)
	je	.L1448
.L1357:
	movq	-1128(%rbp), %rax
.L1275:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1449
	addq	$1176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1312:
	.cfi_restore_state
	movq	-1032(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1297
	call	_ZdlPv@PLT
.L1297:
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1278
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1278:
	cmpb	$0, _ZN2v88internal15FLAG_print_codeE(%rip)
	jne	.L1440
	movl	8(%r13), %r8d
	testl	%r8d, %r8d
	jne	.L1346
	cmpb	$0, _ZN2v88internal19FLAG_print_opt_codeE(%rip)
	jne	.L1316
.L1346:
	movq	-1104(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %r13
.L1456:
	movl	0(%r13), %eax
	testb	$64, %ah
	je	.L1347
.L1444:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	-576(%rbp), %r12
	movq	%r13, %rsi
	movq	.LC74(%rip), %xmm1
	movq	%rax, -1120(%rbp)
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	-848(%rbp), %r13
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r14
	leaq	-976(%rbp), %rbx
	leaq	-960(%rbp), %r15
	movhps	-1120(%rbp), %xmm1
	movaps	%xmm1, -1168(%rbp)
	call	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode@PLT
	movl	$42, %edx
	leaq	.LC100(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1096(%rbp), %rax
	movq	%r12, %rdi
	movq	104(%rax), %rax
	addq	$1248, %rax
	movq	%rax, -1056(%rbp)
	leaq	-1056(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -1152(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_17BlockStartsAsJSONE
	movl	$8, %edx
	leaq	.LC101(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movw	%ax, -624(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -616(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -600(%rbp)
	movq	%rax, -976(%rbp)
	movq	%r14, -848(%rbp)
	movq	$0, -632(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -976(%rbp,%rax)
	movq	-976(%rbp), %rax
	movq	$0, -968(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rax, -960(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -960(%rbp,%rax)
	movq	-960(%rbp), %rax
	movq	-24(%rax), %rcx
	addq	%r15, %rcx
	movq	%rcx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-1168(%rbp), %xmm1
	movq	%rax, -976(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -976(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -976(%rbp)
	addq	$80, %rax
	movq	%rax, -848(%rbp)
	leaq	-896(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -960(%rbp)
	movaps	%xmm0, -944(%rbp)
	movaps	%xmm0, -928(%rbp)
	movaps	%xmm0, -912(%rbp)
	movq	%rax, -1136(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-952(%rbp), %rsi
	movq	%rax, -952(%rbp)
	leaq	-864(%rbp), %rax
	movq	%rax, -1176(%rbp)
	movq	%rax, -880(%rbp)
	movl	$24, -888(%rbp)
	movq	$0, -872(%rbp)
	movb	$0, -864(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-1128(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	-1152(%rbp), %rdi
	movq	%r15, %rdx
	movq	(%rax), %rax
	movq	%rax, -1056(%rbp)
	call	_ZN2v88internal4Code11DisassembleEPKcRSom@PLT
	leaq	-992(%rbp), %rax
	movq	$0, -1000(%rbp)
	leaq	-1008(%rbp), %rdi
	movq	%rax, -1168(%rbp)
	movq	%rax, -1008(%rbp)
	movq	-912(%rbp), %rax
	movb	$0, -992(%rbp)
	testq	%rax, %rax
	je	.L1348
	movq	-928(%rbp), %r8
	movq	-920(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1450
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1350:
	movq	-1008(%rbp), %rdx
	movq	-1000(%rbp), %rbx
	addq	%rdx, %rbx
	cmpq	%rdx, %rbx
	je	.L1355
	movq	-1152(%rbp), %r15
	movq	%r13, -1184(%rbp)
	movq	%rdx, %r13
	.p2align 4,,10
	.p2align 3
.L1354:
	movsbw	0(%r13), %ax
	movq	%r15, %rsi
	movq	%r12, %rdi
	addq	$1, %r13
	movw	%ax, -1056(%rbp)
	call	_ZN2v88internallsERSoRKNS0_20AsEscapedUC16ForJSONE@PLT
	cmpq	%r13, %rbx
	jne	.L1354
	movq	-1184(%rbp), %r13
.L1355:
	movl	$6, %edx
	leaq	.LC102(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$16, %edx
	leaq	.LC103(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1096(%rbp), %rbx
	movq	%r12, %rdi
	movq	392(%rbx), %rdx
	movq	384(%rbx), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC104(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1104(%rbp), %rax
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler31JsonPrintAllSourceWithPositionsERSoPNS0_24OptimizedCompilationInfoEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movl	$2, %edx
	leaq	.LC105(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1008(%rbp), %rdi
	cmpq	-1168(%rbp), %rdi
	je	.L1353
	call	_ZdlPv@PLT
.L1353:
	movq	.LC74(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-880(%rbp), %rdi
	movq	%rax, -976(%rbp)
	addq	$80, %rax
	movhps	.LC76(%rip), %xmm0
	movq	%rax, -848(%rbp)
	movaps	%xmm0, -960(%rbp)
	cmpq	-1176(%rbp), %rdi
	je	.L1356
	call	_ZdlPv@PLT
.L1356:
	movq	-1120(%rbp), %rax
	movq	-1136(%rbp), %rdi
	movq	%rax, -952(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, %rdi
	movq	%rax, -976(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -976(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -960(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -960(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -976(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -976(%rbp,%rax)
	movq	$0, -968(%rbp)
	movq	%r14, -848(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileD1Ev@PLT
	movq	-1104(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rax
	movl	(%rax), %eax
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1442:
	call	_ZN2v88internal8compiler12JSHeapBroker6RetireEv@PLT
	movq	-1104(%rbp), %rax
	movq	(%rax), %rbx
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1440:
	leaq	-1056(%rbp), %rax
	movq	%rax, -1152(%rbp)
.L1314:
	movq	%r13, %rsi
	leaq	-1072(%rbp), %rdi
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movq	-1168(%rbp), %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%rax), %rsi
	movq	%rax, %rbx
	je	.L1319
	testq	%rsi, %rsi
	je	.L1451
.L1321:
	addl	$1, 152(%rbx)
.L1319:
	leaq	-576(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movq	-1128(%rbp), %rax
	movq	(%rax), %rax
	movl	43(%rax), %r15d
	shrl	%r15d
	andl	$31, %r15d
	je	.L1452
.L1322:
	movl	8(%r13), %edx
	testl	%edx, %edx
	je	.L1453
	movl	$13, %edx
	leaq	.LC97(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testl	%r15d, %r15d
	je	.L1454
.L1342:
	movq	-1128(%rbp), %rax
	movq	-1072(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	-1152(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -1056(%rbp)
	call	_ZN2v88internal4Code11DisassembleEPKcRSom@PLT
	movl	$17, %edx
	leaq	.LC99(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, -496(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-512(%rbp), %rdi
	movq	%rax, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -576(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-496(%rbp), %rdi
	movq	%rax, -576(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -496(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L1344
	subl	$1, 152(%rbx)
	je	.L1455
.L1344:
	movq	-1072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1346
	call	_ZdaPv@PLT
	movq	-1104(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %r13
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1365:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1366
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1271:
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%rax, %r12
	movq	-1104(%rbp), %rax
	movq	(%rax), %rbx
	movq	104(%rbx), %rdi
	call	_ZN2v88internal8compiler13CodeGenerator12FinalizeCodeEv@PLT
	movq	%rax, 96(%rbx)
	jmp	.L1371
	.p2align 4,,10
	.p2align 3
.L1453:
	movl	$23, %edx
	leaq	.LC94(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$18, %edx
	leaq	.LC95(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	120(%r13), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC96(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testl	%r15d, %r15d
	jne	.L1342
.L1454:
	movq	24(%r13), %r13
	movl	$18, %edx
	leaq	.LC98(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1152(%rbp), %rdi
	movq	0(%r13), %rax
	movq	%rax, -1056(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC96(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1450:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1316:
	movq	24(%r13), %rax
	movq	_ZN2v88internal26FLAG_print_opt_code_filterE(%rip), %rsi
	movq	(%rax), %rax
	movq	%rax, -1056(%rbp)
	leaq	-1056(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1152(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo12PassesFilterEPKc@PLT
	testb	%al, %al
	jne	.L1314
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	(%rax), %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	movq	%rax, %rbx
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1447:
	movq	-976(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L1443:
	movq	104(%r13), %rbx
	subq	96(%r13), %rbx
	pxor	%xmm0, %xmm0
	movabsq	$1152921504606846975, %rax
	sarq	$5, %rbx
	movaps	%xmm0, -1056(%rbp)
	movaps	%xmm0, -1040(%rbp)
	movaps	%xmm0, -1024(%rbp)
	cmpq	%rax, %rbx
	ja	.L1457
	testq	%rbx, %rbx
	jne	.L1458
.L1282:
	movq	24(%r13), %rcx
	movq	-1168(%rbp), %rsi
	movl	$-1, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_119PrintFunctionSourceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEiNS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	96(%r13), %rdx
	cmpq	%rdx, 104(%r13)
	je	.L1312
	leaq	-1056(%rbp), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	xorl	%ebx, %ebx
	movq	%rax, -1152(%rbp)
	leaq	-1080(%rbp), %rax
	movq	%rcx, %xmm7
	leaq	-576(%rbp), %r12
	movq	%rax, -1184(%rbp)
	leaq	-1064(%rbp), %rax
	movq	%rax, -1176(%rbp)
	leaq	-1072(%rbp), %rax
	movq	%rax, -1192(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm6
	punpcklqdq	%xmm6, %xmm7
	movaps	%xmm7, -1216(%rbp)
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1462:
	movsbl	67(%r15), %esi
.L1308:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movdqa	-1216(%rbp), %xmm3
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	-512(%rbp), %rdi
	movq	%rax, -496(%rbp)
	movaps	%xmm3, -576(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-496(%rbp), %rdi
	movq	%rax, -576(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -496(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L1310
	subl	$1, 152(%r14)
	je	.L1459
.L1310:
	movq	96(%r13), %rdx
	movq	104(%r13), %rax
	addl	$1, -1120(%rbp)
	movl	-1120(%rbp), %ebx
	subq	%rdx, %rax
	sarq	$5, %rax
	cmpq	%rax, %rbx
	jnb	.L1312
.L1295:
	salq	$5, %rbx
	movq	-1152(%rbp), %rdi
	movq	(%rdx,%rbx), %rsi
	call	_ZN2v88internal8compiler16SourceIdAssigner8GetIdForENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	-1168(%rbp), %r14
	movq	%r13, %rdi
	movl	%eax, %r15d
	movq	96(%r13), %rax
	movq	%r14, %rsi
	movl	%r15d, %edx
	movq	(%rax,%rbx), %rcx
	call	_ZN2v88internal8compiler12_GLOBAL__N_119PrintFunctionSourceEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEiNS0_6HandleINS0_18SharedFunctionInfoEEE
	movq	%r14, %rdi
	addq	96(%r13), %rbx
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%rax), %rsi
	movq	%rax, %r14
	je	.L1299
	testq	%rsi, %rsi
	je	.L1460
.L1300:
	addl	$1, 152(%r14)
.L1299:
	movq	%r12, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movl	$8, %edx
	leaq	.LC86(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movq	-1184(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -1080(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	-1192(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	-1176(%rbp), %rdi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -1072(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-1064(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L1461
	movq	%rsi, %rdi
	movq	%rsi, -1136(%rbp)
	call	strlen@PLT
	movq	-1136(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1302:
	movl	$5, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	120(%r13), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -1136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1136(%rbp), %rdi
	movl	%r15d, %esi
	call	_ZNSolsEi@PLT
	movl	$5, %edx
	leaq	.LC87(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-1120(%rbp), %esi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	movl	$4, %edx
	leaq	.LC88(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1303
	call	_ZdaPv@PLT
.L1303:
	movq	16(%rbx), %rax
	movq	%rax, %rbx
	movq	%rax, %r15
	shrq	%rbx
	shrq	$31, %r15
	andl	$1073741823, %ebx
	movzwl	%r15w, %r15d
	testb	$1, %al
	jne	.L1304
	movl	%ebx, %eax
	orl	%r15d, %eax
	jne	.L1304
	movl	$3, %edx
	leaq	.LC89(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L1305:
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-336(%rbp,%rax), %r15
	testq	%r15, %r15
	je	.L1364
	cmpb	$0, 56(%r15)
	jne	.L1462
	movq	%r15, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1308
	movq	%r15, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1304:
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC90(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leal	-1(%r15), %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leal	-1(%rbx), %esi
	movq	%r15, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC91(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1461:
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1459:
	movq	144(%r14), %rdi
	call	fclose@PLT
	movq	$0, 144(%r14)
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1448:
	movq	144(%rbx), %rdi
	call	fclose@PLT
	movq	$0, 144(%rbx)
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1460:
	movq	(%rax), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%r14)
	movq	%rax, %rsi
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1452:
	movq	24(%r13), %r8
	movq	(%r8), %rdx
	movq	31(%rdx), %rax
	testb	$1, %al
	je	.L1322
	movq	-1(%rax), %rsi
	leaq	-1(%rax), %rcx
	cmpw	$86, 11(%rsi)
	je	.L1463
.L1324:
	movq	(%rcx), %rax
	cmpw	$96, 11(%rax)
	jne	.L1322
	movq	-1152(%rbp), %r14
	movq	%r8, -1120(%rbp)
	movq	%rdx, -1056(%rbp)
	movq	%r14, %rdi
	call	_ZNK2v88internal18SharedFunctionInfo6scriptEv
	movq	-1168(%rbp), %rcx
	movq	88(%rcx), %rcx
	cmpq	%rcx, 7(%rax)
	je	.L1322
	leaq	.LC92(%rip), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	-1120(%rbp), %r8
	movq	%r14, %rdi
	leaq	-976(%rbp), %r14
	movq	(%r8), %rax
	movq	%rax, -1056(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	-1120(%rbp), %r8
	movl	%eax, -1168(%rbp)
	movq	(%r8), %rax
	movq	%rax, -1064(%rbp)
	leaq	-1064(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1176(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo6scriptEv
	leaq	-976(%rbp), %rdi
	movl	$32, %ecx
	movl	-1168(%rbp), %edx
	movq	7(%rax), %rsi
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	rep stosq
	movq	%r14, %rdi
	movl	%edx, -1080(%rbp)
	movq	$0, -720(%rbp)
	movb	$0, -696(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_ZN2v88internal6String9VisitFlatINS0_21StringCharacterStreamEEENS0_10ConsStringEPT_S1_i
	movl	-1080(%rbp), %edx
	movl	$0, -712(%rbp)
	testq	%rax, %rax
	movq	-1120(%rbp), %r8
	movq	%rax, %rsi
	je	.L1327
	movq	%r14, %rdi
	call	_ZN2v88internal18ConsStringIterator10InitializeENS0_10ConsStringEi@PLT
	movl	-712(%rbp), %edi
	movl	$0, -1080(%rbp)
	movq	-1120(%rbp), %r8
	testl	%edi, %edi
	jne	.L1464
.L1327:
	movq	(%r8), %rax
	movq	-1152(%rbp), %rdi
	movq	%r8, -1168(%rbp)
	movq	%rax, -1056(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	movq	-1168(%rbp), %r8
	movq	-1176(%rbp), %rdi
	movl	%eax, -1120(%rbp)
	movq	(%r8), %rax
	movq	%rax, -1064(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movl	-1120(%rbp), %ecx
	subl	%eax, %ecx
	leal	1(%rcx), %ecx
	movl	%ecx, %edx
	movl	$0, %ecx
	js	.L1338
	movq	%rbx, -1120(%rbp)
	movl	%edx, %ebx
	movl	%r15d, -1168(%rbp)
	movq	-1152(%rbp), %r15
	movq	%r13, -1136(%rbp)
	movl	%ecx, %r13d
	jmp	.L1339
	.p2align 4,,10
	.p2align 3
.L1330:
	cmpb	$0, -696(%rbp)
	je	.L1336
	leaq	1(%rax), %rdx
	movq	%rdx, -688(%rbp)
	movzbl	(%rax), %eax
.L1337:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movw	%ax, -1056(%rbp)
	call	_ZN2v88internallsERSoRKNS0_23AsReversiblyEscapedUC16E@PLT
.L1333:
	addl	$1, %r13d
	cmpl	%ebx, %r13d
	je	.L1465
.L1339:
	movq	-688(%rbp), %rax
	cmpq	-680(%rbp), %rax
	jne	.L1330
	movl	$0, -1056(%rbp)
	movl	-712(%rbp), %esi
	testl	%esi, %esi
	je	.L1333
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1333
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal6String9VisitFlatINS0_21StringCharacterStreamEEENS0_10ConsStringEPT_S1_i
	movq	-688(%rbp), %rax
	cmpq	-680(%rbp), %rax
	jne	.L1330
	movl	$0, -1056(%rbp)
	movl	-712(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1330
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1441
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal6String9VisitFlatINS0_21StringCharacterStreamEEENS0_10ConsStringEPT_S1_i
.L1441:
	movq	-688(%rbp), %rax
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1446:
	movq	(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %rsi
	jmp	.L1361
	.p2align 4,,10
	.p2align 3
.L1455:
	movq	144(%rbx), %rdi
	call	fclose@PLT
	movq	$0, 144(%rbx)
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	(%rax), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %rsi
	jmp	.L1321
	.p2align 4,,10
	.p2align 3
.L1348:
	leaq	-880(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1458:
	leaq	0(,%rbx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-1048(%rbp), %rdx
	movq	-1056(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%rdi, %rdx
	je	.L1290
	leaq	-8(%rdx), %rax
	leaq	15(%rdi), %rcx
	subq	%rdi, %rax
	subq	%r12, %rcx
	shrq	$3, %rax
	cmpq	$30, %rcx
	jbe	.L1376
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rax
	je	.L1376
	addq	$1, %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1288:
	movdqu	(%rdi,%rdx), %xmm7
	movups	%xmm7, (%r12,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L1288
	movq	%rax, %rsi
	andq	$-2, %rsi
	leaq	0(,%rsi,8), %rdx
	leaq	(%rdi,%rdx), %rcx
	addq	%r12, %rdx
	cmpq	%rax, %rsi
	je	.L1284
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
.L1284:
	call	_ZdlPv@PLT
.L1285:
	movq	-1032(%rbp), %rdx
	movq	-1016(%rbp), %rax
	movq	%r12, %xmm0
	addq	%r14, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, -1040(%rbp)
	subq	%rdx, %rax
	movaps	%xmm0, -1056(%rbp)
	sarq	$2, %rax
	cmpq	%rbx, %rax
	jnb	.L1282
	movq	-1024(%rbp), %r12
	salq	$2, %rbx
	movq	%rbx, %rdi
	subq	%rdx, %r12
	call	_Znwm@PLT
	movq	-1032(%rbp), %r15
	movq	-1024(%rbp), %rdx
	movq	%rax, %r14
	subq	%r15, %rdx
	testq	%rdx, %rdx
	jg	.L1466
	testq	%r15, %r15
	jne	.L1293
.L1294:
	addq	%r14, %r12
	addq	%r14, %rbx
	movq	%r14, -1032(%rbp)
	movq	%r12, -1024(%rbp)
	movq	%rbx, -1016(%rbp)
	jmp	.L1282
.L1466:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	memmove@PLT
.L1293:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L1294
.L1376:
	movq	%r12, %rcx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L1286:
	movq	(%rax), %rsi
	addq	$8, %rax
	addq	$8, %rcx
	movq	%rsi, -8(%rcx)
	cmpq	%rax, %rdx
	jne	.L1286
.L1290:
	testq	%rdi, %rdi
	je	.L1285
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1336:
	leaq	2(%rax), %rdx
	movq	%rdx, -688(%rbp)
	movzwl	(%rax), %eax
	jmp	.L1337
.L1463:
	movq	23(%rax), %rcx
	testb	$1, %cl
	je	.L1322
	subq	$1, %rcx
	jmp	.L1324
.L1465:
	movq	-1120(%rbp), %rbx
	movl	-1168(%rbp), %r15d
	movq	-1136(%rbp), %r13
.L1338:
	leaq	.LC93(%rip), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	jmp	.L1322
.L1464:
	leaq	-1080(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	movq	-1120(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L1327
	movl	-1080(%rbp), %edx
	movq	%r14, %rdi
	call	_ZN2v88internal6String9VisitFlatINS0_21StringCharacterStreamEEENS0_10ConsStringEPT_S1_i
	movq	-1120(%rbp), %r8
	jmp	.L1327
.L1364:
	call	_ZSt16__throw_bad_castv@PLT
.L1449:
	call	__stack_chk_fail@PLT
.L1457:
	leaq	.LC85(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE26866:
	.size	_ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb, .-_ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb
	.section	.rodata._ZN2v88internal8compiler22PipelineCompilationJob15FinalizeJobImplEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC109:
	.string	"v8.optimizingCompile.finalize"
	.section	.text._ZN2v88internal8compiler22PipelineCompilationJob15FinalizeJobImplEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22PipelineCompilationJob15FinalizeJobImplEPNS0_7IsolateE
	.type	_ZN2v88internal8compiler22PipelineCompilationJob15FinalizeJobImplEPNS0_7IsolateE, @function
_ZN2v88internal8compiler22PipelineCompilationJob15FinalizeJobImplEPNS0_7IsolateE:
.LFB26466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal8compiler22PipelineCompilationJob15FinalizeJobImplEPNS0_7IsolateEE29trace_event_unique_atomic1022(%rip), %rax
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1502
.L1469:
	movq	$0, -96(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1503
.L1471:
	leaq	832(%r12), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1504
	movq	832(%r12), %rax
	movq	312(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1479
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler23CompilationDependencies6CommitENS0_6HandleINS0_4CodeEEE@PLT
	testb	%al, %al
	jne	.L1479
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal23OptimizedCompilationJob17RetryOptimizationENS0_13BailoutReasonE@PLT
	movl	%eax, %r12d
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1479:
	movq	24(%r12), %rax
	movq	%r13, 40(%rax)
	movq	24(%r12), %rdi
	call	_ZNK2v88internal24OptimizedCompilationInfo14native_contextEv@PLT
	leaq	-104(%rbp), %rdi
	movq	%rax, -104(%rbp)
	movq	0(%r13), %rsi
	call	_ZN2v88internal13NativeContext16AddOptimizedCodeENS0_4CodeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler22PipelineCompilationJob34RegisterWeakObjectsInOptimizedCodeENS0_6HandleINS0_4CodeEEEPNS0_7IsolateE
	xorl	%r12d, %r12d
.L1478:
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1505
	leaq	-32(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1503:
	.cfi_restore_state
	movq	24(%r12), %rax
	leaq	-120(%rbp), %rdi
	leaq	-128(%rbp), %rsi
	xorl	%ebx, %ebx
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo10TraceIDRefEv@PLT
	leaq	.LC63(%rip), %rax
	movb	$8, -129(%rbp)
	movq	%rax, -112(%rbp)
	movq	-120(%rbp), %rax
	movq	$0, -56(%rbp)
	movq	$0, -120(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1506
.L1472:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1473
	movq	(%rdi), %rax
	call	*8(%rax)
.L1473:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1474
	movq	(%rdi), %rax
	call	*8(%rax)
.L1474:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1475
	movq	(%rdi), %rax
	call	*8(%rax)
.L1475:
	leaq	.LC109(%rip), %rax
	movq	%r13, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%rbx, -72(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1502:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1507
.L1470:
	movq	%r13, _ZZN2v88internal8compiler22PipelineCompilationJob15FinalizeJobImplEPNS0_7IsolateEE29trace_event_unique_atomic1022(%rip)
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1504:
	movq	24(%r12), %rax
	cmpb	$0, 88(%rax)
	je	.L1477
	movl	$1, %r12d
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1477:
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN2v88internal23OptimizedCompilationJob17AbortOptimizationENS0_13BailoutReasonE@PLT
	movl	%eax, %r12d
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1507:
	leaq	.LC55(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1470
	.p2align 4,,10
	.p2align 3
.L1506:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$772
	leaq	.LC109(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	leaq	-104(%rbp), %rdx
	pushq	%rdx
	leaq	-129(%rbp), %rdx
	pushq	%rdx
	leaq	-112(%rbp), %rdx
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$1
	pushq	%r12
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L1472
.L1505:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26466:
	.size	_ZN2v88internal8compiler22PipelineCompilationJob15FinalizeJobImplEPNS0_7IsolateE, .-_ZN2v88internal8compiler22PipelineCompilationJob15FinalizeJobImplEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal8compiler26WasmHeapStubCompilationJob15FinalizeJobImplEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC110:
	.string	"WasmHeapStubCompilationJob::FinalizeJobImpl"
	.section	.text._ZN2v88internal8compiler26WasmHeapStubCompilationJob15FinalizeJobImplEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler26WasmHeapStubCompilationJob15FinalizeJobImplEPNS0_7IsolateE
	.type	_ZN2v88internal8compiler26WasmHeapStubCompilationJob15FinalizeJobImplEPNS0_7IsolateE, @function
_ZN2v88internal8compiler26WasmHeapStubCompilationJob15FinalizeJobImplEPNS0_7IsolateE:
.LFB26511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$368, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 240(%rdi)
	setne	%sil
	addq	$792, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb
	testq	%rax, %rax
	je	.L1530
	movq	%rax, %r12
	movq	792(%rbx), %rax
	movq	312(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1510
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23CompilationDependencies6CommitENS0_6HandleINS0_4CodeEEE@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testb	%r8b, %r8b
	je	.L1508
.L1510:
	cmpb	$0, _ZN2v88internal19FLAG_print_opt_codeE(%rip)
	movq	%r12, 112(%rbx)
	jne	.L1512
.L1519:
	xorl	%eax, %eax
.L1508:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1531
	addq	$368, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1512:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%rax), %rsi
	movq	%rax, %r13
	je	.L1515
	testq	%rsi, %rsi
	je	.L1532
.L1516:
	addl	$1, 152(%r13)
.L1515:
	leaq	-384(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movq	(%r12), %rax
	movq	24(%rbx), %rsi
	leaq	-392(%rbp), %rdi
	movq	%rax, -400(%rbp)
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movq	-392(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	leaq	-400(%rbp), %rdi
	call	_ZN2v88internal4Code11DisassembleEPKcRSom@PLT
	movq	-392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1517
	call	_ZdaPv@PLT
.L1517:
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rax, -304(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdx, %xmm0
	leaq	-320(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-304(%rbp), %rdi
	movq	%rax, -384(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -304(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L1519
	subl	$1, 152(%r13)
	jne	.L1519
	movq	144(%r13), %rdi
	call	fclose@PLT
	movq	$0, 144(%r13)
	jmp	.L1519
	.p2align 4,,10
	.p2align 3
.L1530:
	xorl	%edx, %edx
	leaq	.LC110(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
	.p2align 4,,10
	.p2align 3
.L1532:
	movq	(%rax), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%r13)
	movq	%rax, %rsi
	jmp	.L1516
.L1531:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26511:
	.size	_ZN2v88internal8compiler26WasmHeapStubCompilationJob15FinalizeJobImplEPNS0_7IsolateE, .-_ZN2v88internal8compiler26WasmHeapStubCompilationJob15FinalizeJobImplEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_113TraceSequenceEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPKc.str1.1,"aMS",@progbits,1
.LC111:
	.string	"\",\"type\":\"sequence\","
.LC112:
	.string	"----- Instruction sequence "
.LC113:
	.string	" -----\n"
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_113TraceSequenceEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPKc,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_113TraceSequenceEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPKc, @function
_ZN2v88internal8compiler12_GLOBAL__N_113TraceSequenceEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPKc:
.LFB26870:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$544, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	testb	$64, %ah
	jne	.L1552
.L1534:
	testb	$-128, %ah
	je	.L1533
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1553
	call	_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv@PLT
	movq	%rax, %rbx
.L1537:
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%rbx), %rsi
	je	.L1538
	testq	%rsi, %rsi
	je	.L1554
.L1539:
	addl	$1, 152(%rbx)
.L1538:
	leaq	-560(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movl	$27, %edx
	leaq	.LC112(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$7, %edx
	leaq	.LC113(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	272(%r13), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_19InstructionSequenceE@PLT
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, -480(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-496(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -560(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-480(%rbp), %rdi
	movq	%rax, -560(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -480(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L1533
	subl	$1, 152(%rbx)
	je	.L1555
.L1533:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1556
	addq	$544, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1552:
	.cfi_restore_state
	leaq	-560(%rbp), %r14
	movq	%rdi, %rsi
	movq	%rdi, %rbx
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode@PLT
	movl	$9, %edx
	leaq	.LC65(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$20, %edx
	leaq	.LC111(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	272(%r13), %rax
	leaq	-568(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -568(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_25InstructionSequenceAsJSONE@PLT
	movl	$3, %edx
	leaq	.LC67(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileD1Ev@PLT
	movl	(%rbx), %eax
	jmp	.L1534
	.p2align 4,,10
	.p2align 3
.L1553:
	movq	0(%r13), %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	movq	%rax, %rbx
	jmp	.L1537
	.p2align 4,,10
	.p2align 3
.L1555:
	movq	144(%rbx), %rdi
	call	fclose@PLT
	movq	$0, 144(%rbx)
	jmp	.L1533
	.p2align 4,,10
	.p2align 3
.L1554:
	movq	(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %rsi
	jmp	.L1539
.L1556:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26870:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_113TraceSequenceEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPKc, .-_ZN2v88internal8compiler12_GLOBAL__N_113TraceSequenceEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPKc
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl17AllocateRegistersEPKNS0_21RegisterConfigurationEPNS1_14CallDescriptorEb.str1.8,"aMS",@progbits,1
	.align 8
.LC114:
	.string	"../deps/v8/src/compiler/pipeline.cc:3103"
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl17AllocateRegistersEPKNS0_21RegisterConfigurationEPNS1_14CallDescriptorEb.str1.1,"aMS",@progbits,1
.LC115:
	.string	"V8.TFMeetRegisterConstraints"
.LC116:
	.string	"V8.TFResolvePhis"
.LC117:
	.string	"V8.TFBuildLiveRanges"
.LC118:
	.string	"V8.TFBuildLiveRangeBundles"
.LC119:
	.string	"before register allocation"
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl17AllocateRegistersEPKNS0_21RegisterConfigurationEPNS1_14CallDescriptorEb.str1.8
	.align 8
.LC120:
	.string	"!data->register_allocation_data()->ExistsUseWithoutDefinition()"
	.align 8
.LC121:
	.string	"data->register_allocation_data() ->RangesDefinedInDeferredStayInDeferred()"
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl17AllocateRegistersEPKNS0_21RegisterConfigurationEPNS1_14CallDescriptorEb.str1.1
.LC122:
	.string	"PreAllocation"
.LC123:
	.string	"V8.TFSplinterLiveRanges"
.LC124:
	.string	"PostSplinter"
.LC125:
	.string	"V8.TFAllocateGeneralRegisters"
.LC126:
	.string	"V8.TFAllocateFPRegisters"
.LC127:
	.string	"V8.TFMergeSplinteredRanges"
.LC128:
	.string	"V8.TFDecideSpillingMode"
.LC129:
	.string	"V8.TFAssignSpillSlots"
.LC130:
	.string	"V8.TFCommitAssignment"
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl17AllocateRegistersEPKNS0_21RegisterConfigurationEPNS1_14CallDescriptorEb.str1.8
	.align 8
.LC131:
	.string	"Immediately after CommitAssignmentPhase."
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl17AllocateRegistersEPKNS0_21RegisterConfigurationEPNS1_14CallDescriptorEb.str1.1
.LC132:
	.string	"V8.TFPopulatePointerMaps"
.LC133:
	.string	"V8.TFConnectRanges"
.LC134:
	.string	"V8.TFResolveControlFlow"
.LC135:
	.string	"V8.TFOptimizeMoves"
.LC136:
	.string	"V8.TFLocateSpillSlots"
.LC137:
	.string	"after register allocation"
.LC138:
	.string	"End of regalloc pipeline."
.LC139:
	.string	"CodeGen"
	.section	.text._ZN2v88internal8compiler12PipelineImpl17AllocateRegistersEPKNS0_21RegisterConfigurationEPNS1_14CallDescriptorEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12PipelineImpl17AllocateRegistersEPKNS0_21RegisterConfigurationEPNS1_14CallDescriptorEb
	.type	_ZN2v88internal8compiler12PipelineImpl17AllocateRegistersEPKNS0_21RegisterConfigurationEPNS1_14CallDescriptorEb, @function
_ZN2v88internal8compiler12PipelineImpl17AllocateRegistersEPKNS0_21RegisterConfigurationEPNS1_14CallDescriptorEb:
.LFB26871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$840, %rsp
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -816(%rbp)
	movq	$0, -808(%rbp)
	testb	%cl, %cl
	jne	.L1873
.L1558:
	movq	24(%r13), %r14
	movq	328(%r13), %rcx
	movq	272(%r13), %r8
	movq	32(%r13), %r15
	movl	(%r14), %eax
	leaq	160(%r14), %r10
	movl	%eax, %r9d
	shrl	$20, %r9d
	andl	$1, %r9d
	movl	%r9d, %edx
	orl	$2, %edx
	testl	$2097152, %eax
	cmovne	%edx, %r9d
	movl	%r9d, %edx
	orl	$4, %edx
	testl	$131072, %eax
	cmovne	%edx, %r9d
	movq	360(%r13), %rdx
	movq	16(%rdx), %r14
	movq	24(%rdx), %rsi
	subq	%r14, %rsi
	cmpq	$503, %rsi
	jbe	.L1874
	leaq	504(%r14), %rsi
	movq	%rsi, 16(%rdx)
.L1564:
	pushq	%r15
	movq	%r12, %rsi
	movq	%r14, %rdi
	pushq	%r10
	call	_ZN2v88internal8compiler22RegisterAllocationDataC1EPKNS0_21RegisterConfigurationEPNS0_4ZoneEPNS1_5FrameEPNS1_19InstructionSequenceENS_4base5FlagsINS1_22RegisterAllocationFlagEiEEPNS0_11TickCounterEPKc@PLT
	movq	%r14, 368(%r13)
	movq	(%rbx), %r12
	popq	%rdx
	popq	%rcx
	movq	24(%r12), %rax
	cmpl	$-1, 56(%rax)
	je	.L1565
	movq	328(%r13), %rsi
	leaq	80(%r13), %rdi
	call	_ZN2v88internal8compiler9OsrHelper10SetupFrameEPNS1_5FrameE@PLT
	movq	(%rbx), %r12
.L1565:
	movq	56(%r12), %r15
	testq	%r15, %r15
	je	.L1566
	leaq	.LC115(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1566:
	movq	176(%r12), %r14
	movq	48(%r12), %r8
	testq	%r14, %r14
	je	.L1567
	leaq	.LC115(%rip), %rax
	movq	48(%r14), %rdx
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, 48(%r14)
	leaq	-752(%rbp), %r12
	movq	%rdx, -840(%rbp)
	movq	%r8, -832(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%rbx), %rcx
	movq	%r12, %rdi
	movq	%rax, -824(%rbp)
	movq	368(%rcx), %rsi
	call	_ZN2v88internal8compiler17ConstraintBuilderC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17ConstraintBuilder23MeetRegisterConstraintsEv@PLT
	movq	-840(%rbp), %rdx
	movq	-832(%rbp), %r8
	movq	-824(%rbp), %rax
	movq	%rdx, 48(%r14)
.L1660:
	testq	%rax, %rax
	je	.L1568
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1568:
	testq	%r15, %r15
	je	.L1569
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1569:
	movq	(%rbx), %r14
	movq	56(%r14), %r15
	testq	%r15, %r15
	je	.L1570
	leaq	.LC116(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1570:
	movq	48(%r14), %r8
	movq	176(%r14), %r14
	testq	%r14, %r14
	je	.L1571
	leaq	.LC116(%rip), %rax
	movq	48(%r14), %rdx
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, 48(%r14)
	movq	%rdx, -840(%rbp)
	movq	%r8, -832(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%rbx), %rcx
	movq	%r12, %rdi
	movq	%rax, -824(%rbp)
	movq	368(%rcx), %rsi
	call	_ZN2v88internal8compiler17ConstraintBuilderC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17ConstraintBuilder11ResolvePhisEv@PLT
	movq	-840(%rbp), %rdx
	movq	-832(%rbp), %r8
	movq	-824(%rbp), %rax
	movq	%rdx, 48(%r14)
.L1659:
	testq	%rax, %rax
	je	.L1572
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1572:
	testq	%r15, %r15
	je	.L1573
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1573:
	movq	(%rbx), %r15
	movq	56(%r15), %r14
	testq	%r14, %r14
	je	.L1574
	leaq	.LC117(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1574:
	movq	48(%r15), %rdi
	movq	176(%r15), %r9
	movq	%rdi, -824(%rbp)
	testq	%r9, %r9
	je	.L1575
	movq	48(%r9), %rax
	leaq	.LC3(%rip), %rsi
	movq	%r9, -832(%rbp)
	movq	%rax, -864(%rbp)
	leaq	.LC117(%rip), %rax
	movq	%rax, 48(%r9)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	%r15, %rdx
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler16LiveRangeBuilderC1EPNS1_22RegisterAllocationDataEPNS0_4ZoneE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16LiveRangeBuilder15BuildLiveRangesEv@PLT
	movq	-720(%rbp), %rdx
	movq	-832(%rbp), %r9
	testq	%rdx, %rdx
	je	.L1666
.L1665:
	movq	%r13, -832(%rbp)
	leaq	-744(%rbp), %rax
	movq	%r9, -840(%rbp)
	movq	%rbx, -848(%rbp)
	movq	%rdx, %rbx
	movq	%r12, -856(%rbp)
	movq	%rax, %r12
.L1579:
	movq	24(%rbx), %r13
	testq	%r13, %r13
	je	.L1577
.L1578:
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler18InstructionOperandESt4pairIKS4_PNS2_11UsePositionEESt10_Select1stIS9_ESt4lessIS4_ENS1_13ZoneAllocatorIS9_EEE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L1578
.L1577:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1579
	movq	-840(%rbp), %r9
	movq	-832(%rbp), %r13
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r12
	testq	%r9, %r9
	je	.L1580
.L1666:
	movq	-864(%rbp), %rax
	movq	%rax, 48(%r9)
.L1580:
	testq	%r15, %r15
	je	.L1581
	movq	-824(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1581:
	testq	%r14, %r14
	je	.L1582
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1582:
	movq	(%rbx), %r14
	movq	56(%r14), %r15
	testq	%r15, %r15
	je	.L1583
	leaq	.LC118(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1583:
	movq	48(%r14), %r8
	movq	176(%r14), %r14
	testq	%r14, %r14
	je	.L1584
	leaq	.LC118(%rip), %rax
	movq	48(%r14), %rdx
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, 48(%r14)
	movq	%rdx, -840(%rbp)
	movq	%r8, -832(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%rbx), %rcx
	movq	%r12, %rdi
	movq	%rax, -824(%rbp)
	movq	368(%rcx), %rcx
	movl	$0, -744(%rbp)
	movq	%rcx, -752(%rbp)
	call	_ZN2v88internal8compiler13BundleBuilder12BuildBundlesEv@PLT
	movq	-840(%rbp), %rdx
	movq	-832(%rbp), %r8
	movq	-824(%rbp), %rsi
	movq	%rdx, 48(%r14)
.L1658:
	testq	%rsi, %rsi
	je	.L1585
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1585:
	testq	%r15, %r15
	je	.L1586
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1586:
	movq	(%rbx), %rax
	leaq	.LC119(%rip), %rdx
	movq	%r13, %rsi
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_113TraceSequenceEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPKc
	cmpq	$0, -808(%rbp)
	je	.L1587
	movq	368(%r13), %rdi
	call	_ZN2v88internal8compiler22RegisterAllocationData26ExistsUseWithoutDefinitionEv@PLT
	testb	%al, %al
	jne	.L1875
	movq	368(%r13), %rdi
	call	_ZN2v88internal8compiler22RegisterAllocationData37RangesDefinedInDeferredStayInDeferredEv@PLT
	testb	%al, %al
	je	.L1876
.L1587:
	movq	(%rbx), %r15
	movq	24(%r15), %rdx
	movl	(%rdx), %edx
	testb	$64, %dh
	jne	.L1877
.L1589:
	andl	$2097152, %edx
	movq	56(%r15), %r14
	jne	.L1878
.L1590:
	testq	%r14, %r14
	je	.L1595
	leaq	.LC125(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1595:
	movq	48(%r15), %rdi
	movq	176(%r15), %r9
	movq	%rdi, -824(%rbp)
	testq	%r9, %r9
	je	.L1596
	movq	48(%r9), %rax
	leaq	.LC3(%rip), %rsi
	movq	%r9, -832(%rbp)
	movq	%rax, -872(%rbp)
	leaq	.LC125(%rip), %rax
	movq	%rax, 48(%r9)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	%r15, %rcx
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler19LinearScanAllocatorC1EPNS1_22RegisterAllocationDataENS1_12RegisterKindEPNS0_4ZoneE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator17AllocateRegistersEv@PLT
	movq	-688(%rbp), %rcx
	movq	-832(%rbp), %r9
	testq	%rcx, %rcx
	je	.L1661
.L1663:
	movq	%r13, -832(%rbp)
	leaq	-712(%rbp), %rax
	movq	%r9, -840(%rbp)
	movq	%rbx, -848(%rbp)
	movq	%rcx, %rbx
	movq	%r12, -856(%rbp)
	movq	%rax, %r12
.L1600:
	movq	24(%rbx), %r13
	testq	%r13, %r13
	je	.L1598
.L1599:
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L1599
.L1598:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1600
	movq	-840(%rbp), %r9
	movq	-832(%rbp), %r13
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r12
	testq	%r9, %r9
	je	.L1601
.L1661:
	movq	-872(%rbp), %rax
	movq	%rax, 48(%r9)
.L1601:
	testq	%r15, %r15
	je	.L1602
	movq	-824(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1602:
	testq	%r14, %r14
	je	.L1603
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1603:
	movq	(%rbx), %r14
	movq	272(%r13), %rax
	movq	56(%r14), %r15
	testl	$28672, 408(%rax)
	jne	.L1879
.L1604:
	movq	24(%r14), %rax
	movl	(%rax), %eax
	testl	$2097152, %eax
	jne	.L1880
.L1611:
	testq	%r15, %r15
	je	.L1614
	leaq	.LC128(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1614:
	movq	176(%r14), %rdx
	movq	48(%r14), %r8
	testq	%rdx, %rdx
	je	.L1615
	leaq	.LC128(%rip), %rax
	movq	48(%rdx), %rcx
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, 48(%rdx)
	movq	%rcx, -840(%rbp)
	movq	%rdx, -832(%rbp)
	movq	%r8, -824(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler15OperandAssignerC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler15OperandAssigner18DecideSpillingModeEv@PLT
	movq	-832(%rbp), %rdx
	movq	-840(%rbp), %rcx
	movq	-824(%rbp), %r8
	movq	%rcx, 48(%rdx)
.L1657:
	testq	%r14, %r14
	je	.L1616
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1616:
	testq	%r15, %r15
	je	.L1617
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1617:
	movq	(%rbx), %r14
	movq	56(%r14), %r15
	testq	%r15, %r15
	je	.L1618
	leaq	.LC129(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1618:
	movq	176(%r14), %rdx
	movq	48(%r14), %r9
	testq	%rdx, %rdx
	je	.L1619
	leaq	.LC129(%rip), %rax
	movq	48(%rdx), %rcx
	movq	%r9, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, 48(%rdx)
	movq	%rcx, -840(%rbp)
	movq	%rdx, -832(%rbp)
	movq	%r9, -824(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler15OperandAssignerC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler15OperandAssigner16AssignSpillSlotsEv@PLT
	movq	-832(%rbp), %rdx
	movq	-840(%rbp), %rcx
	movq	-824(%rbp), %r9
	movq	%rcx, 48(%rdx)
.L1656:
	testq	%r14, %r14
	je	.L1620
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1620:
	testq	%r15, %r15
	je	.L1621
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1621:
	movq	(%rbx), %r14
	movq	56(%r14), %r15
	testq	%r15, %r15
	je	.L1622
	leaq	.LC130(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1622:
	movq	176(%r14), %rdx
	movq	48(%r14), %r9
	testq	%rdx, %rdx
	je	.L1623
	leaq	.LC130(%rip), %rax
	movq	48(%rdx), %rcx
	movq	%r9, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, 48(%rdx)
	movq	%rcx, -840(%rbp)
	movq	%rdx, -832(%rbp)
	movq	%r9, -824(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler15OperandAssignerC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler15OperandAssigner16CommitAssignmentEv@PLT
	movq	-832(%rbp), %rdx
	movq	-840(%rbp), %rcx
	movq	-824(%rbp), %r9
	movq	%rcx, 48(%rdx)
.L1655:
	testq	%r14, %r14
	je	.L1624
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1624:
	testq	%r15, %r15
	je	.L1625
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1625:
	movq	-808(%rbp), %rax
	testq	%rax, %rax
	je	.L1626
	leaq	.LC131(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler25RegisterAllocatorVerifier16VerifyAssignmentEPKc@PLT
.L1626:
	movq	(%rbx), %r14
	movq	56(%r14), %r15
	testq	%r15, %r15
	je	.L1627
	leaq	.LC132(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1627:
	movq	176(%r14), %rdx
	movq	48(%r14), %r9
	testq	%rdx, %rdx
	je	.L1628
	leaq	.LC132(%rip), %rax
	movq	48(%rdx), %rcx
	movq	%r9, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, 48(%rdx)
	movq	%rcx, -840(%rbp)
	movq	%rdx, -832(%rbp)
	movq	%r9, -824(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler21ReferenceMapPopulatorC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21ReferenceMapPopulator21PopulateReferenceMapsEv@PLT
	movq	-832(%rbp), %rdx
	movq	-840(%rbp), %rcx
	movq	-824(%rbp), %r9
	movq	%rcx, 48(%rdx)
.L1654:
	testq	%r14, %r14
	je	.L1629
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1629:
	testq	%r15, %r15
	je	.L1630
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1630:
	movq	(%rbx), %r14
	movq	56(%r14), %r15
	testq	%r15, %r15
	je	.L1631
	leaq	.LC133(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1631:
	movq	176(%r14), %rdx
	movq	48(%r14), %r9
	testq	%rdx, %rdx
	je	.L1632
	leaq	.LC133(%rip), %rax
	movq	48(%rdx), %rcx
	movq	%r9, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, 48(%rdx)
	movq	%rcx, -840(%rbp)
	movq	%rdx, -832(%rbp)
	movq	%r9, -824(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler18LiveRangeConnectorC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18LiveRangeConnector13ConnectRangesEPNS0_4ZoneE@PLT
	movq	-832(%rbp), %rdx
	movq	-840(%rbp), %rcx
	movq	-824(%rbp), %r9
	movq	%rcx, 48(%rdx)
.L1653:
	testq	%r14, %r14
	je	.L1633
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1633:
	testq	%r15, %r15
	je	.L1634
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1634:
	movq	(%rbx), %r14
	movq	56(%r14), %r15
	testq	%r15, %r15
	je	.L1635
	leaq	.LC134(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1635:
	movq	176(%r14), %rdx
	movq	48(%r14), %r9
	testq	%rdx, %rdx
	je	.L1636
	leaq	.LC134(%rip), %rax
	movq	48(%rdx), %rcx
	movq	%r9, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, 48(%rdx)
	movq	%rcx, -840(%rbp)
	movq	%rdx, -832(%rbp)
	movq	%r9, -824(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler18LiveRangeConnectorC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18LiveRangeConnector18ResolveControlFlowEPNS0_4ZoneE@PLT
	movq	-832(%rbp), %rdx
	movq	-840(%rbp), %rcx
	movq	-824(%rbp), %r9
	movq	%rcx, 48(%rdx)
.L1652:
	testq	%r14, %r14
	je	.L1637
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1637:
	testq	%r15, %r15
	je	.L1638
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1638:
	movq	(%rbx), %r14
	cmpb	$0, _ZN2v88internal28FLAG_turbo_move_optimizationE(%rip)
	movq	56(%r14), %r15
	jne	.L1881
.L1639:
	testq	%r15, %r15
	je	.L1642
	leaq	.LC136(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1642:
	movq	176(%r14), %rdx
	movq	48(%r14), %r8
	testq	%rdx, %rdx
	je	.L1643
	leaq	.LC136(%rip), %rax
	movq	48(%rdx), %rcx
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, 48(%rdx)
	movq	%rcx, -840(%rbp)
	movq	%rdx, -832(%rbp)
	movq	%r8, -824(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler16SpillSlotLocatorC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16SpillSlotLocator16LocateSpillSlotsEv@PLT
	movq	-832(%rbp), %rdx
	movq	-840(%rbp), %rcx
	movq	-824(%rbp), %r8
	movq	%rcx, 48(%rdx)
.L1651:
	testq	%r14, %r14
	je	.L1644
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1644:
	testq	%r15, %r15
	je	.L1645
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1645:
	movq	(%rbx), %rax
	leaq	.LC137(%rip), %rdx
	movq	%r13, %rsi
	movq	24(%rax), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_113TraceSequenceEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPKc
	movq	-808(%rbp), %r15
	testq	%r15, %r15
	je	.L1646
	movq	%r15, %rdi
	leaq	.LC138(%rip), %rsi
	call	_ZN2v88internal8compiler25RegisterAllocatorVerifier16VerifyAssignmentEPKc@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler25RegisterAllocatorVerifier14VerifyGapMovesEv@PLT
.L1646:
	movq	(%rbx), %rax
	movq	24(%rax), %rdx
	movl	(%rdx), %edx
	andb	$64, %dh
	jne	.L1882
.L1647:
	cmpq	$0, 360(%r13)
	je	.L1648
	movq	352(%r13), %rsi
	testq	%rsi, %rsi
	je	.L1649
	movq	344(%r13), %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1649:
	movq	$0, 368(%r13)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 352(%r13)
.L1648:
	movq	-816(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1557
	movq	%rbx, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movl	$64, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1557:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1883
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1877:
	.cfi_restore_state
	cmpb	$0, 40(%r13)
	jne	.L1589
	leaq	-576(%rbp), %r14
	movq	(%r15), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12TurboCfgFileC1EPNS0_7IsolateE@PLT
	movq	368(%r13), %rax
	movq	%r14, %rdi
	movq	%r12, %rsi
	leaq	.LC122(%rip), %rcx
	movq	%rcx, -752(%rbp)
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_27AsC1VRegisterAllocationDataE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12TurboCfgFileD1Ev@PLT
	movq	(%rbx), %r15
	movq	24(%r15), %rdx
	movq	56(%r15), %r14
	movl	(%rdx), %edx
	andl	$2097152, %edx
	je	.L1590
	.p2align 4,,10
	.p2align 3
.L1878:
	movq	%r14, -752(%rbp)
	testq	%r14, %r14
	je	.L1591
	leaq	.LC123(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1591:
	movq	48(%r15), %rdi
	leaq	.LC3(%rip), %rax
	movq	$0, -728(%rbp)
	movq	%rax, -744(%rbp)
	movq	%rdi, -736(%rbp)
	movq	176(%r15), %rax
	movq	%rax, -720(%rbp)
	testq	%rax, %rax
	je	.L1592
	movq	48(%rax), %rdx
	leaq	.LC123(%rip), %rcx
	movq	%rdx, -712(%rbp)
	movq	%rcx, 48(%rax)
.L1592:
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%rbx), %rdx
	leaq	-800(%rbp), %rdi
	movq	%rax, -728(%rbp)
	movq	368(%rdx), %rdx
	movq	%rax, -792(%rbp)
	movq	%rdx, -800(%rbp)
	call	_ZN2v88internal8compiler18LiveRangeSeparator8SplinterEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116PipelineRunScopeD1Ev
	movq	(%rbx), %r15
	movq	24(%r15), %rdx
	movl	(%rdx), %edx
	andb	$64, %dh
	je	.L1872
	cmpb	$0, 40(%r13)
	je	.L1594
.L1872:
	movq	56(%r15), %r14
	jmp	.L1590
	.p2align 4,,10
	.p2align 3
.L1882:
	cmpb	$0, 40(%r13)
	jne	.L1647
	leaq	-576(%rbp), %r14
	movq	(%rax), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12TurboCfgFileC1EPNS0_7IsolateE@PLT
	movq	368(%r13), %rax
	movq	%r14, %rdi
	movq	%r12, %rsi
	leaq	.LC139(%rip), %rcx
	movq	%rcx, -752(%rbp)
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_27AsC1VRegisterAllocationDataE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12TurboCfgFileD1Ev@PLT
	jmp	.L1647
	.p2align 4,,10
	.p2align 3
.L1873:
	movq	16(%r13), %r14
	movl	$64, %edi
	call	_Znwm@PLT
	leaq	.LC114(%rip), %rdx
	movq	%rax, %r15
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%rax, -816(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	16(%r15), %rax
	movq	24(%r15), %rdx
	movq	272(%r13), %rcx
	subq	%rax, %rdx
	cmpq	$175, %rdx
	jbe	.L1884
	movq	-816(%rbp), %rsi
	leaq	176(%rax), %rdx
	movq	%rdx, 16(%rsi)
.L1560:
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rax, -808(%rbp)
	call	_ZN2v88internal8compiler25RegisterAllocatorVerifierC1EPNS0_4ZoneEPKNS0_21RegisterConfigurationEPKNS1_19InstructionSequenceE@PLT
	jmp	.L1558
	.p2align 4,,10
	.p2align 3
.L1584:
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r8, -832(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, -824(%rbp)
	movq	(%rbx), %rax
	movq	368(%rax), %rax
	movl	$0, -744(%rbp)
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal8compiler13BundleBuilder12BuildBundlesEv@PLT
	movq	-824(%rbp), %rsi
	movq	-832(%rbp), %r8
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1575:
	leaq	.LC3(%rip), %rsi
	movq	%r9, -832(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	%r15, %rdx
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler16LiveRangeBuilderC1EPNS1_22RegisterAllocationDataEPNS0_4ZoneE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16LiveRangeBuilder15BuildLiveRangesEv@PLT
	movq	-720(%rbp), %rdx
	movq	-832(%rbp), %r9
	testq	%rdx, %rdx
	jne	.L1665
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1571:
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r8, -832(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%rbx), %rdx
	movq	%r12, %rdi
	movq	%rax, -824(%rbp)
	movq	368(%rdx), %rsi
	call	_ZN2v88internal8compiler17ConstraintBuilderC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17ConstraintBuilder11ResolvePhisEv@PLT
	movq	-824(%rbp), %rax
	movq	-832(%rbp), %r8
	jmp	.L1659
	.p2align 4,,10
	.p2align 3
.L1567:
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r8, -832(%rbp)
	leaq	-752(%rbp), %r12
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%rbx), %rdx
	movq	%r12, %rdi
	movq	%rax, -824(%rbp)
	movq	368(%rdx), %rsi
	call	_ZN2v88internal8compiler17ConstraintBuilderC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler17ConstraintBuilder23MeetRegisterConstraintsEv@PLT
	movq	-824(%rbp), %rax
	movq	-832(%rbp), %r8
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1880:
	movq	%r15, -752(%rbp)
	testq	%r15, %r15
	je	.L1612
	leaq	.LC127(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1612:
	movq	48(%r14), %rdi
	leaq	.LC3(%rip), %rax
	movq	$0, -728(%rbp)
	movq	%rax, -744(%rbp)
	movq	%rdi, -736(%rbp)
	movq	176(%r14), %rax
	movq	%rax, -720(%rbp)
	testq	%rax, %rax
	je	.L1613
	movq	48(%rax), %rdx
	leaq	.LC127(%rip), %rcx
	movq	%rdx, -712(%rbp)
	movq	%rcx, 48(%rax)
.L1613:
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%rbx), %rdx
	leaq	-800(%rbp), %rdi
	movq	%rax, -728(%rbp)
	movq	368(%rdx), %rdx
	movq	%rax, -792(%rbp)
	movq	%rdx, -800(%rbp)
	call	_ZN2v88internal8compiler15LiveRangeMerger5MergeEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116PipelineRunScopeD1Ev
	movq	(%rbx), %r14
	movq	56(%r14), %r15
	jmp	.L1611
	.p2align 4,,10
	.p2align 3
.L1879:
	movq	%r15, -800(%rbp)
	testq	%r15, %r15
	je	.L1605
	leaq	.LC126(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1605:
	movq	48(%r14), %rdi
	leaq	.LC3(%rip), %rax
	movq	$0, -776(%rbp)
	movq	%rax, -792(%rbp)
	movq	%rdi, -784(%rbp)
	movq	176(%r14), %rax
	movq	%rax, -768(%rbp)
	testq	%rax, %rax
	je	.L1606
	movq	48(%rax), %rdx
	leaq	.LC126(%rip), %rcx
	movq	%rdx, -760(%rbp)
	movq	%rcx, 48(%rax)
.L1606:
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, -776(%rbp)
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler19LinearScanAllocatorC1EPNS1_22RegisterAllocationDataENS1_12RegisterKindEPNS0_4ZoneE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator17AllocateRegistersEv@PLT
	movq	-688(%rbp), %r14
	testq	%r14, %r14
	je	.L1607
	leaq	-712(%rbp), %rax
	movq	%rbx, -824(%rbp)
	movq	%r14, %rbx
	movq	%rax, %r14
.L1610:
	movq	24(%rbx), %r15
	testq	%r15, %r15
	je	.L1608
.L1609:
	movq	24(%r15), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler9LiveRangeES4_St9_IdentityIS4_ENS2_19LinearScanAllocator17LiveRangeOrderingENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.L1609
.L1608:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1610
	movq	-824(%rbp), %rbx
.L1607:
	leaq	-800(%rbp), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116PipelineRunScopeD1Ev
	movq	(%rbx), %r14
	movq	56(%r14), %r15
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1881:
	movq	%r15, -800(%rbp)
	testq	%r15, %r15
	je	.L1640
	leaq	.LC135(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1640:
	movq	48(%r14), %rdi
	leaq	.LC3(%rip), %rax
	movq	$0, -776(%rbp)
	movq	%rax, -792(%rbp)
	movq	%rdi, -784(%rbp)
	movq	176(%r14), %rax
	movq	%rax, -768(%rbp)
	testq	%rax, %rax
	je	.L1641
	movq	48(%rax), %rdx
	leaq	.LC135(%rip), %rcx
	movq	%rdx, -760(%rbp)
	movq	%rcx, 48(%rax)
.L1641:
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -776(%rbp)
	movq	(%rbx), %rax
	movq	272(%rax), %rdx
	call	_ZN2v88internal8compiler13MoveOptimizerC1EPNS0_4ZoneEPNS1_19InstructionSequenceE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13MoveOptimizer3RunEv@PLT
	leaq	-800(%rbp), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116PipelineRunScopeD1Ev
	movq	(%rbx), %r14
	movq	56(%r14), %r15
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1596:
	leaq	.LC3(%rip), %rsi
	movq	%r9, -832(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	%r15, %rcx
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler19LinearScanAllocatorC1EPNS1_22RegisterAllocationDataENS1_12RegisterKindEPNS0_4ZoneE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19LinearScanAllocator17AllocateRegistersEv@PLT
	movq	-688(%rbp), %rcx
	movq	-832(%rbp), %r9
	testq	%rcx, %rcx
	jne	.L1663
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1643:
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r8, -824(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler16SpillSlotLocatorC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler16SpillSlotLocator16LocateSpillSlotsEv@PLT
	movq	-824(%rbp), %r8
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1619:
	movq	%r9, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r9, -824(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler15OperandAssignerC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler15OperandAssigner16AssignSpillSlotsEv@PLT
	movq	-824(%rbp), %r9
	jmp	.L1656
	.p2align 4,,10
	.p2align 3
.L1615:
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r8, -824(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler15OperandAssignerC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler15OperandAssigner18DecideSpillingModeEv@PLT
	movq	-824(%rbp), %r8
	jmp	.L1657
	.p2align 4,,10
	.p2align 3
.L1628:
	movq	%r9, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r9, -824(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler21ReferenceMapPopulatorC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21ReferenceMapPopulator21PopulateReferenceMapsEv@PLT
	movq	-824(%rbp), %r9
	jmp	.L1654
	.p2align 4,,10
	.p2align 3
.L1623:
	movq	%r9, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r9, -824(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler15OperandAssignerC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler15OperandAssigner16CommitAssignmentEv@PLT
	movq	-824(%rbp), %r9
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1636:
	movq	%r9, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r9, -824(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler18LiveRangeConnectorC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18LiveRangeConnector18ResolveControlFlowEPNS0_4ZoneE@PLT
	movq	-824(%rbp), %r9
	jmp	.L1652
	.p2align 4,,10
	.p2align 3
.L1632:
	movq	%r9, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r9, -824(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	368(%rax), %rsi
	call	_ZN2v88internal8compiler18LiveRangeConnectorC1EPNS1_22RegisterAllocationDataE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18LiveRangeConnector13ConnectRangesEPNS0_4ZoneE@PLT
	movq	-824(%rbp), %r9
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1875:
	leaq	.LC120(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1874:
	movq	%rdx, %rdi
	movl	$504, %esi
	movq	%r10, -856(%rbp)
	movq	%r8, -848(%rbp)
	movq	%rcx, -840(%rbp)
	movl	%r9d, -832(%rbp)
	movq	%rdx, -824(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-824(%rbp), %rdx
	movl	-832(%rbp), %r9d
	movq	-840(%rbp), %rcx
	movq	-848(%rbp), %r8
	movq	%rax, %r14
	movq	-856(%rbp), %r10
	jmp	.L1564
	.p2align 4,,10
	.p2align 3
.L1594:
	movq	(%r15), %rsi
	leaq	-576(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12TurboCfgFileC1EPNS0_7IsolateE@PLT
	movq	368(%r13), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	.LC124(%rip), %rcx
	movq	%rcx, -752(%rbp)
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_27AsC1VRegisterAllocationDataE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12TurboCfgFileD1Ev@PLT
	movq	(%rbx), %r15
	jmp	.L1872
	.p2align 4,,10
	.p2align 3
.L1876:
	leaq	.LC121(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1884:
	movq	-816(%rbp), %rdi
	movl	$176, %esi
	movq	%rcx, -808(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-808(%rbp), %rcx
	movq	-816(%rbp), %rsi
	jmp	.L1560
.L1883:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26871:
	.size	_ZN2v88internal8compiler12PipelineImpl17AllocateRegistersEPKNS0_21RegisterConfigurationEPNS1_14CallDescriptorEb, .-_ZN2v88internal8compiler12PipelineImpl17AllocateRegistersEPKNS0_21RegisterConfigurationEPNS1_14CallDescriptorEb
	.section	.rodata._ZN2v88internal8compiler8Pipeline27AllocateRegistersForTestingEPKNS0_21RegisterConfigurationEPNS1_19InstructionSequenceEb.str1.8,"aMS",@progbits,1
	.align 8
.LC140:
	.string	"../deps/v8/src/compiler/pipeline.cc:222"
	.align 8
.LC141:
	.string	"../deps/v8/src/compiler/pipeline.cc:221"
	.section	.rodata._ZN2v88internal8compiler8Pipeline27AllocateRegistersForTestingEPKNS0_21RegisterConfigurationEPNS1_19InstructionSequenceEb.str1.1,"aMS",@progbits,1
.LC142:
	.string	"testing"
	.section	.rodata._ZN2v88internal8compiler8Pipeline27AllocateRegistersForTestingEPKNS0_21RegisterConfigurationEPNS1_19InstructionSequenceEb.str1.8
	.align 8
.LC143:
	.string	"../deps/v8/src/compiler/pipeline.cc:225"
	.align 8
.LC144:
	.string	"../deps/v8/src/compiler/pipeline.cc:227"
	.section	.text._ZN2v88internal8compiler8Pipeline27AllocateRegistersForTestingEPKNS0_21RegisterConfigurationEPNS1_19InstructionSequenceEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Pipeline27AllocateRegistersForTestingEPKNS0_21RegisterConfigurationEPNS1_19InstructionSequenceEb
	.type	_ZN2v88internal8compiler8Pipeline27AllocateRegistersForTestingEPKNS0_21RegisterConfigurationEPNS1_19InstructionSequenceEb, @function
_ZN2v88internal8compiler8Pipeline27AllocateRegistersForTestingEPKNS0_21RegisterConfigurationEPNS1_19InstructionSequenceEb:
.LFB26828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-688(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	movq	%r15, %rdi
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-768(%rbp), %r12
	pushq	%rbx
	movq	%r12, %xmm3
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$792, %rsp
	movl	%edx, -788(%rbp)
	movq	8(%rsi), %rcx
	movl	$8, %edx
	leaq	.LC142(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	.LC140(%rip), %rax
	movq	%rax, %xmm1
	leaq	.LC141(%rip), %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm3, %xmm1
	punpcklqdq	%xmm3, %xmm2
	movaps	%xmm1, -832(%rbp)
	movaps	%xmm2, -816(%rbp)
	call	_ZN2v88internal24OptimizedCompilationInfoC1ENS0_6VectorIKcEEPNS0_4ZoneENS0_4Code4KindE@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	41136(%rax), %rsi
	call	_ZN2v88internal8compiler9ZoneStatsC1EPNS0_19AccountingAllocatorE@PLT
	movq	(%rbx), %r13
	movq	%r15, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -504(%rbp)
	movq	41136(%r13), %rax
	movq	%r13, -512(%rbp)
	movq	%r15, -488(%rbp)
	movq	%rax, -496(%rbp)
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movdqa	-816(%rbp), %xmm2
	movdqa	-832(%rbp), %xmm1
	movw	%ax, -448(%rbp)
	leaq	.LC143(%rip), %rsi
	movq	8(%rbx), %rax
	movq	%rbx, -240(%rbp)
	movaps	%xmm2, -384(%rbp)
	movq	%rsi, -232(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm0, -352(%rbp)
	movaps	%xmm0, -336(%rbp)
	movaps	%xmm0, -320(%rbp)
	movaps	%xmm0, -304(%rbp)
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm1, -272(%rbp)
	movb	$1, -472(%rbp)
	movq	%r12, -464(%rbp)
	movq	$0, -456(%rbp)
	movl	$-1, -444(%rbp)
	movb	$0, -440(%rbp)
	movb	$0, -432(%rbp)
	movq	$0, -400(%rbp)
	movl	$0, -392(%rbp)
	movb	$0, -388(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -248(%rbp)
	movq	%r12, -224(%rbp)
	movq	$0, -216(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-464(%rbp), %rdi
	pxor	%xmm0, %xmm0
	leaq	.LC144(%rip), %rsi
	movq	%rax, %xmm1
	movq	%rsi, -176(%rbp)
	punpcklqdq	%xmm1, %xmm1
	movq	%rdi, -168(%rbp)
	movups	%xmm1, -216(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -184(%rbp)
	movq	$0, -160(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	pxor	%xmm0, %xmm0
	leaq	-88(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rax, %xmm1
	movq	%r13, %rsi
	leaq	-112(%rbp), %rax
	movb	$0, -112(%rbp)
	punpcklqdq	%xmm1, %xmm1
	movq	%rax, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -96(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal16AssemblerOptions7DefaultEPNS0_7IsolateEb@PLT
	movq	-208(%rbp), %rdi
	movq	$0, -64(%rbp)
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$31, %rax
	jbe	.L1890
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1887:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler5FrameC1Ei@PLT
	movzbl	-788(%rbp), %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, -184(%rbp)
	leaq	-776(%rbp), %rdi
	leaq	-512(%rbp), %rbx
	movq	%rbx, -776(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl17AllocateRegistersEPKNS0_21RegisterConfigurationEPNS1_14CallDescriptorEb
	movq	%rbx, %rdi
	movzbl	-448(%rbp), %r13d
	call	_ZN2v88internal8compiler12PipelineDataD1Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9ZoneStatsD1Ev@PLT
	movq	%r15, %rdi
	xorl	$1, %r13d
	call	_ZN2v88internal24OptimizedCompilationInfoD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1891
	addq	$792, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1890:
	.cfi_restore_state
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1887
.L1891:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26828:
	.size	_ZN2v88internal8compiler8Pipeline27AllocateRegistersForTestingEPKNS0_21RegisterConfigurationEPNS1_19InstructionSequenceEb, .-_ZN2v88internal8compiler8Pipeline27AllocateRegistersForTestingEPKNS0_21RegisterConfigurationEPNS1_19InstructionSequenceEb
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE.str1.8,"aMS",@progbits,1
	.align 8
.LC145:
	.string	"--------------------------------------------------\n"
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE.str1.1,"aMS",@progbits,1
.LC146:
	.string	"--- Verifying "
.LC147:
	.string	" generated by TurboFan\n"
.LC148:
	.string	"--- End of "
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE.str1.8
	.align 8
.LC149:
	.string	"../deps/v8/src/compiler/pipeline.cc:2821"
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE.str1.1
.LC150:
	.string	"V8.TFSelectInstructions"
.LC151:
	.string	"\",\"type\":\"instructions\""
.LC152:
	.string	"{}"
.LC153:
	.string	",\n\"NodeOrigins\" : "
.LC154:
	.string	"V8.TFRegisterAllocation"
.LC155:
	.string	"V8.TFFrameElision"
.LC156:
	.string	"V8.TFJumpThreading"
	.section	.text._ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE
	.type	_ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE, @function
_ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE:
.LFB26834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$1144, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -1120(%rbp)
	movq	(%rsi), %rax
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	cmpb	$0, _ZN2v88internal20FLAG_turbo_profilingE(%rip)
	movq	(%rdi), %rbx
	movq	%rax, -1096(%rbp)
	jne	.L2034
.L1893:
	movq	416(%rbx), %rax
	movzbl	65(%rbx), %edx
	testq	%rax, %rax
	je	.L1894
	cmpl	$1, (%rax)
	je	.L1895
.L1894:
	testb	%dl, %dl
	je	.L1895
.L1896:
	cmpb	$0, _ZN2v88internal21FLAG_trace_verify_csaE(%rip)
	jne	.L2035
.L1899:
	leaq	-1056(%rbp), %r15
	movq	16(%rbx), %rsi
	leaq	.LC149(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	24(%rbx), %rax
	movq	32(%rbx), %r8
	movq	%r15, %r9
	movq	160(%rbx), %rdi
	movq	232(%rbx), %rsi
	movl	8(%rax), %eax
	cmpl	$5, %eax
	setne	%dl
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	andl	%edx, %ecx
	movq	-1120(%rbp), %rdx
	call	_ZN2v88internal8compiler20MachineGraphVerifier3RunEPNS1_5GraphEPKNS1_8ScheduleEPNS1_7LinkageEbPKcPNS0_4ZoneE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
.L1897:
	movq	232(%rbx), %rsi
	movq	264(%rbx), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence20InstructionBlocksForEPNS0_4ZoneEPKNS1_8ScheduleE@PLT
	movq	264(%rbx), %rdx
	movq	(%rbx), %r14
	movq	%rax, %rcx
	movq	16(%rdx), %r12
	movq	24(%rdx), %rax
	subq	%r12, %rax
	cmpq	$455, %rax
	jbe	.L2036
	leaq	456(%r12), %rax
	movq	%rax, 16(%rdx)
.L1912:
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler19InstructionSequenceC1EPNS0_7IsolateEPNS0_4ZoneEPNS0_10ZoneVectorIPNS1_16InstructionBlockEEE@PLT
	movq	-1096(%rbp), %rax
	movq	%r12, 272(%rbx)
	testq	%rax, %rax
	je	.L1913
	movl	8(%rax), %eax
	leal	-1(%rax), %edx
	cmpl	$1, %edx
	jbe	.L1963
	cmpl	$4, %eax
	jne	.L1914
.L1963:
	movq	16(%r12), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movb	$1, 124(%rax)
.L1914:
	movq	24(%rbx), %rax
	movq	-1096(%rbp), %rdi
	movl	8(%rax), %esi
	call	_ZNK2v88internal8compiler14CallDescriptor23CalculateFixedFrameSizeENS0_4Code4KindE@PLT
	movl	%eax, %r14d
.L1913:
	movq	304(%rbx), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	jbe	.L2037
	leaq	32(%r12), %rax
	movq	%rax, 16(%rdi)
.L1917:
	movq	%r12, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal8compiler5FrameC1Ei@PLT
	movq	%r12, 328(%rbx)
	movq	0(%r13), %r12
	movq	56(%r12), %rax
	movq	%rax, -1136(%rbp)
	testq	%rax, %rax
	je	.L1918
	leaq	.LC150(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1918:
	movq	48(%r12), %rax
	movq	176(%r12), %r14
	movq	%rax, -1128(%rbp)
	testq	%r14, %r14
	je	.L1919
	movq	48(%r14), %rax
	movq	%rax, -1152(%rbp)
	leaq	.LC150(%rip), %rax
	movq	%rax, 48(%r14)
.L1919:
	movq	-1128(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	0(%r13), %r10
	movzbl	_ZN2v88internal33FLAG_turbo_instruction_schedulingE(%rip), %r11d
	movq	%rax, -1104(%rbp)
	movq	24(%r10), %rdx
	movzbl	124(%r10), %edi
	movl	(%rdx), %eax
	movl	4(%rdx), %esi
	movl	%edi, -1144(%rbp)
	movl	%eax, %ecx
	shrl	$14, %ecx
	andl	$1, %ecx
	cmpb	$0, _ZN2v88internal11CpuFeatures12initialized_E(%rip)
	jne	.L1920
	xorl	%edi, %edi
	movl	%ecx, -1172(%rbp)
	movl	%r11d, -1168(%rbp)
	movl	%esi, -1164(%rbp)
	movq	%r10, -1160(%rbp)
	movb	$1, _ZN2v88internal11CpuFeatures12initialized_E(%rip)
	call	_ZN2v88internal11CpuFeatures9ProbeImplEb@PLT
	movq	-1160(%rbp), %r10
	movl	-1172(%rbp), %ecx
	movl	-1168(%rbp), %r11d
	movl	-1164(%rbp), %esi
	movq	24(%r10), %rdx
	movl	(%rdx), %eax
.L1920:
	movq	160(%r10), %rdi
	subq	$8, %rsp
	movq	232(%r10), %r9
	addq	$160, %rdx
	movq	272(%r10), %r8
	movl	28(%rdi), %r12d
	pushq	%rcx
	movq	%r15, %rdi
	movl	-1144(%rbp), %ecx
	pushq	%rsi
	movq	-1104(%rbp), %rsi
	movq	%r10, -1144(%rbp)
	pushq	%rcx
	movl	_ZN2v88internal11CpuFeatures10supported_E(%rip), %ecx
	pushq	%r11
	pushq	%rcx
	movl	%eax, %ecx
	shrl	$9, %eax
	shrl	$5, %ecx
	andl	$1, %eax
	andl	$1, %ecx
	pushq	%rcx
	leaq	448(%r10), %rcx
	pushq	%rcx
	movq	-1120(%rbp), %rcx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	%rax
	pushq	328(%r10)
	pushq	168(%r10)
	call	_ZN2v88internal8compiler19InstructionSelectorC1EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE@PLT
	addq	$96, %rsp
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18SelectInstructionsEv@PLT
	movq	-1144(%rbp), %r10
	testb	%al, %al
	jne	.L1921
	movb	$1, 64(%r10)
.L1921:
	movq	24(%r10), %rsi
	testl	$16384, (%rsi)
	jne	.L2038
.L1922:
	testq	%r14, %r14
	je	.L1923
	movq	-1152(%rbp), %rax
	movq	%rax, 48(%r14)
.L1923:
	movq	-1104(%rbp), %rax
	testq	%rax, %rax
	je	.L1924
	movq	-1128(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1924:
	movq	-1136(%rbp), %rax
	testq	%rax, %rax
	je	.L1925
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1925:
	movzbl	64(%rbx), %eax
	movb	%al, -1104(%rbp)
	movl	%eax, %ecx
	movq	0(%r13), %rax
	movq	24(%rax), %rdi
	testb	%cl, %cl
	jne	.L2039
	movl	(%rdi), %edx
	andb	$64, %dh
	je	.L1930
	cmpb	$0, 40(%rbx)
	leaq	-576(%rbp), %r12
	je	.L1931
.L1934:
	movq	.LC157(%rip), %xmm1
	leaq	-464(%rbp), %r14
	movq	%r14, %rdi
	movhps	.LC75(%rip), %xmm1
	movaps	%xmm1, -1120(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -464(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movw	%ax, -240(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -232(%rbp)
	movq	%rax, -576(%rbp)
	movups	%xmm0, -216(%rbp)
	movq	$0, -248(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -576(%rbp,%rax)
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1120(%rbp), %xmm1
	movq	%rax, -464(%rbp)
	leaq	-512(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1120(%rbp)
	movaps	%xmm1, -576(%rbp)
	movaps	%xmm0, -560(%rbp)
	movaps	%xmm0, -544(%rbp)
	movaps	%xmm0, -528(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	leaq	-568(%rbp), %rsi
	movq	%rax, -568(%rbp)
	leaq	-480(%rbp), %rax
	movq	%rax, -1128(%rbp)
	movq	%rax, -496(%rbp)
	movl	$16, -504(%rbp)
	movq	$0, -488(%rbp)
	movb	$0, -480(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	0(%r13), %rax
	movq	168(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2040
	movq	%r12, %rsi
	call	_ZNK2v88internal8compiler19SourcePositionTable9PrintJsonERSo@PLT
.L1936:
	movq	%r12, %rdi
	movl	$18, %edx
	leaq	.LC153(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	%r12, %rsi
	leaq	-592(%rbp), %r12
	movq	176(%rax), %rdi
	call	_ZNK2v88internal8compiler15NodeOriginTable9PrintJsonERSo@PLT
	movq	0(%r13), %rax
	movq	%r12, -608(%rbp)
	leaq	-608(%rbp), %r9
	movq	$0, -600(%rbp)
	movq	%rax, -1136(%rbp)
	movq	-528(%rbp), %rax
	movb	$0, -592(%rbp)
	testq	%rax, %rax
	je	.L1937
	movq	-544(%rbp), %r8
	movq	-536(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1938
	subq	%rcx, %rax
	movq	%rax, %r8
.L2030:
	movq	%r9, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, -1144(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1144(%rbp), %r9
.L1939:
	movq	-1136(%rbp), %rdi
	movq	%r9, %rsi
	addq	$384, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-608(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1940
	call	_ZdlPv@PLT
.L1940:
	movq	.LC157(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-496(%rbp), %rdi
	movq	%rax, -464(%rbp)
	movhps	.LC76(%rip), %xmm0
	movaps	%xmm0, -576(%rbp)
	cmpq	-1128(%rbp), %rdi
	je	.L1941
	call	_ZdlPv@PLT
.L1941:
	movq	-1120(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -568(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r14, %rdi
	movq	%rax, -576(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -576(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -464(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
.L1930:
	cmpq	$0, 152(%rbx)
	je	.L1942
	movq	144(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1943
	movq	136(%rbx), %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1943:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 160(%rbx)
	movups	%xmm0, 176(%rbx)
	movups	%xmm0, 192(%rbx)
	movups	%xmm0, 208(%rbx)
	movups	%xmm0, 224(%rbx)
.L1942:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1944
	leaq	.LC154(%rip), %rsi
	call	_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc@PLT
.L1944:
	movq	-1096(%rbp), %rax
	movzbl	_ZN2v88internal28FLAG_turbo_verify_allocationE(%rip), %r14d
	movl	60(%rax), %edi
	testl	%edi, %edi
	jne	.L2041
	movq	24(%rbx), %rax
	cmpl	$1, 4(%rax)
	je	.L1948
	call	_ZN2v88internal21RegisterConfiguration9PoisoningEv@PLT
.L2031:
	movq	-1096(%rbp), %rdx
	movq	%rax, %rsi
	movl	%r14d, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl17AllocateRegistersEPKNS0_21RegisterConfigurationEPNS1_14CallDescriptorEb
.L1946:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl31VerifyGeneratedCodeIsIdempotentEv
	movq	0(%r13), %r12
	movq	56(%r12), %r14
	testq	%r14, %r14
	je	.L1949
	leaq	.LC155(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1949:
	movq	176(%r12), %rdx
	movq	48(%r12), %r8
	testq	%rdx, %rdx
	je	.L1950
	leaq	.LC155(%rip), %rax
	movq	48(%rdx), %rcx
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, 48(%rdx)
	movq	%rcx, -1128(%rbp)
	movq	%rdx, -1120(%rbp)
	movq	%r8, -1096(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	movq	0(%r13), %rax
	movq	272(%rax), %rsi
	call	_ZN2v88internal8compiler11FrameEliderC1EPNS1_19InstructionSequenceE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler11FrameElider3RunEv@PLT
	movq	-1120(%rbp), %rdx
	movq	-1128(%rbp), %rcx
	movq	-1096(%rbp), %r8
	movq	%rcx, 48(%rdx)
.L1959:
	testq	%r12, %r12
	je	.L1951
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L1951:
	testq	%r14, %r14
	je	.L1952
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L1952:
	cmpb	$0, 64(%rbx)
	movq	0(%r13), %r12
	jne	.L2042
	cmpb	$0, _ZN2v88internal13FLAG_turbo_jtE(%rip)
	jne	.L2043
.L1955:
	movq	56(%rbx), %rdi
	movb	$1, -1104(%rbp)
	testq	%rdi, %rdi
	je	.L1892
.L2032:
	call	_ZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEv@PLT
.L1892:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2044
	movzbl	-1104(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1895:
	.cfi_restore_state
	movq	_ZN2v88internal31FLAG_turbo_verify_machine_graphE(%rip), %rdi
	leaq	-1056(%rbp), %r15
	testq	%rdi, %rdi
	je	.L1897
	cmpb	$42, (%rdi)
	jne	.L1964
	cmpb	$0, 1(%rdi)
	je	.L1896
	.p2align 4,,10
	.p2align 3
.L1964:
	movq	32(%rbx), %rsi
	leaq	-1056(%rbp), %r15
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L1897
	cmpb	$0, _ZN2v88internal21FLAG_trace_verify_csaE(%rip)
	je	.L1899
	.p2align 4,,10
	.p2align 3
.L2035:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2045
	call	_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv@PLT
	movq	%rax, %r14
.L1901:
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%r14), %rsi
	je	.L1902
	testq	%rsi, %rsi
	je	.L2046
.L1903:
	addl	$1, 152(%r14)
.L1902:
	leaq	-576(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movl	$51, %edx
	leaq	.LC145(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$14, %edx
	leaq	.LC146(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%rbx), %r15
	testq	%r15, %r15
	je	.L2047
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1905:
	movq	%r12, %rdi
	movl	$23, %edx
	leaq	.LC147(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$51, %edx
	movq	%r12, %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	232(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_8ScheduleE@PLT
	movl	$51, %edx
	leaq	.LC145(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$11, %edx
	leaq	.LC148(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	32(%rbx), %r15
	testq	%r15, %r15
	je	.L2048
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1907:
	movl	$23, %edx
	leaq	.LC147(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC145(%rip), %rsi
	movl	$51, %edx
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rax, -496(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdx, %xmm0
	leaq	-512(%rbp), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -576(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-496(%rbp), %rdi
	movq	%rax, -576(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -496(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L1899
	subl	$1, 152(%r14)
	jne	.L1899
	movq	144(%r14), %rdi
	call	fclose@PLT
	movq	$0, 144(%r14)
	jmp	.L1899
	.p2align 4,,10
	.p2align 3
.L2038:
	leaq	-576(%rbp), %r12
	movl	$1, %edx
	movq	%r10, -1120(%rbp)
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode@PLT
	movq	%r12, %rdi
	movl	$9, %edx
	leaq	.LC65(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$23, %edx
	leaq	.LC150(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$23, %edx
	movq	%r12, %rdi
	leaq	.LC151(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1120(%rbp), %r10
	leaq	-1088(%rbp), %rsi
	movq	%r12, %rdi
	movq	272(%r10), %rax
	movq	%rax, -1088(%rbp)
	leaq	-672(%rbp), %rax
	movq	%rax, -1080(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_23InstructionRangesAsJSONE
	movl	$3, %edx
	leaq	.LC67(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileD1Ev@PLT
	jmp	.L1922
	.p2align 4,,10
	.p2align 3
.L2034:
	movq	232(%rbx), %rdx
	movq	160(%rbx), %rsi
	movq	24(%rbx), %rdi
	movq	(%rbx), %rcx
	call	_ZN2v88internal8compiler22BasicBlockInstrumentor10InstrumentEPNS0_24OptimizedCompilationInfoEPNS1_5GraphEPNS1_8ScheduleEPNS0_7IsolateE@PLT
	movq	%rax, 376(%rbx)
	jmp	.L1893
	.p2align 4,,10
	.p2align 3
.L2041:
	call	_ZN2v88internal21RegisterConfiguration24RestrictGeneralRegistersEj@PLT
	movq	-1096(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler12PipelineImpl17AllocateRegistersEPKNS0_21RegisterConfigurationEPNS1_14CallDescriptorEb
	testq	%r12, %r12
	je	.L1946
	movq	(%r12), %rax
	leaq	_ZN2v88internal21RegisterConfigurationD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1947
	movl	$336, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1946
	.p2align 4,,10
	.p2align 3
.L1950:
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r8, -1096(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	movq	0(%r13), %rax
	movq	272(%rax), %rsi
	call	_ZN2v88internal8compiler11FrameEliderC1EPNS1_19InstructionSequenceE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler11FrameElider3RunEv@PLT
	movq	-1096(%rbp), %r8
	jmp	.L1959
	.p2align 4,,10
	.p2align 3
.L2043:
	movq	272(%r12), %rax
	movq	56(%r12), %rdi
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movzbl	125(%rax), %eax
	movq	%rdi, -1056(%rbp)
	movb	%al, -1096(%rbp)
	testq	%rdi, %rdi
	je	.L1956
	leaq	.LC156(%rip), %rsi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L1956:
	movq	48(%r12), %rdi
	leaq	.LC3(%rip), %rax
	movq	$0, -1032(%rbp)
	movq	%rax, -1048(%rbp)
	movq	%rdi, -1040(%rbp)
	movq	176(%r12), %rax
	movq	%rax, -1024(%rbp)
	testq	%rax, %rax
	je	.L1957
	movq	48(%rax), %rdx
	movq	%rdx, -1016(%rbp)
	leaq	.LC156(%rip), %rdx
	movq	%rdx, 48(%rax)
.L1957:
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	0(%r13), %r14
	movzbl	-1096(%rbp), %ecx
	movq	$0, -1080(%rbp)
	movq	%rax, -1032(%rbp)
	movq	%rax, %rdi
	leaq	-1088(%rbp), %r13
	movq	%rax, %r12
	movq	%rax, -1088(%rbp)
	movq	%r13, %rsi
	movq	$0, -1072(%rbp)
	movq	$0, -1064(%rbp)
	movq	272(%r14), %rdx
	call	_ZN2v88internal8compiler13JumpThreading17ComputeForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceEb@PLT
	testb	%al, %al
	jne	.L2049
.L1958:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116PipelineRunScopeD1Ev
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L2039:
	movl	$2, %esi
	call	_ZN2v88internal24OptimizedCompilationInfo17AbortOptimizationENS0_13BailoutReasonE@PLT
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2033
	call	_ZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEv@PLT
.L2033:
	movb	$0, -1104(%rbp)
	jmp	.L1892
	.p2align 4,,10
	.p2align 3
.L1948:
	call	_ZN2v88internal21RegisterConfiguration7DefaultEv@PLT
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2040:
	leaq	.LC152(%rip), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	jmp	.L1936
	.p2align 4,,10
	.p2align 3
.L2049:
	movq	272(%r14), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13JumpThreading15ApplyForwardingEPNS0_4ZoneERNS0_10ZoneVectorINS1_9RpoNumberEEEPNS1_19InstructionSequenceE@PLT
	jmp	.L1958
	.p2align 4,,10
	.p2align 3
.L2042:
	movq	24(%r12), %rdi
	movl	$9, %esi
	call	_ZN2v88internal24OptimizedCompilationInfo17AbortOptimizationENS0_13BailoutReasonE@PLT
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L2032
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L1931:
	movq	(%rax), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12TurboCfgFileC1EPNS0_7IsolateE@PLT
	movq	168(%rbx), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movdqu	264(%rbx), %xmm3
	movdqu	232(%rbx), %xmm0
	movq	%rax, -1040(%rbp)
	leaq	.LC139(%rip), %rax
	shufpd	$2, %xmm3, %xmm0
	movq	%rax, -1032(%rbp)
	movaps	%xmm0, -1056(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_5AsC1VE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12TurboCfgFileD1Ev@PLT
	movq	0(%r13), %rax
	movq	24(%rax), %rax
	movl	(%rax), %eax
	testb	$64, %ah
	je	.L1930
	jmp	.L1934
	.p2align 4,,10
	.p2align 3
.L1938:
	subq	%rcx, %r8
	jmp	.L2030
	.p2align 4,,10
	.p2align 3
.L2047:
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1905
	.p2align 4,,10
	.p2align 3
.L2048:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1907
	.p2align 4,,10
	.p2align 3
.L2045:
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	movq	%rax, %r14
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L2036:
	movq	%rdx, %rdi
	movl	$456, %esi
	movq	%rcx, -1128(%rbp)
	movq	%rdx, -1104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-1104(%rbp), %rdx
	movq	-1128(%rbp), %rcx
	movq	%rax, %r12
	jmp	.L1912
	.p2align 4,,10
	.p2align 3
.L2037:
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1917
	.p2align 4,,10
	.p2align 3
.L2046:
	movq	(%r14), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%r14)
	movq	%rax, %rsi
	jmp	.L1903
	.p2align 4,,10
	.p2align 3
.L1937:
	movq	%r9, %rdi
	leaq	-496(%rbp), %rsi
	movq	%r9, -1144(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-1144(%rbp), %r9
	jmp	.L1939
	.p2align 4,,10
	.p2align 3
.L1947:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1946
.L2044:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26834:
	.size	_ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE, .-_ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE
	.section	.rodata._ZN2v88internal8compiler8Pipeline29GenerateCodeForWasmNativeStubEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorEPNS1_12MachineGraphENS0_4Code4KindEiPKcRKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE.str1.8,"aMS",@progbits,1
	.align 8
.LC158:
	.string	"../deps/v8/src/compiler/pipeline.cc:161"
	.align 8
.LC159:
	.string	"../deps/v8/src/compiler/pipeline.cc:169"
	.align 8
.LC160:
	.string	"../deps/v8/src/compiler/pipeline.cc:171"
	.align 8
.LC161:
	.string	"../deps/v8/src/compiler/pipeline.cc:173"
	.section	.rodata._ZN2v88internal8compiler8Pipeline29GenerateCodeForWasmNativeStubEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorEPNS1_12MachineGraphENS0_4Code4KindEiPKcRKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE.str1.1,"aMS",@progbits,1
.LC162:
	.string	"V8.WasmStubCodegen"
.LC163:
	.string	"Begin compiling method "
.LC164:
	.string	"-- wasm stub "
.LC165:
	.string	" graph -- "
.LC166:
	.string	"{\"function\":\""
.LC167:
	.string	"\", \"source\":\"\",\n\"phases\":["
.LC168:
	.string	"V8.WasmNativeStubMachineCode"
	.section	.rodata._ZN2v88internal8compiler8Pipeline29GenerateCodeForWasmNativeStubEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorEPNS1_12MachineGraphENS0_4Code4KindEiPKcRKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE.str1.8
	.align 8
.LC169:
	.string	"pipeline.SelectInstructions(&linkage)"
	.section	.rodata._ZN2v88internal8compiler8Pipeline29GenerateCodeForWasmNativeStubEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorEPNS1_12MachineGraphENS0_4Code4KindEiPKcRKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE.str1.1
.LC170:
	.string	"\"}\n]"
	.section	.text._ZN2v88internal8compiler8Pipeline29GenerateCodeForWasmNativeStubEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorEPNS1_12MachineGraphENS0_4Code4KindEiPKcRKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Pipeline29GenerateCodeForWasmNativeStubEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorEPNS1_12MachineGraphENS0_4Code4KindEiPKcRKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE
	.type	_ZN2v88internal8compiler8Pipeline29GenerateCodeForWasmNativeStubEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorEPNS1_12MachineGraphENS0_4Code4KindEiPKcRKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE, @function
_ZN2v88internal8compiler8Pipeline29GenerateCodeForWasmNativeStubEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorEPNS1_12MachineGraphENS0_4Code4KindEiPKcRKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE:
.LFB26739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$2152, %rsp
	.cfi_offset 3, -56
	movq	32(%rbp), %rcx
	movq	%rsi, -2120(%rbp)
	movq	(%r12), %rbx
	movq	16(%rbp), %r15
	movq	%rdx, -2128(%rbp)
	movl	%r8d, -2144(%rbp)
	movq	24(%rbp), %r14
	movq	%rcx, -2152(%rbp)
	movq	%r15, %rdi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	(%rbx), %rcx
	movq	%rcx, -2112(%rbp)
	call	strlen@PLT
	movl	-2144(%rbp), %r8d
	movq	-2112(%rbp), %rcx
	movq	%r15, %rsi
	movq	%rax, %rdx
	leaq	-1968(%rbp), %rdi
	movq	%rdi, -2104(%rbp)
	call	_ZN2v88internal24OptimizedCompilationInfoC1ENS0_6VectorIKcEEPNS0_4ZoneENS0_4Code4KindE@PLT
	movq	-2120(%rbp), %rax
	leaq	408(%rax), %r15
	leaq	-2048(%rbp), %rax
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%rax, -2112(%rbp)
	call	_ZN2v88internal8compiler9ZoneStatsC1EPNS0_19AccountingAllocatorE@PLT
	movq	(%rbx), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	$87, %rax
	jbe	.L2162
	leaq	88(%rdx), %rax
	movq	%rax, 16(%rdi)
.L2052:
	leaq	.LC158(%rip), %r8
	movq	%rdx, %rdi
	movq	%rbx, %rsi
	movq	%rdx, -2160(%rbp)
	movq	%r8, %xmm0
	movhps	-2112(%rbp), %xmm0
	movaps	%xmm0, -2176(%rbp)
	call	_ZN2v88internal8compiler15NodeOriginTableC1EPNS1_5GraphE@PLT
	leaq	-2096(%rbp), %rdi
	call	_ZN2v88internal4wasm21WasmInstructionBuffer3NewEv@PLT
	movq	-2120(%rbp), %rax
	movq	-2104(%rbp), %rsi
	leaq	-1008(%rbp), %rdi
	movq	$0, -1040(%rbp)
	movq	%rax, -1032(%rbp)
	movq	%rsi, -1016(%rbp)
	movq	%r15, -1024(%rbp)
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	xorl	%r8d, %r8d
	movq	-2112(%rbp), %rdi
	movdqa	-2176(%rbp), %xmm0
	movw	%r8w, -976(%rbp)
	pxor	%xmm2, %xmm2
	leaq	.LC158(%rip), %r8
	movq	%r8, %rsi
	movq	%rdi, -992(%rbp)
	movaps	%xmm2, -944(%rbp)
	movaps	%xmm0, -912(%rbp)
	movb	$0, -1000(%rbp)
	movq	$0, -984(%rbp)
	movl	$-1, -972(%rbp)
	movb	$0, -968(%rbp)
	movb	$0, -960(%rbp)
	movq	$0, -928(%rbp)
	movl	$0, -920(%rbp)
	movb	$0, -916(%rbp)
	movq	$0, -896(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-2160(%rbp), %rdx
	pxor	%xmm2, %xmm2
	movq	-992(%rbp), %rdi
	movq	%rax, %xmm0
	movq	(%r12), %rax
	leaq	.LC159(%rip), %rsi
	movaps	%xmm2, -832(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, -864(%rbp)
	movq	%rax, -880(%rbp)
	movq	-2152(%rbp), %rax
	movq	%rsi, -800(%rbp)
	movq	%rax, -872(%rbp)
	movq	16(%r12), %rax
	movq	%rdi, -792(%rbp)
	movq	%rax, -848(%rbp)
	movq	8(%r12), %rax
	movaps	%xmm0, -896(%rbp)
	movq	$0, -856(%rbp)
	movq	%rax, -840(%rbp)
	movq	%r12, -816(%rbp)
	movq	$0, -808(%rbp)
	movq	$0, -784(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-992(%rbp), %rdi
	leaq	.LC160(%rip), %rsi
	movq	$0, -768(%rbp)
	movq	%rax, %xmm0
	movq	%rsi, -760(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rdi, -752(%rbp)
	movaps	%xmm0, -784(%rbp)
	movq	$0, -744(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-992(%rbp), %rdi
	pxor	%xmm2, %xmm2
	leaq	.LC161(%rip), %rsi
	movq	%rax, %xmm0
	movups	%xmm2, -728(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rsi, -704(%rbp)
	movups	%xmm0, -744(%rbp)
	movq	$0, -712(%rbp)
	movq	%rdi, -696(%rbp)
	movq	$0, -688(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movdqu	(%r14), %xmm3
	pxor	%xmm2, %xmm2
	cmpb	$0, _ZN2v88internal16FLAG_turbo_statsE(%rip)
	movq	%rax, %xmm0
	leaq	-640(%rbp), %rax
	movq	$0, -648(%rbp)
	movq	%rax, -656(%rbp)
	movq	16(%r14), %rax
	punpcklqdq	%xmm0, %xmm0
	movb	$0, -640(%rbp)
	movq	$0, -624(%rbp)
	movq	%rax, -600(%rbp)
	movq	$0, -592(%rbp)
	movaps	%xmm0, -688(%rbp)
	movaps	%xmm2, -672(%rbp)
	movups	%xmm3, -616(%rbp)
	jne	.L2053
	cmpb	$0, _ZN2v88internal20FLAG_turbo_stats_nvpE(%rip)
	movq	$0, -2152(%rbp)
	jne	.L2053
.L2054:
	leaq	-1040(%rbp), %rax
	movq	%rax, -2120(%rbp)
	movq	%rax, -2088(%rbp)
	movl	-1968(%rbp), %eax
	testb	$-64, %ah
	je	.L2055
	movq	-1032(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2163
	call	_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv@PLT
	movq	%rax, %r12
.L2057:
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%r12), %rsi
	je	.L2058
	testq	%rsi, %rsi
	je	.L2164
.L2059:
	addl	$1, 152(%r12)
.L2058:
	leaq	-1792(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movl	$52, %edx
	leaq	.LC106(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$23, %edx
	leaq	.LC163(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2104(%rbp), %rsi
	leaq	-2080(%rbp), %rdi
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movq	-2080(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L2165
	movq	%rsi, %rdi
	movq	%rsi, -2160(%rbp)
	call	strlen@PLT
	movq	-2160(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2061:
	movq	%r15, %rdi
	movl	$15, %edx
	leaq	.LC108(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1792(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-1552(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2070
	cmpb	$0, 56(%rdi)
	je	.L2063
	movsbl	67(%rdi), %esi
.L2064:
	movq	%r15, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-2080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2065
	call	_ZdaPv@PLT
.L2065:
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, -1712(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-1728(%rbp), %rdi
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -1792(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-1712(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1712(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L2067
	subl	$1, 152(%r12)
	je	.L2166
.L2067:
	movl	-1968(%rbp), %eax
.L2055:
	testb	$-128, %ah
	jne	.L2167
.L2069:
	testb	$64, %ah
	jne	.L2168
.L2073:
	leaq	-2088(%rbp), %r12
	leaq	.LC168(%rip), %rsi
	movq	%r12, %rdi
	leaq	-2072(%rbp), %r15
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl21ComputeScheduledGraphEv
	movq	-2128(%rbp), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -2072(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE
	testb	%al, %al
	je	.L2169
	movq	-2096(%rbp), %rsi
	leaq	-2064(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm21WasmInstructionBuffer10CreateViewEv@PLT
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2076
	movq	(%rdi), %rax
	call	*8(%rax)
.L2076:
	movq	-2088(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	104(%rax), %rbx
	movups	%xmm0, 8(%r13)
	movups	%xmm0, 24(%r13)
	movups	%xmm0, 40(%r13)
	pxor	%xmm0, %xmm0
	leaq	760(%rbx), %rcx
	leaq	200(%rbx), %rdi
	movq	$0, 0(%r13)
	movq	$0, 56(%r13)
	movl	$0, 64(%r13)
	movq	$0, 88(%r13)
	movq	$0, 96(%r13)
	movq	$0, 104(%r13)
	movq	$0, 112(%r13)
	movq	$0, 120(%r13)
	movl	$-1, 128(%r13)
	movb	$0, 134(%r13)
	movups	%xmm0, 72(%r13)
	movl	1080(%rbx), %r8d
	call	_ZN2v88internal9Assembler7GetCodeEPNS0_7IsolateEPNS0_8CodeDescEPNS0_21SafepointTableBuilderEi@PLT
	movq	-2096(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm21WasmInstructionBuffer13ReleaseBufferEv@PLT
	movq	-2064(%rbp), %rax
	movq	80(%r13), %rdi
	movq	$0, -2064(%rbp)
	movq	%rax, 80(%r13)
	testq	%rdi, %rdi
	je	.L2078
	call	_ZdaPv@PLT
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2078
	call	_ZdaPv@PLT
.L2078:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeGenerator22GetSourcePositionTableEv@PLT
	movq	-2064(%rbp), %rax
	movq	96(%r13), %rdi
	movq	$0, -2064(%rbp)
	movq	%rax, 96(%r13)
	testq	%rdi, %rdi
	je	.L2170
	call	_ZdaPv@PLT
	movq	-2056(%rbp), %rax
	movq	-2064(%rbp), %rdi
	movq	%rax, 104(%r13)
	testq	%rdi, %rdi
	je	.L2081
	call	_ZdaPv@PLT
.L2081:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeGenerator24GetProtectedInstructionsEv@PLT
	movq	-2064(%rbp), %rax
	movq	112(%r13), %rdi
	movq	$0, -2064(%rbp)
	movq	%rax, 112(%r13)
	testq	%rdi, %rdi
	je	.L2171
	call	_ZdaPv@PLT
	movq	-2056(%rbp), %rax
	movq	-2064(%rbp), %rdi
	movq	%rax, 120(%r13)
	testq	%rdi, %rdi
	je	.L2083
	call	_ZdaPv@PLT
.L2083:
	movq	24(%rbx), %rax
	movq	-2128(%rbp), %rdi
	movq	(%rax), %rax
	movl	4(%rax), %eax
	movl	%eax, 88(%r13)
	call	_ZNK2v88internal8compiler14CallDescriptor23GetTaggedParameterSlotsEv@PLT
	movb	$3, 133(%r13)
	movl	%eax, 92(%r13)
	movl	-1968(%rbp), %eax
	testb	$64, %ah
	jne	.L2172
.L2084:
	testb	$-64, %ah
	je	.L2092
	movq	-1032(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2173
	call	_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv@PLT
	movq	%rax, %rbx
.L2094:
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%rbx), %rsi
	je	.L2095
	testq	%rsi, %rsi
	je	.L2174
.L2096:
	addl	$1, 152(%rbx)
.L2095:
	leaq	-576(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movl	$52, %edx
	leaq	.LC106(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$26, %edx
	leaq	.LC107(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2104(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movq	-2064(%rbp), %r14
	testq	%r14, %r14
	je	.L2175
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2098:
	movl	$15, %edx
	leaq	.LC108(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-336(%rbp,%rax), %r14
	testq	%r14, %r14
	je	.L2070
	cmpb	$0, 56(%r14)
	je	.L2099
	movsbl	67(%r14), %esi
.L2100:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2101
	call	_ZdaPv@PLT
.L2101:
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, -496(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-512(%rbp), %rdi
	movq	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -576(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-496(%rbp), %rdi
	movq	%rax, -576(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -496(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L2092
	subl	$1, 152(%rbx)
	je	.L2176
.L2092:
	movq	-2152(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2105
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18PipelineStatisticsD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L2105:
	movq	-2120(%rbp), %rdi
	call	_ZN2v88internal8compiler12PipelineDataD1Ev
	movq	-2096(%rbp), %r12
	testq	%r12, %r12
	je	.L2106
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm21WasmInstructionBufferD1Ev@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2106:
	movq	-2112(%rbp), %rdi
	call	_ZN2v88internal8compiler9ZoneStatsD1Ev@PLT
	movq	-2104(%rbp), %rdi
	call	_ZN2v88internal24OptimizedCompilationInfoD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2177
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2053:
	.cfi_restore_state
	movq	-2120(%rbp), %rdi
	call	_ZN2v88internal4wasm10WasmEngine26GetOrCreateTurboStatisticsEv@PLT
	movl	$176, %edi
	movq	%rax, %r12
	call	_ZN2v88internal8MallocednwEm@PLT
	movq	-2104(%rbp), %rsi
	movq	-2112(%rbp), %rcx
	movq	%r12, %rdx
	movq	%rax, %r15
	movq	%rax, %rdi
	movq	%rax, -2152(%rbp)
	call	_ZN2v88internal8compiler18PipelineStatisticsC1EPNS0_24OptimizedCompilationInfoEPNS0_21CompilationStatisticsEPNS1_9ZoneStatsE@PLT
	leaq	.LC162(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc@PLT
	jmp	.L2054
	.p2align 4,,10
	.p2align 3
.L2170:
	movq	-2056(%rbp), %rax
	movq	%rax, 104(%r13)
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L2063:
	movq	%rdi, -2160(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-2160(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2064
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2064
	.p2align 4,,10
	.p2align 3
.L2171:
	movq	-2056(%rbp), %rax
	movq	%rax, 120(%r13)
	jmp	.L2083
	.p2align 4,,10
	.p2align 3
.L2168:
	movq	-2104(%rbp), %rbx
	leaq	-576(%rbp), %r12
	movl	$32, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode@PLT
	movl	$13, %edx
	leaq	.LC166(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-2064(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movq	-2064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	$26, %edx
	leaq	.LC167(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2074
	call	_ZdaPv@PLT
.L2074:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileD1Ev@PLT
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L2167:
	leaq	-1360(%rbp), %r15
	leaq	-1440(%rbp), %r12
	movq	%r15, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	stdout(%rip), %rdx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%si, -1136(%rbp)
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -1128(%rbp)
	movups	%xmm0, -1112(%rbp)
	movq	%rax, -2160(%rbp)
	movq	%rax, -1360(%rbp)
	movq	$0, -1144(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movl	$13, %edx
	movq	%r12, %rdi
	leaq	.LC164(%rip), %rsi
	movq	%rax, -1440(%rbp)
	addq	$40, %rax
	movq	%rax, -1360(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-2144(%rbp), %edi
	call	_ZN2v88internal4Code11Kind2StringENS1_4KindE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	$10, %edx
	leaq	.LC165(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movq	-24(%rax), %rax
	movq	240(%r12,%rax), %r14
	testq	%r14, %r14
	je	.L2070
	cmpb	$0, 56(%r14)
	je	.L2071
	movsbl	67(%r14), %esi
.L2072:
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	-2064(%rbp), %rsi
	movq	%rbx, -2064(%rbp)
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rbx
	movq	%rax, %rdi
	call	_ZN2v88internal8compilerlsERSoRKNS1_5AsRPOE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rbx, %xmm0
	leaq	-1376(%rbp), %rdi
	movq	%rax, -1360(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -1440(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -1440(%rbp)
	movq	-2160(%rbp), %rax
	movq	%rax, -1360(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movl	-1968(%rbp), %eax
	jmp	.L2069
	.p2align 4,,10
	.p2align 3
.L2099:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2100
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2172:
	movq	.LC74(%rip), %xmm1
	leaq	-576(%rbp), %r12
	movq	-2104(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	addq	$1248, %rbx
	leaq	-1360(%rbp), %r15
	movhps	.LC75(%rip), %xmm1
	movaps	%xmm1, -2144(%rbp)
	call	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode@PLT
	movl	$42, %edx
	leaq	.LC100(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rbx, -2064(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_17BlockStartsAsJSONE
	movl	$8, %edx
	leaq	.LC101(%rip), %rsi
	leaq	-1440(%rbp), %rbx
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-1312(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2128(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	xorl	%eax, %eax
	xorl	%esi, %esi
	movq	%rcx, -1312(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rcx, -2160(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -1080(%rbp)
	movq	%rcx, -1440(%rbp)
	movups	%xmm0, -1064(%rbp)
	movq	$0, -1096(%rbp)
	movw	%ax, -1088(%rbp)
	movq	-24(%rcx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -1440(%rbp,%rax)
	movq	-1440(%rbp), %rax
	movq	$0, -1432(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%esi, %esi
	movq	%rbx, -1424(%rbp)
	movq	-24(%rbx), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -1424(%rbp,%rax)
	movq	-1424(%rbp), %rax
	leaq	-1424(%rbp), %rbx
	movq	-24(%rax), %rcx
	addq	%rbx, %rcx
	movq	%rcx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movdqa	-2144(%rbp), %xmm1
	movq	%rcx, -1440(%rbp)
	movq	-24(%rcx), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -1440(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -1440(%rbp)
	addq	$80, %rcx
	movaps	%xmm1, -1424(%rbp)
	movaps	%xmm0, -1408(%rbp)
	movaps	%xmm0, -1392(%rbp)
	movaps	%xmm0, -1376(%rbp)
	movq	%rcx, -1312(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-2128(%rbp), %rdi
	leaq	-1328(%rbp), %rcx
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-1416(%rbp), %rsi
	movq	%rdx, -1416(%rbp)
	movq	%rcx, -2176(%rbp)
	movq	%rcx, -1344(%rbp)
	movl	$24, -1352(%rbp)
	movq	$0, -1336(%rbp)
	movb	$0, -1328(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	subq	$8, %rsp
	movq	0(%r13), %rdx
	xorl	%edi, %edi
	movslq	16(%r13), %rcx
	pushq	$0
	movl	$3, %r8d
	movq	%r13, %r9
	movq	%rbx, %rsi
	addq	%rdx, %rcx
	call	_ZN2v88internal12Disassembler6DecodeEPNS0_7IsolateEPSoPhS5_NS0_13CodeReferenceEm@PLT
	leaq	-1776(%rbp), %rax
	popq	%rdx
	popq	%rcx
	movq	%rax, -2144(%rbp)
	leaq	-1792(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	-1376(%rbp), %rax
	movq	$0, -1784(%rbp)
	movb	$0, -1776(%rbp)
	testq	%rax, %rax
	je	.L2085
	movq	-1392(%rbp), %r8
	movq	-1384(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L2178
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L2087:
	movq	-1792(%rbp), %rdi
	movq	-1784(%rbp), %rax
	addq	%rdi, %rax
	cmpq	%rdi, %rax
	je	.L2088
	movq	%r13, -2184(%rbp)
	movq	%rax, %rbx
	movq	%rdi, %r13
	.p2align 4,,10
	.p2align 3
.L2089:
	movsbw	0(%r13), %ax
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$1, %r13
	movw	%ax, -2064(%rbp)
	call	_ZN2v88internallsERSoRKNS0_20AsEscapedUC16ForJSONE@PLT
	cmpq	%r13, %rbx
	jne	.L2089
	movq	-2184(%rbp), %r13
	movq	-1792(%rbp), %rdi
.L2088:
	cmpq	-2144(%rbp), %rdi
	je	.L2090
	call	_ZdlPv@PLT
.L2090:
	movl	$4, %edx
	leaq	.LC170(%rip), %rsi
	movq	%r12, %rdi
	movq	.LC74(%rip), %xmm0
	movhps	.LC76(%rip), %xmm0
	movaps	%xmm0, -2144(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	$2, %edx
	leaq	.LC105(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-2144(%rbp), %xmm0
	movq	-1344(%rbp), %rdi
	movq	%rax, -1440(%rbp)
	addq	$80, %rax
	movq	%rax, -1312(%rbp)
	movaps	%xmm0, -1424(%rbp)
	cmpq	-2176(%rbp), %rdi
	je	.L2091
	call	_ZdlPv@PLT
.L2091:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -1416(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	-2128(%rbp), %rdi
	movq	%rax, -1440(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -1440(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -1424(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -1424(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -1440(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -1440(%rbp,%rax)
	movq	-2160(%rbp), %rax
	movq	$0, -1432(%rbp)
	movq	%rax, -1312(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileD1Ev@PLT
	movl	-1968(%rbp), %eax
	jmp	.L2084
	.p2align 4,,10
	.p2align 3
.L2163:
	movq	-1040(%rbp), %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	movq	%rax, %r12
	jmp	.L2057
	.p2align 4,,10
	.p2align 3
.L2165:
	movq	-1792(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2061
	.p2align 4,,10
	.p2align 3
.L2175:
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2098
	.p2align 4,,10
	.p2align 3
.L2173:
	movq	-1040(%rbp), %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	movq	%rax, %rbx
	jmp	.L2094
	.p2align 4,,10
	.p2align 3
.L2071:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2072
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2072
	.p2align 4,,10
	.p2align 3
.L2178:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L2087
	.p2align 4,,10
	.p2align 3
.L2166:
	movq	144(%r12), %rdi
	call	fclose@PLT
	movq	$0, 144(%r12)
	jmp	.L2067
	.p2align 4,,10
	.p2align 3
.L2176:
	movq	144(%rbx), %rdi
	call	fclose@PLT
	movq	$0, 144(%rbx)
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L2162:
	movl	$88, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	jmp	.L2052
	.p2align 4,,10
	.p2align 3
.L2169:
	leaq	.LC169(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2164:
	movq	(%r12), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%r12)
	movq	%rax, %rsi
	jmp	.L2059
	.p2align 4,,10
	.p2align 3
.L2174:
	movq	(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %rsi
	jmp	.L2096
	.p2align 4,,10
	.p2align 3
.L2085:
	leaq	-1344(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L2087
.L2070:
	call	_ZSt16__throw_bad_castv@PLT
.L2177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26739:
	.size	_ZN2v88internal8compiler8Pipeline29GenerateCodeForWasmNativeStubEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorEPNS1_12MachineGraphENS0_4Code4KindEiPKcRKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE, .-_ZN2v88internal8compiler8Pipeline29GenerateCodeForWasmNativeStubEPNS0_4wasm10WasmEngineEPNS1_14CallDescriptorEPNS1_12MachineGraphENS0_4Code4KindEiPKcRKNS0_16AssemblerOptionsEPNS1_19SourcePositionTableE
	.section	.text._ZN2v88internal8compiler12PipelineImpl29SelectInstructionsAndAssembleEPNS1_14CallDescriptorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12PipelineImpl29SelectInstructionsAndAssembleEPNS1_14CallDescriptorE
	.type	_ZN2v88internal8compiler12PipelineImpl29SelectInstructionsAndAssembleEPNS1_14CallDescriptorE, @function
_ZN2v88internal8compiler12PipelineImpl29SelectInstructionsAndAssembleEPNS1_14CallDescriptorE:
.LFB26867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-56(%rbp), %r14
	movq	%rdi, %r13
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -56(%rbp)
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L2190
.L2179:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2191
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2190:
	.cfi_restore_state
	movq	%r13, %rdi
	leaq	-48(%rbp), %rdx
	movq	%r14, %rsi
	movq	$0, -48(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2179
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2179
.L2191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26867:
	.size	_ZN2v88internal8compiler12PipelineImpl29SelectInstructionsAndAssembleEPNS1_14CallDescriptorE, .-_ZN2v88internal8compiler12PipelineImpl29SelectInstructionsAndAssembleEPNS1_14CallDescriptorE
	.section	.text._ZN2v88internal8compiler12PipelineImpl12GenerateCodeEPNS1_14CallDescriptorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12PipelineImpl12GenerateCodeEPNS1_14CallDescriptorE
	.type	_ZN2v88internal8compiler12PipelineImpl12GenerateCodeEPNS1_14CallDescriptorE, @function
_ZN2v88internal8compiler12PipelineImpl12GenerateCodeEPNS1_14CallDescriptorE:
.LFB26868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-40(%rbp), %r13
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -40(%rbp)
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE
	testb	%al, %al
	jne	.L2193
	xorl	%eax, %eax
.L2194:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2201
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2193:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	-32(%rbp), %rdx
	movq	%r13, %rsi
	movq	$0, -32(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2195
	movq	(%rdi), %rax
	call	*8(%rax)
.L2195:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb
	jmp	.L2194
.L2201:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26868:
	.size	_ZN2v88internal8compiler12PipelineImpl12GenerateCodeEPNS1_14CallDescriptorE, .-_ZN2v88internal8compiler12PipelineImpl12GenerateCodeEPNS1_14CallDescriptorE
	.section	.rodata._ZN2v88internal8compiler8Pipeline22GenerateCodeForTestingEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_14CallDescriptorEPNS1_5GraphERKNS0_16AssemblerOptionsEPNS1_8ScheduleE.str1.1,"aMS",@progbits,1
.LC171:
	.string	"V8.TFTestCodegen"
.LC172:
	.string	"V8.TFMachineCode"
	.section	.text._ZN2v88internal8compiler8Pipeline22GenerateCodeForTestingEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_14CallDescriptorEPNS1_5GraphERKNS0_16AssemblerOptionsEPNS1_8ScheduleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Pipeline22GenerateCodeForTestingEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_14CallDescriptorEPNS1_5GraphERKNS0_16AssemblerOptionsEPNS1_8ScheduleE
	.type	_ZN2v88internal8compiler8Pipeline22GenerateCodeForTestingEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_14CallDescriptorEPNS1_5GraphERKNS0_16AssemblerOptionsEPNS1_8ScheduleE, @function
_ZN2v88internal8compiler8Pipeline22GenerateCodeForTestingEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_14CallDescriptorEPNS1_5GraphERKNS0_16AssemblerOptionsEPNS1_8ScheduleE:
.LFB26819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1120(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1144, %rsp
	movq	%rdx, -1160(%rbp)
	movq	41136(%rsi), %rsi
	movq	%r8, -1168(%rbp)
	movq	%r9, -1176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler9ZoneStatsC1EPNS0_19AccountingAllocatorE@PLT
	movq	64(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$87, %rdx
	jbe	.L2243
	leaq	88(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2204:
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rax, -1184(%rbp)
	leaq	-1040(%rbp), %r15
	call	_ZN2v88internal8compiler15NodeOriginTableC1EPNS1_5GraphE@PLT
	subq	$8, %rsp
	movq	%r13, %r9
	movq	%rbx, %rcx
	movq	-1184(%rbp), %rax
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	pushq	-1168(%rbp)
	pushq	$0
	pushq	%rax
	pushq	$0
	pushq	-1176(%rbp)
	movq	41136(%rbx), %r8
	call	_ZN2v88internal8compiler12PipelineDataC1EPNS1_9ZoneStatsEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS0_19AccountingAllocatorEPNS1_5GraphEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableEPNS0_20JumpOptimizationInfoERKNS0_16AssemblerOptionsE
	addq	$48, %rsp
	cmpb	$0, _ZN2v88internal16FLAG_turbo_statsE(%rip)
	jne	.L2205
	xorl	%r13d, %r13d
	cmpb	$0, _ZN2v88internal20FLAG_turbo_stats_nvpE(%rip)
	jne	.L2205
.L2206:
	movq	%r15, -1144(%rbp)
	testl	$16384, (%r12)
	jne	.L2244
.L2207:
	leaq	-1144(%rbp), %r12
	leaq	.LC172(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	cmpq	$0, -808(%rbp)
	je	.L2245
.L2211:
	movq	-1160(%rbp), %rax
	leaq	-1136(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rax, -1136(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE
	testb	%al, %al
	jne	.L2246
.L2213:
	xorl	%r12d, %r12d
.L2217:
	testq	%r13, %r13
	je	.L2218
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18PipelineStatisticsD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L2218:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12PipelineDataD1Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ZoneStatsD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2247
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2205:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal7Isolate18GetTurboStatisticsEv@PLT
	movl	$176, %edi
	movq	%rax, %rbx
	call	_ZN2v88internal8MallocednwEm@PLT
	movq	%r12, %rsi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatisticsC1EPNS0_24OptimizedCompilationInfoEPNS0_21CompilationStatisticsEPNS1_9ZoneStatsE@PLT
	leaq	.LC171(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc@PLT
	jmp	.L2206
	.p2align 4,,10
	.p2align 3
.L2246:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	leaq	-1128(%rbp), %rdx
	movq	$0, -1128(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE
	movq	-1128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2214
	movq	(%rdi), %rax
	call	*8(%rax)
.L2214:
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2213
	movq	-1144(%rbp), %rax
	movq	312(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2217
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23CompilationDependencies6CommitENS0_6HandleINS0_4CodeEEE@PLT
	testb	%al, %al
	jne	.L2217
	jmp	.L2213
	.p2align 4,,10
	.p2align 3
.L2244:
	leaq	-576(%rbp), %rbx
	movq	%r12, %rsi
	movl	$32, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode@PLT
	movl	$13, %edx
	leaq	.LC166(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rsi
	leaq	-1128(%rbp), %rdi
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movq	-1128(%rbp), %r12
	testq	%r12, %r12
	je	.L2248
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2209:
	movq	%rbx, %rdi
	movl	$26, %edx
	leaq	.LC167(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2210
	call	_ZdaPv@PLT
.L2210:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileD1Ev@PLT
	jmp	.L2207
	.p2align 4,,10
	.p2align 3
.L2245:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl21ComputeScheduledGraphEv
	jmp	.L2211
	.p2align 4,,10
	.p2align 3
.L2243:
	movl	$88, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2204
	.p2align 4,,10
	.p2align 3
.L2248:
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2209
.L2247:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26819:
	.size	_ZN2v88internal8compiler8Pipeline22GenerateCodeForTestingEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_14CallDescriptorEPNS1_5GraphERKNS0_16AssemblerOptionsEPNS1_8ScheduleE, .-_ZN2v88internal8compiler8Pipeline22GenerateCodeForTestingEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_14CallDescriptorEPNS1_5GraphERKNS0_16AssemblerOptionsEPNS1_8ScheduleE
	.section	.rodata._ZN2v88internal8compiler26WasmHeapStubCompilationJob14ExecuteJobImplEv.str1.1,"aMS",@progbits,1
.LC173:
	.string	"V8.WasmMachineCode"
	.section	.text._ZN2v88internal8compiler26WasmHeapStubCompilationJob14ExecuteJobImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler26WasmHeapStubCompilationJob14ExecuteJobImplEv
	.type	_ZN2v88internal8compiler26WasmHeapStubCompilationJob14ExecuteJobImplEv, @function
_ZN2v88internal8compiler26WasmHeapStubCompilationJob14ExecuteJobImplEv:
.LFB26485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$920, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal16FLAG_turbo_statsE(%rip)
	jne	.L2250
	xorl	%r12d, %r12d
	cmpb	$0, _ZN2v88internal20FLAG_turbo_stats_nvpE(%rip)
	jne	.L2250
.L2251:
	movl	72(%rbx), %eax
	testb	$-64, %ah
	je	.L2252
	movq	344(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2309
	call	_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv@PLT
	movq	%rax, %r13
.L2254:
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%r13), %rsi
	je	.L2255
	testq	%rsi, %rsi
	je	.L2310
.L2256:
	addl	$1, 152(%r13)
.L2255:
	leaq	-928(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movl	$52, %edx
	leaq	.LC106(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$23, %edx
	leaq	.LC163(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-936(%rbp), %rdi
	leaq	72(%rbx), %rsi
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movq	-936(%rbp), %r15
	testq	%r15, %r15
	je	.L2311
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2258:
	movl	$15, %edx
	leaq	.LC108(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-928(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-688(%rbp,%rax), %r15
	testq	%r15, %r15
	je	.L2269
	cmpb	$0, 56(%r15)
	je	.L2260
	movsbl	67(%r15), %esi
.L2261:
	movq	%r14, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2262
	call	_ZdaPv@PLT
.L2262:
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, -848(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-864(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -928(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-848(%rbp), %rdi
	movq	%rax, -928(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -848(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L2264
	subl	$1, 152(%r13)
	je	.L2312
.L2264:
	movl	72(%rbx), %eax
.L2252:
	testb	$-128, %ah
	jne	.L2313
.L2266:
	testb	$64, %ah
	jne	.L2314
.L2272:
	leaq	792(%rbx), %r13
	leaq	.LC173(%rip), %rsi
	movq	%r13, %rdi
	leaq	-944(%rbp), %r14
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl21ComputeScheduledGraphEv
	movq	240(%rbx), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -944(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE
	testb	%al, %al
	jne	.L2276
	movl	$1, %r13d
.L2277:
	testq	%r12, %r12
	je	.L2249
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18PipelineStatisticsD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L2249:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2315
	addq	$920, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2250:
	.cfi_restore_state
	movq	800(%rbx), %rdi
	call	_ZN2v88internal4wasm10WasmEngine26GetOrCreateTurboStatisticsEv@PLT
	movl	$176, %edi
	movq	%rax, %r13
	call	_ZN2v88internal8MallocednwEm@PLT
	leaq	72(%rbx), %rsi
	leaq	248(%rbx), %rcx
	movq	%r13, %rdx
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatisticsC1EPNS0_24OptimizedCompilationInfoEPNS0_21CompilationStatisticsEPNS1_9ZoneStatsE@PLT
	leaq	.LC162(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc@PLT
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2260:
	movq	%r15, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r15), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2261
	movq	%r15, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2276:
	movq	%r13, %rdi
	movq	%r14, %rsi
	leaq	-936(%rbp), %rdx
	movq	$0, -936(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE
	movq	-936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2278
	movq	(%rdi), %rax
	call	*8(%rax)
.L2278:
	xorl	%r13d, %r13d
	jmp	.L2277
	.p2align 4,,10
	.p2align 3
.L2314:
	leaq	72(%rbx), %r14
	leaq	-576(%rbp), %r13
	movl	$32, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode@PLT
	movl	$13, %edx
	leaq	.LC166(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	leaq	-936(%rbp), %rdi
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movq	-936(%rbp), %r14
	testq	%r14, %r14
	je	.L2316
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2274:
	movq	%r13, %rdi
	movl	$26, %edx
	leaq	.LC167(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2275
	call	_ZdaPv@PLT
.L2275:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileD1Ev@PLT
	jmp	.L2272
	.p2align 4,,10
	.p2align 3
.L2313:
	leaq	-496(%rbp), %r14
	leaq	-576(%rbp), %r13
	movq	%r14, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r15
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%ax, -272(%rbp)
	movups	%xmm0, -264(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	%r15, -496(%rbp)
	movq	$0, -280(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r13, %rdi
	movl	$13, %edx
	movq	%rax, -576(%rbp)
	leaq	.LC164(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -496(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	80(%rbx), %edi
	call	_ZN2v88internal4Code11Kind2StringENS1_4KindE@PLT
	testq	%rax, %rax
	je	.L2317
	movq	%rax, %rdi
	movq	%rax, -952(%rbp)
	call	strlen@PLT
	movq	-952(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2268:
	movq	%r13, %rdi
	movl	$10, %edx
	leaq	.LC165(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-336(%rbp,%rax), %rdi
	testq	%rdi, %rdi
	je	.L2269
	cmpb	$0, 56(%rdi)
	je	.L2270
	movsbl	67(%rdi), %esi
.L2271:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	-936(%rbp), %rsi
	movq	%rax, %rdi
	movq	496(%rbx), %rax
	movq	%rax, -936(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_5AsRPOE@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rax, -496(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-512(%rbp), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -576(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%r15, -496(%rbp)
	movq	%rax, -576(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movl	72(%rbx), %eax
	jmp	.L2266
	.p2align 4,,10
	.p2align 3
.L2311:
	movq	-928(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2258
	.p2align 4,,10
	.p2align 3
.L2309:
	movq	336(%rbx), %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	movq	%rax, %r13
	jmp	.L2254
	.p2align 4,,10
	.p2align 3
.L2270:
	movq	%rdi, -952(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-952(%rbp), %rdi
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2271
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2271
	.p2align 4,,10
	.p2align 3
.L2312:
	movq	144(%r13), %rdi
	call	fclose@PLT
	movq	$0, 144(%r13)
	jmp	.L2264
	.p2align 4,,10
	.p2align 3
.L2316:
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2274
	.p2align 4,,10
	.p2align 3
.L2317:
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2268
	.p2align 4,,10
	.p2align 3
.L2310:
	movq	0(%r13), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%r13)
	movq	%rax, %rsi
	jmp	.L2256
.L2315:
	call	__stack_chk_fail@PLT
.L2269:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE26485:
	.size	_ZN2v88internal8compiler26WasmHeapStubCompilationJob14ExecuteJobImplEv, .-_ZN2v88internal8compiler26WasmHeapStubCompilationJob14ExecuteJobImplEv
	.section	.text._ZN2v88internal8compiler13InliningPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8compiler13InliningPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler13InliningPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler13InliningPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE, @function
_ZN2v88internal8compiler13InliningPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE:
.LFB26521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$968, %rsp
	movq	216(%rsi), %r12
	movq	(%rsi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	movq	352(%r12), %r8
	movq	%rax, -968(%rbp)
	testq	%r8, %r8
	je	.L2368
.L2319:
	leaq	-320(%rbp), %r12
	movq	-968(%rbp), %rax
	movq	.LC174(%rip), %xmm1
	movq	%rbx, %rsi
	movq	%r12, %xmm2
	movq	160(%r14), %rdx
	movq	%r12, %rdi
	leaq	-864(%rbp), %r13
	punpcklqdq	%xmm2, %xmm1
	leaq	160(%rax), %rcx
	movaps	%xmm1, -1008(%rbp)
	call	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE@PLT
	leaq	-912(%rbp), %rax
	movq	%rbx, %r8
	movq	%r12, %rsi
	movq	200(%r14), %rcx
	movq	160(%r14), %rdx
	movq	%rax, %rdi
	movq	%rax, -976(%rbp)
	call	_ZN2v88internal8compiler19DeadCodeEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE@PLT
	leaq	-960(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -984(%rbp)
	call	_ZN2v88internal8compiler21CheckpointEliminationC1EPNS1_15AdvancedReducer6EditorE@PLT
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	320(%r14), %rcx
	movq	160(%r14), %rdx
	pushq	%rbx
	movq	192(%r14), %r9
	movq	200(%r14), %r8
	call	_ZN2v88internal8compiler21CommonOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE@PLT
	movq	24(%r14), %rdi
	movq	312(%r14), %r9
	movdqu	216(%r14), %xmm0
	movdqa	-1008(%rbp), %xmm1
	movl	(%rdi), %eax
	popq	%rcx
	movq	%r9, -600(%rbp)
	movdqu	312(%r14), %xmm3
	popq	%rsi
	movl	$0, -584(%rbp)
	shrl	$6, %eax
	movaps	%xmm1, -640(%rbp)
	andl	$1, %eax
	shufpd	$2, %xmm3, %xmm0
	movq	$0, -576(%rbp)
	movl	%eax, -608(%rbp)
	leaq	-584(%rbp), %rax
	movq	%rax, -568(%rbp)
	movq	%rax, -560(%rbp)
	movq	$0, -552(%rbp)
	movaps	%xmm0, -624(%rbp)
	testb	$2, (%rdi)
	je	.L2320
	movq	32(%rdi), %r8
	movq	%r8, -1008(%rbp)
	call	_ZNK2v88internal24OptimizedCompilationInfo7contextEv@PLT
	movq	41112(%r15), %rdi
	movq	-1008(%rbp), %r8
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2321
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-1008(%rbp), %r8
.L2322:
	movq	24(%r14), %rdi
	movq	312(%r14), %r9
	xorl	%r15d, %r15d
	movl	$1, %esi
.L2324:
	leaq	16+_ZTVN2v88internal8compiler23JSContextSpecializationE(%rip), %r10
	movq	%r12, %xmm4
	movq	320(%r14), %rcx
	movq	216(%r14), %rdx
	movq	%r10, %xmm0
	movb	%sil, -776(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movq	%rdx, -784(%rbp)
	movaps	%xmm0, -800(%rbp)
	movq	%r15, -760(%rbp)
	leaq	-736(%rbp), %r15
	movq	%r8, -752(%rbp)
	movq	%rcx, -744(%rbp)
	movq	%rax, -768(%rbp)
	movl	(%rdi), %eax
	movq	%r15, %rdi
	movl	%eax, %r8d
	andl	$1, %r8d
	movl	%r8d, %esi
	orl	$2, %esi
	testb	$64, %al
	movq	-968(%rbp), %rax
	cmovne	%esi, %r8d
	movq	%r12, %rsi
	pushq	64(%rax)
	pushq	%rbx
	call	_ZN2v88internal8compiler29JSNativeContextSpecializationC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_23CompilationDependenciesEPNS0_4ZoneESH_@PLT
	movq	24(%r14), %rdi
	movq	168(%r14), %rsi
	leaq	16+_ZTVN2v88internal8compiler19JSInliningHeuristicE(%rip), %r8
	movq	320(%r14), %rcx
	movq	216(%r14), %rdx
	leaq	-544(%rbp), %r9
	movl	(%rdi), %eax
	movq	%rbx, -504(%rbp)
	movq	%rbx, -464(%rbp)
	shrl	$2, %eax
	movq	%rbx, -408(%rbp)
	leaq	-944(%rbp), %rbx
	xorl	$1, %eax
	movq	%rdi, -496(%rbp)
	movq	%rbx, %rdi
	andl	$1, %eax
	movq	%rsi, -472(%rbp)
	movl	%eax, -528(%rbp)
	leaq	16+_ZTVN2v88internal8compiler9JSInlinerE(%rip), %rax
	movq	%rax, -520(%rbp)
	leaq	-448(%rbp), %rax
	movq	%rax, -432(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-392(%rbp), %rax
	movq	%rsi, -352(%rbp)
	movq	%r12, %rsi
	movq	%r8, -544(%rbp)
	movq	%rax, -376(%rbp)
	movq	%rax, -368(%rbp)
	movq	%rcx, -480(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%r9, -968(%rbp)
	movq	%rdx, -488(%rbp)
	movq	%rdx, -344(%rbp)
	movq	%r12, -536(%rbp)
	movq	%r12, -512(%rbp)
	movl	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -416(%rbp)
	movl	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	movq	$0, -360(%rbp)
	movl	$0, -328(%rbp)
	call	_ZN2v88internal8compiler19JSIntrinsicLoweringC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE@PLT
	movq	-976(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-984(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	leaq	-800(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	leaq	-640(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-968(%rbp), %r9
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv@PLT
	movq	-384(%rbp), %rbx
	leaq	16+_ZTVN2v88internal8compiler19JSInliningHeuristicE(%rip), %r8
	popq	%rax
	movq	%r8, -544(%rbp)
	popq	%rdx
	testq	%rbx, %rbx
	je	.L2333
	leaq	-408(%rbp), %r14
.L2336:
	movq	24(%rbx), %r13
	testq	%r13, %r13
	je	.L2334
.L2335:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIjjSt9_IdentityIjESt4lessIjEN2v88internal13ZoneAllocatorIjEEE8_M_eraseEPSt13_Rb_tree_nodeIjE
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L2335
.L2334:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2336
.L2333:
	movq	-440(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2337
	leaq	-464(%rbp), %r14
.L2340:
	movq	24(%rbx), %r13
	testq	%r13, %r13
	je	.L2338
.L2339:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal8compiler19JSInliningHeuristic9CandidateES4_St9_IdentityIS4_ENS3_16CandidateCompareENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L2339
.L2338:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2340
.L2337:
	movq	-576(%rbp), %rbx
	leaq	16+_ZTVN2v88internal8compiler13JSCallReducerE(%rip), %rax
	movq	%rax, -640(%rbp)
	testq	%rbx, %rbx
	je	.L2341
	leaq	-592(%rbp), %r13
.L2342:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ESaIS4_EE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2342
.L2341:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2369
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2320:
	.cfi_restore_state
	movq	32(%rdi), %rax
	xorl	%r15d, %r15d
	movq	(%rax), %rax
	movq	31(%rax), %rsi
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2371:
	movq	-1(%rsi), %rax
	cmpw	$144, 11(%rax)
	je	.L2370
	movq	23(%rsi), %rsi
	addq	$1, %r15
.L2331:
	movq	-1(%rsi), %rax
	cmpw	$145, 11(%rax)
	jne	.L2371
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	xorl	%esi, %esi
.L2330:
	xorl	%r8d, %r8d
	jmp	.L2324
	.p2align 4,,10
	.p2align 3
.L2321:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L2372
.L2323:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2322
	.p2align 4,,10
	.p2align 3
.L2370:
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	movq	3520(%rdx), %rdi
	subq	$37592, %rdx
	testq	%rdi, %rdi
	je	.L2327
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2328:
	movq	24(%r14), %rdi
	movq	312(%r14), %r9
	movl	$1, %esi
	jmp	.L2330
	.p2align 4,,10
	.p2align 3
.L2327:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L2373
.L2329:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L2328
	.p2align 4,,10
	.p2align 3
.L2368:
	movq	8(%r12), %rdi
	movq	(%r12), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r12)
	movq	%rax, %r8
	jmp	.L2319
	.p2align 4,,10
	.p2align 3
.L2372:
	movq	%r15, %rdi
	movq	%rsi, -992(%rbp)
	movq	%r8, -1008(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-992(%rbp), %rsi
	movq	-1008(%rbp), %r8
	jmp	.L2323
	.p2align 4,,10
	.p2align 3
.L2373:
	movq	%rdx, %rdi
	movq	%rsi, -992(%rbp)
	movq	%rdx, -1008(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-992(%rbp), %rsi
	movq	-1008(%rbp), %rdx
	jmp	.L2329
.L2369:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26521:
	.size	_ZN2v88internal8compiler13InliningPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE, .-_ZN2v88internal8compiler13InliningPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl3RunINS1_37CopyMetadataForConcurrentCompilePhaseEJEEEvDpOT0_.isra.0.str1.1,"aMS",@progbits,1
.LC175:
	.string	"V8.TFSerializeMetadata"
	.section	.text._ZN2v88internal8compiler12PipelineImpl3RunINS1_37CopyMetadataForConcurrentCompilePhaseEJEEEvDpOT0_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler12PipelineImpl3RunINS1_37CopyMetadataForConcurrentCompilePhaseEJEEEvDpOT0_.isra.0, @function
_ZN2v88internal8compiler12PipelineImpl3RunINS1_37CopyMetadataForConcurrentCompilePhaseEJEEEvDpOT0_.isra.0:
.LFB38187:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	56(%rbx), %r14
	testq	%r14, %r14
	je	.L2375
	leaq	.LC175(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L2375:
	movq	48(%rbx), %rax
	movq	176(%rbx), %rbx
	movq	%rax, -376(%rbp)
	testq	%rbx, %rbx
	je	.L2376
	movq	48(%rbx), %rax
	movq	%rax, -392(%rbp)
	leaq	.LC175(%rip), %rax
	movq	%rax, 48(%rbx)
.L2376:
	movq	-376(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	0(%r13), %r13
	movq	%rax, %r12
	movq	216(%r13), %r15
	movq	352(%r15), %r8
	testq	%r8, %r8
	je	.L2401
.L2377:
	movq	24(%r13), %rax
	movq	160(%r13), %rdx
	leaq	-320(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	160(%rax), %rcx
	call	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE@PLT
	leaq	-368(%rbp), %rdx
	movq	320(%r13), %rsi
	movq	%rdx, %rdi
	movq	%rdx, -384(%rbp)
	call	_ZN2v88internal8compiler17JSHeapCopyReducerC1EPNS1_12JSHeapBrokerE@PLT
	movq	-384(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv@PLT
	movq	216(%r13), %rdi
	leaq	-352(%rbp), %rsi
	movq	%r12, -352(%rbp)
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	movq	$0, -328(%rbp)
	call	_ZN2v88internal8compiler7JSGraph14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	movq	-344(%rbp), %r13
	movq	-336(%rbp), %rax
	cmpq	%rax, %r13
	je	.L2378
	.p2align 4,,10
	.p2align 3
.L2379:
	movq	0(%r13), %rsi
	movq	%r15, %rdi
	movq	%rax, -384(%rbp)
	addq	$8, %r13
	call	_ZN2v88internal8compiler12GraphReducer10ReduceNodeEPNS1_4NodeE@PLT
	movq	-384(%rbp), %rax
	cmpq	%r13, %rax
	jne	.L2379
.L2378:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12GraphReducerD1Ev@PLT
	testq	%rbx, %rbx
	je	.L2380
	movq	-392(%rbp), %rax
	movq	%rax, 48(%rbx)
.L2380:
	testq	%r12, %r12
	je	.L2381
	movq	-376(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L2381:
	testq	%r14, %r14
	je	.L2374
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L2374:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2402
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2401:
	.cfi_restore_state
	movq	(%r15), %r10
	movq	8(%r15), %rdi
	movq	%r10, -384(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	-384(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r15)
	movq	%rax, %r8
	jmp	.L2377
.L2402:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE38187:
	.size	_ZN2v88internal8compiler12PipelineImpl3RunINS1_37CopyMetadataForConcurrentCompilePhaseEJEEEvDpOT0_.isra.0, .-_ZN2v88internal8compiler12PipelineImpl3RunINS1_37CopyMetadataForConcurrentCompilePhaseEJEEEvDpOT0_.isra.0
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl11CreateGraphEv.str1.1,"aMS",@progbits,1
.LC176:
	.string	"V8.TFGraphCreation"
.LC177:
	.string	"V8.TFSerialization"
.LC178:
	.string	"V8.TFBytecodeGraphBuilder"
.LC180:
	.string	"V8.TFInlining"
.LC181:
	.string	"V8.TFEarlyTrimming"
	.section	.text._ZN2v88internal8compiler12PipelineImpl11CreateGraphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12PipelineImpl11CreateGraphEv
	.type	_ZN2v88internal8compiler12PipelineImpl11CreateGraphEv, @function
_ZN2v88internal8compiler12PipelineImpl11CreateGraphEv:
.LFB26720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$744, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	56(%rax), %rdi
	movq	%rax, -696(%rbp)
	testq	%rdi, %rdi
	je	.L2404
	leaq	.LC176(%rip), %rsi
	call	_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc@PLT
	movq	(%r12), %rax
.L2404:
	movq	24(%rax), %rdx
	movl	(%rdx), %edx
	testb	$-64, %dh
	je	.L2563
	movq	-696(%rbp), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2564
	call	_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv@PLT
	movq	%rax, %rbx
.L2407:
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%rbx), %rsi
	je	.L2408
	testq	%rsi, %rsi
	je	.L2565
.L2409:
	addl	$1, 152(%rbx)
.L2408:
	leaq	-576(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movl	$52, %edx
	leaq	.LC106(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$23, %edx
	leaq	.LC163(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	leaq	-624(%rbp), %rdi
	movq	%rdi, -704(%rbp)
	movq	24(%rax), %rsi
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movq	-624(%rbp), %r14
	testq	%r14, %r14
	je	.L2566
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2411:
	movl	$15, %edx
	leaq	.LC108(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-336(%rbp,%rax), %r14
	testq	%r14, %r14
	je	.L2567
	cmpb	$0, 56(%r14)
	je	.L2413
	movsbl	67(%r14), %esi
.L2414:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2415
	call	_ZdaPv@PLT
.L2415:
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, -496(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-512(%rbp), %rdi
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -576(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-496(%rbp), %rdi
	movq	%rax, -576(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -496(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L2417
	subl	$1, 152(%rbx)
	je	.L2568
.L2417:
	movq	(%r12), %rax
	movq	24(%rax), %rdx
	movl	(%rdx), %edx
.L2405:
	andb	$64, %dh
	jne	.L2569
.L2419:
	movq	-696(%rbp), %rbx
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler19SourcePositionTable12AddDecoratorEv@PLT
	movq	24(%rbx), %rdi
	testl	$16384, (%rdi)
	jne	.L2570
.L2420:
	movq	-696(%rbp), %rax
	movq	(%rax), %rbx
	movq	320(%rax), %r13
	call	_ZNK2v88internal24OptimizedCompilationInfo14native_contextEv@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L2421
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2422:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12JSHeapBroker25SetTargetNativeContextRefENS0_6HandleINS0_13NativeContextEEE@PLT
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L2571
.L2424:
	movq	(%r12), %rbx
	movq	56(%rbx), %r10
	testq	%r10, %r10
	je	.L2429
	movq	%r10, %rdi
	leaq	.LC178(%rip), %rsi
	movq	%r10, -712(%rbp)
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
	movq	-712(%rbp), %r10
.L2429:
	movq	176(%rbx), %r15
	movq	48(%rbx), %r11
	testq	%r15, %r15
	je	.L2430
	movq	48(%r15), %rax
	movq	%rax, -768(%rbp)
	leaq	.LC178(%rip), %rax
	movq	%rax, 48(%r15)
.L2430:
	movq	%r11, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r10, -760(%rbp)
	movq	%r11, -752(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%r12), %r13
	leaq	-672(%rbp), %rdi
	movq	%rax, %r14
	movq	%rdi, -712(%rbp)
	movq	24(%r13), %rdx
	movl	(%rdx), %ecx
	movq	32(%rdx), %rdx
	movl	%ecx, %esi
	andl	$8192, %esi
	cmpl	$1, %esi
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$6, %eax
	cmpl	$1, %esi
	movq	320(%r13), %rsi
	sbbl	%ebx, %ebx
	notl	%ebx
	andl	$2, %ebx
	andl	$64, %ecx
	movl	$1, %ecx
	cmovne	%eax, %ebx
	call	_ZN2v88internal8compiler13JSFunctionRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb
	movq	24(%r13), %rcx
	movq	168(%r13), %rsi
	movl	$0x3f800000, -676(%rbp)
	movq	216(%r13), %r9
	movq	-712(%rbp), %rdi
	movl	56(%rcx), %r8d
	movq	%rsi, -744(%rbp)
	movq	%r9, -736(%rbp)
	movq	%rcx, -728(%rbp)
	movl	%r8d, -720(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef15feedback_vectorEv@PLT
	movq	-712(%rbp), %rdi
	movq	%rax, -624(%rbp)
	movq	%rdx, -616(%rbp)
	call	_ZNK2v88internal8compiler13JSFunctionRef6sharedEv@PLT
	movq	-728(%rbp), %rcx
	subq	$8, %rsp
	movq	-744(%rbp), %rsi
	movq	%rax, -656(%rbp)
	leaq	-676(%rbp), %rax
	movq	-736(%rbp), %r9
	addq	$160, %rcx
	movq	%rdx, -648(%rbp)
	movq	320(%r13), %rdi
	leaq	-656(%rbp), %r13
	pushq	%rcx
	movl	-720(%rbp), %r8d
	movq	%r13, %rdx
	pushq	%rbx
	movq	-704(%rbp), %rcx
	pushq	$-1
	pushq	%rsi
	movq	%r14, %rsi
	pushq	%rax
	call	_ZN2v88internal8compiler22BuildGraphFromBytecodeEPNS1_12JSHeapBrokerEPNS0_4ZoneERKNS1_21SharedFunctionInfoRefERKNS1_17FeedbackVectorRefENS0_9BailoutIdEPNS1_7JSGraphERKNS1_13CallFrequencyEPNS1_19SourcePositionTableEiNS_4base5FlagsINS1_24BytecodeGraphBuilderFlagEiEEPNS0_11TickCounterE@PLT
	addq	$48, %rsp
	testq	%r15, %r15
	movq	-752(%rbp), %r11
	movq	-760(%rbp), %r10
	je	.L2433
	movq	-768(%rbp), %rax
	movq	%rax, 48(%r15)
.L2433:
	testq	%r14, %r14
	je	.L2434
	movq	%r14, %rsi
	movq	%r11, %rdi
	movq	%r10, -712(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
	movq	-712(%rbp), %r10
.L2434:
	testq	%r10, %r10
	je	.L2435
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L2435:
	leaq	.LC178(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	jne	.L2572
.L2436:
	movq	(%r12), %rbx
	movq	56(%rbx), %r15
	testq	%r15, %r15
	je	.L2437
	leaq	.LC180(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L2437:
	movq	48(%rbx), %r8
	movq	176(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2438
	leaq	.LC180(%rip), %rax
	movq	48(%rbx), %rcx
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, 48(%rbx)
	movq	%rcx, -720(%rbp)
	movq	%r8, -712(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%r12), %rsi
	movq	-704(%rbp), %rdi
	movq	%rax, %rdx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13InliningPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	movq	-720(%rbp), %rcx
	movq	-712(%rbp), %r8
	movq	%rcx, 48(%rbx)
.L2474:
	testq	%r14, %r14
	je	.L2439
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L2439:
	testq	%r15, %r15
	je	.L2440
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L2440:
	leaq	.LC180(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	(%r12), %rbx
	movq	56(%rbx), %rax
	movq	%rax, -720(%rbp)
	testq	%rax, %rax
	je	.L2441
	leaq	.LC181(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L2441:
	movq	48(%rbx), %rax
	movq	176(%rbx), %rcx
	movq	%rax, -712(%rbp)
	testq	%rcx, %rcx
	je	.L2442
	movq	48(%rcx), %rax
	movq	%rax, -776(%rbp)
	leaq	.LC181(%rip), %rax
	movq	%rax, 48(%rcx)
.L2442:
	movq	-712(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rcx, -728(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%r12), %rbx
	movq	-704(%rbp), %rdi
	movq	%rax, %r14
	movq	%rax, %rsi
	movq	160(%rbx), %rdx
	call	_ZN2v88internal8compiler12GraphTrimmerC1EPNS0_4ZoneEPNS1_5GraphE@PLT
	movq	%r14, -656(%rbp)
	movq	%r13, %rsi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	movq	216(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	movq	-640(%rbp), %r11
	movq	-648(%rbp), %r10
	movq	-728(%rbp), %rcx
	cmpq	%r10, %r11
	je	.L2443
	.p2align 4,,10
	.p2align 3
.L2444:
	movq	(%r10), %r15
	movzbl	23(%r15), %eax
	leaq	32(%r15), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2446
	movq	32(%r15), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
.L2446:
	testl	%eax, %eax
	jle	.L2447
	cmpq	$0, (%rdx)
	je	.L2448
.L2447:
	movl	-616(%rbp), %eax
	cmpl	%eax, 16(%r15)
	ja	.L2448
	addl	$1, %eax
	movl	%eax, 16(%r15)
	movq	-592(%rbp), %rbx
	cmpq	-584(%rbp), %rbx
	je	.L2449
	movq	%r15, (%rbx)
	addq	$8, -592(%rbp)
.L2448:
	addq	$8, %r10
	cmpq	%r10, %r11
	jne	.L2444
.L2443:
	movq	-704(%rbp), %rbx
	movq	%rcx, -728(%rbp)
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12GraphTrimmer9TrimGraphEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12GraphTrimmerD1Ev@PLT
	movq	-728(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L2460
	movq	-776(%rbp), %rax
	movq	%rax, 48(%rcx)
.L2460:
	testq	%r14, %r14
	je	.L2461
	movq	-712(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L2461:
	movq	-720(%rbp), %rax
	testq	%rax, %rax
	je	.L2462
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L2462:
	leaq	.LC181(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	(%r12), %rax
	movq	24(%rax), %rdx
	movq	24(%rdx), %rdx
	movq	(%rdx), %rdx
	movl	47(%rdx), %ecx
	andl	$64, %ecx
	je	.L2573
.L2469:
	movq	24(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movl	47(%rax), %eax
	andl	$31, %eax
	subl	$2, %eax
	cmpb	$3, %al
	ja	.L2470
	movq	-696(%rbp), %rax
	orl	$2, 120(%rax)
.L2470:
	cmpb	$0, _ZN2v88internal24FLAG_concurrent_inliningE(%rip)
	movq	%r12, %rdi
	jne	.L2562
	call	_ZN2v88internal8compiler12PipelineImpl3RunINS1_29HeapBrokerInitializationPhaseEJEEEvDpOT0_.isra.0
	movq	%r12, %rdi
.L2562:
	call	_ZN2v88internal8compiler12PipelineImpl3RunINS1_37CopyMetadataForConcurrentCompilePhaseEJEEEvDpOT0_.isra.0
	movq	-696(%rbp), %rax
	movq	320(%rax), %rdi
	call	_ZN2v88internal8compiler12JSHeapBroker15StopSerializingEv@PLT
	movq	-696(%rbp), %rax
	movq	56(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2473
	call	_ZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEv@PLT
.L2473:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2574
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2413:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2414
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2414
	.p2align 4,,10
	.p2align 3
.L2449:
	movq	-600(%rbp), %r13
	movq	%rbx, %r8
	subq	%r13, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L2575
	testq	%rax, %rax
	je	.L2479
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L2576
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L2451:
	movq	-608(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r9
	subq	%rax, %r9
	cmpq	%rsi, %r9
	jb	.L2577
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2454:
	addq	%rax, %rdx
	leaq	8(%rax), %rsi
.L2452:
	movq	%r15, (%rax,%r8)
	cmpq	%r13, %rbx
	je	.L2455
	subq	$8, %rbx
	leaq	15(%rax), %rsi
	subq	%r13, %rbx
	subq	%r13, %rsi
	movq	%rbx, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rsi
	jbe	.L2482
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rdi
	je	.L2482
	addq	$1, %rdi
	xorl	%esi, %esi
	movq	%rdi, %r8
	shrq	%r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L2457:
	movdqu	0(%r13,%rsi), %xmm1
	movups	%xmm1, (%rax,%rsi)
	addq	$16, %rsi
	cmpq	%r8, %rsi
	jne	.L2457
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rsi
	addq	%rsi, %r13
	addq	%rax, %rsi
	cmpq	%rdi, %r8
	je	.L2459
	movq	0(%r13), %rdi
	movq	%rdi, (%rsi)
.L2459:
	leaq	16(%rax,%rbx), %rsi
.L2455:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%rdx, -584(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -600(%rbp)
	jmp	.L2448
	.p2align 4,,10
	.p2align 3
.L2576:
	testq	%rdx, %rdx
	jne	.L2578
	movl	$8, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L2452
	.p2align 4,,10
	.p2align 3
.L2421:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L2579
.L2423:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r14, (%rsi)
	jmp	.L2422
	.p2align 4,,10
	.p2align 3
.L2570:
	movq	-696(%rbp), %rbx
	movq	176(%rbx), %rdi
	call	_ZN2v88internal8compiler15NodeOriginTable12AddDecoratorEv@PLT
	movq	24(%rbx), %rdi
	jmp	.L2420
	.p2align 4,,10
	.p2align 3
.L2569:
	leaq	-576(%rbp), %r13
	movq	(%rax), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12TurboCfgFileC1EPNS0_7IsolateE@PLT
	movq	(%r12), %rax
	movq	-704(%rbp), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	movq	%rax, -624(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_16AsC1VCompilationE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12TurboCfgFileD1Ev@PLT
	jmp	.L2419
	.p2align 4,,10
	.p2align 3
.L2572:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl3RunINS1_37CopyMetadataForConcurrentCompilePhaseEJEEEvDpOT0_.isra.0
	jmp	.L2436
	.p2align 4,,10
	.p2align 3
.L2571:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl3RunINS1_29HeapBrokerInitializationPhaseEJEEEvDpOT0_.isra.0
	movq	(%r12), %rbx
	movq	56(%rbx), %rdi
	movq	%rdi, -624(%rbp)
	testq	%rdi, %rdi
	je	.L2425
	leaq	.LC177(%rip), %rsi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L2425:
	movq	48(%rbx), %rdi
	leaq	.LC3(%rip), %rax
	movq	$0, -600(%rbp)
	movq	%rax, -616(%rbp)
	movq	%rdi, -608(%rbp)
	movq	176(%rbx), %rax
	movq	%rax, -592(%rbp)
	testq	%rax, %rax
	je	.L2426
	movq	48(%rax), %rdx
	leaq	.LC177(%rip), %rcx
	movq	%rdx, -584(%rbp)
	movq	%rcx, 48(%rax)
.L2426:
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%r12), %rdi
	movq	%rax, -600(%rbp)
	movq	%rax, %rdx
	movq	24(%rdi), %rcx
	movl	(%rcx), %esi
	movq	32(%rcx), %r10
	movl	56(%rcx), %r9d
	movl	%esi, %eax
	movq	%r10, %rcx
	shrl	$6, %eax
	andl	$1, %eax
	movl	%eax, %r8d
	orl	$2, %r8d
	testb	$32, %sil
	cmovne	%r8d, %eax
	movl	%eax, %r8d
	orl	$4, %r8d
	andl	$8192, %esi
	movq	312(%rdi), %rsi
	movq	320(%rdi), %rdi
	cmove	%eax, %r8d
	call	_ZN2v88internal8compiler37RunSerializerForBackgroundCompilationEPNS1_12JSHeapBrokerEPNS1_23CompilationDependenciesEPNS0_4ZoneENS0_6HandleINS0_10JSFunctionEEENS_4base5FlagsINS1_38SerializerForBackgroundCompilationFlagEiEENS0_9BailoutIdE@PLT
	movq	-704(%rbp), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116PipelineRunScopeD1Ev
	jmp	.L2424
	.p2align 4,,10
	.p2align 3
.L2573:
	movq	31(%rdx), %rdx
	testb	$1, %dl
	jne	.L2580
.L2465:
	movq	-704(%rbp), %rdi
	movq	%rdx, -624(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	je	.L2561
	movq	-696(%rbp), %rax
	orl	$1, 120(%rax)
.L2561:
	movq	(%r12), %rax
	jmp	.L2469
	.p2align 4,,10
	.p2align 3
.L2438:
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r8, -712(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	(%r12), %rsi
	movq	-704(%rbp), %rdi
	movq	%rax, %rdx
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13InliningPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	movq	-712(%rbp), %r8
	jmp	.L2474
	.p2align 4,,10
	.p2align 3
.L2479:
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L2451
	.p2align 4,,10
	.p2align 3
.L2563:
	leaq	-624(%rbp), %rcx
	movq	%rcx, -704(%rbp)
	jmp	.L2405
	.p2align 4,,10
	.p2align 3
.L2564:
	movq	(%rax), %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	movq	%rax, %rbx
	jmp	.L2407
	.p2align 4,,10
	.p2align 3
.L2566:
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2411
	.p2align 4,,10
	.p2align 3
.L2568:
	movq	144(%rbx), %rdi
	call	fclose@PLT
	movq	$0, 144(%rbx)
	jmp	.L2417
	.p2align 4,,10
	.p2align 3
.L2482:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L2456:
	movq	0(%r13,%rsi,8), %r8
	movq	%r8, (%rax,%rsi,8)
	movq	%rsi, %r8
	addq	$1, %rsi
	cmpq	%r8, %rdi
	jne	.L2456
	jmp	.L2459
	.p2align 4,,10
	.p2align 3
.L2580:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	je	.L2581
.L2466:
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	%rdx, -37504(%rcx)
	je	.L2469
	jmp	.L2465
	.p2align 4,,10
	.p2align 3
.L2565:
	movq	(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %rsi
	jmp	.L2409
	.p2align 4,,10
	.p2align 3
.L2579:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2423
	.p2align 4,,10
	.p2align 3
.L2577:
	movq	%r10, -760(%rbp)
	movq	%r11, -752(%rbp)
	movq	%rcx, -744(%rbp)
	movq	%r8, -736(%rbp)
	movq	%rdx, -728(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-728(%rbp), %rdx
	movq	-736(%rbp), %r8
	movq	-744(%rbp), %rcx
	movq	-752(%rbp), %r11
	movq	-760(%rbp), %r10
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2581:
	movq	23(%rdx), %rdx
	testb	$1, %dl
	jne	.L2466
	jmp	.L2465
.L2574:
	call	__stack_chk_fail@PLT
.L2575:
	leaq	.LC26(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2567:
	call	_ZSt16__throw_bad_castv@PLT
.L2578:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L2451
	.cfi_endproc
.LFE26720:
	.size	_ZN2v88internal8compiler12PipelineImpl11CreateGraphEv, .-_ZN2v88internal8compiler12PipelineImpl11CreateGraphEv
	.section	.rodata._ZN2v88internal8compiler22PipelineCompilationJob14PrepareJobImplEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC182:
	.string	"v8.optimizingCompile.prepare"
	.section	.rodata._ZN2v88internal8compiler22PipelineCompilationJob14PrepareJobImplEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC183:
	.string	"!isolate->has_pending_exception()"
	.section	.text._ZN2v88internal8compiler22PipelineCompilationJob14PrepareJobImplEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22PipelineCompilationJob14PrepareJobImplEPNS0_7IsolateE
	.type	_ZN2v88internal8compiler22PipelineCompilationJob14PrepareJobImplEPNS0_7IsolateE, @function
_ZN2v88internal8compiler22PipelineCompilationJob14PrepareJobImplEPNS0_7IsolateE:
.LFB26464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal8compiler22PipelineCompilationJob14PrepareJobImplEPNS0_7IsolateEE28trace_event_unique_atomic942(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2627
.L2584:
	movq	$0, -96(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2628
.L2586:
	movq	24(%r12), %rax
	movl	_ZN2v88internal32FLAG_max_optimized_bytecode_sizeE(%rip), %ecx
	movq	16(%rax), %rdx
	movq	(%rdx), %rdx
	cmpl	%ecx, 11(%rdx)
	jg	.L2629
	cmpb	$0, _ZN2v88internal15FLAG_always_optE(%rip)
	jne	.L2593
	orl	$64, (%rax)
	movq	24(%r12), %rax
.L2593:
	cmpb	$0, _ZN2v88internal23FLAG_turbo_loop_peelingE(%rip)
	je	.L2594
	orl	$128, (%rax)
	movq	24(%r12), %rax
.L2594:
	cmpb	$0, _ZN2v88internal19FLAG_turbo_inliningE(%rip)
	je	.L2595
	orl	$4, (%rax)
	movq	24(%r12), %rax
.L2595:
	cmpb	$0, _ZN2v88internal21FLAG_inline_accessorsE(%rip)
	je	.L2596
	orl	$1, (%rax)
	movq	24(%r12), %rax
.L2596:
	xorl	%edx, %edx
	cmpb	$0, _ZN2v88internal31FLAG_untrusted_code_mitigationsE(%rip)
	setne	%dl
	addl	$1, %edx
	cmpb	$0, _ZN2v88internal29FLAG_turbo_allocation_foldingE(%rip)
	movl	%edx, 4(%rax)
	je	.L2598
	movq	24(%r12), %rax
	orl	$4096, (%rax)
.L2598:
	movq	24(%r12), %rax
	movq	32(%rax), %rdx
	movq	(%rdx), %rdx
	movq	39(%rdx), %rdx
	movq	-1(%rdx), %rdx
	cmpq	%rdx, 544(%r13)
	je	.L2630
	movq	24(%rax), %rsi
	testb	$32, (%rax)
	jne	.L2631
.L2600:
	movq	(%rsi), %rax
	leaq	-104(%rbp), %rdi
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	24(%r12), %rsi
	movl	%eax, 444(%r12)
	movq	64(%rsi), %rdi
	call	_ZN2v88internal8compiler7Linkage15ComputeIncomingEPNS0_4ZoneEPNS0_24OptimizedCompilationInfoE@PLT
	movq	%rax, %rbx
	movq	24(%r12), %rax
	movq	64(%rax), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jbe	.L2632
	leaq	8(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L2602:
	movq	%rbx, (%rax)
	movq	%rax, 840(%r12)
	movq	24(%r12), %rax
	cmpl	$-1, 56(%rax)
	je	.L2603
	cmpb	$0, 448(%r12)
	movq	400(%r12), %rsi
	je	.L2604
	movb	$0, 448(%r12)
.L2604:
	leaq	456(%r12), %rdi
	call	_ZN2v88internal8compiler9OsrHelperC1EPNS0_24OptimizedCompilationInfoE@PLT
	movb	$1, 448(%r12)
.L2603:
	movq	%r13, %rdi
	call	_ZN2v88internal11Deoptimizer34EnsureCodeForDeoptimizationEntriesEPNS0_7IsolateE@PLT
	leaq	832(%r12), %rdi
	call	_ZN2v88internal8compiler12PipelineImpl11CreateGraphEv
	testb	%al, %al
	je	.L2633
	xorl	%r12d, %r12d
.L2592:
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2634
	leaq	-32(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2633:
	.cfi_restore_state
	movq	12480(%r13), %rax
	cmpq	%rax, 96(%r13)
	jne	.L2635
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal23OptimizedCompilationJob17AbortOptimizationENS0_13BailoutReasonE@PLT
	movl	%eax, %r12d
	jmp	.L2592
	.p2align 4,,10
	.p2align 3
.L2630:
	orl	$2, (%rax)
	movq	24(%r12), %rax
	movq	24(%rax), %rsi
	testb	$32, (%rax)
	je	.L2600
.L2631:
	movq	%r13, %rdi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	24(%r12), %rax
	movq	24(%rax), %rsi
	jmp	.L2600
	.p2align 4,,10
	.p2align 3
.L2628:
	movq	24(%r12), %rax
	leaq	-120(%rbp), %rdi
	leaq	-128(%rbp), %rsi
	xorl	%r14d, %r14d
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo10TraceIDRefEv@PLT
	leaq	.LC63(%rip), %rax
	movb	$8, -129(%rbp)
	movq	%rax, -112(%rbp)
	movq	-120(%rbp), %rax
	movq	$0, -56(%rbp)
	movq	$0, -120(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2636
.L2587:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2588
	movq	(%rdi), %rax
	call	*8(%rax)
.L2588:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2589
	movq	(%rdi), %rax
	call	*8(%rax)
.L2589:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2590
	movq	(%rdi), %rax
	call	*8(%rax)
.L2590:
	leaq	.LC182(%rip), %rax
	movq	%rbx, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r14, -72(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L2586
	.p2align 4,,10
	.p2align 3
.L2627:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2637
.L2585:
	movq	%rbx, _ZZN2v88internal8compiler22PipelineCompilationJob14PrepareJobImplEPNS0_7IsolateEE28trace_event_unique_atomic942(%rip)
	jmp	.L2584
	.p2align 4,,10
	.p2align 3
.L2629:
	movq	%r12, %rdi
	movl	$6, %esi
	call	_ZN2v88internal23OptimizedCompilationJob17AbortOptimizationENS0_13BailoutReasonE@PLT
	movl	%eax, %r12d
	jmp	.L2592
	.p2align 4,,10
	.p2align 3
.L2632:
	movl	$8, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L2602
	.p2align 4,,10
	.p2align 3
.L2637:
	leaq	.LC55(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2585
	.p2align 4,,10
	.p2align 3
.L2636:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$772
	leaq	.LC182(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	leaq	-104(%rbp), %rdx
	pushq	%rdx
	leaq	-129(%rbp), %rdx
	pushq	%rdx
	leaq	-112(%rbp), %rdx
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$1
	pushq	%r12
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2587
	.p2align 4,,10
	.p2align 3
.L2635:
	leaq	.LC183(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2634:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26464:
	.size	_ZN2v88internal8compiler22PipelineCompilationJob14PrepareJobImplEPNS0_7IsolateE, .-_ZN2v88internal8compiler22PipelineCompilationJob14PrepareJobImplEPNS0_7IsolateE
	.section	.text._ZN2v88internal8compiler19EscapeAnalysisPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8compiler19EscapeAnalysisPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler19EscapeAnalysisPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler19EscapeAnalysisPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE, @function
_ZN2v88internal8compiler19EscapeAnalysisPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE:
.LFB26564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	movq	%rbx, %rcx
	subq	$800, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	movq	216(%rsi), %rsi
	leaq	160(%rax), %rdx
	call	_ZN2v88internal8compiler14EscapeAnalysisC1EPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE@PLT
	movq	-312(%rbp), %rax
	movq	%r13, %rdi
	movq	16(%rax), %rsi
	call	_ZN2v88internal8compiler18EffectGraphReducer10ReduceFromEPNS1_4NodeE@PLT
	movq	216(%r12), %r13
	movq	352(%r13), %r8
	testq	%r8, %r8
	je	.L2690
.L2639:
	movq	24(%r12), %rax
	leaq	-592(%rbp), %r13
	movq	%rbx, %rsi
	movq	160(%r12), %rdx
	movq	%r13, %rdi
	leaq	-832(%rbp), %r14
	leaq	160(%rax), %rcx
	call	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	216(%r12), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21EscapeAnalysisReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphENS1_20EscapeAnalysisResultEPNS0_4ZoneE@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21EscapeAnalysisReducer17VerifyReplacementEv@PLT
	movq	-640(%rbp), %r12
	leaq	16+_ZTVN2v88internal8compiler21EscapeAnalysisReducerE(%rip), %rax
	movq	%rax, -832(%rbp)
	testq	%r12, %r12
	je	.L2640
	leaq	-664(%rbp), %r14
.L2643:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L2641
.L2642:
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal8compiler4NodeES4_St9_IdentityIS4_ESt4lessIS4_ENS1_13ZoneAllocatorIS4_EEE8_M_eraseEPSt13_Rb_tree_nodeIS4_E
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L2642
.L2641:
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.L2643
.L2640:
	movq	-736(%rbp), %rax
	testq	%rax, %rax
	je	.L2644
	.p2align 4,,10
	.p2align 3
.L2645:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L2645
.L2644:
	movq	-744(%rbp), %rax
	movq	-752(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12GraphReducerD1Ev@PLT
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L2646
	leaq	-104(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L2646:
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	je	.L2647
	movq	-112(%rbp), %rbx
	movq	-144(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L2648
	movq	-192(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L2651:
	testq	%rax, %rax
	je	.L2649
	cmpq	$32, 8(%rax)
	ja	.L2650
.L2649:
	movq	(%rdx), %rax
	movq	$32, 8(%rax)
	movq	-192(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -192(%rbp)
.L2650:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L2651
	movq	-184(%rbp), %rax
.L2648:
	movq	-176(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L2647
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L2647:
	movq	-280(%rbp), %rax
	testq	%rax, %rax
	je	.L2638
	movq	-208(%rbp), %rbx
	movq	-240(%rbp), %rdx
	leaq	8(%rbx), %rcx
	cmpq	%rdx, %rcx
	jbe	.L2654
	movq	-288(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L2657:
	testq	%rax, %rax
	je	.L2655
	cmpq	$64, 8(%rax)
	ja	.L2656
.L2655:
	movq	(%rdx), %rax
	movq	$64, 8(%rax)
	movq	-288(%rbp), %rsi
	movq	%rsi, (%rax)
	movq	%rax, -288(%rbp)
.L2656:
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L2657
	movq	-280(%rbp), %rax
.L2654:
	movq	-272(%rbp), %rdx
	leaq	0(,%rdx,8), %rcx
	cmpq	$15, %rcx
	jbe	.L2638
	movq	%rdx, 8(%rax)
	movq	$0, (%rax)
.L2638:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2691
	addq	$800, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2690:
	.cfi_restore_state
	movq	8(%r13), %rdi
	movq	0(%r13), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r13)
	movq	%rax, %r8
	jmp	.L2639
.L2691:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26564:
	.size	_ZN2v88internal8compiler19EscapeAnalysisPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE, .-_ZN2v88internal8compiler19EscapeAnalysisPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler18TypedLoweringPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8compiler18TypedLoweringPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler18TypedLoweringPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler18TypedLoweringPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE, @function
_ZN2v88internal8compiler18TypedLoweringPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE:
.LFB26562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$696, %rsp
	.cfi_offset 3, -56
	movq	216(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	352(%rbx), %r8
	testq	%r8, %r8
	je	.L2696
.L2693:
	leaq	16+_ZTVN2v88internal8compiler16JSCreateLoweringE(%rip), %rax
	leaq	-320(%rbp), %r13
	movq	%r14, %rsi
	movq	160(%r12), %rdx
	movq	%rax, %xmm1
	movq	24(%r12), %rax
	movq	%r13, %xmm2
	movq	%r13, %rdi
	punpcklqdq	%xmm2, %xmm1
	leaq	-448(%rbp), %r15
	leaq	-640(%rbp), %rbx
	leaq	160(%rax), %rcx
	movaps	%xmm1, -704(%rbp)
	call	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE@PLT
	leaq	-608(%rbp), %rax
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	200(%r12), %rcx
	movq	160(%r12), %rdx
	movq	%rax, %rdi
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal8compiler19DeadCodeEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE@PLT
	movdqa	-704(%rbp), %xmm1
	movq	%r14, %r8
	movdqu	216(%r12), %xmm3
	movdqu	312(%r12), %xmm0
	leaq	-512(%rbp), %r11
	movq	%r13, %rsi
	movq	320(%r12), %rcx
	movq	216(%r12), %rdx
	movq	%r11, %rdi
	movaps	%xmm1, -560(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movq	%r11, -728(%rbp)
	movaps	%xmm0, -544(%rbp)
	movq	%rcx, -528(%rbp)
	movq	%r14, -520(%rbp)
	call	_ZN2v88internal8compiler15JSTypedLoweringC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneE@PLT
	leaq	-672(%rbp), %rax
	movq	320(%r12), %rcx
	movq	%r13, %rsi
	movq	216(%r12), %rdx
	movq	%rax, %rdi
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler22ConstantFoldingReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE@PLT
	movq	320(%r12), %r8
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	216(%r12), %rcx
	movq	312(%r12), %rdx
	call	_ZN2v88internal8compiler17TypedOptimizationC1EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE@PLT
	movq	320(%r12), %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	216(%r12), %rdx
	call	_ZN2v88internal8compiler25SimplifiedOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE@PLT
	leaq	-688(%rbp), %r10
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -720(%rbp)
	call	_ZN2v88internal8compiler21CheckpointEliminationC1EPNS1_15AdvancedReducer6EditorE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	320(%r12), %rcx
	pushq	%r14
	movq	192(%r12), %r9
	leaq	-384(%rbp), %rax
	movq	200(%r12), %r8
	movq	160(%r12), %rdx
	movq	%rax, %rdi
	movq	%rax, -712(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE@PLT
	movq	-736(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	leaq	-560(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-704(%rbp), %r14
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-728(%rbp), %r11
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-720(%rbp), %r10
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-712(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorReducerD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler17TypedOptimizationD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler22ConstantFoldingReducerD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12GraphReducerD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2697
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2696:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%rbx)
	movq	%rax, %r8
	jmp	.L2693
.L2697:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26562:
	.size	_ZN2v88internal8compiler18TypedLoweringPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE, .-_ZN2v88internal8compiler18TypedLoweringPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler20LoadEliminationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8compiler20LoadEliminationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler20LoadEliminationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler20LoadEliminationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE, @function
_ZN2v88internal8compiler20LoadEliminationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE:
.LFB26611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$1016, %rsp
	movq	216(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	352(%r13), %r8
	testq	%r8, %r8
	je	.L2702
.L2699:
	leaq	16+_ZTVN2v88internal8compiler15LoadEliminationE(%rip), %rax
	leaq	-320(%rbp), %r13
	movq	%rbx, %rsi
	movq	160(%r12), %rdx
	movq	%rax, %xmm0
	movq	24(%r12), %rax
	movq	%r13, %xmm1
	movq	%r13, %rdi
	punpcklqdq	%xmm1, %xmm0
	leaq	-592(%rbp), %r14
	leaq	-848(%rbp), %r15
	leaq	160(%rax), %rcx
	movaps	%xmm0, -1008(%rbp)
	call	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	216(%r12), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler17BranchEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE@PLT
	leaq	-944(%rbp), %rax
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	200(%r12), %rcx
	movq	160(%r12), %rdx
	movq	%rax, %rdi
	movq	%rax, -1024(%rbp)
	call	_ZN2v88internal8compiler19DeadCodeEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21RedundancyEliminationC1EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE@PLT
	movdqa	-1008(%rbp), %xmm0
	leaq	-992(%rbp), %r11
	movq	216(%r12), %rdx
	movq	%r11, %rdi
	movq	%r13, %rsi
	movq	%r11, -1048(%rbp)
	movaps	%xmm0, -784(%rbp)
	movq	%rbx, -768(%rbp)
	movq	%rdx, -736(%rbp)
	movq	$0, -760(%rbp)
	movq	$0, -752(%rbp)
	movq	$0, -744(%rbp)
	call	_ZN2v88internal8compiler21CheckpointEliminationC1EPNS1_15AdvancedReducer6EditorE@PLT
	movq	160(%r12), %rdx
	leaq	-896(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -1008(%rbp)
	movq	(%rdx), %rdx
	call	_ZN2v88internal8compiler21ValueNumberingReducerC1EPNS0_4ZoneES4_@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	leaq	-720(%rbp), %r10
	pushq	%rbx
	movq	192(%r12), %r9
	movq	%r10, %rdi
	leaq	-656(%rbp), %rbx
	movq	320(%r12), %rcx
	movq	160(%r12), %rdx
	movq	%r10, -1040(%rbp)
	movq	200(%r12), %r8
	call	_ZN2v88internal8compiler21CommonOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE@PLT
	movq	216(%r12), %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	312(%r12), %rdx
	movq	320(%r12), %r8
	call	_ZN2v88internal8compiler17TypedOptimizationC1EPNS1_15AdvancedReducer6EditorEPNS1_23CompilationDependenciesEPNS1_7JSGraphEPNS1_12JSHeapBrokerE@PLT
	leaq	-976(%rbp), %r8
	movq	320(%r12), %rcx
	movq	%r13, %rsi
	movq	216(%r12), %rdx
	movq	%r8, %rdi
	movq	%r8, -1032(%rbp)
	call	_ZN2v88internal8compiler22ConstantFoldingReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE@PLT
	movq	320(%r12), %rcx
	leaq	-464(%rbp), %r9
	movq	%r13, %rsi
	movq	216(%r12), %rdx
	movq	%r9, %rdi
	movq	%r9, -1016(%rbp)
	call	_ZN2v88internal8compiler20TypeNarrowingReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-1024(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	leaq	-784(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-1016(%rbp), %r9
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, %rdx
	movq	%r9, -1024(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-1032(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, %rdx
	movq	%r8, -1016(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-1048(%rbp), %r11
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-1040(%rbp), %r10
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-1008(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv@PLT
	movq	-1024(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler20TypeNarrowingReducerD1Ev@PLT
	movq	-1016(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler22ConstantFoldingReducerD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler17TypedOptimizationD1Ev@PLT
	movq	-1008(%rbp), %rdi
	call	_ZN2v88internal8compiler21ValueNumberingReducerD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21RedundancyEliminationD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler17BranchEliminationD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12GraphReducerD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2703
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2702:
	.cfi_restore_state
	movq	8(%r13), %rdi
	movq	0(%r13), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r13)
	movq	%rax, %r8
	jmp	.L2699
.L2703:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26611:
	.size	_ZN2v88internal8compiler20LoadEliminationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE, .-_ZN2v88internal8compiler20LoadEliminationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler22EarlyOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8compiler22EarlyOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler22EarlyOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler22EarlyOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE, @function
_ZN2v88internal8compiler22EarlyOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE:
.LFB26594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$600, %rsp
	.cfi_offset 3, -56
	movq	216(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	352(%rbx), %r8
	testq	%r8, %r8
	je	.L2708
.L2705:
	movq	24(%r12), %rax
	leaq	-320(%rbp), %r13
	movq	%r14, %rsi
	movq	160(%r12), %rdx
	movq	%r13, %rdi
	leaq	-448(%rbp), %r15
	leaq	-496(%rbp), %rbx
	leaq	160(%rax), %rcx
	call	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE@PLT
	leaq	-544(%rbp), %rax
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	200(%r12), %rcx
	movq	160(%r12), %rdx
	movq	%rax, %rdi
	movq	%rax, -640(%rbp)
	call	_ZN2v88internal8compiler19DeadCodeEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE@PLT
	leaq	-608(%rbp), %rax
	movq	320(%r12), %rcx
	movq	%r13, %rsi
	movq	216(%r12), %rdx
	movq	%rax, %rdi
	movq	%rax, -616(%rbp)
	call	_ZN2v88internal8compiler25SimplifiedOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21RedundancyEliminationC1EPNS1_15AdvancedReducer6EditorEPNS0_4ZoneE@PLT
	movq	160(%r12), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	(%rdx), %rdx
	call	_ZN2v88internal8compiler21ValueNumberingReducerC1EPNS0_4ZoneES4_@PLT
	leaq	-576(%rbp), %r10
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	216(%r12), %rdx
	movq	%r10, %rdi
	movq	%r10, -632(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	320(%r12), %rcx
	pushq	%r14
	movq	192(%r12), %r9
	leaq	-384(%rbp), %r11
	movq	200(%r12), %r8
	movq	160(%r12), %rdx
	movq	%r11, %rdi
	movq	%r11, -624(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE@PLT
	movq	-640(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-616(%rbp), %r14
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-632(%rbp), %r10
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r10, %rdx
	movq	%r10, -616(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-624(%rbp), %r11
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv@PLT
	movq	-616(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducerD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21ValueNumberingReducerD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21RedundancyEliminationD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler25SimplifiedOperatorReducerD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12GraphReducerD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2709
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2708:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%rbx), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%rbx)
	movq	%rax, %r8
	jmp	.L2705
.L2709:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26594:
	.size	_ZN2v88internal8compiler22EarlyOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE, .-_ZN2v88internal8compiler22EarlyOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler8Pipeline23GenerateCodeForCodeStubEPNS0_7IsolateEPNS1_14CallDescriptorEPNS1_5GraphEPNS1_19SourcePositionTableENS0_4Code4KindEPKciNS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsE.str1.1,"aMS",@progbits,1
.LC184:
	.string	"V8.TFStubCodegen"
.LC185:
	.string	"V8.CSAEarlyOptimization"
	.section	.rodata._ZN2v88internal8compiler8Pipeline23GenerateCodeForCodeStubEPNS0_7IsolateEPNS1_14CallDescriptorEPNS1_5GraphEPNS1_19SourcePositionTableENS0_4Code4KindEPKciNS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsE.str1.8,"aMS",@progbits,1
	.align 8
.LC186:
	.string	"cannot create std::vector larger than max_size()"
	.section	.rodata._ZN2v88internal8compiler8Pipeline23GenerateCodeForCodeStubEPNS0_7IsolateEPNS1_14CallDescriptorEPNS1_5GraphEPNS1_19SourcePositionTableENS0_4Code4KindEPKciNS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsE.str1.1
.LC187:
	.string	"V8.CSAOptimization"
.LC188:
	.string	"(location_) != nullptr"
.LC189:
	.string	"Begin compiling "
	.section	.text._ZN2v88internal8compiler8Pipeline23GenerateCodeForCodeStubEPNS0_7IsolateEPNS1_14CallDescriptorEPNS1_5GraphEPNS1_19SourcePositionTableENS0_4Code4KindEPKciNS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Pipeline23GenerateCodeForCodeStubEPNS0_7IsolateEPNS1_14CallDescriptorEPNS1_5GraphEPNS1_19SourcePositionTableENS0_4Code4KindEPKciNS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsE
	.type	_ZN2v88internal8compiler8Pipeline23GenerateCodeForCodeStubEPNS0_7IsolateEPNS1_14CallDescriptorEPNS1_5GraphEPNS1_19SourcePositionTableENS0_4Code4KindEPKciNS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsE, @function
_ZN2v88internal8compiler8Pipeline23GenerateCodeForCodeStubEPNS0_7IsolateEPNS1_14CallDescriptorEPNS1_5GraphEPNS1_19SourcePositionTableENS0_4Code4KindEPKciNS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsE:
.LFB26722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$2568, %rsp
	movq	32(%rbp), %rax
	movq	(%rdx), %r14
	movq	%rdi, -2480(%rbp)
	movq	%rsi, -2544(%rbp)
	movq	%r9, %rdi
	movq	%rax, -2528(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	movl	%r15d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%rax, %rdx
	leaq	-1840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2488(%rbp)
	call	_ZN2v88internal24OptimizedCompilationInfoC1ENS0_6VectorIKcEEPNS0_4ZoneENS0_4Code4KindE@PLT
	movl	16(%rbp), %eax
	cmpl	$1, 24(%rbp)
	movl	%eax, -1828(%rbp)
	je	.L2711
	movl	24(%rbp), %eax
	movl	%eax, -1836(%rbp)
.L2711:
	movq	-2480(%rbp), %r15
	leaq	-2240(%rbp), %rax
	leaq	-2160(%rbp), %r14
	movq	%rax, %rdi
	movq	%rax, -2496(%rbp)
	movq	41136(%r15), %rsi
	call	_ZN2v88internal8compiler9ZoneStatsC1EPNS0_19AccountingAllocatorE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler15NodeOriginTableC1EPNS1_5GraphE@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	cmpb	$0, 41456(%r15)
	movl	$0, -2448(%rbp)
	movb	$0, -2444(%rbp)
	movq	$0, -2424(%rbp)
	movq	$0, -2416(%rbp)
	movups	%xmm0, -2440(%rbp)
	je	.L2712
	cmpb	$0, _ZN2v88internal28FLAG_turbo_rewrite_far_jumpsE(%rip)
	jne	.L2836
.L2712:
	subq	$8, %rsp
	pushq	-2528(%rbp)
	movq	-2488(%rbp), %rdx
	leaq	-1040(%rbp), %rdi
	movq	-2496(%rbp), %rsi
	pushq	%rax
	movq	%rbx, %r9
	movq	-2480(%rbp), %rax
	pushq	%r14
	pushq	%r13
	pushq	$0
	movq	41136(%rax), %r8
	movq	%rax, %rcx
	movq	%rdi, -2520(%rbp)
	call	_ZN2v88internal8compiler12PipelineDataC1EPNS1_9ZoneStatsEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS0_19AccountingAllocatorEPNS1_5GraphEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableEPNS0_20JumpOptimizationInfoERKNS0_16AssemblerOptionsE
	addq	$48, %rsp
	cmpb	$0, _ZN2v88internal16FLAG_turbo_statsE(%rip)
	movb	$0, -975(%rbp)
	jne	.L2713
	cmpb	$0, _ZN2v88internal20FLAG_turbo_stats_nvpE(%rip)
	movq	$0, -2552(%rbp)
	jne	.L2713
.L2714:
	movq	-2520(%rbp), %rax
	movq	%rax, -2456(%rbp)
	testl	$49152, -1840(%rbp)
	je	.L2763
	movq	-1032(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2837
	call	_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv@PLT
	movq	%rax, %rbx
.L2717:
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%rbx), %rsi
	je	.L2718
	testq	%rsi, %rsi
	je	.L2838
.L2719:
	addl	$1, 152(%rbx)
.L2718:
	leaq	-1392(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movl	$52, %edx
	leaq	.LC106(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$16, %edx
	leaq	.LC189(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$15, %edx
	leaq	.LC108(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1392(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-1152(%rbp,%rax), %r12
	testq	%r12, %r12
	je	.L2839
	cmpb	$0, 56(%r12)
	je	.L2720
	movsbl	67(%r12), %esi
.L2721:
	movq	%r13, %rdi
	leaq	-1664(%rbp), %r12
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	testb	$64, -1839(%rbp)
	jne	.L2840
.L2722:
	movq	-2456(%rbp), %r13
	movq	56(%r13), %r14
	testq	%r14, %r14
	je	.L2724
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L2724:
	movq	48(%r13), %r15
	movq	176(%r13), %r13
	testq	%r13, %r13
	je	.L2725
	leaq	.LC4(%rip), %rax
	movq	48(%r13), %r8
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, 48(%r13)
	movq	%r8, -2504(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-2456(%rbp), %rsi
	leaq	.LC172(%rip), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -2472(%rbp)
	call	_ZN2v88internal8compiler15PrintGraphPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneEPKc
	movq	-2504(%rbp), %r8
	movq	-2472(%rbp), %r9
	movq	%r8, 48(%r13)
.L2756:
	testq	%r9, %r9
	je	.L2726
	movq	%r9, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L2726:
	testq	%r14, %r14
	je	.L2727
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L2727:
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, -1312(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-1328(%rbp), %rdi
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -1392(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-1312(%rbp), %rdi
	movq	%rax, -1392(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1312(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L2729
	subl	$1, 152(%rbx)
	je	.L2841
.L2729:
	movq	-2456(%rbp), %rbx
.L2715:
	movq	56(%rbx), %rax
	movq	%rax, -2536(%rbp)
	testq	%rax, %rax
	je	.L2731
	leaq	.LC185(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L2731:
	movq	48(%rbx), %rax
	movq	%rax, -2472(%rbp)
	movq	176(%rbx), %rax
	movq	%rax, -2560(%rbp)
	testq	%rax, %rax
	je	.L2732
	movq	48(%rax), %rcx
	movq	%rcx, -2592(%rbp)
	leaq	.LC185(%rip), %rcx
	movq	%rcx, 48(%rax)
.L2732:
	movq	-2472(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-2456(%rbp), %rbx
	movq	%rax, %r14
	movq	216(%rbx), %r15
	movq	352(%r15), %r8
	testq	%r8, %r8
	je	.L2842
.L2733:
	leaq	16+_ZTVN2v88internal8compiler18CsaLoadEliminationE(%rip), %rax
	movq	%r12, %xmm1
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %xmm0
	movq	24(%rbx), %rax
	movq	160(%rbx), %rdx
	leaq	-1968(%rbp), %r13
	punpcklqdq	%xmm1, %xmm0
	leaq	-2352(%rbp), %r15
	leaq	160(%rax), %rcx
	movaps	%xmm0, -2576(%rbp)
	call	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE@PLT
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	216(%rbx), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler17BranchEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE@PLT
	leaq	-2400(%rbp), %rax
	movq	%r14, %r8
	movq	%r12, %rsi
	movq	200(%rbx), %rcx
	movq	160(%rbx), %rdx
	movq	%rax, %rdi
	movq	%rax, -2504(%rbp)
	call	_ZN2v88internal8compiler19DeadCodeEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE@PLT
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	320(%rbx), %rcx
	movq	192(%rbx), %r9
	movq	200(%rbx), %r8
	leaq	-2304(%rbp), %rax
	movq	160(%rbx), %rdx
	pushq	%r14
	movq	%rax, %rdi
	movq	%rax, -2512(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE@PLT
	movq	160(%rbx), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler21ValueNumberingReducerC1EPNS0_4ZoneES4_@PLT
	movdqa	-2576(%rbp), %xmm0
	movq	216(%rbx), %rcx
	movb	$0, -2032(%rbp)
	movq	%r14, -2024(%rbp)
	popq	%rsi
	movaps	%xmm0, -2064(%rbp)
	pxor	%xmm0, %xmm0
	popq	%rdi
	movaps	%xmm0, -2048(%rbp)
	movq	(%rcx), %rax
	movl	28(%rax), %eax
	cmpq	$268435455, %rax
	ja	.L2843
	movq	%r14, -2016(%rbp)
	xorl	%r8d, %r8d
	leaq	0(,%rax,8), %rdx
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	$0, -1992(%rbp)
	testq	%rax, %rax
	je	.L2735
	movq	16(%r14), %rdi
	movq	24(%r14), %rax
	movq	%rdx, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L2844
	addq	%rdi, %rsi
	movq	%rsi, 16(%r14)
.L2737:
	leaq	(%rdi,%rdx), %r8
	xorl	%esi, %esi
	movq	%rcx, -2584(%rbp)
	movq	%r8, -1992(%rbp)
	movq	%r8, -2576(%rbp)
	movq	%rdi, -2008(%rbp)
	call	memset@PLT
	movq	-2584(%rbp), %rcx
	movq	-2576(%rbp), %r8
.L2735:
	movq	%rcx, %xmm0
	movq	%r14, %xmm2
	movq	%r13, %rdx
	movq	%r12, %rsi
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, %rdi
	movq	%r8, -2000(%rbp)
	movaps	%xmm0, -1984(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-2504(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-2512(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	leaq	-2064(%rbp), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movq	%rax, -2504(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21ValueNumberingReducerD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler17BranchEliminationD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducerD1Ev@PLT
	movq	-2560(%rbp), %rax
	testq	%rax, %rax
	je	.L2738
	movq	-2592(%rbp), %rcx
	movq	%rcx, 48(%rax)
.L2738:
	testq	%r14, %r14
	je	.L2739
	movq	-2472(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L2739:
	movq	-2536(%rbp), %rax
	testq	%rax, %rax
	je	.L2740
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L2740:
	leaq	-2456(%rbp), %r14
	leaq	.LC185(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl3RunINS1_23MemoryOptimizationPhaseEJEEEvDpOT0_.isra.0
	leaq	.LC54(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	-2456(%rbp), %rbx
	movq	56(%rbx), %rax
	movq	%rax, -2560(%rbp)
	testq	%rax, %rax
	je	.L2741
	leaq	.LC187(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L2741:
	movq	48(%rbx), %rax
	movq	176(%rbx), %r11
	movq	%rax, -2536(%rbp)
	testq	%r11, %r11
	je	.L2742
	movq	48(%r11), %rax
	movq	%rax, -2600(%rbp)
	leaq	.LC187(%rip), %rax
	movq	%rax, 48(%r11)
.L2742:
	movq	-2536(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r11, -2576(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-2456(%rbp), %rbx
	movq	-2576(%rbp), %r11
	movq	%rax, -2472(%rbp)
	movq	216(%rbx), %r9
	movq	352(%r9), %r8
	testq	%r8, %r8
	je	.L2845
.L2743:
	movq	24(%rbx), %rax
	movq	160(%rbx), %rdx
	movq	%r12, %rdi
	movq	%r11, -2576(%rbp)
	movq	-2472(%rbp), %rsi
	leaq	160(%rax), %rcx
	call	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE@PLT
	movq	216(%rbx), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	-2472(%rbp), %rcx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler17BranchEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE@PLT
	movq	-2472(%rbp), %r8
	movq	%r12, %rsi
	movq	200(%rbx), %rcx
	movq	160(%rbx), %rdx
	movq	-2512(%rbp), %rdi
	call	_ZN2v88internal8compiler19DeadCodeEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE@PLT
	movl	$1, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	216(%rbx), %rdx
	call	_ZN2v88internal8compiler22MachineOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb@PLT
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	192(%rbx), %r9
	movq	200(%rbx), %r8
	movq	320(%rbx), %rcx
	movq	160(%rbx), %rdx
	pushq	-2472(%rbp)
	movq	-2504(%rbp), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-2512(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-2504(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducerD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler17BranchEliminationD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducerD1Ev@PLT
	movq	-2576(%rbp), %r11
	popq	%rax
	popq	%rdx
	testq	%r11, %r11
	je	.L2744
	movq	-2600(%rbp), %rax
	movq	%rax, 48(%r11)
.L2744:
	movq	-2472(%rbp), %rax
	testq	%rax, %rax
	je	.L2745
	movq	-2536(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L2745:
	movq	-2560(%rbp), %rax
	testq	%rax, %rax
	je	.L2746
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L2746:
	leaq	.LC187(%rip), %rsi
	movq	%r14, %rdi
	leaq	-576(%rbp), %r15
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movb	$1, -1664(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl3RunINS1_16VerifyGraphPhaseEJbEEEvDpOT0_.isra.0
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl21ComputeScheduledGraphEv
	movq	-2480(%rbp), %rax
	subq	$8, %rsp
	pushq	-2528(%rbp)
	movq	-2496(%rbp), %rsi
	pushq	-624(%rbp)
	movq	%r15, %rdi
	movq	-880(%rbp), %r9
	pushq	-864(%rbp)
	movq	%rax, %rcx
	movq	-2488(%rbp), %rdx
	pushq	-872(%rbp)
	pushq	-808(%rbp)
	movq	41136(%rax), %r8
	call	_ZN2v88internal8compiler12PipelineDataC1EPNS1_9ZoneStatsEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS0_19AccountingAllocatorEPNS1_5GraphEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableEPNS0_20JumpOptimizationInfoERKNS0_16AssemblerOptionsE
	movq	-2504(%rbp), %rbx
	addq	$48, %rsp
	movq	%r13, %rsi
	movq	-2544(%rbp), %rax
	movb	$0, -511(%rbp)
	movq	%rbx, %rdi
	movq	%r15, -2064(%rbp)
	movq	%rax, -1968(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE
	testb	%al, %al
	je	.L2747
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	$0, -1664(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE
	movq	-1664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2747
	movq	(%rdi), %rax
	call	*8(%rax)
.L2747:
	cmpb	$0, -2444(%rbp)
	je	.L2749
	movq	-2544(%rbp), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	$1, -2448(%rbp)
	movq	%rax, -1968(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE
	testb	%al, %al
	jne	.L2846
.L2751:
	leaq	.LC188(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2713:
	movq	-2480(%rbp), %rdi
	call	_ZN2v88internal7Isolate18GetTurboStatisticsEv@PLT
	movl	$176, %edi
	movq	%rax, %r13
	call	_ZN2v88internal8MallocednwEm@PLT
	movq	-2488(%rbp), %rsi
	movq	-2496(%rbp), %rcx
	movq	%r13, %rdx
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -2552(%rbp)
	call	_ZN2v88internal8compiler18PipelineStatisticsC1EPNS0_24OptimizedCompilationInfoEPNS0_21CompilationStatisticsEPNS1_9ZoneStatsE@PLT
	leaq	.LC184(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc@PLT
	jmp	.L2714
	.p2align 4,,10
	.p2align 3
.L2720:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2721
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2721
	.p2align 4,,10
	.p2align 3
.L2836:
	leaq	-2448(%rbp), %rax
	jmp	.L2712
	.p2align 4,,10
	.p2align 3
.L2749:
	movq	-2504(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2751
.L2753:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12PipelineDataD1Ev
	movq	-2552(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2754
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18PipelineStatisticsD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L2754:
	movq	-2520(%rbp), %rdi
	call	_ZN2v88internal8compiler12PipelineDataD1Ev
	movq	-2440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2755
	call	_ZdlPv@PLT
.L2755:
	movq	-2496(%rbp), %rdi
	call	_ZN2v88internal8compiler9ZoneStatsD1Ev@PLT
	movq	-2488(%rbp), %rdi
	call	_ZN2v88internal24OptimizedCompilationInfoD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2847
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2763:
	.cfi_restore_state
	movq	%rax, %rbx
	leaq	-1664(%rbp), %r12
	jmp	.L2715
	.p2align 4,,10
	.p2align 3
.L2840:
	movq	-2488(%rbp), %r14
	leaq	-576(%rbp), %r15
	movl	$32, %edx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode@PLT
	movl	$14, %edx
	leaq	.LC60(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%r15, %rdi
	pushq	$0
	movq	-2480(%rbp), %r8
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movl	$-1, %esi
	call	_ZN2v88internal8compiler23JsonPrintFunctionSourceERSoiSt10unique_ptrIA_cSt14default_deleteIS4_EENS0_6HandleINS0_6ScriptEEEPNS0_7IsolateENS8_INS0_18SharedFunctionInfoEEEb@PLT
	movq	-1664(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L2723
	call	_ZdaPv@PLT
.L2723:
	movq	%r15, %rdi
	movl	$12, %edx
	leaq	.LC61(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileD1Ev@PLT
	jmp	.L2722
	.p2align 4,,10
	.p2align 3
.L2846:
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	$0, -1664(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE
	movq	-1664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2752
	movq	(%rdi), %rax
	call	*8(%rax)
.L2752:
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L2753
	jmp	.L2751
	.p2align 4,,10
	.p2align 3
.L2837:
	movq	-1040(%rbp), %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	movq	%rax, %rbx
	jmp	.L2717
	.p2align 4,,10
	.p2align 3
.L2725:
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-2456(%rbp), %rsi
	leaq	.LC172(%rip), %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -2472(%rbp)
	call	_ZN2v88internal8compiler15PrintGraphPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneEPKc
	movq	-2472(%rbp), %r9
	jmp	.L2756
	.p2align 4,,10
	.p2align 3
.L2845:
	movq	(%r9), %r10
	movq	8(%r9), %rdi
	movq	%r11, -2592(%rbp)
	movq	%r9, -2576(%rbp)
	movq	%r10, -2584(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	-2584(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-2576(%rbp), %r9
	movq	-2592(%rbp), %r11
	movq	%rax, %r8
	movq	%rax, 352(%r9)
	jmp	.L2743
	.p2align 4,,10
	.p2align 3
.L2842:
	movq	8(%r15), %rdi
	movq	(%r15), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r15)
	movq	%rax, %r8
	jmp	.L2733
	.p2align 4,,10
	.p2align 3
.L2841:
	movq	144(%rbx), %rdi
	call	fclose@PLT
	movq	$0, 144(%rbx)
	jmp	.L2729
	.p2align 4,,10
	.p2align 3
.L2838:
	movq	(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %rsi
	jmp	.L2719
	.p2align 4,,10
	.p2align 3
.L2844:
	movq	%r14, %rdi
	movq	%rdx, -2584(%rbp)
	movq	%rcx, -2576(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-2576(%rbp), %rcx
	movq	-2584(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L2737
.L2843:
	leaq	.LC186(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2847:
	call	__stack_chk_fail@PLT
.L2839:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE26722:
	.size	_ZN2v88internal8compiler8Pipeline23GenerateCodeForCodeStubEPNS0_7IsolateEPNS1_14CallDescriptorEPNS1_5GraphEPNS1_19SourcePositionTableENS0_4Code4KindEPKciNS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsE, .-_ZN2v88internal8compiler8Pipeline23GenerateCodeForCodeStubEPNS0_7IsolateEPNS1_14CallDescriptorEPNS1_5GraphEPNS1_19SourcePositionTableENS0_4Code4KindEPKciNS0_24PoisoningMitigationLevelERKNS0_16AssemblerOptionsE
	.section	.rodata._ZN2v88internal8compiler8Pipeline27GenerateCodeForWasmFunctionEPNS0_24OptimizedCompilationInfoEPNS0_4wasm10WasmEngineEPNS1_12MachineGraphEPNS1_14CallDescriptorEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS5_12FunctionBodyEPKNS5_10WasmModuleEi.str1.1,"aMS",@progbits,1
.LC190:
	.string	"disabled-by-default-v8.wasm"
.LC191:
	.string	"V8.WasmInitializing"
.LC192:
	.string	"\", \"source\":\""
	.section	.rodata._ZN2v88internal8compiler8Pipeline27GenerateCodeForWasmFunctionEPNS0_24OptimizedCompilationInfoEPNS0_4wasm10WasmEngineEPNS1_12MachineGraphEPNS1_14CallDescriptorEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS5_12FunctionBodyEPKNS5_10WasmModuleEi.str1.8,"aMS",@progbits,1
	.align 8
.LC193:
	.string	"\",\n\"sourceLineToBytecodePosition\" : ["
	.section	.rodata._ZN2v88internal8compiler8Pipeline27GenerateCodeForWasmFunctionEPNS0_24OptimizedCompilationInfoEPNS0_4wasm10WasmEngineEPNS1_12MachineGraphEPNS1_14CallDescriptorEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS5_12FunctionBodyEPKNS5_10WasmModuleEi.str1.1
.LC194:
	.string	"],\n\"phases\":["
.LC195:
	.string	"V8.WasmOptimization"
.LC196:
	.string	"V8.WasmFullOptimization"
.LC197:
	.string	"V8.WasmBaseOptimization"
	.section	.text._ZN2v88internal8compiler8Pipeline27GenerateCodeForWasmFunctionEPNS0_24OptimizedCompilationInfoEPNS0_4wasm10WasmEngineEPNS1_12MachineGraphEPNS1_14CallDescriptorEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS5_12FunctionBodyEPKNS5_10WasmModuleEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Pipeline27GenerateCodeForWasmFunctionEPNS0_24OptimizedCompilationInfoEPNS0_4wasm10WasmEngineEPNS1_12MachineGraphEPNS1_14CallDescriptorEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS5_12FunctionBodyEPKNS5_10WasmModuleEi
	.type	_ZN2v88internal8compiler8Pipeline27GenerateCodeForWasmFunctionEPNS0_24OptimizedCompilationInfoEPNS0_4wasm10WasmEngineEPNS1_12MachineGraphEPNS1_14CallDescriptorEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS5_12FunctionBodyEPKNS5_10WasmModuleEi, @function
_ZN2v88internal8compiler8Pipeline27GenerateCodeForWasmFunctionEPNS0_24OptimizedCompilationInfoEPNS0_4wasm10WasmEngineEPNS1_12MachineGraphEPNS1_14CallDescriptorEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS5_12FunctionBodyEPKNS5_10WasmModuleEi:
.LFB26823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2232, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	48(%rbp), %rax
	movq	%rdi, -2096(%rbp)
	movq	%rsi, -2128(%rbp)
	addq	$408, %rsi
	movq	%rcx, -2136(%rbp)
	movq	%r8, -2168(%rbp)
	movq	%r9, -2176(%rbp)
	movq	%rax, -2160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1824(%rbp), %rax
	movq	%rsi, -2144(%rbp)
	movq	%rax, %rdi
	movq	%rax, -2104(%rbp)
	call	_ZN2v88internal8compiler9ZoneStatsC1EPNS0_19AccountingAllocatorE@PLT
	movdqu	16(%rbp), %xmm3
	movdqu	32(%rbp), %xmm4
	movaps	%xmm3, -1744(%rbp)
	movaps	%xmm4, -1728(%rbp)
	movq	_ZZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsEPNS0_4wasm10WasmEngineENS3_12FunctionBodyEPKNS3_10WasmModuleEPNS0_24OptimizedCompilationInfoEPNS1_9ZoneStatsEE28trace_event_unique_atomic847(%rip), %rax
	testq	%rax, %rax
	je	.L3045
.L2850:
	movzbl	(%rax), %eax
	testb	$5, %al
	jne	.L2852
	cmpb	$0, _ZN2v88internal21FLAG_turbo_stats_wasmE(%rip)
	movq	$0, -2112(%rbp)
	je	.L2853
.L2852:
	movq	-2128(%rbp), %rdi
	call	_ZN2v88internal4wasm10WasmEngine26GetOrCreateTurboStatisticsEv@PLT
	movl	$176, %edi
	movq	%rax, %r12
	call	_ZN2v88internal8MallocednwEm@PLT
	movq	-2096(%rbp), %rsi
	movq	-2104(%rbp), %rcx
	movq	%r12, %rdx
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -2112(%rbp)
	call	_ZN2v88internal8compiler18PipelineStatisticsC1EPNS0_24OptimizedCompilationInfoEPNS0_21CompilationStatisticsEPNS1_9ZoneStatsE@PLT
	leaq	.LC191(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc@PLT
.L2853:
	movq	-2096(%rbp), %rax
	leaq	-1744(%rbp), %r12
	movl	(%rax), %eax
	movl	%eax, -2192(%rbp)
	leaq	-1040(%rbp), %rax
	movq	%rax, -2088(%rbp)
	testl	$16384, -2192(%rbp)
	jne	.L3046
.L2854:
	movq	-2104(%rbp), %rbx
	leaq	.LC158(%rip), %r13
	leaq	-2080(%rbp), %rdi
	movq	%r13, %xmm0
	movq	%rbx, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -2192(%rbp)
	call	_ZN2v88internal4wasm21WasmInstructionBuffer3NewEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20WasmAssemblerOptionsEv@PLT
	movq	-2128(%rbp), %rax
	movq	-2096(%rbp), %rsi
	leaq	-1008(%rbp), %rdi
	movq	$0, -1040(%rbp)
	movq	%rax, -1032(%rbp)
	movq	-2144(%rbp), %rax
	movq	%rsi, -1016(%rbp)
	movq	%rax, -1024(%rbp)
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	xorl	%r8d, %r8d
	pxor	%xmm2, %xmm2
	movq	%r13, %rsi
	movdqa	-2192(%rbp), %xmm0
	movq	-2112(%rbp), %rax
	movq	%rbx, %rdi
	movw	%r8w, -976(%rbp)
	movaps	%xmm2, -944(%rbp)
	movaps	%xmm0, -912(%rbp)
	movb	$0, -1000(%rbp)
	movq	%rbx, -992(%rbp)
	movq	%rax, -984(%rbp)
	movl	$-1, -972(%rbp)
	movb	$0, -968(%rbp)
	movb	$0, -960(%rbp)
	movq	$0, -928(%rbp)
	movl	$0, -920(%rbp)
	movb	$0, -916(%rbp)
	movq	$0, -896(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-992(%rbp), %rdi
	pxor	%xmm2, %xmm2
	leaq	.LC159(%rip), %rsi
	movq	%rax, %xmm0
	movq	(%r14), %rax
	movaps	%xmm2, -832(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rsi, -800(%rbp)
	movq	%rax, -880(%rbp)
	movq	-2168(%rbp), %rax
	movq	%rdi, -792(%rbp)
	movq	%rax, -872(%rbp)
	movq	-2176(%rbp), %rax
	movaps	%xmm0, -896(%rbp)
	movq	%rax, -864(%rbp)
	movq	16(%r14), %rax
	movq	$0, -856(%rbp)
	movq	%rax, -848(%rbp)
	movq	8(%r14), %rax
	movq	%r14, -816(%rbp)
	movq	%rax, -840(%rbp)
	movq	$0, -808(%rbp)
	movq	$0, -784(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-992(%rbp), %rdi
	leaq	.LC160(%rip), %rsi
	movq	$0, -768(%rbp)
	movq	%rax, %xmm0
	movq	%rsi, -760(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rdi, -752(%rbp)
	movaps	%xmm0, -784(%rbp)
	movq	$0, -744(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-992(%rbp), %rdi
	pxor	%xmm2, %xmm2
	leaq	.LC161(%rip), %rsi
	movq	%rax, %xmm0
	movups	%xmm2, -728(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rsi, -704(%rbp)
	movups	%xmm0, -744(%rbp)
	movq	$0, -712(%rbp)
	movq	%rdi, -696(%rbp)
	movq	$0, -688(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	pxor	%xmm2, %xmm2
	movdqa	-1744(%rbp), %xmm5
	movq	$0, -648(%rbp)
	movq	%rax, %xmm0
	leaq	-640(%rbp), %rax
	movb	$0, -640(%rbp)
	movq	%rax, -656(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	-1728(%rbp), %rax
	movq	$0, -624(%rbp)
	movq	%rax, -600(%rbp)
	movq	-2088(%rbp), %rax
	movq	$0, -592(%rbp)
	movq	%rax, -2072(%rbp)
	movq	-1016(%rbp), %rax
	movaps	%xmm0, -688(%rbp)
	movaps	%xmm2, -672(%rbp)
	movups	%xmm5, -616(%rbp)
	testl	$49152, (%rax)
	je	.L2868
	movq	-1032(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3047
	call	_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv@PLT
	movq	%rax, %rbx
.L2870:
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%rbx), %rsi
	je	.L2871
	testq	%rsi, %rsi
	je	.L3048
.L2872:
	addl	$1, 152(%rbx)
.L2871:
	leaq	-1440(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movl	$52, %edx
	leaq	.LC106(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$23, %edx
	leaq	.LC163(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1016(%rbp), %rsi
	leaq	-1984(%rbp), %rdi
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movq	-1984(%rbp), %r14
	testq	%r14, %r14
	je	.L3049
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2874:
	movl	$15, %edx
	leaq	.LC108(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1440(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-1200(%rbp,%rax), %r14
	testq	%r14, %r14
	je	.L2923
	cmpb	$0, 56(%r14)
	je	.L2876
	movsbl	67(%r14), %esi
.L2877:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-1984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2878
	call	_ZdaPv@PLT
.L2878:
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, -1360(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-1376(%rbp), %rdi
	movq	%rax, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -1440(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-1360(%rbp), %rdi
	movq	%rax, -1440(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1360(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L2868
	subl	$1, 152(%rbx)
	je	.L3050
.L2868:
	leaq	-2072(%rbp), %r13
	leaq	.LC173(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	-984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2882
	leaq	.LC195(%rip), %rsi
	call	_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc@PLT
.L2882:
	movq	-2160(%rbp), %rax
	movzbl	392(%rax), %eax
	testb	%al, %al
	setne	%r14b
	cmpb	$0, _ZN2v88internal20FLAG_turbo_splittingE(%rip)
	jne	.L3051
	cmpb	$0, _ZN2v88internal13FLAG_wasm_optE(%rip)
	jne	.L2887
	testb	%r14b, %r14b
	je	.L2888
.L2887:
	movq	-984(%rbp), %rdi
	movq	%rdi, -2032(%rbp)
	testq	%rdi, %rdi
	je	.L2886
	leaq	.LC196(%rip), %rsi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L2886:
	leaq	.LC3(%rip), %rax
	movq	-992(%rbp), %rdi
	movq	$0, -2008(%rbp)
	movq	%rax, -2024(%rbp)
	movq	-864(%rbp), %rax
	movq	%rdi, -2016(%rbp)
	movq	%rax, -2000(%rbp)
	testq	%rax, %rax
	je	.L2889
	movq	48(%rax), %rdx
	leaq	.LC196(%rip), %rcx
	movq	%rdx, -1992(%rbp)
	movq	%rcx, 48(%rax)
.L2889:
	movq	-816(%rbp), %rbx
	movq	352(%rbx), %r8
	testq	%r8, %r8
	je	.L3052
	movq	-1016(%rbp), %rax
	movq	-880(%rbp), %rdx
	leaq	.LC3(%rip), %rsi
	leaq	160(%rax), %rcx
.L2940:
	movq	%r8, -2144(%rbp)
	movq	%rdx, -2160(%rbp)
	movq	%rcx, -2128(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-2144(%rbp), %r8
	movq	-2160(%rbp), %rdx
	movq	%rax, -2008(%rbp)
	movq	-2128(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L2891
	.p2align 4,,10
	.p2align 3
.L3051:
	testb	%al, %al
	jne	.L2887
	movq	-1016(%rbp), %rax
	orl	$16, (%rax)
	cmpb	$0, _ZN2v88internal13FLAG_wasm_optE(%rip)
	jne	.L2887
.L2888:
	movq	-984(%rbp), %rdi
	movq	%rdi, -1936(%rbp)
	testq	%rdi, %rdi
	je	.L2896
	leaq	.LC197(%rip), %rsi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L2896:
	leaq	.LC3(%rip), %rax
	movq	-992(%rbp), %rdi
	movq	$0, -1912(%rbp)
	movq	%rax, -1928(%rbp)
	movq	-864(%rbp), %rax
	movq	%rdi, -1920(%rbp)
	movq	%rax, -1904(%rbp)
	testq	%rax, %rax
	je	.L2897
	movq	48(%rax), %rdx
	leaq	.LC197(%rip), %rcx
	movq	%rdx, -1896(%rbp)
	movq	%rcx, 48(%rax)
.L2897:
	movq	-816(%rbp), %rbx
	movq	352(%rbx), %r8
	testq	%r8, %r8
	je	.L3053
	movq	-1016(%rbp), %rax
	movq	-880(%rbp), %rdx
	leaq	.LC3(%rip), %rsi
	leaq	160(%rax), %rcx
.L2941:
	movq	%rcx, -2144(%rbp)
	movq	%r8, -2160(%rbp)
	movq	%rdx, -2128(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-2144(%rbp), %rcx
	movq	-2160(%rbp), %r8
	movq	%rax, -1912(%rbp)
	movq	-2128(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L2899
	.p2align 4,,10
	.p2align 3
.L2876:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2877
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2877
	.p2align 4,,10
	.p2align 3
.L3052:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%rbx)
	movq	%rax, %r8
	movq	-2008(%rbp), %rsi
	movq	-1016(%rbp), %rax
	movq	-880(%rbp), %rdx
	leaq	160(%rax), %rcx
	testq	%rsi, %rsi
	je	.L3054
.L2891:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE@PLT
	movq	-2008(%rbp), %r8
	testq	%r8, %r8
	je	.L3055
.L2892:
	leaq	-1984(%rbp), %rax
	movq	-880(%rbp), %rdx
	movq	-840(%rbp), %rcx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2128(%rbp)
	call	_ZN2v88internal8compiler19DeadCodeEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE@PLT
	movq	-880(%rbp), %rax
	movq	-2008(%rbp), %rsi
	movq	(%rax), %rdx
	testq	%rsi, %rsi
	je	.L3056
.L2893:
	leaq	-1936(%rbp), %r15
	leaq	-2064(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21ValueNumberingReducerC1EPNS0_4ZoneES4_@PLT
	movzbl	%r14b, %ecx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	-816(%rbp), %rdx
	call	_ZN2v88internal8compiler22MachineOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb@PLT
	movq	-2008(%rbp), %rax
	testq	%rax, %rax
	je	.L3057
.L2894:
	subq	$8, %rsp
	movq	-848(%rbp), %r9
	movq	-840(%rbp), %r8
	leaq	-1888(%rbp), %r14
	pushq	%rax
	movq	-720(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-880(%rbp), %rdx
	call	_ZN2v88internal8compiler21CommonOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE@PLT
	movq	-2128(%rbp), %rdx
	movq	-2088(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-2088(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-2088(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-2088(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducerD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21ValueNumberingReducerD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducerD1Ev@PLT
	leaq	-2032(%rbp), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116PipelineRunScopeD1Ev
	popq	%rsi
	popq	%rdi
.L2895:
	movq	%r13, %rdi
	leaq	.LC195(%rip), %rsi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2901
	call	_ZN2v88internal8compiler15NodeOriginTable15RemoveDecoratorEv@PLT
.L2901:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl21ComputeScheduledGraphEv
	movq	-2136(%rbp), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -1936(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE
	testb	%al, %al
	je	.L3044
	movq	-2080(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm21WasmInstructionBuffer10CreateViewEv@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE
	movq	-1744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2906
	movq	(%rdi), %rax
	call	*8(%rax)
.L2906:
	movq	%r14, %rdi
	call	_ZN2v84base11make_uniqueINS_8internal4wasm21WasmCompilationResultEJEEESt10unique_ptrIT_St14default_deleteIS6_EEDpOT0_
	movq	-2072(%rbp), %rax
	movq	-1888(%rbp), %rdx
	xorl	%esi, %esi
	movq	104(%rax), %rbx
	movl	1080(%rbx), %r8d
	leaq	760(%rbx), %rcx
	leaq	200(%rbx), %rdi
	call	_ZN2v88internal9Assembler7GetCodeEPNS0_7IsolateEPNS0_8CodeDescEPNS0_21SafepointTableBuilderEi@PLT
	movq	-2080(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm21WasmInstructionBuffer13ReleaseBufferEv@PLT
	movq	-1888(%rbp), %rax
	movq	-1744(%rbp), %rdx
	movq	$0, -1744(%rbp)
	movq	80(%rax), %rdi
	movq	%rdx, 80(%rax)
	testq	%rdi, %rdi
	je	.L2907
	call	_ZdaPv@PLT
	movq	-1744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3040
	call	_ZdaPv@PLT
.L3040:
	movq	-1888(%rbp), %rax
.L2907:
	movq	24(%rbx), %rdx
	movq	-2136(%rbp), %rdi
	movq	(%rdx), %rdx
	movl	4(%rdx), %edx
	movl	%edx, 88(%rax)
	call	_ZNK2v88internal8compiler14CallDescriptor23GetTaggedParameterSlotsEv@PLT
	movq	-1888(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movl	%eax, 92(%rdx)
	call	_ZN2v88internal8compiler13CodeGenerator22GetSourcePositionTableEv@PLT
	movq	-1888(%rbp), %r13
	movq	-1744(%rbp), %rax
	movq	$0, -1744(%rbp)
	movq	96(%r13), %rdi
	movq	%rax, 96(%r13)
	testq	%rdi, %rdi
	je	.L3058
	call	_ZdaPv@PLT
	movq	-1744(%rbp), %rdi
	movq	-1736(%rbp), %rax
	movq	%rax, 104(%r13)
	testq	%rdi, %rdi
	je	.L2910
	call	_ZdaPv@PLT
.L2910:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeGenerator24GetProtectedInstructionsEv@PLT
	movq	-1888(%rbp), %r13
	movq	-1744(%rbp), %rax
	movq	$0, -1744(%rbp)
	movq	112(%r13), %rdi
	movq	%rax, 112(%r13)
	testq	%rdi, %rdi
	je	.L3059
	call	_ZdaPv@PLT
	movq	-1744(%rbp), %rdi
	movq	-1736(%rbp), %rax
	movq	%rax, 120(%r13)
	testq	%rdi, %rdi
	je	.L2912
	call	_ZdaPv@PLT
.L2912:
	movq	-1888(%rbp), %rax
	movb	$3, 133(%rax)
	movq	-1016(%rbp), %rsi
	movl	(%rsi), %eax
	testb	$64, %ah
	jne	.L3060
.L2913:
	testb	$-64, %ah
	je	.L2918
	movq	-1032(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3061
	call	_ZN2v88internal4wasm10WasmEngine13GetCodeTracerEv@PLT
	movq	%rax, %rbx
.L2920:
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	movq	144(%rbx), %rsi
	je	.L2921
	testq	%rsi, %rsi
	je	.L3062
.L2922:
	addl	$1, 152(%rbx)
.L2921:
	leaq	-576(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movq	%r13, %rdi
	movl	$52, %edx
	leaq	.LC106(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$26, %edx
	movq	%r13, %rdi
	leaq	.LC107(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1016(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movq	-1744(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movl	$15, %edx
	leaq	.LC108(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r14
	testq	%r14, %r14
	je	.L2923
	cmpb	$0, 56(%r14)
	je	.L2924
	movsbl	67(%r14), %esi
.L2925:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	movq	-1744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2926
	call	_ZdaPv@PLT
.L2926:
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, -496(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-512(%rbp), %rdi
	movq	%rax, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -576(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-496(%rbp), %rdi
	movq	%rax, -576(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -496(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, _ZN2v88internal25FLAG_redirect_code_tracesE(%rip)
	je	.L2918
	subl	$1, 152(%rbx)
	je	.L3063
.L2918:
	movq	-1888(%rbp), %rax
	movq	-2096(%rbp), %rdi
	movq	%r12, %rsi
	movq	$0, -1888(%rbp)
	movq	%rax, -1744(%rbp)
	call	_ZN2v88internal24OptimizedCompilationInfo24SetWasmCompilationResultESt10unique_ptrINS0_4wasm21WasmCompilationResultESt14default_deleteIS4_EE@PLT
	movq	-1744(%rbp), %r12
	testq	%r12, %r12
	je	.L2930
	movq	112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2931
	call	_ZdaPv@PLT
.L2931:
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2932
	call	_ZdaPv@PLT
.L2932:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2933
	call	_ZdaPv@PLT
.L2933:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2930:
	movq	-1888(%rbp), %r12
	testq	%r12, %r12
	je	.L3044
	movq	112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2935
	call	_ZdaPv@PLT
.L2935:
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2936
	call	_ZdaPv@PLT
.L2936:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2937
	call	_ZdaPv@PLT
.L2937:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3044:
	movq	-2088(%rbp), %rdi
	call	_ZN2v88internal8compiler12PipelineDataD1Ev
	movq	-2080(%rbp), %r12
	testq	%r12, %r12
	je	.L2903
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm21WasmInstructionBufferD1Ev@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2903:
	movq	-2112(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2905
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18PipelineStatisticsD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L2905:
	movq	-2104(%rbp), %rdi
	call	_ZN2v88internal8compiler9ZoneStatsD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3064
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3046:
	.cfi_restore_state
	movq	-2096(%rbp), %rbx
	leaq	-576(%rbp), %r13
	movl	$32, %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode@PLT
	leaq	-1984(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZNK2v88internal24OptimizedCompilationInfo12GetDebugNameEv@PLT
	movl	$13, %edx
	leaq	.LC166(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1984(%rbp), %r12
	testq	%r12, %r12
	je	.L3065
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2856:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movl	$13, %edx
	movq	%r13, %rdi
	movq	.LC157(%rip), %xmm1
	movq	%rax, -2192(%rbp)
	leaq	-928(%rbp), %r15
	leaq	.LC192(%rip), %rsi
	leaq	-1040(%rbp), %rbx
	leaq	-1744(%rbp), %r12
	movhps	-2192(%rbp), %xmm1
	movaps	%xmm1, -2208(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	16+_ZTVN2v88internal19AccountingAllocatorE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	%rax, -1936(%rbp)
	movq	%r15, -2248(%rbp)
	movups	%xmm0, -1928(%rbp)
	movq	%rbx, -2088(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%r9d, %r9d
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%r9w, -704(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -928(%rbp)
	movq	%rax, -2216(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -696(%rbp)
	movq	%rax, -1040(%rbp)
	movups	%xmm0, -680(%rbp)
	movq	$0, -712(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1040(%rbp,%rax)
	movq	-1040(%rbp), %rax
	movq	-24(%rax), %rcx
	addq	%rbx, %rcx
	movq	%rcx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movdqa	-2208(%rbp), %xmm1
	movq	%rax, -928(%rbp)
	leaq	-976(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -1040(%rbp)
	movaps	%xmm0, -1024(%rbp)
	movaps	%xmm0, -1008(%rbp)
	movaps	%xmm0, -992(%rbp)
	movq	%rax, -2224(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rdi
	leaq	-1032(%rbp), %rsi
	movq	%rax, -2208(%rbp)
	leaq	-1936(%rbp), %r15
	movq	%rax, -1032(%rbp)
	leaq	-944(%rbp), %rax
	movq	%rax, -2240(%rbp)
	movq	%rax, -960(%rbp)
	movl	$16, -968(%rbp)
	movq	$0, -952(%rbp)
	movb	$0, -944(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	%rbx, %r8
	movq	-2160(%rbp), %rdx
	leaq	-1888(%rbp), %r9
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, -1872(%rbp)
	movaps	%xmm0, -1888(%rbp)
	call	_ZN2v88internal4wasm16PrintRawWasmCodeEPNS0_19AccountingAllocatorERKNS1_12FunctionBodyEPKNS1_10WasmModuleENS1_11PrintLocalsERSoPSt6vectorIiSaIiEE@PLT
	leaq	-1424(%rbp), %rax
	movq	$0, -1432(%rbp)
	leaq	-1440(%rbp), %rdi
	movq	%rax, -2232(%rbp)
	movq	%rax, -1440(%rbp)
	movq	-992(%rbp), %rax
	movb	$0, -1424(%rbp)
	testq	%rax, %rax
	je	.L2857
	movq	-1008(%rbp), %r8
	movq	-1000(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L3066
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L2859:
	movq	-1440(%rbp), %rdx
	movq	-1432(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L2860
	leaq	-2032(%rbp), %rcx
	movq	%r14, -2256(%rbp)
	movq	%rdx, %r14
	movq	%r12, -2264(%rbp)
	movq	%rcx, %rbx
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L2861:
	movsbw	(%r14), %ax
	movq	%rbx, %rsi
	movq	%r13, %rdi
	addq	$1, %r14
	movw	%ax, -2032(%rbp)
	call	_ZN2v88internallsERSoRKNS0_20AsEscapedUC16ForJSONE@PLT
	cmpq	%r14, %r12
	jne	.L2861
	movq	-1440(%rbp), %rax
	movq	-2256(%rbp), %r14
	movq	-2264(%rbp), %r12
	movq	%rax, %rdi
.L2860:
	movq	-2232(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L2862
	call	_ZdlPv@PLT
.L2862:
	movl	$37, %edx
	leaq	.LC193(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1888(%rbp), %rbx
	movq	-1880(%rbp), %rax
	cmpq	%rax, %rbx
	je	.L2863
	movl	(%rbx), %r8d
	movq	%r14, -2232(%rbp)
	addq	$4, %rbx
	movq	%r12, -2256(%rbp)
	movq	%rax, %r12
	movl	%r8d, %r14d
	jmp	.L2864
	.p2align 4,,10
	.p2align 3
.L3067:
	movl	$2, %edx
	movq	%r13, %rdi
	movl	(%rbx), %r14d
	addq	$4, %rbx
	leaq	.LC32(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2864:
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZNSolsEi@PLT
	cmpq	%rbx, %r12
	jne	.L3067
	movq	-2232(%rbp), %r14
	movq	-2256(%rbp), %r12
.L2863:
	movq	%r13, %rdi
	movl	$13, %edx
	leaq	.LC194(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1888(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2865
	call	_ZdlPv@PLT
.L2865:
	movq	.LC157(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-960(%rbp), %rdi
	movq	%rax, -928(%rbp)
	movhps	-2208(%rbp), %xmm0
	movaps	%xmm0, -1040(%rbp)
	cmpq	-2240(%rbp), %rdi
	je	.L2866
	call	_ZdlPv@PLT
.L2866:
	movq	-2192(%rbp), %rax
	movq	-2224(%rbp), %rdi
	movq	%rax, -1032(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-2248(%rbp), %rdi
	movq	%rax, -1040(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1040(%rbp,%rax)
	movq	-2216(%rbp), %rax
	movq	%rax, -928(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal19AccountingAllocatorD1Ev@PLT
	movq	-1984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2867
	call	_ZdaPv@PLT
.L2867:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileD1Ev@PLT
	jmp	.L2854
	.p2align 4,,10
	.p2align 3
.L3045:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L3068
.L2851:
	movq	%rax, _ZZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsEPNS0_4wasm10WasmEngineENS3_12FunctionBodyEPKNS3_10WasmModuleEPNS0_24OptimizedCompilationInfoEPNS1_9ZoneStatsEE28trace_event_unique_atomic847(%rip)
	jmp	.L2850
	.p2align 4,,10
	.p2align 3
.L3059:
	movq	-1736(%rbp), %rax
	movq	%rax, 120(%r13)
	jmp	.L2912
	.p2align 4,,10
	.p2align 3
.L3058:
	movq	-1736(%rbp), %rax
	movq	%rax, 104(%r13)
	jmp	.L2910
	.p2align 4,,10
	.p2align 3
.L2924:
	movq	%r14, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r14), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2925
	movq	%r14, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2925
	.p2align 4,,10
	.p2align 3
.L3047:
	movq	-1040(%rbp), %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	movq	%rax, %rbx
	jmp	.L2870
	.p2align 4,,10
	.p2align 3
.L3049:
	movq	-1440(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2874
	.p2align 4,,10
	.p2align 3
.L3060:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	-576(%rbp), %r13
	movq	.LC74(%rip), %xmm1
	movl	$1, %edx
	movq	%rax, -2192(%rbp)
	movq	%r13, %rdi
	addq	$1248, %rbx
	leaq	-1312(%rbp), %r14
	leaq	-1416(%rbp), %r15
	movhps	-2192(%rbp), %xmm1
	movaps	%xmm1, -2128(%rbp)
	call	_ZN2v88internal8compiler13TurboJsonFileC1EPNS0_24OptimizedCompilationInfoESt13_Ios_Openmode@PLT
	movl	$42, %edx
	leaq	.LC100(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rbx, -1744(%rbp)
	call	_ZN2v88internal8compilerlsERSoRKNS1_17BlockStartsAsJSONE
	leaq	.LC101(%rip), %rsi
	leaq	-1440(%rbp), %rbx
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -1312(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -2216(%rbp)
	xorl	%eax, %eax
	movw	%ax, -1088(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -1080(%rbp)
	movups	%xmm0, -1064(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1096(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1440(%rbp,%rax)
	movq	-1440(%rbp), %rax
	movq	$0, -1432(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
	leaq	-1424(%rbp), %rbx
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rax, -1424(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1424(%rbp,%rax)
	movq	-1424(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-2128(%rbp), %xmm1
	movq	%rax, -1440(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1440(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -1440(%rbp)
	addq	$80, %rax
	movq	%rax, -1312(%rbp)
	leaq	-1360(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -1424(%rbp)
	movaps	%xmm0, -1408(%rbp)
	movaps	%xmm0, -1392(%rbp)
	movaps	%xmm0, -1376(%rbp)
	movq	%rax, -2128(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -2208(%rbp)
	movq	%rax, -1416(%rbp)
	leaq	-1328(%rbp), %rax
	movq	%rax, -2136(%rbp)
	movq	%rax, -1344(%rbp)
	movl	$24, -1352(%rbp)
	movq	$0, -1336(%rbp)
	movb	$0, -1328(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	xorl	%edi, %edi
	movq	-1888(%rbp), %r9
	movl	$3, %r8d
	movq	(%r9), %rdx
	movslq	16(%r9), %rcx
	pushq	$0
	addq	%rdx, %rcx
	call	_ZN2v88internal12Disassembler6DecodeEPNS0_7IsolateEPSoPhS5_NS0_13CodeReferenceEm@PLT
	leaq	-1472(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	-1472(%rbp), %rdi
	movq	-1464(%rbp), %rbx
	popq	%rdx
	popq	%rcx
	addq	%rdi, %rbx
	cmpq	%rdi, %rbx
	je	.L2914
	movq	%rdi, %r15
	.p2align 4,,10
	.p2align 3
.L2915:
	movsbw	(%r15), %ax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addq	$1, %r15
	movw	%ax, -1744(%rbp)
	call	_ZN2v88internallsERSoRKNS0_20AsEscapedUC16ForJSONE@PLT
	cmpq	%r15, %rbx
	jne	.L2915
	movq	-1472(%rbp), %rdi
.L2914:
	leaq	-1456(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2916
	call	_ZdlPv@PLT
.L2916:
	movq	.LC74(%rip), %xmm0
	leaq	.LC170(%rip), %rsi
	movq	%r13, %rdi
	movhps	-2208(%rbp), %xmm0
	movaps	%xmm0, -2160(%rbp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r13, %rdi
	leaq	.LC105(%rip), %rsi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-2160(%rbp), %xmm0
	movq	-1344(%rbp), %rdi
	movq	%rax, -1440(%rbp)
	addq	$80, %rax
	movq	%rax, -1312(%rbp)
	movaps	%xmm0, -1424(%rbp)
	cmpq	-2136(%rbp), %rdi
	je	.L2917
	call	_ZdlPv@PLT
.L2917:
	movq	-2192(%rbp), %rax
	movq	-2128(%rbp), %rdi
	movq	%rax, -1416(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, %rdi
	movq	%rax, -1440(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1440(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -1424(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1424(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -1440(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1440(%rbp,%rax)
	movq	-2216(%rbp), %rax
	movq	$0, -1432(%rbp)
	movq	%rax, -1312(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13TurboJsonFileD1Ev@PLT
	movq	-1016(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L2913
	.p2align 4,,10
	.p2align 3
.L3050:
	movq	144(%rbx), %rdi
	call	fclose@PLT
	movq	$0, 144(%rbx)
	jmp	.L2868
	.p2align 4,,10
	.p2align 3
.L3066:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L2859
	.p2align 4,,10
	.p2align 3
.L3053:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%rbx)
	movq	%rax, %r8
	movq	-1912(%rbp), %rsi
	movq	-1016(%rbp), %rax
	movq	-880(%rbp), %rdx
	leaq	160(%rax), %rcx
	testq	%rsi, %rsi
	je	.L3069
.L2899:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE@PLT
	movq	-880(%rbp), %rax
	movq	-1912(%rbp), %rsi
	movq	(%rax), %rdx
	testq	%rsi, %rsi
	je	.L3070
.L2900:
	leaq	-1888(%rbp), %r14
	leaq	-1936(%rbp), %r15
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21ValueNumberingReducerC1EPNS0_4ZoneES4_@PLT
	movq	-2088(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21ValueNumberingReducerD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducerD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_116PipelineRunScopeD1Ev
	jmp	.L2895
	.p2align 4,,10
	.p2align 3
.L3061:
	movq	-1040(%rbp), %rdi
	call	_ZN2v88internal7Isolate13GetCodeTracerEv@PLT
	movq	%rax, %rbx
	jmp	.L2920
	.p2align 4,,10
	.p2align 3
.L3057:
	movq	-2024(%rbp), %rsi
	movq	-2016(%rbp), %rdi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%rax, -2008(%rbp)
	jmp	.L2894
	.p2align 4,,10
	.p2align 3
.L3056:
	movq	-2024(%rbp), %rsi
	movq	-2016(%rbp), %rdi
	movq	%rdx, -2160(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-2160(%rbp), %rdx
	movq	%rax, -2008(%rbp)
	movq	%rax, %rsi
	jmp	.L2893
	.p2align 4,,10
	.p2align 3
.L3055:
	movq	-2024(%rbp), %rsi
	movq	-2016(%rbp), %rdi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%rax, -2008(%rbp)
	movq	%rax, %r8
	jmp	.L2892
	.p2align 4,,10
	.p2align 3
.L3063:
	movq	144(%rbx), %rdi
	call	fclose@PLT
	movq	$0, 144(%rbx)
	jmp	.L2918
	.p2align 4,,10
	.p2align 3
.L3065:
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2856
	.p2align 4,,10
	.p2align 3
.L3048:
	movq	(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %rsi
	jmp	.L2872
	.p2align 4,,10
	.p2align 3
.L3068:
	leaq	.LC190(%rip), %rsi
	call	*%rdx
	jmp	.L2851
	.p2align 4,,10
	.p2align 3
.L2857:
	leaq	-960(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L2859
	.p2align 4,,10
	.p2align 3
.L3062:
	movq	(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v84base2OS5FOpenEPKcS3_@PLT
	movq	%rax, 144(%rbx)
	movq	%rax, %rsi
	jmp	.L2922
	.p2align 4,,10
	.p2align 3
.L3070:
	movq	-1928(%rbp), %rsi
	movq	-1920(%rbp), %rdi
	movq	%rdx, -2128(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-2128(%rbp), %rdx
	movq	%rax, -1912(%rbp)
	movq	%rax, %rsi
	jmp	.L2900
.L2923:
	call	_ZSt16__throw_bad_castv@PLT
.L3064:
	call	__stack_chk_fail@PLT
.L3069:
	movq	-1920(%rbp), %rdi
	movq	-1928(%rbp), %rsi
	jmp	.L2941
.L3054:
	movq	-2024(%rbp), %rsi
	movq	-2016(%rbp), %rdi
	jmp	.L2940
	.cfi_endproc
.LFE26823:
	.size	_ZN2v88internal8compiler8Pipeline27GenerateCodeForWasmFunctionEPNS0_24OptimizedCompilationInfoEPNS0_4wasm10WasmEngineEPNS1_12MachineGraphEPNS1_14CallDescriptorEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS5_12FunctionBodyEPKNS5_10WasmModuleEi, .-_ZN2v88internal8compiler8Pipeline27GenerateCodeForWasmFunctionEPNS0_24OptimizedCompilationInfoEPNS0_4wasm10WasmEngineEPNS1_12MachineGraphEPNS1_14CallDescriptorEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS5_12FunctionBodyEPKNS5_10WasmModuleEi
	.section	.text._ZN2v88internal8compiler21LateOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal8compiler21LateOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler21LateOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler21LateOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE, @function
_ZN2v88internal8compiler21LateOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE:
.LFB26633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$664, %rsp
	movq	216(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	352(%r13), %r8
	testq	%r8, %r8
	je	.L3075
.L3072:
	movq	24(%r12), %rax
	leaq	-320(%rbp), %r13
	movq	%rbx, %rsi
	movq	160(%r12), %rdx
	movq	%r13, %rdi
	leaq	-448(%rbp), %r14
	leaq	-560(%rbp), %r15
	leaq	160(%rax), %rcx
	call	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE@PLT
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	216(%r12), %rdx
	movl	$1, %r8d
	call	_ZN2v88internal8compiler17BranchEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneENS2_5PhaseE@PLT
	leaq	-608(%rbp), %rax
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	200(%r12), %rcx
	movq	160(%r12), %rdx
	movq	%rax, %rdi
	movq	%rax, -696(%rbp)
	call	_ZN2v88internal8compiler19DeadCodeEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE@PLT
	movq	160(%r12), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	(%rdx), %rdx
	call	_ZN2v88internal8compiler21ValueNumberingReducerC1EPNS0_4ZoneES4_@PLT
	leaq	-640(%rbp), %r10
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	216(%r12), %rdx
	movq	%r10, %rdi
	movq	%r10, -680(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	192(%r12), %r9
	pushq	%rbx
	movq	200(%r12), %r8
	leaq	-512(%rbp), %r11
	leaq	-672(%rbp), %rbx
	movq	320(%r12), %rcx
	movq	160(%r12), %rdx
	movq	%r11, %rdi
	movq	%r11, -688(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE@PLT
	movq	216(%r12), %rcx
	movq	%rbx, %rdi
	movq	8(%rcx), %rdx
	movq	(%rcx), %rsi
	call	_ZN2v88internal8compiler14SelectLoweringC1EPNS1_5GraphEPNS1_21CommonOperatorBuilderE@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-696(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-680(%rbp), %r10
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-688(%rbp), %r11
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler14SelectLoweringD1Ev@PLT
	movq	-680(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducerD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21ValueNumberingReducerD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler17BranchEliminationD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12GraphReducerD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3076
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3075:
	.cfi_restore_state
	movq	8(%r13), %rdi
	movq	0(%r13), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%r13)
	movq	%rax, %r8
	jmp	.L3072
.L3076:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26633:
	.size	_ZN2v88internal8compiler21LateOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE, .-_ZN2v88internal8compiler21LateOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl13OptimizeGraphEPNS1_7LinkageE.str1.1,"aMS",@progbits,1
.LC198:
	.string	"V8.TFLowering"
.LC199:
	.string	"V8.TFTyper"
.LC200:
	.string	"V8.TFTypedLowering"
.LC201:
	.string	"V8.TFLoopPeeling"
.LC202:
	.string	"V8.TFLoopExitElimination"
.LC203:
	.string	"V8.TFLoadElimination"
.LC204:
	.string	"V8.TFEscapeAnalysis"
.LC205:
	.string	"V8.TFTypeAssertions"
.LC206:
	.string	"V8.TFSimplifiedLowering"
.LC207:
	.string	"V8.TFGenericLowering"
.LC208:
	.string	"V8.TFBlockBuilding"
.LC209:
	.string	"V8.TFEarlyOptimization"
.LC210:
	.string	"V8.TFEffectLinearization"
.LC211:
	.string	"effect linearization schedule"
.LC212:
	.string	"V8.TFStoreStoreElimination"
.LC213:
	.string	"V8.TFControlFlowOptimization"
.LC214:
	.string	"V8.TFLateOptimization"
	.section	.rodata._ZN2v88internal8compiler12PipelineImpl13OptimizeGraphEPNS1_7LinkageE.str1.8,"aMS",@progbits,1
	.align 8
.LC215:
	.string	"V8.TFMachineOperatorOptimization"
	.section	.text._ZN2v88internal8compiler12PipelineImpl13OptimizeGraphEPNS1_7LinkageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler12PipelineImpl13OptimizeGraphEPNS1_7LinkageE
	.type	_ZN2v88internal8compiler12PipelineImpl13OptimizeGraphEPNS1_7LinkageE, @function
_ZN2v88internal8compiler12PipelineImpl13OptimizeGraphEPNS1_7LinkageE:
.LFB26721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$600, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -488(%rbp)
	movq	%rsi, -528(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	56(%rax), %rdi
	movq	%rax, -504(%rbp)
	testq	%rdi, %rdi
	je	.L3078
	leaq	.LC198(%rip), %rsi
	call	_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc@PLT
.L3078:
	movq	-504(%rbp), %rax
	movl	$176, %edi
	movq	24(%rax), %rcx
	movq	320(%rax), %r12
	movl	120(%rax), %r13d
	movq	160(%rax), %r15
	leaq	160(%rcx), %rbx
	movq	%rcx, -496(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movl	%r13d, %edx
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler5TyperC1EPNS1_12JSHeapBrokerENS_4base5FlagsINS2_4FlagEiEEPNS1_5GraphEPNS0_11TickCounterE@PLT
	movq	-504(%rbp), %rax
	movq	%r14, 112(%rax)
	movq	-488(%rbp), %rax
	movq	(%rax), %rbx
	movq	56(%rbx), %rax
	movq	%rax, -520(%rbp)
	testq	%rax, %rax
	je	.L3079
	leaq	.LC199(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L3079:
	movq	48(%rbx), %rax
	movq	176(%rbx), %rbx
	movq	%rax, -512(%rbp)
	testq	%rbx, %rbx
	je	.L3080
	movq	48(%rbx), %rax
	movq	%rax, -544(%rbp)
	leaq	.LC199(%rip), %rax
	movq	%rax, 48(%rbx)
.L3080:
	movq	-512(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	leaq	-320(%rbp), %r12
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	movq	%rax, %r13
	movq	-488(%rbp), %rax
	movq	%r13, -400(%rbp)
	movq	$0, -376(%rbp)
	movq	(%rax), %r15
	leaq	-400(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -496(%rbp)
	movq	216(%r15), %rdi
	call	_ZN2v88internal8compiler7JSGraph14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	movq	216(%r15), %rdi
	call	_ZN2v88internal8compiler7JSGraph12TrueConstantEv@PLT
	movq	-496(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -320(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movq	216(%r15), %rdi
	call	_ZN2v88internal8compiler7JSGraph13FalseConstantEv@PLT
	movq	-496(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -320(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movq	216(%r15), %rax
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	200(%r15), %r8
	movq	(%rax), %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler21LoopVariableOptimizerC1EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE@PLT
	cmpb	$0, _ZN2v88internal24FLAG_turbo_loop_variableE(%rip)
	jne	.L3459
.L3081:
	movq	-496(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler5Typer3RunERKNS0_10ZoneVectorIPNS1_4NodeEEEPNS1_21LoopVariableOptimizerE@PLT
	movq	-184(%rbp), %r14
	testq	%r14, %r14
	je	.L3082
	leaq	-208(%rbp), %rdx
	movq	%rbx, -536(%rbp)
	movq	%rdx, %rbx
.L3085:
	movq	24(%r14), %r15
	testq	%r15, %r15
	je	.L3083
.L3084:
	movq	24(%r15), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKiPN2v88internal8compiler17InductionVariableEESt10_Select1stIS7_ESt4lessIiENS3_13ZoneAllocatorIS7_EEE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.L3084
.L3083:
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L3085
	movq	-536(%rbp), %rbx
.L3082:
	testq	%rbx, %rbx
	je	.L3086
	movq	-544(%rbp), %rax
	movq	%rax, 48(%rbx)
.L3086:
	testq	%r13, %r13
	je	.L3087
	movq	-512(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L3087:
	movq	-520(%rbp), %rax
	testq	%rax, %rax
	je	.L3088
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L3088:
	movq	-488(%rbp), %rbx
	xorl	%edx, %edx
	leaq	.LC199(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb
	movq	(%rbx), %rbx
	movq	56(%rbx), %r14
	testq	%r14, %r14
	je	.L3089
	leaq	.LC200(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L3089:
	movq	48(%rbx), %r15
	movq	176(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3090
	leaq	.LC200(%rip), %rax
	movq	48(%rbx), %rcx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, 48(%rbx)
	movq	%rcx, -512(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	movq	-488(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler18TypedLoweringPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	movq	-512(%rbp), %rcx
	movq	%rcx, 48(%rbx)
.L3215:
	testq	%r13, %r13
	je	.L3091
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L3091:
	testq	%r14, %r14
	je	.L3092
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L3092:
	movq	-488(%rbp), %rbx
	xorl	%edx, %edx
	leaq	.LC200(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb
	movq	-504(%rbp), %rax
	movq	24(%rax), %rax
	testb	$-128, (%rax)
	je	.L3093
	movq	(%rbx), %rbx
	movq	56(%rbx), %rax
	movq	%rax, -576(%rbp)
	testq	%rax, %rax
	je	.L3094
	leaq	.LC201(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L3094:
	movq	48(%rbx), %rax
	movq	176(%rbx), %r10
	movq	%rax, -520(%rbp)
	testq	%r10, %r10
	je	.L3095
	movq	48(%r10), %rax
	movq	%rax, -584(%rbp)
	leaq	.LC201(%rip), %rax
	movq	%rax, 48(%r10)
.L3095:
	movq	-520(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r10, -544(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	-488(%rbp), %rcx
	movq	-496(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	movq	%rax, -512(%rbp)
	movq	(%rcx), %rcx
	movq	160(%rcx), %rdx
	movq	%rcx, -536(%rbp)
	call	_ZN2v88internal8compiler12GraphTrimmerC1EPNS0_4ZoneEPNS1_5GraphE@PLT
	movq	-536(%rbp), %rcx
	leaq	-480(%rbp), %rsi
	movq	%rbx, -480(%rbp)
	movq	$0, -472(%rbp)
	movq	216(%rcx), %rdi
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	call	_ZN2v88internal8compiler7JSGraph14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	movq	-464(%rbp), %r11
	movq	-472(%rbp), %r13
	movq	-536(%rbp), %rcx
	movq	-544(%rbp), %r10
	cmpq	%r13, %r11
	je	.L3096
	.p2align 4,,10
	.p2align 3
.L3097:
	movq	0(%r13), %rbx
	movzbl	23(%rbx), %eax
	leaq	32(%rbx), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3099
	movq	32(%rbx), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
.L3099:
	testl	%eax, %eax
	jle	.L3100
	cmpq	$0, (%rdx)
	je	.L3101
.L3100:
	movl	-392(%rbp), %eax
	cmpl	%eax, 16(%rbx)
	ja	.L3101
	addl	$1, %eax
	movl	%eax, 16(%rbx)
	movq	-368(%rbp), %r14
	cmpq	-360(%rbp), %r14
	je	.L3102
	movq	%rbx, (%r14)
	addq	$8, -368(%rbp)
.L3101:
	addq	$8, %r13
	cmpq	%r13, %r11
	jne	.L3097
.L3096:
	movq	-496(%rbp), %rbx
	movq	%r10, -544(%rbp)
	movq	%rcx, -536(%rbp)
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12GraphTrimmer9TrimGraphEv@PLT
	movq	-536(%rbp), %rcx
	movq	-512(%rbp), %rdx
	movq	24(%rcx), %rax
	leaq	160(%rax), %rsi
	movq	216(%rcx), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler10LoopFinder13BuildLoopTreeEPNS1_5GraphEPNS0_11TickCounterEPNS0_4ZoneE@PLT
	movq	-536(%rbp), %rcx
	movq	%r12, %rdi
	movdqu	160(%rcx), %xmm0
	movdqu	192(%rcx), %xmm2
	movdqu	168(%rcx), %xmm1
	shufpd	$2, %xmm2, %xmm0
	movaps	%xmm0, -320(%rbp)
	movq	%rax, %xmm0
	movhps	-512(%rbp), %xmm0
	movaps	%xmm1, -288(%rbp)
	movaps	%xmm0, -304(%rbp)
	call	_ZN2v88internal8compiler10LoopPeeler20PeelInnerLoopsOfTreeEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12GraphTrimmerD1Ev@PLT
	movq	-544(%rbp), %r10
	testq	%r10, %r10
	je	.L3113
	movq	-584(%rbp), %rax
	movq	%rax, 48(%r10)
.L3113:
	movq	-512(%rbp), %rax
	testq	%rax, %rax
	je	.L3114
	movq	-520(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L3114:
	movq	-576(%rbp), %rax
	testq	%rax, %rax
	je	.L3115
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L3115:
	movq	-488(%rbp), %rdi
	leaq	.LC201(%rip), %rsi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	cmpb	$0, _ZN2v88internal27FLAG_turbo_load_eliminationE(%rip)
	jne	.L3460
.L3121:
	movq	-504(%rbp), %rax
	movq	112(%rax), %r13
	testq	%r13, %r13
	je	.L3126
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5TyperD1Ev@PLT
	movl	$176, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3126:
	movq	-504(%rbp), %rax
	cmpb	$0, _ZN2v88internal17FLAG_turbo_escapeE(%rip)
	movq	$0, 112(%rax)
	jne	.L3461
.L3127:
	movq	-488(%rbp), %rax
	cmpb	$0, _ZN2v88internal17FLAG_assert_typesE(%rip)
	movq	(%rax), %rbx
	movq	56(%rbx), %r13
	jne	.L3462
.L3134:
	testq	%r13, %r13
	je	.L3141
	leaq	.LC206(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L3141:
	movq	48(%rbx), %r14
	movq	176(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3142
	leaq	.LC206(%rip), %rax
	movq	48(%rbx), %r10
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, 48(%rbx)
	movq	%r10, -512(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	-488(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rcx
	movq	168(%rax), %r8
	movq	176(%rax), %r9
	movq	320(%rax), %rdx
	movq	216(%rax), %rsi
	leaq	160(%rcx), %rax
	pushq	%rax
	movl	4(%rcx), %eax
	movq	%r15, %rcx
	pushq	%rax
	call	_ZN2v88internal8compiler18SimplifiedLoweringC1EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering13LowerAllNodesEv@PLT
	movq	-512(%rbp), %r10
	movq	%r10, 48(%rbx)
	popq	%rdi
	popq	%r8
.L3211:
	testq	%r15, %r15
	je	.L3143
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L3143:
	testq	%r13, %r13
	je	.L3144
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L3144:
	movq	-488(%rbp), %rbx
	leaq	.LC206(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	(%rbx), %rbx
	movq	56(%rbx), %r14
	testq	%r14, %r14
	je	.L3145
	leaq	.LC207(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L3145:
	movq	48(%rbx), %rax
	movq	176(%rbx), %r15
	movq	%rax, -512(%rbp)
	testq	%r15, %r15
	je	.L3146
	movq	48(%r15), %rax
	movq	%rax, -552(%rbp)
	leaq	.LC207(%rip), %rax
	movq	%rax, 48(%r15)
.L3146:
	movq	-512(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%rax, %rbx
	movq	-488(%rbp), %rax
	movq	(%rax), %r13
	movq	216(%r13), %r10
	movq	352(%r10), %r8
	testq	%r8, %r8
	je	.L3463
.L3147:
	movq	24(%r13), %rax
	movq	160(%r13), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	160(%rax), %rcx
	call	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE@PLT
	movq	320(%r13), %rcx
	movq	216(%r13), %rsi
	movq	%r12, %rdx
	movq	-496(%rbp), %rdi
	call	_ZN2v88internal8compiler17JSGenericLoweringC1EPNS1_7JSGraphEPNS1_15AdvancedReducer6EditorEPNS1_12JSHeapBrokerE@PLT
	movq	-496(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv@PLT
	movq	-496(%rbp), %rdi
	call	_ZN2v88internal8compiler17JSGenericLoweringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducerD1Ev@PLT
	testq	%r15, %r15
	je	.L3148
	movq	-552(%rbp), %rax
	movq	%rax, 48(%r15)
.L3148:
	testq	%rbx, %rbx
	je	.L3149
	movq	-512(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L3149:
	testq	%r14, %r14
	je	.L3150
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L3150:
	movq	-488(%rbp), %rdi
	leaq	.LC207(%rip), %rsi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	-504(%rbp), %rax
	movq	56(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3151
	leaq	.LC208(%rip), %rsi
	call	_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc@PLT
.L3151:
	movq	-488(%rbp), %rax
	movq	(%rax), %rbx
	movq	56(%rbx), %r14
	testq	%r14, %r14
	je	.L3152
	leaq	.LC209(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L3152:
	movq	48(%rbx), %r15
	movq	176(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3153
	leaq	.LC209(%rip), %rax
	movq	48(%rbx), %rcx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, 48(%rbx)
	movq	%rcx, -512(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	movq	-488(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler22EarlyOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	movq	-512(%rbp), %rcx
	movq	%rcx, 48(%rbx)
.L3210:
	testq	%r13, %r13
	je	.L3154
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L3154:
	testq	%r14, %r14
	je	.L3155
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L3155:
	movq	-488(%rbp), %rbx
	leaq	.LC209(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	(%rbx), %rbx
	movq	56(%rbx), %rax
	movq	%rax, -536(%rbp)
	testq	%rax, %rax
	je	.L3156
	leaq	.LC210(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L3156:
	movq	48(%rbx), %rax
	movq	%rax, -520(%rbp)
	movq	176(%rbx), %rax
	movq	%rax, -544(%rbp)
	testq	%rax, %rax
	je	.L3157
	movq	48(%rax), %rcx
	movq	%rcx, -560(%rbp)
	leaq	.LC210(%rip), %rcx
	movq	%rcx, 48(%rax)
.L3157:
	movq	-520(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	-488(%rbp), %rax
	movq	%r14, %rsi
	movq	(%rax), %r13
	movq	160(%r13), %rdx
	call	_ZN2v88internal8compiler12GraphTrimmerC1EPNS0_4ZoneEPNS1_5GraphE@PLT
	leaq	-448(%rbp), %rax
	movq	%r14, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	%rax, %rsi
	movq	$0, -432(%rbp)
	movq	$0, -424(%rbp)
	movq	216(%r13), %rdi
	movq	%rax, -512(%rbp)
	call	_ZN2v88internal8compiler7JSGraph14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	movq	-432(%rbp), %r11
	movq	-440(%rbp), %r10
	cmpq	%r10, %r11
	je	.L3158
	.p2align 4,,10
	.p2align 3
.L3159:
	movq	(%r10), %r15
	movzbl	23(%r15), %eax
	leaq	32(%r15), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L3161
	movq	32(%r15), %rdx
	movl	8(%rdx), %eax
	addq	$16, %rdx
.L3161:
	testl	%eax, %eax
	jle	.L3162
	cmpq	$0, (%rdx)
	je	.L3163
.L3162:
	movl	-312(%rbp), %eax
	cmpl	%eax, 16(%r15)
	ja	.L3163
	addl	$1, %eax
	movl	%eax, 16(%r15)
	movq	-288(%rbp), %rbx
	cmpq	-280(%rbp), %rbx
	je	.L3164
	movq	%r15, (%rbx)
	addq	$8, -288(%rbp)
.L3163:
	addq	$8, %r10
	cmpq	%r10, %r11
	jne	.L3159
.L3158:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphTrimmer9TrimGraphEv@PLT
	movq	24(%r13), %rax
	movl	$4, %edx
	movq	%r14, %rdi
	movq	160(%r13), %rsi
	leaq	160(%rax), %rcx
	call	_ZN2v88internal8compiler9Scheduler15ComputeScheduleEPNS0_4ZoneEPNS1_5GraphENS_4base5FlagsINS2_4FlagEiEEPNS0_11TickCounterE@PLT
	cmpb	$0, _ZN2v88internal17FLAG_turbo_verifyE(%rip)
	movq	%rax, %r15
	jne	.L3464
.L3175:
	movq	24(%r13), %rdi
	leaq	.LC211(%rip), %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_113TraceScheduleEPNS0_24OptimizedCompilationInfoEPNS1_12PipelineDataEPNS1_8ScheduleEPKc
	movq	24(%r13), %rax
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	176(%r13), %r8
	movq	168(%r13), %rcx
	cmpl	$1, 4(%rax)
	movq	216(%r13), %rdi
	setne	%r9b
	movzbl	%r9b, %r9d
	call	_ZN2v88internal8compiler22LinearizeEffectControlEPNS1_7JSGraphEPNS1_8ScheduleEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS1_20MaskArrayIndexEnableE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphTrimmerD1Ev@PLT
	movq	216(%r13), %rbx
	movq	352(%rbx), %r8
	testq	%r8, %r8
	je	.L3465
.L3176:
	movq	24(%r13), %rax
	movq	160(%r13), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	160(%rax), %rcx
	call	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE@PLT
	movq	-512(%rbp), %r15
	movq	%r14, %r8
	movq	%r12, %rsi
	movq	200(%r13), %rcx
	movq	160(%r13), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19DeadCodeEliminationC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS0_4ZoneE@PLT
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	320(%r13), %rcx
	movq	192(%r13), %r9
	movq	200(%r13), %r8
	movq	160(%r13), %rdx
	movq	-496(%rbp), %rbx
	pushq	%r14
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_5GraphEPNS1_12JSHeapBrokerEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_4ZoneE@PLT
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducerD1Ev@PLT
	movq	-544(%rbp), %rax
	popq	%rcx
	popq	%rsi
	testq	%rax, %rax
	je	.L3177
	movq	-560(%rbp), %rcx
	movq	%rcx, 48(%rax)
.L3177:
	testq	%r14, %r14
	je	.L3178
	movq	-520(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L3178:
	movq	-536(%rbp), %rax
	testq	%rax, %rax
	je	.L3179
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L3179:
	movq	-488(%rbp), %rdi
	leaq	.LC210(%rip), %rsi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	cmpb	$0, _ZN2v88internal28FLAG_turbo_store_eliminationE(%rip)
	jne	.L3466
.L3180:
	movq	-488(%rbp), %rax
	cmpb	$0, _ZN2v88internal26FLAG_turbo_cf_optimizationE(%rip)
	movq	(%rax), %rbx
	movq	56(%rbx), %r13
	jne	.L3467
.L3186:
	testq	%r13, %r13
	je	.L3198
	leaq	.LC214(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L3198:
	movq	48(%rbx), %r15
	movq	176(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3199
	leaq	.LC214(%rip), %rax
	movq	48(%rbx), %rcx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, 48(%rbx)
	movq	%rcx, -520(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r14
	movq	-488(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler21LateOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	movq	-520(%rbp), %rcx
	movq	%rcx, 48(%rbx)
.L3209:
	testq	%r14, %r14
	je	.L3200
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L3200:
	testq	%r13, %r13
	je	.L3201
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L3201:
	movq	-488(%rbp), %rbx
	leaq	.LC214(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl3RunINS1_23MemoryOptimizationPhaseEJEEEvDpOT0_.isra.0
	movq	%rbx, %rdi
	leaq	.LC54(%rip), %rsi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	(%rbx), %rbx
	movq	56(%rbx), %r14
	testq	%r14, %r14
	je	.L3202
	leaq	.LC215(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L3202:
	movq	48(%rbx), %rax
	movq	176(%rbx), %r15
	movq	%rax, -520(%rbp)
	testq	%r15, %r15
	je	.L3203
	movq	48(%r15), %rax
	movq	%rax, -568(%rbp)
	leaq	.LC215(%rip), %rax
	movq	%rax, 48(%r15)
.L3203:
	movq	-520(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%rax, %rbx
	movq	-488(%rbp), %rax
	movq	(%rax), %r13
	movq	216(%r13), %r10
	movq	352(%r10), %r8
	testq	%r8, %r8
	je	.L3468
.L3204:
	movq	24(%r13), %rax
	movq	160(%r13), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	160(%rax), %rcx
	call	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE@PLT
	movq	160(%r13), %rax
	movq	-496(%rbp), %rdi
	movq	%rbx, %rsi
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler21ValueNumberingReducerC1EPNS0_4ZoneES4_@PLT
	movq	216(%r13), %rdx
	movl	$1, %ecx
	movq	%r12, %rsi
	movq	-512(%rbp), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_12MachineGraphEb@PLT
	movq	-512(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	-496(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv@PLT
	movq	-512(%rbp), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorReducerD1Ev@PLT
	movq	-496(%rbp), %rdi
	call	_ZN2v88internal8compiler21ValueNumberingReducerD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducerD1Ev@PLT
	testq	%r15, %r15
	je	.L3205
	movq	-568(%rbp), %rax
	movq	%rax, 48(%r15)
.L3205:
	testq	%rbx, %rbx
	je	.L3206
	movq	-520(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L3206:
	testq	%r14, %r14
	je	.L3207
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L3207:
	movq	-488(%rbp), %rdi
	leaq	.LC215(%rip), %rsi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	-504(%rbp), %rbx
	movq	168(%rbx), %rdi
	call	_ZN2v88internal8compiler19SourcePositionTable15RemoveDecoratorEv@PLT
	movq	24(%rbx), %rax
	movl	(%rax), %eax
	testb	$64, %ah
	jne	.L3469
.L3208:
	movq	-488(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl21ComputeScheduledGraphEv
	movq	-528(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl18SelectInstructionsEPNS1_7LinkageE
.L3077:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3470
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3093:
	.cfi_restore_state
	movq	-488(%rbp), %rax
	movq	(%rax), %rbx
	movq	56(%rbx), %r14
	testq	%r14, %r14
	je	.L3117
	leaq	.LC202(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L3117:
	movq	48(%rbx), %r15
	movq	176(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3118
	leaq	.LC202(%rip), %rax
	movq	48(%rbx), %rdx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, 48(%rbx)
	movq	%rdx, -512(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%rax, %r13
	movq	-488(%rbp), %rax
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	160(%rax), %rdi
	call	_ZN2v88internal8compiler10LoopPeeler18EliminateLoopExitsEPNS1_5GraphEPNS0_4ZoneE@PLT
	movq	-512(%rbp), %rdx
	movq	%rdx, 48(%rbx)
.L3214:
	testq	%r13, %r13
	je	.L3119
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L3119:
	testq	%r14, %r14
	je	.L3120
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L3120:
	movq	-488(%rbp), %rdi
	leaq	.LC202(%rip), %rsi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	cmpb	$0, _ZN2v88internal27FLAG_turbo_load_eliminationE(%rip)
	je	.L3121
.L3460:
	movq	-488(%rbp), %rax
	movq	(%rax), %rbx
	movq	56(%rbx), %r14
	testq	%r14, %r14
	je	.L3122
	leaq	.LC203(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L3122:
	movq	48(%rbx), %r15
	movq	176(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3123
	leaq	.LC203(%rip), %rax
	movq	48(%rbx), %rcx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, 48(%rbx)
	movq	%rcx, -512(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	movq	-488(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler20LoadEliminationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	movq	-512(%rbp), %rcx
	movq	%rcx, 48(%rbx)
.L3213:
	testq	%r13, %r13
	je	.L3124
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L3124:
	testq	%r14, %r14
	je	.L3125
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L3125:
	movq	-488(%rbp), %rdi
	xorl	%edx, %edx
	leaq	.LC203(%rip), %rsi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb
	jmp	.L3121
	.p2align 4,,10
	.p2align 3
.L3164:
	movq	-296(%rbp), %rdx
	movq	%rbx, %r8
	subq	%rdx, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L3165
	testq	%rax, %rax
	je	.L3222
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L3471
	movl	$2147483640, %esi
	movl	$2147483640, %ecx
.L3166:
	movq	-304(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r9
	subq	%rax, %r9
	cmpq	%rsi, %r9
	jb	.L3472
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3169:
	addq	%rax, %rcx
	leaq	8(%rax), %rsi
.L3167:
	movq	%r15, (%rax,%r8)
	cmpq	%rdx, %rbx
	je	.L3170
	subq	$8, %rbx
	leaq	15(%rax), %rsi
	subq	%rdx, %rbx
	subq	%rdx, %rsi
	movq	%rbx, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rsi
	jbe	.L3225
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rdi
	je	.L3225
	addq	$1, %rdi
	xorl	%esi, %esi
	movq	%rdi, %r8
	shrq	%r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L3172:
	movdqu	(%rdx,%rsi), %xmm4
	movups	%xmm4, (%rax,%rsi)
	addq	$16, %rsi
	cmpq	%rsi, %r8
	jne	.L3172
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rsi
	addq	%rsi, %rdx
	addq	%rax, %rsi
	cmpq	%rdi, %r8
	je	.L3174
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
.L3174:
	leaq	16(%rax,%rbx), %rsi
.L3170:
	movq	%rax, %xmm0
	movq	%rsi, %xmm5
	movq	%rcx, -280(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, -296(%rbp)
	jmp	.L3163
	.p2align 4,,10
	.p2align 3
.L3471:
	testq	%rcx, %rcx
	jne	.L3473
	movl	$8, %esi
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	jmp	.L3167
	.p2align 4,,10
	.p2align 3
.L3102:
	movq	-376(%rbp), %r15
	movq	%r14, %r8
	subq	%r15, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L3165
	testq	%rax, %rax
	je	.L3217
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L3474
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L3104:
	movq	-384(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r9
	subq	%rax, %r9
	cmpq	%rsi, %r9
	jb	.L3475
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L3107:
	addq	%rax, %rdx
	leaq	8(%rax), %rsi
	jmp	.L3105
	.p2align 4,,10
	.p2align 3
.L3474:
	testq	%rdx, %rdx
	jne	.L3476
	movl	$8, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
.L3105:
	movq	%rbx, (%rax,%r8)
	cmpq	%r15, %r14
	je	.L3108
	subq	$8, %r14
	leaq	15(%rax), %rsi
	subq	%r15, %r14
	subq	%r15, %rsi
	movq	%r14, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rsi
	jbe	.L3220
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rdi
	je	.L3220
	addq	$1, %rdi
	xorl	%esi, %esi
	movq	%rdi, %r8
	shrq	%r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L3110:
	movdqu	(%r15,%rsi), %xmm3
	movups	%xmm3, (%rax,%rsi)
	addq	$16, %rsi
	cmpq	%r8, %rsi
	jne	.L3110
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rsi
	addq	%rsi, %r15
	addq	%rax, %rsi
	cmpq	%rdi, %r8
	je	.L3112
	movq	(%r15), %rdi
	movq	%rdi, (%rsi)
.L3112:
	leaq	16(%rax,%r14), %rsi
.L3108:
	movq	%rax, %xmm0
	movq	%rsi, %xmm6
	movq	%rdx, -360(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, -376(%rbp)
	jmp	.L3101
	.p2align 4,,10
	.p2align 3
.L3459:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21LoopVariableOptimizer3RunEv@PLT
	jmp	.L3081
	.p2align 4,,10
	.p2align 3
.L3461:
	movq	-488(%rbp), %rax
	movq	(%rax), %rbx
	movq	56(%rbx), %r14
	testq	%r14, %r14
	je	.L3128
	leaq	.LC204(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L3128:
	movq	48(%rbx), %r15
	movq	176(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3129
	leaq	.LC204(%rip), %rax
	movq	48(%rbx), %rcx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, 48(%rbx)
	movq	%rcx, -512(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r13
	movq	-488(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler19EscapeAnalysisPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	movq	-512(%rbp), %rcx
	movq	%rcx, 48(%rbx)
.L3212:
	testq	%r13, %r13
	je	.L3130
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L3130:
	testq	%r14, %r14
	je	.L3131
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L3131:
	movq	-504(%rbp), %rax
	cmpb	$0, 64(%rax)
	jne	.L3477
	movq	-488(%rbp), %rdi
	xorl	%edx, %edx
	leaq	.LC204(%rip), %rsi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb
	jmp	.L3127
	.p2align 4,,10
	.p2align 3
.L3090:
	movq	%r15, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rdx
	movq	-488(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler18TypedLoweringPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	jmp	.L3215
	.p2align 4,,10
	.p2align 3
.L3462:
	testq	%r13, %r13
	je	.L3135
	leaq	.LC205(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L3135:
	movq	48(%rbx), %rax
	movq	176(%rbx), %rbx
	movq	%rax, -512(%rbp)
	testq	%rbx, %rbx
	je	.L3136
	movq	48(%rbx), %rax
	movq	%rax, -592(%rbp)
	leaq	.LC205(%rip), %rax
	movq	%rax, 48(%rbx)
.L3136:
	movq	-512(%rbp), %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%rax, %r14
	movq	-488(%rbp), %rax
	movq	(%rax), %r15
	movq	216(%r15), %r9
	movq	352(%r9), %r8
	testq	%r8, %r8
	je	.L3478
.L3137:
	movq	24(%r15), %rax
	movq	160(%r15), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	160(%rax), %rcx
	call	_ZN2v88internal8compiler12GraphReducerC1EPNS0_4ZoneEPNS1_5GraphEPNS0_11TickCounterEPNS1_4NodeE@PLT
	movq	216(%r15), %rdx
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	-496(%rbp), %rdi
	call	_ZN2v88internal8compiler24AddTypeAssertionsReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS0_4ZoneE@PLT
	movq	-496(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_110AddReducerEPNS1_12PipelineDataEPNS1_12GraphReducerEPNS1_7ReducerE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducer11ReduceGraphEv@PLT
	movq	-496(%rbp), %rdi
	call	_ZN2v88internal8compiler24AddTypeAssertionsReducerD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphReducerD1Ev@PLT
	testq	%rbx, %rbx
	je	.L3138
	movq	-592(%rbp), %rax
	movq	%rax, 48(%rbx)
.L3138:
	testq	%r14, %r14
	je	.L3139
	movq	-512(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L3139:
	testq	%r13, %r13
	je	.L3140
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L3140:
	movq	-488(%rbp), %rbx
	xorl	%edx, %edx
	leaq	.LC205(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb
	movq	(%rbx), %rbx
	movq	56(%rbx), %r13
	jmp	.L3134
	.p2align 4,,10
	.p2align 3
.L3469:
	movq	-504(%rbp), %rax
	movq	176(%rax), %rdi
	call	_ZN2v88internal8compiler15NodeOriginTable15RemoveDecoratorEv@PLT
	jmp	.L3208
	.p2align 4,,10
	.p2align 3
.L3466:
	movq	-488(%rbp), %rax
	movq	(%rax), %rbx
	movq	56(%rbx), %r14
	testq	%r14, %r14
	je	.L3181
	leaq	.LC212(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L3181:
	movq	48(%rbx), %r8
	movq	176(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3182
	movq	48(%rbx), %rax
	movq	%rax, -600(%rbp)
	leaq	.LC212(%rip), %rax
	movq	%rax, 48(%rbx)
.L3182:
	movq	%r8, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%r8, -520(%rbp)
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	-488(%rbp), %rax
	movq	%r13, %rsi
	movq	(%rax), %r15
	movq	160(%r15), %rdx
	call	_ZN2v88internal8compiler12GraphTrimmerC1EPNS0_4ZoneEPNS1_5GraphE@PLT
	movq	%r13, -400(%rbp)
	movq	-496(%rbp), %rsi
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	movq	$0, -376(%rbp)
	movq	216(%r15), %rdi
	call	_ZN2v88internal8compiler7JSGraph14GetCachedNodesEPNS0_10ZoneVectorIPNS1_4NodeEEE@PLT
	movq	-384(%rbp), %rdx
	movq	-392(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphTrimmer9TrimGraphIN9__gnu_cxx17__normal_iteratorIPPNS1_4NodeESt6vectorIS7_NS0_13ZoneAllocatorIS7_EEEEEEEvT_SE_
	movq	24(%r15), %rax
	movq	216(%r15), %rdi
	movq	%r13, %rdx
	leaq	160(%rax), %rsi
	call	_ZN2v88internal8compiler21StoreStoreElimination3RunEPNS1_7JSGraphEPNS0_11TickCounterEPNS0_4ZoneE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12GraphTrimmerD1Ev@PLT
	testq	%rbx, %rbx
	movq	-520(%rbp), %r8
	je	.L3183
	movq	-600(%rbp), %rax
	movq	%rax, 48(%rbx)
.L3183:
	testq	%r13, %r13
	je	.L3184
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L3184:
	testq	%r14, %r14
	je	.L3185
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L3185:
	movq	-488(%rbp), %rdi
	leaq	.LC212(%rip), %rsi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	jmp	.L3180
	.p2align 4,,10
	.p2align 3
.L3464:
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler16ScheduleVerifier3RunEPNS1_8ScheduleE@PLT
	jmp	.L3175
	.p2align 4,,10
	.p2align 3
.L3467:
	testq	%r13, %r13
	je	.L3187
	leaq	.LC213(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc@PLT
.L3187:
	movq	48(%rbx), %r14
	movq	176(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3188
	movq	48(%rbx), %rax
	movq	%rax, -608(%rbp)
	leaq	.LC213(%rip), %rax
	movq	%rax, 48(%rbx)
.L3188:
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%rax, %r15
	movq	-488(%rbp), %rax
	movq	%r15, %r9
	movq	(%rax), %rax
	movq	24(%rax), %rdi
	movq	200(%rax), %rdx
	movq	192(%rax), %rcx
	movq	160(%rax), %rsi
	leaq	160(%rdi), %r8
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20ControlFlowOptimizerC1EPNS1_5GraphEPNS1_21CommonOperatorBuilderEPNS1_22MachineOperatorBuilderEPNS0_11TickCounterEPNS0_4ZoneE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler20ControlFlowOptimizer8OptimizeEv@PLT
	movq	-280(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L3189
	movq	-208(%rbp), %rax
	leaq	8(%rax), %rsi
	movq	-240(%rbp), %rax
	cmpq	%rax, %rsi
	jbe	.L3190
	movq	-288(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L3193:
	testq	%rdx, %rdx
	je	.L3191
	cmpq	$64, 8(%rdx)
	ja	.L3192
.L3191:
	movq	(%rax), %rdx
	movq	$64, 8(%rdx)
	movq	-288(%rbp), %rcx
	movq	%rcx, (%rdx)
	movq	%rdx, -288(%rbp)
.L3192:
	addq	$8, %rax
	cmpq	%rax, %rsi
	ja	.L3193
	movq	-280(%rbp), %rdx
.L3190:
	movq	-272(%rbp), %rax
	leaq	0(,%rax,8), %rcx
	cmpq	$15, %rcx
	jbe	.L3189
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L3189:
	testq	%rbx, %rbx
	je	.L3195
	movq	-608(%rbp), %rax
	movq	%rax, 48(%rbx)
.L3195:
	testq	%r15, %r15
	je	.L3196
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10ReturnZoneEPNS0_4ZoneE@PLT
.L3196:
	testq	%r13, %r13
	je	.L3197
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv@PLT
.L3197:
	movq	-488(%rbp), %rbx
	leaq	.LC213(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl17RunPrintAndVerifyEPKcb.constprop.0
	movq	(%rbx), %rbx
	movq	56(%rbx), %r13
	jmp	.L3186
	.p2align 4,,10
	.p2align 3
.L3142:
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	movq	-488(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rcx
	movq	320(%rax), %rdx
	movq	176(%rax), %r9
	movq	168(%rax), %r8
	movq	216(%rax), %rsi
	leaq	160(%rcx), %rax
	pushq	%rax
	movl	4(%rcx), %eax
	movq	%r15, %rcx
	pushq	%rax
	call	_ZN2v88internal8compiler18SimplifiedLoweringC1EPNS1_7JSGraphEPNS1_12JSHeapBrokerEPNS0_4ZoneEPNS1_19SourcePositionTableEPNS1_15NodeOriginTableENS0_24PoisoningMitigationLevelEPNS0_11TickCounterE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18SimplifiedLowering13LowerAllNodesEv@PLT
	popq	%rax
	popq	%rdx
	jmp	.L3211
	.p2align 4,,10
	.p2align 3
.L3153:
	movq	%r15, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rdx
	movq	-488(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler22EarlyOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	jmp	.L3210
	.p2align 4,,10
	.p2align 3
.L3199:
	movq	%r15, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, %rdx
	movq	-488(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler21LateOptimizationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	jmp	.L3209
	.p2align 4,,10
	.p2align 3
.L3222:
	movl	$8, %esi
	movl	$8, %ecx
	jmp	.L3166
	.p2align 4,,10
	.p2align 3
.L3118:
	movq	%r15, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%rax, %r13
	movq	-488(%rbp), %rax
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	160(%rax), %rdi
	call	_ZN2v88internal8compiler10LoopPeeler18EliminateLoopExitsEPNS1_5GraphEPNS0_4ZoneE@PLT
	jmp	.L3214
	.p2align 4,,10
	.p2align 3
.L3463:
	movq	(%r10), %r11
	movq	8(%r10), %rdi
	movq	%r10, -520(%rbp)
	movq	%r11, -536(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	-536(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-520(%rbp), %r10
	movq	%rax, %r8
	movq	%rax, 352(%r10)
	jmp	.L3147
	.p2align 4,,10
	.p2align 3
.L3468:
	movq	(%r10), %r11
	movq	8(%r10), %rdi
	movq	%r10, -536(%rbp)
	movq	%r11, -544(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	-544(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-536(%rbp), %r10
	movq	%rax, %r8
	movq	%rax, 352(%r10)
	jmp	.L3204
	.p2align 4,,10
	.p2align 3
.L3465:
	movq	8(%rbx), %rdi
	movq	(%rbx), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 352(%rbx)
	movq	%rax, %r8
	jmp	.L3176
	.p2align 4,,10
	.p2align 3
.L3217:
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L3104
	.p2align 4,,10
	.p2align 3
.L3225:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L3171:
	movq	(%rdx,%rsi,8), %r8
	movq	%r8, (%rax,%rsi,8)
	movq	%rsi, %r8
	addq	$1, %rsi
	cmpq	%r8, %rdi
	jne	.L3171
	jmp	.L3174
	.p2align 4,,10
	.p2align 3
.L3477:
	movq	-488(%rbp), %rax
	movl	$3, %esi
	movq	(%rax), %rax
	movq	24(%rax), %rdi
	call	_ZN2v88internal24OptimizedCompilationInfo17AbortOptimizationENS0_13BailoutReasonE@PLT
	movq	-504(%rbp), %rax
	movq	56(%rax), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3077
	movb	%al, -488(%rbp)
	call	_ZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEv@PLT
	movzbl	-488(%rbp), %eax
	jmp	.L3077
	.p2align 4,,10
	.p2align 3
.L3123:
	movq	%r15, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rdx
	movq	-488(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler20LoadEliminationPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	jmp	.L3213
	.p2align 4,,10
	.p2align 3
.L3129:
	movq	%r15, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler9ZoneStats12NewEmptyZoneEPKc@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rdx
	movq	-488(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler19EscapeAnalysisPhase3RunEPNS1_12PipelineDataEPNS0_4ZoneE
	jmp	.L3212
	.p2align 4,,10
	.p2align 3
.L3220:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L3109:
	movq	(%r15,%rsi,8), %r8
	movq	%r8, (%rax,%rsi,8)
	movq	%rsi, %r8
	addq	$1, %rsi
	cmpq	%rdi, %r8
	jne	.L3109
	jmp	.L3112
	.p2align 4,,10
	.p2align 3
.L3478:
	movq	(%r9), %r10
	movq	8(%r9), %rdi
	movq	%r9, -520(%rbp)
	movq	%r10, -536(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4DeadEv@PLT
	movq	-536(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-520(%rbp), %r9
	movq	%rax, %r8
	movq	%rax, 352(%r9)
	jmp	.L3137
	.p2align 4,,10
	.p2align 3
.L3472:
	movq	%r11, -616(%rbp)
	movq	%r10, -592(%rbp)
	movq	%r8, -584(%rbp)
	movq	%rdx, -576(%rbp)
	movq	%rcx, -552(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-552(%rbp), %rcx
	movq	-576(%rbp), %rdx
	movq	-584(%rbp), %r8
	movq	-592(%rbp), %r10
	movq	-616(%rbp), %r11
	jmp	.L3169
	.p2align 4,,10
	.p2align 3
.L3475:
	movq	%r11, -632(%rbp)
	movq	%r10, -624(%rbp)
	movq	%rcx, -616(%rbp)
	movq	%rdx, -544(%rbp)
	movq	%r8, -536(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-536(%rbp), %r8
	movq	-544(%rbp), %rdx
	movq	-616(%rbp), %rcx
	movq	-624(%rbp), %r10
	movq	-632(%rbp), %r11
	jmp	.L3107
.L3470:
	call	__stack_chk_fail@PLT
.L3165:
	leaq	.LC26(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3473:
	cmpq	$268435455, %rcx
	movl	$268435455, %eax
	cmova	%rax, %rcx
	salq	$3, %rcx
	movq	%rcx, %rsi
	jmp	.L3166
.L3476:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L3104
	.cfi_endproc
.LFE26721:
	.size	_ZN2v88internal8compiler12PipelineImpl13OptimizeGraphEPNS1_7LinkageE, .-_ZN2v88internal8compiler12PipelineImpl13OptimizeGraphEPNS1_7LinkageE
	.section	.rodata._ZN2v88internal8compiler22PipelineCompilationJob14ExecuteJobImplEv.str1.1,"aMS",@progbits,1
.LC216:
	.string	"v8.optimizingCompile.execute"
	.section	.text._ZN2v88internal8compiler22PipelineCompilationJob14ExecuteJobImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22PipelineCompilationJob14ExecuteJobImplEv
	.type	_ZN2v88internal8compiler22PipelineCompilationJob14ExecuteJobImplEv, @function
_ZN2v88internal8compiler22PipelineCompilationJob14ExecuteJobImplEv:
.LFB26465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal8compiler22PipelineCompilationJob14ExecuteJobImplEvE29trace_event_unique_atomic1011(%rip), %r12
	testq	%r12, %r12
	je	.L3511
.L3481:
	movq	$0, -96(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L3512
.L3483:
	movq	840(%rbx), %rsi
	leaq	832(%rbx), %r12
	movl	$1, %r13d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl13OptimizeGraphEPNS1_7LinkageE
	testb	%al, %al
	jne	.L3513
.L3488:
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3514
	leaq	-24(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3513:
	.cfi_restore_state
	movq	840(%rbx), %rsi
	movq	%r12, %rdi
	leaq	-104(%rbp), %rdx
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3489
	movq	(%rdi), %rax
	call	*8(%rax)
.L3489:
	xorl	%r13d, %r13d
	jmp	.L3488
	.p2align 4,,10
	.p2align 3
.L3512:
	movq	24(%rbx), %rax
	leaq	-120(%rbp), %rdi
	leaq	-128(%rbp), %rsi
	xorl	%r13d, %r13d
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo10TraceIDRefEv@PLT
	leaq	.LC63(%rip), %rax
	movb	$8, -129(%rbp)
	movq	%rax, -112(%rbp)
	movq	-120(%rbp), %rax
	movq	$0, -56(%rbp)
	movq	$0, -120(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3515
.L3484:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3485
	movq	(%rdi), %rax
	call	*8(%rax)
.L3485:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3486
	movq	(%rdi), %rax
	call	*8(%rax)
.L3486:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3487
	movq	(%rdi), %rax
	call	*8(%rax)
.L3487:
	leaq	.LC216(%rip), %rax
	movq	%r12, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r13, -72(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L3483
	.p2align 4,,10
	.p2align 3
.L3511:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3516
.L3482:
	movq	%r12, _ZZN2v88internal8compiler22PipelineCompilationJob14ExecuteJobImplEvE29trace_event_unique_atomic1011(%rip)
	jmp	.L3481
	.p2align 4,,10
	.p2align 3
.L3515:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$772
	leaq	.LC216(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	leaq	-104(%rbp), %rdx
	pushq	%rdx
	leaq	-129(%rbp), %rdx
	pushq	%rdx
	leaq	-112(%rbp), %rdx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$1
	pushq	%rbx
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L3484
	.p2align 4,,10
	.p2align 3
.L3516:
	leaq	.LC55(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L3482
.L3514:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26465:
	.size	_ZN2v88internal8compiler22PipelineCompilationJob14ExecuteJobImplEv, .-_ZN2v88internal8compiler22PipelineCompilationJob14ExecuteJobImplEv
	.section	.text._ZN2v88internal8compiler8Pipeline22GenerateCodeForTestingEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPSt10unique_ptrINS1_12JSHeapBrokerESt14default_deleteIS8_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler8Pipeline22GenerateCodeForTestingEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPSt10unique_ptrINS1_12JSHeapBrokerESt14default_deleteIS8_EE
	.type	_ZN2v88internal8compiler8Pipeline22GenerateCodeForTestingEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPSt10unique_ptrINS1_12JSHeapBrokerESt14default_deleteIS8_EE, @function
_ZN2v88internal8compiler8Pipeline22GenerateCodeForTestingEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPSt10unique_ptrINS1_12JSHeapBrokerESt14default_deleteIS8_EE:
.LFB26818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-592(%rbp), %r14
	leaq	-512(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$600, %rsp
	movq	%rdx, -632(%rbp)
	movq	41136(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler9ZoneStatsC1EPNS0_19AccountingAllocatorE@PLT
	movq	%r12, %rdx
	xorl	%edi, %edi
	movq	%r14, %rcx
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsENS0_6HandleINS0_6ScriptEEEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_9ZoneStatsE
	movq	%r12, %rdx
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%rax, %r8
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler12PipelineDataC1EPNS1_9ZoneStatsEPNS0_7IsolateEPNS0_24OptimizedCompilationInfoEPNS1_18PipelineStatisticsE
	movq	-248(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r15, -616(%rbp)
	call	_ZN2v88internal8compiler7Linkage15ComputeIncomingEPNS0_4ZoneEPNS0_24OptimizedCompilationInfoE@PLT
	movq	%r12, %rdi
	leaq	-616(%rbp), %r12
	movq	%rax, -608(%rbp)
	call	_ZN2v88internal11Deoptimizer34EnsureCodeForDeoptimizationEntriesEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler12PipelineImpl11CreateGraphEv
	testb	%al, %al
	jne	.L3518
.L3522:
	xorl	%r12d, %r12d
.L3519:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler12PipelineDataD1Ev
	testq	%r13, %r13
	je	.L3536
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18PipelineStatisticsD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L3536:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ZoneStatsD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3576
	addq	$600, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3518:
	.cfi_restore_state
	leaq	-608(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rsi, -640(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl13OptimizeGraphEPNS1_7LinkageE
	movq	-640(%rbp), %rsi
	testb	%al, %al
	je	.L3522
	movq	%r12, %rdi
	leaq	-600(%rbp), %rdx
	movq	$0, -600(%rbp)
	call	_ZN2v88internal8compiler12PipelineImpl12AssembleCodeEPNS1_7LinkageESt10unique_ptrINS0_15AssemblerBufferESt14default_deleteIS6_EE
	movq	-600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3521
	movq	(%rdi), %rax
	call	*8(%rax)
.L3521:
	xorl	%esi, %esi
	cmpq	$0, -632(%rbp)
	movq	%r12, %rdi
	sete	%sil
	call	_ZN2v88internal8compiler12PipelineImpl12FinalizeCodeEb
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3522
	movq	-616(%rbp), %rax
	movq	312(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3523
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler23CompilationDependencies6CommitENS0_6HandleINS0_4CodeEEE@PLT
	testb	%al, %al
	je	.L3522
.L3523:
	cmpq	$0, -632(%rbp)
	je	.L3519
	movq	-632(%rbp), %rcx
	movq	-192(%rbp), %rax
	movq	$0, -192(%rbp)
	movq	(%rcx), %r8
	movq	%rax, (%rcx)
	testq	%r8, %r8
	je	.L3519
	movq	640(%r8), %rax
	testq	%rax, %rax
	je	.L3528
	.p2align 4,,10
	.p2align 3
.L3529:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L3529
.L3528:
	movq	632(%r8), %rax
	movq	624(%r8), %rdi
	xorl	%esi, %esi
	movq	%r8, -632(%rbp)
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-632(%rbp), %r8
	movq	576(%r8), %rax
	testq	%rax, %rax
	je	.L3530
	.p2align 4,,10
	.p2align 3
.L3531:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L3531
.L3530:
	movq	568(%r8), %rax
	movq	560(%r8), %rdi
	xorl	%esi, %esi
	movq	%r8, -632(%rbp)
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-632(%rbp), %r8
	movq	512(%r8), %rax
	testq	%rax, %rax
	je	.L3532
	.p2align 4,,10
	.p2align 3
.L3533:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L3533
.L3532:
	movq	504(%r8), %rax
	movq	496(%r8), %rdi
	xorl	%esi, %esi
	movq	%r8, -632(%rbp)
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-632(%rbp), %r8
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rax, 208(%r8)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	192(%r8), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 128(%r8)
	call	_ZNSt6localeD1Ev@PLT
	movq	-632(%rbp), %r8
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%rax, 128(%r8)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	208(%r8), %rdi
	movq	%rax, 208(%r8)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-632(%rbp), %r8
	movq	80(%r8), %rax
	testq	%rax, %rax
	je	.L3534
	.p2align 4,,10
	.p2align 3
.L3535:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L3535
.L3534:
	movq	72(%r8), %rax
	movq	64(%r8), %rdi
	xorl	%esi, %esi
	movq	%r8, -632(%rbp)
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-632(%rbp), %r8
	movl	$712, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3519
.L3576:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26818:
	.size	_ZN2v88internal8compiler8Pipeline22GenerateCodeForTestingEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPSt10unique_ptrINS1_12JSHeapBrokerESt14default_deleteIS8_EE, .-_ZN2v88internal8compiler8Pipeline22GenerateCodeForTestingEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPSt10unique_ptrINS1_12JSHeapBrokerESt14default_deleteIS8_EE
	.weak	_ZTVN2v88internal9AssemblerE
	.section	.data.rel.ro.local._ZTVN2v88internal9AssemblerE,"awG",@progbits,_ZTVN2v88internal9AssemblerE,comdat
	.align 8
	.type	_ZTVN2v88internal9AssemblerE, @object
	.size	_ZTVN2v88internal9AssemblerE, 40
_ZTVN2v88internal9AssemblerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal9AssemblerD1Ev
	.quad	_ZN2v88internal9AssemblerD0Ev
	.quad	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.weak	_ZTVN2v88internal14CompilationJobE
	.section	.data.rel.ro.local._ZTVN2v88internal14CompilationJobE,"awG",@progbits,_ZTVN2v88internal14CompilationJobE,comdat
	.align 8
	.type	_ZTVN2v88internal14CompilationJobE, @object
	.size	_ZTVN2v88internal14CompilationJobE, 32
_ZTVN2v88internal14CompilationJobE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CompilationJobD1Ev
	.quad	_ZN2v88internal14CompilationJobD0Ev
	.hidden	_ZTCN2v88internal8OFStreamE0_So
	.weak	_ZTCN2v88internal8OFStreamE0_So
	.section	.rodata._ZTCN2v88internal8OFStreamE0_So,"aG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal8OFStreamE0_So, @object
	.size	_ZTCN2v88internal8OFStreamE0_So, 80
_ZTCN2v88internal8OFStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal8OFStreamE, @object
	.size	_ZTTN2v88internal8OFStreamE, 32
_ZTTN2v88internal8OFStreamE:
	.quad	_ZTVN2v88internal8OFStreamE+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+64
	.quad	_ZTVN2v88internal8OFStreamE+64
	.weak	_ZTVN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal8OFStreamE, @object
	.size	_ZTVN2v88internal8OFStreamE, 80
_ZTVN2v88internal8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8OFStreamD1Ev
	.quad	_ZN2v88internal8OFStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.weak	_ZTVN2v88internal8compiler9JSInlinerE
	.section	.data.rel.ro._ZTVN2v88internal8compiler9JSInlinerE,"awG",@progbits,_ZTVN2v88internal8compiler9JSInlinerE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler9JSInlinerE, @object
	.size	_ZTVN2v88internal8compiler9JSInlinerE, 56
_ZTVN2v88internal8compiler9JSInlinerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler9JSInlinerD1Ev
	.quad	_ZN2v88internal8compiler9JSInlinerD0Ev
	.quad	_ZNK2v88internal8compiler9JSInliner12reducer_nameEv
	.quad	_ZN2v88internal8compiler9JSInliner6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperE,"aw"
	.align 8
	.type	_ZTVN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperE, @object
	.size	_ZTVN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperE, 56
_ZTVN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperD1Ev
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapperD0Ev
	.quad	_ZNK2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper12reducer_nameEv
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_121SourcePositionWrapper8FinalizeEv
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperE,"aw"
	.align 8
	.type	_ZTVN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperE, @object
	.size	_ZTVN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperE, 56
_ZTVN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperD1Ev
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapperD0Ev
	.quad	_ZNK2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper12reducer_nameEv
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler12_GLOBAL__N_118NodeOriginsWrapper8FinalizeEv
	.weak	_ZTVN2v88internal8compiler22PipelineCompilationJobE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler22PipelineCompilationJobE,"awG",@progbits,_ZTVN2v88internal8compiler22PipelineCompilationJobE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler22PipelineCompilationJobE, @object
	.size	_ZTVN2v88internal8compiler22PipelineCompilationJobE, 56
_ZTVN2v88internal8compiler22PipelineCompilationJobE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler22PipelineCompilationJobD1Ev
	.quad	_ZN2v88internal8compiler22PipelineCompilationJobD0Ev
	.quad	_ZN2v88internal8compiler22PipelineCompilationJob14PrepareJobImplEPNS0_7IsolateE
	.quad	_ZN2v88internal8compiler22PipelineCompilationJob14ExecuteJobImplEv
	.quad	_ZN2v88internal8compiler22PipelineCompilationJob15FinalizeJobImplEPNS0_7IsolateE
	.weak	_ZTVN2v88internal8compiler26WasmHeapStubCompilationJobE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler26WasmHeapStubCompilationJobE,"awG",@progbits,_ZTVN2v88internal8compiler26WasmHeapStubCompilationJobE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler26WasmHeapStubCompilationJobE, @object
	.size	_ZTVN2v88internal8compiler26WasmHeapStubCompilationJobE, 56
_ZTVN2v88internal8compiler26WasmHeapStubCompilationJobE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler26WasmHeapStubCompilationJobD1Ev
	.quad	_ZN2v88internal8compiler26WasmHeapStubCompilationJobD0Ev
	.quad	_ZN2v88internal8compiler26WasmHeapStubCompilationJob14PrepareJobImplEPNS0_7IsolateE
	.quad	_ZN2v88internal8compiler26WasmHeapStubCompilationJob14ExecuteJobImplEv
	.quad	_ZN2v88internal8compiler26WasmHeapStubCompilationJob15FinalizeJobImplEPNS0_7IsolateE
	.section	.bss._ZZN2v88internal8compiler22PipelineCompilationJob15FinalizeJobImplEPNS0_7IsolateEE29trace_event_unique_atomic1022,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8compiler22PipelineCompilationJob15FinalizeJobImplEPNS0_7IsolateEE29trace_event_unique_atomic1022, @object
	.size	_ZZN2v88internal8compiler22PipelineCompilationJob15FinalizeJobImplEPNS0_7IsolateEE29trace_event_unique_atomic1022, 8
_ZZN2v88internal8compiler22PipelineCompilationJob15FinalizeJobImplEPNS0_7IsolateEE29trace_event_unique_atomic1022:
	.zero	8
	.section	.bss._ZZN2v88internal8compiler22PipelineCompilationJob14ExecuteJobImplEvE29trace_event_unique_atomic1011,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8compiler22PipelineCompilationJob14ExecuteJobImplEvE29trace_event_unique_atomic1011, @object
	.size	_ZZN2v88internal8compiler22PipelineCompilationJob14ExecuteJobImplEvE29trace_event_unique_atomic1011, 8
_ZZN2v88internal8compiler22PipelineCompilationJob14ExecuteJobImplEvE29trace_event_unique_atomic1011:
	.zero	8
	.section	.bss._ZZN2v88internal8compiler22PipelineCompilationJob14PrepareJobImplEPNS0_7IsolateEE28trace_event_unique_atomic942,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8compiler22PipelineCompilationJob14PrepareJobImplEPNS0_7IsolateEE28trace_event_unique_atomic942, @object
	.size	_ZZN2v88internal8compiler22PipelineCompilationJob14PrepareJobImplEPNS0_7IsolateEE28trace_event_unique_atomic942, 8
_ZZN2v88internal8compiler22PipelineCompilationJob14PrepareJobImplEPNS0_7IsolateEE28trace_event_unique_atomic942:
	.zero	8
	.section	.bss._ZZN2v88internal8compiler22PipelineCompilationJobD4EvE28trace_event_unique_atomic934,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8compiler22PipelineCompilationJobD4EvE28trace_event_unique_atomic934, @object
	.size	_ZZN2v88internal8compiler22PipelineCompilationJobD4EvE28trace_event_unique_atomic934, 8
_ZZN2v88internal8compiler22PipelineCompilationJobD4EvE28trace_event_unique_atomic934:
	.zero	8
	.section	.bss._ZZN2v88internal8compiler22PipelineCompilationJobC4EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEEE28trace_event_unique_atomic928,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8compiler22PipelineCompilationJobC4EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEEE28trace_event_unique_atomic928, @object
	.size	_ZZN2v88internal8compiler22PipelineCompilationJobC4EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEEE28trace_event_unique_atomic928, 8
_ZZN2v88internal8compiler22PipelineCompilationJobC4EPNS0_7IsolateENS0_6HandleINS0_18SharedFunctionInfoEEENS5_INS0_10JSFunctionEEEE28trace_event_unique_atomic928:
	.zero	8
	.section	.bss._ZZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsEPNS0_4wasm10WasmEngineENS3_12FunctionBodyEPKNS3_10WasmModuleEPNS0_24OptimizedCompilationInfoEPNS1_9ZoneStatsEE28trace_event_unique_atomic847,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsEPNS0_4wasm10WasmEngineENS3_12FunctionBodyEPKNS3_10WasmModuleEPNS0_24OptimizedCompilationInfoEPNS1_9ZoneStatsEE28trace_event_unique_atomic847, @object
	.size	_ZZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsEPNS0_4wasm10WasmEngineENS3_12FunctionBodyEPKNS3_10WasmModuleEPNS0_24OptimizedCompilationInfoEPNS1_9ZoneStatsEE28trace_event_unique_atomic847, 8
_ZZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsEPNS0_4wasm10WasmEngineENS3_12FunctionBodyEPKNS3_10WasmModuleEPNS0_24OptimizedCompilationInfoEPNS1_9ZoneStatsEE28trace_event_unique_atomic847:
	.zero	8
	.section	.bss._ZZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsENS0_6HandleINS0_6ScriptEEEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_9ZoneStatsEE28trace_event_unique_atomic821,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsENS0_6HandleINS0_6ScriptEEEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_9ZoneStatsEE28trace_event_unique_atomic821, @object
	.size	_ZZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsENS0_6HandleINS0_6ScriptEEEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_9ZoneStatsEE28trace_event_unique_atomic821, 8
_ZZN2v88internal8compiler12_GLOBAL__N_124CreatePipelineStatisticsENS0_6HandleINS0_6ScriptEEEPNS0_24OptimizedCompilationInfoEPNS0_7IsolateEPNS1_9ZoneStatsEE28trace_event_unique_atomic821:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC28:
	.quad	0
	.quad	256
	.section	.data.rel.ro,"aw"
	.align 8
.LC74:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC75:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC76:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.align 8
.LC157:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC174:
	.quad	_ZTVN2v88internal8compiler13JSCallReducerE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
