	.file	"node-origin-table.cc"
	.text
	.section	.text._ZN2v88internal8compiler15NodeOriginTable9DecoratorD2Ev,"axG",@progbits,_ZN2v88internal8compiler15NodeOriginTable9DecoratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler15NodeOriginTable9DecoratorD2Ev
	.type	_ZN2v88internal8compiler15NodeOriginTable9DecoratorD2Ev, @function
_ZN2v88internal8compiler15NodeOriginTable9DecoratorD2Ev:
.LFB11911:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11911:
	.size	_ZN2v88internal8compiler15NodeOriginTable9DecoratorD2Ev, .-_ZN2v88internal8compiler15NodeOriginTable9DecoratorD2Ev
	.weak	_ZN2v88internal8compiler15NodeOriginTable9DecoratorD1Ev
	.set	_ZN2v88internal8compiler15NodeOriginTable9DecoratorD1Ev,_ZN2v88internal8compiler15NodeOriginTable9DecoratorD2Ev
	.section	.rodata._ZN2v88internal8compiler15NodeOriginTable9DecoratorD0Ev.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler15NodeOriginTable9DecoratorD0Ev,"axG",@progbits,_ZN2v88internal8compiler15NodeOriginTable9DecoratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler15NodeOriginTable9DecoratorD0Ev
	.type	_ZN2v88internal8compiler15NodeOriginTable9DecoratorD0Ev, @function
_ZN2v88internal8compiler15NodeOriginTable9DecoratorD0Ev:
.LFB11913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE11913:
	.size	_ZN2v88internal8compiler15NodeOriginTable9DecoratorD0Ev, .-_ZN2v88internal8compiler15NodeOriginTable9DecoratorD0Ev
	.section	.rodata._ZNK2v88internal8compiler10NodeOrigin9PrintJsonERSo.str1.1,"aMS",@progbits,1
.LC1:
	.string	"{ "
.LC2:
	.string	"\"nodeId\" : "
.LC3:
	.string	"\"bytecodePosition\" : "
.LC4:
	.string	", \"reducer\" : \""
.LC5:
	.string	"\""
.LC6:
	.string	", \"phase\" : \""
.LC7:
	.string	"}"
	.section	.text._ZNK2v88internal8compiler10NodeOrigin9PrintJsonERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler10NodeOrigin9PrintJsonERSo
	.type	_ZNK2v88internal8compiler10NodeOrigin9PrintJsonERSo, @function
_ZNK2v88internal8compiler10NodeOrigin9PrintJsonERSo:
.LFB10189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	.LC1(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	16(%rbx), %eax
	testl	%eax, %eax
	je	.L6
	cmpl	$1, %eax
	jne	.L7
	movl	$11, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L7:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movl	$15, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %r13
	testq	%r13, %r13
	je	.L13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L9:
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$13, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L14
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L11:
	movq	%r12, %rdi
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1, %edx
	popq	%rbx
	leaq	.LC7(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$21, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L14:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L13:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L9
	.cfi_endproc
.LFE10189:
	.size	_ZNK2v88internal8compiler10NodeOrigin9PrintJsonERSo, .-_ZNK2v88internal8compiler10NodeOrigin9PrintJsonERSo
	.section	.rodata._ZN2v88internal8compiler15NodeOriginTableC2EPNS1_5GraphE.str1.1,"aMS",@progbits,1
.LC8:
	.string	""
.LC9:
	.string	"unknown"
	.section	.text._ZN2v88internal8compiler15NodeOriginTableC2EPNS1_5GraphE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15NodeOriginTableC2EPNS1_5GraphE
	.type	_ZN2v88internal8compiler15NodeOriginTableC2EPNS1_5GraphE, @function
_ZN2v88internal8compiler15NodeOriginTableC2EPNS1_5GraphE:
.LFB10208:
	.cfi_startproc
	endbr64
	leaq	.LC8(%rip), %rax
	movq	%rsi, (%rdi)
	movq	%rax, 16(%rdi)
	movq	%rax, 24(%rdi)
	movabsq	$-9223372036854775808, %rax
	movq	%rax, 40(%rdi)
	leaq	.LC9(%rip), %rax
	movq	%rax, 48(%rdi)
	movq	(%rsi), %rax
	movq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	ret
	.cfi_endproc
.LFE10208:
	.size	_ZN2v88internal8compiler15NodeOriginTableC2EPNS1_5GraphE, .-_ZN2v88internal8compiler15NodeOriginTableC2EPNS1_5GraphE
	.globl	_ZN2v88internal8compiler15NodeOriginTableC1EPNS1_5GraphE
	.set	_ZN2v88internal8compiler15NodeOriginTableC1EPNS1_5GraphE,_ZN2v88internal8compiler15NodeOriginTableC2EPNS1_5GraphE
	.section	.text._ZN2v88internal8compiler15NodeOriginTable12AddDecoratorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15NodeOriginTable12AddDecoratorEv
	.type	_ZN2v88internal8compiler15NodeOriginTable12AddDecoratorEv, @function
_ZN2v88internal8compiler15NodeOriginTable12AddDecoratorEv:
.LFB10210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	(%rax), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	$15, %rax
	jbe	.L20
	leaq	16(%rsi), %rax
	movq	%rax, 16(%rdi)
.L18:
	leaq	16+_ZTVN2v88internal8compiler15NodeOriginTable9DecoratorE(%rip), %rax
	movq	%rbx, 8(%rsi)
	movq	%rax, (%rsi)
	movq	(%rbx), %rdi
	movq	%rsi, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler5Graph12AddDecoratorEPNS1_14GraphDecoratorE@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L18
	.cfi_endproc
.LFE10210:
	.size	_ZN2v88internal8compiler15NodeOriginTable12AddDecoratorEv, .-_ZN2v88internal8compiler15NodeOriginTable12AddDecoratorEv
	.section	.text._ZN2v88internal8compiler15NodeOriginTable15RemoveDecoratorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15NodeOriginTable15RemoveDecoratorEv
	.type	_ZN2v88internal8compiler15NodeOriginTable15RemoveDecoratorEv, @function
_ZN2v88internal8compiler15NodeOriginTable15RemoveDecoratorEv:
.LFB10211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rsi
	movq	(%rdi), %rdi
	call	_ZN2v88internal8compiler5Graph15RemoveDecoratorEPNS1_14GraphDecoratorE@PLT
	movq	$0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10211:
	.size	_ZN2v88internal8compiler15NodeOriginTable15RemoveDecoratorEv, .-_ZN2v88internal8compiler15NodeOriginTable15RemoveDecoratorEv
	.section	.text._ZNK2v88internal8compiler15NodeOriginTable13GetNodeOriginEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15NodeOriginTable13GetNodeOriginEPNS1_4NodeE
	.type	_ZNK2v88internal8compiler15NodeOriginTable13GetNodeOriginEPNS1_4NodeE, @function
_ZNK2v88internal8compiler15NodeOriginTable13GetNodeOriginEPNS1_4NodeE:
.LFB10212:
	.cfi_startproc
	endbr64
	movl	20(%rdx), %ecx
	movq	%rdi, %rax
	movq	72(%rsi), %rdx
	movq	64(%rsi), %rdi
	andl	$16777215, %ecx
	subq	%rdi, %rdx
	sarq	$5, %rdx
	cmpq	%rdx, %rcx
	jnb	.L24
	salq	$5, %rcx
	addq	%rdi, %rcx
	movdqu	(%rcx), %xmm1
	movdqu	16(%rcx), %xmm2
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	.LC8(%rip), %rdi
	movabsq	$-9223372036854775808, %rsi
	movq	%rdi, %xmm0
	movq	%rsi, 24(%rax)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	ret
	.cfi_endproc
.LFE10212:
	.size	_ZNK2v88internal8compiler15NodeOriginTable13GetNodeOriginEPNS1_4NodeE, .-_ZNK2v88internal8compiler15NodeOriginTable13GetNodeOriginEPNS1_4NodeE
	.section	.rodata._ZNK2v88internal8compiler15NodeOriginTable9PrintJsonERSo.str1.1,"aMS",@progbits,1
.LC10:
	.string	"{"
.LC11:
	.string	","
.LC12:
	.string	": "
	.section	.text._ZNK2v88internal8compiler15NodeOriginTable9PrintJsonERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler15NodeOriginTable9PrintJsonERSo
	.type	_ZNK2v88internal8compiler15NodeOriginTable9PrintJsonERSo, @function
_ZNK2v88internal8compiler15NodeOriginTable9PrintJsonERSo:
.LFB10214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	.LC10(%rip), %rsi
	pushq	%r13
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	64(%r15), %rax
	movq	72(%r15), %r12
	subq	%rax, %r12
	sarq	$5, %r12
	je	.L30
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	leaq	-96(%rbp), %r13
.L27:
	movq	%rbx, %rdx
	salq	$5, %rdx
	addq	%rdx, %rax
	movdqu	(%rax), %xmm0
	movl	16(%rax), %edx
	movq	24(%rax), %rax
	movl	%edx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movaps	%xmm0, -96(%rbp)
	testq	%rax, %rax
	js	.L28
	testb	%cl, %cl
	jne	.L40
.L29:
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-104(%rbp), %rdi
	movl	$2, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler10NodeOrigin9PrintJsonERSo
	movl	$1, %ecx
.L28:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	je	.L30
	movq	64(%r15), %rax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L29
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10214:
	.size	_ZNK2v88internal8compiler15NodeOriginTable9PrintJsonERSo, .-_ZNK2v88internal8compiler15NodeOriginTable9PrintJsonERSo
	.section	.rodata._ZNSt6vectorIN2v88internal8compiler10NodeOriginENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_.str1.1,"aMS",@progbits,1
.LC13:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIN2v88internal8compiler10NodeOriginENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler10NodeOriginENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler10NodeOriginENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	.type	_ZNSt6vectorIN2v88internal8compiler10NodeOriginENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_, @function
_ZNSt6vectorIN2v88internal8compiler10NodeOriginENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_:
.LFB11610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L42
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	movq	%rsi, %r12
	movq	%rsi, %r13
	movq	24(%rbx), %rax
	subq	%rdi, %rax
	sarq	$5, %rax
	cmpq	%rdx, %rax
	jb	.L44
	movq	%rdi, %r8
	movdqu	(%rcx), %xmm1
	movdqu	16(%rcx), %xmm0
	subq	%rsi, %r8
	movq	%r8, %rax
	movups	%xmm1, -72(%rbp)
	sarq	$5, %rax
	movups	%xmm0, -56(%rbp)
	cmpq	%rax, %rdx
	jnb	.L45
	salq	$5, %rdx
	movq	%rdx, %r14
	movq	%rdi, %rdx
	subq	%r14, %rdx
	cmpq	%rdx, %rdi
	je	.L71
	movq	%rdx, %rax
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L47:
	movdqu	(%rax), %xmm2
	addq	$32, %rax
	addq	$32, %rcx
	movups	%xmm2, -32(%rcx)
	movdqu	-16(%rax), %xmm3
	movups	%xmm3, -16(%rcx)
	cmpq	%rax, %rdi
	jne	.L47
	movq	16(%rbx), %rax
.L46:
	addq	%r14, %rax
	movq	%rax, 16(%rbx)
	cmpq	%rdx, %r12
	je	.L48
	subq	%r12, %rdx
	movq	%r12, %rsi
	subq	%rdx, %rdi
	call	memmove@PLT
.L48:
	leaq	(%r12,%r14), %rdx
	cmpq	%rdx, %r12
	je	.L42
	movdqu	-72(%rbp), %xmm1
	movdqu	-56(%rbp), %xmm0
	.p2align 4,,10
	.p2align 3
.L49:
	movups	%xmm1, 0(%r13)
	addq	$32, %r13
	movups	%xmm0, -16(%r13)
	cmpq	%r13, %rdx
	jne	.L49
	.p2align 4,,10
	.p2align 3
.L42:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	subq	%rax, %rdx
	movq	%rdi, %rax
	je	.L52
	movq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L53:
	movups	%xmm1, (%rax)
	addq	$32, %rax
	movups	%xmm0, -16(%rax)
	subq	$1, %rcx
	jne	.L53
	salq	$5, %rdx
	leaq	(%rdi,%rdx), %rax
.L52:
	movq	%rax, 16(%rbx)
	cmpq	%r12, %rdi
	je	.L54
	movq	%r12, %rsi
	.p2align 4,,10
	.p2align 3
.L55:
	movdqu	(%rsi), %xmm4
	addq	$32, %rsi
	addq	$32, %rax
	movups	%xmm4, -32(%rax)
	movdqu	-16(%rsi), %xmm5
	movups	%xmm5, -16(%rax)
	cmpq	%rsi, %rdi
	jne	.L55
	addq	%r8, 16(%rbx)
	movdqu	-72(%rbp), %xmm1
	movdqu	-56(%rbp), %xmm0
	.p2align 4,,10
	.p2align 3
.L57:
	movups	%xmm1, 0(%r13)
	addq	$32, %r13
	movups	%xmm0, -16(%r13)
	cmpq	%r13, %rdi
	jne	.L57
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L44:
	movq	8(%rbx), %rax
	movl	$67108863, %r14d
	movq	%r14, %rsi
	subq	%rax, %rdi
	sarq	$5, %rdi
	subq	%rdi, %rsi
	cmpq	%rsi, %rdx
	ja	.L89
	cmpq	%rdi, %rdx
	movq	%rdi, %rsi
	cmovnb	%rdx, %rsi
	addq	%rsi, %rdi
	setc	%sil
	subq	%rax, %r13
	movzbl	%sil, %esi
	testq	%rsi, %rsi
	jne	.L73
	testq	%rdi, %rdi
	jne	.L90
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L62:
	leaq	(%rax,%r13), %rsi
	movq	%rdx, %rdi
	.p2align 4,,10
	.p2align 3
.L65:
	movdqu	(%rcx), %xmm6
	addq	$32, %rsi
	movups	%xmm6, -32(%rsi)
	movdqu	16(%rcx), %xmm7
	movups	%xmm7, -16(%rsi)
	subq	$1, %rdi
	jne	.L65
	movq	8(%rbx), %rdi
	cmpq	%rdi, %r12
	je	.L75
	movq	%rdi, %rcx
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L67:
	movdqu	(%rcx), %xmm6
	addq	$32, %rcx
	addq	$32, %rsi
	movups	%xmm6, -32(%rsi)
	movdqu	-16(%rcx), %xmm7
	movups	%xmm7, -16(%rsi)
	cmpq	%rcx, %r12
	jne	.L67
	movq	%r12, %rsi
	subq	%rdi, %rsi
	movq	%rsi, %rdi
	addq	%rax, %rdi
.L66:
	movq	16(%rbx), %rsi
	salq	$5, %rdx
	addq	%rdx, %rdi
	cmpq	%rsi, %r12
	je	.L68
	movq	%r12, %rdx
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L69:
	movdqu	(%rdx), %xmm2
	addq	$32, %rdx
	addq	$32, %rcx
	movups	%xmm2, -32(%rcx)
	movdqu	-16(%rdx), %xmm3
	movups	%xmm3, -16(%rcx)
	cmpq	%rdx, %rsi
	jne	.L69
	subq	%r12, %rsi
	addq	%rsi, %rdi
.L68:
	movq	%rax, %xmm0
	movq	%rdi, %xmm4
	movq	%r8, 24(%rbx)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%rbx)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$2147483616, %esi
	movl	$2147483616, %r14d
.L61:
	movq	(%rbx), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L91
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L64:
	leaq	(%rax,%r14), %r8
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%rdi, %rax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L54:
	addq	%r8, %rax
	movq	%rax, 16(%rbx)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%rax, %rdi
	jmp	.L66
.L91:
	movq	%r8, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rcx
	jmp	.L64
.L90:
	cmpq	$67108863, %rdi
	cmova	%r14, %rdi
	salq	$5, %rdi
	movq	%rdi, %r14
	movq	%rdi, %rsi
	jmp	.L61
.L88:
	call	__stack_chk_fail@PLT
.L89:
	leaq	.LC13(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11610:
	.size	_ZNSt6vectorIN2v88internal8compiler10NodeOriginENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_, .-_ZNSt6vectorIN2v88internal8compiler10NodeOriginENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	.section	.text._ZN2v88internal8compiler15NodeOriginTable9Decorator8DecorateEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler15NodeOriginTable9Decorator8DecorateEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler15NodeOriginTable9Decorator8DecorateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler15NodeOriginTable9Decorator8DecorateEPNS1_4NodeE, @function
_ZN2v88internal8compiler15NodeOriginTable9Decorator8DecorateEPNS1_4NodeE:
.LFB10200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %r12
	movl	20(%rsi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	72(%r12), %rsi
	movq	64(%r12), %rax
	andl	$16777215, %ebx
	movq	%rsi, %rcx
	subq	%rax, %rcx
	sarq	$5, %rcx
	cmpq	%rcx, %rbx
	jnb	.L100
.L93:
	salq	$5, %rbx
	addq	%rax, %rbx
	movq	24(%r12), %rax
	cmpq	%rax, 8(%rbx)
	je	.L101
.L96:
	movdqu	16(%r12), %xmm1
	movups	%xmm1, (%rbx)
	movdqu	32(%r12), %xmm2
	movups	%xmm2, 16(%rbx)
.L92:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	leaq	.LC8(%rip), %rdx
	movq	%rdx, %xmm0
	movabsq	$-9223372036854775808, %rdx
	movq	%rdx, -40(%rbp)
	punpcklqdq	%xmm0, %xmm0
	leaq	1(%rbx), %rdx
	movaps	%xmm0, -64(%rbp)
	cmpq	%rdx, %rcx
	jb	.L103
	jbe	.L93
	salq	$5, %rdx
	addq	%rax, %rdx
	cmpq	%rdx, %rsi
	je	.L93
	movq	%rdx, 72(%r12)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L101:
	movq	40(%r12), %rax
	cmpq	%rax, 24(%rbx)
	jne	.L96
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L103:
	leaq	-64(%rbp), %r8
	subq	%rcx, %rdx
	leaq	56(%r12), %rdi
	movq	%r8, %rcx
	call	_ZNSt6vectorIN2v88internal8compiler10NodeOriginENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	movq	64(%r12), %rax
	jmp	.L93
.L102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10200:
	.size	_ZN2v88internal8compiler15NodeOriginTable9Decorator8DecorateEPNS1_4NodeE, .-_ZN2v88internal8compiler15NodeOriginTable9Decorator8DecorateEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler15NodeOriginTable13SetNodeOriginEPNS1_4NodeERKNS1_10NodeOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler15NodeOriginTable13SetNodeOriginEPNS1_4NodeERKNS1_10NodeOriginE
	.type	_ZN2v88internal8compiler15NodeOriginTable13SetNodeOriginEPNS1_4NodeERKNS1_10NodeOriginE, @function
_ZN2v88internal8compiler15NodeOriginTable13SetNodeOriginEPNS1_4NodeERKNS1_10NodeOriginE:
.LFB10213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movl	20(%rsi), %ebx
	movq	72(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	movq	%rsi, %rcx
	andl	$16777215, %ebx
	subq	%rax, %rcx
	sarq	$5, %rcx
	cmpq	%rcx, %rbx
	jnb	.L112
.L105:
	salq	$5, %rbx
	addq	%rax, %rbx
	movq	8(%r12), %rax
	cmpq	%rax, 8(%rbx)
	je	.L113
.L108:
	movdqu	(%r12), %xmm1
	movups	%xmm1, (%rbx)
	movdqu	16(%r12), %xmm2
	movups	%xmm2, 16(%rbx)
.L104:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	%rdi, %r13
	leaq	.LC8(%rip), %rdi
	leaq	1(%rbx), %rdx
	movq	%rdi, %xmm0
	movabsq	$-9223372036854775808, %rdi
	punpcklqdq	%xmm0, %xmm0
	movq	%rdi, -56(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	%rdx, %rcx
	jb	.L115
	jbe	.L105
	salq	$5, %rdx
	addq	%rax, %rdx
	cmpq	%rdx, %rsi
	je	.L105
	movq	%rdx, 72(%r13)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L113:
	movq	24(%r12), %rax
	cmpq	%rax, 24(%rbx)
	jne	.L108
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L115:
	leaq	-80(%rbp), %r8
	subq	%rcx, %rdx
	leaq	56(%r13), %rdi
	movq	%r8, %rcx
	call	_ZNSt6vectorIN2v88internal8compiler10NodeOriginENS1_13ZoneAllocatorIS3_EEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS3_S6_EEmRKS3_
	movq	64(%r13), %rax
	jmp	.L105
.L114:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10213:
	.size	_ZN2v88internal8compiler15NodeOriginTable13SetNodeOriginEPNS1_4NodeERKNS1_10NodeOriginE, .-_ZN2v88internal8compiler15NodeOriginTable13SetNodeOriginEPNS1_4NodeERKNS1_10NodeOriginE
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal8compiler10NodeOrigin9PrintJsonERSo,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal8compiler10NodeOrigin9PrintJsonERSo, @function
_GLOBAL__sub_I__ZNK2v88internal8compiler10NodeOrigin9PrintJsonERSo:
.LFB11951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11951:
	.size	_GLOBAL__sub_I__ZNK2v88internal8compiler10NodeOrigin9PrintJsonERSo, .-_GLOBAL__sub_I__ZNK2v88internal8compiler10NodeOrigin9PrintJsonERSo
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal8compiler10NodeOrigin9PrintJsonERSo
	.weak	_ZTVN2v88internal8compiler15NodeOriginTable9DecoratorE
	.section	.data.rel.ro.local._ZTVN2v88internal8compiler15NodeOriginTable9DecoratorE,"awG",@progbits,_ZTVN2v88internal8compiler15NodeOriginTable9DecoratorE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler15NodeOriginTable9DecoratorE, @object
	.size	_ZTVN2v88internal8compiler15NodeOriginTable9DecoratorE, 40
_ZTVN2v88internal8compiler15NodeOriginTable9DecoratorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler15NodeOriginTable9DecoratorD1Ev
	.quad	_ZN2v88internal8compiler15NodeOriginTable9DecoratorD0Ev
	.quad	_ZN2v88internal8compiler15NodeOriginTable9Decorator8DecorateEPNS1_4NodeE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
