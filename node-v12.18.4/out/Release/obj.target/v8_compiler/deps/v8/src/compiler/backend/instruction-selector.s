	.file	"instruction-selector.cc"
	.text
	.section	.text._ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0, @function
_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0:
.LFB32592:
	.cfi_startproc
	cmpq	%rsi, %rdi
	je	.L13
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16(%rdi), %rbx
	subq	$24, %rsp
	cmpq	%rbx, %rsi
	jne	.L9
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	cmpq	%rbx, %r12
	je	.L5
	movl	$16, %eax
	subq	%r12, %rdx
	movq	%r12, %rsi
	movl	%ecx, -52(%rbp)
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
	movl	-52(%rbp), %ecx
.L5:
	addq	$16, %rbx
	movl	%r13d, (%r12)
	movl	%ecx, 4(%r12)
	movq	%r15, 8(%r12)
	cmpq	%rbx, %r14
	je	.L1
.L9:
	movl	(%rbx), %r13d
	movl	4(%rbx), %ecx
	movq	%rbx, %rdx
	movq	8(%rbx), %r15
	cmpl	(%r12), %r13d
	jl	.L17
	leaq	-16(%rbx), %rax
	cmpl	-16(%rbx), %r13d
	jge	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	movdqu	(%rax), %xmm0
	movq	%rax, %rdx
	subq	$16, %rax
	movups	%xmm0, 32(%rax)
	cmpl	(%rax), %r13d
	jl	.L8
.L7:
	addq	$16, %rbx
	movl	%r13d, (%rdx)
	movl	%ecx, 4(%rdx)
	movq	%r15, 8(%rdx)
	cmpq	%rbx, %r14
	jne	.L9
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE32592:
	.size	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0, .-_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	.section	.text._ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_T1_.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_T1_.isra.0, @function
_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_T1_.isra.0:
.LFB32601:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	subq	%rdi, %rax
	movq	%rax, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	sarq	$4, %rcx
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -96(%rbp)
	addq	%rax, %rdx
	movq	%rdi, -88(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rcx, -72(%rbp)
	cmpq	$96, %rax
	jle	.L19
	movq	%rdi, %r13
	movq	%rdx, %r12
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%r13, %rdi
	addq	$112, %r13
	movq	%r13, %rsi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	movq	%r14, %rax
	subq	%r13, %rax
	cmpq	$96, %rax
	jg	.L20
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	cmpq	$112, -112(%rbp)
	jle	.L18
	movq	%r14, -56(%rbp)
	movl	$7, %ebx
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	(%rbx,%rbx), %r13
	cmpq	%r13, -72(%rbp)
	jl	.L61
	movq	%rbx, %rax
	movq	%rbx, %rcx
	movq	-88(%rbp), %rsi
	movq	%r12, -80(%rbp)
	salq	$5, %rax
	salq	$4, %rcx
	movq	-96(%rbp), %rdi
	movq	%rbx, -104(%rbp)
	movq	%rax, -64(%rbp)
	leaq	(%rsi,%rcx), %r14
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L31:
	movq	-64(%rbp), %rax
	movq	%rbx, %r15
	leaq	(%rsi,%rax), %r12
	cmpq	%rbx, %r12
	jne	.L23
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L85:
	movdqu	(%r15), %xmm0
	addq	$16, %rdi
	addq	$16, %r15
	movups	%xmm0, -16(%rdi)
	cmpq	%rsi, %rbx
	je	.L84
.L27:
	cmpq	%r15, %r12
	je	.L24
.L23:
	movl	(%rsi), %eax
	cmpl	%eax, (%r15)
	jl	.L85
	movdqu	(%rsi), %xmm1
	addq	$16, %rsi
	addq	$16, %rdi
	movups	%xmm1, -16(%rdi)
	cmpq	%rsi, %rbx
	jne	.L27
.L84:
	xorl	%r14d, %r14d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%rbx, %r14
	subq	%rsi, %r14
	movq	%r14, %rdx
	call	memmove@PLT
	movq	%rax, %rdi
.L55:
	addq	%r14, %rdi
	movq	%r12, %r14
	subq	%r15, %r14
	cmpq	%r15, %r12
	je	.L29
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memmove@PLT
	movq	%rax, %rdi
.L29:
	movq	-56(%rbp), %r9
	addq	%r14, %rdi
	addq	-64(%rbp), %rbx
	subq	%r12, %r9
	movq	%r9, %rax
	sarq	$4, %rax
	cmpq	%rax, %r13
	jg	.L75
	movq	%r12, %rsi
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r12, %r15
	movq	-104(%rbp), %rbx
	movq	-80(%rbp), %r12
.L22:
	cmpq	%rbx, %rax
	cmovg	%rbx, %rax
	salq	$4, %rax
	leaq	(%r15,%rax), %rdx
	cmpq	%r15, %rdx
	je	.L64
	movq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L37:
	cmpq	%rsi, -56(%rbp)
	je	.L33
.L89:
	movl	(%r15), %eax
	cmpl	%eax, (%rsi)
	jge	.L34
	movdqu	(%rsi), %xmm2
	addq	$16, %rdi
	addq	$16, %rsi
	movups	%xmm2, -16(%rdi)
	cmpq	%r15, %rdx
	jne	.L37
.L36:
	movq	-56(%rbp), %r9
	subq	%rsi, %r9
.L32:
	subq	%r15, %rdx
	addq	%rdx, %rdi
	cmpq	%rsi, -56(%rbp)
	je	.L56
	movq	%r9, %rdx
	call	memmove@PLT
.L56:
	salq	$2, %rbx
	cmpq	%rbx, -72(%rbp)
	jl	.L65
.L90:
	movq	%rbx, %rax
	movq	-96(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r13, -104(%rbp)
	salq	$4, %rax
	salq	$4, %rcx
	movq	-88(%rbp), %rdi
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	leaq	(%rsi,%rcx), %r15
	leaq	(%rsi,%rax), %r14
	cmpq	%r15, %r14
	je	.L39
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%r15, %r13
	.p2align 4,,10
	.p2align 3
.L43:
	movl	(%rsi), %eax
	cmpl	%eax, 0(%r13)
	jge	.L40
	movdqu	0(%r13), %xmm4
	addq	$16, %r13
	addq	$16, %rdi
	movups	%xmm4, -16(%rdi)
	cmpq	%r13, %r14
	je	.L41
	cmpq	%r15, %rsi
	jne	.L43
.L41:
	movq	%r15, %rdx
	subq	%rsi, %rdx
	cmpq	%rsi, %r15
	je	.L44
.L58:
	movq	%rdx, -80(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %rdx
	movq	%rax, %rdi
.L44:
	addq	%rdx, %rdi
	movq	%r14, %rdx
	subq	%r13, %rdx
	cmpq	%r13, %r14
	je	.L45
	movq	%r13, %rsi
	movq	%rdx, -80(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %rdx
	addq	-64(%rbp), %r15
	movq	%rax, %rdi
	movq	%r12, %rax
	subq	%r14, %rax
	addq	%rdx, %rdi
	sarq	$4, %rax
	cmpq	%rax, %rbx
	jg	.L86
.L46:
	movq	-64(%rbp), %rax
	movq	%r14, %rsi
	leaq	(%rsi,%rax), %r14
	cmpq	%r15, %r14
	jne	.L87
.L39:
	movq	%r15, %rdx
	movq	%r15, %r13
	subq	%rsi, %rdx
	cmpq	%rsi, %r15
	jne	.L58
	addq	%rdx, %rdi
	movq	%r15, %r13
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%r12, %rax
	addq	%rdx, %rdi
	addq	-64(%rbp), %r15
	subq	%r13, %rax
	sarq	$4, %rax
	cmpq	%rax, %rbx
	jle	.L46
	movq	%r13, %r9
	movq	-104(%rbp), %r13
	movq	%r9, %r14
.L38:
	cmpq	%rax, %r13
	cmovg	%rax, %r13
	salq	$4, %r13
	addq	%r14, %r13
	movq	%r13, %r15
	cmpq	%r13, %r12
	jne	.L82
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L88:
	movdqu	(%r15), %xmm6
	addq	$16, %r15
	addq	$16, %rdi
	movups	%xmm6, -16(%rdi)
	cmpq	%r15, %r12
	je	.L48
.L82:
	cmpq	%r14, %r13
	je	.L48
.L51:
	movl	(%r14), %eax
	cmpl	%eax, (%r15)
	jl	.L88
	movdqu	(%r14), %xmm7
	addq	$16, %r14
	addq	$16, %rdi
	movups	%xmm7, -16(%rdi)
	cmpq	%r14, %r13
	je	.L48
	cmpq	%r15, %r12
	jne	.L51
.L48:
	movq	%r13, %rdx
	subq	%r14, %rdx
	cmpq	%r14, %r13
	je	.L52
	movq	%r14, %rsi
	movq	%rdx, -64(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %rdx
	movq	%rax, %rdi
.L52:
	cmpq	%r15, %r12
	je	.L53
	movq	%r12, %r8
	addq	%rdx, %rdi
	movq	%r15, %rsi
	subq	%r15, %r8
	movq	%r8, %rdx
	call	memmove@PLT
.L53:
	cmpq	%rbx, -72(%rbp)
	jg	.L21
.L18:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movdqu	(%rsi), %xmm5
	addq	$16, %rsi
	addq	$16, %rdi
	movups	%xmm5, -16(%rdi)
	cmpq	%r15, %rsi
	je	.L41
	cmpq	%r13, %r14
	jne	.L43
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L34:
	movdqu	(%r15), %xmm3
	addq	$16, %r15
	addq	$16, %rdi
	movups	%xmm3, -16(%rdi)
	cmpq	%r15, %rdx
	je	.L36
	cmpq	%rsi, -56(%rbp)
	jne	.L89
.L33:
	subq	%r15, %rdx
	movq	%r15, %rsi
	salq	$2, %rbx
	call	memmove@PLT
	cmpq	%rbx, -72(%rbp)
	jge	.L90
.L65:
	movq	-88(%rbp), %rdi
	movq	-72(%rbp), %rax
	movq	-96(%rbp), %r14
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L86:
	movq	-104(%rbp), %r13
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L61:
	movq	-112(%rbp), %r9
	movq	-96(%rbp), %rdi
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %r15
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%r15, %rsi
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L19:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	.cfi_endproc
.LFE32601:
	.size	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_T1_.isra.0, .-_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_T1_.isra.0
	.section	.text._ZN2v88internal8compiler12_GLOBAL__N_131GetFrameStateDescriptorInternalEPNS0_4ZoneEPNS1_4NodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8compiler12_GLOBAL__N_131GetFrameStateDescriptorInternalEPNS0_4ZoneEPNS1_4NodeE, @function
_ZN2v88internal8compiler12_GLOBAL__N_131GetFrameStateDescriptorInternalEPNS0_4ZoneEPNS1_4NodeE:
.LFB25212:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	32(%r14), %rbx
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16FrameStateInfoOfEPKNS1_8OperatorE@PLT
	movq	%rbx, %rdx
	movl	(%rax), %ecx
	movq	16(%rax), %r13
	movl	%ecx, -84(%rbp)
	movq	8(%rax), %rcx
	movzbl	23(%r14), %eax
	movq	%rcx, -96(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L92
	movq	32(%r14), %rax
	leaq	16(%rax), %rdx
.L92:
	movq	(%rdx), %rax
	leaq	-64(%rbp), %r15
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17StateValuesAccess4sizeEv@PLT
	movq	%rax, -72(%rbp)
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L93
	leaq	8(%rbx), %rax
.L94:
	movq	(%rax), %rax
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17StateValuesAccess4sizeEv@PLT
	movq	%rax, -80(%rbp)
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L95
	leaq	16(%rbx), %rax
.L96:
	movq	(%rax), %rax
	movq	%r15, %rdi
	addq	$40, %rbx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler17StateValuesAccess4sizeEv@PLT
	movq	%rax, %rdx
	movzbl	23(%r14), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L98
	movq	32(%r14), %rbx
	addq	$56, %rbx
.L98:
	movq	(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	(%rsi), %rax
	cmpw	$41, 16(%rax)
	je	.L110
.L99:
	xorl	%r10d, %r10d
	testq	%r13, %r13
	je	.L100
	movl	0(%r13), %r10d
	movq	16(%r13), %r13
.L100:
	movq	16(%r12), %rbx
	movq	24(%r12), %rax
	subq	%rbx, %rax
	cmpq	$127, %rax
	jbe	.L111
	leaq	128(%rbx), %rax
	movq	%rax, 16(%r12)
.L102:
	movslq	-80(%rbp), %rax
	movslq	%edx, %rdx
	pushq	%rcx
	movq	%r12, %rsi
	movslq	-72(%rbp), %r9
	movq	-96(%rbp), %r8
	pushq	%r13
	movq	%rbx, %rdi
	pushq	%rdx
	movl	-84(%rbp), %ecx
	movl	%r10d, %edx
	pushq	%rax
	call	_ZN2v88internal8compiler20FrameStateDescriptorC1EPNS0_4ZoneENS1_14FrameStateTypeENS0_9BailoutIdENS1_23OutputFrameStateCombineEmmmNS0_11MaybeHandleINS0_18SharedFunctionInfoEEEPS2_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L112
	leaq	-40(%rbp), %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movq	32(%r14), %rax
	addq	$32, %rax
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L93:
	movq	32(%r14), %rax
	addq	$24, %rax
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%r12, %rdi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler12_GLOBAL__N_131GetFrameStateDescriptorInternalEPNS0_4ZoneEPNS1_4NodeE
	movq	-104(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$128, %esi
	movq	%r12, %rdi
	movl	%r10d, -88(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rcx
	movl	-88(%rbp), %r10d
	movq	%rax, %rbx
	jmp	.L102
.L112:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25212:
	.size	_ZN2v88internal8compiler12_GLOBAL__N_131GetFrameStateDescriptorInternalEPNS0_4ZoneEPNS1_4NodeE, .-_ZN2v88internal8compiler12_GLOBAL__N_131GetFrameStateDescriptorInternalEPNS0_4ZoneEPNS1_4NodeE
	.section	.text._ZN2v88internal4Zone3NewEm,"axG",@progbits,_ZN2v88internal4Zone3NewEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4Zone3NewEm
	.type	_ZN2v88internal4Zone3NewEm, @function
_ZN2v88internal4Zone3NewEm:
.LFB5831:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	addq	$7, %rsi
	andq	$-8, %rsi
	subq	%r8, %rax
	cmpq	%rsi, %rax
	jb	.L115
	addq	%r8, %rsi
	movq	%r8, %rax
	movq	%rsi, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	jmp	_ZN2v88internal4Zone9NewExpandEm@PLT
	.cfi_endproc
.LFE5831:
	.size	_ZN2v88internal4Zone3NewEm, .-_ZN2v88internal4Zone3NewEm
	.section	.text._ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE, @function
_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE:
.LFB23610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r14
	movl	20(%rsi), %ecx
	movq	288(%r14), %rax
	movl	%ecx, %ebx
	andl	$16777215, %ebx
	movl	(%rax,%rbx,4), %eax
	cmpl	$-1, %eax
	je	.L119
.L117:
	movq	208(%r14), %rsi
	shrq	$6, %rbx
	movl	$1, %edx
	movl	%eax, %eax
	salq	%cl, %rdx
	salq	$3, %rax
	orq	%rdx, (%rsi,%rbx,8)
	popq	%rbx
	movabsq	$377957122049, %rdx
	popq	%r12
	orq	%rdx, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movq	%rdi, %r13
	movq	16(%r14), %rdi
	movq	%rsi, %r12
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%r14), %rdx
	movl	%eax, (%rdx,%rbx,4)
	movl	20(%r12), %ecx
	movq	0(%r13), %r14
	movl	%ecx, %ebx
	andl	$16777215, %ebx
	jmp	.L117
	.cfi_endproc
.LFE23610:
	.size	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE, .-_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE,comdat
	.p2align 4
	.weak	_ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE
	.type	_ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE, @function
_ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE:
.LFB23637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdi), %eax
	cmpw	$60, %ax
	ja	.L121
	cmpw	$22, %ax
	jbe	.L122
	subl	$23, %eax
	cmpw	$37, %ax
	ja	.L122
	leaq	.L124(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE,"aG",@progbits,_ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE,comdat
	.align 4
	.align 4
.L124:
	.long	.L132-.L124
	.long	.L134-.L124
	.long	.L130-.L124
	.long	.L128-.L124
	.long	.L129-.L124
	.long	.L128-.L124
	.long	.L122-.L124
	.long	.L127-.L124
	.long	.L126-.L124
	.long	.L125-.L124
	.long	.L125-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L122-.L124
	.long	.L123-.L124
	.section	.text._ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE,comdat
	.p2align 4,,10
	.p2align 3
.L121:
	cmpw	$284, %ax
	je	.L133
	cmpw	$428, %ax
	jne	.L122
.L134:
	movabsq	$81604378625, %r8
	movq	48(%rdi), %rdx
	.p2align 4,,10
	.p2align 3
.L135:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L142
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%r8, %rax
	ret
.L132:
	.cfi_restore_state
	movl	44(%rdi), %esi
	leaq	-32(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-32(%rbp), %r8
	movq	-24(%rbp), %rdx
	jmp	.L135
.L130:
	movabsq	$81604378626, %r8
	movslq	44(%rdi), %rdx
	jmp	.L135
.L128:
	movabsq	$81604378627, %r8
	movq	48(%rdi), %rdx
	jmp	.L135
.L129:
	movabsq	$81604378628, %r8
	movq	48(%rdi), %rdx
	jmp	.L135
.L127:
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	movabsq	$81604378630, %r8
	movq	%rax, %rdx
	jmp	.L135
.L126:
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	movabsq	$81604378629, %r8
	movq	%rax, %rdx
	jmp	.L135
.L125:
	movq	56(%rdi), %rdx
	movq	48(%rdi), %rsi
	leaq	-32(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1ENS1_26RelocatablePtrConstantInfoE@PLT
	movq	-32(%rbp), %r8
	movq	-24(%rbp), %rdx
	jmp	.L135
.L123:
	call	_ZN2v88internal8compiler25DeadValueRepresentationOfEPKNS1_8OperatorE@PLT
	cmpb	$13, %al
	ja	.L122
	leaq	.L137(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE,"aG",@progbits,_ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE,comdat
	.align 4
	.align 4
.L137:
	.long	.L122-.L137
	.long	.L139-.L137
	.long	.L122-.L137
	.long	.L122-.L137
	.long	.L139-.L137
	.long	.L122-.L137
	.long	.L139-.L137
	.long	.L139-.L137
	.long	.L139-.L137
	.long	.L139-.L137
	.long	.L139-.L137
	.long	.L139-.L137
	.long	.L138-.L137
	.long	.L136-.L137
	.section	.text._ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE,comdat
	.p2align 4,,10
	.p2align 3
.L133:
	call	_ZN2v88internal8compiler20StringConstantBaseOfEPKNS1_8OperatorE@PLT
	movabsq	$81604378632, %r8
	movq	%rax, %rdx
	jmp	.L135
.L122:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L139:
	leaq	-32(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movq	-32(%rbp), %r8
	movq	-24(%rbp), %rdx
	jmp	.L135
.L136:
	movabsq	$81604378627, %r8
	xorl	%edx, %edx
	jmp	.L135
.L138:
	movabsq	$81604378626, %r8
	xorl	%edx, %edx
	jmp	.L135
.L142:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23637:
	.size	_ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE, .-_ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler19InstructionSelectorC2EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.rodata._ZN2v88internal8compiler19InstructionSelectorC2EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::reserve"
	.section	.text._ZN2v88internal8compiler19InstructionSelectorC2EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelectorC2EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE
	.type	_ZN2v88internal8compiler19InstructionSelectorC2EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE, @function
_ZN2v88internal8compiler19InstructionSelectorC2EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE:
.LFB25005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm1
	movq	%rsi, %xmm0
	xorl	%ecx, %ecx
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	56(%rbp), %eax
	movq	%rsi, 56(%rdi)
	movq	$0, 64(%rdi)
	movl	%eax, 32(%rdi)
	movl	64(%rbp), %eax
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movl	%eax, 36(%rdi)
	movq	8(%r8), %rax
	movups	%xmm0, (%rdi)
	movq	%r8, %xmm0
	movq	%rax, 88(%rdi)
	movhps	16(%rbp), %xmm0
	movq	$0, 96(%rdi)
	movq	$0, 104(%rdi)
	movq	$0, 112(%rdi)
	movq	8(%r8), %rax
	movq	%r9, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	%rax, 120(%rdi)
	movq	$0, 128(%rdi)
	movq	$0, 136(%rdi)
	movq	$0, 144(%rdi)
	movq	%rsi, 152(%rdi)
	movq	$0, 160(%rdi)
	movups	%xmm0, 16(%rdi)
	movl	$0, 168(%rdi)
	movq	$0, 176(%rdi)
	movl	$0, 184(%rdi)
	movq	$0, 192(%rdi)
	testq	%rdx, %rdx
	jne	.L266
.L148:
	movq	%r12, 200(%rbx)
	movq	$0, 208(%rbx)
	movl	$0, 216(%rbx)
	movq	$0, 224(%rbx)
	movl	$0, 232(%rbx)
	movq	$0, 240(%rbx)
	testq	%r13, %r13
	jne	.L267
	movq	%rcx, %rax
	xorl	%edi, %edi
	testq	%r15, %r15
	jns	.L268
.L218:
	addq	$64, %r15
	subq	$8, %rax
.L152:
	movq	%rax, 224(%rbx)
	movl	%r15d, 232(%rbx)
	testq	%rdi, %rdi
	je	.L153
	movq	240(%rbx), %rdx
	xorl	%esi, %esi
	subq	%rdi, %rdx
	call	memset@PLT
.L153:
	cmpq	$536870911, %r13
	ja	.L269
.L154:
	movq	%r12, 248(%rbx)
	leaq	0(,%r13,4), %rdx
	movq	$0, 256(%rbx)
	movq	$0, 264(%rbx)
	movq	$0, 272(%rbx)
	testq	%r13, %r13
	je	.L155
	movq	16(%r12), %rdi
	movq	24(%r12), %rax
	leaq	7(%rdx), %r15
	andq	$-8, %r15
	subq	%rdi, %rax
	cmpq	%rax, %r15
	ja	.L270
	leaq	(%r15,%rdi), %rax
	movq	%rax, 16(%r12)
.L157:
	leaq	(%rdi,%rdx), %rcx
	movq	%rdi, 256(%rbx)
	xorl	%esi, %esi
	movq	%rcx, 272(%rbx)
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	memset@PLT
	movq	-64(%rbp), %rcx
	movq	%r12, 280(%rbx)
	movq	$0, 288(%rbx)
	movq	-56(%rbp), %rdx
	movq	%rcx, 264(%rbx)
	movq	$0, 296(%rbx)
	movq	$0, 304(%rbx)
	movq	16(%r12), %rdi
	movq	24(%r12), %rax
	subq	%rdi, %rax
	cmpq	%rax, %r15
	ja	.L271
	addq	%rdi, %r15
	movq	%r15, 16(%r12)
.L159:
	leaq	(%rdi,%rdx), %r15
	movq	%rdi, 288(%rbx)
	movl	$255, %esi
	movq	%r15, 304(%rbx)
	call	memset@PLT
.L217:
	movl	72(%rbp), %eax
	movq	%r15, 296(%rbx)
	movq	%r12, 312(%rbx)
	movq	40(%rbp), %xmm0
	movl	%eax, 352(%rbx)
	movl	80(%rbp), %eax
	movq	$0, 320(%rbx)
	movhps	48(%rbp), %xmm0
	movl	%eax, 356(%rbx)
	movl	32(%rbp), %eax
	movq	$0, 328(%rbx)
	movl	%eax, 360(%rbx)
	movl	88(%rbp), %eax
	movq	$0, 336(%rbx)
	movl	%eax, 364(%rbx)
	movq	24(%rbp), %rax
	movq	$0, 344(%rbx)
	movq	%rax, 368(%rbx)
	movq	8(%r14), %rax
	movb	$0, 376(%rbx)
	movq	%rax, 384(%rbx)
	movl	96(%rbp), %eax
	movq	$0, 392(%rbx)
	movq	$0, 400(%rbx)
	movq	$0, 408(%rbx)
	movl	%eax, 416(%rbx)
	movups	%xmm0, 424(%rbx)
	cmpq	$268435455, %r13
	ja	.L272
	movq	64(%rbx), %r12
	movq	80(%rbx), %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rax, %r13
	ja	.L273
.L161:
	movq	96(%rbx), %r12
	movq	112(%rbx), %rax
	subq	%r12, %rax
	cmpq	$39, %rax
	jbe	.L274
.L172:
	movq	128(%rbx), %r12
	movq	144(%rbx), %rax
	subq	%r12, %rax
	cmpq	$15, %rax
	jbe	.L275
	cmpl	$1, 416(%rbx)
	je	.L276
.L143:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movq	%rcx, 224(%rbx)
	movl	%r15d, 232(%rbx)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L267:
	movq	16(%r12), %rdi
	movq	24(%r12), %rax
	leaq	63(%r13), %rdx
	shrq	$6, %rdx
	salq	$3, %rdx
	subq	%rdi, %rax
	movq	%rdx, %rsi
	cmpq	%rax, %rdx
	ja	.L277
	addq	%rdi, %rsi
	movq	%rsi, 16(%r12)
.L151:
	addq	%rdi, %rdx
	movq	%rdi, 208(%rbx)
	leaq	(%rdi,%rcx), %rax
	movq	%rdx, 240(%rbx)
	movl	$0, 216(%rbx)
	testq	%r15, %r15
	jns	.L152
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L266:
	leaq	63(%rdx), %r15
	movq	16(%r12), %rdi
	movq	24(%r12), %rax
	movq	%r15, %rdx
	shrq	$6, %rdx
	subq	%rdi, %rax
	salq	$3, %rdx
	movq	%rdx, %rsi
	cmpq	%rax, %rdx
	ja	.L278
	addq	%rdi, %rsi
	movq	%rsi, 16(%r12)
.L146:
	addq	%rdi, %rdx
	testq	%r13, %r13
	movq	%r13, %rax
	movq	%rdi, 160(%rbx)
	cmovns	%r13, %r15
	sarq	$63, %rax
	movq	%rdx, 192(%rbx)
	movl	$0, 168(%rbx)
	shrq	$58, %rax
	sarq	$6, %r15
	leaq	0(,%r15,8), %rcx
	leaq	0(%r13,%rax), %r15
	andl	$63, %r15d
	leaq	(%rdi,%rcx), %rdx
	subq	%rax, %r15
	jns	.L220
	leaq	64(%r15), %rax
	subq	$8, %rdx
.L147:
	movq	%rdx, 176(%rbx)
	movl	%eax, 184(%rbx)
	testq	%rdi, %rdi
	je	.L148
	movq	192(%rbx), %rdx
	xorl	%esi, %esi
	movq	%rcx, -56(%rbp)
	subq	%rdi, %rdx
	call	memset@PLT
	movq	-56(%rbp), %rcx
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L275:
	movq	120(%rbx), %rdi
	movq	136(%rbx), %r14
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	movq	%r14, %r15
	subq	%r12, %r15
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L279
	leaq	16(%rax), %rsi
	movq	%rsi, 16(%rdi)
.L185:
	cmpq	%r14, %r12
	je	.L190
	subq	$8, %r14
	leaq	15(%r12), %rdx
	subq	%r12, %r14
	subq	%rax, %rdx
	movq	%r14, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L187
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rdi
	je	.L187
	addq	$1, %rdi
	movq	%r12, %r8
	movq	%rax, %rdx
	movq	%rdi, %rcx
	subq	%rax, %r8
	shrq	%rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L188:
	movdqu	(%rdx,%r8), %xmm4
	addq	$16, %rdx
	movups	%xmm4, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L188
	movq	%rdi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %r12
	addq	%rax, %rcx
	cmpq	%rdx, %rdi
	je	.L190
	movq	(%r12), %rdx
	movq	%rdx, (%rcx)
.L190:
	movq	%rax, 128(%rbx)
	addq	%r15, %rax
	cmpl	$1, 416(%rbx)
	movq	%rax, 136(%rbx)
	movq	%rsi, 144(%rbx)
	jne	.L143
.L276:
	movq	392(%rbx), %rax
	movq	408(%rbx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r13
	ja	.L280
	movq	400(%rbx), %rsi
	movq	%rsi, %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	cmpq	%rcx, %r13
	jbe	.L201
	cmpq	%rsi, %rax
	je	.L208
	leaq	-8(%rsi), %rdi
	movq	%rax, %rdx
	movq	%rdi, %r8
	subq	%rax, %r8
	shrq	$3, %r8
	addq	$1, %r8
	cmpq	%rax, %rdi
	je	.L205
	movq	%r8, %rdi
	movdqa	.LC3(%rip), %xmm0
	shrq	%rdi
	salq	$4, %rdi
	addq	%rax, %rdi
	.p2align 4,,10
	.p2align 3
.L207:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L207
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	(%rax,%rdx,8), %rax
	cmpq	%r8, %rdx
	je	.L208
.L205:
	movl	$4294967295, %edi
	movq	%rdi, (%rax)
.L208:
	subq	%rcx, %r13
	cmpq	$1, %r13
	je	.L225
	leaq	-2(%r13), %rcx
	movdqa	.LC3(%rip), %xmm0
	xorl	%eax, %eax
	shrq	%rcx
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L204:
	movq	%rax, %rdi
	addq	$1, %rax
	salq	$4, %rdi
	movups	%xmm0, (%rsi,%rdi)
	cmpq	%rcx, %rax
	jb	.L204
	leaq	(%rcx,%rcx), %rax
	salq	$4, %rcx
	addq	%rsi, %rcx
	cmpq	%rax, %r13
	je	.L210
.L203:
	movl	$4294967295, %eax
	movq	%rax, (%rcx)
.L210:
	leaq	(%rsi,%r13,8), %rax
	movq	%rax, 400(%rbx)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L274:
	movq	88(%rbx), %rdi
	movq	104(%rbx), %r14
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	movq	%r14, %r15
	subq	%r12, %r15
	subq	%rax, %rdx
	cmpq	$39, %rdx
	jbe	.L281
	leaq	40(%rax), %rsi
	movq	%rsi, 16(%rdi)
.L174:
	cmpq	%r14, %r12
	je	.L179
	subq	$8, %r14
	leaq	15(%r12), %rdx
	subq	%r12, %r14
	subq	%rax, %rdx
	movq	%r14, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L176
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rdi
	je	.L176
	addq	$1, %rdi
	movq	%r12, %r8
	movq	%rax, %rdx
	movq	%rdi, %rcx
	subq	%rax, %r8
	shrq	%rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L177:
	movdqu	(%rdx,%r8), %xmm3
	addq	$16, %rdx
	movups	%xmm3, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L177
	movq	%rdi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %r12
	addq	%rax, %rcx
	cmpq	%rdx, %rdi
	je	.L179
	movq	(%r12), %rdx
	movq	%rdx, (%rcx)
.L179:
	movq	%rax, 96(%rbx)
	addq	%r15, %rax
	movq	%rax, 104(%rbx)
	movq	%rsi, 112(%rbx)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L273:
	movq	72(%rbx), %r14
	leaq	0(,%r13,8), %r8
	xorl	%eax, %eax
	movq	%r14, %r15
	subq	%r12, %r15
	testq	%r13, %r13
	je	.L162
	movq	56(%rbx), %rdi
	movq	%r8, %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %r8
	ja	.L282
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L162:
	cmpq	%r14, %r12
	je	.L169
	leaq	-8(%r14), %rdx
	leaq	15(%r12), %rcx
	subq	%r12, %rdx
	subq	%rax, %rcx
	shrq	$3, %rdx
	cmpq	$30, %rcx
	jbe	.L222
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdx
	je	.L222
	leaq	1(%rdx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L167:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L167
	movq	%rcx, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r12
	addq	%rax, %rsi
	cmpq	%rdx, %rcx
	je	.L169
	movq	(%r12), %rdx
	movq	%rdx, (%rsi)
.L169:
	addq	%rax, %r15
	movq	%rax, 64(%rbx)
	addq	%r8, %rax
	movq	%r15, 72(%rbx)
	movq	%rax, 80(%rbx)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L280:
	movq	384(%rbx), %rdi
	leaq	0(,%r13,8), %r12
	testq	%r13, %r13
	je	.L223
	movq	%r12, %rsi
	call	_ZN2v88internal4Zone3NewEm
	leaq	(%rax,%r12), %rsi
	movq	%rax, %rdx
	cmpq	$1, %r13
	je	.L197
	movq	%r13, %rcx
	movdqa	.LC3(%rip), %xmm0
	shrq	%rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L198:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L198
	movq	%r13, %rcx
	andq	$-2, %rcx
	leaq	(%rax,%rcx,8), %rdx
	cmpq	%rcx, %r13
	je	.L196
.L197:
	movl	$4294967295, %ecx
	movq	%rcx, (%rdx)
.L196:
	movq	%rax, %xmm0
	movq	%rsi, %xmm5
	movq	%rsi, 408(%rbx)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 392(%rbx)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%r12, 280(%rbx)
	xorl	%r15d, %r15d
	movq	$0, 288(%rbx)
	movq	$0, 304(%rbx)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L201:
	testq	%r13, %r13
	je	.L212
	movq	%rax, %rdx
	cmpq	$1, %r13
	je	.L213
	movq	%r13, %rcx
	movdqa	.LC3(%rip), %xmm0
	shrq	%rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L215:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L215
	movq	%r13, %rcx
	andq	$-2, %rcx
	leaq	(%rax,%rcx,8), %rdx
	cmpq	%rcx, %r13
	je	.L216
.L213:
	movl	$4294967295, %ecx
	movq	%rcx, (%rdx)
.L216:
	leaq	(%rax,%r13,8), %rax
.L212:
	cmpq	%rax, %rsi
	je	.L143
	movq	%rax, 400(%rbx)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L176:
	leaq	8(%r14,%rax), %rdi
	movq	%rax, %rdx
	subq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L181:
	movq	(%rdx,%r12), %rcx
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rdi, %rdx
	jne	.L181
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L222:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L166:
	movq	(%r12,%rcx,8), %rsi
	movq	%rsi, (%rax,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rsi, %rdx
	jne	.L166
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	8(%rax,%r14), %rdi
	movq	%rax, %rdx
	subq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L192:
	movq	(%rdx,%r12), %rcx
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rdx, %rdi
	jne	.L192
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L271:
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L270:
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L223:
	xorl	%eax, %eax
	xorl	%esi, %esi
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%r12, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L279:
	movl	$16, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	16(%rax), %rsi
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L281:
	movl	$40, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	leaq	40(%rax), %rsi
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L282:
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	jmp	.L162
.L225:
	movq	%rsi, %rcx
	jmp	.L203
.L272:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L269:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L220:
	movq	%r15, %rax
	jmp	.L147
	.cfi_endproc
.LFE25005:
	.size	_ZN2v88internal8compiler19InstructionSelectorC2EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE, .-_ZN2v88internal8compiler19InstructionSelectorC2EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE
	.globl	_ZN2v88internal8compiler19InstructionSelectorC1EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE
	.set	_ZN2v88internal8compiler19InstructionSelectorC1EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE,_ZN2v88internal8compiler19InstructionSelectorC2EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE
	.section	.text._ZN2v88internal8compiler19InstructionSelector10StartBlockENS1_9RpoNumberE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector10StartBlockENS1_9RpoNumberE
	.type	_ZN2v88internal8compiler19InstructionSelector10StartBlockENS1_9RpoNumberE, @function
_ZN2v88internal8compiler19InstructionSelector10StartBlockENS1_9RpoNumberE:
.LFB25008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpl	$1, 352(%rdi)
	je	.L289
.L284:
	movq	16(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSequence10StartBlockENS1_9RpoNumberE@PLT
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movl	%esi, -20(%rbp)
	call	_ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv@PLT
	movl	-20(%rbp), %esi
	testb	%al, %al
	je	.L284
	movq	344(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20InstructionScheduler10StartBlockENS1_9RpoNumberE@PLT
	.cfi_endproc
.LFE25008:
	.size	_ZN2v88internal8compiler19InstructionSelector10StartBlockENS1_9RpoNumberE, .-_ZN2v88internal8compiler19InstructionSelector10StartBlockENS1_9RpoNumberE
	.section	.text._ZN2v88internal8compiler19InstructionSelector8EndBlockENS1_9RpoNumberE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector8EndBlockENS1_9RpoNumberE
	.type	_ZN2v88internal8compiler19InstructionSelector8EndBlockENS1_9RpoNumberE, @function
_ZN2v88internal8compiler19InstructionSelector8EndBlockENS1_9RpoNumberE:
.LFB25009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpl	$1, 352(%rdi)
	je	.L296
.L291:
	movq	16(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSequence8EndBlockENS1_9RpoNumberE@PLT
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	movl	%esi, -20(%rbp)
	call	_ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv@PLT
	movl	-20(%rbp), %esi
	testb	%al, %al
	je	.L291
	movq	344(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20InstructionScheduler8EndBlockENS1_9RpoNumberE@PLT
	.cfi_endproc
.LFE25009:
	.size	_ZN2v88internal8compiler19InstructionSelector8EndBlockENS1_9RpoNumberE, .-_ZN2v88internal8compiler19InstructionSelector8EndBlockENS1_9RpoNumberE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13AddTerminatorEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13AddTerminatorEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler19InstructionSelector13AddTerminatorEPNS1_11InstructionE, @function
_ZN2v88internal8compiler19InstructionSelector13AddTerminatorEPNS1_11InstructionE:
.LFB25010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpl	$1, 352(%rdi)
	je	.L303
.L298:
	movq	16(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSequence14AddInstructionEPNS1_11InstructionE@PLT
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv@PLT
	movq	-24(%rbp), %rsi
	testb	%al, %al
	je	.L298
	movq	344(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20InstructionScheduler13AddTerminatorEPNS1_11InstructionE@PLT
	.cfi_endproc
.LFE25010:
	.size	_ZN2v88internal8compiler19InstructionSelector13AddTerminatorEPNS1_11InstructionE, .-_ZN2v88internal8compiler19InstructionSelector13AddTerminatorEPNS1_11InstructionE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14AddInstructionEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14AddInstructionEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler19InstructionSelector14AddInstructionEPNS1_11InstructionE, @function
_ZN2v88internal8compiler19InstructionSelector14AddInstructionEPNS1_11InstructionE:
.LFB25011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpl	$1, 352(%rdi)
	je	.L310
.L305:
	movq	16(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSequence14AddInstructionEPNS1_11InstructionE@PLT
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv@PLT
	movq	-24(%rbp), %rsi
	testb	%al, %al
	je	.L305
	movq	344(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler20InstructionScheduler14AddInstructionEPNS1_11InstructionE@PLT
	.cfi_endproc
.LFE25011:
	.size	_ZN2v88internal8compiler19InstructionSelector14AddInstructionEPNS1_11InstructionE, .-_ZN2v88internal8compiler19InstructionSelector14AddInstructionEPNS1_11InstructionE
	.section	.text._ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_
	.type	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_, @function
_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_:
.LFB25021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	40(%rdi), %rdi
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	40(%r12), %rdi
	movq	%rbx, %rsi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	%rax, %r8
	xorl	%eax, %eax
	cmpq	%r8, %r14
	je	.L335
.L311:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movq	0(%r13), %rdx
	movzbl	18(%rdx), %edx
	andl	$124, %edx
	cmpb	$124, %dl
	je	.L336
	movl	20(%r13), %ecx
	movl	20(%rbx), %edx
	movq	256(%r12), %rsi
	andl	$16777215, %edx
	andl	$16777215, %ecx
	movl	(%rsi,%rdx,4), %edi
	cmpl	%edi, (%rsi,%rcx,4)
	jne	.L311
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L315
	movq	(%rdi), %r12
	.p2align 4,,10
	.p2align 3
.L319:
	movl	16(%rdi), %ecx
	movl	%ecx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rdx
	salq	$3, %rax
	andl	$1, %ecx
	leaq	(%rdi,%rdx,8), %rdx
	leaq	32(%rdx,%rax), %rsi
	jne	.L317
	leaq	16(%rdx,%rax), %rsi
	movq	(%rdx), %rdx
.L317:
	cmpq	%rdx, %rbx
	je	.L318
	call	_ZN2v88internal8compiler14NodeProperties11IsValueEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	jne	.L337
.L318:
	testq	%r12, %r12
	je	.L315
	movq	%r12, %rdi
	movq	(%r12), %r12
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L315:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	movq	24(%r13), %rdx
	testq	%rdx, %rdx
	je	.L311
	movl	16(%rdx), %ecx
	movl	%ecx, %eax
	shrl	%eax
	andl	$1, %ecx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,8), %rsi
	jne	.L314
	movq	(%rsi), %rsi
.L314:
	xorl	%eax, %eax
	cmpq	%rsi, %rbx
	jne	.L311
	cmpq	$0, (%rdx)
	sete	%al
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L337:
	xorl	%eax, %eax
	jmp	.L311
	.cfi_endproc
.LFE25021:
	.size	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_, .-_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_
	.section	.text._ZNK2v88internal8compiler19InstructionSelector20CanCoverTransitivelyEPNS1_4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19InstructionSelector20CanCoverTransitivelyEPNS1_4NodeES4_S4_
	.type	_ZNK2v88internal8compiler19InstructionSelector20CanCoverTransitivelyEPNS1_4NodeES4_S4_, @function
_ZNK2v88internal8compiler19InstructionSelector20CanCoverTransitivelyEPNS1_4NodeES4_S4_:
.LFB25022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %rdi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	40(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	cmpq	%rax, %r15
	je	.L339
.L369:
	xorl	%eax, %eax
.L338:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	movq	(%r14), %rax
	movzbl	18(%rax), %eax
	andl	$124, %eax
	cmpb	$124, %al
	je	.L370
	movl	20(%r14), %edx
	movl	20(%r12), %eax
	movq	256(%rbx), %rcx
	andl	$16777215, %eax
	andl	$16777215, %edx
	movl	(%rcx,%rax,4), %eax
	cmpl	%eax, (%rcx,%rdx,4)
	jne	.L369
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.L344
	movq	(%rdi), %r15
	.p2align 4,,10
	.p2align 3
.L349:
	movl	16(%rdi), %ecx
	movl	%ecx, %eax
	shrl	%eax
	leaq	3(%rax,%rax,2), %rdx
	salq	$3, %rax
	andl	$1, %ecx
	leaq	(%rdi,%rdx,8), %rdx
	leaq	32(%rdx,%rax), %rsi
	jne	.L347
	leaq	16(%rdx,%rax), %rsi
	movq	(%rdx), %rdx
.L347:
	cmpq	%rdx, %r12
	je	.L348
	call	_ZN2v88internal8compiler14NodeProperties11IsValueEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	jne	.L369
.L348:
	testq	%r15, %r15
	je	.L344
	movq	%r15, %rdi
	movq	(%r15), %r15
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L370:
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L369
	movl	16(%rax), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %ecx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	jne	.L343
	movq	(%rdx), %rdx
.L343:
	cmpq	%rdx, %r12
	jne	.L369
	cmpq	$0, (%rax)
	jne	.L369
	.p2align 4,,10
	.p2align 3
.L344:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler19InstructionSelector8CanCoverEPNS1_4NodeES4_
	testb	%al, %al
	je	.L369
	movq	(%r14), %rdx
	movzbl	18(%rdx), %edx
	andl	$124, %edx
	cmpb	$124, %dl
	jne	.L338
	movq	0(%r13), %rdx
	movzbl	18(%rdx), %edx
	andl	$124, %edx
	cmpb	$124, %dl
	je	.L338
	movl	20(%r13), %edx
	movl	20(%r12), %eax
	movq	256(%rbx), %rcx
	andl	$16777215, %eax
	andl	$16777215, %edx
	movl	(%rcx,%rax,4), %eax
	cmpl	%eax, (%rcx,%rdx,4)
	sete	%al
	jmp	.L338
	.cfi_endproc
.LFE25022:
	.size	_ZNK2v88internal8compiler19InstructionSelector20CanCoverTransitivelyEPNS1_4NodeES4_S4_, .-_ZNK2v88internal8compiler19InstructionSelector20CanCoverTransitivelyEPNS1_4NodeES4_S4_
	.section	.text._ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_
	.type	_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_, @function
_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_:
.LFB25023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	40(%rdi), %rdi
	movq	%rsi, %rbx
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	movq	40(%r14), %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	cmpq	%rax, %r12
	je	.L384
.L372:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	movq	24(%r13), %rcx
	testq	%rcx, %rcx
	je	.L373
	movq	(%rcx), %r13
	.p2align 4,,10
	.p2align 3
.L378:
	movl	16(%rcx), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%rcx,%rax,8), %rsi
	jne	.L374
	movq	(%rsi), %rsi
.L374:
	cmpq	%rsi, %rbx
	je	.L377
	movq	40(%r14), %rdi
	call	_ZNK2v88internal8compiler8Schedule5blockEPNS1_4NodeE@PLT
	cmpq	%rax, %r12
	je	.L372
.L377:
	testq	%r13, %r13
	je	.L373
	movq	%r13, %rcx
	movq	0(%r13), %r13
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L373:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25023:
	.size	_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_, .-_ZNK2v88internal8compiler19InstructionSelector27IsOnlyUserOfNodeInSameBlockEPNS1_4NodeES4_
	.section	.text._ZN2v88internal8compiler19InstructionSelector13UpdateRenamesEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13UpdateRenamesEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler19InstructionSelector13UpdateRenamesEPNS1_11InstructionE, @function
_ZN2v88internal8compiler19InstructionSelector13UpdateRenamesEPNS1_11InstructionE:
.LFB25024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	4(%rsi), %ebx
	testl	$16776960, %ebx
	je	.L385
	movabsq	$-34359738361, %r9
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L386:
	movzbl	%bl, %eax
	leaq	5(%r8,%rax), %rax
	leaq	(%rsi,%rax,8), %r11
	movq	(%r11), %rdx
	movl	%edx, %eax
	andl	$7, %eax
	cmpl	$1, %eax
	jne	.L387
	movq	320(%rdi), %r12
	movq	328(%rdi), %rcx
	movq	%rdx, %r13
	shrq	$3, %r13
	subq	%r12, %rcx
	movl	%r13d, %eax
	sarq	$2, %rcx
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L396:
	movl	(%r12,%r10,4), %r10d
	cmpl	$-1, %r10d
	je	.L388
	movl	%r10d, %eax
.L389:
	movslq	%eax, %r10
	cmpq	%rcx, %r10
	jb	.L396
.L388:
	cmpl	%eax, %r13d
	je	.L387
	andq	%r9, %rdx
	movq	%rdx, %rcx
	movl	%eax, %edx
	leaq	0(,%rdx,8), %rax
	orq	%rcx, %rax
	movq	%rax, (%r11)
	movl	4(%rsi), %ebx
.L387:
	movl	%ebx, %eax
	addq	$1, %r8
	shrl	$8, %eax
	movzwl	%ax, %eax
	cmpq	%rax, %r8
	jb	.L386
.L385:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25024:
	.size	_ZN2v88internal8compiler19InstructionSelector13UpdateRenamesEPNS1_11InstructionE, .-_ZN2v88internal8compiler19InstructionSelector13UpdateRenamesEPNS1_11InstructionE
	.section	.text._ZN2v88internal8compiler19InstructionSelector18UpdateRenamesInPhiEPNS1_14PhiInstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector18UpdateRenamesInPhiEPNS1_14PhiInstructionE
	.type	_ZN2v88internal8compiler19InstructionSelector18UpdateRenamesInPhiEPNS1_14PhiInstructionE, @function
_ZN2v88internal8compiler19InstructionSelector18UpdateRenamesInPhiEPNS1_14PhiInstructionE:
.LFB25025:
	.cfi_startproc
	endbr64
	movq	24(%rsi), %r8
	cmpq	%r8, 32(%rsi)
	je	.L407
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
.L410:
	movq	320(%r12), %rsi
	movq	328(%r12), %r9
.L403:
	movl	(%r8,%rbx,4), %edi
	movq	%r9, %rcx
	subq	%rsi, %rcx
	sarq	$2, %rcx
	movl	%edi, %edx
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L411:
	movl	(%rsi,%rax,4), %eax
	cmpl	$-1, %eax
	je	.L399
	movl	%eax, %edx
.L400:
	movslq	%edx, %rax
	cmpq	%rcx, %rax
	jb	.L411
.L399:
	cmpl	%edx, %edi
	je	.L401
	movq	%rbx, %rsi
	movq	%r13, %rdi
	addq	$1, %rbx
	call	_ZN2v88internal8compiler14PhiInstruction11RenameInputEmi@PLT
	movq	24(%r13), %r8
	movq	32(%r13), %rax
	subq	%r8, %rax
	sarq	$2, %rax
	cmpq	%rax, %rbx
	jb	.L410
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	movq	32(%r13), %rax
	addq	$1, %rbx
	subq	%r8, %rax
	sarq	$2, %rax
	cmpq	%rbx, %rax
	ja	.L403
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L407:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE25025:
	.size	_ZN2v88internal8compiler19InstructionSelector18UpdateRenamesInPhiEPNS1_14PhiInstructionE, .-_ZN2v88internal8compiler19InstructionSelector18UpdateRenamesInPhiEPNS1_14PhiInstructionE
	.section	.text._ZN2v88internal8compiler19InstructionSelector9GetRenameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector9GetRenameEi
	.type	_ZN2v88internal8compiler19InstructionSelector9GetRenameEi, @function
_ZN2v88internal8compiler19InstructionSelector9GetRenameEi:
.LFB25026:
	.cfi_startproc
	endbr64
	movq	328(%rdi), %rcx
	movl	%esi, %eax
	movq	320(%rdi), %rsi
	subq	%rsi, %rcx
	sarq	$2, %rcx
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L416:
	movl	(%rsi,%rdx,4), %edx
	cmpl	$-1, %edx
	je	.L413
	movl	%edx, %eax
.L414:
	movslq	%eax, %rdx
	cmpq	%rcx, %rdx
	jb	.L416
.L413:
	ret
	.cfi_endproc
.LFE25026:
	.size	_ZN2v88internal8compiler19InstructionSelector9GetRenameEi, .-_ZN2v88internal8compiler19InstructionSelector9GetRenameEi
	.section	.text._ZN2v88internal8compiler19InstructionSelector9TryRenameEPNS1_18InstructionOperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector9TryRenameEPNS1_18InstructionOperandE
	.type	_ZN2v88internal8compiler19InstructionSelector9TryRenameEPNS1_18InstructionOperandE, @function
_ZN2v88internal8compiler19InstructionSelector9TryRenameEPNS1_18InstructionOperandE:
.LFB25027:
	.cfi_startproc
	endbr64
	movq	(%rsi), %r9
	movl	%r9d, %eax
	andl	$7, %eax
	cmpl	$1, %eax
	jne	.L417
	movq	320(%rdi), %r8
	movq	328(%rdi), %rcx
	movq	%r9, %r10
	shrq	$3, %r10
	subq	%r8, %rcx
	movl	%r10d, %edx
	sarq	$2, %rcx
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L422:
	movl	(%r8,%rax,4), %eax
	cmpl	$-1, %eax
	je	.L419
	movl	%eax, %edx
.L420:
	movslq	%edx, %rax
	cmpq	%rcx, %rax
	jb	.L422
.L419:
	cmpl	%r10d, %edx
	je	.L417
	movabsq	$-34359738361, %rax
	andq	%rax, %r9
	movl	%edx, %eax
	leaq	0(,%rax,8), %rdx
	orq	%r9, %rdx
	movq	%rdx, (%rsi)
.L417:
	ret
	.cfi_endproc
.LFE25027:
	.size	_ZN2v88internal8compiler19InstructionSelector9TryRenameEPNS1_18InstructionOperandE, .-_ZN2v88internal8compiler19InstructionSelector9TryRenameEPNS1_18InstructionOperandE
	.section	.text._ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE:
.LFB25029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	20(%rsi), %r12d
	movq	288(%rdi), %rax
	andl	$16777215, %r12d
	movl	(%rax,%r12,4), %eax
	cmpl	$-1, %eax
	je	.L426
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%rbx), %rdx
	movl	%eax, (%rdx,%r12,4)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25029:
	.size	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE
	.section	.text._ZNK2v88internal8compiler19InstructionSelector29GetVirtualRegistersForTestingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19InstructionSelector29GetVirtualRegistersForTestingEv
	.type	_ZNK2v88internal8compiler19InstructionSelector29GetVirtualRegistersForTestingEv, @function
_ZNK2v88internal8compiler19InstructionSelector29GetVirtualRegistersForTestingEv:
.LFB25030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	%rax, 24(%rdi)
	movq	%rax, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	288(%rsi), %rdx
	movq	296(%rsi), %rcx
	movq	%rax, -64(%rbp)
	cmpq	%rcx, %rdx
	je	.L427
	movq	%rsi, %r13
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L428:
	movl	(%rdx,%rbx,4), %r12d
	cmpl	$-1, %r12d
	je	.L429
	movl	$40, %edi
	movl	%ebx, -52(%rbp)
	call	_Znwm@PLT
	movl	-52(%rbp), %r9d
	movl	%r12d, 36(%rax)
	movq	16(%r14), %r12
	movq	%rax, %r15
	movl	%ebx, 32(%rax)
	testq	%r12, %r12
	jne	.L431
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L457:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L432
.L458:
	movq	%rax, %r12
.L431:
	movl	32(%r12), %ecx
	cmpl	%r9d, %ecx
	ja	.L457
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L458
.L432:
	testb	%dl, %dl
	jne	.L459
	cmpl	%r9d, %ecx
	jb	.L441
.L437:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L439:
	movq	288(%r13), %rdx
	movq	296(%r13), %rcx
.L429:
	movq	%rcx, %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$2, %rax
	cmpq	%rax, %rbx
	jb	.L428
.L427:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	cmpq	24(%r14), %r12
	je	.L441
.L442:
	movq	%r12, %rdi
	movl	%r9d, -52(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movl	-52(%rbp), %r9d
	cmpl	32(%rax), %r9d
	jbe	.L437
	testq	%r12, %r12
	je	.L437
.L441:
	movl	$1, %edi
	cmpq	%r12, -64(%rbp)
	jne	.L460
.L438:
	movq	-64(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%r14)
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L460:
	xorl	%edi, %edi
	cmpl	32(%r12), %r9d
	setb	%dil
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L456:
	movq	-64(%rbp), %rax
	movq	%rax, %r12
	cmpq	24(%r14), %rax
	jne	.L442
	movl	$1, %edi
	jmp	.L438
	.cfi_endproc
.LFE25030:
	.size	_ZNK2v88internal8compiler19InstructionSelector29GetVirtualRegistersForTestingEv, .-_ZNK2v88internal8compiler19InstructionSelector29GetVirtualRegistersForTestingEv
	.section	.text._ZNK2v88internal8compiler19InstructionSelector9IsDefinedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19InstructionSelector9IsDefinedEPNS1_4NodeE
	.type	_ZNK2v88internal8compiler19InstructionSelector9IsDefinedEPNS1_4NodeE, @function
_ZNK2v88internal8compiler19InstructionSelector9IsDefinedEPNS1_4NodeE:
.LFB25056:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %ecx
	movq	160(%rdi), %rsi
	movl	$1, %edx
	movl	%ecx, %eax
	salq	%cl, %rdx
	andl	$16777215, %eax
	shrq	$6, %rax
	testq	%rdx, (%rsi,%rax,8)
	setne	%al
	ret
	.cfi_endproc
.LFE25056:
	.size	_ZNK2v88internal8compiler19InstructionSelector9IsDefinedEPNS1_4NodeE, .-_ZNK2v88internal8compiler19InstructionSelector9IsDefinedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE:
.LFB25057:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %ecx
	movl	$1, %edx
	movq	%rcx, %rax
	salq	%cl, %rdx
	shrq	$3, %rax
	andl	$2097144, %eax
	addq	160(%rdi), %rax
	orq	%rdx, (%rax)
	ret
	.cfi_endproc
.LFE25057:
	.size	_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13MarkAsDefinedEPNS1_4NodeE
	.section	.text._ZNK2v88internal8compiler19InstructionSelector6IsUsedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19InstructionSelector6IsUsedEPNS1_4NodeE
	.type	_ZNK2v88internal8compiler19InstructionSelector6IsUsedEPNS1_4NodeE, @function
_ZNK2v88internal8compiler19InstructionSelector6IsUsedEPNS1_4NodeE:
.LFB25058:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movl	$1, %r8d
	cmpw	$56, 16(%rax)
	je	.L463
	movzbl	18(%rax), %eax
	andl	$112, %eax
	cmpb	$112, %al
	je	.L467
.L463:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	movl	20(%rsi), %ecx
	movq	208(%rdi), %rsi
	movl	$1, %edx
	movl	%ecx, %eax
	salq	%cl, %rdx
	andl	$16777215, %eax
	shrq	$6, %rax
	testq	%rdx, (%rsi,%rax,8)
	setne	%r8b
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE25058:
	.size	_ZNK2v88internal8compiler19InstructionSelector6IsUsedEPNS1_4NodeE, .-_ZNK2v88internal8compiler19InstructionSelector6IsUsedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE:
.LFB25059:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %ecx
	movl	$1, %edx
	movq	%rcx, %rax
	salq	%cl, %rdx
	shrq	$3, %rax
	andl	$2097144, %eax
	addq	208(%rdi), %rax
	orq	%rdx, (%rax)
	ret
	.cfi_endproc
.LFE25059:
	.size	_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector10MarkAsUsedEPNS1_4NodeE
	.section	.text._ZNK2v88internal8compiler19InstructionSelector14GetEffectLevelEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19InstructionSelector14GetEffectLevelEPNS1_4NodeE
	.type	_ZNK2v88internal8compiler19InstructionSelector14GetEffectLevelEPNS1_4NodeE, @function
_ZNK2v88internal8compiler19InstructionSelector14GetEffectLevelEPNS1_4NodeE:
.LFB25060:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %eax
	movq	256(%rdi), %rdx
	andl	$16777215, %eax
	movl	(%rdx,%rax,4), %eax
	ret
	.cfi_endproc
.LFE25060:
	.size	_ZNK2v88internal8compiler19InstructionSelector14GetEffectLevelEPNS1_4NodeE, .-_ZNK2v88internal8compiler19InstructionSelector14GetEffectLevelEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14SetEffectLevelEPNS1_4NodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14SetEffectLevelEPNS1_4NodeEi
	.type	_ZN2v88internal8compiler19InstructionSelector14SetEffectLevelEPNS1_4NodeEi, @function
_ZN2v88internal8compiler19InstructionSelector14SetEffectLevelEPNS1_4NodeEi:
.LFB25061:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %eax
	movl	%edx, %r8d
	movq	256(%rdi), %rdx
	andl	$16777215, %eax
	movl	%r8d, (%rdx,%rax,4)
	ret
	.cfi_endproc
.LFE25061:
	.size	_ZN2v88internal8compiler19InstructionSelector14SetEffectLevelEPNS1_4NodeEi, .-_ZN2v88internal8compiler19InstructionSelector14SetEffectLevelEPNS1_4NodeEi
	.section	.text._ZNK2v88internal8compiler19InstructionSelector33CanAddressRelativeToRootsRegisterERKNS0_17ExternalReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19InstructionSelector33CanAddressRelativeToRootsRegisterERKNS0_17ExternalReferenceE
	.type	_ZNK2v88internal8compiler19InstructionSelector33CanAddressRelativeToRootsRegisterERKNS0_17ExternalReferenceE, @function
_ZNK2v88internal8compiler19InstructionSelector33CanAddressRelativeToRootsRegisterERKNS0_17ExternalReferenceE:
.LFB25062:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	testb	$4, 64(%rax)
	je	.L473
	cmpl	$1, 356(%rdi)
	je	.L474
	movq	16(%rdi), %rax
	movq	(%rax), %rdi
	jmp	_ZN2v88internal18TurboAssemblerBase32IsAddressableThroughRootRegisterEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	.p2align 4,,10
	.p2align 3
.L474:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE25062:
	.size	_ZNK2v88internal8compiler19InstructionSelector33CanAddressRelativeToRootsRegisterERKNS0_17ExternalReferenceE, .-_ZNK2v88internal8compiler19InstructionSelector33CanAddressRelativeToRootsRegisterERKNS0_17ExternalReferenceE
	.section	.text._ZNK2v88internal8compiler19InstructionSelector19CanUseRootsRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19InstructionSelector19CanUseRootsRegisterEv
	.type	_ZNK2v88internal8compiler19InstructionSelector19CanUseRootsRegisterEv, @function
_ZNK2v88internal8compiler19InstructionSelector19CanUseRootsRegisterEv:
.LFB25063:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movl	64(%rax), %eax
	shrl	$2, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE25063:
	.size	_ZNK2v88internal8compiler19InstructionSelector19CanUseRootsRegisterEv, .-_ZNK2v88internal8compiler19InstructionSelector19CanUseRootsRegisterEv
	.section	.text._ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationERKNS1_18InstructionOperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationERKNS1_18InstructionOperandE
	.type	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationERKNS1_18InstructionOperandE, @function
_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationERKNS1_18InstructionOperandE:
.LFB25064:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rdx
	movq	16(%rdi), %rdi
	shrq	$3, %rdx
	jmp	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	.cfi_endproc
.LFE25064:
	.size	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationERKNS1_18InstructionOperandE, .-_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationERKNS1_18InstructionOperandE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE:
.LFB25065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	20(%rdx), %r12d
	movq	288(%rbx), %rax
	movq	16(%rdi), %rdi
	andl	$16777215, %r12d
	movl	(%rax,%r12,4), %edx
	cmpl	$-1, %edx
	je	.L480
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	.p2align 4,,10
	.p2align 3
.L480:
	.cfi_restore_state
	movl	%esi, -28(%rbp)
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movl	-28(%rbp), %esi
	movq	-24(%rbp), %rdi
	movl	%eax, %edx
	movq	288(%rbx), %rax
	movl	%edx, (%rax,%r12,4)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	.cfi_endproc
.LFE25065:
	.size	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20IsSourcePositionUsedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20IsSourcePositionUsedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector20IsSourcePositionUsedEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector20IsSourcePositionUsedEPNS1_4NodeE:
.LFB25108:
	.cfi_startproc
	endbr64
	cmpl	$1, 32(%rdi)
	movl	$1, %eax
	je	.L481
	movq	(%rsi), %rax
	movzwl	16(%rax), %edx
	leal	-14(%rdx), %eax
	leal	-502(%rdx), %ecx
	cmpw	$1, %ax
	setbe	%al
	cmpw	$1, %cx
	setbe	%cl
	orl	%ecx, %eax
	cmpw	$49, %dx
	sete	%dl
	orl	%edx, %eax
.L481:
	ret
	.cfi_endproc
.LFE25108:
	.size	_ZN2v88internal8compiler19InstructionSelector20IsSourcePositionUsedEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector20IsSourcePositionUsedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector27MarkPairProjectionsAsWord32EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector27MarkPairProjectionsAsWord32EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector27MarkPairProjectionsAsWord32EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector27MarkPairProjectionsAsWord32EPNS1_4NodeE:
.LFB25124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	.cfi_offset 3, -40
	call	_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm@PLT
	testq	%rax, %rax
	je	.L485
	movl	20(%rax), %ebx
	movq	288(%r13), %rax
	movq	16(%r13), %rdi
	andl	$16777215, %ebx
	movl	(%rax,%rbx,4), %edx
	cmpl	$-1, %edx
	je	.L492
.L486:
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
.L485:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties14FindProjectionEPNS1_4NodeEm@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L484
	addq	$24, %rsp
	movq	%r13, %rdi
	movl	$4, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	-40(%rbp), %rdi
	movl	%eax, %edx
	movq	288(%r13), %rax
	movl	%edx, (%rax,%rbx,4)
	jmp	.L486
	.cfi_endproc
.LFE25124:
	.size	_ZN2v88internal8compiler19InstructionSelector27MarkPairProjectionsAsWord32EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector27MarkPairProjectionsAsWord32EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector28VisitStackPointerGreaterThanEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector28VisitStackPointerGreaterThanEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector28VisitStackPointerGreaterThanEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector28VisitStackPointerGreaterThanEPNS1_4NodeE:
.LFB25130:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rdx
	movq	$0, -64(%rbp)
	movabsq	$38654705669, %rax
	movq	%rax, -80(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rsi, -48(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector28VisitStackPointerGreaterThanEPNS1_4NodeEPNS1_17FlagsContinuationE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L496
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L496:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25130:
	.size	_ZN2v88internal8compiler19InstructionSelector28VisitStackPointerGreaterThanEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector28VisitStackPointerGreaterThanEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitFloat64AcosEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64AcosEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64AcosEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitFloat64AcosEPNS1_4NodeE:
.LFB25133:
	.cfi_startproc
	endbr64
	movl	$74, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25133:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64AcosEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitFloat64AcosEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitFloat64AcoshEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64AcoshEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64AcoshEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitFloat64AcoshEPNS1_4NodeE:
.LFB25134:
	.cfi_startproc
	endbr64
	movl	$75, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25134:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64AcoshEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitFloat64AcoshEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitFloat64AsinEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64AsinEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64AsinEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitFloat64AsinEPNS1_4NodeE:
.LFB25135:
	.cfi_startproc
	endbr64
	movl	$76, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25135:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64AsinEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitFloat64AsinEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitFloat64AsinhEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64AsinhEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64AsinhEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitFloat64AsinhEPNS1_4NodeE:
.LFB25136:
	.cfi_startproc
	endbr64
	movl	$77, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25136:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64AsinhEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitFloat64AsinhEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitFloat64AtanEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64AtanEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64AtanEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitFloat64AtanEPNS1_4NodeE:
.LFB25137:
	.cfi_startproc
	endbr64
	movl	$78, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25137:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64AtanEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitFloat64AtanEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitFloat64AtanhEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64AtanhEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64AtanhEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitFloat64AtanhEPNS1_4NodeE:
.LFB25138:
	.cfi_startproc
	endbr64
	movl	$79, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25138:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64AtanhEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitFloat64AtanhEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitFloat64Atan2EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Atan2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Atan2EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Atan2EPNS1_4NodeE:
.LFB25139:
	.cfi_startproc
	endbr64
	movl	$80, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector24VisitFloat64Ieee754BinopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25139:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Atan2EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Atan2EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitFloat64CbrtEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64CbrtEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64CbrtEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitFloat64CbrtEPNS1_4NodeE:
.LFB25140:
	.cfi_startproc
	endbr64
	movl	$81, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25140:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64CbrtEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitFloat64CbrtEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat64CosEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64CosEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64CosEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat64CosEPNS1_4NodeE:
.LFB25141:
	.cfi_startproc
	endbr64
	movl	$82, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25141:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64CosEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat64CosEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitFloat64CoshEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64CoshEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64CoshEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitFloat64CoshEPNS1_4NodeE:
.LFB25142:
	.cfi_startproc
	endbr64
	movl	$83, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25142:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64CoshEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitFloat64CoshEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat64ExpEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64ExpEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64ExpEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat64ExpEPNS1_4NodeE:
.LFB25143:
	.cfi_startproc
	endbr64
	movl	$84, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25143:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64ExpEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat64ExpEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitFloat64Expm1EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Expm1EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Expm1EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Expm1EPNS1_4NodeE:
.LFB25144:
	.cfi_startproc
	endbr64
	movl	$85, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25144:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Expm1EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Expm1EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat64LogEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64LogEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64LogEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat64LogEPNS1_4NodeE:
.LFB25145:
	.cfi_startproc
	endbr64
	movl	$86, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25145:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64LogEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat64LogEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitFloat64Log1pEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Log1pEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Log1pEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Log1pEPNS1_4NodeE:
.LFB25146:
	.cfi_startproc
	endbr64
	movl	$87, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25146:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Log1pEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Log1pEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitFloat64Log2EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64Log2EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64Log2EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitFloat64Log2EPNS1_4NodeE:
.LFB25147:
	.cfi_startproc
	endbr64
	movl	$89, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25147:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64Log2EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitFloat64Log2EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitFloat64Log10EPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Log10EPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Log10EPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Log10EPNS1_4NodeE:
.LFB25148:
	.cfi_startproc
	endbr64
	movl	$88, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25148:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Log10EPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitFloat64Log10EPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat64PowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64PowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64PowEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat64PowEPNS1_4NodeE:
.LFB25149:
	.cfi_startproc
	endbr64
	movl	$90, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector24VisitFloat64Ieee754BinopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25149:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64PowEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat64PowEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat64SinEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64SinEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64SinEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat64SinEPNS1_4NodeE:
.LFB25150:
	.cfi_startproc
	endbr64
	movl	$91, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25150:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64SinEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat64SinEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitFloat64SinhEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64SinhEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64SinhEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitFloat64SinhEPNS1_4NodeE:
.LFB25151:
	.cfi_startproc
	endbr64
	movl	$92, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25151:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64SinhEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitFloat64SinhEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitFloat64TanEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64TanEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64TanEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitFloat64TanEPNS1_4NodeE:
.LFB25152:
	.cfi_startproc
	endbr64
	movl	$93, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25152:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64TanEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitFloat64TanEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitFloat64TanhEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64TanhEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64TanhEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitFloat64TanhEPNS1_4NodeE:
.LFB25153:
	.cfi_startproc
	endbr64
	movl	$94, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	.cfi_endproc
.LFE25153:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64TanhEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitFloat64TanhEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector17VisitInt32PairAddEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"unimplemented code"
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitInt32PairAddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitInt32PairAddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitInt32PairAddEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitInt32PairAddEPNS1_4NodeE:
.LFB32637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE32637:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitInt32PairAddEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitInt32PairAddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitInt32PairSubEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitInt32PairSubEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitInt32PairSubEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitInt32PairSubEPNS1_4NodeE:
.LFB32639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE32639:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitInt32PairSubEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitInt32PairSubEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitInt32PairMulEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitInt32PairMulEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitInt32PairMulEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitInt32PairMulEPNS1_4NodeE:
.LFB32641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE32641:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitInt32PairMulEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitInt32PairMulEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector18VisitWord32PairShlEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector18VisitWord32PairShlEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector18VisitWord32PairShlEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector18VisitWord32PairShlEPNS1_4NodeE:
.LFB32643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE32643:
	.size	_ZN2v88internal8compiler19InstructionSelector18VisitWord32PairShlEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector18VisitWord32PairShlEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector18VisitWord32PairShrEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector18VisitWord32PairShrEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector18VisitWord32PairShrEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector18VisitWord32PairShrEPNS1_4NodeE:
.LFB32645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE32645:
	.size	_ZN2v88internal8compiler19InstructionSelector18VisitWord32PairShrEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector18VisitWord32PairShrEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector18VisitWord32PairSarEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector18VisitWord32PairSarEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector18VisitWord32PairSarEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector18VisitWord32PairSarEPNS1_4NodeE:
.LFB32647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE32647:
	.size	_ZN2v88internal8compiler19InstructionSelector18VisitWord32PairSarEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector18VisitWord32PairSarEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25VisitWord32AtomicPairLoadEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25VisitWord32AtomicPairLoadEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25VisitWord32AtomicPairLoadEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25VisitWord32AtomicPairLoadEPNS1_4NodeE:
.LFB25168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25168:
	.size	_ZN2v88internal8compiler19InstructionSelector25VisitWord32AtomicPairLoadEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25VisitWord32AtomicPairLoadEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector26VisitWord32AtomicPairStoreEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector26VisitWord32AtomicPairStoreEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector26VisitWord32AtomicPairStoreEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector26VisitWord32AtomicPairStoreEPNS1_4NodeE:
.LFB32621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE32621:
	.size	_ZN2v88internal8compiler19InstructionSelector26VisitWord32AtomicPairStoreEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector26VisitWord32AtomicPairStoreEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairAddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairAddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairAddEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairAddEPNS1_4NodeE:
.LFB32623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE32623:
	.size	_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairAddEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairAddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairSubEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairSubEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairSubEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairSubEPNS1_4NodeE:
.LFB32625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE32625:
	.size	_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairSubEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairSubEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairAndEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairAndEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairAndEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairAndEPNS1_4NodeE:
.LFB32627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE32627:
	.size	_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairAndEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairAndEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector23VisitWord32AtomicPairOrEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector23VisitWord32AtomicPairOrEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector23VisitWord32AtomicPairOrEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector23VisitWord32AtomicPairOrEPNS1_4NodeE:
.LFB32629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE32629:
	.size	_ZN2v88internal8compiler19InstructionSelector23VisitWord32AtomicPairOrEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector23VisitWord32AtomicPairOrEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairXorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairXorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairXorEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairXorEPNS1_4NodeE:
.LFB32631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE32631:
	.size	_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairXorEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector24VisitWord32AtomicPairXorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector29VisitWord32AtomicPairExchangeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector29VisitWord32AtomicPairExchangeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector29VisitWord32AtomicPairExchangeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector29VisitWord32AtomicPairExchangeEPNS1_4NodeE:
.LFB32633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE32633:
	.size	_ZN2v88internal8compiler19InstructionSelector29VisitWord32AtomicPairExchangeEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector29VisitWord32AtomicPairExchangeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector36VisitWord32AtomicPairCompareExchangeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector36VisitWord32AtomicPairCompareExchangeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector36VisitWord32AtomicPairCompareExchangeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector36VisitWord32AtomicPairCompareExchangeEPNS1_4NodeE:
.LFB32635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE32635:
	.size	_ZN2v88internal8compiler19InstructionSelector36VisitWord32AtomicPairCompareExchangeEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector36VisitWord32AtomicPairCompareExchangeEPNS1_4NodeE
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector8VisitPhiEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector8VisitPhiEPNS1_4NodeE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal8compiler19InstructionSelector8VisitPhiEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector8VisitPhiEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector8VisitPhiEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector8VisitPhiEPNS1_4NodeE:
.LFB25182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rax
	movl	20(%rsi), %r12d
	movq	16(%rdi), %r9
	movl	20(%rax), %r8d
	movq	288(%rdi), %rax
	andl	$16777215, %r12d
	movq	8(%r9), %r13
	movl	(%rax,%r12,4), %edx
	movq	%r13, %rdi
	cmpl	$-1, %edx
	je	.L587
.L549:
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L588
	leaq	48(%r12), %rax
	movq	%rax, 16(%rdi)
.L551:
	movslq	%r8d, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal8compiler14PhiInstructionC1EPNS0_4ZoneEim@PLT
	movq	16(%r14), %rax
	movl	-56(%rbp), %r8d
	movq	16(%rax), %rdx
	movq	48(%r14), %rax
	movslq	4(%rax), %rsi
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L589
	movq	(%rax,%rsi,8), %r13
	movq	80(%r13), %r15
	cmpq	88(%r13), %r15
	je	.L553
	movq	%r12, (%r15)
	addq	$8, 80(%r13)
.L554:
	testl	%r8d, %r8d
	jle	.L548
	leal	-1(%r8), %eax
	leaq	32(%rbx), %r15
	xorl	%r13d, %r13d
	movq	%rax, -56(%rbp)
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L591:
	addq	%r15, %rax
.L567:
	movq	(%rax), %rdx
	movl	$1, %esi
	movl	20(%rdx), %ecx
	movq	%rcx, %rax
	salq	%cl, %rsi
	shrq	$3, %rax
	andl	$2097144, %eax
	addq	208(%r14), %rax
	orq	%rsi, (%rax)
	movl	20(%rdx), %ecx
	movq	288(%r14), %rax
	andl	$16777215, %ecx
	movl	(%rax,%rcx,4), %edx
	cmpl	$-1, %edx
	je	.L590
.L568:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14PhiInstruction8SetInputEmi@PLT
	leaq	1(%r13), %rax
	cmpq	-56(%rbp), %r13
	je	.L548
	movq	%rax, %r13
.L571:
	movzbl	23(%rbx), %edx
	leaq	0(,%r13,8), %rax
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L591
	movq	(%r15), %rdx
	leaq	16(%rdx,%rax), %rax
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L590:
	movq	16(%r14), %rdi
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	-64(%rbp), %rcx
	movl	%eax, %edx
	movq	288(%r14), %rax
	movl	%edx, (%rax,%rcx,4)
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L548:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	.cfi_restore_state
	movq	%r9, %rdi
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movl	-56(%rbp), %r8d
	movl	%eax, %edx
	movq	288(%r14), %rax
	movl	%edx, (%rax,%r12,4)
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L553:
	movq	72(%r13), %rcx
	movq	%r15, %r9
	subq	%rcx, %r9
	movq	%r9, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L592
	testq	%rax, %rax
	je	.L573
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L593
	movl	$2147483640, %esi
	movl	$2147483640, %r10d
.L556:
	movq	64(%r13), %rdi
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	subq	%rdx, %rax
	cmpq	%rsi, %rax
	jb	.L594
	addq	%rdx, %rsi
	movq	%rsi, 16(%rdi)
.L559:
	leaq	(%rdx,%r10), %rsi
	leaq	8(%rdx), %rax
.L557:
	movq	%r12, (%rdx,%r9)
	cmpq	%rcx, %r15
	je	.L560
	subq	$8, %r15
	leaq	15(%rdx), %rax
	subq	%rcx, %r15
	subq	%rcx, %rax
	movq	%r15, %r9
	shrq	$3, %r9
	cmpq	$30, %rax
	jbe	.L576
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r9
	je	.L576
	addq	$1, %r9
	xorl	%eax, %eax
	movq	%r9, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L562:
	movdqu	(%rcx,%rax), %xmm1
	movups	%xmm1, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L562
	movq	%r9, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	addq	%rdi, %rcx
	addq	%rdx, %rdi
	cmpq	%r9, %rax
	je	.L564
	movq	(%rcx), %rax
	movq	%rax, (%rdi)
.L564:
	leaq	16(%rdx,%r15), %rax
.L560:
	movq	%rdx, %xmm0
	movq	%rax, %xmm2
	movq	%rsi, 88(%r13)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 72(%r13)
	jmp	.L554
.L593:
	testq	%rdx, %rdx
	jne	.L595
	movl	$8, %eax
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L588:
	movl	$48, %esi
	movl	%edx, -64(%rbp)
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %r8d
	movl	-64(%rbp), %edx
	movq	%rax, %r12
	jmp	.L551
.L573:
	movl	$8, %esi
	movl	$8, %r10d
	jmp	.L556
.L576:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L561:
	movq	(%rcx,%rax,8), %rdi
	movq	%rdi, (%rdx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%r9, %rdi
	jne	.L561
	jmp	.L564
.L594:
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movl	%r8d, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %r8d
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r10
	movq	%rax, %rdx
	jmp	.L559
.L592:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L589:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L595:
	movl	$268435455, %r10d
	cmpq	$268435455, %rdx
	cmova	%r10, %rdx
	leaq	0(,%rdx,8), %r10
	movq	%r10, %rsi
	jmp	.L556
	.cfi_endproc
.LFE25182:
	.size	_ZN2v88internal8compiler19InstructionSelector8VisitPhiEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector8VisitPhiEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector11VisitBranchEPNS1_4NodeEPNS1_10BasicBlockES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector11VisitBranchEPNS1_4NodeEPNS1_10BasicBlockES6_
	.type	_ZN2v88internal8compiler19InstructionSelector11VisitBranchEPNS1_4NodeEPNS1_10BasicBlockES6_, @function
_ZN2v88internal8compiler19InstructionSelector11VisitBranchEPNS1_4NodeEPNS1_10BasicBlockES6_:
.LFB25196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$96, %rsp
	movq	%rdx, -104(%rbp)
	movq	(%rsi), %rdi
	movq	%rcx, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler15IsSafetyCheckOfEPKNS1_8OperatorE@PLT
	movl	364(%r13), %edx
	cmpl	$1, %edx
	je	.L597
	cmpl	$2, %edx
	je	.L598
	testl	%edx, %edx
	je	.L613
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L597:
	movabsq	$4294967297, %rax
	movq	-104(%rbp), %xmm0
	movq	$0, -80(%rbp)
	movq	%rax, -96(%rbp)
	movzbl	23(%r12), %eax
	movhps	-112(%rbp), %xmm0
	movl	$-1, -72(%rbp)
	movq	32(%r12), %rdx
	andl	$15, %eax
	movups	%xmm0, -56(%rbp)
	cmpl	$15, %eax
	je	.L612
.L603:
	leaq	-96(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20VisitWordCompareZeroEPNS1_4NodeES4_PNS1_17FlagsContinuationE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L614
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L613:
	.cfi_restore_state
	cmpb	$2, %al
	setne	%al
.L600:
	testb	%al, %al
	je	.L597
	movabsq	$4294967298, %rax
	movq	-104(%rbp), %xmm0
	movq	$0, -80(%rbp)
	movq	%rax, -96(%rbp)
	movzbl	23(%r12), %eax
	movhps	-112(%rbp), %xmm0
	movl	$-1, -72(%rbp)
	movq	32(%r12), %rdx
	andl	$15, %eax
	movups	%xmm0, -56(%rbp)
	cmpl	$15, %eax
	jne	.L603
.L612:
	movq	16(%rdx), %rdx
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L598:
	testb	%al, %al
	sete	%al
	jmp	.L600
.L614:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25196:
	.size	_ZN2v88internal8compiler19InstructionSelector11VisitBranchEPNS1_4NodeEPNS1_10BasicBlockES6_, .-_ZN2v88internal8compiler19InstructionSelector11VisitBranchEPNS1_4NodeEPNS1_10BasicBlockES6_
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitDeoptimizeIfEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitDeoptimizeIfEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitDeoptimizeIfEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitDeoptimizeIfEPNS1_4NodeE:
.LFB25197:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$80, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler22DeoptimizeParametersOfEPKNS1_8OperatorE@PLT
	movq	8(%rax), %rcx
	movl	16(%rax), %edx
	movzbl	(%rax), %edi
	movzbl	1(%rax), %esi
	movzbl	24(%rax), %r8d
	movl	364(%r13), %eax
	cmpl	$1, %eax
	je	.L616
	cmpl	$2, %eax
	je	.L617
	testl	%eax, %eax
	je	.L635
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L616:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L623
	movabsq	$4294967299, %r11
	movq	40(%r12), %rax
	movq	%r11, -96(%rbp)
.L634:
	movb	%dil, -88(%rbp)
	movb	%sil, -87(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%edx, -72(%rbp)
	movq	%rax, -64(%rbp)
.L624:
	leaq	-96(%rbp), %rcx
	movq	%r8, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20VisitWordCompareZeroEPNS1_4NodeES4_PNS1_17FlagsContinuationE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L636
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L635:
	.cfi_restore_state
	cmpb	$2, %r8b
	setne	%al
.L619:
	testb	%al, %al
	je	.L616
	movzbl	23(%r12), %eax
	movq	32(%r12), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L620
	movabsq	$4294967300, %r10
	movq	40(%r12), %rax
	movq	%r10, -96(%rbp)
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L617:
	testb	%r8b, %r8b
	sete	%al
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L623:
	movabsq	$4294967299, %r11
	movq	24(%r8), %rax
	movq	%r11, -96(%rbp)
.L633:
	movb	%dil, -88(%rbp)
	movb	%sil, -87(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%edx, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	16(%r8), %r8
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L620:
	movabsq	$4294967300, %r10
	movq	24(%r8), %rax
	movq	%r10, -96(%rbp)
	jmp	.L633
.L636:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25197:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitDeoptimizeIfEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitDeoptimizeIfEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitDeoptimizeUnlessEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitDeoptimizeUnlessEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitDeoptimizeUnlessEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitDeoptimizeUnlessEPNS1_4NodeE:
.LFB25198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$80, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler22DeoptimizeParametersOfEPKNS1_8OperatorE@PLT
	movq	8(%rax), %rcx
	movl	16(%rax), %edx
	movzbl	(%rax), %edi
	movzbl	1(%rax), %esi
	movzbl	24(%rax), %r8d
	movl	364(%r13), %eax
	cmpl	$1, %eax
	je	.L638
	cmpl	$2, %eax
	je	.L639
	testl	%eax, %eax
	je	.L657
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L638:
	movzbl	23(%r12), %eax
	movq	32(%r12), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L645
	movq	$3, -96(%rbp)
	movq	40(%r12), %rax
.L656:
	movb	%dil, -88(%rbp)
	movb	%sil, -87(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%edx, -72(%rbp)
	movq	%rax, -64(%rbp)
.L646:
	leaq	-96(%rbp), %rcx
	movq	%r8, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20VisitWordCompareZeroEPNS1_4NodeES4_PNS1_17FlagsContinuationE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L658
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L657:
	.cfi_restore_state
	cmpb	$2, %r8b
	setne	%al
.L641:
	testb	%al, %al
	je	.L638
	movzbl	23(%r12), %eax
	movq	32(%r12), %r8
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L642
	movq	$4, -96(%rbp)
	movq	40(%r12), %rax
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L639:
	testb	%r8b, %r8b
	sete	%al
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L645:
	movq	24(%r8), %rax
	movq	$3, -96(%rbp)
.L655:
	movb	%dil, -88(%rbp)
	movb	%sil, -87(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%edx, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	16(%r8), %r8
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L642:
	movq	24(%r8), %rax
	movq	$4, -96(%rbp)
	jmp	.L655
.L658:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25198:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitDeoptimizeUnlessEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitDeoptimizeUnlessEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector11VisitTrapIfEPNS1_4NodeENS1_6TrapIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector11VisitTrapIfEPNS1_4NodeENS1_6TrapIdE
	.type	_ZN2v88internal8compiler19InstructionSelector11VisitTrapIfEPNS1_4NodeENS1_6TrapIdE, @function
_ZN2v88internal8compiler19InstructionSelector11VisitTrapIfEPNS1_4NodeENS1_6TrapIdE:
.LFB25199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	32(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L660
	movq	40(%rsi), %rax
	movq	$0, -64(%rbp)
	movabsq	$4294967302, %rcx
	movq	%rcx, -80(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rax, -48(%rbp)
	movl	%edx, -24(%rbp)
.L661:
	leaq	-80(%rbp), %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector20VisitWordCompareZeroEPNS1_4NodeES4_PNS1_17FlagsContinuationE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L664
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L660:
	.cfi_restore_state
	movabsq	$4294967302, %rcx
	movq	24(%r8), %rax
	movq	$0, -64(%rbp)
	movq	%rcx, -80(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rax, -48(%rbp)
	movl	%edx, -24(%rbp)
	movq	16(%r8), %r8
	jmp	.L661
.L664:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25199:
	.size	_ZN2v88internal8compiler19InstructionSelector11VisitTrapIfEPNS1_4NodeENS1_6TrapIdE, .-_ZN2v88internal8compiler19InstructionSelector11VisitTrapIfEPNS1_4NodeENS1_6TrapIdE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitTrapUnlessEPNS1_4NodeENS1_6TrapIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitTrapUnlessEPNS1_4NodeENS1_6TrapIdE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitTrapUnlessEPNS1_4NodeENS1_6TrapIdE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitTrapUnlessEPNS1_4NodeENS1_6TrapIdE:
.LFB25200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	32(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L666
	movq	40(%rsi), %rax
	movq	$6, -80(%rbp)
	movq	$0, -64(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rax, -48(%rbp)
	movl	%edx, -24(%rbp)
.L667:
	leaq	-80(%rbp), %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector20VisitWordCompareZeroEPNS1_4NodeES4_PNS1_17FlagsContinuationE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L670
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L666:
	.cfi_restore_state
	movq	24(%r8), %rax
	movq	$6, -80(%rbp)
	movq	$0, -64(%rbp)
	movl	$-1, -56(%rbp)
	movq	%rax, -48(%rbp)
	movl	%edx, -24(%rbp)
	movq	16(%r8), %r8
	jmp	.L667
.L670:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25200:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitTrapUnlessEPNS1_4NodeENS1_6TrapIdE, .-_ZN2v88internal8compiler19InstructionSelector15VisitTrapUnlessEPNS1_4NodeENS1_6TrapIdE
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector17VisitStaticAssertEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"Expected turbofan static assert to hold, but got non-true input!\n"
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitStaticAssertEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitStaticAssertEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitStaticAssertEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitStaticAssertEPNS1_4NodeE:
.LFB25206:
	.cfi_startproc
	endbr64
	movzbl	23(%rsi), %ecx
	movq	32(%rsi), %rdx
	leaq	32(%rsi), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	andl	$15, %ecx
	addq	$16, %rdx
	cmpl	$15, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmove	%rdx, %rax
	movq	(%rax), %rdi
	call	_ZNK2v88internal8compiler4Node5PrintEv@PLT
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25206:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitStaticAssertEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitStaticAssertEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitUnsafePointerAddEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitUnsafePointerAddEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitUnsafePointerAddEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitUnsafePointerAddEPNS1_4NodeE:
.LFB25209:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler19InstructionSelector13VisitInt64AddEPNS1_4NodeE@PLT
	.cfi_endproc
.LFE25209:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitUnsafePointerAddEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitUnsafePointerAddEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector22CanProduceSignalingNaNEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector22CanProduceSignalingNaNEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector22CanProduceSignalingNaNEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector22CanProduceSignalingNaNEPNS1_4NodeE:
.LFB25211:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movzwl	16(%rax), %eax
	subw	$366, %ax
	cmpw	$2, %ax
	seta	%al
	ret
	.cfi_endproc
.LFE25211:
	.size	_ZN2v88internal8compiler19InstructionSelector22CanProduceSignalingNaNEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector22CanProduceSignalingNaNEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector23GetFrameStateDescriptorEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector23GetFrameStateDescriptorEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector23GetFrameStateDescriptorEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector23GetFrameStateDescriptorEPNS1_4NodeE:
.LFB25213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_131GetFrameStateDescriptorInternalEPNS0_4ZoneEPNS1_4NodeE
	movq	432(%rbx), %rcx
	movq	(%rcx), %rdx
	cmpq	%rdx, 40(%rax)
	cmovnb	40(%rax), %rdx
	movq	%rdx, (%rcx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25213:
	.size	_ZN2v88internal8compiler19InstructionSelector23GetFrameStateDescriptorEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector23GetFrameStateDescriptorEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector19CanonicalizeShuffleEbPhPbS4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector19CanonicalizeShuffleEbPhPbS4_
	.type	_ZN2v88internal8compiler19InstructionSelector19CanonicalizeShuffleEbPhPbS4_, @function
_ZN2v88internal8compiler19InstructionSelector19CanonicalizeShuffleEbPhPbS4_:
.LFB25214:
	.cfi_startproc
	endbr64
	movb	$0, (%rdx)
	movl	%edi, %eax
	testb	%dil, %dil
	jne	.L720
	movl	$1, %edi
	cmpb	$16, (%rsi)
	cmovnb	%edi, %eax
	setb	%r8b
	cmpb	$16, 1(%rsi)
	cmovb	%edi, %r8d
	cmovnb	%edi, %eax
	cmpb	$16, 2(%rsi)
	cmovb	%edi, %r8d
	cmovnb	%edi, %eax
	cmpb	$16, 3(%rsi)
	cmovb	%edi, %r8d
	cmovnb	%edi, %eax
	cmpb	$16, 4(%rsi)
	cmovb	%edi, %r8d
	cmovnb	%edi, %eax
	cmpb	$16, 5(%rsi)
	cmovb	%edi, %r8d
	cmovnb	%edi, %eax
	cmpb	$16, 6(%rsi)
	cmovb	%edi, %r8d
	cmovnb	%edi, %eax
	cmpb	$16, 7(%rsi)
	cmovb	%edi, %r8d
	cmovnb	%edi, %eax
	cmpb	$16, 8(%rsi)
	cmovb	%edi, %r8d
	cmovnb	%edi, %eax
	cmpb	$16, 9(%rsi)
	cmovb	%edi, %r8d
	cmovnb	%edi, %eax
	cmpb	$16, 10(%rsi)
	cmovb	%edi, %r8d
	cmovnb	%edi, %eax
	cmpb	$16, 11(%rsi)
	cmovb	%edi, %r8d
	cmovnb	%edi, %eax
	cmpb	$16, 12(%rsi)
	cmovb	%edi, %r8d
	cmovnb	%edi, %eax
	cmpb	$16, 13(%rsi)
	cmovb	%edi, %r8d
	cmovnb	%edi, %eax
	cmpb	$15, 14(%rsi)
	jbe	.L695
	cmpb	$15, 15(%rsi)
	jbe	.L698
	testb	%r8b, %r8b
	jne	.L698
	movb	$1, (%rdx)
	.p2align 4,,10
	.p2align 3
.L720:
	movb	$1, (%rcx)
	movdqu	(%rsi), %xmm0
.L680:
	pand	.LC9(%rip), %xmm0
	movups	%xmm0, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L695:
	cmpb	$15, 15(%rsi)
	jbe	.L702
.L698:
	movb	$0, (%rcx)
	cmpb	$15, (%rsi)
	jbe	.L678
	movb	$1, (%rdx)
	movdqu	(%rsi), %xmm0
	pxor	.LC8(%rip), %xmm0
	movups	%xmm0, (%rsi)
	cmpb	$0, (%rcx)
	jne	.L680
.L678:
	ret
	.p2align 4,,10
	.p2align 3
.L702:
	testb	%al, %al
	je	.L720
	jmp	.L698
	.cfi_endproc
.LFE25214:
	.size	_ZN2v88internal8compiler19InstructionSelector19CanonicalizeShuffleEbPhPbS4_, .-_ZN2v88internal8compiler19InstructionSelector19CanonicalizeShuffleEbPhPbS4_
	.section	.text._ZN2v88internal8compiler19InstructionSelector17SwapShuffleInputsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17SwapShuffleInputsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17SwapShuffleInputsEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17SwapShuffleInputsEPNS1_4NodeE:
.LFB25216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	32(%rsi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L722
	movq	8(%r12), %r14
	movq	%r12, %rax
	cmpq	%r14, %r13
	je	.L721
	leaq	-24(%rsi), %r15
	testq	%r13, %r13
	je	.L725
.L747:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rax
.L725:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L726
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L726:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L746
	movq	8(%r12), %rdi
	cmpq	%rdi, %r13
	je	.L721
	leaq	8(%r12), %r14
.L729:
	leaq	-48(%rbx), %r12
	testq	%rdi, %rdi
	je	.L731
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L731:
	movq	%r13, (%r14)
	testq	%r13, %r13
	je	.L721
	addq	$24, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L721:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L746:
	.cfi_restore_state
	movq	(%r12), %rbx
	movq	24(%rbx), %rdi
	cmpq	%r13, %rdi
	je	.L721
	leaq	24(%rbx), %r14
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L722:
	movq	16(%r13), %rdx
	movq	24(%r13), %r14
	leaq	16(%r13), %rax
	cmpq	%rdx, %r14
	je	.L721
	movq	%r13, %rsi
	movq	%rdx, %r13
	leaq	-24(%rsi), %r15
	testq	%r13, %r13
	jne	.L747
	jmp	.L725
	.cfi_endproc
.LFE25216:
	.size	_ZN2v88internal8compiler19InstructionSelector17SwapShuffleInputsEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17SwapShuffleInputsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector19CanonicalizeShuffleEPNS1_4NodeEPhPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector19CanonicalizeShuffleEPNS1_4NodeEPhPb
	.type	_ZN2v88internal8compiler19InstructionSelector19CanonicalizeShuffleEPNS1_4NodeEPhPb, @function
_ZN2v88internal8compiler19InstructionSelector19CanonicalizeShuffleEPNS1_4NodeEPhPb:
.LFB25215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	32(%rbx), %r15
	subq	$40, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler14S8x16ShuffleOfEPKNS1_8OperatorE@PLT
	movdqu	(%rax), %xmm0
	movups	%xmm0, (%r14)
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L749
	movq	32(%rbx), %rax
	movq	288(%r12), %rsi
	movl	20(%rax), %eax
	andl	$16777215, %eax
	movl	(%rsi,%rax,4), %ecx
	leaq	0(,%rax,4), %r8
	cmpl	$-1, %ecx
	je	.L750
.L751:
	leaq	8(%r15), %rax
.L754:
	movq	(%rax), %rax
	movl	20(%rax), %edx
	andl	$16777215, %edx
	movl	(%rsi,%rdx,4), %eax
	cmpl	$-1, %eax
	je	.L774
.L755:
	xorl	%edi, %edi
	cmpl	%ecx, %eax
	leaq	-57(%rbp), %rdx
	movq	%r13, %rcx
	sete	%dil
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector19CanonicalizeShuffleEbPhPbS4_
	cmpb	$0, -57(%rbp)
	jne	.L775
.L756:
	cmpb	$0, 0(%r13)
	je	.L748
	movzbl	23(%rbx), %eax
	movq	(%r15), %r13
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L759
	movq	8(%r15), %rdi
	addq	$8, %r15
	cmpq	%r13, %rdi
	je	.L748
.L761:
	leaq	-48(%rbx), %r12
	testq	%rdi, %rdi
	je	.L762
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L762:
	movq	%r13, (%r15)
	testq	%r13, %r13
	je	.L748
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L748:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L776
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L749:
	.cfi_restore_state
	movq	32(%rbx), %rdx
	movq	288(%r12), %rsi
	leaq	16(%rdx), %rax
	movq	16(%rdx), %rdx
	movl	20(%rdx), %edx
	andl	$16777215, %edx
	movl	(%rsi,%rdx,4), %ecx
	leaq	0(,%rdx,4), %r8
	cmpl	$-1, %ecx
	je	.L750
.L752:
	addq	$8, %rax
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L775:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector17SwapShuffleInputsEPNS1_4NodeE
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L774:
	movq	16(%r12), %rdi
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%r12), %rsi
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movl	%eax, (%rsi,%rdx,4)
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L750:
	movq	16(%r12), %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	-72(%rbp), %r8
	movl	%eax, %ecx
	movq	288(%r12), %rax
	movl	%ecx, (%rax,%r8)
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L777
	movq	288(%r12), %rsi
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L759:
	movq	16(%r13), %rax
	movq	24(%r13), %rdi
	cmpq	%rax, %rdi
	je	.L748
	leaq	24(%r13), %r15
	movq	%r13, %rbx
	movq	%rax, %r13
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L777:
	movq	(%r15), %rax
	movq	288(%r12), %rsi
	addq	$16, %rax
	jmp	.L752
.L776:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25215:
	.size	_ZN2v88internal8compiler19InstructionSelector19CanonicalizeShuffleEPNS1_4NodeEPhPb, .-_ZN2v88internal8compiler19InstructionSelector19CanonicalizeShuffleEPNS1_4NodeEPhPb
	.section	.text._ZN2v88internal8compiler19InstructionSelector16TryMatchIdentityEPKh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16TryMatchIdentityEPKh
	.type	_ZN2v88internal8compiler19InstructionSelector16TryMatchIdentityEPKh, @function
_ZN2v88internal8compiler19InstructionSelector16TryMatchIdentityEPKh:
.LFB25217:
	.cfi_startproc
	endbr64
	cmpb	$0, (%rdi)
	jne	.L794
	cmpb	$1, 1(%rdi)
	jne	.L794
	cmpb	$2, 2(%rdi)
	jne	.L794
	cmpb	$3, 3(%rdi)
	jne	.L794
	cmpb	$4, 4(%rdi)
	jne	.L794
	cmpb	$5, 5(%rdi)
	jne	.L794
	cmpb	$6, 6(%rdi)
	jne	.L794
	cmpb	$7, 7(%rdi)
	jne	.L794
	cmpb	$8, 8(%rdi)
	jne	.L794
	cmpb	$9, 9(%rdi)
	jne	.L794
	cmpb	$10, 10(%rdi)
	jne	.L794
	cmpb	$11, 11(%rdi)
	jne	.L794
	cmpb	$12, 12(%rdi)
	jne	.L794
	cmpb	$13, 13(%rdi)
	jne	.L794
	cmpb	$14, 14(%rdi)
	jne	.L794
	cmpb	$15, 15(%rdi)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L794:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE25217:
	.size	_ZN2v88internal8compiler19InstructionSelector16TryMatchIdentityEPKh, .-_ZN2v88internal8compiler19InstructionSelector16TryMatchIdentityEPKh
	.section	.text._ZN2v88internal8compiler19InstructionSelector19TryMatch32x4ShuffleEPKhPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector19TryMatch32x4ShuffleEPKhPh
	.type	_ZN2v88internal8compiler19InstructionSelector19TryMatch32x4ShuffleEPKhPh, @function
_ZN2v88internal8compiler19InstructionSelector19TryMatch32x4ShuffleEPKhPh:
.LFB25218:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %eax
	testb	$3, %al
	jne	.L801
	movzbl	1(%rdi), %edx
	movzbl	%al, %ecx
	movl	%edx, %r8d
	subl	%ecx, %r8d
	cmpl	$1, %r8d
	jne	.L801
	movzbl	2(%rdi), %ecx
	movl	%ecx, %r9d
	subl	%edx, %r9d
	cmpl	$1, %r9d
	jne	.L801
	movzbl	3(%rdi), %edx
	subl	%ecx, %edx
	cmpl	$1, %edx
	jne	.L801
	shrb	$2, %al
	movb	%al, (%rsi)
	movzbl	4(%rdi), %eax
	testb	$3, %al
	jne	.L801
	movzbl	5(%rdi), %edx
	movzbl	%al, %ecx
	movl	%edx, %r10d
	subl	%ecx, %r10d
	cmpl	$1, %r10d
	jne	.L801
	movzbl	6(%rdi), %ecx
	movl	%ecx, %r11d
	subl	%edx, %r11d
	cmpl	$1, %r11d
	jne	.L801
	movzbl	7(%rdi), %edx
	subl	%ecx, %edx
	cmpl	$1, %edx
	jne	.L801
	shrb	$2, %al
	movb	%al, 1(%rsi)
	movzbl	8(%rdi), %eax
	testb	$3, %al
	jne	.L801
	movzbl	9(%rdi), %edx
	movzbl	%al, %ecx
	movl	%edx, %r11d
	subl	%ecx, %r11d
	cmpl	$1, %r11d
	jne	.L801
	movzbl	10(%rdi), %ecx
	movl	%ecx, %r10d
	subl	%edx, %r10d
	cmpl	$1, %r10d
	jne	.L801
	movzbl	11(%rdi), %edx
	subl	%ecx, %edx
	cmpl	$1, %edx
	jne	.L801
	shrb	$2, %al
	movb	%al, 2(%rsi)
	movzbl	12(%rdi), %eax
	testb	$3, %al
	jne	.L801
	movzbl	13(%rdi), %edx
	movzbl	%al, %ecx
	movl	%edx, %r9d
	subl	%ecx, %r9d
	cmpl	$1, %r9d
	jne	.L801
	movzbl	14(%rdi), %ecx
	movl	%ecx, %r8d
	subl	%edx, %r8d
	cmpl	$1, %r8d
	jne	.L801
	movzbl	15(%rdi), %edx
	subl	%ecx, %edx
	cmpl	$1, %edx
	jne	.L801
	shrb	$2, %al
	movb	%al, 3(%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE25218:
	.size	_ZN2v88internal8compiler19InstructionSelector19TryMatch32x4ShuffleEPKhPh, .-_ZN2v88internal8compiler19InstructionSelector19TryMatch32x4ShuffleEPKhPh
	.section	.text._ZN2v88internal8compiler19InstructionSelector19TryMatch16x8ShuffleEPKhPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector19TryMatch16x8ShuffleEPKhPh
	.type	_ZN2v88internal8compiler19InstructionSelector19TryMatch16x8ShuffleEPKhPh, @function
_ZN2v88internal8compiler19InstructionSelector19TryMatch16x8ShuffleEPKhPh:
.LFB25219:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %eax
	testb	$1, %al
	jne	.L809
	movzbl	1(%rdi), %edx
	movzbl	%al, %ecx
	subl	%ecx, %edx
	cmpl	$1, %edx
	jne	.L809
	shrb	%al
	movb	%al, (%rsi)
	movzbl	2(%rdi), %eax
	testb	$1, %al
	jne	.L809
	movzbl	3(%rdi), %edx
	movzbl	%al, %ecx
	subl	%ecx, %edx
	cmpl	$1, %edx
	jne	.L809
	shrb	%al
	movb	%al, 1(%rsi)
	movzbl	4(%rdi), %eax
	testb	$1, %al
	jne	.L809
	movzbl	5(%rdi), %edx
	movzbl	%al, %ecx
	subl	%ecx, %edx
	cmpl	$1, %edx
	jne	.L809
	shrb	%al
	movb	%al, 2(%rsi)
	movzbl	6(%rdi), %eax
	testb	$1, %al
	jne	.L809
	movzbl	7(%rdi), %edx
	movzbl	%al, %ecx
	subl	%ecx, %edx
	cmpl	$1, %edx
	jne	.L809
	shrb	%al
	movb	%al, 3(%rsi)
	movzbl	8(%rdi), %eax
	testb	$1, %al
	jne	.L809
	movzbl	9(%rdi), %edx
	movzbl	%al, %ecx
	subl	%ecx, %edx
	cmpl	$1, %edx
	jne	.L809
	shrb	%al
	movb	%al, 4(%rsi)
	movzbl	10(%rdi), %eax
	testb	$1, %al
	jne	.L809
	movzbl	11(%rdi), %edx
	movzbl	%al, %ecx
	subl	%ecx, %edx
	cmpl	$1, %edx
	jne	.L809
	shrb	%al
	movb	%al, 5(%rsi)
	movzbl	12(%rdi), %eax
	testb	$1, %al
	jne	.L809
	movzbl	13(%rdi), %edx
	movzbl	%al, %ecx
	subl	%ecx, %edx
	cmpl	$1, %edx
	jne	.L809
	shrb	%al
	movb	%al, 6(%rsi)
	movzbl	14(%rdi), %eax
	testb	$1, %al
	jne	.L809
	movzbl	15(%rdi), %edx
	movzbl	%al, %ecx
	subl	%ecx, %edx
	cmpl	$1, %edx
	jne	.L809
	shrb	%al
	movb	%al, 7(%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L809:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE25219:
	.size	_ZN2v88internal8compiler19InstructionSelector19TryMatch16x8ShuffleEPKhPh, .-_ZN2v88internal8compiler19InstructionSelector19TryMatch16x8ShuffleEPKhPh
	.section	.text._ZN2v88internal8compiler19InstructionSelector14TryMatchConcatEPKhPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14TryMatchConcatEPKhPh
	.type	_ZN2v88internal8compiler19InstructionSelector14TryMatchConcatEPKhPh, @function
_ZN2v88internal8compiler19InstructionSelector14TryMatchConcatEPKhPh:
.LFB25220:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %r10d
	xorl	%eax, %eax
	testb	%r10b, %r10b
	je	.L811
	leaq	1(%rdi), %rdx
	leaq	16(%rdi), %r9
	movl	%r10d, %ecx
	.p2align 4,,10
	.p2align 3
.L814:
	movzbl	%cl, %eax
	movzbl	(%rdx), %r8d
	movl	%eax, %edi
	addl	$1, %eax
	movl	%r8d, %ecx
	cmpl	%eax, %r8d
	je	.L813
	cmpb	$15, %dil
	jne	.L817
	testb	$15, %r8b
	jne	.L817
.L813:
	addq	$1, %rdx
	cmpq	%rdx, %r9
	jne	.L814
	movb	%r10b, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L817:
	xorl	%eax, %eax
.L811:
	ret
	.cfi_endproc
.LFE25220:
	.size	_ZN2v88internal8compiler19InstructionSelector14TryMatchConcatEPKhPh, .-_ZN2v88internal8compiler19InstructionSelector14TryMatchConcatEPKhPh
	.section	.text._ZN2v88internal8compiler19InstructionSelector13TryMatchBlendEPKh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13TryMatchBlendEPKh
	.type	_ZN2v88internal8compiler19InstructionSelector13TryMatchBlendEPKh, @function
_ZN2v88internal8compiler19InstructionSelector13TryMatchBlendEPKh:
.LFB25221:
	.cfi_startproc
	endbr64
	testb	$15, (%rdi)
	jne	.L836
	movzbl	1(%rdi), %eax
	andl	$15, %eax
	cmpb	$1, %al
	jne	.L836
	movzbl	2(%rdi), %eax
	andl	$15, %eax
	cmpb	$2, %al
	jne	.L836
	movzbl	3(%rdi), %eax
	andl	$15, %eax
	cmpb	$3, %al
	jne	.L836
	movzbl	4(%rdi), %eax
	andl	$15, %eax
	cmpb	$4, %al
	jne	.L836
	movzbl	5(%rdi), %eax
	andl	$15, %eax
	cmpb	$5, %al
	jne	.L836
	movzbl	6(%rdi), %eax
	andl	$15, %eax
	cmpb	$6, %al
	jne	.L836
	movzbl	7(%rdi), %eax
	andl	$15, %eax
	cmpb	$7, %al
	jne	.L836
	movzbl	8(%rdi), %eax
	andl	$15, %eax
	cmpb	$8, %al
	jne	.L836
	movzbl	9(%rdi), %eax
	andl	$15, %eax
	cmpb	$9, %al
	jne	.L836
	movzbl	10(%rdi), %eax
	andl	$15, %eax
	cmpb	$10, %al
	jne	.L836
	movzbl	11(%rdi), %eax
	andl	$15, %eax
	cmpb	$11, %al
	jne	.L836
	movzbl	12(%rdi), %eax
	andl	$15, %eax
	cmpb	$12, %al
	jne	.L836
	movzbl	13(%rdi), %eax
	andl	$15, %eax
	cmpb	$13, %al
	jne	.L836
	movzbl	14(%rdi), %eax
	andl	$15, %eax
	cmpb	$14, %al
	jne	.L836
	movzbl	15(%rdi), %eax
	andl	$15, %eax
	cmpb	$15, %al
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L836:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE25221:
	.size	_ZN2v88internal8compiler19InstructionSelector13TryMatchBlendEPKh, .-_ZN2v88internal8compiler19InstructionSelector13TryMatchBlendEPKh
	.section	.text._ZN2v88internal8compiler19InstructionSelector10Pack4LanesEPKh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector10Pack4LanesEPKh
	.type	_ZN2v88internal8compiler19InstructionSelector10Pack4LanesEPKh, @function
_ZN2v88internal8compiler19InstructionSelector10Pack4LanesEPKh:
.LFB25222:
	.cfi_startproc
	endbr64
	movzbl	3(%rdi), %edx
	movzbl	2(%rdi), %eax
	sall	$8, %edx
	orl	%edx, %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	1(%rdi), %eax
	orl	%edx, %eax
	movzbl	(%rdi), %edx
	sall	$8, %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE25222:
	.size	_ZN2v88internal8compiler19InstructionSelector10Pack4LanesEPKh, .-_ZN2v88internal8compiler19InstructionSelector10Pack4LanesEPKh
	.section	.text._ZNK2v88internal8compiler19InstructionSelector14NeedsPoisoningENS1_13IsSafetyCheckE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8compiler19InstructionSelector14NeedsPoisoningENS1_13IsSafetyCheckE
	.type	_ZNK2v88internal8compiler19InstructionSelector14NeedsPoisoningENS1_13IsSafetyCheckE, @function
_ZNK2v88internal8compiler19InstructionSelector14NeedsPoisoningENS1_13IsSafetyCheckE:
.LFB25223:
	.cfi_startproc
	endbr64
	movl	364(%rdi), %eax
	cmpl	$1, %eax
	je	.L842
	cmpl	$2, %eax
	je	.L840
	testl	%eax, %eax
	je	.L845
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L842:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L845:
	cmpb	$2, %sil
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L840:
	testb	%sil, %sil
	sete	%al
	ret
	.cfi_endproc
.LFE25223:
	.size	_ZNK2v88internal8compiler19InstructionSelector14NeedsPoisoningENS1_13IsSafetyCheckE, .-_ZNK2v88internal8compiler19InstructionSelector14NeedsPoisoningENS1_13IsSafetyCheckE
	.section	.text._ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE7reserveEm,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE7reserveEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE7reserveEm
	.type	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE7reserveEm, @function
_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE7reserveEm:
.LFB27499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$268435455, %rsi
	ja	.L871
	movq	8(%rdi), %r12
	movq	24(%rdi), %rax
	movq	%rdi, %rbx
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L872
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L872:
	.cfi_restore_state
	movq	16(%rdi), %r13
	leaq	0(,%rsi,8), %r15
	xorl	%eax, %eax
	movq	%r13, %r14
	subq	%r12, %r14
	testq	%rsi, %rsi
	je	.L849
	movq	(%rdi), %rdi
	movq	%r15, %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %r15
	ja	.L873
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L849:
	cmpq	%r12, %r13
	je	.L856
	leaq	-8(%r13), %rdx
	leaq	15(%r12), %rcx
	subq	%r12, %rdx
	subq	%rax, %rcx
	shrq	$3, %rdx
	cmpq	$30, %rcx
	jbe	.L860
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdx
	je	.L860
	leaq	1(%rdx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L854:
	movdqu	(%r12,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L854
	movq	%rcx, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r12
	addq	%rax, %rsi
	cmpq	%rdx, %rcx
	je	.L856
	movq	(%r12), %rdx
	movq	%rdx, (%rsi)
.L856:
	movq	%rax, 8(%rbx)
	addq	%rax, %r14
	addq	%r15, %rax
	movq	%r14, 16(%rbx)
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L860:
	.cfi_restore_state
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L853:
	movq	(%r12,%rcx,8), %rsi
	movq	%rsi, (%rax,%rcx,8)
	movq	%rcx, %rsi
	addq	$1, %rcx
	cmpq	%rsi, %rdx
	jne	.L853
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L873:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L849
.L871:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE27499:
	.size	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE7reserveEm, .-_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE7reserveEm
	.section	.text._ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE7reserveEm,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE7reserveEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE7reserveEm
	.type	_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE7reserveEm, @function
_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE7reserveEm:
.LFB27585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	$134217727, %rsi
	ja	.L888
	movq	8(%rdi), %rdx
	movq	24(%rdi), %rax
	movq	%rdi, %rbx
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L889
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L889:
	.cfi_restore_state
	movq	16(%rdi), %r12
	movq	%rsi, %r14
	xorl	%eax, %eax
	salq	$4, %r14
	movq	%r12, %r13
	subq	%rdx, %r13
	testq	%rsi, %rsi
	je	.L877
	movq	(%rdi), %rdi
	movq	%r14, %rsi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %r14
	ja	.L890
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L877:
	movq	%rax, %rcx
	cmpq	%rdx, %r12
	je	.L882
	.p2align 4,,10
	.p2align 3
.L881:
	movq	(%rdx), %r9
	movl	8(%rdx), %r8d
	addq	$16, %rdx
	addq	$16, %rcx
	movzbl	-4(%rdx), %edi
	movzbl	-3(%rdx), %esi
	movq	%r9, -16(%rcx)
	movl	%r8d, -8(%rcx)
	movb	%dil, -4(%rcx)
	movb	%sil, -3(%rcx)
	cmpq	%rdx, %r12
	jne	.L881
.L882:
	movq	%rax, 8(%rbx)
	addq	%rax, %r13
	addq	%r14, %rax
	movq	%r13, 16(%rbx)
	movq	%rax, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L890:
	.cfi_restore_state
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-40(%rbp), %rdx
	jmp	.L877
.L888:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE27585:
	.size	_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE7reserveEm, .-_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE7reserveEm
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8ConstantEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS0_IiS5_EEEES0_ISt17_Rb_tree_iteratorIS6_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8ConstantEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS0_IiS5_EEEES0_ISt17_Rb_tree_iteratorIS6_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8ConstantEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS0_IiS5_EEEES0_ISt17_Rb_tree_iteratorIS6_EbEDpOT_
	.type	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8ConstantEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS0_IiS5_EEEES0_ISt17_Rb_tree_iteratorIS6_EbEDpOT_, @function
_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8ConstantEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS0_IiS5_EEEES0_ISt17_Rb_tree_iteratorIS6_EbEDpOT_:
.LFB28058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L915
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdi)
.L893:
	movl	(%r14), %r15d
	movdqu	8(%r14), %xmm0
	leaq	16(%r13), %r14
	movl	%r15d, 32(%rbx)
	movups	%xmm0, 40(%rbx)
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L895
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L917:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L896
.L918:
	movq	%rax, %r12
.L895:
	movl	32(%r12), %ecx
	cmpl	%ecx, %r15d
	jl	.L917
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L918
.L896:
	testb	%dl, %dl
	jne	.L919
	cmpl	%ecx, %r15d
	jle	.L901
.L904:
	movl	$1, %edi
	cmpq	%r14, %r12
	jne	.L920
.L902:
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%rbx, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 48(%r13)
	addq	$8, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L919:
	.cfi_restore_state
	cmpq	32(%r13), %r12
	je	.L904
.L905:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpl	%r15d, 32(%rax)
	jl	.L921
	movq	%rax, %r12
.L901:
	addq	$8, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L921:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L901
	movl	$1, %edi
	cmpq	%r14, %r12
	je	.L902
.L920:
	xorl	%edi, %edi
	cmpl	32(%r12), %r15d
	setl	%dil
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L916:
	movq	%r14, %r12
	cmpq	32(%r13), %r14
	jne	.L905
	movl	$1, %edi
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L915:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L893
	.cfi_endproc
.LFE28058:
	.size	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8ConstantEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS0_IiS5_EEEES0_ISt17_Rb_tree_iteratorIS6_EbEDpOT_, .-_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8ConstantEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS0_IiS5_EEEES0_ISt17_Rb_tree_iteratorIS6_EbEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_:
.LFB28076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	8(%rdi), %r14
	movq	%r13, %rax
	subq	%r14, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L938
	movq	%rsi, %rcx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	%rdx, %r15
	subq	%r14, %rcx
	testq	%rax, %rax
	je	.L932
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L939
	movl	$2147483632, %esi
	movl	$2147483632, %r9d
.L924:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L940
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L927:
	addq	%rax, %r9
	leaq	16(%rax), %r10
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L939:
	testq	%rdx, %rdx
	jne	.L941
	movl	$16, %r10d
	xorl	%r9d, %r9d
	xorl	%eax, %eax
.L925:
	movl	(%r15), %edi
	movzbl	4(%r15), %esi
	addq	%rax, %rcx
	movq	8(%r15), %rdx
	movl	%edi, (%rcx)
	movb	%sil, 4(%rcx)
	movq	%rdx, 8(%rcx)
	cmpq	%r14, %rbx
	je	.L928
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L929:
	movl	(%rdx), %r8d
	movzbl	4(%rdx), %edi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	-8(%rdx), %rsi
	movl	%r8d, -16(%rcx)
	movb	%dil, -12(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L929
	movq	%rbx, %rdx
	subq	%r14, %rdx
	leaq	16(%rax,%rdx), %r10
.L928:
	cmpq	%r13, %rbx
	je	.L930
	movq	%rbx, %rdx
	movq	%r10, %rcx
	.p2align 4,,10
	.p2align 3
.L931:
	movl	(%rdx), %r8d
	movzbl	4(%rdx), %edi
	addq	$16, %rdx
	addq	$16, %rcx
	movq	-8(%rdx), %rsi
	movl	%r8d, -16(%rcx)
	movb	%dil, -12(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r13
	jne	.L931
	subq	%rbx, %r13
	addq	%r13, %r10
.L930:
	movq	%rax, %xmm0
	movq	%r10, %xmm1
	movq	%r9, 24(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L932:
	.cfi_restore_state
	movl	$16, %esi
	movl	$16, %r9d
	jmp	.L924
.L940:
	movq	%rcx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rcx
	jmp	.L927
.L941:
	cmpq	$134217727, %rdx
	movl	$134217727, %r9d
	cmovbe	%rdx, %r9
	salq	$4, %r9
	movq	%r9, %rsi
	jmp	.L924
.L938:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE28076:
	.size	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.section	.text._ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	.type	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE, @function
_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE:
.LFB23618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	(%rsi), %rdi
	movq	16(%rax), %r12
	movzwl	16(%rdi), %eax
	cmpw	$60, %ax
	ja	.L943
	cmpw	$22, %ax
	jbe	.L944
	subl	$23, %eax
	cmpw	$37, %ax
	ja	.L944
	leaq	.L946(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,"aG",@progbits,_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,comdat
	.align 4
	.align 4
.L946:
	.long	.L954-.L946
	.long	.L956-.L946
	.long	.L952-.L946
	.long	.L950-.L946
	.long	.L951-.L946
	.long	.L950-.L946
	.long	.L944-.L946
	.long	.L949-.L946
	.long	.L948-.L946
	.long	.L947-.L946
	.long	.L947-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L944-.L946
	.long	.L945-.L946
	.section	.text._ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,comdat
	.p2align 4,,10
	.p2align 3
.L943:
	cmpw	$284, %ax
	je	.L955
	cmpw	$428, %ax
	jne	.L944
.L956:
	movq	48(%rdi), %rax
	movl	$1, -48(%rbp)
	movl	$1, %edx
	movl	$19, %ecx
	movb	$19, -44(%rbp)
	movq	%rax, -40(%rbp)
	.p2align 4,,10
	.p2align 3
.L958:
	movq	160(%r12), %rsi
	movq	%rsi, %rbx
	subq	152(%r12), %rbx
	sarq	$4, %rbx
	cmpq	168(%r12), %rsi
	je	.L964
	movl	%edx, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, 160(%r12)
.L965:
	movq	%rbx, %rax
	salq	$32, %rax
	orq	$11, %rax
.L963:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L968
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L954:
	.cfi_restore_state
	movl	44(%rdi), %esi
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-48(%rbp), %edx
	movzbl	-44(%rbp), %ecx
	movq	-40(%rbp), %rax
.L957:
	movl	%edx, -48(%rbp)
	movb	%cl, -44(%rbp)
	movq	%rax, -40(%rbp)
	testl	%edx, %edx
	jne	.L958
	cmpb	$19, %cl
	jne	.L958
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L963
.L952:
	movslq	44(%rdi), %rax
	movl	$2, %edx
	movl	$2, -48(%rbp)
	movl	$19, %ecx
	movb	$19, -44(%rbp)
	movq	%rax, -40(%rbp)
	jmp	.L958
.L949:
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	movb	$19, -44(%rbp)
	movl	$6, %edx
	movl	$19, %ecx
	movl	$6, -48(%rbp)
	movq	%rax, -40(%rbp)
	jmp	.L958
.L948:
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	movb	$19, -44(%rbp)
	movl	$5, %edx
	movl	$19, %ecx
	movl	$5, -48(%rbp)
	movq	%rax, -40(%rbp)
	jmp	.L958
.L947:
	movq	56(%rdi), %rdx
	movq	48(%rdi), %rsi
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1ENS1_26RelocatablePtrConstantInfoE@PLT
	movl	-48(%rbp), %edx
	movzbl	-44(%rbp), %ecx
	movq	-40(%rbp), %rax
	jmp	.L957
.L945:
	call	_ZN2v88internal8compiler25DeadValueRepresentationOfEPKNS1_8OperatorE@PLT
	cmpb	$13, %al
	ja	.L944
	leaq	.L960(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,"aG",@progbits,_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,comdat
	.align 4
	.align 4
.L960:
	.long	.L944-.L960
	.long	.L962-.L960
	.long	.L944-.L960
	.long	.L944-.L960
	.long	.L962-.L960
	.long	.L944-.L960
	.long	.L962-.L960
	.long	.L962-.L960
	.long	.L962-.L960
	.long	.L962-.L960
	.long	.L962-.L960
	.long	.L962-.L960
	.long	.L961-.L960
	.long	.L959-.L960
	.section	.text._ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,"axG",@progbits,_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE,comdat
.L950:
	movq	48(%rdi), %rax
	movl	$3, %edx
	movl	$3, -48(%rbp)
	movl	$19, %ecx
	movb	$19, -44(%rbp)
	movq	%rax, -40(%rbp)
	jmp	.L958
.L951:
	movq	48(%rdi), %rax
	movl	$4, %edx
	movl	$4, -48(%rbp)
	movl	$19, %ecx
	movb	$19, -44(%rbp)
	movq	%rax, -40(%rbp)
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L964:
	leaq	-48(%rbp), %rdx
	leaq	144(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L955:
	call	_ZN2v88internal8compiler20StringConstantBaseOfEPKNS1_8OperatorE@PLT
	movb	$19, -44(%rbp)
	movl	$8, %edx
	movl	$19, %ecx
	movl	$8, -48(%rbp)
	movq	%rax, -40(%rbp)
	jmp	.L958
.L944:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L962:
	leaq	-48(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-48(%rbp), %edx
	movzbl	-44(%rbp), %ecx
	movq	-40(%rbp), %rax
	jmp	.L957
.L959:
	movb	$19, -44(%rbp)
	movl	$3, %edx
	xorl	%eax, %eax
	movl	$19, %ecx
	movl	$3, -48(%rbp)
	movq	$0, -40(%rbp)
	jmp	.L958
.L961:
	movb	$19, -44(%rbp)
	movl	$2, %edx
	xorl	%eax, %eax
	movl	$19, %ecx
	movl	$2, -48(%rbp)
	movq	$0, -40(%rbp)
	jmp	.L958
.L968:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23618:
	.size	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE, .-_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	.section	.text._ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB28984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L1007
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L985
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1008
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L971:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L1009
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L974:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L1008:
	testq	%rdx, %rdx
	jne	.L1010
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L972:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L975
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L988
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L988
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L977:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L977
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L979
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L979:
	leaq	16(%rax,%r8), %rcx
.L975:
	cmpq	%r14, %r12
	je	.L980
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L989
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L989
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L982:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L982
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L984
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L984:
	leaq	8(%rcx,%r9), %rcx
.L980:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L985:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L989:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L981:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L981
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L988:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L976:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L976
	jmp	.L979
.L1009:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L974
.L1007:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1010:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L971
	.cfi_endproc
.LFE28984:
	.size	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal8compiler19InstructionSelector4EmitEPNS1_11InstructionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector4EmitEPNS1_11InstructionE
	.type	_ZN2v88internal8compiler19InstructionSelector4EmitEPNS1_11InstructionE, @function
_ZN2v88internal8compiler19InstructionSelector4EmitEPNS1_11InstructionE:
.LFB25020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -8(%rbp)
	movq	72(%rdi), %rsi
	cmpq	80(%rdi), %rsi
	je	.L1012
	movq	-8(%rbp), %rax
	movq	%rax, (%rsi)
	movq	-8(%rbp), %rax
	addq	$8, 72(%rdi)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1012:
	.cfi_restore_state
	leaq	-8(%rbp), %rdx
	addq	$56, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25020:
	.size	_ZN2v88internal8compiler19InstructionSelector4EmitEPNS1_11InstructionE, .-_ZN2v88internal8compiler19InstructionSelector4EmitEPNS1_11InstructionE
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"InputCountField::is_valid(input_count)"
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0.str1.1,"aMS",@progbits,1
.LC11:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0, @function
_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0:
.LFB32658:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	testl	$-65536, %ecx
	jne	.L1026
	movl	%esi, %r14d
	movq	%rdx, %r15
	movq	%r8, %r9
	movl	$48, %esi
	testq	%rcx, %rcx
	jne	.L1027
.L1017:
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rsi, %rax
	jb	.L1028
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1019:
	pushq	$0
	movq	%rcx, %r8
	xorl	%edx, %edx
	movl	%r14d, %esi
	pushq	$0
	movq	%r15, %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r13, -48(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1020
	movq	%r13, (%rsi)
	addq	$8, 72(%rbx)
.L1021:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	movq	-48(%rbp), %rax
	jne	.L1029
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1027:
	.cfi_restore_state
	leal	40(,%rcx,8), %esi
	movslq	%esi, %rsi
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1020:
	leaq	-48(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1026:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1028:
	movq	%r9, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r9
	movq	%rax, %r13
	jmp	.L1019
.L1029:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE32658:
	.size	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0, .-_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0
	.section	.text._ZN2v88internal8compiler19InstructionSelector11VisitReturnEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector11VisitReturnEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector11VisitReturnEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector11VisitReturnEPNS1_4NodeE:
.LFB25195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	$0, (%rax)
	je	.L1055
	movq	(%rsi), %rax
	movslq	20(%rax), %rax
	movq	%rax, -152(%rbp)
	movq	%rax, %rbx
	leaq	0(,%rax,8), %rsi
.L1031:
	movq	(%r14), %rdi
	movq	16(%rdi), %rax
	movq	%rax, %rcx
	movq	%rax, -88(%rbp)
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rsi, %rax
	jb	.L1058
	addq	-88(%rbp), %rsi
	movq	%rsi, 16(%rdi)
.L1033:
	leaq	32(%r9), %rax
	movq	32(%r9), %r12
	movq	%rax, -96(%rbp)
	movzbl	23(%r9), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1034
	movq	16(%r12), %r12
.L1034:
	movq	(%r12), %rax
	movq	%r9, -104(%rbp)
	movzwl	16(%rax), %eax
	subl	$23, %eax
	cmpl	$1, %eax
	ja	.L1035
	movq	%r12, %rdi
	movq	16(%r14), %r13
	call	_ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE
	movq	-104(%rbp), %r9
	movq	%rax, %rcx
	movq	%rdx, %rax
	testl	%ecx, %ecx
	movq	%rcx, -80(%rbp)
	movl	%ecx, %edx
	movq	%rax, -72(%rbp)
	jne	.L1036
	cmpb	$19, -76(%rbp)
	je	.L1059
.L1036:
	movq	160(%r13), %rsi
	movq	%rsi, %r12
	subq	152(%r13), %r12
	sarq	$4, %r12
	cmpq	168(%r13), %rsi
	je	.L1038
	movq	-72(%rbp), %rax
	movzbl	-76(%rbp), %ecx
	movl	%edx, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, 160(%r13)
.L1039:
	movq	%r12, %rax
	salq	$32, %rax
	orq	$11, %rax
.L1037:
	movq	-88(%rbp), %rsi
	movq	%rax, (%rsi)
.L1043:
	cmpl	$1, %ebx
	jle	.L1051
	leal	-2(%rbx), %eax
	xorl	%r12d, %r12d
	leaq	40(%r9), %r8
	movabsq	$790273982465, %r11
	leaq	8(,%rax,8), %r10
	movq	-88(%rbp), %rax
	leaq	8(%rax), %rsi
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1047:
	salq	$36, %rdx
	orq	%rdx, %rax
	orq	$1, %rax
.L1049:
	movq	208(%r14), %r15
	shrq	$6, %rbx
	movl	$1, %edx
	movl	%edi, %ecx
	salq	%cl, %rdx
	orq	%rdx, (%r15,%rbx,8)
	movq	%rax, 0(%r13)
	cmpq	%r10, %r12
	je	.L1051
.L1052:
	movq	8(%r14), %rax
	movq	%r12, %r13
	movq	(%rax), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rcx
	movzbl	23(%r9), %eax
	addq	%r12, %rcx
	andl	$15, %eax
	addq	$8, %r12
	movl	(%rcx), %r15d
	movzbl	4(%rcx), %ecx
	cmpl	$15, %eax
	je	.L1044
	leaq	(%r8,%r13), %rax
.L1045:
	movq	(%rax), %rdx
	movq	288(%r14), %rax
	addq	%rsi, %r13
	movl	20(%rdx), %edi
	movl	%edi, %ebx
	andl	$16777215, %ebx
	movl	(%rax,%rbx,4), %eax
	cmpl	$-1, %eax
	je	.L1060
.L1046:
	movl	%r15d, %edx
	movl	%eax, %eax
	sarl	%edx
	salq	$3, %rax
	andl	$1, %r15d
	jne	.L1047
	cmpl	$-1, %edx
	je	.L1061
	movabsq	$858993459201, %r15
	salq	$41, %rdx
	orq	%rax, %rdx
	movq	%rdx, %rax
	orq	%rdx, %r15
	orq	%r11, %rax
	cmpb	$12, %cl
	cmovnb	%r15, %rax
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1055:
	movq	$1, -152(%rbp)
	movl	$8, %esi
	movl	$1, %ebx
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	leaq	16(%rax,%r12), %rax
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1060:
	movq	16(%r14), %rdi
	movq	%r8, -144(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%r9, -128(%rbp)
	movq	%r10, -120(%rbp)
	movb	%cl, -105(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%r14), %rdi
	movq	-104(%rbp), %rdx
	movabsq	$790273982465, %r11
	movq	-144(%rbp), %r8
	movq	-136(%rbp), %rsi
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %r10
	movl	%eax, (%rdi,%rbx,4)
	movl	20(%rdx), %edi
	movzbl	-105(%rbp), %ecx
	movl	%edi, %ebx
	andl	$16777215, %ebx
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1061:
	movabsq	$927712935937, %rcx
	orq	%rcx, %rax
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1051:
	cmpq	$65534, -152(%rbp)
	ja	.L1062
	movq	-88(%rbp), %r8
	movq	-152(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	$23, %esi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0
.L1030:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1063
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1035:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE
	movl	20(%r12), %ecx
	movl	$1, %esi
	movq	-104(%rbp), %r9
	movl	%eax, %eax
	salq	%cl, %rsi
	movq	%rcx, %rdx
	salq	$3, %rax
	shrq	$3, %rdx
	andl	$2097144, %edx
	addq	208(%r14), %rdx
	orq	%rsi, (%rdx)
	movq	-88(%rbp), %rsi
	movabsq	$377957122049, %rdx
	orq	%rdx, %rax
	movq	%rax, (%rsi)
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1062:
	movb	$1, 376(%r14)
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1059:
	movslq	-72(%rbp), %rax
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L1037
	.p2align 4,,10
	.p2align 3
.L1058:
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %r9
	movq	%rax, -88(%rbp)
	jmp	.L1033
.L1038:
	leaq	-80(%rbp), %rdx
	leaq	144(%r13), %rdi
	movq	%r9, -104(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-104(%rbp), %r9
	jmp	.L1039
.L1063:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25195:
	.size	_ZN2v88internal8compiler19InstructionSelector11VisitReturnEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector11VisitReturnEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15EmitTableSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15EmitTableSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE
	.type	_ZN2v88internal8compiler19InstructionSelector15EmitTableSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE, @function
_ZN2v88internal8compiler19InstructionSelector15EmitTableSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE:
.LFB25154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movq	16(%rdi), %r12
	addq	$2, %rax
	leaq	0(,%rax,8), %r13
	movq	%rax, -112(%rbp)
	movq	24(%rdi), %rax
	movq	%r13, %rsi
	subq	%r12, %rax
	cmpq	%rax, %r13
	ja	.L1085
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L1066:
	movq	(%rdx), %rax
	movq	%rax, (%r12)
	movq	24(%rcx), %rdx
	movq	16(%r15), %rax
	movslq	4(%rdx), %rdx
	movl	$7, -80(%rbp)
	movb	$19, -76(%rbp)
	movq	%rdx, -72(%rbp)
	movq	160(%rax), %rsi
	movq	%rsi, %rbx
	subq	152(%rax), %rbx
	sarq	$4, %rbx
	cmpq	168(%rax), %rsi
	je	.L1067
	movl	$7, (%rsi)
	movb	$19, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rax)
.L1068:
	salq	$32, %rbx
	leaq	8(%r12), %rdi
	leaq	(%r12,%r13), %rax
	movq	%rbx, %rdx
	movq	%rdi, %rsi
	orq	$11, %rdx
	cmpq	%rdi, %rax
	je	.L1069
	subq	%r12, %rax
	subq	$16, %rax
	movq	%rax, %r9
	shrq	$3, %r9
	addq	$1, %r9
	testq	%rax, %rax
	je	.L1070
	movq	%r9, %rsi
	movq	%rdx, %xmm0
	movq	%rdi, %rax
	shrq	%rsi
	punpcklqdq	%xmm0, %xmm0
	salq	$4, %rsi
	addq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L1071:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L1071
	movq	%r9, %rax
	andq	$-2, %rax
	leaq	(%rdi,%rax,8), %rsi
	cmpq	%rax, %r9
	je	.L1069
.L1070:
	movq	%rdx, (%rsi)
.L1069:
	movq	(%rcx), %rax
	movq	8(%rax), %r14
	movq	16(%rax), %r13
	cmpq	%r13, %r14
	je	.L1073
	leaq	-80(%rbp), %rax
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L1076:
	movl	(%r14), %edx
	subl	8(%rcx), %edx
	movslq	%edx, %rdx
	movq	16(%r15), %rdi
	leaq	16(%r12,%rdx,8), %r9
	movq	8(%r14), %rdx
	movslq	4(%rdx), %rdx
	movl	$7, -80(%rbp)
	movb	$19, -76(%rbp)
	movq	%rdx, -72(%rbp)
	movq	160(%rdi), %rsi
	movq	%rsi, %rbx
	subq	152(%rdi), %rbx
	sarq	$4, %rbx
	cmpq	168(%rdi), %rsi
	je	.L1074
	salq	$32, %rbx
	addq	$16, %r14
	movl	$7, (%rsi)
	orq	$11, %rbx
	movb	$19, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rdi)
	movq	%rbx, (%r9)
	cmpq	%r14, %r13
	jne	.L1076
.L1073:
	cmpq	$65534, -112(%rbp)
	ja	.L1086
.L1077:
	movq	-112(%rbp), %rcx
	movq	%r12, %r8
	xorl	%edx, %edx
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0
.L1064:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1087
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1074:
	.cfi_restore_state
	movq	-104(%rbp), %rdx
	movq	%rcx, -96(%rbp)
	salq	$32, %rbx
	addq	$16, %r14
	addq	$144, %rdi
	movq	%r9, -88(%rbp)
	orq	$11, %rbx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-88(%rbp), %r9
	cmpq	%r14, %r13
	movq	-96(%rbp), %rcx
	movq	%rbx, (%r9)
	jne	.L1076
	cmpq	$65534, -112(%rbp)
	jbe	.L1077
.L1086:
	movb	$1, 376(%r15)
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1067:
	leaq	-80(%rbp), %rdx
	leaq	144(%rax), %rdi
	movq	%rcx, -88(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-88(%rbp), %rcx
	jmp	.L1068
	.p2align 4,,10
	.p2align 3
.L1085:
	movq	%rdx, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L1066
.L1087:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25154:
	.size	_ZN2v88internal8compiler19InstructionSelector15EmitTableSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE, .-_ZN2v88internal8compiler19InstructionSelector15EmitTableSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE
	.section	.text._ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.1,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.1, @function
_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.1:
.LFB32657:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	movl	$48, %esi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	movq	%rdx, %rax
	addq	%r8, %rax
	je	.L1089
	leal	40(,%rax,8), %esi
	movslq	%esi, %rsi
.L1089:
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	%rsi, %rax
	jb	.L1099
	addq	%r14, %rsi
	movq	%rsi, 16(%rdi)
.L1091:
	pushq	%r9
	movl	%r13d, %esi
	xorl	%r9d, %r9d
	movq	%r14, %rdi
	pushq	%r8
	xorl	%r8d, %r8d
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r14, -48(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1092
	movq	%r14, (%rsi)
	addq	$8, 72(%rbx)
.L1093:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	movq	-48(%rbp), %rax
	jne	.L1100
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1092:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
	movq	%rax, %r14
	jmp	.L1091
.L1100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE32657:
	.size	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.1, .-_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.1
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitDeadValueEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitDeadValueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitDeadValueEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitDeadValueEPNS1_4NodeE:
.LFB25207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler25DeadValueRepresentationOfEPKNS1_8OperatorE@PLT
	movl	20(%r13), %ebx
	movq	16(%r12), %rdi
	movl	%eax, %esi
	movq	288(%r12), %rax
	andl	$16777215, %ebx
	movl	(%rax,%rbx,4), %edx
	cmpl	$-1, %edx
	je	.L1126
.L1102:
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movl	20(%r13), %ecx
	movl	$1, %edx
	movq	%rcx, %rax
	salq	%cl, %rdx
	shrq	$3, %rax
	andl	$2097144, %eax
	addq	160(%r12), %rax
	orq	%rdx, (%rax)
	movl	20(%r13), %r14d
	movq	288(%r12), %rax
	andl	$16777215, %r14d
	movl	(%rax,%r14,4), %ebx
	cmpl	$-1, %ebx
	je	.L1127
.L1103:
	movq	0(%r13), %rdi
	movq	16(%r12), %r14
	movzwl	16(%rdi), %eax
	cmpw	$60, %ax
	ja	.L1104
	cmpw	$22, %ax
	jbe	.L1105
	subl	$23, %eax
	cmpw	$37, %ax
	ja	.L1105
	leaq	.L1107(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector14VisitDeadValueEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L1107:
	.long	.L1115-.L1107
	.long	.L1117-.L1107
	.long	.L1113-.L1107
	.long	.L1111-.L1107
	.long	.L1112-.L1107
	.long	.L1111-.L1107
	.long	.L1105-.L1107
	.long	.L1110-.L1107
	.long	.L1109-.L1107
	.long	.L1108-.L1107
	.long	.L1108-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1105-.L1107
	.long	.L1106-.L1107
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitDeadValueEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1104:
	cmpw	$284, %ax
	je	.L1116
	cmpw	$428, %ax
	jne	.L1105
.L1117:
	movq	48(%rdi), %rax
	movl	$19, %edx
	movl	$1, %ecx
	leaq	-64(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L1118:
	leaq	88(%r14), %rdi
	movq	%r13, %rsi
	movl	%ebx, -64(%rbp)
	leaq	2(,%rbx,8), %rbx
	movl	%ecx, -56(%rbp)
	movb	%dl, -52(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8ConstantEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS0_IiS5_EEEES0_ISt17_Rb_tree_iteratorIS6_EbEDpOT_
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movl	$19, %esi
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.1
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1128
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1127:
	.cfi_restore_state
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movl	%eax, %ebx
	movq	288(%r12), %rax
	movl	%ebx, (%rax,%r14,4)
	jmp	.L1103
	.p2align 4,,10
	.p2align 3
.L1126:
	movb	%sil, -73(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movzbl	-73(%rbp), %esi
	movq	-72(%rbp), %rdi
	movl	%eax, %edx
	movq	288(%r12), %rax
	movl	%edx, (%rax,%rbx,4)
	jmp	.L1102
.L1115:
	movl	44(%rdi), %esi
	leaq	-64(%rbp), %r13
.L1125:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-64(%rbp), %ecx
	movzbl	-60(%rbp), %edx
	movq	-56(%rbp), %rax
	jmp	.L1118
.L1113:
	movslq	44(%rdi), %rax
	movl	$19, %edx
	movl	$2, %ecx
	leaq	-64(%rbp), %r13
	jmp	.L1118
.L1111:
	movq	48(%rdi), %rax
	movl	$19, %edx
	movl	$3, %ecx
	leaq	-64(%rbp), %r13
	jmp	.L1118
.L1112:
	movq	48(%rdi), %rax
	movl	$19, %edx
	movl	$4, %ecx
	leaq	-64(%rbp), %r13
	jmp	.L1118
.L1110:
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	leaq	-64(%rbp), %r13
	movl	$19, %edx
	movl	$6, %ecx
	jmp	.L1118
.L1109:
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	leaq	-64(%rbp), %r13
	movl	$19, %edx
	movl	$5, %ecx
	jmp	.L1118
.L1108:
	movq	56(%rdi), %rdx
	movq	48(%rdi), %rsi
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8ConstantC1ENS1_26RelocatablePtrConstantInfoE@PLT
	movl	-64(%rbp), %ecx
	movzbl	-60(%rbp), %edx
	movq	-56(%rbp), %rax
	jmp	.L1118
.L1106:
	call	_ZN2v88internal8compiler25DeadValueRepresentationOfEPKNS1_8OperatorE@PLT
	cmpb	$13, %al
	ja	.L1105
	leaq	.L1119(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector14VisitDeadValueEPNS1_4NodeE
	.align 4
	.align 4
.L1119:
	.long	.L1105-.L1119
	.long	.L1121-.L1119
	.long	.L1105-.L1119
	.long	.L1105-.L1119
	.long	.L1121-.L1119
	.long	.L1105-.L1119
	.long	.L1121-.L1119
	.long	.L1121-.L1119
	.long	.L1121-.L1119
	.long	.L1121-.L1119
	.long	.L1121-.L1119
	.long	.L1121-.L1119
	.long	.L1120-.L1119
	.long	.L1123-.L1119
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitDeadValueEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1116:
	call	_ZN2v88internal8compiler20StringConstantBaseOfEPKNS1_8OperatorE@PLT
	leaq	-64(%rbp), %r13
	movl	$19, %edx
	movl	$8, %ecx
	jmp	.L1118
.L1105:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1121:
	leaq	-64(%rbp), %r13
	xorl	%esi, %esi
	jmp	.L1125
.L1123:
	xorl	%eax, %eax
	movl	$19, %edx
	leaq	-64(%rbp), %r13
	movl	$3, %ecx
	jmp	.L1118
.L1120:
	xorl	%eax, %eax
	movl	$19, %edx
	leaq	-64(%rbp), %r13
	movl	$2, %ecx
	jmp	.L1118
.L1128:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25207:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitDeadValueEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitDeadValueEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitOsrValueEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitOsrValueEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitOsrValueEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitOsrValueEPNS1_4NodeE:
.LFB25181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler15OsrValueIndexOfEPKNS1_8OperatorE@PLT
	movq	8(%r12), %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal8compiler7Linkage19GetOsrValueLocationEi@PLT
	movl	20(%r15), %ecx
	movq	%rax, %r13
	movq	%rax, %r14
	movq	288(%r12), %rax
	movl	%ecx, %ebx
	shrq	$32, %r13
	andl	$16777215, %ebx
	movl	(%rax,%rbx,4), %eax
	cmpl	$-1, %eax
	je	.L1137
.L1130:
	movl	%r14d, %edx
	movl	%eax, %eax
	sarl	%edx
	salq	$3, %rax
	andl	$1, %r14d
	je	.L1138
	salq	$36, %rdx
	orq	%rdx, %rax
	orq	$1, %rax
.L1133:
	shrq	$6, %rbx
	movl	$1, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	160(%r12), %rsi
	salq	%cl, %rdx
	movq	%r12, %rdi
	leaq	-64(%rbp), %rcx
	orq	%rdx, (%rsi,%rbx,8)
	movl	$1, %edx
	movl	$17, %esi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.1
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1139
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1138:
	.cfi_restore_state
	cmpl	$-1, %edx
	je	.L1140
	salq	$41, %rdx
	movq	%rdx, %rsi
	orq	%rax, %rsi
	movabsq	$790273982465, %rax
	orq	%rsi, %rax
	cmpb	$11, %r13b
	jbe	.L1133
	movabsq	$858993459201, %rdx
	movq	%rsi, %rax
	orq	%rdx, %rax
	jmp	.L1133
	.p2align 4,,10
	.p2align 3
.L1137:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%r12), %rdx
	movl	%eax, (%rdx,%rbx,4)
	movl	20(%r15), %ecx
	movl	%ecx, %ebx
	andl	$16777215, %ebx
	jmp	.L1130
	.p2align 4,,10
	.p2align 3
.L1140:
	movabsq	$927712935937, %rdx
	orq	%rdx, %rax
	jmp	.L1133
.L1139:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25181:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitOsrValueEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitOsrValueEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14VisitParameterEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14VisitParameterEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14VisitParameterEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14VisitParameterEPNS1_4NodeE:
.LFB25178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16ParameterIndexOfEPKNS1_8OperatorE@PLT
	movq	8(%r13), %rdi
	movl	%eax, %esi
	movl	%eax, %ebx
	call	_ZNK2v88internal8compiler7Linkage29ParameterHasSecondaryLocationEi@PLT
	testb	%al, %al
	je	.L1142
	movq	8(%r13), %rdi
	movl	%ebx, %esi
	addl	$1, %ebx
	movslq	%ebx, %rbx
	call	_ZNK2v88internal8compiler7Linkage29GetParameterSecondaryLocationEi@PLT
	movq	%rax, %r14
	movq	8(%r13), %rax
	movq	(%rax), %rax
	movl	16(%rax), %r15d
	testq	%rbx, %rbx
	jne	.L1161
.L1143:
	movl	20(%r12), %ecx
	movq	288(%r13), %rax
	movl	%ecx, %ebx
	andl	$16777215, %ebx
	movl	(%rax,%rbx,4), %edx
	cmpl	$-1, %edx
	je	.L1162
.L1144:
	movl	%r14d, %eax
	sarl	%r15d
	leaq	0(,%rdx,8), %r14
	movabsq	$1889785610241, %rdx
	sarl	%eax
	salq	$41, %r15
	salq	$47, %rax
	orq	%r14, %rax
	orq	%r15, %rax
.L1160:
	orq	%rdx, %rax
.L1150:
	movq	160(%r13), %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	shrq	$6, %rbx
	movl	$1, %edx
	salq	%cl, %rdx
	orq	%rdx, (%rsi,%rbx,8)
	leaq	-64(%rbp), %rcx
	movl	$1, %edx
	movl	$17, %esi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.1
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1163
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1161:
	.cfi_restore_state
	movq	24(%rax), %rax
	addq	(%rax), %rbx
	movq	16(%rax), %rax
	movl	-8(%rax,%rbx,8), %r15d
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1142:
	movq	8(%r13), %rax
	addl	$1, %ebx
	movslq	%ebx, %rbx
	movq	(%rax), %rax
	movl	16(%rax), %r15d
	movzbl	20(%rax), %r14d
	testq	%rbx, %rbx
	je	.L1146
	movq	24(%rax), %rax
	movq	(%rax), %rdx
	movq	16(%rax), %rax
	leaq	-1(%rbx,%rdx), %rdx
	leaq	(%rax,%rdx,8), %rax
	movl	(%rax), %r15d
	movzbl	4(%rax), %r14d
.L1146:
	movl	20(%r12), %ecx
	movq	288(%r13), %rax
	movl	%ecx, %ebx
	andl	$16777215, %ebx
	movl	(%rax,%rbx,4), %eax
	cmpl	$-1, %eax
	je	.L1164
.L1147:
	movl	%r15d, %edx
	movl	%eax, %eax
	sarl	%edx
	salq	$3, %rax
	andl	$1, %r15d
	je	.L1165
	salq	$36, %rdx
	orq	%rdx, %rax
	orq	$1, %rax
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1165:
	cmpl	$-1, %edx
	je	.L1166
	salq	$41, %rdx
	orq	%rax, %rdx
	movabsq	$790273982465, %rax
	orq	%rdx, %rax
	cmpb	$11, %r14b
	jbe	.L1150
	movabsq	$858993459201, %rax
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1166:
	movabsq	$927712935937, %rdx
	orq	%rdx, %rax
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1164:
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%r13), %rdx
	movl	%eax, (%rdx,%rbx,4)
	movl	20(%r12), %ecx
	movl	%ecx, %ebx
	andl	$16777215, %ebx
	jmp	.L1147
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	16(%r13), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movl	%eax, %edx
	movq	288(%r13), %rax
	movl	%edx, (%rax,%rbx,4)
	movl	20(%r12), %ecx
	movl	%ecx, %ebx
	andl	$16777215, %ebx
	jmp	.L1144
.L1163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25178:
	.size	_ZN2v88internal8compiler19InstructionSelector14VisitParameterEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14VisitParameterEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0, @function
_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0:
.LFB32459:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	16(%rbp), %r14
	movq	24(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
	testl	$-65536, %r8d
	jne	.L1178
	leaq	(%r8,%r14), %rax
	movl	%esi, %r13d
	movl	$48, %esi
	addq	%rdx, %rax
	jne	.L1179
.L1169:
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	%rsi, %rax
	jb	.L1180
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L1171:
	pushq	%r15
	movl	%r13d, %esi
	movq	%r12, %rdi
	pushq	%r14
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r12, -64(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1172
	movq	%r12, (%rsi)
	addq	$8, 72(%rbx)
.L1173:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	movq	-64(%rbp), %rax
	jne	.L1181
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1179:
	.cfi_restore_state
	leal	40(,%rax,8), %esi
	movslq	%esi, %rsi
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1172:
	leaq	-64(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1178:
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1180:
	movq	%r9, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %r9
	movq	%rax, %r12
	jmp	.L1171
.L1181:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE32459:
	.size	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0, .-_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitProjectionEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitProjectionEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitProjectionEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitProjectionEPNS1_4NodeE:
.LFB25183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1183
	movq	16(%r13), %r13
.L1183:
	movq	0(%r13), %rax
	movzwl	16(%rax), %eax
	cmpw	$328, %ax
	ja	.L1184
	cmpw	$295, %ax
	ja	.L1193
.L1182:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1194
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1184:
	.cfi_restore_state
	subw	$439, %ax
	cmpw	$62, %ax
	ja	.L1182
	movabsq	$9079256848782852097, %rdx
	btq	%rax, %rdx
	jnc	.L1182
.L1187:
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler17ProjectionIndexOfEPKNS1_8OperatorE@PLT
	testq	%rax, %rax
	je	.L1195
	movl	20(%r13), %ecx
	movl	$1, %edx
	movq	%rcx, %rax
	salq	%cl, %rdx
	shrq	$3, %rax
	andl	$2097144, %eax
	addq	208(%r12), %rax
	orq	%rdx, (%rax)
	jmp	.L1182
	.p2align 4,,10
	.p2align 3
.L1193:
	subw	$296, %ax
	cmpw	$32, %ax
	ja	.L1182
	leaq	.L1188(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector15VisitProjectionEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L1188:
	.long	.L1187-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1187-.L1188
	.long	.L1182-.L1188
	.long	.L1187-.L1188
	.long	.L1182-.L1188
	.long	.L1187-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1182-.L1188
	.long	.L1187-.L1188
	.long	.L1182-.L1188
	.long	.L1187-.L1188
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitProjectionEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1195:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE
	movl	20(%r13), %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	movl	$1, %r13d
	movq	%rcx, %rax
	movq	%r13, %rdx
	salq	$3, %r14
	shrq	$3, %rax
	salq	%cl, %rdx
	andl	$2097144, %eax
	addq	208(%r12), %rax
	orq	%rdx, (%rax)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE
	movl	20(%rbx), %ecx
	leaq	-48(%rbp), %r9
	movq	%r12, %rdi
	movl	%eax, %eax
	movl	$1, %r8d
	movl	$17, %esi
	salq	%cl, %r13
	movq	%rcx, %rdx
	salq	$3, %rax
	leaq	-56(%rbp), %rcx
	shrq	$3, %rdx
	andl	$2097144, %edx
	addq	160(%r12), %rdx
	orq	%r13, (%rdx)
	movabsq	$1065151889409, %rdx
	pushq	$0
	orq	%rdx, %rax
	movl	$1, %edx
	pushq	$0
	movq	%rax, -56(%rbp)
	movabsq	$34359738369, %rax
	orq	%rax, %r14
	movq	%r14, -48(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0
	popq	%rax
	popq	%rdx
	jmp	.L1182
.L1194:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25183:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitProjectionEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitProjectionEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_
	.type	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_, @function
_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_:
.LFB25013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$62, %r8
	jbe	.L1197
	movb	$1, 376(%rdi)
.L1196:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1207
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1197:
	.cfi_restore_state
	movq	16(%rdi), %rax
	xorl	%edx, %edx
	testb	$7, -56(%rbp)
	movl	%esi, %r13d
	setne	%dl
	movq	8(%rax), %rdi
	leaq	(%r8,%rdx), %rax
	leal	48(,%rax,8), %esi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	%rax, %rsi
	ja	.L1208
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L1199:
	pushq	%r9
	movl	%r13d, %esi
	leaq	-56(%rbp), %rcx
	leaq	-64(%rbp), %r9
	pushq	%r8
	movq	%r12, %rdi
	movl	$1, %r8d
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r12, -48(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1200
	movq	%r12, (%rsi)
	addq	$8, 72(%rbx)
.L1201:
	movq	-48(%rbp), %rax
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1200:
	leaq	-48(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1208:
	movq	%r9, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %r9
	movq	%rax, %r12
	jmp	.L1199
.L1207:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25013:
	.size	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_, .-_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_mPS3_
	.section	.text._ZN2v88internal8compiler19InstructionSelector24VisitBitcastWordToTaggedEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector24VisitBitcastWordToTaggedEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector24VisitBitcastWordToTaggedEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector24VisitBitcastWordToTaggedEPNS1_4NodeE:
.LFB25161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	32(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1210
	movq	16(%r12), %r12
.L1210:
	movl	20(%r12), %ecx
	movq	288(%rbx), %rax
	movl	%ecx, %r13d
	andl	$16777215, %r13d
	movl	(%rax,%r13,4), %eax
	cmpl	$-1, %eax
	je	.L1219
.L1211:
	movl	%eax, %r12d
	movq	208(%rbx), %rdx
	shrq	$6, %r13
	movabsq	$34359738369, %rax
	salq	$3, %r12
	orq	%rax, %r12
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, (%rdx,%r13,8)
	movl	20(%r14), %ecx
	movq	288(%rbx), %rax
	movl	%ecx, %r13d
	andl	$16777215, %r13d
	movl	(%rax,%r13,4), %eax
	cmpl	$-1, %eax
	je	.L1220
.L1212:
	movq	160(%rbx), %rsi
	shrq	$6, %r13
	movl	$1, %edx
	movl	%eax, %eax
	salq	%cl, %rdx
	salq	$3, %rax
	orq	%rdx, (%rsi,%r13,8)
	movabsq	$1065151889409, %rdx
	orq	%rdx, %rax
	movq	%r12, -56(%rbp)
	movq	%rax, -64(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$55, %rax
	jbe	.L1221
	leaq	56(%r12), %rax
	movq	%rax, 16(%rdi)
.L1214:
	pushq	$0
	movl	$1, %edx
	movl	$17, %esi
	leaq	-64(%rbp), %rcx
	pushq	$0
	leaq	-56(%rbp), %r9
	movl	$1, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r12, -48(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1215
	movq	%r12, (%rsi)
	addq	$8, 72(%rbx)
.L1209:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1222
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1219:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%rbx), %rdx
	movl	%eax, (%rdx,%r13,4)
	movl	20(%r12), %ecx
	movl	%ecx, %r13d
	andl	$16777215, %r13d
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1220:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%rbx), %rdx
	movl	%eax, (%rdx,%r13,4)
	movl	20(%r14), %ecx
	movl	%ecx, %r13d
	andl	$16777215, %r13d
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1215:
	leaq	-48(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1221:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1214
.L1222:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25161:
	.size	_ZN2v88internal8compiler19InstructionSelector24VisitBitcastWordToTaggedEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector24VisitBitcastWordToTaggedEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitConstantEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitConstantEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitConstantEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitConstantEPNS1_4NodeE:
.LFB25184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movl	20(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rcx, %rax
	salq	%cl, %rdx
	shrq	$3, %rax
	andl	$2097144, %eax
	addq	160(%rdi), %rax
	orq	%rdx, (%rax)
	movl	20(%rsi), %r14d
	movq	288(%rdi), %rax
	andl	$16777215, %r14d
	movl	(%rax,%r14,4), %r12d
	cmpl	$-1, %r12d
	je	.L1251
.L1224:
	movq	0(%r13), %rdi
	movq	16(%rbx), %r14
	movzwl	16(%rdi), %eax
	cmpw	$60, %ax
	ja	.L1225
	cmpw	$22, %ax
	jbe	.L1226
	subl	$23, %eax
	cmpw	$37, %ax
	ja	.L1226
	leaq	.L1228(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector13VisitConstantEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L1228:
	.long	.L1236-.L1228
	.long	.L1238-.L1228
	.long	.L1234-.L1228
	.long	.L1232-.L1228
	.long	.L1233-.L1228
	.long	.L1232-.L1228
	.long	.L1226-.L1228
	.long	.L1231-.L1228
	.long	.L1230-.L1228
	.long	.L1229-.L1228
	.long	.L1229-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1226-.L1228
	.long	.L1227-.L1228
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitConstantEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1225:
	cmpw	$284, %ax
	je	.L1237
	cmpw	$428, %ax
	jne	.L1226
.L1238:
	movq	48(%rdi), %rax
	movl	$19, %edx
	movl	$1, %ecx
	leaq	-64(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L1239:
	leaq	88(%r14), %rdi
	movq	%r13, %rsi
	movl	%r12d, -64(%rbp)
	leaq	2(,%r12,8), %r12
	movl	%ecx, -56(%rbp)
	movb	%dl, -52(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZNSt8_Rb_treeIiSt4pairIKiN2v88internal8compiler8ConstantEESt10_Select1stIS6_ESt4lessIiENS3_13ZoneAllocatorIS6_EEE17_M_emplace_uniqueIJS0_IiS5_EEEES0_ISt17_Rb_tree_iteratorIS6_EbEDpOT_
	movq	16(%rbx), %rax
	movq	%r12, -72(%rbp)
	movq	8(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L1252
	leaq	48(%r12), %rax
	movq	%rax, 16(%rdi)
.L1244:
	pushq	$0
	movl	$1, %edx
	movl	$17, %esi
	leaq	-72(%rbp), %rcx
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r12, -64(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1245
	movq	%r12, (%rsi)
	addq	$8, 72(%rbx)
.L1223:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1253
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1251:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movl	%eax, %r12d
	movq	288(%rbx), %rax
	movl	%r12d, (%rax,%r14,4)
	jmp	.L1224
.L1236:
	movl	44(%rdi), %esi
	leaq	-64(%rbp), %r13
.L1250:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-64(%rbp), %ecx
	movzbl	-60(%rbp), %edx
	movq	-56(%rbp), %rax
	jmp	.L1239
.L1234:
	movslq	44(%rdi), %rax
	movl	$19, %edx
	movl	$2, %ecx
	leaq	-64(%rbp), %r13
	jmp	.L1239
.L1232:
	movq	48(%rdi), %rax
	movl	$19, %edx
	movl	$3, %ecx
	leaq	-64(%rbp), %r13
	jmp	.L1239
.L1233:
	movq	48(%rdi), %rax
	movl	$19, %edx
	movl	$4, %ecx
	leaq	-64(%rbp), %r13
	jmp	.L1239
.L1231:
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	leaq	-64(%rbp), %r13
	movl	$19, %edx
	movl	$6, %ecx
	jmp	.L1239
.L1230:
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	leaq	-64(%rbp), %r13
	movl	$19, %edx
	movl	$5, %ecx
	jmp	.L1239
.L1229:
	movq	56(%rdi), %rdx
	movq	48(%rdi), %rsi
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8ConstantC1ENS1_26RelocatablePtrConstantInfoE@PLT
	movl	-64(%rbp), %ecx
	movzbl	-60(%rbp), %edx
	movq	-56(%rbp), %rax
	jmp	.L1239
.L1227:
	call	_ZN2v88internal8compiler25DeadValueRepresentationOfEPKNS1_8OperatorE@PLT
	cmpb	$13, %al
	ja	.L1226
	leaq	.L1240(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector13VisitConstantEPNS1_4NodeE
	.align 4
	.align 4
.L1240:
	.long	.L1226-.L1240
	.long	.L1242-.L1240
	.long	.L1226-.L1240
	.long	.L1226-.L1240
	.long	.L1242-.L1240
	.long	.L1226-.L1240
	.long	.L1242-.L1240
	.long	.L1242-.L1240
	.long	.L1242-.L1240
	.long	.L1242-.L1240
	.long	.L1242-.L1240
	.long	.L1242-.L1240
	.long	.L1241-.L1240
	.long	.L1248-.L1240
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitConstantEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1245:
	leaq	56(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1237:
	call	_ZN2v88internal8compiler20StringConstantBaseOfEPKNS1_8OperatorE@PLT
	leaq	-64(%rbp), %r13
	movl	$19, %edx
	movl	$8, %ecx
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1252:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1244
.L1226:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1242:
	leaq	-64(%rbp), %r13
	xorl	%esi, %esi
	jmp	.L1250
.L1248:
	xorl	%eax, %eax
	movl	$19, %edx
	leaq	-64(%rbp), %r13
	movl	$3, %ecx
	jmp	.L1239
.L1241:
	xorl	%eax, %eax
	movl	$19, %edx
	leaq	-64(%rbp), %r13
	movl	$2, %ecx
	jmp	.L1239
.L1253:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25184:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitConstantEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitConstantEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector21VisitLoadFramePointerEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector21VisitLoadFramePointerEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector21VisitLoadFramePointerEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector21VisitLoadFramePointerEPNS1_4NodeE:
.LFB25131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	20(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	288(%rdi), %rax
	movl	%ecx, %r12d
	andl	$16777215, %r12d
	movl	(%rax,%r12,4), %eax
	cmpl	$-1, %eax
	je	.L1262
.L1255:
	movq	160(%rbx), %rsi
	shrq	$6, %r12
	movl	$1, %edx
	movl	%eax, %eax
	salq	%cl, %rdx
	salq	$3, %rax
	orq	%rdx, (%rsi,%r12,8)
	movabsq	$927712935937, %rdx
	orq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L1263
	leaq	48(%r12), %rax
	movq	%rax, 16(%rdi)
.L1257:
	pushq	$0
	movl	$1, %edx
	movl	$24, %esi
	leaq	-56(%rbp), %rcx
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r12, -48(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1258
	movq	%r12, (%rsi)
	addq	$8, 72(%rbx)
.L1254:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1264
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1262:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	movq	%rsi, %r13
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%rbx), %rdx
	movl	%eax, (%rdx,%r12,4)
	movl	20(%r13), %ecx
	movl	%ecx, %r12d
	andl	$16777215, %r12d
	jmp	.L1255
	.p2align 4,,10
	.p2align 3
.L1258:
	leaq	-48(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1263:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1257
.L1264:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25131:
	.size	_ZN2v88internal8compiler19InstructionSelector21VisitLoadFramePointerEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector21VisitLoadFramePointerEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector27VisitLoadParentFramePointerEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector27VisitLoadParentFramePointerEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector27VisitLoadParentFramePointerEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector27VisitLoadParentFramePointerEPNS1_4NodeE:
.LFB25132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	20(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	288(%rdi), %rax
	movl	%ecx, %r12d
	andl	$16777215, %r12d
	movl	(%rax,%r12,4), %eax
	cmpl	$-1, %eax
	je	.L1273
.L1266:
	movq	160(%rbx), %rsi
	shrq	$6, %r12
	movl	$1, %edx
	movl	%eax, %eax
	salq	%cl, %rdx
	salq	$3, %rax
	orq	%rdx, (%rsi,%r12,8)
	movabsq	$927712935937, %rdx
	orq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L1274
	leaq	48(%r12), %rax
	movq	%rax, 16(%rdi)
.L1268:
	pushq	$0
	movl	$1, %edx
	movl	$25, %esi
	leaq	-56(%rbp), %rcx
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r12, -48(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1269
	movq	%r12, (%rsi)
	addq	$8, 72(%rbx)
.L1265:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1275
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1273:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	movq	%rsi, %r13
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%rbx), %rdx
	movl	%eax, (%rdx,%r12,4)
	movl	20(%r13), %ecx
	movl	%ecx, %r12d
	andl	$16777215, %r12d
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1269:
	leaq	-48(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1274:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1268
.L1275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25132:
	.size	_ZN2v88internal8compiler19InstructionSelector27VisitLoadParentFramePointerEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector27VisitLoadParentFramePointerEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitIfExceptionEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitIfExceptionEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitIfExceptionEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitIfExceptionEPNS1_4NodeE:
.LFB25180:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	20(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	288(%rdi), %rax
	movl	%ecx, %r12d
	andl	$16777215, %r12d
	movl	(%rax,%r12,4), %eax
	cmpl	$-1, %eax
	je	.L1284
.L1277:
	movq	160(%rbx), %rsi
	shrq	$6, %r12
	movl	$1, %edx
	movl	%eax, %eax
	salq	%cl, %rdx
	salq	$3, %rax
	orq	%rdx, (%rsi,%r12,8)
	movabsq	$790273982465, %rdx
	orq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L1285
	leaq	48(%r12), %rax
	movq	%rax, 16(%rdi)
.L1279:
	pushq	$0
	movl	$1, %edx
	movl	$17, %esi
	leaq	-56(%rbp), %rcx
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r12, -48(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1280
	movq	%r12, (%rsi)
	addq	$8, 72(%rbx)
.L1276:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1286
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1284:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	movq	%rsi, %r13
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%rbx), %rdx
	movl	%eax, (%rdx,%r12,4)
	movl	20(%r13), %ecx
	movl	%ecx, %r12d
	andl	$16777215, %r12d
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1280:
	leaq	-48(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1285:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1279
.L1286:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25180:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitIfExceptionEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitIfExceptionEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_
	.type	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_, @function
_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_:
.LFB25014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%rdx, -88(%rbp)
	movq	16(%rbp), %r14
	movq	%fs:40, %rdx
	movq	%rdx, -40(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	cmpq	$62, %r9
	jbe	.L1288
	movb	$1, 376(%rdi)
	xorl	%eax, %eax
.L1287:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1298
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1288:
	.cfi_restore_state
	xorl	%edx, %edx
	testb	$7, %al
	movq	16(%rdi), %rax
	movl	%esi, %r12d
	setne	%dl
	movq	8(%rax), %rdi
	leaq	1(%rdx,%r9), %rax
	leal	48(,%rax,8), %esi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L1299
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1290:
	pushq	%r14
	movl	%r12d, %esi
	leaq	-88(%rbp), %rcx
	movl	$2, %r8d
	pushq	%r9
	movq	%r13, %rdi
	leaq	-64(%rbp), %r9
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r13, -72(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1291
	movq	%r13, (%rsi)
	addq	$8, 72(%rbx)
.L1292:
	movq	-72(%rbp), %rax
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1299:
	movq	%r9, -104(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %r9
	movq	%rax, %r13
	jmp	.L1290
	.p2align 4,,10
	.p2align 3
.L1291:
	leaq	-72(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1292
.L1298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25014:
	.size	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_, .-_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_mPS3_
	.section	.text._ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_mPS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_mPS3_
	.type	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_mPS3_, @function
_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_mPS3_:
.LFB25015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %r14
	movq	24(%rbp), %r15
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	cmpq	$62, %r14
	jbe	.L1301
	movb	$1, 376(%rdi)
	xorl	%eax, %eax
.L1300:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1311
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1301:
	.cfi_restore_state
	xorl	%edx, %edx
	testb	$7, %al
	movq	16(%rdi), %rax
	movl	%esi, %r12d
	setne	%dl
	movq	8(%rax), %rdi
	leaq	2(%rdx,%r14), %rax
	leal	48(,%rax,8), %esi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L1312
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1303:
	pushq	%r15
	movl	%r12d, %esi
	leaq	-104(%rbp), %rcx
	leaq	-80(%rbp), %r9
	pushq	%r14
	movl	$3, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r13, -88(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1304
	movq	%r13, (%rsi)
	addq	$8, 72(%rbx)
.L1305:
	movq	-88(%rbp), %rax
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1312:
	movq	%rdx, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1304:
	leaq	-88(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1305
.L1311:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25015:
	.size	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_mPS3_, .-_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_mPS3_
	.section	.text._ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_mPS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_mPS3_
	.type	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_mPS3_, @function
_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_mPS3_:
.LFB25016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	24(%rbp), %r14
	movq	32(%rbp), %r15
	movq	%rdx, -120(%rbp)
	movq	16(%rbp), %rdx
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	%rcx, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%rdx, -72(%rbp)
	cmpq	$62, %r14
	jbe	.L1314
	movb	$1, 376(%rbx)
	xorl	%eax, %eax
.L1313:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1324
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1314:
	.cfi_restore_state
	xorl	%edx, %edx
	testb	$7, %al
	movq	16(%rbx), %rax
	movl	%esi, %r12d
	setne	%dl
	movq	8(%rax), %rdi
	leaq	3(%rdx,%r14), %rax
	leal	48(,%rax,8), %esi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L1325
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1316:
	pushq	%r15
	movl	%r12d, %esi
	leaq	-120(%rbp), %rcx
	leaq	-96(%rbp), %r9
	pushq	%r14
	movl	$4, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r13, -104(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1317
	movq	%r13, (%rsi)
	addq	$8, 72(%rbx)
.L1318:
	movq	-104(%rbp), %rax
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1325:
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L1316
	.p2align 4,,10
	.p2align 3
.L1317:
	leaq	-104(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1318
.L1324:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25016:
	.size	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_mPS3_, .-_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_mPS3_
	.section	.text._ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_S3_mPS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_S3_mPS3_
	.type	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_S3_mPS3_, @function
_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_S3_mPS3_:
.LFB25017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	32(%rbp), %r14
	movq	40(%rbp), %r15
	movq	%rdx, -120(%rbp)
	movq	16(%rbp), %rdx
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	%rdx, -72(%rbp)
	movq	24(%rbp), %rdx
	movq	%rcx, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%rdx, -64(%rbp)
	cmpq	$62, %r14
	jbe	.L1327
	movb	$1, 376(%rbx)
	xorl	%eax, %eax
.L1326:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1337
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1327:
	.cfi_restore_state
	xorl	%edx, %edx
	testb	$7, %al
	movq	16(%rbx), %rax
	movl	%esi, %r12d
	setne	%dl
	movq	8(%rax), %rdi
	leaq	4(%rdx,%r14), %rax
	leal	48(,%rax,8), %esi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L1338
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1329:
	pushq	%r15
	movl	%r12d, %esi
	leaq	-120(%rbp), %rcx
	leaq	-96(%rbp), %r9
	pushq	%r14
	movl	$5, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r13, -104(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1330
	movq	%r13, (%rsi)
	addq	$8, 72(%rbx)
.L1331:
	movq	-104(%rbp), %rax
	jmp	.L1326
	.p2align 4,,10
	.p2align 3
.L1338:
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-128(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1330:
	leaq	-104(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1331
.L1337:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25017:
	.size	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_S3_mPS3_, .-_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_S3_mPS3_
	.section	.text._ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandEmPS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandEmPS3_
	.type	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandEmPS3_, @function
_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandEmPS3_:
.LFB25012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%rdx, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$62, %rcx
	ja	.L1351
	movq	16(%rdi), %rax
	xorl	%edx, %edx
	testb	$7, -56(%rbp)
	movl	%esi, %r14d
	setne	%dl
	movq	%rcx, %r12
	movl	$48, %esi
	movq	8(%rax), %rdi
	movq	%rcx, %rax
	addq	%rdx, %rax
	jne	.L1352
.L1342:
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L1353
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1344:
	pushq	%r8
	movl	%r14d, %esi
	leaq	-56(%rbp), %rcx
	xorl	%r9d, %r9d
	pushq	%r12
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r13, -48(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1345
	movq	%r13, (%rsi)
	addq	$8, 72(%rbx)
.L1346:
	movq	-48(%rbp), %rax
.L1339:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1354
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1352:
	.cfi_restore_state
	leal	40(,%rax,8), %esi
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1351:
	movb	$1, 376(%rdi)
	jmp	.L1339
	.p2align 4,,10
	.p2align 3
.L1345:
	leaq	-48(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1353:
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	jmp	.L1344
.L1354:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25012:
	.size	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandEmPS3_, .-_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandEmPS3_
	.section	.text._ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_S3_S3_mPS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_S3_S3_mPS3_
	.type	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_S3_S3_mPS3_, @function
_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_S3_S3_mPS3_:
.LFB25018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%rdx, -136(%rbp)
	movq	16(%rbp), %rdx
	movq	40(%rbp), %r14
	movq	48(%rbp), %r15
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	%rdx, -88(%rbp)
	movq	24(%rbp), %rdx
	movq	%rcx, -112(%rbp)
	movq	%rdx, -80(%rbp)
	movq	32(%rbp), %rdx
	movq	%r8, -104(%rbp)
	movq	%r9, -96(%rbp)
	movq	%rdx, -72(%rbp)
	cmpq	$62, %r14
	jbe	.L1356
	movb	$1, 376(%rbx)
	xorl	%eax, %eax
.L1355:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1366
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1356:
	.cfi_restore_state
	xorl	%edx, %edx
	testb	$7, %al
	movq	16(%rbx), %rax
	movl	%esi, %r12d
	setne	%dl
	movq	8(%rax), %rdi
	leaq	5(%rdx,%r14), %rax
	leal	48(,%rax,8), %esi
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L1367
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1358:
	pushq	%r15
	movl	%r12d, %esi
	leaq	-136(%rbp), %rcx
	leaq	-112(%rbp), %r9
	pushq	%r14
	movl	$6, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r13, -120(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1359
	movq	%r13, (%rsi)
	addq	$8, 72(%rbx)
.L1360:
	movq	-120(%rbp), %rax
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1367:
	movq	%rdx, -144(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-144(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L1358
	.p2align 4,,10
	.p2align 3
.L1359:
	leaq	-120(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1360
.L1366:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25018:
	.size	_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_S3_S3_mPS3_, .-_ZN2v88internal8compiler19InstructionSelector4EmitEiNS1_18InstructionOperandES3_S3_S3_S3_S3_S3_mPS3_
	.section	.text._ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_
	.type	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_, @function
_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_:
.LFB25019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	16(%rbp), %r14
	movq	24(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$65534, %r8
	seta	%sil
	cmpq	$62, %r14
	seta	%al
	orb	%al, %sil
	jne	.L1379
	cmpq	$254, %rdx
	ja	.L1379
	movq	16(%rdi), %rax
	movl	$48, %esi
	movq	8(%rax), %rdi
	leaq	(%r8,%r14), %rax
	addq	%rdx, %rax
	je	.L1372
	leal	40(,%rax,8), %esi
.L1372:
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L1382
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L1374:
	pushq	%r15
	movl	%r12d, %esi
	movq	%r13, %rdi
	pushq	%r14
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r13, -64(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1375
	movq	%r13, (%rsi)
	addq	$8, 72(%rbx)
.L1376:
	movq	-64(%rbp), %rax
.L1368:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1383
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1379:
	.cfi_restore_state
	movb	$1, 376(%rbx)
	xorl	%eax, %eax
	jmp	.L1368
	.p2align 4,,10
	.p2align 3
.L1382:
	movq	%r9, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %r9
	movq	%rax, %r13
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1375:
	leaq	-64(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1376
.L1383:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25019:
	.size	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_, .-_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_
	.section	.text._ZN2v88internal8compiler19InstructionSelector16VisitUnreachableEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16VisitUnreachableEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector16VisitUnreachableEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector16VisitUnreachableEPNS1_4NodeE:
.LFB32649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	$0, -40(%rbp)
	movq	8(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L1391
	leaq	48(%r12), %rax
	movq	%rax, 16(%rdi)
.L1386:
	pushq	$0
	xorl	%edx, %edx
	movl	$19, %esi
	leaq	-40(%rbp), %rcx
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r12, -32(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1387
	movq	%r12, (%rsi)
	addq	$8, 72(%rbx)
.L1384:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1392
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1387:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1384
	.p2align 4,,10
	.p2align 3
.L1391:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1386
.L1392:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE32649:
	.size	_ZN2v88internal8compiler19InstructionSelector16VisitUnreachableEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector16VisitUnreachableEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector10VisitThrowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector10VisitThrowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector10VisitThrowEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector10VisitThrowEPNS1_4NodeE:
.LFB25203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	$0, -40(%rbp)
	movq	8(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L1400
	leaq	48(%r12), %rax
	movq	%rax, 16(%rdi)
.L1395:
	pushq	$0
	xorl	%edx, %edx
	movl	$21, %esi
	leaq	-40(%rbp), %rcx
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r12, -32(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1396
	movq	%r12, (%rsi)
	addq	$8, 72(%rbx)
.L1393:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1401
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1396:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1393
	.p2align 4,,10
	.p2align 3
.L1400:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1395
.L1401:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25203:
	.size	_ZN2v88internal8compiler19InstructionSelector10VisitThrowEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector10VisitThrowEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitDebugBreakEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitDebugBreakEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitDebugBreakEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitDebugBreakEPNS1_4NodeE:
.LFB25204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	$0, -40(%rbp)
	movq	8(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L1409
	leaq	48(%r12), %rax
	movq	%rax, 16(%rdi)
.L1404:
	pushq	$0
	xorl	%edx, %edx
	movl	$19, %esi
	leaq	-40(%rbp), %rcx
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r12, -32(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1405
	movq	%r12, (%rsi)
	addq	$8, 72(%rbx)
.L1402:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1410
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1405:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1402
	.p2align 4,,10
	.p2align 3
.L1409:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1404
.L1410:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25204:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitDebugBreakEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitDebugBreakEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitCommentEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitCommentEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitCommentEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitCommentEPNS1_4NodeE:
.LFB25208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	16(%rdi), %r13
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdi), %eax
	cmpw	$60, %ax
	ja	.L1412
	cmpw	$22, %ax
	jbe	.L1413
	subl	$23, %eax
	cmpw	$37, %ax
	ja	.L1413
	leaq	.L1415(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector12VisitCommentEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L1415:
	.long	.L1423-.L1415
	.long	.L1425-.L1415
	.long	.L1421-.L1415
	.long	.L1419-.L1415
	.long	.L1420-.L1415
	.long	.L1419-.L1415
	.long	.L1413-.L1415
	.long	.L1418-.L1415
	.long	.L1417-.L1415
	.long	.L1416-.L1415
	.long	.L1416-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1413-.L1415
	.long	.L1414-.L1415
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitCommentEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L1412:
	cmpw	$284, %ax
	je	.L1424
	cmpw	$428, %ax
	jne	.L1413
.L1425:
	movq	48(%rdi), %rax
	movl	$1, -64(%rbp)
	movl	$19, %ecx
	movl	$1, %edx
	movb	$19, -60(%rbp)
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L1427:
	movq	160(%r13), %rsi
	movq	%rsi, %r12
	subq	152(%r13), %r12
	sarq	$4, %r12
	cmpq	168(%r13), %rsi
	je	.L1433
	movl	%edx, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, 160(%r13)
.L1434:
	movq	%r12, %rax
	salq	$32, %rax
	orq	$11, %rax
.L1432:
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L1441
	leaq	48(%r12), %rax
	movq	%rax, 16(%rdi)
.L1436:
	pushq	$0
	xorl	%edx, %edx
	movl	$20, %esi
	leaq	-72(%rbp), %r9
	pushq	$0
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r12, -64(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1437
	movq	%r12, (%rsi)
	addq	$8, 72(%rbx)
.L1411:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1442
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1423:
	.cfi_restore_state
	movl	44(%rdi), %esi
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-64(%rbp), %edx
	movzbl	-60(%rbp), %ecx
	movq	-56(%rbp), %rax
.L1426:
	movl	%edx, -64(%rbp)
	movb	%cl, -60(%rbp)
	movq	%rax, -56(%rbp)
	testl	%edx, %edx
	jne	.L1427
	cmpb	$19, %cl
	jne	.L1427
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L1432
.L1421:
	movslq	44(%rdi), %rax
	movl	$19, %ecx
	movl	$2, -64(%rbp)
	movl	$2, %edx
	movb	$19, -60(%rbp)
	movq	%rax, -56(%rbp)
	jmp	.L1427
.L1418:
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	movb	$19, -60(%rbp)
	movl	$19, %ecx
	movl	$6, %edx
	movl	$6, -64(%rbp)
	movq	%rax, -56(%rbp)
	jmp	.L1427
.L1417:
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	movb	$19, -60(%rbp)
	movl	$19, %ecx
	movl	$5, %edx
	movl	$5, -64(%rbp)
	movq	%rax, -56(%rbp)
	jmp	.L1427
.L1416:
	movq	56(%rdi), %rdx
	movq	48(%rdi), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal8compiler8ConstantC1ENS1_26RelocatablePtrConstantInfoE@PLT
	movl	-64(%rbp), %edx
	movzbl	-60(%rbp), %ecx
	movq	-56(%rbp), %rax
	jmp	.L1426
.L1414:
	call	_ZN2v88internal8compiler25DeadValueRepresentationOfEPKNS1_8OperatorE@PLT
	cmpb	$13, %al
	ja	.L1413
	leaq	.L1429(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector12VisitCommentEPNS1_4NodeE
	.align 4
	.align 4
.L1429:
	.long	.L1413-.L1429
	.long	.L1431-.L1429
	.long	.L1413-.L1429
	.long	.L1413-.L1429
	.long	.L1431-.L1429
	.long	.L1413-.L1429
	.long	.L1431-.L1429
	.long	.L1431-.L1429
	.long	.L1431-.L1429
	.long	.L1431-.L1429
	.long	.L1431-.L1429
	.long	.L1431-.L1429
	.long	.L1430-.L1429
	.long	.L1428-.L1429
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitCommentEPNS1_4NodeE
.L1419:
	movq	48(%rdi), %rax
	movl	$19, %ecx
	movl	$3, -64(%rbp)
	movl	$3, %edx
	movb	$19, -60(%rbp)
	movq	%rax, -56(%rbp)
	jmp	.L1427
.L1420:
	movq	48(%rdi), %rax
	movl	$19, %ecx
	movl	$4, -64(%rbp)
	movl	$4, %edx
	movb	$19, -60(%rbp)
	movq	%rax, -56(%rbp)
	jmp	.L1427
	.p2align 4,,10
	.p2align 3
.L1437:
	leaq	-64(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1433:
	leaq	-64(%rbp), %rdx
	leaq	144(%r13), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1434
	.p2align 4,,10
	.p2align 3
.L1424:
	call	_ZN2v88internal8compiler20StringConstantBaseOfEPKNS1_8OperatorE@PLT
	movb	$19, -60(%rbp)
	movl	$19, %ecx
	movl	$8, %edx
	movl	$8, -64(%rbp)
	movq	%rax, -56(%rbp)
	jmp	.L1427
	.p2align 4,,10
	.p2align 3
.L1441:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1436
.L1413:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1431:
	leaq	-64(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-64(%rbp), %edx
	movzbl	-60(%rbp), %ecx
	movq	-56(%rbp), %rax
	jmp	.L1426
.L1428:
	movb	$19, -60(%rbp)
	movl	$19, %ecx
	movl	$3, %edx
	xorl	%eax, %eax
	movl	$3, -64(%rbp)
	movq	$0, -56(%rbp)
	jmp	.L1427
.L1430:
	movb	$19, -60(%rbp)
	movl	$19, %ecx
	movl	$2, %edx
	xorl	%eax, %eax
	movl	$2, -64(%rbp)
	movq	$0, -56(%rbp)
	jmp	.L1427
.L1442:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25208:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitCommentEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12VisitCommentEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector9VisitGotoEPNS1_10BasicBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector9VisitGotoEPNS1_10BasicBlockE
	.type	_ZN2v88internal8compiler19InstructionSelector9VisitGotoEPNS1_10BasicBlockE, @function
_ZN2v88internal8compiler19InstructionSelector9VisitGotoEPNS1_10BasicBlockE:
.LFB25194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movslq	4(%rsi), %rax
	movl	$7, -48(%rbp)
	movb	$19, -44(%rbp)
	movq	%rax, -40(%rbp)
	movq	160(%rdi), %rsi
	movq	%rsi, %r12
	subq	152(%rdi), %r12
	sarq	$4, %r12
	cmpq	168(%rdi), %rsi
	je	.L1444
	movl	$7, (%rsi)
	movb	$19, 4(%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, 160(%rdi)
.L1445:
	movq	16(%rbx), %rax
	salq	$32, %r12
	movq	$0, -64(%rbp)
	orq	$11, %r12
	movq	8(%rax), %rdi
	movq	%r12, -56(%rbp)
	movq	24(%rdi), %rax
	movq	16(%rdi), %r12
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L1452
	leaq	48(%r12), %rax
	movq	%rax, 16(%rdi)
.L1447:
	pushq	$0
	xorl	%edx, %edx
	movl	$13, %esi
	leaq	-64(%rbp), %rcx
	pushq	$0
	leaq	-56(%rbp), %r9
	movl	$1, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r12, -48(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1448
	movq	%r12, (%rsi)
	addq	$8, 72(%rbx)
.L1443:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1453
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1448:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1444:
	leaq	-48(%rbp), %rdx
	addq	$144, %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1452:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1447
.L1453:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25194:
	.size	_ZN2v88internal8compiler19InstructionSelector9VisitGotoEPNS1_10BasicBlockE, .-_ZN2v88internal8compiler19InstructionSelector9VisitGotoEPNS1_10BasicBlockE
	.section	.text._ZN2v88internal8compiler19InstructionSelector11VisitRetainEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector11VisitRetainEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector11VisitRetainEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector11VisitRetainEPNS1_4NodeE:
.LFB25210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1455
	movq	16(%r13), %r13
.L1455:
	movl	20(%r13), %ecx
	movq	288(%rbx), %rax
	movl	%ecx, %r12d
	andl	$16777215, %r12d
	movl	(%rax,%r12,4), %eax
	cmpl	$-1, %eax
	je	.L1463
.L1456:
	movq	208(%rbx), %rsi
	shrq	$6, %r12
	movl	$1, %edx
	movl	%eax, %eax
	salq	%cl, %rdx
	salq	$3, %rax
	orq	%rdx, (%rsi,%r12,8)
	movabsq	$103079215105, %rdx
	orq	%rdx, %rax
	movq	$0, -64(%rbp)
	movq	%rax, -56(%rbp)
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	16(%rdi), %r12
	movq	24(%rdi), %rax
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L1464
	leaq	48(%r12), %rax
	movq	%rax, 16(%rdi)
.L1458:
	pushq	$0
	xorl	%edx, %edx
	movl	$17, %esi
	leaq	-64(%rbp), %rcx
	pushq	$0
	leaq	-56(%rbp), %r9
	movl	$1, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r12, -48(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L1459
	movq	%r12, (%rsi)
	addq	$8, 72(%rbx)
.L1454:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1465
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1463:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%rbx), %rdx
	movl	%eax, (%rdx,%r12,4)
	movl	20(%r13), %ecx
	movl	%ecx, %r12d
	andl	$16777215, %r12d
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1459:
	leaq	-48(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1454
	.p2align 4,,10
	.p2align 3
.L1464:
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L1458
.L1465:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25210:
	.size	_ZN2v88internal8compiler19InstructionSelector11VisitRetainEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector11VisitRetainEPNS1_4NodeE
	.section	.rodata._ZNSt6vectorIiN2v88internal13ZoneAllocatorIiEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPiS4_EEmRKi.str1.1,"aMS",@progbits,1
.LC12:
	.string	"vector::_M_fill_insert"
	.section	.text._ZNSt6vectorIiN2v88internal13ZoneAllocatorIiEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPiS4_EEmRKi,"axG",@progbits,_ZNSt6vectorIiN2v88internal13ZoneAllocatorIiEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPiS4_EEmRKi,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiN2v88internal13ZoneAllocatorIiEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPiS4_EEmRKi
	.type	_ZNSt6vectorIiN2v88internal13ZoneAllocatorIiEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPiS4_EEmRKi, @function
_ZNSt6vectorIiN2v88internal13ZoneAllocatorIiEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPiS4_EEmRKi:
.LFB28985:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L1582
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rdi
	movq	24(%r12), %rax
	subq	%rdi, %rax
	sarq	$2, %rax
	cmpq	%rdx, %rax
	jb	.L1469
	movq	%rdi, %rsi
	movl	(%rcx), %r14d
	subq	%rbx, %rsi
	movq	%rsi, %rax
	sarq	$2, %rax
	cmpq	%rax, %rdx
	jnb	.L1470
	leaq	0(,%rdx,4), %r15
	movq	%rdi, %rdx
	subq	%r15, %rdx
	cmpq	%rdx, %rdi
	je	.L1517
	leaq	-4(%rdi), %rcx
	movl	$16, %eax
	subq	%rdx, %rcx
	subq	%r15, %rax
	shrq	$2, %rcx
	testq	%rax, %rax
	leaq	16(%rdi), %rax
	setle	%sil
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %sil
	je	.L1518
	movabsq	$4611686018427387900, %rax
	testq	%rax, %rcx
	je	.L1518
	addq	$1, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rsi
	shrq	$2, %rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1473:
	movdqu	(%rdx,%rax), %xmm1
	movups	%xmm1, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L1473
	movq	%rcx, %rsi
	andq	$-4, %rsi
	leaq	0(,%rsi,4), %rax
	leaq	(%rdx,%rax), %r8
	addq	%rdi, %rax
	cmpq	%rsi, %rcx
	je	.L1475
	movl	(%r8), %ecx
	movl	%ecx, (%rax)
	leaq	4(%r8), %rcx
	cmpq	%rcx, %rdi
	je	.L1475
	movl	4(%r8), %ecx
	movl	%ecx, 4(%rax)
	leaq	8(%r8), %rcx
	cmpq	%rcx, %rdi
	je	.L1475
	movl	8(%r8), %ecx
	movl	%ecx, 8(%rax)
.L1475:
	movq	16(%r12), %rax
.L1471:
	addq	%r15, %rax
	movq	%rax, 16(%r12)
	cmpq	%rdx, %rbx
	je	.L1476
	subq	%rbx, %rdx
	movq	%rbx, %rsi
	subq	%rdx, %rdi
	call	memmove@PLT
.L1476:
	leaq	(%rbx,%r15), %rdx
	cmpq	%rdx, %rbx
	je	.L1466
	movq	%rdx, %rcx
	movq	%rbx, %rax
	subq	%rbx, %rcx
	subq	$4, %rcx
	movq	%rcx, %rsi
	shrq	$2, %rsi
	addq	$1, %rsi
	cmpq	$8, %rcx
	jbe	.L1478
	movq	%rsi, %rcx
	movd	%r14d, %xmm6
	shrq	$2, %rcx
	pshufd	$0, %xmm6, %xmm0
	salq	$4, %rcx
	addq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L1480:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L1480
	movq	%rsi, %rax
	andq	$-4, %rax
	leaq	(%rbx,%rax,4), %r13
	cmpq	%rax, %rsi
	je	.L1466
.L1478:
	leaq	4(%r13), %rax
	movl	%r14d, 0(%r13)
	cmpq	%rax, %rdx
	je	.L1466
	leaq	8(%r13), %rax
	movl	%r14d, 4(%r13)
	cmpq	%rax, %rdx
	je	.L1466
.L1585:
	movl	%r14d, 8(%r13)
.L1466:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1582:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1470:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	subq	%rax, %rdx
	je	.L1519
	leaq	-1(%rdx), %rax
	cmpq	$2, %rax
	jbe	.L1520
	movq	%rdx, %r8
	movd	%r14d, %xmm7
	xorl	%eax, %eax
	shrq	$2, %r8
	pshufd	$0, %xmm7, %xmm0
	.p2align 4,,10
	.p2align 3
.L1484:
	movq	%rax, %rcx
	addq	$1, %rax
	salq	$4, %rcx
	movups	%xmm0, (%rdi,%rcx)
	cmpq	%rax, %r8
	jne	.L1484
	movq	%rdx, %rcx
	movq	%rdx, %r8
	andq	$-4, %rcx
	andl	$3, %r8d
	leaq	(%rdi,%rcx,4), %rax
	cmpq	%rcx, %rdx
	je	.L1485
.L1483:
	movl	%r14d, (%rax)
	cmpq	$1, %r8
	je	.L1485
	movl	%r14d, 4(%rax)
	cmpq	$2, %r8
	je	.L1485
	movl	%r14d, 8(%rax)
.L1485:
	leaq	(%rdi,%rdx,4), %r8
.L1482:
	movq	%r8, 16(%r12)
	cmpq	%rbx, %rdi
	je	.L1486
	movq	%rdi, %rax
	leaq	15(%rbx), %rcx
	movq	%rbx, %rdx
	subq	%rbx, %rax
	subq	%r8, %rcx
	leaq	-4(%rax), %r9
	movq	%r9, %rax
	shrq	$2, %rax
	cmpq	$30, %rcx
	jbe	.L1521
	movabsq	$4611686018427387900, %rcx
	testq	%rcx, %rax
	je	.L1521
	leaq	1(%rax), %rcx
	xorl	%eax, %eax
	movq	%rcx, %r9
	shrq	$2, %r9
	salq	$4, %r9
	.p2align 4,,10
	.p2align 3
.L1488:
	movdqu	(%rbx,%rax), %xmm2
	movups	%xmm2, (%r8,%rax)
	addq	$16, %rax
	cmpq	%r9, %rax
	jne	.L1488
	movq	%rcx, %r9
	andq	$-4, %r9
	leaq	0(,%r9,4), %rax
	leaq	(%rbx,%rax), %r10
	addq	%r8, %rax
	cmpq	%r9, %rcx
	je	.L1489
	movl	(%r10), %r8d
	movl	%r8d, (%rax)
	leaq	4(%r10), %r8
	cmpq	%r8, %rdi
	je	.L1489
	movl	4(%r10), %r8d
	movl	%r8d, 4(%rax)
	leaq	8(%r10), %r8
	cmpq	%r8, %rdi
	je	.L1489
	movl	8(%r10), %r8d
	movl	%r8d, 8(%rax)
.L1489:
	addq	%rsi, 16(%r12)
	movq	%rcx, %rax
.L1515:
	movq	%rax, %rcx
	movd	%r14d, %xmm6
	shrq	$2, %rcx
	pshufd	$0, %xmm6, %xmm0
	salq	$4, %rcx
	addq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L1493:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L1493
	movq	%rax, %rdx
	andq	$-4, %rdx
	leaq	(%rbx,%rdx,4), %r13
	cmpq	%rax, %rdx
	je	.L1466
.L1516:
	leaq	4(%r13), %rax
	movl	%r14d, 0(%r13)
	cmpq	%rax, %rdi
	je	.L1466
	leaq	8(%r13), %rax
	movl	%r14d, 4(%r13)
	cmpq	%rax, %rdi
	jne	.L1585
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1469:
	movq	8(%r12), %rsi
	movl	$536870911, %r8d
	movq	%r8, %rax
	subq	%rsi, %rdi
	sarq	$2, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L1586
	cmpq	%rdi, %rdx
	movq	%rdi, %rax
	cmovnb	%rdx, %rax
	addq	%rax, %rdi
	setc	%al
	subq	%rsi, %r13
	movzbl	%al, %eax
	testq	%rax, %rax
	jne	.L1522
	testq	%rdi, %rdi
	jne	.L1587
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L1498:
	leaq	0(,%rdx,4), %r9
	leaq	(%rax,%r13), %r11
	leaq	0(%r13,%r9), %rsi
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	leaq	4(%rcx), %rsi
	setnb	%dil
	cmpq	%rsi, %r11
	setnb	%sil
	orb	%sil, %dil
	je	.L1524
	leaq	-1(%rdx), %rsi
	cmpq	$3, %rsi
	jbe	.L1524
	movd	(%rcx), %xmm7
	leaq	-4(%rdx), %rdi
	xorl	%esi, %esi
	shrq	$2, %rdi
	addq	$1, %rdi
	pshufd	$0, %xmm7, %xmm0
	.p2align 4,,10
	.p2align 3
.L1502:
	movq	%rsi, %r10
	addq	$1, %rsi
	salq	$4, %r10
	movups	%xmm0, (%r11,%r10)
	cmpq	%rsi, %rdi
	ja	.L1502
	leaq	0(,%rdi,4), %rsi
	movq	%rdx, %r10
	salq	$4, %rdi
	subq	%rsi, %r10
	addq	%r11, %rdi
	cmpq	%rsi, %rdx
	je	.L1504
	movl	(%rcx), %edx
	movl	%edx, (%rdi)
	cmpq	$1, %r10
	je	.L1504
	movl	(%rcx), %edx
	movl	%edx, 4(%rdi)
	cmpq	$2, %r10
	je	.L1504
	movl	(%rcx), %edx
	movl	%edx, 8(%rdi)
.L1504:
	movq	8(%r12), %rcx
	cmpq	%rcx, %rbx
	je	.L1525
	leaq	-4(%rbx), %r10
	leaq	15(%rcx), %rdx
	subq	%rcx, %r10
	subq	%rax, %rdx
	movq	%r10, %rsi
	shrq	$2, %rsi
	cmpq	$30, %rdx
	jbe	.L1526
	movabsq	$4611686018427387900, %rdx
	testq	%rdx, %rsi
	je	.L1526
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rdi
	shrq	$2, %rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1507:
	movdqu	(%rcx,%rdx), %xmm3
	movups	%xmm3, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L1507
	movq	%rsi, %rdx
	andq	$-4, %rdx
	leaq	0(,%rdx,4), %rdi
	addq	%rdi, %rcx
	addq	%rax, %rdi
	cmpq	%rdx, %rsi
	je	.L1509
	movl	(%rcx), %edx
	movl	%edx, (%rdi)
	leaq	4(%rcx), %rdx
	cmpq	%rdx, %rbx
	je	.L1509
	movl	4(%rcx), %edx
	movl	%edx, 4(%rdi)
	leaq	8(%rcx), %rdx
	cmpq	%rdx, %rbx
	je	.L1509
	movl	8(%rcx), %edx
	movl	%edx, 8(%rdi)
.L1509:
	leaq	4(%rax,%r10), %rdx
.L1505:
	movq	16(%r12), %r10
	leaq	(%rdx,%r9), %rcx
	cmpq	%r10, %rbx
	je	.L1510
	movq	%r10, %rsi
	leaq	16(%rdx,%r9), %rdx
	subq	%rbx, %rsi
	leaq	-4(%rsi), %rdi
	movq	%rdi, %r11
	shrq	$2, %r11
	cmpq	%rdx, %rbx
	leaq	16(%rbx), %rdx
	setnb	%sil
	cmpq	%rdx, %rcx
	setnb	%dl
	orb	%dl, %sil
	je	.L1527
	movabsq	$4611686018427387900, %rdx
	testq	%rdx, %r11
	je	.L1527
	addq	$1, %r11
	xorl	%edx, %edx
	movq	%r11, %rsi
	shrq	$2, %rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1512:
	movdqu	(%rbx,%rdx), %xmm4
	movups	%xmm4, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L1512
	movq	%r11, %rdx
	andq	$-4, %rdx
	leaq	0(,%rdx,4), %r9
	addq	%r9, %rbx
	addq	%rcx, %r9
	cmpq	%rdx, %r11
	je	.L1514
	movl	(%rbx), %edx
	movl	%edx, (%r9)
	leaq	4(%rbx), %rdx
	cmpq	%rdx, %r10
	je	.L1514
	movl	4(%rbx), %edx
	movl	%edx, 4(%r9)
	leaq	8(%rbx), %rdx
	cmpq	%rdx, %r10
	je	.L1514
	movl	8(%rbx), %edx
	movl	%edx, 8(%r9)
.L1514:
	leaq	4(%rcx,%rdi), %rcx
.L1510:
	movq	%rax, %xmm0
	movq	%rcx, %xmm5
	movq	%r8, 24(%r12)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1522:
	.cfi_restore_state
	movl	$2147483648, %esi
	movl	$2147483644, %r14d
.L1497:
	movq	(%r12), %r8
	movq	16(%r8), %rax
	movq	24(%r8), %rdi
	subq	%rax, %rdi
	cmpq	%rsi, %rdi
	jb	.L1588
	addq	%rax, %rsi
	movq	%rsi, 16(%r8)
.L1500:
	leaq	(%rax,%r14), %r8
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L1524:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L1501:
	movl	(%rcx), %edi
	movl	%edi, (%r11,%rsi,4)
	addq	$1, %rsi
	cmpq	%rsi, %rdx
	jne	.L1501
	jmp	.L1504
	.p2align 4,,10
	.p2align 3
.L1518:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1472:
	movl	(%rdx,%rax,4), %esi
	movl	%esi, (%rdi,%rax,4)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rsi, %rcx
	jne	.L1472
	jmp	.L1475
	.p2align 4,,10
	.p2align 3
.L1521:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L1487:
	movl	(%rbx,%rcx,4), %r10d
	movl	%r10d, (%r8,%rcx,4)
	movq	%rcx, %r10
	addq	$1, %rcx
	cmpq	%r10, %rax
	jne	.L1487
	addq	%rsi, 16(%r12)
	addq	$1, %rax
	cmpq	$8, %r9
	jbe	.L1516
	jmp	.L1515
	.p2align 4,,10
	.p2align 3
.L1526:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1506:
	movl	(%rcx,%rdx,4), %edi
	movl	%edi, (%rax,%rdx,4)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%rdi, %rsi
	jne	.L1506
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1527:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1511:
	movl	(%rbx,%rdx,4), %esi
	movl	%esi, (%rcx,%rdx,4)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %r11
	jne	.L1511
	jmp	.L1514
	.p2align 4,,10
	.p2align 3
.L1519:
	movq	%rdi, %r8
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1517:
	movq	%rdi, %rax
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1486:
	leaq	(%r8,%rsi), %rax
	movq	%rax, 16(%r12)
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1525:
	movq	%rax, %rdx
	jmp	.L1505
.L1520:
	movq	%rdi, %rax
	movq	%rdx, %r8
	jmp	.L1483
.L1588:
	movq	%r8, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	jmp	.L1500
.L1587:
	cmpq	$536870911, %rdi
	cmova	%r8, %rdi
	leaq	0(,%rdi,4), %r14
	leaq	7(%r14), %rsi
	andq	$-8, %rsi
	jmp	.L1497
.L1586:
	leaq	.LC12(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE28985:
	.size	_ZNSt6vectorIiN2v88internal13ZoneAllocatorIiEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPiS4_EEmRKi, .-_ZNSt6vectorIiN2v88internal13ZoneAllocatorIiEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPiS4_EEmRKi
	.section	.text._ZN2v88internal8compiler19InstructionSelector9SetRenameEPKNS1_4NodeES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector9SetRenameEPKNS1_4NodeES5_
	.type	_ZN2v88internal8compiler19InstructionSelector9SetRenameEPKNS1_4NodeES5_, @function
_ZN2v88internal8compiler19InstructionSelector9SetRenameEPKNS1_4NodeES5_:
.LFB25028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	20(%rsi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	288(%rdi), %rax
	andl	$16777215, %r13d
	movl	(%rax,%r13,4), %eax
	cmpl	$-1, %eax
	je	.L1597
.L1590:
	movq	328(%rbx), %r8
	movq	320(%rbx), %rsi
	movslq	%eax, %r13
	movq	%r8, %rcx
	subq	%rsi, %rcx
	sarq	$2, %rcx
	cmpq	%rcx, %r13
	jnb	.L1598
.L1591:
	movl	20(%r12), %r12d
	movq	288(%rbx), %rax
	leaq	(%rsi,%r13,4), %r13
	andl	$16777215, %r12d
	movl	(%rax,%r12,4), %eax
	cmpl	$-1, %eax
	je	.L1599
.L1594:
	movl	%eax, 0(%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1600
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1598:
	.cfi_restore_state
	addl	$1, %eax
	movl	$-1, -44(%rbp)
	movslq	%eax, %rdx
	cmpq	%rcx, %rdx
	ja	.L1601
	jnb	.L1591
	leaq	(%rsi,%rdx,4), %rax
	cmpq	%rax, %r8
	je	.L1591
	movq	%rax, 328(%rbx)
	jmp	.L1591
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	16(%rdi), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%rbx), %rdx
	movl	%eax, (%rdx,%r13,4)
	jmp	.L1590
	.p2align 4,,10
	.p2align 3
.L1599:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%rbx), %rdx
	movl	%eax, (%rdx,%r12,4)
	jmp	.L1594
	.p2align 4,,10
	.p2align 3
.L1601:
	leaq	-44(%rbp), %r9
	subq	%rcx, %rdx
	movq	%r8, %rsi
	leaq	312(%rbx), %rdi
	movq	%r9, %rcx
	call	_ZNSt6vectorIiN2v88internal13ZoneAllocatorIiEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPiS4_EEmRKi
	movq	320(%rbx), %rsi
	jmp	.L1591
.L1600:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25028:
	.size	_ZN2v88internal8compiler19InstructionSelector9SetRenameEPKNS1_4NodeES5_, .-_ZN2v88internal8compiler19InstructionSelector9SetRenameEPKNS1_4NodeES5_
	.section	.text._ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE:
.LFB25201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	32(%rsi), %rdi
	subq	$16, %rsp
	movq	32(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rsi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L1603
	movq	16(%rdx), %rdx
.L1603:
	movl	20(%rdx), %ecx
	movl	$1, %edx
	movq	%rcx, %rax
	salq	%cl, %rdx
	shrq	$3, %rax
	andl	$2097144, %eax
	addq	208(%rbx), %rax
	orq	%rdx, (%rax)
	movl	20(%rsi), %eax
	movl	%eax, %edx
	xorl	$251658240, %edx
	andl	$251658240, %edx
	jne	.L1604
	movq	32(%rsi), %rdi
	addq	$16, %rdi
.L1604:
	movl	%eax, %r12d
	movq	288(%rbx), %rax
	movq	(%rdi), %r13
	andl	$16777215, %r12d
	movl	(%rax,%r12,4), %eax
	cmpl	$-1, %eax
	je	.L1612
.L1605:
	movq	328(%rbx), %r8
	movq	320(%rbx), %rsi
	movslq	%eax, %r12
	movq	%r8, %rcx
	subq	%rsi, %rcx
	sarq	$2, %rcx
	cmpq	%rcx, %r12
	jnb	.L1613
.L1606:
	leaq	(%rsi,%r12,4), %r14
	movl	20(%r13), %r12d
	movq	288(%rbx), %rax
	andl	$16777215, %r12d
	movl	(%rax,%r12,4), %eax
	cmpl	$-1, %eax
	je	.L1614
.L1609:
	movl	%eax, (%r14)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1615
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1613:
	.cfi_restore_state
	addl	$1, %eax
	movl	$-1, -44(%rbp)
	movslq	%eax, %rdx
	cmpq	%rdx, %rcx
	jb	.L1616
	jbe	.L1606
	leaq	(%rsi,%rdx,4), %rax
	cmpq	%rax, %r8
	je	.L1606
	movq	%rax, 328(%rbx)
	jmp	.L1606
	.p2align 4,,10
	.p2align 3
.L1612:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%rbx), %rdx
	movl	%eax, (%rdx,%r12,4)
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1614:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%rbx), %rdx
	movl	%eax, (%rdx,%r12,4)
	jmp	.L1609
	.p2align 4,,10
	.p2align 3
.L1616:
	leaq	-44(%rbp), %r9
	subq	%rcx, %rdx
	movq	%r8, %rsi
	leaq	312(%rbx), %rdi
	movq	%r9, %rcx
	call	_ZNSt6vectorIiN2v88internal13ZoneAllocatorIiEEE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPiS4_EEmRKi
	movq	320(%rbx), %rsi
	jmp	.L1606
.L1615:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25201:
	.size	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector27EmitWordPoisonOnSpeculationEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector27EmitWordPoisonOnSpeculationEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector27EmitWordPoisonOnSpeculationEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector27EmitWordPoisonOnSpeculationEPNS1_4NodeE:
.LFB25126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 364(%rdi)
	je	.L1618
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	20(%rax), %ecx
	movq	%rax, %rbx
	movq	288(%r12), %rax
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	movl	(%rax,%r14,4), %eax
	cmpl	$-1, %eax
	je	.L1628
.L1619:
	movl	%eax, %ebx
	movq	208(%r12), %rdx
	shrq	$6, %r14
	movabsq	$377957122049, %rax
	salq	$3, %rbx
	orq	%rax, %rbx
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, (%rdx,%r14,8)
	movl	20(%r13), %ecx
	movq	288(%r12), %rax
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	movl	(%rax,%r14,4), %eax
	cmpl	$-1, %eax
	je	.L1629
.L1620:
	movq	160(%r12), %rsi
	shrq	$6, %r14
	movl	$1, %edx
	movl	%eax, %eax
	salq	%cl, %rdx
	salq	$3, %rax
	orq	%rdx, (%rsi,%r14,8)
	movabsq	$1065151889409, %rdx
	orq	%rdx, %rax
	movq	%rbx, -64(%rbp)
	movq	%rax, -56(%rbp)
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L1630
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1622:
	pushq	$0
	movl	$1, %edx
	movl	$29, %esi
	leaq	-56(%rbp), %rcx
	pushq	$0
	leaq	-64(%rbp), %r9
	movl	$1, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%r12), %rsi
	movq	%rbx, -48(%rbp)
	popq	%rdx
	cmpq	80(%r12), %rsi
	je	.L1623
	movq	%rbx, (%rsi)
	addq	$8, 72(%r12)
.L1617:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1631
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1618:
	.cfi_restore_state
	call	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1623:
	leaq	-48(%rbp), %rdx
	leaq	56(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1628:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%r12), %rdx
	movl	%eax, (%rdx,%r14,4)
	movl	20(%rbx), %ecx
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	jmp	.L1619
	.p2align 4,,10
	.p2align 3
.L1629:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%r12), %rdx
	movl	%eax, (%rdx,%r14,4)
	movl	20(%r13), %ecx
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	jmp	.L1620
	.p2align 4,,10
	.p2align 3
.L1630:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1622
.L1631:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25126:
	.size	_ZN2v88internal8compiler19InstructionSelector27EmitWordPoisonOnSpeculationEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector27EmitWordPoisonOnSpeculationEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector24VisitBitcastTaggedToWordEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector24VisitBitcastTaggedToWordEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector24VisitBitcastTaggedToWordEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector24VisitBitcastTaggedToWordEPNS1_4NodeE:
.LFB25160:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE
	.cfi_endproc
.LFE25160:
	.size	_ZN2v88internal8compiler19InstructionSelector24VisitBitcastTaggedToWordEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector24VisitBitcastTaggedToWordEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector17VisitFinishRegionEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector17VisitFinishRegionEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector17VisitFinishRegionEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector17VisitFinishRegionEPNS1_4NodeE:
.LFB32651:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE
	.cfi_endproc
.LFE32651:
	.size	_ZN2v88internal8compiler19InstructionSelector17VisitFinishRegionEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector17VisitFinishRegionEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector30VisitWord64PoisonOnSpeculationEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector30VisitWord64PoisonOnSpeculationEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector30VisitWord64PoisonOnSpeculationEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector30VisitWord64PoisonOnSpeculationEPNS1_4NodeE:
.LFB32655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 364(%rdi)
	je	.L1635
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	20(%rax), %ecx
	movq	%rax, %rbx
	movq	288(%r12), %rax
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	movl	(%rax,%r14,4), %eax
	cmpl	$-1, %eax
	je	.L1645
.L1636:
	movl	%eax, %ebx
	movq	208(%r12), %rdx
	shrq	$6, %r14
	movabsq	$377957122049, %rax
	salq	$3, %rbx
	orq	%rax, %rbx
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, (%rdx,%r14,8)
	movl	20(%r13), %ecx
	movq	288(%r12), %rax
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	movl	(%rax,%r14,4), %eax
	cmpl	$-1, %eax
	je	.L1646
.L1637:
	movq	160(%r12), %rsi
	shrq	$6, %r14
	movl	$1, %edx
	movl	%eax, %eax
	salq	%cl, %rdx
	salq	$3, %rax
	orq	%rdx, (%rsi,%r14,8)
	movabsq	$1065151889409, %rdx
	orq	%rdx, %rax
	movq	%rbx, -64(%rbp)
	movq	%rax, -56(%rbp)
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L1647
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1639:
	pushq	$0
	movl	$1, %edx
	movl	$29, %esi
	leaq	-56(%rbp), %rcx
	pushq	$0
	leaq	-64(%rbp), %r9
	movl	$1, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%r12), %rsi
	movq	%rbx, -48(%rbp)
	popq	%rdx
	cmpq	80(%r12), %rsi
	je	.L1640
	movq	%rbx, (%rsi)
	addq	$8, 72(%r12)
.L1634:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1648
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1635:
	.cfi_restore_state
	call	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE
	jmp	.L1634
	.p2align 4,,10
	.p2align 3
.L1640:
	leaq	-48(%rbp), %rdx
	leaq	56(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1634
	.p2align 4,,10
	.p2align 3
.L1645:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%r12), %rdx
	movl	%eax, (%rdx,%r14,4)
	movl	20(%rbx), %ecx
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1646:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%r12), %rdx
	movl	%eax, (%rdx,%r14,4)
	movl	20(%r13), %ecx
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	jmp	.L1637
	.p2align 4,,10
	.p2align 3
.L1647:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1639
.L1648:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE32655:
	.size	_ZN2v88internal8compiler19InstructionSelector30VisitWord64PoisonOnSpeculationEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector30VisitWord64PoisonOnSpeculationEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector30VisitTaggedPoisonOnSpeculationEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector30VisitTaggedPoisonOnSpeculationEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector30VisitTaggedPoisonOnSpeculationEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector30VisitTaggedPoisonOnSpeculationEPNS1_4NodeE:
.LFB25129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 364(%rdi)
	je	.L1650
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	20(%rax), %ecx
	movq	%rax, %rbx
	movq	288(%r12), %rax
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	movl	(%rax,%r14,4), %eax
	cmpl	$-1, %eax
	je	.L1660
.L1651:
	movl	%eax, %ebx
	movq	208(%r12), %rdx
	shrq	$6, %r14
	movabsq	$377957122049, %rax
	salq	$3, %rbx
	orq	%rax, %rbx
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, (%rdx,%r14,8)
	movl	20(%r13), %ecx
	movq	288(%r12), %rax
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	movl	(%rax,%r14,4), %eax
	cmpl	$-1, %eax
	je	.L1661
.L1652:
	movq	160(%r12), %rsi
	shrq	$6, %r14
	movl	$1, %edx
	movl	%eax, %eax
	salq	%cl, %rdx
	salq	$3, %rax
	orq	%rdx, (%rsi,%r14,8)
	movabsq	$1065151889409, %rdx
	orq	%rdx, %rax
	movq	%rbx, -64(%rbp)
	movq	%rax, -56(%rbp)
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L1662
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1654:
	pushq	$0
	movl	$1, %edx
	movl	$29, %esi
	leaq	-56(%rbp), %rcx
	pushq	$0
	leaq	-64(%rbp), %r9
	movl	$1, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%r12), %rsi
	movq	%rbx, -48(%rbp)
	popq	%rdx
	cmpq	80(%r12), %rsi
	je	.L1655
	movq	%rbx, (%rsi)
	addq	$8, 72(%r12)
.L1649:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1663
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1650:
	.cfi_restore_state
	call	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE
	jmp	.L1649
	.p2align 4,,10
	.p2align 3
.L1655:
	leaq	-48(%rbp), %rdx
	leaq	56(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1649
	.p2align 4,,10
	.p2align 3
.L1660:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%r12), %rdx
	movl	%eax, (%rdx,%r14,4)
	movl	20(%rbx), %ecx
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1661:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%r12), %rdx
	movl	%eax, (%rdx,%r14,4)
	movl	20(%r13), %ecx
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	jmp	.L1652
	.p2align 4,,10
	.p2align 3
.L1662:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1654
.L1663:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25129:
	.size	_ZN2v88internal8compiler19InstructionSelector30VisitTaggedPoisonOnSpeculationEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector30VisitTaggedPoisonOnSpeculationEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector30VisitWord32PoisonOnSpeculationEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector30VisitWord32PoisonOnSpeculationEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector30VisitWord32PoisonOnSpeculationEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector30VisitWord32PoisonOnSpeculationEPNS1_4NodeE:
.LFB32653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 364(%rdi)
	je	.L1665
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties13GetValueInputEPNS1_4NodeEi@PLT
	movl	20(%rax), %ecx
	movq	%rax, %rbx
	movq	288(%r12), %rax
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	movl	(%rax,%r14,4), %eax
	cmpl	$-1, %eax
	je	.L1675
.L1666:
	movl	%eax, %ebx
	movq	208(%r12), %rdx
	shrq	$6, %r14
	movabsq	$377957122049, %rax
	salq	$3, %rbx
	orq	%rax, %rbx
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, (%rdx,%r14,8)
	movl	20(%r13), %ecx
	movq	288(%r12), %rax
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	movl	(%rax,%r14,4), %eax
	cmpl	$-1, %eax
	je	.L1676
.L1667:
	movq	160(%r12), %rsi
	shrq	$6, %r14
	movl	$1, %edx
	movl	%eax, %eax
	salq	%cl, %rdx
	salq	$3, %rax
	orq	%rdx, (%rsi,%r14,8)
	movabsq	$1065151889409, %rdx
	orq	%rdx, %rax
	movq	%rbx, -64(%rbp)
	movq	%rax, -56(%rbp)
	movq	16(%r12), %rax
	movq	8(%rax), %rdi
	movq	16(%rdi), %rbx
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	$55, %rax
	jbe	.L1677
	leaq	56(%rbx), %rax
	movq	%rax, 16(%rdi)
.L1669:
	pushq	$0
	movl	$1, %edx
	movl	$29, %esi
	leaq	-56(%rbp), %rcx
	pushq	$0
	leaq	-64(%rbp), %r9
	movl	$1, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%r12), %rsi
	movq	%rbx, -48(%rbp)
	popq	%rdx
	cmpq	80(%r12), %rsi
	je	.L1670
	movq	%rbx, (%rsi)
	addq	$8, 72(%r12)
.L1664:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1678
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1665:
	.cfi_restore_state
	call	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L1670:
	leaq	-48(%rbp), %rdx
	leaq	56(%r12), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L1675:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%r12), %rdx
	movl	%eax, (%rdx,%r14,4)
	movl	20(%rbx), %ecx
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	jmp	.L1666
	.p2align 4,,10
	.p2align 3
.L1676:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	288(%r12), %rdx
	movl	%eax, (%rdx,%r14,4)
	movl	20(%r13), %ecx
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1677:
	movl	$56, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rbx
	jmp	.L1669
.L1678:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE32653:
	.size	_ZN2v88internal8compiler19InstructionSelector30VisitWord32PoisonOnSpeculationEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector30VisitWord32PoisonOnSpeculationEPNS1_4NodeE
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB29028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L1717
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L1695
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1718
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L1681:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L1719
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1684:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L1682
	.p2align 4,,10
	.p2align 3
.L1718:
	testq	%rdx, %rdx
	jne	.L1720
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L1682:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L1685
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L1698
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L1698
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1687:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L1687
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L1689
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L1689:
	leaq	16(%rax,%r8), %rcx
.L1685:
	cmpq	%r14, %r12
	je	.L1690
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L1699
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L1699
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1692:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L1692
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L1694
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L1694:
	leaq	8(%rcx,%r9), %rcx
.L1690:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1695:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L1681
	.p2align 4,,10
	.p2align 3
.L1699:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1691:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L1691
	jmp	.L1694
	.p2align 4,,10
	.p2align 3
.L1698:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L1686
	jmp	.L1689
.L1719:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L1684
.L1717:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1720:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L1681
	.cfi_endproc
.LFE29028:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_:
.LFB29036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L1759
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L1737
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1760
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L1723:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L1761
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1726:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L1724
	.p2align 4,,10
	.p2align 3
.L1760:
	testq	%rdx, %rdx
	jne	.L1762
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L1724:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L1727
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L1740
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L1740
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1729:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L1729
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L1731
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L1731:
	leaq	16(%rax,%r8), %rcx
.L1727:
	cmpq	%r14, %r12
	je	.L1732
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L1741
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L1741
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1734:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L1734
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L1736
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L1736:
	leaq	8(%rcx,%r9), %rcx
.L1732:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1737:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1741:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1733:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L1733
	jmp	.L1736
	.p2align 4,,10
	.p2align 3
.L1740:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1728:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L1728
	jmp	.L1731
.L1761:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L1726
.L1759:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1762:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L1723
	.cfi_endproc
.LFE29036:
	.size	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.section	.rodata._ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC13:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm
	.type	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm, @function
_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm:
.LFB29037:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1803
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$268435455, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$8, %rsp
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	movq	%rcx, %r13
	subq	8(%rdi), %r13
	subq	%rcx, %rax
	movq	%r13, %r14
	sarq	$3, %rax
	sarq	$3, %r14
	subq	%r14, %rsi
	cmpq	%rbx, %rax
	jb	.L1765
	cmpq	$1, %rbx
	je	.L1785
	leaq	-2(%rbx), %rdx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	addq	$1, %rdx
	.p2align 4,,10
	.p2align 3
.L1767:
	movq	%rax, %rsi
	addq	$1, %rax
	salq	$4, %rsi
	movups	%xmm0, (%rcx,%rsi)
	cmpq	%rax, %rdx
	ja	.L1767
	leaq	(%rdx,%rdx), %rax
	salq	$4, %rdx
	addq	%rcx, %rdx
	cmpq	%rax, %rbx
	je	.L1768
.L1766:
	movq	$0, (%rdx)
.L1768:
	leaq	(%rcx,%rbx,8), %rax
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1803:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1765:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rsi
	jb	.L1806
	cmpq	%r14, %rbx
	movq	%r14, %r15
	movq	(%rdi), %rdi
	cmovnb	%rbx, %r15
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	addq	%r14, %r15
	cmpq	$268435455, %r15
	cmova	%rdx, %r15
	subq	%rsi, %rax
	salq	$3, %r15
	movq	%r15, %r8
	cmpq	%r15, %rax
	jb	.L1807
	addq	%rsi, %r8
	movq	%r8, 16(%rdi)
.L1772:
	leaq	(%rsi,%r13), %rdx
	cmpq	$1, %rbx
	je	.L1773
	leaq	-2(%rbx), %rcx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rcx
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L1774:
	movq	%rax, %rdi
	addq	$1, %rax
	salq	$4, %rdi
	movups	%xmm0, (%rdx,%rdi)
	cmpq	%rax, %rcx
	ja	.L1774
	leaq	(%rcx,%rcx), %rax
	salq	$4, %rcx
	addq	%rcx, %rdx
	cmpq	%rax, %rbx
	je	.L1775
.L1773:
	movq	$0, (%rdx)
.L1775:
	movq	16(%r12), %rax
	movq	8(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L1780
	subq	$8, %rax
	leaq	15(%rdx), %rcx
	subq	%rdx, %rax
	subq	%rsi, %rcx
	movq	%rax, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rcx
	jbe	.L1777
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L1777
	addq	$1, %rdi
	movq	%rdx, %r8
	movq	%rsi, %rax
	movq	%rdi, %rcx
	subq	%rsi, %r8
	shrq	%rcx
	salq	$4, %rcx
	addq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L1778:
	movdqu	(%rax,%r8), %xmm1
	addq	$16, %rax
	movups	%xmm1, -16(%rax)
	cmpq	%rax, %rcx
	jne	.L1778
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rcx
	addq	%rcx, %rdx
	addq	%rsi, %rcx
	cmpq	%rax, %rdi
	je	.L1780
	movq	(%rdx), %rax
	movq	%rax, (%rcx)
.L1780:
	addq	%r14, %rbx
	addq	%rsi, %r15
	movq	%rsi, 8(%r12)
	leaq	(%rsi,%rbx,8), %rax
	movq	%r15, 24(%r12)
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1785:
	.cfi_restore_state
	movq	%rcx, %rdx
	jmp	.L1766
.L1777:
	leaq	8(%rsi,%rax), %rdi
	subq	%rsi, %rdx
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L1782:
	movq	(%rax,%rdx), %rcx
	addq	$8, %rax
	movq	%rcx, -8(%rax)
	cmpq	%rax, %rdi
	jne	.L1782
	jmp	.L1780
.L1807:
	movq	%r15, %rsi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rsi
	jmp	.L1772
.L1806:
	leaq	.LC13(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29037:
	.size	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm, .-_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm
	.section	.text._ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_:
.LFB29039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r13
	cmpq	24(%rdi), %r13
	je	.L1809
	movq	(%rsi), %rax
	movq	%rax, 0(%r13)
	addq	$8, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1809:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r13, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L1834
	testq	%rax, %rax
	je	.L1821
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1835
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L1812:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1836
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1815:
	leaq	(%rax,%r15), %rdi
	leaq	8(%rax), %rdx
.L1813:
	movq	(%r12), %rsi
	movq	%rsi, (%rax,%rcx)
	cmpq	%r14, %r13
	je	.L1816
	leaq	-8(%r13), %rdx
	movq	%rdx, %rcx
	leaq	15(%rax), %rdx
	subq	%r14, %rcx
	subq	%r14, %rdx
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L1824
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L1824
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1818:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L1818
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r14
	addq	%rax, %rsi
	cmpq	%rdx, %r8
	je	.L1820
	movq	(%r14), %rdx
	movq	%rdx, (%rsi)
.L1820:
	leaq	16(%rax,%rcx), %rdx
.L1816:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1835:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L1837
	movl	$8, %edx
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1821:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L1812
	.p2align 4,,10
	.p2align 3
.L1824:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1817:
	movq	(%r14,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %rsi
	jne	.L1817
	jmp	.L1820
.L1836:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1815
.L1834:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1837:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L1812
	.cfi_endproc
.LFE29039:
	.size	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	.section	.text._ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_:
.LFB29065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	8(%rdi), %r14
	movq	%r13, %rax
	subq	%r14, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L1854
	movq	%rsi, %rcx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	%rdx, %r15
	subq	%r14, %rcx
	testq	%rax, %rax
	je	.L1848
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L1855
	movl	$2147483632, %esi
	movl	$2147483632, %r10d
.L1840:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L1856
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1843:
	addq	%rax, %r10
	leaq	16(%rax), %r11
	jmp	.L1841
	.p2align 4,,10
	.p2align 3
.L1855:
	testq	%rdx, %rdx
	jne	.L1857
	movl	$16, %r11d
	xorl	%r10d, %r10d
	xorl	%eax, %eax
.L1841:
	movq	(%r15), %rdi
	movl	8(%r15), %esi
	addq	%rax, %rcx
	movzwl	12(%r15), %edx
	movq	%rdi, (%rcx)
	movl	%esi, 8(%rcx)
	movw	%dx, 12(%rcx)
	cmpq	%r14, %rbx
	je	.L1844
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1845:
	movq	(%rdx), %r9
	movl	8(%rdx), %r8d
	addq	$16, %rdx
	addq	$16, %rcx
	movzbl	-4(%rdx), %edi
	movzbl	-3(%rdx), %esi
	movq	%r9, -16(%rcx)
	movl	%r8d, -8(%rcx)
	movb	%dil, -4(%rcx)
	movb	%sil, -3(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1845
	movq	%rbx, %rdx
	subq	%r14, %rdx
	leaq	16(%rax,%rdx), %r11
.L1844:
	cmpq	%r13, %rbx
	je	.L1846
	movq	%rbx, %rdx
	movq	%r11, %rcx
	.p2align 4,,10
	.p2align 3
.L1847:
	movq	(%rdx), %r9
	movl	8(%rdx), %r8d
	addq	$16, %rdx
	addq	$16, %rcx
	movzbl	-4(%rdx), %edi
	movzbl	-3(%rdx), %esi
	movq	%r9, -16(%rcx)
	movl	%r8d, -8(%rcx)
	movb	%dil, -4(%rcx)
	movb	%sil, -3(%rcx)
	cmpq	%rdx, %r13
	jne	.L1847
	subq	%rbx, %r13
	addq	%r13, %r11
.L1846:
	movq	%rax, %xmm0
	movq	%r11, %xmm1
	movq	%r10, 24(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1848:
	.cfi_restore_state
	movl	$16, %esi
	movl	$16, %r10d
	jmp	.L1840
.L1856:
	movq	%rcx, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %rcx
	jmp	.L1843
.L1857:
	cmpq	$134217727, %rdx
	movl	$134217727, %r10d
	cmovbe	%rdx, %r10
	salq	$4, %r10
	movq	%r10, %rsi
	jmp	.L1840
.L1854:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29065:
	.size	_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm
	.type	_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm, @function
_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm:
.LFB29066:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1879
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$134217727, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	movq	%rcx, %rbx
	subq	8(%rdi), %rbx
	subq	%rcx, %rax
	movq	%rbx, %r14
	sarq	$4, %rax
	sarq	$4, %r14
	subq	%r14, %rsi
	cmpq	%r12, %rax
	jb	.L1860
	movq	%rcx, %rax
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L1861:
	movq	$0, (%rax)
	addq	$16, %rax
	movl	$-2, -8(%rax)
	movb	$0, -4(%rax)
	movb	$0, -3(%rax)
	subq	$1, %rdx
	jne	.L1861
	salq	$4, %r12
	addq	%r12, %rcx
	movq	%rcx, 16(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1879:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1860:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%r12, %rsi
	jb	.L1882
	cmpq	%r14, %r12
	movq	%r14, %r15
	movq	(%rdi), %rdi
	cmovnb	%r12, %r15
	movq	16(%rdi), %r9
	movq	24(%rdi), %rax
	addq	%r14, %r15
	cmpq	$134217727, %r15
	cmova	%rdx, %r15
	subq	%r9, %rax
	salq	$4, %r15
	movq	%r15, %rsi
	cmpq	%r15, %rax
	jb	.L1883
	addq	%r9, %rsi
	movq	%rsi, 16(%rdi)
.L1865:
	leaq	(%r9,%rbx), %rax
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L1866:
	movq	$0, (%rax)
	addq	$16, %rax
	movl	$-2, -8(%rax)
	movb	$0, -4(%rax)
	movb	$0, -3(%rax)
	subq	$1, %rdx
	jne	.L1866
	movq	8(%r13), %rax
	movq	16(%r13), %r10
	movq	%r9, %r11
	subq	%rax, %r11
	cmpq	%rax, %r10
	je	.L1869
	.p2align 4,,10
	.p2align 3
.L1868:
	movq	(%rax), %r8
	movl	8(%rax), %edi
	leaq	(%r11,%rax), %rdx
	addq	$16, %rax
	movzbl	-4(%rax), %esi
	movzbl	-3(%rax), %ecx
	movq	%r8, (%rdx)
	movl	%edi, 8(%rdx)
	movb	%sil, 12(%rdx)
	movb	%cl, 13(%rdx)
	cmpq	%rax, %r10
	jne	.L1868
.L1869:
	addq	%r14, %r12
	addq	%r9, %r15
	movq	%r9, 8(%r13)
	salq	$4, %r12
	movq	%r15, 24(%r13)
	leaq	(%r9,%r12), %rax
	movq	%rax, 16(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1883:
	.cfi_restore_state
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r9
	jmp	.L1865
.L1882:
	leaq	.LC13(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29066:
	.size	_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm, .-_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm
	.section	.text._ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_:
.LFB29393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	8(%rdi), %r14
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$4, %rax
	cmpq	$134217727, %rax
	je	.L1900
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L1894
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1901
	movl	$2147483632, %esi
	movl	$2147483632, %ecx
.L1886:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L1902
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1889:
	leaq	(%rax,%rcx), %rdi
	leaq	16(%rax), %rsi
	jmp	.L1887
	.p2align 4,,10
	.p2align 3
.L1901:
	testq	%rcx, %rcx
	jne	.L1903
	movl	$16, %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
.L1887:
	movdqu	(%r15), %xmm3
	movups	%xmm3, (%rax,%rdx)
	cmpq	%r14, %rbx
	je	.L1890
	movq	%r14, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1891:
	movdqu	(%rdx), %xmm1
	addq	$16, %rdx
	addq	$16, %rcx
	movups	%xmm1, -16(%rcx)
	cmpq	%rdx, %rbx
	jne	.L1891
	movq	%rbx, %rdx
	subq	%r14, %rdx
	leaq	16(%rax,%rdx), %rsi
.L1890:
	cmpq	%r12, %rbx
	je	.L1892
	movq	%rbx, %rdx
	movq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L1893:
	movdqu	(%rdx), %xmm2
	addq	$16, %rdx
	addq	$16, %rcx
	movups	%xmm2, -16(%rcx)
	cmpq	%rdx, %r12
	jne	.L1893
	subq	%rbx, %r12
	addq	%r12, %rsi
.L1892:
	movq	%rax, %xmm0
	movq	%rsi, %xmm4
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1894:
	.cfi_restore_state
	movl	$16, %esi
	movl	$16, %ecx
	jmp	.L1886
.L1902:
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	jmp	.L1889
.L1903:
	cmpq	$134217727, %rcx
	movl	$134217727, %eax
	cmova	%rax, %rcx
	salq	$4, %rcx
	movq	%rcx, %rsi
	jmp	.L1886
.L1900:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29393:
	.size	_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_:
.LFB28035:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r8
	cmpq	24(%rdi), %r8
	je	.L1905
	movdqu	(%rsi), %xmm0
	movups	%xmm0, (%r8)
	addq	$16, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1905:
	movq	%rsi, %rdx
	movq	%r8, %rsi
	jmp	_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.cfi_endproc
.LFE28035:
	.size	_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	.section	.text._ZN2v88internal8compiler19InstructionSelector32AddOperandToStateValueDescriptorEPNS1_14StateValueListEPNS0_10ZoneVectorINS1_18InstructionOperandEEEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS1_4NodeENS0_11MachineTypeENS1_19FrameStateInputKindEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector32AddOperandToStateValueDescriptorEPNS1_14StateValueListEPNS0_10ZoneVectorINS1_18InstructionOperandEEEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS1_4NodeENS0_11MachineTypeENS1_19FrameStateInputKindEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler19InstructionSelector32AddOperandToStateValueDescriptorEPNS1_14StateValueListEPNS0_10ZoneVectorINS1_18InstructionOperandEEEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS1_4NodeENS0_11MachineTypeENS1_19FrameStateInputKindEPNS0_4ZoneE, @function
_ZN2v88internal8compiler19InstructionSelector32AddOperandToStateValueDescriptorEPNS1_14StateValueListEPNS0_10ZoneVectorINS1_18InstructionOperandEEEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS1_4NodeENS0_11MachineTypeENS1_19FrameStateInputKindEPNS0_4ZoneE:
.LFB25076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rbp), %rax
	movq	%rdi, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.L2006
	movq	(%r9), %rdi
	movq	%r9, %r12
	movzwl	16(%rdi), %eax
	cmpw	$45, %ax
	je	.L1911
	movq	%rdx, %r14
	movq	%r8, %r15
	jbe	.L2007
	cmpw	$46, %ax
	je	.L1915
	leal	-47(%rax), %edx
	cmpw	$1, %dx
	ja	.L1916
	movq	8(%r8), %rax
	movq	16(%r8), %rsi
	xorl	%ecx, %ecx
	cmpq	%rax, %rsi
	je	.L2008
	movq	%r13, -128(%rbp)
	movq	%rcx, %rbx
	movq	%r8, %r13
	movq	%r9, %r15
	jmp	.L1917
	.p2align 4,,10
	.p2align 3
.L2011:
	movq	(%rdx), %rdi
	movzwl	16(%rdi), %edx
	subl	$47, %edx
	cmpl	$1, %edx
	ja	.L1923
	movq	(%r15), %rdx
	movzwl	16(%rdx), %edx
	subl	$47, %edx
	cmpl	$1, %edx
	ja	.L1923
	call	_ZN2v88internal8compiler10ObjectIdOfEPKNS1_8OperatorE@PLT
	movq	(%r15), %rdi
	movl	%eax, %r12d
	call	_ZN2v88internal8compiler10ObjectIdOfEPKNS1_8OperatorE@PLT
	cmpl	%eax, %r12d
	je	.L2009
	movq	8(%r13), %rax
	movq	16(%r13), %rsi
.L1923:
	movq	%rsi, %rdx
	addq	$1, %rbx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rbx, %rdx
	jbe	.L2010
.L1917:
	movq	(%rax,%rbx,8), %rdx
	cmpq	%rdx, %r15
	jne	.L2011
	movq	%r15, %r12
	movq	%r13, %r15
	movq	-128(%rbp), %r13
	movq	%rbx, %rcx
.L1922:
	movq	24(%r15), %rax
	cmpq	$-1, %rcx
	je	.L2012
	movq	%r12, -80(%rbp)
	cmpq	%rax, %rsi
	je	.L1944
	movq	%r12, (%rsi)
	leaq	-80(%rbp), %r9
	addq	$8, 16(%r15)
.L1945:
	movl	$2053, %esi
	movq	%r13, %rdi
	movb	$7, -78(%rbp)
	xorl	%ebx, %ebx
	movw	%si, -80(%rbp)
	movq	%r9, %rsi
	movq	%rcx, -72(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	jmp	.L1908
	.p2align 4,,10
	.p2align 3
.L2007:
	cmpw	$44, %ax
	jne	.L2013
	call	_ZN2v88internal8compiler20ArgumentsStateTypeOfEPKNS1_8OperatorE@PLT
	leaq	-80(%rbp), %r14
	movq	%r13, %rdi
	movb	$7, -78(%rbp)
	movl	$2048, %r9d
	movq	%r14, %rsi
	movb	%al, -72(%rbp)
	movw	%r9w, -80(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movq	%r12, -80(%rbp)
	movq	16(%r15), %rsi
	cmpq	24(%r15), %rsi
	je	.L1919
	movq	%r12, (%rsi)
	addq	$8, 16(%r15)
.L2005:
	xorl	%ebx, %ebx
	jmp	.L1908
	.p2align 4,,10
	.p2align 3
.L1911:
	call	_ZN2v88internal8compiler20ArgumentsStateTypeOfEPKNS1_8OperatorE@PLT
	movl	$2049, %r8d
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	leaq	-80(%rbp), %rsi
	movw	%r8w, -80(%rbp)
	movb	$7, -78(%rbp)
	movb	%al, -72(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
.L1908:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2014
	leaq	-40(%rbp), %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1916:
	.cfi_restore_state
	movzbl	16(%rbp), %ebx
	testb	%bl, %bl
	jne	.L1953
.L1970:
	movq	-112(%rbp), %rax
	leaq	-80(%rbp), %r9
	movl	$57005, %esi
	movq	%r9, %rdi
	movq	%r9, -104(%rbp)
	movq	(%rax), %rax
	movq	16(%rax), %rbx
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	movq	-104(%rbp), %r9
	testl	%eax, %eax
	jne	.L1947
	cmpb	$19, -76(%rbp)
	jne	.L1947
	movslq	-72(%rbp), %rax
	salq	$32, %rax
	orq	$3, %rax
.L1948:
	movl	%eax, %edx
	xorl	%ebx, %ebx
	andl	$7, %edx
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L1947:
	movq	160(%rbx), %rsi
	movq	%rsi, %r12
	subq	152(%rbx), %r12
	sarq	$4, %r12
	cmpq	168(%rbx), %rsi
	je	.L1949
	movzbl	-76(%rbp), %ecx
	movq	-72(%rbp), %rdx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rbx)
.L1950:
	movq	%r12, %rax
	salq	$32, %rax
	orq	$11, %rax
	jmp	.L1948
	.p2align 4,,10
	.p2align 3
.L1953:
	cmpw	$284, %ax
	je	.L1955
.L1956:
	movl	24(%rbp), %edx
	testl	%edx, %edx
	je	.L1965
	cmpl	$1, 24(%rbp)
	jne	.L1915
	movq	-112(%rbp), %r15
	movq	%r12, %rsi
	movq	(%r15), %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE
	movabsq	$996432412673, %rdx
	movl	%eax, %eax
	salq	$3, %rax
.L2004:
	movl	20(%r12), %ecx
	movq	(%r15), %rdi
	orq	%rdx, %rax
	leaq	-80(%rbp), %r9
	movq	%rcx, %rsi
	shrq	$3, %rsi
	andl	$2097144, %esi
	addq	208(%rdi), %rsi
	movl	$1, %edi
	salq	%cl, %rdi
	orq	%rdi, (%rsi)
.L1951:
	movq	%rax, -88(%rbp)
	testl	%edx, %edx
	je	.L1962
	movq	16(%r14), %rsi
	cmpq	24(%r14), %rsi
	je	.L1968
	movq	%rax, (%rsi)
	addq	$8, 16(%r14)
.L1969:
	movzwl	16(%rbp), %eax
	movq	%r9, %rsi
	movq	%r13, %rdi
	movb	%bl, -79(%rbp)
	movb	$2, -80(%rbp)
	movl	$1, %ebx
	movzbl	%ah, %eax
	movb	%al, -78(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	jmp	.L1908
	.p2align 4,,10
	.p2align 3
.L2013:
	movzbl	16(%rbp), %ebx
	testb	%bl, %bl
	je	.L1970
	cmpw	$30, %ax
	jne	.L2015
	movl	%ebx, %eax
	subl	$7, %eax
	cmpb	$1, %al
	jbe	.L1961
	movzbl	16(%rbp), %eax
	subl	$10, %eax
	cmpb	$1, %al
	jbe	.L1961
.L1964:
	movq	$0, -88(%rbp)
	leaq	-80(%rbp), %r9
.L1962:
	movl	$2051, %eax
	movq	%r9, %rsi
	movq	%r13, %rdi
	movb	$7, -78(%rbp)
	movw	%ax, -80(%rbp)
	xorl	%ebx, %ebx
	call	_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	jmp	.L1908
	.p2align 4,,10
	.p2align 3
.L2006:
	movl	$2051, %r10d
	leaq	-80(%rbp), %rsi
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	movw	%r10w, -80(%rbp)
	movb	$7, -78(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	jmp	.L1908
	.p2align 4,,10
	.p2align 3
.L2010:
	movq	%r15, %r12
	movq	%r13, %r15
	movq	-128(%rbp), %r13
	movq	%rdx, %rbx
	movq	24(%r15), %rax
.L1921:
	movq	%r12, -80(%rbp)
	cmpq	%rax, %rsi
	je	.L1926
	movq	%r12, (%rsi)
	leaq	-80(%rbp), %r9
	addq	$8, 16(%r15)
.L1927:
	movl	$2052, %edi
	movq	%r9, %rsi
	movb	$7, -78(%rbp)
	movw	%di, -80(%rbp)
	movq	%r13, %rdi
	movq	%rbx, -72(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler20StateValueDescriptorENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movq	-104(%rbp), %rax
	movq	16(%rax), %r10
	movq	24(%rax), %rax
	movq	%rax, -128(%rbp)
	subq	%r10, %rax
	cmpq	$63, %rax
	jbe	.L2016
	movq	-104(%rbp), %rbx
	leaq	64(%r10), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, %rax
.L1929:
	movq	%rax, (%r10)
	movq	$0, 8(%r10)
	movq	$0, 16(%r10)
	movq	$0, 24(%r10)
	movq	%rax, 32(%r10)
	movq	$0, 40(%r10)
	movq	$0, 48(%r10)
	movq	$0, 56(%r10)
	movq	48(%r13), %rbx
	cmpq	56(%r13), %rbx
	je	.L1930
	movq	%r10, (%rbx)
	addq	$8, 48(%r13)
.L1931:
	movq	(%r12), %rdi
	movq	%r10, -128(%rbp)
	movl	20(%rdi), %ebx
	call	_ZN2v88internal8compiler14MachineTypesOfEPKNS1_8OperatorE@PLT
	movq	%rax, %r11
	testl	%ebx, %ebx
	jle	.L2005
	movq	-128(%rbp), %r10
	leal	-1(%rbx), %eax
	movq	%r15, %r8
	xorl	%r13d, %r13d
	movq	%rax, -136(%rbp)
	xorl	%ebx, %ebx
	movq	%r11, %r15
	movq	%r10, -144(%rbp)
	jmp	.L1943
	.p2align 4,,10
	.p2align 3
.L2018:
	movq	32(%r12,%rcx), %r9
.L2003:
	movl	24(%rbp), %eax
	subq	$8, %rsp
	movq	-112(%rbp), %rcx
	pushq	-104(%rbp)
	movq	-144(%rbp), %rsi
	movq	%r8, -128(%rbp)
	pushq	%rax
	movzwl	(%rdx), %eax
	movq	%r14, %rdx
	movq	-120(%rbp), %rdi
	pushq	%rax
	call	_ZN2v88internal8compiler19InstructionSelector32AddOperandToStateValueDescriptorEPNS1_14StateValueListEPNS0_10ZoneVectorINS1_18InstructionOperandEEEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS1_4NodeENS0_11MachineTypeENS1_19FrameStateInputKindEPNS0_4ZoneE
	addq	$32, %rsp
	movq	-128(%rbp), %r8
	addq	%rax, %rbx
	cmpq	-136(%rbp), %r13
	leaq	1(%r13), %rax
	je	.L1908
	movq	%rax, %r13
.L1943:
	movq	8(%r15), %rax
	movq	16(%r15), %rdx
	subq	%rax, %rdx
	sarq	%rdx
	cmpq	%r13, %rdx
	jbe	.L2017
	leaq	(%rax,%r13,2), %rdx
	movzbl	23(%r12), %eax
	leaq	0(,%r13,8), %rcx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2018
	movq	32(%r12), %rax
	movq	16(%rax,%rcx), %r9
	jmp	.L2003
	.p2align 4,,10
	.p2align 3
.L2015:
	ja	.L1953
	cmpw	$26, %ax
	ja	.L1954
	cmpw	$22, %ax
	jbe	.L1956
.L1955:
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %r15
	call	_ZN2v88internal8compiler16OperandGenerator10ToConstantEPKNS1_4NodeE
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	testl	%eax, %eax
	jne	.L1957
	cmpb	$19, -76(%rbp)
	je	.L2019
.L1957:
	movq	160(%r15), %rsi
	movq	%rsi, %r12
	subq	152(%r15), %r12
	sarq	$4, %r12
	cmpq	168(%r15), %rsi
	je	.L1959
	movzbl	-76(%rbp), %ecx
	movq	-72(%rbp), %rdx
	movl	%eax, (%rsi)
	leaq	-80(%rbp), %r9
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%r15)
.L1960:
	movq	%r12, %rax
	salq	$32, %rax
	orq	$11, %rax
.L1958:
	movl	%eax, %edx
	andl	$7, %edx
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L1954:
	cmpw	$28, %ax
	je	.L1955
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L1919:
	movq	%r14, %rdx
	movq	%r15, %rdi
	xorl	%ebx, %ebx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1908
	.p2align 4,,10
	.p2align 3
.L1965:
	movq	-112(%rbp), %r15
	movq	%r12, %rsi
	movq	(%r15), %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE
	movabsq	$652835028993, %rdx
	movl	%eax, %eax
	salq	$3, %rax
	jmp	.L2004
	.p2align 4,,10
	.p2align 3
.L1961:
	movq	-120(%rbp), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rdx
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler14HeapConstantOfEPKNS1_8OperatorE@PLT
	movq	-104(%rbp), %rdx
	leaq	56(%rdx), %rcx
	addq	$4856, %rdx
	cmpq	%rax, %rdx
	jbe	.L1963
	cmpq	%rax, %rcx
	ja	.L1963
	subq	%rcx, %rax
	shrq	$3, %rax
	cmpw	$34, %ax
	je	.L1964
.L1963:
	movq	-112(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	leaq	-80(%rbp), %r9
	movl	%eax, %edx
	andl	$7, %edx
	jmp	.L1951
.L1949:
	movq	%r9, %rdx
	leaq	144(%rbx), %rdi
	movq	%r9, -104(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-104(%rbp), %r9
	jmp	.L1950
	.p2align 4,,10
	.p2align 3
.L2009:
	movq	%r15, %r12
	movq	%r13, %r15
	movq	%rbx, %rcx
	movq	-128(%rbp), %r13
	movq	16(%r15), %rsi
	jmp	.L1922
	.p2align 4,,10
	.p2align 3
.L1930:
	movq	40(%r13), %rdx
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L2020
	testq	%rax, %rax
	je	.L1973
	leaq	(%rax,%rax), %rdi
	movl	$2147483640, %esi
	cmpq	%rdi, %rax
	jbe	.L2021
.L1933:
	movq	32(%r13), %rdi
	movq	%rcx, -152(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%r10, -136(%rbp)
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal4Zone3NewEm
	movq	-128(%rbp), %rsi
	movq	-136(%rbp), %r10
	movq	-144(%rbp), %rdx
	movq	-152(%rbp), %rcx
	leaq	(%rax,%rsi), %r9
	leaq	8(%rax), %rsi
.L1934:
	movq	%r10, (%rax,%rcx)
	cmpq	%rdx, %rbx
	je	.L1935
	subq	$8, %rbx
	leaq	15(%rax), %rcx
	subq	%rdx, %rbx
	subq	%rdx, %rcx
	movq	%rbx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rcx
	jbe	.L1976
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rsi
	je	.L1976
	addq	$1, %rsi
	xorl	%ecx, %ecx
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1937:
	movdqu	(%rdx,%rcx), %xmm1
	movups	%xmm1, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rdi, %rcx
	jne	.L1937
	movq	%rsi, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rcx
	addq	%rcx, %rdx
	addq	%rax, %rcx
	cmpq	%rsi, %rdi
	je	.L1939
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L1939:
	leaq	16(%rax,%rbx), %rsi
.L1935:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%r9, 56(%r13)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 40(%r13)
	jmp	.L1931
	.p2align 4,,10
	.p2align 3
.L1926:
	leaq	-80(%rbp), %r9
	movq	%r15, %rdi
	movq	%r9, %rdx
	movq	%r9, -128(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-128(%rbp), %r9
	jmp	.L1927
	.p2align 4,,10
	.p2align 3
.L1944:
	leaq	-80(%rbp), %r9
	movq	%r15, %rdi
	movq	%rcx, -112(%rbp)
	movq	%r9, %rdx
	movq	%r9, -104(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %r9
	jmp	.L1945
	.p2align 4,,10
	.p2align 3
.L1968:
	leaq	-88(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r9, -104(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-104(%rbp), %r9
	jmp	.L1969
	.p2align 4,,10
	.p2align 3
.L2012:
	movq	%rsi, %rbx
	subq	8(%r15), %rbx
	sarq	$3, %rbx
	jmp	.L1921
.L2021:
	testq	%rdi, %rdi
	jne	.L2022
	movl	$8, %esi
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	jmp	.L1934
	.p2align 4,,10
	.p2align 3
.L2019:
	movslq	-72(%rbp), %rax
	leaq	-80(%rbp), %r9
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L1958
	.p2align 4,,10
	.p2align 3
.L2016:
	movq	-104(%rbp), %rdi
	movl	$64, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r10
	movq	-104(%rbp), %rax
	jmp	.L1929
	.p2align 4,,10
	.p2align 3
.L2008:
	movq	24(%r8), %rax
	xorl	%ebx, %ebx
	jmp	.L1921
.L1959:
	leaq	-80(%rbp), %r9
	leaq	144(%r15), %rdi
	movq	%r9, %rdx
	movq	%r9, -104(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-104(%rbp), %r9
	jmp	.L1960
.L1973:
	movl	$8, %esi
	jmp	.L1933
.L1976:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L1936:
	movq	(%rdx,%rcx,8), %rdi
	movq	%rdi, (%rax,%rcx,8)
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	%rsi, %rdi
	jne	.L1936
	jmp	.L1939
.L1915:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2014:
	call	__stack_chk_fail@PLT
.L2017:
	movq	%r13, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2022:
	cmpq	$268435455, %rdi
	movl	$268435455, %esi
	cmovbe	%rdi, %rsi
	salq	$3, %rsi
	jmp	.L1933
.L2020:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE25076:
	.size	_ZN2v88internal8compiler19InstructionSelector32AddOperandToStateValueDescriptorEPNS1_14StateValueListEPNS0_10ZoneVectorINS1_18InstructionOperandEEEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS1_4NodeENS0_11MachineTypeENS1_19FrameStateInputKindEPNS0_4ZoneE, .-_ZN2v88internal8compiler19InstructionSelector32AddOperandToStateValueDescriptorEPNS1_14StateValueListEPNS0_10ZoneVectorINS1_18InstructionOperandEEEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS1_4NodeENS0_11MachineTypeENS1_19FrameStateInputKindEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler19InstructionSelector31AddInputsToFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_4NodeEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS1_19FrameStateInputKindEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector31AddInputsToFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_4NodeEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS1_19FrameStateInputKindEPNS0_4ZoneE
	.type	_ZN2v88internal8compiler19InstructionSelector31AddInputsToFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_4NodeEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS1_19FrameStateInputKindEPNS0_4ZoneE, @function
_ZN2v88internal8compiler19InstructionSelector31AddInputsToFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_4NodeEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS1_19FrameStateInputKindEPNS0_4ZoneE:
.LFB25077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	32(%rdx), %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$520, %rsp
	movq	24(%rbp), %rax
	movq	%rsi, -504(%rbp)
	movq	%rdi, -472(%rbp)
	movq	120(%rsi), %rsi
	movq	%rcx, -480(%rbp)
	movq	%rax, -488(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%rdx), %eax
	andl	$15, %eax
	testq	%rsi, %rsi
	je	.L2024
	cmpl	$15, %eax
	je	.L2025
	leaq	72(%rdx), %rax
.L2026:
	movl	16(%rbp), %ecx
	pushq	-488(%rbp)
	movq	%r14, %r9
	movq	%r13, %r8
	movq	%r10, -496(%rbp)
	pushq	%rcx
	movq	-480(%rbp), %rcx
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler19InstructionSelector31AddInputsToFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_4NodeEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS1_19FrameStateInputKindEPNS0_4ZoneE
	movq	-496(%rbp), %r10
	popq	%rdx
	movq	%rax, %r12
	movzbl	23(%rbx), %eax
	popq	%rcx
	andl	$15, %eax
.L2024:
	movq	32(%rbx), %r15
	cmpl	$15, %eax
	jne	.L2059
	leaq	16(%r15), %r10
	movq	16(%r15), %r15
.L2059:
	movq	8(%r10), %rax
	movq	-504(%rbp), %rdi
	addq	$32, %r10
	movq	%rax, -528(%rbp)
	movq	-16(%r10), %rax
	movq	%rax, -520(%rbp)
	movq	-8(%r10), %rax
	movq	(%r10), %r9
	movq	%rax, -536(%rbp)
	leaq	48(%rdi), %rax
	movq	%r9, -512(%rbp)
	movq	%rax, -496(%rbp)
	call	_ZNK2v88internal8compiler20FrameStateDescriptor7GetSizeEv@PLT
	movq	-512(%rbp), %r9
	cmpq	$134217727, %rax
	ja	.L2060
	movq	-504(%rbp), %rcx
	movq	56(%rcx), %rbx
	movq	72(%rcx), %rcx
	movq	%rcx, %rdx
	movq	%rcx, -512(%rbp)
	subq	%rbx, %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rax
	ja	.L2061
.L2030:
	subq	$8, %rsp
	pushq	-488(%rbp)
	movq	-480(%rbp), %rcx
	movq	%r13, %r8
	pushq	$1
	movq	%r14, %rdx
	leaq	-464(%rbp), %rbx
	pushq	$1800
	movq	-496(%rbp), %rsi
	movq	-472(%rbp), %rdi
	call	_ZN2v88internal8compiler19InstructionSelector32AddOperandToStateValueDescriptorEPNS1_14StateValueListEPNS0_10ZoneVectorINS1_18InstructionOperandEEEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS1_4NodeENS0_11MachineTypeENS1_19FrameStateInputKindEPNS0_4ZoneE
	movq	%r15, %rsi
	addq	$32, %rsp
	movq	%rbx, %rdi
	addq	%rax, %r12
	leaq	-256(%rbp), %r15
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorC1EPNS1_4NodeE@PLT
	movl	$-1, -64(%rbp)
	jmp	.L2038
	.p2align 4,,10
	.p2align 3
.L2062:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratordeEv@PLT
	subq	$8, %rsp
	movq	%r13, %r8
	pushq	-488(%rbp)
	movq	%rax, %r9
	movl	16(%rbp), %eax
	movq	-472(%rbp), %rdi
	movq	-480(%rbp), %rcx
	pushq	%rax
	movq	-496(%rbp), %rsi
	pushq	%rdx
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector32AddOperandToStateValueDescriptorEPNS1_14StateValueListEPNS0_10ZoneVectorINS1_18InstructionOperandEEEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS1_4NodeENS0_11MachineTypeENS1_19FrameStateInputKindEPNS0_4ZoneE
	addq	$32, %rsp
	movq	%rbx, %rdi
	addq	%rax, %r12
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorppEv@PLT
.L2038:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorneERKS3_@PLT
	testb	%al, %al
	jne	.L2062
	movq	-504(%rbp), %rax
	movl	(%rax), %eax
	leal	-2(%rax), %edx
	cmpl	$3, %edx
	jbe	.L2048
	testl	%eax, %eax
	jne	.L2039
.L2048:
	subq	$8, %rsp
	pushq	-488(%rbp)
	movq	-536(%rbp), %r9
	movq	%r13, %r8
	movq	-480(%rbp), %rcx
	pushq	$1
	movq	%r14, %rdx
	pushq	$1800
	movq	-496(%rbp), %rsi
	movq	-472(%rbp), %rdi
	call	_ZN2v88internal8compiler19InstructionSelector32AddOperandToStateValueDescriptorEPNS1_14StateValueListEPNS0_10ZoneVectorINS1_18InstructionOperandEEEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS1_4NodeENS0_11MachineTypeENS1_19FrameStateInputKindEPNS0_4ZoneE
	addq	$32, %rsp
	addq	%rax, %r12
.L2039:
	movq	-528(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorC1EPNS1_4NodeE@PLT
	movl	$-1, -64(%rbp)
	jmp	.L2042
	.p2align 4,,10
	.p2align 3
.L2063:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratordeEv@PLT
	subq	$8, %rsp
	movq	%r13, %r8
	pushq	-488(%rbp)
	movq	%rax, %r9
	movl	16(%rbp), %eax
	movq	-472(%rbp), %rdi
	movq	-480(%rbp), %rcx
	pushq	%rax
	movq	-496(%rbp), %rsi
	pushq	%rdx
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector32AddOperandToStateValueDescriptorEPNS1_14StateValueListEPNS0_10ZoneVectorINS1_18InstructionOperandEEEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS1_4NodeENS0_11MachineTypeENS1_19FrameStateInputKindEPNS0_4ZoneE
	addq	$32, %rsp
	movq	%rbx, %rdi
	addq	%rax, %r12
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorppEv@PLT
.L2042:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorneERKS3_@PLT
	testb	%al, %al
	jne	.L2063
	movq	-520(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorC1EPNS1_4NodeE@PLT
	movl	$-1, -64(%rbp)
	jmp	.L2044
	.p2align 4,,10
	.p2align 3
.L2064:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratordeEv@PLT
	subq	$8, %rsp
	movq	%r13, %r8
	pushq	-488(%rbp)
	movq	%rax, %r9
	movl	16(%rbp), %eax
	movq	-472(%rbp), %rdi
	movq	-480(%rbp), %rcx
	pushq	%rax
	movq	-496(%rbp), %rsi
	pushq	%rdx
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector32AddOperandToStateValueDescriptorEPNS1_14StateValueListEPNS0_10ZoneVectorINS1_18InstructionOperandEEEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS1_4NodeENS0_11MachineTypeENS1_19FrameStateInputKindEPNS0_4ZoneE
	addq	$32, %rsp
	movq	%rbx, %rdi
	addq	%rax, %r12
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorppEv@PLT
.L2044:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler17StateValuesAccess8iteratorneERKS3_@PLT
	testb	%al, %al
	jne	.L2064
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2065
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2025:
	.cfi_restore_state
	movq	32(%rdx), %rax
	addq	$56, %rax
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2061:
	movq	-504(%rbp), %rsi
	movq	%rax, %r8
	xorl	%edi, %edi
	salq	$4, %r8
	movq	64(%rsi), %rdx
	movq	%rdx, %rcx
	subq	%rbx, %rcx
	testq	%rax, %rax
	je	.L2031
	movq	48(%rsi), %r10
	movq	%r8, %rsi
	movq	16(%r10), %rdi
	movq	24(%r10), %rax
	subq	%rdi, %rax
	cmpq	%rax, %r8
	ja	.L2066
	addq	%rdi, %rsi
	movq	%rsi, 16(%r10)
.L2031:
	movq	%rbx, %rax
	movq	%rdi, %rsi
	cmpq	%rdx, %rbx
	je	.L2036
	.p2align 4,,10
	.p2align 3
.L2035:
	movdqu	(%rax), %xmm0
	addq	$16, %rax
	addq	$16, %rsi
	movups	%xmm0, -16(%rsi)
	cmpq	%rax, %rdx
	jne	.L2035
.L2036:
	movq	-504(%rbp), %rax
	addq	%rdi, %rcx
	addq	%rdi, %r8
	movq	%rdi, 56(%rax)
	movq	%rcx, 64(%rax)
	movq	%r8, 72(%rax)
	jmp	.L2030
.L2066:
	movq	%r10, %rdi
	movq	%rcx, -560(%rbp)
	movq	%rdx, -552(%rbp)
	movq	%r8, -544(%rbp)
	movq	%r9, -512(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-512(%rbp), %r9
	movq	-544(%rbp), %r8
	movq	-552(%rbp), %rdx
	movq	-560(%rbp), %rcx
	movq	%rax, %rdi
	jmp	.L2031
.L2065:
	call	__stack_chk_fail@PLT
.L2060:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE25077:
	.size	_ZN2v88internal8compiler19InstructionSelector31AddInputsToFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_4NodeEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS1_19FrameStateInputKindEPNS0_4ZoneE, .-_ZN2v88internal8compiler19InstructionSelector31AddInputsToFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_4NodeEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS1_19FrameStateInputKindEPNS0_4ZoneE
	.section	.text._ZN2v88internal8compiler19InstructionSelector25AppendDeoptimizeArgumentsEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector25AppendDeoptimizeArgumentsEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector25AppendDeoptimizeArgumentsEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector25AppendDeoptimizeArgumentsEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE:
.LFB25084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movq	%r9, %rsi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$88, %rsp
	movl	%ecx, -124(%rbp)
	movq	%r8, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%rdi, -104(%rbp)
	movq	8(%rax), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_131GetFrameStateDescriptorInternalEPNS0_4ZoneEPNS1_4NodeE
	movq	432(%r12), %rdx
	movq	-120(%rbp), %r8
	movq	%rax, %r13
	movl	-124(%rbp), %ecx
	movq	(%rdx), %rax
	cmpq	%rax, 40(%r13)
	movq	%r13, %rsi
	cmovnb	40(%r13), %rax
	movq	%rax, (%rdx)
	movq	16(%r12), %rdi
	movl	%ebx, %edx
	leaq	-96(%rbp), %rbx
	call	_ZN2v88internal8compiler19InstructionSequence22AddDeoptimizationEntryEPNS1_20FrameStateDescriptorENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	movq	-104(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-96(%rbp), %edx
	movq	-120(%rbp), %rax
	testl	%edx, %edx
	jne	.L2068
	cmpb	$19, -92(%rbp)
	je	.L2074
.L2068:
	movq	160(%rax), %rsi
	movq	%rsi, %rcx
	subq	152(%rax), %rcx
	sarq	$4, %rcx
	cmpq	168(%rax), %rsi
	je	.L2070
	movzbl	-92(%rbp), %edi
	movq	-88(%rbp), %r8
	movl	%edx, (%rsi)
	movb	%dil, 4(%rsi)
	movq	%r8, 8(%rsi)
	addq	$16, 160(%rax)
.L2071:
	movq	%rcx, %rax
	salq	$32, %rax
	orq	$11, %rax
.L2069:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movq	16(%r12), %rax
	movq	%r15, %r9
	movq	%rbx, %r8
	leaq	-104(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%r14, %rdx
	movq	$0, -72(%rbp)
	pushq	8(%rax)
	pushq	$0
	call	_ZN2v88internal8compiler19InstructionSelector31AddInputsToFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_4NodeEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS1_19FrameStateInputKindEPNS0_4ZoneE
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2075
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2074:
	.cfi_restore_state
	movslq	-88(%rbp), %rax
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L2069
	.p2align 4,,10
	.p2align 3
.L2070:
	leaq	144(%rax), %rdi
	movq	%rbx, %rdx
	movq	%rcx, -120(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-120(%rbp), %rcx
	jmp	.L2071
.L2075:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25084:
	.size	_ZN2v88internal8compiler19InstructionSelector25AppendDeoptimizeArgumentsEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector25AppendDeoptimizeArgumentsEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE
	.type	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE, @function
_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE:
.LFB25082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r12
	movq	%rdx, -96(%rbp)
	movq	%rcx, -120(%rbp)
	movl	(%r12), %edx
	movq	%r8, -104(%rbp)
	movl	%edx, %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	sall	$14, %ebx
	orl	%esi, %ebx
	testl	%edx, %edx
	je	.L2077
	movl	4(%r12), %edx
	sall	$17, %edx
	orl	%edx, %ebx
.L2077:
	movl	%ebx, -108(%rbp)
	movq	96(%r15), %rsi
	leaq	88(%r15), %r14
	cmpq	104(%r15), %rsi
	je	.L2078
	movq	%rsi, 104(%r15)
.L2078:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L2079
	leaq	(%r9,%rax,8), %r13
	jmp	.L2082
	.p2align 4,,10
	.p2align 3
.L2123:
	movq	(%r9), %rdx
	addq	$8, %r9
	movq	%rdx, (%rsi)
	addq	$8, 104(%r15)
	cmpq	%r9, %r13
	je	.L2079
.L2124:
	movq	104(%r15), %rsi
.L2082:
	cmpq	%rsi, 112(%r15)
	jne	.L2123
	movq	%r9, %rdx
	movq	%r14, %rdi
	movq	%r9, -88(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-88(%rbp), %r9
	addq	$8, %r9
	cmpq	%r9, %r13
	jne	.L2124
	.p2align 4,,10
	.p2align 3
.L2079:
	movq	128(%r15), %rsi
	leaq	120(%r15), %r8
	cmpq	136(%r15), %rsi
	je	.L2083
	movq	%rsi, 136(%r15)
.L2083:
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L2084
	movq	-120(%rbp), %rcx
	leaq	(%rcx,%rax,8), %r13
	jmp	.L2087
	.p2align 4,,10
	.p2align 3
.L2125:
	movq	(%rcx), %rdx
	addq	$8, %rcx
	movq	%rdx, (%rsi)
	addq	$8, 136(%r15)
	cmpq	%r13, %rcx
	je	.L2084
.L2126:
	movq	136(%r15), %rsi
.L2087:
	cmpq	%rsi, 144(%r15)
	jne	.L2125
	movq	%rcx, %rdx
	movq	%r8, %rdi
	movq	%rcx, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %r8
	addq	$8, %rcx
	cmpq	%r13, %rcx
	jne	.L2126
	.p2align 4,,10
	.p2align 3
.L2084:
	movl	(%r12), %edx
	leal	-1(%rdx), %ecx
	cmpl	$1, %ecx
	jbe	.L2127
	leal	-3(%rdx), %ecx
	cmpl	$1, %ecx
	jbe	.L2128
	cmpl	$5, %edx
	je	.L2129
	cmpl	$6, %edx
	je	.L2130
.L2093:
	movq	96(%r15), %r9
	movq	104(%r15), %r8
	movq	128(%r15), %rcx
	movq	136(%r15), %rdx
	subq	%r9, %r8
	subq	%rcx, %rdx
	sarq	$3, %r8
	je	.L2100
	sarq	$3, %rdx
	movl	$0, %eax
	cmove	%rax, %rcx
.L2101:
	cmpq	$254, %rdx
	ja	.L2109
	cmpq	$65534, %r8
	ja	.L2109
.L2102:
	pushq	$0
	movl	-108(%rbp), %esi
	movq	%r15, %rdi
	pushq	$0
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0
	popq	%rdx
	popq	%rcx
.L2076:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L2131
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2100:
	.cfi_restore_state
	sarq	$3, %rdx
	jne	.L2108
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	jmp	.L2102
	.p2align 4,,10
	.p2align 3
.L2127:
	movq	40(%r12), %rcx
	movq	16(%r15), %rdx
	movslq	4(%rcx), %rcx
	movl	$7, -80(%rbp)
	movb	$19, -76(%rbp)
	movq	%rcx, -72(%rbp)
	movq	160(%rdx), %rsi
	movq	%rsi, %rbx
	subq	152(%rdx), %rbx
	sarq	$4, %rbx
	cmpq	168(%rdx), %rsi
	je	.L2089
	movl	$7, (%rsi)
	leaq	-80(%rbp), %r8
	movb	$19, 4(%rsi)
	movq	%rcx, 8(%rsi)
	addq	$16, 160(%rdx)
.L2090:
	salq	$32, %rbx
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%r8, -88(%rbp)
	orq	$11, %rbx
	movq	%rbx, -80(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movq	48(%r12), %rax
	movq	16(%r15), %rdx
	movq	-88(%rbp), %r8
	movslq	4(%rax), %rax
	movl	$7, -80(%rbp)
	movb	$19, -76(%rbp)
	movq	%rax, -72(%rbp)
	movq	160(%rdx), %rsi
	movq	%rsi, %rbx
	subq	152(%rdx), %rbx
	sarq	$4, %rbx
	cmpq	168(%rdx), %rsi
	je	.L2091
	movl	$7, (%rsi)
	movb	$19, 4(%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, 160(%rdx)
.L2092:
	salq	$32, %rbx
	movq	%r8, %rsi
	movq	%r14, %rdi
	orq	$11, %rbx
	movq	%rbx, -80(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	jmp	.L2093
	.p2align 4,,10
	.p2align 3
.L2108:
	xorl	%r9d, %r9d
	jmp	.L2101
	.p2align 4,,10
	.p2align 3
.L2129:
	movq	32(%r12), %rbx
	movq	%r15, %rdi
	movq	%r8, -88(%rbp)
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE
	movl	20(%rbx), %ecx
	movl	$1, %esi
	movq	-88(%rbp), %r8
	movl	%eax, %eax
	salq	%cl, %rsi
	movq	%rcx, %rdx
	salq	$3, %rax
	movq	%r8, %rdi
	shrq	$3, %rdx
	andl	$2097144, %edx
	addq	160(%r15), %rdx
	orq	%rsi, (%rdx)
	leaq	-80(%rbp), %rsi
	movabsq	$927712935937, %rdx
	orq	%rdx, %rax
	movq	%rax, -80(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	jmp	.L2093
	.p2align 4,,10
	.p2align 3
.L2128:
	movl	-104(%rbp), %eax
	movzbl	9(%r12), %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movzbl	8(%r12), %edx
	movq	32(%r12), %r9
	leaq	16(%r12), %r8
	sall	$22, %eax
	orl	%ebx, %eax
	movl	%eax, -108(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector25AppendDeoptimizeArgumentsEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE
	jmp	.L2093
	.p2align 4,,10
	.p2align 3
.L2109:
	movb	$1, 376(%r15)
	xorl	%eax, %eax
	jmp	.L2076
.L2130:
	leaq	-80(%rbp), %r8
	movl	56(%r12), %esi
	movq	16(%r15), %r13
	movq	%r8, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	movq	-88(%rbp), %r8
	testl	%eax, %eax
	jne	.L2096
	cmpb	$19, -76(%rbp)
	je	.L2132
.L2096:
	movq	160(%r13), %rsi
	movq	%rsi, %rbx
	subq	152(%r13), %rbx
	sarq	$4, %rbx
	cmpq	168(%r13), %rsi
	je	.L2098
	movzbl	-76(%rbp), %ecx
	movq	-72(%rbp), %rdx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%r13)
.L2099:
	movq	%rbx, %rax
	salq	$32, %rax
	orq	$11, %rax
.L2097:
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	jmp	.L2093
.L2132:
	movslq	-72(%rbp), %rax
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L2097
.L2089:
	leaq	-80(%rbp), %r8
	leaq	144(%rdx), %rdi
	movq	%r8, %rdx
	movq	%r8, -88(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-88(%rbp), %r8
	jmp	.L2090
.L2091:
	leaq	144(%rdx), %rdi
	movq	%r8, %rdx
	movq	%r8, -88(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-88(%rbp), %r8
	jmp	.L2092
.L2098:
	movq	%r8, %rdx
	leaq	144(%r13), %rdi
	movq	%r8, -88(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-88(%rbp), %r8
	jmp	.L2099
.L2131:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25082:
	.size	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE, .-_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14EmitDeoptimizeEimPNS1_18InstructionOperandEmS4_NS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector14EmitDeoptimizeEimPNS1_18InstructionOperandEmS4_NS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector14EmitDeoptimizeEimPNS1_18InstructionOperandEmS4_NS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector14EmitDeoptimizeEimPNS1_18InstructionOperandEmS4_NS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE:
.LFB25088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-96(%rbp), %rbx
	subq	$104, %rsp
	movl	16(%rbp), %edi
	movq	16(%r15), %rdx
	movl	%esi, -100(%rbp)
	movq	32(%rbp), %r8
	movq	40(%rbp), %r9
	movq	%rcx, -120(%rbp)
	movl	%edi, -104(%rbp)
	movl	24(%rbp), %edi
	movq	8(%rdx), %rdx
	movl	%edi, -108(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rdx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	testq	%r14, %r14
	je	.L2134
	leaq	-96(%rbp), %rbx
	movq	%r13, -128(%rbp)
	leaq	(%rax,%r14,8), %r12
	xorl	%edx, %edx
	movq	%rbx, %r13
	movq	%r8, -136(%rbp)
	xorl	%esi, %esi
	movq	%rax, %rbx
	movq	%r9, -144(%rbp)
	jmp	.L2137
	.p2align 4,,10
	.p2align 3
.L2148:
	movq	(%rbx), %rdx
	addq	$8, %rbx
	movq	%rdx, (%rsi)
	addq	$8, -80(%rbp)
	cmpq	%r12, %rbx
	je	.L2146
.L2149:
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
.L2137:
	cmpq	%rdx, %rsi
	jne	.L2148
	movq	%rbx, %rdx
	movq	%r13, %rdi
	addq	$8, %rbx
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	cmpq	%r12, %rbx
	jne	.L2149
	.p2align 4,,10
	.p2align 3
.L2146:
	movq	%r13, %rbx
	movq	-136(%rbp), %r8
	movq	-128(%rbp), %r13
	movq	-144(%rbp), %r9
.L2134:
	movl	-100(%rbp), %r12d
	movl	%r14d, %ecx
	movzbl	-104(%rbp), %edx
	movq	%rbx, %rsi
	sall	$22, %ecx
	movq	%r15, %rdi
	orl	%ecx, %r12d
	movzbl	-108(%rbp), %ecx
	call	_ZN2v88internal8compiler19InstructionSelector25AppendDeoptimizeArgumentsEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rax
	subq	%r9, %rax
	movq	%rax, %r8
	sarq	$3, %r8
	cmpq	$524279, %rax
	ja	.L2142
	cmpq	$254, %r13
	ja	.L2142
	pushq	$0
	movq	-120(%rbp), %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	pushq	$0
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0
	popq	%rdx
	popq	%rcx
.L2133:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L2150
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2142:
	.cfi_restore_state
	movb	$1, 376(%r15)
	xorl	%eax, %eax
	jmp	.L2133
.L2150:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25088:
	.size	_ZN2v88internal8compiler19InstructionSelector14EmitDeoptimizeEimPNS1_18InstructionOperandEmS4_NS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector14EmitDeoptimizeEimPNS1_18InstructionOperandEmS4_NS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE.constprop.0, @function
_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE.constprop.0:
.LFB32660:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	movl	%eax, %r13d
	sall	$14, %r13d
	orl	%esi, %r13d
	testl	%eax, %eax
	je	.L2152
	movl	4(%r8), %eax
	sall	$17, %eax
	orl	%eax, %r13d
.L2152:
	movl	%r13d, -100(%rbp)
	movq	96(%r12), %rsi
	leaq	88(%r12), %r15
	cmpq	104(%r12), %rsi
	je	.L2153
	movq	%rsi, 104(%r12)
.L2153:
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L2154
	movq	%rcx, %rbx
	leaq	(%rcx,%rax,8), %r14
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L2193:
	movq	(%rbx), %rax
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, 104(%r12)
	cmpq	%rbx, %r14
	je	.L2154
.L2194:
	movq	104(%r12), %rsi
.L2157:
	cmpq	112(%r12), %rsi
	jne	.L2193
	movq	%rbx, %rdx
	movq	%r15, %rdi
	addq	$8, %rbx
	movq	%r8, -88(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-88(%rbp), %r8
	cmpq	%rbx, %r14
	jne	.L2194
	.p2align 4,,10
	.p2align 3
.L2154:
	movq	128(%r12), %rcx
	movq	%rcx, %rdx
	cmpq	136(%r12), %rcx
	je	.L2158
	movq	%rcx, 136(%r12)
.L2158:
	movl	(%r8), %eax
	leal	-1(%rax), %esi
	cmpl	$1, %esi
	jbe	.L2195
	leal	-3(%rax), %esi
	cmpl	$1, %esi
	jbe	.L2196
	cmpl	$5, %eax
	je	.L2197
	cmpl	$6, %eax
	je	.L2198
.L2164:
	movq	96(%r12), %r9
	movq	104(%r12), %r8
	subq	%rcx, %rdx
	subq	%r9, %r8
	sarq	$3, %r8
	je	.L2171
	sarq	$3, %rdx
	movl	$0, %eax
	cmove	%rax, %rcx
.L2172:
	cmpq	$254, %rdx
	ja	.L2181
	cmpq	$65534, %r8
	ja	.L2181
.L2173:
	pushq	$0
	movl	-100(%rbp), %esi
	movq	%r12, %rdi
	pushq	$0
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0
	popq	%rdx
	popq	%rcx
.L2151:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L2199
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2171:
	.cfi_restore_state
	xorl	%r9d, %r9d
	sarq	$3, %rdx
	jne	.L2172
	xorl	%ecx, %ecx
	jmp	.L2173
	.p2align 4,,10
	.p2align 3
.L2195:
	movq	40(%r8), %rdx
	movq	16(%r12), %rax
	movslq	4(%rdx), %rdx
	movl	$7, -80(%rbp)
	movb	$19, -76(%rbp)
	movq	%rdx, -72(%rbp)
	movq	160(%rax), %rsi
	movq	%rsi, %rbx
	subq	152(%rax), %rbx
	sarq	$4, %rbx
	cmpq	168(%rax), %rsi
	je	.L2160
	movl	$7, (%rsi)
	leaq	-80(%rbp), %r13
	movb	$19, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rax)
.L2161:
	salq	$32, %rbx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r8, -88(%rbp)
	orq	$11, %rbx
	movq	%rbx, -80(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movq	-88(%rbp), %r8
	movq	16(%r12), %rax
	movq	48(%r8), %rdx
	movslq	4(%rdx), %rdx
	movl	$7, -80(%rbp)
	movb	$19, -76(%rbp)
	movq	%rdx, -72(%rbp)
	movq	160(%rax), %rsi
	movq	%rsi, %rbx
	subq	152(%rax), %rbx
	sarq	$4, %rbx
	cmpq	168(%rax), %rsi
	je	.L2162
	movl	$7, (%rsi)
	movb	$19, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rax)
.L2163:
	salq	$32, %rbx
	orq	$11, %rbx
	movq	%rbx, -80(%rbp)
.L2192:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movq	128(%r12), %rcx
	movq	136(%r12), %rdx
	jmp	.L2164
	.p2align 4,,10
	.p2align 3
.L2197:
	movq	32(%r8), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE
	movl	20(%rbx), %ecx
	movl	$1, %esi
	leaq	120(%r12), %rdi
	movl	%eax, %eax
	salq	%cl, %rsi
	movq	%rcx, %rdx
	salq	$3, %rax
	shrq	$3, %rdx
	andl	$2097144, %edx
	addq	160(%r12), %rdx
	orq	%rsi, (%rdx)
	leaq	-80(%rbp), %rsi
	movabsq	$927712935937, %rdx
	orq	%rdx, %rax
	movq	%rax, -80(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movq	128(%r12), %rcx
	movq	136(%r12), %rdx
	jmp	.L2164
	.p2align 4,,10
	.p2align 3
.L2196:
	movl	-96(%rbp), %r14d
	movzbl	9(%r8), %ecx
	movq	%r15, %rsi
	addq	$16, %r8
	movzbl	-8(%r8), %edx
	movq	16(%r8), %r9
	movq	%r12, %rdi
	sall	$22, %r14d
	orl	%r13d, %r14d
	movl	%r14d, -100(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector25AppendDeoptimizeArgumentsEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE
	movq	128(%r12), %rcx
	movq	136(%r12), %rdx
	jmp	.L2164
	.p2align 4,,10
	.p2align 3
.L2181:
	movb	$1, 376(%r12)
	xorl	%eax, %eax
	jmp	.L2151
.L2198:
	movl	56(%r8), %esi
	leaq	-80(%rbp), %r13
	movq	16(%r12), %r14
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	jne	.L2167
	cmpb	$19, -76(%rbp)
	je	.L2200
.L2167:
	movq	160(%r14), %rsi
	movq	%rsi, %rbx
	subq	152(%r14), %rbx
	sarq	$4, %rbx
	cmpq	168(%r14), %rsi
	je	.L2169
	movzbl	-76(%rbp), %ecx
	movq	-72(%rbp), %rdx
	movl	%eax, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%r14)
.L2170:
	movq	%rbx, %rax
	salq	$32, %rax
	orq	$11, %rax
.L2168:
	movq	%rax, -80(%rbp)
	jmp	.L2192
.L2200:
	movslq	-72(%rbp), %rax
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L2168
.L2160:
	leaq	-80(%rbp), %r13
	leaq	144(%rax), %rdi
	movq	%r8, -88(%rbp)
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-88(%rbp), %r8
	jmp	.L2161
.L2162:
	leaq	144(%rax), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L2163
.L2169:
	leaq	144(%r14), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L2170
.L2199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE32660:
	.size	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE.constprop.0, .-_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE.constprop.0
	.section	.text._ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiPNS1_17FlagsContinuationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiPNS1_17FlagsContinuationE
	.type	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiPNS1_17FlagsContinuationE, @function
_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiPNS1_17FlagsContinuationE:
.LFB25078:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE.constprop.0
	.cfi_endproc
.LFE25078:
	.size	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiPNS1_17FlagsContinuationE, .-_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiPNS1_17FlagsContinuationE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandEPNS1_17FlagsContinuationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandEPNS1_17FlagsContinuationE
	.type	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandEPNS1_17FlagsContinuationE, @function
_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandEPNS1_17FlagsContinuationE:
.LFB25079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	leaq	-8(%rbp), %rcx
	movq	%rdx, -8(%rbp)
	movl	$1, %edx
	call	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE.constprop.0
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25079:
	.size	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandEPNS1_17FlagsContinuationE, .-_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandEPNS1_17FlagsContinuationE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_PNS1_17FlagsContinuationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_PNS1_17FlagsContinuationE
	.type	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_PNS1_17FlagsContinuationE, @function
_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_PNS1_17FlagsContinuationE:
.LFB25080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -32(%rbp)
	movl	$2, %edx
	movq	%rcx, -24(%rbp)
	leaq	-32(%rbp), %rcx
	call	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE.constprop.0
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2207
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2207:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25080:
	.size	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_PNS1_17FlagsContinuationE, .-_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_PNS1_17FlagsContinuationE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_S3_PNS1_17FlagsContinuationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_S3_PNS1_17FlagsContinuationE
	.type	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_S3_PNS1_17FlagsContinuationE, @function
_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_S3_PNS1_17FlagsContinuationE:
.LFB25081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -32(%rbp)
	movl	$3, %edx
	movq	%rcx, -24(%rbp)
	leaq	-32(%rbp), %rcx
	movq	%r8, -16(%rbp)
	movq	%r9, %r8
	call	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEimPNS1_18InstructionOperandEmS4_PNS1_17FlagsContinuationE.constprop.0
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2211
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2211:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25081:
	.size	_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_S3_PNS1_17FlagsContinuationE, .-_ZN2v88internal8compiler19InstructionSelector20EmitWithContinuationEiNS1_18InstructionOperandES3_S3_PNS1_17FlagsContinuationE
	.section	.text._ZN2v88internal8compiler19InstructionSelector14EmitDeoptimizeEimPNS1_18InstructionOperandEmS4_NS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler19InstructionSelector14EmitDeoptimizeEimPNS1_18InstructionOperandEmS4_NS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE.constprop.0, @function
_ZN2v88internal8compiler19InstructionSelector14EmitDeoptimizeEimPNS1_18InstructionOperandEmS4_NS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE.constprop.0:
.LFB32659:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	movq	%rcx, %r8
	movl	%edx, %ecx
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-64(%rbp), %r10
	movq	%r10, %rsi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	8(%rax), %rax
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movq	%rax, -64(%rbp)
	movq	$0, -40(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector25AppendDeoptimizeArgumentsEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE
	movq	-56(%rbp), %r8
	movq	-48(%rbp), %rax
	subq	%r8, %rax
	movq	%rax, %rcx
	sarq	$3, %rcx
	cmpq	$524279, %rax
	ja	.L2217
	xorl	%edx, %edx
	movl	$22, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0
.L2212:
	movq	-24(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L2218
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2217:
	.cfi_restore_state
	movb	$1, 376(%r12)
	xorl	%eax, %eax
	jmp	.L2212
.L2218:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE32659:
	.size	_ZN2v88internal8compiler19InstructionSelector14EmitDeoptimizeEimPNS1_18InstructionOperandEmS4_NS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE.constprop.0, .-_ZN2v88internal8compiler19InstructionSelector14EmitDeoptimizeEimPNS1_18InstructionOperandEmS4_NS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE.constprop.0
	.section	.text._ZN2v88internal8compiler19InstructionSelector15VisitDeoptimizeENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector15VisitDeoptimizeENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector15VisitDeoptimizeENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector15VisitDeoptimizeENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE:
.LFB25202:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal8compiler19InstructionSelector14EmitDeoptimizeEimPNS1_18InstructionOperandEmS4_NS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE.constprop.0
	.cfi_endproc
.LFE25202:
	.size	_ZN2v88internal8compiler19InstructionSelector15VisitDeoptimizeENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector15VisitDeoptimizeENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector20InitializeCallBufferEPNS1_4NodeEPNS1_10CallBufferENS_4base5FlagsINS2_14CallBufferFlagEiEEbi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector20InitializeCallBufferEPNS1_4NodeEPNS1_10CallBufferENS_4base5FlagsINS2_14CallBufferFlagEiEEbi
	.type	_ZN2v88internal8compiler19InstructionSelector20InitializeCallBufferEPNS1_4NodeEPNS1_10CallBufferENS_4base5FlagsINS2_14CallBufferFlagEiEEbi, @function
_ZN2v88internal8compiler19InstructionSelector20InitializeCallBufferEPNS1_4NodeEPNS1_10CallBufferENS_4base5FlagsINS2_14CallBufferFlagEiEEbi:
.LFB25098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$184, %rsp
	movl	%ecx, -160(%rbp)
	movq	(%rdx), %rcx
	movq	%rdi, -128(%rbp)
	movl	%r8d, -176(%rbp)
	movq	24(%rcx), %rdx
	movl	%r9d, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -112(%rbp)
	movq	(%rdx), %r13
	testq	%r13, %r13
	jne	.L2378
	leaq	-96(%rbp), %rax
	movq	%rax, -136(%rbp)
.L2221:
	leaq	32(%r12), %rax
	movq	32(%r12), %r13
	movq	%rax, -120(%rbp)
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2254
	movq	16(%r13), %r13
.L2254:
	movl	-160(%rbp), %eax
	movl	%eax, %edi
	andl	$8, %eax
	andl	$2, %edi
	movl	%eax, %esi
	leaq	80(%rbx), %rax
	cmpl	$6, 8(%rcx)
	movq	%rax, -208(%rbp)
	ja	.L2255
	movl	8(%rcx), %eax
	leaq	.L2257(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector20InitializeCallBufferEPNS1_4NodeEPNS1_10CallBufferENS_4base5FlagsINS2_14CallBufferFlagEiEEbi,"a",@progbits
	.align 4
	.align 4
.L2257:
	.long	.L2261-.L2257
	.long	.L2260-.L2257
	.long	.L2259-.L2257
	.long	.L2258-.L2257
	.long	.L2258-.L2257
	.long	.L2258-.L2257
	.long	.L2256-.L2257
	.section	.text._ZN2v88internal8compiler19InstructionSelector20InitializeCallBufferEPNS1_4NodeEPNS1_10CallBufferENS_4base5FlagsINS2_14CallBufferFlagEiEEbi
.L2259:
	testl	%edi, %edi
	je	.L2256
	movq	0(%r13), %rax
	cmpw	$27, 16(%rax)
	je	.L2391
	.p2align 4,,10
	.p2align 3
.L2256:
	testl	%esi, %esi
	je	.L2276
.L2394:
	movq	-112(%rbp), %rdx
	movl	20(%r13), %ecx
	movq	288(%rdx), %rax
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	movl	(%rax,%r14,4), %eax
	cmpl	$-1, %eax
	je	.L2399
.L2277:
	movq	208(%rdx), %rsi
	shrq	$6, %r14
	movl	%eax, %eax
	movl	$1, %edx
	salq	%cl, %rdx
	salq	$3, %rax
	orq	%rdx, (%rsi,%r14,8)
	movabsq	$2989297238017, %rdx
	orq	%rdx, %rax
	movq	%rax, -96(%rbp)
.L2390:
	movq	-136(%rbp), %rsi
	leaq	80(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
.L2255:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %rdi
	movl	$-1, %esi
	movq	16(%rax), %r14
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-96(%rbp), %edx
	testl	%edx, %edx
	jne	.L2284
	cmpb	$19, -92(%rbp)
	je	.L2400
.L2284:
	movq	160(%r14), %rsi
	movq	%rsi, %r13
	subq	152(%r14), %r13
	sarq	$4, %r13
	cmpq	168(%r14), %rsi
	je	.L2286
	movzbl	-92(%rbp), %ecx
	movq	-88(%rbp), %rax
	movl	%edx, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, 160(%r14)
.L2287:
	movq	%r13, %rax
	salq	$32, %rax
	orq	$11, %rax
.L2285:
	movq	-136(%rbp), %rsi
	leaq	80(%rbx), %rdi
	movq	%rax, -96(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2288
	movq	(%rbx), %rax
	movzbl	23(%r12), %edx
	movq	24(%rax), %rax
	andl	$15, %edx
	movq	8(%rax), %rax
	addq	$1, %rax
	cltq
	salq	$3, %rax
	cmpl	$15, %edx
	je	.L2289
	addq	-120(%rbp), %rax
	cmpb	$0, -176(%rbp)
	movq	(%rax), %r13
	jne	.L2401
.L2291:
	movq	-128(%rbp), %rax
	movl	$32, %ecx
	movq	-136(%rbp), %r15
	movl	$2, %edx
	movq	$0, -96(%rbp)
	movq	16(%rax), %rdi
	movq	%r15, %r8
	movl	$-1, -88(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence22AddDeoptimizationEntryEPNS1_20FrameStateDescriptorENS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceE@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	movq	-112(%rbp), %rax
	movq	16(%rax), %rdx
	movq	%rdx, -144(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-96(%rbp), %ecx
	movq	-144(%rbp), %rdx
	testl	%ecx, %ecx
	jne	.L2293
	cmpb	$19, -92(%rbp)
	je	.L2402
.L2293:
	movq	160(%rdx), %rsi
	movq	%rsi, %r14
	subq	152(%rdx), %r14
	sarq	$4, %r14
	cmpq	168(%rdx), %rsi
	je	.L2295
	movzbl	-92(%rbp), %edi
	movq	-88(%rbp), %rax
	movl	%ecx, (%rsi)
	movb	%dil, 4(%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, 160(%rdx)
.L2296:
	movq	%r14, %rax
	salq	$32, %rax
	orq	$11, %rax
.L2294:
	movq	-136(%rbp), %r14
	movq	-208(%rbp), %r15
	movq	%rax, -96(%rbp)
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movq	-128(%rbp), %rdi
	movq	8(%rbx), %rsi
	movq	%r15, %r9
	leaq	-112(%rbp), %rcx
	movq	%r14, %r8
	movq	16(%rdi), %rax
	movq	8(%rax), %rdx
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%r13, %rdx
	movq	$0, -72(%rbp)
	pushq	8(%rax)
	pushq	$1
	call	_ZN2v88internal8compiler19InstructionSelector31AddInputsToFrameStateDescriptorEPNS1_20FrameStateDescriptorEPNS1_4NodeEPNS1_16OperandGeneratorEPNS1_23StateObjectDeduplicatorEPNS0_10ZoneVectorINS1_18InstructionOperandEEENS1_19FrameStateInputKindEPNS0_4ZoneE
	popq	%rdx
	popq	%rcx
.L2288:
	movq	(%rbx), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %r9
	movzbl	23(%r12), %eax
	andl	$15, %eax
	leaq	1(%r9), %rdx
	cmpl	$15, %eax
	jne	.L2297
	movq	32(%r12), %rax
	addq	$16, %rax
	movq	%rax, -120(%rbp)
.L2297:
	movl	-160(%rbp), %r8d
	movq	%rbx, %r10
	andl	$4, %r8d
	setne	-209(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	jne	.L2298
	jmp	.L2304
	.p2align 4,,10
	.p2align 3
.L2308:
	movq	%r12, %rax
	salq	$36, %rax
	orq	%rcx, %rax
	orq	$1, %rax
.L2311:
	movq	208(%r11), %r12
	movl	%edi, %ecx
	shrq	$6, %rbx
	movl	$1, %r11d
	salq	%cl, %r11
	movq	%rax, %rcx
	shrq	$35, %rcx
	orq	%r11, (%r12,%rbx,8)
	movq	%rax, -104(%rbp)
	testb	$1, %cl
	jne	.L2337
	testl	%r8d, %r8d
	je	.L2313
.L2337:
	movq	-128(%rbp), %rdi
	movq	96(%r10), %rsi
	cmpl	$1, 364(%rdi)
	je	.L2310
	andl	$1, %ecx
	je	.L2310
	movq	%rax, %rdx
	shrq	$36, %rdx
	andl	$7, %edx
	cmpl	$3, %edx
	je	.L2403
	.p2align 4,,10
	.p2align 3
.L2310:
	cmpq	%rsi, 104(%r10)
	je	.L2325
.L2408:
	movq	%rax, (%rsi)
	addq	$8, 96(%r10)
.L2320:
	movq	%r14, %rax
.L2298:
	leaq	1(%rax), %r14
	cmpq	%rax, %r9
	je	.L2404
	movq	(%r10), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdx
	movq	16(%rax), %rax
	leaq	-1(%r14,%rdx), %rdx
	leaq	(%rax,%rdx,8), %rax
	movl	(%rax), %r15d
	movzbl	4(%rax), %esi
	movzwl	4(%rax), %edx
	movl	%r15d, %r13d
	movl	%r15d, %r12d
	notl	%r13d
	sarl	%r12d
	andl	$1, %r13d
	testl	%r8d, %r8d
	je	.L2303
	testb	%r13b, %r13b
	jne	.L2303
	addl	-152(%rbp), %r12d
	leal	1(%r12,%r12), %r15d
	movl	%r15d, %r12d
	sarl	%r12d
	.p2align 4,,10
	.p2align 3
.L2303:
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %r11
	movq	(%rax,%r14,8), %rcx
	movq	288(%r11), %rax
	movl	20(%rcx), %edi
	movl	%edi, %ebx
	andl	$16777215, %ebx
	movl	(%rax,%rbx,4), %eax
	cmpl	$-1, %eax
	je	.L2405
.L2307:
	movl	%eax, %eax
	leaq	0(,%rax,8), %rcx
	testb	%r13b, %r13b
	je	.L2308
	cmpl	$-1, %r12d
	je	.L2406
	movabsq	$790273982465, %rax
	salq	$41, %r12
	orq	%rcx, %r12
	movabsq	$858993459201, %rcx
	orq	%r12, %rcx
	orq	%rax, %r12
	cmpb	$12, %sil
	movq	%rcx, %rax
	cmovb	%r12, %rax
	jmp	.L2311
	.p2align 4,,10
	.p2align 3
.L2313:
	movq	128(%r10), %r12
	movq	120(%r10), %rdi
	sarq	$36, %rax
	notl	%eax
	movq	%r12, %r11
	movslq	%eax, %rbx
	subq	%rdi, %r11
	sarq	$4, %r11
	cmpq	%r11, %rbx
	jb	.L2317
	subl	$1, %esi
	cmpb	$13, %sil
	ja	.L2229
	movzbl	%sil, %esi
	leaq	CSWTCH.528(%rip), %rcx
	movl	(%rcx,%rsi,4), %ecx
	movl	$1, %esi
	movl	%esi, %r13d
	sall	%cl, %r13d
	movl	%r13d, %ecx
	sarl	$3, %ecx
	cmpl	$15, %r13d
	cmovg	%ecx, %esi
	addl	%eax, %esi
	movslq	%esi, %rsi
	cmpq	%r11, %rsi
	ja	.L2407
	jnb	.L2317
	salq	$4, %rsi
	addq	%rdi, %rsi
	cmpq	%rsi, %r12
	je	.L2317
	movq	%rsi, 128(%r10)
	.p2align 4,,10
	.p2align 3
.L2317:
	movq	-120(%rbp), %rax
	salq	$4, %rbx
	addq	%rbx, %rdi
	movq	(%rax,%r14,8), %rax
	movl	%r15d, 8(%rdi)
	movw	%dx, 12(%rdi)
	movq	%rax, (%rdi)
	jmp	.L2320
	.p2align 4,,10
	.p2align 3
.L2405:
	movq	16(%r11), %rdi
	movq	%r10, -200(%rbp)
	movq	%r9, -192(%rbp)
	movl	%r8d, -184(%rbp)
	movl	%edx, -176(%rbp)
	movb	%sil, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%r11, -144(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	-144(%rbp), %r11
	movq	-160(%rbp), %rcx
	movq	-200(%rbp), %r10
	movq	-192(%rbp), %r9
	movq	288(%r11), %rdi
	movl	-184(%rbp), %r8d
	movl	-176(%rbp), %edx
	movzbl	-168(%rbp), %esi
	movl	%eax, (%rdi,%rbx,4)
	movl	20(%rcx), %edi
	movq	-112(%rbp), %r11
	movl	%edi, %ebx
	andl	$16777215, %ebx
	jmp	.L2307
	.p2align 4,,10
	.p2align 3
.L2406:
	movq	208(%r11), %rsi
	shrq	$6, %rbx
	movl	$1, %edx
	movabsq	$927712935937, %rax
	orq	%rcx, %rax
	movl	%edi, %ecx
	salq	%cl, %rdx
	orq	%rdx, (%rsi,%rbx,8)
	movq	96(%r10), %rsi
	movq	%rax, -104(%rbp)
	cmpq	%rsi, 104(%r10)
	jne	.L2408
.L2325:
	movq	-208(%rbp), %rdi
	leaq	-104(%rbp), %rdx
	movq	%r10, -168(%rbp)
	movq	%r9, -160(%rbp)
	movl	%r8d, -144(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movl	-144(%rbp), %r8d
	movq	-160(%rbp), %r9
	movq	-168(%rbp), %r10
	jmp	.L2320
	.p2align 4,,10
	.p2align 3
.L2404:
	movq	%r10, %rbx
.L2304:
	movl	-152(%rbp), %eax
	testl	%eax, %eax
	je	.L2220
	cmpb	$0, -209(%rbp)
	jne	.L2409
.L2220:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2410
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2403:
	.cfi_restore_state
	movq	%rax, %rdx
	shrq	$41, %rdx
	andl	$63, %edx
	cmpl	$12, %edx
	jne	.L2310
	movq	88(%r10), %r12
	movq	-112(%rbp), %rax
	movq	%r9, -168(%rbp)
	movq	-136(%rbp), %rdi
	movl	%r8d, -160(%rbp)
	subq	%r12, %rsi
	movq	%r10, -144(%rbp)
	movq	16(%rax), %r13
	sarq	$3, %rsi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-96(%rbp), %edx
	movq	-144(%rbp), %r10
	movl	-160(%rbp), %r8d
	movq	-168(%rbp), %r9
	testl	%edx, %edx
	jne	.L2321
	cmpb	$19, -92(%rbp)
	je	.L2411
.L2321:
	movq	160(%r13), %rsi
	movq	%rsi, %rbx
	subq	152(%r13), %rbx
	sarq	$4, %rbx
	cmpq	168(%r13), %rsi
	je	.L2323
	movzbl	-92(%rbp), %ecx
	movq	-88(%rbp), %rax
	movl	%edx, (%rsi)
	movb	%cl, 4(%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, 160(%r13)
.L2324:
	movq	%rbx, %rax
	salq	$32, %rax
	orq	$11, %rax
.L2322:
	movq	%rax, 8(%r12)
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rdi
	movq	%r10, -168(%rbp)
	movq	(%rax,%r14,8), %rbx
	movq	%r9, -160(%rbp)
	movl	%r8d, -144(%rbp)
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE
	movl	20(%rbx), %ecx
	movq	-112(%rbp), %rsi
	movabsq	$171798691841, %rdi
	movl	%eax, %eax
	movq	-168(%rbp), %r10
	movq	-160(%rbp), %r9
	movq	%rcx, %rdx
	salq	$3, %rax
	movl	-144(%rbp), %r8d
	shrq	$3, %rdx
	orq	%rdi, %rax
	andl	$2097144, %edx
	addq	208(%rsi), %rdx
	movl	$1, %esi
	salq	%cl, %rsi
	orq	%rsi, (%rdx)
	movq	96(%r10), %rsi
	movq	%rax, -104(%rbp)
	jmp	.L2310
	.p2align 4,,10
	.p2align 3
.L2378:
	movq	32(%rbx), %rsi
	leaq	16(%rbx), %rdi
	cmpq	$1, %r13
	je	.L2412
	movq	24(%rbx), %rax
	movq	%rsi, %r8
	subq	%rax, %r8
	sarq	$4, %r8
	cmpq	%r8, %r13
	ja	.L2413
	jb	.L2414
.L2227:
	xorl	%esi, %esi
	xorl	%r15d, %r15d
	leaq	CSWTCH.528(%rip), %r9
	movq	%rax, %r10
	movl	$1, %r8d
	jmp	.L2231
	.p2align 4,,10
	.p2align 3
.L2228:
	addq	$1, %rsi
	cmpq	%r13, %rsi
	je	.L2230
.L2415:
	movq	(%rbx), %rax
	movq	24(%rbx), %r10
	movq	24(%rax), %rdx
.L2231:
	movq	16(%rdx), %rdx
	movq	%rsi, %rax
	salq	$4, %rax
	leaq	(%rdx,%rsi,8), %rdx
	addq	%r10, %rax
	movl	(%rdx), %ecx
	movzbl	4(%rdx), %edi
	movzwl	4(%rdx), %edx
	movq	$0, (%rax)
	movl	%ecx, 8(%rax)
	movw	%dx, 12(%rax)
	testb	$1, %cl
	je	.L2228
	testl	%ecx, %ecx
	jns	.L2228
	subl	$1, %edi
	cmpb	$13, %dil
	ja	.L2229
	movzbl	%dil, %edi
	movl	%r8d, %edx
	movl	(%r9,%rdi,4), %ecx
	sall	%cl, %edx
	leal	14(%rdx), %eax
	addl	$7, %edx
	cmovns	%edx, %eax
	addq	$1, %rsi
	sarl	$3, %eax
	addl	%eax, %r15d
	cmpq	%r13, %rsi
	jne	.L2415
.L2230:
	movq	24(%r12), %r14
	testq	%r14, %r14
	je	.L2232
	movq	(%r14), %r13
	.p2align 4,,10
	.p2align 3
.L2237:
	movl	16(%r14), %ecx
	movq	%r14, %rdi
	movl	%ecx, %edx
	shrl	%edx
	leaq	3(%rdx,%rdx,2), %rax
	leaq	(%r14,%rax,8), %rax
	leaq	32(%rax), %rsi
	addq	$16, %rax
	andl	$1, %ecx
	cmovne	%rsi, %rax
	leaq	(%rax,%rdx,8), %rsi
	call	_ZN2v88internal8compiler14NodeProperties11IsValueEdgeENS1_4EdgeE@PLT
	testb	%al, %al
	je	.L2235
	movl	16(%r14), %edx
	movl	%edx, %eax
	shrl	%eax
	andl	$1, %edx
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r14,%rax,8), %r14
	movq	(%r14), %rdi
	jne	.L2236
	movq	%rdi, %r14
	movq	(%rdi), %rdi
.L2236:
	call	_ZN2v88internal8compiler17ProjectionIndexOfEPKNS1_8OperatorE@PLT
	salq	$4, %rax
	addq	24(%rbx), %rax
	movq	%r14, (%rax)
.L2235:
	testq	%r13, %r13
	je	.L2232
	movq	%r13, %r14
	movq	0(%r13), %r13
	jmp	.L2237
	.p2align 4,,10
	.p2align 3
.L2409:
	movq	-112(%rbp), %rax
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	-136(%rbp), %rcx
	movq	-112(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	%eax, %r9d
	movl	$17, %esi
	movl	$1, %edx
	leaq	1(,%r9,8), %r12
	xorl	%r9d, %r9d
	movq	%r12, -96(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.1
	movq	-152(%rbp), %r9
	movq	96(%rbx), %rsi
	salq	$36, %r9
	orq	%r12, %r9
	movq	%r9, -96(%rbp)
	cmpq	104(%rbx), %rsi
	je	.L2326
	movq	%r9, (%rsi)
	addq	$8, 96(%rbx)
	jmp	.L2220
	.p2align 4,,10
	.p2align 3
.L2289:
	movq	32(%r12), %rdx
	cmpb	$0, -176(%rbp)
	leaq	16(%rdx,%rax), %rax
	movq	(%rax), %r13
	je	.L2291
.L2401:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movq	%rax, %r13
	movq	8(%rbx), %rax
	movq	120(%rax), %rsi
	movq	%rsi, 8(%rbx)
	testq	%rsi, %rsi
	je	.L2291
	movq	%r13, %rdi
	jmp	.L2292
	.p2align 4,,10
	.p2align 3
.L2416:
	call	_ZN2v88internal8compiler14NodeProperties18GetFrameStateInputEPNS1_4NodeE@PLT
	movq	%rax, %rdi
	movq	8(%rbx), %rax
	movq	120(%rax), %rsi
	movq	%rsi, 8(%rbx)
	testq	%rsi, %rsi
	je	.L2384
.L2292:
	cmpl	$1, (%rsi)
	je	.L2416
.L2384:
	movq	%rdi, %r13
	jmp	.L2291
	.p2align 4,,10
	.p2align 3
.L2400:
	movslq	-88(%rbp), %rax
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L2285
	.p2align 4,,10
	.p2align 3
.L2407:
	leaq	112(%r10), %rdi
	subq	%r11, %rsi
	movq	%r9, -176(%rbp)
	movl	%r8d, -168(%rbp)
	movl	%edx, -160(%rbp)
	movq	%r10, -144(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm
	movq	-144(%rbp), %r10
	movl	-160(%rbp), %edx
	movl	-168(%rbp), %r8d
	movq	-176(%rbp), %r9
	movq	120(%r10), %rdi
	jmp	.L2317
	.p2align 4,,10
	.p2align 3
.L2232:
	movq	-128(%rbp), %rax
	movq	368(%rax), %rax
	movl	12(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L2387
	movl	%r15d, %edi
	movl	%r15d, 12(%rax)
	subl	%edx, %edi
	addl	%edi, 4(%rax)
.L2387:
	leaq	-96(%rbp), %rax
	movq	32(%rbx), %rsi
	movq	%rax, -136(%rbp)
.L2225:
	movq	$0, -168(%rbp)
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L2239
	cmpq	$-1, 8(%rax)
	setne	%al
	movzbl	%al, %eax
	movq	%rax, -168(%rbp)
.L2239:
	movq	24(%rbx), %rdx
	xorl	%r14d, %r14d
	cmpq	%rsi, %rdx
	je	.L2389
	movq	%rbx, %rax
	movq	%r12, -184(%rbp)
	movq	%r14, %rbx
	movq	-128(%rbp), %r15
	movq	%rax, %r14
	jmp	.L2240
	.p2align 4,,10
	.p2align 3
.L2241:
	movl	8(%rax), %r11d
	movq	-112(%rbp), %rdi
	movq	%r10, %rsi
	movq	%r10, -120(%rbp)
	movzbl	12(%rax), %r12d
	movl	%r11d, -144(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE
	movl	-144(%rbp), %r11d
	movq	-120(%rbp), %r10
	movl	%r11d, %ecx
	sarl	%ecx
	andl	$1, %r11d
	je	.L2328
	movq	%rcx, %rdx
	movl	%eax, %eax
	salq	$36, %rdx
	leaq	0(,%rax,8), %rcx
	orq	%rcx, %rdx
	orq	$1, %rdx
.L2249:
	movl	20(%r10), %ecx
	movq	-112(%rbp), %rsi
	movl	$1, %edi
	movq	%rcx, %rax
	salq	%cl, %rdi
	shrq	$3, %rax
	andl	$2097144, %eax
	addq	160(%rsi), %rax
	orq	%rdi, (%rax)
.L2388:
	movq	16(%r15), %rdi
	movq	%rdx, -96(%rbp)
	movl	%r12d, %esi
	shrq	$3, %rdx
	call	_ZN2v88internal8compiler19InstructionSequence20MarkAsRepresentationENS0_21MachineRepresentationEi@PLT
	movq	-96(%rbp), %rax
	btq	$35, %rax
	jc	.L2417
.L2251:
	movq	24(%r14), %rdx
	movq	32(%r14), %rsi
.L2243:
	movq	%rsi, %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rbx
	jnb	.L2418
.L2240:
	movq	%rbx, %r13
	salq	$4, %r13
	leaq	(%rdx,%r13), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L2241
	cmpq	%rbx, -168(%rbp)
	jbe	.L2243
	movl	8(%rax), %edx
	movzbl	12(%rax), %r12d
	movq	-112(%rbp), %rax
	movl	%edx, -120(%rbp)
	movq	16(%rax), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movl	-120(%rbp), %edx
	movl	%edx, %ecx
	sarl	%ecx
	andl	$1, %edx
	je	.L2330
	movl	%eax, %eax
	salq	$36, %rcx
	leaq	0(,%rax,8), %rdx
	orq	%rcx, %rdx
	orq	$1, %rdx
	jmp	.L2388
	.p2align 4,,10
	.p2align 3
.L2328:
	movl	%eax, %eax
	leaq	0(,%rax,8), %rdx
	cmpl	$-1, %ecx
	je	.L2419
	movabsq	$790273982465, %rax
	salq	$41, %rcx
	orq	%rdx, %rcx
	movabsq	$858993459201, %rdx
	orq	%rcx, %rdx
	orq	%rax, %rcx
	cmpb	$12, %r12b
	cmovb	%rcx, %rdx
	jmp	.L2249
	.p2align 4,,10
	.p2align 3
.L2417:
	movq	64(%r14), %rsi
	cmpq	72(%r14), %rsi
	je	.L2252
	movq	%rax, (%rsi)
	addq	$8, 64(%r14)
.L2253:
	movq	24(%r14), %rax
	movq	$0, (%rax,%r13)
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2330:
	movl	%eax, %eax
	leaq	0(,%rax,8), %rdx
	cmpl	$-1, %ecx
	je	.L2420
	movabsq	$790273982465, %rdi
	salq	$41, %rcx
	movq	%rcx, %rax
	movabsq	$858993459201, %rcx
	orq	%rdx, %rax
	orq	%rax, %rdi
	orq	%rax, %rcx
	cmpb	$12, %r12b
	movq	%rdi, %rdx
	cmovnb	%rcx, %rdx
	jmp	.L2388
	.p2align 4,,10
	.p2align 3
.L2418:
	movq	-184(%rbp), %r12
	movq	%r14, %rbx
.L2389:
	movq	(%rbx), %rcx
	jmp	.L2221
	.p2align 4,,10
	.p2align 3
.L2419:
	movabsq	$927712935937, %rax
	orq	%rax, %rdx
	jmp	.L2249
.L2261:
	testb	$1, -160(%rbp)
	je	.L2262
	movq	0(%r13), %rax
	cmpw	$30, 16(%rax)
	je	.L2391
.L2262:
	testl	%esi, %esi
	jne	.L2394
	cmpb	$0, -176(%rbp)
	jne	.L2421
	testb	$16, -160(%rbp)
	jne	.L2422
	.p2align 4,,10
	.p2align 3
.L2276:
	leaq	-112(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler16OperandGenerator11UseRegisterEPNS1_4NodeE
	movq	%rax, -96(%rbp)
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2258:
	testl	%edi, %edi
	je	.L2256
	movq	0(%r13), %rax
	movzwl	16(%rax), %eax
	subl	$32, %eax
	cmpl	$1, %eax
	ja	.L2256
.L2391:
	leaq	-112(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler16OperandGenerator12UseImmediateEPNS1_4NodeE
	movq	%rax, -96(%rbp)
	jmp	.L2390
.L2412:
	movq	16(%rdx), %rax
	movl	(%rax), %ecx
	movzbl	4(%rax), %edx
	movzbl	5(%rax), %eax
	movq	%r12, -96(%rbp)
	movl	%ecx, -88(%rbp)
	movb	%dl, -84(%rbp)
	movb	%al, -83(%rbp)
	cmpq	%rsi, 40(%rbx)
	je	.L2223
	movq	%r12, (%rsi)
	movl	%ecx, 8(%rsi)
	movb	%dl, 12(%rsi)
	movb	%al, 13(%rsi)
	movq	32(%rbx), %rax
	leaq	16(%rax), %rsi
	leaq	-96(%rbp), %rax
	movq	%rsi, 32(%rbx)
	movq	%rax, -136(%rbp)
	jmp	.L2225
.L2260:
	movl	16(%rcx), %esi
	movzbl	20(%rcx), %r8d
	movq	-112(%rbp), %rdx
	movl	20(%r13), %ecx
	movq	288(%rdx), %rax
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	movl	(%rax,%r14,4), %eax
	cmpl	$-1, %eax
	je	.L2423
.L2279:
	movl	%esi, %edi
	movl	%eax, %eax
	sarl	%edi
	salq	$3, %rax
	andl	$1, %esi
	je	.L2424
	movq	%rdi, %rsi
	salq	$36, %rsi
	orq	%rsi, %rax
	orq	$1, %rax
	movq	%rax, %rsi
.L2282:
	movq	208(%rdx), %rdi
	movq	%r14, %rax
	movl	$1, %edx
	shrq	$6, %rax
	salq	%cl, %rdx
	orq	%rdx, (%rdi,%rax,8)
	movq	%rsi, -96(%rbp)
	jmp	.L2390
.L2414:
	movq	%r13, %rdi
	salq	$4, %rdi
	addq	%rax, %rdi
	cmpq	%rsi, %rdi
	je	.L2227
	movq	%rdi, 32(%rbx)
	movq	24(%rcx), %rdx
	jmp	.L2227
	.p2align 4,,10
	.p2align 3
.L2252:
	movq	-136(%rbp), %rdx
	leaq	48(%r14), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L2253
.L2420:
	movabsq	$927712935937, %rax
	orq	%rax, %rdx
	jmp	.L2388
.L2402:
	movslq	-88(%rbp), %rax
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L2294
.L2286:
	movq	-136(%rbp), %rdx
	leaq	144(%r14), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L2287
.L2413:
	movq	%r13, %rsi
	subq	%r8, %rsi
	call	_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_default_appendEm
	movq	(%rbx), %rax
	movq	24(%rax), %rdx
	movq	24(%rbx), %rax
	jmp	.L2227
.L2295:
	leaq	144(%rdx), %rdi
	movq	-136(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L2296
.L2424:
	movabsq	$927712935937, %rsi
	orq	%rax, %rsi
	cmpl	$-1, %edi
	je	.L2282
	salq	$41, %rdi
	orq	%rax, %rdi
	movabsq	$790273982465, %rax
	orq	%rdi, %rax
	movq	%rax, %rsi
	cmpb	$11, %r8b
	jbe	.L2282
	movabsq	$858993459201, %rax
	orq	%rax, %rdi
	movq	%rdi, %rsi
	jmp	.L2282
.L2326:
	movq	-136(%rbp), %rdx
	movq	-208(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L2220
.L2399:
	movq	16(%rdx), %rdi
	movq	%rdx, -144(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	-144(%rbp), %rdx
	movq	288(%rdx), %rdx
	movl	%eax, (%rdx,%r14,4)
	movl	20(%r13), %ecx
	movq	-112(%rbp), %rdx
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	jmp	.L2277
.L2422:
	movq	-112(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE
	movl	20(%r13), %ecx
	movq	-112(%rbp), %rsi
	movl	%eax, %eax
	movq	%rcx, %rdx
	salq	$3, %rax
	shrq	$3, %rdx
	andl	$2097144, %edx
	addq	208(%rsi), %rdx
	movl	$1, %esi
	salq	%cl, %rsi
	orq	%rsi, (%rdx)
	movabsq	$996432412673, %rdx
	orq	%rdx, %rax
	movq	%rax, -96(%rbp)
	jmp	.L2390
.L2423:
	movq	16(%rdx), %rdi
	movl	%esi, -184(%rbp)
	movb	%r8b, -168(%rbp)
	movq	%rdx, -144(%rbp)
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	-144(%rbp), %rdx
	movl	-184(%rbp), %esi
	movzbl	-168(%rbp), %r8d
	movq	288(%rdx), %rdx
	movl	%eax, (%rdx,%r14,4)
	movl	20(%r13), %ecx
	movq	-112(%rbp), %rdx
	movl	%ecx, %r14d
	andl	$16777215, %r14d
	jmp	.L2279
.L2421:
	movq	-112(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18GetVirtualRegisterEPKNS1_4NodeE
	movl	20(%r13), %ecx
	movq	-112(%rbp), %rsi
	movl	%eax, %eax
	movq	%rcx, %rdx
	salq	$3, %rax
	shrq	$3, %rdx
	andl	$2097144, %edx
	addq	208(%rsi), %rdx
	movl	$1, %esi
	salq	%cl, %rsi
	orq	%rsi, (%rdx)
	movabsq	$927712935937, %rdx
	orq	%rdx, %rax
	movq	%rax, -96(%rbp)
	jmp	.L2390
.L2411:
	movslq	-88(%rbp), %rax
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L2322
.L2223:
	leaq	-96(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -136(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	32(%rbx), %rsi
	jmp	.L2225
.L2323:
	movq	-136(%rbp), %rdx
	leaq	144(%r13), %rdi
	movq	%r10, -168(%rbp)
	movq	%r9, -160(%rbp)
	movl	%r8d, -144(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-168(%rbp), %r10
	movq	-160(%rbp), %r9
	movl	-144(%rbp), %r8d
	jmp	.L2324
.L2229:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2410:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25098:
	.size	_ZN2v88internal8compiler19InstructionSelector20InitializeCallBufferEPNS1_4NodeEPNS1_10CallBufferENS_4base5FlagsINS2_14CallBufferFlagEiEEbi, .-_ZN2v88internal8compiler19InstructionSelector20InitializeCallBufferEPNS1_4NodeEPNS1_10CallBufferENS_4base5FlagsINS2_14CallBufferFlagEiEEbi
	.section	.text.unlikely._ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE,"ax",@progbits
	.align 2
.LCOLDB14:
	.section	.text._ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE,"ax",@progbits
.LHOTB14:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE
	.type	_ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE, @function
_ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE:
.LFB25185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -240(%rbp)
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16CallDescriptorOfEPKNS1_8OperatorE@PLT
	movl	64(%rax), %edx
	movq	%rax, %rbx
	leaq	-208(%rbp), %rax
	movq	%rax, -232(%rbp)
	testb	$2, %dh
	jne	.L2467
.L2426:
	xorl	%eax, %eax
	andl	$1, %edx
	je	.L2427
	movq	24(%rbx), %rax
	movzbl	23(%r13), %edx
	leaq	32(%r13), %rcx
	movq	8(%rax), %rax
	andl	$15, %edx
	addq	$1, %rax
	cltq
	salq	$3, %rax
	cmpl	$15, %edx
	je	.L2428
	addq	%rcx, %rax
.L2429:
	movq	16(%r12), %rdx
	movq	(%rax), %rsi
	movq	8(%rdx), %rdi
	call	_ZN2v88internal8compiler12_GLOBAL__N_131GetFrameStateDescriptorInternalEPNS0_4ZoneEPNS1_4NodeE
	movq	432(%r12), %rcx
	movq	(%rcx), %rdx
	cmpq	%rdx, 40(%rax)
	cmovnb	40(%rax), %rdx
	movq	%rdx, (%rcx)
.L2427:
	movq	%rax, %xmm1
	movq	%rbx, %xmm0
	movq	(%r12), %rdx
	movq	$0, -184(%rbp)
	punpcklqdq	%xmm1, %xmm0
	leaq	-96(%rbp), %r14
	movq	$0, -152(%rbp)
	leaq	-192(%rbp), %rcx
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rcx, %rdi
	movq	%rdx, -192(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rdx, -96(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	24(%rbx), %rax
	movq	%rcx, -256(%rbp)
	movq	(%rax), %rsi
	call	_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE7reserveEm
	movq	24(%rbx), %rax
	leaq	-160(%rbp), %rdi
	movq	(%rax), %rsi
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE7reserveEm
	movq	-208(%rbp), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rax
	movq	8(%rax), %rsi
	addq	$1, %rsi
	call	_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE7reserveEm
	movq	-208(%rbp), %rax
	movq	-200(%rbp), %rdi
	movq	24(%rax), %rax
	movq	8(%rax), %r15
	leaq	1(%r15), %rsi
	testq	%rdi, %rdi
	je	.L2430
	call	_ZNK2v88internal8compiler20FrameStateDescriptor12GetTotalSizeEv@PLT
	leaq	2(%r15,%rax), %rsi
.L2430:
	leaq	-128(%rbp), %r15
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE7reserveEm
	movl	64(%rbx), %eax
	testb	$1, %ah
	jne	.L2468
	movq	-232(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movl	$3, %ecx
	movq	%r12, %rdi
	movl	%eax, -244(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector20InitializeCallBufferEPNS1_4NodeEPNS1_10CallBufferENS_4base5FlagsINS2_14CallBufferFlagEiEEbi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20EmitPrepareArgumentsEPNS0_10ZoneVectorINS1_13PushParameterEEEPKNS1_14CallDescriptorEPNS1_4NodeE@PLT
	movq	-240(%rbp), %rcx
	movl	-244(%rbp), %eax
	testq	%rcx, %rcx
	je	.L2432
	movq	16(%r12), %rdi
	movslq	4(%rcx), %rdx
	orl	$2, %eax
	movl	$7, -224(%rbp)
	movb	$19, -220(%rbp)
	movq	%rdx, -216(%rbp)
	movq	160(%rdi), %rsi
	movq	%rsi, %r14
	subq	152(%rdi), %r14
	sarq	$4, %r14
	cmpq	168(%rdi), %rsi
	je	.L2433
	movl	$7, (%rsi)
	leaq	-224(%rbp), %r8
	movb	$19, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rdi)
.L2434:
	salq	$32, %r14
	movq	%r8, %rsi
	movq	%r15, %rdi
	movl	%eax, -232(%rbp)
	orq	$11, %r14
	movq	%r14, -224(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movl	-232(%rbp), %eax
.L2432:
	cmpl	$6, 8(%rbx)
	ja	.L2450
	movl	8(%rbx), %edx
	leaq	.L2437(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE,"a",@progbits
	.align 4
	.align 4
.L2437:
	.long	.L2441-.L2437
	.long	.L2440-.L2437
	.long	.L2439-.L2437
	.long	.L2438-.L2437
	.long	.L2438-.L2437
	.long	.L2438-.L2437
	.long	.L2436-.L2437
	.section	.text._ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE
	.p2align 4,,10
	.p2align 3
.L2467:
	movl	(%rbx), %esi
	movq	%rax, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	$0, -208(%rbp)
	movq	%r12, %rdi
	sall	$22, %esi
	orl	$6, %esi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0
	movl	64(%rbx), %edx
	jmp	.L2426
	.p2align 4,,10
	.p2align 3
.L2438:
	sall	$22, %eax
	orl	$10, %eax
	movl	%eax, %esi
.L2435:
	movq	-152(%rbp), %rcx
	movq	-144(%rbp), %rdx
	movl	$0, %eax
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %r8
	subq	%rcx, %rdx
	sarq	$3, %rdx
	cmove	%rax, %rcx
	subq	%r9, %r8
	sarq	$3, %r8
	cmpq	$254, %rdx
	ja	.L2452
	cmpq	$65534, %r8
	ja	.L2452
	pushq	$0
	movq	%r12, %rdi
	pushq	$0
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0
	popq	%rdx
	popq	%rcx
	cmpb	$0, 376(%r12)
	jne	.L2425
	orl	$1073741824, 4(%rax)
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	-256(%rbp), %rsi
	call	_ZN2v88internal8compiler19InstructionSelector18EmitPrepareResultsEPNS0_10ZoneVectorINS1_13PushParameterEEEPKNS1_14CallDescriptorEPNS1_4NodeE@PLT
	testb	$2, 65(%rbx)
	jne	.L2469
.L2425:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2470
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2428:
	.cfi_restore_state
	movq	32(%r13), %rdx
	leaq	16(%rdx,%rax), %rax
	jmp	.L2429
	.p2align 4,,10
	.p2align 3
.L2439:
	movq	24(%rbx), %rax
	movq	8(%rax), %rsi
	movl	%esi, %eax
	sall	$22, %eax
	orl	$8, %eax
	movl	%eax, %esi
	jmp	.L2435
	.p2align 4,,10
	.p2align 3
.L2441:
	sall	$22, %eax
	movl	%eax, %esi
	jmp	.L2435
	.p2align 4,,10
	.p2align 3
.L2440:
	sall	$22, %eax
	orl	$3, %eax
	movl	%eax, %esi
	jmp	.L2435
	.p2align 4,,10
	.p2align 3
.L2436:
	sall	$22, %eax
	orl	$12, %eax
	movl	%eax, %esi
	jmp	.L2435
	.p2align 4,,10
	.p2align 3
.L2433:
	leaq	-224(%rbp), %r8
	addq	$144, %rdi
	movl	%eax, -240(%rbp)
	movq	%r8, %rdx
	movq	%r8, -232(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movl	-240(%rbp), %eax
	movq	-232(%rbp), %r8
	jmp	.L2434
	.p2align 4,,10
	.p2align 3
.L2452:
	movb	$1, 376(%r12)
	jmp	.L2425
	.p2align 4,,10
	.p2align 3
.L2469:
	movl	(%rbx), %esi
	leaq	-224(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	$0, -224(%rbp)
	movq	%r12, %rdi
	sall	$22, %esi
	orl	$7, %esi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0
	jmp	.L2425
.L2470:
	call	__stack_chk_fail@PLT
.L2468:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE
	.cfi_startproc
	.type	_ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE.cold, @function
_ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE.cold:
.LFSB25185:
.L2450:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$17, %esi
	jmp	.L2435
	.cfi_endproc
.LFE25185:
	.section	.text._ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE
	.size	_ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE, .-_ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE
	.section	.text.unlikely._ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE
	.size	_ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE.cold, .-_ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE.cold
.LCOLDE14:
	.section	.text._ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE
.LHOTE14:
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector9VisitNodeEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"Unexpected operator #%d:%s @ node #%d"
	.section	.text._ZN2v88internal8compiler19InstructionSelector9VisitNodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector9VisitNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector9VisitNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector9VisitNodeEPNS1_4NodeE:
.LFB25125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -40
	movq	424(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11TickCounter6DoTickEv@PLT
	movq	(%r12), %rdi
	movzwl	16(%rdi), %ecx
	cmpw	$681, %cx
	ja	.L2472
	leaq	.L2474(%rip), %rsi
	movzwl	%cx, %edx
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector9VisitNodeEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L2474:
	.long	.L2471-.L2474
	.long	.L2471-.L2474
	.long	.L2471-.L2474
	.long	.L2471-.L2474
	.long	.L2471-.L2474
	.long	.L2471-.L2474
	.long	.L2471-.L2474
	.long	.L2846-.L2474
	.long	.L2471-.L2474
	.long	.L2471-.L2474
	.long	.L2471-.L2474
	.long	.L2472-.L2474
	.long	.L2845-.L2474
	.long	.L2844-.L2474
	.long	.L2843-.L2474
	.long	.L2842-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2471-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2471-.L2474
	.long	.L2837-.L2474
	.long	.L2837-.L2474
	.long	.L2841-.L2474
	.long	.L2840-.L2474
	.long	.L2837-.L2474
	.long	.L2839-.L2474
	.long	.L2472-.L2474
	.long	.L2825-.L2474
	.long	.L2838-.L2474
	.long	.L2837-.L2474
	.long	.L2837-.L2474
	.long	.L2472-.L2474
	.long	.L2836-.L2474
	.long	.L2471-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2471-.L2474
	.long	.L2835-.L2474
	.long	.L2471-.L2474
	.long	.L2471-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2471-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2833-.L2474
	.long	.L2832-.L2474
	.long	.L2831-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2830-.L2474
	.long	.L2829-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2828-.L2474
	.long	.L2827-.L2474
	.long	.L2472-.L2474
	.long	.L2826-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2825-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2824-.L2474
	.long	.L2823-.L2474
	.long	.L2822-.L2474
	.long	.L2821-.L2474
	.long	.L2820-.L2474
	.long	.L2819-.L2474
	.long	.L2818-.L2474
	.long	.L2817-.L2474
	.long	.L2816-.L2474
	.long	.L2815-.L2474
	.long	.L2814-.L2474
	.long	.L2813-.L2474
	.long	.L2812-.L2474
	.long	.L2811-.L2474
	.long	.L2810-.L2474
	.long	.L2809-.L2474
	.long	.L2808-.L2474
	.long	.L2807-.L2474
	.long	.L2806-.L2474
	.long	.L2805-.L2474
	.long	.L2804-.L2474
	.long	.L2803-.L2474
	.long	.L2802-.L2474
	.long	.L2801-.L2474
	.long	.L2800-.L2474
	.long	.L2799-.L2474
	.long	.L2798-.L2474
	.long	.L2797-.L2474
	.long	.L2796-.L2474
	.long	.L2795-.L2474
	.long	.L2794-.L2474
	.long	.L2629-.L2474
	.long	.L2793-.L2474
	.long	.L2792-.L2474
	.long	.L2791-.L2474
	.long	.L2790-.L2474
	.long	.L2789-.L2474
	.long	.L2788-.L2474
	.long	.L2787-.L2474
	.long	.L2786-.L2474
	.long	.L2785-.L2474
	.long	.L2784-.L2474
	.long	.L2783-.L2474
	.long	.L2782-.L2474
	.long	.L2781-.L2474
	.long	.L2780-.L2474
	.long	.L2779-.L2474
	.long	.L2778-.L2474
	.long	.L2777-.L2474
	.long	.L2776-.L2474
	.long	.L2775-.L2474
	.long	.L2774-.L2474
	.long	.L2773-.L2474
	.long	.L2772-.L2474
	.long	.L2771-.L2474
	.long	.L2770-.L2474
	.long	.L2769-.L2474
	.long	.L2768-.L2474
	.long	.L2767-.L2474
	.long	.L2766-.L2474
	.long	.L2765-.L2474
	.long	.L2764-.L2474
	.long	.L2763-.L2474
	.long	.L2762-.L2474
	.long	.L2761-.L2474
	.long	.L2760-.L2474
	.long	.L2759-.L2474
	.long	.L2758-.L2474
	.long	.L2757-.L2474
	.long	.L2756-.L2474
	.long	.L2755-.L2474
	.long	.L2754-.L2474
	.long	.L2753-.L2474
	.long	.L2752-.L2474
	.long	.L2751-.L2474
	.long	.L2750-.L2474
	.long	.L2749-.L2474
	.long	.L2748-.L2474
	.long	.L2747-.L2474
	.long	.L2746-.L2474
	.long	.L2745-.L2474
	.long	.L2744-.L2474
	.long	.L2743-.L2474
	.long	.L2742-.L2474
	.long	.L2741-.L2474
	.long	.L2740-.L2474
	.long	.L2739-.L2474
	.long	.L2738-.L2474
	.long	.L2737-.L2474
	.long	.L2736-.L2474
	.long	.L2735-.L2474
	.long	.L2734-.L2474
	.long	.L2733-.L2474
	.long	.L2732-.L2474
	.long	.L2731-.L2474
	.long	.L2730-.L2474
	.long	.L2729-.L2474
	.long	.L2728-.L2474
	.long	.L2727-.L2474
	.long	.L2726-.L2474
	.long	.L2725-.L2474
	.long	.L2724-.L2474
	.long	.L2723-.L2474
	.long	.L2722-.L2474
	.long	.L2721-.L2474
	.long	.L2720-.L2474
	.long	.L2719-.L2474
	.long	.L2718-.L2474
	.long	.L2717-.L2474
	.long	.L2716-.L2474
	.long	.L2715-.L2474
	.long	.L2714-.L2474
	.long	.L2713-.L2474
	.long	.L2712-.L2474
	.long	.L2638-.L2474
	.long	.L2711-.L2474
	.long	.L2638-.L2474
	.long	.L2638-.L2474
	.long	.L2638-.L2474
	.long	.L2638-.L2474
	.long	.L2638-.L2474
	.long	.L2638-.L2474
	.long	.L2638-.L2474
	.long	.L2710-.L2474
	.long	.L2709-.L2474
	.long	.L2708-.L2474
	.long	.L2707-.L2474
	.long	.L2706-.L2474
	.long	.L2705-.L2474
	.long	.L2704-.L2474
	.long	.L2703-.L2474
	.long	.L2702-.L2474
	.long	.L2701-.L2474
	.long	.L2700-.L2474
	.long	.L2699-.L2474
	.long	.L2698-.L2474
	.long	.L2697-.L2474
	.long	.L2696-.L2474
	.long	.L2695-.L2474
	.long	.L2694-.L2474
	.long	.L2693-.L2474
	.long	.L2692-.L2474
	.long	.L2691-.L2474
	.long	.L2690-.L2474
	.long	.L2689-.L2474
	.long	.L2688-.L2474
	.long	.L2687-.L2474
	.long	.L2687-.L2474
	.long	.L2686-.L2474
	.long	.L2685-.L2474
	.long	.L2684-.L2474
	.long	.L2683-.L2474
	.long	.L2682-.L2474
	.long	.L2681-.L2474
	.long	.L2680-.L2474
	.long	.L2679-.L2474
	.long	.L2678-.L2474
	.long	.L2677-.L2474
	.long	.L2676-.L2474
	.long	.L2675-.L2474
	.long	.L2674-.L2474
	.long	.L2673-.L2474
	.long	.L2672-.L2474
	.long	.L2671-.L2474
	.long	.L2670-.L2474
	.long	.L2669-.L2474
	.long	.L2668-.L2474
	.long	.L2667-.L2474
	.long	.L2666-.L2474
	.long	.L2665-.L2474
	.long	.L2664-.L2474
	.long	.L2663-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2662-.L2474
	.long	.L2661-.L2474
	.long	.L2660-.L2474
	.long	.L2659-.L2474
	.long	.L2658-.L2474
	.long	.L2657-.L2474
	.long	.L2656-.L2474
	.long	.L2655-.L2474
	.long	.L2654-.L2474
	.long	.L2653-.L2474
	.long	.L2652-.L2474
	.long	.L2651-.L2474
	.long	.L2650-.L2474
	.long	.L2649-.L2474
	.long	.L2648-.L2474
	.long	.L2647-.L2474
	.long	.L2646-.L2474
	.long	.L2645-.L2474
	.long	.L2644-.L2474
	.long	.L2643-.L2474
	.long	.L2642-.L2474
	.long	.L2641-.L2474
	.long	.L2640-.L2474
	.long	.L2639-.L2474
	.long	.L2638-.L2474
	.long	.L2638-.L2474
	.long	.L2638-.L2474
	.long	.L2638-.L2474
	.long	.L2638-.L2474
	.long	.L2638-.L2474
	.long	.L2637-.L2474
	.long	.L2636-.L2474
	.long	.L2635-.L2474
	.long	.L2634-.L2474
	.long	.L2633-.L2474
	.long	.L2632-.L2474
	.long	.L2631-.L2474
	.long	.L2630-.L2474
	.long	.L2629-.L2474
	.long	.L2628-.L2474
	.long	.L2627-.L2474
	.long	.L2626-.L2474
	.long	.L2625-.L2474
	.long	.L2624-.L2474
	.long	.L2623-.L2474
	.long	.L2622-.L2474
	.long	.L2621-.L2474
	.long	.L2620-.L2474
	.long	.L2619-.L2474
	.long	.L2618-.L2474
	.long	.L2617-.L2474
	.long	.L2616-.L2474
	.long	.L2615-.L2474
	.long	.L2614-.L2474
	.long	.L2613-.L2474
	.long	.L2612-.L2474
	.long	.L2611-.L2474
	.long	.L2610-.L2474
	.long	.L2609-.L2474
	.long	.L2608-.L2474
	.long	.L2607-.L2474
	.long	.L2606-.L2474
	.long	.L2605-.L2474
	.long	.L2604-.L2474
	.long	.L2603-.L2474
	.long	.L2602-.L2474
	.long	.L2601-.L2474
	.long	.L2600-.L2474
	.long	.L2599-.L2474
	.long	.L2598-.L2474
	.long	.L2597-.L2474
	.long	.L2596-.L2474
	.long	.L2595-.L2474
	.long	.L2594-.L2474
	.long	.L2593-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2592-.L2474
	.long	.L2591-.L2474
	.long	.L2590-.L2474
	.long	.L2589-.L2474
	.long	.L2588-.L2474
	.long	.L2587-.L2474
	.long	.L2586-.L2474
	.long	.L2585-.L2474
	.long	.L2584-.L2474
	.long	.L2583-.L2474
	.long	.L2582-.L2474
	.long	.L2581-.L2474
	.long	.L2580-.L2474
	.long	.L2579-.L2474
	.long	.L2578-.L2474
	.long	.L2577-.L2474
	.long	.L2576-.L2474
	.long	.L2575-.L2474
	.long	.L2574-.L2474
	.long	.L2573-.L2474
	.long	.L2572-.L2474
	.long	.L2571-.L2474
	.long	.L2570-.L2474
	.long	.L2569-.L2474
	.long	.L2568-.L2474
	.long	.L2567-.L2474
	.long	.L2566-.L2474
	.long	.L2565-.L2474
	.long	.L2564-.L2474
	.long	.L2563-.L2474
	.long	.L2562-.L2474
	.long	.L2561-.L2474
	.long	.L2560-.L2474
	.long	.L2559-.L2474
	.long	.L2558-.L2474
	.long	.L2557-.L2474
	.long	.L2556-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2555-.L2474
	.long	.L2554-.L2474
	.long	.L2553-.L2474
	.long	.L2552-.L2474
	.long	.L2551-.L2474
	.long	.L2550-.L2474
	.long	.L2549-.L2474
	.long	.L2548-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2547-.L2474
	.long	.L2546-.L2474
	.long	.L2545-.L2474
	.long	.L2544-.L2474
	.long	.L2543-.L2474
	.long	.L2542-.L2474
	.long	.L2541-.L2474
	.long	.L2540-.L2474
	.long	.L2539-.L2474
	.long	.L2538-.L2474
	.long	.L2537-.L2474
	.long	.L2536-.L2474
	.long	.L2535-.L2474
	.long	.L2534-.L2474
	.long	.L2533-.L2474
	.long	.L2532-.L2474
	.long	.L2531-.L2474
	.long	.L2530-.L2474
	.long	.L2529-.L2474
	.long	.L2528-.L2474
	.long	.L2527-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2526-.L2474
	.long	.L2525-.L2474
	.long	.L2524-.L2474
	.long	.L2523-.L2474
	.long	.L2522-.L2474
	.long	.L2521-.L2474
	.long	.L2520-.L2474
	.long	.L2519-.L2474
	.long	.L2518-.L2474
	.long	.L2517-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2516-.L2474
	.long	.L2515-.L2474
	.long	.L2514-.L2474
	.long	.L2513-.L2474
	.long	.L2512-.L2474
	.long	.L2511-.L2474
	.long	.L2510-.L2474
	.long	.L2509-.L2474
	.long	.L2508-.L2474
	.long	.L2507-.L2474
	.long	.L2506-.L2474
	.long	.L2505-.L2474
	.long	.L2504-.L2474
	.long	.L2503-.L2474
	.long	.L2502-.L2474
	.long	.L2501-.L2474
	.long	.L2500-.L2474
	.long	.L2499-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2498-.L2474
	.long	.L2497-.L2474
	.long	.L2496-.L2474
	.long	.L2495-.L2474
	.long	.L2494-.L2474
	.long	.L2493-.L2474
	.long	.L2492-.L2474
	.long	.L2491-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2490-.L2474
	.long	.L2489-.L2474
	.long	.L2472-.L2474
	.long	.L2472-.L2474
	.long	.L2488-.L2474
	.long	.L2487-.L2474
	.long	.L2486-.L2474
	.long	.L2485-.L2474
	.long	.L2484-.L2474
	.long	.L2483-.L2474
	.long	.L2482-.L2474
	.long	.L2481-.L2474
	.long	.L2480-.L2474
	.long	.L2479-.L2474
	.long	.L2478-.L2474
	.long	.L2477-.L2474
	.long	.L2476-.L2474
	.long	.L2475-.L2474
	.long	.L2473-.L2474
	.section	.text._ZN2v88internal8compiler19InstructionSelector9VisitNodeEPNS1_4NodeE
.L2839:
	movsd	48(%rdi), %xmm0
	comisd	.LC15(%rip), %xmm0
	jb	.L2848
	movsd	.LC16(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L2848
	movl	$1, %eax
	movq	%xmm0, %rbx
	salq	$63, %rax
	cmpq	%rax, %rbx
	je	.L2848
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L2848
	jne	.L2848
.L2851:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitConstantEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L2471:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2865
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2472:
	.cfi_restore_state
	movl	20(%r12), %r8d
	movq	8(%rdi), %rdx
	movzwl	%cx, %esi
	xorl	%eax, %eax
	leaq	.LC17(%rip), %rdi
	andl	$16777215, %r8d
	movl	%r8d, %ecx
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2638:
	movq	%r12, %rdx
	movl	$4, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector27MarkPairProjectionsAsWord32EPNS1_4NodeE
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2837:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitConstantEPNS1_4NodeE
	jmp	.L2471
.L2825:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$8, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitConstantEPNS1_4NodeE
	jmp	.L2471
.L2629:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitInt64AddEPNS1_4NodeE@PLT
	jmp	.L2471
.L2687:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE
	jmp	.L2471
.L2677:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector26VisitChangeFloat64ToUint64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2678:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector26VisitChangeFloat64ToUint32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2679:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitChangeFloat64ToInt64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2656:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint32ToFloat32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2657:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt64ToFloat64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2658:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt64ToFloat32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2659:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector24VisitRoundInt32ToFloat32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2660:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector24VisitRoundFloat64ToInt32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2661:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitTruncateInt64ToInt32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2662:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector29VisitTruncateFloat64ToFloat32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2663:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitChangeUint32ToUint64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2664:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector26VisitChangeUint32ToFloat64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2665:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitChangeInt64ToFloat64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2666:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitChangeInt32ToInt64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2667:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitChangeInt32ToFloat64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2668:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector31VisitTryTruncateFloat64ToUint64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2669:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector31VisitTryTruncateFloat32ToUint64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2670:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector30VisitTryTruncateFloat64ToInt64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2671:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector30VisitTryTruncateFloat32ToInt64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2712:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicXorEPNS1_4NodeE@PLT
	jmp	.L2471
.L2713:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector19VisitWord32AtomicOrEPNS1_4NodeE@PLT
	jmp	.L2471
.L2714:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicAndEPNS1_4NodeE@PLT
	jmp	.L2471
.L2715:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicSubEPNS1_4NodeE@PLT
	jmp	.L2471
.L2716:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20VisitWord32AtomicAddEPNS1_4NodeE@PLT
	jmp	.L2471
.L2717:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector32VisitWord32AtomicCompareExchangeEPNS1_4NodeE@PLT
	jmp	.L2471
.L2718:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitWord32AtomicExchangeEPNS1_4NodeE@PLT
	jmp	.L2471
.L2704:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicXorEPNS1_4NodeE@PLT
	jmp	.L2471
.L2705:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector19VisitWord64AtomicOrEPNS1_4NodeE@PLT
	jmp	.L2471
.L2706:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicAndEPNS1_4NodeE@PLT
	jmp	.L2471
.L2707:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicSubEPNS1_4NodeE@PLT
	jmp	.L2471
.L2708:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20VisitWord64AtomicAddEPNS1_4NodeE@PLT
	jmp	.L2471
.L2709:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector22VisitWord64AtomicStoreEPNS1_4NodeE@PLT
	jmp	.L2471
.L2710:
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitWord64AtomicLoadEPNS1_4NodeE@PLT
	jmp	.L2471
.L2688:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitInt64AbsWithOverflowEPNS1_4NodeE@PLT
	jmp	.L2471
.L2689:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitWord64ReverseBytesEPNS1_4NodeE@PLT
	jmp	.L2471
.L2690:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector22VisitWord64ReverseBitsEPNS1_4NodeE@PLT
	jmp	.L2471
.L2691:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitWord64CtzEPNS1_4NodeE@PLT
	jmp	.L2471
.L2692:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitWord64ClzEPNS1_4NodeE@PLT
	jmp	.L2471
.L2693:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector17VisitWord64PopcntEPNS1_4NodeE@PLT
	jmp	.L2471
.L2694:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector17VisitWord32PopcntEPNS1_4NodeE@PLT
	jmp	.L2471
.L2695:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitStackSlotEPNS1_4NodeE@PLT
	jmp	.L2471
.L2696:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector10VisitStoreEPNS1_4NodeE@PLT
	jmp	.L2471
.L2697:
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector17VisitPoisonedLoadEPNS1_4NodeE@PLT
	jmp	.L2471
.L2698:
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector9VisitLoadEPNS1_4NodeE@PLT
	jmp	.L2471
.L2699:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitCommentEPNS1_4NodeE
	jmp	.L2471
.L2700:
	leaq	-112(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$19, %esi
	movq	%r13, %rdi
	movq	$0, -112(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0
	jmp	.L2471
.L2701:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector19VisitAbortCSAAssertEPNS1_4NodeE@PLT
	jmp	.L2471
.L2702:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector32VisitWord64AtomicCompareExchangeEPNS1_4NodeE@PLT
	jmp	.L2471
.L2703:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitWord64AtomicExchangeEPNS1_4NodeE@PLT
	jmp	.L2471
.L2590:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitI64x2ReplaceLaneEPNS1_4NodeE@PLT
	jmp	.L2471
.L2591:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitI64x2ExtractLaneEPNS1_4NodeE@PLT
	jmp	.L2471
.L2592:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitI64x2SplatEPNS1_4NodeE@PLT
	jmp	.L2471
.L2593:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitF32x4LeEPNS1_4NodeE@PLT
	jmp	.L2471
.L2594:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitF32x4LtEPNS1_4NodeE@PLT
	jmp	.L2471
.L2595:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitF32x4NeEPNS1_4NodeE@PLT
	jmp	.L2471
.L2596:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitF32x4EqEPNS1_4NodeE@PLT
	jmp	.L2471
.L2597:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MaxEPNS1_4NodeE@PLT
	jmp	.L2471
.L2598:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MinEPNS1_4NodeE@PLT
	jmp	.L2471
.L2599:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4DivEPNS1_4NodeE@PLT
	jmp	.L2471
.L2600:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4MulEPNS1_4NodeE@PLT
	jmp	.L2471
.L2601:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4SubEPNS1_4NodeE@PLT
	jmp	.L2471
.L2602:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18VisitF32x4AddHorizEPNS1_4NodeE@PLT
	jmp	.L2471
.L2603:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4AddEPNS1_4NodeE@PLT
	jmp	.L2471
.L2604:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitF32x4RecipSqrtApproxEPNS1_4NodeE@PLT
	jmp	.L2471
.L2605:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitF32x4RecipApproxEPNS1_4NodeE@PLT
	jmp	.L2471
.L2606:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4NegEPNS1_4NodeE@PLT
	jmp	.L2471
.L2607:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitF32x4AbsEPNS1_4NodeE@PLT
	jmp	.L2471
.L2608:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitF32x4UConvertI32x4EPNS1_4NodeE@PLT
	jmp	.L2471
.L2609:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitF32x4SConvertI32x4EPNS1_4NodeE@PLT
	jmp	.L2471
.L2610:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitF32x4ReplaceLaneEPNS1_4NodeE@PLT
	jmp	.L2471
.L2611:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitF32x4ExtractLaneEPNS1_4NodeE@PLT
	jmp	.L2471
.L2612:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitF32x4SplatEPNS1_4NodeE@PLT
	jmp	.L2471
.L2613:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitF64x2LeEPNS1_4NodeE@PLT
	jmp	.L2471
.L2614:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitF64x2LtEPNS1_4NodeE@PLT
	jmp	.L2471
.L2615:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitF64x2NeEPNS1_4NodeE@PLT
	jmp	.L2471
.L2616:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitF64x2EqEPNS1_4NodeE@PLT
	jmp	.L2471
.L2617:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MaxEPNS1_4NodeE@PLT
	jmp	.L2471
.L2618:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MinEPNS1_4NodeE@PLT
	jmp	.L2471
.L2619:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2DivEPNS1_4NodeE@PLT
	jmp	.L2471
.L2620:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2MulEPNS1_4NodeE@PLT
	jmp	.L2471
.L2621:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2SubEPNS1_4NodeE@PLT
	jmp	.L2471
.L2622:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2AddEPNS1_4NodeE@PLT
	jmp	.L2471
.L2623:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2NegEPNS1_4NodeE@PLT
	jmp	.L2471
.L2624:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitF64x2AbsEPNS1_4NodeE@PLT
	jmp	.L2471
.L2625:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitF64x2ReplaceLaneEPNS1_4NodeE@PLT
	jmp	.L2471
.L2626:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitF64x2ExtractLaneEPNS1_4NodeE@PLT
	jmp	.L2471
.L2627:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitF64x2SplatEPNS1_4NodeE@PLT
	jmp	.L2471
.L2628:
	leaq	-112(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r12, -80(%rbp)
	movabsq	$38654705669, %rax
	movq	$0, -96(%rbp)
	movq	%rax, -112(%rbp)
	movl	$-1, -88(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector28VisitStackPointerGreaterThanEPNS1_4NodeEPNS1_17FlagsContinuationE@PLT
	jmp	.L2471
.L2630:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord32ToInt64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2631:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord16ToInt64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2632:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector27VisitSignExtendWord8ToInt64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2633:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector28VisitSignExtendWord16ToInt32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2634:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector27VisitSignExtendWord8ToInt32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2635:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18VisitMemoryBarrierEPNS1_4NodeE@PLT
	jmp	.L2471
.L2636:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector19VisitProtectedStoreEPNS1_4NodeE@PLT
	jmp	.L2471
.L2637:
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18VisitProtectedLoadEPNS1_4NodeE@PLT
	jmp	.L2471
.L2639:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector19VisitUnalignedStoreEPNS1_4NodeE@PLT
	jmp	.L2471
.L2640:
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18VisitUnalignedLoadEPNS1_4NodeE@PLT
	jmp	.L2471
.L2641:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector27VisitLoadParentFramePointerEPNS1_4NodeE
	jmp	.L2471
.L2642:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitLoadFramePointerEPNS1_4NodeE
	jmp	.L2471
.L2643:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector27EmitWordPoisonOnSpeculationEPNS1_4NodeE
	jmp	.L2471
.L2644:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector27EmitWordPoisonOnSpeculationEPNS1_4NodeE
	jmp	.L2471
.L2645:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$8, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector27EmitWordPoisonOnSpeculationEPNS1_4NodeE
	jmp	.L2471
.L2646:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector28VisitFloat64InsertHighWord32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2647:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector27VisitFloat64InsertLowWord32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2648:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector29VisitFloat64ExtractHighWord32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2649:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector28VisitFloat64ExtractLowWord32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2650:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector26VisitBitcastInt64ToFloat64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2651:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector26VisitBitcastInt32ToFloat32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2652:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector26VisitBitcastFloat64ToInt64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2653:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector26VisitBitcastFloat32ToInt32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2654:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint64ToFloat64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2780:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector26VisitUint32LessThanOrEqualEPNS1_4NodeE@PLT
	jmp	.L2471
.L2778:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitInt64LessThanOrEqualEPNS1_4NodeE@PLT
	jmp	.L2471
.L2779:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18VisitInt64LessThanEPNS1_4NodeE@PLT
	jmp	.L2471
.L2675:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector27VisitTruncateFloat64ToInt64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2676:
	movq	%r12, %rdx
	movl	$13, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movzbl	23(%r12), %eax
	leaq	32(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2853
	movq	32(%r12), %rax
	leaq	16(%rax), %rdx
.L2853:
	movq	(%rdx), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	(%rax), %rax
	movzwl	16(%rax), %eax
	subw	$366, %ax
	cmpw	$2, %ax
	ja	.L2854
	call	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE
	jmp	.L2471
.L2770:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector27VisitFloat64LessThanOrEqualEPNS1_4NodeE@PLT
	jmp	.L2471
.L2774:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20VisitFloat32LessThanEPNS1_4NodeE@PLT
	jmp	.L2471
.L2775:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector17VisitFloat32EqualEPNS1_4NodeE@PLT
	jmp	.L2471
.L2776:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector26VisitUint64LessThanOrEqualEPNS1_4NodeE@PLT
	jmp	.L2471
.L2777:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector19VisitUint64LessThanEPNS1_4NodeE@PLT
	jmp	.L2471
.L2772:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector17VisitFloat64EqualEPNS1_4NodeE@PLT
	jmp	.L2471
.L2773:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector27VisitFloat32LessThanOrEqualEPNS1_4NodeE@PLT
	jmp	.L2471
.L2771:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20VisitFloat64LessThanEPNS1_4NodeE@PLT
	jmp	.L2471
.L2754:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MinEPNS1_4NodeE@PLT
	jmp	.L2471
.L2755:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MaxEPNS1_4NodeE@PLT
	jmp	.L2471
.L2756:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$80, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector24VisitFloat64Ieee754BinopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2757:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector16VisitFloat32SqrtEPNS1_4NodeE@PLT
	jmp	.L2471
.L2758:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector19VisitFloat32RoundUpEPNS1_4NodeE@PLT
	jmp	.L2471
.L2759:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitFloat32RoundTruncateEPNS1_4NodeE@PLT
	jmp	.L2471
.L2760:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitFloat32RoundTiesEvenEPNS1_4NodeE@PLT
	jmp	.L2471
.L2761:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitFloat32RoundDownEPNS1_4NodeE@PLT
	jmp	.L2471
.L2762:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32NegEPNS1_4NodeE@PLT
	jmp	.L2471
.L2763:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32AbsEPNS1_4NodeE@PLT
	jmp	.L2471
.L2764:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MinEPNS1_4NodeE@PLT
	jmp	.L2471
.L2765:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MaxEPNS1_4NodeE@PLT
	jmp	.L2471
.L2766:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32DivEPNS1_4NodeE@PLT
	jmp	.L2471
.L2767:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32MulEPNS1_4NodeE@PLT
	jmp	.L2471
.L2768:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32SubEPNS1_4NodeE@PLT
	jmp	.L2471
.L2769:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat32AddEPNS1_4NodeE@PLT
	jmp	.L2471
.L2722:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$93, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2723:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector16VisitFloat64SqrtEPNS1_4NodeE@PLT
	jmp	.L2471
.L2724:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$92, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2725:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$91, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2726:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector19VisitFloat64RoundUpEPNS1_4NodeE@PLT
	jmp	.L2471
.L2727:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTruncateEPNS1_4NodeE@PLT
	jmp	.L2471
.L2728:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTiesEvenEPNS1_4NodeE@PLT
	jmp	.L2471
.L2729:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitFloat64RoundTiesAwayEPNS1_4NodeE@PLT
	jmp	.L2471
.L2730:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitFloat64RoundDownEPNS1_4NodeE@PLT
	jmp	.L2471
.L2731:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64NegEPNS1_4NodeE@PLT
	jmp	.L2471
.L2732:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$89, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2733:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$88, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2734:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$87, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2735:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$86, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2736:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$85, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2737:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$84, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2738:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$83, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2739:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$82, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2740:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$81, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2741:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$79, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2742:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$78, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2743:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$77, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2744:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$76, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2745:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$75, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2746:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$74, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2747:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64AbsEPNS1_4NodeE@PLT
	jmp	.L2471
.L2748:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$90, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector24VisitFloat64Ieee754BinopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2749:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64ModEPNS1_4NodeE@PLT
	jmp	.L2471
.L2750:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64DivEPNS1_4NodeE@PLT
	jmp	.L2471
.L2751:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64MulEPNS1_4NodeE@PLT
	jmp	.L2471
.L2752:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64SubEPNS1_4NodeE@PLT
	jmp	.L2471
.L2753:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitFloat64AddEPNS1_4NodeE@PLT
	jmp	.L2471
.L2483:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitS128SelectEPNS1_4NodeE@PLT
	jmp	.L2471
.L2484:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitS128XorEPNS1_4NodeE@PLT
	jmp	.L2471
.L2485:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector11VisitS128OrEPNS1_4NodeE@PLT
	jmp	.L2471
.L2486:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitS128AndEPNS1_4NodeE@PLT
	jmp	.L2471
.L2487:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitS128NotEPNS1_4NodeE@PLT
	jmp	.L2471
.L2488:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitS128ZeroEPNS1_4NodeE@PLT
	jmp	.L2471
.L2781:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector19VisitUint32LessThanEPNS1_4NodeE@PLT
	jmp	.L2471
.L2782:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitInt32LessThanOrEqualEPNS1_4NodeE@PLT
	jmp	.L2471
.L2479:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector16VisitS1x4AnyTrueEPNS1_4NodeE@PLT
	jmp	.L2471
.L2480:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector16VisitS1x2AllTrueEPNS1_4NodeE@PLT
	jmp	.L2471
.L2478:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector16VisitS1x4AllTrueEPNS1_4NodeE@PLT
	jmp	.L2471
.L2788:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitInt64ModEPNS1_4NodeE@PLT
	jmp	.L2471
.L2789:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitInt64DivEPNS1_4NodeE@PLT
	jmp	.L2471
.L2790:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitInt64MulEPNS1_4NodeE@PLT
	jmp	.L2471
.L2798:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitWord64XorEPNS1_4NodeE@PLT
	jmp	.L2471
.L2799:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitWord64OrEPNS1_4NodeE@PLT
	jmp	.L2471
.L2800:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitWord64AndEPNS1_4NodeE@PLT
	jmp	.L2471
.L2801:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18VisitUint32MulHighEPNS1_4NodeE@PLT
	jmp	.L2471
.L2802:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitUint32ModEPNS1_4NodeE@PLT
	jmp	.L2471
.L2803:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitUint32DivEPNS1_4NodeE@PLT
	jmp	.L2471
.L2804:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitInt32ModEPNS1_4NodeE@PLT
	jmp	.L2471
.L2805:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitInt32DivEPNS1_4NodeE@PLT
	jmp	.L2471
.L2806:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector17VisitInt32MulHighEPNS1_4NodeE@PLT
	jmp	.L2471
.L2807:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitInt32MulWithOverflowEPNS1_4NodeE@PLT
	jmp	.L2471
.L2808:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitInt32MulEPNS1_4NodeE@PLT
	jmp	.L2471
.L2809:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitInt32SubWithOverflowEPNS1_4NodeE@PLT
	jmp	.L2471
.L2810:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitInt32SubEPNS1_4NodeE@PLT
	jmp	.L2471
.L2811:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitInt32AddWithOverflowEPNS1_4NodeE@PLT
	jmp	.L2471
.L2812:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitInt32AddEPNS1_4NodeE@PLT
	jmp	.L2471
.L2813:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitWord32RorEPNS1_4NodeE@PLT
	jmp	.L2471
.L2842:
	call	_ZN2v88internal8compiler8TrapIdOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal8compiler19InstructionSelector15VisitTrapUnlessEPNS1_4NodeENS1_6TrapIdE
	jmp	.L2471
.L2836:
	call	_ZN2v88internal8compiler19PhiRepresentationOfEPKNS1_8OperatorE@PLT
	movl	%eax, %esi
	testb	%al, %al
	je	.L2471
	movq	%r13, %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector8VisitPhiEPNS1_4NodeE
	jmp	.L2471
.L2816:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitWord32ShlEPNS1_4NodeE@PLT
	jmp	.L2471
.L2817:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitWord32XorEPNS1_4NodeE@PLT
	jmp	.L2471
.L2818:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitWord32OrEPNS1_4NodeE@PLT
	jmp	.L2471
.L2819:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitWord32AndEPNS1_4NodeE@PLT
	jmp	.L2471
.L2820:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitWord32ReverseBytesEPNS1_4NodeE@PLT
	jmp	.L2471
.L2821:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector22VisitWord32ReverseBitsEPNS1_4NodeE@PLT
	jmp	.L2471
.L2822:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitInt32AbsWithOverflowEPNS1_4NodeE@PLT
	jmp	.L2471
.L2823:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitWord32CtzEPNS1_4NodeE@PLT
	jmp	.L2471
.L2824:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitWord32ClzEPNS1_4NodeE@PLT
	jmp	.L2471
.L2827:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitDeadValueEPNS1_4NodeE
	jmp	.L2471
.L2828:
	leaq	-112(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$19, %esi
	movq	%r13, %rdi
	movq	$0, -112(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0
	jmp	.L2471
.L2829:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector11VisitRetainEPNS1_4NodeE
	jmp	.L2471
.L2830:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitProjectionEPNS1_4NodeE
	jmp	.L2471
.L2719:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector22VisitWord32AtomicStoreEPNS1_4NodeE@PLT
	jmp	.L2471
.L2720:
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitWord32AtomicLoadEPNS1_4NodeE@PLT
	jmp	.L2471
.L2721:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movl	$94, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitFloat64Ieee754UnopEPNS1_4NodeEi@PLT
	jmp	.L2471
.L2476:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector16VisitS1x8AllTrueEPNS1_4NodeE@PLT
	jmp	.L2471
.L2477:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector16VisitS1x8AnyTrueEPNS1_4NodeE@PLT
	jmp	.L2471
.L2843:
	call	_ZN2v88internal8compiler8TrapIdOfEPKNS1_8OperatorE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal8compiler19InstructionSelector11VisitTrapIfEPNS1_4NodeENS1_6TrapIdE
	jmp	.L2471
.L2844:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitDeoptimizeUnlessEPNS1_4NodeE
	jmp	.L2471
.L2845:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector17VisitDeoptimizeIfEPNS1_4NodeE
	jmp	.L2471
.L2846:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$8, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector16VisitIfExceptionEPNS1_4NodeE
	jmp	.L2471
.L2814:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitWord32SarEPNS1_4NodeE@PLT
	jmp	.L2471
.L2815:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitWord32ShrEPNS1_4NodeE@PLT
	jmp	.L2471
.L2833:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE
	jmp	.L2471
.L2835:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$8, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE
	jmp	.L2471
.L2473:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector17VisitS1x16AllTrueEPNS1_4NodeE@PLT
	jmp	.L2471
.L2475:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector17VisitS1x16AnyTrueEPNS1_4NodeE@PLT
	jmp	.L2471
.L2831:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$8, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitOsrValueEPNS1_4NodeE
	jmp	.L2471
.L2832:
	movq	8(%r13), %rbx
	call	_ZN2v88internal8compiler16ParameterIndexOfEPKNS1_8OperatorE@PLT
	addl	$1, %eax
	movq	(%rbx), %rdx
	cltq
	movzbl	12(%rdx), %esi
	testq	%rax, %rax
	je	.L2847
	movq	24(%rdx), %rdx
	addq	(%rdx), %rax
	movq	16(%rdx), %rdx
	movzbl	-4(%rdx,%rax,8), %esi
.L2847:
	movq	%r13, %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitParameterEPNS1_4NodeE
	jmp	.L2471
.L2838:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$11, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitConstantEPNS1_4NodeE
	jmp	.L2471
.L2655:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitRoundUint64ToFloat32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2840:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitConstantEPNS1_4NodeE
	jmp	.L2471
.L2841:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$12, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitConstantEPNS1_4NodeE
	jmp	.L2471
.L2791:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitInt64SubWithOverflowEPNS1_4NodeE@PLT
	jmp	.L2471
.L2792:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitInt64SubEPNS1_4NodeE@PLT
	jmp	.L2471
.L2794:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitWord64RorEPNS1_4NodeE@PLT
	jmp	.L2471
.L2795:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitWord64SarEPNS1_4NodeE@PLT
	jmp	.L2471
.L2796:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitWord64ShrEPNS1_4NodeE@PLT
	jmp	.L2471
.L2797:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitWord64ShlEPNS1_4NodeE@PLT
	jmp	.L2471
.L2783:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18VisitInt32LessThanEPNS1_4NodeE@PLT
	jmp	.L2471
.L2784:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector16VisitWord64EqualEPNS1_4NodeE@PLT
	jmp	.L2471
.L2785:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector16VisitWord32EqualEPNS1_4NodeE@PLT
	jmp	.L2471
.L2786:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitUint64ModEPNS1_4NodeE@PLT
	jmp	.L2471
.L2787:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitUint64DivEPNS1_4NodeE@PLT
	jmp	.L2471
.L2537:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitI16x8SConvertI32x4EPNS1_4NodeE@PLT
	jmp	.L2471
.L2538:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8ShrSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2539:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8ShlEPNS1_4NodeE@PLT
	jmp	.L2471
.L2540:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8NegEPNS1_4NodeE@PLT
	jmp	.L2471
.L2541:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector27VisitI16x8SConvertI8x16HighEPNS1_4NodeE@PLT
	jmp	.L2471
.L2542:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector26VisitI16x8SConvertI8x16LowEPNS1_4NodeE@PLT
	jmp	.L2471
.L2543:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitI16x8ReplaceLaneEPNS1_4NodeE@PLT
	jmp	.L2471
.L2544:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitI16x8ExtractLaneEPNS1_4NodeE@PLT
	jmp	.L2471
.L2545:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitI16x8SplatEPNS1_4NodeE@PLT
	jmp	.L2471
.L2546:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GeUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2547:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GtUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2548:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MaxUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2549:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MinUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2550:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4ShrUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2551:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector27VisitI32x4UConvertI16x8HighEPNS1_4NodeE@PLT
	jmp	.L2471
.L2552:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector26VisitI32x4UConvertI16x8LowEPNS1_4NodeE@PLT
	jmp	.L2471
.L2553:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitI32x4UConvertF32x4EPNS1_4NodeE@PLT
	jmp	.L2471
.L2554:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GeSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2555:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4GtSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2556:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitI32x4NeEPNS1_4NodeE@PLT
	jmp	.L2471
.L2557:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitI32x4EqEPNS1_4NodeE@PLT
	jmp	.L2471
.L2558:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MaxSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2559:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4MinSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2560:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4MulEPNS1_4NodeE@PLT
	jmp	.L2471
.L2561:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4SubEPNS1_4NodeE@PLT
	jmp	.L2471
.L2562:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18VisitI32x4AddHorizEPNS1_4NodeE@PLT
	jmp	.L2471
.L2563:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4AddEPNS1_4NodeE@PLT
	jmp	.L2471
.L2564:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI32x4ShrSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2565:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4ShlEPNS1_4NodeE@PLT
	jmp	.L2471
.L2566:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI32x4NegEPNS1_4NodeE@PLT
	jmp	.L2471
.L2567:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector27VisitI32x4SConvertI16x8HighEPNS1_4NodeE@PLT
	jmp	.L2471
.L2568:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector26VisitI32x4SConvertI16x8LowEPNS1_4NodeE@PLT
	jmp	.L2471
.L2569:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitI32x4SConvertF32x4EPNS1_4NodeE@PLT
	jmp	.L2471
.L2570:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitI32x4ReplaceLaneEPNS1_4NodeE@PLT
	jmp	.L2471
.L2571:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitI32x4ExtractLaneEPNS1_4NodeE@PLT
	jmp	.L2471
.L2572:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitI32x4SplatEPNS1_4NodeE@PLT
	jmp	.L2471
.L2573:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GeUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2574:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GtUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2575:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MaxUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2576:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MinUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2577:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2ShrUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2578:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GeSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2579:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2GtSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2580:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitI64x2NeEPNS1_4NodeE@PLT
	jmp	.L2471
.L2581:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitI64x2EqEPNS1_4NodeE@PLT
	jmp	.L2471
.L2582:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MaxSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2583:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2MinSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2584:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2MulEPNS1_4NodeE@PLT
	jmp	.L2471
.L2585:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2SubEPNS1_4NodeE@PLT
	jmp	.L2471
.L2586:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2AddEPNS1_4NodeE@PLT
	jmp	.L2471
.L2587:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI64x2ShrSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2588:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2ShlEPNS1_4NodeE@PLT
	jmp	.L2471
.L2589:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI64x2NegEPNS1_4NodeE@PLT
	jmp	.L2471
.L2793:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$5, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitInt64AddWithOverflowEPNS1_4NodeE@PLT
	jmp	.L2471
.L2686:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$8, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector24VisitBitcastWordToTaggedEPNS1_4NodeE
	jmp	.L2471
.L2684:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$9, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE
	jmp	.L2471
.L2685:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$6, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE
	jmp	.L2471
.L2680:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector25VisitChangeFloat64ToInt32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2681:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$13, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector27VisitChangeFloat32ToFloat64EPNS1_4NodeE@PLT
	jmp	.L2471
.L2682:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat64ToWord32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2683:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12EmitIdentityEPNS1_4NodeE
	jmp	.L2471
.L2672:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat32ToUint32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2673:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector27VisitTruncateFloat32ToInt32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2674:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector28VisitTruncateFloat64ToUint32EPNS1_4NodeE@PLT
	jmp	.L2471
.L2505:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16SubEPNS1_4NodeE@PLT
	jmp	.L2471
.L2506:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector22VisitI8x16AddSaturateSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2507:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16AddEPNS1_4NodeE@PLT
	jmp	.L2471
.L2508:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16ShrSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2509:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16ShlEPNS1_4NodeE@PLT
	jmp	.L2471
.L2510:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16NegEPNS1_4NodeE@PLT
	jmp	.L2471
.L2511:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitI8x16SConvertI16x8EPNS1_4NodeE@PLT
	jmp	.L2471
.L2512:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitI8x16ReplaceLaneEPNS1_4NodeE@PLT
	jmp	.L2471
.L2513:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector21VisitI8x16ExtractLaneEPNS1_4NodeE@PLT
	jmp	.L2471
.L2514:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector15VisitI8x16SplatEPNS1_4NodeE@PLT
	jmp	.L2471
.L2515:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GeUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2516:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GtUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2517:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MaxUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2518:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MinUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2519:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector22VisitI16x8SubSaturateUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2520:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector22VisitI16x8AddSaturateUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2521:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitI16x8UConvertI32x4EPNS1_4NodeE@PLT
	jmp	.L2471
.L2522:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8ShrUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2523:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector27VisitI16x8UConvertI8x16HighEPNS1_4NodeE@PLT
	jmp	.L2471
.L2524:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector26VisitI16x8UConvertI8x16LowEPNS1_4NodeE@PLT
	jmp	.L2471
.L2525:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GeSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2526:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8GtSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2527:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitI16x8NeEPNS1_4NodeE@PLT
	jmp	.L2471
.L2528:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitI16x8EqEPNS1_4NodeE@PLT
	jmp	.L2471
.L2529:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MaxSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2530:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI16x8MinSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2531:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8MulEPNS1_4NodeE@PLT
	jmp	.L2471
.L2532:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector22VisitI16x8SubSaturateSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2533:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8SubEPNS1_4NodeE@PLT
	jmp	.L2471
.L2534:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector18VisitI16x8AddHorizEPNS1_4NodeE@PLT
	jmp	.L2471
.L2535:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector22VisitI16x8AddSaturateSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2536:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI16x8AddEPNS1_4NodeE@PLT
	jmp	.L2471
.L2489:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GeUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2490:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GtUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2491:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MaxUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2492:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MinUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2493:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16ShrUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2494:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector22VisitI8x16SubSaturateUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2495:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector22VisitI8x16AddSaturateUEPNS1_4NodeE@PLT
	jmp	.L2471
.L2496:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector23VisitI8x16UConvertI16x8EPNS1_4NodeE@PLT
	jmp	.L2471
.L2497:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GeSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2498:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16GtSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2499:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitI8x16NeEPNS1_4NodeE@PLT
	jmp	.L2471
.L2500:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitI8x16EqEPNS1_4NodeE@PLT
	jmp	.L2471
.L2501:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MaxSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2502:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector14VisitI8x16MinSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2503:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitI8x16MulEPNS1_4NodeE@PLT
	jmp	.L2471
.L2504:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector22VisitI8x16SubSaturateSEPNS1_4NodeE@PLT
	jmp	.L2471
.L2481:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$4, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector16VisitS1x2AnyTrueEPNS1_4NodeE@PLT
	jmp	.L2471
.L2482:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	$14, %esi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector17VisitS8x16ShuffleEPNS1_4NodeE@PLT
	jmp	.L2471
.L2826:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector17VisitStaticAssertEPNS1_4NodeE
.L2711:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2848:
	movq	%r12, %rdx
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector20MarkAsRepresentationENS0_21MachineRepresentationEPNS1_4NodeE
	jmp	.L2851
.L2854:
	call	_ZN2v88internal8compiler19InstructionSelector22VisitFloat64SilenceNaNEPNS1_4NodeE@PLT
	jmp	.L2471
.L2865:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25125:
	.size	_ZN2v88internal8compiler19InstructionSelector9VisitNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector9VisitNodeEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector13VisitTailCallEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector13VisitTailCallEPNS1_4NodeE
	.type	_ZN2v88internal8compiler19InstructionSelector13VisitTailCallEPNS1_4NodeE, @function
_ZN2v88internal8compiler19InstructionSelector13VisitTailCallEPNS1_4NodeE:
.LFB25193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler16CallDescriptorOfEPKNS1_8OperatorE@PLT
	movq	0(%r13), %rdi
	movq	%rax, %rbx
	movq	8(%r12), %rax
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler16CallDescriptorOfEPKNS1_8OperatorE@PLT
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%rax, -280(%rbp)
	call	_ZNK2v88internal8compiler14CallDescriptor22GetStackParameterDeltaEPKS2_@PLT
	movq	$0, -120(%rbp)
	leaq	-192(%rbp), %rdi
	movl	%eax, -264(%rbp)
	movq	(%r12), %rax
	movq	$0, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rax, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	24(%rbx), %rax
	movq	%rbx, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	(%rax), %rsi
	call	_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE7reserveEm
	movq	24(%rbx), %rax
	leaq	-160(%rbp), %rdi
	movq	(%rax), %rsi
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE7reserveEm
	movq	-208(%rbp), %rax
	leaq	-96(%rbp), %rdi
	movq	24(%rax), %rax
	movq	8(%rax), %rsi
	addq	$1, %rsi
	call	_ZNSt6vectorIN2v88internal8compiler13PushParameterENS1_13ZoneAllocatorIS3_EEE7reserveEm
	movq	-208(%rbp), %rax
	movq	-200(%rbp), %rdi
	movq	24(%rax), %rax
	movq	8(%rax), %r15
	leaq	1(%r15), %rsi
	testq	%rdi, %rdi
	je	.L2867
	call	_ZNK2v88internal8compiler20FrameStateDescriptor12GetTotalSizeEv@PLT
	leaq	2(%r15,%rax), %rsi
.L2867:
	leaq	-128(%rbp), %r15
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE7reserveEm
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector26IsTailCallAddressImmediateEv@PLT
	movl	-264(%rbp), %r9d
	movq	%r13, %rsi
	movq	%r12, %rdi
	cmpb	$1, %al
	movl	$1, %r8d
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$15, %edx
	cmpb	$1, %al
	movq	-280(%rbp), %rax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$7, %ecx
	testb	$-128, 64(%rax)
	cmovne	%edx, %ecx
	leaq	-208(%rbp), %rdx
	call	_ZN2v88internal8compiler19InstructionSelector20InitializeCallBufferEPNS1_4NodeEPNS1_10CallBufferENS_4base5FlagsINS2_14CallBufferFlagEiEEbi
	movq	(%r12), %rax
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	8(%r12), %rax
	movq	$0, -216(%rbp)
	movq	(%rax), %rax
	movl	8(%rax), %eax
	movl	%eax, -284(%rbp)
	cmpl	$1, %eax
	je	.L2904
	movl	8(%rbx), %eax
	cmpl	$2, %eax
	je	.L2889
	cmpl	$4, %eax
	je	.L2890
	testl	%eax, %eax
	je	.L2905
.L2871:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2905:
	movl	$2, -284(%rbp)
.L2903:
	leaq	-256(%rbp), %r13
.L2874:
	movl	64(%rbx), %eax
	movq	%r13, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$9, %esi
	movq	%r12, %rdi
	movq	$0, -256(%rbp)
	movl	%eax, -260(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0
	movq	-280(%rbp), %rdi
	call	_ZNK2v88internal8compiler14CallDescriptor23GetFirstUnusedStackSlotEv@PLT
	movq	%r13, %rdi
	movq	16(%r12), %rbx
	movl	%eax, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-256(%rbp), %edx
	testl	%edx, %edx
	jne	.L2875
	cmpb	$19, -252(%rbp)
	je	.L2906
.L2875:
	movq	160(%rbx), %rsi
	movq	%rsi, %r14
	subq	152(%rbx), %r14
	sarq	$4, %r14
	cmpq	168(%rbx), %rsi
	je	.L2877
	movzbl	-252(%rbp), %edi
	movq	-248(%rbp), %rcx
	movl	%edx, (%rsi)
	movb	%dil, 4(%rsi)
	movq	%rcx, 8(%rsi)
	addq	$16, 160(%rbx)
.L2878:
	movq	%r14, %rax
	salq	$32, %rax
	orq	$11, %rax
.L2876:
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, -256(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movl	-264(%rbp), %esi
	movq	%r13, %rdi
	movq	16(%r12), %rbx
	addl	$1, %esi
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-256(%rbp), %edx
	testl	%edx, %edx
	jne	.L2879
	cmpb	$19, -252(%rbp)
	je	.L2907
.L2879:
	movq	160(%rbx), %rsi
	movq	%rsi, %r14
	subq	152(%rbx), %r14
	sarq	$4, %r14
	cmpq	168(%rbx), %rsi
	je	.L2881
	movzbl	-252(%rbp), %edi
	movq	-248(%rbp), %rcx
	movl	%edx, (%rsi)
	movb	%dil, 4(%rsi)
	movq	%rcx, 8(%rsi)
	addq	$16, 160(%rbx)
.L2882:
	movq	%r14, %rax
	salq	$32, %rax
	orq	$11, %rax
.L2880:
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, -256(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movq	-232(%rbp), %rdx
	movq	-224(%rbp), %rax
	movl	$0, %ecx
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %r8
	cmpq	%rdx, %rax
	cmovne	%rdx, %rcx
	subq	%rdx, %rax
	subq	%r9, %r8
	movq	%rax, %rdx
	sarq	$3, %r8
	sarq	$3, %rdx
	cmpq	$503, %rax
	ja	.L2893
	cmpq	$65534, %r8
	ja	.L2893
	pushq	%rcx
	movl	-260(%rbp), %esi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	pushq	%rdx
	xorl	%edx, %edx
	sall	$22, %esi
	orl	-284(%rbp), %esi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0
	popq	%rax
	popq	%rdx
.L2866:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2908
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2890:
	.cfi_restore_state
	movl	$11, -284(%rbp)
	leaq	-256(%rbp), %r13
	jmp	.L2874
	.p2align 4,,10
	.p2align 3
.L2889:
	movl	$4, -284(%rbp)
	leaq	-256(%rbp), %r13
	jmp	.L2874
	.p2align 4,,10
	.p2align 3
.L2893:
	movb	$1, 376(%r12)
	jmp	.L2866
	.p2align 4,,10
	.p2align 3
.L2904:
	movl	8(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L2871
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector38GetTempsCountForTailCallFromJSFunctionEv@PLT
	movl	%eax, -260(%rbp)
	testl	%eax, %eax
	jle	.L2903
	leaq	-240(%rbp), %rax
	xorl	%r14d, %r14d
	leaq	-256(%rbp), %r13
	movq	%rax, -272(%rbp)
	.p2align 4,,10
	.p2align 3
.L2873:
	movq	16(%r12), %rdi
	addl	$1, %r14d
	call	_ZN2v88internal8compiler19InstructionSequence19NextVirtualRegisterEv@PLT
	movq	-272(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$377957122049, %rcx
	movl	%eax, %eax
	salq	$3, %rax
	orq	%rcx, %rax
	movq	%rax, -256(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler18InstructionOperandENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	cmpl	%r14d, -260(%rbp)
	jne	.L2873
	jmp	.L2874
	.p2align 4,,10
	.p2align 3
.L2906:
	movslq	-248(%rbp), %rax
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L2876
	.p2align 4,,10
	.p2align 3
.L2907:
	movslq	-248(%rbp), %rax
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L2880
	.p2align 4,,10
	.p2align 3
.L2881:
	leaq	144(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L2882
	.p2align 4,,10
	.p2align 3
.L2877:
	leaq	144(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L2878
.L2908:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25193:
	.size	_ZN2v88internal8compiler19InstructionSelector13VisitTailCallEPNS1_4NodeE, .-_ZN2v88internal8compiler19InstructionSelector13VisitTailCallEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitControlEPNS1_10BasicBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector12VisitControlEPNS1_10BasicBlockE
	.type	_ZN2v88internal8compiler19InstructionSelector12VisitControlEPNS1_10BasicBlockE, @function
_ZN2v88internal8compiler19InstructionSelector12VisitControlEPNS1_10BasicBlockE:
.LFB25116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	56(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	cmpl	$8, 52(%rsi)
	movq	%rax, -152(%rbp)
	movq	72(%rdi), %rax
	movq	%rax, -160(%rbp)
	ja	.L2910
	movl	52(%rsi), %eax
	leaq	.L2912(%rip), %rcx
	movq	%rdi, %r15
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler19InstructionSelector12VisitControlEPNS1_10BasicBlockE,"a",@progbits
	.align 4
	.align 4
.L2912:
	.long	.L2920-.L2912
	.long	.L2919-.L2912
	.long	.L2918-.L2912
	.long	.L2917-.L2912
	.long	.L2916-.L2912
	.long	.L2915-.L2912
	.long	.L2914-.L2912
	.long	.L2913-.L2912
	.long	.L2911-.L2912
	.section	.text._ZN2v88internal8compiler19InstructionSelector12VisitControlEPNS1_10BasicBlockE
	.p2align 4,,10
	.p2align 3
.L2916:
	movq	112(%rsi), %r13
	movq	(%rdi), %rdi
	movq	-8(%r13), %rax
	subq	104(%rsi), %r13
	movq	%r13, %r12
	sarq	$3, %r12
	movq	%rax, -176(%rbp)
	leaq	-1(%r12), %rax
	cmpq	$134217727, %rax
	ja	.L2940
	movq	%rax, %r14
	movq	%rsi, -136(%rbp)
	movq	%rdi, -128(%rbp)
	salq	$4, %r14
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	testq	%rax, %rax
	je	.L2923
	movq	%r14, %rsi
	call	_ZN2v88internal4Zone3NewEm
	movq	%r12, %rcx
	movq	-136(%rbp), %rdx
	leaq	(%rax,%r14), %rsi
	movq	%rax, -120(%rbp)
	salq	$4, %rcx
	movq	%rsi, -104(%rbp)
	leaq	-16(%rax,%rcx), %rcx
	.p2align 4,,10
	.p2align 3
.L2925:
	movl	$0, (%rax)
	addq	$16, %rax
	movl	$0, -12(%rax)
	movq	$0, -8(%rax)
	cmpq	%rcx, %rax
	jne	.L2925
	leaq	-8(%r13), %rax
	movq	%rsi, -112(%rbp)
	xorl	%r12d, %r12d
	movl	$-2147483648, %r14d
	movq	%rax, -168(%rbp)
	movl	$2147483647, %r13d
	.p2align 4,,10
	.p2align 3
.L2926:
	movq	104(%rdx), %rax
	movq	%rdx, -144(%rbp)
	movq	(%rax,%r12), %r10
	movq	72(%r10), %rax
	movq	%r10, -136(%rbp)
	movq	(%rax), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal8compiler19IfValueParametersOfEPKNS1_8OperatorE@PLT
	movq	-120(%rbp), %rdi
	movq	-136(%rbp), %r10
	movq	(%rax), %r11
	movq	-144(%rbp), %rdx
	leaq	(%rdi,%r12,2), %rdi
	movq	%r11, (%rdi)
	movq	%r10, 8(%rdi)
	movl	(%rax), %eax
	cmpl	%eax, %r13d
	cmovg	%eax, %r13d
	cmpl	%eax, %r14d
	cmovl	%eax, %r14d
	addq	$8, %r12
	cmpq	-168(%rbp), %r12
	jne	.L2926
	leaq	-128(%rbp), %rax
	movl	%r13d, -88(%rbp)
	movq	-112(%rbp), %rsi
	movq	%rax, -96(%rbp)
	movq	-176(%rbp), %rax
	movl	%r14d, -84(%rbp)
	movq	%rax, -72(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, -120(%rbp)
	je	.L2927
	leal	1(%r14), %eax
	subl	%r13d, %eax
.L2927:
	leaq	-96(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector11VisitSwitchEPNS1_4NodeERKNS1_10SwitchInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2920:
	cmpl	$1, 416(%r15)
	jne	.L2909
	testq	%rbx, %rbx
	jne	.L2941
.L2909:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2942
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2941:
	.cfi_restore_state
	movl	20(%rbx), %eax
	movq	392(%r15), %rdx
	movq	-160(%rbp), %r12
	subq	-152(%rbp), %r12
	andl	$16777215, %eax
	sarq	$3, %r12
	leaq	(%rdx,%rax,8), %rdx
	movq	72(%r15), %rax
	subq	64(%r15), %rax
	sarq	$3, %rax
	movl	%r12d, 4(%rdx)
	movl	%eax, (%rdx)
	jmp	.L2909
	.p2align 4,,10
	.p2align 3
.L2913:
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector11VisitReturnEPNS1_4NodeE
	jmp	.L2920
	.p2align 4,,10
	.p2align 3
.L2911:
	leaq	-96(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$21, %esi
	movq	$0, -96(%rbp)
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0
	jmp	.L2920
	.p2align 4,,10
	.p2align 3
.L2919:
	movq	104(%rsi), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler19InstructionSelector9VisitGotoEPNS1_10BasicBlockE
	jmp	.L2920
	.p2align 4,,10
	.p2align 3
.L2918:
	movq	104(%rsi), %rax
	movq	%rbx, %rsi
	movq	(%rax), %r13
	movq	8(%rax), %rdx
	call	_ZN2v88internal8compiler19InstructionSelector9VisitCallEPNS1_4NodeEPNS1_10BasicBlockE
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector9VisitGotoEPNS1_10BasicBlockE
	jmp	.L2920
	.p2align 4,,10
	.p2align 3
.L2917:
	movq	104(%rsi), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L2943
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector11VisitBranchEPNS1_4NodeEPNS1_10BasicBlockES6_
	jmp	.L2920
	.p2align 4,,10
	.p2align 3
.L2915:
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler22DeoptimizeParametersOfEPKNS1_8OperatorE@PLT
	leaq	32(%rbx), %rdx
	movdqu	(%rax), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -80(%rbp)
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L2928
	movq	32(%rbx), %rax
	leaq	16(%rax), %rdx
.L2928:
	movq	(%rdx), %r8
	movzbl	-96(%rbp), %esi
	leaq	-88(%rbp), %rcx
	movq	%r15, %rdi
	movzbl	-95(%rbp), %edx
	call	_ZN2v88internal8compiler19InstructionSelector14EmitDeoptimizeEimPNS1_18InstructionOperandEmS4_NS0_14DeoptimizeKindENS0_16DeoptimizeReasonERKNS1_14FeedbackSourceEPNS1_4NodeE.constprop.0
	jmp	.L2920
	.p2align 4,,10
	.p2align 3
.L2914:
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector13VisitTailCallEPNS1_4NodeE
	jmp	.L2920
	.p2align 4,,10
	.p2align 3
.L2943:
	movq	%rcx, %rsi
	call	_ZN2v88internal8compiler19InstructionSelector9VisitGotoEPNS1_10BasicBlockE
	jmp	.L2920
	.p2align 4,,10
	.p2align 3
.L2923:
	movabsq	$-9223372034707292161, %rsi
	leaq	-128(%rbp), %rdx
	movq	%rsi, -88(%rbp)
	movq	-176(%rbp), %rsi
	movq	%rdx, -96(%rbp)
	movq	%rsi, -72(%rbp)
	jmp	.L2927
.L2910:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2942:
	call	__stack_chk_fail@PLT
.L2940:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE25116:
	.size	_ZN2v88internal8compiler19InstructionSelector12VisitControlEPNS1_10BasicBlockE, .-_ZN2v88internal8compiler19InstructionSelector12VisitControlEPNS1_10BasicBlockE
	.section	.text._ZN2v88internal8compiler19InstructionSelector10VisitBlockEPNS1_10BasicBlockE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector10VisitBlockEPNS1_10BasicBlockE
	.type	_ZN2v88internal8compiler19InstructionSelector10VisitBlockEPNS1_10BasicBlockE, @function
_ZN2v88internal8compiler19InstructionSelector10VisitBlockEPNS1_10BasicBlockE:
.LFB25109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	64(%rdi), %r13
	movq	72(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, 48(%rdi)
	movq	72(%rsi), %rdx
	movq	80(%rsi), %rdi
	xorl	%esi, %esi
	cmpq	%rdi, %rdx
	je	.L2945
	.p2align 4,,10
	.p2align 3
.L2948:
	movq	(%rdx), %rcx
	movq	256(%rbx), %r8
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	movl	%esi, (%r8,%rax,4)
	movq	(%rcx), %rax
	movzwl	16(%rax), %eax
	leal	-502(%rax), %ecx
	movl	%eax, %r8d
	cmpw	$2, %cx
	setbe	%cl
	andl	$-65, %r8d
	cmpw	$431, %r8w
	sete	%r8b
	orl	%r8d, %ecx
	cmpw	$49, %ax
	sete	%r8b
	orb	%r8b, %cl
	jne	.L2989
	subw	$399, %ax
	cmpw	$26, %ax
	ja	.L2946
.L2989:
	addl	$1, %esi
.L2946:
	addq	$8, %rdx
	cmpq	%rdx, %rdi
	jne	.L2948
.L2945:
	movq	56(%r15), %rax
	testq	%rax, %rax
	je	.L2949
	movl	20(%rax), %eax
	movq	256(%rbx), %rdx
	andl	$16777215, %eax
	movl	%esi, (%rdx,%rax,4)
.L2949:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector12VisitControlEPNS1_10BasicBlockE
	cmpb	$0, 376(%rbx)
	jne	.L2944
	movq	72(%rbx), %rax
	movq	64(%rbx), %rcx
	subq	%r13, %r12
	movq	%r12, %rsi
	movq	%rax, %rdx
	sarq	$3, %rsi
	subq	%rcx, %rdx
	movq	%rsi, -72(%rbp)
	sarq	$3, %rdx
	cmpl	%edx, %esi
	je	.L2963
	movslq	%esi, %r12
	movq	56(%r15), %r13
	leaq	(%rcx,%r12,8), %rdi
	cmpq	%rdi, %rax
	je	.L2954
	leaq	-8(%rax), %rsi
	cmpq	%rsi, %rdi
	jnb	.L2954
	leaq	-9(%rax), %r9
	movq	%rdi, %rdx
	subq	%rdi, %r9
	movq	%r9, %r10
	shrq	$4, %r10
	movq	%r10, %r11
	leaq	1(%r10), %r8
	leaq	1(%r12,%r10), %r10
	notq	%r11
	leaq	(%rcx,%r10,8), %rcx
	leaq	(%rax,%r11,8), %r11
	cmpq	%rcx, %r11
	setnb	%r10b
	cmpq	%rdi, %rax
	setbe	%cl
	orb	%cl, %r10b
	je	.L2955
	cmpq	$31, %r9
	jbe	.L2955
	movq	%r8, %r9
	subq	$16, %rax
	movq	%rdi, %rcx
	shrq	%r9
	salq	$4, %r9
	addq	%rdi, %r9
	.p2align 4,,10
	.p2align 3
.L2956:
	movdqu	(%rdx), %xmm0
	movdqu	(%rax), %xmm1
	addq	$16, %rdx
	subq	$16, %rax
	addq	$16, %rcx
	shufpd	$1, %xmm1, %xmm1
	shufpd	$1, %xmm0, %xmm0
	movups	%xmm1, -16(%rcx)
	movups	%xmm0, 16(%rax)
	cmpq	%rdx, %r9
	jne	.L2956
	movq	%r8, %rcx
	andq	$-2, %rcx
	movq	%rcx, %rax
	leaq	(%rdi,%rcx,8), %rdx
	negq	%rax
	leaq	(%rsi,%rax,8), %rax
	cmpq	%rcx, %r8
	je	.L2954
	movq	(%rdx), %rcx
	movq	(%rax), %rsi
	movq	%rsi, (%rdx)
	movq	%rcx, (%rax)
.L2954:
	testq	%r13, %r13
	je	.L2963
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2963
	movq	%r13, %rsi
	call	_ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE@PLT
	movq	%rax, %rdx
	testb	$1, %al
	jne	.L2961
	movq	%rax, %rcx
	shrq	$31, %rax
	shrq	%rcx
	movzwl	%ax, %eax
	andl	$1073741823, %ecx
	orl	%eax, %ecx
	jne	.L2961
	.p2align 4,,10
	.p2align 3
.L2963:
	movq	80(%r15), %r13
	movq	72(%r15), %r10
	movl	$1, %r11d
	cmpq	%r13, %r10
	je	.L2981
	movq	%r15, -96(%rbp)
	movq	%r10, %r15
	jmp	.L2965
	.p2align 4,,10
	.p2align 3
.L3039:
	movq	208(%rbx), %rax
	testq	%rdi, (%rax,%rdx,8)
	jne	.L2968
.L2969:
	cmpl	$1, 416(%rbx)
	jne	.L2980
	movl	20(%r14), %eax
	movq	392(%rbx), %rdx
	andl	$16777215, %eax
	leaq	(%rdx,%rax,8), %rdx
	movq	72(%rbx), %rax
	subq	64(%rbx), %rax
	sarq	$3, %rax
	movl	%r12d, 4(%rdx)
	movl	%eax, (%rdx)
.L2980:
	subq	$8, %r13
	cmpq	%r13, %r15
	je	.L3038
.L2965:
	movq	-8(%r13), %r14
	movq	72(%rbx), %r12
	movq	%r11, %rdi
	subq	64(%rbx), %r12
	movl	20(%r14), %ecx
	movq	(%r14), %rax
	sarq	$3, %r12
	movl	%ecx, %edx
	salq	%cl, %rdi
	andl	$16777215, %edx
	movq	%rdi, %rcx
	shrq	$6, %rdx
	cmpw	$56, 16(%rax)
	je	.L2968
	movzbl	18(%rax), %eax
	andl	$112, %eax
	cmpb	$112, %al
	je	.L3039
.L2968:
	movq	160(%rbx), %rax
	testq	%rcx, (%rax,%rdx,8)
	jne	.L2969
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector9VisitNodeEPNS1_4NodeE
	cmpb	$0, 376(%rbx)
	jne	.L2944
	movq	72(%rbx), %rdx
	movq	64(%rbx), %rsi
	movl	$1, %r11d
	movq	%rdx, %rax
	subq	%rsi, %rax
	sarq	$3, %rax
	cmpl	%eax, %r12d
	je	.L2969
	movslq	%r12d, %rax
	leaq	(%rsi,%rax,8), %rcx
	movq	%rax, -104(%rbp)
	cmpq	%rcx, %rdx
	je	.L2972
	leaq	-8(%rdx), %r8
	cmpq	%r8, %rcx
	jnb	.L2972
	leaq	-9(%rdx), %rdi
	movq	%rcx, -80(%rbp)
	subq	%rcx, %rdi
	movq	%rdi, %r9
	movq	%rdi, -88(%rbp)
	shrq	$4, %r9
	movq	%r9, %r10
	leaq	1(%r9), %rdi
	leaq	1(%rax,%r9), %r9
	notq	%r10
	leaq	(%rsi,%r9,8), %rsi
	leaq	(%rdx,%r10,8), %r10
	cmpq	%rsi, %r10
	setnb	%r9b
	cmpq	%rcx, %rdx
	setbe	%sil
	movl	%r9d, %eax
	orb	%sil, %al
	je	.L2973
	cmpq	$31, -88(%rbp)
	jbe	.L2973
	movq	%rdi, %rsi
	subq	$16, %rdx
	movq	%rcx, %rax
	shrq	%rsi
	salq	$4, %rsi
	addq	%rcx, %rsi
	.p2align 4,,10
	.p2align 3
.L2974:
	movdqu	(%rdx), %xmm1
	movdqu	(%rax), %xmm0
	addq	$16, %rax
	subq	$16, %rdx
	shufpd	$1, %xmm1, %xmm1
	shufpd	$1, %xmm0, %xmm0
	movups	%xmm1, -16(%rax)
	movups	%xmm0, 16(%rdx)
	cmpq	%rax, %rsi
	jne	.L2974
	movq	%rdi, %rsi
	andq	$-2, %rsi
	movq	%rsi, %rax
	leaq	(%rcx,%rsi,8), %rdx
	negq	%rax
	leaq	(%r8,%rax,8), %rax
	cmpq	%rdi, %rsi
	je	.L2972
	movq	(%rdx), %rcx
	movq	(%rax), %rsi
	movq	%rsi, (%rdx)
	movq	%rcx, (%rax)
.L2972:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2969
	movq	%r14, %rsi
	call	_ZNK2v88internal8compiler19SourcePositionTable17GetSourcePositionEPNS1_4NodeE@PLT
	movl	$1, %r11d
	testb	$1, %al
	movq	%rax, %rdx
	jne	.L2978
	movq	%rax, %rcx
	shrq	$31, %rax
	shrq	%rcx
	movzwl	%ax, %eax
	andl	$1073741823, %ecx
	orl	%eax, %ecx
	je	.L2969
.L2978:
	cmpl	$1, 32(%rbx)
	je	.L2979
	movq	(%r14), %rax
	movzwl	16(%rax), %eax
	leal	-14(%rax), %ecx
	cmpw	$1, %cx
	leal	-502(%rax), %ecx
	setbe	%sil
	cmpw	$1, %cx
	setbe	%cl
	orb	%cl, %sil
	jne	.L2979
	cmpw	$49, %ax
	jne	.L2969
.L2979:
	movq	-104(%rbp), %rdi
	movq	64(%rbx), %rax
	movq	(%rax,%rdi,8), %rsi
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence17SetSourcePositionEPKNS1_11InstructionENS0_14SourcePositionE@PLT
	movl	$1, %r11d
	jmp	.L2969
	.p2align 4,,10
	.p2align 3
.L3038:
	movq	-96(%rbp), %r15
.L2981:
	movq	16(%rbx), %rcx
	movslq	4(%r15), %rsi
	movq	16(%rcx), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L3040
	movq	(%rax,%rsi,8), %r12
	movq	72(%rbx), %rax
	subq	64(%rbx), %rax
	sarq	$3, %rax
	movl	%eax, %edx
	cmpl	%eax, -72(%rbp)
	jne	.L2982
	movq	8(%rcx), %rdi
	movl	$48, %esi
	call	_ZN2v88internal4Zone3NewEm
	pushq	$0
	xorl	%edx, %edx
	movl	$17, %esi
	pushq	$0
	movq	%rax, %r13
	movq	%rax, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler11InstructionC1EimPNS1_18InstructionOperandEmS4_mS4_@PLT
	popq	%rax
	movq	72(%rbx), %rsi
	movq	%r13, -64(%rbp)
	popq	%rdx
	cmpq	80(%rbx), %rsi
	je	.L2983
	movq	%r13, (%rsi)
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%rbx)
.L2984:
	subq	64(%rbx), %rdx
	sarq	$3, %rdx
	.p2align 4,,10
	.p2align 3
.L2982:
	movl	-72(%rbp), %eax
	movl	%edx, 112(%r12)
	movl	%eax, 116(%r12)
	movq	$0, 48(%rbx)
.L2944:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3041
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2961:
	.cfi_restore_state
	cmpl	$1, 32(%rbx)
	je	.L2962
	movq	0(%r13), %rax
	movzwl	16(%rax), %eax
	leal	-502(%rax), %ecx
	cmpw	$1, %cx
	leal	-14(%rax), %ecx
	setbe	%sil
	cmpw	$1, %cx
	setbe	%cl
	orb	%cl, %sil
	jne	.L2962
	cmpw	$49, %ax
	jne	.L2963
.L2962:
	movq	64(%rbx), %rax
	movq	16(%rbx), %rdi
	movq	(%rax,%r12,8), %rsi
	call	_ZN2v88internal8compiler19InstructionSequence17SetSourcePositionEPKNS1_11InstructionENS0_14SourcePositionE@PLT
	jmp	.L2963
	.p2align 4,,10
	.p2align 3
.L2973:
	movq	-80(%rbp), %rax
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L2976:
	movq	(%rax), %rcx
	movq	(%rdx), %rsi
	addq	$8, %rax
	subq	$8, %rdx
	movq	%rsi, -8(%rax)
	movq	%rcx, 8(%rdx)
	cmpq	%rax, %rdx
	ja	.L2976
	jmp	.L2972
.L2955:
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L2959:
	movq	(%rdx), %rcx
	movq	(%rax), %rsi
	addq	$8, %rdx
	subq	$8, %rax
	movq	%rsi, -8(%rdx)
	movq	%rcx, 8(%rax)
	cmpq	%rdx, %rax
	ja	.L2959
	jmp	.L2954
.L2983:
	leaq	-64(%rbp), %rdx
	leaq	56(%rbx), %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler11InstructionENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	72(%rbx), %rdx
	jmp	.L2984
.L3041:
	call	__stack_chk_fail@PLT
.L3040:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE25109:
	.size	_ZN2v88internal8compiler19InstructionSelector10VisitBlockEPNS1_10BasicBlockE, .-_ZN2v88internal8compiler19InstructionSelector10VisitBlockEPNS1_10BasicBlockE
	.section	.text._ZN2v88internal8compiler19InstructionSelector18SelectInstructionsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector18SelectInstructionsEv
	.type	_ZN2v88internal8compiler19InstructionSelector18SelectInstructionsEv, @function
_ZN2v88internal8compiler19InstructionSelector18SelectInstructionsEv:
.LFB25007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	40(%rdi), %rbx
	movq	80(%rbx), %r10
	movq	88(%rbx), %r13
	cmpq	%r13, %r10
	je	.L3043
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L3045:
	movq	(%r10), %rax
	cmpq	$0, 40(%rax)
	je	.L3047
	movq	72(%rax), %rsi
	movq	80(%rax), %r9
	cmpq	%r9, %rsi
	jne	.L3049
	jmp	.L3047
	.p2align 4,,10
	.p2align 3
.L3052:
	addq	$8, %rsi
	cmpq	%rsi, %r9
	je	.L3047
.L3049:
	movq	(%rsi), %rax
	movq	(%rax), %rdx
	cmpw	$35, 16(%rdx)
	jne	.L3052
	movzbl	23(%rax), %ecx
	leaq	32(%rax), %rdx
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L3051
	movq	32(%rax), %rdx
	movl	8(%rdx), %ecx
	addq	$16, %rdx
.L3051:
	movslq	%ecx, %rcx
	leaq	(%rdx,%rcx,8), %rdi
	cmpq	%rdi, %rdx
	je	.L3052
	.p2align 4,,10
	.p2align 3
.L3053:
	movq	(%rdx), %rax
	movq	%r8, %r11
	addq	$8, %rdx
	movl	20(%rax), %ecx
	movq	%rcx, %rax
	salq	%cl, %r11
	shrq	$3, %rax
	andl	$2097144, %eax
	addq	208(%r12), %rax
	orq	%r11, (%rax)
	cmpq	%rdx, %rdi
	jne	.L3053
	addq	$8, %rsi
	cmpq	%rsi, %r9
	jne	.L3049
	.p2align 4,,10
	.p2align 3
.L3047:
	addq	$8, %r10
	cmpq	%r10, %r13
	jne	.L3045
	movq	88(%rbx), %r13
	cmpq	80(%rbx), %r13
	jne	.L3055
	jmp	.L3043
	.p2align 4,,10
	.p2align 3
.L3132:
	subq	$8, %r13
	cmpq	%r13, 80(%rbx)
	je	.L3043
.L3055:
	movq	-8(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector10VisitBlockEPNS1_10BasicBlockE
	cmpb	$0, 376(%r12)
	je	.L3132
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3043:
	.cfi_restore_state
	cmpl	$1, 352(%r12)
	je	.L3133
.L3056:
	movq	88(%rbx), %rax
	movq	%r13, -56(%rbp)
	movq	%rax, -72(%rbp)
	cmpq	%r13, %rax
	je	.L3090
	.p2align 4,,10
	.p2align 3
.L3089:
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	movslq	4(%rax), %r8
	movq	%rax, -64(%rbp)
	movq	16(%r12), %rax
	movq	%r8, %rsi
	movq	16(%rax), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %r8
	jnb	.L3134
	movq	(%rax,%r8,8), %r13
	movq	72(%r13), %rdx
	movq	80(%r13), %rcx
	cmpq	%rdx, %rcx
	je	.L3060
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L3068:
	movq	(%rdx,%rbx,8), %r15
	movq	24(%r15), %rsi
	cmpq	32(%r15), %rsi
	je	.L3061
	xorl	%r14d, %r14d
.L3131:
	movq	320(%r12), %rcx
	movq	328(%r12), %r8
.L3067:
	movl	(%rsi,%r14,4), %edi
	movq	%r8, %rax
	subq	%rcx, %rax
	sarq	$2, %rax
	movl	%edi, %edx
	jmp	.L3063
	.p2align 4,,10
	.p2align 3
.L3135:
	movl	(%rcx,%r9,4), %r9d
	cmpl	$-1, %r9d
	je	.L3062
	movl	%r9d, %edx
.L3063:
	movslq	%edx, %r9
	cmpq	%rax, %r9
	jb	.L3135
.L3062:
	cmpl	%edx, %edi
	je	.L3064
	movq	%r14, %rsi
	movq	%r15, %rdi
	addq	$1, %r14
	call	_ZN2v88internal8compiler14PhiInstruction11RenameInputEmi@PLT
	movq	24(%r15), %rsi
	movq	32(%r15), %rax
	subq	%rsi, %rax
	sarq	$2, %rax
	cmpq	%r14, %rax
	ja	.L3131
.L3066:
	movq	72(%r13), %rdx
	movq	80(%r13), %rcx
.L3061:
	movq	%rcx, %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L3068
	movq	-64(%rbp), %rax
	movl	4(%rax), %esi
.L3060:
	cmpl	$1, 352(%r12)
	movslq	116(%r13), %rbx
	movslq	112(%r13), %r14
	je	.L3136
.L3069:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence10StartBlockENS1_9RpoNumberE@PLT
.L3070:
	cmpq	%r14, %rbx
	jne	.L3137
.L3071:
	cmpl	$1, 352(%r12)
	movq	-64(%rbp), %rax
	movl	4(%rax), %esi
	je	.L3138
.L3087:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler19InstructionSequence8EndBlockENS1_9RpoNumberE@PLT
.L3088:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	jne	.L3089
.L3090:
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3064:
	.cfi_restore_state
	movq	32(%r15), %rax
	addq	$1, %r14
	subq	%rsi, %rax
	sarq	$2, %rax
	cmpq	%r14, %rax
	ja	.L3067
	jmp	.L3066
	.p2align 4,,10
	.p2align 3
.L3137:
	leaq	1(%rbx), %rax
	leaq	-1(%r14), %r13
	cmpq	%rax, %r14
	jbe	.L3073
	.p2align 4,,10
	.p2align 3
.L3072:
	movq	64(%r12), %rax
	leaq	0(,%r13,8), %rdi
	movq	(%rax,%r13,8), %r14
	movl	4(%r14), %r10d
	testl	$16776960, %r10d
	je	.L3074
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L3078:
	movzbl	%r10b, %eax
	leaq	5(%rsi,%rax), %rax
	leaq	(%r14,%rax,8), %r8
	movq	(%r8), %rdx
	movl	%edx, %eax
	andl	$7, %eax
	cmpl	$1, %eax
	jne	.L3075
	movq	320(%r12), %r11
	movq	328(%r12), %rcx
	movq	%rdx, %r15
	shrq	$3, %r15
	subq	%r11, %rcx
	movl	%r15d, %eax
	sarq	$2, %rcx
	jmp	.L3077
	.p2align 4,,10
	.p2align 3
.L3139:
	movl	(%r11,%r9,4), %r9d
	cmpl	$-1, %r9d
	je	.L3076
	movl	%r9d, %eax
.L3077:
	movslq	%eax, %r9
	cmpq	%rcx, %r9
	jb	.L3139
.L3076:
	cmpl	%eax, %r15d
	je	.L3075
	movabsq	$-34359738361, %rcx
	andq	%rdx, %rcx
	movl	%eax, %edx
	leaq	0(,%rdx,8), %rax
	orq	%rcx, %rax
	movq	%rax, (%r8)
	movl	4(%r14), %r10d
.L3075:
	movl	%r10d, %eax
	addq	$1, %rsi
	shrl	$8, %eax
	movzwl	%ax, %eax
	cmpq	%rsi, %rax
	ja	.L3078
	movq	64(%r12), %rax
	movq	(%rax,%rdi), %r14
.L3074:
	cmpl	$1, 352(%r12)
	je	.L3140
.L3079:
	movq	16(%r12), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler19InstructionSequence14AddInstructionEPNS1_11InstructionE@PLT
.L3080:
	subq	$1, %r13
	cmpq	%r13, %rbx
	jne	.L3072
.L3073:
	movq	64(%r12), %rax
	movq	(%rax,%rbx,8), %r13
	movl	4(%r13), %r9d
	testl	$16776960, %r9d
	je	.L3081
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L3085:
	movzbl	%r9b, %eax
	leaq	5(%r8,%rax), %rax
	leaq	0(%r13,%rax,8), %r10
	movq	(%r10), %rdi
	movl	%edi, %eax
	andl	$7, %eax
	cmpl	$1, %eax
	jne	.L3082
	movq	320(%r12), %rsi
	movq	328(%r12), %rcx
	movq	%rdi, %r11
	shrq	$3, %r11
	subq	%rsi, %rcx
	movl	%r11d, %edx
	sarq	$2, %rcx
	jmp	.L3084
	.p2align 4,,10
	.p2align 3
.L3141:
	movl	(%rsi,%rax,4), %eax
	cmpl	$-1, %eax
	je	.L3083
	movl	%eax, %edx
.L3084:
	movslq	%edx, %rax
	cmpq	%rcx, %rax
	jb	.L3141
.L3083:
	cmpl	%edx, %r11d
	je	.L3082
	movabsq	$-34359738361, %rax
	andq	%rax, %rdi
	movl	%edx, %eax
	leaq	0(,%rax,8), %rdx
	orq	%rdi, %rdx
	movq	%rdx, (%r10)
	movl	4(%r13), %r9d
.L3082:
	movl	%r9d, %eax
	addq	$1, %r8
	shrl	$8, %eax
	movzwl	%ax, %eax
	cmpq	%r8, %rax
	ja	.L3085
	movq	64(%r12), %rax
	movq	(%rax,%rbx,8), %r13
.L3081:
	cmpl	$1, 352(%r12)
	je	.L3142
.L3086:
	movq	16(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler19InstructionSequence14AddInstructionEPNS1_11InstructionE@PLT
	jmp	.L3071
	.p2align 4,,10
	.p2align 3
.L3140:
	call	_ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv@PLT
	testb	%al, %al
	je	.L3079
	movq	344(%r12), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler20InstructionScheduler14AddInstructionEPNS1_11InstructionE@PLT
	jmp	.L3080
.L3138:
	movl	%esi, -64(%rbp)
	call	_ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv@PLT
	movl	-64(%rbp), %esi
	testb	%al, %al
	je	.L3087
	movq	344(%r12), %rdi
	call	_ZN2v88internal8compiler20InstructionScheduler8EndBlockENS1_9RpoNumberE@PLT
	jmp	.L3088
.L3136:
	movl	%esi, -76(%rbp)
	call	_ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv@PLT
	movl	-76(%rbp), %esi
	testb	%al, %al
	je	.L3069
	movq	344(%r12), %rdi
	call	_ZN2v88internal8compiler20InstructionScheduler10StartBlockENS1_9RpoNumberE@PLT
	jmp	.L3070
.L3142:
	call	_ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv@PLT
	testb	%al, %al
	je	.L3086
	movq	344(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler20InstructionScheduler13AddTerminatorEPNS1_11InstructionE@PLT
	jmp	.L3071
.L3133:
	call	_ZN2v88internal8compiler20InstructionScheduler18SchedulerSupportedEv@PLT
	testb	%al, %al
	je	.L3130
	movq	(%r12), %r13
	movq	16(%r12), %r14
	movl	$160, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone3NewEm
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler20InstructionSchedulerC1EPNS0_4ZoneEPNS1_19InstructionSequenceE@PLT
	movq	%r15, 344(%r12)
.L3130:
	movq	80(%rbx), %r13
	jmp	.L3056
.L3134:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE25007:
	.size	_ZN2v88internal8compiler19InstructionSelector18SelectInstructionsEv, .-_ZN2v88internal8compiler19InstructionSelector18SelectInstructionsEv
	.section	.text._ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_,"axG",@progbits,_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_,comdat
	.p2align 4
	.weak	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	.type	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_, @function
_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_:
.LFB30320:
	.cfi_startproc
	endbr64
	cmpq	%rdi, %rsi
	je	.L3155
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16(%rdi), %rbx
	subq	$24, %rsp
	cmpq	%rsi, %rbx
	jne	.L3151
	jmp	.L3143
	.p2align 4,,10
	.p2align 3
.L3158:
	cmpq	%rbx, %r12
	je	.L3147
	movl	$16, %eax
	subq	%r12, %rdx
	movq	%r12, %rsi
	movl	%ecx, -52(%rbp)
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
	movl	-52(%rbp), %ecx
.L3147:
	addq	$16, %rbx
	movl	%ecx, (%r12)
	movl	%r13d, 4(%r12)
	movq	%r15, 8(%r12)
	cmpq	%rbx, %r14
	je	.L3143
.L3151:
	movl	4(%rbx), %r13d
	movl	(%rbx), %ecx
	movq	%rbx, %rdx
	movq	8(%rbx), %r15
	cmpl	4(%r12), %r13d
	jl	.L3158
	cmpl	-12(%rbx), %r13d
	jge	.L3149
	leaq	-16(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L3150:
	movdqu	(%rax), %xmm0
	movq	%rax, %rdx
	subq	$16, %rax
	movups	%xmm0, 32(%rax)
	cmpl	4(%rax), %r13d
	jl	.L3150
.L3149:
	addq	$16, %rbx
	movl	%ecx, (%rdx)
	movl	%r13d, 4(%rdx)
	movq	%r15, 8(%rdx)
	cmpq	%rbx, %r14
	jne	.L3151
.L3143:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3155:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE30320:
	.size	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_, .-_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	.section	.text._ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_.isra.0,"axG",@progbits,_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_T2_,comdat
	.p2align 4
	.type	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_.isra.0, @function
_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_.isra.0:
.LFB32597:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	subq	%rdi, %rax
	movq	%rax, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	sarq	$4, %rcx
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -88(%rbp)
	addq	%rax, %rdx
	movq	%rdi, -80(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rcx, -64(%rbp)
	cmpq	$96, %rax
	jle	.L3160
	movq	%rdi, %r13
	movq	%rdx, %r12
	.p2align 4,,10
	.p2align 3
.L3161:
	movq	%r13, %rdi
	addq	$112, %r13
	movq	%r13, %rsi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	%r14, %rax
	subq	%r13, %rax
	cmpq	$96, %rax
	jg	.L3161
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	cmpq	$112, -112(%rbp)
	jle	.L3159
	movl	$7, %ebx
	.p2align 4,,10
	.p2align 3
.L3162:
	leaq	(%rbx,%rbx), %r13
	cmpq	%r13, -64(%rbp)
	jl	.L3203
.L3234:
	movq	%rbx, %rax
	movq	%rbx, %rcx
	movq	-80(%rbp), %rsi
	movq	%r12, -96(%rbp)
	salq	$5, %rax
	salq	$4, %rcx
	movq	-88(%rbp), %rdi
	movq	%rbx, -104(%rbp)
	movq	%rax, -56(%rbp)
	addq	%rsi, %rcx
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L3172:
	movq	-56(%rbp), %rax
	movq	%rbx, %r8
	leaq	(%rsi,%rax), %r12
	cmpq	%rbx, %r12
	jne	.L3164
	jmp	.L3165
	.p2align 4,,10
	.p2align 3
.L3228:
	movdqu	(%r8), %xmm0
	addq	$16, %rdi
	addq	$16, %r8
	movups	%xmm0, -16(%rdi)
	cmpq	%rsi, %rbx
	je	.L3227
.L3168:
	cmpq	%r8, %r12
	je	.L3165
.L3164:
	movl	4(%rsi), %eax
	cmpl	%eax, 4(%r8)
	jl	.L3228
	movdqu	(%rsi), %xmm1
	addq	$16, %rsi
	addq	$16, %rdi
	movups	%xmm1, -16(%rdi)
	cmpq	%rsi, %rbx
	jne	.L3168
.L3227:
	xorl	%r15d, %r15d
	jmp	.L3197
	.p2align 4,,10
	.p2align 3
.L3165:
	movq	%rbx, %r15
	movq	%r8, -72(%rbp)
	subq	%rsi, %r15
	movq	%r15, %rdx
	call	memmove@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rdi
.L3197:
	addq	%r15, %rdi
	movq	%r12, %r15
	subq	%r8, %r15
	cmpq	%r8, %r12
	je	.L3170
	movq	%r15, %rdx
	movq	%r8, %rsi
	call	memmove@PLT
	movq	%rax, %rdi
.L3170:
	movq	%r14, %r9
	addq	%r15, %rdi
	addq	-56(%rbp), %rbx
	subq	%r12, %r9
	movq	%r9, %rax
	sarq	$4, %rax
	cmpq	%rax, %r13
	jg	.L3217
	movq	%r12, %rsi
	jmp	.L3172
	.p2align 4,,10
	.p2align 3
.L3217:
	movq	%r12, %r15
	movq	-104(%rbp), %rbx
	movq	-96(%rbp), %r12
.L3163:
	cmpq	%rbx, %rax
	cmovg	%rbx, %rax
	salq	$4, %rax
	leaq	(%r15,%rax), %rdx
	cmpq	%r15, %rdx
	je	.L3206
	movq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L3178:
	cmpq	%rsi, %r14
	je	.L3174
.L3232:
	movl	4(%r15), %eax
	cmpl	%eax, 4(%rsi)
	jge	.L3175
	movdqu	(%rsi), %xmm2
	addq	$16, %rdi
	addq	$16, %rsi
	movups	%xmm2, -16(%rdi)
	cmpq	%r15, %rdx
	jne	.L3178
.L3177:
	movq	%r14, %r9
	subq	%rsi, %r9
.L3173:
	subq	%r15, %rdx
	addq	%rdx, %rdi
	cmpq	%rsi, %r14
	je	.L3198
	movq	%r9, %rdx
	call	memmove@PLT
.L3198:
	salq	$2, %rbx
	cmpq	%rbx, -64(%rbp)
	jl	.L3207
.L3233:
	movq	%rbx, %rax
	movq	-88(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r13, -96(%rbp)
	salq	$4, %rax
	salq	$4, %rcx
	movq	%r14, -104(%rbp)
	movq	-80(%rbp), %rdi
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	leaq	(%rsi,%rcx), %r15
	movq	%r15, %r13
	leaq	(%rsi,%rax), %r15
	cmpq	%r13, %r15
	je	.L3180
	.p2align 4,,10
	.p2align 3
.L3230:
	movq	%r13, %r14
	.p2align 4,,10
	.p2align 3
.L3184:
	movl	4(%rsi), %eax
	cmpl	%eax, 4(%r14)
	jge	.L3181
	movdqu	(%r14), %xmm4
	addq	$16, %r14
	addq	$16, %rdi
	movups	%xmm4, -16(%rdi)
	cmpq	%r14, %r15
	je	.L3182
	cmpq	%r13, %rsi
	jne	.L3184
.L3182:
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%rsi, %r13
	je	.L3185
.L3200:
	movq	%rdx, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rdi
.L3185:
	addq	%rdx, %rdi
	movq	%r15, %rdx
	subq	%r14, %rdx
	cmpq	%r14, %r15
	je	.L3186
	movq	%r14, %rsi
	movq	%rdx, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %rdx
	addq	-56(%rbp), %r13
	movq	%rax, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	addq	%rdx, %rdi
	sarq	$4, %rax
	cmpq	%rax, %rbx
	jg	.L3229
.L3187:
	movq	-56(%rbp), %rax
	movq	%r15, %rsi
	leaq	(%rsi,%rax), %r15
	cmpq	%r13, %r15
	jne	.L3230
.L3180:
	movq	%r13, %rdx
	movq	%r13, %r14
	subq	%rsi, %rdx
	cmpq	%rsi, %r13
	jne	.L3200
	addq	%rdx, %rdi
	movq	%r13, %r14
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L3186:
	movq	%r12, %rax
	addq	%rdx, %rdi
	addq	-56(%rbp), %r13
	subq	%r14, %rax
	sarq	$4, %rax
	cmpq	%rax, %rbx
	jle	.L3187
	movq	%r14, %r9
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r14
	movq	%r9, %r8
.L3179:
	cmpq	%rax, %r13
	cmovg	%rax, %r13
	salq	$4, %r13
	addq	%r8, %r13
	movq	%r13, %r15
	cmpq	%r13, %r12
	jne	.L3224
	jmp	.L3189
	.p2align 4,,10
	.p2align 3
.L3231:
	movdqu	(%r15), %xmm6
	addq	$16, %r15
	addq	$16, %rdi
	movups	%xmm6, -16(%rdi)
	cmpq	%r15, %r12
	je	.L3189
.L3224:
	cmpq	%r8, %r13
	je	.L3189
.L3192:
	movl	4(%r8), %eax
	cmpl	%eax, 4(%r15)
	jl	.L3231
	movdqu	(%r8), %xmm7
	addq	$16, %r8
	addq	$16, %rdi
	movups	%xmm7, -16(%rdi)
	cmpq	%r8, %r13
	je	.L3189
	cmpq	%r15, %r12
	jne	.L3192
.L3189:
	movq	%r13, %rdx
	subq	%r8, %rdx
	cmpq	%r8, %r13
	je	.L3193
	movq	%r8, %rsi
	movq	%rdx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
.L3193:
	cmpq	%r15, %r12
	je	.L3194
	movq	%r12, %r8
	addq	%rdx, %rdi
	movq	%r15, %rsi
	subq	%r15, %r8
	movq	%r8, %rdx
	call	memmove@PLT
	cmpq	%rbx, -64(%rbp)
	jg	.L3162
.L3159:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3181:
	.cfi_restore_state
	movdqu	(%rsi), %xmm5
	addq	$16, %rsi
	addq	$16, %rdi
	movups	%xmm5, -16(%rdi)
	cmpq	%r13, %rsi
	je	.L3182
	cmpq	%r14, %r15
	jne	.L3184
	jmp	.L3182
	.p2align 4,,10
	.p2align 3
.L3175:
	movdqu	(%r15), %xmm3
	addq	$16, %r15
	addq	$16, %rdi
	movups	%xmm3, -16(%rdi)
	cmpq	%r15, %rdx
	je	.L3177
	cmpq	%rsi, %r14
	jne	.L3232
.L3174:
	subq	%r15, %rdx
	movq	%r15, %rsi
	salq	$2, %rbx
	call	memmove@PLT
	cmpq	%rbx, -64(%rbp)
	jge	.L3233
.L3207:
	movq	-80(%rbp), %rdi
	movq	-64(%rbp), %rax
	movq	-88(%rbp), %r8
	jmp	.L3179
	.p2align 4,,10
	.p2align 3
.L3229:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r14
	movq	%r15, %r8
	jmp	.L3179
	.p2align 4,,10
	.p2align 3
.L3194:
	cmpq	%rbx, -64(%rbp)
	jle	.L3159
	leaq	(%rbx,%rbx), %r13
	cmpq	%r13, -64(%rbp)
	jge	.L3234
.L3203:
	movq	-112(%rbp), %r9
	movq	-88(%rbp), %rdi
	movq	-64(%rbp), %rax
	movq	-80(%rbp), %r15
	jmp	.L3163
	.p2align 4,,10
	.p2align 3
.L3206:
	movq	%r15, %rsi
	jmp	.L3173
	.p2align 4,,10
	.p2align 3
.L3160:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	.cfi_endproc
.LFE32597:
	.size	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_.isra.0, .-_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_.isra.0
	.section	.text._ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_,"axG",@progbits,_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_,comdat
	.p2align 4
	.weak	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	.type	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_, @function
_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_:
.LFB30328:
	.cfi_startproc
	endbr64
	cmpq	%rdi, %rsi
	je	.L3247
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16(%rdi), %rbx
	subq	$24, %rsp
	cmpq	%rsi, %rbx
	jne	.L3243
	jmp	.L3235
	.p2align 4,,10
	.p2align 3
.L3250:
	cmpq	%rbx, %r12
	je	.L3239
	movl	$16, %eax
	subq	%r12, %rdx
	movq	%r12, %rsi
	movl	%ecx, -52(%rbp)
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
	movl	-52(%rbp), %ecx
.L3239:
	addq	$16, %rbx
	movl	%r13d, (%r12)
	movl	%ecx, 4(%r12)
	movq	%r15, 8(%r12)
	cmpq	%rbx, %r14
	je	.L3235
.L3243:
	movl	(%rbx), %r13d
	movl	4(%rbx), %ecx
	movq	%rbx, %rdx
	movq	8(%rbx), %r15
	cmpl	(%r12), %r13d
	jl	.L3250
	leaq	-16(%rbx), %rax
	cmpl	-16(%rbx), %r13d
	jge	.L3241
	.p2align 4,,10
	.p2align 3
.L3242:
	movdqu	(%rax), %xmm0
	movq	%rax, %rdx
	subq	$16, %rax
	movups	%xmm0, 32(%rax)
	cmpl	(%rax), %r13d
	jl	.L3242
.L3241:
	addq	$16, %rbx
	movl	%r13d, (%rdx)
	movl	%ecx, 4(%rdx)
	movq	%r15, 8(%rdx)
	cmpq	%rbx, %r14
	jne	.L3243
.L3235:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3247:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE30328:
	.size	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_, .-_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	.section	.text._ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_,"axG",@progbits,_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_,comdat
	.p2align 4
	.weak	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_
	.type	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_, @function
_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_:
.LFB30331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	subq	%rdi, %rax
	movq	%rax, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	sarq	$4, %rcx
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -88(%rbp)
	addq	%rax, %rdx
	movq	%rdi, -80(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rcx, -64(%rbp)
	cmpq	$96, %rax
	jle	.L3252
	movq	%rdi, %r13
	movq	%rdx, %r12
	.p2align 4,,10
	.p2align 3
.L3253:
	movq	%r13, %rdi
	addq	$112, %r13
	movq	%r13, %rsi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	%r14, %rax
	subq	%r13, %rax
	cmpq	$96, %rax
	jg	.L3253
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	cmpq	$112, -112(%rbp)
	jle	.L3251
	movl	$7, %ebx
	.p2align 4,,10
	.p2align 3
.L3254:
	leaq	(%rbx,%rbx), %r13
	cmpq	%r13, -64(%rbp)
	jl	.L3295
.L3326:
	movq	%rbx, %rax
	movq	%rbx, %rcx
	movq	-80(%rbp), %rsi
	movq	%r12, -96(%rbp)
	salq	$5, %rax
	salq	$4, %rcx
	movq	-88(%rbp), %rdi
	movq	%rbx, -104(%rbp)
	movq	%rax, -56(%rbp)
	addq	%rsi, %rcx
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L3264:
	movq	-56(%rbp), %rax
	movq	%rbx, %r8
	leaq	(%rsi,%rax), %r12
	cmpq	%rbx, %r12
	jne	.L3256
	jmp	.L3257
	.p2align 4,,10
	.p2align 3
.L3320:
	movdqu	(%r8), %xmm0
	addq	$16, %rdi
	addq	$16, %r8
	movups	%xmm0, -16(%rdi)
	cmpq	%rsi, %rbx
	je	.L3319
.L3260:
	cmpq	%r8, %r12
	je	.L3257
.L3256:
	movl	(%rsi), %eax
	cmpl	%eax, (%r8)
	jl	.L3320
	movdqu	(%rsi), %xmm1
	addq	$16, %rsi
	addq	$16, %rdi
	movups	%xmm1, -16(%rdi)
	cmpq	%rsi, %rbx
	jne	.L3260
.L3319:
	xorl	%r15d, %r15d
	jmp	.L3289
	.p2align 4,,10
	.p2align 3
.L3257:
	movq	%rbx, %r15
	movq	%r8, -72(%rbp)
	subq	%rsi, %r15
	movq	%r15, %rdx
	call	memmove@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rdi
.L3289:
	addq	%r15, %rdi
	movq	%r12, %r15
	subq	%r8, %r15
	cmpq	%r8, %r12
	je	.L3262
	movq	%r15, %rdx
	movq	%r8, %rsi
	call	memmove@PLT
	movq	%rax, %rdi
.L3262:
	movq	%r14, %r9
	addq	%r15, %rdi
	addq	-56(%rbp), %rbx
	subq	%r12, %r9
	movq	%r9, %rax
	sarq	$4, %rax
	cmpq	%rax, %r13
	jg	.L3309
	movq	%r12, %rsi
	jmp	.L3264
	.p2align 4,,10
	.p2align 3
.L3309:
	movq	%r12, %r15
	movq	-104(%rbp), %rbx
	movq	-96(%rbp), %r12
.L3255:
	cmpq	%rbx, %rax
	cmovg	%rbx, %rax
	salq	$4, %rax
	leaq	(%r15,%rax), %rdx
	cmpq	%r15, %rdx
	je	.L3298
	movq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L3270:
	cmpq	%rsi, %r14
	je	.L3266
.L3324:
	movl	(%r15), %eax
	cmpl	%eax, (%rsi)
	jge	.L3267
	movdqu	(%rsi), %xmm2
	addq	$16, %rdi
	addq	$16, %rsi
	movups	%xmm2, -16(%rdi)
	cmpq	%r15, %rdx
	jne	.L3270
.L3269:
	movq	%r14, %r9
	subq	%rsi, %r9
.L3265:
	subq	%r15, %rdx
	addq	%rdx, %rdi
	cmpq	%rsi, %r14
	je	.L3290
	movq	%r9, %rdx
	call	memmove@PLT
.L3290:
	salq	$2, %rbx
	cmpq	%rbx, -64(%rbp)
	jl	.L3299
.L3325:
	movq	%rbx, %rax
	movq	-88(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r13, -96(%rbp)
	salq	$4, %rax
	salq	$4, %rcx
	movq	%r14, -104(%rbp)
	movq	-80(%rbp), %rdi
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	leaq	(%rsi,%rcx), %r15
	movq	%r15, %r13
	leaq	(%rsi,%rax), %r15
	cmpq	%r13, %r15
	je	.L3272
	.p2align 4,,10
	.p2align 3
.L3322:
	movq	%r13, %r14
	.p2align 4,,10
	.p2align 3
.L3276:
	movl	(%rsi), %eax
	cmpl	%eax, (%r14)
	jge	.L3273
	movdqu	(%r14), %xmm4
	addq	$16, %r14
	addq	$16, %rdi
	movups	%xmm4, -16(%rdi)
	cmpq	%r14, %r15
	je	.L3274
	cmpq	%r13, %rsi
	jne	.L3276
.L3274:
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%rsi, %r13
	je	.L3277
.L3292:
	movq	%rdx, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rdi
.L3277:
	addq	%rdx, %rdi
	movq	%r15, %rdx
	subq	%r14, %rdx
	cmpq	%r14, %r15
	je	.L3278
	movq	%r14, %rsi
	movq	%rdx, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %rdx
	addq	-56(%rbp), %r13
	movq	%rax, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	addq	%rdx, %rdi
	sarq	$4, %rax
	cmpq	%rax, %rbx
	jg	.L3321
.L3279:
	movq	-56(%rbp), %rax
	movq	%r15, %rsi
	leaq	(%rsi,%rax), %r15
	cmpq	%r13, %r15
	jne	.L3322
.L3272:
	movq	%r13, %rdx
	movq	%r13, %r14
	subq	%rsi, %rdx
	cmpq	%rsi, %r13
	jne	.L3292
	addq	%rdx, %rdi
	movq	%r13, %r14
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L3278:
	movq	%r12, %rax
	addq	%rdx, %rdi
	addq	-56(%rbp), %r13
	subq	%r14, %rax
	sarq	$4, %rax
	cmpq	%rax, %rbx
	jle	.L3279
	movq	%r14, %r9
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r14
	movq	%r9, %r8
.L3271:
	cmpq	%rax, %r13
	cmovg	%rax, %r13
	salq	$4, %r13
	addq	%r8, %r13
	movq	%r13, %r15
	cmpq	%r13, %r12
	jne	.L3316
	jmp	.L3281
	.p2align 4,,10
	.p2align 3
.L3323:
	movdqu	(%r15), %xmm6
	addq	$16, %r15
	addq	$16, %rdi
	movups	%xmm6, -16(%rdi)
	cmpq	%r15, %r12
	je	.L3281
.L3316:
	cmpq	%r8, %r13
	je	.L3281
.L3284:
	movl	(%r8), %eax
	cmpl	%eax, (%r15)
	jl	.L3323
	movdqu	(%r8), %xmm7
	addq	$16, %r8
	addq	$16, %rdi
	movups	%xmm7, -16(%rdi)
	cmpq	%r8, %r13
	je	.L3281
	cmpq	%r15, %r12
	jne	.L3284
.L3281:
	movq	%r13, %rdx
	subq	%r8, %rdx
	cmpq	%r8, %r13
	je	.L3285
	movq	%r8, %rsi
	movq	%rdx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
.L3285:
	cmpq	%r15, %r12
	je	.L3286
	movq	%r12, %r8
	addq	%rdx, %rdi
	movq	%r15, %rsi
	subq	%r15, %r8
	movq	%r8, %rdx
	call	memmove@PLT
	cmpq	%rbx, -64(%rbp)
	jg	.L3254
.L3251:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3273:
	.cfi_restore_state
	movdqu	(%rsi), %xmm5
	addq	$16, %rsi
	addq	$16, %rdi
	movups	%xmm5, -16(%rdi)
	cmpq	%r13, %rsi
	je	.L3274
	cmpq	%r14, %r15
	jne	.L3276
	jmp	.L3274
	.p2align 4,,10
	.p2align 3
.L3267:
	movdqu	(%r15), %xmm3
	addq	$16, %r15
	addq	$16, %rdi
	movups	%xmm3, -16(%rdi)
	cmpq	%r15, %rdx
	je	.L3269
	cmpq	%rsi, %r14
	jne	.L3324
.L3266:
	subq	%r15, %rdx
	movq	%r15, %rsi
	salq	$2, %rbx
	call	memmove@PLT
	cmpq	%rbx, -64(%rbp)
	jge	.L3325
.L3299:
	movq	-80(%rbp), %rdi
	movq	-64(%rbp), %rax
	movq	-88(%rbp), %r8
	jmp	.L3271
	.p2align 4,,10
	.p2align 3
.L3321:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r14
	movq	%r15, %r8
	jmp	.L3271
	.p2align 4,,10
	.p2align 3
.L3286:
	cmpq	%rbx, -64(%rbp)
	jle	.L3251
	leaq	(%rbx,%rbx), %r13
	cmpq	%r13, -64(%rbp)
	jge	.L3326
.L3295:
	movq	-112(%rbp), %r9
	movq	-88(%rbp), %rdi
	movq	-64(%rbp), %rax
	movq	-80(%rbp), %r15
	jmp	.L3255
	.p2align 4,,10
	.p2align 3
.L3298:
	movq	%r15, %rsi
	jmp	.L3265
	.p2align 4,,10
	.p2align 3
.L3252:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	.cfi_endproc
.LFE30331:
	.size	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_, .-_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_
	.section	.text._ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag,"axG",@progbits,_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag
	.type	_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag, @function
_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag:
.LFB30926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	cmpq	%rsi, %rdi
	je	.L3351
	movq	%rdi, %rax
	cmpq	%rsi, %rdx
	je	.L3329
	movq	%rdx, %rax
	movq	%rsi, %r8
	subq	%rsi, %rbx
	movq	%rdi, %r12
	subq	%rdi, %rax
	subq	%rdi, %r8
	addq	%rdi, %rbx
	sarq	$4, %rax
	sarq	$4, %r8
	movq	%rax, %rdx
	subq	%r8, %rdx
	cmpq	%r8, %rdx
	je	.L3352
	.p2align 4,,10
	.p2align 3
.L3333:
	movq	%rax, %rdi
	subq	%r8, %rdi
	cmpq	%r8, %rdi
	jle	.L3334
.L3354:
	cmpq	$1, %r8
	je	.L3353
	movq	%r8, %rcx
	salq	$4, %rcx
	addq	%r12, %rcx
	testq	%rdi, %rdi
	jle	.L3337
	movq	%r12, %rsi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L3338:
	movl	(%rsi), %r11d
	movl	4(%rsi), %r10d
	addq	$1, %rdx
	addq	$16, %rcx
	movq	8(%rsi), %r9
	movdqu	-16(%rcx), %xmm0
	addq	$16, %rsi
	movups	%xmm0, -16(%rsi)
	movl	%r11d, -16(%rcx)
	movl	%r10d, -12(%rcx)
	movq	%r9, -8(%rcx)
	cmpq	%rdx, %rdi
	jne	.L3338
	salq	$4, %rdi
	addq	%rdi, %r12
.L3337:
	cqto
	idivq	%r8
	testq	%rdx, %rdx
	je	.L3351
	movq	%r8, %rax
	subq	%rdx, %r8
	movq	%rax, %rdi
	subq	%r8, %rdi
	cmpq	%r8, %rdi
	jg	.L3354
.L3334:
	movq	%rax, %rcx
	salq	$4, %rcx
	addq	%r12, %rcx
	cmpq	$1, %rdi
	je	.L3355
	movq	%rdi, %rdx
	movq	%rcx, %r12
	salq	$4, %rdx
	subq	%rdx, %r12
	testq	%r8, %r8
	jle	.L3343
	subq	$16, %rcx
	leaq	-16(%r12), %rsi
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L3344:
	movl	(%rsi), %r11d
	movl	4(%rsi), %r10d
	addq	$1, %r9
	subq	$16, %rcx
	movq	8(%rsi), %rdx
	movdqu	16(%rcx), %xmm1
	subq	$16, %rsi
	movups	%xmm1, 16(%rsi)
	movl	%r11d, 16(%rcx)
	movl	%r10d, 20(%rcx)
	movq	%rdx, 24(%rcx)
	cmpq	%r8, %r9
	jne	.L3344
	salq	$4, %r9
	subq	%r9, %r12
.L3343:
	cqto
	idivq	%rdi
	movq	%rdx, %r8
	testq	%rdx, %rdx
	je	.L3351
	movq	%rdi, %rax
	jmp	.L3333
.L3352:
	movq	%rdi, %rax
	movq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L3332:
	movdqu	(%rdx), %xmm2
	movl	(%rax), %r8d
	addq	$16, %rax
	addq	$16, %rdx
	movl	-12(%rax), %edi
	movq	-8(%rax), %rcx
	movups	%xmm2, -16(%rax)
	movl	%r8d, -16(%rdx)
	movl	%edi, -12(%rdx)
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rsi
	jne	.L3332
	movq	%rsi, %rax
.L3329:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3351:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3355:
	.cfi_restore_state
	leaq	-16(%rcx), %rax
	movl	-16(%rcx), %r15d
	movl	-12(%rcx), %r14d
	movq	-8(%rcx), %r13
	cmpq	%rax, %r12
	je	.L3342
	subq	%r12, %rax
	movq	%rcx, %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	subq	%rax, %rdi
	call	memmove@PLT
.L3342:
	movl	%r15d, (%r12)
	movq	%rbx, %rax
	movl	%r14d, 4(%r12)
	movq	%r13, 8(%r12)
	jmp	.L3329
.L3353:
	salq	$4, %rax
	leaq	16(%r12), %rsi
	movl	(%r12), %ecx
	movl	4(%r12), %r15d
	leaq	(%r12,%rax), %r13
	movq	8(%r12), %r14
	cmpq	%r13, %rsi
	je	.L3336
	leaq	-16(%rax), %rdx
	movq	%r12, %rdi
	movl	%ecx, -52(%rbp)
	call	memmove@PLT
	movl	-52(%rbp), %ecx
.L3336:
	movl	%ecx, -16(%r13)
	movq	%rbx, %rax
	movl	%r15d, -12(%r13)
	movq	%r14, -8(%r13)
	jmp	.L3329
	.cfi_endproc
.LFE30926:
	.size	_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag, .-_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag
	.section	.text._ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_,"axG",@progbits,_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_,comdat
	.p2align 4
	.weak	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
	.type	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_, @function
_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_:
.LFB30323:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L3370
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r8, %r8
	je	.L3356
	leaq	(%rcx,%r8), %rax
	movq	%rdi, %r9
	movq	%rcx, %rbx
	cmpq	$2, %rax
	je	.L3373
	movq	%rdx, %r10
	cmpq	%r8, %rcx
	jle	.L3359
	movq	%rcx, %r15
	movq	%rdx, %rax
	movq	%rsi, %r12
	shrq	$63, %r15
	subq	%rsi, %rax
	addq	%rcx, %r15
	sarq	$4, %rax
	sarq	%r15
	movq	%r15, %r11
	salq	$4, %r11
	addq	%rdi, %r11
	.p2align 4,,10
	.p2align 3
.L3361:
	testq	%rax, %rax
	jle	.L3360
.L3374:
	movq	%rax, %rcx
	movl	4(%r11), %edi
	sarq	%rcx
	movq	%rcx, %rdx
	salq	$4, %rdx
	addq	%r12, %rdx
	cmpl	%edi, 4(%rdx)
	jge	.L3367
	subq	%rcx, %rax
	leaq	16(%rdx), %r12
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L3374
.L3360:
	movq	%r12, %r14
	subq	%rsi, %r14
	sarq	$4, %r14
.L3363:
	movq	%r12, %rdx
	movq	%r11, %rdi
	movq	%r10, -80(%rbp)
	subq	%r15, %rbx
	movq	%r9, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r9
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%rax, %rdx
	movq	%rax, -56(%rbp)
	movq	%r11, %rsi
	movq	%r9, %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
	movq	-80(%rbp), %r10
	movq	-56(%rbp), %rax
	movq	%r13, %r8
	addq	$40, %rsp
	movq	%rbx, %rcx
	subq	%r14, %r8
	movq	%r12, %rsi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r10, %rdx
	popq	%r12
	.cfi_restore 12
	movq	%rax, %rdi
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
	.p2align 4,,10
	.p2align 3
.L3359:
	.cfi_restore_state
	movq	%r8, %r14
	movq	%rsi, %rax
	movq	%rdi, %r11
	shrq	$63, %r14
	subq	%rdi, %rax
	addq	%r8, %r14
	sarq	$4, %rax
	sarq	%r14
	movq	%r14, %r12
	salq	$4, %r12
	addq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L3365:
	testq	%rax, %rax
	jle	.L3364
.L3375:
	movq	%rax, %rcx
	sarq	%rcx
	movq	%rcx, %rdx
	salq	$4, %rdx
	addq	%r11, %rdx
	movl	4(%rdx), %edi
	cmpl	%edi, 4(%r12)
	jl	.L3368
	subq	%rcx, %rax
	leaq	16(%rdx), %r11
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L3375
.L3364:
	movq	%r11, %r15
	subq	%r9, %r15
	sarq	$4, %r15
	jmp	.L3363
	.p2align 4,,10
	.p2align 3
.L3368:
	movq	%rcx, %rax
	jmp	.L3365
	.p2align 4,,10
	.p2align 3
.L3367:
	movq	%rcx, %rax
	jmp	.L3361
	.p2align 4,,10
	.p2align 3
.L3373:
	movl	4(%rdi), %eax
	cmpl	%eax, 4(%rsi)
	jge	.L3356
	movq	8(%rdi), %rax
	movq	(%rdi), %rdx
	movdqu	(%rsi), %xmm0
	movups	%xmm0, (%rdi)
	movq	%rdx, (%rsi)
	movq	%rax, 8(%rsi)
.L3356:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3370:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE30323:
	.size	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_, .-_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
	.section	.text._ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_,"axG",@progbits,_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_,comdat
	.p2align 4
	.weak	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	.type	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_, @function
_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_:
.LFB29822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	$224, %rcx
	jle	.L3379
	sarq	$5, %rcx
	movq	%rcx, %rbx
	salq	$4, %rbx
	leaq	(%rdi,%rbx), %r14
	sarq	$4, %rbx
	movq	%r14, %rsi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r13, %rdx
	subq	%r14, %r8
	popq	%rbx
	movq	%r14, %rsi
	movq	%r12, %rdi
	sarq	$4, %r8
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
	.p2align 4,,10
	.p2align 3
.L3379:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	.cfi_endproc
.LFE29822:
	.size	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_, .-_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	.section	.text._ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_,"axG",@progbits,_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_,comdat
	.p2align 4
	.weak	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
	.type	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_, @function
_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_:
.LFB30329:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L3394
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r8, %r8
	je	.L3380
	leaq	(%rcx,%r8), %rax
	movq	%rdi, %r9
	movq	%rcx, %rbx
	cmpq	$2, %rax
	je	.L3397
	movq	%rdx, %r10
	cmpq	%r8, %rcx
	jle	.L3383
	movq	%rcx, %r15
	movq	%rdx, %rax
	movq	%rsi, %r12
	shrq	$63, %r15
	subq	%rsi, %rax
	addq	%rcx, %r15
	sarq	$4, %rax
	sarq	%r15
	movq	%r15, %r11
	salq	$4, %r11
	addq	%rdi, %r11
	.p2align 4,,10
	.p2align 3
.L3385:
	testq	%rax, %rax
	jle	.L3384
.L3398:
	movq	%rax, %rcx
	movl	(%r11), %edi
	sarq	%rcx
	movq	%rcx, %rdx
	salq	$4, %rdx
	addq	%r12, %rdx
	cmpl	%edi, (%rdx)
	jge	.L3391
	subq	%rcx, %rax
	leaq	16(%rdx), %r12
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L3398
.L3384:
	movq	%r12, %r14
	subq	%rsi, %r14
	sarq	$4, %r14
.L3387:
	movq	%r12, %rdx
	movq	%r11, %rdi
	movq	%r10, -80(%rbp)
	subq	%r15, %rbx
	movq	%r9, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r9
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%rax, %rdx
	movq	%rax, -56(%rbp)
	movq	%r11, %rsi
	movq	%r9, %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
	movq	-80(%rbp), %r10
	movq	-56(%rbp), %rax
	movq	%r13, %r8
	addq	$40, %rsp
	movq	%rbx, %rcx
	subq	%r14, %r8
	movq	%r12, %rsi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r10, %rdx
	popq	%r12
	.cfi_restore 12
	movq	%rax, %rdi
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
	.p2align 4,,10
	.p2align 3
.L3383:
	.cfi_restore_state
	movq	%r8, %r14
	movq	%rsi, %rax
	movq	%rdi, %r11
	shrq	$63, %r14
	subq	%rdi, %rax
	addq	%r8, %r14
	sarq	$4, %rax
	sarq	%r14
	movq	%r14, %r12
	salq	$4, %r12
	addq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L3389:
	testq	%rax, %rax
	jle	.L3388
.L3399:
	movq	%rax, %rcx
	sarq	%rcx
	movq	%rcx, %rdx
	salq	$4, %rdx
	addq	%r11, %rdx
	movl	(%rdx), %edi
	cmpl	%edi, (%r12)
	jl	.L3392
	subq	%rcx, %rax
	leaq	16(%rdx), %r11
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L3399
.L3388:
	movq	%r11, %r15
	subq	%r9, %r15
	sarq	$4, %r15
	jmp	.L3387
	.p2align 4,,10
	.p2align 3
.L3392:
	movq	%rcx, %rax
	jmp	.L3389
	.p2align 4,,10
	.p2align 3
.L3391:
	movq	%rcx, %rax
	jmp	.L3385
	.p2align 4,,10
	.p2align 3
.L3397:
	movl	(%rdi), %eax
	cmpl	%eax, (%rsi)
	jge	.L3380
	movq	8(%rdi), %rax
	movq	(%rdi), %rdx
	movdqu	(%rsi), %xmm0
	movups	%xmm0, (%rdi)
	movq	%rdx, (%rsi)
	movq	%rax, 8(%rsi)
.L3380:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3394:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE30329:
	.size	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_, .-_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
	.section	.text._ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_,"axG",@progbits,_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_,comdat
	.p2align 4
	.weak	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	.type	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_, @function
_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_:
.LFB29832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	$224, %rcx
	jle	.L3403
	sarq	$5, %rcx
	movq	%rcx, %rbx
	salq	$4, %rbx
	leaq	(%rdi,%rbx), %r14
	sarq	$4, %rbx
	movq	%r14, %rsi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r13, %rdx
	subq	%r14, %r8
	popq	%rbx
	movq	%r14, %rsi
	movq	%r12, %rdi
	sarq	$4, %r8
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
	.p2align 4,,10
	.p2align 3
.L3403:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	.cfi_endproc
.LFE29832:
	.size	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_, .-_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	.section	.text._ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_,"ax",@progbits
	.p2align 4
	.type	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_, @function
_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_:
.LFB30495:
	.cfi_startproc
	testq	%rcx, %rcx
	je	.L3418
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r8, %r8
	je	.L3404
	leaq	(%rcx,%r8), %rax
	movq	%rdi, %r9
	movq	%rcx, %rbx
	cmpq	$2, %rax
	je	.L3421
	movq	%rdx, %r10
	cmpq	%r8, %rcx
	jle	.L3407
	movq	%rcx, %r15
	movq	%rdx, %rax
	movq	%rsi, %r12
	shrq	$63, %r15
	subq	%rsi, %rax
	addq	%rcx, %r15
	sarq	$4, %rax
	sarq	%r15
	movq	%r15, %r11
	salq	$4, %r11
	addq	%rdi, %r11
	.p2align 4,,10
	.p2align 3
.L3409:
	testq	%rax, %rax
	jle	.L3408
.L3422:
	movq	%rax, %rcx
	movl	(%r11), %edi
	sarq	%rcx
	movq	%rcx, %rdx
	salq	$4, %rdx
	addq	%r12, %rdx
	cmpl	%edi, (%rdx)
	jge	.L3415
	subq	%rcx, %rax
	leaq	16(%rdx), %r12
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L3422
.L3408:
	movq	%r12, %r14
	subq	%rsi, %r14
	sarq	$4, %r14
.L3411:
	movq	%r12, %rdx
	movq	%r11, %rdi
	movq	%r10, -80(%rbp)
	subq	%r15, %rbx
	movq	%r9, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r9
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%rax, %rdx
	movq	%rax, -56(%rbp)
	movq	%r11, %rsi
	movq	%r9, %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	movq	-80(%rbp), %r10
	movq	-56(%rbp), %rax
	movq	%r13, %r8
	addq	$40, %rsp
	movq	%rbx, %rcx
	subq	%r14, %r8
	movq	%r12, %rsi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r10, %rdx
	popq	%r12
	.cfi_restore 12
	movq	%rax, %rdi
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	.p2align 4,,10
	.p2align 3
.L3407:
	.cfi_restore_state
	movq	%r8, %r14
	movq	%rsi, %rax
	movq	%rdi, %r11
	shrq	$63, %r14
	subq	%rdi, %rax
	addq	%r8, %r14
	sarq	$4, %rax
	sarq	%r14
	movq	%r14, %r12
	salq	$4, %r12
	addq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L3413:
	testq	%rax, %rax
	jle	.L3412
.L3423:
	movq	%rax, %rcx
	sarq	%rcx
	movq	%rcx, %rdx
	salq	$4, %rdx
	addq	%r11, %rdx
	movl	(%rdx), %edi
	cmpl	%edi, (%r12)
	jl	.L3416
	subq	%rcx, %rax
	leaq	16(%rdx), %r11
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L3423
.L3412:
	movq	%r11, %r15
	subq	%r9, %r15
	sarq	$4, %r15
	jmp	.L3411
	.p2align 4,,10
	.p2align 3
.L3416:
	movq	%rcx, %rax
	jmp	.L3413
	.p2align 4,,10
	.p2align 3
.L3415:
	movq	%rcx, %rax
	jmp	.L3409
	.p2align 4,,10
	.p2align 3
.L3421:
	movl	(%rdi), %eax
	cmpl	%eax, (%rsi)
	jge	.L3404
	movq	8(%rdi), %rax
	movq	(%rdi), %rdx
	movdqu	(%rsi), %xmm0
	movups	%xmm0, (%rdi)
	movq	%rdx, (%rsi)
	movq	%rax, 8(%rsi)
.L3404:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3418:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE30495:
	.size	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_, .-_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	.section	.text._ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_,"ax",@progbits
	.p2align 4
	.type	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_, @function
_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_:
.LFB30035:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpq	$224, %rcx
	jle	.L3427
	sarq	$5, %rcx
	movq	%rcx, %rbx
	salq	$4, %rbx
	leaq	(%rdi,%rbx), %r14
	sarq	$4, %rbx
	movq	%r14, %rsi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r13, %rdx
	subq	%r14, %r8
	popq	%rbx
	movq	%r14, %rsi
	movq	%r12, %rdi
	sarq	$4, %r8
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	.p2align 4,,10
	.p2align 3
.L3427:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	.cfi_endproc
.LFE30035:
	.size	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_, .-_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	.section	.text._ZSt17__rotate_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lET_SB_SB_SB_T1_SC_T0_SC_,"axG",@progbits,_ZSt17__rotate_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lET_SB_SB_SB_T1_SC_T0_SC_,comdat
	.p2align 4
	.weak	_ZSt17__rotate_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lET_SB_SB_SB_T1_SC_T0_SC_
	.type	_ZSt17__rotate_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lET_SB_SB_SB_T1_SC_T0_SC_, @function
_ZSt17__rotate_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lET_SB_SB_SB_T1_SC_T0_SC_:
.LFB30706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	cmpq	%r8, %rcx
	jle	.L3429
	cmpq	%rax, %r8
	jg	.L3429
	movq	%rdi, %rax
	testq	%r8, %r8
	je	.L3434
	movq	%rdx, %rbx
	subq	%rsi, %rbx
	cmpq	%rsi, %rdx
	je	.L3431
	movq	%rbx, %rdx
	movq	%r9, %rdi
	call	memmove@PLT
.L3431:
	cmpq	%r12, %r13
	je	.L3432
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	subq	%r13, %rdx
	subq	%rdx, %rdi
	call	memmove@PLT
.L3432:
	testq	%rbx, %rbx
	je	.L3433
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	memmove@PLT
.L3433:
	leaq	0(%r13,%rbx), %rax
.L3434:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3429:
	.cfi_restore_state
	cmpq	%rax, %rcx
	jg	.L3435
	movq	%r14, %rax
	testq	%rcx, %rcx
	je	.L3434
	movq	%r12, %rbx
	subq	%r13, %rbx
	cmpq	%r12, %r13
	je	.L3437
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	memmove@PLT
.L3437:
	cmpq	%r12, %r14
	je	.L3438
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	subq	%r12, %rdx
	call	memmove@PLT
.L3438:
	subq	%rbx, %r14
	testq	%rbx, %rbx
	je	.L3439
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	memmove@PLT
.L3439:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3435:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag
	.cfi_endproc
.LFE30706:
	.size	_ZSt17__rotate_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lET_SB_SB_SB_T1_SC_T0_SC_, .-_ZSt17__rotate_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lET_SB_SB_SB_T1_SC_T0_SC_
	.section	.text._ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_SE_T2_,"axG",@progbits,_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_SE_T2_,comdat
	.p2align 4
	.weak	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_SE_T2_
	.type	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_SE_T2_, @function
_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_SE_T2_:
.LFB30326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$56, %rsp
	movq	16(%rbp), %r15
	cmpq	%r15, %r8
	movq	%r15, %rax
	cmovle	%r8, %rax
	cmpq	%rax, %rcx
	jg	.L3456
	movq	%rsi, %rbx
	movq	%rsi, %r8
	subq	%rdi, %rbx
	cmpq	%rdi, %rsi
	je	.L3457
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rdx
	movq	%rdi, %rsi
	movq	%r9, %rdi
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L3457:
	leaq	0(%r13,%rbx), %rdx
	cmpq	%r13, %rdx
	je	.L3455
	.p2align 4,,10
	.p2align 3
.L3464:
	cmpq	%r8, %r14
	je	.L3460
.L3488:
	movl	4(%r13), %eax
	cmpl	%eax, 4(%r8)
	jge	.L3461
	movdqu	(%r8), %xmm0
	addq	$16, %r12
	addq	$16, %r8
	movups	%xmm0, -16(%r12)
	cmpq	%r13, %rdx
	jne	.L3464
.L3455:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3461:
	.cfi_restore_state
	movdqu	0(%r13), %xmm1
	addq	$16, %r13
	addq	$16, %r12
	movups	%xmm1, -16(%r12)
	cmpq	%r13, %rdx
	je	.L3455
	cmpq	%r8, %r14
	jne	.L3488
.L3460:
	subq	%r13, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
.L3486:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L3456:
	.cfi_restore_state
	cmpq	%r15, %r8
	jg	.L3466
	movq	%rdx, %r15
	subq	%rsi, %r15
	cmpq	%rsi, %rdx
	je	.L3467
	movq	%r15, %rdx
	movq	%r9, %rdi
	movq	%rsi, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rsi
.L3467:
	leaq	0(%r13,%r15), %rax
	cmpq	%r12, %rsi
	je	.L3489
	cmpq	%rax, %r13
	je	.L3455
	subq	$16, %rsi
	subq	$16, %rax
	subq	$16, %r14
	jmp	.L3470
	.p2align 4,,10
	.p2align 3
.L3491:
	movdqu	(%rsi), %xmm2
	movups	%xmm2, (%r14)
	cmpq	%r12, %rsi
	je	.L3490
	subq	$16, %rsi
.L3473:
	subq	$16, %r14
.L3470:
	movl	4(%rsi), %ebx
	cmpl	%ebx, 4(%rax)
	jl	.L3491
	movdqu	(%rax), %xmm3
	movups	%xmm3, (%r14)
	cmpq	%rax, %r13
	je	.L3455
	subq	$16, %rax
	jmp	.L3473
	.p2align 4,,10
	.p2align 3
.L3466:
	cmpq	%r8, %rcx
	jle	.L3474
	movq	%rcx, %rax
	movq	%rsi, %r10
	shrq	$63, %rax
	addq	%rcx, %rax
	sarq	%rax
	movq	%rax, %r11
	movq	%rax, -56(%rbp)
	movq	%rdx, %rax
	salq	$4, %r11
	subq	%rsi, %rax
	addq	%rdi, %r11
	sarq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L3476:
	testq	%rax, %rax
	jle	.L3475
.L3492:
	movq	%rax, %rdi
	movl	4(%r11), %r9d
	sarq	%rdi
	movq	%rdi, %rdx
	salq	$4, %rdx
	addq	%r10, %rdx
	cmpl	%r9d, 4(%rdx)
	jge	.L3482
	subq	%rdi, %rax
	leaq	16(%rdx), %r10
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L3492
.L3475:
	movq	%r10, %r8
	subq	%rsi, %r8
	sarq	$4, %r8
.L3478:
	subq	$8, %rsp
	movq	%rcx, %rax
	subq	-56(%rbp), %rax
	movq	%r10, %rdx
	pushq	%r15
	movq	%r13, %r9
	movq	%r11, %rdi
	movq	%rax, %rcx
	movq	%r10, -88(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r11, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZSt17__rotate_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lET_SB_SB_SB_T1_SC_T0_SC_
	movq	-80(%rbp), %r11
	movq	-56(%rbp), %rcx
	movq	%r13, %r9
	movq	-72(%rbp), %r8
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	%r15, (%rsp)
	movq	%r11, %rsi
	movq	%rax, -56(%rbp)
	call	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_SE_T2_
	movq	-72(%rbp), %r8
	movq	-88(%rbp), %r10
	movq	%r13, %r9
	popq	%rax
	movq	-56(%rbp), %rax
	movq	%r15, 16(%rbp)
	movq	-64(%rbp), %rcx
	popq	%rdx
	subq	%r8, %rbx
	leaq	-40(%rbp), %rsp
	movq	%rbx, %r8
	movq	%r14, %rdx
	popq	%rbx
	movq	%r10, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_SE_T2_
	.p2align 4,,10
	.p2align 3
.L3474:
	.cfi_restore_state
	shrq	$63, %r8
	movq	%rsi, %rax
	movq	%rdi, %r11
	addq	%rbx, %r8
	subq	%rdi, %rax
	sarq	%r8
	sarq	$4, %rax
	movq	%r8, %r10
	salq	$4, %r10
	addq	%rsi, %r10
	.p2align 4,,10
	.p2align 3
.L3480:
	testq	%rax, %rax
	jle	.L3479
.L3493:
	movq	%rax, %rdi
	sarq	%rdi
	movq	%rdi, %rdx
	salq	$4, %rdx
	addq	%r11, %rdx
	movl	4(%rdx), %r9d
	cmpl	%r9d, 4(%r10)
	jl	.L3483
	subq	%rdi, %rax
	leaq	16(%rdx), %r11
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L3493
.L3479:
	movq	%r11, %rax
	subq	%r12, %rax
	sarq	$4, %rax
	movq	%rax, -56(%rbp)
	jmp	.L3478
	.p2align 4,,10
	.p2align 3
.L3482:
	movq	%rdi, %rax
	jmp	.L3476
	.p2align 4,,10
	.p2align 3
.L3483:
	movq	%rdi, %rax
	jmp	.L3480
	.p2align 4,,10
	.p2align 3
.L3490:
	addq	$16, %rax
	cmpq	%rax, %r13
	je	.L3455
	subq	%r13, %rax
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	subq	%rax, %rdi
	jmp	.L3486
	.p2align 4,,10
	.p2align 3
.L3489:
	cmpq	%rax, %r13
	je	.L3455
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	subq	%r15, %rdi
	jmp	.L3486
	.cfi_endproc
.LFE30326:
	.size	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_SE_T2_, .-_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_SE_T2_
	.section	.text._ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_T2_,"axG",@progbits,_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_T2_
	.type	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_T2_, @function
_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_T2_:
.LFB29824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	subq	%rdi, %rax
	sarq	$4, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	1(%rax), %rsi
	movq	%rsi, %rax
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	shrq	$63, %rax
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	addq	%rsi, %rax
	sarq	%rax
	movq	%rax, %r9
	subq	$24, %rsp
	salq	$4, %r9
	movq	%r9, -56(%rbp)
	leaq	(%rdi,%r9), %r15
	cmpq	%rcx, %rax
	jle	.L3495
	movq	%r15, %rsi
	call	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_T2_
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_T2_
	movq	-56(%rbp), %r9
.L3496:
	subq	$8, %rsp
	movq	%r13, %r8
	movq	%r9, %rcx
	movq	%r13, %rdx
	pushq	%r14
	subq	%r15, %r8
	movq	%rbx, %r9
	movq	%r15, %rsi
	sarq	$4, %rcx
	movq	%r12, %rdi
	sarq	$4, %r8
	call	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_SE_T2_
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3495:
	.cfi_restore_state
	movq	%r15, %rsi
	call	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_.isra.0
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_.isra.0
	movq	-56(%rbp), %r9
	jmp	.L3496
	.cfi_endproc
.LFE29824:
	.size	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_T2_, .-_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_T2_
	.section	.text._ZN2v88internal8compiler19InstructionSelector16EmitLookupSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector16EmitLookupSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE
	.type	_ZN2v88internal8compiler19InstructionSelector16EmitLookupSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE, @function
_ZN2v88internal8compiler19InstructionSelector16EmitLookupSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE:
.LFB25155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	16(%rax), %rdx
	movq	8(%rax), %rsi
	movq	%rdx, %rbx
	subq	%rsi, %rbx
	movq	%rbx, %rax
	sarq	$4, %rax
	movq	%rax, -96(%rbp)
	movq	%rax, %rcx
	movq	%rax, %r14
	movabsq	$576460752303423487, %rax
	cmpq	%rax, %rcx
	ja	.L3569
	cmpq	$0, -96(%rbp)
	movq	%rdi, %r12
	je	.L3500
	movq	%rbx, %rdi
	movq	%rsi, -120(%rbp)
	movq	%rdx, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movq	%rax, -112(%rbp)
	addq	%rbx, %rax
	movq	%rax, -88(%rbp)
	cmpq	%rsi, %rdx
	jne	.L3557
.L3554:
	leaq	_ZSt7nothrow(%rip), %rdx
.L3505:
	movq	%r14, %rdi
	movq	%rdx, %rsi
	salq	$4, %rdi
	call	_ZnwmRKSt9nothrow_t@PLT
	leaq	_ZSt7nothrow(%rip), %rdx
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L3503
	movq	-88(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	%r14, %rcx
	movq	%rax, %rdx
	call	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_less_iterEEvT_SD_T0_T1_T2_
.L3504:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	movq	0(%r13), %rax
	movq	(%r12), %rdi
	movq	16(%rax), %rsi
	subq	8(%rax), %rsi
	movq	%rsi, %rax
	addq	$23, %rsi
	sarq	$4, %rax
	andq	$-8, %rsi
	leaq	2(%rax,%rax), %rax
	movq	%rax, -120(%rbp)
	movq	16(%rdi), %rax
	movq	%rax, %rcx
	movq	%rax, -104(%rbp)
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L3570
	addq	-104(%rbp), %rsi
	movq	%rsi, 16(%rdi)
.L3538:
	movq	-128(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	24(%r13), %rdx
	movq	16(%r12), %rax
	movslq	4(%rdx), %rdx
	movl	$7, -80(%rbp)
	movb	$19, -76(%rbp)
	movq	%rdx, -72(%rbp)
	movq	160(%rax), %rsi
	movq	%rsi, %r13
	subq	152(%rax), %r13
	sarq	$4, %r13
	cmpq	168(%rax), %rsi
	je	.L3539
	movl	$7, (%rsi)
	movb	$19, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rax)
	movq	%rcx, %rax
.L3540:
	salq	$32, %r13
	orq	$11, %r13
	cmpq	$0, -96(%rbp)
	movq	%r13, 8(%rax)
	je	.L3550
	leaq	24(%rax,%rbx), %rcx
	movq	-112(%rbp), %r13
	leaq	24(%rax), %r14
	movq	%rcx, -96(%rbp)
	leaq	-80(%rbp), %r15
	subq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L3551:
	movq	16(%r12), %rdx
	movl	-24(%r13,%r14), %esi
	movq	%r15, %rdi
	leaq	-8(%r14), %rbx
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %ecx
	movq	-88(%rbp), %rdx
	testl	%ecx, %ecx
	jne	.L3544
	cmpb	$19, -76(%rbp)
	je	.L3571
.L3544:
	movq	160(%rdx), %rsi
	movq	%rsi, %rax
	subq	152(%rdx), %rax
	sarq	$4, %rax
	cmpq	168(%rdx), %rsi
	je	.L3546
	movzbl	-76(%rbp), %r8d
	movq	-72(%rbp), %rdi
	movl	%ecx, (%rsi)
	movb	%r8b, 4(%rsi)
	movq	%rdi, 8(%rsi)
	addq	$16, 160(%rdx)
.L3547:
	salq	$32, %rax
	orq	$11, %rax
.L3545:
	movq	%rax, (%rbx)
	movq	-16(%r13,%r14), %rax
	movq	16(%r12), %rdi
	movslq	4(%rax), %rax
	movl	$7, -80(%rbp)
	movb	$19, -76(%rbp)
	movq	%rax, -72(%rbp)
	movq	160(%rdi), %rsi
	movq	%rsi, %rbx
	subq	152(%rdi), %rbx
	sarq	$4, %rbx
	cmpq	168(%rdi), %rsi
	je	.L3548
	salq	$32, %rbx
	movl	$7, (%rsi)
	addq	$16, %r14
	orq	$11, %rbx
	movb	$19, 4(%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, 160(%rdi)
	movq	%rbx, -16(%r14)
	cmpq	-96(%rbp), %r14
	jne	.L3551
.L3550:
	cmpq	$65534, -120(%rbp)
	ja	.L3572
.L3566:
	movq	-104(%rbp), %r8
	movq	-120(%rbp), %rcx
	xorl	%edx, %edx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0
.L3552:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L3498
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3498:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3573
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3500:
	.cfi_restore_state
	movq	%rbx, -88(%rbp)
	movq	$0, -112(%rbp)
	cmpq	%rsi, %rdx
	je	.L3574
.L3557:
	movq	-112(%rbp), %rdi
	movq	%rbx, %rdx
	call	memcpy@PLT
	testq	%rbx, %rbx
	jne	.L3554
	jmp	.L3502
	.p2align 4,,10
	.p2align 3
.L3548:
	salq	$32, %rbx
	addq	$144, %rdi
	movq	%r15, %rdx
	addq	$16, %r14
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	orq	$11, %rbx
	movq	%rbx, -16(%r14)
	cmpq	%r14, -96(%rbp)
	jne	.L3551
	cmpq	$65534, -120(%rbp)
	jbe	.L3566
.L3572:
	movb	$1, 376(%r12)
	jmp	.L3552
	.p2align 4,,10
	.p2align 3
.L3571:
	movslq	-72(%rbp), %rax
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L3545
	.p2align 4,,10
	.p2align 3
.L3546:
	leaq	144(%rdx), %rdi
	movq	%r15, %rdx
	movq	%rax, -88(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-88(%rbp), %rax
	jmp	.L3547
.L3574:
	movq	%rcx, -112(%rbp)
	cmpq	$224, %rbx
	jle	.L3555
	movq	$0, -104(%rbp)
	xorl	%r14d, %r14d
.L3556:
	movq	-112(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
.L3510:
	movq	-88(%rbp), %rax
	subq	%r14, %rax
	movq	%rax, %rcx
	sarq	$4, %rcx
	movq	%rcx, -120(%rbp)
	cmpq	$224, %rax
	jle	.L3575
	movq	%rcx, %rax
	shrq	$63, %rax
	addq	%rcx, %rax
	sarq	%rax
	salq	$4, %rax
	movq	%rax, %rcx
	leaq	(%r14,%rax), %r15
	sarq	$4, %rcx
	movq	%rcx, -136(%rbp)
	cmpq	$224, %rax
	jle	.L3576
	movq	%rcx, %r10
	shrq	$63, %r10
	addq	%rcx, %r10
	sarq	%r10
	salq	$4, %r10
	movq	%r10, %rcx
	leaq	(%r14,%r10), %r9
	sarq	$4, %rcx
	movq	%rcx, -144(%rbp)
	cmpq	$224, %r10
	jle	.L3577
	movq	%rax, -184(%rbp)
	movq	-144(%rbp), %rax
	movq	%r14, %rdi
	movq	%r10, -192(%rbp)
	movq	%rax, %rcx
	movq	%r9, -152(%rbp)
	shrq	$63, %rcx
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r14,%rcx), %r11
	movq	%rcx, -176(%rbp)
	movq	%r11, %rsi
	movq	%r11, -160(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-160(%rbp), %r11
	movq	-152(%rbp), %r9
	movq	%r11, %rdi
	movq	%r9, %rsi
	movq	%r11, -168(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-192(%rbp), %r10
	movq	%r14, %rdi
	movq	-176(%rbp), %rcx
	movq	-152(%rbp), %r9
	movq	-168(%rbp), %r11
	movq	%r10, %r8
	movq	%r10, -160(%rbp)
	subq	%rcx, %r8
	movq	%r9, %rdx
	sarq	$4, %rcx
	movq	%r11, %rsi
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
	movq	-184(%rbp), %rax
	movq	-160(%rbp), %r10
	movq	-152(%rbp), %r9
.L3528:
	subq	%r10, %rax
	movq	%rax, %r10
	sarq	$4, %r10
	cmpq	$224, %rax
	jle	.L3578
	movq	%r10, %rcx
	movq	%r9, %rdi
	movq	%r10, -176(%rbp)
	shrq	$63, %rcx
	movq	%r9, -160(%rbp)
	addq	%r10, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r9,%rcx), %r11
	movq	%rcx, -168(%rbp)
	movq	%r11, %rsi
	movq	%r11, -152(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-152(%rbp), %r11
	movq	%r15, %rsi
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-152(%rbp), %r11
	movq	%r15, %r8
	movq	%r15, %rdx
	movq	-160(%rbp), %r9
	movq	-168(%rbp), %rcx
	subq	%r11, %r8
	movq	%r11, %rsi
	movq	%r9, %rdi
	sarq	$4, %rcx
	movq	%r9, -152(%rbp)
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
	movq	-176(%rbp), %r10
	movq	-152(%rbp), %r9
.L3530:
	movq	-144(%rbp), %rcx
	movq	%r10, %r8
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
.L3526:
	movq	-88(%rbp), %rax
	subq	%r15, %rax
	movq	%rax, %rcx
	sarq	$4, %rcx
	movq	%rcx, -144(%rbp)
	cmpq	$224, %rax
	jle	.L3579
	movq	%rcx, %r8
	shrq	$63, %r8
	addq	%rcx, %r8
	sarq	%r8
	salq	$4, %r8
	movq	%r8, %rax
	leaq	(%r15,%r8), %r10
	sarq	$4, %rax
	movq	%rax, -152(%rbp)
	cmpq	$224, %r8
	jle	.L3580
	movq	%rax, %rcx
	movq	%r15, %rdi
	movq	%r8, -184(%rbp)
	shrq	$63, %rcx
	movq	%r10, -160(%rbp)
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r15,%rcx), %r11
	movq	%rcx, -176(%rbp)
	movq	%r11, %rsi
	movq	%r11, -168(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-160(%rbp), %r10
	movq	-168(%rbp), %r11
	movq	%r10, %rsi
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-176(%rbp), %rcx
	movq	%r15, %rdi
	movq	-184(%rbp), %r8
	movq	-160(%rbp), %r10
	movq	-168(%rbp), %r11
	subq	%rcx, %r8
	sarq	$4, %rcx
	movq	%r10, %rdx
	sarq	$4, %r8
	movq	%r11, %rsi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
	movq	-160(%rbp), %r10
.L3534:
	movq	-88(%rbp), %rdx
	subq	%r10, %rdx
	movq	%rdx, %rax
	sarq	$4, %rax
	cmpq	$224, %rdx
	jle	.L3581
	movq	%rax, %rcx
	movq	%r10, %rdi
	movq	%rax, -184(%rbp)
	shrq	$63, %rcx
	movq	%r10, -168(%rbp)
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r10,%rcx), %r11
	movq	%rcx, -176(%rbp)
	movq	%r11, %rsi
	movq	%r11, -160(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-160(%rbp), %r11
	movq	-88(%rbp), %rsi
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-88(%rbp), %rdx
	movq	-160(%rbp), %r11
	movq	-168(%rbp), %r10
	movq	-176(%rbp), %rcx
	movq	%rdx, %r8
	movq	%r11, %rsi
	subq	%r11, %r8
	movq	%r10, %rdi
	sarq	$4, %rcx
	movq	%r10, -160(%rbp)
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
	movq	-184(%rbp), %rax
	movq	-160(%rbp), %r10
.L3536:
	movq	-152(%rbp), %rcx
	movq	%rax, %r8
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	-88(%rbp), %rdx
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
.L3532:
	movq	-144(%rbp), %r8
	movq	-136(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	-88(%rbp), %rdx
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
.L3524:
	movq	-120(%rbp), %r8
	movq	-104(%rbp), %rcx
	movq	%r14, %rsi
	movq	-88(%rbp), %rdx
	movq	-112(%rbp), %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
	jmp	.L3508
.L3555:
	movq	-88(%rbp), %rsi
	movq	-112(%rbp), %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
.L3508:
	xorl	%r15d, %r15d
	jmp	.L3504
	.p2align 4,,10
	.p2align 3
.L3539:
	leaq	-80(%rbp), %r15
	leaq	144(%rax), %rdi
	movq	%r15, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-104(%rbp), %rax
	jmp	.L3540
	.p2align 4,,10
	.p2align 3
.L3570:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %rcx
	jmp	.L3538
.L3575:
	movq	-88(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	jmp	.L3524
.L3579:
	movq	-88(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	jmp	.L3532
.L3576:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	jmp	.L3526
.L3578:
	movq	%r9, %rdi
	movq	%r15, %rsi
	movq	%r10, -160(%rbp)
	movq	%r9, -152(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %r10
	jmp	.L3530
.L3577:
	movq	%r9, %rsi
	movq	%r14, %rdi
	movq	%r10, -168(%rbp)
	movq	%rax, -160(%rbp)
	movq	%r9, -152(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %r10
	jmp	.L3528
.L3581:
	movq	-88(%rbp), %rsi
	movq	%r10, %rdi
	movq	%rax, -168(%rbp)
	movq	%r10, -160(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-160(%rbp), %r10
	movq	-168(%rbp), %rax
	jmp	.L3536
.L3580:
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	%r10, -160(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-160(%rbp), %r10
	jmp	.L3534
.L3503:
	sarq	%r14
	jne	.L3505
.L3502:
	movq	-88(%rbp), %rax
	subq	-112(%rbp), %rax
	movq	%rax, %rdx
	sarq	$4, %rdx
	cmpq	$224, %rax
	jle	.L3555
	movq	%rdx, %rax
	movq	-112(%rbp), %rcx
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	salq	$4, %rax
	leaq	(%rcx,%rax), %r14
	movq	%rax, %rcx
	sarq	$4, %rcx
	movq	%rcx, -104(%rbp)
	cmpq	$224, %rax
	jle	.L3556
	movq	%rcx, %rdx
	movq	-112(%rbp), %r15
	shrq	$63, %rdx
	addq	%rcx, %rdx
	sarq	%rdx
	salq	$4, %rdx
	movq	%rdx, -120(%rbp)
	movq	%rdx, %rcx
	addq	%rdx, %r15
	sarq	$4, %rdx
	movq	%rdx, -136(%rbp)
	cmpq	$224, %rcx
	jle	.L3582
	movq	%rdx, %r9
	movq	-112(%rbp), %rcx
	shrq	$63, %r9
	addq	%rdx, %r9
	sarq	%r9
	salq	$4, %r9
	leaq	(%rcx,%r9), %r10
	movq	%r9, %rcx
	sarq	$4, %rcx
	movq	%rcx, -144(%rbp)
	cmpq	$224, %r9
	jle	.L3583
	movq	%rax, -184(%rbp)
	movq	-144(%rbp), %rax
	movq	%r9, -192(%rbp)
	movq	%rax, %rcx
	movq	%r10, -152(%rbp)
	shrq	$63, %rcx
	addq	%rax, %rcx
	movq	-112(%rbp), %rax
	sarq	%rcx
	salq	$4, %rcx
	movq	%rax, %rdi
	leaq	(%rax,%rcx), %r11
	movq	%rcx, -176(%rbp)
	movq	%r11, %rsi
	movq	%r11, -160(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-160(%rbp), %r11
	movq	-152(%rbp), %r10
	movq	%r11, %rdi
	movq	%r10, %rsi
	movq	%r11, -168(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-192(%rbp), %r9
	movq	-112(%rbp), %rdi
	movq	-176(%rbp), %rcx
	movq	-152(%rbp), %r10
	movq	%r9, %r8
	movq	-168(%rbp), %r11
	movq	%r9, -160(%rbp)
	subq	%rcx, %r8
	movq	%r10, %rdx
	sarq	$4, %rcx
	sarq	$4, %r8
	movq	%r11, %rsi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
	movq	-184(%rbp), %rax
	movq	-160(%rbp), %r9
	movq	-152(%rbp), %r10
.L3514:
	movq	-120(%rbp), %rdx
	subq	%r9, %rdx
	movq	%rdx, %r9
	sarq	$4, %r9
	cmpq	$224, %rdx
	jle	.L3584
	movq	%r9, %rcx
	movq	%r10, %rdi
	movq	%rax, -184(%rbp)
	shrq	$63, %rcx
	movq	%r9, -176(%rbp)
	addq	%r9, %rcx
	movq	%r10, -160(%rbp)
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r10,%rcx), %r11
	movq	%rcx, -168(%rbp)
	movq	%r11, %rsi
	movq	%r11, -152(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-152(%rbp), %r11
	movq	%r15, %rsi
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-152(%rbp), %r11
	movq	%r15, %r8
	movq	%r15, %rdx
	movq	-160(%rbp), %r10
	movq	-168(%rbp), %rcx
	subq	%r11, %r8
	movq	%r11, %rsi
	movq	%r10, %rdi
	sarq	$4, %rcx
	movq	%r10, -152(%rbp)
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
	movq	-184(%rbp), %rax
	movq	-176(%rbp), %r9
	movq	-152(%rbp), %r10
.L3516:
	movq	-144(%rbp), %rcx
	movq	%r9, %r8
	movq	%r15, %rdx
	movq	%r10, %rsi
	movq	-112(%rbp), %rdi
	movq	%rax, -152(%rbp)
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
	movq	-152(%rbp), %rax
.L3512:
	subq	-120(%rbp), %rax
	movq	%rax, %rcx
	sarq	$4, %rcx
	movq	%rcx, -120(%rbp)
	cmpq	$224, %rax
	jle	.L3585
	movq	%rcx, %r8
	shrq	$63, %r8
	addq	%rcx, %r8
	sarq	%r8
	salq	$4, %r8
	movq	%r8, %rax
	leaq	(%r15,%r8), %r9
	sarq	$4, %rax
	movq	%rax, -144(%rbp)
	cmpq	$224, %r8
	jle	.L3586
	movq	%rax, %rcx
	movq	%r15, %rdi
	movq	%r8, -176(%rbp)
	shrq	$63, %rcx
	movq	%r9, -152(%rbp)
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r15,%rcx), %r10
	movq	%rcx, -168(%rbp)
	movq	%r10, %rsi
	movq	%r10, -160(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %r10
	movq	%r9, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-168(%rbp), %rcx
	movq	%r15, %rdi
	movq	-176(%rbp), %r8
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %r10
	subq	%rcx, %r8
	sarq	$4, %rcx
	movq	%r9, %rdx
	sarq	$4, %r8
	movq	%r10, %rsi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
	movq	-152(%rbp), %r9
.L3520:
	movq	%r14, %rdx
	subq	%r9, %rdx
	movq	%rdx, %rax
	sarq	$4, %rax
	cmpq	$224, %rdx
	jle	.L3587
	movq	%rax, %rcx
	movq	%r9, %rdi
	movq	%rax, -176(%rbp)
	shrq	$63, %rcx
	movq	%r9, -160(%rbp)
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r9,%rcx), %r10
	movq	%rcx, -168(%rbp)
	movq	%r10, %rsi
	movq	%r10, -152(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-152(%rbp), %r10
	movq	%r14, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-152(%rbp), %r10
	movq	%r14, %r8
	movq	%r14, %rdx
	movq	-160(%rbp), %r9
	movq	-168(%rbp), %rcx
	subq	%r10, %r8
	movq	%r10, %rsi
	movq	%r9, %rdi
	sarq	$4, %rcx
	movq	%r9, -152(%rbp)
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
	movq	-176(%rbp), %rax
	movq	-152(%rbp), %r9
.L3522:
	movq	-144(%rbp), %rcx
	movq	%rax, %r8
	movq	%r14, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
.L3518:
	movq	-120(%rbp), %r8
	movq	-112(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	-136(%rbp), %rcx
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_less_iterEEvT_SD_SD_T0_SE_T1_
	jmp	.L3510
.L3582:
	movq	-112(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, -144(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-144(%rbp), %rax
	jmp	.L3512
.L3585:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	jmp	.L3518
.L3587:
	movq	%r9, %rdi
	movq	%r14, %rsi
	movq	%rax, -160(%rbp)
	movq	%r9, -152(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %rax
	jmp	.L3522
.L3586:
	movq	%r9, %rsi
	movq	%r15, %rdi
	movq	%r9, -152(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-152(%rbp), %r9
	jmp	.L3520
.L3584:
	movq	%r10, %rdi
	movq	%r15, %rsi
	movq	%r9, -168(%rbp)
	movq	%rax, -160(%rbp)
	movq	%r10, -152(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-152(%rbp), %r10
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %r9
	jmp	.L3516
.L3583:
	movq	-112(%rbp), %rdi
	movq	%r10, %rsi
	movq	%r9, -168(%rbp)
	movq	%rax, -160(%rbp)
	movq	%r10, -152(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_less_iterEEvT_SD_T0_
	movq	-152(%rbp), %r10
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %r9
	jmp	.L3514
.L3569:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3573:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25155:
	.size	_ZN2v88internal8compiler19InstructionSelector16EmitLookupSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE, .-_ZN2v88internal8compiler19InstructionSelector16EmitLookupSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE
	.section	.text._ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_SH_T2_,"axG",@progbits,_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_SH_T2_,comdat
	.p2align 4
	.weak	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_SH_T2_
	.type	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_SH_T2_, @function
_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_SH_T2_:
.LFB30332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$56, %rsp
	movq	16(%rbp), %r15
	cmpq	%r15, %r8
	movq	%r15, %rax
	cmovle	%r8, %rax
	cmpq	%rax, %rcx
	jg	.L3589
	movq	%rsi, %rbx
	movq	%rsi, %r8
	subq	%rdi, %rbx
	cmpq	%rdi, %rsi
	je	.L3590
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rdx
	movq	%rdi, %rsi
	movq	%r9, %rdi
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L3590:
	leaq	0(%r13,%rbx), %rdx
	cmpq	%r13, %rdx
	je	.L3588
	.p2align 4,,10
	.p2align 3
.L3597:
	cmpq	%r8, %r14
	je	.L3593
.L3621:
	movl	0(%r13), %eax
	cmpl	%eax, (%r8)
	jge	.L3594
	movdqu	(%r8), %xmm0
	addq	$16, %r12
	addq	$16, %r8
	movups	%xmm0, -16(%r12)
	cmpq	%r13, %rdx
	jne	.L3597
.L3588:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3594:
	.cfi_restore_state
	movdqu	0(%r13), %xmm1
	addq	$16, %r13
	addq	$16, %r12
	movups	%xmm1, -16(%r12)
	cmpq	%r13, %rdx
	je	.L3588
	cmpq	%r8, %r14
	jne	.L3621
.L3593:
	subq	%r13, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
.L3619:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L3589:
	.cfi_restore_state
	cmpq	%r15, %r8
	jg	.L3599
	movq	%rdx, %r15
	subq	%rsi, %r15
	cmpq	%rsi, %rdx
	je	.L3600
	movq	%r15, %rdx
	movq	%r9, %rdi
	movq	%rsi, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rsi
.L3600:
	leaq	0(%r13,%r15), %rax
	cmpq	%r12, %rsi
	je	.L3622
	cmpq	%rax, %r13
	je	.L3588
	subq	$16, %rsi
	subq	$16, %rax
	subq	$16, %r14
	jmp	.L3603
	.p2align 4,,10
	.p2align 3
.L3624:
	movdqu	(%rsi), %xmm2
	movups	%xmm2, (%r14)
	cmpq	%r12, %rsi
	je	.L3623
	subq	$16, %rsi
.L3606:
	subq	$16, %r14
.L3603:
	movl	(%rsi), %ebx
	cmpl	%ebx, (%rax)
	jl	.L3624
	movdqu	(%rax), %xmm3
	movups	%xmm3, (%r14)
	cmpq	%rax, %r13
	je	.L3588
	subq	$16, %rax
	jmp	.L3606
	.p2align 4,,10
	.p2align 3
.L3599:
	cmpq	%r8, %rcx
	jle	.L3607
	movq	%rcx, %rax
	movq	%rsi, %r10
	shrq	$63, %rax
	addq	%rcx, %rax
	sarq	%rax
	movq	%rax, %r11
	movq	%rax, -56(%rbp)
	movq	%rdx, %rax
	salq	$4, %r11
	subq	%rsi, %rax
	addq	%rdi, %r11
	sarq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L3609:
	testq	%rax, %rax
	jle	.L3608
.L3625:
	movq	%rax, %rdi
	movl	(%r11), %r9d
	sarq	%rdi
	movq	%rdi, %rdx
	salq	$4, %rdx
	addq	%r10, %rdx
	cmpl	%r9d, (%rdx)
	jge	.L3615
	subq	%rdi, %rax
	leaq	16(%rdx), %r10
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L3625
.L3608:
	movq	%r10, %r8
	subq	%rsi, %r8
	sarq	$4, %r8
.L3611:
	subq	$8, %rsp
	movq	%rcx, %rax
	subq	-56(%rbp), %rax
	movq	%r10, %rdx
	pushq	%r15
	movq	%r13, %r9
	movq	%r11, %rdi
	movq	%rax, %rcx
	movq	%r10, -88(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r11, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZSt17__rotate_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lET_SB_SB_SB_T1_SC_T0_SC_
	movq	-80(%rbp), %r11
	movq	-56(%rbp), %rcx
	movq	%r13, %r9
	movq	-72(%rbp), %r8
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	%r15, (%rsp)
	movq	%r11, %rsi
	movq	%rax, -56(%rbp)
	call	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_SH_T2_
	movq	-72(%rbp), %r8
	movq	-88(%rbp), %r10
	movq	%r13, %r9
	popq	%rax
	movq	-56(%rbp), %rax
	movq	%r15, 16(%rbp)
	movq	-64(%rbp), %rcx
	popq	%rdx
	subq	%r8, %rbx
	leaq	-40(%rbp), %rsp
	movq	%rbx, %r8
	movq	%r14, %rdx
	popq	%rbx
	movq	%r10, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_SH_T2_
	.p2align 4,,10
	.p2align 3
.L3607:
	.cfi_restore_state
	shrq	$63, %r8
	movq	%rsi, %rax
	movq	%rdi, %r11
	addq	%rbx, %r8
	subq	%rdi, %rax
	sarq	%r8
	sarq	$4, %rax
	movq	%r8, %r10
	salq	$4, %r10
	addq	%rsi, %r10
	.p2align 4,,10
	.p2align 3
.L3613:
	testq	%rax, %rax
	jle	.L3612
.L3626:
	movq	%rax, %rdi
	sarq	%rdi
	movq	%rdi, %rdx
	salq	$4, %rdx
	addq	%r11, %rdx
	movl	(%rdx), %r9d
	cmpl	%r9d, (%r10)
	jl	.L3616
	subq	%rdi, %rax
	leaq	16(%rdx), %r11
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L3626
.L3612:
	movq	%r11, %rax
	subq	%r12, %rax
	sarq	$4, %rax
	movq	%rax, -56(%rbp)
	jmp	.L3611
	.p2align 4,,10
	.p2align 3
.L3615:
	movq	%rdi, %rax
	jmp	.L3609
	.p2align 4,,10
	.p2align 3
.L3616:
	movq	%rdi, %rax
	jmp	.L3613
	.p2align 4,,10
	.p2align 3
.L3623:
	addq	$16, %rax
	cmpq	%rax, %r13
	je	.L3588
	subq	%r13, %rax
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	subq	%rax, %rdi
	jmp	.L3619
	.p2align 4,,10
	.p2align 3
.L3622:
	cmpq	%rax, %r13
	je	.L3588
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	subq	%r15, %rdi
	jmp	.L3619
	.cfi_endproc
.LFE30332:
	.size	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_SH_T2_, .-_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_SH_T2_
	.section	.text._ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_T2_,"axG",@progbits,_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_T2_
	.type	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_T2_, @function
_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_T2_:
.LFB29833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	subq	%rdi, %rax
	sarq	$4, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	1(%rax), %rsi
	movq	%rsi, %rax
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	shrq	$63, %rax
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	addq	%rsi, %rax
	sarq	%rax
	movq	%rax, %r9
	subq	$24, %rsp
	salq	$4, %r9
	movq	%r9, -56(%rbp)
	leaq	(%rdi,%r9), %r15
	cmpq	%rcx, %rax
	jle	.L3628
	movq	%r15, %rsi
	call	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_T2_
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_T2_
	movq	-56(%rbp), %r9
.L3629:
	subq	$8, %rsp
	movq	%r13, %r8
	movq	%r9, %rcx
	movq	%r13, %rdx
	pushq	%r14
	subq	%r15, %r8
	movq	%rbx, %r9
	movq	%r15, %rsi
	sarq	$4, %rcx
	movq	%r12, %rdi
	sarq	$4, %r8
	call	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_SH_T2_
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3628:
	.cfi_restore_state
	movq	%r15, %rsi
	call	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_
	movq	-56(%rbp), %r9
	jmp	.L3629
	.cfi_endproc
.LFE29833:
	.size	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_T2_, .-_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_T2_
	.section	.text._ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_SM_T2_,"ax",@progbits
	.p2align 4
	.type	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_SM_T2_, @function
_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_SM_T2_:
.LFB30498:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$56, %rsp
	movq	16(%rbp), %r15
	cmpq	%r15, %r8
	movq	%r15, %rax
	cmovle	%r8, %rax
	cmpq	%rax, %rcx
	jg	.L3632
	movq	%rsi, %rbx
	movq	%rsi, %r8
	subq	%rdi, %rbx
	cmpq	%rdi, %rsi
	je	.L3633
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rdx
	movq	%rdi, %rsi
	movq	%r9, %rdi
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L3633:
	leaq	0(%r13,%rbx), %rdx
	cmpq	%r13, %rdx
	je	.L3631
	.p2align 4,,10
	.p2align 3
.L3640:
	cmpq	%r8, %r14
	je	.L3636
.L3664:
	movl	0(%r13), %eax
	cmpl	%eax, (%r8)
	jge	.L3637
	movdqu	(%r8), %xmm0
	addq	$16, %r12
	addq	$16, %r8
	movups	%xmm0, -16(%r12)
	cmpq	%r13, %rdx
	jne	.L3640
.L3631:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3637:
	.cfi_restore_state
	movdqu	0(%r13), %xmm1
	addq	$16, %r13
	addq	$16, %r12
	movups	%xmm1, -16(%r12)
	cmpq	%r13, %rdx
	je	.L3631
	cmpq	%r8, %r14
	jne	.L3664
.L3636:
	subq	%r13, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
.L3662:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L3632:
	.cfi_restore_state
	cmpq	%r15, %r8
	jg	.L3642
	movq	%rdx, %r15
	subq	%rsi, %r15
	cmpq	%rsi, %rdx
	je	.L3643
	movq	%r15, %rdx
	movq	%r9, %rdi
	movq	%rsi, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rsi
.L3643:
	leaq	0(%r13,%r15), %rax
	cmpq	%r12, %rsi
	je	.L3665
	cmpq	%rax, %r13
	je	.L3631
	subq	$16, %rsi
	subq	$16, %rax
	subq	$16, %r14
	jmp	.L3646
	.p2align 4,,10
	.p2align 3
.L3667:
	movdqu	(%rsi), %xmm2
	movups	%xmm2, (%r14)
	cmpq	%r12, %rsi
	je	.L3666
	subq	$16, %rsi
.L3649:
	subq	$16, %r14
.L3646:
	movl	(%rsi), %ebx
	cmpl	%ebx, (%rax)
	jl	.L3667
	movdqu	(%rax), %xmm3
	movups	%xmm3, (%r14)
	cmpq	%rax, %r13
	je	.L3631
	subq	$16, %rax
	jmp	.L3649
	.p2align 4,,10
	.p2align 3
.L3642:
	cmpq	%r8, %rcx
	jle	.L3650
	movq	%rcx, %rax
	movq	%rsi, %r10
	shrq	$63, %rax
	addq	%rcx, %rax
	sarq	%rax
	movq	%rax, %r11
	movq	%rax, -56(%rbp)
	movq	%rdx, %rax
	salq	$4, %r11
	subq	%rsi, %rax
	addq	%rdi, %r11
	sarq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L3652:
	testq	%rax, %rax
	jle	.L3651
.L3668:
	movq	%rax, %rdi
	movl	(%r11), %r9d
	sarq	%rdi
	movq	%rdi, %rdx
	salq	$4, %rdx
	addq	%r10, %rdx
	cmpl	%r9d, (%rdx)
	jge	.L3658
	subq	%rdi, %rax
	leaq	16(%rdx), %r10
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L3668
.L3651:
	movq	%r10, %r8
	subq	%rsi, %r8
	sarq	$4, %r8
.L3654:
	subq	$8, %rsp
	movq	%rcx, %rax
	subq	-56(%rbp), %rax
	movq	%r10, %rdx
	pushq	%r15
	movq	%r13, %r9
	movq	%r11, %rdi
	movq	%rax, %rcx
	movq	%r10, -88(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r11, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZSt17__rotate_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lET_SB_SB_SB_T1_SC_T0_SC_
	movq	-80(%rbp), %r11
	movq	-56(%rbp), %rcx
	movq	%r13, %r9
	movq	-72(%rbp), %r8
	movq	%rax, %rdx
	movq	%r12, %rdi
	movq	%r15, (%rsp)
	movq	%r11, %rsi
	movq	%rax, -56(%rbp)
	call	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_SM_T2_
	movq	-72(%rbp), %r8
	movq	-88(%rbp), %r10
	movq	%r13, %r9
	popq	%rax
	movq	-56(%rbp), %rax
	movq	%r15, 16(%rbp)
	movq	-64(%rbp), %rcx
	popq	%rdx
	subq	%r8, %rbx
	leaq	-40(%rbp), %rsp
	movq	%rbx, %r8
	movq	%r14, %rdx
	popq	%rbx
	movq	%r10, %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_SM_T2_
	.p2align 4,,10
	.p2align 3
.L3650:
	.cfi_restore_state
	shrq	$63, %r8
	movq	%rsi, %rax
	movq	%rdi, %r11
	addq	%rbx, %r8
	subq	%rdi, %rax
	sarq	%r8
	sarq	$4, %rax
	movq	%r8, %r10
	salq	$4, %r10
	addq	%rsi, %r10
	.p2align 4,,10
	.p2align 3
.L3656:
	testq	%rax, %rax
	jle	.L3655
.L3669:
	movq	%rax, %rdi
	sarq	%rdi
	movq	%rdi, %rdx
	salq	$4, %rdx
	addq	%r11, %rdx
	movl	(%rdx), %r9d
	cmpl	%r9d, (%r10)
	jl	.L3659
	subq	%rdi, %rax
	leaq	16(%rdx), %r11
	subq	$1, %rax
	testq	%rax, %rax
	jg	.L3669
.L3655:
	movq	%r11, %rax
	subq	%r12, %rax
	sarq	$4, %rax
	movq	%rax, -56(%rbp)
	jmp	.L3654
	.p2align 4,,10
	.p2align 3
.L3658:
	movq	%rdi, %rax
	jmp	.L3652
	.p2align 4,,10
	.p2align 3
.L3659:
	movq	%rdi, %rax
	jmp	.L3656
	.p2align 4,,10
	.p2align 3
.L3666:
	addq	$16, %rax
	cmpq	%rax, %r13
	je	.L3631
	subq	%r13, %rax
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	subq	%rax, %rdi
	jmp	.L3662
	.p2align 4,,10
	.p2align 3
.L3665:
	cmpq	%rax, %r13
	je	.L3631
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	subq	%r15, %rdi
	jmp	.L3662
	.cfi_endproc
.LFE30498:
	.size	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_SM_T2_, .-_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_SM_T2_
	.section	.text._ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_T1_T2_,"ax",@progbits
	.p2align 4
	.type	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_T1_T2_, @function
_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_T1_T2_:
.LFB30036:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	subq	%rdi, %rax
	sarq	$4, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	1(%rax), %rsi
	movq	%rsi, %rax
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	shrq	$63, %rax
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	addq	%rsi, %rax
	sarq	%rax
	movq	%rax, %r9
	subq	$24, %rsp
	salq	$4, %r9
	movq	%r9, -56(%rbp)
	leaq	(%rdi,%r9), %r15
	cmpq	%rcx, %rax
	jle	.L3671
	movq	%r15, %rsi
	call	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_T1_T2_
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_T1_T2_
	movq	-56(%rbp), %r9
.L3672:
	subq	$8, %rsp
	movq	%r13, %r8
	movq	%r9, %rcx
	movq	%r13, %rdx
	pushq	%r14
	subq	%r15, %r8
	movq	%rbx, %r9
	movq	%r15, %rsi
	sarq	$4, %rcx
	movq	%r12, %rdi
	sarq	$4, %r8
	call	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_SM_T2_
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3671:
	.cfi_restore_state
	movq	%r15, %rsi
	call	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_T1_.isra.0
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_T1_.isra.0
	movq	-56(%rbp), %r9
	jmp	.L3672
	.cfi_endproc
.LFE30036:
	.size	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_T1_T2_, .-_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_T1_T2_
	.section	.text._ZN2v88internal8compiler19InstructionSelector22EmitBinarySearchSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler19InstructionSelector22EmitBinarySearchSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE
	.type	_ZN2v88internal8compiler19InstructionSelector22EmitBinarySearchSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE, @function
_ZN2v88internal8compiler19InstructionSelector22EmitBinarySearchSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE:
.LFB25156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	16(%rax), %rsi
	subq	8(%rax), %rsi
	movq	%rsi, %rax
	addq	$23, %rsi
	sarq	$4, %rax
	andq	$-8, %rsi
	leaq	2(%rax,%rax), %rax
	movq	%rax, -128(%rbp)
	movq	16(%rdi), %rax
	movq	%rax, %rbx
	movq	%rax, -112(%rbp)
	movq	24(%rdi), %rax
	subq	%rbx, %rax
	cmpq	%rax, %rsi
	ja	.L3813
	addq	-112(%rbp), %rsi
	movq	%rsi, 16(%rdi)
.L3676:
	movq	(%rdx), %rax
	movq	%rax, (%rbx)
	movq	24(%r13), %rdx
	movq	16(%r12), %rax
	movslq	4(%rdx), %rdx
	movl	$7, -80(%rbp)
	movb	$19, -76(%rbp)
	movq	%rdx, -72(%rbp)
	movq	160(%rax), %rsi
	movq	%rsi, %r14
	subq	152(%rax), %r14
	sarq	$4, %r14
	cmpq	168(%rax), %rsi
	je	.L3677
	movl	$7, (%rsi)
	movb	$19, 4(%rsi)
	movq	%rdx, 8(%rsi)
	addq	$16, 160(%rax)
	movq	%rbx, %rax
.L3678:
	salq	$32, %r14
	orq	$11, %r14
	movq	%r14, 8(%rax)
	movq	0(%r13), %rax
	movq	16(%rax), %rdx
	movq	8(%rax), %r15
	movq	%rdx, %rax
	subq	%r15, %rax
	movq	%rax, -96(%rbp)
	sarq	$4, %rax
	movq	%rax, -120(%rbp)
	movq	%rax, %rbx
	movq	%rax, %r14
	movabsq	$576460752303423487, %rax
	cmpq	%rax, %rbx
	ja	.L3814
	cmpq	$0, -120(%rbp)
	je	.L3680
	movq	-96(%rbp), %rbx
	movq	%rdx, -136(%rbp)
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	-136(%rbp), %rdx
	movq	%rax, -104(%rbp)
	addq	%rbx, %rax
	movq	%rax, -88(%rbp)
	cmpq	%r15, %rdx
	jne	.L3796
.L3795:
	movq	-120(%rbp), %r13
	leaq	_ZSt7nothrow(%rip), %rbx
.L3685:
	movq	%r13, %rdi
	movq	%rbx, %rsi
	salq	$4, %rdi
	call	_ZnwmRKSt9nothrow_t@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3683
	movq	-88(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	%r13, %rcx
	movq	%rax, %rdx
	call	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_T1_T2_
.L3684:
	movq	%r15, %rdi
	leaq	_ZSt7nothrow(%rip), %rbx
	call	_ZdlPv@PLT
	cmpq	$0, -96(%rbp)
	je	.L3722
.L3717:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	salq	$4, %rdi
	call	_ZnwmRKSt9nothrow_t@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3720
	movq	-88(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	%r14, %rcx
	movq	%rax, %rdx
	call	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_T1_T2_
.L3721:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	cmpq	$0, -120(%rbp)
	je	.L3791
	movq	-112(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %r15
	leaq	24(%rax,%rcx), %rbx
	leaq	24(%rax), %r13
	movq	%rbx, -96(%rbp)
	subq	%rax, %r15
	leaq	-80(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L3792:
	movq	16(%r12), %rdx
	movl	-24(%r15,%r13), %esi
	movq	%rbx, %rdi
	leaq	-8(%r13), %r14
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal8compiler8ConstantC1Ei@PLT
	movl	-80(%rbp), %ecx
	movq	-88(%rbp), %rdx
	testl	%ecx, %ecx
	jne	.L3785
	cmpb	$19, -76(%rbp)
	je	.L3815
.L3785:
	movq	160(%rdx), %rsi
	movq	%rsi, %rax
	subq	152(%rdx), %rax
	sarq	$4, %rax
	cmpq	168(%rdx), %rsi
	je	.L3787
	movzbl	-76(%rbp), %r8d
	movq	-72(%rbp), %rdi
	movl	%ecx, (%rsi)
	movb	%r8b, 4(%rsi)
	movq	%rdi, 8(%rsi)
	addq	$16, 160(%rdx)
.L3788:
	salq	$32, %rax
	orq	$11, %rax
.L3786:
	movq	%rax, (%r14)
	movq	-16(%r15,%r13), %rax
	movq	16(%r12), %rdi
	movslq	4(%rax), %rax
	movl	$7, -80(%rbp)
	movb	$19, -76(%rbp)
	movq	%rax, -72(%rbp)
	movq	160(%rdi), %rsi
	movq	%rsi, %r14
	subq	152(%rdi), %r14
	sarq	$4, %r14
	cmpq	168(%rdi), %rsi
	je	.L3789
	salq	$32, %r14
	movl	$7, (%rsi)
	addq	$16, %r13
	orq	$11, %r14
	movb	$19, 4(%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, 160(%rdi)
	movq	%r14, -16(%r13)
	cmpq	-96(%rbp), %r13
	jne	.L3792
.L3791:
	cmpq	$65534, -128(%rbp)
	ja	.L3816
.L3809:
	movq	-112(%rbp), %r8
	movq	-128(%rbp), %rcx
	xorl	%edx, %edx
	movl	$14, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler19InstructionSelector4EmitEimPNS1_18InstructionOperandEmS4_mS4_.part.0.constprop.0
.L3793:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L3674
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3674:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3817
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3680:
	.cfi_restore_state
	movq	-96(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%rax, -88(%rbp)
	cmpq	%r15, %rdx
	je	.L3818
.L3796:
	movq	-96(%rbp), %rbx
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	call	memcpy@PLT
	testq	%rbx, %rbx
	jne	.L3795
.L3682:
	movq	-88(%rbp), %rax
	subq	-104(%rbp), %rax
	movq	%rax, %rdx
	sarq	$4, %rdx
	jmp	.L3686
	.p2align 4,,10
	.p2align 3
.L3789:
	salq	$32, %r14
	addq	$144, %rdi
	movq	%rbx, %rdx
	addq	$16, %r13
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	orq	$11, %r14
	movq	%r14, -16(%r13)
	cmpq	%r13, -96(%rbp)
	jne	.L3792
	cmpq	$65534, -128(%rbp)
	jbe	.L3809
.L3816:
	movb	$1, 376(%r12)
	jmp	.L3793
	.p2align 4,,10
	.p2align 3
.L3815:
	movslq	-72(%rbp), %rax
	salq	$32, %rax
	orq	$3, %rax
	jmp	.L3786
	.p2align 4,,10
	.p2align 3
.L3787:
	leaq	144(%rdx), %rdi
	movq	%rbx, %rdx
	movq	%rax, -88(%rbp)
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-88(%rbp), %rax
	jmp	.L3788
.L3720:
	sarq	%r14
	jne	.L3717
.L3722:
	movq	-88(%rbp), %r15
	subq	-104(%rbp), %r15
	cmpq	$224, %r15
	jle	.L3819
	movq	-104(%rbp), %rax
	sarq	$5, %r15
	salq	$4, %r15
	leaq	(%rax,%r15), %r13
	movq	%r15, %rax
	sarq	$4, %rax
	movq	%rax, -144(%rbp)
	cmpq	$224, %r15
	jbe	.L3820
	movq	%r15, %rax
	movq	-104(%rbp), %rbx
	sarq	$5, %rax
	salq	$4, %rax
	movq	%rax, %rcx
	movq	%rax, -136(%rbp)
	addq	%rax, %rbx
	sarq	$4, %rcx
	movq	%rcx, -152(%rbp)
	cmpq	$224, %rax
	jbe	.L3821
	movq	-104(%rbp), %rcx
	sarq	$5, %rax
	salq	$4, %rax
	leaq	(%rcx,%rax), %r14
	movq	%rax, %rcx
	sarq	$4, %rcx
	movq	%rcx, -168(%rbp)
	cmpq	$224, %rax
	jbe	.L3822
	movq	%rax, %r9
	movq	-104(%rbp), %rcx
	sarq	$5, %r9
	salq	$4, %r9
	leaq	(%rcx,%r9), %r10
	movq	%r9, %rcx
	sarq	$4, %rcx
	movq	%rcx, -160(%rbp)
	cmpq	$224, %r9
	jbe	.L3823
	movq	%r9, %rcx
	movq	%rax, -216(%rbp)
	movq	-104(%rbp), %rax
	sarq	$5, %rcx
	movq	%r9, -208(%rbp)
	salq	$4, %rcx
	movq	%rax, %rdi
	movq	%r10, -176(%rbp)
	leaq	(%rax,%rcx), %r11
	movq	%rcx, -200(%rbp)
	movq	%r11, %rsi
	movq	%r11, -184(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-184(%rbp), %r11
	movq	-176(%rbp), %r10
	movq	%r11, %rdi
	movq	%r10, %rsi
	movq	%r11, -192(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-208(%rbp), %r9
	movq	-104(%rbp), %rdi
	movq	-200(%rbp), %rcx
	movq	-176(%rbp), %r10
	movq	%r9, %r8
	movq	-192(%rbp), %r11
	movq	%r9, -184(%rbp)
	subq	%rcx, %r8
	movq	%r10, %rdx
	sarq	$4, %rcx
	sarq	$4, %r8
	movq	%r11, %rsi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	movq	-216(%rbp), %rax
	movq	-184(%rbp), %r9
	movq	-176(%rbp), %r10
.L3731:
	movq	%rax, %rdx
	subq	%r9, %rdx
	movq	%rdx, %r9
	sarq	$4, %r9
	cmpq	$224, %rdx
	jle	.L3824
	movq	%r9, %rcx
	movq	%r10, %rdi
	movq	%rax, -208(%rbp)
	shrq	$63, %rcx
	movq	%r9, -200(%rbp)
	addq	%r9, %rcx
	movq	%r10, -184(%rbp)
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r10,%rcx), %r11
	movq	%rcx, -192(%rbp)
	movq	%r11, %rsi
	movq	%r11, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-176(%rbp), %r11
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-176(%rbp), %r11
	movq	%r14, %r8
	movq	%r14, %rdx
	movq	-184(%rbp), %r10
	movq	-192(%rbp), %rcx
	subq	%r11, %r8
	movq	%r11, %rsi
	movq	%r10, %rdi
	sarq	$4, %rcx
	movq	%r10, -176(%rbp)
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	movq	-208(%rbp), %rax
	movq	-200(%rbp), %r9
	movq	-176(%rbp), %r10
.L3733:
	movq	-160(%rbp), %rcx
	movq	%r9, %r8
	movq	%r14, %rdx
	movq	%r10, %rsi
	movq	-104(%rbp), %rdi
	movq	%rax, -176(%rbp)
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	movq	-176(%rbp), %rax
.L3729:
	movq	-136(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	sarq	$4, %rcx
	movq	%rcx, -160(%rbp)
	cmpq	$224, %rax
	jle	.L3825
	movq	%rcx, %r8
	shrq	$63, %r8
	addq	%rcx, %r8
	sarq	%r8
	salq	$4, %r8
	movq	%r8, %rax
	leaq	(%r14,%r8), %r9
	sarq	$4, %rax
	movq	%rax, -176(%rbp)
	cmpq	$224, %r8
	jle	.L3826
	movq	%rax, %rcx
	movq	%r14, %rdi
	movq	%r8, -208(%rbp)
	shrq	$63, %rcx
	movq	%r9, -184(%rbp)
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r14,%rcx), %r10
	movq	%rcx, -200(%rbp)
	movq	%r10, %rsi
	movq	%r10, -192(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r10
	movq	%r9, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-200(%rbp), %rcx
	movq	%r14, %rdi
	movq	-208(%rbp), %r8
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r10
	subq	%rcx, %r8
	sarq	$4, %rcx
	movq	%r9, %rdx
	sarq	$4, %r8
	movq	%r10, %rsi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	movq	-184(%rbp), %r9
.L3737:
	movq	%rbx, %rdx
	subq	%r9, %rdx
	movq	%rdx, %rax
	sarq	$4, %rax
	cmpq	$224, %rdx
	jle	.L3827
	movq	%rax, %rcx
	movq	%r9, %rdi
	movq	%rax, -208(%rbp)
	shrq	$63, %rcx
	movq	%r9, -192(%rbp)
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r9,%rcx), %r10
	movq	%rcx, -200(%rbp)
	movq	%r10, %rsi
	movq	%r10, -184(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-184(%rbp), %r10
	movq	%rbx, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-184(%rbp), %r10
	movq	%rbx, %r8
	movq	%rbx, %rdx
	movq	-192(%rbp), %r9
	movq	-200(%rbp), %rcx
	subq	%r10, %r8
	movq	%r10, %rsi
	movq	%r9, %rdi
	sarq	$4, %rcx
	movq	%r9, -184(%rbp)
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	movq	-208(%rbp), %rax
	movq	-184(%rbp), %r9
.L3739:
	movq	-176(%rbp), %rcx
	movq	%rax, %r8
	movq	%rbx, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
.L3735:
	movq	-160(%rbp), %r8
	movq	-168(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	-104(%rbp), %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
.L3727:
	subq	-136(%rbp), %r15
	movq	%r15, %rax
	sarq	$4, %rax
	movq	%rax, -136(%rbp)
	cmpq	$224, %r15
	jle	.L3828
	movq	%rax, %r14
	shrq	$63, %r14
	addq	%rax, %r14
	sarq	%r14
	salq	$4, %r14
	movq	%r14, %rax
	leaq	(%rbx,%r14), %r15
	sarq	$4, %rax
	movq	%rax, -160(%rbp)
	cmpq	$224, %r14
	jle	.L3829
	movq	%rax, %rcx
	shrq	$63, %rax
	addq	%rcx, %rax
	sarq	%rax
	salq	$4, %rax
	movq	%rax, %rcx
	leaq	(%rbx,%rax), %r9
	sarq	$4, %rcx
	movq	%rcx, -168(%rbp)
	cmpq	$224, %rax
	jle	.L3830
	movq	%rax, -208(%rbp)
	movq	-168(%rbp), %rax
	movq	%rbx, %rdi
	movq	%r9, -176(%rbp)
	movq	%rax, %rcx
	shrq	$63, %rcx
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%rbx,%rcx), %r10
	movq	%rcx, -200(%rbp)
	movq	%r10, %rsi
	movq	%r10, -184(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-184(%rbp), %r10
	movq	-176(%rbp), %r9
	movq	%r10, %rdi
	movq	%r9, %rsi
	movq	%r10, -192(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-208(%rbp), %rax
	movq	%rbx, %rdi
	movq	-200(%rbp), %rcx
	movq	-176(%rbp), %r9
	movq	-192(%rbp), %r10
	movq	%rax, %r8
	movq	%rax, -184(%rbp)
	subq	%rcx, %r8
	movq	%r9, %rdx
	sarq	$4, %rcx
	movq	%r10, %rsi
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	movq	-184(%rbp), %rax
	movq	-176(%rbp), %r9
.L3745:
	subq	%rax, %r14
	movq	%r14, %rax
	sarq	$4, %rax
	cmpq	$224, %r14
	jle	.L3831
	movq	%rax, %rcx
	movq	%r9, %rdi
	movq	%rax, -192(%rbp)
	shrq	$63, %rcx
	movq	%r9, -176(%rbp)
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r9,%rcx), %r14
	movq	%rcx, -184(%rbp)
	movq	%r14, %rsi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	%r15, %r8
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	-176(%rbp), %r9
	movq	-184(%rbp), %rcx
	subq	%r14, %r8
	sarq	$4, %r8
	movq	%r9, %rdi
	sarq	$4, %rcx
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	movq	-192(%rbp), %rax
	movq	-176(%rbp), %r9
.L3747:
	movq	-168(%rbp), %rcx
	movq	%rax, %r8
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	%rbx, %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
.L3743:
	movq	%r13, %rax
	subq	%r15, %rax
	movq	%rax, %rcx
	sarq	$4, %rcx
	movq	%rcx, -168(%rbp)
	cmpq	$224, %rax
	jle	.L3832
	movq	%rcx, %r8
	shrq	$63, %r8
	addq	%rcx, %r8
	sarq	%r8
	salq	$4, %r8
	movq	%r8, %rax
	leaq	(%r15,%r8), %r14
	sarq	$4, %rax
	movq	%rax, -176(%rbp)
	cmpq	$224, %r8
	jle	.L3833
	movq	%rax, %rcx
	movq	%r15, %rdi
	movq	%r8, -200(%rbp)
	shrq	$63, %rcx
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r15,%rcx), %r10
	movq	%rcx, -192(%rbp)
	movq	%r10, %rsi
	movq	%r10, -184(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-184(%rbp), %r10
	movq	%r14, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-192(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	-200(%rbp), %r8
	movq	-184(%rbp), %r10
	subq	%rcx, %r8
	movq	%r10, %rsi
	sarq	$4, %rcx
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
.L3751:
	movq	%r13, %rdx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$4, %rax
	cmpq	$224, %rdx
	jle	.L3834
	movq	%rax, %rcx
	movq	%r14, %rdi
	movq	%rax, -200(%rbp)
	shrq	$63, %rcx
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r14,%rcx), %r10
	movq	%rcx, -192(%rbp)
	movq	%r10, %rsi
	movq	%r10, -184(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-184(%rbp), %r10
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	%r13, %r8
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	-184(%rbp), %r10
	movq	-192(%rbp), %rcx
	subq	%r10, %r8
	sarq	$4, %rcx
	movq	%r10, %rsi
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	movq	-200(%rbp), %rax
.L3753:
	movq	-176(%rbp), %rcx
	movq	%rax, %r8
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
.L3749:
	movq	-168(%rbp), %r8
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	-160(%rbp), %rcx
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
.L3741:
	movq	-136(%rbp), %r8
	movq	-152(%rbp), %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	-104(%rbp), %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	jmp	.L3725
	.p2align 4,,10
	.p2align 3
.L3819:
	movq	-88(%rbp), %rsi
	movq	-104(%rbp), %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
.L3723:
	xorl	%r15d, %r15d
	jmp	.L3721
.L3818:
	movq	%rbx, -104(%rbp)
	xorl	%edx, %edx
.L3686:
	cmpq	$224, %rax
	jle	.L3835
	movq	%rdx, %rax
	movq	-104(%rbp), %rcx
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	salq	$4, %rax
	leaq	(%rcx,%rax), %r15
	movq	%rax, %rcx
	sarq	$4, %rcx
	movq	%rcx, -136(%rbp)
	cmpq	$224, %rax
	jle	.L3836
	movq	%rcx, %rdx
	shrq	$63, %rdx
	addq	%rcx, %rdx
	movq	-104(%rbp), %rcx
	sarq	%rdx
	movq	%rdx, %rbx
	salq	$4, %rbx
	leaq	(%rcx,%rbx), %r13
	movq	%rbx, %rcx
	sarq	$4, %rcx
	movq	%rcx, -144(%rbp)
	cmpq	$224, %rbx
	jle	.L3837
	movq	%rcx, %r9
	shrq	$63, %r9
	addq	%rcx, %r9
	movq	-104(%rbp), %rcx
	sarq	%r9
	salq	$4, %r9
	leaq	(%rcx,%r9), %r10
	movq	%r9, %rcx
	sarq	$4, %rcx
	movq	%rcx, -152(%rbp)
	cmpq	$224, %r9
	jle	.L3838
	movq	%rax, -192(%rbp)
	movq	-152(%rbp), %rax
	movq	%r9, -200(%rbp)
	movq	%rax, %rcx
	movq	%r10, -160(%rbp)
	shrq	$63, %rcx
	addq	%rax, %rcx
	movq	-104(%rbp), %rax
	sarq	%rcx
	salq	$4, %rcx
	movq	%rax, %rdi
	leaq	(%rax,%rcx), %r11
	movq	%rcx, -184(%rbp)
	movq	%r11, %rsi
	movq	%r11, -168(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-168(%rbp), %r11
	movq	-160(%rbp), %r10
	movq	%r11, %rdi
	movq	%r10, %rsi
	movq	%r11, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-200(%rbp), %r9
	movq	-104(%rbp), %rdi
	movq	-184(%rbp), %rcx
	movq	-160(%rbp), %r10
	movq	%r9, %r8
	movq	-176(%rbp), %r11
	movq	%r9, -168(%rbp)
	subq	%rcx, %r8
	movq	%r10, %rdx
	sarq	$4, %rcx
	sarq	$4, %r8
	movq	%r11, %rsi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
	movq	-192(%rbp), %rax
	movq	-168(%rbp), %r9
	movq	-160(%rbp), %r10
.L3694:
	movq	%rbx, %rdx
	subq	%r9, %rdx
	movq	%rdx, %r9
	sarq	$4, %r9
	cmpq	$224, %rdx
	jle	.L3839
	movq	%r9, %rcx
	movq	%r10, %rdi
	movq	%rax, -192(%rbp)
	shrq	$63, %rcx
	movq	%r9, -184(%rbp)
	addq	%r9, %rcx
	movq	%r10, -168(%rbp)
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r10,%rcx), %r11
	movq	%rcx, -176(%rbp)
	movq	%r11, %rsi
	movq	%r11, -160(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-160(%rbp), %r11
	movq	%r13, %rsi
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-160(%rbp), %r11
	movq	%r13, %r8
	movq	%r13, %rdx
	movq	-168(%rbp), %r10
	movq	-176(%rbp), %rcx
	subq	%r11, %r8
	movq	%r11, %rsi
	movq	%r10, %rdi
	sarq	$4, %rcx
	movq	%r10, -160(%rbp)
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r9
	movq	-160(%rbp), %r10
.L3696:
	movq	-152(%rbp), %rcx
	movq	%r9, %r8
	movq	%r13, %rdx
	movq	%r10, %rsi
	movq	-104(%rbp), %rdi
	movq	%rax, -160(%rbp)
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
	movq	-160(%rbp), %rax
.L3692:
	subq	%rbx, %rax
	movq	%rax, %rcx
	sarq	$4, %rcx
	movq	%rcx, -152(%rbp)
	cmpq	$224, %rax
	jle	.L3840
	movq	%rcx, %r8
	shrq	$63, %r8
	addq	%rcx, %r8
	sarq	%r8
	salq	$4, %r8
	movq	%r8, %rax
	leaq	0(%r13,%r8), %rbx
	sarq	$4, %rax
	movq	%rax, -160(%rbp)
	cmpq	$224, %r8
	jle	.L3841
	movq	%rax, %rcx
	movq	%r13, %rdi
	movq	%r8, -184(%rbp)
	shrq	$63, %rcx
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	0(%r13,%rcx), %r10
	movq	%rcx, -176(%rbp)
	movq	%r10, %rsi
	movq	%r10, -168(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-168(%rbp), %r10
	movq	%rbx, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-176(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	-184(%rbp), %r8
	movq	-168(%rbp), %r10
	subq	%rcx, %r8
	movq	%r10, %rsi
	sarq	$4, %rcx
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
.L3700:
	movq	%r15, %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rax
	sarq	$4, %rax
	cmpq	$224, %rdx
	jle	.L3842
	movq	%rax, %rcx
	movq	%rbx, %rdi
	movq	%rax, -184(%rbp)
	shrq	$63, %rcx
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%rbx,%rcx), %r10
	movq	%rcx, -176(%rbp)
	movq	%r10, %rsi
	movq	%r10, -168(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-168(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	%r15, %r8
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	-168(%rbp), %r10
	movq	-176(%rbp), %rcx
	subq	%r10, %r8
	sarq	$4, %rcx
	movq	%r10, %rsi
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
	movq	-184(%rbp), %rax
.L3702:
	movq	-160(%rbp), %rcx
	movq	%rax, %r8
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
.L3698:
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	-104(%rbp), %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
.L3690:
	movq	-88(%rbp), %rax
	subq	%r15, %rax
	movq	%rax, %rbx
	sarq	$4, %rbx
	cmpq	$224, %rax
	jle	.L3843
	movq	%rbx, %rax
	shrq	$63, %rax
	addq	%rbx, %rax
	sarq	%rax
	salq	$4, %rax
	movq	%rax, %rcx
	leaq	(%r15,%rax), %r13
	sarq	$4, %rcx
	movq	%rcx, -144(%rbp)
	cmpq	$224, %rax
	jle	.L3844
	movq	%rcx, %r10
	shrq	$63, %r10
	addq	%rcx, %r10
	sarq	%r10
	salq	$4, %r10
	movq	%r10, %rcx
	leaq	(%r15,%r10), %r9
	sarq	$4, %rcx
	movq	%rcx, -152(%rbp)
	cmpq	$224, %r10
	jle	.L3845
	movq	%rax, -192(%rbp)
	movq	-152(%rbp), %rax
	movq	%r15, %rdi
	movq	%r10, -200(%rbp)
	movq	%rax, %rcx
	movq	%r9, -160(%rbp)
	shrq	$63, %rcx
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r15,%rcx), %r11
	movq	%rcx, -184(%rbp)
	movq	%r11, %rsi
	movq	%r11, -168(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-168(%rbp), %r11
	movq	-160(%rbp), %r9
	movq	%r11, %rdi
	movq	%r9, %rsi
	movq	%r11, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-200(%rbp), %r10
	movq	%r15, %rdi
	movq	-184(%rbp), %rcx
	movq	-160(%rbp), %r9
	movq	-176(%rbp), %r11
	movq	%r10, %r8
	movq	%r10, -168(%rbp)
	subq	%rcx, %r8
	movq	%r9, %rdx
	sarq	$4, %rcx
	movq	%r11, %rsi
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
	movq	-192(%rbp), %rax
	movq	-168(%rbp), %r10
	movq	-160(%rbp), %r9
.L3708:
	subq	%r10, %rax
	movq	%rax, %r10
	sarq	$4, %r10
	cmpq	$224, %rax
	jle	.L3846
	movq	%r10, %rcx
	movq	%r9, %rdi
	movq	%r10, -184(%rbp)
	shrq	$63, %rcx
	movq	%r9, -168(%rbp)
	addq	%r10, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r9,%rcx), %r11
	movq	%rcx, -176(%rbp)
	movq	%r11, %rsi
	movq	%r11, -160(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-160(%rbp), %r11
	movq	%r13, %rsi
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-160(%rbp), %r11
	movq	%r13, %r8
	movq	%r13, %rdx
	movq	-168(%rbp), %r9
	movq	-176(%rbp), %rcx
	subq	%r11, %r8
	movq	%r11, %rsi
	movq	%r9, %rdi
	sarq	$4, %rcx
	movq	%r9, -160(%rbp)
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
	movq	-184(%rbp), %r10
	movq	-160(%rbp), %r9
.L3710:
	movq	-152(%rbp), %rcx
	movq	%r10, %r8
	movq	%r13, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
.L3706:
	movq	-88(%rbp), %rax
	subq	%r13, %rax
	movq	%rax, %rcx
	sarq	$4, %rcx
	movq	%rcx, -152(%rbp)
	cmpq	$224, %rax
	jle	.L3847
	movq	%rcx, %r8
	shrq	$63, %r8
	addq	%rcx, %r8
	sarq	%r8
	salq	$4, %r8
	movq	%r8, %rax
	leaq	0(%r13,%r8), %r10
	sarq	$4, %rax
	movq	%rax, -160(%rbp)
	cmpq	$224, %r8
	jle	.L3848
	movq	%rax, %rcx
	movq	%r13, %rdi
	movq	%r8, -192(%rbp)
	shrq	$63, %rcx
	movq	%r10, -168(%rbp)
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	0(%r13,%rcx), %r11
	movq	%rcx, -184(%rbp)
	movq	%r11, %rsi
	movq	%r11, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-168(%rbp), %r10
	movq	-176(%rbp), %r11
	movq	%r10, %rsi
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-184(%rbp), %rcx
	movq	%r13, %rdi
	movq	-192(%rbp), %r8
	movq	-168(%rbp), %r10
	movq	-176(%rbp), %r11
	subq	%rcx, %r8
	sarq	$4, %rcx
	movq	%r10, %rdx
	sarq	$4, %r8
	movq	%r11, %rsi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
	movq	-168(%rbp), %r10
.L3714:
	movq	-88(%rbp), %rdx
	subq	%r10, %rdx
	movq	%rdx, %rax
	sarq	$4, %rax
	cmpq	$224, %rdx
	jle	.L3849
	movq	%rax, %rcx
	movq	%r10, %rdi
	movq	%rax, -192(%rbp)
	shrq	$63, %rcx
	movq	%r10, -176(%rbp)
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r10,%rcx), %r11
	movq	%rcx, -184(%rbp)
	movq	%r11, %rsi
	movq	%r11, -168(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-168(%rbp), %r11
	movq	-88(%rbp), %rsi
	movq	%r11, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-88(%rbp), %rdx
	movq	-168(%rbp), %r11
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %rcx
	movq	%rdx, %r8
	movq	%r11, %rsi
	subq	%r11, %r8
	movq	%r10, %rdi
	sarq	$4, %rcx
	movq	%r10, -168(%rbp)
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
	movq	-192(%rbp), %rax
	movq	-168(%rbp), %r10
.L3716:
	movq	-160(%rbp), %rcx
	movq	%rax, %r8
	movq	%r10, %rsi
	movq	%r13, %rdi
	movq	-88(%rbp), %rdx
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
.L3712:
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-88(%rbp), %rdx
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
.L3704:
	movq	-88(%rbp), %rdx
	movq	-104(%rbp), %rdi
	movq	%rbx, %r8
	movq	%r15, %rsi
	movq	-136(%rbp), %rcx
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_SG_T0_SH_T1_
.L3688:
	xorl	%r15d, %r15d
	jmp	.L3684
	.p2align 4,,10
	.p2align 3
.L3677:
	leaq	-80(%rbp), %rbx
	leaq	144(%rax), %rdi
	movq	%rbx, %rdx
	call	_ZNSt6vectorIN2v88internal8compiler8ConstantENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	movq	-112(%rbp), %rax
	jmp	.L3678
	.p2align 4,,10
	.p2align 3
.L3835:
	movq	-88(%rbp), %rsi
	movq	-104(%rbp), %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	jmp	.L3688
	.p2align 4,,10
	.p2align 3
.L3813:
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rdx
	movq	%rax, -112(%rbp)
	movq	%rax, %rbx
	jmp	.L3676
.L3836:
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	jmp	.L3690
.L3843:
	movq	-88(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	jmp	.L3704
.L3820:
	movq	-104(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
.L3725:
	movq	-88(%rbp), %rax
	subq	%r13, %rax
	movq	%rax, %rcx
	sarq	$4, %rcx
	movq	%rcx, -136(%rbp)
	cmpq	$224, %rax
	jle	.L3850
	movq	%rcx, %r15
	shrq	$63, %r15
	addq	%rcx, %r15
	sarq	%r15
	salq	$4, %r15
	movq	%r15, %rax
	leaq	0(%r13,%r15), %rbx
	sarq	$4, %rax
	movq	%rax, -152(%rbp)
	cmpq	$224, %r15
	jle	.L3851
	movq	%rax, %r11
	shrq	$63, %r11
	addq	%rax, %r11
	sarq	%r11
	salq	$4, %r11
	movq	%r11, %rax
	leaq	0(%r13,%r11), %r14
	sarq	$4, %rax
	movq	%rax, -160(%rbp)
	cmpq	$224, %r11
	jle	.L3852
	movq	%rax, %rcx
	shrq	$63, %rax
	addq	%rcx, %rax
	sarq	%rax
	salq	$4, %rax
	movq	%rax, %rcx
	leaq	0(%r13,%rax), %r9
	sarq	$4, %rcx
	movq	%rcx, -168(%rbp)
	cmpq	$224, %rax
	jle	.L3853
	movq	%rax, -216(%rbp)
	movq	-168(%rbp), %rax
	movq	%r13, %rdi
	movq	%r11, -208(%rbp)
	movq	%rax, %rcx
	movq	%r9, -176(%rbp)
	shrq	$63, %rcx
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	0(%r13,%rcx), %r10
	movq	%rcx, -200(%rbp)
	movq	%r10, %rsi
	movq	%r10, -184(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-184(%rbp), %r10
	movq	-176(%rbp), %r9
	movq	%r10, %rdi
	movq	%r9, %rsi
	movq	%r10, -192(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-216(%rbp), %rax
	movq	%r13, %rdi
	movq	-200(%rbp), %rcx
	movq	-176(%rbp), %r9
	movq	-192(%rbp), %r10
	movq	%rax, %r8
	movq	%rax, -184(%rbp)
	subq	%rcx, %r8
	movq	%r9, %rdx
	sarq	$4, %rcx
	movq	%r10, %rsi
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	movq	-208(%rbp), %r11
	movq	-184(%rbp), %rax
	movq	-176(%rbp), %r9
.L3761:
	movq	%r11, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	sarq	$4, %rax
	cmpq	$224, %rdx
	jle	.L3854
	movq	%rax, %rcx
	movq	%r9, %rdi
	movq	%r11, -208(%rbp)
	shrq	$63, %rcx
	movq	%rax, -200(%rbp)
	addq	%rax, %rcx
	movq	%r9, -184(%rbp)
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r9,%rcx), %r10
	movq	%rcx, -192(%rbp)
	movq	%r10, %rsi
	movq	%r10, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-176(%rbp), %r10
	movq	%r14, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-176(%rbp), %r10
	movq	%r14, %r8
	movq	%r14, %rdx
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %rcx
	subq	%r10, %r8
	movq	%r10, %rsi
	movq	%r9, %rdi
	sarq	$4, %rcx
	movq	%r9, -176(%rbp)
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	movq	-208(%rbp), %r11
	movq	-200(%rbp), %rax
	movq	-176(%rbp), %r9
.L3763:
	movq	-168(%rbp), %rcx
	movq	%rax, %r8
	movq	%r14, %rdx
	movq	%r9, %rsi
	movq	%r13, %rdi
	movq	%r11, -176(%rbp)
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	movq	-176(%rbp), %r11
.L3759:
	movq	%r15, %rax
	subq	%r11, %rax
	movq	%rax, %r15
	sarq	$4, %r15
	cmpq	$224, %rax
	jle	.L3855
	movq	%r15, %rcx
	movq	%r14, %rdi
	shrq	$63, %rcx
	addq	%r15, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r14,%rcx), %r9
	movq	%rcx, -168(%rbp)
	movq	%r9, %rsi
	movq	%r9, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-176(%rbp), %r9
	movq	%rbx, %rdx
	subq	%r9, %rdx
	movq	%rdx, %rax
	sarq	$4, %rax
	cmpq	$224, %rdx
	jle	.L3856
	movq	%rax, %rcx
	movq	%r9, %rdi
	movq	%rax, -200(%rbp)
	shrq	$63, %rcx
	movq	%r9, -184(%rbp)
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r9,%rcx), %r10
	movq	%rcx, -192(%rbp)
	movq	%r10, %rsi
	movq	%r10, -176(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-176(%rbp), %r10
	movq	%rbx, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-176(%rbp), %r10
	movq	%rbx, %r8
	movq	%rbx, %rdx
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %rcx
	subq	%r10, %r8
	movq	%r10, %rsi
	movq	%r9, %rdi
	sarq	$4, %rcx
	movq	%r9, -176(%rbp)
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	movq	-200(%rbp), %rax
	movq	-176(%rbp), %r9
.L3767:
	movq	-168(%rbp), %rcx
	movq	%rax, %r8
	movq	%rbx, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	sarq	$4, %rcx
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
.L3765:
	movq	-160(%rbp), %rcx
	movq	%r15, %r8
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
.L3757:
	movq	-88(%rbp), %rax
	subq	%rbx, %rax
	movq	%rax, %rcx
	sarq	$4, %rcx
	movq	%rcx, -160(%rbp)
	cmpq	$224, %rax
	jle	.L3857
	movq	%rcx, %r14
	shrq	$63, %r14
	addq	%rcx, %r14
	sarq	%r14
	salq	$4, %r14
	movq	%r14, %rax
	leaq	(%rbx,%r14), %r15
	sarq	$4, %rax
	movq	%rax, -168(%rbp)
	cmpq	$224, %r14
	jle	.L3858
	movq	%rax, %rcx
	shrq	$63, %rax
	addq	%rcx, %rax
	sarq	%rax
	salq	$4, %rax
	movq	%rax, %rcx
	leaq	(%rbx,%rax), %r9
	sarq	$4, %rcx
	movq	%rcx, -176(%rbp)
	cmpq	$224, %rax
	jle	.L3859
	movq	%rax, -216(%rbp)
	movq	-176(%rbp), %rax
	movq	%rbx, %rdi
	movq	%r9, -184(%rbp)
	movq	%rax, %rcx
	shrq	$63, %rcx
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%rbx,%rcx), %r10
	movq	%rcx, -208(%rbp)
	movq	%r10, %rsi
	movq	%r10, -192(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-192(%rbp), %r10
	movq	-184(%rbp), %r9
	movq	%r10, %rdi
	movq	%r9, %rsi
	movq	%r10, -200(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-216(%rbp), %rax
	movq	%rbx, %rdi
	movq	-208(%rbp), %rcx
	movq	-184(%rbp), %r9
	movq	-200(%rbp), %r10
	movq	%rax, %r8
	movq	%rax, -192(%rbp)
	subq	%rcx, %r8
	movq	%r9, %rdx
	sarq	$4, %rcx
	movq	%r10, %rsi
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r9
.L3773:
	subq	%rax, %r14
	movq	%r14, %rax
	sarq	$4, %rax
	cmpq	$224, %r14
	jle	.L3860
	movq	%rax, %rcx
	movq	%r9, %rdi
	movq	%rax, -200(%rbp)
	shrq	$63, %rcx
	movq	%r9, -184(%rbp)
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r9,%rcx), %r14
	movq	%rcx, -192(%rbp)
	movq	%r14, %rsi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	%r15, %r8
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %rcx
	subq	%r14, %r8
	sarq	$4, %r8
	movq	%r9, %rdi
	sarq	$4, %rcx
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	movq	-200(%rbp), %rax
	movq	-184(%rbp), %r9
.L3775:
	movq	-176(%rbp), %rcx
	movq	%rax, %r8
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	%rbx, %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
.L3771:
	movq	-88(%rbp), %rax
	subq	%r15, %rax
	movq	%rax, %rcx
	sarq	$4, %rcx
	movq	%rcx, -176(%rbp)
	cmpq	$224, %rax
	jle	.L3861
	movq	%rcx, %r8
	shrq	$63, %r8
	addq	%rcx, %r8
	sarq	%r8
	salq	$4, %r8
	movq	%r8, %rax
	leaq	(%r15,%r8), %r14
	sarq	$4, %rax
	movq	%rax, -184(%rbp)
	cmpq	$224, %r8
	jle	.L3862
	movq	%rax, %rcx
	movq	%r15, %rdi
	movq	%r8, -208(%rbp)
	shrq	$63, %rcx
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r15,%rcx), %r10
	movq	%rcx, -200(%rbp)
	movq	%r10, %rsi
	movq	%r10, -192(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-192(%rbp), %r10
	movq	%r14, %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-200(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	-208(%rbp), %r8
	movq	-192(%rbp), %r10
	subq	%rcx, %r8
	movq	%r10, %rsi
	sarq	$4, %rcx
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
.L3779:
	movq	-88(%rbp), %rdx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$4, %rax
	cmpq	$224, %rdx
	jle	.L3863
	movq	%rax, %rcx
	movq	%r14, %rdi
	movq	%rax, -208(%rbp)
	shrq	$63, %rcx
	addq	%rax, %rcx
	sarq	%rcx
	salq	$4, %rcx
	leaq	(%r14,%rcx), %r10
	movq	%rcx, -200(%rbp)
	movq	%r10, %rsi
	movq	%r10, -192(%rbp)
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-192(%rbp), %r10
	movq	-88(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_
	movq	-88(%rbp), %rdx
	movq	-192(%rbp), %r10
	movq	%r14, %rdi
	movq	-200(%rbp), %rcx
	movq	%rdx, %r8
	movq	%r10, %rsi
	subq	%r10, %r8
	sarq	$4, %rcx
	sarq	$4, %r8
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	movq	-208(%rbp), %rax
.L3781:
	movq	-184(%rbp), %rcx
	movq	%rax, %r8
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-88(%rbp), %rdx
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
.L3777:
	movq	-176(%rbp), %r8
	movq	-168(%rbp), %rcx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	-88(%rbp), %rdx
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
.L3769:
	movq	-160(%rbp), %r8
	movq	-152(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	-88(%rbp), %rdx
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	jmp	.L3755
.L3850:
	movq	-88(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
.L3755:
	movq	-136(%rbp), %r8
	movq	-144(%rbp), %rcx
	movq	%r13, %rsi
	movq	-88(%rbp), %rdx
	movq	-104(%rbp), %rdi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_SL_T0_SM_T1_
	jmp	.L3723
.L3853:
	movq	%r9, %rsi
	movq	%r13, %rdi
	movq	%rax, -192(%rbp)
	movq	%r11, -184(%rbp)
	movq	%r9, -176(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	movq	-176(%rbp), %r9
	movq	-184(%rbp), %r11
	movq	-192(%rbp), %rax
	jmp	.L3761
.L3823:
	movq	-104(%rbp), %rdi
	movq	%r10, %rsi
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%r10, -176(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %rax
	movq	-192(%rbp), %r9
	jmp	.L3731
.L3851:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	jmp	.L3757
.L3837:
	movq	-104(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -152(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-152(%rbp), %rax
	jmp	.L3692
.L3821:
	movq	-104(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	jmp	.L3727
.L3844:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	jmp	.L3706
.L3840:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	jmp	.L3698
.L3857:
	movq	-88(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	jmp	.L3769
.L3847:
	movq	-88(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	jmp	.L3712
.L3828:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	jmp	.L3741
.L3846:
	movq	%r9, %rdi
	movq	%r13, %rsi
	movq	%r10, -168(%rbp)
	movq	%r9, -160(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-160(%rbp), %r9
	movq	-168(%rbp), %r10
	jmp	.L3710
.L3845:
	movq	%r9, %rsi
	movq	%r15, %rdi
	movq	%r10, -176(%rbp)
	movq	%rax, -168(%rbp)
	movq	%r9, -160(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-160(%rbp), %r9
	movq	-168(%rbp), %rax
	movq	-176(%rbp), %r10
	jmp	.L3708
.L3848:
	movq	%r10, %rsi
	movq	%r13, %rdi
	movq	%r10, -168(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-168(%rbp), %r10
	jmp	.L3714
.L3829:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	jmp	.L3743
.L3825:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	jmp	.L3735
.L3852:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r11, -168(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	movq	-168(%rbp), %r11
	jmp	.L3759
.L3832:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	jmp	.L3749
.L3841:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	jmp	.L3700
.L3855:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	jmp	.L3765
.L3861:
	movq	-88(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	jmp	.L3777
.L3858:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	jmp	.L3771
.L3849:
	movq	-88(%rbp), %rsi
	movq	%r10, %rdi
	movq	%rax, -176(%rbp)
	movq	%r10, -168(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-168(%rbp), %r10
	movq	-176(%rbp), %rax
	jmp	.L3716
.L3822:
	movq	-104(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, -160(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	movq	-160(%rbp), %rax
	jmp	.L3729
.L3842:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, -168(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-168(%rbp), %rax
	jmp	.L3702
.L3839:
	movq	%r10, %rdi
	movq	%r13, %rsi
	movq	%r9, -176(%rbp)
	movq	%rax, -168(%rbp)
	movq	%r10, -160(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-160(%rbp), %r10
	movq	-168(%rbp), %rax
	movq	-176(%rbp), %r9
	jmp	.L3696
.L3838:
	movq	-104(%rbp), %rdi
	movq	%r10, %rsi
	movq	%r9, -176(%rbp)
	movq	%rax, -168(%rbp)
	movq	%r10, -160(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNKS4_10SwitchInfo18CasesSortedByValueEvEUlS5_S5_E_EEEvT_SG_T0_
	movq	-160(%rbp), %r10
	movq	-168(%rbp), %rax
	movq	-176(%rbp), %r9
	jmp	.L3694
.L3863:
	movq	-88(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -192(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	movq	-192(%rbp), %rax
	jmp	.L3781
.L3862:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	jmp	.L3779
.L3824:
	movq	%r10, %rdi
	movq	%r14, %rsi
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%r10, -176(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %rax
	movq	-192(%rbp), %r9
	jmp	.L3733
.L3834:
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -184(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	movq	-184(%rbp), %rax
	jmp	.L3753
.L3833:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	jmp	.L3751
.L3856:
	movq	%r9, %rdi
	movq	%rbx, %rsi
	movq	%rax, -184(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	movq	-176(%rbp), %r9
	movq	-184(%rbp), %rax
	jmp	.L3767
.L3859:
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%rax, -192(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %rax
	jmp	.L3773
.L3854:
	movq	%r9, %rdi
	movq	%r14, %rsi
	movq	%rax, -192(%rbp)
	movq	%r11, -184(%rbp)
	movq	%r9, -176(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	movq	-176(%rbp), %r9
	movq	-184(%rbp), %r11
	movq	-192(%rbp), %rax
	jmp	.L3763
.L3860:
	movq	%r9, %rdi
	movq	%r15, %rsi
	movq	%rax, -192(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %rax
	jmp	.L3775
.L3831:
	movq	%r9, %rdi
	movq	%r15, %rsi
	movq	%rax, -184(%rbp)
	movq	%r9, -176(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	movq	-176(%rbp), %r9
	movq	-184(%rbp), %rax
	jmp	.L3747
.L3830:
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%rax, -184(%rbp)
	movq	%r9, -176(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	movq	-176(%rbp), %r9
	movq	-184(%rbp), %rax
	jmp	.L3745
.L3827:
	movq	%r9, %rdi
	movq	%rbx, %rsi
	movq	%rax, -192(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %rax
	jmp	.L3739
.L3826:
	movq	%r9, %rsi
	movq	%r14, %rdi
	movq	%r9, -184(%rbp)
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal8compiler8CaseInfoESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_19InstructionSelector22EmitBinarySearchSwitchERKNS4_10SwitchInfoERNS4_18InstructionOperandEEUlS5_S5_E_EEEvT_SL_T0_.isra.0
	movq	-184(%rbp), %r9
	jmp	.L3737
.L3817:
	call	__stack_chk_fail@PLT
.L3683:
	sarq	%r13
	jne	.L3685
	jmp	.L3682
.L3814:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE25156:
	.size	_ZN2v88internal8compiler19InstructionSelector22EmitBinarySearchSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE, .-_ZN2v88internal8compiler19InstructionSelector22EmitBinarySearchSwitchERKNS1_10SwitchInfoERNS1_18InstructionOperandE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler19InstructionSelectorC2EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler19InstructionSelectorC2EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler19InstructionSelectorC2EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE:
.LFB32025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE32025:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler19InstructionSelectorC2EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE, .-_GLOBAL__sub_I__ZN2v88internal8compiler19InstructionSelectorC2EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler19InstructionSelectorC2EPNS0_4ZoneEmPNS1_7LinkageEPNS1_19InstructionSequenceEPNS1_8ScheduleEPNS1_19SourcePositionTableEPNS1_5FrameENS2_21EnableSwitchJumpTableEPNS0_11TickCounterEPmNS2_18SourcePositionModeENS2_8FeaturesENS2_16EnableSchedulingENS2_29EnableRootsRelativeAddressingENS0_24PoisoningMitigationLevelENS2_20EnableTraceTurboJsonE
	.section	.rodata.CSWTCH.528,"a"
	.align 32
	.type	CSWTCH.528, @object
	.size	CSWTCH.528, 56
CSWTCH.528:
	.long	0
	.long	0
	.long	1
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	2
	.long	3
	.long	4
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	-1
	.long	0
	.long	-1
	.long	0
	.align 16
.LC8:
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.align 16
.LC9:
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC15:
	.long	0
	.long	-1042284544
	.align 8
.LC16:
	.long	4290772992
	.long	1105199103
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
