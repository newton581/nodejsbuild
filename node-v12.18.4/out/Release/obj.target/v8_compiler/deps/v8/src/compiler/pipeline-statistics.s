	.file	"pipeline-statistics.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4754:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4754:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4755:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4755:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v88internal8compiler18PipelineStatistics11CommonStats5BeginEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PipelineStatistics11CommonStats5BeginEPS2_
	.type	_ZN2v88internal8compiler18PipelineStatistics11CommonStats5BeginEPS2_, @function
_ZN2v88internal8compiler18PipelineStatistics11CommonStats5BeginEPS2_:
.LFB8505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$72, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	call	_Znwm@PLT
	movq	8(%r13), %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler9ZoneStats10StatsScopeC1EPS2_@PLT
	movq	(%r12), %r14
	movq	%rbx, (%r12)
	testq	%r14, %r14
	je	.L5
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10StatsScopeD1Ev@PLT
	movl	$72, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L5:
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	xorl	%ebx, %ebx
	movq	%rax, 8(%r12)
	movq	0(%r13), %rax
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L6
	movq	16(%rax), %rcx
	leaq	-24(%rcx), %rbx
	subq	%rdx, %rbx
.L6:
	addq	(%rax), %rbx
	movq	%rbx, 16(%r12)
	movq	8(%r13), %rdi
	movq	72(%r13), %r14
	call	_ZNK2v88internal8compiler9ZoneStats24GetCurrentAllocatedBytesEv@PLT
	subq	%r14, %rax
	addq	%rax, %rbx
	movq	%rbx, 24(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8505:
	.size	_ZN2v88internal8compiler18PipelineStatistics11CommonStats5BeginEPS2_, .-_ZN2v88internal8compiler18PipelineStatistics11CommonStats5BeginEPS2_
	.section	.text._ZN2v88internal8compiler18PipelineStatistics11CommonStats3EndEPS2_PNS0_21CompilationStatistics10BasicStatsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PipelineStatistics11CommonStats3EndEPS2_PNS0_21CompilationStatistics10BasicStatsE
	.type	_ZN2v88internal8compiler18PipelineStatistics11CommonStats3EndEPS2_PNS0_21CompilationStatistics10BasicStatsE, @function
_ZN2v88internal8compiler18PipelineStatistics11CommonStats3EndEPS2_PNS0_21CompilationStatistics10BasicStatsE:
.LFB8506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	addq	$24, %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	32(%rdx), %rdi
	subq	$8, %rsp
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	call	_ZN2v84base9TimeTicks17HighResolutionNowEv@PLT
	subq	8(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	(%r12), %rax
	xorl	%r12d, %r12d
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L14
	movq	16(%rax), %rcx
	leaq	-24(%rcx), %r12
	subq	%rdx, %r12
.L14:
	movq	(%rbx), %rdi
	addq	(%rax), %r12
	subq	16(%rbx), %r12
	call	_ZN2v88internal8compiler9ZoneStats10StatsScope20GetMaxAllocatedBytesEv@PLT
	addq	%r12, %rax
	movq	%rax, 16(%r13)
	addq	24(%rbx), %rax
	movq	%rax, 24(%r13)
	movq	(%rbx), %rdi
	call	_ZN2v88internal8compiler9ZoneStats10StatsScope22GetTotalAllocatedBytesEv@PLT
	addq	%rax, %r12
	movq	%r12, 8(%r13)
	movq	(%rbx), %r12
	movq	$0, (%rbx)
	testq	%r12, %r12
	je	.L15
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10StatsScopeD1Ev@PLT
	movl	$72, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L15:
	movq	$0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8506:
	.size	_ZN2v88internal8compiler18PipelineStatistics11CommonStats3EndEPS2_PNS0_21CompilationStatistics10BasicStatsE, .-_ZN2v88internal8compiler18PipelineStatistics11CommonStats3EndEPS2_PNS0_21CompilationStatistics10BasicStatsE
	.section	.text._ZN2v88internal8compiler18PipelineStatisticsC2EPNS0_24OptimizedCompilationInfoEPNS0_21CompilationStatisticsEPNS1_9ZoneStatsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PipelineStatisticsC2EPNS0_24OptimizedCompilationInfoEPNS0_21CompilationStatisticsEPNS1_9ZoneStatsE
	.type	_ZN2v88internal8compiler18PipelineStatisticsC2EPNS0_24OptimizedCompilationInfoEPNS0_21CompilationStatisticsEPNS1_9ZoneStatsE, @function
_ZN2v88internal8compiler18PipelineStatisticsC2EPNS0_24OptimizedCompilationInfoEPNS0_21CompilationStatisticsEPNS1_9ZoneStatsE:
.LFB8511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	56(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%rsi), %rax
	movq	$0, 32(%rdi)
	movq	$0, 56(%rdi)
	movq	%rax, (%rdi)
	leaq	40(%rdi), %rax
	movq	%rax, 24(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 112(%rdi)
	movq	$0, 120(%rdi)
	movq	$0, 152(%rdi)
	movq	$0, 160(%rdi)
	movq	24(%rsi), %rax
	movq	%rcx, 8(%rdi)
	movq	%rdx, 16(%rdi)
	movb	$0, 40(%rdi)
	movq	$0, 88(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 136(%rdi)
	testq	%rax, %rax
	je	.L24
	leaq	-64(%rbp), %r15
	movq	(%rax), %rax
	leaq	24(%rdi), %r14
	movq	%rsi, %rbx
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo10SourceSizeEv@PLT
	movq	%r15, %rdi
	cltq
	movq	%rax, 88(%r12)
	movq	24(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	leaq	-72(%rbp), %rsi
	xorl	%r8d, %r8d
	leaq	-80(%rbp), %rdi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-80(%rbp), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	movq	32(%r12), %rdx
	movq	%r14, %rdi
	movq	%r15, %rcx
	movq	%rax, %r8
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	_ZdaPv@PLT
.L24:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18PipelineStatistics11CommonStats5BeginEPS2_
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L34:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8511:
	.size	_ZN2v88internal8compiler18PipelineStatisticsC2EPNS0_24OptimizedCompilationInfoEPNS0_21CompilationStatisticsEPNS1_9ZoneStatsE, .-_ZN2v88internal8compiler18PipelineStatisticsC2EPNS0_24OptimizedCompilationInfoEPNS0_21CompilationStatisticsEPNS1_9ZoneStatsE
	.globl	_ZN2v88internal8compiler18PipelineStatisticsC1EPNS0_24OptimizedCompilationInfoEPNS0_21CompilationStatisticsEPNS1_9ZoneStatsE
	.set	_ZN2v88internal8compiler18PipelineStatisticsC1EPNS0_24OptimizedCompilationInfoEPNS0_21CompilationStatisticsEPNS1_9ZoneStatsE,_ZN2v88internal8compiler18PipelineStatisticsC2EPNS0_24OptimizedCompilationInfoEPNS0_21CompilationStatisticsEPNS1_9ZoneStatsE
	.section	.text._ZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEv
	.type	_ZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEv, @function
_ZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEv:
.LFB8517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-112(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdx
	addq	$104, %rdi
	movq	%rbx, %rsi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -104(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	call	_ZN2v88internal8compiler18PipelineStatistics11CommonStats3EndEPS2_PNS0_21CompilationStatistics10BasicStatsE
	movq	96(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal21CompilationStatistics20RecordPhaseKindStatsEPKcRKNS1_10BasicStatsE@PLT
	movq	_ZZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEvE27trace_event_unique_atomic97(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L56
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L57
.L39:
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L59
.L38:
	movq	%r12, _ZZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEvE27trace_event_unique_atomic97(%rip)
	movzbl	(%r12), %eax
	testb	$5, %al
	je	.L39
.L57:
	pxor	%xmm0, %xmm0
	movq	96(%rbx), %r14
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L60
.L40:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L41
	movq	(%rdi), %rax
	call	*8(%rax)
.L41:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L39
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	_ZN2v88internal8compiler12_GLOBAL__N_1L14kTraceCategoryE(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L60:
	subq	$8, %rsp
	leaq	-128(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r14, %rcx
	movl	$69, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L40
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8517:
	.size	_ZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEv, .-_ZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEv
	.section	.text._ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc
	.type	_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc, @function
_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc:
.LFB8516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 104(%rdi)
	je	.L62
	call	_ZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEv
.L62:
	movq	_ZZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKcE27trace_event_unique_atomic87(%rip), %rdx
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L82
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L83
.L66:
	movq	%rbx, 96(%r12)
	leaq	104(%r12), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18PipelineStatistics11CommonStats5BeginEPS2_
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L85
.L65:
	movq	%r13, _ZZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKcE27trace_event_unique_atomic87(%rip)
	movzbl	0(%r13), %eax
	testb	$5, %al
	je	.L66
.L83:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L86
.L67:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L68
	movq	(%rdi), %rax
	call	*8(%rax)
.L68:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L66
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	_ZN2v88internal8compiler12_GLOBAL__N_1L14kTraceCategoryE(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L86:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%rbx, %rcx
	movl	$66, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L67
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8516:
	.size	_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc, .-_ZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKc
	.section	.text._ZN2v88internal8compiler18PipelineStatisticsD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PipelineStatisticsD2Ev
	.type	_ZN2v88internal8compiler18PipelineStatisticsD2Ev, @function
_ZN2v88internal8compiler18PipelineStatisticsD2Ev:
.LFB8514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 104(%rdi)
	je	.L88
	call	_ZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEv
.L88:
	leaq	-112(%rbp), %r13
	pxor	%xmm0, %xmm0
	leaq	56(%rbx), %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	leaq	-64(%rbp), %r12
	movups	%xmm0, -104(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -88(%rbp)
	movq	%r12, -80(%rbp)
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	call	_ZN2v88internal8compiler18PipelineStatistics11CommonStats3EndEPS2_PNS0_21CompilationStatistics10BasicStatsE
	movq	16(%rbx), %rdi
	movq	88(%rbx), %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal21CompilationStatistics16RecordTotalStatsEmRKNS1_10BasicStatsE@PLT
	movq	-80(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L89
	call	_ZdlPv@PLT
.L89:
	movq	144(%rbx), %r12
	testq	%r12, %r12
	je	.L90
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10StatsScopeD1Ev@PLT
	movl	$72, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L90:
	movq	104(%rbx), %r12
	testq	%r12, %r12
	je	.L91
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10StatsScopeD1Ev@PLT
	movl	$72, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L91:
	movq	56(%rbx), %r12
	testq	%r12, %r12
	je	.L92
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler9ZoneStats10StatsScopeD1Ev@PLT
	movl	$72, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L92:
	movq	24(%rbx), %rdi
	addq	$40, %rbx
	cmpq	%rbx, %rdi
	je	.L87
	call	_ZdlPv@PLT
.L87:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L105:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8514:
	.size	_ZN2v88internal8compiler18PipelineStatisticsD2Ev, .-_ZN2v88internal8compiler18PipelineStatisticsD2Ev
	.globl	_ZN2v88internal8compiler18PipelineStatisticsD1Ev
	.set	_ZN2v88internal8compiler18PipelineStatisticsD1Ev,_ZN2v88internal8compiler18PipelineStatisticsD2Ev
	.section	.text._ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc
	.type	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc, @function
_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc:
.LFB8518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKcE28trace_event_unique_atomic101(%rip), %rdx
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L126
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L127
.L110:
	movq	%rbx, 136(%r12)
	leaq	144(%r12), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18PipelineStatistics11CommonStats5BeginEPS2_
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L128
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L129
.L109:
	movq	%r13, _ZZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKcE28trace_event_unique_atomic101(%rip)
	movzbl	0(%r13), %eax
	testb	$5, %al
	je	.L110
.L127:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L130
.L111:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L112
	movq	(%rdi), %rax
	call	*8(%rax)
.L112:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L110
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L129:
	leaq	_ZN2v88internal8compiler12_GLOBAL__N_1L14kTraceCategoryE(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L130:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%rbx, %rcx
	movl	$66, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L111
.L128:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8518:
	.size	_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc, .-_ZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKc
	.section	.text._ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv
	.type	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv, @function
_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv:
.LFB8519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-112(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdx
	addq	$144, %rdi
	movq	%rbx, %rsi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -104(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -72(%rbp)
	movb	$0, -64(%rbp)
	call	_ZN2v88internal8compiler18PipelineStatistics11CommonStats3EndEPS2_PNS0_21CompilationStatistics10BasicStatsE
	movq	96(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%r12, %rcx
	movq	136(%rbx), %rdx
	call	_ZN2v88internal21CompilationStatistics16RecordPhaseStatsEPKcS3_RKNS1_10BasicStatsE@PLT
	movq	_ZZN2v88internal8compiler18PipelineStatistics8EndPhaseEvE28trace_event_unique_atomic112(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L152
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L153
.L135:
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L131
	call	_ZdlPv@PLT
.L131:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L155
.L134:
	movq	%r12, _ZZN2v88internal8compiler18PipelineStatistics8EndPhaseEvE28trace_event_unique_atomic112(%rip)
	movzbl	(%r12), %eax
	testb	$5, %al
	je	.L135
.L153:
	pxor	%xmm0, %xmm0
	movq	136(%rbx), %r14
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L156
.L136:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L137
	movq	(%rdi), %rax
	call	*8(%rax)
.L137:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L135
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L155:
	leaq	_ZN2v88internal8compiler12_GLOBAL__N_1L14kTraceCategoryE(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L156:
	subq	$8, %rsp
	leaq	-128(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r14, %rcx
	movl	$69, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L136
.L154:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8519:
	.size	_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv, .-_ZN2v88internal8compiler18PipelineStatistics8EndPhaseEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler18PipelineStatistics11CommonStats5BeginEPS2_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler18PipelineStatistics11CommonStats5BeginEPS2_, @function
_GLOBAL__sub_I__ZN2v88internal8compiler18PipelineStatistics11CommonStats5BeginEPS2_:
.LFB9601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE9601:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler18PipelineStatistics11CommonStats5BeginEPS2_, .-_GLOBAL__sub_I__ZN2v88internal8compiler18PipelineStatistics11CommonStats5BeginEPS2_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler18PipelineStatistics11CommonStats5BeginEPS2_
	.section	.bss._ZZN2v88internal8compiler18PipelineStatistics8EndPhaseEvE28trace_event_unique_atomic112,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8compiler18PipelineStatistics8EndPhaseEvE28trace_event_unique_atomic112, @object
	.size	_ZZN2v88internal8compiler18PipelineStatistics8EndPhaseEvE28trace_event_unique_atomic112, 8
_ZZN2v88internal8compiler18PipelineStatistics8EndPhaseEvE28trace_event_unique_atomic112:
	.zero	8
	.section	.bss._ZZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKcE28trace_event_unique_atomic101,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKcE28trace_event_unique_atomic101, @object
	.size	_ZZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKcE28trace_event_unique_atomic101, 8
_ZZN2v88internal8compiler18PipelineStatistics10BeginPhaseEPKcE28trace_event_unique_atomic101:
	.zero	8
	.section	.bss._ZZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEvE27trace_event_unique_atomic97,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEvE27trace_event_unique_atomic97, @object
	.size	_ZZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEvE27trace_event_unique_atomic97, 8
_ZZN2v88internal8compiler18PipelineStatistics12EndPhaseKindEvE27trace_event_unique_atomic97:
	.zero	8
	.section	.bss._ZZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKcE27trace_event_unique_atomic87,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKcE27trace_event_unique_atomic87, @object
	.size	_ZZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKcE27trace_event_unique_atomic87, 8
_ZZN2v88internal8compiler18PipelineStatistics14BeginPhaseKindEPKcE27trace_event_unique_atomic87:
	.zero	8
	.section	.rodata._ZN2v88internal8compiler12_GLOBAL__N_1L14kTraceCategoryE,"a"
	.align 32
	.type	_ZN2v88internal8compiler12_GLOBAL__N_1L14kTraceCategoryE, @object
	.size	_ZN2v88internal8compiler12_GLOBAL__N_1L14kTraceCategoryE, 60
_ZN2v88internal8compiler12_GLOBAL__N_1L14kTraceCategoryE:
	.string	"disabled-by-default-v8.turbofan,disabled-by-default-v8.wasm"
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
