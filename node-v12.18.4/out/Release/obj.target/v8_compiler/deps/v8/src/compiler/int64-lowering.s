	.file	"int64-lowering.cc"
	.text
	.section	.rodata._ZN2v88internal8compiler13Int64LoweringC2EPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderEPNS0_4ZoneEPNS0_9SignatureINS0_21MachineRepresentationEEESt10unique_ptrINS1_24Int64LoweringSpecialCaseESt14default_deleteISG_EE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"placeholder"
	.section	.text._ZN2v88internal8compiler13Int64LoweringC2EPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderEPNS0_4ZoneEPNS0_9SignatureINS0_21MachineRepresentationEEESt10unique_ptrINS1_24Int64LoweringSpecialCaseESt14default_deleteISG_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64LoweringC2EPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderEPNS0_4ZoneEPNS0_9SignatureINS0_21MachineRepresentationEEESt10unique_ptrINS1_24Int64LoweringSpecialCaseESt14default_deleteISG_EE
	.type	_ZN2v88internal8compiler13Int64LoweringC2EPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderEPNS0_4ZoneEPNS0_9SignatureINS0_21MachineRepresentationEEESt10unique_ptrINS1_24Int64LoweringSpecialCaseESt14default_deleteISG_EE, @function
_ZN2v88internal8compiler13Int64LoweringC2EPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderEPNS0_4ZoneEPNS0_9SignatureINS0_21MachineRepresentationEEESt10unique_ptrINS1_24Int64LoweringSpecialCaseESt14default_deleteISG_EE:
.LFB15536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %xmm0
	movq	%rsi, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$40, %rsp
	movq	%rcx, -72(%rbp)
	movq	16(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -32(%rdi)
	movq	%rdx, %xmm0
	movl	$3, %edx
	movhps	-72(%rbp), %xmm0
	movups	%xmm0, -16(%rdi)
	call	_ZN2v88internal8compiler14NodeMarkerBaseC2EPNS1_5GraphEj@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	movups	%xmm0, 72(%rbx)
	movups	%xmm0, 88(%rbx)
	movups	%xmm0, 104(%rbx)
	movups	%xmm0, 120(%rbx)
	movq	16(%r13), %rdx
	movq	24(%r13), %rcx
	movq	$8, 64(%rbx)
	subq	%rdx, %rcx
	cmpq	$63, %rcx
	jbe	.L15
	leaq	64(%rdx), %rax
	movq	%rdx, 56(%rbx)
	addq	$24, %rdx
	movq	%rax, 16(%r13)
.L4:
	movq	40(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	$511, %rcx
	jbe	.L5
	leaq	512(%rax), %rcx
	movq	%rcx, 16(%rdi)
.L7:
	movq	%rax, (%rdx)
	movq	-72(%rbp), %rdi
	movl	$-2, %esi
	movq	%rdx, 96(%rbx)
	movq	(%rdx), %rcx
	movq	%rdx, 128(%rbx)
	leaq	512(%rcx), %rax
	movq	%rcx, 80(%rbx)
	movq	%rax, 88(%rbx)
	movq	(%rdx), %rax
	movq	%rcx, 72(%rbx)
	leaq	512(%rax), %rdx
	movq	%r14, 144(%rbx)
	movq	8(%r12), %r14
	movq	%rdx, 120(%rbx)
	leaq	.LC0(%rip), %rdx
	movq	%rax, 112(%rbx)
	movq	%rax, 104(%rbx)
	movq	$0, 136(%rbx)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	movq	%r12, %rdi
	leaq	-64(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, 152(%rbx)
	movq	(%r15), %rax
	movq	$0, (%r15)
	movq	%rax, 160(%rbx)
	movl	28(%r12), %esi
	movq	16(%r13), %rdi
	movq	24(%r13), %rax
	salq	$4, %rsi
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	ja	.L16
	addq	%rdi, %rsi
	movq	%rsi, 16(%r13)
.L10:
	movl	28(%r12), %edx
	movq	%rdi, 136(%rbx)
	xorl	%esi, %esi
	salq	$4, %rdx
	call	memset@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L17
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	movq	64(%rbx), %rax
	movq	%rcx, 56(%rbx)
	leaq	-4(,%rax,4), %rdx
	movq	48(%rbx), %rax
	andq	$-8, %rdx
	addq	%rcx, %rdx
	testq	%rax, %rax
	je	.L4
	cmpq	$31, 8(%rax)
	jbe	.L4
	movq	(%rax), %rcx
	movq	%rcx, 48(%rbx)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$512, %esi
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %rdx
	jmp	.L7
.L17:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15536:
	.size	_ZN2v88internal8compiler13Int64LoweringC2EPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderEPNS0_4ZoneEPNS0_9SignatureINS0_21MachineRepresentationEEESt10unique_ptrINS1_24Int64LoweringSpecialCaseESt14default_deleteISG_EE, .-_ZN2v88internal8compiler13Int64LoweringC2EPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderEPNS0_4ZoneEPNS0_9SignatureINS0_21MachineRepresentationEEESt10unique_ptrINS1_24Int64LoweringSpecialCaseESt14default_deleteISG_EE
	.globl	_ZN2v88internal8compiler13Int64LoweringC1EPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderEPNS0_4ZoneEPNS0_9SignatureINS0_21MachineRepresentationEEESt10unique_ptrINS1_24Int64LoweringSpecialCaseESt14default_deleteISG_EE
	.set	_ZN2v88internal8compiler13Int64LoweringC1EPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderEPNS0_4ZoneEPNS0_9SignatureINS0_21MachineRepresentationEEESt10unique_ptrINS1_24Int64LoweringSpecialCaseESt14default_deleteISG_EE,_ZN2v88internal8compiler13Int64LoweringC2EPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderEPNS0_4ZoneEPNS0_9SignatureINS0_21MachineRepresentationEEESt10unique_ptrINS1_24Int64LoweringSpecialCaseESt14default_deleteISG_EE
	.section	.text._ZN2v88internal8compiler13Int64Lowering30GetParameterCountAfterLoweringEPNS0_9SignatureINS0_21MachineRepresentationEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering30GetParameterCountAfterLoweringEPNS0_9SignatureINS0_21MachineRepresentationEEE
	.type	_ZN2v88internal8compiler13Int64Lowering30GetParameterCountAfterLoweringEPNS0_9SignatureINS0_21MachineRepresentationEEE, @function
_ZN2v88internal8compiler13Int64Lowering30GetParameterCountAfterLoweringEPNS0_9SignatureINS0_21MachineRepresentationEEE:
.LFB15545:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	movl	%edx, %eax
	testl	%edx, %edx
	jle	.L18
	leal	-1(%rdx), %eax
	movq	(%rdi), %rsi
	movq	16(%rdi), %rdi
	cmpl	$14, %eax
	jbe	.L40
	movl	%edx, %ecx
	pxor	%xmm1, %xmm1
	pxor	%xmm5, %xmm5
	addq	%rdi, %rsi
	shrl	$4, %ecx
	movdqa	.LC1(%rip), %xmm7
	movq	%rsi, %rax
	pxor	%xmm4, %xmm4
	salq	$4, %rcx
	movdqa	.LC2(%rip), %xmm6
	addq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L22:
	movdqu	(%rax), %xmm0
	movdqa	%xmm5, %xmm3
	addq	$16, %rax
	pcmpeqb	%xmm7, %xmm0
	pand	%xmm6, %xmm0
	pcmpgtb	%xmm0, %xmm3
	movdqa	%xmm0, %xmm2
	punpcklbw	%xmm3, %xmm2
	punpckhbw	%xmm3, %xmm0
	movdqa	%xmm4, %xmm3
	pcmpgtw	%xmm2, %xmm3
	movdqa	%xmm2, %xmm8
	punpcklwd	%xmm3, %xmm8
	punpckhwd	%xmm3, %xmm2
	movdqa	%xmm0, %xmm3
	paddd	%xmm8, %xmm1
	paddd	%xmm2, %xmm1
	movdqa	%xmm4, %xmm2
	pcmpgtw	%xmm0, %xmm2
	punpcklwd	%xmm2, %xmm3
	punpckhwd	%xmm2, %xmm0
	paddd	%xmm3, %xmm1
	paddd	%xmm0, %xmm1
	cmpq	%rcx, %rax
	jne	.L22
	movdqa	%xmm1, %xmm0
	movl	%edx, %ecx
	psrldq	$8, %xmm0
	andl	$-16, %ecx
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	addl	%edx, %eax
	cmpl	%ecx, %edx
	je	.L56
.L20:
	movslq	%ecx, %rdi
	cmpb	$5, (%rsi,%rdi)
	sete	%dil
	movzbl	%dil, %edi
	addl	%edi, %eax
	leal	1(%rcx), %edi
	cmpl	%edi, %edx
	jle	.L18
	movslq	%edi, %rdi
	cmpb	$5, (%rsi,%rdi)
	sete	%dil
	movzbl	%dil, %edi
	addl	%edi, %eax
	leal	2(%rcx), %edi
	cmpl	%edi, %edx
	jle	.L18
	movslq	%edi, %rdi
	cmpb	$5, (%rsi,%rdi)
	sete	%dil
	movzbl	%dil, %edi
	addl	%edi, %eax
	leal	3(%rcx), %edi
	cmpl	%edi, %edx
	jle	.L18
	movslq	%edi, %rdi
	cmpb	$5, (%rsi,%rdi)
	sete	%dil
	movzbl	%dil, %edi
	addl	%edi, %eax
	leal	4(%rcx), %edi
	cmpl	%edi, %edx
	jle	.L18
	movslq	%edi, %rdi
	cmpb	$5, (%rsi,%rdi)
	sete	%dil
	movzbl	%dil, %edi
	addl	%edi, %eax
	leal	5(%rcx), %edi
	cmpl	%edi, %edx
	jle	.L18
	movslq	%edi, %rdi
	cmpb	$5, (%rsi,%rdi)
	sete	%dil
	movzbl	%dil, %edi
	addl	%edi, %eax
	leal	6(%rcx), %edi
	cmpl	%edi, %edx
	jle	.L18
	movslq	%edi, %rdi
	cmpb	$5, (%rsi,%rdi)
	sete	%dil
	movzbl	%dil, %edi
	addl	%edi, %eax
	leal	7(%rcx), %edi
	cmpl	%edi, %edx
	jle	.L18
	movslq	%edi, %rdi
	cmpb	$5, (%rsi,%rdi)
	sete	%dil
	movzbl	%dil, %edi
	addl	%edi, %eax
	leal	8(%rcx), %edi
	cmpl	%edi, %edx
	jle	.L18
	movslq	%edi, %rdi
	cmpb	$5, (%rsi,%rdi)
	sete	%dil
	movzbl	%dil, %edi
	addl	%edi, %eax
	leal	9(%rcx), %edi
	cmpl	%edi, %edx
	jle	.L18
	movslq	%edi, %rdi
	cmpb	$5, (%rsi,%rdi)
	sete	%dil
	movzbl	%dil, %edi
	addl	%edi, %eax
	leal	10(%rcx), %edi
	cmpl	%edi, %edx
	jle	.L18
	movslq	%edi, %rdi
	cmpb	$5, (%rsi,%rdi)
	sete	%dil
	movzbl	%dil, %edi
	addl	%edi, %eax
	leal	11(%rcx), %edi
	cmpl	%edi, %edx
	jle	.L18
	movslq	%edi, %rdi
	cmpb	$5, (%rsi,%rdi)
	sete	%dil
	movzbl	%dil, %edi
	addl	%edi, %eax
	leal	12(%rcx), %edi
	cmpl	%edi, %edx
	jle	.L18
	movslq	%edi, %rdi
	cmpb	$5, (%rsi,%rdi)
	sete	%dil
	movzbl	%dil, %edi
	addl	%edi, %eax
	leal	13(%rcx), %edi
	cmpl	%edi, %edx
	jle	.L18
	movslq	%edi, %rdi
	cmpb	$5, (%rsi,%rdi)
	sete	%dil
	addl	$14, %ecx
	movzbl	%dil, %edi
	addl	%edi, %eax
	cmpl	%ecx, %edx
	jle	.L18
	movslq	%ecx, %rcx
	xorl	%edx, %edx
	cmpb	$5, (%rsi,%rcx)
	sete	%dl
	addl	%edx, %eax
.L18:
	ret
.L40:
	movl	%edx, %eax
	xorl	%ecx, %ecx
	addq	%rdi, %rsi
	jmp	.L20
.L56:
	ret
	.cfi_endproc
.LFE15545:
	.size	_ZN2v88internal8compiler13Int64Lowering30GetParameterCountAfterLoweringEPNS0_9SignatureINS0_21MachineRepresentationEEE, .-_ZN2v88internal8compiler13Int64Lowering30GetParameterCountAfterLoweringEPNS0_9SignatureINS0_21MachineRepresentationEEE
	.section	.text._ZN2v88internal8compiler13Int64Lowering13GetIndexNodesEPNS1_4NodeEPS4_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering13GetIndexNodesEPNS1_4NodeEPS4_S5_
	.type	_ZN2v88internal8compiler13Int64Lowering13GetIndexNodesEPNS1_4NodeEPS4_S5_, @function
_ZN2v88internal8compiler13Int64Lowering13GetIndexNodesEPNS1_4NodeEPS4_S5_:
.LFB15546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rsi, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, (%rdx)
	movq	8(%rdi), %r13
	movl	$4, %esi
	movq	24(%rdi), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	xorl	%r8d, %r8d
	leaq	-64(%rbp), %rcx
	movq	%r13, %rdi
	movq	-80(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-72(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, (%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L60:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15546:
	.size	_ZN2v88internal8compiler13Int64Lowering13GetIndexNodesEPNS1_4NodeEPS4_S5_, .-_ZN2v88internal8compiler13Int64Lowering13GetIndexNodesEPNS1_4NodeEPS4_S5_
	.section	.text._ZN2v88internal8compiler13Int64Lowering15LowerComparisonEPNS1_4NodeEPKNS1_8OperatorES7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering15LowerComparisonEPNS1_4NodeEPKNS1_8OperatorES7_
	.type	_ZN2v88internal8compiler13Int64Lowering15LowerComparisonEPNS1_4NodeEPKNS1_8OperatorES7_, @function
_ZN2v88internal8compiler13Int64Lowering15LowerComparisonEPNS1_4NodeEPKNS1_8OperatorES7_:
.LFB15551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rcx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	32(%r12), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	23(%r12), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L62
	leaq	40(%r12), %rax
.L63:
	movq	(%rax), %r10
	movl	20(%r13), %edx
	leaq	-80(%rbp), %r15
	xorl	%r8d, %r8d
	movq	136(%rbx), %rcx
	movq	8(%rbx), %r14
	movq	%r9, -128(%rbp)
	movl	20(%r10), %eax
	andl	$16777215, %edx
	movq	%r10, -88(%rbp)
	salq	$4, %rdx
	movq	%r14, %rdi
	movq	(%rcx,%rdx), %xmm0
	andl	$16777215, %eax
	movl	$2, %edx
	salq	$4, %rax
	movhps	(%rcx,%rax), %xmm0
	movq	%r15, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-88(%rbp), %r10
	movq	136(%rbx), %rdx
	movq	8(%rbx), %r11
	movq	16(%rbx), %rdi
	movq	%rax, -104(%rbp)
	movl	20(%r10), %eax
	movq	%r10, -112(%rbp)
	movq	%r11, -120(%rbp)
	andl	$16777215, %eax
	salq	$4, %rax
	movq	8(%rdx,%rax), %rax
	movq	%rax, -96(%rbp)
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	8(%rdx,%rax), %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	movq	-120(%rbp), %r11
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r11, %rdi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-112(%rbp), %r10
	movl	20(%r13), %edx
	xorl	%r8d, %r8d
	movq	-128(%rbp), %r9
	movq	8(%rbx), %rdi
	movq	%rax, -96(%rbp)
	movq	%r15, %rcx
	movl	20(%r10), %eax
	movq	136(%rbx), %rsi
	andl	$16777215, %edx
	salq	$4, %rdx
	movq	8(%rsi,%rdx), %xmm0
	andl	$16777215, %eax
	movl	$2, %edx
	salq	$4, %rax
	movhps	8(%rsi,%rax), %xmm0
	movq	%r9, %rsi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Word32OrEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movl	20(%r12), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%rbx), %rax
	movq	%r8, (%rax)
	movl	20(%r12), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%rbx), %rax
	movq	$0, 8(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L66
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	leaq	16(%r13), %rax
	movq	16(%r13), %r13
	addq	$8, %rax
	jmp	.L63
.L66:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15551:
	.size	_ZN2v88internal8compiler13Int64Lowering15LowerComparisonEPNS1_4NodeEPKNS1_8OperatorES7_, .-_ZN2v88internal8compiler13Int64Lowering15LowerComparisonEPNS1_4NodeEPKNS1_8OperatorES7_
	.section	.text._ZN2v88internal8compiler13Int64Lowering15DefaultLoweringEPNS1_4NodeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering15DefaultLoweringEPNS1_4NodeEb
	.type	_ZN2v88internal8compiler13Int64Lowering15DefaultLoweringEPNS1_4NodeEb, @function
_ZN2v88internal8compiler13Int64Lowering15DefaultLoweringEPNS1_4NodeEb:
.LFB15552:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal8compiler14NodeProperties14PastValueIndexEPNS1_4NodeE@PLT
	movl	%eax, %ebx
	subl	$1, %ebx
	js	.L76
	movl	%eax, %edx
	cltq
	leaq	32(%r15), %r13
	movslq	%ebx, %rbx
	negl	%edx
	xorl	%r9d, %r9d
	movslq	%edx, %rdx
	addq	%rdx, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	-24(,%rax,8), %rax
	movq	%rax, -88(%rbp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L84:
	movq	32(%r15,%rbx,8), %rcx
	addq	%r13, %rdx
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rdi,%rax), %r10
	testq	%r10, %r10
	je	.L71
	movq	%r15, %rsi
	cmpq	%r10, %rcx
	je	.L83
.L73:
	leaq	0(,%rbx,4), %rax
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	subq	%rax, %rdi
	movq	%r10, -80(%rbp)
	movq	%rdi, %rax
	movq	-88(%rbp), %rdi
	movq	%rdx, -72(%rbp)
	leaq	(%rdi,%rax,8), %rax
	movq	%rcx, %rdi
	addq	%rax, %rsi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r10, (%rdx)
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rcx
	movl	$1, %r9d
.L71:
	testb	%r12b, %r12b
	jne	.L74
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r14), %rax
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	je	.L74
	movq	(%r14), %rsi
	leal	1(%rbx), %edx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movl	$1, %r9d
.L74:
	subq	$1, %rbx
	testl	%ebx, %ebx
	js	.L67
.L75:
	movzbl	23(%r15), %eax
	movq	136(%r14), %rdi
	leaq	0(,%rbx,8), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L84
	movq	0(%r13), %rsi
	leaq	16(%rsi,%rdx), %rdx
	movq	(%rdx), %rcx
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rdi,%rax), %r10
	testq	%r10, %r10
	je	.L71
	cmpq	%r10, %rcx
	jne	.L73
.L83:
	movl	$1, %r9d
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L76:
	xorl	%r9d, %r9d
.L67:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15552:
	.size	_ZN2v88internal8compiler13Int64Lowering15DefaultLoweringEPNS1_4NodeEb, .-_ZN2v88internal8compiler13Int64Lowering15DefaultLoweringEPNS1_4NodeEb
	.section	.text._ZN2v88internal8compiler13Int64Lowering25LowerWord64AtomicNarrowOpEPNS1_4NodeEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering25LowerWord64AtomicNarrowOpEPNS1_4NodeEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler13Int64Lowering25LowerWord64AtomicNarrowOpEPNS1_4NodeEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler13Int64Lowering25LowerWord64AtomicNarrowOpEPNS1_4NodeEPKNS1_8OperatorE:
.LFB15544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	movl	$1, %edx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13Int64Lowering15DefaultLoweringEPNS1_4NodeEb
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	24(%rbx), %rdi
	movq	8(%rbx), %r13
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movl	20(%r12), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%rbx), %rax
	movq	%r12, (%rax)
	movl	20(%r12), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%rbx), %rax
	movq	%r8, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15544:
	.size	_ZN2v88internal8compiler13Int64Lowering25LowerWord64AtomicNarrowOpEPNS1_4NodeEPKNS1_8OperatorE, .-_ZN2v88internal8compiler13Int64Lowering25LowerWord64AtomicNarrowOpEPNS1_4NodeEPKNS1_8OperatorE
	.section	.text._ZN2v88internal8compiler13Int64Lowering19LowerCallDescriptorEPKNS1_14CallDescriptorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering19LowerCallDescriptorEPKNS1_14CallDescriptorE
	.type	_ZN2v88internal8compiler13Int64Lowering19LowerCallDescriptorEPKNS1_14CallDescriptorE, @function
_ZN2v88internal8compiler13Int64Lowering19LowerCallDescriptorEPKNS1_14CallDescriptorE:
.LFB15553:
	.cfi_startproc
	endbr64
	movq	160(%rdi), %rax
	testq	%rax, %rax
	je	.L88
	cmpq	%rsi, (%rax)
	je	.L94
	cmpq	%rsi, 8(%rax)
	je	.L95
.L88:
	movq	(%rdi), %rdi
	jmp	_ZN2v88internal8compiler24GetI32WasmCallDescriptorEPNS0_4ZoneEPKNS1_14CallDescriptorE@PLT
	.p2align 4,,10
	.p2align 3
.L94:
	movq	16(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	movq	24(%rax), %rax
	ret
	.cfi_endproc
.LFE15553:
	.size	_ZN2v88internal8compiler13Int64Lowering19LowerCallDescriptorEPKNS1_14CallDescriptorE, .-_ZN2v88internal8compiler13Int64Lowering19LowerCallDescriptorEPKNS1_14CallDescriptorE
	.section	.text._ZN2v88internal8compiler13Int64Lowering11ReplaceNodeEPNS1_4NodeES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering11ReplaceNodeEPNS1_4NodeES4_S4_
	.type	_ZN2v88internal8compiler13Int64Lowering11ReplaceNodeEPNS1_4NodeES4_S4_, @function
_ZN2v88internal8compiler13Int64Lowering11ReplaceNodeEPNS1_4NodeES4_S4_:
.LFB15554:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%rdi), %rax
	movq	%rdx, (%rax)
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%rdi), %rax
	movq	%rcx, 8(%rax)
	ret
	.cfi_endproc
.LFE15554:
	.size	_ZN2v88internal8compiler13Int64Lowering11ReplaceNodeEPNS1_4NodeES4_S4_, .-_ZN2v88internal8compiler13Int64Lowering11ReplaceNodeEPNS1_4NodeES4_S4_
	.section	.text._ZN2v88internal8compiler13Int64Lowering17HasReplacementLowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering17HasReplacementLowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler13Int64Lowering17HasReplacementLowEPNS1_4NodeE, @function
_ZN2v88internal8compiler13Int64Lowering17HasReplacementLowEPNS1_4NodeE:
.LFB15555:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%rdi), %rax
	cmpq	$0, (%rax)
	setne	%al
	ret
	.cfi_endproc
.LFE15555:
	.size	_ZN2v88internal8compiler13Int64Lowering17HasReplacementLowEPNS1_4NodeE, .-_ZN2v88internal8compiler13Int64Lowering17HasReplacementLowEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler13Int64Lowering17GetReplacementLowEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering17GetReplacementLowEPNS1_4NodeE
	.type	_ZN2v88internal8compiler13Int64Lowering17GetReplacementLowEPNS1_4NodeE, @function
_ZN2v88internal8compiler13Int64Lowering17GetReplacementLowEPNS1_4NodeE:
.LFB15556:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE15556:
	.size	_ZN2v88internal8compiler13Int64Lowering17GetReplacementLowEPNS1_4NodeE, .-_ZN2v88internal8compiler13Int64Lowering17GetReplacementLowEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler13Int64Lowering18HasReplacementHighEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering18HasReplacementHighEPNS1_4NodeE
	.type	_ZN2v88internal8compiler13Int64Lowering18HasReplacementHighEPNS1_4NodeE, @function
_ZN2v88internal8compiler13Int64Lowering18HasReplacementHighEPNS1_4NodeE:
.LFB15557:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%rdi), %rax
	cmpq	$0, 8(%rax)
	setne	%al
	ret
	.cfi_endproc
.LFE15557:
	.size	_ZN2v88internal8compiler13Int64Lowering18HasReplacementHighEPNS1_4NodeE, .-_ZN2v88internal8compiler13Int64Lowering18HasReplacementHighEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler13Int64Lowering18GetReplacementHighEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering18GetReplacementHighEPNS1_4NodeE
	.type	_ZN2v88internal8compiler13Int64Lowering18GetReplacementHighEPNS1_4NodeE, @function
_ZN2v88internal8compiler13Int64Lowering18GetReplacementHighEPNS1_4NodeE:
.LFB15558:
	.cfi_startproc
	endbr64
	movl	20(%rsi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%rdi), %rax
	movq	8(%rax), %rax
	ret
	.cfi_endproc
.LFE15558:
	.size	_ZN2v88internal8compiler13Int64Lowering18GetReplacementHighEPNS1_4NodeE, .-_ZN2v88internal8compiler13Int64Lowering18GetReplacementHighEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler13Int64Lowering21PreparePhiReplacementEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering21PreparePhiReplacementEPNS1_4NodeE
	.type	_ZN2v88internal8compiler13Int64Lowering21PreparePhiReplacementEPNS1_4NodeE, @function
_ZN2v88internal8compiler13Int64Lowering21PreparePhiReplacementEPNS1_4NodeE:
.LFB15559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rdi
	call	_ZN2v88internal8compiler19PhiRepresentationOfEPKNS1_8OperatorE@PLT
	cmpb	$5, %al
	je	.L121
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	(%rbx), %rdi
	movl	20(%rax), %r15d
	movq	16(%rdi), %r13
	movq	24(%rdi), %rax
	leal	1(%r15), %r9d
	movslq	%r9d, %r14
	subq	%r13, %rax
	salq	$3, %r14
	movq	%r14, %rsi
	cmpq	%rax, %r14
	ja	.L122
	leaq	(%r14,%r13), %rax
	movq	%rax, 16(%rdi)
.L104:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	%rsi, %rax
	jb	.L123
	addq	%rcx, %rsi
	movq	%rsi, 16(%rdi)
.L106:
	testl	%r15d, %r15d
	jle	.L107
	movl	%r15d, %eax
	leaq	152(%rbx), %r8
	leaq	160(%rbx), %rdi
	salq	$3, %rax
	leal	-1(%r15), %esi
	leaq	0(%r13,%rax), %rdx
	cmpq	%rdx, %r8
	setnb	%dl
	cmpq	%rdi, %r13
	setnb	%r10b
	orl	%r10d, %edx
	cmpl	$2, %esi
	seta	%r10b
	addq	%rcx, %rax
	andl	%r10d, %edx
	cmpq	%rax, %r8
	setnb	%al
	cmpq	%rdi, %rcx
	setnb	%dil
	orl	%edi, %eax
	testb	%al, %dl
	je	.L108
	leaq	15(%rcx), %rax
	subq	%r13, %rax
	cmpq	$30, %rax
	jbe	.L108
	movl	%r15d, %edx
	movq	%rcx, %rsi
	movq	%r13, %rax
	movq	152(%rbx), %xmm0
	shrl	%edx
	subq	%r13, %rsi
	salq	$4, %rdx
	punpcklqdq	%xmm0, %xmm0
	addq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L109:
	movups	%xmm0, (%rax)
	movups	%xmm0, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L109
	movl	%r15d, %eax
	andl	$-2, %eax
	testb	$1, %r15b
	je	.L107
	movq	152(%rbx), %rdx
	movq	%rdx, 0(%r13,%rax,8)
	movq	%rdx, (%rcx,%rax,8)
.L107:
	subq	$8, %r14
	movq	%r12, %rdi
	xorl	%esi, %esi
	movl	%r9d, -60(%rbp)
	leaq	0(%r13,%r14), %rdx
	movq	%rcx, -72(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rcx
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, (%rdx)
	addq	%rcx, %r14
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal8compiler14NodeProperties15GetControlInputEPNS1_4NodeEi@PLT
	movl	%r15d, %edx
	movl	$4, %esi
	movq	%rax, (%r14)
	movq	24(%rbx), %rdi
	movq	8(%rbx), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movl	-60(%rbp), %r9d
	movq	-56(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	%r9d, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %r10
	movq	24(%rbx), %rdi
	movl	%r15d, %edx
	movl	$4, %esi
	movq	%rax, %r14
	movq	%r10, -56(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movl	-60(%rbp), %r9d
	movq	-56(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rax, %rsi
	movl	%r9d, %edx
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movl	20(%r12), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%rbx), %rax
	movq	%r8, (%rax)
	movl	20(%r12), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%rbx), %rax
	movq	%r14, 8(%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movl	%r9d, -60(%rbp)
	movq	%r14, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rsi
	movl	-60(%rbp), %r9d
	movq	%rax, %r13
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L108:
	leaq	8(%r13,%rsi,8), %rdi
	movq	%rcx, %rsi
	movq	%r13, %rax
	subq	%r13, %rsi
	.p2align 4,,10
	.p2align 3
.L112:
	movq	152(%rbx), %rdx
	movq	%rdx, (%rax)
	movq	%rdx, (%rsi,%rax)
	addq	$8, %rax
	cmpq	%rdi, %rax
	jne	.L112
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L123:
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-56(%rbp), %r9d
	movq	%rax, %rcx
	jmp	.L106
	.cfi_endproc
.LFE15559:
	.size	_ZN2v88internal8compiler13Int64Lowering21PreparePhiReplacementEPNS1_4NodeE, .-_ZN2v88internal8compiler13Int64Lowering21PreparePhiReplacementEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler13Int64Lowering26ReplaceNodeWithProjectionsEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering26ReplaceNodeWithProjectionsEPNS1_4NodeE
	.type	_ZN2v88internal8compiler13Int64Lowering26ReplaceNodeWithProjectionsEPNS1_4NodeE, @function
_ZN2v88internal8compiler13Int64Lowering26ReplaceNodeWithProjectionsEPNS1_4NodeE:
.LFB15560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	8(%rdi), %r14
	movq	24(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%r14), %r13
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10ProjectionEm@PLT
	movq	%r12, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r13, %xmm1
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movq	%rax, %rsi
	punpcklqdq	%xmm1, %xmm0
	movq	%rcx, -88(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%rbx), %r15
	movq	24(%rbx), %rdi
	movl	$1, %esi
	movq	%rax, %r13
	movq	8(%r15), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10ProjectionEm@PLT
	movq	-88(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%r14, %xmm2
	movq	%r12, %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movl	20(%r12), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%rbx), %rax
	movq	%r13, (%rax)
	movl	20(%r12), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%rbx), %rax
	movq	%r8, 8(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L127
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L127:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15560:
	.size	_ZN2v88internal8compiler13Int64Lowering26ReplaceNodeWithProjectionsEPNS1_4NodeE, .-_ZN2v88internal8compiler13Int64Lowering26ReplaceNodeWithProjectionsEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler13Int64Lowering23LowerMemoryBaseAndIndexEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering23LowerMemoryBaseAndIndexEPNS1_4NodeE
	.type	_ZN2v88internal8compiler13Int64Lowering23LowerMemoryBaseAndIndexEPNS1_4NodeE, @function
_ZN2v88internal8compiler13Int64Lowering23LowerMemoryBaseAndIndexEPNS1_4NodeE:
.LFB15561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rsi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movzbl	23(%rsi), %eax
	movq	32(%rsi), %rdi
	movq	136(%r12), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L129
	movl	20(%rdi), %eax
	movq	8(%r15), %r14
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rdx,%rax), %r13
	cmpq	%r13, %rdi
	je	.L132
	movq	%r15, %rax
	testq	%r13, %r13
	je	.L132
.L130:
	subq	$24, %rsi
	movq	%rax, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r13, (%rax)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L128
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L135
	movq	8(%r15), %r14
	addq	$8, %r15
	cmpq	%r14, %r12
	jne	.L137
.L128:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rdx,%rax), %r12
	testq	%r12, %r12
	je	.L128
	addq	$8, %r15
	cmpq	%r14, %r12
	je	.L128
.L137:
	leaq	-48(%rbx), %r13
	testq	%r14, %r14
	je	.L139
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
.L139:
	movq	%r12, (%r15)
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	16(%rdi), %r8
	leaq	16(%rdi), %rax
	movq	8(%rax), %r14
	movl	20(%r8), %ecx
	andl	$16777215, %ecx
	salq	$4, %rcx
	movq	(%rdx,%rcx), %r13
	cmpq	%r8, %r13
	je	.L134
	testq	%r13, %r13
	je	.L134
	movq	%rdi, %rsi
	movq	%r8, %rdi
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L134:
	movl	20(%r14), %ecx
	andl	$16777215, %ecx
	salq	$4, %rcx
	movq	(%rdx,%rcx), %r12
	testq	%r12, %r12
	je	.L128
.L142:
	cmpq	%r14, %r12
	je	.L128
	leaq	8(%rax), %r15
	movq	%rdi, %rbx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L135:
	movq	(%r15), %rdi
	movq	24(%rdi), %r14
	leaq	16(%rdi), %rax
	jmp	.L142
	.cfi_endproc
.LFE15561:
	.size	_ZN2v88internal8compiler13Int64Lowering23LowerMemoryBaseAndIndexEPNS1_4NodeE, .-_ZN2v88internal8compiler13Int64Lowering23LowerMemoryBaseAndIndexEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler13Int64Lowering22LowerWord64AtomicBinopEPNS1_4NodeEPKNS1_8OperatorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering22LowerWord64AtomicBinopEPNS1_4NodeEPKNS1_8OperatorE
	.type	_ZN2v88internal8compiler13Int64Lowering22LowerWord64AtomicBinopEPNS1_4NodeEPKNS1_8OperatorE, @function
_ZN2v88internal8compiler13Int64Lowering22LowerWord64AtomicBinopEPNS1_4NodeEPKNS1_8OperatorE:
.LFB15543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal8compiler13Int64Lowering23LowerMemoryBaseAndIndexEPNS1_4NodeE
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L156
	movq	48(%r13), %rbx
	leaq	48(%r13), %rdx
	movq	%r13, %rsi
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r14), %rax
	movq	(%rax), %r8
	cmpq	%r8, %rbx
	je	.L158
.L159:
	leaq	-72(%rsi), %r15
	movq	%rbx, %rdi
	movq	%r8, -64(%rbp)
	movq	%r15, %rsi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	movq	%r8, (%rdx)
	testq	%r8, %r8
	je	.L162
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L162:
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r14), %rax
.L158:
	movq	(%r14), %rsi
	movq	8(%rax), %rcx
	movq	%r13, %rdi
	movl	$3, %edx
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13Int64Lowering26ReplaceNodeWithProjectionsEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movq	32(%r13), %rsi
	movq	32(%rsi), %rbx
	leaq	32(%rsi), %rdx
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r14), %rax
	movq	(%rax), %r8
	cmpq	%r8, %rbx
	jne	.L159
	jmp	.L158
	.cfi_endproc
.LFE15543:
	.size	_ZN2v88internal8compiler13Int64Lowering22LowerWord64AtomicBinopEPNS1_4NodeEPKNS1_8OperatorE, .-_ZN2v88internal8compiler13Int64Lowering22LowerWord64AtomicBinopEPNS1_4NodeEPKNS1_8OperatorE
	.section	.rodata._ZN2v88internal8compiler13Int64Lowering9LowerNodeEPNS1_4NodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal8compiler13Int64Lowering9LowerNodeEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering9LowerNodeEPNS1_4NodeE
	.type	_ZN2v88internal8compiler13Int64Lowering9LowerNodeEPNS1_4NodeE, @function
_ZN2v88internal8compiler13Int64Lowering9LowerNodeEPNS1_4NodeE:
.LFB15547:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	16(%rdi), %ecx
	movq	%rcx, %rax
	cmpw	$50, %cx
	jbe	.L711
	subw	$318, %ax
	cmpw	$191, %ax
	ja	.L166
	leaq	.L168(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler13Int64Lowering9LowerNodeEPNS1_4NodeE,"a",@progbits
	.align 4
	.align 4
.L168:
	.long	.L204-.L168
	.long	.L203-.L168
	.long	.L202-.L168
	.long	.L201-.L168
	.long	.L200-.L168
	.long	.L199-.L168
	.long	.L198-.L168
	.long	.L197-.L168
	.long	.L166-.L168
	.long	.L196-.L168
	.long	.L166-.L168
	.long	.L195-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L194-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L193-.L168
	.long	.L192-.L168
	.long	.L191-.L168
	.long	.L190-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L189-.L168
	.long	.L188-.L168
	.long	.L187-.L168
	.long	.L186-.L168
	.long	.L185-.L168
	.long	.L184-.L168
	.long	.L183-.L168
	.long	.L182-.L168
	.long	.L181-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L172-.L168
	.long	.L166-.L168
	.long	.L171-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L180-.L168
	.long	.L179-.L168
	.long	.L178-.L168
	.long	.L166-.L168
	.long	.L177-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L167-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L176-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L175-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L174-.L168
	.long	.L166-.L168
	.long	.L173-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L172-.L168
	.long	.L171-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L166-.L168
	.long	.L170-.L168
	.long	.L169-.L168
	.long	.L167-.L168
	.section	.text._ZN2v88internal8compiler13Int64Lowering9LowerNodeEPNS1_4NodeE
	.p2align 4,,10
	.p2align 3
.L711:
	leaq	.L206(%rip), %rdx
	movslq	(%rdx,%rcx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8compiler13Int64Lowering9LowerNodeEPNS1_4NodeE
	.align 4
	.align 4
.L206:
	.long	.L212-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L211-.L206
	.long	.L210-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L209-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L208-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L166-.L206
	.long	.L207-.L206
	.long	.L205-.L206
	.section	.text._ZN2v88internal8compiler13Int64Lowering9LowerNodeEPNS1_4NodeE
.L166:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13Int64Lowering15DefaultLoweringEPNS1_4NodeEb
	.p2align 4,,10
	.p2align 3
.L163:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L712
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L205:
	.cfi_restore_state
	movq	144(%r12), %rax
	movq	8(%rax), %rbx
	testl	%ebx, %ebx
	jle	.L163
	movq	(%rax), %rcx
	movq	16(%rax), %rsi
	leal	-1(%rbx), %eax
	cmpl	$14, %eax
	jbe	.L544
	movl	%ebx, %edx
	pxor	%xmm1, %xmm1
	pxor	%xmm7, %xmm7
	addq	%rsi, %rcx
	shrl	$4, %edx
	movdqa	.LC1(%rip), %xmm3
	movq	%rcx, %rax
	pxor	%xmm6, %xmm6
	salq	$4, %rdx
	movdqa	.LC2(%rip), %xmm2
	addq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L287:
	movdqu	(%rax), %xmm0
	movdqa	%xmm7, %xmm4
	movdqa	%xmm6, %xmm8
	addq	$16, %rax
	pcmpeqb	%xmm3, %xmm0
	pand	%xmm2, %xmm0
	pcmpgtb	%xmm0, %xmm4
	movdqa	%xmm0, %xmm5
	punpcklbw	%xmm4, %xmm5
	punpckhbw	%xmm4, %xmm0
	pcmpgtw	%xmm5, %xmm8
	movdqa	%xmm5, %xmm4
	punpcklwd	%xmm8, %xmm4
	punpckhwd	%xmm8, %xmm5
	paddd	%xmm4, %xmm1
	movdqa	%xmm6, %xmm4
	pcmpgtw	%xmm0, %xmm4
	paddd	%xmm5, %xmm1
	movdqa	%xmm0, %xmm5
	punpcklwd	%xmm4, %xmm5
	punpckhwd	%xmm4, %xmm0
	paddd	%xmm5, %xmm1
	paddd	%xmm0, %xmm1
	cmpq	%rax, %rdx
	jne	.L287
	movdqa	%xmm1, %xmm0
	movl	%ebx, %edx
	psrldq	$8, %xmm0
	andl	$-16, %edx
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	addl	%ebx, %eax
	testb	$15, %bl
	je	.L288
.L286:
	movslq	%edx, %rsi
	cmpb	$5, (%rcx,%rsi)
	sete	%sil
	movzbl	%sil, %esi
	addl	%esi, %eax
	leal	1(%rdx), %esi
	cmpl	%esi, %ebx
	jle	.L288
	movslq	%esi, %rsi
	cmpb	$5, (%rcx,%rsi)
	sete	%sil
	movzbl	%sil, %esi
	addl	%esi, %eax
	leal	2(%rdx), %esi
	cmpl	%esi, %ebx
	jle	.L288
	movslq	%esi, %rsi
	cmpb	$5, (%rcx,%rsi)
	sete	%sil
	movzbl	%sil, %esi
	addl	%esi, %eax
	leal	3(%rdx), %esi
	cmpl	%esi, %ebx
	jle	.L288
	movslq	%esi, %rsi
	cmpb	$5, (%rcx,%rsi)
	sete	%sil
	movzbl	%sil, %esi
	addl	%esi, %eax
	leal	4(%rdx), %esi
	cmpl	%esi, %ebx
	jle	.L288
	movslq	%esi, %rsi
	cmpb	$5, (%rcx,%rsi)
	sete	%sil
	movzbl	%sil, %esi
	addl	%esi, %eax
	leal	5(%rdx), %esi
	cmpl	%esi, %ebx
	jle	.L288
	movslq	%esi, %rsi
	cmpb	$5, (%rcx,%rsi)
	sete	%sil
	movzbl	%sil, %esi
	addl	%esi, %eax
	leal	6(%rdx), %esi
	cmpl	%esi, %ebx
	jle	.L288
	movslq	%esi, %rsi
	cmpb	$5, (%rcx,%rsi)
	sete	%sil
	movzbl	%sil, %esi
	addl	%esi, %eax
	leal	7(%rdx), %esi
	cmpl	%esi, %ebx
	jle	.L288
	movslq	%esi, %rsi
	cmpb	$5, (%rcx,%rsi)
	sete	%sil
	movzbl	%sil, %esi
	addl	%esi, %eax
	leal	8(%rdx), %esi
	cmpl	%esi, %ebx
	jle	.L288
	movslq	%esi, %rsi
	cmpb	$5, (%rcx,%rsi)
	sete	%sil
	movzbl	%sil, %esi
	addl	%esi, %eax
	leal	9(%rdx), %esi
	cmpl	%esi, %ebx
	jle	.L288
	movslq	%esi, %rsi
	cmpb	$5, (%rcx,%rsi)
	sete	%sil
	movzbl	%sil, %esi
	addl	%esi, %eax
	leal	10(%rdx), %esi
	cmpl	%esi, %ebx
	jle	.L288
	movslq	%esi, %rsi
	cmpb	$5, (%rcx,%rsi)
	sete	%sil
	movzbl	%sil, %esi
	addl	%esi, %eax
	leal	11(%rdx), %esi
	cmpl	%esi, %ebx
	jle	.L288
	movslq	%esi, %rsi
	cmpb	$5, (%rcx,%rsi)
	sete	%sil
	movzbl	%sil, %esi
	addl	%esi, %eax
	leal	12(%rdx), %esi
	cmpl	%esi, %ebx
	jle	.L288
	movslq	%esi, %rsi
	cmpb	$5, (%rcx,%rsi)
	sete	%sil
	movzbl	%sil, %esi
	addl	%esi, %eax
	leal	13(%rdx), %esi
	cmpl	%esi, %ebx
	jle	.L288
	movslq	%esi, %rsi
	cmpb	$5, (%rcx,%rsi)
	sete	%sil
	addl	$14, %edx
	movzbl	%sil, %esi
	addl	%esi, %eax
	cmpl	%edx, %ebx
	jle	.L288
	movslq	%edx, %rdx
	cmpb	$5, (%rcx,%rdx)
	sete	%dl
	movzbl	%dl, %edx
	addl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L288:
	cmpl	%eax, %ebx
	je	.L163
	call	_ZN2v88internal8compiler16ParameterIndexOfEPKNS1_8OperatorE@PLT
	movq	144(%r12), %rdx
	leal	-1(%rax), %r15d
	movl	%eax, %esi
	movq	8(%rdx), %rax
	cmpl	%eax, %r15d
	movl	%eax, %ecx
	cmovl	%r15d, %ecx
	testl	%ecx, %ecx
	jle	.L545
	movq	(%rdx), %rax
	movq	16(%rdx), %rsi
	subl	$1, %ecx
	movl	%r15d, %r14d
	leaq	(%rsi,%rax), %rdx
	leaq	1(%rsi,%rax), %rax
	addq	%rcx, %rax
	.p2align 4,,10
	.p2align 3
.L307:
	cmpb	$5, (%rdx)
	leal	1(%r14), %esi
	jne	.L306
	leal	2(%r14), %ecx
	movl	%esi, %r14d
	movl	%ecx, %esi
.L306:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L307
.L305:
	movq	24(%r12), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	testl	%r15d, %r15d
	js	.L163
	cmpl	%ebx, %r15d
	jge	.L163
	movq	144(%r12), %rdx
	movslq	%r15d, %rax
	addq	16(%rdx), %rax
	addq	(%rdx), %rax
	cmpb	$5, (%rax)
	jne	.L163
	movq	8(%r12), %r15
	movq	24(%r12), %rdi
	leal	2(%r14), %esi
	xorl	%edx, %edx
	movq	8(%r15), %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder9ParameterEiPKc@PLT
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rbx, -96(%rbp)
	movq	%rax, %rsi
	movl	$1, %edx
	jmp	.L694
.L208:
	call	_ZN2v88internal8compiler19PhiRepresentationOfEPKNS1_8OperatorE@PLT
	cmpb	$5, %al
	jne	.L166
	movl	20(%r13), %edx
	movq	136(%r12), %rsi
	movl	%edx, %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rsi, %rax
	movq	8(%rax), %rbx
	movq	(%rax), %rcx
	movq	0(%r13), %rax
	movq	%rbx, -112(%rbp)
	movl	20(%rax), %eax
	testl	%eax, %eax
	jle	.L163
	movq	%rcx, %rax
	addq	$32, %rbx
	leaq	32(%rcx), %rcx
	movq	%r12, -104(%rbp)
	movq	%rbx, -120(%rbp)
	leaq	32(%r13), %r15
	movq	%r13, %r12
	xorl	%r14d, %r14d
	movq	%rcx, -128(%rbp)
	xorl	%ebx, %ebx
	movq	%rax, %r13
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L713:
	movq	32(%r13,%r14), %rdi
	leaq	(%r14,%rax), %rcx
	movq	%r13, %r10
	cmpq	%rdi, %r9
	je	.L465
.L464:
	leaq	0(,%r14,4), %rdx
	movq	%r14, %rax
	subq	%rdx, %rax
	leaq	-24(%r10,%rax), %rsi
	testq	%rdi, %rdi
	je	.L466
	movq	%rcx, -152(%rbp)
	movq	%r9, -144(%rbp)
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-152(%rbp), %rcx
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %rsi
.L466:
	movq	%r9, (%rcx)
	testq	%r9, %r9
	je	.L688
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L688:
	movq	-104(%rbp), %rax
	movzbl	23(%r12), %edx
	movq	136(%rax), %rsi
	andl	$15, %edx
.L465:
	cmpl	$15, %edx
	je	.L468
	leaq	(%r14,%r15), %rdx
.L469:
	movq	(%rdx), %rdx
	movq	-112(%rbp), %rax
	movl	20(%rdx), %edx
	andl	$16777215, %edx
	salq	$4, %rdx
	movq	8(%rsi,%rdx), %r9
	movzbl	23(%rax), %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L470
	movq	-120(%rbp), %rcx
	movq	32(%rax,%r14), %rdi
	leaq	(%r14,%rcx), %rdx
	movq	%rax, %rcx
	cmpq	%rdi, %r9
	je	.L472
.L471:
	leaq	0(,%r14,4), %rsi
	movq	%r14, %rax
	subq	%rsi, %rax
	leaq	-24(%rcx,%rax), %rsi
	testq	%rdi, %rdi
	je	.L474
	movq	%rdx, -152(%rbp)
	movq	%r9, -144(%rbp)
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-152(%rbp), %rdx
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %rsi
.L474:
	movq	%r9, (%rdx)
	testq	%r9, %r9
	je	.L472
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L472:
	movq	(%r12), %rdx
	addl	$1, %ebx
	addq	$8, %r14
	cmpl	20(%rdx), %ebx
	jge	.L163
	movq	-104(%rbp), %rax
	movl	20(%r12), %edx
	movq	136(%rax), %rsi
.L475:
	shrl	$24, %edx
	leaq	(%r14,%r15), %rcx
	andl	$15, %edx
	cmpl	$15, %edx
	jne	.L462
	movq	(%r15), %rcx
	leaq	16(%rcx,%r14), %rcx
.L462:
	movq	(%rcx), %rcx
	movq	-128(%rbp), %rax
	movl	20(%rcx), %ecx
	andl	$16777215, %ecx
	salq	$4, %rcx
	movq	(%rsi,%rcx), %r9
	movzbl	23(%r13), %ecx
	andl	$15, %ecx
	cmpl	$15, %ecx
	jne	.L713
	movq	(%rax), %r10
	leaq	16(%r10,%r14), %rcx
	movq	(%rcx), %rdi
	cmpq	%rdi, %r9
	jne	.L464
	jmp	.L465
.L209:
	movq	48(%rdi), %rbx
	movq	24(%r12), %rdi
	movq	8(%r12), %r14
	movl	%ebx, %esi
	sarq	$32, %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %rdi
	movq	8(%r12), %r15
	movq	%rbx, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	%r14, (%rax)
	.p2align 4,,10
	.p2align 3
.L691:
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	%r8, 8(%rax)
	jmp	.L163
.L207:
	call	_ZN2v88internal8compiler16CallDescriptorOfEPKNS1_8OperatorE@PLT
	movq	%rax, %r10
	movq	24(%rax), %rax
	movq	(%rax), %r15
	testl	%r15d, %r15d
	jle	.L549
	leal	-1(%r15), %esi
	movq	16(%rax), %rcx
	cmpl	$15, %esi
	jbe	.L550
	movl	%esi, %edx
	movq	%rcx, %rax
	pxor	%xmm4, %xmm4
	movdqa	.LC1(%rip), %xmm3
	shrl	$4, %edx
	movdqa	.LC2(%rip), %xmm2
	pxor	%xmm6, %xmm6
	pxor	%xmm5, %xmm5
	salq	$7, %rdx
	movdqa	.LC3(%rip), %xmm0
	addq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L358:
	movdqu	4(%rax), %xmm1
	movdqu	20(%rax), %xmm7
	subq	$-128, %rax
	movdqu	-76(%rax), %xmm8
	movdqu	-12(%rax), %xmm9
	pand	%xmm0, %xmm7
	pand	%xmm0, %xmm1
	packuswb	%xmm7, %xmm1
	movdqu	-92(%rax), %xmm7
	pand	%xmm0, %xmm8
	pand	%xmm0, %xmm9
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm7
	packuswb	%xmm8, %xmm7
	movdqu	-44(%rax), %xmm8
	pand	%xmm0, %xmm7
	packuswb	%xmm7, %xmm1
	movdqu	-60(%rax), %xmm7
	pand	%xmm0, %xmm8
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm7
	packuswb	%xmm8, %xmm7
	movdqu	-28(%rax), %xmm8
	pand	%xmm0, %xmm7
	pand	%xmm0, %xmm8
	packuswb	%xmm9, %xmm8
	movdqa	%xmm5, %xmm9
	pand	%xmm0, %xmm8
	packuswb	%xmm8, %xmm7
	pand	%xmm0, %xmm7
	packuswb	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	pcmpeqb	%xmm3, %xmm1
	pand	%xmm2, %xmm1
	pcmpgtb	%xmm1, %xmm7
	movdqa	%xmm1, %xmm8
	punpcklbw	%xmm7, %xmm8
	punpckhbw	%xmm7, %xmm1
	pcmpgtw	%xmm8, %xmm9
	movdqa	%xmm8, %xmm7
	punpcklwd	%xmm9, %xmm7
	punpckhwd	%xmm9, %xmm8
	paddd	%xmm7, %xmm4
	movdqa	%xmm5, %xmm7
	pcmpgtw	%xmm1, %xmm7
	paddd	%xmm8, %xmm4
	movdqa	%xmm1, %xmm8
	punpcklwd	%xmm7, %xmm8
	punpckhwd	%xmm7, %xmm1
	paddd	%xmm8, %xmm4
	paddd	%xmm1, %xmm4
	cmpq	%rdx, %rax
	jne	.L358
	movdqa	%xmm4, %xmm1
	psrldq	$8, %xmm1
	paddd	%xmm4, %xmm1
	movdqa	%xmm1, %xmm4
	psrldq	$4, %xmm4
	paddd	%xmm4, %xmm1
	movd	%xmm1, %eax
	leal	(%rax,%r15), %ebx
	movl	%esi, %eax
	andl	$-16, %eax
.L357:
	movslq	%eax, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L359
	addl	$1, %ebx
.L359:
	leal	1(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L360
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L361
	addl	$1, %ebx
.L361:
	leal	2(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L360
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L362
	addl	$1, %ebx
.L362:
	leal	3(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L360
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L363
	addl	$1, %ebx
.L363:
	leal	4(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L360
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L364
	addl	$1, %ebx
.L364:
	leal	5(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L360
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L365
	addl	$1, %ebx
.L365:
	leal	6(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L360
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L366
	addl	$1, %ebx
.L366:
	leal	7(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L360
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L367
	addl	$1, %ebx
.L367:
	leal	8(%rax), %edx
	cmpl	%r15d, %edx
	jge	.L360
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L368
	addl	$1, %ebx
.L368:
	leal	9(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L360
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L369
	addl	$1, %ebx
.L369:
	leal	10(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L360
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L370
	addl	$1, %ebx
.L370:
	leal	11(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L360
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L371
	addl	$1, %ebx
.L371:
	leal	12(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L360
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L372
	addl	$1, %ebx
.L372:
	leal	13(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L360
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L373
	addl	$1, %ebx
.L373:
	leal	14(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L360
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L374
	addl	$1, %ebx
.L374:
	addl	$15, %eax
	cmpl	%eax, %r15d
	jle	.L360
	cltq
	cmpb	$5, 4(%rcx,%rax,8)
	jne	.L360
	addl	$1, %ebx
	.p2align 4,,10
	.p2align 3
.L360:
	cmpl	%ebx, %r15d
	setne	%r14b
.L356:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal8compiler13Int64Lowering15DefaultLoweringEPNS1_4NodeEb
	testb	%r14b, %r14b
	movq	-104(%rbp), %r10
	jne	.L569
	testb	%al, %al
	je	.L163
.L569:
	movq	160(%r12), %rax
	movq	24(%r12), %r14
	testq	%rax, %rax
	je	.L378
	cmpq	(%rax), %r10
	je	.L714
	cmpq	8(%rax), %r10
	je	.L715
.L378:
	movq	(%r12), %rdi
	movq	%r10, %rsi
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal8compiler24GetI32WasmCallDescriptorEPNS0_4ZoneEPKNS1_14CallDescriptorE@PLT
	movq	-104(%rbp), %r10
	movq	%rax, %rsi
.L380:
	movq	%r14, %rdi
	movq	%r10, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder4CallEPKNS1_14CallDescriptorE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	cmpl	%ebx, %r15d
	je	.L163
	movq	-104(%rbp), %r10
	movq	24(%r10), %rax
	movq	(%rax), %rax
	movq	%rax, -104(%rbp)
	cmpq	$1, %rax
	je	.L695
	cmpq	$268435455, -104(%rbp)
	movq	(%r12), %rdi
	ja	.L716
	testq	%rax, %rax
	je	.L383
	leaq	0(,%rax,8), %rdx
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	movq	%rdx, %rsi
	subq	%r15, %rax
	cmpq	%rax, %rdx
	ja	.L717
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L385:
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r10, -112(%rbp)
	xorl	%r14d, %r14d
	call	memset@PLT
	movq	-104(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	call	_ZN2v88internal8compiler14NodeProperties23CollectValueProjectionsEPNS1_4NodeEPS4_m@PLT
	movq	-112(%rbp), %r10
	leaq	-96(%rbp), %rax
	movq	%r12, %r9
	movq	%rax, -136(%rbp)
	movq	%r13, -144(%rbp)
	movq	%r10, %r12
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L551:
	movq	%rsi, %r14
.L387:
	addq	$1, %rbx
	addq	$8, %r15
	cmpq	%rbx, -104(%rbp)
	je	.L163
.L388:
	movq	(%r15), %r13
	leaq	0(,%rbx,8), %rdx
	cmpq	%r14, %rbx
	je	.L386
	movq	24(%r9), %rdi
	movq	%r14, %rsi
	movq	%rdx, -120(%rbp)
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10ProjectionEm@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
.L386:
	movq	24(%r12), %rax
	leaq	1(%r14), %rsi
	movq	16(%rax), %rax
	cmpb	$5, 4(%rax,%rdx)
	jne	.L551
	movq	8(%r9), %r11
	movq	24(%r9), %rdi
	movq	%r9, -120(%rbp)
	addq	$2, %r14
	movq	8(%r11), %rax
	movq	%r11, -128(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder10ProjectionEm@PLT
	movq	-128(%rbp), %r11
	xorl	%r8d, %r8d
	movq	-144(%rbp), %xmm0
	movq	-136(%rbp), %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-112(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-120(%rbp), %r9
	movq	%rax, %r8
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r9), %rax
	movq	%r13, (%rax)
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r9), %rax
	movq	%r8, 8(%rax)
	jmp	.L387
.L210:
	call	_ZN2v88internal8compiler16CallDescriptorOfEPKNS1_8OperatorE@PLT
	movq	%rax, %r15
	movq	24(%rax), %rax
	movq	(%rax), %r14
	testl	%r14d, %r14d
	jle	.L547
	leal	-1(%r14), %esi
	movq	16(%rax), %rcx
	cmpl	$15, %esi
	jbe	.L548
	movl	%esi, %edx
	movq	%rcx, %rax
	pxor	%xmm4, %xmm4
	movdqa	.LC1(%rip), %xmm3
	shrl	$4, %edx
	movdqa	.LC2(%rip), %xmm2
	pxor	%xmm6, %xmm6
	pxor	%xmm5, %xmm5
	salq	$7, %rdx
	movdqa	.LC3(%rip), %xmm0
	addq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L333:
	movdqu	4(%rax), %xmm1
	movdqu	20(%rax), %xmm7
	subq	$-128, %rax
	movdqu	-76(%rax), %xmm8
	movdqu	-12(%rax), %xmm9
	pand	%xmm0, %xmm7
	pand	%xmm0, %xmm1
	packuswb	%xmm7, %xmm1
	movdqu	-92(%rax), %xmm7
	pand	%xmm0, %xmm8
	pand	%xmm0, %xmm9
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm7
	packuswb	%xmm8, %xmm7
	movdqu	-44(%rax), %xmm8
	pand	%xmm0, %xmm7
	packuswb	%xmm7, %xmm1
	movdqu	-60(%rax), %xmm7
	pand	%xmm0, %xmm8
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm7
	packuswb	%xmm8, %xmm7
	movdqu	-28(%rax), %xmm8
	pand	%xmm0, %xmm7
	pand	%xmm0, %xmm8
	packuswb	%xmm9, %xmm8
	movdqa	%xmm5, %xmm9
	pand	%xmm0, %xmm8
	packuswb	%xmm8, %xmm7
	pand	%xmm0, %xmm7
	packuswb	%xmm7, %xmm1
	movdqa	%xmm6, %xmm7
	pcmpeqb	%xmm3, %xmm1
	pand	%xmm2, %xmm1
	pcmpgtb	%xmm1, %xmm7
	movdqa	%xmm1, %xmm8
	punpcklbw	%xmm7, %xmm8
	punpckhbw	%xmm7, %xmm1
	pcmpgtw	%xmm8, %xmm9
	movdqa	%xmm8, %xmm7
	punpcklwd	%xmm9, %xmm7
	punpckhwd	%xmm9, %xmm8
	paddd	%xmm7, %xmm4
	movdqa	%xmm5, %xmm7
	pcmpgtw	%xmm1, %xmm7
	paddd	%xmm8, %xmm4
	movdqa	%xmm1, %xmm8
	punpcklwd	%xmm7, %xmm8
	punpckhwd	%xmm7, %xmm1
	paddd	%xmm8, %xmm4
	paddd	%xmm1, %xmm4
	cmpq	%rax, %rdx
	jne	.L333
	movdqa	%xmm4, %xmm1
	psrldq	$8, %xmm1
	paddd	%xmm4, %xmm1
	movdqa	%xmm1, %xmm4
	psrldq	$4, %xmm4
	paddd	%xmm4, %xmm1
	movd	%xmm1, %eax
	leal	(%rax,%r14), %ebx
	movl	%esi, %eax
	andl	$-16, %eax
.L332:
	movslq	%eax, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L334
	addl	$1, %ebx
.L334:
	leal	1(%rax), %edx
	cmpl	%r14d, %edx
	jge	.L331
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L336
	addl	$1, %ebx
.L336:
	leal	2(%rax), %edx
	cmpl	%r14d, %edx
	jge	.L331
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L337
	addl	$1, %ebx
.L337:
	leal	3(%rax), %edx
	cmpl	%edx, %r14d
	jle	.L331
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L338
	addl	$1, %ebx
.L338:
	leal	4(%rax), %edx
	cmpl	%edx, %r14d
	jle	.L331
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L339
	addl	$1, %ebx
.L339:
	leal	5(%rax), %edx
	cmpl	%edx, %r14d
	jle	.L331
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L340
	addl	$1, %ebx
.L340:
	leal	6(%rax), %edx
	cmpl	%edx, %r14d
	jle	.L331
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L341
	addl	$1, %ebx
.L341:
	leal	7(%rax), %edx
	cmpl	%edx, %r14d
	jle	.L331
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L342
	addl	$1, %ebx
.L342:
	leal	8(%rax), %edx
	cmpl	%edx, %r14d
	jle	.L331
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L343
	addl	$1, %ebx
.L343:
	leal	9(%rax), %edx
	cmpl	%edx, %r14d
	jle	.L331
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L344
	addl	$1, %ebx
.L344:
	leal	10(%rax), %edx
	cmpl	%r14d, %edx
	jge	.L331
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L345
	addl	$1, %ebx
.L345:
	leal	11(%rax), %edx
	cmpl	%r14d, %edx
	jge	.L331
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L346
	addl	$1, %ebx
.L346:
	leal	12(%rax), %edx
	cmpl	%edx, %r14d
	jle	.L331
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L347
	addl	$1, %ebx
.L347:
	leal	13(%rax), %edx
	cmpl	%edx, %r14d
	jle	.L331
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L348
	addl	$1, %ebx
.L348:
	leal	14(%rax), %edx
	cmpl	%edx, %r14d
	jle	.L331
	movslq	%edx, %rdx
	cmpb	$5, 4(%rcx,%rdx,8)
	jne	.L349
	addl	$1, %ebx
.L349:
	addl	$15, %eax
	cmpl	%eax, %r14d
	jle	.L331
	cltq
	cmpb	$5, 4(%rcx,%rax,8)
	jne	.L331
	addl	$1, %ebx
	.p2align 4,,10
	.p2align 3
.L331:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13Int64Lowering15DefaultLoweringEPNS1_4NodeEb
	cmpl	%r14d, %ebx
	jne	.L568
	testb	%al, %al
	je	.L163
.L568:
	movq	160(%r12), %rax
	movq	24(%r12), %r14
	testq	%rax, %rax
	je	.L353
	cmpq	(%rax), %r15
	je	.L718
	cmpq	8(%rax), %r15
	je	.L719
.L353:
	movq	(%r12), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler24GetI32WasmCallDescriptorEPNS0_4ZoneEPKNS1_14CallDescriptorE@PLT
	movq	%rax, %rsi
.L355:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder8TailCallEPKNS1_14CallDescriptorE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L163
.L212:
	movq	144(%r12), %rax
	movq	8(%rax), %rdx
	testl	%edx, %edx
	jle	.L163
	movq	(%rax), %rsi
	movq	16(%rax), %r8
	leal	-1(%rdx), %eax
	cmpl	$14, %eax
	jbe	.L543
	movl	%edx, %ecx
	pxor	%xmm1, %xmm1
	pxor	%xmm7, %xmm7
	addq	%r8, %rsi
	shrl	$4, %ecx
	movdqa	.LC1(%rip), %xmm3
	movq	%rsi, %rax
	pxor	%xmm6, %xmm6
	salq	$4, %rcx
	movdqa	.LC2(%rip), %xmm2
	addq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L267:
	movdqu	(%rax), %xmm0
	movdqa	%xmm7, %xmm4
	movdqa	%xmm6, %xmm8
	addq	$16, %rax
	pcmpeqb	%xmm3, %xmm0
	pand	%xmm2, %xmm0
	pcmpgtb	%xmm0, %xmm4
	movdqa	%xmm0, %xmm5
	punpcklbw	%xmm4, %xmm5
	punpckhbw	%xmm4, %xmm0
	pcmpgtw	%xmm5, %xmm8
	movdqa	%xmm5, %xmm4
	punpcklwd	%xmm8, %xmm4
	punpckhwd	%xmm8, %xmm5
	paddd	%xmm4, %xmm1
	movdqa	%xmm6, %xmm4
	pcmpgtw	%xmm0, %xmm4
	paddd	%xmm5, %xmm1
	movdqa	%xmm0, %xmm5
	punpcklwd	%xmm4, %xmm5
	punpckhwd	%xmm4, %xmm0
	paddd	%xmm5, %xmm1
	paddd	%xmm0, %xmm1
	cmpq	%rcx, %rax
	jne	.L267
	movdqa	%xmm1, %xmm0
	movl	%edx, %ecx
	psrldq	$8, %xmm0
	andl	$-16, %ecx
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	addl	%edx, %eax
	testb	$15, %dl
	je	.L268
.L266:
	movslq	%ecx, %r8
	cmpb	$5, (%rsi,%r8)
	sete	%r8b
	movzbl	%r8b, %r8d
	addl	%r8d, %eax
	leal	1(%rcx), %r8d
	cmpl	%edx, %r8d
	jge	.L268
	movslq	%r8d, %r8
	cmpb	$5, (%rsi,%r8)
	sete	%r8b
	movzbl	%r8b, %r8d
	addl	%r8d, %eax
	leal	2(%rcx), %r8d
	cmpl	%r8d, %edx
	jle	.L268
	movslq	%r8d, %r8
	cmpb	$5, (%rsi,%r8)
	sete	%r8b
	movzbl	%r8b, %r8d
	addl	%r8d, %eax
	leal	3(%rcx), %r8d
	cmpl	%edx, %r8d
	jge	.L268
	movslq	%r8d, %r8
	cmpb	$5, (%rsi,%r8)
	sete	%r8b
	movzbl	%r8b, %r8d
	addl	%r8d, %eax
	leal	4(%rcx), %r8d
	cmpl	%r8d, %edx
	jle	.L268
	movslq	%r8d, %r8
	cmpb	$5, (%rsi,%r8)
	sete	%r8b
	movzbl	%r8b, %r8d
	addl	%r8d, %eax
	leal	5(%rcx), %r8d
	cmpl	%r8d, %edx
	jle	.L268
	movslq	%r8d, %r8
	cmpb	$5, (%rsi,%r8)
	sete	%r8b
	movzbl	%r8b, %r8d
	addl	%r8d, %eax
	leal	6(%rcx), %r8d
	cmpl	%r8d, %edx
	jle	.L268
	movslq	%r8d, %r8
	cmpb	$5, (%rsi,%r8)
	sete	%r8b
	movzbl	%r8b, %r8d
	addl	%r8d, %eax
	leal	7(%rcx), %r8d
	cmpl	%r8d, %edx
	jle	.L268
	movslq	%r8d, %r8
	cmpb	$5, (%rsi,%r8)
	sete	%r8b
	movzbl	%r8b, %r8d
	addl	%r8d, %eax
	leal	8(%rcx), %r8d
	cmpl	%r8d, %edx
	jle	.L268
	movslq	%r8d, %r8
	cmpb	$5, (%rsi,%r8)
	sete	%r8b
	movzbl	%r8b, %r8d
	addl	%r8d, %eax
	leal	9(%rcx), %r8d
	cmpl	%r8d, %edx
	jle	.L268
	movslq	%r8d, %r8
	cmpb	$5, (%rsi,%r8)
	sete	%r8b
	movzbl	%r8b, %r8d
	addl	%r8d, %eax
	leal	10(%rcx), %r8d
	cmpl	%r8d, %edx
	jle	.L268
	movslq	%r8d, %r8
	cmpb	$5, (%rsi,%r8)
	sete	%r8b
	movzbl	%r8b, %r8d
	addl	%r8d, %eax
	leal	11(%rcx), %r8d
	cmpl	%r8d, %edx
	jle	.L268
	movslq	%r8d, %r8
	cmpb	$5, (%rsi,%r8)
	sete	%r8b
	movzbl	%r8b, %r8d
	addl	%r8d, %eax
	leal	12(%rcx), %r8d
	cmpl	%r8d, %edx
	jle	.L268
	movslq	%r8d, %r8
	cmpb	$5, (%rsi,%r8)
	sete	%r8b
	movzbl	%r8b, %r8d
	addl	%r8d, %eax
	leal	13(%rcx), %r8d
	cmpl	%r8d, %edx
	jle	.L268
	movslq	%r8d, %r8
	cmpb	$5, (%rsi,%r8)
	sete	%r8b
	addl	$14, %ecx
	movzbl	%r8b, %r8d
	addl	%r8d, %eax
	cmpl	%ecx, %edx
	jle	.L268
	movslq	%ecx, %rcx
	cmpb	$5, (%rsi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %eax
	.p2align 4,,10
	.p2align 3
.L268:
	cmpl	%eax, %edx
	je	.L163
	subl	%edx, %eax
	addl	32(%rdi), %eax
	movq	24(%r12), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5StartEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L163
.L170:
	movzbl	23(%r13), %eax
	movq	32(%r13), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L477
	movq	16(%rdx), %rdx
.L477:
	movl	20(%rdx), %eax
	movq	16(%r12), %rdi
	movq	8(%r12), %r15
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	cmove	%rdx, %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder22SignExtendWord8ToInt32Ev@PLT
.L698:
	leaq	-96(%rbp), %r14
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%r14, %rcx
	movq	%rax, %rsi
	movq	%rbx, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %rdi
	movq	8(%r12), %r15
	movl	$31, %esi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-104(%rbp), %rbx
	movq	%rax, %r8
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
.L704:
	movq	%rbx, (%rax)
	movl	20(%r13), %eax
	movq	%r13, %rdi
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	%r8, 8(%rax)
	call	_ZN2v88internal8compiler4Node13NullAllInputsEv@PLT
	jmp	.L163
.L173:
	movzbl	23(%r13), %eax
	movq	32(%r13), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L447
	movq	16(%rbx), %rbx
.L447:
	movq	16(%r12), %rdi
	movq	8(%r12), %r14
	xorl	%edx, %edx
	movl	$5, %esi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9StackSlotENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r15
	movq	24(%r12), %rdi
	movl	$4, %esi
	movq	%rax, -104(%rbp)
	movl	20(%rbx), %eax
	movq	8(%r15), %r14
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	8(%rax), %rax
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movl	$4, %esi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder5StoreENS1_19StoreRepresentationE@PLT
	movq	%r14, %xmm2
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	-104(%rbp), %xmm0
	movq	%r14, -64(%rbp)
	leaq	-96(%rbp), %r14
	movl	$5, %edx
	movq	%r14, %rcx
	movq	%rax, %rsi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-120(%rbp), %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r15
	movq	24(%r12), %rdi
	xorl	%esi, %esi
	movq	%rax, -120(%rbp)
	movl	20(%rbx), %eax
	movq	8(%r15), %r9
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	%r9, -128(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movl	$4, %esi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder5StoreENS1_19StoreRepresentationE@PLT
	movq	-128(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-104(%rbp), %xmm0
	movl	$5, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%r9, -64(%rbp)
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r15
	movq	24(%r12), %rdi
	xorl	%esi, %esi
	movq	%rax, -120(%rbp)
	movq	8(%r15), %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movl	$1549, %esi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder4LoadENS0_11MachineTypeE@PLT
	movq	%rbx, %xmm3
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$4, %edx
	movq	%r15, %rdi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-120(%rbp), %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
.L699:
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	%r8, (%rax)
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	$0, 8(%rax)
	jmp	.L163
.L169:
	movzbl	23(%r13), %eax
	movq	32(%r13), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L479
	movq	16(%rdx), %rdx
.L479:
	movl	20(%rdx), %eax
	movq	16(%r12), %rdi
	movq	8(%r12), %r15
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	cmove	%rdx, %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder23SignExtendWord16ToInt32Ev@PLT
	jmp	.L698
.L211:
	movzbl	23(%rsi), %ebx
	andl	$15, %ebx
	cmpl	$15, %ebx
	jne	.L309
	movq	32(%rsi), %rax
	movl	8(%rax), %ebx
.L309:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13Int64Lowering15DefaultLoweringEPNS1_4NodeEb
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L311
	movq	32(%r13), %rax
	movl	8(%rax), %eax
.L311:
	cmpl	%ebx, %eax
	je	.L163
	movq	144(%r12), %rax
	movq	(%rax), %rdx
	testl	%edx, %edx
	jle	.L163
	movq	16(%rax), %rdi
	leal	-1(%rdx), %eax
	cmpl	$14, %eax
	jbe	.L546
	movl	%edx, %ecx
	pxor	%xmm1, %xmm1
	pxor	%xmm7, %xmm7
	movq	%rdi, %rax
	shrl	$4, %ecx
	movdqa	.LC1(%rip), %xmm3
	movdqa	.LC2(%rip), %xmm2
	pxor	%xmm6, %xmm6
	salq	$4, %rcx
	addq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L313:
	movdqu	(%rax), %xmm0
	movdqa	%xmm7, %xmm4
	movdqa	%xmm6, %xmm8
	addq	$16, %rax
	pcmpeqb	%xmm3, %xmm0
	pand	%xmm2, %xmm0
	pcmpgtb	%xmm0, %xmm4
	movdqa	%xmm0, %xmm5
	punpcklbw	%xmm4, %xmm5
	punpckhbw	%xmm4, %xmm0
	pcmpgtw	%xmm5, %xmm8
	movdqa	%xmm5, %xmm4
	punpcklwd	%xmm8, %xmm4
	punpckhwd	%xmm8, %xmm5
	paddd	%xmm4, %xmm1
	movdqa	%xmm6, %xmm4
	pcmpgtw	%xmm0, %xmm4
	paddd	%xmm5, %xmm1
	movdqa	%xmm0, %xmm5
	punpcklwd	%xmm4, %xmm5
	punpckhwd	%xmm4, %xmm0
	paddd	%xmm5, %xmm1
	paddd	%xmm0, %xmm1
	cmpq	%rax, %rcx
	jne	.L313
	movdqa	%xmm1, %xmm0
	movl	%edx, %eax
	psrldq	$8, %xmm0
	andl	$-16, %eax
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %esi
	addl	%edx, %esi
	testb	$15, %dl
	je	.L314
.L312:
	movslq	%eax, %rcx
	cmpb	$5, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %esi
	leal	1(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L314
	movslq	%ecx, %rcx
	cmpb	$5, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %esi
	leal	2(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L314
	movslq	%ecx, %rcx
	cmpb	$5, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %esi
	leal	3(%rax), %ecx
	cmpl	%edx, %ecx
	jge	.L314
	movslq	%ecx, %rcx
	cmpb	$5, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %esi
	leal	4(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L314
	movslq	%ecx, %rcx
	cmpb	$5, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %esi
	leal	5(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L314
	movslq	%ecx, %rcx
	cmpb	$5, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %esi
	leal	6(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L314
	movslq	%ecx, %rcx
	cmpb	$5, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %esi
	leal	7(%rax), %ecx
	cmpl	%edx, %ecx
	jge	.L314
	movslq	%ecx, %rcx
	cmpb	$5, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %esi
	leal	8(%rax), %ecx
	cmpl	%edx, %ecx
	jge	.L314
	movslq	%ecx, %rcx
	cmpb	$5, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %esi
	leal	9(%rax), %ecx
	cmpl	%edx, %ecx
	jge	.L314
	movslq	%ecx, %rcx
	cmpb	$5, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %esi
	leal	10(%rax), %ecx
	cmpl	%edx, %ecx
	jge	.L314
	movslq	%ecx, %rcx
	cmpb	$5, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %esi
	leal	11(%rax), %ecx
	cmpl	%edx, %ecx
	jge	.L314
	movslq	%ecx, %rcx
	cmpb	$5, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %esi
	leal	12(%rax), %ecx
	cmpl	%edx, %ecx
	jge	.L314
	movslq	%ecx, %rcx
	cmpb	$5, (%rdi,%rcx)
	sete	%cl
	movzbl	%cl, %ecx
	addl	%ecx, %esi
	leal	13(%rax), %ecx
	cmpl	%edx, %ecx
	jge	.L314
	movslq	%ecx, %rcx
	cmpb	$5, (%rdi,%rcx)
	sete	%cl
	addl	$14, %eax
	movzbl	%cl, %ecx
	addl	%ecx, %esi
	cmpl	%edx, %eax
	jge	.L314
	cltq
	cmpb	$5, (%rdi,%rax)
	sete	%al
	movzbl	%al, %eax
	addl	%eax, %esi
	.p2align 4,,10
	.p2align 3
.L314:
	cmpl	%esi, %edx
	je	.L163
	movq	24(%r12), %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6ReturnEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L163
.L174:
	movzbl	23(%r13), %eax
	movq	32(%r13), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L448
	movq	16(%rdx), %rdx
.L448:
	movl	20(%rdx), %eax
	movq	16(%r12), %rdi
	movl	$5, %esi
	movq	8(%r12), %r14
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	cmove	%rdx, %rbx
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9StackSlotENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r14
	movq	24(%r12), %rdi
	xorl	%esi, %esi
	movq	%rax, -104(%rbp)
	movq	8(%r14), %r15
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movl	$13, %esi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder5StoreENS1_19StoreRepresentationE@PLT
	movq	%r15, %xmm2
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-104(%rbp), %xmm0
	movq	%r15, -64(%rbp)
	leaq	-96(%rbp), %r15
	movl	$5, %edx
	movq	%r15, %rcx
	movq	%rax, %rsi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r14
	movq	24(%r12), %rdi
	movl	$4, %esi
	movq	%rax, -112(%rbp)
	movq	8(%r14), %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movl	$516, %esi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder4LoadENS0_11MachineTypeE@PLT
	movq	%rbx, %xmm3
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	-104(%rbp), %xmm0
	movl	$4, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-112(%rbp), %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r14
	movq	24(%r12), %rdi
	xorl	%esi, %esi
	movq	%rax, %rbx
	movq	8(%r14), %rax
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movl	$516, %esi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder4LoadENS0_11MachineTypeE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$4, %edx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-112(%rbp), %xmm0
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
.L703:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	%r8, (%rax)
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	%rbx, 8(%rax)
	jmp	.L163
.L175:
	movl	20(%r13), %eax
	movq	32(%r13), %rdx
	movl	%eax, %ecx
	xorl	$251658240, %ecx
	andl	$251658240, %ecx
	jne	.L391
	movq	16(%rdx), %rdx
.L391:
	movl	20(%rdx), %edx
	movq	136(%r12), %rcx
	andl	$16777215, %eax
	movq	%r13, %rdi
	salq	$4, %rax
	andl	$16777215, %edx
	salq	$4, %rdx
	movq	(%rcx,%rdx), %rdx
	movq	%rdx, (%rcx,%rax)
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	$0, 8(%rax)
	call	_ZN2v88internal8compiler4Node13NullAllInputsEv@PLT
	jmp	.L163
.L176:
	movzbl	23(%r13), %eax
	movq	32(%r13), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L445
	movq	16(%rdx), %rdx
.L445:
	movl	20(%rdx), %eax
	movq	24(%r12), %rdi
	movq	8(%r12), %r14
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	cmove	%rdx, %rbx
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
.L705:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	jmp	.L704
.L167:
	movzbl	23(%r13), %eax
	movq	32(%r13), %rdx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L443
	movq	16(%rdx), %rdx
.L443:
	movl	20(%rdx), %eax
	movq	24(%r12), %rdi
	movl	$31, %esi
	movq	8(%r12), %r14
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	cmove	%rdx, %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
	movq	%rbx, %xmm0
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movhps	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -96(%rbp)
	jmp	.L705
.L177:
	movzbl	23(%r13), %eax
	movq	32(%r13), %r9
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L476
	movq	16(%r9), %r9
.L476:
	movl	20(%r9), %eax
	movq	16(%r12), %rdi
	movq	%r9, -104(%rbp)
	leaq	-96(%rbp), %r15
	movq	8(%r12), %r14
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder18Word32ReverseBytesEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rbx, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-104(%rbp), %r9
	movq	16(%r12), %rdi
	movq	%rax, %rbx
	movq	8(%r12), %r14
	movl	20(%r9), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	8(%rax), %rdx
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder18Word32ReverseBytesEv@PLT
	movq	-104(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rax, %rsi
	movq	%rdx, -96(%rbp)
	movl	$1, %edx
	jmp	.L703
.L178:
	movzbl	23(%r13), %eax
	movq	32(%r13), %r10
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L459
	movq	16(%r10), %r10
.L459:
	movq	24(%r12), %rdi
	movq	8(%r12), %r15
	xorl	%esi, %esi
	movq	%r10, -112(%rbp)
	leaq	-96(%rbp), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-112(%rbp), %r10
	movq	16(%r12), %rdi
	movq	%rax, -104(%rbp)
	movl	20(%r10), %eax
	movq	%r10, -136(%rbp)
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	%rbx, %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r15
	movq	24(%r12), %r9
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -104(%rbp)
	movq	%r9, %rdi
	movq	8(%r15), %rbx
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%rbx, %xmm3
	movq	%rax, %rsi
	movq	%r15, %rdi
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-112(%rbp), %r9
	movq	%rax, %rbx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%rbx, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-112(%rbp), %r9
	movq	%rax, -104(%rbp)
	movq	%r9, %rdi
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%rbx, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-120(%rbp), %r9
	movl	$2, %esi
	movq	%rax, -112(%rbp)
	movq	%r9, %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r14, %rcx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	8(%r12), %rbx
	movq	-136(%rbp), %r10
	movq	%rax, -128(%rbp)
	movl	20(%r10), %eax
	movq	%r10, -144(%rbp)
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %rcx
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32CtzEv@PLT
	movq	-104(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%rdx, %rsi
	movl	$1, %edx
	movq	%rcx, -96(%rbp)
	movq	%r14, %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %rdi
	movq	8(%r12), %rbx
	movl	$32, %esi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r11
	movq	16(%r12), %rdi
	movq	-144(%rbp), %r10
	movq	%rax, -112(%rbp)
	movq	%r11, -136(%rbp)
	movl	20(%r10), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	8(%rax), %rcx
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32CtzEv@PLT
	movq	-104(%rbp), %rcx
	movq	%rdx, %rsi
	movq	%rcx, -96(%rbp)
.L710:
	movq	-136(%rbp), %r11
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movl	$1, %edx
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %edx
	movl	$4, %esi
	movq	-152(%rbp), %r9
	movq	%rax, -104(%rbp)
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-128(%rbp), %rax
	movq	%r15, %rdi
	movhps	-120(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %rdi
	movq	8(%r12), %r14
	xorl	%esi, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
.L709:
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	%rbx, (%rax)
	jmp	.L691
.L179:
	movzbl	23(%r13), %eax
	movq	32(%r13), %r10
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L458
	movq	16(%r10), %r10
.L458:
	movq	24(%r12), %rdi
	movq	8(%r12), %r15
	xorl	%esi, %esi
	movq	%r10, -112(%rbp)
	leaq	-96(%rbp), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-112(%rbp), %r10
	movq	16(%r12), %rdi
	movq	%rax, -104(%rbp)
	movl	20(%r10), %eax
	movq	%r10, -136(%rbp)
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	8(%rax), %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	%rbx, %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r15
	movq	24(%r12), %r9
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rax, -104(%rbp)
	movq	%r9, %rdi
	movq	8(%r15), %rbx
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%rbx, %xmm2
	movq	%rax, %rsi
	movq	%r15, %rdi
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-112(%rbp), %r9
	movq	%rax, %rbx
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%rbx, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-112(%rbp), %r9
	movq	%rax, -104(%rbp)
	movq	%r9, %rdi
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%rbx, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-120(%rbp), %r9
	movl	$2, %esi
	movq	%rax, -112(%rbp)
	movq	%r9, %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	8(%r12), %rbx
	movq	-136(%rbp), %r10
	movq	%rax, -128(%rbp)
	movl	20(%r10), %eax
	movq	%r10, -144(%rbp)
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	8(%rax), %rdx
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ClzEv@PLT
	movq	-104(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rdx, -96(%rbp)
	movl	$1, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %rdi
	movq	8(%r12), %rbx
	movl	$32, %esi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r11
	movq	16(%r12), %rdi
	movq	-144(%rbp), %r10
	movq	%rax, -112(%rbp)
	movq	%r11, -136(%rbp)
	movl	20(%r10), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %rdx
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ClzEv@PLT
	movq	-104(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rdx, -96(%rbp)
	jmp	.L710
.L180:
	movzbl	23(%r13), %eax
	movq	32(%r13), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L460
	movq	16(%rbx), %rbx
.L460:
	movq	24(%r12), %rdi
	movq	8(%r12), %r14
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	8(%r12), %r14
	movq	%rax, -128(%rbp)
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	8(%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder12Word32PopcntEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%r15, -96(%rbp)
	leaq	-96(%rbp), %r15
	movq	%rdx, %rsi
	movl	$1, %edx
	movq	%r15, %rcx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r10
	movq	16(%r12), %rdi
	movq	%rax, -112(%rbp)
	movl	20(%rbx), %eax
	movq	%r10, -120(%rbp)
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %r8
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder12Word32PopcntEv@PLT
	movq	-120(%rbp), %r10
	movq	-104(%rbp), %r8
	movq	%r15, %rcx
	movq	%rdx, %rsi
	movl	$1, %edx
	movq	%r10, %rdi
	movq	%r8, -96(%rbp)
	xorl	%r8d, %r8d
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-128(%rbp), %r9
	movq	%rax, %r8
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	%r8, (%rax)
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	%r9, 8(%rax)
	jmp	.L163
.L171:
	cmpl	$431, %ecx
	je	.L720
	call	_ZN2v88internal8compiler30UnalignedStoreRepresentationOfEPKNS1_8OperatorE@PLT
	movzbl	(%rax), %eax
.L237:
	cmpb	$5, %al
	je	.L721
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13Int64Lowering15DefaultLoweringEPNS1_4NodeEb
	jmp	.L163
.L172:
	call	_ZN2v88internal8compiler20LoadRepresentationOfEPKNS1_8OperatorE@PLT
	cmpb	$5, %al
	jne	.L166
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	32(%r13), %rbx
	call	_ZN2v88internal8compiler13Int64Lowering23LowerMemoryBaseAndIndexEPNS1_4NodeE
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	movq	32(%r13), %rax
	je	.L218
	movq	%rax, -120(%rbp)
	leaq	40(%r13), %rax
.L219:
	movq	(%rax), %r14
	movq	24(%r12), %rdi
	movl	$4, %esi
	movq	8(%r12), %r15
	movq	%r14, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	movq	%r14, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	leaq	-96(%rbp), %r14
	movq	%rax, %rsi
	movhps	-112(%rbp), %xmm0
	movl	$2, %edx
	movq	%r14, %rcx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movl	$516, %esi
	movq	%rax, -112(%rbp)
	movq	0(%r13), %rax
	cmpw	$429, 16(%rax)
	je	.L722
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13UnalignedLoadENS0_11MachineTypeE@PLT
	movq	%rax, %r15
.L221:
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L222
	cmpl	$2, %eax
	jbe	.L224
	movq	16(%rbx), %rdx
	leaq	24(%rbx), %rax
.L226:
	movq	-120(%rbp), %xmm0
	movq	(%rax), %rax
	movq	%r14, %rcx
	movq	%rdx, -80(%rbp)
	movq	8(%r12), %rdi
	xorl	%r8d, %r8d
	movl	$4, %edx
	movq	%r15, %rsi
	movhps	-112(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L227
	movq	16(%rbx), %rdi
	cmpq	%rdi, %r14
	je	.L228
	leaq	16(%rbx), %rax
	movq	%r13, %rdx
.L229:
	leaq	-72(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L231
	movq	%rax, -120(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rsi
.L231:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L232
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L232:
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L723
.L228:
	movq	8(%rbx), %rdi
	cmpq	%rdi, -104(%rbp)
	je	.L233
	addq	$8, %rbx
	movq	%r13, %rax
.L234:
	leaq	-48(%rax), %rsi
	testq	%rdi, %rdi
	je	.L235
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-112(%rbp), %rsi
.L235:
	movq	-104(%rbp), %rax
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L233
	movq	%rax, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L233:
	movq	%r15, %rsi
.L692:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	%r13, (%rax)
.L701:
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	%r14, 8(%rax)
	jmp	.L163
.L181:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movl	%eax, %r14d
	cmpb	$5, %al
	je	.L724
.L507:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal8compiler13Int64Lowering15DefaultLoweringEPNS1_4NodeEb
	movq	16(%r12), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder27Word32AtomicCompareExchangeENS0_11MachineTypeE@PLT
.L697:
	movq	%rax, %rsi
.L693:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	24(%r12), %rdi
	movq	8(%r12), %r14
	xorl	%esi, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
.L694:
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r8
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	%r13, (%rax)
	jmp	.L691
.L182:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rdi
	movl	%eax, %esi
	cmpb	$5, %al
	je	.L725
.L505:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Word32AtomicExchangeENS0_11MachineTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L706:
	movq	%rax, %r14
	movq	%r13, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13Int64Lowering15DefaultLoweringEPNS1_4NodeEb
	movq	%r14, %rsi
	jmp	.L693
.L183:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rdi
	movl	%eax, %esi
	cmpb	$5, %al
	je	.L726
.L502:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Word32AtomicXorENS0_11MachineTypeE@PLT
	jmp	.L706
.L184:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rdi
	movl	%eax, %esi
	cmpb	$5, %al
	je	.L727
.L499:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Word32AtomicOrENS0_11MachineTypeE@PLT
	jmp	.L706
.L185:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rdi
	movl	%eax, %esi
	cmpb	$5, %al
	je	.L728
.L496:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Word32AtomicAndENS0_11MachineTypeE@PLT
	jmp	.L706
.L187:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rdi
	movl	%eax, %esi
	cmpb	$5, %al
	je	.L729
.L490:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Word32AtomicAddENS0_11MachineTypeE@PLT
	jmp	.L706
.L186:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rdi
	movl	%eax, %esi
	cmpb	$5, %al
	je	.L730
.L493:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder15Word32AtomicSubENS0_11MachineTypeE@PLT
	jmp	.L706
.L188:
	call	_ZN2v88internal8compiler27AtomicStoreRepresentationOfEPKNS1_8OperatorE@PLT
	movl	%eax, %r14d
	cmpb	$5, %al
	je	.L731
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13Int64Lowering15DefaultLoweringEPNS1_4NodeEb
	movq	16(%r12), %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder17Word32AtomicStoreENS0_21MachineRepresentationE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L163
.L189:
	call	_ZN2v88internal8compiler12AtomicOpTypeEPKNS1_8OperatorE@PLT
	movq	%r12, %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movl	%eax, %r14d
	call	_ZN2v88internal8compiler13Int64Lowering15DefaultLoweringEPNS1_4NodeEb
	movq	16(%r12), %rdi
	cmpb	$5, %r14b
	je	.L732
.L482:
	movl	%r14d, %esi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder16Word32AtomicLoadENS0_11MachineTypeE@PLT
	jmp	.L697
.L197:
	movzbl	23(%r13), %eax
	movq	136(%r12), %rdx
	leaq	32(%r13), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L392
	movq	8(%rbx), %r14
	movq	%r13, %rcx
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rdx, %rax
	leaq	40(%r13), %rdx
	movq	(%rax), %r8
	cmpq	%r8, %r14
	je	.L394
.L395:
	leaq	-48(%rcx), %r15
	movq	%r14, %rdi
	movq	%r8, -112(%rbp)
	movq	%r15, %rsi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	movq	%r8, (%rdx)
	testq	%r8, %r8
	je	.L679
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L679:
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
.L394:
	movq	8(%rax), %rdx
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L396
	movq	32(%r13), %r14
	movq	%r13, %rdx
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %r8
	cmpq	%r8, %r14
	je	.L398
.L397:
	leaq	-24(%rdx), %r15
	movq	%r14, %rdi
	movq	%r8, -104(%rbp)
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %r8
	movq	%r8, (%rbx)
	testq	%r8, %r8
	je	.L680
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L680:
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
.L398:
	movq	8(%rax), %rcx
	movq	(%r12), %rsi
	movq	%r13, %rdi
	movl	$1, %edx
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder12Int32PairAddEv@PLT
	.p2align 4,,10
	.p2align 3
.L696:
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
.L695:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13Int64Lowering26ReplaceNodeWithProjectionsEPNS1_4NodeE
	jmp	.L163
.L193:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
.L700:
	movq	16(%r12), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
.L702:
	movq	%rax, %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13Int64Lowering15LowerComparisonEPNS1_4NodeEPKNS1_8OperatorES7_
	jmp	.L163
.L194:
	movzbl	23(%r13), %eax
	movq	32(%r13), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L441
	leaq	40(%r13), %rax
.L442:
	movq	8(%r12), %r11
	movq	24(%r12), %rdi
	xorl	%esi, %esi
	movq	(%rax), %rbx
	movq	%r11, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	movq	-104(%rbp), %r11
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	movq	%r11, %rdi
	movq	%r11, -144(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r10
	movq	16(%r12), %rdi
	movq	%rax, -120(%rbp)
	movl	20(%rbx), %eax
	movq	136(%r12), %rdx
	movq	%r10, -112(%rbp)
	andl	$16777215, %eax
	salq	$4, %rax
	movq	8(%rdx,%rax), %rax
	movq	%rax, -104(%rbp)
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	8(%rdx,%rax), %r15
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32XorEv@PLT
	movq	-112(%rbp), %r10
	xorl	%r8d, %r8d
	movl	$2, %edx
	movq	%r15, %xmm0
	leaq	-96(%rbp), %r15
	movq	%rax, %rsi
	movq	%r10, %rdi
	movhps	-104(%rbp), %xmm0
	movq	%r15, %rcx
	movq	%r10, -136(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r9
	movq	16(%r12), %rdi
	movq	%rax, -112(%rbp)
	movl	20(%rbx), %eax
	movq	136(%r12), %rdx
	movq	%r9, -128(%rbp)
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rdx,%rax), %rax
	movq	%rax, -104(%rbp)
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rdx,%rax), %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32XorEv@PLT
	movq	-128(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%rbx, %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r9, %rdi
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Word32OrEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-136(%rbp), %r10
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movhps	-112(%rbp), %xmm0
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder11Word32EqualEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	-144(%rbp), %r11
	movq	%rax, %rsi
	movhps	-120(%rbp), %xmm0
	movq	%r11, %rdi
	movaps	%xmm0, -96(%rbp)
	jmp	.L699
.L195:
	movzbl	23(%r13), %eax
	movq	136(%r12), %rdx
	leaq	32(%r13), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L406
	movq	8(%rbx), %r14
	movq	%r13, %rcx
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rdx, %rax
	leaq	40(%r13), %rdx
	movq	(%rax), %r8
	cmpq	%r8, %r14
	je	.L408
.L409:
	leaq	-48(%rcx), %r15
	movq	%r14, %rdi
	movq	%r8, -112(%rbp)
	movq	%r15, %rsi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	movq	%r8, (%rdx)
	testq	%r8, %r8
	je	.L683
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L683:
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
.L408:
	movq	8(%rax), %rdx
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L410
	movq	32(%r13), %r14
	movq	%r13, %rdx
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %r8
	cmpq	%r8, %r14
	je	.L412
.L411:
	leaq	-24(%rdx), %r15
	movq	%r14, %rdi
	movq	%r8, -104(%rbp)
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %r8
	movq	%r8, (%rbx)
	testq	%r8, %r8
	je	.L684
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L684:
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
.L412:
	movq	8(%rax), %rcx
	movq	(%r12), %rsi
	movq	%r13, %rdi
	movl	$1, %edx
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder12Int32PairMulEv@PLT
	jmp	.L696
.L196:
	movzbl	23(%r13), %eax
	movq	136(%r12), %rdx
	leaq	32(%r13), %rbx
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L399
	movq	8(%rbx), %r14
	movq	%r13, %rcx
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rdx, %rax
	leaq	40(%r13), %rdx
	movq	(%rax), %r8
	cmpq	%r8, %r14
	je	.L401
.L402:
	leaq	-48(%rcx), %r15
	movq	%r14, %rdi
	movq	%r8, -112(%rbp)
	movq	%r15, %rsi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	movq	%r8, (%rdx)
	testq	%r8, %r8
	je	.L681
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L681:
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
.L401:
	movq	8(%rax), %rdx
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node11AppendInputEPNS0_4ZoneEPS2_@PLT
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L403
	movq	32(%r13), %r14
	movq	%r13, %rdx
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %r8
	cmpq	%r8, %r14
	je	.L405
.L404:
	leaq	-24(%rdx), %r15
	movq	%r14, %rdi
	movq	%r8, -104(%rbp)
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %r8
	movq	%r8, (%rbx)
	testq	%r8, %r8
	je	.L682
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L682:
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
.L405:
	movq	8(%rax), %rcx
	movq	(%r12), %rsi
	movq	%r13, %rdi
	movl	$1, %edx
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder12Int32PairSubEv@PLT
	jmp	.L696
.L191:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	movq	16(%r12), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	jmp	.L702
.L192:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21Uint32LessThanOrEqualEv@PLT
	jmp	.L700
.L190:
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21Uint32LessThanOrEqualEv@PLT
	movq	16(%r12), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14Uint32LessThanEv@PLT
	jmp	.L702
.L201:
	movzbl	23(%r13), %edx
	movq	136(%r12), %rcx
	leaq	32(%r13), %r14
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L417
	movq	8(%r14), %rdi
	movl	20(%rdi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rcx,%rax), %r15
	cmpq	%r15, %rdi
	je	.L420
	testq	%r15, %r15
	leaq	40(%r13), %rax
	movq	%r13, %rdx
	setne	%sil
	testb	%sil, %sil
	je	.L420
.L422:
	leaq	-48(%rdx), %rbx
	movq	%rax, -104(%rbp)
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%r15, (%rax)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L733
	movq	136(%r12), %rcx
.L420:
	movq	32(%r13), %rbx
	movq	%r13, %rdx
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rcx, %rax
	movq	(%rax), %r15
	cmpq	%r15, %rbx
	je	.L424
.L423:
	leaq	-24(%rdx), %rsi
	movq	%rbx, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	testq	%r15, %r15
	movq	%r15, (%r14)
	movq	-104(%rbp), %rsi
	je	.L685
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L685:
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
.L424:
	movq	8(%rax), %rcx
	movq	(%r12), %rsi
	movq	%r13, %rdi
	movl	$1, %edx
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Word32PairShlEv@PLT
	jmp	.L696
.L202:
	movzbl	23(%r13), %eax
	movq	32(%r13), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L415
	leaq	40(%r13), %rax
.L416:
	movq	(%rax), %r9
	movq	136(%r12), %rdx
	movq	16(%r12), %rdi
	movq	8(%r12), %r15
	movl	20(%r9), %eax
	movq	%r9, -120(%rbp)
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rdx,%rax), %rax
	movq	%rax, -104(%rbp)
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rdx,%rax), %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32XorEv@PLT
	movq	%r15, %rdi
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rbx, %xmm0
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%rcx, -112(%rbp)
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-120(%rbp), %r9
	movq	16(%r12), %rdi
	movq	%rax, %rbx
	movq	136(%r12), %rdx
	movq	8(%r12), %r15
	movl	20(%r9), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	8(%rdx,%rax), %rax
	movq	%rax, -104(%rbp)
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	8(%rdx,%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32XorEv@PLT
.L707:
	movq	%r14, %xmm0
	movq	-112(%rbp), %rcx
	movq	%rax, %rsi
	xorl	%r8d, %r8d
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
.L708:
	movl	$2, %edx
	movq	%r15, %rdi
	jmp	.L709
.L203:
	movzbl	23(%r13), %eax
	movq	32(%r13), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L413
	leaq	40(%r13), %rax
.L414:
	movq	(%rax), %r9
	movq	136(%r12), %rdx
	movq	16(%r12), %rdi
	movq	8(%r12), %r15
	movl	20(%r9), %eax
	movq	%r9, -120(%rbp)
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rdx,%rax), %rax
	movq	%rax, -104(%rbp)
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rdx,%rax), %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Word32OrEv@PLT
	leaq	-96(%rbp), %rcx
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%rbx, %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%rcx, -112(%rbp)
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-120(%rbp), %r9
	movq	16(%r12), %rdi
	movq	%rax, %rbx
	movq	136(%r12), %rdx
	movq	8(%r12), %r15
	movl	20(%r9), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	8(%rdx,%rax), %rax
	movq	%rax, -104(%rbp)
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	8(%rdx,%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Word32OrEv@PLT
	jmp	.L707
.L204:
	movzbl	23(%r13), %eax
	movq	32(%r13), %r14
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L389
	leaq	40(%r13), %rax
.L390:
	movq	(%rax), %r9
	movq	136(%r12), %rdx
	movq	16(%r12), %rdi
	movq	8(%r12), %r15
	movl	20(%r9), %eax
	movq	%r9, -120(%rbp)
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rdx,%rax), %rax
	movq	%rax, -104(%rbp)
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rdx,%rax), %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	leaq	-96(%rbp), %rcx
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movq	%rbx, %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%rcx, -112(%rbp)
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-120(%rbp), %r9
	movq	16(%r12), %rdi
	movq	%rax, %rbx
	movq	136(%r12), %rdx
	movq	8(%r12), %r15
	movl	20(%r9), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	8(%rdx,%rax), %rax
	movq	%rax, -104(%rbp)
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	8(%rdx,%rax), %r14
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	jmp	.L707
.L199:
	movzbl	23(%r13), %edx
	movq	136(%r12), %rcx
	leaq	32(%r13), %r14
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L433
	movq	8(%r14), %rdi
	movl	20(%rdi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rcx,%rax), %r15
	cmpq	%r15, %rdi
	je	.L436
	testq	%r15, %r15
	leaq	40(%r13), %rax
	movq	%r13, %rdx
	setne	%sil
	testb	%sil, %sil
	je	.L436
.L438:
	leaq	-48(%rdx), %rbx
	movq	%rax, -104(%rbp)
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%r15, (%rax)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L734
	movq	136(%r12), %rcx
.L436:
	movq	32(%r13), %rbx
	movq	%r13, %rdx
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rcx, %rax
	movq	(%rax), %r15
	cmpq	%r15, %rbx
	je	.L440
.L439:
	leaq	-24(%rdx), %rsi
	movq	%rbx, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	testq	%r15, %r15
	movq	%r15, (%r14)
	movq	-104(%rbp), %rsi
	je	.L687
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L687:
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
.L440:
	movq	8(%rax), %rcx
	movq	(%r12), %rsi
	movq	%r13, %rdi
	movl	$1, %edx
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Word32PairSarEv@PLT
	jmp	.L696
.L198:
	movl	20(%r13), %edx
	movq	32(%r13), %r10
	movl	%edx, %eax
	xorl	$251658240, %eax
	testl	$251658240, %eax
	je	.L450
	leaq	40(%r13), %rax
.L451:
	movq	(%rax), %rcx
	movq	136(%r12), %rsi
	movl	20(%rcx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rsi,%rax), %rbx
	testq	%rbx, %rbx
	cmove	%rcx, %rbx
	movq	(%rbx), %rax
	cmpw	$23, 16(%rax)
	jne	.L453
	movl	44(%rax), %ecx
	movl	20(%r10), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rsi, %rax
	movq	8(%rax), %r10
	movq	(%rax), %r14
	movl	%ecx, %eax
	andl	$63, %eax
	jne	.L454
	andl	$16777215, %edx
	salq	$4, %rdx
	movq	%r14, (%rsi,%rdx)
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	%r10, 8(%rax)
	jmp	.L163
.L200:
	movzbl	23(%r13), %edx
	movq	136(%r12), %rcx
	leaq	32(%r13), %r14
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L425
	movq	8(%r14), %rdi
	movl	20(%rdi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rcx,%rax), %r15
	cmpq	%r15, %rdi
	je	.L428
	testq	%r15, %r15
	leaq	40(%r13), %rax
	movq	%r13, %rdx
	setne	%sil
	testb	%sil, %sil
	je	.L428
.L430:
	leaq	-48(%rdx), %rbx
	movq	%rax, -104(%rbp)
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%r15, (%rax)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L735
	movq	136(%r12), %rcx
.L428:
	movq	32(%r13), %rbx
	movq	%r13, %rdx
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rcx, %rax
	movq	(%rax), %r15
	cmpq	%r15, %rbx
	je	.L432
.L431:
	leaq	-24(%rdx), %rsi
	movq	%rbx, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	testq	%r15, %r15
	movq	%r15, (%r14)
	movq	-104(%rbp), %rsi
	je	.L686
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L686:
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
.L432:
	movq	8(%rax), %rcx
	movq	(%r12), %rsi
	movq	%r13, %rdi
	movl	$1, %edx
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Word32PairShrEv@PLT
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L470:
	movq	-120(%rbp), %rax
	movq	(%rax), %rcx
	leaq	16(%rcx,%r14), %rdx
	movq	(%rdx), %rdi
	cmpq	%rdi, %r9
	jne	.L471
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L468:
	movq	(%r15), %rdx
	leaq	16(%rdx,%r14), %rdx
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L453:
	movq	16(%r12), %rax
	movq	8(%r12), %r15
	movq	24(%r12), %rdi
	testb	$8, 21(%rax)
	je	.L524
	movq	%rbx, -112(%rbp)
	leaq	-96(%rbp), %r14
.L525:
	movl	$1, %esi
	movq	%r10, -136(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r9
	movq	24(%r12), %rdi
	movl	$-2147483648, %esi
	movq	%rax, -120(%rbp)
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	movq	-104(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r9, %rdi
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32SarEv@PLT
	movq	-128(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r9, %rdi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShlEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%r15, %rdi
	movq	%rax, %rsi
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %rdi
	movq	8(%r12), %r15
	movl	$-1, %esi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32XorEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-136(%rbp), %r10
	movq	%rax, -120(%rbp)
	movq	16(%r12), %rax
	testb	$8, 21(%rax)
	jne	.L736
.L457:
	movq	24(%r12), %rdi
	movq	8(%r12), %r15
	movl	$32, %esi
	movq	%r10, -144(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder13Int32LessThanEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movhps	-128(%rbp), %xmm0
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	$2, %edx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r15
	movq	24(%r12), %rbx
	xorl	%esi, %esi
	movq	%rax, -128(%rbp)
	movl	$1, %edx
	movq	8(%r15), %rax
	movq	%rbx, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-128(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-128(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-128(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rbx, %rdi
	movl	$2, %esi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%r14, %rcx
	movq	-136(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-144(%rbp), %r10
	movq	%rbx, %rdi
	movl	$2, %edx
	movq	%rax, -128(%rbp)
	movl	$4, %esi
	movl	20(%r10), %eax
	movq	%r10, -160(%rbp)
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	8(%rax), %rcx
	movq	%rcx, -144(%rbp)
	movq	(%rax), %rcx
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%r14, %rcx
	movq	-136(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-128(%rbp), %rax
	movl	$3, %edx
	movhps	-144(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-160(%rbp), %r10
	movq	%rbx, %rdi
	movl	$2, %edx
	movq	%rax, -152(%rbp)
	movl	$4, %esi
	movl	20(%r10), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %rcx
	movq	%rcx, -144(%rbp)
	movq	8(%rax), %rcx
	movq	%rcx, -136(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$3, %edx
	movq	-136(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-128(%rbp), %rax
	movq	%r15, %rdi
	movhps	-144(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	8(%r12), %r15
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32RorEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-152(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	8(%r12), %r15
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32RorEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-136(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	8(%r12), %r15
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	8(%r12), %rbx
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-128(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Word32OrEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-136(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	8(%r12), %r15
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$2, %edx
	movq	-128(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r9
	movq	16(%r12), %rdi
	movq	%rax, -128(%rbp)
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movq	-104(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	-112(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r9, %rdi
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Word32OrEv@PLT
	movq	-104(%rbp), %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%rax, %rsi
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	jmp	.L708
.L450:
	leaq	16(%r10), %rax
	movq	16(%r10), %r10
	addq	$8, %rax
	jmp	.L451
.L389:
	leaq	16(%r14), %rax
	movq	16(%r14), %r14
	addq	$8, %rax
	jmp	.L390
.L415:
	leaq	16(%r14), %rax
	movq	16(%r14), %r14
	addq	$8, %rax
	jmp	.L416
.L413:
	leaq	16(%r14), %rax
	movq	16(%r14), %r14
	addq	$8, %rax
	jmp	.L414
.L731:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13Int64Lowering23LowerMemoryBaseAndIndexEPNS1_4NodeE
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L485
	movq	48(%r13), %rbx
	leaq	48(%r13), %rcx
	movq	%r13, %rdx
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %r15
	cmpq	%r15, %rbx
	je	.L487
.L488:
	leaq	-72(%rdx), %r14
	movq	%rbx, %rdi
	movq	%rcx, -104(%rbp)
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %rcx
	movq	%r15, (%rcx)
	testq	%r15, %r15
	je	.L689
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L689:
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
.L487:
	movq	(%r12), %rsi
	movq	8(%rax), %rcx
	movl	$3, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder21Word32AtomicPairStoreEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	jmp	.L163
.L417:
	movq	32(%r13), %rdx
	movq	24(%rdx), %rdi
	leaq	16(%rdx), %rsi
	movl	20(%rdi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rcx,%rax), %r15
	cmpq	%r15, %rdi
	je	.L555
	leaq	24(%rdx), %rax
	testq	%r15, %r15
	jne	.L422
.L555:
	movq	%rsi, %r14
.L421:
	movq	(%r14), %rbx
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rcx, %rax
	movq	(%rax), %r15
	cmpq	%r15, %rbx
	jne	.L423
	jmp	.L424
.L403:
	movq	32(%r13), %rdx
	movq	16(%rdx), %r14
	leaq	16(%rdx), %rbx
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %r8
	cmpq	%r8, %r14
	jne	.L404
	jmp	.L405
.L399:
	movq	32(%r13), %rcx
	movq	24(%rcx), %r14
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rdx, %rax
	leaq	24(%rcx), %rdx
	movq	(%rax), %r8
	cmpq	%r8, %r14
	jne	.L402
	jmp	.L401
.L410:
	movq	32(%r13), %rdx
	movq	16(%rdx), %r14
	leaq	16(%rdx), %rbx
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %r8
	cmpq	%r8, %r14
	jne	.L411
	jmp	.L412
.L441:
	leaq	16(%r14), %rax
	movq	16(%r14), %r14
	addq	$8, %rax
	jmp	.L442
.L406:
	movq	32(%r13), %rcx
	movq	24(%rcx), %r14
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rdx, %rax
	leaq	24(%rcx), %rdx
	movq	(%rax), %r8
	cmpq	%r8, %r14
	jne	.L409
	jmp	.L408
.L396:
	movq	32(%r13), %rdx
	movq	16(%rdx), %r14
	leaq	16(%rdx), %rbx
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %r8
	cmpq	%r8, %r14
	jne	.L397
	jmp	.L398
.L425:
	movq	32(%r13), %rdx
	movq	24(%rdx), %rdi
	leaq	16(%rdx), %rsi
	movl	20(%rdi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rcx,%rax), %r15
	cmpq	%r15, %rdi
	je	.L557
	leaq	24(%rdx), %rax
	testq	%r15, %r15
	jne	.L430
.L557:
	movq	%rsi, %r14
.L429:
	movq	(%r14), %rbx
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rcx, %rax
	movq	(%rax), %r15
	cmpq	%r15, %rbx
	jne	.L431
	jmp	.L432
.L433:
	movq	32(%r13), %rdx
	movq	24(%rdx), %rdi
	leaq	16(%rdx), %rsi
	movl	20(%rdi), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rcx,%rax), %r15
	cmpq	%r15, %rdi
	je	.L559
	leaq	24(%rdx), %rax
	testq	%r15, %r15
	jne	.L438
.L559:
	movq	%rsi, %r14
.L437:
	movq	(%r14), %rbx
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rcx, %rax
	movq	(%rax), %r15
	cmpq	%r15, %rbx
	jne	.L439
	jmp	.L440
.L732:
	movl	%r14d, %eax
	movzbl	%ah, %eax
	cmpb	$5, %al
	jne	.L482
	call	_ZN2v88internal8compiler22MachineOperatorBuilder20Word32AtomicPairLoadEv@PLT
	jmp	.L696
.L392:
	movq	32(%r13), %rcx
	movq	24(%rcx), %r14
	movl	20(%r14), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rdx, %rax
	leaq	24(%rcx), %rdx
	movq	(%rax), %r8
	cmpq	%r8, %r14
	jne	.L395
	jmp	.L394
.L724:
	movzbl	%ah, %eax
	cmpb	$5, %al
	jne	.L507
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	32(%r13), %r14
	call	_ZN2v88internal8compiler13Int64Lowering23LowerMemoryBaseAndIndexEPNS1_4NodeE
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L508
	movq	16(%r14), %r15
	movq	24(%r14), %rdi
	leaq	48(%r13), %rcx
	movq	%r13, %rsi
	movq	136(%r12), %rdx
	movl	20(%r15), %eax
	movq	%rdi, %rbx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rdx, %rax
	movq	(%rax), %r8
	cmpq	%r15, %r8
	je	.L737
.L512:
	subq	$72, %rsi
	movq	%r15, %rdi
	movq	%r8, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-120(%rbp), %r8
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rsi
	testq	%r8, %r8
	movq	%r8, (%rcx)
	je	.L534
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L534:
	movl	20(%r15), %eax
	movq	136(%r12), %rdx
	andl	$16777215, %eax
	salq	$4, %rax
	movq	8(%rdx,%rax), %r8
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L738
	movq	24(%r14), %rdi
.L536:
	cmpq	%rdi, %r8
	je	.L514
	addq	$24, %r14
	movq	%r13, %rax
.L515:
	leaq	-96(%rax), %r15
	testq	%rdi, %rdi
	je	.L516
	movq	%r15, %rsi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-104(%rbp), %r8
.L516:
	movq	%r8, (%r14)
	testq	%r8, %r8
	je	.L690
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L690:
	movq	136(%r12), %rdx
.L514:
	movl	20(%rbx), %eax
	movq	(%r12), %rsi
	movq	%r13, %rdi
	andl	$16777215, %eax
	salq	$4, %rax
	movq	(%rdx,%rax), %rcx
	movl	$4, %edx
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movl	20(%rbx), %eax
	movq	(%r12), %rsi
	movq	%r13, %rdi
	movl	$5, %edx
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	8(%rax), %rcx
	call	_ZN2v88internal8compiler4Node11InsertInputEPNS0_4ZoneEiPS2_@PLT
	movq	16(%r12), %rdi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder31Word32AtomicPairCompareExchangeEv@PLT
	jmp	.L696
.L725:
	movzbl	%ah, %eax
	cmpb	$5, %al
	jne	.L505
	call	_ZN2v88internal8compiler22MachineOperatorBuilder24Word32AtomicPairExchangeEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13Int64Lowering22LowerWord64AtomicBinopEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L163
.L726:
	movzbl	%ah, %eax
	cmpb	$5, %al
	jne	.L502
	call	_ZN2v88internal8compiler22MachineOperatorBuilder19Word32AtomicPairXorEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13Int64Lowering22LowerWord64AtomicBinopEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L163
.L721:
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	32(%r13), %rbx
	call	_ZN2v88internal8compiler13Int64Lowering23LowerMemoryBaseAndIndexEPNS1_4NodeE
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	movq	32(%r13), %rax
	je	.L239
	movq	%rax, -128(%rbp)
	leaq	40(%r13), %rax
.L240:
	movq	(%rax), %r14
	movq	24(%r12), %rdi
	movl	$4, %esi
	movq	8(%r12), %r15
	movq	%r14, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Int32AddEv@PLT
	movq	%r14, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	leaq	-96(%rbp), %r14
	movhps	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r14, %rcx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, -120(%rbp)
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L241
	leaq	16(%rbx), %rax
.L242:
	movq	0(%r13), %rdi
	movq	(%rax), %r15
	cmpw	$431, 16(%rdi)
	je	.L739
	movq	16(%r12), %rdi
	movl	$4, %esi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder14UnalignedStoreENS0_21MachineRepresentationE@PLT
	movq	%rax, -104(%rbp)
.L244:
	movzbl	23(%r13), %eax
	movl	%eax, %edx
	andl	$15, %edx
	cmpl	$15, %edx
	je	.L245
	testb	$12, %al
	je	.L247
	movq	24(%rbx), %rdx
	leaq	32(%rbx), %rax
.L249:
	movq	(%rax), %rcx
	movl	20(%r15), %eax
	xorl	%r8d, %r8d
	movq	-128(%rbp), %xmm0
	movq	8(%r12), %rdi
	andl	$16777215, %eax
	movq	-104(%rbp), %rsi
	salq	$4, %rax
	addq	136(%r12), %rax
	movhps	-120(%rbp), %xmm0
	movq	8(%rax), %rax
	movq	%rdx, -72(%rbp)
	movl	$5, %edx
	movq	%rcx, -64(%rbp)
	movq	%r14, %rcx
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L250
	movq	24(%rbx), %rdi
	cmpq	%rdi, %r14
	je	.L251
	leaq	24(%rbx), %rax
	movq	%r13, %rdx
.L252:
	leaq	-96(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L254
	movq	%rax, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rsi
.L254:
	movq	%r14, (%rax)
	testq	%r14, %r14
	je	.L255
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L255:
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L740
.L251:
	movq	8(%rbx), %rdi
	cmpq	%rdi, -112(%rbp)
	je	.L256
	leaq	8(%rbx), %rax
	movq	%r13, %rdx
.L257:
	leaq	-48(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L259
	movq	%rax, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rsi
.L259:
	movq	-112(%rbp), %rcx
	movq	%rcx, (%rax)
	testq	%rcx, %rcx
	je	.L260
	movq	%rcx, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L260:
	movl	20(%r15), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %r8
	movzbl	23(%r13), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L741
.L537:
	movq	16(%rbx), %rdi
	cmpq	%rdi, %r8
	je	.L262
	addq	$16, %rbx
	movq	%r13, %rax
.L263:
	leaq	-72(%rax), %r15
	testq	%rdi, %rdi
	je	.L264
	movq	%r15, %rsi
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-112(%rbp), %r8
.L264:
	movq	%r8, (%rbx)
	testq	%r8, %r8
	je	.L262
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L262:
	movq	-104(%rbp), %rsi
	jmp	.L692
.L720:
	call	_ZN2v88internal8compiler21StoreRepresentationOfEPKNS1_8OperatorE@PLT
	movzbl	(%rax), %eax
	jmp	.L237
.L727:
	movzbl	%ah, %eax
	cmpb	$5, %al
	jne	.L499
	call	_ZN2v88internal8compiler22MachineOperatorBuilder18Word32AtomicPairOrEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13Int64Lowering22LowerWord64AtomicBinopEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L163
.L729:
	movzbl	%ah, %eax
	cmpb	$5, %al
	jne	.L490
	call	_ZN2v88internal8compiler22MachineOperatorBuilder19Word32AtomicPairAddEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13Int64Lowering22LowerWord64AtomicBinopEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L163
.L728:
	movzbl	%ah, %eax
	cmpb	$5, %al
	jne	.L496
	call	_ZN2v88internal8compiler22MachineOperatorBuilder19Word32AtomicPairAndEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13Int64Lowering22LowerWord64AtomicBinopEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L163
.L730:
	movzbl	%ah, %eax
	cmpb	$5, %al
	jne	.L493
	call	_ZN2v88internal8compiler22MachineOperatorBuilder19Word32AtomicPairSubEv@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13Int64Lowering22LowerWord64AtomicBinopEPNS1_4NodeEPKNS1_8OperatorE
	jmp	.L163
.L454:
	cmpl	$32, %eax
	je	.L742
	testb	$32, %cl
	je	.L456
	movq	%r14, %rax
	movq	%r10, %r14
	movq	%rax, %r10
.L456:
	andl	$31, %ecx
	movq	24(%r12), %rdi
	movq	8(%r12), %rbx
	movq	%r10, -120(%rbp)
	movl	%ecx, %esi
	movl	%ecx, %r15d
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	24(%r12), %rdi
	movl	$32, %esi
	movq	8(%r12), %rbx
	subl	%r15d, %esi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	leaq	-96(%rbp), %rbx
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	8(%r12), %r15
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShlEv@PLT
	movq	-120(%rbp), %r10
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	movq	%r10, %xmm0
	movq	%r10, -136(%rbp)
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r9
	movq	16(%r12), %rdi
	movq	%rax, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShrEv@PLT
	movq	-120(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r14, %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r9, %rdi
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Word32OrEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-120(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	8(%r12), %r15
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShlEv@PLT
	movq	%r14, %xmm0
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movhps	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	8(%r12), %r14
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32ShrEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-136(%rbp), %r10
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	%r10, %xmm0
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder8Word32OrEv@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$2, %edx
	movq	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r15, %rdi
	movhps	-120(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-128(%rbp), %r9
	movq	%rax, %r8
	movl	20(%r13), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	%r9, (%rax)
	jmp	.L691
.L714:
	movq	16(%rax), %rsi
	jmp	.L380
.L718:
	movq	16(%rax), %rsi
	jmp	.L355
.L736:
	movq	24(%r12), %rdi
	movq	8(%r12), %r15
	movl	$63, %esi
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movq	%rbx, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movhps	-128(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-136(%rbp), %r10
	movq	%rax, %rbx
	jmp	.L457
.L524:
	movl	$31, %esi
	movq	%r10, -120(%rbp)
	leaq	-96(%rbp), %r14
	call	_ZN2v88internal8compiler21CommonOperatorBuilder13Int32ConstantEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	16(%r12), %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler22MachineOperatorBuilder9Word32AndEv@PLT
	movq	%rbx, %xmm0
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movhps	-104(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rcx
	movl	$2, %edx
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	8(%r12), %r15
	movq	24(%r12), %rdi
	movq	%rax, -112(%rbp)
	movq	-120(%rbp), %r10
	jmp	.L525
.L719:
	movq	24(%rax), %rsi
	jmp	.L355
.L715:
	movq	24(%rax), %rsi
	jmp	.L380
.L734:
	movq	32(%r13), %rdx
	movq	136(%r12), %rcx
	leaq	16(%rdx), %r14
	jmp	.L437
.L733:
	movq	32(%r13), %rdx
	movq	136(%r12), %rcx
	leaq	16(%rdx), %r14
	jmp	.L421
.L735:
	movq	32(%r13), %rdx
	movq	136(%r12), %rcx
	leaq	16(%rdx), %r14
	jmp	.L429
.L485:
	movq	32(%r13), %rdx
	movq	32(%rdx), %rbx
	leaq	32(%rdx), %rcx
	movl	20(%rbx), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %r15
	cmpq	%r15, %rbx
	jne	.L488
	jmp	.L487
.L722:
	call	_ZN2v88internal8compiler22MachineOperatorBuilder4LoadENS0_11MachineTypeE@PLT
	movq	%rax, %r15
	jmp	.L221
.L218:
	movq	16(%rax), %rcx
	addq	$24, %rax
	movq	%rcx, -120(%rbp)
	jmp	.L219
.L245:
	movq	32(%r13), %rax
	cmpl	$3, 8(%rax)
	jle	.L247
	movq	40(%rax), %rdx
	addq	$48, %rax
	jmp	.L249
.L239:
	movq	16(%rax), %rcx
	addq	$24, %rax
	movq	%rcx, -128(%rbp)
	jmp	.L240
.L739:
	call	_ZN2v88internal8compiler21StoreRepresentationOfEPKNS1_8OperatorE@PLT
	movq	16(%r12), %rdi
	movzbl	1(%rax), %edx
	movl	$4, %eax
	movb	%dl, %ah
	movl	%eax, %esi
	call	_ZN2v88internal8compiler22MachineOperatorBuilder5StoreENS1_19StoreRepresentationE@PLT
	movq	%rax, -104(%rbp)
	jmp	.L244
.L241:
	movq	32(%r13), %rax
	addq	$32, %rax
	jmp	.L242
.L222:
	movq	32(%r13), %rax
	cmpl	$2, 8(%rax)
	jle	.L224
	movq	32(%rax), %rdx
	addq	$40, %rax
	jmp	.L226
.L247:
	movl	20(%r15), %eax
	movq	-128(%rbp), %xmm0
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movq	8(%r12), %rdi
	movq	-104(%rbp), %rsi
	movl	$3, %edx
	andl	$16777215, %eax
	movhps	-120(%rbp), %xmm0
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	8(%rax), %rax
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	jmp	.L255
.L224:
	movq	-120(%rbp), %xmm0
	movq	%r14, %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movq	8(%r12), %rdi
	movl	$2, %edx
	movhps	-112(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%rax, %r14
	jmp	.L232
.L549:
	movl	%r15d, %ebx
	xorl	%r14d, %r14d
	jmp	.L356
.L547:
	movl	%r14d, %ebx
	jmp	.L331
.L740:
	movq	32(%r13), %rax
.L253:
	movq	24(%rax), %rdi
	movq	%rax, %rdx
	cmpq	%rdi, -112(%rbp)
	je	.L258
	leaq	24(%rax), %rax
	jmp	.L257
.L723:
	movq	32(%r13), %rax
.L230:
	movq	24(%rax), %rdi
	cmpq	%rdi, -104(%rbp)
	je	.L233
	leaq	24(%rax), %rbx
	jmp	.L234
.L550:
	movl	%r15d, %ebx
	xorl	%eax, %eax
	jmp	.L357
.L548:
	movl	%r14d, %ebx
	xorl	%eax, %eax
	jmp	.L332
.L543:
	movl	%edx, %eax
	xorl	%ecx, %ecx
	addq	%r8, %rsi
	jmp	.L266
.L544:
	movl	%ebx, %eax
	xorl	%edx, %edx
	addq	%rsi, %rcx
	jmp	.L286
.L742:
	movl	%edx, %eax
	andl	$16777215, %eax
	salq	$4, %rax
	movq	%r10, (%rsi,%rax)
	jmp	.L701
.L741:
	movq	32(%r13), %rax
.L261:
	movq	32(%rax), %rdi
	cmpq	%rdi, %r8
	je	.L262
	leaq	32(%rax), %rbx
	jmp	.L263
.L227:
	movq	32(%r13), %rdx
	movq	32(%rdx), %rdi
	movq	%rdx, %rax
	cmpq	%rdi, %r14
	je	.L230
	leaq	32(%rdx), %rax
	jmp	.L229
.L250:
	movq	32(%r13), %rdx
	movq	40(%rdx), %rdi
	movq	%rdx, %rax
	cmpq	%rdi, %r14
	je	.L253
	leaq	40(%rdx), %rax
	jmp	.L252
.L256:
	movl	20(%r15), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	136(%r12), %rax
	movq	(%rax), %r8
	jmp	.L537
.L546:
	movl	%edx, %esi
	xorl	%eax, %eax
	jmp	.L312
.L545:
	movl	%r15d, %r14d
	jmp	.L305
.L383:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler14NodeProperties23CollectValueProjectionsEPNS1_4NodeEPS4_m@PLT
	jmp	.L163
.L508:
	movq	32(%r13), %rsi
	movq	136(%r12), %rdx
	movq	32(%rsi), %r15
	movq	40(%rsi), %rbx
	movq	%rsi, %rcx
	movl	20(%r15), %eax
	andl	$16777215, %eax
	salq	$4, %rax
	addq	%rdx, %rax
	movq	(%rax), %r8
	cmpq	%r15, %r8
	je	.L511
	leaq	32(%rsi), %rcx
	jmp	.L512
.L258:
	movl	20(%r15), %edx
	andl	$16777215, %edx
	salq	$4, %rdx
	addq	136(%r12), %rdx
	movq	(%rdx), %r8
	jmp	.L261
.L738:
	movq	32(%r13), %rcx
.L513:
	movq	40(%rcx), %rdi
	movq	%rcx, %rax
	cmpq	%rdi, %r8
	je	.L514
	leaq	40(%rcx), %r14
	jmp	.L515
.L737:
	movq	8(%rax), %r8
	jmp	.L536
.L717:
	movq	%rdx, -120(%rbp)
	movq	%r10, -112(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L385
.L712:
	call	__stack_chk_fail@PLT
.L511:
	movq	8(%rax), %r8
	jmp	.L513
.L716:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE15547:
	.size	_ZN2v88internal8compiler13Int64Lowering9LowerNodeEPNS1_4NodeE, .-_ZN2v88internal8compiler13Int64Lowering9LowerNodeEPNS1_4NodeE
	.section	.text._ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb,"axG",@progbits,_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	.type	_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb, @function
_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb:
.LFB17527:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	88(%rdi), %rax
	movq	56(%rdi), %rsi
	movq	24(%rdi), %rbx
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	1(%r14,%rcx), %r8
	leaq	(%r8,%r8), %rcx
	cmpq	%rcx, %rbx
	jbe	.L744
	subq	%r8, %rbx
	shrq	%rbx
	salq	$3, %rbx
	testb	%dl, %dl
	leaq	(%rbx,%r14,8), %rcx
	cmovne	%rcx, %rbx
	addq	$8, %rax
	addq	16(%rdi), %rbx
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	%rbx, %rsi
	jbe	.L746
	cmpq	%rax, %rsi
	je	.L747
	movq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L744:
	cmpq	%r14, %rbx
	movq	%r14, %rax
	movq	(%rdi), %rdi
	cmovnb	%rbx, %rax
	movq	16(%rdi), %r15
	leaq	2(%rbx,%rax), %rcx
	movq	24(%rdi), %rax
	leaq	0(,%rcx,8), %rsi
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L754
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L749:
	movq	%rcx, %rax
	subq	%r8, %rax
	shrq	%rax
	salq	$3, %rax
	testb	%dl, %dl
	leaq	(%rax,%r14,8), %rsi
	cmovne	%rsi, %rax
	movq	56(%r12), %rsi
	leaq	(%r15,%rax), %rbx
	movq	88(%r12), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L751
	subq	%rsi, %rdx
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
.L751:
	movq	24(%r12), %rax
	leaq	0(,%rax,8), %rdx
	cmpq	$15, %rdx
	jbe	.L752
	movq	16(%r12), %rdx
	movq	%rax, 8(%rdx)
	movq	$0, (%rdx)
.L752:
	movq	%r15, 16(%r12)
	movq	%rcx, 24(%r12)
.L747:
	movq	%rbx, 56(%r12)
	movq	(%rbx), %rax
	movq	(%rbx), %xmm0
	addq	%r13, %rbx
	addq	$512, %rax
	movq	%rbx, 88(%r12)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%r12)
	movq	(%rbx), %rax
	movq	%rax, 72(%r12)
	addq	$512, %rax
	movq	%rax, 80(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L746:
	.cfi_restore_state
	cmpq	%rax, %rsi
	je	.L747
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%rbx, %rdi
	call	memmove@PLT
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L754:
	movl	%edx, -68(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	movl	-68(%rbp), %edx
	movq	%rax, %r15
	jmp	.L749
	.cfi_endproc
.LFE17527:
	.size	_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb, .-_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	.section	.rodata._ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_:
.LFB16937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rax
	cmpq	40(%rdi), %rax
	je	.L756
	movq	(%rsi), %rcx
	movl	8(%rsi), %edx
	movq	%rcx, -16(%rax)
	movl	%edx, -8(%rax)
	subq	$16, 32(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L756:
	.cfi_restore_state
	movq	56(%rdi), %r13
	movq	88(%rdi), %rdx
	subq	%r13, %rdx
	sarq	$3, %rdx
	subq	$1, %rdx
	movq	%rdx, %rcx
	movq	64(%rdi), %rdx
	subq	72(%rdi), %rdx
	salq	$5, %rcx
	sarq	$4, %rdx
	addq	%rcx, %rdx
	movq	48(%rdi), %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$134217727, %rax
	je	.L768
	cmpq	16(%rdi), %r13
	je	.L769
.L759:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L760
	cmpq	$31, 8(%rax)
	ja	.L770
.L760:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L771
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L761:
	movq	%rax, -8(%r13)
	movq	56(%rbx), %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 56(%rbx)
	movq	-8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 40(%rbx)
	movq	%rdx, 48(%rbx)
	leaq	496(%rax), %rdx
	movq	%rdx, 32(%rbx)
	movq	(%r12), %rcx
	movl	8(%r12), %edx
	movq	%rcx, 496(%rax)
	movl	%edx, 504(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L769:
	.cfi_restore_state
	movl	$1, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	movq	56(%rbx), %r13
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L770:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L771:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L761
.L768:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE16937:
	.size	_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_
	.section	.text._ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_:
.LFB16930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %rcx
	movq	64(%rdi), %rax
	leaq	-16(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L773
	movq	(%rsi), %rcx
	movl	8(%rsi), %edx
	movq	%rcx, (%rax)
	movl	%edx, 8(%rax)
	addq	$16, 64(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L773:
	.cfi_restore_state
	movq	88(%rdi), %r13
	subq	72(%rdi), %rax
	sarq	$4, %rax
	movq	%r13, %rdx
	subq	56(%rdi), %rdx
	sarq	$3, %rdx
	subq	$1, %rdx
	salq	$5, %rdx
	addq	%rax, %rdx
	movq	48(%rdi), %rax
	subq	32(%rdi), %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	cmpq	$134217727, %rax
	je	.L785
	movq	24(%rdi), %rsi
	movq	%r13, %rax
	subq	16(%rdi), %rax
	sarq	$3, %rax
	subq	%rax, %rsi
	cmpq	$1, %rsi
	jbe	.L786
.L776:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L777
	cmpq	$31, 8(%rax)
	ja	.L787
.L777:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$511, %rdx
	jbe	.L788
	leaq	512(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L778:
	movq	%rax, 8(%r13)
	movl	8(%r12), %edx
	movq	(%r12), %rcx
	movq	64(%rbx), %rax
	movl	%edx, 8(%rax)
	movq	%rcx, (%rax)
	movq	88(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 88(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 72(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rax, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L786:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE17_M_reallocate_mapEmb
	movq	88(%rbx), %r13
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L787:
	movq	(%rax), %rdx
	movq	%rdx, 8(%rbx)
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L788:
	movl	$512, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L778
.L785:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE16930:
	.size	_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.section	.text._ZN2v88internal8compiler13Int64Lowering10LowerGraphEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler13Int64Lowering10LowerGraphEv
	.type	_ZN2v88internal8compiler13Int64Lowering10LowerGraphEv, @function
_ZN2v88internal8compiler13Int64Lowering10LowerGraphEv:
.LFB15538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	cmpb	$4, 16(%rax)
	je	.L816
.L789:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L817
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L816:
	.cfi_restore_state
	movq	8(%rdi), %rax
	leaq	40(%rdi), %r12
	leaq	-64(%rbp), %r13
	movq	%rdi, %rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	16(%rax), %rax
	movl	$0, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movq	8(%rbx), %rax
	movq	16(%rax), %rdx
	movl	32(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 16(%rdx)
	movq	104(%rbx), %rcx
	cmpq	%rcx, 72(%rbx)
	jne	.L791
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L795:
	leal	1(%rdx), %eax
	leaq	32(%rdi), %rcx
	salq	$3, %rdx
	movl	%eax, -8(%rsi)
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L801
	addq	%rcx, %rdx
.L802:
	movq	(%rdx), %r14
	movl	32(%rbx), %eax
	movl	16(%r14), %edx
	cmpl	%edx, %eax
	ja	.L803
	cmpb	%al, %dl
	je	.L803
.L800:
	movq	104(%rbx), %rcx
	cmpq	72(%rbx), %rcx
	je	.L789
.L791:
	movq	112(%rbx), %r8
	movq	128(%rbx), %r9
	movq	%rcx, %rsi
	cmpq	%rcx, %r8
	je	.L818
.L792:
	movq	-16(%rsi), %rdi
	movslq	-8(%rsi), %rdx
	movzbl	23(%rdi), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	jne	.L794
	movq	32(%rdi), %rax
	movl	8(%rax), %eax
.L794:
	cmpl	%eax, %edx
	jne	.L795
	cmpq	%rcx, %r8
	je	.L796
	subq	$16, %rcx
	movq	%rcx, 104(%rbx)
.L797:
	movl	32(%rbx), %eax
	movq	-16(%rsi), %rdx
	movq	%rbx, %rdi
	addl	$2, %eax
	movl	%eax, 16(%rdx)
	movq	-16(%rsi), %rsi
	call	_ZN2v88internal8compiler13Int64Lowering9LowerNodeEPNS1_4NodeE
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L803:
	movq	(%r14), %rax
	movzwl	16(%rax), %eax
	cmpl	$35, %eax
	je	.L819
	movq	%r14, -64(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$0, -56(%rbp)
	cmpl	$1, %eax
	je	.L810
	cmpl	$36, %eax
	je	.L810
	call	_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
.L805:
	movl	32(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 16(%r14)
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L818:
	movq	-8(%r9), %rax
	leaq	512(%rax), %rsi
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L801:
	movq	32(%rdi), %rax
	leaq	16(%rax,%rdx), %rdx
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L810:
	call	_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L819:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13Int64Lowering21PreparePhiReplacementEPNS1_4NodeE
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, -64(%rbp)
	movl	$0, -56(%rbp)
	call	_ZNSt5dequeIN2v88internal8compiler13Int64Lowering9NodeStateENS1_22RecyclingZoneAllocatorIS4_EEE13emplace_frontIJS4_EEEvDpOT_
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L796:
	movq	48(%rbx), %rax
	testq	%rax, %rax
	je	.L798
	cmpq	$32, 8(%rax)
	ja	.L799
.L798:
	movq	$32, 8(%rcx)
	movq	48(%rbx), %rax
	movq	%rax, (%rcx)
	movq	128(%rbx), %r9
	movq	%rcx, 48(%rbx)
.L799:
	leaq	-8(%r9), %rax
	movq	%rax, 128(%rbx)
	movq	-8(%r9), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 112(%rbx)
	addq	$496, %rax
	movq	%rdx, 120(%rbx)
	movq	%rax, 104(%rbx)
	jmp	.L797
.L817:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15538:
	.size	_ZN2v88internal8compiler13Int64Lowering10LowerGraphEv, .-_ZN2v88internal8compiler13Int64Lowering10LowerGraphEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler13Int64LoweringC2EPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderEPNS0_4ZoneEPNS0_9SignatureINS0_21MachineRepresentationEEESt10unique_ptrINS1_24Int64LoweringSpecialCaseESt14default_deleteISG_EE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler13Int64LoweringC2EPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderEPNS0_4ZoneEPNS0_9SignatureINS0_21MachineRepresentationEEESt10unique_ptrINS1_24Int64LoweringSpecialCaseESt14default_deleteISG_EE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler13Int64LoweringC2EPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderEPNS0_4ZoneEPNS0_9SignatureINS0_21MachineRepresentationEEESt10unique_ptrINS1_24Int64LoweringSpecialCaseESt14default_deleteISG_EE:
.LFB18449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE18449:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler13Int64LoweringC2EPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderEPNS0_4ZoneEPNS0_9SignatureINS0_21MachineRepresentationEEESt10unique_ptrINS1_24Int64LoweringSpecialCaseESt14default_deleteISG_EE, .-_GLOBAL__sub_I__ZN2v88internal8compiler13Int64LoweringC2EPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderEPNS0_4ZoneEPNS0_9SignatureINS0_21MachineRepresentationEEESt10unique_ptrINS1_24Int64LoweringSpecialCaseESt14default_deleteISG_EE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler13Int64LoweringC2EPNS1_5GraphEPNS1_22MachineOperatorBuilderEPNS1_21CommonOperatorBuilderEPNS0_4ZoneEPNS0_9SignatureINS0_21MachineRepresentationEEESt10unique_ptrINS1_24Int64LoweringSpecialCaseESt14default_deleteISG_EE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.align 16
.LC2:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.align 16
.LC3:
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
