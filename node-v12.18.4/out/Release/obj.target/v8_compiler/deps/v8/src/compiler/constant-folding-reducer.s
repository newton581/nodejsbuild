	.file	"constant-folding-reducer.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler22ConstantFoldingReducer12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ConstantFoldingReducer"
	.section	.text._ZNK2v88internal8compiler22ConstantFoldingReducer12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler22ConstantFoldingReducer12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler22ConstantFoldingReducer12reducer_nameEv
	.type	_ZNK2v88internal8compiler22ConstantFoldingReducer12reducer_nameEv, @function
_ZNK2v88internal8compiler22ConstantFoldingReducer12reducer_nameEv:
.LFB10163:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10163:
	.size	_ZNK2v88internal8compiler22ConstantFoldingReducer12reducer_nameEv, .-_ZNK2v88internal8compiler22ConstantFoldingReducer12reducer_nameEv
	.section	.text._ZN2v88internal8compiler22ConstantFoldingReducerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22ConstantFoldingReducerD2Ev
	.type	_ZN2v88internal8compiler22ConstantFoldingReducerD2Ev, @function
_ZN2v88internal8compiler22ConstantFoldingReducerD2Ev:
.LFB19117:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE19117:
	.size	_ZN2v88internal8compiler22ConstantFoldingReducerD2Ev, .-_ZN2v88internal8compiler22ConstantFoldingReducerD2Ev
	.globl	_ZN2v88internal8compiler22ConstantFoldingReducerD1Ev
	.set	_ZN2v88internal8compiler22ConstantFoldingReducerD1Ev,_ZN2v88internal8compiler22ConstantFoldingReducerD2Ev
	.section	.text._ZN2v88internal8compiler22ConstantFoldingReducerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22ConstantFoldingReducerD0Ev
	.type	_ZN2v88internal8compiler22ConstantFoldingReducerD0Ev, @function
_ZN2v88internal8compiler22ConstantFoldingReducerD0Ev:
.LFB19119:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE19119:
	.size	_ZN2v88internal8compiler22ConstantFoldingReducerD0Ev, .-_ZN2v88internal8compiler22ConstantFoldingReducerD0Ev
	.section	.text._ZN2v88internal8compiler22ConstantFoldingReducer6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22ConstantFoldingReducer6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler22ConstantFoldingReducer6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler22ConstantFoldingReducer6ReduceEPNS1_4NodeE:
.LFB19120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movzwl	16(%rax), %edx
	leal	-23(%rdx), %ecx
	cmpl	$10, %ecx
	jbe	.L6
	movq	8(%rsi), %rcx
	movq	%rsi, %r12
	testq	%rcx, %rcx
	jne	.L42
.L6:
	xorl	%eax, %eax
.L25:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L43
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movzbl	18(%rax), %eax
	andl	$112, %eax
	cmpb	$112, %al
	jne	.L6
	cmpl	$40, %edx
	je	.L6
	movq	%rcx, -72(%rbp)
	cmpq	$1, %rcx
	je	.L6
	movq	%rdi, %rbx
	testb	$1, %cl
	jne	.L10
	movl	(%rcx), %eax
	testl	%eax, %eax
	je	.L44
.L10:
	cmpq	$2049, %rcx
	je	.L12
	leaq	-72(%rbp), %r13
	movl	$2049, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L12
	cmpq	$4097, -72(%rbp)
	je	.L13
	movl	$4097, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L13
	cmpq	$129, -72(%rbp)
	je	.L15
	movl	$129, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L15
	cmpq	$1119, -72(%rbp)
	je	.L21
	movl	$1119, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	jne	.L21
.L20:
	cmpq	$257, -72(%rbp)
	je	.L23
	movl	$257, %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type6SlowIsES2_@PLT
	testb	%al, %al
	je	.L6
.L23:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph17UndefinedConstantEv@PLT
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L11:
	testq	%r13, %r13
	je	.L6
	cmpq	$0, 8(%r13)
	jne	.L24
	movq	-72(%rbp), %rax
	movq	%rax, 8(%r13)
.L24:
	movq	8(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r13, %rax
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L12:
	movq	16(%rbx), %rax
	leaq	-64(%rbp), %r13
	movq	24(%rbx), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	360(%rax), %rdx
	addq	$1112, %rdx
	call	_ZN2v88internal8compiler9ObjectRefC1EPNS1_12JSHeapBrokerENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%rax, %r13
	jmp	.L11
.L44:
	movq	16(%rdi), %r13
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal8compiler4Type14AsHeapConstantEv@PLT
	leaq	8(%rax), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantERKNS1_9ObjectRefE@PLT
	movq	%rax, %r13
	jmp	.L11
.L13:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph11NaNConstantEv@PLT
	movq	%rax, %r13
	jmp	.L11
.L15:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal8compiler7JSGraph12NullConstantEv@PLT
	movq	%rax, %r13
	jmp	.L11
.L43:
	call	__stack_chk_fail@PLT
.L21:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r13, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNK2v88internal8compiler4Type3MaxEv@PLT
	movsd	-88(%rbp), %xmm1
	ucomisd	%xmm0, %xmm1
	jp	.L20
	jne	.L20
	movq	16(%rbx), %r14
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler4Type3MinEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler7JSGraph8ConstantEd@PLT
	movq	%rax, %r13
	jmp	.L11
	.cfi_endproc
.LFE19120:
	.size	_ZN2v88internal8compiler22ConstantFoldingReducer6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler22ConstantFoldingReducer6ReduceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler22ConstantFoldingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler22ConstantFoldingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.type	_ZN2v88internal8compiler22ConstantFoldingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, @function
_ZN2v88internal8compiler22ConstantFoldingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE:
.LFB19114:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler22ConstantFoldingReducerE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rdx, 16(%rdi)
	movq	%rcx, 24(%rdi)
	ret
	.cfi_endproc
.LFE19114:
	.size	_ZN2v88internal8compiler22ConstantFoldingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, .-_ZN2v88internal8compiler22ConstantFoldingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.globl	_ZN2v88internal8compiler22ConstantFoldingReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.set	_ZN2v88internal8compiler22ConstantFoldingReducerC1EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE,_ZN2v88internal8compiler22ConstantFoldingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler22ConstantFoldingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler22ConstantFoldingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler22ConstantFoldingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE:
.LFB22937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22937:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler22ConstantFoldingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE, .-_GLOBAL__sub_I__ZN2v88internal8compiler22ConstantFoldingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler22ConstantFoldingReducerC2EPNS1_15AdvancedReducer6EditorEPNS1_7JSGraphEPNS1_12JSHeapBrokerE
	.weak	_ZTVN2v88internal8compiler22ConstantFoldingReducerE
	.section	.data.rel.ro._ZTVN2v88internal8compiler22ConstantFoldingReducerE,"awG",@progbits,_ZTVN2v88internal8compiler22ConstantFoldingReducerE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler22ConstantFoldingReducerE, @object
	.size	_ZTVN2v88internal8compiler22ConstantFoldingReducerE, 56
_ZTVN2v88internal8compiler22ConstantFoldingReducerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler22ConstantFoldingReducerD1Ev
	.quad	_ZN2v88internal8compiler22ConstantFoldingReducerD0Ev
	.quad	_ZNK2v88internal8compiler22ConstantFoldingReducer12reducer_nameEv
	.quad	_ZN2v88internal8compiler22ConstantFoldingReducer6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
