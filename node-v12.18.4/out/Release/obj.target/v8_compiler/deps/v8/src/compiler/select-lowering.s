	.file	"select-lowering.cc"
	.text
	.section	.rodata._ZNK2v88internal8compiler14SelectLowering12reducer_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"SelectLowering"
	.section	.text._ZNK2v88internal8compiler14SelectLowering12reducer_nameEv,"axG",@progbits,_ZNK2v88internal8compiler14SelectLowering12reducer_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal8compiler14SelectLowering12reducer_nameEv
	.type	_ZNK2v88internal8compiler14SelectLowering12reducer_nameEv, @function
_ZNK2v88internal8compiler14SelectLowering12reducer_nameEv:
.LFB10163:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE10163:
	.size	_ZNK2v88internal8compiler14SelectLowering12reducer_nameEv, .-_ZNK2v88internal8compiler14SelectLowering12reducer_nameEv
	.section	.text._ZN2v88internal8compiler14SelectLoweringD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14SelectLoweringD2Ev
	.type	_ZN2v88internal8compiler14SelectLoweringD2Ev, @function
_ZN2v88internal8compiler14SelectLoweringD2Ev:
.LFB10499:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10499:
	.size	_ZN2v88internal8compiler14SelectLoweringD2Ev, .-_ZN2v88internal8compiler14SelectLoweringD2Ev
	.globl	_ZN2v88internal8compiler14SelectLoweringD1Ev
	.set	_ZN2v88internal8compiler14SelectLoweringD1Ev,_ZN2v88internal8compiler14SelectLoweringD2Ev
	.section	.text._ZN2v88internal8compiler14SelectLoweringD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14SelectLoweringD0Ev
	.type	_ZN2v88internal8compiler14SelectLoweringD0Ev, @function
_ZN2v88internal8compiler14SelectLoweringD0Ev:
.LFB10501:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10501:
	.size	_ZN2v88internal8compiler14SelectLoweringD0Ev, .-_ZN2v88internal8compiler14SelectLoweringD0Ev
	.section	.text._ZN2v88internal8compiler14SelectLowering6ReduceEPNS1_4NodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14SelectLowering6ReduceEPNS1_4NodeE
	.type	_ZN2v88internal8compiler14SelectLowering6ReduceEPNS1_4NodeE, @function
_ZN2v88internal8compiler14SelectLowering6ReduceEPNS1_4NodeE:
.LFB10502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpw	$34, 16(%rdi)
	je	.L46
.L7:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L47
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	%rsi, %rbx
	call	_ZN2v88internal8compiler18SelectParametersOfEPKNS1_8OperatorE@PLT
	leaq	32(%rbx), %rdx
	movzbl	(%rax), %ecx
	movzbl	1(%rax), %esi
	movq	%rdx, -96(%rbp)
	movzbl	23(%rbx), %eax
	movb	%cl, -105(%rbp)
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L8
	movq	32(%rbx), %r15
	movq	40(%rbx), %r11
	leaq	48(%rbx), %rax
.L9:
	movq	16(%r12), %r14
	movq	(%rax), %rax
	movl	$1, %edx
	movq	%r11, -128(%rbp)
	movq	8(%r12), %r13
	movq	%rax, -104(%rbp)
	movq	8(%r14), %rax
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6BranchENS1_10BranchHintENS1_13IsSafetyCheckE@PLT
	movq	%r15, %xmm0
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	leaq	-80(%rbp), %r15
	movhps	-88(%rbp), %xmm0
	movq	%rax, %rsi
	movl	$2, %edx
	movq	%r15, %rcx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder6IfTrueEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-88(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	%r13, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder7IfFalseEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	-88(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler21CommonOperatorBuilder5MergeEi@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$2, %edx
	movq	-120(%rbp), %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movhps	-88(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler5Graph7NewNodeEPKNS1_8OperatorEiPKPNS1_4NodeEb@PLT
	movq	-128(%rbp), %r11
	movq	%rax, %r8
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L10
	movq	32(%rbx), %rdi
	movq	-96(%rbp), %rax
	movq	%rbx, %rsi
	cmpq	%r11, %rdi
	je	.L12
.L11:
	leaq	-24(%rsi), %r14
	testq	%rdi, %rdi
	je	.L14
	movq	%r14, %rsi
	movq	%r11, -128(%rbp)
	movq	%r8, -120(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-128(%rbp), %r11
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rax
.L14:
	movq	%r11, (%rax)
	testq	%r11, %r11
	je	.L15
	movq	%r14, %rsi
	movq	%r11, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %r8
.L15:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L48
.L12:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rdi
	cmpq	%rdi, -104(%rbp)
	je	.L16
	addq	$8, %rax
	movq	%rbx, %rsi
.L17:
	leaq	-48(%rsi), %r14
	testq	%rdi, %rdi
	je	.L19
	movq	%r14, %rsi
	movq	%r8, -120(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rax
.L19:
	movq	-104(%rbp), %rdi
	movq	%rdi, (%rax)
	testq	%rdi, %rdi
	je	.L20
	movq	%r14, %rsi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %r8
.L20:
	movzbl	23(%rbx), %eax
	andl	$15, %eax
	cmpl	$15, %eax
	je	.L49
.L16:
	movq	-96(%rbp), %rax
	movq	16(%rax), %rdi
	cmpq	%r8, %rdi
	je	.L23
	addq	$16, %rax
	movq	%rbx, %rsi
	movq	%rax, %r13
.L22:
	leaq	-72(%rsi), %r14
	testq	%rdi, %rdi
	je	.L24
	movq	%r14, %rsi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal8compiler4Node9RemoveUseEPNS2_3UseE@PLT
	movq	-88(%rbp), %r8
.L24:
	movq	%r8, 0(%r13)
	testq	%r8, %r8
	je	.L23
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler4Node9AppendUseEPNS2_3UseE@PLT
.L23:
	movq	8(%r12), %rdi
	movzbl	-105(%rbp), %esi
	movl	$2, %edx
	call	_ZN2v88internal8compiler21CommonOperatorBuilder3PhiENS0_21MachineRepresentationEi@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler14NodeProperties8ChangeOpEPNS1_4NodeEPKNS1_8OperatorE@PLT
	movq	%rbx, %rax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	movq	32(%rbx), %rax
	movq	16(%rax), %r15
	movq	24(%rax), %r11
	addq	$32, %rax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	movq	32(%rbx), %rsi
	movq	16(%rsi), %rdi
	leaq	16(%rsi), %rax
	cmpq	%r11, %rdi
	jne	.L11
.L13:
	movq	8(%rax), %rdi
	cmpq	-104(%rbp), %rdi
	je	.L18
	addq	$8, %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L49:
	movq	32(%rbx), %rsi
	leaq	16(%rsi), %rax
.L18:
	movq	16(%rax), %rdi
	cmpq	%r8, %rdi
	je	.L23
	leaq	16(%rax), %r13
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L48:
	movq	32(%rbx), %rsi
	leaq	16(%rsi), %rax
	jmp	.L13
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10502:
	.size	_ZN2v88internal8compiler14SelectLowering6ReduceEPNS1_4NodeE, .-_ZN2v88internal8compiler14SelectLowering6ReduceEPNS1_4NodeE
	.section	.text._ZN2v88internal8compiler14SelectLoweringC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8compiler14SelectLoweringC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderE
	.type	_ZN2v88internal8compiler14SelectLoweringC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderE, @function
_ZN2v88internal8compiler14SelectLoweringC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderE:
.LFB10496:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal8compiler14SelectLoweringE(%rip), %rax
	movq	%rdx, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE10496:
	.size	_ZN2v88internal8compiler14SelectLoweringC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderE, .-_ZN2v88internal8compiler14SelectLoweringC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderE
	.globl	_ZN2v88internal8compiler14SelectLoweringC1EPNS1_5GraphEPNS1_21CommonOperatorBuilderE
	.set	_ZN2v88internal8compiler14SelectLoweringC1EPNS1_5GraphEPNS1_21CommonOperatorBuilderE,_ZN2v88internal8compiler14SelectLoweringC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8compiler14SelectLoweringC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8compiler14SelectLoweringC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderE, @function
_GLOBAL__sub_I__ZN2v88internal8compiler14SelectLoweringC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderE:
.LFB12140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12140:
	.size	_GLOBAL__sub_I__ZN2v88internal8compiler14SelectLoweringC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderE, .-_GLOBAL__sub_I__ZN2v88internal8compiler14SelectLoweringC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8compiler14SelectLoweringC2EPNS1_5GraphEPNS1_21CommonOperatorBuilderE
	.weak	_ZTVN2v88internal8compiler14SelectLoweringE
	.section	.data.rel.ro._ZTVN2v88internal8compiler14SelectLoweringE,"awG",@progbits,_ZTVN2v88internal8compiler14SelectLoweringE,comdat
	.align 8
	.type	_ZTVN2v88internal8compiler14SelectLoweringE, @object
	.size	_ZTVN2v88internal8compiler14SelectLoweringE, 56
_ZTVN2v88internal8compiler14SelectLoweringE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8compiler14SelectLoweringD1Ev
	.quad	_ZN2v88internal8compiler14SelectLoweringD0Ev
	.quad	_ZNK2v88internal8compiler14SelectLowering12reducer_nameEv
	.quad	_ZN2v88internal8compiler14SelectLowering6ReduceEPNS1_4NodeE
	.quad	_ZN2v88internal8compiler7Reducer8FinalizeEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
