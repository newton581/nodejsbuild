	.file	"Protocol.cpp"
	.text
	.section	.text._ZN12v8_inspector8protocol5ValueD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol5ValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol5ValueD2Ev
	.type	_ZN12v8_inspector8protocol5ValueD2Ev, @function
_ZN12v8_inspector8protocol5ValueD2Ev:
.LFB4281:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4281:
	.size	_ZN12v8_inspector8protocol5ValueD2Ev, .-_ZN12v8_inspector8protocol5ValueD2Ev
	.weak	_ZN12v8_inspector8protocol5ValueD1Ev
	.set	_ZN12v8_inspector8protocol5ValueD1Ev,_ZN12v8_inspector8protocol5ValueD2Ev
	.section	.text._ZNK12v8_inspector8protocol5Value9asBooleanEPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol5Value9asBooleanEPb
	.type	_ZNK12v8_inspector8protocol5Value9asBooleanEPb, @function
_ZNK12v8_inspector8protocol5Value9asBooleanEPb:
.LFB4949:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4949:
	.size	_ZNK12v8_inspector8protocol5Value9asBooleanEPb, .-_ZNK12v8_inspector8protocol5Value9asBooleanEPb
	.globl	_ZNK12v8_inspector8protocol5Value8asBinaryEPNS0_6BinaryE
	.set	_ZNK12v8_inspector8protocol5Value8asBinaryEPNS0_6BinaryE,_ZNK12v8_inspector8protocol5Value9asBooleanEPb
	.globl	_ZNK12v8_inspector8protocol5Value8asStringEPNS_8String16E
	.set	_ZNK12v8_inspector8protocol5Value8asStringEPNS_8String16E,_ZNK12v8_inspector8protocol5Value9asBooleanEPb
	.globl	_ZNK12v8_inspector8protocol5Value9asIntegerEPi
	.set	_ZNK12v8_inspector8protocol5Value9asIntegerEPi,_ZNK12v8_inspector8protocol5Value9asBooleanEPb
	.globl	_ZNK12v8_inspector8protocol5Value8asDoubleEPd
	.set	_ZNK12v8_inspector8protocol5Value8asDoubleEPd,_ZNK12v8_inspector8protocol5Value9asBooleanEPb
	.section	.text._ZN12v8_inspector8protocol5Value17serializeToBinaryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol5Value17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol5Value17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol5Value17serializeToBinaryEv:
.LFB4959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	subq	$8, %rsp
	movups	%xmm0, (%r12)
	movq	$0, 16(%r12)
	movq	(%rsi), %rax
	movq	%r12, %rsi
	call	*80(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4959:
	.size	_ZN12v8_inspector8protocol5Value17serializeToBinaryEv, .-_ZN12v8_inspector8protocol5Value17serializeToBinaryEv
	.section	.text._ZNK12v8_inspector8protocol16FundamentalValue9asBooleanEPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol16FundamentalValue9asBooleanEPb
	.type	_ZNK12v8_inspector8protocol16FundamentalValue9asBooleanEPb, @function
_ZNK12v8_inspector8protocol16FundamentalValue9asBooleanEPb:
.LFB4960:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$1, 8(%rdi)
	jne	.L6
	movzbl	16(%rdi), %eax
	movb	%al, (%rsi)
	movl	$1, %eax
.L6:
	ret
	.cfi_endproc
.LFE4960:
	.size	_ZNK12v8_inspector8protocol16FundamentalValue9asBooleanEPb, .-_ZNK12v8_inspector8protocol16FundamentalValue9asBooleanEPb
	.section	.text._ZNK12v8_inspector8protocol16FundamentalValue8asDoubleEPd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol16FundamentalValue8asDoubleEPd
	.type	_ZNK12v8_inspector8protocol16FundamentalValue8asDoubleEPd, @function
_ZNK12v8_inspector8protocol16FundamentalValue8asDoubleEPd:
.LFB4961:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	cmpl	$3, %eax
	je	.L13
	xorl	%r8d, %r8d
	cmpl	$2, %eax
	je	.L14
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movsd	16(%rdi), %xmm0
	movl	$1, %r8d
	movl	%r8d, %eax
	movsd	%xmm0, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	pxor	%xmm0, %xmm0
	movl	$1, %r8d
	cvtsi2sdl	16(%rdi), %xmm0
	movl	%r8d, %eax
	movsd	%xmm0, (%rsi)
	ret
	.cfi_endproc
.LFE4961:
	.size	_ZNK12v8_inspector8protocol16FundamentalValue8asDoubleEPd, .-_ZNK12v8_inspector8protocol16FundamentalValue8asDoubleEPd
	.section	.text._ZNK12v8_inspector8protocol16FundamentalValue9asIntegerEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol16FundamentalValue9asIntegerEPi
	.type	_ZNK12v8_inspector8protocol16FundamentalValue9asIntegerEPi, @function
_ZNK12v8_inspector8protocol16FundamentalValue9asIntegerEPi:
.LFB4962:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$2, 8(%rdi)
	jne	.L15
	movl	16(%rdi), %eax
	movl	%eax, (%rsi)
	movl	$1, %eax
.L15:
	ret
	.cfi_endproc
.LFE4962:
	.size	_ZNK12v8_inspector8protocol16FundamentalValue9asIntegerEPi, .-_ZNK12v8_inspector8protocol16FundamentalValue9asIntegerEPi
	.section	.text._ZNK12v8_inspector8protocol11BinaryValue8asBinaryEPNS0_6BinaryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol11BinaryValue8asBinaryEPNS0_6BinaryE
	.type	_ZNK12v8_inspector8protocol11BinaryValue8asBinaryEPNS0_6BinaryE, @function
_ZNK12v8_inspector8protocol11BinaryValue8asBinaryEPNS0_6BinaryE:
.LFB4971:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4971:
	.size	_ZNK12v8_inspector8protocol11BinaryValue8asBinaryEPNS0_6BinaryE, .-_ZNK12v8_inspector8protocol11BinaryValue8asBinaryEPNS0_6BinaryE
	.section	.text._ZN12v8_inspector8protocol16FundamentalValueD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol16FundamentalValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16FundamentalValueD2Ev
	.type	_ZN12v8_inspector8protocol16FundamentalValueD2Ev, @function
_ZN12v8_inspector8protocol16FundamentalValueD2Ev:
.LFB6417:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6417:
	.size	_ZN12v8_inspector8protocol16FundamentalValueD2Ev, .-_ZN12v8_inspector8protocol16FundamentalValueD2Ev
	.weak	_ZN12v8_inspector8protocol16FundamentalValueD1Ev
	.set	_ZN12v8_inspector8protocol16FundamentalValueD1Ev,_ZN12v8_inspector8protocol16FundamentalValueD2Ev
	.section	.text._ZN12v8_inspector8protocol11BinaryValueD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol11BinaryValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol11BinaryValueD2Ev
	.type	_ZN12v8_inspector8protocol11BinaryValueD2Ev, @function
_ZN12v8_inspector8protocol11BinaryValueD2Ev:
.LFB6454:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6454:
	.size	_ZN12v8_inspector8protocol11BinaryValueD2Ev, .-_ZN12v8_inspector8protocol11BinaryValueD2Ev
	.weak	_ZN12v8_inspector8protocol11BinaryValueD1Ev
	.set	_ZN12v8_inspector8protocol11BinaryValueD1Ev,_ZN12v8_inspector8protocol11BinaryValueD2Ev
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsItEEvPKT_S5_PS5_,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsItEEvPKT_S5_PS5_, @function
_ZN12v8_inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsItEEvPKT_S5_PS5_:
.LFB7665:
	.cfi_startproc
	cmpq	%rsi, %rdi
	jnb	.L22
	.p2align 4,,10
	.p2align 3
.L23:
	movzwl	(%rdi), %eax
	testl	$65408, %eax
	jne	.L24
	cmpw	$32, %ax
	jbe	.L45
.L24:
	cmpw	$47, %ax
	jne	.L22
	cmpq	%rdi, %rsi
	je	.L22
	leaq	2(%rdi), %rax
	cmpq	%rax, %rsi
	jbe	.L22
	movzwl	2(%rdi), %eax
	cmpw	$47, %ax
	je	.L46
	cmpw	$42, %ax
	je	.L47
.L22:
	movq	%rdi, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	leal	-9(%rax), %ecx
	cmpw	$4, %cx
	jbe	.L34
	cmpw	$32, %ax
	jne	.L22
.L34:
	addq	$2, %rdi
.L26:
	cmpq	%rdi, %rsi
	ja	.L23
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L46:
	addq	$4, %rdi
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L48:
	movzwl	(%rdi), %eax
	addq	$2, %rdi
	cmpw	$10, %ax
	je	.L26
	cmpw	$13, %ax
	je	.L26
.L44:
	cmpq	%rdi, %rsi
	ja	.L48
	movq	%rsi, %rdi
	movq	%rdi, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	4(%rdi), %rcx
	cmpq	%rcx, %rsi
	ja	.L29
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L50:
	cmpw	$47, (%rax)
	je	.L49
.L30:
	movq	%rax, %rcx
.L29:
	leaq	2(%rcx), %rax
	movzwl	-2(%rax), %r8d
	cmpq	%rax, %rsi
	jbe	.L22
	cmpw	$42, %r8w
	jne	.L30
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	4(%rcx), %rdi
	jmp	.L26
	.cfi_endproc
.LFE7665:
	.size	_ZN12v8_inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsItEEvPKT_S5_PS5_, .-_ZN12v8_inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsItEEvPKT_S5_PS5_
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsIhEEvPKT_S5_PS5_,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsIhEEvPKT_S5_PS5_, @function
_ZN12v8_inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsIhEEvPKT_S5_PS5_:
.LFB7668:
	.cfi_startproc
	cmpq	%rsi, %rdi
	jnb	.L52
	.p2align 4,,10
	.p2align 3
.L53:
	movzbl	(%rdi), %ecx
	movl	%ecx, %eax
	testb	$-128, %al
	jne	.L54
	cmpw	$32, %cx
	jbe	.L74
.L54:
	cmpb	$47, %al
	jne	.L52
	cmpq	%rdi, %rsi
	je	.L52
	leaq	1(%rdi), %rax
	cmpq	%rax, %rsi
	jbe	.L52
	movzbl	1(%rdi), %eax
	cmpb	$47, %al
	je	.L75
	cmpb	$42, %al
	je	.L76
.L52:
	movq	%rdi, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	leal	-9(%rcx), %eax
	cmpw	$4, %ax
	jbe	.L64
	cmpw	$32, %cx
	jne	.L52
.L64:
	addq	$1, %rdi
.L56:
	cmpq	%rdi, %rsi
	ja	.L53
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L75:
	addq	$2, %rdi
	cmpq	%rdi, %rsi
	ja	.L58
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L77:
	cmpb	$13, %al
	je	.L56
	cmpq	%rdi, %rsi
	je	.L52
.L58:
	movzbl	(%rdi), %eax
	addq	$1, %rdi
	cmpb	$10, %al
	jne	.L77
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	2(%rdi), %rcx
	cmpq	%rcx, %rsi
	ja	.L59
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L79:
	cmpb	$47, (%rax)
	je	.L78
.L60:
	movq	%rax, %rcx
.L59:
	leaq	1(%rcx), %rax
	movzbl	-1(%rax), %r8d
	cmpq	%rax, %rsi
	je	.L52
	cmpb	$42, %r8b
	jne	.L60
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	2(%rcx), %rdi
	jmp	.L56
.L62:
	movq	%rsi, %rdi
	jmp	.L52
	.cfi_endproc
.LFE7668:
	.size	_ZN12v8_inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsIhEEvPKT_S5_PS5_, .-_ZN12v8_inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsIhEEvPKT_S5_PS5_
	.section	.text._ZN12v8_inspector8protocol11StringValueD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol11StringValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol11StringValueD2Ev
	.type	_ZN12v8_inspector8protocol11StringValueD2Ev, @function
_ZN12v8_inspector8protocol11StringValueD2Ev:
.LFB6434:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r8
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	addq	$32, %rdi
	movq	%rax, -32(%rdi)
	cmpq	%rdi, %r8
	je	.L80
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L80:
	ret
	.cfi_endproc
.LFE6434:
	.size	_ZN12v8_inspector8protocol11StringValueD2Ev, .-_ZN12v8_inspector8protocol11StringValueD2Ev
	.weak	_ZN12v8_inspector8protocol11StringValueD1Ev
	.set	_ZN12v8_inspector8protocol11StringValueD1Ev,_ZN12v8_inspector8protocol11StringValueD2Ev
	.section	.text._ZN12v8_inspector8protocol15SerializedValueD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol15SerializedValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol15SerializedValueD2Ev
	.type	_ZN12v8_inspector8protocol15SerializedValueD2Ev, @function
_ZN12v8_inspector8protocol15SerializedValueD2Ev:
.LFB6471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol15SerializedValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L83
	call	_ZdlPv@PLT
.L83:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L82
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6471:
	.size	_ZN12v8_inspector8protocol15SerializedValueD2Ev, .-_ZN12v8_inspector8protocol15SerializedValueD2Ev
	.weak	_ZN12v8_inspector8protocol15SerializedValueD1Ev
	.set	_ZN12v8_inspector8protocol15SerializedValueD1Ev,_ZN12v8_inspector8protocol15SerializedValueD2Ev
	.section	.text._ZN12v8_inspector8protocol16InternalResponseD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16InternalResponseD2Ev
	.type	_ZN12v8_inspector8protocol16InternalResponseD2Ev, @function
_ZN12v8_inspector8protocol16InternalResponseD2Ev:
.LFB4410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L90
	movq	(%rdi), %rax
	call	*24(%rax)
.L90:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L89
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4410:
	.size	_ZN12v8_inspector8protocol16InternalResponseD2Ev, .-_ZN12v8_inspector8protocol16InternalResponseD2Ev
	.weak	_ZN12v8_inspector8protocol16InternalResponseD1Ev
	.set	_ZN12v8_inspector8protocol16InternalResponseD1Ev,_ZN12v8_inspector8protocol16InternalResponseD2Ev
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorD2Ev, @function
_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorD2Ev:
.LFB5150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L97
	call	_ZdlPv@PLT
.L97:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L96
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5150:
	.size	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorD2Ev, .-_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorD2Ev
	.set	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorD1Ev,_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorD2Ev
	.section	.text._ZNK12v8_inspector8protocol5Value5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol5Value5cloneEv
	.type	_ZNK12v8_inspector8protocol5Value5cloneEv, @function
_ZNK12v8_inspector8protocol5Value5cloneEv:
.LFB4956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$16, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol5ValueE(%rip), %rdx
	movq	%rdx, (%rax)
	movl	$0, 8(%rax)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4956:
	.size	_ZNK12v8_inspector8protocol5Value5cloneEv, .-_ZNK12v8_inspector8protocol5Value5cloneEv
	.section	.text._ZNK12v8_inspector8protocol16FundamentalValue5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol16FundamentalValue5cloneEv
	.type	_ZNK12v8_inspector8protocol16FundamentalValue5cloneEv, @function
_ZNK12v8_inspector8protocol16FundamentalValue5cloneEv:
.LFB4965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movl	8(%rsi), %eax
	cmpl	$2, %eax
	je	.L103
	cmpl	$3, %eax
	je	.L104
	cmpl	$1, %eax
	je	.L110
	movq	$0, (%rdi)
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movl	16(%rsi), %ebx
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movl	$2, 8(%rax)
	movq	%rcx, (%rax)
	movl	%ebx, 16(%rax)
	movq	%rax, (%r12)
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movzbl	16(%rsi), %ebx
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rdx
	movl	$1, 8(%rax)
	movq	%rdx, (%rax)
	movb	%bl, 16(%rax)
	movq	%rax, (%r12)
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movsd	16(%rsi), %xmm0
	movl	$24, %edi
	movsd	%xmm0, -24(%rbp)
	call	_Znwm@PLT
	movsd	-24(%rbp), %xmm0
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rdx
	movl	$3, 8(%rax)
	movq	%rdx, (%rax)
	movsd	%xmm0, 16(%rax)
	movq	%rax, (%r12)
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4965:
	.size	_ZNK12v8_inspector8protocol16FundamentalValue5cloneEv, .-_ZNK12v8_inspector8protocol16FundamentalValue5cloneEv
	.section	.text._ZNK12v8_inspector8protocol11BinaryValue5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol11BinaryValue5cloneEv
	.type	_ZNK12v8_inspector8protocol11BinaryValue5cloneEv, @function
_ZNK12v8_inspector8protocol11BinaryValue5cloneEv:
.LFB4974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$16, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol11BinaryValueE(%rip), %rdx
	movl	$5, 8(%rax)
	movq	%rdx, (%rax)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4974:
	.size	_ZNK12v8_inspector8protocol11BinaryValue5cloneEv, .-_ZNK12v8_inspector8protocol11BinaryValue5cloneEv
	.section	.text._ZNK12v8_inspector8protocol9ListValue9writeJSONEPNS_15String16BuilderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol9ListValue9writeJSONEPNS_15String16BuilderE
	.type	_ZNK12v8_inspector8protocol9ListValue9writeJSONEPNS_15String16BuilderE, @function
_ZNK12v8_inspector8protocol9ListValue9writeJSONEPNS_15String16BuilderE:
.LFB5046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$91, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	movq	16(%r12), %rbx
	movq	24(%r12), %r12
	cmpq	%r12, %rbx
	je	.L114
.L115:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*72(%rax)
	cmpq	%rbx, %r12
	je	.L114
	movl	$44, %esi
	movq	%r13, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L114:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$93, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector15String16Builder6appendEt@PLT
	.cfi_endproc
.LFE5046:
	.size	_ZNK12v8_inspector8protocol9ListValue9writeJSONEPNS_15String16BuilderE, .-_ZNK12v8_inspector8protocol9ListValue9writeJSONEPNS_15String16BuilderE
	.section	.text._ZNK12v8_inspector8protocol15SerializedValue9writeJSONEPNS_15String16BuilderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15SerializedValue9writeJSONEPNS_15String16BuilderE
	.type	_ZNK12v8_inspector8protocol15SerializedValue9writeJSONEPNS_15String16BuilderE, @function
_ZNK12v8_inspector8protocol15SerializedValue9writeJSONEPNS_15String16BuilderE:
.LFB4975:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	leaq	16(%r8), %rsi
	jmp	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	.cfi_endproc
.LFE4975:
	.size	_ZNK12v8_inspector8protocol15SerializedValue9writeJSONEPNS_15String16BuilderE, .-_ZNK12v8_inspector8protocol15SerializedValue9writeJSONEPNS_15String16BuilderE
	.section	.text._ZN12v8_inspector8protocol5ValueD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol5ValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol5ValueD0Ev
	.type	_ZN12v8_inspector8protocol5ValueD0Ev, @function
_ZN12v8_inspector8protocol5ValueD0Ev:
.LFB4283:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4283:
	.size	_ZN12v8_inspector8protocol5ValueD0Ev, .-_ZN12v8_inspector8protocol5ValueD0Ev
	.section	.text._ZN12v8_inspector8protocol16FundamentalValueD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol16FundamentalValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16FundamentalValueD0Ev
	.type	_ZN12v8_inspector8protocol16FundamentalValueD0Ev, @function
_ZN12v8_inspector8protocol16FundamentalValueD0Ev:
.LFB6419:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6419:
	.size	_ZN12v8_inspector8protocol16FundamentalValueD0Ev, .-_ZN12v8_inspector8protocol16FundamentalValueD0Ev
	.section	.text._ZN12v8_inspector8protocol11BinaryValueD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol11BinaryValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol11BinaryValueD0Ev
	.type	_ZN12v8_inspector8protocol11BinaryValueD0Ev, @function
_ZN12v8_inspector8protocol11BinaryValueD0Ev:
.LFB6456:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6456:
	.size	_ZN12v8_inspector8protocol11BinaryValueD0Ev, .-_ZN12v8_inspector8protocol11BinaryValueD0Ev
	.section	.text._ZN12v8_inspector8protocol11StringValueD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol11StringValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol11StringValueD0Ev
	.type	_ZN12v8_inspector8protocol11StringValueD0Ev, @function
_ZN12v8_inspector8protocol11StringValueD0Ev:
.LFB6436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L123
	call	_ZdlPv@PLT
.L123:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6436:
	.size	_ZN12v8_inspector8protocol11StringValueD0Ev, .-_ZN12v8_inspector8protocol11StringValueD0Ev
	.section	.text._ZN12v8_inspector8protocol9ListValueD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol9ListValueD0Ev
	.type	_ZN12v8_inspector8protocol9ListValueD0Ev, @function
_ZN12v8_inspector8protocol9ListValueD0Ev:
.LFB5045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol9ListValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	24(%rdi), %rbx
	movq	16(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %rbx
	je	.L126
	.p2align 4,,10
	.p2align 3
.L130:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L127
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*24(%rax)
	cmpq	%r12, %rbx
	jne	.L130
.L128:
	movq	16(%r13), %r12
.L126:
	testq	%r12, %r12
	je	.L131
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L131:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$40, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L130
	jmp	.L128
	.cfi_endproc
.LFE5045:
	.size	_ZN12v8_inspector8protocol9ListValueD0Ev, .-_ZN12v8_inspector8protocol9ListValueD0Ev
	.section	.rodata._ZNK12v8_inspector8protocol11BinaryValue9writeJSONEPNS_15String16BuilderE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unimplemented code"
	.section	.text._ZNK12v8_inspector8protocol11BinaryValue9writeJSONEPNS_15String16BuilderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol11BinaryValue9writeJSONEPNS_15String16BuilderE
	.type	_ZNK12v8_inspector8protocol11BinaryValue9writeJSONEPNS_15String16BuilderE, @function
_ZNK12v8_inspector8protocol11BinaryValue9writeJSONEPNS_15String16BuilderE:
.LFB4972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4972:
	.size	_ZNK12v8_inspector8protocol11BinaryValue9writeJSONEPNS_15String16BuilderE, .-_ZNK12v8_inspector8protocol11BinaryValue9writeJSONEPNS_15String16BuilderE
	.section	.rodata._ZNK12v8_inspector8protocol5Value9writeJSONEPNS_15String16BuilderE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"null"
	.section	.text._ZNK12v8_inspector8protocol5Value9writeJSONEPNS_15String16BuilderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol5Value9writeJSONEPNS_15String16BuilderE
	.type	_ZNK12v8_inspector8protocol5Value9writeJSONEPNS_15String16BuilderE, @function
_ZNK12v8_inspector8protocol5Value9writeJSONEPNS_15String16BuilderE:
.LFB4954:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movl	$4, %edx
	leaq	.LC1(%rip), %rsi
	jmp	_ZN12v8_inspector15String16Builder6appendEPKcm@PLT
	.cfi_endproc
.LFE4954:
	.size	_ZNK12v8_inspector8protocol5Value9writeJSONEPNS_15String16BuilderE, .-_ZNK12v8_inspector8protocol5Value9writeJSONEPNS_15String16BuilderE
	.section	.text._ZNK12v8_inspector8protocol11StringValue9writeJSONEPNS_15String16BuilderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol11StringValue9writeJSONEPNS_15String16BuilderE
	.type	_ZNK12v8_inspector8protocol11StringValue9writeJSONEPNS_15String16BuilderE, @function
_ZNK12v8_inspector8protocol11StringValue9writeJSONEPNS_15String16BuilderE:
.LFB4967:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	leaq	16(%r8), %rsi
	jmp	_ZN12v8_inspector8protocol10StringUtil25builderAppendQuotedStringERNS_15String16BuilderERKNS_8String16E@PLT
	.cfi_endproc
.LFE4967:
	.size	_ZNK12v8_inspector8protocol11StringValue9writeJSONEPNS_15String16BuilderE, .-_ZNK12v8_inspector8protocol11StringValue9writeJSONEPNS_15String16BuilderE
	.section	.text._ZNK12v8_inspector8protocol11BinaryValue11writeBinaryEPSt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol11BinaryValue11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZNK12v8_inspector8protocol11BinaryValue11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZNK12v8_inspector8protocol11BinaryValue11writeBinaryEPSt6vectorIhSaIhEE:
.LFB4973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4973:
	.size	_ZNK12v8_inspector8protocol11BinaryValue11writeBinaryEPSt6vectorIhSaIhEE, .-_ZNK12v8_inspector8protocol11BinaryValue11writeBinaryEPSt6vectorIhSaIhEE
	.section	.text._ZNK12v8_inspector8protocol11StringValue11writeBinaryEPSt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol11StringValue11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZNK12v8_inspector8protocol11StringValue11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZNK12v8_inspector8protocol11StringValue11writeBinaryEPSt6vectorIhSaIhEE:
.LFB4969:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	24(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L145
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L142
	jmp	_ZN30v8_inspector_protocol_encoding4cbor15EncodeFromUTF16ENS_4spanItEEPSt6vectorIhSaIhEE@PLT
	.p2align 4,,10
	.p2align 3
.L142:
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	xorl	%edi, %edi
	jmp	_ZN30v8_inspector_protocol_encoding4cbor13EncodeString8ENS_4spanIhEEPSt6vectorIhSaIhEE@PLT
	.cfi_endproc
.LFE4969:
	.size	_ZNK12v8_inspector8protocol11StringValue11writeBinaryEPSt6vectorIhSaIhEE, .-_ZNK12v8_inspector8protocol11StringValue11writeBinaryEPSt6vectorIhSaIhEE
	.section	.rodata._ZNK12v8_inspector8protocol16FundamentalValue9writeJSONEPNS_15String16BuilderE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"true"
.LC3:
	.string	"false"
	.section	.text._ZNK12v8_inspector8protocol16FundamentalValue9writeJSONEPNS_15String16BuilderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol16FundamentalValue9writeJSONEPNS_15String16BuilderE
	.type	_ZNK12v8_inspector8protocol16FundamentalValue9writeJSONEPNS_15String16BuilderE, @function
_ZNK12v8_inspector8protocol16FundamentalValue9writeJSONEPNS_15String16BuilderE:
.LFB4963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	8(%rdi), %eax
	cmpl	$1, %eax
	je	.L162
	cmpl	$3, %eax
	je	.L163
	cmpl	$2, %eax
	je	.L164
.L146:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movsd	16(%rdi), %xmm0
	movsd	.LC5(%rip), %xmm2
	movapd	%xmm0, %xmm1
	andpd	.LC4(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L166
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String1610fromDoubleEd@PLT
.L161:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L146
	call	_ZdlPv@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L162:
	cmpb	$0, 16(%rdi)
	je	.L148
	movl	$4, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEPKcm@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L148:
	movl	$5, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEPKcm@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L166:
	movl	$4, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEPKcm@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L164:
	movl	16(%rdi), %esi
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	jmp	.L161
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4963:
	.size	_ZNK12v8_inspector8protocol16FundamentalValue9writeJSONEPNS_15String16BuilderE, .-_ZNK12v8_inspector8protocol16FundamentalValue9writeJSONEPNS_15String16BuilderE
	.section	.rodata._ZNK12v8_inspector8protocol15SerializedValue11writeBinaryEPSt6vectorIhSaIhEE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"vector::_M_range_insert"
	.section	.text._ZNK12v8_inspector8protocol15SerializedValue11writeBinaryEPSt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15SerializedValue11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZNK12v8_inspector8protocol15SerializedValue11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZNK12v8_inspector8protocol15SerializedValue11writeBinaryEPSt6vectorIhSaIhEE:
.LFB4976:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	64(%rdi), %r12
	movq	56(%rdi), %r13
	movq	8(%rsi), %r14
	cmpq	%r13, %r12
	je	.L167
	movq	16(%rsi), %rax
	subq	%r13, %r12
	movq	%rsi, %rbx
	subq	%r14, %rax
	cmpq	%rax, %r12
	ja	.L169
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memmove@PLT
	addq	%r12, 8(%rbx)
.L167:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rcx
	movq	(%rsi), %r9
	movq	%r14, %rdx
	movq	%rcx, %rax
	subq	%r9, %rdx
	subq	%rdx, %rax
	cmpq	%rax, %r12
	ja	.L189
	cmpq	%r12, %rdx
	movq	%r12, %rax
	cmovnb	%rdx, %rax
	addq	%rdx, %rax
	movq	%rax, %r15
	jc	.L181
	testq	%rax, %rax
	js	.L181
	jne	.L173
	movq	$0, -56(%rbp)
	xorl	%r15d, %r15d
.L179:
	movq	-56(%rbp), %rax
	leaq	(%rax,%rdx), %r11
	leaq	(%r11,%r12), %r10
	testq	%rdx, %rdx
	jne	.L190
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	subq	%r14, %rdx
	leaq	(%r10,%rdx), %r12
	jne	.L175
.L177:
	testq	%r9, %r9
	jne	.L176
.L178:
	movq	-56(%rbp), %xmm0
	movq	%r12, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movq	%rcx, %r15
.L173:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r9
	movq	%r14, %rdx
	movq	%rax, -56(%rbp)
	addq	%rax, %r15
	subq	%r9, %rdx
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%r9, %rsi
	movq	%rax, %rdi
	movq	%r10, -80(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r11, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r11
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-80(%rbp), %r10
	movq	-64(%rbp), %r9
	subq	%r14, %rdx
	leaq	(%r10,%rdx), %r12
	jne	.L175
.L176:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%r14, %rsi
	movq	%r10, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	jmp	.L177
.L189:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4976:
	.size	_ZNK12v8_inspector8protocol15SerializedValue11writeBinaryEPSt6vectorIhSaIhEE, .-_ZNK12v8_inspector8protocol15SerializedValue11writeBinaryEPSt6vectorIhSaIhEE
	.section	.text._ZN12v8_inspector8protocol14UberDispatcherD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14UberDispatcherD0Ev
	.type	_ZN12v8_inspector8protocol14UberDispatcherD0Ev, @function
_ZN12v8_inspector8protocol14UberDispatcherD0Ev:
.LFB5217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol14UberDispatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	88(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L196
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L226:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L192
.L195:
	movq	%rbx, %r13
.L196:
	movq	48(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L193
	movq	(%rdi), %rax
	call	*8(%rax)
.L193:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L226
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L195
.L192:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L197
	call	_ZdlPv@PLT
.L197:
	movq	32(%r12), %r13
	testq	%r13, %r13
	jne	.L202
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L227:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L198
.L201:
	movq	%rbx, %r13
.L202:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L199
	call	_ZdlPv@PLT
.L199:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L227
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L201
.L198:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	16(%r12), %rdi
	leaq	64(%r12), %rax
	movq	$0, 40(%r12)
	movq	$0, 32(%r12)
	cmpq	%rax, %rdi
	je	.L203
	call	_ZdlPv@PLT
.L203:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5217:
	.size	_ZN12v8_inspector8protocol14UberDispatcherD0Ev, .-_ZN12v8_inspector8protocol14UberDispatcherD0Ev
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_, @function
_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_:
.LFB7666:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsIhEEvPKT_S5_PS5_
	movq	(%rdx), %rax
	cmpq	%rax, %rsi
	je	.L310
	movzbl	(%rax), %ecx
	leal	-34(%rcx), %edx
	cmpb	$91, %dl
	ja	.L310
	leaq	.L231(%rip), %rdi
	movzbl	%dl, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_,"a",@progbits
	.align 4
	.align 4
.L231:
	.long	.L241-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L240-.L231
	.long	.L239-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L239-.L231
	.long	.L239-.L231
	.long	.L239-.L231
	.long	.L239-.L231
	.long	.L239-.L231
	.long	.L239-.L231
	.long	.L239-.L231
	.long	.L239-.L231
	.long	.L239-.L231
	.long	.L239-.L231
	.long	.L238-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L237-.L231
	.long	.L310-.L231
	.long	.L236-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L277-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L278-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L279-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L310-.L231
	.long	.L232-.L231
	.long	.L310-.L231
	.long	.L230-.L231
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	.p2align 4,,10
	.p2align 3
.L346:
	movzbl	(%rax), %r8d
	addq	$1, %rcx
	movsbl	%dl, %edi
	addq	$1, %rax
	movzbl	(%rcx), %edx
	cmpl	%edi, %r8d
	je	.L234
.L242:
	testb	%dl, %dl
	je	.L243
	.p2align 4,,10
	.p2align 3
.L310:
	movl	$11, %r8d
.L228:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	cmpb	$45, %cl
	je	.L336
.L248:
	cmpq	%rax, %rsi
	jbe	.L310
	xorl	%edi, %edi
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L338:
	addq	$1, %rax
	addl	$1, %edi
	cmpq	%rax, %rsi
	je	.L337
.L251:
	movzbl	(%rax), %edx
	subl	$48, %edx
	cmpb	$9, %dl
	jbe	.L338
	movl	$11, %r8d
	testl	%edi, %edi
	je	.L228
	cmpl	$1, %edi
	je	.L312
	cmpb	$48, %cl
	je	.L228
.L312:
	cmpq	%rax, %rsi
	je	.L256
	movzbl	(%rax), %edx
	cmpb	$46, %dl
	je	.L339
.L254:
	andl	$-33, %edx
	cmpb	$69, %dl
	jne	.L259
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L310
	movzbl	1(%rax), %ecx
	subl	$43, %ecx
	andl	$253, %ecx
	jne	.L260
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L310
.L260:
	cmpq	%rdx, %rsi
	jbe	.L310
	xorl	%ecx, %ecx
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L341:
	addq	$1, %rdx
	addl	$1, %ecx
	cmpq	%rdx, %rsi
	je	.L340
.L262:
	movzbl	(%rdx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L341
	movl	$11, %r8d
	movq	%rdx, %rax
	testl	%ecx, %ecx
	je	.L228
	.p2align 4,,10
	.p2align 3
.L259:
	movq	%rax, (%r9)
	movl	$5, %r8d
	jmp	.L228
.L238:
	addq	$1, %rax
	movl	$10, %r8d
	movq	%rax, (%r9)
	jmp	.L228
.L237:
	addq	$1, %rax
	movl	$2, %r8d
	movq	%rax, (%r9)
	jmp	.L228
.L236:
	addq	$1, %rax
	movl	$3, %r8d
	movq	%rax, (%r9)
	jmp	.L228
.L232:
	addq	$1, %rax
	xorl	%r8d, %r8d
	movq	%rax, (%r9)
	jmp	.L228
.L230:
	addq	$1, %rax
	movl	$1, %r8d
	movq	%rax, (%r9)
	jmp	.L228
.L241:
	addq	$1, %rax
	cmpq	%rsi, %rax
	jnb	.L310
	movzbl	(%rax), %edx
	leaq	.L267(%rip), %rdi
	leaq	1(%rax), %rcx
	cmpb	$92, %dl
	je	.L342
	.p2align 4,,10
	.p2align 3
.L265:
	cmpb	$34, %dl
	je	.L343
.L309:
	movq	%rcx, %rax
	cmpq	%rcx, %rsi
	jbe	.L310
	movzbl	(%rax), %edx
	leaq	1(%rax), %rcx
	cmpb	$92, %dl
	jne	.L265
.L342:
	cmpq	%rcx, %rsi
	je	.L310
	movzbl	1(%rax), %edx
	leaq	2(%rax), %rcx
	subl	$34, %edx
	cmpb	$86, %dl
	ja	.L310
	movzbl	%dl, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	.align 4
	.align 4
.L267:
	.long	.L309-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L309-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L309-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L309-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L309-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L309-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L310-.L267
	.long	.L309-.L267
	.long	.L310-.L267
	.long	.L309-.L267
	.long	.L268-.L267
	.long	.L309-.L267
	.long	.L310-.L267
	.long	.L266-.L267
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
.L240:
	addq	$1, %rax
	movl	$9, %r8d
	movq	%rax, (%r9)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L336:
	leaq	1(%rax), %rdx
	movl	$11, %r8d
	cmpq	%rdx, %rsi
	je	.L228
	movzbl	1(%rax), %ecx
	movq	%rdx, %rax
	jmp	.L248
.L277:
	movl	$102, %edx
	leaq	.LC3(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L235:
	cmpq	%rax, %rsi
	jbe	.L246
	testb	%dl, %dl
	jne	.L344
.L247:
	movq	%rax, (%r9)
	movl	$7, %r8d
	jmp	.L228
.L279:
	movl	$116, %edx
	leaq	.LC2(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L233:
	cmpq	%rax, %rsi
	jbe	.L244
	testb	%dl, %dl
	jne	.L345
.L245:
	movq	%rax, (%r9)
	movl	$6, %r8d
	jmp	.L228
.L278:
	movl	$110, %edx
	leaq	.LC1(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L234:
	cmpq	%rax, %rsi
	jbe	.L242
	testb	%dl, %dl
	jne	.L346
.L243:
	movq	%rax, (%r9)
	movl	$8, %r8d
	jmp	.L228
.L266:
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	cmpq	$1, %rdx
	jle	.L310
	movzbl	2(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpb	$9, %cl
	jbe	.L269
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$5, %dl
	ja	.L310
.L269:
	leaq	4(%rax), %rcx
	movzbl	3(%rax), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L309
.L335:
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$5, %al
	jbe	.L309
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L268:
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	cmpq	$3, %rdx
	jle	.L310
	movzbl	2(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpb	$9, %cl
	jbe	.L270
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$5, %dl
	ja	.L310
.L270:
	movzbl	3(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpb	$9, %cl
	jbe	.L271
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$5, %dl
	ja	.L310
.L271:
	movzbl	4(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpb	$9, %cl
	jbe	.L272
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$5, %dl
	ja	.L310
.L272:
	leaq	6(%rax), %rcx
	movzbl	5(%rax), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L309
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L344:
	movzbl	(%rax), %r8d
	addq	$1, %rcx
	movsbl	%dl, %edi
	addq	$1, %rax
	movzbl	(%rcx), %edx
	cmpl	%edi, %r8d
	je	.L235
.L246:
	testb	%dl, %dl
	jne	.L310
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L345:
	movzbl	(%rax), %r8d
	addq	$1, %rcx
	movsbl	%dl, %edi
	addq	$1, %rax
	movzbl	(%rcx), %edx
	cmpl	%edi, %r8d
	je	.L233
.L244:
	testb	%dl, %dl
	jne	.L310
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L343:
	movq	%rcx, (%r9)
	movl	$4, %r8d
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L337:
	cmpl	$1, %edi
	je	.L256
	movl	$11, %r8d
	cmpb	$48, %cl
	je	.L228
.L256:
	movq	%rsi, (%r9)
	movl	$5, %r8d
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L339:
	addq	$1, %rax
	cmpq	%rax, %rsi
	je	.L310
	jbe	.L310
	xorl	%ecx, %ecx
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L347:
	addq	$1, %rax
	addl	$1, %ecx
	cmpq	%rax, %rsi
	je	.L256
.L257:
	movzbl	(%rax), %edx
	leal	-48(%rdx), %edi
	cmpb	$9, %dil
	jbe	.L347
	movl	$11, %r8d
	testl	%ecx, %ecx
	je	.L228
	cmpq	%rax, %rsi
	jne	.L254
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L340:
	movq	%rsi, %rax
	jmp	.L259
	.cfi_endproc
.LFE7666:
	.size	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_, .-_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_, @function
_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_:
.LFB7661:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsItEEvPKT_S5_PS5_
	movq	(%rdx), %rax
	cmpq	%rax, %rsi
	je	.L428
	movzwl	(%rax), %ecx
	leal	-34(%rcx), %edx
	cmpw	$91, %dx
	ja	.L428
	leaq	.L351(%rip), %rdi
	movzwl	%dx, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_,"a",@progbits
	.align 4
	.align 4
.L351:
	.long	.L361-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L360-.L351
	.long	.L359-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L359-.L351
	.long	.L359-.L351
	.long	.L359-.L351
	.long	.L359-.L351
	.long	.L359-.L351
	.long	.L359-.L351
	.long	.L359-.L351
	.long	.L359-.L351
	.long	.L359-.L351
	.long	.L359-.L351
	.long	.L358-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L357-.L351
	.long	.L428-.L351
	.long	.L356-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L395-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L396-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L397-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L428-.L351
	.long	.L352-.L351
	.long	.L428-.L351
	.long	.L350-.L351
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	.p2align 4,,10
	.p2align 3
.L459:
	movzwl	(%rax), %r8d
	addq	$1, %rcx
	movsbl	%dl, %edi
	addq	$2, %rax
	movzbl	(%rcx), %edx
	cmpl	%edi, %r8d
	je	.L354
.L362:
	testb	%dl, %dl
	je	.L363
	.p2align 4,,10
	.p2align 3
.L428:
	movl	$11, %r8d
.L348:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	cmpw	$45, %cx
	je	.L451
.L368:
	cmpq	%rax, %rsi
	jbe	.L428
	xorl	%edi, %edi
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L452:
	addq	$2, %rax
	addl	$1, %edi
	cmpq	%rax, %rsi
	jbe	.L370
.L371:
	movzwl	(%rax), %edx
	subl	$48, %edx
	cmpw	$9, %dx
	jbe	.L452
	movl	$11, %r8d
	testl	%edi, %edi
	je	.L348
.L370:
	cmpl	$1, %edi
	je	.L429
	cmpw	$48, %cx
	je	.L428
.L429:
	cmpq	%rax, %rsi
	je	.L449
	movzwl	(%rax), %edx
	cmpw	$46, %dx
	je	.L453
.L374:
	andl	$-33, %edx
	cmpw	$69, %dx
	jne	.L379
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L428
	movzwl	2(%rax), %ecx
	subl	$43, %ecx
	testw	$-3, %cx
	jne	.L380
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L428
.L380:
	cmpq	%rdx, %rsi
	jbe	.L428
	xorl	%ecx, %ecx
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L454:
	addq	$2, %rdx
	addl	$1, %ecx
	cmpq	%rdx, %rsi
	jbe	.L410
.L382:
	movzwl	(%rdx), %eax
	subl	$48, %eax
	cmpw	$9, %ax
	jbe	.L454
	movl	$11, %r8d
	testl	%ecx, %ecx
	je	.L348
.L410:
	movq	%rdx, %rax
.L379:
	movq	%rax, (%r9)
	movl	$5, %r8d
	jmp	.L348
.L358:
	addq	$2, %rax
	movl	$10, %r8d
	movq	%rax, (%r9)
	jmp	.L348
.L357:
	addq	$2, %rax
	movl	$2, %r8d
	movq	%rax, (%r9)
	jmp	.L348
.L356:
	addq	$2, %rax
	movl	$3, %r8d
	movq	%rax, (%r9)
	jmp	.L348
.L352:
	addq	$2, %rax
	xorl	%r8d, %r8d
	movq	%rax, (%r9)
	jmp	.L348
.L350:
	addq	$2, %rax
	movl	$1, %r8d
	movq	%rax, (%r9)
	jmp	.L348
.L361:
	addq	$2, %rax
	cmpq	%rsi, %rax
	jnb	.L428
	movzwl	(%rax), %edx
	leaq	.L387(%rip), %rdi
	leaq	2(%rax), %rcx
	cmpw	$92, %dx
	je	.L455
	.p2align 4,,10
	.p2align 3
.L385:
	cmpw	$34, %dx
	je	.L456
.L427:
	movq	%rcx, %rax
	cmpq	%rcx, %rsi
	jbe	.L428
	movzwl	(%rax), %edx
	leaq	2(%rax), %rcx
	cmpw	$92, %dx
	jne	.L385
.L455:
	cmpq	%rcx, %rsi
	je	.L428
	movzwl	2(%rax), %edx
	leaq	4(%rax), %rcx
	subl	$34, %edx
	cmpw	$86, %dx
	ja	.L428
	movzwl	%dx, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	.align 4
	.align 4
.L387:
	.long	.L427-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L427-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L427-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L427-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L427-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L427-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L428-.L387
	.long	.L427-.L387
	.long	.L428-.L387
	.long	.L427-.L387
	.long	.L388-.L387
	.long	.L427-.L387
	.long	.L428-.L387
	.long	.L386-.L387
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
.L360:
	addq	$2, %rax
	movl	$9, %r8d
	movq	%rax, (%r9)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L451:
	leaq	2(%rax), %rdx
	movl	$11, %r8d
	cmpq	%rdx, %rsi
	je	.L348
	movzwl	2(%rax), %ecx
	movq	%rdx, %rax
	jmp	.L368
.L395:
	movl	$102, %edx
	leaq	.LC3(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L355:
	cmpq	%rax, %rsi
	jbe	.L366
	testb	%dl, %dl
	jne	.L457
.L367:
	movq	%rax, (%r9)
	movl	$7, %r8d
	jmp	.L348
.L397:
	movl	$116, %edx
	leaq	.LC2(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L353:
	cmpq	%rax, %rsi
	jbe	.L364
	testb	%dl, %dl
	jne	.L458
.L365:
	movq	%rax, (%r9)
	movl	$6, %r8d
	jmp	.L348
.L396:
	movl	$110, %edx
	leaq	.LC1(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L354:
	cmpq	%rax, %rsi
	jbe	.L362
	testb	%dl, %dl
	jne	.L459
.L363:
	movq	%rax, (%r9)
	movl	$8, %r8d
	jmp	.L348
.L386:
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	cmpq	$2, %rdx
	jle	.L428
	movzwl	4(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpw	$9, %cx
	jbe	.L389
	andl	$-33, %edx
	subl	$65, %edx
	cmpw	$5, %dx
	ja	.L428
.L389:
	leaq	8(%rax), %rcx
	movzwl	6(%rax), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L427
.L450:
	andl	$-33, %eax
	subl	$65, %eax
	cmpw	$5, %ax
	jbe	.L427
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L388:
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	cmpq	$6, %rdx
	jle	.L428
	movzwl	4(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpw	$9, %cx
	jbe	.L390
	andl	$-33, %edx
	subl	$65, %edx
	cmpw	$5, %dx
	ja	.L428
.L390:
	movzwl	6(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpw	$9, %cx
	jbe	.L391
	andl	$-33, %edx
	subl	$65, %edx
	cmpw	$5, %dx
	ja	.L428
.L391:
	movzwl	8(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpw	$9, %cx
	jbe	.L392
	andl	$-33, %edx
	subl	$65, %edx
	cmpw	$5, %dx
	ja	.L428
.L392:
	leaq	12(%rax), %rcx
	movzwl	10(%rax), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L427
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L457:
	movzwl	(%rax), %r8d
	addq	$1, %rcx
	movsbl	%dl, %edi
	addq	$2, %rax
	movzbl	(%rcx), %edx
	cmpl	%edi, %r8d
	je	.L355
.L366:
	testb	%dl, %dl
	jne	.L428
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L458:
	movzwl	(%rax), %r8d
	addq	$1, %rcx
	movsbl	%dl, %edi
	addq	$2, %rax
	movzbl	(%rcx), %edx
	cmpl	%edi, %r8d
	je	.L353
.L364:
	testb	%dl, %dl
	jne	.L428
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L456:
	movq	%rcx, (%r9)
	movl	$4, %r8d
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L453:
	addq	$2, %rax
	cmpq	%rax, %rsi
	je	.L428
	jbe	.L428
	xorl	%ecx, %ecx
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L460:
	addq	$2, %rax
	addl	$1, %ecx
	cmpq	%rax, %rsi
	jbe	.L376
.L377:
	movzwl	(%rax), %edi
	leal	-48(%rdi), %edx
	cmpw	$9, %dx
	jbe	.L460
	movl	$11, %r8d
	testl	%ecx, %ecx
	je	.L348
.L376:
	cmpq	%rax, %rsi
	je	.L449
	movzwl	(%rax), %edx
	jmp	.L374
.L449:
	movq	%rsi, (%r9)
	movl	$5, %r8d
	jmp	.L348
	.cfi_endproc
.LFE7661:
	.size	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_, .-_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	.section	.text._ZN12v8_inspector8protocol15DictionaryValueD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol15DictionaryValueD0Ev
	.type	_ZN12v8_inspector8protocol15DictionaryValueD0Ev, @function
_ZN12v8_inspector8protocol15DictionaryValueD0Ev:
.LFB4984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol15DictionaryValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	80(%rdi), %rbx
	movq	72(%rdi), %r13
	movq	%rax, (%rdi)
	cmpq	%r13, %rbx
	je	.L462
	.p2align 4,,10
	.p2align 3
.L466:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L463
	call	_ZdlPv@PLT
	addq	$40, %r13
	cmpq	%r13, %rbx
	jne	.L466
.L464:
	movq	72(%r12), %r13
.L462:
	testq	%r13, %r13
	je	.L467
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L467:
	movq	32(%r12), %r13
	testq	%r13, %r13
	jne	.L468
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L490:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L473
.L474:
	movq	%rbx, %r13
.L468:
	movq	48(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L471
	movq	(%rdi), %rax
	call	*24(%rax)
.L471:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L490
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L474
.L473:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	16(%r12), %rdi
	leaq	64(%r12), %rax
	movq	$0, 40(%r12)
	movq	$0, 32(%r12)
	cmpq	%rax, %rdi
	je	.L469
	call	_ZdlPv@PLT
.L469:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	addq	$40, %r13
	cmpq	%r13, %rbx
	jne	.L466
	jmp	.L464
	.cfi_endproc
.LFE4984:
	.size	_ZN12v8_inspector8protocol15DictionaryValueD0Ev, .-_ZN12v8_inspector8protocol15DictionaryValueD0Ev
	.section	.text._ZN12v8_inspector8protocol15SerializedValueD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol15SerializedValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol15SerializedValueD0Ev
	.type	_ZN12v8_inspector8protocol15SerializedValueD0Ev, @function
_ZN12v8_inspector8protocol15SerializedValueD0Ev:
.LFB6473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol15SerializedValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L492
	call	_ZdlPv@PLT
.L492:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L493
	call	_ZdlPv@PLT
.L493:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6473:
	.size	_ZN12v8_inspector8protocol15SerializedValueD0Ev, .-_ZN12v8_inspector8protocol15SerializedValueD0Ev
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorD0Ev, @function
_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorD0Ev:
.LFB5152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L499
	call	_ZdlPv@PLT
.L499:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L500
	call	_ZdlPv@PLT
.L500:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5152:
	.size	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorD0Ev, .-_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorD0Ev
	.section	.text._ZN12v8_inspector8protocol16InternalResponseD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16InternalResponseD0Ev
	.type	_ZN12v8_inspector8protocol16InternalResponseD0Ev, @function
_ZN12v8_inspector8protocol16InternalResponseD0Ev:
.LFB4412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L503
	movq	(%rdi), %rax
	call	*24(%rax)
.L503:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L504
	call	_ZdlPv@PLT
.L504:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4412:
	.size	_ZN12v8_inspector8protocol16InternalResponseD0Ev, .-_ZN12v8_inspector8protocol16InternalResponseD0Ev
	.section	.text._ZN12v8_inspector8protocol14UberDispatcherD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14UberDispatcherD2Ev
	.type	_ZN12v8_inspector8protocol14UberDispatcherD2Ev, @function
_ZN12v8_inspector8protocol14UberDispatcherD2Ev:
.LFB5215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol14UberDispatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	88(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	jne	.L514
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L544:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L510
.L513:
	movq	%r13, %r12
.L514:
	movq	48(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L511
	movq	(%rdi), %rax
	call	*8(%rax)
.L511:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L544
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L513
.L510:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%rbx), %rdi
	leaq	120(%rbx), %rax
	movq	$0, 96(%rbx)
	movq	$0, 88(%rbx)
	cmpq	%rax, %rdi
	je	.L515
	call	_ZdlPv@PLT
.L515:
	movq	32(%rbx), %r12
	testq	%r12, %r12
	jne	.L520
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L545:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L516
.L519:
	movq	%r13, %r12
.L520:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	movq	(%r12), %r13
	cmpq	%rax, %rdi
	je	.L517
	call	_ZdlPv@PLT
.L517:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L545
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L519
.L516:
	movq	24(%rbx), %rax
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	addq	$64, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L509
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L509:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5215:
	.size	_ZN12v8_inspector8protocol14UberDispatcherD2Ev, .-_ZN12v8_inspector8protocol14UberDispatcherD2Ev
	.globl	_ZN12v8_inspector8protocol14UberDispatcherD1Ev
	.set	_ZN12v8_inspector8protocol14UberDispatcherD1Ev,_ZN12v8_inspector8protocol14UberDispatcherD2Ev
	.section	.text._ZN12v8_inspector8protocol9ListValueD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol9ListValueD2Ev
	.type	_ZN12v8_inspector8protocol9ListValueD2Ev, @function
_ZN12v8_inspector8protocol9ListValueD2Ev:
.LFB5043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol9ListValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %r13
	movq	16(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L547
	.p2align 4,,10
	.p2align 3
.L551:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L548
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*24(%rax)
	cmpq	%r12, %r13
	jne	.L551
.L549:
	movq	16(%rbx), %r12
.L547:
	testq	%r12, %r12
	je	.L546
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L548:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L551
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L546:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5043:
	.size	_ZN12v8_inspector8protocol9ListValueD2Ev, .-_ZN12v8_inspector8protocol9ListValueD2Ev
	.globl	_ZN12v8_inspector8protocol9ListValueD1Ev
	.set	_ZN12v8_inspector8protocol9ListValueD1Ev,_ZN12v8_inspector8protocol9ListValueD2Ev
	.section	.text._ZN12v8_inspector8protocol15DictionaryValueD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol15DictionaryValueD2Ev
	.type	_ZN12v8_inspector8protocol15DictionaryValueD2Ev, @function
_ZN12v8_inspector8protocol15DictionaryValueD2Ev:
.LFB4982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol15DictionaryValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %r13
	movq	72(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L555
	.p2align 4,,10
	.p2align 3
.L559:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L556
	call	_ZdlPv@PLT
	addq	$40, %r12
	cmpq	%r12, %r13
	jne	.L559
.L557:
	movq	72(%rbx), %r12
.L555:
	testq	%r12, %r12
	je	.L560
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L560:
	movq	32(%rbx), %r12
	testq	%r12, %r12
	jne	.L561
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L583:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L566
.L567:
	movq	%r13, %r12
.L561:
	movq	48(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L564
	movq	(%rdi), %rax
	call	*24(%rax)
.L564:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L583
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L567
.L566:
	movq	24(%rbx), %rax
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	addq	$64, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L584
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L556:
	.cfi_restore_state
	addq	$40, %r12
	cmpq	%r12, %r13
	jne	.L559
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L584:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4982:
	.size	_ZN12v8_inspector8protocol15DictionaryValueD2Ev, .-_ZN12v8_inspector8protocol15DictionaryValueD2Ev
	.globl	_ZN12v8_inspector8protocol15DictionaryValueD1Ev
	.set	_ZN12v8_inspector8protocol15DictionaryValueD1Ev,_ZN12v8_inspector8protocol15DictionaryValueD2Ev
	.section	.text._ZN12v8_inspector8protocol5Value15serializeToJSONEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol5Value15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol5Value15serializeToJSONEv, @function
_ZN12v8_inspector8protocol5Value15serializeToJSONEv:
.LFB4958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movl	$512, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder15reserveCapacityEm@PLT
	movq	0(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*72(%rax)
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L585
	call	_ZdlPv@PLT
.L585:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L592
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L592:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4958:
	.size	_ZN12v8_inspector8protocol5Value15serializeToJSONEv, .-_ZN12v8_inspector8protocol5Value15serializeToJSONEv
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0.str1.1,"aMS",@progbits,1
.LC8:
	.string	"basic_string::_M_create"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0:
.LFB8832:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	jne	.L594
	testq	%rdx, %rdx
	jne	.L610
.L594:
	subq	%r13, %rbx
	movq	%rbx, %r14
	sarq	%r14
	cmpq	$15, %rbx
	ja	.L595
	movq	(%r12), %rdi
.L596:
	cmpq	$2, %rbx
	je	.L611
	testq	%rbx, %rbx
	je	.L599
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L599:
	xorl	%eax, %eax
	movq	%r14, 8(%r12)
	movw	%ax, (%rdi,%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L611:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L595:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L612
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r14, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L596
.L610:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L612:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8832:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	.section	.rodata._ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S5_PNS_8String16E.constprop.0.str1.1,"aMS",@progbits,1
.LC9:
	.string	""
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S5_PNS_8String16E.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S5_PNS_8String16E.constprop.0, @function
_ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S5_PNS_8String16E.constprop.0:
.LFB8836:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L689
	movl	$0, %eax
	jbe	.L690
.L613:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L691
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L690:
	.cfi_restore_state
	leaq	-128(%rbp), %r15
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%r15, %rdi
	leaq	.L630(%rip), %r14
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	subq	%rbx, %rsi
	sarq	%rsi
	call	_ZN12v8_inspector15String16Builder15reserveCapacityEm@PLT
	.p2align 4,,10
	.p2align 3
.L623:
	movzwl	(%rbx), %esi
	leaq	2(%rbx), %rax
	cmpw	$92, %si
	jne	.L692
	cmpq	%rax, %r12
	je	.L663
	movzwl	2(%rbx), %esi
	leaq	4(%rbx), %rdx
	cmpw	$120, %si
	je	.L663
	cmpw	$34, %si
	je	.L636
	leal	-47(%rsi), %eax
	cmpw	$71, %ax
	ja	.L663
	movzwl	%ax, %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S5_PNS_8String16E.constprop.0,"a",@progbits
	.align 4
	.align 4
.L630:
	.long	.L636-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L636-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L664-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L635-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L634-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L663-.L630
	.long	.L633-.L630
	.long	.L663-.L630
	.long	.L632-.L630
	.long	.L631-.L630
	.long	.L629-.L630
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S5_PNS_8String16E.constprop.0
.L635:
	movl	$12, %esi
.L636:
	movq	%r15, %rdi
	movq	%rdx, -136(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	movq	-136(%rbp), %rdx
	movq	%rdx, %rbx
.L626:
	cmpq	%rbx, %r12
	ja	.L623
	leaq	-96(%rbp), %rdi
	movq	%r15, %rsi
	leaq	-80(%rbp), %rbx
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-96(%rbp), %rdx
	movq	0(%r13), %rdi
	cmpq	%rbx, %rdx
	je	.L693
	leaq	16(%r13), %rsi
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rax
	cmpq	%rsi, %rdi
	je	.L694
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	16(%r13), %rsi
	movq	%rdx, 0(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r13)
	testq	%rdi, %rdi
	je	.L654
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L652:
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movw	%ax, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 32(%r13)
	cmpq	%rbx, %rdi
	je	.L655
	call	_ZdlPv@PLT
.L655:
	movl	$1, %eax
	jmp	.L627
.L664:
	movl	$8, %esi
	jmp	.L636
.L663:
	xorl	%eax, %eax
.L627:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L613
	movb	%al, -136(%rbp)
	call	_ZdlPv@PLT
	movzbl	-136(%rbp), %eax
	jmp	.L613
.L629:
	movl	$11, %esi
	jmp	.L636
.L631:
	movzwl	4(%rbx), %eax
	leal	-48(%rax), %esi
	cmpw	$9, %si
	jbe	.L688
	leal	-65(%rax), %edx
	cmpw	$5, %dx
	jbe	.L695
	leal	-87(%rax), %esi
	leal	-97(%rax), %edx
	movl	%esi, %eax
	movl	$0, %esi
	sall	$12, %eax
	cmpw	$6, %dx
	cmovb	%eax, %esi
.L638:
	movzwl	6(%rbx), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L696
	leal	-65(%rax), %edx
	cmpw	$5, %dx
	jbe	.L697
	leal	-97(%rax), %edx
	cmpw	$5, %dx
	ja	.L641
	subl	$87, %eax
	sall	$8, %eax
	addl	%eax, %esi
	.p2align 4,,10
	.p2align 3
.L641:
	movzwl	8(%rbx), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L698
	leal	-65(%rax), %edx
	cmpw	$5, %dx
	jbe	.L699
	leal	-97(%rax), %edx
	cmpw	$5, %dx
	ja	.L644
	subl	$87, %eax
	sall	$4, %eax
	addl	%eax, %esi
	.p2align 4,,10
	.p2align 3
.L644:
	movzwl	10(%rbx), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L700
	leal	-65(%rax), %edx
	cmpw	$5, %dx
	jbe	.L701
	leal	-97(%rax), %edx
	leal	-87(%rsi,%rax), %eax
	cmpw	$6, %dx
	cmovb	%eax, %esi
.L647:
	leaq	12(%rbx), %rdx
	movzwl	%si, %esi
	jmp	.L636
.L632:
	movl	$9, %esi
	jmp	.L636
.L633:
	movl	$13, %esi
	jmp	.L636
.L634:
	movl	$10, %esi
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L692:
	movq	%r15, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	movq	-136(%rbp), %rax
	movq	%rax, %rbx
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L695:
	leal	-55(%rax), %esi
.L688:
	sall	$12, %esi
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L693:
	movq	-88(%rbp), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L650
	cmpq	$1, %rax
	je	.L702
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L650
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	0(%r13), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L650:
	xorl	%ecx, %ecx
	movq	%rax, 8(%r13)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L689:
	leaq	-96(%rbp), %rdi
	leaq	.LC9(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rbx
	movq	0(%r13), %rdi
	cmpq	%rbx, %rdx
	je	.L703
	leaq	16(%r13), %rsi
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rax
	cmpq	%rsi, %rdi
	je	.L704
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	16(%r13), %rsi
	movq	%rdx, 0(%r13)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%r13)
	testq	%rdi, %rdi
	je	.L620
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L618:
	xorl	%esi, %esi
	movq	$0, -88(%rbp)
	movw	%si, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 32(%r13)
	cmpq	%rbx, %rdi
	je	.L621
	call	_ZdlPv@PLT
.L621:
	movl	$1, %eax
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L698:
	sall	$4, %edx
	addl	%edx, %esi
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L696:
	sall	$8, %edx
	addl	%edx, %esi
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L700:
	addl	%edx, %esi
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L701:
	leal	-55(%rsi,%rax), %esi
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L697:
	subl	$55, %eax
	sall	$8, %eax
	addl	%eax, %esi
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L699:
	subl	$55, %eax
	sall	$4, %eax
	addl	%eax, %esi
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L704:
	movq	%rax, %xmm0
	movq	%rcx, %xmm4
	movq	%rdx, 0(%r13)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r13)
.L620:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L694:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdx, 0(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
.L654:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L703:
	movq	-88(%rbp), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L616
	cmpq	$1, %rax
	je	.L705
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L616
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	0(%r13), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L616:
	xorl	%r8d, %r8d
	movq	%rax, 8(%r13)
	movw	%r8w, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L702:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	0(%r13), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L650
.L705:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	0(%r13), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L616
.L691:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8836:
	.size	_ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S5_PNS_8String16E.constprop.0, .-_ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S5_PNS_8String16E.constprop.0
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S5_PNS_8String16E.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S5_PNS_8String16E.constprop.0, @function
_ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S5_PNS_8String16E.constprop.0:
.LFB8840:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L782
	movl	$0, %eax
	jbe	.L783
.L706:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L784
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L783:
	.cfi_restore_state
	leaq	-128(%rbp), %r15
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%r15, %rdi
	leaq	.L723(%rip), %r14
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	subq	%rbx, %rsi
	call	_ZN12v8_inspector15String16Builder15reserveCapacityEm@PLT
	.p2align 4,,10
	.p2align 3
.L716:
	movzbl	(%rbx), %esi
	leaq	1(%rbx), %rax
	cmpb	$92, %sil
	jne	.L785
	cmpq	%rax, %r12
	je	.L756
	movzbl	1(%rbx), %esi
	leaq	2(%rbx), %rdx
	cmpb	$120, %sil
	je	.L756
	cmpb	$34, %sil
	je	.L728
	leal	-47(%rsi), %eax
	cmpb	$71, %al
	ja	.L756
	movzbl	%al, %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S5_PNS_8String16E.constprop.0,"a",@progbits
	.align 4
	.align 4
.L723:
	.long	.L728-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L728-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L729-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L757-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L727-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L756-.L723
	.long	.L726-.L723
	.long	.L756-.L723
	.long	.L725-.L723
	.long	.L724-.L723
	.long	.L722-.L723
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S5_PNS_8String16E.constprop.0
.L757:
	movl	$12, %esi
.L728:
	movq	%r15, %rdi
	movq	%rdx, -136(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	movq	-136(%rbp), %rdx
	movq	%rdx, %rbx
.L719:
	cmpq	%rbx, %r12
	ja	.L716
	leaq	-96(%rbp), %rdi
	movq	%r15, %rsi
	leaq	-80(%rbp), %rbx
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-96(%rbp), %rdx
	movq	0(%r13), %rdi
	cmpq	%rbx, %rdx
	je	.L786
	leaq	16(%r13), %rsi
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rax
	cmpq	%rsi, %rdi
	je	.L787
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	16(%r13), %rsi
	movq	%rdx, 0(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r13)
	testq	%rdi, %rdi
	je	.L747
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L745:
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movw	%ax, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 32(%r13)
	cmpq	%rbx, %rdi
	je	.L748
	call	_ZdlPv@PLT
.L748:
	movl	$1, %eax
	jmp	.L720
.L729:
	movl	$8, %esi
	jmp	.L728
.L756:
	xorl	%eax, %eax
.L720:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L706
	movb	%al, -136(%rbp)
	call	_ZdlPv@PLT
	movzbl	-136(%rbp), %eax
	jmp	.L706
.L722:
	movl	$11, %esi
	jmp	.L728
.L724:
	movzbl	2(%rbx), %esi
	leal	-48(%rsi), %edx
	movl	%esi, %eax
	cmpb	$9, %dl
	jbe	.L781
	leal	-65(%rsi), %edx
	cmpb	$5, %dl
	jbe	.L788
	subl	$97, %eax
	subl	$87, %esi
	sall	$12, %esi
	cmpb	$6, %al
	movl	$0, %eax
	cmovnb	%eax, %esi
.L731:
	movzbl	3(%rbx), %edx
	leal	-48(%rdx), %ecx
	movl	%edx, %eax
	cmpb	$9, %cl
	jbe	.L789
	leal	-65(%rdx), %ecx
	cmpb	$5, %cl
	jbe	.L790
	subl	$97, %eax
	cmpb	$5, %al
	ja	.L734
	subl	$87, %edx
	sall	$8, %edx
	addl	%edx, %esi
	.p2align 4,,10
	.p2align 3
.L734:
	movzbl	4(%rbx), %edx
	leal	-48(%rdx), %ecx
	movl	%edx, %eax
	cmpb	$9, %cl
	jbe	.L791
	leal	-65(%rdx), %ecx
	cmpb	$5, %cl
	jbe	.L792
	subl	$97, %eax
	cmpb	$5, %al
	ja	.L737
	subl	$87, %edx
	sall	$4, %edx
	addl	%edx, %esi
	.p2align 4,,10
	.p2align 3
.L737:
	movzbl	5(%rbx), %edx
	leal	-48(%rdx), %ecx
	movl	%edx, %eax
	cmpb	$9, %cl
	jbe	.L793
	leal	-65(%rdx), %ecx
	cmpb	$5, %cl
	jbe	.L794
	subl	$97, %eax
	leal	-87(%rsi,%rdx), %edx
	cmpb	$6, %al
	cmovb	%edx, %esi
.L740:
	leaq	6(%rbx), %rdx
	movzwl	%si, %esi
	jmp	.L728
.L725:
	movl	$9, %esi
	jmp	.L728
.L726:
	movl	$13, %esi
	jmp	.L728
.L727:
	movl	$10, %esi
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L785:
	movq	%r15, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	movq	-136(%rbp), %rax
	movq	%rax, %rbx
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L788:
	subl	$55, %esi
.L781:
	sall	$12, %esi
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L786:
	movq	-88(%rbp), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L743
	cmpq	$1, %rax
	je	.L795
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L743
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	0(%r13), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L743:
	xorl	%ecx, %ecx
	movq	%rax, 8(%r13)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L782:
	leaq	-96(%rbp), %rdi
	leaq	.LC9(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rbx
	movq	0(%r13), %rdi
	cmpq	%rbx, %rdx
	je	.L796
	leaq	16(%r13), %rsi
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rax
	cmpq	%rsi, %rdi
	je	.L797
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	16(%r13), %rsi
	movq	%rdx, 0(%r13)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%r13)
	testq	%rdi, %rdi
	je	.L713
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L711:
	xorl	%esi, %esi
	movq	$0, -88(%rbp)
	movw	%si, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 32(%r13)
	cmpq	%rbx, %rdi
	je	.L714
	call	_ZdlPv@PLT
.L714:
	movl	$1, %eax
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L791:
	subl	$48, %edx
	sall	$4, %edx
	addl	%edx, %esi
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L789:
	subl	$48, %edx
	sall	$8, %edx
	addl	%edx, %esi
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L793:
	leal	-48(%rsi,%rdx), %esi
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L794:
	leal	-55(%rsi,%rdx), %esi
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L790:
	subl	$55, %edx
	sall	$8, %edx
	addl	%edx, %esi
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L792:
	subl	$55, %edx
	sall	$4, %edx
	addl	%edx, %esi
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L797:
	movq	%rax, %xmm0
	movq	%rcx, %xmm4
	movq	%rdx, 0(%r13)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r13)
.L713:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L787:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdx, 0(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
.L747:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L796:
	movq	-88(%rbp), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L709
	cmpq	$1, %rax
	je	.L798
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L709
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	0(%r13), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L709:
	xorl	%r8d, %r8d
	movq	%rax, 8(%r13)
	movw	%r8w, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L795:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	0(%r13), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L743
.L798:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	0(%r13), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L709
.L784:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8840:
	.size	_ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S5_PNS_8String16E.constprop.0, .-_ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S5_PNS_8String16E.constprop.0
	.section	.text._ZNK12v8_inspector8protocol11StringValue5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol11StringValue5cloneEv
	.type	_ZNK12v8_inspector8protocol11StringValue5cloneEv, @function
_ZNK12v8_inspector8protocol11StringValue5cloneEv:
.LFB4970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$56, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	_Znwm@PLT
	movq	16(%r14), %r13
	movq	24(%r14), %rcx
	movq	%rax, %rbx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	(%rcx,%rcx), %r12
	movq	%r13, %rax
	leaq	32(%rbx), %rdi
	addq	%r12, %rax
	movq	%rdi, 16(%rbx)
	je	.L800
	testq	%r13, %r13
	je	.L815
.L800:
	movq	%r12, %r8
	sarq	%r8
	cmpq	$14, %r12
	ja	.L816
.L801:
	cmpq	$2, %r12
	je	.L817
	testq	%r12, %r12
	je	.L804
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	16(%rbx), %rdi
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
.L804:
	xorl	%eax, %eax
	movq	%r8, 24(%rbx)
	movw	%ax, (%rdi,%rcx,2)
	movq	48(%r14), %rax
	movq	%rbx, (%r15)
	movq	%rax, 48(%rbx)
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L817:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	16(%rbx), %rdi
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L816:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r8
	ja	.L818
	leaq	2(%r12), %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	%r8, 32(%rbx)
	jmp	.L801
.L815:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L818:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4970:
	.size	_ZNK12v8_inspector8protocol11StringValue5cloneEv, .-_ZNK12v8_inspector8protocol11StringValue5cloneEv
	.section	.text._ZNK12v8_inspector8protocol15SerializedValue5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15SerializedValue5cloneEv
	.type	_ZNK12v8_inspector8protocol15SerializedValue5cloneEv, @function
_ZNK12v8_inspector8protocol15SerializedValue5cloneEv:
.LFB4977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$80, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_Znwm@PLT
	movq	16(%r12), %r15
	movq	24(%r12), %rcx
	movq	%rax, %rbx
	movl	$8, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol15SerializedValueE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	(%rcx,%rcx), %r14
	movq	%r15, %rax
	leaq	32(%rbx), %rdi
	addq	%r14, %rax
	movq	%rdi, 16(%rbx)
	je	.L820
	testq	%r15, %r15
	je	.L840
.L820:
	movq	%r14, %r8
	sarq	%r8
	cmpq	$14, %r14
	ja	.L841
.L821:
	cmpq	$2, %r14
	je	.L842
	testq	%r14, %r14
	je	.L824
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	16(%rbx), %rdi
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
.L824:
	xorl	%eax, %eax
	movq	%r8, 24(%rbx)
	pxor	%xmm0, %xmm0
	movw	%ax, (%rdi,%rcx,2)
	movq	48(%r12), %rax
	movq	$0, 72(%rbx)
	movq	%rax, 48(%rbx)
	movq	64(%r12), %rax
	subq	56(%r12), %rax
	movups	%xmm0, 56(%rbx)
	movq	%rax, %r14
	je	.L825
	js	.L843
	movq	%rax, %rdi
	call	_Znwm@PLT
	movq	56(%r12), %rsi
	movq	64(%r12), %r12
	movq	%rax, %xmm0
	movq	%rax, %rcx
	leaq	(%rax,%r14), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 72(%rbx)
	movups	%xmm0, 56(%rbx)
	subq	%rsi, %r12
	jne	.L844
.L827:
	addq	%r12, %rcx
	movq	%rbx, 0(%r13)
	movq	%r13, %rax
	movq	%rcx, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L844:
	.cfi_restore_state
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L842:
	movzwl	(%r15), %eax
	movw	%ax, (%rdi)
	movq	16(%rbx), %rdi
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L841:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r8
	ja	.L845
	leaq	2(%r14), %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	%r8, 32(%rbx)
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L825:
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
	jmp	.L827
.L840:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L843:
	call	_ZSt17__throw_bad_allocv@PLT
.L845:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4977:
	.size	_ZNK12v8_inspector8protocol15SerializedValue5cloneEv, .-_ZNK12v8_inspector8protocol15SerializedValue5cloneEv
	.section	.text._ZNK12v8_inspector8protocol11StringValue8asStringEPNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol11StringValue8asStringEPNS_8String16E
	.type	_ZNK12v8_inspector8protocol11StringValue8asStringEPNS_8String16E, @function
_ZNK12v8_inspector8protocol11StringValue8asStringEPNS_8String16E:
.LFB4966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	cmpq	%rax, %rsi
	je	.L847
	movq	(%rsi), %r13
	leaq	16(%rsi), %rdx
	movq	24(%rdi), %r12
	cmpq	%rdx, %r13
	je	.L856
	movq	16(%rsi), %rax
.L848:
	cmpq	%rax, %r12
	ja	.L866
	leaq	(%r12,%r12), %rdx
	testq	%r12, %r12
	je	.L852
.L869:
	movq	16(%r15), %rsi
	cmpq	$1, %r12
	je	.L867
	testq	%rdx, %rdx
	je	.L852
	movq	%r13, %rdi
	call	memmove@PLT
	movq	(%rbx), %r13
	.p2align 4,,10
	.p2align 3
.L852:
	xorl	%eax, %eax
	movq	%r12, 8(%rbx)
	movw	%ax, 0(%r13,%r12,2)
.L847:
	movq	48(%r15), %rax
	movq	%rax, 32(%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L866:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r12
	ja	.L868
	addq	%rax, %rax
	movq	%r12, %r14
	cmpq	%rax, %r12
	jnb	.L851
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r14
.L851:
	leaq	2(%r14,%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdi, %rdx
	je	.L855
	call	_ZdlPv@PLT
.L855:
	movq	%r13, (%rbx)
	leaq	(%r12,%r12), %rdx
	movq	%r14, 16(%rbx)
	testq	%r12, %r12
	je	.L852
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L856:
	movl	$7, %eax
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L867:
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	movq	(%rbx), %r13
	jmp	.L852
.L868:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4966:
	.size	_ZNK12v8_inspector8protocol11StringValue8asStringEPNS_8String16E, .-_ZNK12v8_inspector8protocol11StringValue8asStringEPNS_8String16E
	.section	.text._ZN12v8_inspector8protocol12ErrorSupportC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12ErrorSupportC2Ev
	.type	_ZN12v8_inspector8protocol12ErrorSupportC2Ev, @function
_ZN12v8_inspector8protocol12ErrorSupportC2Ev:
.LFB4898:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE4898:
	.size	_ZN12v8_inspector8protocol12ErrorSupportC2Ev, .-_ZN12v8_inspector8protocol12ErrorSupportC2Ev
	.globl	_ZN12v8_inspector8protocol12ErrorSupportC1Ev
	.set	_ZN12v8_inspector8protocol12ErrorSupportC1Ev,_ZN12v8_inspector8protocol12ErrorSupportC2Ev
	.section	.text._ZN12v8_inspector8protocol12ErrorSupportD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12ErrorSupportD2Ev
	.type	_ZN12v8_inspector8protocol12ErrorSupportD2Ev, @function
_ZN12v8_inspector8protocol12ErrorSupportD2Ev:
.LFB4901:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %r13
	movq	24(%rdi), %r12
	cmpq	%r12, %r13
	je	.L872
	.p2align 4,,10
	.p2align 3
.L876:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L873
	call	_ZdlPv@PLT
	addq	$40, %r12
	cmpq	%r12, %r13
	jne	.L876
.L874:
	movq	24(%rbx), %r12
.L872:
	testq	%r12, %r12
	je	.L877
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L877:
	movq	8(%rbx), %r13
	movq	(%rbx), %r12
	cmpq	%r12, %r13
	je	.L878
	.p2align 4,,10
	.p2align 3
.L882:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L879
	call	_ZdlPv@PLT
	addq	$40, %r12
	cmpq	%r12, %r13
	jne	.L882
.L880:
	movq	(%rbx), %r12
.L878:
	testq	%r12, %r12
	je	.L871
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L879:
	.cfi_restore_state
	addq	$40, %r12
	cmpq	%r12, %r13
	jne	.L882
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L873:
	addq	$40, %r12
	cmpq	%r12, %r13
	jne	.L876
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L871:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4901:
	.size	_ZN12v8_inspector8protocol12ErrorSupportD2Ev, .-_ZN12v8_inspector8protocol12ErrorSupportD2Ev
	.globl	_ZN12v8_inspector8protocol12ErrorSupportD1Ev
	.set	_ZN12v8_inspector8protocol12ErrorSupportD1Ev,_ZN12v8_inspector8protocol12ErrorSupportD2Ev
	.section	.text._ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc
	.type	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc, @function
_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc:
.LFB4903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rbx
	cmpq	%r12, %rbx
	je	.L889
	movq	(%rbx), %r13
	leaq	16(%rbx), %r15
	movq	-88(%rbp), %r12
	cmpq	%r15, %r13
	je	.L900
	movq	16(%rbx), %rax
.L890:
	cmpq	%rax, %r12
	ja	.L909
	leaq	(%r12,%r12), %rdx
	testq	%r12, %r12
	jne	.L910
.L894:
	xorl	%eax, %eax
	movq	%r12, 8(%rbx)
	movw	%ax, 0(%r13,%r12,2)
.L889:
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 32(%rbx)
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L888
	call	_ZdlPv@PLT
.L888:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L911
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L910:
	.cfi_restore_state
	movq	-96(%rbp), %rsi
	cmpq	$1, %r12
	je	.L912
	testq	%rdx, %rdx
	je	.L894
	movq	%r13, %rdi
	call	memmove@PLT
	movq	(%rbx), %r13
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L909:
	movabsq	$2305843009213693951, %rdx
	cmpq	%rdx, %r12
	ja	.L913
	addq	%rax, %rax
	movq	%r12, %r14
	cmpq	%rax, %r12
	jnb	.L893
	cmpq	%rdx, %rax
	cmovbe	%rax, %rdx
	movq	%rdx, %r14
.L893:
	leaq	2(%r14,%r14), %rdi
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r13
	cmpq	%rdi, %r15
	je	.L898
	call	_ZdlPv@PLT
.L898:
	movq	%r13, (%rbx)
	leaq	(%r12,%r12), %rdx
	movq	%r14, 16(%rbx)
	testq	%r12, %r12
	je	.L894
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L900:
	movl	$7, %eax
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L912:
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	movq	(%rbx), %r13
	jmp	.L894
.L911:
	call	__stack_chk_fail@PLT
.L913:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4903:
	.size	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc, .-_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc
	.section	.text._ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E
	.type	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E, @function
_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E:
.LFB4904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	leaq	-40(%rax), %rbx
	cmpq	%rbx, %rsi
	je	.L915
	movq	(%rbx), %r14
	leaq	16(%rbx), %rdx
	movq	8(%rsi), %r13
	cmpq	%rdx, %r14
	je	.L924
	movq	16(%rbx), %rax
.L916:
	cmpq	%rax, %r13
	ja	.L934
	leaq	(%r13,%r13), %rdx
	testq	%r13, %r13
	je	.L920
.L937:
	movq	(%r12), %rsi
	cmpq	$1, %r13
	je	.L935
	testq	%rdx, %rdx
	je	.L920
	movq	%r14, %rdi
	call	memmove@PLT
	movq	(%rbx), %r14
	.p2align 4,,10
	.p2align 3
.L920:
	xorl	%eax, %eax
	movq	%r13, 8(%rbx)
	movw	%ax, (%r14,%r13,2)
.L915:
	movq	32(%r12), %rax
	movq	%rax, 32(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L934:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r13
	ja	.L936
	addq	%rax, %rax
	movq	%r13, %r15
	cmpq	%rax, %r13
	jnb	.L919
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r15
.L919:
	leaq	2(%r15,%r15), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r14
	cmpq	%rdi, %rdx
	je	.L923
	call	_ZdlPv@PLT
.L923:
	movq	%r14, (%rbx)
	leaq	(%r13,%r13), %rdx
	movq	%r15, 16(%rbx)
	testq	%r13, %r13
	je	.L920
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L924:
	movl	$7, %eax
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L935:
	movzwl	(%rsi), %eax
	movw	%ax, (%r14)
	movq	(%rbx), %r14
	jmp	.L920
.L936:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4904:
	.size	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E, .-_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E
	.section	.text._ZN12v8_inspector8protocol12ErrorSupport3popEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12ErrorSupport3popEv
	.type	_ZN12v8_inspector8protocol12ErrorSupport3popEv, @function
_ZN12v8_inspector8protocol12ErrorSupport3popEv:
.LFB4907:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	leaq	-40(%rax), %rdx
	subq	$24, %rax
	movq	%rdx, 8(%rdi)
	movq	-16(%rax), %rdi
	cmpq	%rax, %rdi
	je	.L938
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L938:
	ret
	.cfi_endproc
.LFE4907:
	.size	_ZN12v8_inspector8protocol12ErrorSupport3popEv, .-_ZN12v8_inspector8protocol12ErrorSupport3popEv
	.section	.text._ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv
	.type	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv, @function
_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv:
.LFB4913:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	cmpq	%rax, 24(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE4913:
	.size	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv, .-_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv
	.section	.rodata._ZN12v8_inspector8protocol12ErrorSupport6errorsEv.str1.1,"aMS",@progbits,1
.LC10:
	.string	"; "
	.section	.text._ZN12v8_inspector8protocol12ErrorSupport6errorsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12ErrorSupport6errorsEv
	.type	_ZN12v8_inspector8protocol12ErrorSupport6errorsEv, @function
_ZN12v8_inspector8protocol12ErrorSupport6errorsEv:
.LFB4914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movabsq	$-3689348814741910323, %rbx
	subq	$104, %rsp
	movq	%rdi, -136(%rbp)
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movq	24(%r15), %rdx
	cmpq	%rdx, 32(%r15)
	je	.L948
	.p2align 4,,10
	.p2align 3
.L942:
	leaq	(%r14,%r14,4), %rax
	movq	%r12, %rdi
	addq	$1, %r14
	leaq	(%rdx,%rax,8), %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	24(%r15), %rdx
	movq	32(%r15), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rbx, %rax
	cmpq	%rax, %r14
	jnb	.L948
	testq	%r14, %r14
	je	.L942
	leaq	-96(%rbp), %r13
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L946
	call	_ZdlPv@PLT
.L946:
	movq	24(%r15), %rdx
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L948:
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L941
	call	_ZdlPv@PLT
.L941:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L956
	movq	-136(%rbp), %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L956:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4914:
	.size	_ZN12v8_inspector8protocol12ErrorSupport6errorsEv, .-_ZN12v8_inspector8protocol12ErrorSupport6errorsEv
	.section	.text._ZN12v8_inspector8protocolL21reportProtocolErrorToEPNS0_15FrontendChannelEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE.part.0,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector8protocolL21reportProtocolErrorToEPNS0_15FrontendChannelEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE.part.0, @function
_ZN12v8_inspector8protocolL21reportProtocolErrorToEPNS0_15FrontendChannelEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE.part.0:
.LFB8694:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	%edx, -124(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$104, %edi
	movq	16(%rax), %rax
	movq	%rax, -120(%rbp)
	call	_Znwm@PLT
	movq	(%r12), %rsi
	movl	-124(%rbp), %edx
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movq	8(%r12), %rax
	movl	%edx, 8(%rbx)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r12), %rax
	xorl	%esi, %esi
	leaq	72(%rbx), %r12
	movq	%r12, 56(%rbx)
	movq	%rax, 48(%rbx)
	movq	$0, 64(%rbx)
	movw	%si, 72(%rbx)
	movq	$0, 88(%rbx)
	movl	%r15d, 96(%rbx)
	movb	$1, 100(%rbx)
	testq	%r13, %r13
	je	.L958
	movq	32(%r13), %rax
	cmpq	%rax, 24(%r13)
	je	.L958
	leaq	-96(%rbp), %rdi
	movq	%r13, %rsi
	leaq	-80(%rbp), %r13
	call	_ZN12v8_inspector8protocol12ErrorSupport6errorsEv
	movq	-96(%rbp), %rdx
	movq	56(%rbx), %rdi
	cmpq	%r13, %rdx
	je	.L983
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rax
	cmpq	%rdi, %r12
	je	.L984
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	72(%rbx), %rsi
	movq	%rdx, 56(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	testq	%rdi, %rdi
	je	.L964
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L962:
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movw	%ax, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 88(%rbx)
	cmpq	%r13, %rdi
	je	.L958
	call	_ZdlPv@PLT
.L958:
	movq	%r14, %rdi
	movq	%rbx, -104(%rbp)
	movq	-120(%rbp), %rax
	leaq	-104(%rbp), %rdx
	movl	%r15d, %esi
	call	*%rax
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L957
	movq	(%rdi), %rax
	call	*24(%rax)
.L957:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L985
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L984:
	.cfi_restore_state
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, 56(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 64(%rbx)
.L964:
	movq	%r13, -96(%rbp)
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L983:
	movq	-88(%rbp), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L960
	cmpq	$1, %rax
	je	.L986
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L960
	movq	%r13, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	56(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L960:
	xorl	%ecx, %ecx
	movq	%rax, 64(%rbx)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L986:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	56(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L960
.L985:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8694:
	.size	_ZN12v8_inspector8protocolL21reportProtocolErrorToEPNS0_15FrontendChannelEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE.part.0, .-_ZN12v8_inspector8protocolL21reportProtocolErrorToEPNS0_15FrontendChannelEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE.part.0
	.section	.text._ZNK12v8_inspector8protocol5Value12toJSONStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol5Value12toJSONStringEv
	.type	_ZNK12v8_inspector8protocol5Value12toJSONStringEv, @function
_ZNK12v8_inspector8protocol5Value12toJSONStringEv:
.LFB4957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movl	$512, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder15reserveCapacityEm@PLT
	movq	0(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*72(%rax)
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L987
	call	_ZdlPv@PLT
.L987:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L994
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L994:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4957:
	.size	_ZNK12v8_inspector8protocol5Value12toJSONStringEv, .-_ZNK12v8_inspector8protocol5Value12toJSONStringEv
	.section	.text._ZN12v8_inspector8protocol15DictionaryValueC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol15DictionaryValueC2Ev
	.type	_ZN12v8_inspector8protocol15DictionaryValueC2Ev, @function
_ZN12v8_inspector8protocol15DictionaryValueC2Ev:
.LFB5040:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector8protocol15DictionaryValueE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$6, 8(%rdi)
	movq	%rax, (%rdi)
	leaq	64(%rdi), %rax
	movq	%rax, 16(%rdi)
	movq	$1, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movl	$0x3f800000, 48(%rdi)
	movq	$0, 56(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 80(%rdi)
	ret
	.cfi_endproc
.LFE5040:
	.size	_ZN12v8_inspector8protocol15DictionaryValueC2Ev, .-_ZN12v8_inspector8protocol15DictionaryValueC2Ev
	.globl	_ZN12v8_inspector8protocol15DictionaryValueC1Ev
	.set	_ZN12v8_inspector8protocol15DictionaryValueC1Ev,_ZN12v8_inspector8protocol15DictionaryValueC2Ev
	.section	.text._ZN12v8_inspector8protocol9ListValueC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol9ListValueC2Ev
	.type	_ZN12v8_inspector8protocol9ListValueC2Ev, @function
_ZN12v8_inspector8protocol9ListValueC2Ev:
.LFB5060:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector8protocol9ListValueE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$7, 8(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 32(%rdi)
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE5060:
	.size	_ZN12v8_inspector8protocol9ListValueC2Ev, .-_ZN12v8_inspector8protocol9ListValueC2Ev
	.globl	_ZN12v8_inspector8protocol9ListValueC1Ev
	.set	_ZN12v8_inspector8protocol9ListValueC1Ev,_ZN12v8_inspector8protocol9ListValueC2Ev
	.section	.text._ZN12v8_inspector8protocol9ListValue2atEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol9ListValue2atEm
	.type	_ZN12v8_inspector8protocol9ListValue2atEm, @function
_ZN12v8_inspector8protocol9ListValue2atEm:
.LFB5063:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE5063:
	.size	_ZN12v8_inspector8protocol9ListValue2atEm, .-_ZN12v8_inspector8protocol9ListValue2atEm
	.section	.rodata._ZN12v8_inspector8protocol24escapeLatinStringForJSONEPKhjPNS_15String16BuilderE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"\\b"
.LC13:
	.string	"\\f"
.LC14:
	.string	"\\n"
.LC15:
	.string	"\\r"
.LC16:
	.string	"\\t"
.LC17:
	.string	"\\\\"
.LC18:
	.string	"\\\""
.LC19:
	.string	"\\u"
	.section	.text._ZN12v8_inspector8protocol24escapeLatinStringForJSONEPKhjPNS_15String16BuilderE,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector8protocol24escapeLatinStringForJSONEPKhjPNS_15String16BuilderE
	.type	_ZN12v8_inspector8protocol24escapeLatinStringForJSONEPKhjPNS_15String16BuilderE, @function
_ZN12v8_inspector8protocol24escapeLatinStringForJSONEPKhjPNS_15String16BuilderE:
.LFB5064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.L998
	movq	%rdi, %r13
	leal	-1(%rsi), %eax
	leaq	-96(%rbp), %rbx
	movq	%rdx, %r12
	movzbl	0(%r13), %r15d
	addq	%rdi, %rax
	leaq	-80(%rbp), %r14
	movq	%rax, -112(%rbp)
	cmpb	$34, %r15b
	ja	.L1000
	.p2align 4,,10
	.p2align 3
.L1042:
	cmpb	$7, %r15b
	jbe	.L1001
	leal	-8(%r15), %edx
	cmpb	$26, %dl
	ja	.L1001
	leaq	.L1003(%rip), %rax
	movzbl	%dl, %edx
	movslq	(%rax,%rdx,4), %rdx
	addq	%rax, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN12v8_inspector8protocol24escapeLatinStringForJSONEPKhjPNS_15String16BuilderE,"a",@progbits
	.align 4
	.align 4
.L1003:
	.long	.L1008-.L1003
	.long	.L1007-.L1003
	.long	.L1006-.L1003
	.long	.L1001-.L1003
	.long	.L1005-.L1003
	.long	.L1004-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1001-.L1003
	.long	.L1002-.L1003
	.section	.text._ZN12v8_inspector8protocol24escapeLatinStringForJSONEPKhjPNS_15String16BuilderE
.L1002:
	leaq	.LC18(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L1036:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1013
	call	_ZdlPv@PLT
.L1013:
	leaq	1(%r13), %rax
	cmpq	%r13, -112(%rbp)
	je	.L998
.L1024:
	movq	%rax, %r13
	movzbl	0(%r13), %r15d
	cmpb	$34, %r15b
	jbe	.L1042
.L1000:
	leaq	.LC17(%rip), %rsi
	cmpb	$92, %r15b
	je	.L1036
.L1001:
	leal	-32(%r15), %edx
	cmpb	$94, %dl
	jbe	.L1043
	leaq	.LC19(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1020
	call	_ZdlPv@PLT
.L1020:
	movzbl	%r15b, %eax
	movl	$4, %r15d
.L1021:
	movl	%eax, %ecx
	movl	%eax, -100(%rbp)
	leaq	_ZN12v8_inspector8protocol12_GLOBAL__N_1L9hexDigitsE(%rip), %rax
	movq	%r12, %rdi
	shrw	$12, %cx
	andl	$15, %ecx
	movsbw	(%rax,%rcx), %si
	movzwl	%si, %esi
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	movl	-100(%rbp), %eax
	sall	$4, %eax
	subq	$1, %r15
	jne	.L1021
	leaq	1(%r13), %rax
	cmpq	%r13, -112(%rbp)
	jne	.L1024
.L998:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1044
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1004:
	.cfi_restore_state
	leaq	.LC15(%rip), %rsi
	jmp	.L1036
.L1005:
	leaq	.LC13(%rip), %rsi
	jmp	.L1036
.L1006:
	leaq	.LC14(%rip), %rsi
	jmp	.L1036
.L1007:
	leaq	.LC16(%rip), %rsi
	jmp	.L1036
.L1008:
	leaq	.LC12(%rip), %rsi
	jmp	.L1036
	.p2align 4,,10
	.p2align 3
.L1043:
	movzbl	%r15b, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	jmp	.L1013
.L1044:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5064:
	.size	_ZN12v8_inspector8protocol24escapeLatinStringForJSONEPKhjPNS_15String16BuilderE, .-_ZN12v8_inspector8protocol24escapeLatinStringForJSONEPKhjPNS_15String16BuilderE
	.section	.text._ZN12v8_inspector8protocol23escapeWideStringForJSONEPKtjPNS_15String16BuilderE,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector8protocol23escapeWideStringForJSONEPKtjPNS_15String16BuilderE
	.type	_ZN12v8_inspector8protocol23escapeWideStringForJSONEPKtjPNS_15String16BuilderE, @function
_ZN12v8_inspector8protocol23escapeWideStringForJSONEPKtjPNS_15String16BuilderE:
.LFB5065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.L1045
	leal	-1(%rsi), %eax
	movq	%rdx, %r12
	leaq	-96(%rbp), %rbx
	movq	%rdi, %r13
	leaq	2(%rdi,%rax,2), %rax
	leaq	-80(%rbp), %r14
	movq	%rax, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L1069:
	movzwl	0(%r13), %r15d
	cmpw	$34, %r15w
	ja	.L1047
	cmpw	$7, %r15w
	jbe	.L1048
	leal	-8(%r15), %edx
	cmpw	$26, %dx
	ja	.L1048
	leaq	.L1050(%rip), %rax
	movzwl	%dx, %edx
	movslq	(%rax,%rdx,4), %rdx
	addq	%rax, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN12v8_inspector8protocol23escapeWideStringForJSONEPKtjPNS_15String16BuilderE,"a",@progbits
	.align 4
	.align 4
.L1050:
	.long	.L1055-.L1050
	.long	.L1054-.L1050
	.long	.L1053-.L1050
	.long	.L1048-.L1050
	.long	.L1052-.L1050
	.long	.L1051-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1048-.L1050
	.long	.L1049-.L1050
	.section	.text._ZN12v8_inspector8protocol23escapeWideStringForJSONEPKtjPNS_15String16BuilderE
.L1049:
	leaq	.LC18(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L1083:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1060
	call	_ZdlPv@PLT
.L1060:
	addq	$2, %r13
	cmpq	-112(%rbp), %r13
	jne	.L1069
.L1045:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1089
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1051:
	.cfi_restore_state
	leaq	.LC15(%rip), %rsi
	jmp	.L1083
.L1055:
	leaq	.LC12(%rip), %rsi
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1047:
	leaq	.LC17(%rip), %rsi
	cmpw	$92, %r15w
	je	.L1083
.L1048:
	leal	-32(%r15), %edx
	cmpw	$94, %dx
	jbe	.L1090
	leaq	.LC19(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1067
	call	_ZdlPv@PLT
.L1067:
	movl	$4, %edx
.L1068:
	movl	%r15d, %ecx
	movq	%r12, %rdi
	movq	%rdx, -104(%rbp)
	sall	$4, %r15d
	shrw	$12, %cx
	leaq	_ZN12v8_inspector8protocol12_GLOBAL__N_1L9hexDigitsE(%rip), %rax
	andl	$15, %ecx
	movsbw	(%rax,%rcx), %si
	movzwl	%si, %esi
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	movq	-104(%rbp), %rdx
	subq	$1, %rdx
	jne	.L1068
	jmp	.L1060
.L1053:
	leaq	.LC14(%rip), %rsi
	jmp	.L1083
.L1054:
	leaq	.LC16(%rip), %rsi
	jmp	.L1083
.L1052:
	leaq	.LC13(%rip), %rsi
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1090:
	movzwl	%r15w, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	jmp	.L1060
.L1089:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5065:
	.size	_ZN12v8_inspector8protocol23escapeWideStringForJSONEPKtjPNS_15String16BuilderE, .-_ZN12v8_inspector8protocol23escapeWideStringForJSONEPKtjPNS_15String16BuilderE
	.section	.text._ZNK12v8_inspector8protocol6Object7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol6Object7toValueEv
	.type	_ZNK12v8_inspector8protocol6Object7toValueEv, @function
_ZNK12v8_inspector8protocol6Object7toValueEv:
.LFB5069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	call	*88(%rax)
	movq	-32(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1092
	cmpl	$6, 8(%rdx)
	movl	$0, %eax
	cmovne	%rax, %rdx
.L1092:
	movq	%rdx, (%r12)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1099
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1099:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5069:
	.size	_ZNK12v8_inspector8protocol6Object7toValueEv, .-_ZNK12v8_inspector8protocol6Object7toValueEv
	.section	.text._ZNK12v8_inspector8protocol6Object5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol6Object5cloneEv
	.type	_ZNK12v8_inspector8protocol6Object5cloneEv, @function
_ZNK12v8_inspector8protocol6Object5cloneEv:
.LFB5070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	leaq	-32(%rbp), %rdi
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	call	*88(%rax)
	movq	-32(%rbp), %rbx
	movq	$0, -32(%rbp)
	testq	%rbx, %rbx
	je	.L1101
	cmpl	$6, 8(%rbx)
	movl	$0, %eax
	cmovne	%rax, %rbx
.L1101:
	movl	$8, %edi
	call	_Znwm@PLT
	movq	-32(%rbp), %rdi
	movq	%rbx, (%rax)
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L1100
	movq	(%rdi), %rax
	call	*24(%rax)
.L1100:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1112
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1112:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5070:
	.size	_ZNK12v8_inspector8protocol6Object5cloneEv, .-_ZNK12v8_inspector8protocol6Object5cloneEv
	.section	.text._ZN12v8_inspector8protocol6ObjectC2ESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol6ObjectC2ESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS3_EE
	.type	_ZN12v8_inspector8protocol6ObjectC2ESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS3_EE, @function
_ZN12v8_inspector8protocol6ObjectC2ESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS3_EE:
.LFB5072:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE5072:
	.size	_ZN12v8_inspector8protocol6ObjectC2ESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS3_EE, .-_ZN12v8_inspector8protocol6ObjectC2ESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS3_EE
	.globl	_ZN12v8_inspector8protocol6ObjectC1ESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS3_EE
	.set	_ZN12v8_inspector8protocol6ObjectC1ESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS3_EE,_ZN12v8_inspector8protocol6ObjectC2ESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS3_EE
	.section	.text._ZN12v8_inspector8protocol6ObjectD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol6ObjectD2Ev
	.type	_ZN12v8_inspector8protocol6ObjectD2Ev, @function
_ZN12v8_inspector8protocol6ObjectD2Ev:
.LFB5075:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1114
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L1114:
	ret
	.cfi_endproc
.LFE5075:
	.size	_ZN12v8_inspector8protocol6ObjectD2Ev, .-_ZN12v8_inspector8protocol6ObjectD2Ev
	.globl	_ZN12v8_inspector8protocol6ObjectD1Ev
	.set	_ZN12v8_inspector8protocol6ObjectD1Ev,_ZN12v8_inspector8protocol6ObjectD2Ev
	.section	.text._ZN12v8_inspector8protocol16DispatchResponse2OKEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol16DispatchResponse2OKEv
	.type	_ZN12v8_inspector8protocol16DispatchResponse2OKEv, @function
_ZN12v8_inspector8protocol16DispatchResponse2OKEv:
.LFB5077:
	.cfi_startproc
	endbr64
	leaq	24(%rdi), %rdx
	movl	$0, (%rdi)
	movq	%rdi, %rax
	movq	%rdx, 8(%rdi)
	xorl	%edx, %edx
	movq	$0, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$0, 40(%rdi)
	movl	$-32700, 48(%rdi)
	ret
	.cfi_endproc
.LFE5077:
	.size	_ZN12v8_inspector8protocol16DispatchResponse2OKEv, .-_ZN12v8_inspector8protocol16DispatchResponse2OKEv
	.section	.text._ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E
	.type	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E, @function
_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E:
.LFB5087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	24(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%r14, 8(%rdi)
	movq	$0, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$0, 40(%rdi)
	movl	$1, (%rdi)
	movl	$-32000, 48(%rdi)
	cmpq	%rax, %rsi
	je	.L1118
	movq	8(%rsi), %r13
	cmpq	$7, %r13
	ja	.L1129
	leaq	(%r13,%r13), %r15
	testq	%r13, %r13
	jne	.L1130
.L1123:
	xorl	%eax, %eax
	movq	%r13, 16(%r12)
	movw	%ax, (%r14,%r15)
.L1118:
	movq	32(%rbx), %rax
	movq	%rax, 40(%r12)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1129:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r13
	ja	.L1131
	cmpq	$14, %r13
	movl	$14, %r15d
	cmovnb	%r13, %r15
	leaq	2(%r15,%r15), %rdi
	call	_Znwm@PLT
	movq	8(%r12), %rdi
	cmpq	%rdi, %r14
	je	.L1121
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L1121:
	movq	%r15, 24(%r12)
	leaq	(%r13,%r13), %r15
	movq	%rax, 8(%r12)
	movq	(%rbx), %rsi
.L1122:
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	memmove@PLT
	movq	8(%r12), %r14
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1130:
	movq	(%rsi), %rsi
	cmpq	$1, %r13
	jne	.L1124
	movzwl	(%rsi), %eax
	movl	$2, %r15d
	movw	%ax, 24(%rdi)
	jmp	.L1123
.L1124:
	movq	%r14, %rax
	jmp	.L1122
.L1131:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5087:
	.size	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E, .-_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E
	.section	.rodata._ZN12v8_inspector8protocol16DispatchResponse13InternalErrorEv.str1.1,"aMS",@progbits,1
.LC20:
	.string	"Internal error"
	.section	.text._ZN12v8_inspector8protocol16DispatchResponse13InternalErrorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol16DispatchResponse13InternalErrorEv
	.type	_ZN12v8_inspector8protocol16DispatchResponse13InternalErrorEv, @function
_ZN12v8_inspector8protocol16DispatchResponse13InternalErrorEv:
.LFB5088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	24(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-64(%rbp), %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movw	%si, 24(%rdi)
	leaq	.LC20(%rip), %rsi
	movq	%r13, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 40(%rdi)
	movl	$1, (%rdi)
	movl	$-32603, 48(%rdi)
	leaq	-80(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-80(%rbp), %rdx
	movq	8(%r12), %rdi
	movq	-72(%rbp), %rax
	cmpq	%rbx, %rdx
	je	.L1150
	movq	-64(%rbp), %rcx
	cmpq	%rdi, %r13
	je	.L1151
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	24(%r12), %rsi
	movq	%rdx, 8(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%r12)
	testq	%rdi, %rdi
	je	.L1138
	movq	%rdi, -80(%rbp)
	movq	%rsi, -64(%rbp)
.L1136:
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	movw	%ax, (%rdi)
	movq	-48(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	%rax, 40(%r12)
	cmpq	%rbx, %rdi
	je	.L1132
	call	_ZdlPv@PLT
.L1132:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1152
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1151:
	.cfi_restore_state
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, 8(%r12)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 16(%r12)
.L1138:
	movq	%rbx, -80(%rbp)
	leaq	-64(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1150:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L1134
	cmpq	$1, %rax
	je	.L1153
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L1134
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-72(%rbp), %rax
	movq	8(%r12), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L1134:
	xorl	%ecx, %ecx
	movq	%rax, 16(%r12)
	movw	%cx, (%rdi,%rdx)
	movq	-80(%rbp), %rdi
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1153:
	movzwl	-64(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-72(%rbp), %rax
	movq	8(%r12), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L1134
.L1152:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5088:
	.size	_ZN12v8_inspector8protocol16DispatchResponse13InternalErrorEv, .-_ZN12v8_inspector8protocol16DispatchResponse13InternalErrorEv
	.section	.text._ZN12v8_inspector8protocol16DispatchResponse13InvalidParamsERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol16DispatchResponse13InvalidParamsERKNS_8String16E
	.type	_ZN12v8_inspector8protocol16DispatchResponse13InvalidParamsERKNS_8String16E, @function
_ZN12v8_inspector8protocol16DispatchResponse13InvalidParamsERKNS_8String16E:
.LFB5089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	24(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%r14, 8(%rdi)
	movq	$0, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$0, 40(%rdi)
	movl	$1, (%rdi)
	movl	$-32602, 48(%rdi)
	cmpq	%rax, %rsi
	je	.L1155
	movq	8(%rsi), %r13
	cmpq	$7, %r13
	ja	.L1166
	leaq	(%r13,%r13), %r15
	testq	%r13, %r13
	jne	.L1167
.L1160:
	xorl	%eax, %eax
	movq	%r13, 16(%r12)
	movw	%ax, (%r14,%r15)
.L1155:
	movq	32(%rbx), %rax
	movq	%rax, 40(%r12)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1166:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r13
	ja	.L1168
	cmpq	$14, %r13
	movl	$14, %r15d
	cmovnb	%r13, %r15
	leaq	2(%r15,%r15), %rdi
	call	_Znwm@PLT
	movq	8(%r12), %rdi
	cmpq	%rdi, %r14
	je	.L1158
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L1158:
	movq	%r15, 24(%r12)
	leaq	(%r13,%r13), %r15
	movq	%rax, 8(%r12)
	movq	(%rbx), %rsi
.L1159:
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	memmove@PLT
	movq	8(%r12), %r14
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1167:
	movq	(%rsi), %rsi
	cmpq	$1, %r13
	jne	.L1161
	movzwl	(%rsi), %eax
	movl	$2, %r15d
	movw	%ax, 24(%rdi)
	jmp	.L1160
.L1161:
	movq	%r14, %rax
	jmp	.L1159
.L1168:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5089:
	.size	_ZN12v8_inspector8protocol16DispatchResponse13InvalidParamsERKNS_8String16E, .-_ZN12v8_inspector8protocol16DispatchResponse13InvalidParamsERKNS_8String16E
	.section	.text._ZN12v8_inspector8protocol16DispatchResponse11FallThroughEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol16DispatchResponse11FallThroughEv
	.type	_ZN12v8_inspector8protocol16DispatchResponse11FallThroughEv, @function
_ZN12v8_inspector8protocol16DispatchResponse11FallThroughEv:
.LFB5090:
	.cfi_startproc
	endbr64
	leaq	24(%rdi), %rdx
	movl	$2, (%rdi)
	movq	%rdi, %rax
	movq	%rdx, 8(%rdi)
	xorl	%edx, %edx
	movq	$0, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$0, 40(%rdi)
	movl	$-32700, 48(%rdi)
	ret
	.cfi_endproc
.LFE5090:
	.size	_ZN12v8_inspector8protocol16DispatchResponse11FallThroughEv, .-_ZN12v8_inspector8protocol16DispatchResponse11FallThroughEv
	.section	.text._ZN12v8_inspector8protocol14DispatcherBase7WeakPtrC2EPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrC2EPS1_
	.type	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrC2EPS1_, @function
_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrC2EPS1_:
.LFB5092:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE5092:
	.size	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrC2EPS1_, .-_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrC2EPS1_
	.globl	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrC1EPS1_
	.set	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrC1EPS1_,_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrC2EPS1_
	.section	.text._ZN12v8_inspector8protocol14DispatcherBase8CallbackC2ESt10unique_ptrINS1_7WeakPtrESt14default_deleteIS4_EEiRKNS_8String16ERKNS0_15ProtocolMessageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14DispatcherBase8CallbackC2ESt10unique_ptrINS1_7WeakPtrESt14default_deleteIS4_EEiRKNS_8String16ERKNS0_15ProtocolMessageE
	.type	_ZN12v8_inspector8protocol14DispatcherBase8CallbackC2ESt10unique_ptrINS1_7WeakPtrESt14default_deleteIS4_EEiRKNS_8String16ERKNS0_15ProtocolMessageE, @function
_ZN12v8_inspector8protocol14DispatcherBase8CallbackC2ESt10unique_ptrINS1_7WeakPtrESt14default_deleteIS4_EEiRKNS_8String16ERKNS0_15ProtocolMessageE:
.LFB5102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol14DispatcherBase8CallbackE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$24, %rsp
	movq	%rax, -40(%rdi)
	movq	(%rsi), %rax
	movl	%edx, -24(%rdi)
	movq	%rax, -32(%rdi)
	movq	$0, (%rsi)
	movq	%rdi, 24(%rbx)
	movq	(%rcx), %r15
	movq	8(%rcx), %rcx
	movq	%r15, %rax
	leaq	(%rcx,%rcx), %r13
	addq	%r13, %rax
	je	.L1172
	testq	%r15, %r15
	je	.L1177
.L1172:
	movq	%r13, %r8
	sarq	%r8
	cmpq	$14, %r13
	ja	.L1206
.L1173:
	cmpq	$2, %r13
	je	.L1207
	testq	%r13, %r13
	je	.L1176
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
.L1176:
	xorl	%edx, %edx
	movq	%r8, 32(%rbx)
	movw	%dx, (%rdi,%rcx,2)
	movq	32(%r14), %rax
	leaq	80(%rbx), %rdi
	movq	%rdi, 64(%rbx)
	movq	%rax, 56(%rbx)
	movq	(%r12), %r14
	movq	8(%r12), %r15
	movq	%r14, %rax
	leaq	(%r15,%r15), %r13
	addq	%r13, %rax
	je	.L1188
	testq	%r14, %r14
	je	.L1177
.L1188:
	movq	%r13, %rcx
	sarq	%rcx
	cmpq	$14, %r13
	ja	.L1208
.L1179:
	cmpq	$2, %r13
	je	.L1209
	testq	%r13, %r13
	je	.L1182
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	64(%rbx), %rdi
	movq	-56(%rbp), %rcx
.L1182:
	xorl	%eax, %eax
	movq	%rcx, 72(%rbx)
	pxor	%xmm0, %xmm0
	movw	%ax, (%rdi,%r15,2)
	movq	32(%r12), %rax
	movq	%rax, 96(%rbx)
	movq	48(%r12), %r13
	subq	40(%r12), %r13
	movq	$0, 120(%rbx)
	movups	%xmm0, 104(%rbx)
	je	.L1186
	js	.L1210
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rcx
.L1183:
	movq	%rcx, %xmm0
	addq	%rcx, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 120(%rbx)
	movups	%xmm0, 104(%rbx)
	movq	40(%r12), %rsi
	movq	48(%r12), %rax
	xorl	%r12d, %r12d
	subq	%rsi, %rax
	jne	.L1211
.L1185:
	addq	%r12, %rcx
	movq	%rcx, 112(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1207:
	.cfi_restore_state
	movzwl	(%r15), %eax
	movw	%ax, (%rdi)
	movq	24(%rbx), %rdi
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1209:
	movzwl	(%r14), %eax
	movw	%ax, (%rdi)
	movq	64(%rbx), %rdi
	jmp	.L1182
	.p2align 4,,10
	.p2align 3
.L1206:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r8
	ja	.L1180
	leaq	2(%r13), %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	movq	%rax, 24(%rbx)
	movq	%rax, %rdi
	movq	%r8, 40(%rbx)
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1208:
	movabsq	$2305843009213693951, %rax
	movq	%rcx, -56(%rbp)
	cmpq	%rax, %rcx
	ja	.L1180
	leaq	2(%r13), %rdi
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, 64(%rbx)
	movq	%rax, %rdi
	movq	%rcx, 80(%rbx)
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1211:
	movq	%rcx, %rdi
	movq	%rax, %rdx
	movq	%rax, %r12
	call	memmove@PLT
	movq	%rax, %rcx
	jmp	.L1185
	.p2align 4,,10
	.p2align 3
.L1186:
	xorl	%ecx, %ecx
	jmp	.L1183
.L1177:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1180:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1210:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE5102:
	.size	_ZN12v8_inspector8protocol14DispatcherBase8CallbackC2ESt10unique_ptrINS1_7WeakPtrESt14default_deleteIS4_EEiRKNS_8String16ERKNS0_15ProtocolMessageE, .-_ZN12v8_inspector8protocol14DispatcherBase8CallbackC2ESt10unique_ptrINS1_7WeakPtrESt14default_deleteIS4_EEiRKNS_8String16ERKNS0_15ProtocolMessageE
	.globl	_ZN12v8_inspector8protocol14DispatcherBase8CallbackC1ESt10unique_ptrINS1_7WeakPtrESt14default_deleteIS4_EEiRKNS_8String16ERKNS0_15ProtocolMessageE
	.set	_ZN12v8_inspector8protocol14DispatcherBase8CallbackC1ESt10unique_ptrINS1_7WeakPtrESt14default_deleteIS4_EEiRKNS_8String16ERKNS0_15ProtocolMessageE,_ZN12v8_inspector8protocol14DispatcherBase8CallbackC2ESt10unique_ptrINS1_7WeakPtrESt14default_deleteIS4_EEiRKNS_8String16ERKNS0_15ProtocolMessageE
	.section	.text._ZN12v8_inspector8protocol14DispatcherBaseC2EPNS0_15FrontendChannelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14DispatcherBaseC2EPNS0_15FrontendChannelE
	.type	_ZN12v8_inspector8protocol14DispatcherBaseC2EPNS0_15FrontendChannelE, @function
_ZN12v8_inspector8protocol14DispatcherBaseC2EPNS0_15FrontendChannelE:
.LFB5133:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector8protocol14DispatcherBaseE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	leaq	64(%rdi), %rax
	movq	%rax, 16(%rdi)
	movq	$1, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movl	$0x3f800000, 48(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	ret
	.cfi_endproc
.LFE5133:
	.size	_ZN12v8_inspector8protocol14DispatcherBaseC2EPNS0_15FrontendChannelE, .-_ZN12v8_inspector8protocol14DispatcherBaseC2EPNS0_15FrontendChannelE
	.globl	_ZN12v8_inspector8protocol14DispatcherBaseC1EPNS0_15FrontendChannelE
	.set	_ZN12v8_inspector8protocol14DispatcherBaseC1EPNS0_15FrontendChannelE,_ZN12v8_inspector8protocol14DispatcherBaseC2EPNS0_15FrontendChannelE
	.section	.text._ZN12v8_inspector8protocol14DispatcherBaseD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev
	.type	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev, @function
_ZN12v8_inspector8protocol14DispatcherBaseD2Ev:
.LFB5136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol14DispatcherBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	movq	32(%rdi), %rax
	movq	$0, 8(%rdi)
	testq	%rax, %rax
	je	.L1214
	.p2align 4,,10
	.p2align 3
.L1215:
	movq	8(%rax), %rdx
	movq	$0, (%rdx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L1215
	movq	32(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1214
	.p2align 4,,10
	.p2align 3
.L1216:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1216
.L1214:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	addq	$64, %r12
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-40(%r12), %rax
	movq	-48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%r12), %rdi
	movq	$0, -24(%r12)
	movq	$0, -32(%r12)
	cmpq	%r12, %rdi
	je	.L1213
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1213:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5136:
	.size	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev, .-_ZN12v8_inspector8protocol14DispatcherBaseD2Ev
	.globl	_ZN12v8_inspector8protocol14DispatcherBaseD1Ev
	.set	_ZN12v8_inspector8protocol14DispatcherBaseD1Ev,_ZN12v8_inspector8protocol14DispatcherBaseD2Ev
	.section	.text._ZN12v8_inspector8protocol14DispatcherBaseD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14DispatcherBaseD0Ev
	.type	_ZN12v8_inspector8protocol14DispatcherBaseD0Ev, @function
_ZN12v8_inspector8protocol14DispatcherBaseD0Ev:
.LFB5138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol14DispatcherBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	movq	32(%rdi), %rax
	movq	$0, 8(%rdi)
	testq	%rax, %rax
	je	.L1228
	.p2align 4,,10
	.p2align 3
.L1229:
	movq	8(%rax), %rdx
	movq	$0, (%rdx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L1229
	movq	32(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1228
	.p2align 4,,10
	.p2align 3
.L1230:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1230
.L1228:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	16(%r12), %rdi
	leaq	64(%r12), %rax
	movq	$0, 40(%r12)
	movq	$0, 32(%r12)
	cmpq	%rax, %rdi
	je	.L1231
	call	_ZdlPv@PLT
.L1231:
	popq	%rbx
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5138:
	.size	_ZN12v8_inspector8protocol14DispatcherBaseD0Ev, .-_ZN12v8_inspector8protocol14DispatcherBaseD0Ev
	.section	.text._ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE:
.LFB5159:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1241
	jmp	_ZN12v8_inspector8protocolL21reportProtocolErrorToEPNS0_15FrontendChannelEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE.part.0
	.p2align 4,,10
	.p2align 3
.L1241:
	ret
	.cfi_endproc
.LFE5159:
	.size	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol14DispatcherBase13clearFrontendEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14DispatcherBase13clearFrontendEv
	.type	_ZN12v8_inspector8protocol14DispatcherBase13clearFrontendEv, @function
_ZN12v8_inspector8protocol14DispatcherBase13clearFrontendEv:
.LFB5160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rdi), %rax
	movq	$0, 8(%rdi)
	testq	%rax, %rax
	je	.L1244
	.p2align 4,,10
	.p2align 3
.L1245:
	movq	8(%rax), %rdx
	movq	$0, (%rdx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L1245
	movq	32(%r12), %rbx
	testq	%rbx, %rbx
	je	.L1244
	.p2align 4,,10
	.p2align 3
.L1246:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1246
.L1244:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	popq	%rbx
	movq	$0, 40(%r12)
	movq	$0, 32(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5160:
	.size	_ZN12v8_inspector8protocol14DispatcherBase13clearFrontendEv, .-_ZN12v8_inspector8protocol14DispatcherBase13clearFrontendEv
	.section	.text._ZN12v8_inspector8protocol14UberDispatcherC2EPNS0_15FrontendChannelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14UberDispatcherC2EPNS0_15FrontendChannelE
	.type	_ZN12v8_inspector8protocol14UberDispatcherC2EPNS0_15FrontendChannelE, @function
_ZN12v8_inspector8protocol14UberDispatcherC2EPNS0_15FrontendChannelE:
.LFB5205:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector8protocol14UberDispatcherE(%rip), %rax
	movss	.LC11(%rip), %xmm0
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	leaq	64(%rdi), %rax
	movq	%rax, 16(%rdi)
	leaq	120(%rdi), %rax
	movq	$1, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	%rax, 72(%rdi)
	movq	$1, 80(%rdi)
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	movq	$0, 112(%rdi)
	movq	$0, 120(%rdi)
	movss	%xmm0, 48(%rdi)
	movss	%xmm0, 104(%rdi)
	ret
	.cfi_endproc
.LFE5205:
	.size	_ZN12v8_inspector8protocol14UberDispatcherC2EPNS0_15FrontendChannelE, .-_ZN12v8_inspector8protocol14UberDispatcherC2EPNS0_15FrontendChannelE
	.globl	_ZN12v8_inspector8protocol14UberDispatcherC1EPNS0_15FrontendChannelE
	.set	_ZN12v8_inspector8protocol14UberDispatcherC1EPNS0_15FrontendChannelE,_ZN12v8_inspector8protocol14UberDispatcherC2EPNS0_15FrontendChannelE
	.section	.text._ZN12v8_inspector8protocol14UberDispatcher15registerBackendERKNS_8String16ESt10unique_ptrINS0_14DispatcherBaseESt14default_deleteIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14UberDispatcher15registerBackendERKNS_8String16ESt10unique_ptrINS0_14DispatcherBaseESt14default_deleteIS6_EE
	.type	_ZN12v8_inspector8protocol14UberDispatcher15registerBackendERKNS_8String16ESt10unique_ptrINS0_14DispatcherBaseESt14default_deleteIS6_EE, @function
_ZN12v8_inspector8protocol14UberDispatcher15registerBackendERKNS_8String16ESt10unique_ptrINS0_14DispatcherBaseESt14default_deleteIS6_EE:
.LFB5207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	32(%rsi), %rbx
	testq	%rbx, %rbx
	jne	.L1258
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rcx
	cmpq	%rcx, %rax
	je	.L1261
	.p2align 4,,10
	.p2align 3
.L1260:
	movq	%rbx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rbx, %rdx
	movsbq	-2(%rax), %rbx
	addq	%rdx, %rbx
	movq	%rbx, 32(%r13)
	cmpq	%rax, %rcx
	jne	.L1260
	testq	%rbx, %rbx
	je	.L1261
.L1258:
	movq	80(%r12), %rdi
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	72(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	leaq	0(,%rdx,8), %r15
	testq	%rax, %rax
	je	.L1269
	movq	(%rax), %rcx
	movq	56(%rcx), %rsi
.L1270:
	cmpq	%rbx, %rsi
	je	.L1314
.L1265:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1269
	movq	56(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	je	.L1270
.L1269:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	0(%r13), %rsi
	movq	%rax, %r8
	movq	$0, (%rax)
	leaq	8(%rax), %rdi
	leaq	24(%rax), %rax
	movq	%rax, 8(%r8)
	movq	8(%r13), %rax
	movq	%r8, -56(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r13), %rax
	movq	-56(%rbp), %r8
	leaq	104(%r12), %rdi
	movq	96(%r12), %rdx
	movq	80(%r12), %rsi
	movl	$1, %ecx
	movq	%rax, 40(%r8)
	movq	$0, 48(%r8)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	movq	%rdx, %r13
	jne	.L1315
	movq	72(%r12), %r9
	movq	%rbx, 56(%r8)
	leaq	(%r9,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1280
.L1318:
	movq	(%rdx), %rdx
	movq	%rdx, (%r8)
	movq	(%rax), %rax
	movq	%r8, (%rax)
.L1281:
	addq	$1, 96(%r12)
	leaq	48(%r8), %rcx
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1314:
	movq	8(%r13), %rax
	movq	16(%rcx), %rsi
	movq	8(%rcx), %r10
	movq	0(%r13), %r11
	cmpq	%rsi, %rax
	movq	%rsi, %r9
	cmovbe	%rax, %r9
	testq	%r9, %r9
	je	.L1266
	movq	%rax, -56(%rbp)
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1267:
	movzwl	(%r10,%rdx,2), %eax
	cmpw	%ax, (%r11,%rdx,2)
	jne	.L1265
	addq	$1, %rdx
	cmpq	%r9, %rdx
	jne	.L1267
	movq	-56(%rbp), %rax
.L1266:
	subq	%rsi, %rax
	movl	$2147483648, %esi
	cmpq	%rsi, %rax
	jge	.L1265
	movabsq	$-2147483649, %rsi
	cmpq	%rsi, %rax
	jle	.L1265
	testl	%eax, %eax
	jne	.L1265
	addq	$48, %rcx
.L1284:
	movq	(%r14), %rax
	movq	(%rcx), %rdi
	movq	$0, (%r14)
	movq	%rax, (%rcx)
	testq	%rdi, %rdi
	je	.L1257
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1261:
	.cfi_restore_state
	movq	$1, 32(%r13)
	movl	$1, %ebx
	jmp	.L1258
	.p2align 4,,10
	.p2align 3
.L1257:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1315:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L1316
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1317
	leaq	0(,%rdx,8), %r15
	movq	%r8, -56(%rbp)
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-56(%rbp), %r8
	leaq	120(%r12), %r11
	movq	%rax, %r9
.L1273:
	movq	88(%r12), %rsi
	movq	$0, 88(%r12)
	testq	%rsi, %rsi
	je	.L1275
	xorl	%edi, %edi
	leaq	88(%r12), %r10
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1277:
	movq	(%r15), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1278:
	testq	%rsi, %rsi
	je	.L1275
.L1276:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	56(%rcx), %rax
	divq	%r13
	leaq	(%r9,%rdx,8), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	jne	.L1277
	movq	88(%r12), %r15
	movq	%r15, (%rcx)
	movq	%rcx, 88(%r12)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L1286
	movq	%rcx, (%r9,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1276
	.p2align 4,,10
	.p2align 3
.L1275:
	movq	72(%r12), %rdi
	cmpq	%r11, %rdi
	je	.L1279
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
.L1279:
	movq	%rbx, %rax
	xorl	%edx, %edx
	movq	%r13, 80(%r12)
	divq	%r13
	movq	%r9, 72(%r12)
	movq	%rbx, 56(%r8)
	leaq	0(,%rdx,8), %r15
	leaq	(%r9,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L1318
.L1280:
	movq	88(%r12), %rdx
	movq	%r8, 88(%r12)
	movq	%rdx, (%r8)
	testq	%rdx, %rdx
	je	.L1282
	movq	56(%rdx), %rax
	xorl	%edx, %edx
	divq	80(%r12)
	movq	%r8, (%r9,%rdx,8)
	movq	72(%r12), %rax
	addq	%r15, %rax
.L1282:
	leaq	88(%r12), %rdx
	movq	%rdx, (%rax)
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1286:
	movq	%rdx, %rdi
	jmp	.L1278
	.p2align 4,,10
	.p2align 3
.L1316:
	movq	$0, 120(%r12)
	leaq	120(%r12), %r9
	movq	%r9, %r11
	jmp	.L1273
.L1317:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE5207:
	.size	_ZN12v8_inspector8protocol14UberDispatcher15registerBackendERKNS_8String16ESt10unique_ptrINS0_14DispatcherBaseESt14default_deleteIS6_EE, .-_ZN12v8_inspector8protocol14UberDispatcher15registerBackendERKNS_8String16ESt10unique_ptrINS0_14DispatcherBaseESt14default_deleteIS6_EE
	.section	.rodata._ZN12v8_inspector8protocol14UberDispatcher14findDispatcherERKNS_8String16E.str1.1,"aMS",@progbits,1
.LC21:
	.string	"."
	.section	.text._ZN12v8_inspector8protocol14UberDispatcher14findDispatcherERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14UberDispatcher14findDispatcherERKNS_8String16E
	.type	_ZN12v8_inspector8protocol14UberDispatcher14findDispatcherERKNS_8String16E, @function
_ZN12v8_inspector8protocol14UberDispatcher14findDispatcherERKNS_8String16E:
.LFB5211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	.LC21(%rip), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-88(%rbp), %r12
	movq	-96(%rbp), %rdi
	testq	%r12, %r12
	je	.L1320
	movq	8(%r13), %rdx
	testq	%rdx, %rdx
	je	.L1321
	movq	0(%r13), %rsi
	movzwl	(%rdi), %r8d
	leaq	(%rsi,%rdx,2), %r9
	cmpq	%rdx, %r12
	ja	.L1321
	movl	$1, %ecx
	movq	%rsi, %rax
	subq	%r12, %rcx
	.p2align 4,,10
	.p2align 3
.L1355:
	addq	%rcx, %rdx
	je	.L1321
	xorl	%r10d, %r10d
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1399:
	addq	$1, %r10
	addq	$2, %rax
	cmpq	%r10, %rdx
	je	.L1321
.L1323:
	cmpw	(%rax), %r8w
	jne	.L1399
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1322:
	addq	$1, %rdx
	cmpq	%rdx, %r12
	je	.L1400
	movzwl	(%rdi,%rdx,2), %r11d
	cmpw	%r11w, (%rax,%rdx,2)
	je	.L1322
	addq	$2, %rax
	movq	%r9, %rdx
	subq	%rax, %rdx
	sarq	%rdx
	cmpq	%rdx, %r12
	jbe	.L1355
	.p2align 4,,10
	.p2align 3
.L1321:
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1397
	call	_ZdlPv@PLT
	xorl	%r12d, %r12d
.L1319:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1401
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1320:
	.cfi_restore_state
	leaq	-80(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rax, %rdi
	jne	.L1402
.L1327:
	leaq	-112(%rbp), %r14
	xorl	%r8d, %r8d
	movq	%r14, -128(%rbp)
	movq	%r14, %rdi
.L1333:
	xorl	%eax, %eax
	movq	%r8, -120(%rbp)
	leaq	-128(%rbp), %rsi
	movw	%ax, (%rdi,%r12)
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1335
	call	_ZdlPv@PLT
.L1335:
	movq	-64(%rbp), %rcx
	movq	-96(%rbp), %rdi
	testq	%rcx, %rcx
	jne	.L1336
	movq	-88(%rbp), %rax
	leaq	(%rdi,%rax,2), %rsi
	movq	%rdi, %rax
	cmpq	%rdi, %rsi
	je	.L1339
	.p2align 4,,10
	.p2align 3
.L1338:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	movsbq	-2(%rax), %rdx
	addq	%rdx, %rcx
	movq	%rcx, -64(%rbp)
	cmpq	%rax, %rsi
	jne	.L1338
	testq	%rcx, %rcx
	je	.L1339
.L1336:
	movq	80(%rbx), %r8
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	72(%rbx), %rax
	movq	(%rax,%rdx,8), %r12
	movq	%rdx, %r10
	testq	%r12, %r12
	je	.L1340
	movq	(%r12), %r12
	movq	-88(%rbp), %r9
	movl	$2147483648, %r11d
	movabsq	$-2147483649, %rbx
	movq	56(%r12), %rsi
.L1345:
	cmpq	%rsi, %rcx
	je	.L1403
.L1341:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L1340
	movq	56(%r12), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r10
	je	.L1345
	xorl	%r12d, %r12d
	jmp	.L1340
	.p2align 4,,10
	.p2align 3
.L1403:
	movq	16(%r12), %rdx
	movq	8(%r12), %r15
	cmpq	%rdx, %r9
	movq	%rdx, %rsi
	cmovbe	%r9, %rsi
	testq	%rsi, %rsi
	je	.L1342
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1343:
	movzwl	(%r15,%rax,2), %r14d
	cmpw	%r14w, (%rdi,%rax,2)
	jne	.L1341
	addq	$1, %rax
	cmpq	%rsi, %rax
	jne	.L1343
.L1342:
	movq	%r9, %rax
	subq	%rdx, %rax
	cmpq	%r11, %rax
	jge	.L1341
	cmpq	%rbx, %rax
	jle	.L1341
	testl	%eax, %eax
	jne	.L1341
	movq	48(%r12), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L1404
	movq	48(%r12), %r12
	movq	-96(%rbp), %rdi
.L1340:
	cmpq	-136(%rbp), %rdi
	je	.L1319
	call	_ZdlPv@PLT
	jmp	.L1319
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	-96(%rbp), %rdi
	xorl	%r12d, %r12d
	jmp	.L1340
	.p2align 4,,10
	.p2align 3
.L1339:
	movq	$1, -64(%rbp)
	movl	$1, %ecx
	jmp	.L1336
	.p2align 4,,10
	.p2align 3
.L1402:
	call	_ZdlPv@PLT
	jmp	.L1327
	.p2align 4,,10
	.p2align 3
.L1397:
	xorl	%r12d, %r12d
	jmp	.L1319
	.p2align 4,,10
	.p2align 3
.L1400:
	subq	%rsi, %rax
	sarq	%rax
	movq	%rax, %r12
	leaq	-80(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rax, %rdi
	je	.L1354
	call	_ZdlPv@PLT
.L1354:
	cmpq	$-1, %r12
	je	.L1397
	cmpq	%r12, 8(%r13)
	movq	0(%r13), %rsi
	leaq	-112(%rbp), %r14
	cmovbe	8(%r13), %r12
	movq	%r14, -128(%rbp)
	movq	%rsi, %rax
	addq	%r12, %r12
	addq	%r12, %rax
	je	.L1329
	testq	%rsi, %rsi
	je	.L1405
.L1329:
	movq	%r12, %rax
	sarq	%rax
	movq	%rax, %r8
	cmpq	$7, %rax
	jbe	.L1362
	movabsq	$2305843009213693951, %rdx
	cmpq	%rdx, %rax
	ja	.L1406
	leaq	2(%r12), %rdi
	movq	%rsi, -144(%rbp)
	movq	%rax, -152(%rbp)
	call	_Znwm@PLT
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %rsi
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	%r8, -112(%rbp)
.L1330:
	cmpq	$2, %r12
	jne	.L1332
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-128(%rbp), %rdi
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1332:
	testq	%r12, %r12
	je	.L1333
	movq	%r12, %rdx
	movq	%r8, -144(%rbp)
	call	memmove@PLT
	movq	-128(%rbp), %rdi
	movq	-144(%rbp), %r8
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1362:
	movq	%r14, %rdi
	jmp	.L1330
.L1401:
	call	__stack_chk_fail@PLT
.L1406:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1405:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE5211:
	.size	_ZN12v8_inspector8protocol14UberDispatcher14findDispatcherERKNS_8String16E, .-_ZN12v8_inspector8protocol14UberDispatcher14findDispatcherERKNS_8String16E
	.section	.text._ZN12v8_inspector8protocol16InternalResponse14createResponseEiSt10unique_ptrINS0_12SerializableESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol16InternalResponse14createResponseEiSt10unique_ptrINS0_12SerializableESt14default_deleteIS3_EE
	.type	_ZN12v8_inspector8protocol16InternalResponse14createResponseEiSt10unique_ptrINS0_12SerializableESt14default_deleteIS3_EE, @function
_ZN12v8_inspector8protocol16InternalResponse14createResponseEiSt10unique_ptrINS0_12SerializableESt14default_deleteIS3_EE:
.LFB5218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$64, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rdx), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rdx)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movw	%cx, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%rbx)
	movq	-88(%rbp), %rax
	leaq	32(%rbx), %rdi
	movl	%r12d, 8(%rbx)
	leaq	(%rax,%rax), %r12
	movq	%rsi, %rax
	movq	%rdi, 16(%rbx)
	addq	%r12, %rax
	je	.L1408
	testq	%rsi, %rsi
	je	.L1427
.L1408:
	movq	%r12, %rcx
	sarq	%rcx
	cmpq	$14, %r12
	ja	.L1428
.L1409:
	cmpq	$2, %r12
	je	.L1429
	testq	%r12, %r12
	je	.L1412
	movq	%r12, %rdx
	movq	%rcx, -104(%rbp)
	call	memmove@PLT
	movq	16(%rbx), %rdi
	movq	-104(%rbp), %rcx
.L1412:
	xorl	%eax, %eax
	movq	%rcx, 24(%rbx)
	movw	%ax, (%rdi,%r12)
	movq	-64(%rbp), %rax
	movq	%rax, 48(%rbx)
	testq	%r15, %r15
	je	.L1413
	movq	%r15, 56(%rbx)
.L1414:
	movq	-96(%rbp), %rdi
	movq	%rbx, 0(%r13)
	cmpq	%r14, %rdi
	je	.L1407
	call	_ZdlPv@PLT
.L1407:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1430
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1429:
	.cfi_restore_state
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	16(%rbx), %rdi
	jmp	.L1412
	.p2align 4,,10
	.p2align 3
.L1428:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rcx
	ja	.L1431
	leaq	2(%r12), %rdi
	movq	%rcx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	%rcx, 32(%rbx)
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1413:
	movq	$0, 56(%rbx)
	jmp	.L1414
.L1427:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1430:
	call	__stack_chk_fail@PLT
.L1431:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5218:
	.size	_ZN12v8_inspector8protocol16InternalResponse14createResponseEiSt10unique_ptrINS0_12SerializableESt14default_deleteIS3_EE, .-_ZN12v8_inspector8protocol16InternalResponse14createResponseEiSt10unique_ptrINS0_12SerializableESt14default_deleteIS3_EE
	.section	.text._ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE
	.type	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE, @function
_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE:
.LFB5139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L1432
	cmpl	$1, (%rdx)
	movl	%esi, %r12d
	je	.L1458
	movq	0(%r13), %rax
	leaq	-56(%rbp), %rdi
	leaq	-64(%rbp), %rdx
	movq	16(%rax), %rbx
	movq	(%rcx), %rax
	movq	$0, (%rcx)
	movq	%rax, -64(%rbp)
	call	_ZN12v8_inspector8protocol16InternalResponse14createResponseEiSt10unique_ptrINS0_12SerializableESt14default_deleteIS3_EE
	movq	-56(%rbp), %rax
	movq	%r13, %rdi
	leaq	-48(%rbp), %rdx
	movq	$0, -56(%rbp)
	movl	%r12d, %esi
	movq	%rax, -48(%rbp)
	call	*%rbx
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1435
	movq	(%rdi), %rax
	call	*24(%rax)
.L1435:
	movq	-56(%rbp), %r12
	testq	%r12, %r12
	je	.L1436
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1437
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L1438
	movq	(%rdi), %rax
	call	*24(%rax)
.L1438:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1439
	call	_ZdlPv@PLT
.L1439:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1436:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1432
	movq	(%rdi), %rax
	call	*24(%rax)
.L1432:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1459
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1458:
	.cfi_restore_state
	leaq	8(%rdx), %rcx
	movl	48(%rdx), %edx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocolL21reportProtocolErrorToEPNS0_15FrontendChannelEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE.part.0
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1437:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1436
.L1459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5139:
	.size	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE, .-_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE
	.section	.text._ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE
	.type	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE, @function
_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE:
.LFB5142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$96, %edi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movl	%r13d, %esi
	leaq	64(%rax), %rdx
	leaq	16+_ZTVN12v8_inspector8protocol15DictionaryValueE(%rip), %rcx
	movl	$6, 8(%rax)
	movq	%rcx, (%rax)
	leaq	-48(%rbp), %rcx
	movq	%rdx, 16(%rax)
	movq	%r14, %rdx
	movq	$1, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movl	$0x3f800000, 48(%rax)
	movq	$0, 56(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -48(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1460
	movq	(%rdi), %rax
	call	*24(%rax)
.L1460:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1467
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1467:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5142:
	.size	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE, .-_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE
	.section	.text._ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE
	.type	_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE, @function
_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE:
.LFB5220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$64, %edi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdx), %r14
	movq	$0, (%rdx)
	call	_Znwm@PLT
	movq	0(%r13), %rsi
	movq	8(%r13), %rcx
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	(%rcx,%rcx), %r12
	movq	%rsi, %rax
	leaq	32(%rbx), %rdi
	addq	%r12, %rax
	movl	$0, 8(%rbx)
	movq	%rdi, 16(%rbx)
	je	.L1469
	testq	%rsi, %rsi
	je	.L1486
.L1469:
	movq	%r12, %r8
	sarq	%r8
	cmpq	$14, %r12
	ja	.L1487
.L1470:
	cmpq	$2, %r12
	je	.L1488
	testq	%r12, %r12
	je	.L1473
	movq	%r12, %rdx
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	16(%rbx), %rdi
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
.L1473:
	xorl	%eax, %eax
	movq	%r8, 24(%rbx)
	movw	%ax, (%rdi,%rcx,2)
	movq	32(%r13), %rax
	movq	%rax, 48(%rbx)
	testq	%r14, %r14
	je	.L1474
	movq	%r14, 56(%rbx)
.L1475:
	movq	%rbx, (%r15)
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1488:
	.cfi_restore_state
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	16(%rbx), %rdi
	jmp	.L1473
	.p2align 4,,10
	.p2align 3
.L1487:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r8
	ja	.L1489
	leaq	2(%r12), %rdi
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	%rax, 16(%rbx)
	movq	-56(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r8, 32(%rbx)
	jmp	.L1470
	.p2align 4,,10
	.p2align 3
.L1474:
	movq	$0, 56(%rbx)
	jmp	.L1475
.L1486:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1489:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5220:
	.size	_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE, .-_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE
	.section	.text._ZN12v8_inspector8protocol16InternalResponseC2EiRKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol16InternalResponseC2EiRKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE
	.type	_ZN12v8_inspector8protocol16InternalResponseC2EiRKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE, @function
_ZN12v8_inspector8protocol16InternalResponseC2EiRKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE:
.LFB5224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rax, -32(%rdi)
	movl	%esi, -24(%rdi)
	movq	%rdi, 16(%rbx)
	movq	(%rdx), %r15
	movq	8(%rdx), %rcx
	movq	%r15, %rax
	leaq	(%rcx,%rcx), %r12
	addq	%r12, %rax
	je	.L1491
	testq	%r15, %r15
	je	.L1508
.L1491:
	movq	%r12, %r8
	sarq	%r8
	cmpq	$14, %r12
	ja	.L1509
.L1492:
	cmpq	$2, %r12
	je	.L1510
	testq	%r12, %r12
	je	.L1495
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	16(%rbx), %rdi
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
.L1495:
	xorl	%eax, %eax
	movq	%r8, 24(%rbx)
	movw	%ax, (%rdi,%rcx,2)
	movq	32(%r13), %rax
	movq	%rax, 48(%rbx)
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L1496
	movq	$0, (%r14)
	movq	%rax, 56(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1510:
	.cfi_restore_state
	movzwl	(%r15), %eax
	movw	%ax, (%rdi)
	movq	16(%rbx), %rdi
	jmp	.L1495
	.p2align 4,,10
	.p2align 3
.L1509:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r8
	ja	.L1511
	leaq	2(%r12), %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	%r8, 32(%rbx)
	jmp	.L1492
	.p2align 4,,10
	.p2align 3
.L1496:
	movq	$0, 56(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1508:
	.cfi_restore_state
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1511:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5224:
	.size	_ZN12v8_inspector8protocol16InternalResponseC2EiRKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE, .-_ZN12v8_inspector8protocol16InternalResponseC2EiRKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE
	.globl	_ZN12v8_inspector8protocol16InternalResponseC1EiRKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE
	.set	_ZN12v8_inspector8protocol16InternalResponseC1EiRKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE,_ZN12v8_inspector8protocol16InternalResponseC2EiRKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE
	.section	.text._ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_,"axG",@progbits,_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	.type	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_, @function
_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_:
.LFB6639:
	.cfi_startproc
	endbr64
	movq	32(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L1513
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %r8
	cmpq	%r8, %rax
	je	.L1516
	.p2align 4,,10
	.p2align 3
.L1515:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	movsbq	-2(%rax), %rdx
	addq	%rdx, %rcx
	movq	%rcx, 32(%rsi)
	cmpq	%rax, %r8
	jne	.L1515
	testq	%rcx, %rcx
	je	.L1516
.L1513:
	movq	8(%rdi), %r9
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	%r9
	movq	(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1543
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2147483648, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rax), %rdi
	movabsq	$-2147483649, %rbx
	movq	56(%rdi), %r8
.L1522:
	cmpq	%rcx, %r8
	je	.L1546
.L1518:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1517
	movq	56(%rdi), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%r9
	cmpq	%rdx, %r10
	je	.L1522
.L1517:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1546:
	.cfi_restore_state
	movq	8(%rsi), %rdx
	movq	16(%rdi), %r14
	movq	8(%rdi), %r12
	movq	(%rsi), %r13
	cmpq	%r14, %rdx
	movq	%r14, %r8
	cmovbe	%rdx, %r8
	testq	%r8, %r8
	je	.L1519
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1520:
	movzwl	(%r12,%rax,2), %r15d
	cmpw	%r15w, 0(%r13,%rax,2)
	jne	.L1518
	addq	$1, %rax
	cmpq	%r8, %rax
	jne	.L1520
.L1519:
	subq	%r14, %rdx
	cmpq	%r11, %rdx
	jge	.L1518
	cmpq	%rbx, %rdx
	jle	.L1518
	testl	%edx, %edx
	jne	.L1518
	popq	%rbx
	movq	%rdi, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1516:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	$1, 32(%rsi)
	movl	$1, %ecx
	jmp	.L1513
	.p2align 4,,10
	.p2align 3
.L1543:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6639:
	.size	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_, .-_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	.section	.text._ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E
	.type	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E, @function
_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E:
.LFB4998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L1547
	movq	48(%rax), %rax
.L1547:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4998:
	.size	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E, .-_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E
	.section	.text._ZNK12v8_inspector8protocol15DictionaryValue10getBooleanERKNS_8String16EPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15DictionaryValue10getBooleanERKNS_8String16EPb
	.type	_ZNK12v8_inspector8protocol15DictionaryValue10getBooleanERKNS_8String16EPb, @function
_ZNK12v8_inspector8protocol15DictionaryValue10getBooleanERKNS_8String16EPb:
.LFB4992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L1553
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1553
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1553:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4992:
	.size	_ZNK12v8_inspector8protocol15DictionaryValue10getBooleanERKNS_8String16EPb, .-_ZNK12v8_inspector8protocol15DictionaryValue10getBooleanERKNS_8String16EPb
	.section	.text._ZNK12v8_inspector8protocol15DictionaryValue10getIntegerERKNS_8String16EPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15DictionaryValue10getIntegerERKNS_8String16EPi
	.type	_ZNK12v8_inspector8protocol15DictionaryValue10getIntegerERKNS_8String16EPi, @function
_ZNK12v8_inspector8protocol15DictionaryValue10getIntegerERKNS_8String16EPi:
.LFB4993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L1560
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1560
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movq	48(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1560:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4993:
	.size	_ZNK12v8_inspector8protocol15DictionaryValue10getIntegerERKNS_8String16EPi, .-_ZNK12v8_inspector8protocol15DictionaryValue10getIntegerERKNS_8String16EPi
	.section	.text._ZNK12v8_inspector8protocol15DictionaryValue9getDoubleERKNS_8String16EPd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15DictionaryValue9getDoubleERKNS_8String16EPd
	.type	_ZNK12v8_inspector8protocol15DictionaryValue9getDoubleERKNS_8String16EPd, @function
_ZNK12v8_inspector8protocol15DictionaryValue9getDoubleERKNS_8String16EPd:
.LFB4994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L1567
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1567
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movq	40(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1567:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4994:
	.size	_ZNK12v8_inspector8protocol15DictionaryValue9getDoubleERKNS_8String16EPd, .-_ZNK12v8_inspector8protocol15DictionaryValue9getDoubleERKNS_8String16EPd
	.section	.text._ZNK12v8_inspector8protocol15DictionaryValue9getStringERKNS_8String16EPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15DictionaryValue9getStringERKNS_8String16EPS2_
	.type	_ZNK12v8_inspector8protocol15DictionaryValue9getStringERKNS_8String16EPS2_, @function
_ZNK12v8_inspector8protocol15DictionaryValue9getStringERKNS_8String16EPS2_:
.LFB4995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L1574
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1574
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movq	56(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1574:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4995:
	.size	_ZNK12v8_inspector8protocol15DictionaryValue9getStringERKNS_8String16EPS2_, .-_ZNK12v8_inspector8protocol15DictionaryValue9getStringERKNS_8String16EPS2_
	.section	.text._ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E
	.type	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E, @function
_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E:
.LFB4996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L1581
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L1581
	cmpl	$6, 8(%rax)
	movl	$0, %edx
	cmovne	%rdx, %rax
.L1581:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4996:
	.size	_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E, .-_ZNK12v8_inspector8protocol15DictionaryValue9getObjectERKNS_8String16E
	.section	.text._ZNK12v8_inspector8protocol15DictionaryValue8getArrayERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15DictionaryValue8getArrayERKNS_8String16E
	.type	_ZNK12v8_inspector8protocol15DictionaryValue8getArrayERKNS_8String16E, @function
_ZNK12v8_inspector8protocol15DictionaryValue8getArrayERKNS_8String16E:
.LFB4997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L1591
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L1591
	cmpl	$7, 8(%rax)
	movl	$0, %edx
	cmovne	%rdx, %rax
.L1591:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4997:
	.size	_ZNK12v8_inspector8protocol15DictionaryValue8getArrayERKNS_8String16E, .-_ZNK12v8_inspector8protocol15DictionaryValue8getArrayERKNS_8String16E
	.section	.text._ZNK12v8_inspector8protocol15DictionaryValue2atEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15DictionaryValue2atEm
	.type	_ZNK12v8_inspector8protocol15DictionaryValue2atEm, @function
_ZNK12v8_inspector8protocol15DictionaryValue2atEm:
.LFB4999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	(%rdx,%rdx,4), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	72(%rsi), %rax
	movq	%r14, -96(%rbp)
	leaq	(%rax,%rdx,8), %r15
	movq	(%r15), %rsi
	movq	8(%r15), %rcx
	movq	%rsi, %rax
	leaq	(%rcx,%rcx), %r13
	addq	%r13, %rax
	je	.L1602
	testq	%rsi, %rsi
	je	.L1607
.L1602:
	movq	%r13, %r8
	movq	%r14, %rdi
	sarq	%r8
	cmpq	$14, %r13
	ja	.L1633
.L1603:
	cmpq	$2, %r13
	je	.L1634
	testq	%r13, %r13
	je	.L1606
	movq	%r13, %rdx
	movq	%r8, -112(%rbp)
	movq	%rcx, -104(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %rdi
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rcx
.L1606:
	xorl	%edx, %edx
	movq	%r8, -88(%rbp)
	leaq	-96(%rbp), %rsi
	movw	%dx, (%rdi,%rcx,2)
	movq	32(%r15), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	-96(%rbp), %r13
	leaq	16(%r12), %rdi
	movq	48(%rax), %rcx
	movq	-88(%rbp), %rax
	movq	%rdi, (%r12)
	leaq	(%rax,%rax), %rbx
	movq	%r13, %rax
	addq	%rbx, %rax
	je	.L1616
	testq	%r13, %r13
	je	.L1607
.L1616:
	movq	%rbx, %r15
	sarq	%r15
	cmpq	$14, %rbx
	ja	.L1635
.L1609:
	cmpq	$2, %rbx
	je	.L1636
	testq	%rbx, %rbx
	je	.L1612
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rcx, -104(%rbp)
	call	memmove@PLT
	movq	(%r12), %rdi
	movq	-104(%rbp), %rcx
.L1612:
	xorl	%eax, %eax
	movq	%r15, 8(%r12)
	movw	%ax, (%rdi,%rbx)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rcx, 40(%r12)
	movq	%rax, 32(%r12)
	cmpq	%r14, %rdi
	je	.L1601
	call	_ZdlPv@PLT
.L1601:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1637
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1634:
	.cfi_restore_state
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-96(%rbp), %rdi
	jmp	.L1606
	.p2align 4,,10
	.p2align 3
.L1636:
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L1612
	.p2align 4,,10
	.p2align 3
.L1633:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r8
	ja	.L1610
	leaq	2(%r13), %rdi
	movq	%r8, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_Znwm@PLT
	movq	-120(%rbp), %r8
	movq	-112(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	-104(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r8, -80(%rbp)
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1635:
	movabsq	$2305843009213693951, %rax
	movq	%rcx, -104(%rbp)
	cmpq	%rax, %r15
	ja	.L1610
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r15, 16(%r12)
	movq	-104(%rbp), %rcx
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L1609
.L1607:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1637:
	call	__stack_chk_fail@PLT
.L1610:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4999:
	.size	_ZNK12v8_inspector8protocol15DictionaryValue2atEm, .-_ZNK12v8_inspector8protocol15DictionaryValue2atEm
	.section	.rodata._ZNK12v8_inspector8protocol15DictionaryValue9writeJSONEPNS_15String16BuilderE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"it != m_data.end()"
.LC23:
	.string	"Check failed: %s."
	.section	.text._ZNK12v8_inspector8protocol15DictionaryValue9writeJSONEPNS_15String16BuilderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15DictionaryValue9writeJSONEPNS_15String16BuilderE
	.type	_ZNK12v8_inspector8protocol15DictionaryValue9writeJSONEPNS_15String16BuilderE, @function
_ZNK12v8_inspector8protocol15DictionaryValue9writeJSONEPNS_15String16BuilderE:
.LFB5014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$123, %esi
	leaq	16(%r13), %r14
	pushq	%rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	movq	72(%r13), %rcx
	cmpq	%rcx, 80(%r13)
	je	.L1643
	.p2align 4,,10
	.p2align 3
.L1644:
	leaq	(%r15,%r15,4), %rax
	movq	%r14, %rdi
	leaq	(%rcx,%rax,8), %rsi
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1647
	testq	%r15, %r15
	jne	.L1648
	leaq	8(%rax), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol10StringUtil25builderAppendQuotedStringERNS_15String16BuilderERKNS_8String16E@PLT
	movl	$58, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	movq	48(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*72(%rax)
	movq	72(%r13), %rcx
	movq	80(%r13), %rax
	movabsq	$-3689348814741910323, %rdx
	subq	%rcx, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L1643
	movl	$1, %r15d
	jmp	.L1644
	.p2align 4,,10
	.p2align 3
.L1648:
	movl	$44, %esi
	movq	%r12, %rdi
	addq	$1, %r15
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	leaq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol10StringUtil25builderAppendQuotedStringERNS_15String16BuilderERKNS_8String16E@PLT
	movl	$58, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	movq	48(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*72(%rax)
	movq	72(%r13), %rcx
	movq	80(%r13), %rax
	movabsq	$-3689348814741910323, %rdx
	subq	%rcx, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	%rax, %r15
	jb	.L1644
.L1643:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$125, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector15String16Builder6appendEt@PLT
	.p2align 4,,10
	.p2align 3
.L1647:
	.cfi_restore_state
	leaq	.LC22(%rip), %rsi
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5014:
	.size	_ZNK12v8_inspector8protocol15DictionaryValue9writeJSONEPNS_15String16BuilderE, .-_ZNK12v8_inspector8protocol15DictionaryValue9writeJSONEPNS_15String16BuilderE
	.section	.text._ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb
	.type	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb, @function
_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb:
.LFB5010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movb	%dl, -9(%rbp)
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L1650
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1650
	movq	(%rdi), %rax
	leaq	-9(%rbp), %rsi
	call	*32(%rax)
.L1650:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	movzbl	-9(%rbp), %eax
	jne	.L1659
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1659:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5010:
	.size	_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb, .-_ZNK12v8_inspector8protocol15DictionaryValue15booleanPropertyERKNS_8String16Eb
	.section	.text._ZNK12v8_inspector8protocol15DictionaryValue15integerPropertyERKNS_8String16Ei,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15DictionaryValue15integerPropertyERKNS_8String16Ei
	.type	_ZNK12v8_inspector8protocol15DictionaryValue15integerPropertyERKNS_8String16Ei, @function
_ZNK12v8_inspector8protocol15DictionaryValue15integerPropertyERKNS_8String16Ei:
.LFB5011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	%edx, -12(%rbp)
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L1661
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1661
	movq	(%rdi), %rax
	leaq	-12(%rbp), %rsi
	call	*48(%rax)
.L1661:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	movl	-12(%rbp), %eax
	jne	.L1670
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1670:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5011:
	.size	_ZNK12v8_inspector8protocol15DictionaryValue15integerPropertyERKNS_8String16Ei, .-_ZNK12v8_inspector8protocol15DictionaryValue15integerPropertyERKNS_8String16Ei
	.section	.text._ZNK12v8_inspector8protocol15DictionaryValue14doublePropertyERKNS_8String16Ed,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15DictionaryValue14doublePropertyERKNS_8String16Ed
	.type	_ZNK12v8_inspector8protocol15DictionaryValue14doublePropertyERKNS_8String16Ed, @function
_ZNK12v8_inspector8protocol15DictionaryValue14doublePropertyERKNS_8String16Ed:
.LFB5012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movsd	%xmm0, -16(%rbp)
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L1672
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1672
	movq	(%rdi), %rax
	leaq	-16(%rbp), %rsi
	call	*40(%rax)
.L1672:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	movsd	-16(%rbp), %xmm0
	jne	.L1681
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1681:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5012:
	.size	_ZNK12v8_inspector8protocol15DictionaryValue14doublePropertyERKNS_8String16Ed, .-_ZNK12v8_inspector8protocol15DictionaryValue14doublePropertyERKNS_8String16Ed
	.section	.text._ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S1_ESaIS4_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_,"axG",@progbits,_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S1_ESaIS4_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S1_ESaIS4_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	.type	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S1_ESaIS4_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_, @function
_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S1_ESaIS4_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_:
.LFB6798:
	.cfi_startproc
	endbr64
	movq	32(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L1683
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %r8
	cmpq	%r8, %rax
	je	.L1686
	.p2align 4,,10
	.p2align 3
.L1685:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	movsbq	-2(%rax), %rdx
	addq	%rdx, %rcx
	movq	%rcx, 32(%rsi)
	cmpq	%rax, %r8
	jne	.L1685
	testq	%rcx, %rcx
	je	.L1686
.L1683:
	movq	8(%rdi), %r9
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	%r9
	movq	(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1713
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2147483648, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rax), %rdi
	movabsq	$-2147483649, %rbx
	movq	88(%rdi), %r8
.L1692:
	cmpq	%rcx, %r8
	je	.L1716
.L1688:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1687
	movq	88(%rdi), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%r9
	cmpq	%rdx, %r10
	je	.L1692
.L1687:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1716:
	.cfi_restore_state
	movq	8(%rsi), %rdx
	movq	16(%rdi), %r14
	movq	8(%rdi), %r12
	movq	(%rsi), %r13
	cmpq	%r14, %rdx
	movq	%r14, %r8
	cmovbe	%rdx, %r8
	testq	%r8, %r8
	je	.L1689
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1690:
	movzwl	(%r12,%rax,2), %r15d
	cmpw	%r15w, 0(%r13,%rax,2)
	jne	.L1688
	addq	$1, %rax
	cmpq	%r8, %rax
	jne	.L1690
.L1689:
	subq	%r14, %rdx
	cmpq	%r11, %rdx
	jge	.L1688
	cmpq	%rbx, %rdx
	jle	.L1688
	testl	%edx, %edx
	jne	.L1688
	popq	%rbx
	movq	%rdi, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1686:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	$1, 32(%rsi)
	movl	$1, %ecx
	jmp	.L1683
	.p2align 4,,10
	.p2align 3
.L1713:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6798:
	.size	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S1_ESaIS4_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_, .-_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S1_ESaIS4_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_:
.LFB7263:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L1736
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	8(%rsi), %r15
	cmpq	%r13, %rdx
	je	.L1727
	movq	16(%rdi), %rax
.L1719:
	cmpq	%r15, %rax
	jb	.L1739
	leaq	(%r15,%r15), %rdx
	testq	%r15, %r15
	je	.L1723
.L1742:
	movq	(%r12), %rsi
	cmpq	$1, %r15
	je	.L1740
	testq	%rdx, %rdx
	je	.L1723
	movq	%r13, %rdi
	call	memmove@PLT
	movq	(%rbx), %r13
.L1723:
	xorl	%eax, %eax
	movq	%r15, 8(%rbx)
	movw	%ax, 0(%r13,%r15,2)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1739:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r15
	ja	.L1741
	addq	%rax, %rax
	movq	%r15, %r14
	cmpq	%rax, %r15
	jnb	.L1722
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r14
.L1722:
	leaq	2(%r14,%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdi, %rdx
	je	.L1726
	call	_ZdlPv@PLT
.L1726:
	movq	%r13, (%rbx)
	leaq	(%r15,%r15), %rdx
	movq	%r14, 16(%rbx)
	testq	%r15, %r15
	je	.L1723
	jmp	.L1742
	.p2align 4,,10
	.p2align 3
.L1736:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1727:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, %eax
	jmp	.L1719
	.p2align 4,,10
	.p2align 3
.L1740:
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	movq	(%rbx), %r13
	jmp	.L1723
.L1741:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7263:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.section	.rodata._ZN12v8_inspector8protocol14UberDispatcher12parseCommandEPNS0_5ValueEPiPNS_8String16E.str1.1,"aMS",@progbits,1
.LC24:
	.string	"Message must be a valid JSON"
.LC25:
	.string	"Message must be an object"
.LC26:
	.string	"id"
	.section	.rodata._ZN12v8_inspector8protocol14UberDispatcher12parseCommandEPNS0_5ValueEPiPNS_8String16E.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"Message must have integer 'id' property"
	.section	.rodata._ZN12v8_inspector8protocol14UberDispatcher12parseCommandEPNS0_5ValueEPiPNS_8String16E.str1.1
.LC28:
	.string	"method"
	.section	.rodata._ZN12v8_inspector8protocol14UberDispatcher12parseCommandEPNS0_5ValueEPiPNS_8String16E.str1.8
	.align 8
.LC29:
	.string	"Message must have string 'method' property"
	.section	.text._ZN12v8_inspector8protocol14UberDispatcher12parseCommandEPNS0_5ValueEPiPNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14UberDispatcher12parseCommandEPNS0_5ValueEPiPNS_8String16E
	.type	_ZN12v8_inspector8protocol14UberDispatcher12parseCommandEPNS0_5ValueEPiPNS_8String16E, @function
_ZN12v8_inspector8protocol14UberDispatcher12parseCommandEPNS0_5ValueEPiPNS_8String16E:
.LFB5210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%rcx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1801
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r12
	jne	.L1802
	leaq	-96(%rbp), %r14
	addq	$16, %r12
	movq	%rdx, %r13
	movl	$0, -156(%rbp)
	leaq	.LC26(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L1753
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r15
	movq	48(%rax), %r8
	cmpq	%r15, %rdi
	je	.L1754
	movq	%r8, -176(%rbp)
	call	_ZdlPv@PLT
	movq	-176(%rbp), %r8
.L1754:
	testq	%r8, %r8
	je	.L1756
	movq	(%r8), %rax
	leaq	-156(%rbp), %rsi
	movq	%r8, %rdi
	call	*48(%rax)
	testb	%al, %al
	je	.L1756
	testq	%r13, %r13
	je	.L1762
	movl	-156(%rbp), %eax
	movl	%eax, 0(%r13)
.L1762:
	leaq	.LC28(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L1765
	movq	-96(%rbp), %rdi
	movq	48(%rax), %r13
	cmpq	%r15, %rdi
	je	.L1766
	call	_ZdlPv@PLT
.L1766:
	leaq	-128(%rbp), %r12
	xorl	%edx, %edx
	movq	$0, -136(%rbp)
	leaq	-144(%rbp), %rsi
	movq	%r12, -144(%rbp)
	movw	%dx, -128(%rbp)
	movq	$0, -112(%rbp)
	testq	%r13, %r13
	je	.L1768
	movq	0(%r13), %rax
	movq	%rsi, -176(%rbp)
	movq	%r13, %rdi
	call	*56(%rax)
	movq	-176(%rbp), %rsi
	testb	%al, %al
	movl	%eax, %r13d
	je	.L1768
	cmpq	$0, -168(%rbp)
	je	.L1774
	movq	-168(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-112(%rbp), %rax
	movq	%rax, 32(%rbx)
.L1774:
	movq	-144(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1743
	call	_ZdlPv@PLT
	jmp	.L1743
	.p2align 4,,10
	.p2align 3
.L1753:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L1756
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1756:
	leaq	.LC27(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L1760
	movq	(%r12), %rax
	movl	$104, %edi
	movq	24(%rax), %r13
	call	_Znwm@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movq	-88(%rbp), %rax
	movl	$-32600, 8(%rbx)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	$0, 64(%rbx)
	leaq	-152(%rbp), %rsi
	movq	%rax, 48(%rbx)
	leaq	72(%rbx), %rax
	movq	%rax, 56(%rbx)
	movw	%cx, 72(%rbx)
	movq	$0, 88(%rbx)
	movl	$0, 96(%rbx)
	movb	$0, 100(%rbx)
	movq	%rbx, -152(%rbp)
	call	*%r13
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1760
	movq	(%rdi), %rax
	call	*24(%rax)
.L1760:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1764
.L1796:
	call	_ZdlPv@PLT
.L1764:
	xorl	%r13d, %r13d
.L1743:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1803
	addq	$136, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1802:
	.cfi_restore_state
	leaq	-96(%rbp), %rdi
	leaq	.LC25(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L1750
	movq	(%r12), %rax
	movl	$104, %edi
	movq	24(%rax), %r13
	call	_Znwm@PLT
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorE(%rip), %rax
	movq	%rax, (%rbx)
	movl	$-32600, 8(%rbx)
.L1800:
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	xorl	%esi, %esi
	movb	$0, 100(%rbx)
	movw	%si, 72(%rbx)
	movq	%r12, %rdi
	leaq	-152(%rbp), %rsi
	movq	%rax, 48(%rbx)
	leaq	72(%rbx), %rax
	movq	%rax, 56(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 88(%rbx)
	movl	$0, 96(%rbx)
	movq	%rbx, -152(%rbp)
	call	*%r13
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1750
	movq	(%rdi), %rax
	call	*24(%rax)
.L1750:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1796
	jmp	.L1764
	.p2align 4,,10
	.p2align 3
.L1765:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1795
	call	_ZdlPv@PLT
.L1795:
	leaq	-128(%rbp), %r12
	xorl	%eax, %eax
	movq	$0, -136(%rbp)
	movq	%r12, -144(%rbp)
	movw	%ax, -128(%rbp)
	movq	$0, -112(%rbp)
.L1768:
	movq	%r14, %rdi
	leaq	.LC29(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1772
	movl	-156(%rbp), %esi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$-32600, %edx
	call	_ZN12v8_inspector8protocolL21reportProtocolErrorToEPNS0_15FrontendChannelEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE.part.0
.L1772:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1775
	call	_ZdlPv@PLT
.L1775:
	xorl	%r13d, %r13d
	jmp	.L1774
	.p2align 4,,10
	.p2align 3
.L1801:
	leaq	-96(%rbp), %rdi
	leaq	.LC24(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L1750
	movq	(%r12), %rax
	movl	$104, %edi
	movq	24(%rax), %r13
	call	_Znwm@PLT
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorE(%rip), %rax
	movq	%rax, (%rbx)
	movl	$-32700, 8(%rbx)
	jmp	.L1800
.L1803:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5210:
	.size	_ZN12v8_inspector8protocol14UberDispatcher12parseCommandEPNS0_5ValueEPiPNS_8String16E, .-_ZN12v8_inspector8protocol14UberDispatcher12parseCommandEPNS0_5ValueEPiPNS_8String16E
	.section	.text._ZN12v8_inspector8protocol14UberDispatcher11canDispatchERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14UberDispatcher11canDispatchERKNS_8String16E
	.type	_ZN12v8_inspector8protocol14UberDispatcher11canDispatchERKNS_8String16E, @function
_ZN12v8_inspector8protocol14UberDispatcher11canDispatchERKNS_8String16E:
.LFB5212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	(%rsi), %r15
	movq	8(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -96(%rbp)
	leaq	(%r8,%r8), %r12
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L1805
	testq	%r15, %r15
	je	.L1827
.L1805:
	movq	%r12, %rcx
	movq	%r14, %rdi
	sarq	%rcx
	cmpq	$14, %r12
	ja	.L1828
.L1806:
	cmpq	$2, %r12
	je	.L1829
	testq	%r12, %r12
	je	.L1809
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rcx, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %rdi
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %r8
.L1809:
	xorl	%eax, %eax
	movq	%rcx, -88(%rbp)
	leaq	-96(%rbp), %r12
	movw	%ax, (%rdi,%r8,2)
	movq	32(%rbx), %rax
	leaq	16(%r13), %rdi
	movq	%r12, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S1_ESaIS4_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1810
	leaq	48(%rax), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	80(%rbx), %rax
	movq	%rax, -64(%rbp)
.L1810:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol14UberDispatcher14findDispatcherERKNS_8String16E
	movq	-96(%rbp), %rdi
	testq	%rax, %rax
	setne	%r12b
	cmpq	%r14, %rdi
	je	.L1804
	call	_ZdlPv@PLT
.L1804:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1830
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1829:
	.cfi_restore_state
	movzwl	(%r15), %eax
	movw	%ax, (%rdi)
	movq	-96(%rbp), %rdi
	jmp	.L1809
	.p2align 4,,10
	.p2align 3
.L1828:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rcx
	ja	.L1831
	leaq	2(%r12), %rdi
	movq	%rcx, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	%rcx, -80(%rbp)
	jmp	.L1806
.L1827:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1830:
	call	__stack_chk_fail@PLT
.L1831:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5212:
	.size	_ZN12v8_inspector8protocol14UberDispatcher11canDispatchERKNS_8String16E, .-_ZN12v8_inspector8protocol14UberDispatcher11canDispatchERKNS_8String16E
	.section	.text._ZN12v8_inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINS_8String16ES3_St4hashIS3_ESt8equal_toIS3_ESaISt4pairIKS3_S3_EEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINS_8String16ES3_St4hashIS3_ESt8equal_toIS3_ESaISt4pairIKS3_S3_EEE
	.type	_ZN12v8_inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINS_8String16ES3_St4hashIS3_ESt8equal_toIS3_ESaISt4pairIKS3_S3_EEE, @function
_ZN12v8_inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINS_8String16ES3_St4hashIS3_ESt8equal_toIS3_ESaISt4pairIKS3_S3_EEE:
.LFB5209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rsi), %r13
	testq	%r13, %r13
	je	.L1832
	movq	%rdi, %r14
	.p2align 4,,10
	.p2align 3
.L1833:
	movq	40(%r13), %r12
	testq	%r12, %r12
	jne	.L1834
	movq	8(%r13), %rax
	movq	16(%r13), %rdx
	leaq	(%rax,%rdx,2), %rcx
	cmpq	%rcx, %rax
	je	.L1837
	.p2align 4,,10
	.p2align 3
.L1836:
	movq	%r12, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%r12, %rdx
	movsbq	-2(%rax), %r12
	addq	%rdx, %r12
	movq	%r12, 40(%r13)
	cmpq	%rax, %rcx
	jne	.L1836
	testq	%r12, %r12
	je	.L1837
.L1834:
	movq	24(%r14), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	16(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	leaq	0(,%rdx,8), %r15
	testq	%rax, %rax
	je	.L1845
	movq	(%rax), %rcx
	movq	88(%rcx), %rsi
.L1846:
	cmpq	%r12, %rsi
	je	.L1892
.L1841:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1845
	movq	88(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	je	.L1846
.L1845:
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rbx
	movq	$0, (%rax)
	leaq	8(%rax), %rdi
	leaq	24(%rax), %rax
	movq	%rax, 8(%rbx)
	movq	8(%r13), %rsi
	movq	16(%r13), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	40(%r13), %rax
	movq	40(%r14), %rdx
	movq	$0, 80(%rbx)
	movq	$0, 56(%rbx)
	pxor	%xmm0, %xmm0
	movq	24(%r14), %rsi
	leaq	48(%r14), %rdi
	movq	%rax, 40(%rbx)
	leaq	64(%rbx), %rax
	movl	$1, %ecx
	movq	%rax, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r8
	testb	%al, %al
	jne	.L1893
	movq	16(%r14), %r9
	movq	%r12, 88(%rbx)
	leaq	(%r9,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1856
.L1896:
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	(%rax), %rax
	movq	%rbx, (%rax)
.L1857:
	addq	$1, 40(%r14)
	addq	$48, %rbx
.L1860:
	leaq	48(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	80(%r13), %rax
	movq	%rax, 32(%rbx)
	movq	0(%r13), %r13
	testq	%r13, %r13
	jne	.L1833
.L1832:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1892:
	.cfi_restore_state
	movq	16(%r13), %rdx
	movq	16(%rcx), %rsi
	movq	8(%rcx), %r10
	movq	8(%r13), %r11
	cmpq	%rsi, %rdx
	movq	%rsi, %r9
	cmovbe	%rdx, %r9
	testq	%r9, %r9
	je	.L1842
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1843:
	movzwl	(%r10,%rax,2), %ebx
	cmpw	%bx, (%r11,%rax,2)
	jne	.L1841
	addq	$1, %rax
	cmpq	%r9, %rax
	jne	.L1843
.L1842:
	subq	%rsi, %rdx
	movl	$2147483648, %eax
	cmpq	%rax, %rdx
	jge	.L1841
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rdx
	jle	.L1841
	testl	%edx, %edx
	jne	.L1841
	leaq	48(%rcx), %rbx
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1837:
	movq	$1, 40(%r13)
	movl	$1, %r12d
	jmp	.L1834
	.p2align 4,,10
	.p2align 3
.L1893:
	cmpq	$1, %rdx
	je	.L1894
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1895
	leaq	0(,%rdx,8), %r15
	movq	%rdx, -56(%rbp)
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-56(%rbp), %r8
	leaq	64(%r14), %r11
	movq	%rax, %r9
.L1849:
	movq	32(%r14), %rsi
	movq	$0, 32(%r14)
	testq	%rsi, %rsi
	je	.L1851
	xorl	%edi, %edi
	leaq	32(%r14), %r10
	jmp	.L1852
	.p2align 4,,10
	.p2align 3
.L1853:
	movq	(%r15), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1854:
	testq	%rsi, %rsi
	je	.L1851
.L1852:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	88(%rcx), %rax
	divq	%r8
	leaq	(%r9,%rdx,8), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	jne	.L1853
	movq	32(%r14), %r15
	movq	%r15, (%rcx)
	movq	%rcx, 32(%r14)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L1862
	movq	%rcx, (%r9,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1852
	.p2align 4,,10
	.p2align 3
.L1851:
	movq	16(%r14), %rdi
	cmpq	%r11, %rdi
	je	.L1855
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r9
.L1855:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r8, 24(%r14)
	divq	%r8
	movq	%r9, 16(%r14)
	movq	%r12, 88(%rbx)
	leaq	0(,%rdx,8), %r15
	leaq	(%r9,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L1896
.L1856:
	movq	32(%r14), %rdx
	movq	%rbx, 32(%r14)
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	je	.L1858
	movq	88(%rdx), %rax
	xorl	%edx, %edx
	divq	24(%r14)
	movq	%rbx, (%r9,%rdx,8)
	movq	16(%r14), %rax
	addq	%r15, %rax
.L1858:
	leaq	32(%r14), %rdx
	movq	%rdx, (%rax)
	jmp	.L1857
	.p2align 4,,10
	.p2align 3
.L1862:
	movq	%rdx, %rdi
	jmp	.L1854
	.p2align 4,,10
	.p2align 3
.L1894:
	leaq	64(%r14), %r9
	movq	$0, 64(%r14)
	movq	%r9, %r11
	jmp	.L1849
.L1895:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE5209:
	.size	_ZN12v8_inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINS_8String16ES3_St4hashIS3_ESt8equal_toIS3_ESaISt4pairIKS3_S3_EEE, .-_ZN12v8_inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINS_8String16ES3_St4hashIS3_ESt8equal_toIS3_ESaISt4pairIKS3_S3_EEE
	.section	.rodata._ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_.str1.1,"aMS",@progbits,1
.LC30:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB7265:
	.cfi_startproc
	endbr64
	movabsq	$-3689348814741910323, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r14
	movq	%rdi, -80(%rbp)
	movabsq	$230584300921369395, %rdi
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rdi, %rax
	je	.L1925
	movq	%rsi, %rcx
	subq	%r14, %rcx
	testq	%rax, %rax
	je	.L1917
	movabsq	$9223372036854775800, %r12
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L1926
.L1899:
	movq	%r12, %rdi
	movq	%rdx, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movq	%rax, -64(%rbp)
	addq	%rax, %r12
	movq	%r12, -72(%rbp)
	leaq	40(%rax), %r12
.L1916:
	movq	-64(%rbp), %rax
	movq	(%rdx), %rdi
	addq	%rcx, %rax
	leaq	16(%rax), %rcx
	movq	%rcx, (%rax)
	leaq	16(%rdx), %rcx
	cmpq	%rcx, %rdi
	je	.L1927
	movq	%rdi, (%rax)
	movq	16(%rdx), %rdi
	movq	%rdi, 16(%rax)
.L1902:
	movq	%rcx, (%rdx)
	xorl	%ecx, %ecx
	movq	8(%rdx), %rdi
	movw	%cx, 16(%rdx)
	movq	$0, 8(%rdx)
	movq	32(%rdx), %rdx
	movq	%rdi, 8(%rax)
	movq	%rdx, 32(%rax)
	movq	-56(%rbp), %rax
	cmpq	%r14, %rax
	je	.L1903
	leaq	-40(%rax), %rdx
	movq	-64(%rbp), %r15
	leaq	16(%r14), %r13
	movabsq	$922337203685477581, %rcx
	subq	%r14, %rdx
	shrq	$3, %rdx
	imulq	%rcx, %rdx
	movabsq	$2305843009213693951, %rcx
	andq	%rcx, %rdx
	movq	%rdx, -88(%rbp)
	leaq	(%rdx,%rdx,4), %rdx
	leaq	56(%r14,%rdx,8), %r12
	.p2align 4,,10
	.p2align 3
.L1909:
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	je	.L1928
.L1904:
	movq	%rcx, (%r15)
	movq	0(%r13), %rcx
	movq	%rcx, 16(%r15)
.L1905:
	movq	-8(%r13), %rcx
	xorl	%eax, %eax
	movq	%rcx, 8(%r15)
	movq	16(%r13), %rcx
	movq	%r13, -16(%r13)
	movq	$0, -8(%r13)
	movw	%ax, 0(%r13)
	movq	%rcx, 32(%r15)
	movq	-16(%r13), %rdi
	cmpq	%r13, %rdi
	je	.L1906
	call	_ZdlPv@PLT
	addq	$40, %r13
	addq	$40, %r15
	cmpq	%r13, %r12
	jne	.L1909
.L1907:
	movq	-88(%rbp), %rax
	movq	-64(%rbp), %rsi
	leaq	10(%rax,%rax,4), %rax
	leaq	(%rsi,%rax,8), %r12
.L1903:
	movq	-56(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L1910
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L1914:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rdi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rdi, %rcx
	je	.L1929
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	addq	$40, %rax
	addq	$40, %rdx
	movq	%rcx, -24(%rdx)
	movq	-32(%rax), %rcx
	movq	%rcx, -32(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rbx, %rax
	jne	.L1914
.L1912:
	movq	%rbx, %rsi
	subq	-56(%rbp), %rsi
	leaq	-40(%rsi), %rax
	shrq	$3, %rax
	leaq	40(%r12,%rax,8), %r12
.L1910:
	testq	%r14, %r14
	je	.L1915
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1915:
	movq	-64(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r12, %xmm3
	movq	-72(%rbp), %rsi
	punpcklqdq	%xmm3, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1906:
	.cfi_restore_state
	addq	$40, %r13
	addq	$40, %r15
	cmpq	%r12, %r13
	je	.L1907
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	jne	.L1904
.L1928:
	movdqu	0(%r13), %xmm1
	movups	%xmm1, 16(%r15)
	jmp	.L1905
	.p2align 4,,10
	.p2align 3
.L1929:
	movq	8(%rax), %rcx
	movdqu	16(%rax), %xmm2
	addq	$40, %rax
	addq	$40, %rdx
	movq	%rcx, -32(%rdx)
	movq	-8(%rax), %rcx
	movups	%xmm2, -24(%rdx)
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L1914
	jmp	.L1912
	.p2align 4,,10
	.p2align 3
.L1926:
	testq	%r8, %r8
	jne	.L1900
	movq	$0, -72(%rbp)
	movl	$40, %r12d
	movq	$0, -64(%rbp)
	jmp	.L1916
	.p2align 4,,10
	.p2align 3
.L1917:
	movl	$40, %r12d
	jmp	.L1899
	.p2align 4,,10
	.p2align 3
.L1927:
	movdqu	16(%rdx), %xmm4
	movups	%xmm4, 16(%rax)
	jmp	.L1902
.L1900:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	imulq	$40, %rdi, %r12
	jmp	.L1899
.L1925:
	leaq	.LC30(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7265:
	.size	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.rodata._ZN12v8_inspector8protocol12ErrorSupport8addErrorERKNS_8String16E.str1.1,"aMS",@progbits,1
.LC31:
	.string	": "
	.section	.text._ZN12v8_inspector8protocol12ErrorSupport8addErrorERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12ErrorSupport8addErrorERKNS_8String16E
	.type	_ZN12v8_inspector8protocol12ErrorSupport8addErrorERKNS_8String16E, @function
_ZN12v8_inspector8protocol12ErrorSupport8addErrorERKNS_8String16E:
.LFB4909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movabsq	$-3689348814741910323, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movq	(%r15), %rdx
	cmpq	8(%r15), %rdx
	je	.L1936
	.p2align 4,,10
	.p2align 3
.L1931:
	leaq	(%r14,%r14,4), %rax
	movq	%r12, %rdi
	addq	$1, %r14
	leaq	(%rdx,%rax,8), %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	(%r15), %rdx
	movq	8(%r15), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rbx, %rax
	cmpq	%rax, %r14
	jnb	.L1936
	testq	%r14, %r14
	je	.L1931
	movl	$46, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendEt@PLT
	movq	(%r15), %rdx
	jmp	.L1931
	.p2align 4,,10
	.p2align 3
.L1936:
	leaq	-96(%rbp), %r14
	leaq	.LC31(%rip), %rsi
	movq	%r14, %rdi
	leaq	-80(%rbp), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1933
	call	_ZdlPv@PLT
.L1933:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	32(%r15), %rsi
	cmpq	40(%r15), %rsi
	je	.L1937
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-96(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L1952
	movq	%rax, (%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
.L1939:
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 32(%rsi)
	addq	$40, 32(%r15)
.L1940:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1930
	call	_ZdlPv@PLT
.L1930:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1953
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1937:
	.cfi_restore_state
	leaq	24(%r15), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1940
	call	_ZdlPv@PLT
	jmp	.L1940
	.p2align 4,,10
	.p2align 3
.L1952:
	movdqa	-80(%rbp), %xmm0
	movups	%xmm0, 16(%rsi)
	jmp	.L1939
.L1953:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4909:
	.size	_ZN12v8_inspector8protocol12ErrorSupport8addErrorERKNS_8String16E, .-_ZN12v8_inspector8protocol12ErrorSupport8addErrorERKNS_8String16E
	.section	.text._ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc
	.type	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc, @function
_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc:
.LFB4908:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorERKNS_8String16E
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1954
	call	_ZdlPv@PLT
.L1954:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1958
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1958:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4908:
	.size	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc, .-_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc
	.section	.rodata._ZN12v8_inspector8protocol6Object9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC32:
	.string	"object expected"
	.section	.text._ZN12v8_inspector8protocol6Object9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol6Object9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol6Object9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol6Object9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB5068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1960
	cmpl	$6, 8(%rsi)
	jne	.L1960
	movq	(%rsi), %rax
	leaq	-88(%rbp), %rdi
	call	*88(%rax)
	movq	-88(%rbp), %r13
	movl	$8, %edi
	call	_Znwm@PLT
	movq	%r13, (%rax)
	movq	%rax, (%r12)
.L1959:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1969
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1960:
	.cfi_restore_state
	leaq	-80(%rbp), %r14
	leaq	.LC32(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorERKNS_8String16E
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1962
	call	_ZdlPv@PLT
.L1962:
	movq	$0, (%r12)
	jmp	.L1959
.L1969:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5068:
	.size	_ZN12v8_inspector8protocol6Object9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol6Object9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol12ErrorSupport4pushEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12ErrorSupport4pushEv
	.type	_ZN12v8_inspector8protocol12ErrorSupport4pushEv, @function
_ZN12v8_inspector8protocol12ErrorSupport4pushEv:
.LFB4906:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	-48(%rbp), %rbx
	subq	$56, %rsp
	movq	8(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	xorl	%eax, %eax
	movq	%rbx, -64(%rbp)
	movq	$0, -56(%rbp)
	movw	%ax, -48(%rbp)
	movq	$0, -32(%rbp)
	cmpq	16(%rdi), %rsi
	je	.L1971
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-64(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L1977
	movq	%rax, (%rsi)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rsi)
.L1973:
	movq	-56(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-32(%rbp), %rax
	movq	%rax, 32(%rsi)
	addq	$40, 8(%rdi)
.L1970:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1978
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1971:
	.cfi_restore_state
	leaq	-64(%rbp), %rdx
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	movq	-64(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1970
	call	_ZdlPv@PLT
	jmp	.L1970
	.p2align 4,,10
	.p2align 3
.L1977:
	movdqa	-48(%rbp), %xmm0
	movups	%xmm0, 16(%rsi)
	jmp	.L1973
.L1978:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4906:
	.size	_ZN12v8_inspector8protocol12ErrorSupport4pushEv, .-_ZN12v8_inspector8protocol12ErrorSupport4pushEv
	.section	.text._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.type	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, @function
_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_:
.LFB7297:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L1993
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L1988
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L1994
.L1990:
	movq	%rcx, %r14
.L1981:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1987:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1995
	testq	%rsi, %rsi
	jg	.L1983
	testq	%r15, %r15
	jne	.L1986
.L1984:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1995:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L1983
.L1986:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L1984
	.p2align 4,,10
	.p2align 3
.L1994:
	testq	%r14, %r14
	js	.L1990
	jne	.L1981
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1987
	.p2align 4,,10
	.p2align 3
.L1983:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L1984
	jmp	.L1986
	.p2align 4,,10
	.p2align 3
.L1988:
	movl	$1, %r14d
	jmp	.L1981
.L1993:
	leaq	.LC30(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7297:
	.size	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, .-_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.section	.text._ZNK12v8_inspector8protocol16FundamentalValue11writeBinaryEPSt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol16FundamentalValue11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZNK12v8_inspector8protocol16FundamentalValue11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZNK12v8_inspector8protocol16FundamentalValue11writeBinaryEPSt6vectorIhSaIhEE:
.LFB4964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	8(%rdi), %eax
	cmpl	$2, %eax
	je	.L1997
	cmpl	$3, %eax
	je	.L1998
	cmpl	$1, %eax
	je	.L2007
.L1996:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2008
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1997:
	.cfi_restore_state
	movl	16(%rdi), %edi
	call	_ZN30v8_inspector_protocol_encoding4cbor11EncodeInt32EiPSt6vectorIhSaIhEE@PLT
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L2007:
	cmpb	$0, 16(%rdi)
	jne	.L2009
	call	_ZN30v8_inspector_protocol_encoding4cbor11EncodeFalseEv@PLT
.L2002:
	movb	%al, -25(%rbp)
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L2003
	movb	%al, (%rsi)
	addq	$1, 8(%r12)
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L1998:
	movsd	16(%rdi), %xmm0
	movq	%rsi, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor12EncodeDoubleEdPSt6vectorIhSaIhEE@PLT
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L2009:
	call	_ZN30v8_inspector_protocol_encoding4cbor10EncodeTrueEv@PLT
	jmp	.L2002
	.p2align 4,,10
	.p2align 3
.L2003:
	leaq	-25(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L1996
.L2008:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4964:
	.size	_ZNK12v8_inspector8protocol16FundamentalValue11writeBinaryEPSt6vectorIhSaIhEE, .-_ZNK12v8_inspector8protocol16FundamentalValue11writeBinaryEPSt6vectorIhSaIhEE
	.section	.text._ZNK12v8_inspector8protocol5Value11writeBinaryEPSt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol5Value11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZNK12v8_inspector8protocol5Value11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZNK12v8_inspector8protocol5Value11writeBinaryEPSt6vectorIhSaIhEE:
.LFB4955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN30v8_inspector_protocol_encoding4cbor10EncodeNullEv@PLT
	movq	8(%r12), %rsi
	movb	%al, -25(%rbp)
	cmpq	16(%r12), %rsi
	je	.L2011
	movb	%al, (%rsi)
	addq	$1, 8(%r12)
.L2010:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2015
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2011:
	.cfi_restore_state
	leaq	-25(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L2010
.L2015:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4955:
	.size	_ZNK12v8_inspector8protocol5Value11writeBinaryEPSt6vectorIhSaIhEE, .-_ZNK12v8_inspector8protocol5Value11writeBinaryEPSt6vectorIhSaIhEE
	.section	.text._ZNK12v8_inspector8protocol9ListValue11writeBinaryEPSt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol9ListValue11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZNK12v8_inspector8protocol9ListValue11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZNK12v8_inspector8protocol9ListValue11writeBinaryEPSt6vectorIhSaIhEE:
.LFB5047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-48(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPSt6vectorIhSaIhEE@PLT
	call	_ZN30v8_inspector_protocol_encoding4cbor32EncodeIndefiniteLengthArrayStartEv@PLT
	movq	8(%r14), %rsi
	movb	%al, -49(%rbp)
	cmpq	16(%r14), %rsi
	je	.L2017
	movb	%al, (%rsi)
	addq	$1, 8(%r14)
.L2018:
	movq	16(%r13), %rdx
	cmpq	%rdx, 24(%r13)
	je	.L2019
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L2020:
	movq	(%rdx,%rbx,8), %rdi
	movq	%r14, %rsi
	addq	$1, %rbx
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	16(%r13), %rdx
	movq	24(%r13), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L2020
.L2019:
	call	_ZN30v8_inspector_protocol_encoding4cbor10EncodeStopEv@PLT
	movq	8(%r14), %rsi
	movb	%al, -49(%rbp)
	cmpq	16(%r14), %rsi
	je	.L2021
	movb	%al, (%rsi)
	addq	$1, 8(%r14)
.L2022:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder10EncodeStopEPSt6vectorIhSaIhEE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2026
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2017:
	.cfi_restore_state
	leaq	-49(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L2018
	.p2align 4,,10
	.p2align 3
.L2021:
	leaq	-49(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L2022
.L2026:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5047:
	.size	_ZNK12v8_inspector8protocol9ListValue11writeBinaryEPSt6vectorIhSaIhEE, .-_ZNK12v8_inspector8protocol9ListValue11writeBinaryEPSt6vectorIhSaIhEE
	.section	.text._ZNK12v8_inspector8protocol15DictionaryValue11writeBinaryEPSt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15DictionaryValue11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZNK12v8_inspector8protocol15DictionaryValue11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZNK12v8_inspector8protocol15DictionaryValue11writeBinaryEPSt6vectorIhSaIhEE:
.LFB5015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-64(%rbp), %rax
	movq	$0, -64(%rbp)
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder11EncodeStartEPSt6vectorIhSaIhEE@PLT
	call	_ZN30v8_inspector_protocol_encoding4cbor30EncodeIndefiniteLengthMapStartEv@PLT
	movq	8(%r12), %rsi
	movb	%al, -65(%rbp)
	cmpq	16(%r12), %rsi
	je	.L2028
	movb	%al, (%rsi)
	addq	$1, 8(%r12)
.L2029:
	leaq	16(%r13), %rax
	movq	72(%r13), %rdx
	xorl	%ebx, %ebx
	movq	%rax, -88(%rbp)
	cmpq	80(%r13), %rdx
	jne	.L2035
	jmp	.L2036
	.p2align 4,,10
	.p2align 3
.L2033:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2034
	movq	%r12, %rdx
	call	_ZN30v8_inspector_protocol_encoding4cbor15EncodeFromUTF16ENS_4spanItEEPSt6vectorIhSaIhEE@PLT
.L2034:
	movq	48(%r14), %rdi
	movq	%r12, %rsi
	addq	$1, %rbx
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	72(%r13), %rdx
	movq	80(%r13), %rax
	movabsq	$-3689348814741910323, %rcx
	subq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rax, %rbx
	jnb	.L2036
.L2035:
	leaq	(%rbx,%rbx,4), %rax
	movq	-88(%rbp), %rdi
	leaq	(%rdx,%rax,8), %r15
	movq	%r15, %rsi
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	8(%r15), %rsi
	movq	%rax, %r14
	testq	%rsi, %rsi
	jne	.L2033
	movq	%r12, %rdx
	xorl	%edi, %edi
	call	_ZN30v8_inspector_protocol_encoding4cbor13EncodeString8ENS_4spanIhEEPSt6vectorIhSaIhEE@PLT
	jmp	.L2034
	.p2align 4,,10
	.p2align 3
.L2036:
	call	_ZN30v8_inspector_protocol_encoding4cbor10EncodeStopEv@PLT
	movq	8(%r12), %rsi
	movb	%al, -65(%rbp)
	cmpq	16(%r12), %rsi
	je	.L2046
	movb	%al, (%rsi)
	addq	$1, 8(%r12)
.L2037:
	movq	-96(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN30v8_inspector_protocol_encoding4cbor15EnvelopeEncoder10EncodeStopEPSt6vectorIhSaIhEE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2047
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2028:
	.cfi_restore_state
	leaq	-65(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L2029
	.p2align 4,,10
	.p2align 3
.L2046:
	leaq	-65(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L2037
.L2047:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5015:
	.size	_ZNK12v8_inspector8protocol15DictionaryValue11writeBinaryEPSt6vectorIhSaIhEE, .-_ZNK12v8_inspector8protocol15DictionaryValue11writeBinaryEPSt6vectorIhSaIhEE
	.section	.text._ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_,"axG",@progbits,_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	.type	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_, @function
_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_:
.LFB7317:
	.cfi_startproc
	endbr64
	movq	32(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L2049
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %r8
	cmpq	%r8, %rax
	je	.L2052
	.p2align 4,,10
	.p2align 3
.L2051:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	movsbq	-2(%rax), %rdx
	addq	%rdx, %rcx
	movq	%rcx, 32(%rsi)
	cmpq	%rax, %r8
	jne	.L2051
	testq	%rcx, %rcx
	je	.L2052
.L2049:
	movq	8(%rdi), %r9
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	%r9
	movq	(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2079
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2147483648, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rax), %rdi
	movabsq	$-2147483649, %rbx
	movq	56(%rdi), %r8
.L2058:
	cmpq	%rcx, %r8
	je	.L2082
.L2054:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2053
	movq	56(%rdi), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%r9
	cmpq	%rdx, %r10
	je	.L2058
.L2053:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2082:
	.cfi_restore_state
	movq	8(%rsi), %rdx
	movq	16(%rdi), %r14
	movq	8(%rdi), %r12
	movq	(%rsi), %r13
	cmpq	%r14, %rdx
	movq	%r14, %r8
	cmovbe	%rdx, %r8
	testq	%r8, %r8
	je	.L2055
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2056:
	movzwl	(%r12,%rax,2), %r15d
	cmpw	%r15w, 0(%r13,%rax,2)
	jne	.L2054
	addq	$1, %rax
	cmpq	%r8, %rax
	jne	.L2056
.L2055:
	subq	%r14, %rdx
	cmpq	%r11, %rdx
	jge	.L2054
	cmpq	%rbx, %rdx
	jle	.L2054
	testl	%edx, %edx
	jne	.L2054
	popq	%rbx
	movq	%rdi, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2052:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	$1, 32(%rsi)
	movl	$1, %ecx
	jmp	.L2049
	.p2align 4,,10
	.p2align 3
.L2079:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7317:
	.size	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_, .-_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	.section	.text._ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB7322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movabsq	$-3689348814741910323, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r14
	movq	%rdi, -80(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$230584300921369395, %rdx
	cmpq	%rdx, %rax
	je	.L2109
	subq	%r14, %rsi
	testq	%rax, %rax
	je	.L2101
	movabsq	$9223372036854775800, %r12
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L2110
.L2085:
	movq	%r12, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rcx
	movq	%rax, -64(%rbp)
	addq	%rax, %r12
	movq	%r12, -72(%rbp)
	leaq	40(%rax), %r12
.L2100:
	movq	-64(%rbp), %r15
	movq	%rcx, -88(%rbp)
	leaq	(%r15,%rsi), %r13
	movq	(%rcx), %rsi
	leaq	16(%r13), %rdx
	movq	%r13, %rdi
	movq	%rdx, 0(%r13)
	movq	8(%rcx), %rdx
	leaq	(%rsi,%rdx,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-88(%rbp), %rcx
	movq	-56(%rbp), %rax
	movq	32(%rcx), %rdx
	movq	%rdx, 32(%r13)
	cmpq	%r14, %rax
	je	.L2087
	leaq	-40(%rax), %rdx
	leaq	16(%r14), %r13
	movabsq	$922337203685477581, %rcx
	subq	%r14, %rdx
	shrq	$3, %rdx
	imulq	%rcx, %rdx
	movabsq	$2305843009213693951, %rcx
	andq	%rcx, %rdx
	movq	%rdx, -88(%rbp)
	leaq	(%rdx,%rdx,4), %rdx
	leaq	56(%r14,%rdx,8), %r12
	.p2align 4,,10
	.p2align 3
.L2093:
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	je	.L2111
.L2088:
	movq	%rcx, (%r15)
	movq	0(%r13), %rcx
	movq	%rcx, 16(%r15)
.L2089:
	movq	-8(%r13), %rcx
	xorl	%eax, %eax
	movq	%rcx, 8(%r15)
	movq	16(%r13), %rcx
	movq	%r13, -16(%r13)
	movq	$0, -8(%r13)
	movw	%ax, 0(%r13)
	movq	%rcx, 32(%r15)
	movq	-16(%r13), %rdi
	cmpq	%r13, %rdi
	je	.L2090
	call	_ZdlPv@PLT
	addq	$40, %r13
	addq	$40, %r15
	cmpq	%r13, %r12
	jne	.L2093
.L2091:
	movq	-88(%rbp), %rax
	movq	-64(%rbp), %rsi
	leaq	10(%rax,%rax,4), %rax
	leaq	(%rsi,%rax,8), %r12
.L2087:
	movq	-56(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L2094
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L2098:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rsi, %rcx
	je	.L2112
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	addq	$40, %rax
	addq	$40, %rdx
	movq	%rcx, -24(%rdx)
	movq	-32(%rax), %rcx
	movq	%rcx, -32(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rbx, %rax
	jne	.L2098
.L2096:
	subq	-56(%rbp), %rbx
	leaq	-40(%rbx), %rax
	shrq	$3, %rax
	leaq	40(%r12,%rax,8), %r12
.L2094:
	testq	%r14, %r14
	je	.L2099
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2099:
	movq	-64(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r12, %xmm3
	movq	-72(%rbp), %rbx
	punpcklqdq	%xmm3, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2090:
	.cfi_restore_state
	addq	$40, %r13
	addq	$40, %r15
	cmpq	%r12, %r13
	je	.L2091
	leaq	16(%r15), %rcx
	movq	%rcx, (%r15)
	movq	-16(%r13), %rcx
	cmpq	%r13, %rcx
	jne	.L2088
.L2111:
	movdqu	0(%r13), %xmm1
	movups	%xmm1, 16(%r15)
	jmp	.L2089
	.p2align 4,,10
	.p2align 3
.L2112:
	movq	8(%rax), %rcx
	movdqu	16(%rax), %xmm2
	addq	$40, %rax
	addq	$40, %rdx
	movq	%rcx, -32(%rdx)
	movq	-8(%rax), %rcx
	movups	%xmm2, -24(%rdx)
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L2098
	jmp	.L2096
	.p2align 4,,10
	.p2align 3
.L2110:
	testq	%rdi, %rdi
	jne	.L2086
	movq	$0, -72(%rbp)
	movl	$40, %r12d
	movq	$0, -64(%rbp)
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2101:
	movl	$40, %r12d
	jmp	.L2085
.L2086:
	cmpq	%rdx, %rdi
	cmovbe	%rdi, %rdx
	imulq	$40, %rdx, %r12
	jmp	.L2085
.L2109:
	leaq	.LC30(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7322:
	.size	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_:
.LFB7364:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L2137
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L2128
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L2138
.L2115:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L2127:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L2117
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L2121:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2118
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*24(%rcx)
	cmpq	%r14, %r15
	jne	.L2121
.L2119:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L2117:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L2122
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L2130
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L2124:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2124
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L2125
.L2123:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L2125:
	leaq	8(%rcx,%r9), %rcx
.L2122:
	testq	%r12, %r12
	je	.L2126
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L2126:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2118:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L2121
	jmp	.L2119
	.p2align 4,,10
	.p2align 3
.L2138:
	testq	%rdi, %rdi
	jne	.L2116
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L2127
	.p2align 4,,10
	.p2align 3
.L2128:
	movl	$8, %r13d
	jmp	.L2115
.L2130:
	movq	%rcx, %rdx
	jmp	.L2123
.L2116:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L2115
.L2137:
	leaq	.LC30(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7364:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.section	.text._ZNK12v8_inspector8protocol9ListValue5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol9ListValue5cloneEv
	.type	_ZNK12v8_inspector8protocol9ListValue5cloneEv, @function
_ZNK12v8_inspector8protocol9ListValue5cloneEv:
.LFB5048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$40, %edi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	24(%r12), %r13
	movq	16(%r12), %r15
	pxor	%xmm0, %xmm0
	movq	%rax, %rbx
	movl	$7, 8(%rax)
	leaq	-64(%rbp), %r12
	leaq	16+_ZTVN12v8_inspector8protocol9ListValueE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	16(%rbx), %rax
	movq	$0, 32(%rbx)
	movq	%rax, -72(%rbp)
	movups	%xmm0, 16(%rbx)
	cmpq	%r13, %r15
	je	.L2145
	.p2align 4,,10
	.p2align 3
.L2146:
	movq	(%r15), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rdx
	call	*88(%rdx)
	movq	24(%rbx), %rsi
	cmpq	32(%rbx), %rsi
	je	.L2141
	movq	-64(%rbp), %rdx
	movq	$0, -64(%rbp)
	movq	%rdx, (%rsi)
	addq	$8, 24(%rbx)
.L2142:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2143
	movq	(%rdi), %rdx
	addq	$8, %r15
	call	*24(%rdx)
	cmpq	%r15, %r13
	jne	.L2146
.L2145:
	movq	%rbx, (%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2151
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2143:
	.cfi_restore_state
	addq	$8, %r15
	cmpq	%r15, %r13
	jne	.L2146
	jmp	.L2145
	.p2align 4,,10
	.p2align 3
.L2141:
	movq	-72(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	jmp	.L2142
.L2151:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5048:
	.size	_ZNK12v8_inspector8protocol9ListValue5cloneEv, .-_ZNK12v8_inspector8protocol9ListValue5cloneEv
	.section	.text._ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE
	.type	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE, @function
_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE:
.LFB5062:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	24(%rdi), %rsi
	cmpq	32(%rdi), %rsi
	je	.L2153
	movq	(%rdx), %rax
	movq	$0, (%rdx)
	movq	%rax, (%rsi)
	addq	$8, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L2153:
	addq	$16, %rdi
	jmp	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.cfi_endproc
.LFE5062:
	.size	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE, .-_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE
	.section	.text._ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS4_,"axG",@progbits,_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS4_
	.type	_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS4_, @function
_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS4_:
.LFB7387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r9
	movq	8(%rdi), %r8
	movq	(%rdi), %r13
	movq	%r9, %rax
	divq	%r8
	leaq	0(,%rdx,8), %r15
	leaq	0(%r13,%r15), %r14
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2167
	movq	%rdi, %rbx
	movq	(%r12), %rdi
	movq	%rdx, %r11
	movq	%r12, %r10
	movq	8(%rdi), %rcx
	jmp	.L2159
	.p2align 4,,10
	.p2align 3
.L2174:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L2167
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rcx, %rax
	divq	%r8
	cmpq	%r11, %rdx
	jne	.L2167
	movq	%rsi, %rdi
.L2159:
	cmpq	%rcx, %r9
	jne	.L2174
	movq	(%rdi), %rcx
	cmpq	%r10, %r12
	je	.L2175
	testq	%rcx, %rcx
	je	.L2161
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L2161
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rcx
.L2161:
	movq	%rcx, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2167:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2175:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L2168
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L2161
	movq	%r10, 0(%r13,%rdx,8)
	addq	(%rbx), %r15
	movq	(%r15), %rax
	movq	%r15, %r14
.L2160:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L2176
.L2162:
	movq	$0, (%r14)
	movq	(%rdi), %rcx
	jmp	.L2161
.L2168:
	movq	%r10, %rax
	jmp	.L2160
.L2176:
	movq	%rcx, 16(%rbx)
	jmp	.L2162
	.cfi_endproc
.LFE7387:
	.size	_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS4_, .-_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS4_
	.section	.text._ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD2Ev
	.type	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD2Ev, @function
_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD2Ev:
.LFB5095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L2177
	movq	%rdi, -16(%rbp)
	leaq	-16(%rbp), %rsi
	leaq	16(%rax), %rdi
	call	_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS4_
.L2177:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2184
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2184:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5095:
	.size	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD2Ev, .-_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD2Ev
	.globl	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev
	.set	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev,_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD2Ev
	.section	.text._ZN12v8_inspector8protocol14DispatcherBase8Callback12sendIfActiveESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS4_EERKNS0_16DispatchResponseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14DispatcherBase8Callback12sendIfActiveESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS4_EERKNS0_16DispatchResponseE
	.type	_ZN12v8_inspector8protocol14DispatcherBase8Callback12sendIfActiveESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS4_EERKNS0_16DispatchResponseE, @function
_ZN12v8_inspector8protocol14DispatcherBase8Callback12sendIfActiveESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS4_EERKNS0_16DispatchResponseE:
.LFB5109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L2185
	movq	%rdi, %rbx
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2185
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	leaq	-48(%rbp), %r12
	movl	16(%rbx), %esi
	movq	%r12, %rcx
	movq	%rax, -48(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2189
	movq	(%rdi), %rax
	call	*24(%rax)
.L2189:
	movq	8(%rbx), %r13
	movq	$0, 8(%rbx)
	testq	%r13, %r13
	je	.L2185
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2190
	addq	$16, %rdi
	movq	%r12, %rsi
	movq	%r13, -48(%rbp)
	call	_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS4_
.L2190:
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2185:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2205
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2205:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5109:
	.size	_ZN12v8_inspector8protocol14DispatcherBase8Callback12sendIfActiveESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS4_EERKNS0_16DispatchResponseE, .-_ZN12v8_inspector8protocol14DispatcherBase8Callback12sendIfActiveESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS4_EERKNS0_16DispatchResponseE
	.section	.text._ZN12v8_inspector8protocol14DispatcherBase8CallbackD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14DispatcherBase8CallbackD0Ev
	.type	_ZN12v8_inspector8protocol14DispatcherBase8CallbackD0Ev, @function
_ZN12v8_inspector8protocol14DispatcherBase8CallbackD0Ev:
.LFB5107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN12v8_inspector8protocol14DispatcherBase8CallbackE(%rip), %rax
	movq	%rax, (%rdi)
	movq	104(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2207
	call	_ZdlPv@PLT
.L2207:
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2208
	call	_ZdlPv@PLT
.L2208:
	movq	24(%r12), %rdi
	leaq	40(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2209
	call	_ZdlPv@PLT
.L2209:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2210
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2211
	leaq	-32(%rbp), %rsi
	addq	$16, %rdi
	movq	%r13, -32(%rbp)
	call	_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS4_
.L2211:
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2210:
	movl	$128, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2223
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2223:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5107:
	.size	_ZN12v8_inspector8protocol14DispatcherBase8CallbackD0Ev, .-_ZN12v8_inspector8protocol14DispatcherBase8CallbackD0Ev
	.section	.text._ZN12v8_inspector8protocol14DispatcherBase8Callback7disposeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14DispatcherBase8Callback7disposeEv
	.type	_ZN12v8_inspector8protocol14DispatcherBase8Callback7disposeEv, @function
_ZN12v8_inspector8protocol14DispatcherBase8Callback7disposeEv:
.LFB5108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, 8(%rdi)
	testq	%r12, %r12
	je	.L2224
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2226
	leaq	-32(%rbp), %rsi
	addq	$16, %rdi
	movq	%r12, -32(%rbp)
	call	_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS4_
.L2226:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2224:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2235
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2235:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5108:
	.size	_ZN12v8_inspector8protocol14DispatcherBase8Callback7disposeEv, .-_ZN12v8_inspector8protocol14DispatcherBase8Callback7disposeEv
	.section	.text._ZN12v8_inspector8protocol14DispatcherBase8CallbackD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14DispatcherBase8CallbackD2Ev
	.type	_ZN12v8_inspector8protocol14DispatcherBase8CallbackD2Ev, @function
_ZN12v8_inspector8protocol14DispatcherBase8CallbackD2Ev:
.LFB5105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN12v8_inspector8protocol14DispatcherBase8CallbackE(%rip), %rax
	movq	%rax, (%rdi)
	movq	104(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2237
	call	_ZdlPv@PLT
.L2237:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2238
	call	_ZdlPv@PLT
.L2238:
	movq	24(%rbx), %rdi
	leaq	40(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2239
	call	_ZdlPv@PLT
.L2239:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L2236
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2241
	leaq	-32(%rbp), %rsi
	addq	$16, %rdi
	movq	%r12, -32(%rbp)
	call	_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS4_
.L2241:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2236:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2253
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2253:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5105:
	.size	_ZN12v8_inspector8protocol14DispatcherBase8CallbackD2Ev, .-_ZN12v8_inspector8protocol14DispatcherBase8CallbackD2Ev
	.globl	_ZN12v8_inspector8protocol14DispatcherBase8CallbackD1Ev
	.set	_ZN12v8_inspector8protocol14DispatcherBase8CallbackD1Ev,_ZN12v8_inspector8protocol14DispatcherBase8CallbackD2Ev
	.section	.text._ZN12v8_inspector8protocol14DispatcherBase8Callback19fallThroughIfActiveEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14DispatcherBase8Callback19fallThroughIfActiveEv
	.type	_ZN12v8_inspector8protocol14DispatcherBase8Callback19fallThroughIfActiveEv, @function
_ZN12v8_inspector8protocol14DispatcherBase8Callback19fallThroughIfActiveEv:
.LFB5110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L2254
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L2254
	movq	%rdi, %rbx
	movq	8(%rax), %rdi
	movl	16(%rbx), %esi
	leaq	64(%rbx), %rcx
	leaq	24(%rbx), %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	8(%rbx), %r12
	movq	$0, 8(%rbx)
	testq	%r12, %r12
	je	.L2254
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2258
	leaq	-32(%rbp), %rsi
	addq	$16, %rdi
	movq	%r12, -32(%rbp)
	call	_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS4_
.L2258:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2254:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2270
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2270:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5110:
	.size	_ZN12v8_inspector8protocol14DispatcherBase8Callback19fallThroughIfActiveEv, .-_ZN12v8_inspector8protocol14DispatcherBase8Callback19fallThroughIfActiveEv
	.section	.text._ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	.type	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_, @function
_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_:
.LFB7660:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L2285
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L2280
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L2286
.L2282:
	movq	%rcx, %r14
.L2273:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L2279:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L2287
	testq	%rsi, %rsi
	jg	.L2275
	testq	%r15, %r15
	jne	.L2278
.L2276:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2287:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L2275
.L2278:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L2276
	.p2align 4,,10
	.p2align 3
.L2286:
	testq	%r14, %r14
	js	.L2282
	jne	.L2273
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L2279
	.p2align 4,,10
	.p2align 3
.L2275:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L2276
	jmp	.L2278
	.p2align 4,,10
	.p2align 3
.L2280:
	movl	$1, %r14d
	jmp	.L2273
.L2285:
	leaq	.LC30(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7660:
	.size	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_, .-_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm:
.LFB7714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	subq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, %r13
	leaq	(%r9,%rax), %r15
	subq	%rsi, %r13
	cmpq	(%rdi), %r14
	je	.L2301
	movq	16(%rdi), %rax
.L2289:
	movabsq	$2305843009213693951, %rdx
	cmpq	%rdx, %r15
	ja	.L2322
	cmpq	%rax, %r15
	jbe	.L2291
	addq	%rax, %rax
	cmpq	%rax, %r15
	jnb	.L2291
	movabsq	$4611686018427387904, %rdi
	movq	%rdx, %r15
	cmpq	%rdx, %rax
	ja	.L2292
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L2291:
	leaq	2(%r15,%r15), %rdi
.L2292:
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	testq	%r12, %r12
	movq	(%rbx), %r11
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	movq	%rax, %r10
	je	.L2294
	cmpq	$1, %r12
	je	.L2323
	movq	%r12, %rdx
	addq	%rdx, %rdx
	je	.L2294
	movq	%r11, %rsi
	movq	%rax, %rdi
	movq	%r8, -80(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r11
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L2294:
	testq	%rcx, %rcx
	je	.L2296
	testq	%r8, %r8
	je	.L2296
	leaq	(%r10,%r12,2), %rdi
	cmpq	$1, %r8
	je	.L2324
	movq	%r8, %rdx
	addq	%rdx, %rdx
	je	.L2296
	movq	%rcx, %rsi
	movq	%r8, -80(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
.L2296:
	testq	%r13, %r13
	jne	.L2325
.L2298:
	cmpq	%r11, %r14
	je	.L2300
	movq	%r11, %rdi
	movq	%r10, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r10
.L2300:
	movq	%r15, 16(%rbx)
	movq	%r10, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2325:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	addq	%r12, %r8
	leaq	(%r10,%r8,2), %rdi
	leaq	(%r11,%rax,2), %rsi
	cmpq	$1, %r13
	je	.L2326
	addq	%r13, %r13
	je	.L2298
	movq	%r13, %rdx
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
	jmp	.L2298
	.p2align 4,,10
	.p2align 3
.L2301:
	movl	$7, %eax
	jmp	.L2289
	.p2align 4,,10
	.p2align 3
.L2326:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	jmp	.L2298
	.p2align 4,,10
	.p2align 3
.L2323:
	movzwl	(%r11), %eax
	movw	%ax, (%r10)
	jmp	.L2294
	.p2align 4,,10
	.p2align 3
.L2324:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	testq	%r13, %r13
	je	.L2298
	jmp	.L2325
.L2322:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7714:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.section	.rodata._ZN12v8_inspector8protocol14UberDispatcher8dispatchEiRKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EERKNS0_15ProtocolMessageE.str1.1,"aMS",@progbits,1
.LC33:
	.string	"' wasn't found"
.LC34:
	.string	"'"
	.section	.text._ZN12v8_inspector8protocol14UberDispatcher8dispatchEiRKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EERKNS0_15ProtocolMessageE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14UberDispatcher8dispatchEiRKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EERKNS0_15ProtocolMessageE
	.type	_ZN12v8_inspector8protocol14UberDispatcher8dispatchEiRKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EERKNS0_15ProtocolMessageE, @function
_ZN12v8_inspector8protocol14UberDispatcher8dispatchEiRKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EERKNS0_15ProtocolMessageE:
.LFB5213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-240(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$280, %rsp
	movq	%r8, -304(%rbp)
	movq	(%rdx), %rsi
	movq	8(%rdx), %rdx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rax, -296(%rbp)
	movq	%r13, -240(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-296(%rbp), %rax
	leaq	16(%rbx), %rdi
	movq	%r15, %rsi
	movq	32(%rax), %rax
	movq	%rax, -208(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S1_ESaIS4_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	testq	%rax, %rax
	je	.L2328
	leaq	48(%rax), %rsi
	movq	%r15, %rdi
	movq	%rax, -296(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-296(%rbp), %rax
	movq	80(%rax), %rax
	movq	%rax, -208(%rbp)
.L2328:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol14UberDispatcher14findDispatcherERKNS_8String16E
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2380
	movq	(%r14), %rax
	movq	$0, (%r14)
	testq	%rax, %rax
	je	.L2347
	cmpl	$6, 8(%rax)
	movl	$0, %edx
	cmovne	%rdx, %rax
.L2347:
	movq	(%rdi), %r10
	movq	-304(%rbp), %rcx
	movq	%r15, %rdx
	movl	%r12d, %esi
	movq	%rax, -280(%rbp)
	leaq	-280(%rbp), %r8
	call	*24(%r10)
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2348
	movq	(%rdi), %rax
	call	*24(%rax)
.L2348:
	movq	-240(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2327
	call	_ZdlPv@PLT
.L2327:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2381
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2380:
	.cfi_restore_state
	leaq	-144(%rbp), %rdi
	leaq	.LC33(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-96(%rbp), %rax
	leaq	.LC34(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -296(%rbp)
	leaq	-272(%rbp), %r15
	leaq	-256(%rbp), %r14
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rax
	movq	%r15, %rdi
	movq	%r14, -272(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-272(%rbp), %rdx
	movl	$7, %eax
	movq	-232(%rbp), %r8
	movq	-264(%rbp), %rsi
	movq	-240(%rbp), %rcx
	cmpq	%r14, %rdx
	cmovne	-256(%rbp), %rax
	leaq	(%r8,%rsi), %r9
	cmpq	%rax, %r9
	ja	.L2331
	testq	%r8, %r8
	jne	.L2382
.L2332:
	xorl	%ecx, %ecx
	movq	%r9, -264(%rbp)
	leaq	-192(%rbp), %rdi
	movq	%r15, %rsi
	movw	%cx, (%rdx,%r9,2)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-272(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2334
	call	_ZdlPv@PLT
.L2334:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -304(%rbp)
	cmpq	%rax, %rdi
	je	.L2335
	call	_ZdlPv@PLT
.L2335:
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	movq	%r15, %rdi
	movq	%r14, -272(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-272(%rbp), %rdx
	movl	$7, %eax
	movq	-136(%rbp), %r8
	movq	-264(%rbp), %rsi
	movq	-144(%rbp), %rcx
	cmpq	%r14, %rdx
	cmovne	-256(%rbp), %rax
	leaq	(%r8,%rsi), %r9
	cmpq	%rax, %r9
	ja	.L2337
	testq	%r8, %r8
	jne	.L2383
.L2338:
	xorl	%eax, %eax
	movq	%r9, -264(%rbp)
	movq	-296(%rbp), %rdi
	movq	%r15, %rsi
	movw	%ax, (%rdx,%r9,2)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-272(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2340
	call	_ZdlPv@PLT
.L2340:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2341
	movq	-296(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$-32601, %edx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocolL21reportProtocolErrorToEPNS0_15FrontendChannelEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE.part.0
.L2341:
	movq	-96(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L2342
	call	_ZdlPv@PLT
.L2342:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2343
	call	_ZdlPv@PLT
.L2343:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2348
	call	_ZdlPv@PLT
	jmp	.L2348
	.p2align 4,,10
	.p2align 3
.L2383:
	leaq	(%rdx,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L2384
	addq	%r8, %r8
	je	.L2338
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r9, -312(%rbp)
	call	memmove@PLT
	movq	-272(%rbp), %rdx
	movq	-312(%rbp), %r9
	jmp	.L2338
	.p2align 4,,10
	.p2align 3
.L2382:
	leaq	(%rdx,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L2385
	addq	%r8, %r8
	je	.L2332
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r9, -304(%rbp)
	call	memmove@PLT
	movq	-272(%rbp), %rdx
	movq	-304(%rbp), %r9
	jmp	.L2332
	.p2align 4,,10
	.p2align 3
.L2331:
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r9, -304(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-272(%rbp), %rdx
	movq	-304(%rbp), %r9
	jmp	.L2332
	.p2align 4,,10
	.p2align 3
.L2337:
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r9, -312(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-272(%rbp), %rdx
	movq	-312(%rbp), %r9
	jmp	.L2338
	.p2align 4,,10
	.p2align 3
.L2384:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-272(%rbp), %rdx
	jmp	.L2338
	.p2align 4,,10
	.p2align 3
.L2385:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-272(%rbp), %rdx
	jmp	.L2332
.L2381:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5213:
	.size	_ZN12v8_inspector8protocol14UberDispatcher8dispatchEiRKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EERKNS0_15ProtocolMessageE, .-_ZN12v8_inspector8protocol14UberDispatcher8dispatchEiRKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EERKNS0_15ProtocolMessageE
	.section	.text._ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeISA_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeISA_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeISA_Lb1EEEm
	.type	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeISA_Lb1EEEm, @function
_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeISA_Lb1EEEm:
.LFB7840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L2410
.L2387:
	movq	%r14, 56(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L2396
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L2397:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2410:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L2411
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2412
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L2389:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L2391
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L2392
	.p2align 4,,10
	.p2align 3
.L2393:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2394:
	testq	%rsi, %rsi
	je	.L2391
.L2392:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	56(%rcx), %rax
	divq	%r13
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L2393
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L2399
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L2392
	.p2align 4,,10
	.p2align 3
.L2391:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L2395
	call	_ZdlPv@PLT
.L2395:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r15, (%rbx)
	divq	%r13
	movq	%r13, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L2387
	.p2align 4,,10
	.p2align 3
.L2396:
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L2398
	movq	56(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L2398:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L2397
	.p2align 4,,10
	.p2align 3
.L2399:
	movq	%rdx, %rdi
	jmp	.L2394
	.p2align 4,,10
	.p2align 3
.L2411:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L2389
.L2412:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7840:
	.size	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeISA_Lb1EEEm, .-_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeISA_Lb1EEEm
	.section	.text._ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_,"axG",@progbits,_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	.type	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_, @function
_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_:
.LFB7319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	32(%rsi), %r12
	testq	%r12, %r12
	jne	.L2414
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rcx
	cmpq	%rcx, %rax
	je	.L2417
	.p2align 4,,10
	.p2align 3
.L2416:
	movq	%r12, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%r12, %rdx
	movq	%rdx, %r12
	movsbq	-2(%rax), %rdx
	addq	%rdx, %r12
	movq	%r12, 32(%rbx)
	cmpq	%rax, %rcx
	jne	.L2416
	testq	%r12, %r12
	je	.L2417
.L2414:
	movq	8(%r13), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L2424
	movq	(%rax), %rcx
	movl	$2147483648, %r8d
	movq	56(%rcx), %rsi
.L2425:
	cmpq	%rsi, %r12
	je	.L2440
.L2420:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2424
	movq	56(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	je	.L2425
.L2424:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	(%rbx), %rsi
	movq	%rax, %r15
	movq	$0, (%rax)
	leaq	8(%rax), %rdi
	leaq	24(%rax), %rax
	movq	%rax, 8(%r15)
	movq	8(%rbx), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%rbx), %rax
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	$0, 48(%r15)
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	$1, %r8d
	movq	%rax, 40(%r15)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSC_10_Hash_nodeISA_Lb1EEEm
	addq	$8, %rsp
	popq	%rbx
	addq	$48, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2440:
	.cfi_restore_state
	movq	8(%rbx), %rdx
	movq	16(%rcx), %r15
	movq	8(%rcx), %r10
	movq	(%rbx), %r11
	cmpq	%r15, %rdx
	movq	%r15, %rsi
	cmovbe	%rdx, %rsi
	testq	%rsi, %rsi
	je	.L2421
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2422:
	movzwl	(%r10,%rax,2), %r9d
	cmpw	%r9w, (%r11,%rax,2)
	jne	.L2420
	addq	$1, %rax
	cmpq	%rsi, %rax
	jne	.L2422
.L2421:
	subq	%r15, %rdx
	cmpq	%r8, %rdx
	jge	.L2420
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rdx
	jle	.L2420
	testl	%edx, %edx
	jne	.L2420
	addq	$8, %rsp
	leaq	48(%rcx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2417:
	.cfi_restore_state
	movq	$1, 32(%rbx)
	movl	$1, %r12d
	jmp	.L2414
	.cfi_endproc
.LFE7319:
	.size	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_, .-_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	.section	.rodata._ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i.str1.1,"aMS",@progbits,1
.LC36:
	.string	"vector::reserve"
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i, @function
_ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i:
.LFB6826:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1001, %r8d
	jne	.L2442
.L2444:
	movq	$0, (%r12)
.L2441:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2572
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2442:
	.cfi_restore_state
	movq	%rdx, %r13
	leaq	-152(%rbp), %rax
	movq	%rsi, %rdi
	movl	%r8d, %ebx
	leaq	-144(%rbp), %r15
	movq	%rax, %rdx
	movq	%r13, %rsi
	movq	%rax, -176(%rbp)
	movq	%r15, %rcx
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$11, %eax
	ja	.L2444
	leaq	.L2446(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i,"a",@progbits
	.align 4
	.align 4
.L2446:
	.long	.L2453-.L2446
	.long	.L2444-.L2446
	.long	.L2452-.L2446
	.long	.L2444-.L2446
	.long	.L2451-.L2446
	.long	.L2450-.L2446
	.long	.L2449-.L2446
	.long	.L2448-.L2446
	.long	.L2447-.L2446
	.long	.L2444-.L2446
	.long	.L2444-.L2446
	.long	.L2444-.L2446
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i
	.p2align 4,,10
	.p2align 3
.L2447:
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol5ValueE(%rip), %rax
	movq	%rax, (%r14)
	movl	$0, 8(%r14)
.L2454:
	movq	-168(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsItEEvPKT_S5_PS5_
	movq	%r14, (%r12)
	jmp	.L2441
	.p2align 4,,10
	.p2align 3
.L2453:
	movl	$96, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rax, %r14
	movl	$6, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol15DictionaryValueE(%rip), %rax
	movq	-176(%rbp), %rdx
	movq	%rax, (%r14)
	leaq	64(%r14), %rax
	movq	-144(%rbp), %rdi
	movq	%rax, 16(%r14)
	movq	$1, 24(%r14)
	movq	$0, 32(%r14)
	movq	$0, 40(%r14)
	movl	$0x3f800000, 48(%r14)
	movq	$0, 56(%r14)
	movups	%xmm0, 64(%r14)
	movups	%xmm0, 80(%r14)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$1, %eax
	je	.L2454
	leaq	-96(%rbp), %rcx
	movq	%rcx, %rdx
	leaq	-80(%rbp), %rcx
	movq	%rcx, -184(%rbp)
	leaq	-128(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	cmpl	$4, %eax
	jne	.L2500
	movq	%r14, -200(%rbp)
	movq	-176(%rbp), %r14
	movq	%r12, -224(%rbp)
	movq	%rdx, %r12
	movl	%ebx, -212(%rbp)
	jmp	.L2498
	.p2align 4,,10
	.p2align 3
.L2509:
	cmpl	$1, %eax
	jne	.L2569
.L2510:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2512
	movq	(%rdi), %rax
	call	*24(%rax)
.L2512:
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2513
	call	_ZdlPv@PLT
.L2513:
	cmpl	$1, %ebx
	je	.L2573
	cmpl	$4, %ebx
	jne	.L2567
.L2498:
	movq	-184(%rbp), %rax
	movq	%r12, %rdx
	movq	$0, -88(%rbp)
	movq	$0, -64(%rbp)
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	movw	%ax, -80(%rbp)
	movq	-144(%rbp), %rax
	leaq	-2(%rax), %rsi
	movq	-152(%rbp), %rax
	leaq	2(%rax), %rdi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S5_PNS_8String16E.constprop.0
	testb	%al, %al
	je	.L2571
	movq	-144(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$10, %eax
	jne	.L2571
	movl	-212(%rbp), %eax
	movq	-144(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	-208(%rbp), %rdi
	leal	1(%rax), %r8d
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i
	movq	-128(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2571
	movq	-200(%rbp), %rax
	movq	%r12, %rsi
	movq	%rdx, -192(%rbp)
	movq	$0, -128(%rbp)
	leaq	16(%rax), %rdi
	movq	%rdi, -176(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	-176(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, %rbx
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	-192(%rbp), %rdx
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L2506
	movq	(%rdi), %rax
	call	*24(%rax)
.L2506:
	testq	%rbx, %rbx
	je	.L2574
.L2507:
	movq	-144(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	movl	%eax, %ebx
	cmpl	$9, %eax
	jne	.L2509
	movq	-144(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	movl	%eax, %ebx
	cmpl	$1, %eax
	jne	.L2510
.L2569:
	movq	-224(%rbp), %r12
	movq	-200(%rbp), %r14
	movq	$0, (%r12)
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2502
	movq	(%rdi), %rax
	call	*24(%rax)
.L2502:
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2499
	call	_ZdlPv@PLT
.L2499:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L2441
	.p2align 4,,10
	.p2align 3
.L2452:
	movl	$40, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, %rcx
	movq	%r13, %rsi
	movl	$7, 8(%rax)
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol9ListValueE(%rip), %rax
	movq	-144(%rbp), %r9
	movq	-176(%rbp), %r10
	movq	%rax, (%r14)
	movq	$0, 32(%r14)
	movq	%r9, %rdi
	movups	%xmm0, 16(%r14)
	movq	%r10, %rdx
	movq	%r9, -184(%rbp)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$3, %eax
	je	.L2454
	leal	1(%rbx), %eax
	movq	-184(%rbp), %r9
	leaq	-136(%rbp), %rbx
	movq	%r12, -184(%rbp)
	movl	%eax, -176(%rbp)
	leaq	-128(%rbp), %rax
	movq	%r10, %r12
	movq	%rax, -200(%rbp)
.L2489:
	movl	-176(%rbp), %r8d
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r9, %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L2575
	movq	$0, -136(%rbp)
	movq	24(%r14), %rsi
	movq	%rax, -128(%rbp)
	cmpq	32(%r14), %rsi
	je	.L2482
	movq	$0, -128(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 24(%r14)
.L2483:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2484
	movq	(%rdi), %rax
	call	*24(%rax)
.L2484:
	movq	-144(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$9, %eax
	je	.L2576
	movq	-184(%rbp), %r12
	movq	-136(%rbp), %rdi
	cmpl	$3, %eax
	jne	.L2577
	testq	%rdi, %rdi
	je	.L2454
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2451:
	movq	-144(%rbp), %rax
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %rbx
	leaq	-96(%rbp), %rdx
	movq	%rbx, -96(%rbp)
	leaq	-2(%rax), %rsi
	movq	-152(%rbp), %rax
	movq	$0, -88(%rbp)
	movw	%cx, -80(%rbp)
	leaq	2(%rax), %rdi
	movq	$0, -64(%rbp)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S5_PNS_8String16E.constprop.0
	testb	%al, %al
	jne	.L2477
	movq	-96(%rbp), %rdi
	movq	$0, (%r12)
	cmpq	%rbx, %rdi
	je	.L2441
	call	_ZdlPv@PLT
	jmp	.L2441
	.p2align 4,,10
	.p2align 3
.L2450:
	movq	-152(%rbp), %rcx
	movq	-144(%rbp), %r15
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	subq	%rcx, %r15
	sarq	%r15
	movq	%r15, %rbx
	addq	$1, %rbx
	js	.L2578
	jne	.L2456
.L2462:
	xorl	%r14d, %r14d
	leaq	-136(%rbp), %rdx
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L2457:
	movzwl	(%rbx,%r14,2), %eax
	testl	$65408, %eax
	jne	.L2579
	movb	%al, -136(%rbp)
	movq	-120(%rbp), %rsi
	cmpq	-112(%rbp), %rsi
	je	.L2465
	addq	$1, %r14
	movb	%al, (%rsi)
	addq	$1, -120(%rbp)
	cmpq	%r14, %r15
	ja	.L2457
	leaq	-136(%rbp), %rbx
.L2466:
	movq	-120(%rbp), %r14
	movq	-112(%rbp), %rax
.L2461:
	movb	$0, -136(%rbp)
	cmpq	%rax, %r14
	je	.L2468
	movb	$0, (%r14)
	addq	$1, -120(%rbp)
.L2469:
	movq	-128(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol10StringUtil8toDoubleEPKcmPb@PLT
.L2464:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2470
	movsd	%xmm0, -176(%rbp)
	call	_ZdlPv@PLT
	movsd	-176(%rbp), %xmm0
.L2470:
	cmpb	$0, -136(%rbp)
	je	.L2444
	comisd	.LC37(%rip), %xmm0
	jb	.L2472
	movsd	.LC38(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L2472
	cvttsd2sil	%xmm0, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm0, %xmm1
	jnp	.L2580
.L2472:
	movl	$24, %edi
	movsd	%xmm0, -176(%rbp)
	call	_Znwm@PLT
	movsd	-176(%rbp), %xmm0
	movl	$3, 8(%rax)
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%r14)
	movsd	%xmm0, 16(%r14)
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2449:
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$1, 8(%rax)
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%r14)
	movb	$1, 16(%r14)
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2448:
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$1, 8(%rax)
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%r14)
	movb	$0, 16(%r14)
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2465:
	leaq	-128(%rbp), %rdi
	addq	$1, %r14
	movq	%rdx, -184(%rbp)
	movq	%rdx, -176(%rbp)
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	cmpq	%r14, %r15
	movq	-176(%rbp), %rdx
	ja	.L2457
	movq	-184(%rbp), %rbx
	jmp	.L2466
	.p2align 4,,10
	.p2align 3
.L2576:
	movq	-144(%rbp), %r9
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r9, %rdi
	movq	%r9, -192(%rbp)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	movq	-192(%rbp), %r9
	cmpl	$3, %eax
	je	.L2581
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2489
	movq	(%rdi), %rax
	movq	%r9, -192(%rbp)
	call	*24(%rax)
	movq	-192(%rbp), %r9
	jmp	.L2489
	.p2align 4,,10
	.p2align 3
.L2482:
	movq	-200(%rbp), %rdx
	leaq	16(%r14), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	jmp	.L2483
	.p2align 4,,10
	.p2align 3
.L2574:
	movq	-200(%rbp), %rax
	movq	80(%rax), %rbx
	cmpq	88(%rax), %rbx
	je	.L2508
	leaq	16(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	%rax, 32(%rbx)
	movq	-200(%rbp), %rax
	addq	$40, 80(%rax)
	jmp	.L2507
	.p2align 4,,10
	.p2align 3
.L2571:
	movq	-224(%rbp), %r12
	movq	-200(%rbp), %r14
	movq	$0, (%r12)
	jmp	.L2502
	.p2align 4,,10
	.p2align 3
.L2579:
	movb	$0, -136(%rbp)
	pxor	%xmm0, %xmm0
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2575:
	movq	-184(%rbp), %r12
	movq	$0, (%r12)
.L2481:
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol9ListValueD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2490
	movq	24(%r14), %rbx
	movq	16(%r14), %r13
	leaq	16+_ZTVN12v8_inspector8protocol9ListValueE(%rip), %rax
	movq	%rax, (%r14)
	cmpq	%r13, %rbx
	je	.L2491
	.p2align 4,,10
	.p2align 3
.L2495:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2492
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*24(%rax)
	cmpq	%r13, %rbx
	jne	.L2495
.L2493:
	movq	16(%r14), %r13
.L2491:
	testq	%r13, %r13
	je	.L2496
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2496:
	movl	$40, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2441
	.p2align 4,,10
	.p2align 3
.L2492:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L2495
	jmp	.L2493
	.p2align 4,,10
	.p2align 3
.L2477:
	movl	$56, %edi
	call	_Znwm@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r14
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%r14)
	leaq	32(%r14), %rax
	leaq	16(%r14), %rdi
	movq	%rax, 16(%r14)
	movq	-88(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 48(%r14)
	cmpq	%rbx, %rdi
	je	.L2454
	call	_ZdlPv@PLT
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2456:
	movq	%rbx, %rdi
	movq	%rcx, -176(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rdx
	movq	-176(%rbp), %rcx
	movq	%rax, %r14
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L2582
	testq	%r8, %r8
	jne	.L2459
.L2460:
	movq	%r14, %xmm0
	leaq	(%r14,%rbx), %rax
	leaq	-136(%rbp), %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	testq	%r15, %r15
	jne	.L2462
	jmp	.L2461
.L2508:
	leaq	72(%rax), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L2507
.L2577:
	movq	$0, (%r12)
.L2487:
	testq	%rdi, %rdi
	je	.L2481
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2481
.L2582:
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	call	memmove@PLT
	movq	-176(%rbp), %r8
	movq	-184(%rbp), %rcx
.L2459:
	movq	%r8, %rdi
	movq	%rcx, -176(%rbp)
	call	_ZdlPv@PLT
	movq	-176(%rbp), %rcx
	jmp	.L2460
.L2567:
	movq	-200(%rbp), %r14
	movq	-224(%rbp), %r12
.L2500:
	movq	$0, (%r12)
	jmp	.L2499
.L2573:
	movq	-200(%rbp), %r14
	movq	-224(%rbp), %r12
	jmp	.L2454
.L2490:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2441
.L2468:
	leaq	-128(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	jmp	.L2469
.L2581:
	movq	-184(%rbp), %r12
	movq	-136(%rbp), %rdi
	movq	$0, (%r12)
	jmp	.L2487
.L2580:
	jne	.L2472
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$2, 8(%rax)
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%r14)
	movl	%ebx, 16(%r14)
	jmp	.L2454
.L2572:
	call	__stack_chk_fail@PLT
.L2578:
	leaq	.LC36(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6826:
	.size	_ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i, .-_ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i
	.section	.text._ZN12v8_inspector8protocol15DictionaryValue8setArrayERKNS_8String16ESt10unique_ptrINS0_9ListValueESt14default_deleteIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol15DictionaryValue8setArrayERKNS_8String16ESt10unique_ptrINS0_9ListValueESt14default_deleteIS6_EE
	.type	_ZN12v8_inspector8protocol15DictionaryValue8setArrayERKNS_8String16ESt10unique_ptrINS0_9ListValueESt14default_deleteIS6_EE, @function
_ZN12v8_inspector8protocol15DictionaryValue8setArrayERKNS_8String16ESt10unique_ptrINS0_9ListValueESt14default_deleteIS6_EE:
.LFB4991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$8, %rsp
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rax, %r13
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	(%r14), %rdx
	movq	$0, (%r14)
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L2584
	movq	(%rdi), %rax
	call	*24(%rax)
.L2584:
	testq	%r13, %r13
	je	.L2591
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2591:
	.cfi_restore_state
	movq	80(%rbx), %r13
	cmpq	88(%rbx), %r13
	je	.L2586
	leaq	16(%r13), %rax
	movq	%r13, %rdi
	movq	%rax, 0(%r13)
	movq	8(%r12), %rax
	movq	(%r12), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r12), %rax
	movq	%rax, 32(%r13)
	addq	$40, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2586:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	72(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.cfi_endproc
.LFE4991:
	.size	_ZN12v8_inspector8protocol15DictionaryValue8setArrayERKNS_8String16ESt10unique_ptrINS0_9ListValueESt14default_deleteIS6_EE, .-_ZN12v8_inspector8protocol15DictionaryValue8setArrayERKNS_8String16ESt10unique_ptrINS0_9ListValueESt14default_deleteIS6_EE
	.section	.text._ZN12v8_inspector8protocol15DictionaryValue9setObjectERKNS_8String16ESt10unique_ptrIS1_St14default_deleteIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol15DictionaryValue9setObjectERKNS_8String16ESt10unique_ptrIS1_St14default_deleteIS1_EE
	.type	_ZN12v8_inspector8protocol15DictionaryValue9setObjectERKNS_8String16ESt10unique_ptrIS1_St14default_deleteIS1_EE, @function
_ZN12v8_inspector8protocol15DictionaryValue9setObjectERKNS_8String16ESt10unique_ptrIS1_St14default_deleteIS1_EE:
.LFB4990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$8, %rsp
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rax, %r13
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	(%r14), %rdx
	movq	$0, (%r14)
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L2593
	movq	(%rdi), %rax
	call	*24(%rax)
.L2593:
	testq	%r13, %r13
	je	.L2600
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2600:
	.cfi_restore_state
	movq	80(%rbx), %r13
	cmpq	88(%rbx), %r13
	je	.L2595
	leaq	16(%r13), %rax
	movq	%r13, %rdi
	movq	%rax, 0(%r13)
	movq	8(%r12), %rax
	movq	(%r12), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r12), %rax
	movq	%rax, 32(%r13)
	addq	$40, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2595:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	72(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.cfi_endproc
.LFE4990:
	.size	_ZN12v8_inspector8protocol15DictionaryValue9setObjectERKNS_8String16ESt10unique_ptrIS1_St14default_deleteIS1_EE, .-_ZN12v8_inspector8protocol15DictionaryValue9setObjectERKNS_8String16ESt10unique_ptrIS1_St14default_deleteIS1_EE
	.section	.text._ZNK12v8_inspector8protocol15DictionaryValue5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol15DictionaryValue5cloneEv
	.type	_ZNK12v8_inspector8protocol15DictionaryValue5cloneEv, @function
_ZNK12v8_inspector8protocol15DictionaryValue5cloneEv:
.LFB5019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -152(%rbp)
	movl	$96, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	72(%r13), %rdx
	movq	%rax, %r14
	movl	$6, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol15DictionaryValueE(%rip), %rax
	movq	%rax, (%r14)
	leaq	64(%r14), %rax
	movq	%rax, 16(%r14)
	movq	$1, 24(%r14)
	movq	$0, 32(%r14)
	movq	$0, 40(%r14)
	movl	$0x3f800000, 48(%r14)
	movq	$0, 56(%r14)
	movups	%xmm0, 64(%r14)
	movups	%xmm0, 80(%r14)
	cmpq	%rdx, 80(%r13)
	je	.L2609
	leaq	16(%r13), %rax
	leaq	16(%r14), %r15
	xorl	%ebx, %ebx
	movq	%rax, -136(%rbp)
	leaq	-80(%rbp), %rax
	leaq	-96(%rbp), %r12
	movq	%rax, -120(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L2610:
	movq	-120(%rbp), %rcx
	leaq	(%rbx,%rbx,4), %rax
	movq	%r12, %rdi
	leaq	(%rdx,%rax,8), %rax
	movq	%rcx, -96(%rbp)
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	movq	%rax, -128(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-128(%rbp), %rax
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	movq	32(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	-144(%rbp), %rdi
	movq	48(%rax), %rsi
	movq	(%rsi), %rax
	call	*88(%rax)
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rax, -128(%rbp)
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	-104(%rbp), %rdx
	movq	$0, -104(%rbp)
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L2603
	movq	(%rdi), %rax
	call	*24(%rax)
.L2603:
	cmpq	$0, -128(%rbp)
	je	.L2620
.L2604:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2606
	movq	(%rdi), %rax
	call	*24(%rax)
.L2606:
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L2607
	call	_ZdlPv@PLT
	movq	72(%r13), %rdx
	movq	80(%r13), %rax
	movabsq	$-3689348814741910323, %rcx
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rax, %rbx
	jb	.L2610
.L2609:
	movq	-152(%rbp), %rax
	movq	%r14, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2621
	movq	-152(%rbp), %rax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2607:
	.cfi_restore_state
	movq	72(%r13), %rdx
	movq	80(%r13), %rax
	addq	$1, %rbx
	movabsq	$-3689348814741910323, %rcx
	subq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rbx, %rax
	ja	.L2610
	jmp	.L2609
	.p2align 4,,10
	.p2align 3
.L2620:
	movq	80(%r14), %r8
	cmpq	88(%r14), %r8
	je	.L2605
	leaq	16(%r8), %rax
	movq	%r8, %rdi
	movq	%r8, -128(%rbp)
	movq	%rax, (%r8)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	-128(%rbp), %r8
	movq	%rax, 32(%r8)
	addq	$40, 80(%r14)
	jmp	.L2604
	.p2align 4,,10
	.p2align 3
.L2605:
	leaq	72(%r14), %rdi
	movq	%r12, %rdx
	movq	%r8, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L2604
.L2621:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5019:
	.size	_ZNK12v8_inspector8protocol15DictionaryValue5cloneEv, .-_ZNK12v8_inspector8protocol15DictionaryValue5cloneEv
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_18parseMapEiPN30v8_inspector_protocol_encoding4cbor13CBORTokenizerE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector8protocol12_GLOBAL__N_18parseMapEiPN30v8_inspector_protocol_encoding4cbor13CBORTokenizerE, @function
_ZN12v8_inspector8protocol12_GLOBAL__N_18parseMapEiPN30v8_inspector_protocol_encoding4cbor13CBORTokenizerE:
.LFB4943:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -192(%rbp)
	movl	$96, %edi
	movl	%esi, -180(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movl	$6, 8(%rax)
	movq	%rax, %r13
	leaq	16+_ZTVN12v8_inspector8protocol15DictionaryValueE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	64(%r13), %rax
	movq	%rax, 16(%r13)
	movq	$1, 24(%r13)
	movq	$0, 32(%r13)
	movq	$0, 40(%r13)
	movl	$0x3f800000, 48(%r13)
	movq	$0, 56(%r13)
	movups	%xmm0, 64(%r13)
	movups	%xmm0, 80(%r13)
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv@PLT
	.p2align 4,,10
	.p2align 3
.L2652:
	movq	%r12, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv@PLT
	movq	%r12, %rdi
	cmpl	$11, %eax
	je	.L2623
.L2693:
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv@PLT
	cmpl	$13, %eax
	je	.L2679
	movq	%r12, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv@PLT
	testl	%eax, %eax
	je	.L2679
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r15, -144(%rbp)
	movq	$0, -136(%rbp)
	movw	%si, -128(%rbp)
	movq	$0, -112(%rbp)
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv@PLT
	movq	%r12, %rdi
	cmpl	$6, %eax
	je	.L2690
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv@PLT
	cmpl	$7, %eax
	jne	.L2678
	movq	%r12, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer18GetString16WireRepEv@PLT
	movq	%rax, %rsi
	testb	$1, %dl
	jne	.L2678
	leaq	-96(%rbp), %rdi
	shrq	%rdx
	leaq	-80(%rbp), %rbx
	call	_ZN12v8_inspector8String1611fromUTF16LEEPKtm@PLT
	movq	-96(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rdx
	je	.L2689
	movq	-80(%rbp), %rcx
	cmpq	%r15, %rdi
	je	.L2691
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	-128(%rbp), %rsi
	movq	%rdx, -144(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -136(%rbp)
	testq	%rdi, %rdi
	je	.L2644
.L2682:
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L2642:
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movw	%ax, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -112(%rbp)
	cmpq	%rbx, %rdi
	je	.L2645
	call	_ZdlPv@PLT
.L2645:
	movq	%r12, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv@PLT
	movl	-180(%rbp), %esi
	movq	%r12, %rdx
	leaq	-152(%rbp), %rdi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseValueEiPN30v8_inspector_protocol_encoding4cbor13CBORTokenizerE
	movq	-152(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2678
	leaq	-144(%rbp), %r14
	leaq	16(%r13), %rdi
	movq	%rdx, -176(%rbp)
	movq	%r14, %rsi
	movq	%rdi, -168(%rbp)
	movq	$0, -152(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	-168(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, %rbx
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	-176(%rbp), %rdx
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L2647
	movq	(%rdi), %rax
	call	*24(%rax)
.L2647:
	testq	%rbx, %rbx
	je	.L2692
.L2648:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2650
	movq	(%rdi), %rax
	call	*24(%rax)
.L2650:
	movq	-144(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2652
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv@PLT
	movq	%r12, %rdi
	cmpl	$11, %eax
	jne	.L2693
.L2623:
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv@PLT
	movq	-192(%rbp), %rax
	movq	%r13, (%rax)
	jmp	.L2622
	.p2align 4,,10
	.p2align 3
.L2690:
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer10GetString8Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rbx
	movq	%rax, %rsi
	call	_ZN12v8_inspector8String168fromUTF8EPKcm@PLT
	movq	-96(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rdx
	je	.L2689
	movq	-80(%rbp), %rcx
	cmpq	%r15, %rdi
	je	.L2694
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	-128(%rbp), %rsi
	movq	%rdx, -144(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -136(%rbp)
	testq	%rdi, %rdi
	jne	.L2682
.L2644:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L2642
	.p2align 4,,10
	.p2align 3
.L2689:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L2640
	cmpq	$1, %rax
	je	.L2695
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L2640
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	-144(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L2640:
	xorl	%ecx, %ecx
	movq	%rax, -136(%rbp)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L2642
	.p2align 4,,10
	.p2align 3
.L2692:
	movq	80(%r13), %rbx
	cmpq	88(%r13), %rbx
	je	.L2649
	leaq	16(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	movq	%rax, 32(%rbx)
	addq	$40, 80(%r13)
	jmp	.L2648
	.p2align 4,,10
	.p2align 3
.L2691:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	punpcklqdq	%xmm3, %xmm0
	movq	%rdx, -144(%rbp)
	movq	%rbx, %rdi
	movups	%xmm0, -136(%rbp)
	jmp	.L2642
	.p2align 4,,10
	.p2align 3
.L2694:
	movq	%rax, %xmm0
	movq	%rcx, %xmm4
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	punpcklqdq	%xmm4, %xmm0
	movq	%rdx, -144(%rbp)
	movq	%rbx, %rdi
	movups	%xmm0, -136(%rbp)
	jmp	.L2642
	.p2align 4,,10
	.p2align 3
.L2649:
	leaq	72(%r13), %rdi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L2648
	.p2align 4,,10
	.p2align 3
.L2695:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	-144(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L2640
	.p2align 4,,10
	.p2align 3
.L2678:
	movq	-192(%rbp), %rax
	movq	$0, (%rax)
	movq	-144(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2625
	call	_ZdlPv@PLT
.L2625:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
.L2622:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2696
	movq	-192(%rbp), %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2679:
	.cfi_restore_state
	movq	-192(%rbp), %rax
	movq	$0, (%rax)
	jmp	.L2625
.L2696:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4943:
	.size	_ZN12v8_inspector8protocol12_GLOBAL__N_18parseMapEiPN30v8_inspector_protocol_encoding4cbor13CBORTokenizerE, .-_ZN12v8_inspector8protocol12_GLOBAL__N_18parseMapEiPN30v8_inspector_protocol_encoding4cbor13CBORTokenizerE
	.section	.text._ZN12v8_inspector8protocol5Value11parseBinaryEPKhm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol5Value11parseBinaryEPKhm
	.type	_ZN12v8_inspector8protocol5Value11parseBinaryEPKhm, @function
_ZN12v8_inspector8protocol5Value11parseBinaryEPKhm:
.LFB4948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	jne	.L2698
.L2711:
	movq	$0, (%r12)
.L2697:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2712
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2698:
	.cfi_restore_state
	movzbl	(%rsi), %ebx
	movq	%rsi, %r14
	movq	%rdx, %r13
	call	_ZN30v8_inspector_protocol_encoding4cbor22InitialByteForEnvelopeEv@PLT
	cmpb	%al, %bl
	jne	.L2711
	leaq	-128(%rbp), %r15
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerC1ENS_4spanIhEE@PLT
	movq	%r15, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv@PLT
	testl	%eax, %eax
	jne	.L2701
.L2710:
	movq	$0, (%r12)
.L2702:
	movq	%r15, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizerD1Ev@PLT
	jmp	.L2697
	.p2align 4,,10
	.p2align 3
.L2701:
	movq	%r15, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13EnterEnvelopeEv@PLT
	movq	%r15, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv@PLT
	cmpl	$9, %eax
	jne	.L2710
	leaq	-136(%rbp), %rdi
	movq	%r15, %rdx
	movl	$1, %esi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_18parseMapEiPN30v8_inspector_protocol_encoding4cbor13CBORTokenizerE
	movq	-136(%rbp), %r13
	testq	%r13, %r13
	je	.L2710
	movq	%r15, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv@PLT
	cmpl	$13, %eax
	jne	.L2705
	movq	%r13, (%r12)
	jmp	.L2702
.L2705:
	movq	%r15, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv@PLT
	movq	$0, (%r12)
	movq	%r13, %rdi
	movq	0(%r13), %rax
	call	*24(%rax)
	jmp	.L2702
.L2712:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4948:
	.size	_ZN12v8_inspector8protocol5Value11parseBinaryEPKhm, .-_ZN12v8_inspector8protocol5Value11parseBinaryEPKhm
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_110parseValueEiPN30v8_inspector_protocol_encoding4cbor13CBORTokenizerE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseValueEiPN30v8_inspector_protocol_encoding4cbor13CBORTokenizerE, @function
_ZN12v8_inspector8protocol12_GLOBAL__N_110parseValueEiPN30v8_inspector_protocol_encoding4cbor13CBORTokenizerE:
.LFB4942:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1001, %esi
	jne	.L2714
.L2717:
	movq	$0, (%r12)
.L2713:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2773
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2714:
	.cfi_restore_state
	movq	%rdx, %rdi
	movl	%esi, %ebx
	movq	%rdx, %r15
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv@PLT
	cmpl	$12, %eax
	je	.L2774
.L2716:
	movq	%r15, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv@PLT
	cmpl	$13, %eax
	ja	.L2717
	leaq	.L2719(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN12v8_inspector8protocol12_GLOBAL__N_110parseValueEiPN30v8_inspector_protocol_encoding4cbor13CBORTokenizerE,"a",@progbits
	.align 4
	.align 4
.L2719:
	.long	.L2717-.L2719
	.long	.L2729-.L2719
	.long	.L2728-.L2719
	.long	.L2727-.L2719
	.long	.L2726-.L2719
	.long	.L2725-.L2719
	.long	.L2724-.L2719
	.long	.L2723-.L2719
	.long	.L2722-.L2719
	.long	.L2721-.L2719
	.long	.L2720-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.long	.L2717-.L2719
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_110parseValueEiPN30v8_inspector_protocol_encoding4cbor13CBORTokenizerE
	.p2align 4,,10
	.p2align 3
.L2774:
	movq	%r15, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer13EnterEnvelopeEv@PLT
	jmp	.L2716
	.p2align 4,,10
	.p2align 3
.L2721:
	leaq	-104(%rbp), %rdi
	leal	1(%rbx), %esi
	movq	%r15, %rdx
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_18parseMapEiPN30v8_inspector_protocol_encoding4cbor13CBORTokenizerE
	movq	-104(%rbp), %rax
	movq	%rax, (%r12)
	jmp	.L2713
	.p2align 4,,10
	.p2align 3
.L2720:
	movq	%r15, %rdi
	addl	$1, %ebx
	leaq	-112(%rbp), %r14
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv@PLT
	movl	$40, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	$7, 8(%rax)
	movq	%rax, %r13
	leaq	16+_ZTVN12v8_inspector8protocol9ListValueE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	$0, 32(%r13)
	movups	%xmm0, 16(%r13)
.L2739:
	movq	%r15, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv@PLT
	movq	%r15, %rdi
	cmpl	$11, %eax
	je	.L2733
.L2775:
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv@PLT
	cmpl	$13, %eax
	je	.L2734
	movq	%r15, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv@PLT
	testl	%eax, %eax
	je	.L2734
	movq	%r15, %rdx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseValueEiPN30v8_inspector_protocol_encoding4cbor13CBORTokenizerE
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L2734
	movq	$0, -112(%rbp)
	movq	24(%r13), %rsi
	movq	%rax, -104(%rbp)
	cmpq	32(%r13), %rsi
	je	.L2735
	movq	$0, -104(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 24(%r13)
.L2736:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2737
	movq	(%rdi), %rax
	call	*24(%rax)
.L2737:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2739
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%r15, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8TokenTagEv@PLT
	movq	%r15, %rdi
	cmpl	$11, %eax
	jne	.L2775
.L2733:
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv@PLT
.L2740:
	movq	%r13, (%r12)
	jmp	.L2713
	.p2align 4,,10
	.p2align 3
.L2729:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%r15, %rdi
	movl	$1, 8(%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%rbx)
	movb	$1, 16(%rbx)
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv@PLT
	movq	%rbx, (%r12)
	jmp	.L2713
	.p2align 4,,10
	.p2align 3
.L2728:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%r15, %rdi
	movl	$1, 8(%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%rbx)
	movb	$0, 16(%rbx)
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv@PLT
	movq	%rbx, (%r12)
	jmp	.L2713
	.p2align 4,,10
	.p2align 3
.L2727:
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol5ValueE(%rip), %rax
	movq	%rax, (%rbx)
	movl	$0, 8(%rbx)
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv@PLT
	movq	%rbx, (%r12)
	jmp	.L2713
	.p2align 4,,10
	.p2align 3
.L2726:
	movq	%r15, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer8GetInt32Ev@PLT
	movl	$24, %edi
	movl	%eax, %r13d
	call	_Znwm@PLT
	movq	%r15, %rdi
	movl	$2, 8(%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%rbx)
	movl	%r13d, 16(%rbx)
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv@PLT
	movq	%rbx, (%r12)
	jmp	.L2713
	.p2align 4,,10
	.p2align 3
.L2725:
	movq	%r15, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetDoubleEv@PLT
	movl	$24, %edi
	movsd	%xmm0, -120(%rbp)
	call	_Znwm@PLT
	movsd	-120(%rbp), %xmm0
	movq	%r15, %rdi
	movl	$3, 8(%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%rbx)
	movsd	%xmm0, 16(%rbx)
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv@PLT
	movq	%rbx, (%r12)
	jmp	.L2713
	.p2align 4,,10
	.p2align 3
.L2724:
	movq	%r15, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer10GetString8Ev@PLT
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector8String168fromUTF8EPKcm@PLT
.L2772:
	movl	$56, %edi
	call	_Znwm@PLT
	movl	$4, 8(%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	%rax, 48(%rbx)
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2732
	call	_ZdlPv@PLT
.L2732:
	movq	%r15, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv@PLT
	movq	%rbx, (%r12)
	jmp	.L2713
	.p2align 4,,10
	.p2align 3
.L2723:
	movq	%r15, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer18GetString16WireRepEv@PLT
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	shrq	%rdx
	call	_ZN12v8_inspector8String1611fromUTF16LEEPKtm@PLT
	jmp	.L2772
.L2722:
	movq	%r15, %rdi
	call	_ZNK30v8_inspector_protocol_encoding4cbor13CBORTokenizer9GetBinaryEv@PLT
	movq	%r15, %rdi
	call	_ZN30v8_inspector_protocol_encoding4cbor13CBORTokenizer4NextEv@PLT
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2735:
	leaq	-104(%rbp), %rdx
	leaq	16(%r13), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	jmp	.L2736
	.p2align 4,,10
	.p2align 3
.L2734:
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol9ListValueD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2776
	movq	24(%r13), %rbx
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol9ListValueE(%rip), %rax
	movq	%rax, 0(%r13)
	cmpq	%r14, %rbx
	je	.L2741
	.p2align 4,,10
	.p2align 3
.L2745:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2742
	movq	(%rdi), %rax
	addq	$8, %r14
	call	*24(%rax)
	cmpq	%r14, %rbx
	jne	.L2745
.L2743:
	movq	16(%r13), %r14
.L2741:
	testq	%r14, %r14
	je	.L2746
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2746:
	movq	%r13, %rdi
	movl	$40, %esi
	xorl	%r13d, %r13d
	call	_ZdlPvm@PLT
	jmp	.L2740
	.p2align 4,,10
	.p2align 3
.L2742:
	addq	$8, %r14
	cmpq	%r14, %rbx
	jne	.L2745
	jmp	.L2743
.L2776:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	*%rax
	jmp	.L2740
.L2773:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4942:
	.size	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseValueEiPN30v8_inspector_protocol_encoding4cbor13CBORTokenizerE, .-_ZN12v8_inspector8protocol12_GLOBAL__N_110parseValueEiPN30v8_inspector_protocol_encoding4cbor13CBORTokenizerE
	.section	.text._ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE
	.type	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE, @function
_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE:
.LFB4989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$8, %rsp
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rax, %r13
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	(%r14), %rdx
	movq	$0, (%r14)
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L2778
	movq	(%rdi), %rax
	call	*24(%rax)
.L2778:
	testq	%r13, %r13
	je	.L2785
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2785:
	.cfi_restore_state
	movq	80(%rbx), %r13
	cmpq	88(%rbx), %r13
	je	.L2780
	leaq	16(%r13), %rax
	movq	%r13, %rdi
	movq	%rax, 0(%r13)
	movq	8(%r12), %rax
	movq	(%r12), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r12), %rax
	movq	%rax, 32(%r13)
	addq	$40, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2780:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	72(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.cfi_endproc
.LFE4989:
	.size	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE, .-_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE
	.section	.text._ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb
	.type	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb, @function
_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb:
.LFB4985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$24, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	16(%r13), %r15
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$1, 8(%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movb	%r14b, 16(%rbx)
	movq	%rax, (%rbx)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rax, %r14
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	(%rax), %rdi
	movq	%rbx, (%rax)
	testq	%rdi, %rdi
	je	.L2787
	movq	(%rdi), %rax
	call	*24(%rax)
.L2787:
	testq	%r14, %r14
	je	.L2794
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2794:
	.cfi_restore_state
	movq	80(%r13), %r14
	cmpq	88(%r13), %r14
	je	.L2789
	leaq	16(%r14), %rax
	movq	%r14, %rdi
	movq	%rax, (%r14)
	movq	8(%r12), %rax
	movq	(%r12), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r12), %rax
	movq	%rax, 32(%r14)
	addq	$40, 80(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2789:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	72(%r13), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.cfi_endproc
.LFE4985:
	.size	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb, .-_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb
	.section	.text._ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei
	.type	_ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei, @function
_ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei:
.LFB4986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$24, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	16(%r13), %r15
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$2, 8(%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movl	%r14d, 16(%rbx)
	movq	%rax, (%rbx)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rax, %r14
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	(%rax), %rdi
	movq	%rbx, (%rax)
	testq	%rdi, %rdi
	je	.L2796
	movq	(%rdi), %rax
	call	*24(%rax)
.L2796:
	testq	%r14, %r14
	je	.L2803
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2803:
	.cfi_restore_state
	movq	80(%r13), %r14
	cmpq	88(%r13), %r14
	je	.L2798
	leaq	16(%r14), %rax
	movq	%r14, %rdi
	movq	%rax, (%r14)
	movq	8(%r12), %rax
	movq	(%r12), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r12), %rax
	movq	%rax, 32(%r14)
	addq	$40, 80(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2798:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	72(%r13), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.cfi_endproc
.LFE4986:
	.size	_ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei, .-_ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei
	.section	.text._ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_
	.type	_ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_, @function
_ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_:
.LFB4988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$56, %edi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	16(%r14), %r15
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_Znwm@PLT
	movq	(%r12), %rsi
	movq	%rax, %rbx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movq	8(%r12), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r12), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, 48(%rbx)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	(%rax), %rdi
	movq	%rbx, (%rax)
	testq	%rdi, %rdi
	je	.L2805
	movq	(%rdi), %rax
	call	*24(%rax)
.L2805:
	testq	%r12, %r12
	je	.L2812
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2812:
	.cfi_restore_state
	movq	80(%r14), %r12
	cmpq	88(%r14), %r12
	je	.L2807
	leaq	16(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movq	8(%r13), %rax
	movq	0(%r13), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r13), %rax
	movq	%rax, 32(%r12)
	addq	$40, 80(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2807:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	72(%r14), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.cfi_endproc
.LFE4988:
	.size	_ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_, .-_ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_
	.section	.text._ZN12v8_inspector8protocol15DictionaryValue9setDoubleERKNS_8String16Ed,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol15DictionaryValue9setDoubleERKNS_8String16Ed
	.type	_ZN12v8_inspector8protocol15DictionaryValue9setDoubleERKNS_8String16Ed, @function
_ZN12v8_inspector8protocol15DictionaryValue9setDoubleERKNS_8String16Ed:
.LFB4987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$24, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	leaq	16(%r13), %r15
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movsd	%xmm0, -56(%rbp)
	call	_Znwm@PLT
	movsd	-56(%rbp), %xmm0
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$3, 8(%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movsd	%xmm0, 16(%rbx)
	movq	%rax, (%rbx)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rax, %r14
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	(%rax), %rdi
	movq	%rbx, (%rax)
	testq	%rdi, %rdi
	je	.L2814
	movq	(%rdi), %rax
	call	*24(%rax)
.L2814:
	testq	%r14, %r14
	je	.L2821
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2821:
	.cfi_restore_state
	movq	80(%r13), %r14
	cmpq	88(%r13), %r14
	je	.L2816
	leaq	16(%r14), %rax
	movq	%r14, %rdi
	movq	%rax, (%r14)
	movq	8(%r12), %rax
	movq	(%r12), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	32(%r12), %rax
	movq	%rax, 32(%r14)
	addq	$40, 80(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2816:
	.cfi_restore_state
	addq	$24, %rsp
	leaq	72(%r13), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.cfi_endproc
.LFE4987:
	.size	_ZN12v8_inspector8protocol15DictionaryValue9setDoubleERKNS_8String16Ed, .-_ZN12v8_inspector8protocol15DictionaryValue9setDoubleERKNS_8String16Ed
	.section	.rodata._ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError9serializeEv.str1.1,"aMS",@progbits,1
.LC39:
	.string	"code"
.LC40:
	.string	"message"
.LC41:
	.string	"data"
.LC42:
	.string	"error"
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError9serializeEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError9serializeEv, @function
_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError9serializeEv:
.LFB5156:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%rdi, -104(%rbp)
	movl	$96, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movl	8(%rbx), %r15d
	movq	%rax, %r13
	movl	$6, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol15DictionaryValueE(%rip), %rax
	leaq	.LC39(%rip), %rsi
	movq	%rax, 0(%r13)
	leaq	64(%r13), %rax
	leaq	16(%r13), %r14
	movq	%rax, 16(%r13)
	movq	$1, 24(%r13)
	movq	$0, 32(%r13)
	movq	$0, 40(%r13)
	movl	$0x3f800000, 48(%r13)
	movq	$0, 56(%r13)
	movups	%xmm0, 64(%r13)
	movups	%xmm0, 80(%r13)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	movl	$2, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movl	%r15d, 16(%rdx)
	movq	%rax, (%rdx)
	movq	%rdx, -112(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rax, %r15
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	-112(%rbp), %rdx
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L2823
	movq	(%rdi), %rax
	call	*24(%rax)
.L2823:
	testq	%r15, %r15
	je	.L2850
.L2824:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L2826
	call	_ZdlPv@PLT
.L2826:
	leaq	.LC40(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	$56, %edi
	call	_Znwm@PLT
	movq	16(%rbx), %rsi
	movq	%rax, %r8
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%r8)
	leaq	32(%r8), %rax
	leaq	16(%r8), %rdi
	movq	%rax, 16(%r8)
	movq	24(%rbx), %rax
	movq	%r8, -112(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %r8
	movq	48(%rbx), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, 48(%r8)
	movq	%r8, -120(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rax, -112(%rbp)
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	-120(%rbp), %r8
	movq	(%rax), %rdi
	movq	%r8, (%rax)
	testq	%rdi, %rdi
	je	.L2827
	movq	(%rdi), %rax
	call	*24(%rax)
.L2827:
	cmpq	$0, -112(%rbp)
	je	.L2851
.L2828:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2830
	call	_ZdlPv@PLT
.L2830:
	cmpq	$0, 64(%rbx)
	jne	.L2852
.L2831:
	movl	$96, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	movl	$6, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol15DictionaryValueE(%rip), %rax
	movq	%rax, (%r14)
	leaq	64(%r14), %rax
	leaq	16(%r14), %r9
	movq	%rax, 16(%r14)
	movq	-104(%rbp), %rax
	movq	$1, 24(%r14)
	movq	%r14, (%rax)
	movq	$0, 32(%r14)
	movq	$0, 40(%r14)
	movl	$0x3f800000, 48(%r14)
	movq	$0, 56(%r14)
	movups	%xmm0, 64(%r14)
	movups	%xmm0, 80(%r14)
	movq	%r9, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-112(%rbp), %r9
	movq	%r12, %rsi
	movq	%r9, %rdi
	movq	%r9, -120(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	-120(%rbp), %r9
	movq	%r12, %rsi
	movq	%rax, -112(%rbp)
	movq	%r9, %rdi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	(%rax), %rdi
	movq	%r13, (%rax)
	testq	%rdi, %rdi
	je	.L2833
	movq	(%rdi), %rax
	call	*24(%rax)
.L2833:
	cmpq	$0, -112(%rbp)
	je	.L2853
.L2834:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2836
	call	_ZdlPv@PLT
.L2836:
	cmpb	$0, 100(%rbx)
	jne	.L2854
.L2822:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2855
	movq	-104(%rbp), %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2852:
	.cfi_restore_state
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	leaq	56(%rbx), %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2831
	call	_ZdlPv@PLT
	jmp	.L2831
	.p2align 4,,10
	.p2align 3
.L2854:
	movq	-104(%rbp), %rax
	movl	96(%rbx), %ebx
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2822
	call	_ZdlPv@PLT
	jmp	.L2822
	.p2align 4,,10
	.p2align 3
.L2850:
	movq	80(%r13), %r15
	cmpq	88(%r13), %r15
	je	.L2825
	leaq	16(%r15), %rax
	movq	%r15, %rdi
	movq	%rax, (%r15)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	%rax, 32(%r15)
	addq	$40, 80(%r13)
	jmp	.L2824
	.p2align 4,,10
	.p2align 3
.L2853:
	movq	80(%r14), %r13
	cmpq	88(%r14), %r13
	je	.L2835
	leaq	16(%r13), %rax
	movq	%r13, %rdi
	movq	%rax, 0(%r13)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	%rax, 32(%r13)
	addq	$40, 80(%r14)
	jmp	.L2834
	.p2align 4,,10
	.p2align 3
.L2851:
	movq	80(%r13), %r14
	cmpq	88(%r13), %r14
	je	.L2829
	leaq	16(%r14), %rax
	movq	%r14, %rdi
	movq	%rax, (%r14)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	%rax, 32(%r14)
	addq	$40, 80(%r13)
	jmp	.L2828
	.p2align 4,,10
	.p2align 3
.L2825:
	leaq	72(%r13), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L2824
	.p2align 4,,10
	.p2align 3
.L2835:
	leaq	72(%r14), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L2834
	.p2align 4,,10
	.p2align 3
.L2829:
	leaq	72(%r13), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L2828
.L2855:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5156:
	.size	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError9serializeEv, .-_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError9serializeEv
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError17serializeToBinaryEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError17serializeToBinaryEv:
.LFB5148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError9serializeEv
	movq	-32(%rbp), %rdi
	leaq	_ZN12v8_inspector8protocol5Value17serializeToBinaryEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2857
	movq	$0, 16(%r12)
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movups	%xmm0, (%r12)
	movq	(%rdi), %rax
	call	*80(%rax)
.L2858:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2856
	movq	(%rdi), %rax
	call	*24(%rax)
.L2856:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2865
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2857:
	.cfi_restore_state
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2858
.L2865:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5148:
	.size	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError17serializeToBinaryEv, .-_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError15serializeToJSONEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError15serializeToJSONEv, @function
_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError15serializeToJSONEv:
.LFB5147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	-72(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError9serializeEv
	movq	-72(%rbp), %r14
	leaq	_ZN12v8_inspector8protocol5Value15serializeToJSONEv(%rip), %rdx
	movq	(%r14), %rax
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2867
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movl	$512, %esi
	movq	%r13, %rdi
	call	_ZN12v8_inspector15String16Builder15reserveCapacityEm@PLT
	movq	(%r14), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*72(%rax)
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2869
	call	_ZdlPv@PLT
.L2869:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2866
	movq	(%rdi), %rax
	call	*24(%rax)
.L2866:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2879
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2867:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2869
.L2879:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5147:
	.size	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError15serializeToJSONEv, .-_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError15serializeToJSONEv
	.section	.rodata._ZN12v8_inspector8protocol16InternalResponse15serializeToJSONEv.str1.1,"aMS",@progbits,1
.LC43:
	.string	"params"
.LC44:
	.string	"result"
	.section	.text._ZN12v8_inspector8protocol16InternalResponse15serializeToJSONEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol16InternalResponse15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol16InternalResponse15serializeToJSONEv, @function
_ZN12v8_inspector8protocol16InternalResponse15serializeToJSONEv:
.LFB5221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	16+_ZTVN12v8_inspector8protocol15DictionaryValueE(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$168, %rsp
	movq	%rdi, -184(%rbp)
	movl	$96, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	56(%rbx), %r12
	pxor	%xmm0, %xmm0
	movl	$6, 8(%rax)
	movq	%rax, %r14
	addq	$64, %rax
	movq	%r13, -64(%rax)
	movq	%rax, 16(%r14)
	movq	$1, 24(%r14)
	movq	$0, 32(%r14)
	movq	$0, 40(%r14)
	movl	$0x3f800000, 48(%r14)
	movq	$0, 56(%r14)
	movups	%xmm0, 64(%r14)
	movups	%xmm0, 80(%r14)
	testq	%r12, %r12
	je	.L2881
	movq	$0, 56(%rbx)
.L2882:
	cmpq	$0, 24(%rbx)
	je	.L2883
	leaq	-96(%rbp), %r13
	leaq	.LC28(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	16(%rbx), %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rbx
	cmpq	%rbx, %rdi
	je	.L2884
	call	_ZdlPv@PLT
.L2884:
	movq	(%r12), %rax
	movq	%r12, %rsi
	leaq	-144(%rbp), %rdi
	call	*(%rax)
	movl	$80, %edi
	call	_Znwm@PLT
	movq	-144(%rbp), %rsi
	movq	%rax, %r15
	movl	$8, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol15SerializedValueE(%rip), %rax
	movq	%rax, (%r15)
	leaq	32(%r15), %rax
	leaq	16(%r15), %rdi
	movq	%rax, 16(%r15)
	movq	-136(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 72(%r15)
	movups	%xmm0, 56(%r15)
	leaq	.LC43(%rip), %rsi
	movq	%rax, 48(%r15)
.L2921:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	16(%r14), %rdi
	movq	%r13, %rsi
	movq	%rdi, -200(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	-200(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -192(%rbp)
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	(%rax), %rdi
	movq	%r15, (%rax)
	testq	%rdi, %rdi
	je	.L2892
	movq	(%rdi), %rax
	call	*24(%rax)
.L2892:
	cmpq	$0, -192(%rbp)
	je	.L2922
.L2893:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2895
	call	_ZdlPv@PLT
.L2895:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2890
	call	_ZdlPv@PLT
.L2890:
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol5Value15serializeToJSONEv(%rip), %rdx
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2897
	leaq	-176(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movl	$512, %esi
	movq	%r13, %rdi
	call	_ZN12v8_inspector15String16Builder15reserveCapacityEm@PLT
	movq	(%r14), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*72(%rax)
	movq	-184(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2899
	call	_ZdlPv@PLT
.L2899:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2923
	movq	-184(%rbp), %rax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2883:
	.cfi_restore_state
	movl	8(%rbx), %r15d
	leaq	-96(%rbp), %r13
	leaq	.LC26(%rip), %rsi
	movq	%r13, %rdi
	leaq	-80(%rbp), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movl	%r15d, %edx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2891
	call	_ZdlPv@PLT
.L2891:
	movq	(%r12), %rax
	movq	%r12, %rsi
	leaq	-144(%rbp), %rdi
	call	*(%rax)
	movl	$80, %edi
	call	_Znwm@PLT
	movq	-144(%rbp), %rsi
	movq	%rax, %r15
	movl	$8, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol15SerializedValueE(%rip), %rax
	movq	%rax, (%r15)
	leaq	32(%r15), %rax
	leaq	16(%r15), %rdi
	movq	%rax, 16(%r15)
	movq	-136(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-112(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 72(%r15)
	movups	%xmm0, 56(%r15)
	leaq	.LC44(%rip), %rsi
	movq	%rax, 48(%r15)
	jmp	.L2921
	.p2align 4,,10
	.p2align 3
.L2897:
	movq	-184(%rbp), %rdi
	movq	%r14, %rsi
	call	*%rax
	jmp	.L2899
	.p2align 4,,10
	.p2align 3
.L2922:
	movq	80(%r14), %r15
	cmpq	88(%r14), %r15
	je	.L2894
	leaq	16(%r15), %rax
	movq	%r15, %rdi
	movq	%rax, (%r15)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	%rax, 32(%r15)
	addq	$40, 80(%r14)
	jmp	.L2893
	.p2align 4,,10
	.p2align 3
.L2881:
	movl	$96, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	$6, 8(%rax)
	movq	%rax, %r12
	leaq	64(%rax), %rax
	movq	%r13, -64(%rax)
	movq	%rax, 16(%r12)
	movq	$1, 24(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movl	$0x3f800000, 48(%r12)
	movq	$0, 56(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	jmp	.L2882
	.p2align 4,,10
	.p2align 3
.L2894:
	leaq	72(%r14), %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L2893
.L2923:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5221:
	.size	_ZN12v8_inspector8protocol16InternalResponse15serializeToJSONEv, .-_ZN12v8_inspector8protocol16InternalResponse15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol16InternalResponse17serializeToBinaryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol16InternalResponse17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol16InternalResponse17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol16InternalResponse17serializeToBinaryEv:
.LFB5222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16+_ZTVN12v8_inspector8protocol15DictionaryValueE(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	56(%rbx), %r12
	pxor	%xmm0, %xmm0
	movl	$6, 8(%rax)
	movq	%rax, %r15
	addq	$64, %rax
	movq	%r14, -64(%rax)
	movq	%rax, 16(%r15)
	movq	$1, 24(%r15)
	movq	$0, 32(%r15)
	movq	$0, 40(%r15)
	movl	$0x3f800000, 48(%r15)
	movq	$0, 56(%r15)
	movups	%xmm0, 64(%r15)
	movups	%xmm0, 80(%r15)
	testq	%r12, %r12
	je	.L2925
	movq	$0, 56(%rbx)
.L2926:
	cmpq	$0, 24(%rbx)
	je	.L2927
	leaq	-96(%rbp), %r14
	leaq	.LC28(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	16(%rbx), %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue9setStringERKNS_8String16ES4_
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rbx
	cmpq	%rbx, %rdi
	je	.L2928
	call	_ZdlPv@PLT
.L2928:
	movq	(%r12), %rax
	movq	%r12, %rsi
	leaq	-128(%rbp), %rdi
	call	*8(%rax)
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movaps	%xmm0, -128(%rbp)
	movq	%rcx, -144(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rax, -136(%rbp)
	movq	%rcx, -152(%rbp)
	movq	$0, -112(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol15SerializedValueE(%rip), %rcx
	movq	-136(%rbp), %xmm0
	leaq	.LC43(%rip), %rsi
	movq	%rcx, (%rax)
	movq	%rax, %rdx
	xorl	%ecx, %ecx
	leaq	32(%rax), %rax
	movl	$8, -24(%rax)
	movhps	-144(%rbp), %xmm0
	movw	%cx, 32(%rdx)
	movq	-152(%rbp), %rcx
	movq	%rax, 16(%rdx)
	movq	$0, 24(%rdx)
	movq	$0, 48(%rdx)
	movq	%rcx, 72(%rdx)
	movq	%rdx, -152(%rbp)
	movups	%xmm0, 56(%rdx)
.L2967:
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	16(%r15), %rdi
	movq	%r14, %rsi
	movq	%rdi, -144(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	-144(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, -136(%rbp)
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	-152(%rbp), %rdx
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L2936
	movq	(%rdi), %rax
	call	*24(%rax)
.L2936:
	cmpq	$0, -136(%rbp)
	je	.L2968
.L2937:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2939
	call	_ZdlPv@PLT
.L2939:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2934
	call	_ZdlPv@PLT
.L2934:
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol5Value17serializeToBinaryEv(%rip), %rcx
	movq	8(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L2941
	pxor	%xmm0, %xmm0
	movq	$0, 16(%r13)
	movq	%r13, %rsi
	movq	%r15, %rdi
	movups	%xmm0, 0(%r13)
	call	*80(%rax)
.L2942:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2969
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2927:
	.cfi_restore_state
	movl	8(%rbx), %ebx
	leaq	-96(%rbp), %r14
	leaq	.LC26(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movl	%ebx, %edx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setIntegerERKNS_8String16Ei
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rbx
	cmpq	%rbx, %rdi
	je	.L2935
	call	_ZdlPv@PLT
.L2935:
	movq	(%r12), %rax
	movq	%r12, %rsi
	leaq	-128(%rbp), %rdi
	call	*8(%rax)
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movaps	%xmm0, -128(%rbp)
	movq	%rcx, -144(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rax, -136(%rbp)
	movq	%rcx, -152(%rbp)
	movq	$0, -112(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol15SerializedValueE(%rip), %rcx
	movq	-136(%rbp), %xmm0
	leaq	.LC44(%rip), %rsi
	movq	%rcx, (%rax)
	movq	%rax, %rdx
	movq	-152(%rbp), %rcx
	leaq	32(%rax), %rax
	movl	$8, -24(%rax)
	movhps	-144(%rbp), %xmm0
	movq	%rax, 16(%rdx)
	xorl	%eax, %eax
	movq	$0, 24(%rdx)
	movw	%ax, 32(%rdx)
	movq	$0, 48(%rdx)
	movq	%rcx, 72(%rdx)
	movq	%rdx, -152(%rbp)
	movups	%xmm0, 56(%rdx)
	jmp	.L2967
	.p2align 4,,10
	.p2align 3
.L2941:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L2942
	.p2align 4,,10
	.p2align 3
.L2968:
	movq	80(%r15), %r8
	cmpq	88(%r15), %r8
	je	.L2938
	leaq	16(%r8), %rax
	movq	%r8, %rdi
	movq	%rax, (%r8)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rsi
	movq	%r8, -136(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	-136(%rbp), %r8
	movq	%rax, 32(%r8)
	addq	$40, 80(%r15)
	jmp	.L2937
	.p2align 4,,10
	.p2align 3
.L2925:
	movl	$96, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	$6, 8(%rax)
	movq	%rax, %r12
	leaq	64(%rax), %rax
	movq	%r14, -64(%rax)
	movq	%rax, 16(%r12)
	movq	$1, 24(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movl	$0x3f800000, 48(%r12)
	movq	$0, 56(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	jmp	.L2926
	.p2align 4,,10
	.p2align 3
.L2938:
	leaq	72(%r15), %rdi
	movq	%r14, %rdx
	movq	%r8, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L2937
.L2969:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5222:
	.size	_ZN12v8_inspector8protocol16InternalResponse17serializeToBinaryEv, .-_ZN12v8_inspector8protocol16InternalResponse17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i, @function
_ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i:
.LFB6830:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1001, %r8d
	jne	.L2971
.L2973:
	movq	$0, (%r12)
.L2970:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3091
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2971:
	.cfi_restore_state
	movq	%rdx, %r13
	leaq	-128(%rbp), %rax
	leaq	-120(%rbp), %r15
	movq	%rsi, %rdi
	movq	%r15, %rcx
	movq	%rax, %rdx
	movq	%r13, %rsi
	movq	%rax, -152(%rbp)
	movl	%r8d, %ebx
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$11, %eax
	ja	.L2973
	leaq	.L2975(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i,"a",@progbits
	.align 4
	.align 4
.L2975:
	.long	.L2982-.L2975
	.long	.L2973-.L2975
	.long	.L2981-.L2975
	.long	.L2973-.L2975
	.long	.L2980-.L2975
	.long	.L2979-.L2975
	.long	.L2978-.L2975
	.long	.L2977-.L2975
	.long	.L2976-.L2975
	.long	.L2973-.L2975
	.long	.L2973-.L2975
	.long	.L2973-.L2975
	.section	.text._ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i
	.p2align 4,,10
	.p2align 3
.L2976:
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol5ValueE(%rip), %rax
	movq	%rax, (%r14)
	movl	$0, 8(%r14)
.L2983:
	movq	-136(%rbp), %rdx
	movq	-120(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsIhEEvPKT_S5_PS5_
	movq	%r14, (%r12)
	jmp	.L2970
	.p2align 4,,10
	.p2align 3
.L2982:
	movl	$96, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	-120(%rbp), %rdi
	movq	%r15, %rcx
	movq	%rax, %r14
	movl	$6, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol15DictionaryValueE(%rip), %rax
	movq	%r13, %rsi
	movq	%rax, (%r14)
	leaq	64(%r14), %rax
	movq	-152(%rbp), %rdx
	movq	%rax, 16(%r14)
	movq	$1, 24(%r14)
	movq	$0, 32(%r14)
	movq	$0, 40(%r14)
	movl	$0x3f800000, 48(%r14)
	movq	$0, 56(%r14)
	movups	%xmm0, 64(%r14)
	movups	%xmm0, 80(%r14)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$1, %eax
	je	.L2983
	leaq	-80(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	leaq	-96(%rbp), %rcx
	movq	%rcx, %rsi
	leaq	-104(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	cmpl	$4, %eax
	jne	.L3019
	movq	%r14, -168(%rbp)
	movq	-152(%rbp), %r14
	movq	%r12, -192(%rbp)
	movq	%rsi, %r12
	movl	%ebx, -180(%rbp)
	jmp	.L3017
	.p2align 4,,10
	.p2align 3
.L3028:
	cmpl	$1, %eax
	jne	.L3088
.L3029:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3031
	movq	(%rdi), %rax
	call	*24(%rax)
.L3031:
	movq	-96(%rbp), %rdi
	cmpq	-144(%rbp), %rdi
	je	.L3032
	call	_ZdlPv@PLT
.L3032:
	cmpl	$1, %ebx
	je	.L3092
	cmpl	$4, %ebx
	jne	.L3086
.L3017:
	movq	-144(%rbp), %rax
	movq	%r12, %rdx
	movq	$0, -88(%rbp)
	movq	$0, -64(%rbp)
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	movw	%ax, -80(%rbp)
	movq	-120(%rbp), %rax
	leaq	-1(%rax), %rsi
	movq	-128(%rbp), %rax
	leaq	1(%rax), %rdi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S5_PNS_8String16E.constprop.0
	testb	%al, %al
	je	.L3090
	movq	-120(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$10, %eax
	jne	.L3090
	movl	-180(%rbp), %eax
	movq	-120(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	-176(%rbp), %rdi
	leal	1(%rax), %r8d
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i
	movq	-104(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L3090
	movq	-168(%rbp), %rax
	movq	%r12, %rsi
	movq	%rdx, -160(%rbp)
	movq	$0, -104(%rbp)
	leaq	16(%rax), %rdi
	movq	%rdi, -152(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	-152(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, %rbx
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	-160(%rbp), %rdx
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L3025
	movq	(%rdi), %rax
	call	*24(%rax)
.L3025:
	testq	%rbx, %rbx
	je	.L3093
.L3026:
	movq	-120(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	movl	%eax, %ebx
	cmpl	$9, %eax
	jne	.L3028
	movq	-120(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	movl	%eax, %ebx
	cmpl	$1, %eax
	jne	.L3029
.L3088:
	movq	-192(%rbp), %r12
	movq	-168(%rbp), %r14
	movq	$0, (%r12)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3021
	movq	(%rdi), %rax
	call	*24(%rax)
.L3021:
	movq	-96(%rbp), %rdi
	cmpq	-144(%rbp), %rdi
	je	.L3018
	call	_ZdlPv@PLT
.L3018:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L2970
	.p2align 4,,10
	.p2align 3
.L2981:
	movl	$40, %edi
	call	_Znwm@PLT
	movq	-120(%rbp), %r9
	pxor	%xmm0, %xmm0
	movq	%r15, %rcx
	movl	$7, 8(%rax)
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol9ListValueE(%rip), %rax
	movq	%r13, %rsi
	movq	-152(%rbp), %r10
	movq	%rax, (%r14)
	movq	%r9, %rdi
	movq	$0, 32(%r14)
	movups	%xmm0, 16(%r14)
	movq	%r10, %rdx
	movq	%r9, -152(%rbp)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$3, %eax
	je	.L2983
	leal	1(%rbx), %eax
	movq	-152(%rbp), %r9
	leaq	-112(%rbp), %rbx
	movq	%r12, -152(%rbp)
	movl	%eax, -144(%rbp)
	leaq	-104(%rbp), %rax
	movq	%r10, %r12
	movq	%rax, -168(%rbp)
.L3008:
	movl	-144(%rbp), %r8d
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r9, %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L3094
	movq	$0, -112(%rbp)
	movq	24(%r14), %rsi
	movq	%rax, -104(%rbp)
	cmpq	32(%r14), %rsi
	je	.L3001
	movq	$0, -104(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 24(%r14)
.L3002:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3003
	movq	(%rdi), %rax
	call	*24(%rax)
.L3003:
	movq	-120(%rbp), %rdi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$9, %eax
	je	.L3095
	movq	-152(%rbp), %r12
	movq	-112(%rbp), %rdi
	cmpl	$3, %eax
	jne	.L3096
	testq	%rdi, %rdi
	je	.L2983
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2983
	.p2align 4,,10
	.p2align 3
.L2980:
	movq	-120(%rbp), %rax
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %rbx
	leaq	-96(%rbp), %rdx
	movq	%rbx, -96(%rbp)
	leaq	-1(%rax), %rsi
	movq	-128(%rbp), %rax
	movq	$0, -88(%rbp)
	movw	%cx, -80(%rbp)
	leaq	1(%rax), %rdi
	movq	$0, -64(%rbp)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S5_PNS_8String16E.constprop.0
	testb	%al, %al
	jne	.L2996
	movq	-96(%rbp), %rdi
	movq	$0, (%r12)
	cmpq	%rbx, %rdi
	je	.L2970
	call	_ZdlPv@PLT
	jmp	.L2970
	.p2align 4,,10
	.p2align 3
.L2979:
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %r15
	leaq	-80(%rbp), %rbx
	movq	%rbx, -144(%rbp)
	movq	%rax, %r14
	movq	%rbx, -96(%rbp)
	subq	%r15, %r14
	testq	%r15, %r15
	jne	.L2984
	testq	%rax, %rax
	jne	.L3097
.L2984:
	movq	%r14, -104(%rbp)
	cmpq	$15, %r14
	ja	.L3098
	cmpq	$1, %r14
	jne	.L2987
	movzbl	(%r15), %eax
	movq	-144(%rbp), %rdx
	leaq	-104(%rbp), %rbx
	movb	%al, -80(%rbp)
	movl	$1, %eax
.L2988:
	movq	%rax, -88(%rbp)
	movq	%r14, %rsi
	movb	$0, (%rdx,%rax)
	movq	-96(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZN12v8_inspector8protocol10StringUtil8toDoubleEPKcmPb@PLT
	movq	-96(%rbp), %rdi
	cmpq	-144(%rbp), %rdi
	je	.L2989
	movsd	%xmm0, -144(%rbp)
	call	_ZdlPv@PLT
	movsd	-144(%rbp), %xmm0
.L2989:
	cmpb	$0, -104(%rbp)
	je	.L2973
	comisd	.LC37(%rip), %xmm0
	jb	.L2991
	movsd	.LC38(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L2991
	cvttsd2sil	%xmm0, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm0, %xmm1
	jnp	.L3099
.L2991:
	movl	$24, %edi
	movsd	%xmm0, -144(%rbp)
	call	_Znwm@PLT
	movsd	-144(%rbp), %xmm0
	movl	$3, 8(%rax)
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%r14)
	movsd	%xmm0, 16(%r14)
	jmp	.L2983
	.p2align 4,,10
	.p2align 3
.L2978:
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$1, 8(%rax)
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%r14)
	movb	$1, 16(%r14)
	jmp	.L2983
	.p2align 4,,10
	.p2align 3
.L2977:
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$1, 8(%rax)
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%r14)
	movb	$0, 16(%r14)
	jmp	.L2983
	.p2align 4,,10
	.p2align 3
.L3095:
	movq	-120(%rbp), %r9
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r9, %rdi
	movq	%r9, -160(%rbp)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	movq	-160(%rbp), %r9
	cmpl	$3, %eax
	je	.L3100
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3008
	movq	(%rdi), %rax
	movq	%r9, -160(%rbp)
	call	*24(%rax)
	movq	-160(%rbp), %r9
	jmp	.L3008
	.p2align 4,,10
	.p2align 3
.L3001:
	movq	-168(%rbp), %rdx
	leaq	16(%r14), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	jmp	.L3002
	.p2align 4,,10
	.p2align 3
.L3093:
	movq	-168(%rbp), %rax
	movq	80(%rax), %rbx
	cmpq	88(%rax), %rbx
	je	.L3027
	leaq	16(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	%rax, 32(%rbx)
	movq	-168(%rbp), %rax
	addq	$40, 80(%rax)
	jmp	.L3026
	.p2align 4,,10
	.p2align 3
.L3090:
	movq	-192(%rbp), %r12
	movq	-168(%rbp), %r14
	movq	$0, (%r12)
	jmp	.L3021
	.p2align 4,,10
	.p2align 3
.L2987:
	testq	%r14, %r14
	jne	.L3101
	movq	-144(%rbp), %rdx
	xorl	%eax, %eax
	leaq	-104(%rbp), %rbx
	jmp	.L2988
	.p2align 4,,10
	.p2align 3
.L3094:
	movq	-152(%rbp), %r12
	movq	$0, (%r12)
.L3000:
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol9ListValueD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3009
	movq	24(%r14), %rbx
	movq	16(%r14), %r13
	leaq	16+_ZTVN12v8_inspector8protocol9ListValueE(%rip), %rax
	movq	%rax, (%r14)
	cmpq	%r13, %rbx
	je	.L3010
	.p2align 4,,10
	.p2align 3
.L3014:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3011
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*24(%rax)
	cmpq	%r13, %rbx
	jne	.L3014
.L3012:
	movq	16(%r14), %r13
.L3010:
	testq	%r13, %r13
	je	.L3015
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3015:
	movl	$40, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2970
	.p2align 4,,10
	.p2align 3
.L3011:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L3014
	jmp	.L3012
	.p2align 4,,10
	.p2align 3
.L3098:
	leaq	-104(%rbp), %rbx
	leaq	-96(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L2986:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L2988
	.p2align 4,,10
	.p2align 3
.L2996:
	movl	$56, %edi
	call	_Znwm@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r14
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%r14)
	leaq	32(%r14), %rax
	leaq	16(%r14), %rdi
	movq	%rax, 16(%r14)
	movq	-88(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 48(%r14)
	cmpq	%rbx, %rdi
	je	.L2983
	call	_ZdlPv@PLT
	jmp	.L2983
.L3027:
	leaq	72(%rax), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L3026
.L3096:
	movq	$0, (%r12)
.L3006:
	testq	%rdi, %rdi
	je	.L3000
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L3000
.L3086:
	movq	-168(%rbp), %r14
	movq	-192(%rbp), %r12
.L3019:
	movq	$0, (%r12)
	jmp	.L3018
.L3092:
	movq	-168(%rbp), %r14
	movq	-192(%rbp), %r12
	jmp	.L2983
.L3009:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2970
.L3100:
	movq	-152(%rbp), %r12
	movq	-112(%rbp), %rdi
	movq	$0, (%r12)
	jmp	.L3006
.L3099:
	jne	.L2991
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$2, 8(%rax)
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%r14)
	movl	%ebx, 16(%r14)
	jmp	.L2983
.L3091:
	call	__stack_chk_fail@PLT
.L3097:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L3101:
	movq	-144(%rbp), %rdi
	leaq	-104(%rbp), %rbx
	jmp	.L2986
	.cfi_endproc
.LFE6830:
	.size	_ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i, .-_ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i
	.section	.text._ZN12v8_inspector8protocol19parseJSONCharactersEPKhj,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector8protocol19parseJSONCharactersEPKhj
	.type	_ZN12v8_inspector8protocol19parseJSONCharactersEPKhj, @function
_ZN12v8_inspector8protocol19parseJSONCharactersEPKhj:
.LFB5253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-120(%rbp), %r13
	pushq	%r12
	movq	%r13, %rcx
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	movq	%r15, %rdx
	addq	%rsi, %rbx
	movq	%rbx, %rsi
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$8, %eax
	ja	.L3103
	leaq	.L3105(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN12v8_inspector8protocol19parseJSONCharactersEPKhj,"a",@progbits
	.align 4
	.align 4
.L3105:
	.long	.L3111-.L3105
	.long	.L3103-.L3105
	.long	.L3110-.L3105
	.long	.L3103-.L3105
	.long	.L3109-.L3105
	.long	.L3108-.L3105
	.long	.L3107-.L3105
	.long	.L3106-.L3105
	.long	.L3104-.L3105
	.section	.text._ZN12v8_inspector8protocol19parseJSONCharactersEPKhj
	.p2align 4,,10
	.p2align 3
.L3134:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3129
	movq	(%rdi), %rax
	call	*24(%rax)
.L3129:
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol9ListValueD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3139
	movq	24(%r12), %rbx
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol9ListValueE(%rip), %rax
	movq	%rax, (%r12)
	cmpq	%r13, %rbx
	je	.L3140
	.p2align 4,,10
	.p2align 3
.L3144:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3141
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*24(%rax)
	cmpq	%r13, %rbx
	jne	.L3144
.L3142:
	movq	16(%r12), %r13
.L3140:
	testq	%r13, %r13
	je	.L3145
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3145:
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L3103:
	movq	$0, (%r14)
.L3102:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3225
	addq	$152, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3104:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16+_ZTVN12v8_inspector8protocol5ValueE(%rip), %rax
	movq	%rax, (%r12)
	leaq	-104(%rbp), %rax
	movl	$0, 8(%r12)
	movq	%rax, -136(%rbp)
.L3112:
	movq	-136(%rbp), %rdx
	movq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsIhEEvPKT_S5_PS5_
	cmpq	-104(%rbp), %rbx
	je	.L3226
	movq	(%r12), %rax
	movq	$0, (%r14)
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L3102
	.p2align 4,,10
	.p2align 3
.L3111:
	movl	$96, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	-120(%rbp), %rdi
	movq	%r13, %rcx
	movq	%rax, %r12
	movl	$6, 8(%rax)
	movq	%r15, %rdx
	movq	%rbx, %rsi
	leaq	16+_ZTVN12v8_inspector8protocol15DictionaryValueE(%rip), %rax
	movups	%xmm0, 64(%r12)
	movq	%rax, (%r12)
	leaq	64(%r12), %rax
	movq	%rax, 16(%r12)
	movq	$1, 24(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movl	$0x3f800000, 48(%r12)
	movq	$0, 56(%r12)
	movups	%xmm0, 80(%r12)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$1, %eax
	je	.L3224
	leaq	-80(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	leaq	-96(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	leaq	-104(%rbp), %rcx
	movq	%rcx, -184(%rbp)
	cmpl	$4, %eax
	je	.L3147
	jmp	.L3150
	.p2align 4,,10
	.p2align 3
.L3159:
	cmpl	$1, %eax
	jne	.L3160
.L3161:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3162
	movq	(%rdi), %rdx
	movl	%eax, -160(%rbp)
	call	*24(%rdx)
	movl	-160(%rbp), %eax
.L3162:
	movq	-96(%rbp), %rdi
	cmpq	-152(%rbp), %rdi
	je	.L3163
	movl	%eax, -160(%rbp)
	call	_ZdlPv@PLT
	movl	-160(%rbp), %eax
.L3163:
	cmpl	$1, %eax
	je	.L3112
	cmpl	$4, %eax
	jne	.L3150
.L3147:
	movq	-152(%rbp), %rax
	movq	-144(%rbp), %rdx
	movq	$0, -88(%rbp)
	movq	$0, -64(%rbp)
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	movw	%ax, -80(%rbp)
	movq	-120(%rbp), %rax
	leaq	-1(%rax), %rsi
	movq	-128(%rbp), %rax
	leaq	1(%rax), %rdi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S5_PNS_8String16E.constprop.0
	testb	%al, %al
	je	.L3152
	movq	-120(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$10, %eax
	jne	.L3152
	movq	-120(%rbp), %rsi
	movq	%rbx, %rdx
	movl	$1, %r8d
	movq	%r13, %rcx
	movq	-184(%rbp), %rdi
	movq	%rdi, -136(%rbp)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i
	movq	-104(%rbp), %rdx
	testq	%rdx, %rdx
	movq	%rdx, -160(%rbp)
	je	.L3152
	movq	-144(%rbp), %rsi
	leaq	16(%r12), %rdi
	movq	$0, -104(%rbp)
	movq	%rdi, -176(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	-176(%rbp), %rdi
	movq	-144(%rbp), %rsi
	movq	%rax, -168(%rbp)
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	-160(%rbp), %rdx
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L3156
	movq	(%rdi), %rax
	call	*24(%rax)
.L3156:
	cmpq	$0, -168(%rbp)
	je	.L3227
.L3157:
	movq	-120(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$9, %eax
	jne	.L3159
	movq	-120(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$1, %eax
	jne	.L3161
.L3160:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3152
	movq	(%rdi), %rax
	call	*24(%rax)
.L3152:
	movq	-96(%rbp), %rdi
	cmpq	-152(%rbp), %rdi
	je	.L3150
	call	_ZdlPv@PLT
.L3150:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L3103
	.p2align 4,,10
	.p2align 3
.L3110:
	movl	$40, %edi
	call	_Znwm@PLT
	movq	-120(%rbp), %r9
	pxor	%xmm0, %xmm0
	movq	%r13, %rcx
	movl	$7, 8(%rax)
	movq	%rax, %r12
	movq	%r15, %rdx
	movq	%rbx, %rsi
	leaq	16+_ZTVN12v8_inspector8protocol9ListValueE(%rip), %rax
	movups	%xmm0, 16(%r12)
	movq	%r9, %rdi
	movq	%rax, (%r12)
	movq	$0, 32(%r12)
	movq	%r9, -144(%rbp)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$3, %eax
	je	.L3224
	leaq	-112(%rbp), %rax
	movq	-144(%rbp), %r9
	movq	%rax, -136(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -152(%rbp)
.L3137:
	movq	-136(%rbp), %rdi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r9, %rsi
	movl	$1, %r8d
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L3129
	movq	$0, -112(%rbp)
	movq	24(%r12), %rsi
	movq	%rax, -104(%rbp)
	cmpq	32(%r12), %rsi
	je	.L3130
	movq	$0, -104(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 24(%r12)
.L3131:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3132
	movq	(%rdi), %rax
	call	*24(%rax)
.L3132:
	movq	-120(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$9, %eax
	je	.L3228
	cmpl	$3, %eax
	jne	.L3134
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3224
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L3224:
	leaq	-104(%rbp), %rax
	movq	%rax, -136(%rbp)
	jmp	.L3112
	.p2align 4,,10
	.p2align 3
.L3109:
	movq	-120(%rbp), %rax
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %r13
	leaq	-96(%rbp), %rdx
	movq	%r13, -96(%rbp)
	leaq	-1(%rax), %rsi
	movq	-128(%rbp), %rax
	movq	$0, -88(%rbp)
	movw	%cx, -80(%rbp)
	leaq	1(%rax), %rdi
	movq	$0, -64(%rbp)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S5_PNS_8String16E.constprop.0
	testb	%al, %al
	jne	.L3125
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3103
	call	_ZdlPv@PLT
	jmp	.L3103
	.p2align 4,,10
	.p2align 3
.L3108:
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %r13
	leaq	-80(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	movq	%rax, %r12
	movq	%rcx, -96(%rbp)
	subq	%r13, %r12
	testq	%r13, %r13
	jne	.L3113
	testq	%rax, %rax
	jne	.L3229
.L3113:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L3230
	cmpq	$1, %r12
	jne	.L3116
	leaq	-104(%rbp), %rcx
	movzbl	0(%r13), %eax
	movq	-152(%rbp), %rdx
	movq	%rcx, -136(%rbp)
	movb	%al, -80(%rbp)
	movl	$1, %eax
.L3117:
	movq	%rax, -88(%rbp)
	movq	%r12, %rsi
	movb	$0, (%rdx,%rax)
	movq	-136(%rbp), %rdx
	movq	-96(%rbp), %rdi
	call	_ZN12v8_inspector8protocol10StringUtil8toDoubleEPKcmPb@PLT
	movq	-96(%rbp), %rdi
	cmpq	-152(%rbp), %rdi
	je	.L3118
	movsd	%xmm0, -144(%rbp)
	call	_ZdlPv@PLT
	movsd	-144(%rbp), %xmm0
.L3118:
	cmpb	$0, -104(%rbp)
	je	.L3103
	comisd	.LC37(%rip), %xmm0
	jb	.L3120
	movsd	.LC38(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L3120
	cvttsd2sil	%xmm0, %r13d
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%r13d, %xmm1
	ucomisd	%xmm0, %xmm1
	jnp	.L3231
.L3120:
	movl	$24, %edi
	movsd	%xmm0, -144(%rbp)
	call	_Znwm@PLT
	movsd	-144(%rbp), %xmm0
	movl	$3, 8(%rax)
	movq	%rax, %r12
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%r12)
	movsd	%xmm0, 16(%r12)
	jmp	.L3112
	.p2align 4,,10
	.p2align 3
.L3107:
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$1, 8(%rax)
	movq	%rax, %r12
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%r12)
	leaq	-104(%rbp), %rax
	movb	$1, 16(%r12)
	movq	%rax, -136(%rbp)
	jmp	.L3112
	.p2align 4,,10
	.p2align 3
.L3106:
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$1, 8(%rax)
	movq	%rax, %r12
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%r12)
	leaq	-104(%rbp), %rax
	movb	$0, 16(%r12)
	movq	%rax, -136(%rbp)
	jmp	.L3112
	.p2align 4,,10
	.p2align 3
.L3226:
	movq	%r12, (%r14)
	jmp	.L3102
	.p2align 4,,10
	.p2align 3
.L3228:
	movq	-120(%rbp), %r9
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r9, %rdi
	movq	%r9, -144(%rbp)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenIhEENS1_5TokenEPKT_S6_PS6_S7_
	movq	-144(%rbp), %r9
	cmpl	$3, %eax
	je	.L3134
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3137
	movq	(%rdi), %rax
	movq	%r9, -144(%rbp)
	call	*24(%rax)
	movq	-144(%rbp), %r9
	jmp	.L3137
	.p2align 4,,10
	.p2align 3
.L3130:
	movq	-152(%rbp), %rdx
	leaq	16(%r12), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	jmp	.L3131
	.p2align 4,,10
	.p2align 3
.L3227:
	movq	80(%r12), %r8
	cmpq	88(%r12), %r8
	je	.L3158
	leaq	16(%r8), %rax
	movq	%r8, %rdi
	movq	%r8, -160(%rbp)
	movq	%rax, (%r8)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	-160(%rbp), %r8
	movq	%rax, 32(%r8)
	addq	$40, 80(%r12)
	jmp	.L3157
	.p2align 4,,10
	.p2align 3
.L3141:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L3144
	jmp	.L3142
	.p2align 4,,10
	.p2align 3
.L3116:
	testq	%r12, %r12
	jne	.L3232
	leaq	-104(%rbp), %rcx
	movq	-152(%rbp), %rdx
	xorl	%eax, %eax
	movq	%rcx, -136(%rbp)
	jmp	.L3117
	.p2align 4,,10
	.p2align 3
.L3125:
	movl	$56, %edi
	call	_Znwm@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r12
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%r12)
	leaq	32(%r12), %rax
	leaq	16(%r12), %rdi
	movq	%rax, 16(%r12)
	movq	-88(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 48(%r12)
	leaq	-104(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	%r13, %rdi
	je	.L3112
	call	_ZdlPv@PLT
	jmp	.L3112
	.p2align 4,,10
	.p2align 3
.L3230:
	leaq	-104(%rbp), %rax
	leaq	-96(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L3115:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L3117
	.p2align 4,,10
	.p2align 3
.L3158:
	movq	-144(%rbp), %rdx
	leaq	72(%r12), %rdi
	movq	%r8, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L3157
	.p2align 4,,10
	.p2align 3
.L3139:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3103
.L3231:
	jne	.L3120
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$2, 8(%rax)
	movq	%rax, %r12
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%r12)
	movl	%r13d, 16(%r12)
	jmp	.L3112
.L3225:
	call	__stack_chk_fail@PLT
.L3232:
	leaq	-104(%rbp), %rax
	movq	-152(%rbp), %rdi
	movq	%rax, -136(%rbp)
	jmp	.L3115
.L3229:
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE5253:
	.size	_ZN12v8_inspector8protocol19parseJSONCharactersEPKhj, .-_ZN12v8_inspector8protocol19parseJSONCharactersEPKhj
	.section	.text._ZN12v8_inspector8protocol19parseJSONCharactersEPKtj,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector8protocol19parseJSONCharactersEPKtj
	.type	_ZN12v8_inspector8protocol19parseJSONCharactersEPKtj, @function
_ZN12v8_inspector8protocol19parseJSONCharactersEPKtj:
.LFB5252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-152(%rbp), %r15
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	movq	%r13, %rcx
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	(%rsi,%rdx,2), %rbx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$8, %eax
	ja	.L3234
	leaq	.L3236(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN12v8_inspector8protocol19parseJSONCharactersEPKtj,"a",@progbits
	.align 4
	.align 4
.L3236:
	.long	.L3242-.L3236
	.long	.L3234-.L3236
	.long	.L3241-.L3236
	.long	.L3234-.L3236
	.long	.L3240-.L3236
	.long	.L3239-.L3236
	.long	.L3238-.L3236
	.long	.L3237-.L3236
	.long	.L3235-.L3236
	.section	.text._ZN12v8_inspector8protocol19parseJSONCharactersEPKtj
.L3374:
	movq	-136(%rbp), %rdi
.L3277:
	testq	%rdi, %rdi
	je	.L3270
	movq	(%rdi), %rax
	call	*24(%rax)
.L3270:
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol9ListValueD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3281
	movq	24(%r12), %rbx
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol9ListValueE(%rip), %rax
	movq	%rax, (%r12)
	cmpq	%r13, %rbx
	je	.L3282
	.p2align 4,,10
	.p2align 3
.L3286:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3283
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*24(%rax)
	cmpq	%r13, %rbx
	jne	.L3286
.L3284:
	movq	16(%r12), %r13
.L3282:
	testq	%r13, %r13
	je	.L3287
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3287:
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L3234:
	movq	$0, (%r14)
.L3233:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3368
	addq	$184, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3235:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16+_ZTVN12v8_inspector8protocol5ValueE(%rip), %rax
	movq	%rax, (%r12)
	leaq	-128(%rbp), %rax
	movl	$0, 8(%r12)
	movq	%rax, -176(%rbp)
.L3243:
	movq	-176(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsItEEvPKT_S5_PS5_
	cmpq	-128(%rbp), %rbx
	je	.L3369
	movq	(%r12), %rax
	movq	$0, (%r14)
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L3233
	.p2align 4,,10
	.p2align 3
.L3242:
	movl	$96, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rax, %r12
	movl	$6, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol15DictionaryValueE(%rip), %rax
	movq	%rbx, %rsi
	movq	%rax, (%r12)
	leaq	64(%r12), %rax
	movq	-144(%rbp), %rdi
	movq	%rax, 16(%r12)
	movq	$1, 24(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movl	$0x3f800000, 48(%r12)
	movq	$0, 56(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$1, %eax
	je	.L3367
	leaq	-96(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	leaq	-80(%rbp), %rcx
	movq	%rcx, -184(%rbp)
	leaq	-128(%rbp), %rcx
	movq	%rcx, -216(%rbp)
	cmpl	$4, %eax
	je	.L3289
	jmp	.L3292
	.p2align 4,,10
	.p2align 3
.L3301:
	cmpl	$1, %eax
	jne	.L3302
.L3303:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3304
	movq	(%rdi), %rdx
	movl	%eax, -192(%rbp)
	call	*24(%rdx)
	movl	-192(%rbp), %eax
.L3304:
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L3305
	movl	%eax, -192(%rbp)
	call	_ZdlPv@PLT
	movl	-192(%rbp), %eax
.L3305:
	cmpl	$1, %eax
	je	.L3243
	cmpl	$4, %eax
	jne	.L3292
.L3289:
	movq	-184(%rbp), %rax
	movq	-168(%rbp), %rdx
	movq	$0, -88(%rbp)
	movq	$0, -64(%rbp)
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	movw	%ax, -80(%rbp)
	movq	-144(%rbp), %rax
	leaq	-2(%rax), %rsi
	movq	-152(%rbp), %rax
	leaq	2(%rax), %rdi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S5_PNS_8String16E.constprop.0
	testb	%al, %al
	je	.L3294
	movq	-144(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$10, %eax
	jne	.L3294
	movq	-216(%rbp), %rdi
	movq	-144(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	movl	$1, %r8d
	movq	%rdi, -176(%rbp)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i
	movq	-128(%rbp), %rdx
	testq	%rdx, %rdx
	movq	%rdx, -192(%rbp)
	je	.L3294
	movq	-168(%rbp), %rsi
	leaq	16(%r12), %rdi
	movq	$0, -128(%rbp)
	movq	%rdi, -208(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_St10unique_ptrINS0_8protocol5ValueESt14default_deleteIS6_EEESaISA_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSC_18_Mod_range_hashingENSC_20_Default_ranged_hashENSC_20_Prime_rehash_policyENSC_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS3_
	movq	-208(%rbp), %rdi
	movq	-168(%rbp), %rsi
	movq	%rax, -200(%rbp)
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_St10unique_ptrINS1_8protocol5ValueESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	-192(%rbp), %rdx
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L3298
	movq	(%rdi), %rax
	call	*24(%rax)
.L3298:
	cmpq	$0, -200(%rbp)
	je	.L3370
.L3299:
	movq	-144(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$9, %eax
	jne	.L3301
	movq	-144(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$1, %eax
	jne	.L3303
.L3302:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3294
	movq	(%rdi), %rax
	call	*24(%rax)
.L3294:
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L3292
	call	_ZdlPv@PLT
.L3292:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L3234
	.p2align 4,,10
	.p2align 3
.L3241:
	movl	$40, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rcx
	movq	%r15, %rdx
	movl	$7, 8(%rax)
	movq	%rax, %r12
	leaq	16+_ZTVN12v8_inspector8protocol9ListValueE(%rip), %rax
	movq	%rbx, %rsi
	movq	-144(%rbp), %r9
	movq	%rax, (%r12)
	movq	$0, 32(%r12)
	movups	%xmm0, 16(%r12)
	movq	%r9, %rdi
	movq	%r9, -176(%rbp)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$3, %eax
	je	.L3367
	leaq	-136(%rbp), %rax
	movq	-176(%rbp), %r9
	movq	%rax, -168(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -184(%rbp)
.L3279:
	movq	-168(%rbp), %rdi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r9, %rsi
	movl	$1, %r8d
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS0_5ValueESt14default_deleteIS4_EEPKT_SA_PSA_i
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L3270
	movq	$0, -136(%rbp)
	movq	24(%r12), %rsi
	movq	%rax, -128(%rbp)
	cmpq	32(%r12), %rsi
	je	.L3271
	movq	$0, -128(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 24(%r12)
.L3272:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3273
	movq	(%rdi), %rax
	call	*24(%rax)
.L3273:
	movq	-144(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	cmpl	$9, %eax
	je	.L3371
	movq	-136(%rbp), %rdi
	cmpl	$3, %eax
	jne	.L3277
	testq	%rdi, %rdi
	je	.L3367
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L3367:
	leaq	-128(%rbp), %rax
	movq	%rax, -176(%rbp)
	jmp	.L3243
	.p2align 4,,10
	.p2align 3
.L3240:
	movq	-144(%rbp), %rax
	xorl	%ecx, %ecx
	leaq	-80(%rbp), %r13
	leaq	-96(%rbp), %rdx
	movq	%r13, -96(%rbp)
	leaq	-2(%rax), %rsi
	movq	-152(%rbp), %rax
	movq	$0, -88(%rbp)
	movw	%cx, -80(%rbp)
	leaq	2(%rax), %rdi
	movq	$0, -64(%rbp)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S5_PNS_8String16E.constprop.0
	testb	%al, %al
	jne	.L3266
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3234
	call	_ZdlPv@PLT
	jmp	.L3234
	.p2align 4,,10
	.p2align 3
.L3239:
	movq	-152(%rbp), %r15
	movq	-144(%rbp), %r13
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	subq	%r15, %r13
	sarq	%r13
	movq	%r13, %r12
	addq	$1, %r12
	js	.L3372
	jne	.L3245
.L3251:
	xorl	%r12d, %r12d
	leaq	-136(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L3246:
	movzwl	(%r15,%r12,2), %eax
	testl	$65408, %eax
	jne	.L3373
	movb	%al, -136(%rbp)
	movq	-120(%rbp), %rsi
	cmpq	-112(%rbp), %rsi
	je	.L3254
	addq	$1, %r12
	movb	%al, (%rsi)
	addq	$1, -120(%rbp)
	cmpq	%r12, %r13
	ja	.L3246
	leaq	-136(%rbp), %rax
	movq	%rax, -168(%rbp)
.L3255:
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %rsi
.L3250:
	movb	$0, -136(%rbp)
	cmpq	%rcx, %rsi
	je	.L3257
	movb	$0, (%rcx)
	addq	$1, -120(%rbp)
.L3258:
	movq	-168(%rbp), %rdx
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol10StringUtil8toDoubleEPKcmPb@PLT
	jmp	.L3253
	.p2align 4,,10
	.p2align 3
.L3238:
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$1, 8(%rax)
	movq	%rax, %r12
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%r12)
	leaq	-128(%rbp), %rax
	movb	$1, 16(%r12)
	movq	%rax, -176(%rbp)
	jmp	.L3243
	.p2align 4,,10
	.p2align 3
.L3237:
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$1, 8(%rax)
	movq	%rax, %r12
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%r12)
	leaq	-128(%rbp), %rax
	movb	$0, 16(%r12)
	movq	%rax, -176(%rbp)
	jmp	.L3243
	.p2align 4,,10
	.p2align 3
.L3369:
	movq	%r12, (%r14)
	jmp	.L3233
	.p2align 4,,10
	.p2align 3
.L3254:
	leaq	-128(%rbp), %rdi
	addq	$1, %r12
	movq	%rdx, -168(%rbp)
	movq	%rdx, -176(%rbp)
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	cmpq	%r12, %r13
	movq	-176(%rbp), %rdx
	ja	.L3246
	jmp	.L3255
	.p2align 4,,10
	.p2align 3
.L3371:
	movq	-144(%rbp), %r9
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r9, %rdi
	movq	%r9, -176(%rbp)
	call	_ZN12v8_inspector8protocol12_GLOBAL__N_110parseTokenItEENS1_5TokenEPKT_S6_PS6_S7_
	movq	-176(%rbp), %r9
	cmpl	$3, %eax
	je	.L3374
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3279
	movq	(%rdi), %rax
	movq	%r9, -176(%rbp)
	call	*24(%rax)
	movq	-176(%rbp), %r9
	jmp	.L3279
	.p2align 4,,10
	.p2align 3
.L3271:
	movq	-184(%rbp), %rdx
	leaq	16(%r12), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	jmp	.L3272
	.p2align 4,,10
	.p2align 3
.L3370:
	movq	80(%r12), %r8
	cmpq	88(%r12), %r8
	je	.L3300
	leaq	16(%r8), %rax
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	movq	%rax, (%r8)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	-192(%rbp), %r8
	movq	%rax, 32(%r8)
	addq	$40, 80(%r12)
	jmp	.L3299
	.p2align 4,,10
	.p2align 3
.L3283:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L3286
	jmp	.L3284
	.p2align 4,,10
	.p2align 3
.L3373:
	movb	$0, -136(%rbp)
	pxor	%xmm0, %xmm0
.L3253:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3259
	movsd	%xmm0, -168(%rbp)
	call	_ZdlPv@PLT
	movsd	-168(%rbp), %xmm0
.L3259:
	cmpb	$0, -136(%rbp)
	je	.L3234
	comisd	.LC37(%rip), %xmm0
	jb	.L3261
	movsd	.LC38(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L3261
	cvttsd2sil	%xmm0, %r13d
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%r13d, %xmm1
	ucomisd	%xmm0, %xmm1
	jnp	.L3375
.L3261:
	movl	$24, %edi
	movsd	%xmm0, -168(%rbp)
	call	_Znwm@PLT
	movsd	-168(%rbp), %xmm0
	movl	$3, 8(%rax)
	movq	%rax, %r12
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%r12)
	leaq	-128(%rbp), %rax
	movq	%rax, -176(%rbp)
	movsd	%xmm0, 16(%r12)
	jmp	.L3243
.L3257:
	leaq	-128(%rbp), %rax
	movq	-168(%rbp), %rdx
	movq	%rax, %rdi
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	jmp	.L3258
	.p2align 4,,10
	.p2align 3
.L3266:
	movl	$56, %edi
	call	_Znwm@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r12
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%r12)
	leaq	32(%r12), %rax
	leaq	16(%r12), %rdi
	movq	%rax, 16(%r12)
	movq	-88(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 48(%r12)
	leaq	-128(%rbp), %rax
	movq	%rax, -176(%rbp)
	cmpq	%r13, %rdi
	je	.L3243
	call	_ZdlPv@PLT
	jmp	.L3243
	.p2align 4,,10
	.p2align 3
.L3245:
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rdx
	movq	%rax, %rcx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L3376
	testq	%r8, %r8
	jne	.L3248
.L3249:
	movq	%rcx, %xmm0
	leaq	(%rcx,%r12), %rsi
	punpcklqdq	%xmm0, %xmm0
	movq	%rsi, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	testq	%r13, %r13
	jne	.L3251
	leaq	-136(%rbp), %rax
	movq	%rax, -168(%rbp)
	jmp	.L3250
	.p2align 4,,10
	.p2align 3
.L3300:
	movq	-168(%rbp), %rdx
	leaq	72(%r12), %rdi
	movq	%r8, %rsi
	call	_ZNSt6vectorIN12v8_inspector8String16ESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L3299
	.p2align 4,,10
	.p2align 3
.L3376:
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%r8, -168(%rbp)
	call	memmove@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %rcx
.L3248:
	movq	%r8, %rdi
	movq	%rcx, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %rcx
	jmp	.L3249
	.p2align 4,,10
	.p2align 3
.L3281:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3234
.L3375:
	jne	.L3261
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$2, 8(%rax)
	movq	%rax, %r12
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%r12)
	leaq	-128(%rbp), %rax
	movl	%r13d, 16(%r12)
	movq	%rax, -176(%rbp)
	jmp	.L3243
.L3368:
	call	__stack_chk_fail@PLT
.L3372:
	leaq	.LC36(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5252:
	.size	_ZN12v8_inspector8protocol19parseJSONCharactersEPKtj, .-_ZN12v8_inspector8protocol19parseJSONCharactersEPKtj
	.section	.text._ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag:
.LFB7853:
	.cfi_startproc
	endbr64
	movabsq	$-3689348814741910323, %rcx
	movq	%rsi, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subq	%rdi, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rax, %r8
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	sarq	$2, %r8
	testq	%r8, %r8
	jle	.L3378
	leaq	(%r8,%r8,4), %r12
	movq	8(%rdx), %rcx
	movq	(%rdx), %r10
	movl	$2147483648, %r11d
	movabsq	$-2147483649, %rbx
	salq	$5, %r12
	addq	%rdi, %r12
	.p2align 4,,10
	.p2align 3
.L3392:
	movq	8(%rdi), %r9
	movq	%rcx, %rax
	movq	(%rdi), %r13
	cmpq	%rcx, %r9
	cmovbe	%r9, %rax
	testq	%rax, %rax
	je	.L3379
	xorl	%r8d, %r8d
	jmp	.L3381
	.p2align 4,,10
	.p2align 3
.L3446:
	addq	$1, %r8
	cmpq	%rax, %r8
	je	.L3379
.L3381:
	movzwl	(%r10,%r8,2), %r14d
	cmpw	%r14w, 0(%r13,%r8,2)
	je	.L3446
.L3380:
	movq	48(%rdi), %rax
	movq	40(%rdi), %r13
	cmpq	%rax, %rcx
	movq	%rax, %r9
	cmovbe	%rcx, %r9
	testq	%r9, %r9
	je	.L3383
	xorl	%r8d, %r8d
	jmp	.L3385
	.p2align 4,,10
	.p2align 3
.L3447:
	addq	$1, %r8
	cmpq	%r8, %r9
	je	.L3383
.L3385:
	movzwl	(%r10,%r8,2), %r14d
	cmpw	%r14w, 0(%r13,%r8,2)
	je	.L3447
.L3384:
	movq	88(%rdi), %rax
	movq	80(%rdi), %r13
	cmpq	%rax, %rcx
	movq	%rax, %r9
	cmovbe	%rcx, %r9
	testq	%r9, %r9
	je	.L3386
	xorl	%r8d, %r8d
	jmp	.L3388
	.p2align 4,,10
	.p2align 3
.L3448:
	addq	$1, %r8
	cmpq	%r9, %r8
	je	.L3386
.L3388:
	movzwl	(%r10,%r8,2), %r14d
	cmpw	%r14w, 0(%r13,%r8,2)
	je	.L3448
.L3387:
	movq	128(%rdi), %rax
	movq	120(%rdi), %r13
	cmpq	%rax, %rcx
	movq	%rax, %r9
	cmovbe	%rcx, %r9
	testq	%r9, %r9
	je	.L3389
	xorl	%r8d, %r8d
	jmp	.L3391
	.p2align 4,,10
	.p2align 3
.L3449:
	addq	$1, %r8
	cmpq	%r9, %r8
	je	.L3389
.L3391:
	movzwl	(%r10,%r8,2), %r14d
	cmpw	%r14w, 0(%r13,%r8,2)
	je	.L3449
.L3390:
	addq	$160, %rdi
	cmpq	%rdi, %r12
	jne	.L3392
	movabsq	$-3689348814741910323, %rcx
	movq	%rsi, %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
.L3378:
	cmpq	$2, %rax
	je	.L3393
	cmpq	$3, %rax
	je	.L3394
	cmpq	$1, %rax
	je	.L3450
.L3396:
	movq	%rsi, %rax
	jmp	.L3436
	.p2align 4,,10
	.p2align 3
.L3379:
	subq	%rcx, %r9
	cmpq	%r11, %r9
	jge	.L3380
	cmpq	%rbx, %r9
	jle	.L3380
	testl	%r9d, %r9d
	jne	.L3380
.L3445:
	movq	%rdi, %rax
.L3436:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3383:
	.cfi_restore_state
	subq	%rcx, %rax
	cmpq	%r11, %rax
	jge	.L3384
	cmpq	%rbx, %rax
	jle	.L3384
	testl	%eax, %eax
	jne	.L3384
	popq	%rbx
	leaq	40(%rdi), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3386:
	.cfi_restore_state
	subq	%rcx, %rax
	cmpq	%r11, %rax
	jge	.L3387
	cmpq	%rbx, %rax
	jle	.L3387
	testl	%eax, %eax
	jne	.L3387
	popq	%rbx
	leaq	80(%rdi), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3389:
	.cfi_restore_state
	subq	%rcx, %rax
	cmpq	%r11, %rax
	jge	.L3390
	cmpq	%rbx, %rax
	jle	.L3390
	testl	%eax, %eax
	jne	.L3390
	popq	%rbx
	leaq	120(%rdi), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3394:
	.cfi_restore_state
	movq	8(%rdx), %r9
	movq	8(%rdi), %r10
	movq	(%rdx), %rdx
	movq	(%rdi), %r8
	cmpq	%r9, %r10
	movq	%r9, %rcx
	cmovbe	%r10, %rcx
	testq	%rcx, %rcx
	je	.L3399
	xorl	%eax, %eax
	jmp	.L3401
	.p2align 4,,10
	.p2align 3
.L3451:
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L3399
.L3401:
	movzwl	(%rdx,%rax,2), %ebx
	cmpw	%bx, (%r8,%rax,2)
	je	.L3451
.L3400:
	addq	$40, %rdi
	jmp	.L3397
.L3450:
	movq	8(%rdx), %r9
	movq	(%rdx), %rdx
.L3398:
	movq	8(%rdi), %r10
	movq	(%rdi), %r8
	cmpq	%r10, %r9
	movq	%r10, %rcx
	cmovbe	%r9, %rcx
	testq	%rcx, %rcx
	je	.L3405
	xorl	%eax, %eax
	jmp	.L3406
	.p2align 4,,10
	.p2align 3
.L3452:
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L3405
.L3406:
	movzwl	(%rdx,%rax,2), %ebx
	cmpw	%bx, (%r8,%rax,2)
	je	.L3452
	jmp	.L3396
.L3393:
	movq	8(%rdx), %r9
	movq	(%rdx), %rdx
.L3397:
	movq	8(%rdi), %r10
	movq	(%rdi), %r8
	cmpq	%r10, %r9
	movq	%r10, %rcx
	cmovbe	%r9, %rcx
	testq	%rcx, %rcx
	je	.L3402
	xorl	%eax, %eax
	jmp	.L3404
	.p2align 4,,10
	.p2align 3
.L3453:
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L3402
.L3404:
	movzwl	(%rdx,%rax,2), %ebx
	cmpw	%bx, (%r8,%rax,2)
	je	.L3453
.L3403:
	addq	$40, %rdi
	jmp	.L3398
.L3405:
	subq	%r9, %r10
	cmpq	$2147483647, %r10
	jg	.L3396
	cmpq	$-2147483648, %r10
	jl	.L3396
	testl	%r10d, %r10d
	je	.L3445
	jmp	.L3396
	.p2align 4,,10
	.p2align 3
.L3402:
	subq	%r9, %r10
	cmpq	$2147483647, %r10
	jg	.L3403
	cmpq	$-2147483648, %r10
	jl	.L3403
	testl	%r10d, %r10d
	je	.L3445
	jmp	.L3403
	.p2align 4,,10
	.p2align 3
.L3399:
	subq	%r9, %r10
	cmpq	$2147483647, %r10
	jg	.L3400
	cmpq	$-2147483648, %r10
	jl	.L3400
	testl	%r10d, %r10d
	je	.L3445
	jmp	.L3400
	.cfi_endproc
.LFE7853:
	.size	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag
	.section	.text._ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E
	.type	_ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E, @function
_ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E:
.LFB5013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	32(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L3455
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L3458
	.p2align 4,,10
	.p2align 3
.L3457:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%rbx)
	cmpq	%rax, %rsi
	jne	.L3457
	testq	%rcx, %rcx
	je	.L3458
.L3455:
	movq	24(%r13), %rsi
	movq	%rcx, %rax
	xorl	%edx, %edx
	movq	16(%r13), %r14
	divq	%rsi
	leaq	0(,%rdx,8), %rax
	movq	%rdx, %r10
	leaq	(%r14,%rax), %r15
	movq	%rax, -88(%rbp)
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3459
	movq	(%rdi), %r12
	movq	%rcx, %r11
	movq	%rdi, %r9
	movq	56(%r12), %r8
	cmpq	%r11, %r8
	je	.L3553
	.p2align 4,,10
	.p2align 3
.L3460:
	movq	(%r12), %rcx
	testq	%rcx, %rcx
	je	.L3459
	movq	56(%rcx), %r8
	xorl	%edx, %edx
	movq	%r12, %r9
	movq	%r8, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L3459
	movq	%rcx, %r12
	cmpq	%r11, %r8
	jne	.L3460
.L3553:
	movq	8(%rbx), %rax
	movq	16(%r12), %rcx
	movq	8(%r12), %rdx
	cmpq	%rcx, %rax
	movq	%rcx, %r8
	movq	%rdx, -56(%rbp)
	cmovbe	%rax, %r8
	movq	(%rbx), %rdx
	movq	%rdx, -64(%rbp)
	testq	%r8, %r8
	je	.L3461
	movq	%rax, -72(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L3462:
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rax
	movzwl	(%rcx,%rdx,2), %ecx
	cmpw	%cx, (%rax,%rdx,2)
	jne	.L3460
	addq	$1, %rdx
	cmpq	%r8, %rdx
	jne	.L3462
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rcx
.L3461:
	subq	%rcx, %rax
	movl	$2147483648, %ecx
	cmpq	%rcx, %rax
	jge	.L3460
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L3460
	testl	%eax, %eax
	jne	.L3460
	movq	(%r12), %rcx
	cmpq	%r9, %rdi
	je	.L3554
	testq	%rcx, %rcx
	je	.L3466
	movq	56(%rcx), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r10
	je	.L3466
	movq	%r9, (%r14,%rdx,8)
	movq	(%r12), %rcx
.L3466:
	movq	%rcx, (%r9)
	movq	48(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3468
	movq	(%rdi), %rax
	call	*24(%rax)
.L3468:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3469
	call	_ZdlPv@PLT
.L3469:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 40(%r13)
.L3459:
	movq	80(%r13), %r15
	movq	72(%r13), %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN12v8_inspector8String16ESt6vectorIS3_SaIS3_EEEENS0_5__ops16_Iter_equals_valIKS3_EEET_SD_SD_T0_St26random_access_iterator_tag
	movq	%rax, %r12
	cmpq	%rax, %r15
	je	.L3454
	leaq	40(%rax), %rax
	cmpq	%rax, %r15
	je	.L3484
	movq	%r15, %rax
	leaq	56(%r12), %r14
	movl	$2147483648, %r8d
	subq	%r12, %rax
	subq	$80, %rax
	andq	$-8, %rax
	leaq	96(%r12,%rax), %r9
	.p2align 4,,10
	.p2align 3
.L3483:
	movq	-8(%r14), %rax
	movq	8(%rbx), %rdi
	movq	(%rbx), %r11
	movq	-16(%r14), %rsi
	cmpq	%rdi, %rax
	movq	%rdi, %r10
	cmovbe	%rax, %r10
	testq	%r10, %r10
	je	.L3473
	xorl	%edx, %edx
	jmp	.L3475
	.p2align 4,,10
	.p2align 3
.L3555:
	addq	$1, %rdx
	cmpq	%r10, %rdx
	je	.L3473
.L3475:
	movzwl	(%r11,%rdx,2), %ecx
	cmpw	%cx, (%rsi,%rdx,2)
	je	.L3555
.L3474:
	movq	(%r12), %rdi
	cmpq	%r14, %rsi
	je	.L3556
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3557
	movq	%rsi, (%r12)
	movq	-8(%r14), %rdx
	movq	16(%r12), %rax
	movq	%rdx, 8(%r12)
	movq	(%r14), %rdx
	movq	%rdx, 16(%r12)
	testq	%rdi, %rdi
	je	.L3482
	movq	%rdi, -16(%r14)
	movq	%rax, (%r14)
.L3480:
	xorl	%r10d, %r10d
	movq	$0, -8(%r14)
	addq	$40, %r12
	movw	%r10w, (%rdi)
	movq	16(%r14), %rax
	movq	%rax, -8(%r12)
.L3476:
	addq	$40, %r14
	cmpq	%r14, %r9
	jne	.L3483
	cmpq	%r12, %r15
	je	.L3454
.L3484:
	movq	80(%r13), %r14
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	%r14, %r15
	je	.L3485
	movabsq	$-3689348814741910323, %rdx
	movq	%rax, %rcx
	sarq	$3, %rcx
	imulq	%rdx, %rcx
	testq	%rax, %rax
	jle	.L3485
	leaq	16(%r15), %rbx
	leaq	16(%r12), %r14
	jmp	.L3493
	.p2align 4,,10
	.p2align 3
.L3487:
	cmpq	%r14, %rdi
	je	.L3558
	movq	%rax, -16(%r14)
	movq	-8(%rbx), %rax
	movq	(%r14), %rdx
	movq	%rax, -8(%r14)
	movq	(%rbx), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L3492
	movq	%rdi, -16(%rbx)
	movq	%rdx, (%rbx)
.L3490:
	xorl	%eax, %eax
	movq	$0, -8(%rbx)
	addq	$40, %r14
	addq	$40, %rbx
	movw	%ax, (%rdi)
	movq	-24(%rbx), %rax
	movq	%rax, -24(%r14)
	subq	$1, %rcx
	je	.L3559
.L3493:
	movq	-16(%rbx), %rax
	movq	-16(%r14), %rdi
	cmpq	%rbx, %rax
	jne	.L3487
	movq	-8(%rbx), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L3488
	cmpq	$1, %rax
	je	.L3560
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L3488
	movq	%rbx, %rsi
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-8(%rbx), %rax
	movq	-16(%r14), %rdi
	movq	-56(%rbp), %rcx
	leaq	(%rax,%rax), %rdx
.L3488:
	xorl	%esi, %esi
	movq	%rax, -8(%r14)
	movw	%si, (%rdi,%rdx)
	movq	-16(%rbx), %rdi
	jmp	.L3490
	.p2align 4,,10
	.p2align 3
.L3559:
	movq	80(%r13), %r14
	movq	%r14, %rax
	subq	%r15, %rax
.L3485:
	addq	%rax, %r12
	cmpq	%r14, %r12
	je	.L3454
	movq	%r12, %rbx
	.p2align 4,,10
	.p2align 3
.L3498:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3495
	call	_ZdlPv@PLT
	addq	$40, %rbx
	cmpq	%r14, %rbx
	jne	.L3498
	movq	%r12, 80(%r13)
.L3454:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3473:
	.cfi_restore_state
	movq	%rax, %rdx
	subq	%rdi, %rdx
	cmpq	%r8, %rdx
	jge	.L3474
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rdx
	jle	.L3474
	testl	%edx, %edx
	je	.L3476
	jmp	.L3474
	.p2align 4,,10
	.p2align 3
.L3556:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L3478
	cmpq	$1, %rax
	je	.L3561
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L3478
	movq	%r14, %rsi
	movq	%r9, -56(%rbp)
	call	memmove@PLT
	movq	-8(%r14), %rax
	movq	(%r12), %rdi
	movl	$2147483648, %r8d
	movq	-56(%rbp), %r9
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L3478:
	xorl	%r11d, %r11d
	movq	%rax, 8(%r12)
	movw	%r11w, (%rdi,%rdx)
	movq	-16(%r14), %rdi
	jmp	.L3480
	.p2align 4,,10
	.p2align 3
.L3554:
	testq	%rcx, %rcx
	je	.L3503
	movq	56(%rcx), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r10
	je	.L3466
	movq	%r9, (%r14,%rdx,8)
	movq	-88(%rbp), %r15
	addq	16(%r13), %r15
	movq	(%r15), %rax
.L3465:
	leaq	32(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L3562
.L3467:
	movq	$0, (%r15)
	movq	(%r12), %rcx
	jmp	.L3466
	.p2align 4,,10
	.p2align 3
.L3458:
	movq	$1, 32(%rbx)
	movl	$1, %ecx
	jmp	.L3455
	.p2align 4,,10
	.p2align 3
.L3495:
	addq	$40, %rbx
	cmpq	%r14, %rbx
	jne	.L3498
	movq	%r12, 80(%r13)
	jmp	.L3454
	.p2align 4,,10
	.p2align 3
.L3558:
	movq	%rax, -16(%r14)
	movq	-8(%rbx), %rax
	movq	%rax, -8(%r14)
	movq	(%rbx), %rax
	movq	%rax, (%r14)
.L3492:
	movq	%rbx, -16(%rbx)
	movq	%rbx, %rdi
	jmp	.L3490
	.p2align 4,,10
	.p2align 3
.L3557:
	movq	%rsi, (%r12)
	movq	-8(%r14), %rax
	movq	%rax, 8(%r12)
	movq	(%r14), %rax
	movq	%rax, 16(%r12)
.L3482:
	movq	%r14, -16(%r14)
	movq	%r14, %rdi
	jmp	.L3480
	.p2align 4,,10
	.p2align 3
.L3561:
	movzwl	(%r14), %eax
	movw	%ax, (%rdi)
	movq	-8(%r14), %rax
	movq	(%r12), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L3478
	.p2align 4,,10
	.p2align 3
.L3503:
	movq	%r9, %rax
	jmp	.L3465
	.p2align 4,,10
	.p2align 3
.L3560:
	movzwl	(%rbx), %eax
	xorl	%esi, %esi
	movw	%ax, (%rdi)
	movq	-8(%rbx), %rax
	movq	-16(%r14), %rdi
	leaq	(%rax,%rax), %rdx
	movq	%rax, -8(%r14)
	movw	%si, (%rdi,%rdx)
	movq	-16(%rbx), %rdi
	jmp	.L3490
	.p2align 4,,10
	.p2align 3
.L3562:
	movq	%rcx, 32(%r13)
	jmp	.L3467
	.cfi_endproc
.LFE5013:
	.size	_ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E, .-_ZN12v8_inspector8protocol15DictionaryValue6removeERKNS_8String16E
	.section	.text._ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	.type	_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm, @function
_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm:
.LFB7914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L3564
	movq	(%rbx), %r8
.L3565:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L3574
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L3575:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3564:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L3588
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L3589
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L3567:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L3569
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L3570
	.p2align 4,,10
	.p2align 3
.L3571:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L3572:
	testq	%rsi, %rsi
	je	.L3569
.L3570:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L3571
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L3577
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L3570
	.p2align 4,,10
	.p2align 3
.L3569:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L3573
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L3573:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L3565
	.p2align 4,,10
	.p2align 3
.L3574:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L3576
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L3576:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L3575
	.p2align 4,,10
	.p2align 3
.L3577:
	movq	%rdx, %rdi
	jmp	.L3572
	.p2align 4,,10
	.p2align 3
.L3588:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L3567
.L3589:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7914:
	.size	_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm, .-_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	.section	.text._ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv
	.type	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv, @function
_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv:
.LFB5161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$8, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	%rax, 0(%r13)
	movq	24(%rbx), %rsi
	movq	%rax, %r12
	movq	%rbx, (%rax)
	divq	%rsi
	movq	16(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L3591
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L3593
	.p2align 4,,10
	.p2align 3
.L3602:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3591
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r14
	jne	.L3591
.L3593:
	cmpq	%rdi, %r12
	jne	.L3602
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3591:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r12, 8(%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	$0, (%rax)
	call	_ZNSt10_HashtableIPN12v8_inspector8protocol14DispatcherBase7WeakPtrES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5161:
	.size	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv, .-_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv
	.weak	_ZTVN12v8_inspector8protocol5ValueE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol5ValueE,"awG",@progbits,_ZTVN12v8_inspector8protocol5ValueE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol5ValueE, @object
	.size	_ZTVN12v8_inspector8protocol5ValueE, 112
_ZTVN12v8_inspector8protocol5ValueE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol5Value15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol5Value17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol5ValueD1Ev
	.quad	_ZN12v8_inspector8protocol5ValueD0Ev
	.quad	_ZNK12v8_inspector8protocol5Value9asBooleanEPb
	.quad	_ZNK12v8_inspector8protocol5Value8asDoubleEPd
	.quad	_ZNK12v8_inspector8protocol5Value9asIntegerEPi
	.quad	_ZNK12v8_inspector8protocol5Value8asStringEPNS_8String16E
	.quad	_ZNK12v8_inspector8protocol5Value8asBinaryEPNS0_6BinaryE
	.quad	_ZNK12v8_inspector8protocol5Value9writeJSONEPNS_15String16BuilderE
	.quad	_ZNK12v8_inspector8protocol5Value11writeBinaryEPSt6vectorIhSaIhEE
	.quad	_ZNK12v8_inspector8protocol5Value5cloneEv
	.weak	_ZTVN12v8_inspector8protocol16FundamentalValueE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol16FundamentalValueE,"awG",@progbits,_ZTVN12v8_inspector8protocol16FundamentalValueE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol16FundamentalValueE, @object
	.size	_ZTVN12v8_inspector8protocol16FundamentalValueE, 112
_ZTVN12v8_inspector8protocol16FundamentalValueE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol5Value15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol5Value17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol16FundamentalValueD1Ev
	.quad	_ZN12v8_inspector8protocol16FundamentalValueD0Ev
	.quad	_ZNK12v8_inspector8protocol16FundamentalValue9asBooleanEPb
	.quad	_ZNK12v8_inspector8protocol16FundamentalValue8asDoubleEPd
	.quad	_ZNK12v8_inspector8protocol16FundamentalValue9asIntegerEPi
	.quad	_ZNK12v8_inspector8protocol5Value8asStringEPNS_8String16E
	.quad	_ZNK12v8_inspector8protocol5Value8asBinaryEPNS0_6BinaryE
	.quad	_ZNK12v8_inspector8protocol16FundamentalValue9writeJSONEPNS_15String16BuilderE
	.quad	_ZNK12v8_inspector8protocol16FundamentalValue11writeBinaryEPSt6vectorIhSaIhEE
	.quad	_ZNK12v8_inspector8protocol16FundamentalValue5cloneEv
	.weak	_ZTVN12v8_inspector8protocol11StringValueE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol11StringValueE,"awG",@progbits,_ZTVN12v8_inspector8protocol11StringValueE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol11StringValueE, @object
	.size	_ZTVN12v8_inspector8protocol11StringValueE, 112
_ZTVN12v8_inspector8protocol11StringValueE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol5Value15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol5Value17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol11StringValueD1Ev
	.quad	_ZN12v8_inspector8protocol11StringValueD0Ev
	.quad	_ZNK12v8_inspector8protocol5Value9asBooleanEPb
	.quad	_ZNK12v8_inspector8protocol5Value8asDoubleEPd
	.quad	_ZNK12v8_inspector8protocol5Value9asIntegerEPi
	.quad	_ZNK12v8_inspector8protocol11StringValue8asStringEPNS_8String16E
	.quad	_ZNK12v8_inspector8protocol5Value8asBinaryEPNS0_6BinaryE
	.quad	_ZNK12v8_inspector8protocol11StringValue9writeJSONEPNS_15String16BuilderE
	.quad	_ZNK12v8_inspector8protocol11StringValue11writeBinaryEPSt6vectorIhSaIhEE
	.quad	_ZNK12v8_inspector8protocol11StringValue5cloneEv
	.weak	_ZTVN12v8_inspector8protocol11BinaryValueE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol11BinaryValueE,"awG",@progbits,_ZTVN12v8_inspector8protocol11BinaryValueE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol11BinaryValueE, @object
	.size	_ZTVN12v8_inspector8protocol11BinaryValueE, 112
_ZTVN12v8_inspector8protocol11BinaryValueE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol5Value15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol5Value17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol11BinaryValueD1Ev
	.quad	_ZN12v8_inspector8protocol11BinaryValueD0Ev
	.quad	_ZNK12v8_inspector8protocol5Value9asBooleanEPb
	.quad	_ZNK12v8_inspector8protocol5Value8asDoubleEPd
	.quad	_ZNK12v8_inspector8protocol5Value9asIntegerEPi
	.quad	_ZNK12v8_inspector8protocol5Value8asStringEPNS_8String16E
	.quad	_ZNK12v8_inspector8protocol11BinaryValue8asBinaryEPNS0_6BinaryE
	.quad	_ZNK12v8_inspector8protocol11BinaryValue9writeJSONEPNS_15String16BuilderE
	.quad	_ZNK12v8_inspector8protocol11BinaryValue11writeBinaryEPSt6vectorIhSaIhEE
	.quad	_ZNK12v8_inspector8protocol11BinaryValue5cloneEv
	.weak	_ZTVN12v8_inspector8protocol15SerializedValueE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol15SerializedValueE,"awG",@progbits,_ZTVN12v8_inspector8protocol15SerializedValueE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol15SerializedValueE, @object
	.size	_ZTVN12v8_inspector8protocol15SerializedValueE, 112
_ZTVN12v8_inspector8protocol15SerializedValueE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol5Value15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol5Value17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol15SerializedValueD1Ev
	.quad	_ZN12v8_inspector8protocol15SerializedValueD0Ev
	.quad	_ZNK12v8_inspector8protocol5Value9asBooleanEPb
	.quad	_ZNK12v8_inspector8protocol5Value8asDoubleEPd
	.quad	_ZNK12v8_inspector8protocol5Value9asIntegerEPi
	.quad	_ZNK12v8_inspector8protocol5Value8asStringEPNS_8String16E
	.quad	_ZNK12v8_inspector8protocol5Value8asBinaryEPNS0_6BinaryE
	.quad	_ZNK12v8_inspector8protocol15SerializedValue9writeJSONEPNS_15String16BuilderE
	.quad	_ZNK12v8_inspector8protocol15SerializedValue11writeBinaryEPSt6vectorIhSaIhEE
	.quad	_ZNK12v8_inspector8protocol15SerializedValue5cloneEv
	.weak	_ZTVN12v8_inspector8protocol15DictionaryValueE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol15DictionaryValueE,"awG",@progbits,_ZTVN12v8_inspector8protocol15DictionaryValueE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol15DictionaryValueE, @object
	.size	_ZTVN12v8_inspector8protocol15DictionaryValueE, 112
_ZTVN12v8_inspector8protocol15DictionaryValueE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol5Value15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol5Value17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol15DictionaryValueD1Ev
	.quad	_ZN12v8_inspector8protocol15DictionaryValueD0Ev
	.quad	_ZNK12v8_inspector8protocol5Value9asBooleanEPb
	.quad	_ZNK12v8_inspector8protocol5Value8asDoubleEPd
	.quad	_ZNK12v8_inspector8protocol5Value9asIntegerEPi
	.quad	_ZNK12v8_inspector8protocol5Value8asStringEPNS_8String16E
	.quad	_ZNK12v8_inspector8protocol5Value8asBinaryEPNS0_6BinaryE
	.quad	_ZNK12v8_inspector8protocol15DictionaryValue9writeJSONEPNS_15String16BuilderE
	.quad	_ZNK12v8_inspector8protocol15DictionaryValue11writeBinaryEPSt6vectorIhSaIhEE
	.quad	_ZNK12v8_inspector8protocol15DictionaryValue5cloneEv
	.weak	_ZTVN12v8_inspector8protocol9ListValueE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol9ListValueE,"awG",@progbits,_ZTVN12v8_inspector8protocol9ListValueE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol9ListValueE, @object
	.size	_ZTVN12v8_inspector8protocol9ListValueE, 112
_ZTVN12v8_inspector8protocol9ListValueE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol5Value15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol5Value17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol9ListValueD1Ev
	.quad	_ZN12v8_inspector8protocol9ListValueD0Ev
	.quad	_ZNK12v8_inspector8protocol5Value9asBooleanEPb
	.quad	_ZNK12v8_inspector8protocol5Value8asDoubleEPd
	.quad	_ZNK12v8_inspector8protocol5Value9asIntegerEPi
	.quad	_ZNK12v8_inspector8protocol5Value8asStringEPNS_8String16E
	.quad	_ZNK12v8_inspector8protocol5Value8asBinaryEPNS0_6BinaryE
	.quad	_ZNK12v8_inspector8protocol9ListValue9writeJSONEPNS_15String16BuilderE
	.quad	_ZNK12v8_inspector8protocol9ListValue11writeBinaryEPSt6vectorIhSaIhEE
	.quad	_ZNK12v8_inspector8protocol9ListValue5cloneEv
	.weak	_ZTVN12v8_inspector8protocol14DispatcherBase8CallbackE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol14DispatcherBase8CallbackE,"awG",@progbits,_ZTVN12v8_inspector8protocol14DispatcherBase8CallbackE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol14DispatcherBase8CallbackE, @object
	.size	_ZTVN12v8_inspector8protocol14DispatcherBase8CallbackE, 32
_ZTVN12v8_inspector8protocol14DispatcherBase8CallbackE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol14DispatcherBase8CallbackD1Ev
	.quad	_ZN12v8_inspector8protocol14DispatcherBase8CallbackD0Ev
	.weak	_ZTVN12v8_inspector8protocol14DispatcherBaseE
	.section	.data.rel.ro._ZTVN12v8_inspector8protocol14DispatcherBaseE,"awG",@progbits,_ZTVN12v8_inspector8protocol14DispatcherBaseE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol14DispatcherBaseE, @object
	.size	_ZTVN12v8_inspector8protocol14DispatcherBaseE, 48
_ZTVN12v8_inspector8protocol14DispatcherBaseE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorE, @object
	.size	_ZTVN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorE, 48
_ZTVN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolError17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorD1Ev
	.quad	_ZN12v8_inspector8protocol12_GLOBAL__N_113ProtocolErrorD0Ev
	.weak	_ZTVN12v8_inspector8protocol14UberDispatcherE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol14UberDispatcherE,"awG",@progbits,_ZTVN12v8_inspector8protocol14UberDispatcherE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol14UberDispatcherE, @object
	.size	_ZTVN12v8_inspector8protocol14UberDispatcherE, 32
_ZTVN12v8_inspector8protocol14UberDispatcherE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol14UberDispatcherD1Ev
	.quad	_ZN12v8_inspector8protocol14UberDispatcherD0Ev
	.weak	_ZTVN12v8_inspector8protocol16InternalResponseE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol16InternalResponseE,"awG",@progbits,_ZTVN12v8_inspector8protocol16InternalResponseE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol16InternalResponseE, @object
	.size	_ZTVN12v8_inspector8protocol16InternalResponseE, 48
_ZTVN12v8_inspector8protocol16InternalResponseE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol16InternalResponse15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol16InternalResponse17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol16InternalResponseD1Ev
	.quad	_ZN12v8_inspector8protocol16InternalResponseD0Ev
	.globl	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE
	.section	.rodata._ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE,"a"
	.align 16
	.type	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE, @object
	.size	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE, 19
_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE:
	.string	"Invalid parameters"
	.section	.rodata._ZN12v8_inspector8protocol12_GLOBAL__N_1L9hexDigitsE,"a"
	.align 16
	.type	_ZN12v8_inspector8protocol12_GLOBAL__N_1L9hexDigitsE, @object
	.size	_ZN12v8_inspector8protocol12_GLOBAL__N_1L9hexDigitsE, 17
_ZN12v8_inspector8protocol12_GLOBAL__N_1L9hexDigitsE:
	.string	"0123456789ABCDEF"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC5:
	.long	4294967295
	.long	2146435071
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC11:
	.long	1065353216
	.section	.rodata.cst8
	.align 8
.LC37:
	.long	0
	.long	-1042284544
	.align 8
.LC38:
	.long	4290772992
	.long	1105199103
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
