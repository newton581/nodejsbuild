	.file	"HeapProfiler.cpp"
	.text
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv, @function
_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv:
.LFB4422:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movq	8(%rsi), %rcx
	leaq	24(%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L5
	movq	%rcx, (%rdi)
	movq	24(%rsi), %rcx
	movq	%rcx, 16(%rdi)
.L3:
	movq	16(%rsi), %rcx
	movq	%rdx, 8(%rsi)
	xorl	%edx, %edx
	movq	$0, 16(%rsi)
	movq	%rcx, 8(%rax)
	movw	%dx, 24(%rsi)
	movq	40(%rsi), %rdx
	movq	%rdx, 32(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movdqu	24(%rsi), %xmm0
	movups	%xmm0, 16(%rdi)
	jmp	.L3
	.cfi_endproc
.LFE4422:
	.size	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv, .-_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv:
.LFB4423:
	.cfi_startproc
	endbr64
	movdqu	48(%rsi), %xmm1
	movq	64(%rsi), %rdx
	pxor	%xmm0, %xmm0
	movq	%rdi, %rax
	movq	$0, 64(%rsi)
	movq	%rdx, 16(%rdi)
	movups	%xmm1, (%rdi)
	movups	%xmm0, 48(%rsi)
	ret
	.cfi_endproc
.LFE4423:
	.size	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv, .-_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD2Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD2Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD2Ev:
.LFB5549:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5549:
	.size	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD2Ev, .-_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD2Ev
	.weak	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD1Ev
	.set	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD1Ev,_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD2Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD2Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD2Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD2Ev:
.LFB5665:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5665:
	.size	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD2Ev, .-_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD2Ev
	.weak	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD1Ev
	.set	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD1Ev,_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD2Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD2Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD2Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD2Ev:
.LFB5693:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5693:
	.size	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD2Ev, .-_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD2Ev
	.weak	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD1Ev
	.set	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD1Ev,_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD2Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev:
.LFB5551:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5551:
	.size	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev, .-_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD0Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD0Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD0Ev:
.LFB5667:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5667:
	.size	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD0Ev, .-_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD0Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD0Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD0Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD0Ev:
.LFB5695:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5695:
	.size	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD0Ev, .-_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv:
.LFB5133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L13
	movq	(%rdi), %rax
	call	*24(%rax)
.L13:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L20:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5133:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv:
.LFB5132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L21
	movq	(%rdi), %rax
	call	*24(%rax)
.L21:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L28:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5132:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev:
.LFB5119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L30
	call	_ZdlPv@PLT
.L30:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L31
	call	_ZdlPv@PLT
.L31:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L29
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5119:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev,_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD2Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD2Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD2Ev:
.LFB5612:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	cmpq	%rdi, %r8
	je	.L34
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	ret
	.cfi_endproc
.LFE5612:
	.size	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD2Ev, .-_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD2Ev
	.weak	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD1Ev
	.set	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD1Ev,_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD2Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD0Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD0Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD0Ev:
.LFB5614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5614:
	.size	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD0Ev, .-_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev:
.LFB4606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L40
	call	_ZdlPv@PLT
.L40:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L39
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4606:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotificationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev
	.type	_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev, @function
_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev:
.LFB4419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L44
	call	_ZdlPv@PLT
.L44:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L43
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4419:
	.size	_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev, .-_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev
	.weak	_ZN12v8_inspector8protocol23InternalRawNotificationD1Ev
	.set	_ZN12v8_inspector8protocol23InternalRawNotificationD1Ev,_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv:
.LFB4762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12EntryPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L50
	movq	(%rdi), %rax
	call	*24(%rax)
.L50:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L57:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4762:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv:
.LFB4761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12EntryPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L58
	movq	(%rdi), %rax
	call	*24(%rax)
.L58:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L65:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4761:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv:
.LFB4730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime15PropertyPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L66
	movq	(%rdi), %rax
	call	*24(%rax)
.L66:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L73:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4730:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv:
.LFB4729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime15PropertyPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L74
	movq	(%rdi), %rax
	call	*24(%rax)
.L74:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4729:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv:
.LFB4661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13ObjectPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L82
	movq	(%rdi), %rax
	call	*24(%rax)
.L82:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L89:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4661:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv:
.LFB4660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13ObjectPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L90
	movq	(%rdi), %rax
	call	*24(%rax)
.L90:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L97:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4660:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv:
.LFB4615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L98
	movq	(%rdi), %rax
	call	*24(%rax)
.L98:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L105:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4615:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv:
.LFB4614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L106
	movq	(%rdi), %rax
	call	*24(%rax)
.L106:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L113:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4614:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD2Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD2Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD2Ev:
.LFB5818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler14DispatcherImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L119
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L145:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L115
.L118:
	movq	%rbx, %r13
.L119:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L116
	call	_ZdlPv@PLT
.L116:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L145
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L118
.L115:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L120
	call	_ZdlPv@PLT
.L120:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L124
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L146:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L121
.L123:
	movq	%rbx, %r13
.L124:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L146
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L123
.L121:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L125
	call	_ZdlPv@PLT
.L125:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev@PLT
	.cfi_endproc
.LFE5818:
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD2Ev, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD2Ev
	.weak	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD1Ev
	.set	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD1Ev,_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD2Ev
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotificationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev
	.type	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev, @function
_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev:
.LFB4421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L148
	call	_ZdlPv@PLT
.L148:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L149
	call	_ZdlPv@PLT
.L149:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4421:
	.size	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev, .-_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev:
.LFB4608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L155
	call	_ZdlPv@PLT
.L155:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L156
	call	_ZdlPv@PLT
.L156:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4608:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev:
.LFB5121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L159
	call	_ZdlPv@PLT
.L159:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L160
	call	_ZdlPv@PLT
.L160:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L161
	call	_ZdlPv@PLT
.L161:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5121:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.section	.text._ZN12v8_inspector8protocol16InternalResponseD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16InternalResponseD0Ev
	.type	_ZN12v8_inspector8protocol16InternalResponseD0Ev, @function
_ZN12v8_inspector8protocol16InternalResponseD0Ev:
.LFB4412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L164
	movq	(%rdi), %rax
	call	*24(%rax)
.L164:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L165
	call	_ZdlPv@PLT
.L165:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4412:
	.size	_ZN12v8_inspector8protocol16InternalResponseD0Ev, .-_ZN12v8_inspector8protocol16InternalResponseD0Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD0Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD0Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD0Ev:
.LFB5820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler14DispatcherImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L175
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L201:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L171
.L174:
	movq	%rbx, %r13
.L175:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L172
	call	_ZdlPv@PLT
.L172:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L201
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L174
.L171:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L176
	call	_ZdlPv@PLT
.L176:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L180
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L202:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L177
.L179:
	movq	%rbx, %r13
.L180:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L202
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L179
.L177:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L181
	call	_ZdlPv@PLT
.L181:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5820:
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD0Ev, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD0Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD2Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD2Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD2Ev:
.LFB5639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L203
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L205
	call	_ZdlPv@PLT
.L205:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5639:
	.size	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD2Ev, .-_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD2Ev
	.weak	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD1Ev
	.set	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD1Ev,_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD2Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD0Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD0Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD0Ev:
.LFB5641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L211
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L212
	call	_ZdlPv@PLT
.L212:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L211:
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5641:
	.size	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD0Ev, .-_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD0Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD2Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD2Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD2Ev:
.LFB5513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %r15
	movq	%rax, (%rdi)
	testq	%r15, %r15
	je	.L221
	movq	8(%r15), %r14
	movq	(%r15), %r12
	cmpq	%r12, %r14
	jne	.L225
	testq	%r12, %r12
	je	.L226
	.p2align 4,,10
	.p2align 3
.L245:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L226:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L221:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L220
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L228
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L229
	call	_ZdlPv@PLT
.L229:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L230
	call	_ZdlPv@PLT
.L230:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L231
	call	_ZdlPv@PLT
.L231:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L223:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L243
.L225:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L223
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L244
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L225
	.p2align 4,,10
	.p2align 3
.L243:
	movq	(%r15), %r12
	testq	%r12, %r12
	jne	.L245
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L220:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE5513:
	.size	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD2Ev, .-_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD2Ev
	.weak	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	.set	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev,_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD2Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl14collectGarbageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl14collectGarbageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl14collectGarbageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl14collectGarbageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB5831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	cmpl	$2, -112(%rbp)
	je	.L259
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L248
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L248:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L249
	call	_ZdlPv@PLT
.L249:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L246
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L246:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L260
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L248
.L260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5831:
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl14collectGarbageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl14collectGarbageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB5832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*32(%rax)
	cmpl	$2, -112(%rbp)
	je	.L274
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L263
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L263:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L264
	call	_ZdlPv@PLT
.L264:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L261
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L261:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L275
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L263
.L275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5832:
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB5833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*40(%rax)
	cmpl	$2, -112(%rbp)
	je	.L289
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L278
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L278:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L279
	call	_ZdlPv@PLT
.L279:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L276
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L276:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L290
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L278
.L290:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5833:
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev:
.LFB5584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movq	%rax, (%rdi)
	testq	%r15, %r15
	je	.L292
	movq	8(%r15), %rbx
	movq	(%r15), %r13
	cmpq	%r13, %rbx
	je	.L293
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %r14
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L339:
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L294:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L338
.L296:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L294
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L339
	call	*%rax
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L296
	.p2align 4,,10
	.p2align 3
.L338:
	movq	(%r15), %r13
.L293:
	testq	%r13, %r13
	je	.L297
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L297:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L292:
	movq	8(%r12), %r15
	testq	%r15, %r15
	je	.L298
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L299
	movq	32(%r15), %r14
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r14, %r14
	je	.L300
	movq	8(%r14), %rax
	movq	(%r14), %rbx
	movq	%rax, -56(%rbp)
	cmpq	%rbx, %rax
	jne	.L304
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L341:
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L302:
	addq	$8, %rbx
	cmpq	%rbx, -56(%rbp)
	je	.L340
.L304:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L302
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L341
	call	*%rax
	addq	$8, %rbx
	cmpq	%rbx, -56(%rbp)
	jne	.L304
	.p2align 4,,10
	.p2align 3
.L340:
	movq	(%r14), %rbx
.L301:
	testq	%rbx, %rbx
	je	.L305
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L305:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L300:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L306
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L307
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	104(%r13), %rax
	cmpq	%rax, %rdi
	je	.L308
	call	_ZdlPv@PLT
.L308:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L309
	call	_ZdlPv@PLT
.L309:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L310
	call	_ZdlPv@PLT
.L310:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L306:
	movl	$40, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L298:
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	movq	%r15, %rdi
	call	*%rax
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L307:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L306
	.cfi_endproc
.LFE5584:
	.size	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev, .-_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl23stopTrackingHeapObjectsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"params"
.LC1:
	.string	"reportProgress"
.LC2:
	.string	"boolean value expected"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl23stopTrackingHeapObjectsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl23stopTrackingHeapObjectsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl23stopTrackingHeapObjectsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl23stopTrackingHeapObjectsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB5867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	%rdx, -152(%rbp)
	movq	(%r8), %r15
	movq	%rcx, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	testq	%rax, %rax
	je	.L344
	cmpl	$6, 8(%rax)
	jne	.L344
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L373
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-136(%rbp), %rax
.L347:
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r15, %rdi
	je	.L349
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L349:
	testq	%r8, %r8
	movq	%r8, -136(%rbp)
	je	.L362
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %r8
	movb	$0, -120(%rbp)
	leaq	-120(%rbp), %rsi
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*32(%rax)
	testb	%al, %al
	jne	.L350
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L350:
	movzbl	-120(%rbp), %eax
	movb	$1, -137(%rbp)
	movb	%al, -136(%rbp)
.L348:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L374
	leaq	-120(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movzbl	-137(%rbp), %ecx
	leaq	-122(%rbp), %rdx
	movq	%r12, %rdi
	movq	184(%r13), %rsi
	movq	(%rsi), %rax
	movb	%cl, -122(%rbp)
	movzbl	-136(%rbp), %ecx
	movb	%cl, -121(%rbp)
	call	*96(%rax)
	cmpl	$2, -112(%rbp)
	je	.L375
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L355
	movq	%r12, %rdx
	movl	%r14d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L355:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L356
	call	_ZdlPv@PLT
.L356:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L342
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L342:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L376
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L373:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-136(%rbp), %rax
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L344:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L377
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L362:
	movb	$0, -136(%rbp)
	movb	$0, -137(%rbp)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	$-32602, %edx
	movl	%r14d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L342
	call	_ZdlPv@PLT
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L377:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movb	$0, -136(%rbp)
	movb	$0, -137(%rbp)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L375:
	movq	8(%r13), %rdi
	movq	-160(%rbp), %rcx
	movl	%r14d, %esi
	movq	-152(%rbp), %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L355
.L376:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5867:
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl23stopTrackingHeapObjectsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl23stopTrackingHeapObjectsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl16takeHeapSnapshotEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl16takeHeapSnapshotEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl16takeHeapSnapshotEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl16takeHeapSnapshotEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB5868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	%rdx, -152(%rbp)
	movq	(%r8), %r15
	movq	%rcx, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	testq	%rax, %rax
	je	.L380
	cmpl	$6, 8(%rax)
	jne	.L380
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L409
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-136(%rbp), %rax
.L383:
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r15, %rdi
	je	.L385
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L385:
	testq	%r8, %r8
	movq	%r8, -136(%rbp)
	je	.L398
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %r8
	movb	$0, -120(%rbp)
	leaq	-120(%rbp), %rsi
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*32(%rax)
	testb	%al, %al
	jne	.L386
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L386:
	movzbl	-120(%rbp), %eax
	movb	$1, -137(%rbp)
	movb	%al, -136(%rbp)
.L384:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L410
	leaq	-120(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movzbl	-137(%rbp), %ecx
	leaq	-122(%rbp), %rdx
	movq	%r12, %rdi
	movq	184(%r13), %rsi
	movq	(%rsi), %rax
	movb	%cl, -122(%rbp)
	movzbl	-136(%rbp), %ecx
	movb	%cl, -121(%rbp)
	call	*104(%rax)
	cmpl	$2, -112(%rbp)
	je	.L411
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L391
	movq	%r12, %rdx
	movl	%r14d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L391:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L392
	call	_ZdlPv@PLT
.L392:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L378
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L378:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L412
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-136(%rbp), %rax
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L380:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L413
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L398:
	movb	$0, -136(%rbp)
	movb	$0, -137(%rbp)
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L410:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	$-32602, %edx
	movl	%r14d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L378
	call	_ZdlPv@PLT
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L413:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movb	$0, -136(%rbp)
	movb	$0, -137(%rbp)
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L411:
	movq	8(%r13), %rdi
	movq	-160(%rbp), %rcx
	movl	%r14d, %esi
	movq	-152(%rbp), %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L391
.L412:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5868:
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl16takeHeapSnapshotEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl16takeHeapSnapshotEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl24startTrackingHeapObjectsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"trackAllocations"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl24startTrackingHeapObjectsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl24startTrackingHeapObjectsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl24startTrackingHeapObjectsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl24startTrackingHeapObjectsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB5865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	%rdx, -152(%rbp)
	movq	(%r8), %r15
	movq	%rcx, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	testq	%rax, %rax
	je	.L416
	cmpl	$6, 8(%rax)
	jne	.L416
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L445
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-136(%rbp), %rax
.L419:
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r15, %rdi
	je	.L421
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L421:
	testq	%r8, %r8
	movq	%r8, -136(%rbp)
	je	.L434
	leaq	.LC3(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %r8
	movb	$0, -120(%rbp)
	leaq	-120(%rbp), %rsi
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*32(%rax)
	testb	%al, %al
	jne	.L422
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L422:
	movzbl	-120(%rbp), %eax
	movb	$1, -137(%rbp)
	movb	%al, -136(%rbp)
.L420:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L446
	leaq	-120(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movzbl	-137(%rbp), %ecx
	leaq	-122(%rbp), %rdx
	movq	%r12, %rdi
	movq	184(%r13), %rsi
	movq	(%rsi), %rax
	movb	%cl, -122(%rbp)
	movzbl	-136(%rbp), %ecx
	movb	%cl, -121(%rbp)
	call	*80(%rax)
	cmpl	$2, -112(%rbp)
	je	.L447
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L427
	movq	%r12, %rdx
	movl	%r14d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L427:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L428
	call	_ZdlPv@PLT
.L428:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L414
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L414:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L448
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-136(%rbp), %rax
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L416:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L449
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L434:
	movb	$0, -136(%rbp)
	movb	$0, -137(%rbp)
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L446:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	$-32602, %edx
	movl	%r14d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L414
	call	_ZdlPv@PLT
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L449:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movb	$0, -136(%rbp)
	movb	$0, -137(%rbp)
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L447:
	movq	8(%r13), %rdi
	movq	-160(%rbp), %rcx
	movl	%r14d, %esi
	movq	-152(%rbp), %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L427
.L448:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5865:
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl24startTrackingHeapObjectsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl24startTrackingHeapObjectsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl22addInspectedHeapObjectEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"heapObjectId"
.LC5:
	.string	"string value expected"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl22addInspectedHeapObjectEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl22addInspectedHeapObjectEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl22addInspectedHeapObjectEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl22addInspectedHeapObjectEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB5824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$168, %rsp
	movl	%esi, -180(%rbp)
	movq	(%r8), %r14
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -192(%rbp)
	movq	%rcx, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L451
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	jne	.L452
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L484
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L455:
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r15, %rdi
	je	.L457
	movq	%rax, -208(%rbp)
	call	_ZdlPv@PLT
	movq	-208(%rbp), %r8
.L457:
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r8, -208(%rbp)
	leaq	-144(%rbp), %r14
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-208(%rbp), %r8
	xorl	%eax, %eax
	movq	%r14, -160(%rbp)
	movw	%ax, -144(%rbp)
	leaq	-160(%rbp), %rsi
	movq	$0, -152(%rbp)
	movq	$0, -128(%rbp)
	testq	%r8, %r8
	je	.L456
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L459
.L456:
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L459:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L485
	leaq	-168(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	leaq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*16(%rax)
	cmpl	$2, -112(%rbp)
	je	.L486
	movq	-168(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L465
	movl	-180(%rbp), %esi
	movq	%r12, %rdx
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L465:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L466
	call	_ZdlPv@PLT
.L466:
	movq	-168(%rbp), %r12
	testq	%r12, %r12
	je	.L463
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L463:
	movq	-160(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L450
	call	_ZdlPv@PLT
.L450:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L487
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L451:
	movq	-112(%rbp), %rdi
.L452:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L488
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L454:
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	xorl	%edx, %edx
	movq	%r14, -160(%rbp)
	movq	$0, -152(%rbp)
	movw	%dx, -144(%rbp)
	movq	$0, -128(%rbp)
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L485:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-180(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L463
	call	_ZdlPv@PLT
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L488:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L486:
	movq	8(%r13), %rdi
	movq	-200(%rbp), %rcx
	movq	-192(%rbp), %rdx
	movl	-180(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L465
.L487:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5824:
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl22addInspectedHeapObjectEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl22addInspectedHeapObjectEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev:
.LFB5515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rdi), %r15
	movq	%rax, (%rdi)
	testq	%r15, %r15
	je	.L490
	movq	8(%r15), %rbx
	movq	(%r15), %r12
	cmpq	%r12, %rbx
	jne	.L494
	testq	%r12, %r12
	je	.L495
	.p2align 4,,10
	.p2align 3
.L517:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L495:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L490:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L496
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L497
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L498
	call	_ZdlPv@PLT
.L498:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L499
	call	_ZdlPv@PLT
.L499:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L500
	call	_ZdlPv@PLT
.L500:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L496:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$40, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_restore_state
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L492:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L515
.L494:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L492
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L516
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L494
	.p2align 4,,10
	.p2align 3
.L515:
	movq	(%r15), %r12
	testq	%r12, %r12
	jne	.L517
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L497:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L496
	.cfi_endproc
.LFE5515:
	.size	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev, .-_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD2Ev
	.type	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD2Ev, @function
_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD2Ev:
.LFB5582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r15
	movq	%rax, (%rdi)
	testq	%r15, %r15
	je	.L519
	movq	8(%r15), %r13
	movq	(%r15), %r12
	cmpq	%r12, %r13
	je	.L520
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %r14
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L563:
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L521:
	addq	$8, %r12
	cmpq	%r12, %r13
	je	.L562
.L523:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L521
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L563
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L523
	.p2align 4,,10
	.p2align 3
.L562:
	movq	(%r15), %r12
.L520:
	testq	%r12, %r12
	je	.L524
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L524:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L519:
	movq	8(%rbx), %r15
	testq	%r15, %r15
	je	.L518
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L526
	movq	32(%r15), %r14
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r14, %r14
	je	.L527
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -56(%rbp)
	cmpq	%r13, %rax
	jne	.L531
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L565:
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L529:
	addq	$8, %r13
	cmpq	%r13, -56(%rbp)
	je	.L564
.L531:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L529
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	je	.L565
	call	*%rax
	addq	$8, %r13
	cmpq	%r13, -56(%rbp)
	jne	.L531
	.p2align 4,,10
	.p2align 3
.L564:
	movq	(%r14), %r13
.L528:
	testq	%r13, %r13
	je	.L532
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L532:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L527:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L533
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L534
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L535
	call	_ZdlPv@PLT
.L535:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L536
	call	_ZdlPv@PLT
.L536:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L537
	call	_ZdlPv@PLT
.L537:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L533:
	addq	$24, %rsp
	movq	%r15, %rdi
	movl	$40, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L533
	.cfi_endproc
.LFE5582:
	.size	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD2Ev, .-_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD2Ev
	.weak	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD1Ev
	.set	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD1Ev,_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD2Ev
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl13startSamplingEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"samplingInterval"
.LC8:
	.string	"double value expected"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl13startSamplingEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl13startSamplingEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl13startSamplingEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl13startSamplingEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB5863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$136, %rsp
	movq	%rdx, -168(%rbp)
	movq	(%r8), %r15
	movq	%rcx, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	testq	%rax, %rax
	je	.L568
	cmpl	$6, 8(%rax)
	jne	.L568
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L597
	movq	%rax, -152(%rbp)
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-152(%rbp), %rax
.L571:
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-152(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r15, %rdi
	je	.L573
	movq	%rax, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %r8
.L573:
	testq	%r8, %r8
	movq	%r8, -152(%rbp)
	je	.L586
	leaq	.LC7(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-152(%rbp), %r8
	leaq	-128(%rbp), %rsi
	movq	$0x000000000, -128(%rbp)
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L574
	leaq	.LC8(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L574:
	movsd	-128(%rbp), %xmm1
	movb	$1, -153(%rbp)
	movsd	%xmm1, -152(%rbp)
.L572:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L598
	leaq	-136(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	leaq	-128(%rbp), %rdx
	movq	%r12, %rdi
	movzbl	-153(%rbp), %ecx
	movsd	-152(%rbp), %xmm0
	movq	(%rsi), %rax
	movq	72(%rax), %rax
	movb	%cl, -128(%rbp)
	movsd	%xmm0, -120(%rbp)
	call	*%rax
	cmpl	$2, -112(%rbp)
	je	.L599
	movq	-136(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L579
	movq	%r12, %rdx
	movl	%r14d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L579:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L580
	call	_ZdlPv@PLT
.L580:
	movq	-136(%rbp), %r12
	testq	%r12, %r12
	je	.L566
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L566:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L600
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L597:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-152(%rbp), %rax
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L568:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L601
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L570:
	pxor	%xmm2, %xmm2
	movb	$0, -153(%rbp)
	movsd	%xmm2, -152(%rbp)
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L598:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	$-32602, %edx
	movl	%r14d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L566
	call	_ZdlPv@PLT
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L601:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L599:
	movq	8(%r13), %rdi
	movq	-176(%rbp), %rcx
	movl	%r14d, %esi
	movq	-168(%rbp), %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L586:
	pxor	%xmm3, %xmm3
	movb	$0, -153(%rbp)
	movsd	%xmm3, -152(%rbp)
	jmp	.L572
.L600:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5863:
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl13startSamplingEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl13startSamplingEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev:
.LFB4640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	160(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L603
	movq	8(%r13), %rax
	movq	0(%r13), %r12
	movq	%rax, -64(%rbp)
	cmpq	%r12, %rax
	je	.L604
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r14
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L659:
	movq	16(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L607
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L608
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L607:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L609
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L610
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L609:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L605:
	addq	$8, %r12
	cmpq	%r12, -64(%rbp)
	je	.L658
.L611:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L605
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L659
	movq	%r15, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -64(%rbp)
	jne	.L611
	.p2align 4,,10
	.p2align 3
.L658:
	movq	0(%r13), %r12
.L604:
	testq	%r12, %r12
	je	.L612
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L612:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L603:
	movq	152(%rbx), %r13
	testq	%r13, %r13
	je	.L613
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	jne	.L623
	testq	%r12, %r12
	je	.L624
	.p2align 4,,10
	.p2align 3
.L662:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L624:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L613:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L625
	call	_ZdlPv@PLT
.L625:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L626
	call	_ZdlPv@PLT
.L626:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L602
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L661:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L617
	call	_ZdlPv@PLT
.L617:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L618
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L619
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L618:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L620
	call	_ZdlPv@PLT
.L620:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L621
	call	_ZdlPv@PLT
.L621:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L622
	call	_ZdlPv@PLT
.L622:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L615:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L660
.L623:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L615
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L661
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %r14
	jne	.L623
	.p2align 4,,10
	.p2align 3
.L660:
	movq	0(%r13), %r12
	testq	%r12, %r12
	jne	.L662
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L602:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L610:
	.cfi_restore_state
	call	*%rax
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L608:
	call	*%rax
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L619:
	call	*%rax
	jmp	.L618
	.cfi_endproc
.LFE4640:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl15getHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"objectId"
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl15getHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl15getHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1
.LC11:
	.string	"basic_string::_M_create"
.LC12:
	.string	"heapSnapshotObjectId"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl15getHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl15getHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl15getHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl15getHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB5834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$264, %rsp
	movl	%esi, -276(%rbp)
	movq	(%r8), %r14
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -288(%rbp)
	movq	%rcx, -296(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L664
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r14
	jne	.L665
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L723
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L668:
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r15, %rdi
	je	.L670
	movq	%rax, -304(%rbp)
	call	_ZdlPv@PLT
	movq	-304(%rbp), %r8
.L670:
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r8, -304(%rbp)
	leaq	-240(%rbp), %r14
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-304(%rbp), %r8
	xorl	%edi, %edi
	movq	%r14, -256(%rbp)
	movw	%di, -240(%rbp)
	leaq	-256(%rbp), %rsi
	movq	$0, -248(%rbp)
	movq	$0, -224(%rbp)
	testq	%r8, %r8
	je	.L669
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L672
.L669:
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L672:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L724
	xorl	%ecx, %ecx
	leaq	-272(%rbp), %rdi
	movq	%r13, %rsi
	movq	$0, -200(%rbp)
	leaq	-192(%rbp), %rbx
	movw	%cx, -192(%rbp)
	leaq	-208(%rbp), %r15
	movq	%rbx, -208(%rbp)
	movq	$0, -176(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdi
	leaq	-256(%rbp), %rdx
	movq	(%rsi), %rax
	call	*48(%rax)
	cmpl	$2, -112(%rbp)
	je	.L725
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %edx
	testl	%edx, %edx
	je	.L726
.L679:
	movq	-272(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L727
	movl	-276(%rbp), %esi
	leaq	-264(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r13, -264(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L678
	movq	(%rdi), %rax
	call	*24(%rax)
.L678:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L688
	call	_ZdlPv@PLT
.L688:
	movq	-272(%rbp), %r12
	testq	%r12, %r12
	je	.L689
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L689:
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L676
.L722:
	call	_ZdlPv@PLT
.L676:
	movq	-256(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L663
	call	_ZdlPv@PLT
.L663:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L728
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L723:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L664:
	movq	-112(%rbp), %rdi
.L665:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L729
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L667:
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	leaq	-240(%rbp), %r14
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	xorl	%r8d, %r8d
	movq	%r14, -256(%rbp)
	movq	$0, -248(%rbp)
	movw	%r8w, -240(%rbp)
	movq	$0, -224(%rbp)
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L724:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-276(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	jne	.L722
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L727:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L725:
	movq	8(%r13), %rdi
	movq	-296(%rbp), %rcx
	movq	-288(%rbp), %rdx
	movl	-276(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L729:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L726:
	movl	$56, %edi
	call	_Znwm@PLT
	movq	-208(%rbp), %rsi
	movq	%rax, %r15
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%r15)
	movq	-200(%rbp), %rax
	leaq	32(%r15), %rdi
	movq	%rdi, 16(%r15)
	leaq	(%rax,%rax), %rdx
	movq	%rsi, %rax
	addq	%rdx, %rax
	je	.L680
	testq	%rsi, %rsi
	je	.L730
.L680:
	movq	%rdx, %rcx
	sarq	%rcx
	cmpq	$14, %rdx
	ja	.L731
.L681:
	cmpq	$2, %rdx
	je	.L732
	testq	%rdx, %rdx
	je	.L684
	movq	%rcx, -296(%rbp)
	movq	%rdx, -288(%rbp)
	call	memmove@PLT
	movq	16(%r15), %rdi
	movq	-296(%rbp), %rcx
	movq	-288(%rbp), %rdx
.L684:
	xorl	%eax, %eax
	movq	%rcx, 24(%r15)
	leaq	.LC12(%rip), %rsi
	movw	%ax, (%rdi,%rdx)
	movq	-176(%rbp), %rax
	movq	%r15, -264(%rbp)
	movq	%rax, 48(%r15)
	leaq	-160(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	leaq	-264(%rbp), %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L685
	call	_ZdlPv@PLT
.L685:
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L679
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L732:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	16(%r15), %rdi
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L731:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rcx
	ja	.L733
	leaq	2(%rdx), %rdi
	movq	%rcx, -304(%rbp)
	movq	%rsi, -296(%rbp)
	movq	%rdx, -288(%rbp)
	call	_Znwm@PLT
	movq	-304(%rbp), %rcx
	movq	-296(%rbp), %rsi
	movq	%rax, 16(%r15)
	movq	-288(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rcx, 32(%r15)
	jmp	.L681
.L728:
	call	__stack_chk_fail@PLT
.L730:
	leaq	.LC10(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L733:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5834:
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl15getHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl15getHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev:
.LFB4715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movq	152(%rdi), %rdi
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L735
	call	_ZdlPv@PLT
.L735:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L736
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L737
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L738
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	movq	%rdx, -56(%rbp)
	cmpq	%r14, %rdx
	jne	.L746
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L800:
	movq	16(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L742
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L743
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L742:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L744
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L745
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L744:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L740:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L799
.L746:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L740
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L800
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L746
	.p2align 4,,10
	.p2align 3
.L799:
	movq	-64(%rbp), %rax
	movq	(%rax), %r14
.L739:
	testq	%r14, %r14
	je	.L747
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L747:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L738:
	movq	152(%r13), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L748
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -56(%rbp)
	cmpq	%r14, %rcx
	jne	.L758
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L802:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L752
	call	_ZdlPv@PLT
.L752:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L753
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L754
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L753:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L755
	call	_ZdlPv@PLT
.L755:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L756
	call	_ZdlPv@PLT
.L756:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L757
	call	_ZdlPv@PLT
.L757:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L750:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L801
.L758:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L750
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L802
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L758
	.p2align 4,,10
	.p2align 3
.L801:
	movq	-64(%rbp), %rax
	movq	(%rax), %r14
.L749:
	testq	%r14, %r14
	je	.L759
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L759:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L748:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L760
	call	_ZdlPv@PLT
.L760:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L761
	call	_ZdlPv@PLT
.L761:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L762
	call	_ZdlPv@PLT
.L762:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L736:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L763
	call	_ZdlPv@PLT
.L763:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L764
	call	_ZdlPv@PLT
.L764:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L765
	call	_ZdlPv@PLT
.L765:
	addq	$40, %rsp
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L737:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L754:
	call	*%rax
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L745:
	call	*%rax
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L743:
	call	*%rax
	jmp	.L742
	.cfi_endproc
.LFE4715:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev:
.LFB4755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L804
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L805
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L806
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	movq	%rdx, -56(%rbp)
	cmpq	%r14, %rdx
	jne	.L814
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L924:
	movq	16(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L810
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L811
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L810:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L812
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L813
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L812:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L808:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L923
.L814:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L808
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L924
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L814
	.p2align 4,,10
	.p2align 3
.L923:
	movq	-64(%rbp), %rax
	movq	(%rax), %r14
.L807:
	testq	%r14, %r14
	je	.L815
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L815:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L806:
	movq	152(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L816
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -56(%rbp)
	cmpq	%r14, %rcx
	jne	.L826
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L926:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L820
	call	_ZdlPv@PLT
.L820:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L821
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L822
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L821:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L823
	call	_ZdlPv@PLT
.L823:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L824
	call	_ZdlPv@PLT
.L824:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L825
	call	_ZdlPv@PLT
.L825:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L818:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L925
.L826:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L818
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L926
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L826
	.p2align 4,,10
	.p2align 3
.L925:
	movq	-64(%rbp), %rax
	movq	(%rax), %r14
.L817:
	testq	%r14, %r14
	je	.L827
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L827:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L816:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L828
	call	_ZdlPv@PLT
.L828:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L829
	call	_ZdlPv@PLT
.L829:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L830
	call	_ZdlPv@PLT
.L830:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L804:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L831
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L832
	movq	160(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L833
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -56(%rbp)
	cmpq	%r14, %rax
	jne	.L841
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L928:
	movq	16(%r8), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r8)
	testq	%rdi, %rdi
	je	.L837
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L838
	movq	%r8, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	-72(%rbp), %r8
.L837:
	movq	8(%r8), %rdi
	testq	%rdi, %rdi
	je	.L839
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L840
	movq	%r8, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	-72(%rbp), %r8
.L839:
	movl	$24, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L835:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L927
.L841:
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L835
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L928
	movq	%r8, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L841
	.p2align 4,,10
	.p2align 3
.L927:
	movq	(%r15), %r14
.L834:
	testq	%r14, %r14
	je	.L842
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L842:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L833:
	movq	152(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L843
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	movq	%rdx, -56(%rbp)
	cmpq	%r14, %rdx
	jne	.L853
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L930:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L847
	call	_ZdlPv@PLT
.L847:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L848
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L849
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L848:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L850
	call	_ZdlPv@PLT
.L850:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L851
	call	_ZdlPv@PLT
.L851:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L852
	call	_ZdlPv@PLT
.L852:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L845:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L929
.L853:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L845
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L930
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L853
	.p2align 4,,10
	.p2align 3
.L929:
	movq	-64(%rbp), %rax
	movq	(%rax), %r14
.L844:
	testq	%r14, %r14
	je	.L854
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L854:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L843:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L855
	call	_ZdlPv@PLT
.L855:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L856
	call	_ZdlPv@PLT
.L856:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L857
	call	_ZdlPv@PLT
.L857:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L831:
	addq	$40, %rsp
	movq	%r13, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L832:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L805:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L811:
	call	*%rax
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L822:
	call	*%rax
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L849:
	call	*%rax
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L838:
	movq	%r8, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %r8
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L813:
	call	*%rax
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L840:
	movq	%r8, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %r8
	jmp	.L839
	.cfi_endproc
.LFE4755:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev:
.LFB4642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	160(%rdi), %rax
	movq	%rdi, -64(%rbp)
	movq	%rcx, (%rdi)
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L932
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -112(%rbp)
	movq	%rax, -72(%rbp)
	cmpq	%rax, %rbx
	je	.L933
	.p2align 4,,10
	.p2align 3
.L1140:
	movq	-72(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -88(%rbp)
	testq	%rcx, %rcx
	je	.L934
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L935
	movq	16(%rcx), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%rbx, %rbx
	je	.L936
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%rcx, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L937
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L938
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -120(%rbp)
	movq	%rax, -80(%rbp)
	cmpq	%rax, %rdx
	je	.L939
	movq	%rbx, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L996:
	movq	-80(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L940
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L941
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L942
	movq	(%r12), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L943
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L944
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L945
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rsi, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L1636:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L948
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L949
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L948:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L950
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L951
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L950:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L946:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1635
.L952:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L946
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1636
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L952
	.p2align 4,,10
	.p2align 3
.L1635:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L945:
	testq	%rbx, %rbx
	je	.L953
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L953:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L944:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L954
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L955
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L1638:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L958
	call	_ZdlPv@PLT
.L958:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L959
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L960
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L959:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L961
	call	_ZdlPv@PLT
.L961:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L962
	call	_ZdlPv@PLT
.L962:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L963
	call	_ZdlPv@PLT
.L963:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L956:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1637
.L964:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L956
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1638
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L964
	.p2align 4,,10
	.p2align 3
.L1637:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L955:
	testq	%r14, %r14
	je	.L965
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L965:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L954:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L966
	call	_ZdlPv@PLT
.L966:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L967
	call	_ZdlPv@PLT
.L967:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L968
	call	_ZdlPv@PLT
.L968:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L942:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L969
	movq	(%r12), %rax
	movq	-56(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L970
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L971
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L972
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rcx, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1640:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L975
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L976
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L975:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L977
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L978
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L977:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L973:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1639
.L979:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L973
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1640
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L979
	.p2align 4,,10
	.p2align 3
.L1639:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L972:
	testq	%rbx, %rbx
	je	.L980
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L980:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L971:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L981
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L982
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L1642:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L985
	call	_ZdlPv@PLT
.L985:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L986
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L987
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L986:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L988
	call	_ZdlPv@PLT
.L988:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L989
	call	_ZdlPv@PLT
.L989:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L990
	call	_ZdlPv@PLT
.L990:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L983:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1641
.L991:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L983
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1642
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L991
	.p2align 4,,10
	.p2align 3
.L1641:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L982:
	testq	%r14, %r14
	je	.L992
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L992:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L981:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L993
	call	_ZdlPv@PLT
.L993:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L994
	call	_ZdlPv@PLT
.L994:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L995
	call	_ZdlPv@PLT
.L995:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L969:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L940:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%rax, -120(%rbp)
	jne	.L996
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
.L939:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L997
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L997:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L938:
	movq	152(%rbx), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L998
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -104(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L999
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1033:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1000
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1001
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1002
	call	_ZdlPv@PLT
.L1002:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1003
	movq	0(%r13), %rax
	movq	-56(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1004
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L1005
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1006
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -136(%rbp)
	movq	%r13, -152(%rbp)
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1644:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1009
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L1010
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1009:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1011
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L1012
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1011:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1007:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1643
.L1013:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1007
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1644
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L1013
	.p2align 4,,10
	.p2align 3
.L1643:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	-152(%rbp), %r13
	movq	(%rax), %r14
.L1006:
	testq	%r14, %r14
	je	.L1014
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1014:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1005:
	movq	152(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L1015
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1016
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1646:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1019
	call	_ZdlPv@PLT
.L1019:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1020
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L1021
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1020:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1022
	call	_ZdlPv@PLT
.L1022:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1023
	call	_ZdlPv@PLT
.L1023:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1024
	call	_ZdlPv@PLT
.L1024:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1017:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1645
.L1025:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1017
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1646
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L1025
	.p2align 4,,10
	.p2align 3
.L1645:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L1016:
	testq	%r14, %r14
	je	.L1026
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1026:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1015:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1027
	call	_ZdlPv@PLT
.L1027:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1028
	call	_ZdlPv@PLT
.L1028:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1029
	call	_ZdlPv@PLT
.L1029:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1003:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1030
	call	_ZdlPv@PLT
.L1030:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1031
	call	_ZdlPv@PLT
.L1031:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1032
	call	_ZdlPv@PLT
.L1032:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1000:
	addq	$8, %rbx
	cmpq	%rbx, -104(%rbp)
	jne	.L1033
	movq	-80(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L999:
	testq	%rdi, %rdi
	je	.L1034
	call	_ZdlPv@PLT
.L1034:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L998:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1035
	call	_ZdlPv@PLT
.L1035:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1036
	call	_ZdlPv@PLT
.L1036:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1037
	call	_ZdlPv@PLT
.L1037:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L936:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1038
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%rsi, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1039
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L1040
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -120(%rbp)
	movq	%rax, -80(%rbp)
	cmpq	%rax, %rsi
	je	.L1041
	movq	%rbx, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L1098:
	movq	-80(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L1042
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1043
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L1044
	movq	(%r12), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1045
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L1046
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L1047
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rsi, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1648:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1050
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L1051
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1050:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L1052
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L1053
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1052:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1048:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1647
.L1054:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1048
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1648
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L1054
	.p2align 4,,10
	.p2align 3
.L1647:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L1047:
	testq	%rbx, %rbx
	je	.L1055
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1055:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1046:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L1056
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L1057
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1650:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1060
	call	_ZdlPv@PLT
.L1060:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1061
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L1062
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1061:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1063
	call	_ZdlPv@PLT
.L1063:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1064
	call	_ZdlPv@PLT
.L1064:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1065
	call	_ZdlPv@PLT
.L1065:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1058:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1649
.L1066:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1058
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1650
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L1066
	.p2align 4,,10
	.p2align 3
.L1649:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L1057:
	testq	%r14, %r14
	je	.L1067
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1067:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1056:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1068
	call	_ZdlPv@PLT
.L1068:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1069
	call	_ZdlPv@PLT
.L1069:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1070
	call	_ZdlPv@PLT
.L1070:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1044:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L1071
	movq	(%r12), %rax
	movq	-56(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1072
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L1073
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L1074
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rcx, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1652:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1077
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L1078
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1077:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L1079
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L1080
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1079:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1075:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1651
.L1081:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1075
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1652
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L1081
	.p2align 4,,10
	.p2align 3
.L1651:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L1074:
	testq	%rbx, %rbx
	je	.L1082
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1082:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1073:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L1083
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L1084
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1654:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1087
	call	_ZdlPv@PLT
.L1087:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1088
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L1089
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1088:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1090
	call	_ZdlPv@PLT
.L1090:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1091
	call	_ZdlPv@PLT
.L1091:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1092
	call	_ZdlPv@PLT
.L1092:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1085:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1653
.L1093:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1085
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1654
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L1093
	.p2align 4,,10
	.p2align 3
.L1653:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L1084:
	testq	%r14, %r14
	je	.L1094
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1094:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1083:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1095
	call	_ZdlPv@PLT
.L1095:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1096
	call	_ZdlPv@PLT
.L1096:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1097
	call	_ZdlPv@PLT
.L1097:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1071:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1042:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%rax, -120(%rbp)
	jne	.L1098
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
.L1041:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L1099
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1099:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1040:
	movq	152(%rbx), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L1100
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -104(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L1101
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1135:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1102
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1103
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1104
	call	_ZdlPv@PLT
.L1104:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1105
	movq	0(%r13), %rax
	movq	-56(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1106
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L1107
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1108
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -136(%rbp)
	movq	%r13, -152(%rbp)
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1656:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1111
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L1112
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1111:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1113
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L1114
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1113:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1109:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1655
.L1115:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1109
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1656
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L1115
	.p2align 4,,10
	.p2align 3
.L1655:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	-152(%rbp), %r13
	movq	(%rax), %r14
.L1108:
	testq	%r14, %r14
	je	.L1116
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1116:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1107:
	movq	152(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L1117
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1118
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1658:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1121
	call	_ZdlPv@PLT
.L1121:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1122
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L1123
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1122:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1124
	call	_ZdlPv@PLT
.L1124:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1125
	call	_ZdlPv@PLT
.L1125:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1126
	call	_ZdlPv@PLT
.L1126:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1119:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1657
.L1127:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1119
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1658
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L1127
	.p2align 4,,10
	.p2align 3
.L1657:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L1118:
	testq	%r14, %r14
	je	.L1128
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1128:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1117:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1129
	call	_ZdlPv@PLT
.L1129:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1130
	call	_ZdlPv@PLT
.L1130:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1131
	call	_ZdlPv@PLT
.L1131:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1105:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1132
	call	_ZdlPv@PLT
.L1132:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1133
	call	_ZdlPv@PLT
.L1133:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1134
	call	_ZdlPv@PLT
.L1134:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1102:
	addq	$8, %rbx
	cmpq	%rbx, -104(%rbp)
	jne	.L1135
	movq	-80(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1101:
	testq	%rdi, %rdi
	je	.L1136
	call	_ZdlPv@PLT
.L1136:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1100:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1137
	call	_ZdlPv@PLT
.L1137:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1138
	call	_ZdlPv@PLT
.L1138:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1139
	call	_ZdlPv@PLT
.L1139:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1038:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L934:
	addq	$8, -72(%rbp)
	movq	-72(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	jne	.L1140
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
.L933:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L1141
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1141:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L932:
	movq	-64(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L1142
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -112(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rbx
	je	.L1143
	.p2align 4,,10
	.p2align 3
.L1252:
	movq	-56(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1144
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1145
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1146
	call	_ZdlPv@PLT
.L1146:
	movq	136(%rbx), %rcx
	movq	%rcx, -72(%rbp)
	testq	%rcx, %rcx
	je	.L1147
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%rsi, -88(%rbp)
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1148
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L1149
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -120(%rbp)
	movq	%rax, -80(%rbp)
	cmpq	%rax, %rcx
	je	.L1150
	movq	%rbx, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L1207:
	movq	-80(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L1151
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1152
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L1153
	movq	(%r12), %rax
	movq	-88(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1154
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L1155
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L1156
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rsi, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1660:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1159
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L1160
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1159:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L1161
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L1162
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1161:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1157:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1659
.L1163:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1157
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1660
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L1163
	.p2align 4,,10
	.p2align 3
.L1659:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L1156:
	testq	%rbx, %rbx
	je	.L1164
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1164:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1155:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L1165
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L1166
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L1175
	.p2align 4,,10
	.p2align 3
.L1662:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1169
	call	_ZdlPv@PLT
.L1169:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1170
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-88(%rbp), %rax
	jne	.L1171
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1170:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1172
	call	_ZdlPv@PLT
.L1172:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1173
	call	_ZdlPv@PLT
.L1173:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1174
	call	_ZdlPv@PLT
.L1174:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1167:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1661
.L1175:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1167
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1662
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L1175
	.p2align 4,,10
	.p2align 3
.L1661:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L1166:
	testq	%r14, %r14
	je	.L1176
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1176:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1165:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1177
	call	_ZdlPv@PLT
.L1177:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1178
	call	_ZdlPv@PLT
.L1178:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1179
	call	_ZdlPv@PLT
.L1179:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1153:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L1180
	movq	(%r12), %rax
	movq	-88(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1181
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L1182
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L1183
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rcx, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1664:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1186
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L1187
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1186:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L1188
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L1189
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1188:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1184:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1663
.L1190:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1184
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1664
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L1190
	.p2align 4,,10
	.p2align 3
.L1663:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L1183:
	testq	%rbx, %rbx
	je	.L1191
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1191:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1182:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L1192
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L1193
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1666:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1196
	call	_ZdlPv@PLT
.L1196:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1197
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-88(%rbp), %rax
	jne	.L1198
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1197:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1199
	call	_ZdlPv@PLT
.L1199:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1200
	call	_ZdlPv@PLT
.L1200:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1201
	call	_ZdlPv@PLT
.L1201:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1194:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1665
.L1202:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1194
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1666
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L1202
	.p2align 4,,10
	.p2align 3
.L1665:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L1193:
	testq	%r14, %r14
	je	.L1203
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1203:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1192:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1204
	call	_ZdlPv@PLT
.L1204:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1205
	call	_ZdlPv@PLT
.L1205:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1206
	call	_ZdlPv@PLT
.L1206:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1180:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1151:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%rax, -120(%rbp)
	jne	.L1207
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
.L1150:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L1208
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1208:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1149:
	movq	-72(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L1209
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -104(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L1210
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1211
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1212
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1213
	call	_ZdlPv@PLT
.L1213:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1214
	movq	0(%r13), %rax
	movq	-88(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1215
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L1216
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1217
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -136(%rbp)
	movq	%r13, -152(%rbp)
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1668:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1220
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L1221
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1220:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1222
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L1223
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1222:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1218:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1667
.L1224:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1218
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1668
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L1224
	.p2align 4,,10
	.p2align 3
.L1667:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	-152(%rbp), %r13
	movq	(%rax), %r14
.L1217:
	testq	%r14, %r14
	je	.L1225
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1225:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1216:
	movq	152(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L1226
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1227
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1670:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1230
	call	_ZdlPv@PLT
.L1230:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1231
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-88(%rbp), %rax
	jne	.L1232
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1231:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1233
	call	_ZdlPv@PLT
.L1233:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1234
	call	_ZdlPv@PLT
.L1234:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1235
	call	_ZdlPv@PLT
.L1235:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1228:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1669
.L1236:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1228
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1670
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L1236
	.p2align 4,,10
	.p2align 3
.L1669:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L1227:
	testq	%r14, %r14
	je	.L1237
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1237:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1226:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1238
	call	_ZdlPv@PLT
.L1238:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1239
	call	_ZdlPv@PLT
.L1239:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1240
	call	_ZdlPv@PLT
.L1240:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1214:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1241
	call	_ZdlPv@PLT
.L1241:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1242
	call	_ZdlPv@PLT
.L1242:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1243
	call	_ZdlPv@PLT
.L1243:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1211:
	addq	$8, %rbx
	cmpq	%rbx, -104(%rbp)
	jne	.L1244
	movq	-80(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1210:
	testq	%rdi, %rdi
	je	.L1245
	call	_ZdlPv@PLT
.L1245:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1209:
	movq	-72(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1246
	call	_ZdlPv@PLT
.L1246:
	movq	-72(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1247
	call	_ZdlPv@PLT
.L1247:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1248
	call	_ZdlPv@PLT
.L1248:
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1147:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1249
	call	_ZdlPv@PLT
.L1249:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1250
	call	_ZdlPv@PLT
.L1250:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1251
	call	_ZdlPv@PLT
.L1251:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1144:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	jne	.L1252
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L1143:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L1253
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1253:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1142:
	movq	-64(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1254
	call	_ZdlPv@PLT
.L1254:
	movq	-64(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1255
	call	_ZdlPv@PLT
.L1255:
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1256
	call	_ZdlPv@PLT
.L1256:
	movq	-64(%rbp), %rdi
	addq	$120, %rsp
	movl	$168, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L935:
	.cfi_restore_state
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L1145:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1212:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1152:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1103:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L941:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L1148:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1147
	.p2align 4,,10
	.p2align 3
.L1039:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L937:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L1181:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L970:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L1215:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1214
	.p2align 4,,10
	.p2align 3
.L943:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1072:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1154:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1187:
	call	*%rdx
	jmp	.L1186
	.p2align 4,,10
	.p2align 3
.L1112:
	call	*%rdx
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1078:
	call	*%rdx
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1053:
	call	*%rdx
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L949:
	call	*%rdx
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L951:
	call	*%rdx
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L1012:
	call	*%rdx
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1051:
	call	*%rdx
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1010:
	call	*%rdx
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L987:
	call	*%rax
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L978:
	call	*%rdx
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1221:
	call	*%rdx
	jmp	.L1220
	.p2align 4,,10
	.p2align 3
.L1160:
	call	*%rdx
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1189:
	call	*%rdx
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1162:
	call	*%rdx
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1089:
	call	*%rax
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1198:
	call	*%rax
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1062:
	call	*%rax
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1021:
	call	*%rax
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1123:
	call	*%rax
	jmp	.L1122
	.p2align 4,,10
	.p2align 3
.L960:
	call	*%rax
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L1232:
	call	*%rax
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1171:
	call	*%rax
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1080:
	call	*%rdx
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L976:
	call	*%rdx
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L1114:
	call	*%rdx
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1223:
	call	*%rdx
	jmp	.L1222
	.cfi_endproc
.LFE4642:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev:
.LFB4753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L1672
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r13
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1673
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L1674
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	movq	%rdx, -56(%rbp)
	cmpq	%r14, %rdx
	jne	.L1682
	jmp	.L1675
	.p2align 4,,10
	.p2align 3
.L1789:
	movq	16(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L1678
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1679
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1678:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1680
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1681
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1680:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1676:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L1788
.L1682:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L1676
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1789
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L1682
	.p2align 4,,10
	.p2align 3
.L1788:
	movq	-64(%rbp), %rax
	movq	(%rax), %r14
.L1675:
	testq	%r14, %r14
	je	.L1683
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1683:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1674:
	movq	152(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L1684
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -56(%rbp)
	cmpq	%r14, %rcx
	jne	.L1694
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1791:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1688
	call	_ZdlPv@PLT
.L1688:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1689
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1690
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1689:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1691
	call	_ZdlPv@PLT
.L1691:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1692
	call	_ZdlPv@PLT
.L1692:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1693
	call	_ZdlPv@PLT
.L1693:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1686:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L1790
.L1694:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L1686
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1791
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L1694
	.p2align 4,,10
	.p2align 3
.L1790:
	movq	-64(%rbp), %rax
	movq	(%rax), %r14
.L1685:
	testq	%r14, %r14
	je	.L1695
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1695:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1684:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1696
	call	_ZdlPv@PLT
.L1696:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1697
	call	_ZdlPv@PLT
.L1697:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1698
	call	_ZdlPv@PLT
.L1698:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1672:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L1671
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r13
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1700
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L1701
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	jne	.L1709
	jmp	.L1702
	.p2align 4,,10
	.p2align 3
.L1793:
	movq	16(%r8), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r8)
	testq	%rdi, %rdi
	je	.L1705
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1706
	movq	%r8, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L1705:
	movq	8(%r8), %rdi
	testq	%rdi, %rdi
	je	.L1707
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1708
	movq	%r8, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L1707:
	movl	$24, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1703:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1792
.L1709:
	movq	(%rbx), %r8
	testq	%r8, %r8
	je	.L1703
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1793
	addq	$8, %rbx
	movq	%r8, %rdi
	call	*%rax
	cmpq	%rbx, %r15
	jne	.L1709
	.p2align 4,,10
	.p2align 3
.L1792:
	movq	(%r14), %rbx
.L1702:
	testq	%rbx, %rbx
	je	.L1710
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1710:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1701:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L1711
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -56(%rbp)
	cmpq	%r14, %rax
	jne	.L1721
	jmp	.L1712
	.p2align 4,,10
	.p2align 3
.L1795:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1715
	call	_ZdlPv@PLT
.L1715:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1716
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1717
	movq	%rdi, -64(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1716:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1718
	call	_ZdlPv@PLT
.L1718:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1719
	call	_ZdlPv@PLT
.L1719:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1720
	call	_ZdlPv@PLT
.L1720:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1713:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L1794
.L1721:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L1713
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1795
	movq	%rbx, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L1721
	.p2align 4,,10
	.p2align 3
.L1794:
	movq	(%r15), %r14
.L1712:
	testq	%r14, %r14
	je	.L1722
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1722:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1711:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1723
	call	_ZdlPv@PLT
.L1723:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1724
	call	_ZdlPv@PLT
.L1724:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1725
	call	_ZdlPv@PLT
.L1725:
	addq	$40, %rsp
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1671:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1673:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1672
	.p2align 4,,10
	.p2align 3
.L1700:
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1679:
	.cfi_restore_state
	call	*%rax
	jmp	.L1678
	.p2align 4,,10
	.p2align 3
.L1706:
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L1705
	.p2align 4,,10
	.p2align 3
.L1681:
	call	*%rax
	jmp	.L1680
	.p2align 4,,10
	.p2align 3
.L1690:
	call	*%rax
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1708:
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1717:
	call	*%rax
	jmp	.L1716
	.cfi_endproc
.LFE4753:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev:
.LFB4713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	168(%rbx), %rax
	subq	$40, %rsp
	movq	%r15, (%rdi)
	movq	152(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L1797
	call	_ZdlPv@PLT
.L1797:
	movq	136(%rbx), %r12
	testq	%r12, %r12
	je	.L1798
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1799
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L1800
	movq	8(%rax), %rsi
	movq	(%rax), %r13
	movq	%rsi, -56(%rbp)
	cmpq	%r13, %rsi
	jne	.L1808
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1862:
	movq	16(%r14), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L1804
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1805
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1804:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1806
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1807
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1806:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1802:
	addq	$8, %r13
	cmpq	%r13, -56(%rbp)
	je	.L1861
.L1808:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L1802
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1862
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -56(%rbp)
	jne	.L1808
	.p2align 4,,10
	.p2align 3
.L1861:
	movq	-64(%rbp), %rax
	movq	(%rax), %r13
.L1801:
	testq	%r13, %r13
	je	.L1809
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1809:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1800:
	movq	152(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L1810
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	movq	%rcx, -56(%rbp)
	cmpq	%r13, %rcx
	jne	.L1820
	jmp	.L1811
	.p2align 4,,10
	.p2align 3
.L1864:
	movq	152(%r14), %rdi
	leaq	168(%r14), %rax
	movq	%r15, (%r14)
	cmpq	%rax, %rdi
	je	.L1814
	call	_ZdlPv@PLT
.L1814:
	movq	136(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1815
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1816
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1815:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1817
	call	_ZdlPv@PLT
.L1817:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1818
	call	_ZdlPv@PLT
.L1818:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1819
	call	_ZdlPv@PLT
.L1819:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1812:
	addq	$8, %r13
	cmpq	%r13, -56(%rbp)
	je	.L1863
.L1820:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L1812
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1864
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -56(%rbp)
	jne	.L1820
	.p2align 4,,10
	.p2align 3
.L1863:
	movq	-64(%rbp), %rax
	movq	(%rax), %r13
.L1811:
	testq	%r13, %r13
	je	.L1821
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1821:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1810:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1822
	call	_ZdlPv@PLT
.L1822:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1823
	call	_ZdlPv@PLT
.L1823:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1824
	call	_ZdlPv@PLT
.L1824:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1798:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1825
	call	_ZdlPv@PLT
.L1825:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1826
	call	_ZdlPv@PLT
.L1826:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L1796
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1796:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1799:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L1816:
	call	*%rax
	jmp	.L1815
	.p2align 4,,10
	.p2align 3
.L1807:
	call	*%rax
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1805:
	call	*%rax
	jmp	.L1804
	.cfi_endproc
.LFE4713:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev:
.LFB4459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	312(%rdi), %r12
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L1866
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1867
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1868
	call	_ZdlPv@PLT
.L1868:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1869
	call	_ZdlPv@PLT
.L1869:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1866:
	movq	304(%r14), %rsi
	movq	%rsi, -80(%rbp)
	testq	%rsi, %rsi
	je	.L1870
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1871
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L1872
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -96(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rbx
	je	.L1873
	movq	%r14, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L2130:
	movq	-56(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -72(%rbp)
	testq	%rcx, %rcx
	je	.L1874
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1875
	movq	16(%rcx), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%rbx, %rbx
	je	.L1876
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1877
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L1878
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -64(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L1879
	movq	%rbx, -120(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1936:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1880
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1881
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1882
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1883
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L1884
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L1885
	movq	%rbx, -136(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -128(%rbp)
	jmp	.L1892
	.p2align 4,,10
	.p2align 3
.L2970:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L1888
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1889
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1888:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L1890
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1891
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1890:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1886:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2969
.L1892:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L1886
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2970
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L3005:
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %r13
	movq	(%rax), %r14
.L2108:
	testq	%r14, %r14
	je	.L2118
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2118:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2107:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2119
	call	_ZdlPv@PLT
.L2119:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2120
	call	_ZdlPv@PLT
.L2120:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2121
	call	_ZdlPv@PLT
.L2121:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2095:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2122
	call	_ZdlPv@PLT
.L2122:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2123
	call	_ZdlPv@PLT
.L2123:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2124
	call	_ZdlPv@PLT
.L2124:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2092:
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L2125
	movq	-88(%rbp), %rax
	movq	-120(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2091:
	testq	%rdi, %rdi
	je	.L2126
	call	_ZdlPv@PLT
.L2126:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2090:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2127
	call	_ZdlPv@PLT
.L2127:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2128
	call	_ZdlPv@PLT
.L2128:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2129
	call	_ZdlPv@PLT
.L2129:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2003:
	movq	-72(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1874:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -96(%rbp)
	jne	.L2130
	movq	-112(%rbp), %rax
	movq	-152(%rbp), %r14
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L1873:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L2131
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2131:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1872:
	movq	-80(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2132
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -104(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rbx
	je	.L2133
	movq	%r14, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L2367:
	movq	-64(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -56(%rbp)
	testq	%rcx, %rcx
	je	.L2134
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2135
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rcx), %rdi
	movq	%rax, (%rcx)
	leaq	168(%rcx), %rax
	cmpq	%rax, %rdi
	je	.L2136
	call	_ZdlPv@PLT
.L2136:
	movq	-56(%rbp), %rax
	movq	136(%rax), %rdx
	movq	%rdx, -88(%rbp)
	testq	%rdx, %rdx
	je	.L2137
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2138
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L2139
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -112(%rbp)
	movq	%rax, -72(%rbp)
	cmpq	%rax, %rbx
	je	.L2140
	.p2align 4,,10
	.p2align 3
.L2272:
	movq	-72(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2141
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2142
	movq	16(%rbx), %rdx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%rdx, -96(%rbp)
	testq	%rdx, %rdx
	je	.L2143
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2144
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L2145
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -128(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L2146
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2178:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2147
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2148
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2149
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2150
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2149:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2151
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2152
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2153
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2154
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -152(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2161
	.p2align 4,,10
	.p2align 3
.L2972:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2157
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2158
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2157:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2159
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2160
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2159:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2155:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2971
.L2161:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2155
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2972
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L2155
	.p2align 4,,10
	.p2align 3
.L2981:
	movq	-128(%rbp), %r13
	movq	0(%r13), %r12
.L2342:
	testq	%r12, %r12
	je	.L2352
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2352:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2341:
	movq	-96(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2353
	call	_ZdlPv@PLT
.L2353:
	movq	-96(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2354
	call	_ZdlPv@PLT
.L2354:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2355
	call	_ZdlPv@PLT
.L2355:
	movq	-96(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2279:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2356
	call	_ZdlPv@PLT
.L2356:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2357
	call	_ZdlPv@PLT
.L2357:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2358
	call	_ZdlPv@PLT
.L2358:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2276:
	addq	$8, -72(%rbp)
	movq	-72(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	jne	.L2359
	movq	-136(%rbp), %rax
	movq	(%rax), %rbx
.L2275:
	testq	%rbx, %rbx
	je	.L2360
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L2360:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2274:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2361
	call	_ZdlPv@PLT
.L2361:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2362
	call	_ZdlPv@PLT
.L2362:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2363
	call	_ZdlPv@PLT
.L2363:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2137:
	movq	-56(%rbp), %rax
	movq	96(%rax), %rdi
	addq	$112, %rax
	cmpq	%rax, %rdi
	je	.L2364
	call	_ZdlPv@PLT
.L2364:
	movq	-56(%rbp), %rax
	movq	48(%rax), %rdi
	addq	$64, %rax
	cmpq	%rax, %rdi
	je	.L2365
	call	_ZdlPv@PLT
.L2365:
	movq	-56(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2366
	call	_ZdlPv@PLT
.L2366:
	movq	-56(%rbp), %rdi
	movl	$192, %esi
	call	_ZdlPvm@PLT
.L2134:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	jne	.L2367
	movq	-120(%rbp), %rax
	movq	-160(%rbp), %r14
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L2133:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L2368
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2368:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2132:
	movq	-80(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2369
	call	_ZdlPv@PLT
.L2369:
	movq	-80(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2370
	call	_ZdlPv@PLT
.L2370:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2371
	call	_ZdlPv@PLT
.L2371:
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1870:
	movq	264(%r14), %rdi
	leaq	280(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2372
	call	_ZdlPv@PLT
.L2372:
	movq	216(%r14), %rdi
	leaq	232(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2373
	call	_ZdlPv@PLT
.L2373:
	movq	168(%r14), %rdi
	leaq	184(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2374
	call	_ZdlPv@PLT
.L2374:
	movq	152(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2375
	movq	(%rdi), %rax
	call	*24(%rax)
.L2375:
	movq	112(%r14), %rdi
	leaq	128(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2376
	call	_ZdlPv@PLT
.L2376:
	movq	64(%r14), %rdi
	leaq	80(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2377
	call	_ZdlPv@PLT
.L2377:
	movq	16(%r14), %rdi
	leaq	32(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2378
	call	_ZdlPv@PLT
.L2378:
	addq	$152, %rsp
	movq	%r14, %rdi
	movl	$320, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1881:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L1880:
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L1936
	movq	-88(%rbp), %rax
	movq	-120(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1879:
	testq	%rdi, %rdi
	je	.L1937
	call	_ZdlPv@PLT
.L1937:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1878:
	movq	152(%rbx), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L1938
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -104(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rsi
	je	.L1939
	movq	%rbx, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L1998:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1940
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1941
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1942
	call	_ZdlPv@PLT
.L1942:
	movq	136(%rbx), %rcx
	movq	%rcx, -88(%rbp)
	testq	%rcx, %rcx
	je	.L1943
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1944
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L1945
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -120(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L1946
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1978:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1947
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1948
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1949
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1950
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1949:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1951
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1952
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1953
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L1954
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -144(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1961
	.p2align 4,,10
	.p2align 3
.L2974:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1957
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1958
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1957:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1959
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1960
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1959:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1955:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2973
.L1961:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1955
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2974
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L2987:
	movq	-120(%rbp), %r13
	movq	0(%r13), %r12
.L1981:
	testq	%r12, %r12
	je	.L1991
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1991:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1980:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1992
	call	_ZdlPv@PLT
.L1992:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1993
	call	_ZdlPv@PLT
.L1993:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1994
	call	_ZdlPv@PLT
.L1994:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1943:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1995
	call	_ZdlPv@PLT
.L1995:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1996
	call	_ZdlPv@PLT
.L1996:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1997
	call	_ZdlPv@PLT
.L1997:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1940:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	jne	.L1998
	movq	-128(%rbp), %rax
	movq	-160(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L1939:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L1999
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1999:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1938:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2000
	call	_ZdlPv@PLT
.L2000:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2001
	call	_ZdlPv@PLT
.L2001:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2002
	call	_ZdlPv@PLT
.L2002:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1876:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2003
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2004
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L2005
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -104(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rdx
	je	.L2006
	movq	%rbx, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L2088:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2007
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2008
	movq	16(%rbx), %rcx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%rcx, -88(%rbp)
	testq	%rcx, %rcx
	je	.L2009
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2010
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L2011
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -120(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L2012
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2044:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2013
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2014
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2015
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2016
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2015:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2017
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2018
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2019
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2020
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -144(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2027
	.p2align 4,,10
	.p2align 3
.L2976:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2023
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2024
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2023:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2025
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2026
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2025:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2021:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2975
.L2027:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2021
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2976
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L2021
	.p2align 4,,10
	.p2align 3
.L2008:
	movq	%rbx, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L2007:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	jne	.L2088
	movq	-128(%rbp), %rax
	movq	-160(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L2006:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L2089
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2089:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2005:
	movq	152(%rbx), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L2090
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -64(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L2091
	movq	%rbx, -120(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2125:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2092
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2093
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2094
	call	_ZdlPv@PLT
.L2094:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2095
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2096
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2097
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2098
	movq	%rbx, -128(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -104(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L2105
	.p2align 4,,10
	.p2align 3
.L2978:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2101
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2102
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2101:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2103
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2104
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2103:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2099:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2977
.L2105:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2099
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2978
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L3011:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2250:
	testq	%r14, %r14
	je	.L2260
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2260:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2249:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2261
	call	_ZdlPv@PLT
.L2261:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2262
	call	_ZdlPv@PLT
.L2262:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2263
	call	_ZdlPv@PLT
.L2263:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2237:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2264
	call	_ZdlPv@PLT
.L2264:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2265
	call	_ZdlPv@PLT
.L2265:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2266
	call	_ZdlPv@PLT
.L2266:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2234:
	addq	$8, %rbx
	cmpq	%rbx, -128(%rbp)
	jne	.L2267
	movq	-144(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2233:
	testq	%rdi, %rdi
	je	.L2268
	call	_ZdlPv@PLT
.L2268:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2232:
	movq	-96(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2269
	call	_ZdlPv@PLT
.L2269:
	movq	-96(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2270
	call	_ZdlPv@PLT
.L2270:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2271
	call	_ZdlPv@PLT
.L2271:
	movq	-96(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2195:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2141:
	addq	$8, -72(%rbp)
	movq	-72(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	jne	.L2272
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
.L2140:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L2273
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2273:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2139:
	movq	-88(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L2274
	movq	8(%rax), %rsi
	movq	(%rax), %rbx
	movq	%rsi, -112(%rbp)
	cmpq	%rbx, %rsi
	je	.L2275
	movq	%rbx, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L2359:
	movq	-72(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2276
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2277
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2278
	call	_ZdlPv@PLT
.L2278:
	movq	136(%rbx), %rcx
	movq	%rcx, -96(%rbp)
	testq	%rcx, %rcx
	je	.L2279
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2280
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L2281
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -128(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L2282
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2339:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2283
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2284
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2285
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2286
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2287
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L2288
	movq	%rbx, -184(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -176(%rbp)
	jmp	.L2295
	.p2align 4,,10
	.p2align 3
.L2980:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2291
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2292
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2291:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2293
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2294
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2293:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2289:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2979
.L2295:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2289
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2980
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L2289
	.p2align 4,,10
	.p2align 3
.L2284:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L2283:
	addq	$8, %rbx
	cmpq	%rbx, -128(%rbp)
	jne	.L2339
	movq	-144(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2282:
	testq	%rdi, %rdi
	je	.L2340
	call	_ZdlPv@PLT
.L2340:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2281:
	movq	-96(%rbp), %rax
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L2341
	movq	8(%r13), %r15
	movq	0(%r13), %r12
	cmpq	%r12, %r15
	je	.L2342
	movq	%r13, -128(%rbp)
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2982:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2345
	call	_ZdlPv@PLT
.L2345:
	movq	136(%r14), %r13
	testq	%r13, %r13
	je	.L2346
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2347
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2346:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2348
	call	_ZdlPv@PLT
.L2348:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2349
	call	_ZdlPv@PLT
.L2349:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2350
	call	_ZdlPv@PLT
.L2350:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2343:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2981
.L2351:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L2343
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2982
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2343
	.p2align 4,,10
	.p2align 3
.L2148:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L2147:
	addq	$8, %rbx
	cmpq	%rbx, -128(%rbp)
	jne	.L2178
	movq	-144(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2146:
	testq	%rdi, %rdi
	je	.L2179
	call	_ZdlPv@PLT
.L2179:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2145:
	movq	-96(%rbp), %rax
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L2180
	movq	8(%r13), %r15
	movq	0(%r13), %r12
	cmpq	%r12, %r15
	je	.L2181
	movq	%r13, -128(%rbp)
	jmp	.L2190
	.p2align 4,,10
	.p2align 3
.L2984:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2184
	call	_ZdlPv@PLT
.L2184:
	movq	136(%r14), %r13
	testq	%r13, %r13
	je	.L2185
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2186
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2185:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2187
	call	_ZdlPv@PLT
.L2187:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2188
	call	_ZdlPv@PLT
.L2188:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2189
	call	_ZdlPv@PLT
.L2189:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2182:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2983
.L2190:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L2182
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2984
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2182
	.p2align 4,,10
	.p2align 3
.L2014:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L2013:
	addq	$8, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L2044
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2012:
	testq	%rdi, %rdi
	je	.L2045
	call	_ZdlPv@PLT
.L2045:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2011:
	movq	-88(%rbp), %rax
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L2046
	movq	8(%r13), %r15
	movq	0(%r13), %r12
	cmpq	%r12, %r15
	je	.L2047
	movq	%r13, -120(%rbp)
	jmp	.L2056
	.p2align 4,,10
	.p2align 3
.L2986:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2050
	call	_ZdlPv@PLT
.L2050:
	movq	136(%r14), %r13
	testq	%r13, %r13
	je	.L2051
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2052
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2051:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2053
	call	_ZdlPv@PLT
.L2053:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2054
	call	_ZdlPv@PLT
.L2054:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2055
	call	_ZdlPv@PLT
.L2055:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2048:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2985
.L2056:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L2048
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2986
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L1948:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L1947:
	addq	$8, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L1978
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1946:
	testq	%rdi, %rdi
	je	.L1979
	call	_ZdlPv@PLT
.L1979:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1945:
	movq	-88(%rbp), %rax
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L1980
	movq	8(%r13), %r15
	movq	0(%r13), %r12
	cmpq	%r12, %r15
	je	.L1981
	movq	%r13, -120(%rbp)
	jmp	.L1990
	.p2align 4,,10
	.p2align 3
.L2988:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1984
	call	_ZdlPv@PLT
.L1984:
	movq	136(%r14), %r13
	testq	%r13, %r13
	je	.L1985
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1986
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1985:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1987
	call	_ZdlPv@PLT
.L1987:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1988
	call	_ZdlPv@PLT
.L1988:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1989
	call	_ZdlPv@PLT
.L1989:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1982:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2987
.L1990:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L1982
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2988
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1982
	.p2align 4,,10
	.p2align 3
.L2999:
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %r13
	movq	(%rax), %r14
.L1895:
	testq	%r14, %r14
	je	.L1905
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1905:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1894:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1906
	call	_ZdlPv@PLT
.L1906:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1907
	call	_ZdlPv@PLT
.L1907:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1908
	call	_ZdlPv@PLT
.L1908:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1882:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1909
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1910
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L1911
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L1912
	movq	%rbx, -136(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -128(%rbp)
	jmp	.L1919
	.p2align 4,,10
	.p2align 3
.L2990:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L1915
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1916
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1915:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L1917
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1918
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1917:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1913:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2989
.L1919:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L1913
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2990
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L1913
	.p2align 4,,10
	.p2align 3
.L3001:
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %r13
	movq	(%rax), %r14
.L1922:
	testq	%r14, %r14
	je	.L1932
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1932:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1921:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1933
	call	_ZdlPv@PLT
.L1933:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1934
	call	_ZdlPv@PLT
.L1934:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1935
	call	_ZdlPv@PLT
.L1935:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1909:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1880
	.p2align 4,,10
	.p2align 3
.L2985:
	movq	-120(%rbp), %r13
	movq	0(%r13), %r12
.L2047:
	testq	%r12, %r12
	je	.L2057
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2057:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2046:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2058
	call	_ZdlPv@PLT
.L2058:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2059
	call	_ZdlPv@PLT
.L2059:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2060
	call	_ZdlPv@PLT
.L2060:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2009:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L2061
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2062
	movq	160(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2063
	movq	8(%r13), %rax
	movq	0(%r13), %r15
	cmpq	%r15, %rax
	je	.L2064
	movq	%rbx, -88(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -120(%rbp)
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2992:
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L2067
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2068
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2067:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L2069
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2070
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2069:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2065:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2991
.L2071:
	movq	(%rbx), %r14
	testq	%r14, %r14
	je	.L2065
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2992
	movq	%r14, %rdi
	call	*%rdx
	jmp	.L2065
	.p2align 4,,10
	.p2align 3
.L3003:
	movq	-120(%rbp), %r14
	movq	-88(%rbp), %r12
	movq	(%r14), %r13
.L2074:
	testq	%r13, %r13
	je	.L2084
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2084:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2073:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2085
	call	_ZdlPv@PLT
.L2085:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2086
	call	_ZdlPv@PLT
.L2086:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2087
	call	_ZdlPv@PLT
.L2087:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2061:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2007
	.p2align 4,,10
	.p2align 3
.L2983:
	movq	-128(%rbp), %r13
	movq	0(%r13), %r12
.L2181:
	testq	%r12, %r12
	je	.L2191
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2191:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2180:
	movq	-96(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2192
	call	_ZdlPv@PLT
.L2192:
	movq	-96(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2193
	call	_ZdlPv@PLT
.L2193:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2194
	call	_ZdlPv@PLT
.L2194:
	movq	-96(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2143:
	movq	8(%rbx), %rsi
	movq	%rsi, -96(%rbp)
	testq	%rsi, %rsi
	je	.L2195
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2196
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L2197
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -128(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L2198
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2230:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2199
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2200
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2201
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2202
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2201:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2203
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2204
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2205
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2206
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -152(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2213
	.p2align 4,,10
	.p2align 3
.L2994:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2209
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2210
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2209:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2211
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2212
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2211:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2207:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2993
.L2213:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2207
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2994
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L2207
	.p2align 4,,10
	.p2align 3
.L2200:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L2199:
	addq	$8, %rbx
	cmpq	%rbx, -128(%rbp)
	jne	.L2230
	movq	-144(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2198:
	testq	%rdi, %rdi
	je	.L2231
	call	_ZdlPv@PLT
.L2231:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2197:
	movq	-96(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L2232
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -128(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L2233
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2267:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2234
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2235
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2236
	call	_ZdlPv@PLT
.L2236:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2237
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2238
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2239
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2240
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -152(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2247
	.p2align 4,,10
	.p2align 3
.L2996:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2243
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2244
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2243:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2245
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2246
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2245:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2241:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2995
.L2247:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2241
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2996
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L2241
	.p2align 4,,10
	.p2align 3
.L3013:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2216:
	testq	%r14, %r14
	je	.L2226
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2226:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2215:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2227
	call	_ZdlPv@PLT
.L2227:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2228
	call	_ZdlPv@PLT
.L2228:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2229
	call	_ZdlPv@PLT
.L2229:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2203:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2199
	.p2align 4,,10
	.p2align 3
.L3019:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2030:
	testq	%r14, %r14
	je	.L2040
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2040:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2029:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2041
	call	_ZdlPv@PLT
.L2041:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2042
	call	_ZdlPv@PLT
.L2042:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2043
	call	_ZdlPv@PLT
.L2043:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2017:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2013
	.p2align 4,,10
	.p2align 3
.L3007:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2298:
	testq	%r14, %r14
	je	.L2308
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2308:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2297:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2309
	call	_ZdlPv@PLT
.L2309:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2310
	call	_ZdlPv@PLT
.L2310:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2311
	call	_ZdlPv@PLT
.L2311:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2285:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2312
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2313
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2314
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	cmpq	%r14, %rdx
	je	.L2315
	movq	%rbx, -184(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -176(%rbp)
	jmp	.L2322
	.p2align 4,,10
	.p2align 3
.L2998:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2318
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2319
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2318:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2320
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2321
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2320:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2316:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2997
.L2322:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2316
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2998
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L2316
	.p2align 4,,10
	.p2align 3
.L3009:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2325:
	testq	%r14, %r14
	je	.L2335
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2335:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2324:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2336
	call	_ZdlPv@PLT
.L2336:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2337
	call	_ZdlPv@PLT
.L2337:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2338
	call	_ZdlPv@PLT
.L2338:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2312:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2283
	.p2align 4,,10
	.p2align 3
.L3015:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L2164:
	testq	%r14, %r14
	je	.L2174
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2174:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2163:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2175
	call	_ZdlPv@PLT
.L2175:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2176
	call	_ZdlPv@PLT
.L2176:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2177
	call	_ZdlPv@PLT
.L2177:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2151:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2147
	.p2align 4,,10
	.p2align 3
.L3017:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L1964:
	testq	%r14, %r14
	je	.L1974
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1974:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1963:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1975
	call	_ZdlPv@PLT
.L1975:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1976
	call	_ZdlPv@PLT
.L1976:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1977
	call	_ZdlPv@PLT
.L1977:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1951:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1947
	.p2align 4,,10
	.p2align 3
.L2969:
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	(%rax), %r14
.L1885:
	testq	%r14, %r14
	je	.L1893
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1893:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1884:
	movq	152(%r13), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L1894
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1895
	movq	%r12, -128(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L1904
	.p2align 4,,10
	.p2align 3
.L3000:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1898
	call	_ZdlPv@PLT
.L1898:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1899
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1900
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1899:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1901
	call	_ZdlPv@PLT
.L1901:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1902
	call	_ZdlPv@PLT
.L1902:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1903
	call	_ZdlPv@PLT
.L1903:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1896:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2999
.L1904:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1896
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3000
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1896
	.p2align 4,,10
	.p2align 3
.L2989:
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	(%rax), %r14
.L1912:
	testq	%r14, %r14
	je	.L1920
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1920:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1911:
	movq	152(%r13), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L1921
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1922
	movq	%r12, -128(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L1931
	.p2align 4,,10
	.p2align 3
.L3002:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1925
	call	_ZdlPv@PLT
.L1925:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1926
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1927
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1926:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1928
	call	_ZdlPv@PLT
.L1928:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1929
	call	_ZdlPv@PLT
.L1929:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1930
	call	_ZdlPv@PLT
.L1930:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1923:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3001
.L1931:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1923
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3002
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1923
	.p2align 4,,10
	.p2align 3
.L2991:
	movq	-88(%rbp), %rbx
	movq	-120(%rbp), %r12
	movq	0(%r13), %r15
.L2064:
	testq	%r15, %r15
	je	.L2072
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2072:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2063:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L2073
	movq	8(%r14), %r15
	movq	(%r14), %r13
	cmpq	%r13, %r15
	je	.L2074
	movq	%r12, -88(%rbp)
	movq	%r14, -120(%rbp)
	jmp	.L2083
	.p2align 4,,10
	.p2align 3
.L3004:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2077
	call	_ZdlPv@PLT
.L2077:
	movq	136(%r12), %r14
	testq	%r14, %r14
	je	.L2078
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2079
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2078:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2080
	call	_ZdlPv@PLT
.L2080:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2081
	call	_ZdlPv@PLT
.L2081:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2082
	call	_ZdlPv@PLT
.L2082:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2075:
	addq	$8, %r13
	cmpq	%r13, %r15
	je	.L3003
.L2083:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L2075
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3004
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2075
	.p2align 4,,10
	.p2align 3
.L2977:
	movq	-104(%rbp), %r12
	movq	-128(%rbp), %rbx
	movq	-136(%rbp), %r13
	movq	(%r14), %r15
.L2098:
	testq	%r15, %r15
	je	.L2106
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2106:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2097:
	movq	152(%r13), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L2107
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2108
	movq	%r12, -128(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L2117
	.p2align 4,,10
	.p2align 3
.L3006:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2111
	call	_ZdlPv@PLT
.L2111:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2112
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2113
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2112:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2114
	call	_ZdlPv@PLT
.L2114:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2115
	call	_ZdlPv@PLT
.L2115:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2116
	call	_ZdlPv@PLT
.L2116:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2109:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3005
.L2117:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2109
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3006
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2109
	.p2align 4,,10
	.p2align 3
.L2979:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %rbx
	movq	(%rax), %r14
.L2288:
	testq	%r14, %r14
	je	.L2296
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2296:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2287:
	movq	152(%r13), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2297
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2298
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2307
	.p2align 4,,10
	.p2align 3
.L3008:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2301
	call	_ZdlPv@PLT
.L2301:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2302
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2303
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2302:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2304
	call	_ZdlPv@PLT
.L2304:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2305
	call	_ZdlPv@PLT
.L2305:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2306
	call	_ZdlPv@PLT
.L2306:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2299:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3007
.L2307:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2299
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3008
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2299
	.p2align 4,,10
	.p2align 3
.L2997:
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %rbx
	movq	(%rax), %r14
.L2315:
	testq	%r14, %r14
	je	.L2323
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2323:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2314:
	movq	152(%r13), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2324
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2325
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2334
	.p2align 4,,10
	.p2align 3
.L3010:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2328
	call	_ZdlPv@PLT
.L2328:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2329
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2330
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2329:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2331
	call	_ZdlPv@PLT
.L2331:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2332
	call	_ZdlPv@PLT
.L2332:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2333
	call	_ZdlPv@PLT
.L2333:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2326:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3009
.L2334:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2326
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3010
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2326
	.p2align 4,,10
	.p2align 3
.L2995:
	movq	-152(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L2240:
	testq	%r15, %r15
	je	.L2248
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2248:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2239:
	movq	152(%r13), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2249
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2250
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2259
	.p2align 4,,10
	.p2align 3
.L3012:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2253
	call	_ZdlPv@PLT
.L2253:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2254
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2255
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2254:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2256
	call	_ZdlPv@PLT
.L2256:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2257
	call	_ZdlPv@PLT
.L2257:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2258
	call	_ZdlPv@PLT
.L2258:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2251:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3011
.L2259:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2251
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3012
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2993:
	movq	-152(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L2206:
	testq	%r15, %r15
	je	.L2214
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2214:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2205:
	movq	152(%r13), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2215
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2216
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2225
	.p2align 4,,10
	.p2align 3
.L3014:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2219
	call	_ZdlPv@PLT
.L2219:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2220
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2221
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2220:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2222
	call	_ZdlPv@PLT
.L2222:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2223
	call	_ZdlPv@PLT
.L2223:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2224
	call	_ZdlPv@PLT
.L2224:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2217:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3013
.L2225:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2217
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3014
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2217
	.p2align 4,,10
	.p2align 3
.L2971:
	movq	-152(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L2154:
	testq	%r15, %r15
	je	.L2162
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2162:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2153:
	movq	152(%r13), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2163
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2164
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2173
	.p2align 4,,10
	.p2align 3
.L3016:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2167
	call	_ZdlPv@PLT
.L2167:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2168
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2169
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2168:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2170
	call	_ZdlPv@PLT
.L2170:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2171
	call	_ZdlPv@PLT
.L2171:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2172
	call	_ZdlPv@PLT
.L2172:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2165:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3015
.L2173:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2165
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3016
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2165
	.p2align 4,,10
	.p2align 3
.L2973:
	movq	-144(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L1954:
	testq	%r15, %r15
	je	.L1962
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1962:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1953:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L1963
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1964
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L3018:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1967
	call	_ZdlPv@PLT
.L1967:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1968
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1969
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1968:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1970
	call	_ZdlPv@PLT
.L1970:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1971
	call	_ZdlPv@PLT
.L1971:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1972
	call	_ZdlPv@PLT
.L1972:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1965:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3017
.L1973:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1965
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3018
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1965
	.p2align 4,,10
	.p2align 3
.L2975:
	movq	-144(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L2020:
	testq	%r15, %r15
	je	.L2028
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2028:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2019:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L2029
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2030
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L2039
	.p2align 4,,10
	.p2align 3
.L3020:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2033
	call	_ZdlPv@PLT
.L2033:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2034
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2035
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2034:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2036
	call	_ZdlPv@PLT
.L2036:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2037
	call	_ZdlPv@PLT
.L2037:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2038
	call	_ZdlPv@PLT
.L2038:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2031:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3019
.L2039:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2031
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3020
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2135:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2134
	.p2align 4,,10
	.p2align 3
.L1875:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1874
	.p2align 4,,10
	.p2align 3
.L1867:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1866
	.p2align 4,,10
	.p2align 3
.L1871:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L1870
	.p2align 4,,10
	.p2align 3
.L2093:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L1941:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1940
	.p2align 4,,10
	.p2align 3
.L2277:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2276
	.p2align 4,,10
	.p2align 3
.L2142:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2141
	.p2align 4,,10
	.p2align 3
.L2004:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2003
	.p2align 4,,10
	.p2align 3
.L2138:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L2137
	.p2align 4,,10
	.p2align 3
.L1877:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1876
	.p2align 4,,10
	.p2align 3
.L2235:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2234
	.p2align 4,,10
	.p2align 3
.L2144:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L2143
	.p2align 4,,10
	.p2align 3
.L1883:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1882
	.p2align 4,,10
	.p2align 3
.L2010:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2009
	.p2align 4,,10
	.p2align 3
.L2280:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2279
	.p2align 4,,10
	.p2align 3
.L2062:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2061
	.p2align 4,,10
	.p2align 3
.L1944:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1943
	.p2align 4,,10
	.p2align 3
.L2196:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L2195
	.p2align 4,,10
	.p2align 3
.L2096:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2095
	.p2align 4,,10
	.p2align 3
.L1910:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1909
	.p2align 4,,10
	.p2align 3
.L2347:
	call	*%rax
	jmp	.L2346
	.p2align 4,,10
	.p2align 3
.L2238:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2237
	.p2align 4,,10
	.p2align 3
.L2204:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2203
	.p2align 4,,10
	.p2align 3
.L2286:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2285
	.p2align 4,,10
	.p2align 3
.L2313:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2312
	.p2align 4,,10
	.p2align 3
.L2202:
	call	*%rax
	jmp	.L2201
	.p2align 4,,10
	.p2align 3
.L2016:
	call	*%rax
	jmp	.L2015
	.p2align 4,,10
	.p2align 3
.L1891:
	call	*%rdx
	jmp	.L1890
	.p2align 4,,10
	.p2align 3
.L1918:
	call	*%rdx
	jmp	.L1917
	.p2align 4,,10
	.p2align 3
.L1986:
	call	*%rax
	jmp	.L1985
	.p2align 4,,10
	.p2align 3
.L1900:
	call	*%rax
	jmp	.L1899
	.p2align 4,,10
	.p2align 3
.L2102:
	call	*%rdx
	jmp	.L2101
	.p2align 4,,10
	.p2align 3
.L2018:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2017
	.p2align 4,,10
	.p2align 3
.L2152:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2151
	.p2align 4,,10
	.p2align 3
.L1952:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L2186:
	call	*%rax
	jmp	.L2185
	.p2align 4,,10
	.p2align 3
.L1950:
	call	*%rax
	jmp	.L1949
	.p2align 4,,10
	.p2align 3
.L2150:
	call	*%rax
	jmp	.L2149
	.p2align 4,,10
	.p2align 3
.L2068:
	call	*%rdx
	jmp	.L2067
	.p2align 4,,10
	.p2align 3
.L1889:
	call	*%rdx
	jmp	.L1888
	.p2align 4,,10
	.p2align 3
.L2079:
	call	*%rax
	jmp	.L2078
	.p2align 4,,10
	.p2align 3
.L2113:
	call	*%rax
	jmp	.L2112
	.p2align 4,,10
	.p2align 3
.L2070:
	call	*%rdx
	jmp	.L2069
	.p2align 4,,10
	.p2align 3
.L2104:
	call	*%rdx
	jmp	.L2103
	.p2align 4,,10
	.p2align 3
.L1927:
	call	*%rax
	jmp	.L1926
	.p2align 4,,10
	.p2align 3
.L1916:
	call	*%rdx
	jmp	.L1915
	.p2align 4,,10
	.p2align 3
.L2052:
	call	*%rax
	jmp	.L2051
	.p2align 4,,10
	.p2align 3
.L2244:
	call	*%rdx
	jmp	.L2243
	.p2align 4,,10
	.p2align 3
.L2294:
	call	*%rdx
	jmp	.L2293
	.p2align 4,,10
	.p2align 3
.L2292:
	call	*%rdx
	jmp	.L2291
	.p2align 4,,10
	.p2align 3
.L2160:
	call	*%rdx
	jmp	.L2159
	.p2align 4,,10
	.p2align 3
.L2158:
	call	*%rdx
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L2035:
	call	*%rax
	jmp	.L2034
	.p2align 4,,10
	.p2align 3
.L2246:
	call	*%rdx
	jmp	.L2245
	.p2align 4,,10
	.p2align 3
.L2212:
	call	*%rdx
	jmp	.L2211
	.p2align 4,,10
	.p2align 3
.L2210:
	call	*%rdx
	jmp	.L2209
	.p2align 4,,10
	.p2align 3
.L2026:
	call	*%rdx
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L2024:
	call	*%rdx
	jmp	.L2023
	.p2align 4,,10
	.p2align 3
.L1960:
	call	*%rdx
	jmp	.L1959
	.p2align 4,,10
	.p2align 3
.L1958:
	call	*%rdx
	jmp	.L1957
	.p2align 4,,10
	.p2align 3
.L1969:
	call	*%rax
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L2169:
	call	*%rax
	jmp	.L2168
	.p2align 4,,10
	.p2align 3
.L2221:
	call	*%rax
	jmp	.L2220
	.p2align 4,,10
	.p2align 3
.L2255:
	call	*%rax
	jmp	.L2254
	.p2align 4,,10
	.p2align 3
.L2330:
	call	*%rax
	jmp	.L2329
	.p2align 4,,10
	.p2align 3
.L2303:
	call	*%rax
	jmp	.L2302
	.p2align 4,,10
	.p2align 3
.L2321:
	call	*%rdx
	jmp	.L2320
	.p2align 4,,10
	.p2align 3
.L2319:
	call	*%rdx
	jmp	.L2318
	.cfi_endproc
.LFE4459:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.section	.text._ZN12v8_inspector8protocol16InternalResponseD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16InternalResponseD2Ev
	.type	_ZN12v8_inspector8protocol16InternalResponseD2Ev, @function
_ZN12v8_inspector8protocol16InternalResponseD2Ev:
.LFB4410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3022
	movq	(%rdi), %rax
	call	*24(%rax)
.L3022:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L3021
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3021:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4410:
	.size	_ZN12v8_inspector8protocol16InternalResponseD2Ev, .-_ZN12v8_inspector8protocol16InternalResponseD2Ev
	.weak	_ZN12v8_inspector8protocol16InternalResponseD1Ev
	.set	_ZN12v8_inspector8protocol16InternalResponseD1Ev,_ZN12v8_inspector8protocol16InternalResponseD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev:
.LFB4457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	312(%rdi), %r12
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L3029
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3030
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3031
	call	_ZdlPv@PLT
.L3031:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3032
	call	_ZdlPv@PLT
.L3032:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3029:
	movq	304(%rbx), %rcx
	movq	%rcx, -64(%rbp)
	testq	%rcx, %rcx
	je	.L3033
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3034
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L3035
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -80(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rsi
	je	.L3036
	movq	%rbx, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L3168:
	movq	-56(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L3037
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3038
	movq	16(%rbx), %rsi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%rsi, -72(%rbp)
	testq	%rsi, %rsi
	je	.L3039
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3040
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L3041
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L3042
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3099:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3043
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3044
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3045
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3046
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3047
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	cmpq	%r14, %rdx
	je	.L3048
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -136(%rbp)
	jmp	.L3055
	.p2align 4,,10
	.p2align 3
.L3431:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3051
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3052
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3051:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L3053
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3054
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3053:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3049:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3430
.L3055:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L3049
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3431
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L3049
	.p2align 4,,10
	.p2align 3
.L3038:
	movq	%rbx, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L3037:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -80(%rbp)
	jne	.L3168
	movq	-96(%rbp), %rax
	movq	-120(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L3036:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L3169
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3169:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3035:
	movq	-64(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L3170
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	%rcx, -56(%rbp)
	cmpq	%r15, %rcx
	je	.L3171
	movq	%rbx, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L3205:
	movq	(%r15), %r12
	testq	%r12, %r12
	je	.L3172
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3173
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3174
	call	_ZdlPv@PLT
.L3174:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3175
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3176
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3177
	movq	8(%r14), %rax
	movq	(%r14), %rbx
	cmpq	%rbx, %rax
	je	.L3178
	movq	%r12, -80(%rbp)
	movq	%rbx, %r12
	movq	%rax, %rbx
	movq	%r13, -96(%rbp)
	movq	%r14, -104(%rbp)
	jmp	.L3185
	.p2align 4,,10
	.p2align 3
.L3433:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3181
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3182
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3181:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L3183
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3184
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3183:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3179:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L3432
.L3185:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L3179
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3433
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %rbx
	jne	.L3185
	.p2align 4,,10
	.p2align 3
.L3432:
	movq	-104(%rbp), %r14
	movq	-80(%rbp), %r12
	movq	-96(%rbp), %r13
	movq	(%r14), %rbx
.L3178:
	testq	%rbx, %rbx
	je	.L3186
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L3186:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3177:
	movq	152(%r13), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L3187
	movq	8(%rax), %rbx
	movq	(%rax), %r14
	cmpq	%r14, %rbx
	je	.L3188
	movq	%r12, -96(%rbp)
	movq	%r13, -104(%rbp)
	jmp	.L3197
	.p2align 4,,10
	.p2align 3
.L3435:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3191
	call	_ZdlPv@PLT
.L3191:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3192
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3193
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3192:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3194
	call	_ZdlPv@PLT
.L3194:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3195
	call	_ZdlPv@PLT
.L3195:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3196
	call	_ZdlPv@PLT
.L3196:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3189:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L3434
.L3197:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3189
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3435
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L3197
	.p2align 4,,10
	.p2align 3
.L3434:
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %r12
	movq	-104(%rbp), %r13
	movq	(%rax), %r14
.L3188:
	testq	%r14, %r14
	je	.L3198
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3198:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3187:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3199
	call	_ZdlPv@PLT
.L3199:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3200
	call	_ZdlPv@PLT
.L3200:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3201
	call	_ZdlPv@PLT
.L3201:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3175:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3202
	call	_ZdlPv@PLT
.L3202:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3203
	call	_ZdlPv@PLT
.L3203:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3204
	call	_ZdlPv@PLT
.L3204:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3172:
	addq	$8, %r15
	cmpq	%r15, -56(%rbp)
	jne	.L3205
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rbx
	movq	(%rax), %r15
.L3171:
	testq	%r15, %r15
	je	.L3206
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3206:
	movq	-72(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3170:
	movq	-64(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L3207
	call	_ZdlPv@PLT
.L3207:
	movq	-64(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L3208
	call	_ZdlPv@PLT
.L3208:
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L3209
	call	_ZdlPv@PLT
.L3209:
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3033:
	movq	264(%rbx), %rdi
	leaq	280(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3210
	call	_ZdlPv@PLT
.L3210:
	movq	216(%rbx), %rdi
	leaq	232(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3211
	call	_ZdlPv@PLT
.L3211:
	movq	168(%rbx), %rdi
	leaq	184(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3212
	call	_ZdlPv@PLT
.L3212:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3213
	movq	(%rdi), %rax
	call	*24(%rax)
.L3213:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3214
	call	_ZdlPv@PLT
.L3214:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3215
	call	_ZdlPv@PLT
.L3215:
	movq	16(%rbx), %r8
	leaq	32(%rbx), %rdi
	cmpq	%rdi, %r8
	je	.L3028
	addq	$104, %rsp
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3028:
	.cfi_restore_state
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3044:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L3043:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L3099
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3042:
	testq	%rdi, %rdi
	je	.L3100
	call	_ZdlPv@PLT
.L3100:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3041:
	movq	-72(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L3101
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L3102
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3136:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3103
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3104
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3105
	call	_ZdlPv@PLT
.L3105:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3106
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3107
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3108
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L3109
	movq	%rbx, -136(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -112(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3437:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3112
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3113
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3112:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L3114
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3115
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3114:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3110:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3436
.L3116:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3110
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3437
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L3110
	.p2align 4,,10
	.p2align 3
.L3444:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L3119:
	testq	%r14, %r14
	je	.L3129
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3129:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3118:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3130
	call	_ZdlPv@PLT
.L3130:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3131
	call	_ZdlPv@PLT
.L3131:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3132
	call	_ZdlPv@PLT
.L3132:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3106:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3133
	call	_ZdlPv@PLT
.L3133:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3134
	call	_ZdlPv@PLT
.L3134:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3135
	call	_ZdlPv@PLT
.L3135:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3103:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L3136
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3102:
	testq	%rdi, %rdi
	je	.L3137
	call	_ZdlPv@PLT
.L3137:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3101:
	movq	-72(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L3138
	call	_ZdlPv@PLT
.L3138:
	movq	-72(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L3139
	call	_ZdlPv@PLT
.L3139:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L3140
	call	_ZdlPv@PLT
.L3140:
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3039:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L3141
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3142
	movq	160(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3143
	movq	8(%r13), %rax
	movq	0(%r13), %r15
	cmpq	%r15, %rax
	je	.L3144
	movq	%rbx, -72(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -88(%rbp)
	jmp	.L3151
	.p2align 4,,10
	.p2align 3
.L3439:
	movq	16(%r14), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L3147
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3148
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3147:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L3149
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3150
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3149:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3145:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3438
.L3151:
	movq	(%rbx), %r14
	testq	%r14, %r14
	je	.L3145
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3439
	addq	$8, %rbx
	movq	%r14, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L3151
	.p2align 4,,10
	.p2align 3
.L3438:
	movq	-72(%rbp), %rbx
	movq	-88(%rbp), %r12
	movq	0(%r13), %r15
.L3144:
	testq	%r15, %r15
	je	.L3152
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3152:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3143:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L3153
	movq	8(%r14), %r15
	movq	(%r14), %r13
	cmpq	%r13, %r15
	je	.L3154
	movq	%r12, -72(%rbp)
	movq	%r14, -88(%rbp)
	jmp	.L3163
	.p2align 4,,10
	.p2align 3
.L3441:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3157
	call	_ZdlPv@PLT
.L3157:
	movq	136(%r12), %r14
	testq	%r14, %r14
	je	.L3158
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r14, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3159
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3158:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3160
	call	_ZdlPv@PLT
.L3160:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3161
	call	_ZdlPv@PLT
.L3161:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3162
	call	_ZdlPv@PLT
.L3162:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3155:
	addq	$8, %r13
	cmpq	%r13, %r15
	je	.L3440
.L3163:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L3155
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3441
	addq	$8, %r13
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r13, %r15
	jne	.L3163
	.p2align 4,,10
	.p2align 3
.L3440:
	movq	-88(%rbp), %r14
	movq	-72(%rbp), %r12
	movq	(%r14), %r13
.L3154:
	testq	%r13, %r13
	je	.L3164
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3164:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3153:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3165
	call	_ZdlPv@PLT
.L3165:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3166
	call	_ZdlPv@PLT
.L3166:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3167
	call	_ZdlPv@PLT
.L3167:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3141:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3037
	.p2align 4,,10
	.p2align 3
.L3446:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L3058:
	testq	%r14, %r14
	je	.L3068
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3068:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3057:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3069
	call	_ZdlPv@PLT
.L3069:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3070
	call	_ZdlPv@PLT
.L3070:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3071
	call	_ZdlPv@PLT
.L3071:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3045:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L3072
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3073
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3074
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L3075
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -136(%rbp)
	jmp	.L3082
	.p2align 4,,10
	.p2align 3
.L3443:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3078
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3079
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3078:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L3080
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3081
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3080:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3076:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3442
.L3082:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L3076
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3443
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L3076
	.p2align 4,,10
	.p2align 3
.L3448:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L3085:
	testq	%r14, %r14
	je	.L3095
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3095:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3084:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3096
	call	_ZdlPv@PLT
.L3096:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3097
	call	_ZdlPv@PLT
.L3097:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3098
	call	_ZdlPv@PLT
.L3098:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3072:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3043
	.p2align 4,,10
	.p2align 3
.L3436:
	movq	-112(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %r13
	movq	(%r14), %r15
.L3109:
	testq	%r15, %r15
	je	.L3117
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3117:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3108:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3118
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3119
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L3128
	.p2align 4,,10
	.p2align 3
.L3445:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3122
	call	_ZdlPv@PLT
.L3122:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3123
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3124
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3123:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3125
	call	_ZdlPv@PLT
.L3125:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3126
	call	_ZdlPv@PLT
.L3126:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3127
	call	_ZdlPv@PLT
.L3127:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3120:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3444
.L3128:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3120
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3445
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L3430:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L3048:
	testq	%r14, %r14
	je	.L3056
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3056:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3047:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3057
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3058
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L3067
	.p2align 4,,10
	.p2align 3
.L3447:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3061
	call	_ZdlPv@PLT
.L3061:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3062
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3063
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3062:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3064
	call	_ZdlPv@PLT
.L3064:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3065
	call	_ZdlPv@PLT
.L3065:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3066
	call	_ZdlPv@PLT
.L3066:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3059:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3446
.L3067:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3059
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3447
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3059
	.p2align 4,,10
	.p2align 3
.L3442:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L3075:
	testq	%r14, %r14
	je	.L3083
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3083:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3074:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3084
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3085
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L3094
	.p2align 4,,10
	.p2align 3
.L3449:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3088
	call	_ZdlPv@PLT
.L3088:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3089
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3090
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3089:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3091
	call	_ZdlPv@PLT
.L3091:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3092
	call	_ZdlPv@PLT
.L3092:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3093
	call	_ZdlPv@PLT
.L3093:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3086:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3448
.L3094:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3086
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3449
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3086
	.p2align 4,,10
	.p2align 3
.L3173:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3172
	.p2align 4,,10
	.p2align 3
.L3030:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3029
	.p2align 4,,10
	.p2align 3
.L3034:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L3033
	.p2align 4,,10
	.p2align 3
.L3104:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3103
	.p2align 4,,10
	.p2align 3
.L3040:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L3039
	.p2align 4,,10
	.p2align 3
.L3176:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3175
	.p2align 4,,10
	.p2align 3
.L3142:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3037
	.p2align 4,,10
	.p2align 3
.L3193:
	call	*%rax
	jmp	.L3192
	.p2align 4,,10
	.p2align 3
.L3159:
	call	*%rax
	jmp	.L3158
	.p2align 4,,10
	.p2align 3
.L3148:
	call	*%rdx
	jmp	.L3147
	.p2align 4,,10
	.p2align 3
.L3073:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3072
	.p2align 4,,10
	.p2align 3
.L3107:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3106
	.p2align 4,,10
	.p2align 3
.L3150:
	call	*%rdx
	jmp	.L3149
	.p2align 4,,10
	.p2align 3
.L3182:
	call	*%rdx
	jmp	.L3181
	.p2align 4,,10
	.p2align 3
.L3184:
	call	*%rdx
	jmp	.L3183
	.p2align 4,,10
	.p2align 3
.L3046:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3045
	.p2align 4,,10
	.p2align 3
.L3081:
	call	*%rdx
	jmp	.L3080
	.p2align 4,,10
	.p2align 3
.L3054:
	call	*%rdx
	jmp	.L3053
	.p2align 4,,10
	.p2align 3
.L3063:
	call	*%rax
	jmp	.L3062
	.p2align 4,,10
	.p2align 3
.L3124:
	call	*%rax
	jmp	.L3123
	.p2align 4,,10
	.p2align 3
.L3090:
	call	*%rax
	jmp	.L3089
	.p2align 4,,10
	.p2align 3
.L3052:
	call	*%rdx
	jmp	.L3051
	.p2align 4,,10
	.p2align 3
.L3113:
	call	*%rdx
	jmp	.L3112
	.p2align 4,,10
	.p2align 3
.L3115:
	call	*%rdx
	jmp	.L3114
	.p2align 4,,10
	.p2align 3
.L3079:
	call	*%rdx
	jmp	.L3078
	.cfi_endproc
.LFE4457:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev:
.LFB11517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rdx
	leaq	-64(%rdx), %rax
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	304(%rdi), %r12
	movups	%xmm0, -8(%rdi)
	testq	%r12, %r12
	je	.L3451
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rcx
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L3526
	movq	%r12, %rdi
	call	*%rdx
.L3451:
	movq	296(%r14), %r15
	testq	%r15, %r15
	je	.L3455
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r13
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	je	.L3527
	movq	%r15, %rdi
	call	*%rdx
.L3455:
	movq	256(%r14), %rdi
	leaq	272(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3482
	call	_ZdlPv@PLT
.L3482:
	movq	208(%r14), %rdi
	leaq	224(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3483
	call	_ZdlPv@PLT
.L3483:
	movq	160(%r14), %rdi
	leaq	176(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3484
	call	_ZdlPv@PLT
.L3484:
	movq	144(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3485
	movq	(%rdi), %rdx
	call	*24(%rdx)
.L3485:
	movq	104(%r14), %rdi
	leaq	120(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3486
	call	_ZdlPv@PLT
.L3486:
	movq	56(%r14), %rdi
	leaq	72(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3487
	call	_ZdlPv@PLT
.L3487:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3450
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3450:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3527:
	.cfi_restore_state
	movq	160(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3457
	movq	8(%r12), %rax
	movq	%rax, -64(%rbp)
	movq	(%r12), %rax
	movq	%rax, -56(%rbp)
.L3465:
	movq	-56(%rbp), %rax
	cmpq	%rax, -64(%rbp)
	je	.L3458
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L3459
	movq	(%rbx), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3460
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L3461
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L3462
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3461:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3463
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L3464
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3463:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3459:
	addq	$8, -56(%rbp)
	jmp	.L3465
.L3460:
	movq	%rbx, %rdi
	call	*%rdx
	jmp	.L3459
.L3464:
	call	*%rdx
	jmp	.L3463
.L3458:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3466
	call	_ZdlPv@PLT
.L3466:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3457:
	movq	152(%r15), %r12
	testq	%r12, %r12
	je	.L3467
	movq	8(%r12), %rax
	movq	%rax, -64(%rbp)
	movq	(%r12), %rax
	movq	%rax, -56(%rbp)
.L3477:
	movq	-56(%rbp), %rax
	cmpq	%rax, -64(%rbp)
	je	.L3468
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L3469
	movq	(%rbx), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3470
	movq	152(%rbx), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	leaq	168(%rbx), %rdx
	movq	%rax, (%rbx)
	cmpq	%rdx, %rdi
	je	.L3471
	call	_ZdlPv@PLT
.L3471:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3472
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L3473
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3472:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L3474
	call	_ZdlPv@PLT
.L3474:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L3475
	call	_ZdlPv@PLT
.L3475:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L3476
	call	_ZdlPv@PLT
.L3476:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3469:
	addq	$8, -56(%rbp)
	jmp	.L3477
.L3470:
	movq	%rbx, %rdi
	call	*%rdx
	jmp	.L3469
.L3473:
	call	*%rdx
	jmp	.L3472
.L3468:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3478
	call	_ZdlPv@PLT
.L3478:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3467:
	movq	104(%r15), %rdi
	leaq	120(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3479
	call	_ZdlPv@PLT
.L3479:
	movq	56(%r15), %rdi
	leaq	72(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3480
	call	_ZdlPv@PLT
.L3480:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3481
	call	_ZdlPv@PLT
.L3481:
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3455
.L3526:
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	leaq	72(%r12), %rdx
	movq	%rax, (%r12)
	cmpq	%rdx, %rdi
	je	.L3453
	call	_ZdlPv@PLT
.L3453:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L3454
	call	_ZdlPv@PLT
.L3454:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3451
.L3462:
	call	*%rdx
	jmp	.L3461
	.cfi_endproc
.LFE11517:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev:
.LFB11516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rdx
	leaq	-64(%rdx), %rax
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-8(%rdi), %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	304(%rdi), %r13
	movups	%xmm0, -8(%rdi)
	testq	%r13, %r13
	je	.L3529
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rcx
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L3604
	movq	%r13, %rdi
	call	*%rdx
.L3529:
	movq	296(%r15), %r14
	testq	%r14, %r14
	je	.L3533
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	je	.L3605
	movq	%r14, %rdi
	call	*%rdx
.L3533:
	movq	256(%r15), %rdi
	leaq	272(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3560
	call	_ZdlPv@PLT
.L3560:
	movq	208(%r15), %rdi
	leaq	224(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3561
	call	_ZdlPv@PLT
.L3561:
	movq	160(%r15), %rdi
	leaq	176(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3562
	call	_ZdlPv@PLT
.L3562:
	movq	144(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3563
	movq	(%rdi), %rdx
	call	*24(%rdx)
.L3563:
	movq	104(%r15), %rdi
	leaq	120(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3564
	call	_ZdlPv@PLT
.L3564:
	movq	56(%r15), %rdi
	leaq	72(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3565
	call	_ZdlPv@PLT
.L3565:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3566
	call	_ZdlPv@PLT
.L3566:
	addq	$40, %rsp
	movq	%r12, %rdi
	movl	$320, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
.L3604:
	.cfi_restore_state
	movq	56(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	leaq	72(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L3531
	call	_ZdlPv@PLT
.L3531:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L3532
	call	_ZdlPv@PLT
.L3532:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3529
.L3605:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r14)
	movq	160(%r14), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L3535
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, -64(%rbp)
.L3543:
	movq	-64(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	je	.L3536
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L3537
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3538
	movq	16(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L3539
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3540
	movq	%rdi, -80(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3539:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3541
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3542
	movq	%rdi, -80(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3541:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3537:
	addq	$8, -64(%rbp)
	jmp	.L3543
.L3536:
	movq	-56(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3544
	call	_ZdlPv@PLT
.L3544:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3535:
	movq	152(%r14), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L3545
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, -64(%rbp)
.L3555:
	movq	-64(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	je	.L3546
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L3547
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3548
	movq	152(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	leaq	168(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L3549
	call	_ZdlPv@PLT
.L3549:
	movq	136(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3550
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3551
	movq	%rdi, -80(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3550:
	movq	96(%r13), %rdi
	leaq	112(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L3552
	call	_ZdlPv@PLT
.L3552:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L3553
	call	_ZdlPv@PLT
.L3553:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L3554
	call	_ZdlPv@PLT
.L3554:
	movl	$192, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3547:
	addq	$8, -64(%rbp)
	jmp	.L3555
.L3546:
	movq	-56(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3556
	call	_ZdlPv@PLT
.L3556:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3545:
	movq	104(%r14), %rdi
	leaq	120(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3557
	call	_ZdlPv@PLT
.L3557:
	movq	56(%r14), %rdi
	leaq	72(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3558
	call	_ZdlPv@PLT
.L3558:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3559
	call	_ZdlPv@PLT
.L3559:
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3533
.L3540:
	call	*%rdx
	jmp	.L3539
.L3548:
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L3547
.L3551:
	call	*%rdx
	jmp	.L3550
.L3538:
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L3537
.L3542:
	call	*%rdx
	jmp	.L3541
	.cfi_endproc
.LFE11516:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"object expected"
.LC14:
	.string	"size"
.LC15:
	.string	"nodeId"
.LC16:
	.string	"integer value expected"
.LC17:
	.string	"ordinal"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB5735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L3607
	cmpl	$6, 8(%rsi)
	movq	%rsi, %rbx
	je	.L3608
.L3607:
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, 0(%r13)
.L3606:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3640
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3608:
	.cfi_restore_state
	movl	$32, %edi
	leaq	-96(%rbp), %r15
	call	_Znwm@PLT
	cmpl	$6, 8(%rbx)
	movq	%r12, %rdi
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleE(%rip), %rax
	movq	%rax, (%r14)
	movl	$0, %eax
	cmovne	%rax, %rbx
	movq	$0x000000000, 8(%r14)
	movl	$0, 16(%r14)
	movq	$0x000000000, 24(%r14)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC14(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r9
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L3611
	movq	%r9, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r9
.L3611:
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	movq	%r9, -128(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %r9
	movq	$0x000000000, -104(%rbp)
	testq	%r9, %r9
	je	.L3614
	movq	(%r9), %rax
	leaq	-104(%rbp), %rsi
	movq	%r9, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L3613
.L3614:
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L3613:
	movsd	-104(%rbp), %xmm0
	leaq	.LC15(%rip), %rsi
	movq	%r15, %rdi
	movsd	%xmm0, 8(%r14)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r9
	cmpq	-120(%rbp), %rdi
	je	.L3615
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r9
.L3615:
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	movq	%r9, -128(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %r9
	movl	$0, -104(%rbp)
	testq	%r9, %r9
	je	.L3618
	movq	(%r9), %rax
	leaq	-104(%rbp), %rsi
	movq	%r9, %rdi
	call	*48(%rax)
	testb	%al, %al
	jne	.L3617
.L3618:
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L3617:
	movl	-104(%rbp), %eax
	movq	%r15, %rdi
	leaq	.LC17(%rip), %rsi
	movl	%eax, 16(%r14)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r15
	cmpq	-120(%rbp), %rdi
	je	.L3619
	call	_ZdlPv@PLT
.L3619:
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	$0x000000000, -104(%rbp)
	testq	%r15, %r15
	je	.L3622
	movq	(%r15), %rax
	leaq	-104(%rbp), %rsi
	movq	%r15, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L3621
.L3622:
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L3621:
	movsd	-104(%rbp), %xmm0
	movq	%r12, %rdi
	movsd	%xmm0, 24(%r14)
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L3641
	movq	%r14, 0(%r13)
	jmp	.L3606
	.p2align 4,,10
	.p2align 3
.L3641:
	movq	(%r14), %rax
	movq	$0, 0(%r13)
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3642
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3606
	.p2align 4,,10
	.p2align 3
.L3642:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L3606
.L3640:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5735:
	.size	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample7toValueEv
	.type	_ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample7toValueEv, @function
_ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample7toValueEv:
.LFB5736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	-120(%rbp), %r8
	movl	$24, %edi
	movq	%r8, 0(%r13)
	movsd	8(%rbx), %xmm0
	movq	%r8, -128(%rbp)
	movsd	%xmm0, -120(%rbp)
	call	_Znwm@PLT
	movsd	-120(%rbp), %xmm0
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	movl	$3, 8(%rax)
	movq	%r15, (%rax)
	movsd	%xmm0, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L3644
	call	_ZdlPv@PLT
.L3644:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3645
	movq	(%rdi), %rax
	call	*24(%rax)
.L3645:
	movq	0(%r13), %r8
	movl	16(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -136(%rbp)
	movl	%edx, -128(%rbp)
	call	_Znwm@PLT
	movl	-128(%rbp), %edx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	movl	%edx, 16(%rax)
	movq	%r15, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L3646
	call	_ZdlPv@PLT
.L3646:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3647
	movq	(%rdi), %rax
	call	*24(%rax)
.L3647:
	movq	0(%r13), %r8
	movsd	24(%rbx), %xmm0
	movl	$24, %edi
	movq	%r8, -136(%rbp)
	movsd	%xmm0, -128(%rbp)
	call	_Znwm@PLT
	movsd	-128(%rbp), %xmm0
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	movl	$3, 8(%rax)
	movq	%r15, (%rax)
	movsd	%xmm0, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L3648
	call	_ZdlPv@PLT
.L3648:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3643
	movq	(%rdi), %rax
	call	*24(%rax)
.L3643:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3661
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3661:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5736:
	.size	_ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample7toValueEv, .-_ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample7toValueEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample17serializeToBinaryEv:
.LFB5559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3662
	movq	(%rdi), %rax
	call	*24(%rax)
.L3662:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3669
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3669:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5559:
	.size	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample17serializeToBinaryEv, .-_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample15serializeToJSONEv, @function
_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample15serializeToJSONEv:
.LFB5558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3670
	movq	(%rdi), %rax
	call	*24(%rax)
.L3670:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3677
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3677:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5558:
	.size	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample15serializeToJSONEv, .-_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample5cloneEv
	.type	_ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample5cloneEv, @function
_ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample5cloneEv:
.LFB5737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3679
	movq	(%rdi), %rax
	call	*24(%rax)
.L3679:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3685
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3685:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5737:
	.size	_ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample5cloneEv, .-_ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample5cloneEv
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"chunk"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB5741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L3687
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r12
	je	.L3688
.L3687:
	leaq	.LC13(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, 0(%r13)
.L3686:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3725
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3688:
	.cfi_restore_state
	movl	$48, %edi
	leaq	-96(%rbp), %rbx
	call	_Znwm@PLT
	xorl	%edi, %edi
	cmpl	$6, 8(%r12)
	movq	%rax, %r15
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationE(%rip), %rax
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	movq	%rax, -104(%rbp)
	movq	%rax, 8(%r15)
	movl	$0, %eax
	cmovne	%rax, %r12
	movw	%di, 24(%r15)
	movq	%r14, %rdi
	movq	$0, 16(%r15)
	movq	$0, 40(%r15)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	leaq	-80(%rbp), %r12
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r10
	cmpq	%r12, %rdi
	je	.L3691
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %r10
.L3691:
	leaq	.LC18(%rip), %rsi
	movq	%r14, %rdi
	movq	%r10, -112(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-112(%rbp), %r10
	xorl	%esi, %esi
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movw	%si, -80(%rbp)
	movq	$0, -64(%rbp)
	testq	%r10, %r10
	je	.L3694
	movq	(%r10), %rax
	movq	%rbx, %rsi
	movq	%r10, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L3693
.L3694:
	leaq	.LC5(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L3693:
	movq	-96(%rbp), %rdx
	movq	8(%r15), %rdi
	movq	-88(%rbp), %rax
	cmpq	%r12, %rdx
	je	.L3726
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -104(%rbp)
	je	.L3727
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	24(%r15), %rsi
	movq	%rdx, 8(%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%r15)
	testq	%rdi, %rdi
	je	.L3700
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L3698:
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movw	%ax, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 40(%r15)
	cmpq	%r12, %rdi
	je	.L3701
	call	_ZdlPv@PLT
.L3701:
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L3728
	movq	%r15, 0(%r13)
	jmp	.L3686
	.p2align 4,,10
	.p2align 3
.L3728:
	movq	(%r15), %rax
	movq	$0, 0(%r13)
	leaq	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3729
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationE(%rip), %rax
	movq	8(%r15), %rdi
	movq	%rax, (%r15)
	cmpq	%rdi, -104(%rbp)
	je	.L3702
	call	_ZdlPv@PLT
.L3702:
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3686
	.p2align 4,,10
	.p2align 3
.L3726:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L3696
	cmpq	$1, %rax
	je	.L3730
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L3696
	movq	%r12, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	8(%r15), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L3696:
	xorl	%ecx, %ecx
	movq	%rax, 16(%r15)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L3698
	.p2align 4,,10
	.p2align 3
.L3727:
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, 8(%r15)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 16(%r15)
.L3700:
	movq	%r12, -96(%rbp)
	leaq	-80(%rbp), %r12
	movq	%r12, %rdi
	jmp	.L3698
	.p2align 4,,10
	.p2align 3
.L3729:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L3686
.L3730:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	8(%r15), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L3696
.L3725:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5741:
	.size	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNK12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification7toValueEv
	.type	_ZNK12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification7toValueEv, @function
_ZNK12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification7toValueEv:
.LFB5746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$96, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r13, (%r15)
	movl	$56, %edi
	call	_Znwm@PLT
	movq	8(%r14), %rsi
	movq	16(%r14), %rcx
	movq	%rax, %rbx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	(%rcx,%rcx), %r12
	movq	%rsi, %rax
	leaq	32(%rbx), %rdi
	addq	%r12, %rax
	movq	%rdi, 16(%rbx)
	je	.L3732
	testq	%rsi, %rsi
	je	.L3753
.L3732:
	movq	%r12, %r8
	sarq	%r8
	cmpq	$14, %r12
	ja	.L3754
.L3733:
	cmpq	$2, %r12
	je	.L3755
	testq	%r12, %r12
	je	.L3736
	movq	%r12, %rdx
	movq	%r8, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	memmove@PLT
	movq	16(%rbx), %rdi
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rcx
.L3736:
	xorl	%eax, %eax
	movq	%r8, 24(%rbx)
	leaq	-96(%rbp), %r12
	leaq	.LC18(%rip), %rsi
	movw	%ax, (%rdi,%rcx,2)
	movq	40(%r14), %rax
	movq	%r12, %rdi
	movq	%rbx, -104(%rbp)
	movq	%rax, 48(%rbx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	leaq	-104(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3737
	call	_ZdlPv@PLT
.L3737:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3731
	movq	(%rdi), %rax
	call	*24(%rax)
.L3731:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3756
	addq	$104, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3755:
	.cfi_restore_state
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	16(%rbx), %rdi
	jmp	.L3736
	.p2align 4,,10
	.p2align 3
.L3754:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r8
	ja	.L3757
	leaq	2(%r12), %rdi
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	call	_Znwm@PLT
	movq	-136(%rbp), %r8
	movq	-128(%rbp), %rcx
	movq	%rax, 16(%rbx)
	movq	-120(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r8, 32(%rbx)
	jmp	.L3733
.L3753:
	leaq	.LC10(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L3756:
	call	__stack_chk_fail@PLT
.L3757:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5746:
	.size	_ZNK12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification7toValueEv, .-_ZNK12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification7toValueEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification17serializeToBinaryEv:
.LFB5618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3758
	movq	(%rdi), %rax
	call	*24(%rax)
.L3758:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3765
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3765:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5618:
	.size	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification17serializeToBinaryEv, .-_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification15serializeToJSONEv, @function
_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification15serializeToJSONEv:
.LFB5617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3766
	movq	(%rdi), %rax
	call	*24(%rax)
.L3766:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3773
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3773:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5617:
	.size	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification15serializeToJSONEv, .-_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification5cloneEv
	.type	_ZNK12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification5cloneEv, @function
_ZNK12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification5cloneEv:
.LFB5747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3775
	movq	(%rdi), %rax
	call	*24(%rax)
.L3775:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3781
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3781:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5747:
	.size	_ZNK12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification5cloneEv, .-_ZNK12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification5cloneEv
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"lastSeenObjectId"
.LC20:
	.string	"timestamp"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB5751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L3783
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r12
	je	.L3784
.L3783:
	leaq	.LC13(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, 0(%r13)
.L3782:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3809
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3784:
	.cfi_restore_state
	movl	$24, %edi
	leaq	-96(%rbp), %r15
	call	_Znwm@PLT
	cmpl	$6, 8(%r12)
	movq	%rbx, %rdi
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationE(%rip), %rax
	movq	%rax, (%r14)
	movl	$0, %eax
	cmovne	%rax, %r12
	movl	$0, 8(%r14)
	movq	$0x000000000, 16(%r14)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC19(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r9
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L3787
	movq	%r9, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r9
.L3787:
	leaq	.LC19(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r9, -128(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %r9
	movl	$0, -104(%rbp)
	testq	%r9, %r9
	je	.L3790
	movq	(%r9), %rax
	leaq	-104(%rbp), %rsi
	movq	%r9, %rdi
	call	*48(%rax)
	testb	%al, %al
	jne	.L3789
.L3790:
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L3789:
	movl	-104(%rbp), %eax
	leaq	.LC20(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, 8(%r14)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	cmpq	-120(%rbp), %rdi
	je	.L3791
	call	_ZdlPv@PLT
.L3791:
	leaq	.LC20(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	$0x000000000, -104(%rbp)
	testq	%r12, %r12
	je	.L3794
	movq	(%r12), %rax
	leaq	-104(%rbp), %rsi
	movq	%r12, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L3793
.L3794:
	leaq	.LC8(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L3793:
	movsd	-104(%rbp), %xmm0
	movq	%rbx, %rdi
	movsd	%xmm0, 16(%r14)
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L3810
	movq	%r14, 0(%r13)
	jmp	.L3782
	.p2align 4,,10
	.p2align 3
.L3810:
	movq	(%r14), %rax
	movq	$0, 0(%r13)
	leaq	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3811
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3782
	.p2align 4,,10
	.p2align 3
.L3811:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L3782
.L3809:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5751:
	.size	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNK12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification7toValueEv
	.type	_ZNK12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification7toValueEv, @function
_ZNK12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification7toValueEv:
.LFB5752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$96, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r15, (%r12)
	movl	$24, %edi
	movl	8(%rbx), %r13d
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	leaq	.LC19(%rip), %rsi
	movl	%r13d, 16(%rax)
	leaq	-96(%rbp), %r13
	movl	$2, 8(%rax)
	movq	%r13, %rdi
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, %r15
	cmpq	%rax, %rdi
	je	.L3813
	call	_ZdlPv@PLT
.L3813:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3814
	movq	(%rdi), %rax
	call	*24(%rax)
.L3814:
	movq	(%r12), %r8
	movsd	16(%rbx), %xmm0
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movsd	%xmm0, -120(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rdx
	movsd	-120(%rbp), %xmm0
	movq	%r13, %rdi
	movq	%rdx, (%rax)
	leaq	.LC20(%rip), %rsi
	movl	$3, 8(%rax)
	movsd	%xmm0, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3815
	call	_ZdlPv@PLT
.L3815:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3812
	movq	(%rdi), %rax
	call	*24(%rax)
.L3812:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3825
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3825:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5752:
	.size	_ZNK12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification7toValueEv, .-_ZNK12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification7toValueEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification17serializeToBinaryEv:
.LFB5673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3826
	movq	(%rdi), %rax
	call	*24(%rax)
.L3826:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3833
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3833:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5673:
	.size	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification17serializeToBinaryEv, .-_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification15serializeToJSONEv, @function
_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification15serializeToJSONEv:
.LFB5672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3834
	movq	(%rdi), %rax
	call	*24(%rax)
.L3834:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3841
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3841:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5672:
	.size	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification15serializeToJSONEv, .-_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification5cloneEv
	.type	_ZNK12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification5cloneEv, @function
_ZNK12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification5cloneEv:
.LFB5753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3843
	movq	(%rdi), %rax
	call	*24(%rax)
.L3843:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3849
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3849:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5753:
	.size	_ZNK12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification5cloneEv, .-_ZNK12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification5cloneEv
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC21:
	.string	"done"
.LC22:
	.string	"total"
.LC23:
	.string	"finished"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB5754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L3851
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r12
	je	.L3852
.L3851:
	leaq	.LC13(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, 0(%r13)
.L3850:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3886
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3852:
	.cfi_restore_state
	movl	$24, %edi
	leaq	-96(%rbp), %r15
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationE(%rip), %rax
	movq	%rax, (%r14)
	xorl	%eax, %eax
	cmpl	$6, 8(%r12)
	movw	%ax, 16(%r14)
	movl	$0, %eax
	cmovne	%rax, %r12
	movq	$0, 8(%r14)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC21(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r9
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L3855
	movq	%r9, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r9
.L3855:
	leaq	.LC21(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r9, -128(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %r9
	movl	$0, -100(%rbp)
	testq	%r9, %r9
	je	.L3858
	movq	(%r9), %rax
	leaq	-100(%rbp), %rsi
	movq	%r9, %rdi
	call	*48(%rax)
	testb	%al, %al
	jne	.L3857
.L3858:
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L3857:
	movl	-100(%rbp), %eax
	leaq	.LC22(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, 8(%r14)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r9
	cmpq	-120(%rbp), %rdi
	je	.L3859
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r9
.L3859:
	leaq	.LC22(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r9, -128(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %r9
	movl	$0, -100(%rbp)
	testq	%r9, %r9
	je	.L3862
	movq	(%r9), %rax
	leaq	-100(%rbp), %rsi
	movq	%r9, %rdi
	call	*48(%rax)
	testb	%al, %al
	jne	.L3861
.L3862:
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L3861:
	movl	-100(%rbp), %eax
	leaq	.LC23(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, 12(%r14)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	cmpq	-120(%rbp), %rdi
	je	.L3863
	call	_ZdlPv@PLT
.L3863:
	testq	%r12, %r12
	je	.L3864
	leaq	.LC23(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movb	$0, -100(%rbp)
	movq	(%r12), %rax
	movq	%r12, %rdi
	leaq	-100(%rbp), %rsi
	call	*32(%rax)
	testb	%al, %al
	je	.L3887
.L3865:
	movzbl	-100(%rbp), %eax
	movb	$1, 16(%r14)
	movb	%al, 17(%r14)
.L3864:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L3888
	movq	%r14, 0(%r13)
	jmp	.L3850
	.p2align 4,,10
	.p2align 3
.L3887:
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L3865
	.p2align 4,,10
	.p2align 3
.L3888:
	movq	(%r14), %rax
	movq	$0, 0(%r13)
	leaq	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3889
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3850
	.p2align 4,,10
	.p2align 3
.L3889:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L3850
.L3886:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5754:
	.size	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNK12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification7toValueEv
	.type	_ZNK12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification7toValueEv, @function
_ZNK12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification7toValueEv:
.LFB5755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$96, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r15, (%r12)
	movl	$24, %edi
	movl	8(%rbx), %r13d
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	leaq	.LC21(%rip), %rsi
	movl	%r13d, 16(%rax)
	leaq	-96(%rbp), %r13
	movl	$2, 8(%rax)
	movq	%r13, %rdi
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, %r15
	cmpq	%rax, %rdi
	je	.L3891
	call	_ZdlPv@PLT
.L3891:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3892
	movq	(%rdi), %rax
	call	*24(%rax)
.L3892:
	movq	(%r12), %r8
	movl	12(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	movl	%edx, -120(%rbp)
	call	_Znwm@PLT
	movl	-120(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r13, %rdi
	movl	$2, 8(%rax)
	leaq	.LC22(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3893
	call	_ZdlPv@PLT
.L3893:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3894
	movq	(%rdi), %rax
	call	*24(%rax)
.L3894:
	cmpb	$0, 16(%rbx)
	jne	.L3909
.L3890:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3910
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3909:
	.cfi_restore_state
	movq	(%r12), %r8
	movzbl	17(%rbx), %ebx
	movl	$24, %edi
	movq	%r8, -120(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	movl	$1, 8(%rax)
	movq	%rcx, (%rax)
	movb	%bl, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3896
	call	_ZdlPv@PLT
.L3896:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3890
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L3890
.L3910:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5755:
	.size	_ZNK12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification7toValueEv, .-_ZNK12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification7toValueEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification17serializeToBinaryEv:
.LFB5704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3911
	movq	(%rdi), %rax
	call	*24(%rax)
.L3911:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3918
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3918:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5704:
	.size	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification17serializeToBinaryEv, .-_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification15serializeToJSONEv, @function
_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification15serializeToJSONEv:
.LFB5703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3919
	movq	(%rdi), %rax
	call	*24(%rax)
.L3919:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3926
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3926:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5703:
	.size	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification15serializeToJSONEv, .-_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification5cloneEv
	.type	_ZNK12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification5cloneEv, @function
_ZNK12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification5cloneEv:
.LFB5756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3928
	movq	(%rdi), %rax
	call	*24(%rax)
.L3928:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3934
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3934:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5756:
	.size	_ZNK12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification5cloneEv, .-_ZNK12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification5cloneEv
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler8Frontend15heapStatsUpdateESt10unique_ptrISt6vectorIiSaIiEESt14default_deleteIS6_EE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"HeapProfiler.heapStatsUpdate"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler8Frontend15heapStatsUpdateESt10unique_ptrISt6vectorIiSaIiEESt14default_deleteIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler8Frontend15heapStatsUpdateESt10unique_ptrISt6vectorIiSaIiEESt14default_deleteIS6_EE
	.type	_ZN12v8_inspector8protocol12HeapProfiler8Frontend15heapStatsUpdateESt10unique_ptrISt6vectorIiSaIiEESt14default_deleteIS6_EE, @function
_ZN12v8_inspector8protocol12HeapProfiler8Frontend15heapStatsUpdateESt10unique_ptrISt6vectorIiSaIiEESt14default_deleteIS6_EE:
.LFB5761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L3935
	movq	%rsi, %r12
	movq	%rdi, %rbx
	leaq	-80(%rbp), %r13
	movl	$16, %edi
	call	_Znwm@PLT
	movq	(%r12), %rdx
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationE(%rip), %rcx
	movq	%r13, %rdi
	movq	$0, (%r12)
	movq	(%rbx), %r12
	leaq	.LC24(%rip), %rsi
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	movq	(%r12), %rdx
	movq	%rax, -104(%rbp)
	movq	24(%rdx), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rax
	movq	%r12, %rdi
	leaq	-88(%rbp), %rsi
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	*%rbx
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3937
	movq	(%rdi), %rax
	call	*24(%rax)
.L3937:
	movq	-96(%rbp), %r12
	testq	%r12, %r12
	je	.L3938
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3939
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L3940
	movq	(%rdi), %rax
	call	*24(%rax)
.L3940:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3941
	call	_ZdlPv@PLT
.L3941:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3938:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3942
	call	_ZdlPv@PLT
.L3942:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3935
	movq	(%rdi), %rax
	call	*24(%rax)
.L3935:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3958
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3939:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3938
.L3958:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5761:
	.size	_ZN12v8_inspector8protocol12HeapProfiler8Frontend15heapStatsUpdateESt10unique_ptrISt6vectorIiSaIiEESt14default_deleteIS6_EE, .-_ZN12v8_inspector8protocol12HeapProfiler8Frontend15heapStatsUpdateESt10unique_ptrISt6vectorIiSaIiEESt14default_deleteIS6_EE
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler8Frontend16lastSeenObjectIdEid.str1.1,"aMS",@progbits,1
.LC25:
	.string	"HeapProfiler.lastSeenObjectId"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler8Frontend16lastSeenObjectIdEid,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler8Frontend16lastSeenObjectIdEid
	.type	_ZN12v8_inspector8protocol12HeapProfiler8Frontend16lastSeenObjectIdEid, @function
_ZN12v8_inspector8protocol12HeapProfiler8Frontend16lastSeenObjectIdEid:
.LFB5763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movsd	%xmm0, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L3959
	movq	%rdi, %rbx
	movl	$24, %edi
	leaq	-80(%rbp), %r13
	movl	%esi, %r12d
	call	_Znwm@PLT
	movsd	-120(%rbp), %xmm0
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationE(%rip), %rcx
	movq	%r13, %rdi
	movl	%r12d, 8(%rax)
	movq	(%rbx), %r12
	leaq	.LC25(%rip), %rsi
	movq	%rcx, (%rax)
	movsd	%xmm0, 16(%rax)
	movq	(%r12), %rdx
	movq	%rax, -104(%rbp)
	movq	24(%rdx), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rax
	movq	%r12, %rdi
	leaq	-88(%rbp), %rsi
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	*%rbx
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3961
	movq	(%rdi), %rax
	call	*24(%rax)
.L3961:
	movq	-96(%rbp), %r12
	testq	%r12, %r12
	je	.L3962
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3963
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L3964
	movq	(%rdi), %rax
	call	*24(%rax)
.L3964:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3965
	call	_ZdlPv@PLT
.L3965:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3962:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3966
	call	_ZdlPv@PLT
.L3966:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3959
	movq	(%rdi), %rax
	call	*24(%rax)
.L3959:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3982
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3963:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3962
.L3982:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5763:
	.size	_ZN12v8_inspector8protocol12HeapProfiler8Frontend16lastSeenObjectIdEid, .-_ZN12v8_inspector8protocol12HeapProfiler8Frontend16lastSeenObjectIdEid
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler8Frontend26reportHeapSnapshotProgressEiiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"HeapProfiler.reportHeapSnapshotProgress"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler8Frontend26reportHeapSnapshotProgressEiiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler8Frontend26reportHeapSnapshotProgressEiiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE
	.type	_ZN12v8_inspector8protocol12HeapProfiler8Frontend26reportHeapSnapshotProgressEiiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE, @function
_ZN12v8_inspector8protocol12HeapProfiler8Frontend26reportHeapSnapshotProgressEiiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE:
.LFB5765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L3983
	movq	%rcx, %r12
	movq	%rdi, %rbx
	movl	$24, %edi
	movl	%edx, %r13d
	movl	%esi, %r14d
	call	_Znwm@PLT
	xorl	%edx, %edx
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationE(%rip), %rcx
	cmpb	$0, (%r12)
	movq	%rcx, (%rax)
	movw	%dx, 16(%rax)
	movl	%r14d, 8(%rax)
	movl	%r13d, 12(%rax)
	je	.L3985
	movzbl	1(%r12), %edx
	movb	$1, 16(%rax)
	movb	%dl, 17(%rax)
.L3985:
	movq	(%rbx), %r12
	leaq	-80(%rbp), %r13
	leaq	.LC26(%rip), %rsi
	movq	%r13, %rdi
	movq	(%r12), %rdx
	movq	%rax, -104(%rbp)
	movq	24(%rdx), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rax
	movq	%r12, %rdi
	leaq	-88(%rbp), %rsi
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	*%rbx
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3986
	movq	(%rdi), %rax
	call	*24(%rax)
.L3986:
	movq	-96(%rbp), %r12
	testq	%r12, %r12
	je	.L3987
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3988
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L3989
	movq	(%rdi), %rax
	call	*24(%rax)
.L3989:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3990
	call	_ZdlPv@PLT
.L3990:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3987:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3991
	call	_ZdlPv@PLT
.L3991:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3983
	movq	(%rdi), %rax
	call	*24(%rax)
.L3983:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4007
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3988:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3987
.L4007:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5765:
	.size	_ZN12v8_inspector8protocol12HeapProfiler8Frontend26reportHeapSnapshotProgressEiiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE, .-_ZN12v8_inspector8protocol12HeapProfiler8Frontend26reportHeapSnapshotProgressEiiN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIbEE
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler8Frontend13resetProfilesEv.str1.1,"aMS",@progbits,1
.LC27:
	.string	"HeapProfiler.resetProfiles"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler8Frontend13resetProfilesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler8Frontend13resetProfilesEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler8Frontend13resetProfilesEv, @function
_ZN12v8_inspector8protocol12HeapProfiler8Frontend13resetProfilesEv:
.LFB5768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L4008
	movq	(%r12), %rax
	leaq	-80(%rbp), %r13
	leaq	.LC27(%rip), %rsi
	movq	$0, -104(%rbp)
	movq	%r13, %rdi
	movq	24(%rax), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rax
	movq	%r12, %rdi
	leaq	-88(%rbp), %rsi
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	*%rbx
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4010
	movq	(%rdi), %rax
	call	*24(%rax)
.L4010:
	movq	-96(%rbp), %r12
	testq	%r12, %r12
	je	.L4011
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4012
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L4013
	movq	(%rdi), %rax
	call	*24(%rax)
.L4013:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4014
	call	_ZdlPv@PLT
.L4014:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4011:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4015
	call	_ZdlPv@PLT
.L4015:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4008
	movq	(%rdi), %rax
	call	*24(%rax)
.L4008:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4034
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4012:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4011
.L4034:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5768:
	.size	_ZN12v8_inspector8protocol12HeapProfiler8Frontend13resetProfilesEv, .-_ZN12v8_inspector8protocol12HeapProfiler8Frontend13resetProfilesEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler8Frontend5flushEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler8Frontend5flushEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler8Frontend5flushEv, @function
_ZN12v8_inspector8protocol12HeapProfiler8Frontend5flushEv:
.LFB5769:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE5769:
	.size	_ZN12v8_inspector8protocol12HeapProfiler8Frontend5flushEv, .-_ZN12v8_inspector8protocol12HeapProfiler8Frontend5flushEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler8Frontend23sendRawJSONNotificationENS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler8Frontend23sendRawJSONNotificationENS_8String16E
	.type	_ZN12v8_inspector8protocol12HeapProfiler8Frontend23sendRawJSONNotificationENS_8String16E, @function
_ZN12v8_inspector8protocol12HeapProfiler8Frontend23sendRawJSONNotificationENS_8String16E:
.LFB5770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rsi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	movq	24(%rax), %r14
	movq	(%rsi), %rax
	cmpq	%rdx, %rax
	je	.L4050
	movq	%rdx, (%rsi)
	xorl	%edx, %edx
	movq	16(%rsi), %rcx
	leaq	-64(%rbp), %r13
	movw	%dx, 16(%rsi)
	movq	32(%rsi), %rdx
	leaq	-112(%rbp), %rbx
	movq	8(%rsi), %rdi
	movq	%rcx, -112(%rbp)
	movq	$0, 8(%rsi)
	movq	%rdx, -96(%rbp)
	movq	%r13, -80(%rbp)
	cmpq	%rbx, %rax
	je	.L4038
	movq	%rax, -80(%rbp)
	movq	%rcx, -64(%rbp)
.L4040:
	movq	%rdi, -72(%rbp)
	xorl	%eax, %eax
	movl	$72, %edi
	movq	%rdx, -48(%rbp)
	movq	%rbx, -128(%rbp)
	movq	$0, -120(%rbp)
	movw	%ax, -112(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rcx
	leaq	24(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	movq	-80(%rbp), %rdx
	cmpq	%r13, %rdx
	je	.L4051
	movq	%rdx, 8(%rax)
	movq	-64(%rbp), %rdx
	movq	%rdx, 24(%rax)
.L4042:
	movq	-72(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, 64(%rax)
	movq	%rax, -136(%rbp)
	leaq	-136(%rbp), %rsi
	movq	%rdx, 16(%rax)
	movq	-48(%rbp), %rdx
	movups	%xmm0, 48(%rax)
	movq	%rdx, 40(%rax)
	call	*%r14
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4043
	movq	(%rdi), %rax
	call	*24(%rax)
.L4043:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4036
	call	_ZdlPv@PLT
.L4036:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4052
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4050:
	.cfi_restore_state
	movq	32(%rsi), %rdx
	xorl	%ecx, %ecx
	leaq	-64(%rbp), %r13
	movdqu	16(%rsi), %xmm1
	movq	8(%rsi), %rdi
	movw	%cx, 16(%rsi)
	leaq	-112(%rbp), %rbx
	movq	$0, 8(%rsi)
	movq	%rdx, -96(%rbp)
	movq	%r13, -80(%rbp)
	movaps	%xmm1, -112(%rbp)
.L4038:
	movdqa	-112(%rbp), %xmm2
	movaps	%xmm2, -64(%rbp)
	jmp	.L4040
	.p2align 4,,10
	.p2align 3
.L4051:
	movdqa	-64(%rbp), %xmm3
	movups	%xmm3, 24(%rax)
	jmp	.L4042
.L4052:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5770:
	.size	_ZN12v8_inspector8protocol12HeapProfiler8Frontend23sendRawJSONNotificationENS_8String16E, .-_ZN12v8_inspector8protocol12HeapProfiler8Frontend23sendRawJSONNotificationENS_8String16E
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE
	.type	_ZN12v8_inspector8protocol12HeapProfiler8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE, @function
_ZN12v8_inspector8protocol12HeapProfiler8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE:
.LFB5771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	(%rsi), %rbx
	movl	$72, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %r15
	movq	8(%rsi), %r12
	movq	0(%r13), %rax
	movq	24(%rax), %r14
	movq	$0, 16(%rsi)
	movups	%xmm0, (%rsi)
	call	_Znwm@PLT
	movq	%r12, %xmm1
	movq	%rbx, %xmm0
	movq	%r13, %rdi
	leaq	24(%rax), %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%r15, 64(%rax)
	leaq	-64(%rbp), %rsi
	movq	%rdx, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rcx
	xorl	%edx, %edx
	movq	%rcx, (%rax)
	movq	$0, 16(%rax)
	movw	%dx, 24(%rax)
	movq	$0, 40(%rax)
	movq	%rax, -64(%rbp)
	movups	%xmm0, 48(%rax)
	call	*%r14
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4053
	movq	(%rdi), %rax
	call	*24(%rax)
.L4053:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4060
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4060:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5771:
	.size	_ZN12v8_inspector8protocol12HeapProfiler8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE, .-_ZN12v8_inspector8protocol12HeapProfiler8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm.str1.1,"aMS",@progbits,1
.LC28:
	.string	"vector::reserve"
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm:
.LFB6413:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	ja	.L4077
	movq	(%rdi), %r12
	movq	16(%rdi), %rax
	movq	%rdi, %r15
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L4078
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4078:
	.cfi_restore_state
	movq	8(%rdi), %rbx
	xorl	%r13d, %r13d
	movq	%rbx, %rax
	subq	%r12, %rax
	movq	%rax, -56(%rbp)
	leaq	0(,%rsi,8), %rax
	movq	%rax, -64(%rbp)
	testq	%rsi, %rsi
	je	.L4064
	movq	%rax, %rdi
	call	_Znwm@PLT
	movq	8(%r15), %rbx
	movq	(%r15), %r12
	movq	%rax, %r13
.L4064:
	cmpq	%r12, %rbx
	je	.L4065
	movq	%r13, %r14
	.p2align 4,,10
	.p2align 3
.L4069:
	movq	(%r12), %rcx
	movq	$0, (%r12)
	movq	%rcx, (%r14)
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4066
	movq	(%rdi), %rcx
	addq	$8, %r12
	addq	$8, %r14
	call	*24(%rcx)
	cmpq	%rbx, %r12
	jne	.L4069
.L4067:
	movq	(%r15), %r12
.L4065:
	testq	%r12, %r12
	je	.L4070
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4070:
	movq	-56(%rbp), %r14
	movq	%r13, (%r15)
	addq	%r13, %r14
	addq	-64(%rbp), %r13
	movq	%r14, 8(%r15)
	movq	%r13, 16(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4066:
	.cfi_restore_state
	addq	$8, %r12
	addq	$8, %r14
	cmpq	%r12, %rbx
	jne	.L4069
	jmp	.L4067
.L4077:
	leaq	.LC28(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6413:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	.section	.rodata._ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode7toValueEv.str1.1,"aMS",@progbits,1
.LC29:
	.string	"callFrame"
.LC30:
	.string	"selfSize"
.LC31:
	.string	"id"
.LC32:
	.string	"children"
	.section	.text._ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode7toValueEv
	.type	_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode7toValueEv, @function
_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode7toValueEv:
.LFB5733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	-120(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, 0(%r13)
	movq	8(%rbx), %rsi
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-104(%rbp), %rax
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L4080
	call	_ZdlPv@PLT
.L4080:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4081
	movq	(%rdi), %rax
	call	*24(%rax)
.L4081:
	movq	0(%r13), %r8
	movsd	16(%rbx), %xmm0
	movl	$24, %edi
	movq	%r8, -136(%rbp)
	movsd	%xmm0, -128(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movsd	-128(%rbp), %xmm0
	movq	%r12, %rdi
	movl	$3, 8(%rax)
	leaq	.LC30(%rip), %rsi
	movq	%rcx, (%rax)
	movsd	%xmm0, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L4082
	call	_ZdlPv@PLT
.L4082:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4083
	movq	(%rdi), %rax
	call	*24(%rax)
.L4083:
	movq	0(%r13), %r8
	movl	24(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -136(%rbp)
	movl	%edx, -128(%rbp)
	call	_Znwm@PLT
	movl	-128(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	leaq	.LC31(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L4084
	call	_ZdlPv@PLT
.L4084:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4085
	movq	(%rdi), %rax
	call	*24(%rax)
.L4085:
	movq	32(%rbx), %rdx
	movq	0(%r13), %rax
	movl	$40, %edi
	movq	%rdx, -128(%rbp)
	movq	%rax, -136(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	-128(%rbp), %rdx
	leaq	16(%rbx), %rdi
	movq	8(%rdx), %rsi
	subq	(%rdx), %rsi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	-128(%rbp), %rdx
	movq	8(%rdx), %rcx
	movq	(%rdx), %rax
	movq	%rcx, -128(%rbp)
	cmpq	%rcx, %rax
	je	.L4086
	.p2align 4,,10
	.p2align 3
.L4089:
	movq	(%rax), %rsi
	movq	%r14, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode7toValueEv
	movq	-104(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rdx, -112(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-112(%rbp), %rdi
	movq	-144(%rbp), %rax
	testq	%rdi, %rdi
	je	.L4087
	movq	(%rdi), %rdx
	call	*24(%rdx)
	movq	-144(%rbp), %rax
	addq	$8, %rax
	cmpq	%rax, -128(%rbp)
	jne	.L4089
.L4086:
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	movq	%rbx, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L4090
	call	_ZdlPv@PLT
.L4090:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4079
	movq	(%rdi), %rax
	call	*24(%rax)
.L4079:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4106
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4087:
	.cfi_restore_state
	addq	$8, %rax
	cmpq	%rax, -128(%rbp)
	jne	.L4089
	jmp	.L4086
.L4106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5733:
	.size	_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode7toValueEv, .-_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode7toValueEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode17serializeToBinaryEv:
.LFB5527:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4107
	movq	(%rdi), %rax
	call	*24(%rax)
.L4107:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4114
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4114:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5527:
	.size	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode17serializeToBinaryEv, .-_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode15serializeToJSONEv, @function
_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode15serializeToJSONEv:
.LFB5526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4115
	movq	(%rdi), %rax
	call	*24(%rax)
.L4115:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4122
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4122:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5526:
	.size	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode15serializeToJSONEv, .-_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode15serializeToJSONEv
	.section	.rodata._ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile7toValueEv.str1.1,"aMS",@progbits,1
.LC33:
	.string	"head"
.LC34:
	.string	"samples"
	.section	.text._ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile7toValueEv
	.type	_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile7toValueEv, @function
_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile7toValueEv:
.LFB5739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$96, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	-120(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, (%r12)
	movq	8(%rbx), %rsi
	call	_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode7toValueEv
	movq	-104(%rbp), %rax
	leaq	.LC33(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %r8
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rax, %rdi
	je	.L4124
	call	_ZdlPv@PLT
.L4124:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4125
	movq	(%rdi), %rax
	call	*24(%rax)
.L4125:
	movq	16(%rbx), %rdx
	movq	(%r12), %rax
	movl	$40, %edi
	movq	%rdx, -120(%rbp)
	movq	%rax, -128(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	-120(%rbp), %rdx
	leaq	16(%rbx), %rdi
	movq	8(%rdx), %rsi
	subq	(%rdx), %rsi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	-120(%rbp), %rdx
	movq	8(%rdx), %rcx
	movq	(%rdx), %rax
	movq	%rcx, -120(%rbp)
	cmpq	%rcx, %rax
	je	.L4126
	.p2align 4,,10
	.p2align 3
.L4129:
	movq	(%rax), %rsi
	movq	%r15, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample7toValueEv
	movq	-112(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%rdx, -104(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-104(%rbp), %rdi
	movq	-144(%rbp), %rax
	testq	%rdi, %rdi
	je	.L4127
	movq	(%rdi), %rdx
	call	*24(%rdx)
	movq	-144(%rbp), %rax
	addq	$8, %rax
	cmpq	%rax, -120(%rbp)
	jne	.L4129
.L4126:
	leaq	.LC34(%rip), %rsi
	movq	%r13, %rdi
	movq	%rbx, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L4130
	call	_ZdlPv@PLT
.L4130:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4123
	movq	(%rdi), %rax
	call	*24(%rax)
.L4123:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4140
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4127:
	.cfi_restore_state
	addq	$8, %rax
	cmpq	%rax, -120(%rbp)
	jne	.L4129
	jmp	.L4126
.L4140:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5739:
	.size	_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile7toValueEv, .-_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile7toValueEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile17serializeToBinaryEv:
.LFB5592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4141
	movq	(%rdi), %rax
	call	*24(%rax)
.L4141:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4148
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4148:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5592:
	.size	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile17serializeToBinaryEv, .-_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile15serializeToJSONEv, @function
_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile15serializeToJSONEv:
.LFB5591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4149
	movq	(%rdi), %rax
	call	*24(%rax)
.L4149:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4156
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4156:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5591:
	.size	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile15serializeToJSONEv, .-_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile15serializeToJSONEv
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl18getSamplingProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC35:
	.string	"profile"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl18getSamplingProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl18getSamplingProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl18getSamplingProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl18getSamplingProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB5837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-184(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -192(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	leaq	-192(%rbp), %rdx
	movq	%r14, %rdi
	movq	(%rsi), %rax
	call	*64(%rax)
	cmpl	$2, -112(%rbp)
	je	.L4230
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L4231
.L4160:
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L4232
.L4187:
	leaq	-168(%rbp), %rcx
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, -168(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4159
	movq	(%rdi), %rax
	call	*24(%rax)
.L4159:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4164
	call	_ZdlPv@PLT
.L4164:
	movq	-184(%rbp), %r12
	testq	%r12, %r12
	je	.L4165
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4165:
	movq	-192(%rbp), %r13
	testq	%r13, %r13
	je	.L4157
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4167
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L4168
	movq	8(%r14), %rbx
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L4169
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %r15
	jmp	.L4172
	.p2align 4,,10
	.p2align 3
.L4234:
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L4170:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L4233
.L4172:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4170
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	je	.L4234
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L4172
	.p2align 4,,10
	.p2align 3
.L4233:
	movq	(%r14), %r12
.L4169:
	testq	%r12, %r12
	je	.L4173
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4173:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4168:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L4174
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L4175
	movq	32(%r14), %r15
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r15, %r15
	je	.L4176
	movq	8(%r15), %rax
	movq	(%r15), %r12
	movq	%rax, -208(%rbp)
	cmpq	%r12, %rax
	jne	.L4180
	jmp	.L4177
	.p2align 4,,10
	.p2align 3
.L4236:
	movq	%rdi, -200(%rbp)
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movq	-200(%rbp), %rdi
	movl	$40, %esi
	call	_ZdlPvm@PLT
.L4178:
	addq	$8, %r12
	cmpq	%r12, -208(%rbp)
	je	.L4235
.L4180:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4178
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	je	.L4236
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, -208(%rbp)
	jne	.L4180
	.p2align 4,,10
	.p2align 3
.L4235:
	movq	(%r15), %r12
.L4177:
	testq	%r12, %r12
	je	.L4181
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4181:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4176:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L4182
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4183
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4184
	call	_ZdlPv@PLT
.L4184:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4185
	call	_ZdlPv@PLT
.L4185:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4186
	call	_ZdlPv@PLT
.L4186:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4182:
	movl	$40, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4174:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4157:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4237
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4232:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L4159
	.p2align 4,,10
	.p2align 3
.L4231:
	movq	-192(%rbp), %rsi
	leaq	-168(%rbp), %rdi
	leaq	-160(%rbp), %r15
	call	_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile7toValueEv
	movq	-168(%rbp), %rax
	leaq	.LC35(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	leaq	-176(%rbp), %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4161
	call	_ZdlPv@PLT
.L4161:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4160
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L4187
	jmp	.L4232
	.p2align 4,,10
	.p2align 3
.L4230:
	movq	8(%rbx), %rdi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L4159
	.p2align 4,,10
	.p2align 3
.L4167:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4157
	.p2align 4,,10
	.p2align 3
.L4175:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L4174
	.p2align 4,,10
	.p2align 3
.L4183:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4182
.L4237:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5837:
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl18getSamplingProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl18getSamplingProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl12stopSamplingEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl12stopSamplingEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl12stopSamplingEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl12stopSamplingEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB5866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-184(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -192(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	leaq	-192(%rbp), %rdx
	movq	%r14, %rdi
	movq	(%rsi), %rax
	call	*88(%rax)
	cmpl	$2, -112(%rbp)
	je	.L4311
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L4312
.L4241:
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L4313
.L4268:
	leaq	-168(%rbp), %rcx
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, -168(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4240
	movq	(%rdi), %rax
	call	*24(%rax)
.L4240:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4245
	call	_ZdlPv@PLT
.L4245:
	movq	-184(%rbp), %r12
	testq	%r12, %r12
	je	.L4246
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4246:
	movq	-192(%rbp), %r13
	testq	%r13, %r13
	je	.L4238
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4248
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L4249
	movq	8(%r14), %rbx
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L4250
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %r15
	jmp	.L4253
	.p2align 4,,10
	.p2align 3
.L4315:
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L4251:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L4314
.L4253:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4251
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	je	.L4315
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L4253
	.p2align 4,,10
	.p2align 3
.L4314:
	movq	(%r14), %r12
.L4250:
	testq	%r12, %r12
	je	.L4254
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4254:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4249:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L4255
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L4256
	movq	32(%r14), %r15
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r15, %r15
	je	.L4257
	movq	8(%r15), %rax
	movq	(%r15), %r12
	movq	%rax, -208(%rbp)
	cmpq	%r12, %rax
	jne	.L4261
	jmp	.L4258
	.p2align 4,,10
	.p2align 3
.L4317:
	movq	%rdi, -200(%rbp)
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movq	-200(%rbp), %rdi
	movl	$40, %esi
	call	_ZdlPvm@PLT
.L4259:
	addq	$8, %r12
	cmpq	%r12, -208(%rbp)
	je	.L4316
.L4261:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4259
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	je	.L4317
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, -208(%rbp)
	jne	.L4261
	.p2align 4,,10
	.p2align 3
.L4316:
	movq	(%r15), %r12
.L4258:
	testq	%r12, %r12
	je	.L4262
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4262:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4257:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L4263
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4264
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4265
	call	_ZdlPv@PLT
.L4265:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4266
	call	_ZdlPv@PLT
.L4266:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4267
	call	_ZdlPv@PLT
.L4267:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4263:
	movl	$40, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4255:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4238:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4318
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4313:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L4240
	.p2align 4,,10
	.p2align 3
.L4312:
	movq	-192(%rbp), %rsi
	leaq	-168(%rbp), %rdi
	leaq	-160(%rbp), %r15
	call	_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile7toValueEv
	movq	-168(%rbp), %rax
	leaq	.LC35(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	leaq	-176(%rbp), %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4242
	call	_ZdlPv@PLT
.L4242:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4241
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L4268
	jmp	.L4313
	.p2align 4,,10
	.p2align 3
.L4311:
	movq	8(%rbx), %rdi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L4240
	.p2align 4,,10
	.p2align 3
.L4248:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4238
	.p2align 4,,10
	.p2align 3
.L4256:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L4255
	.p2align 4,,10
	.p2align 3
.L4264:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4263
.L4318:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5866:
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl12stopSamplingEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl12stopSamplingEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZNK12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification7toValueEv.str1.1,"aMS",@progbits,1
.LC36:
	.string	"statsUpdate"
	.section	.text._ZNK12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification7toValueEv
	.type	_ZNK12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification7toValueEv, @function
_ZNK12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification7toValueEv:
.LFB5749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r12, 0(%r13)
	movl	$40, %edi
	movq	8(%rbx), %r14
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	8(%r14), %rsi
	subq	(%r14), %rsi
	leaq	16(%rbx), %rdi
	sarq	$2, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	8(%r14), %rax
	movq	(%r14), %rdx
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdx
	je	.L4320
	movq	%rdx, %r14
	leaq	-104(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L4323:
	movl	(%r14), %ecx
	movl	$24, %edi
	movl	%ecx, -124(%rbp)
	call	_Znwm@PLT
	movl	-124(%rbp), %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rdx
	movl	$2, 8(%rax)
	movq	%rdx, (%rax)
	movl	%ecx, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4321
	movq	(%rdi), %rax
	addq	$4, %r14
	call	*24(%rax)
	cmpq	%r14, -120(%rbp)
	jne	.L4323
.L4320:
	leaq	-96(%rbp), %r14
	leaq	.LC36(%rip), %rsi
	movq	%rbx, -104(%rbp)
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4324
	call	_ZdlPv@PLT
.L4324:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4319
	movq	(%rdi), %rax
	call	*24(%rax)
.L4319:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4333
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4321:
	.cfi_restore_state
	addq	$4, %r14
	cmpq	%r14, -120(%rbp)
	jne	.L4323
	jmp	.L4320
.L4333:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5749:
	.size	_ZNK12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification7toValueEv, .-_ZNK12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification7toValueEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification17serializeToBinaryEv:
.LFB5646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4334
	movq	(%rdi), %rax
	call	*24(%rax)
.L4334:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4341
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4341:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5646:
	.size	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification17serializeToBinaryEv, .-_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification15serializeToJSONEv, @function
_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification15serializeToJSONEv:
.LFB5645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4342
	movq	(%rdi), %rax
	call	*24(%rax)
.L4342:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4349
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4349:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5645:
	.size	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification15serializeToJSONEv, .-_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification15serializeToJSONEv
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_:
.LFB8972:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L4369
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	8(%rsi), %r15
	cmpq	%r13, %rdx
	je	.L4360
	movq	16(%rdi), %rax
.L4352:
	cmpq	%r15, %rax
	jb	.L4372
	leaq	(%r15,%r15), %rdx
	testq	%r15, %r15
	je	.L4356
.L4375:
	movq	(%r12), %rsi
	cmpq	$1, %r15
	je	.L4373
	testq	%rdx, %rdx
	je	.L4356
	movq	%r13, %rdi
	call	memmove@PLT
	movq	(%rbx), %r13
.L4356:
	xorl	%eax, %eax
	movq	%r15, 8(%rbx)
	movw	%ax, 0(%r13,%r15,2)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4372:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r15
	ja	.L4374
	addq	%rax, %rax
	movq	%r15, %r14
	cmpq	%rax, %r15
	jnb	.L4355
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r14
.L4355:
	leaq	2(%r14,%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdi, %rdx
	je	.L4359
	call	_ZdlPv@PLT
.L4359:
	movq	%r13, (%rbx)
	leaq	(%r15,%r15), %rdx
	movq	%r14, 16(%rbx)
	testq	%r15, %r15
	je	.L4356
	jmp	.L4375
	.p2align 4,,10
	.p2align 3
.L4369:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L4360:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, %eax
	jmp	.L4352
	.p2align 4,,10
	.p2align 3
.L4373:
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	movq	(%rbx), %r13
	jmp	.L4356
.L4374:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8972:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler8Frontend20addHeapSnapshotChunkERKNS_8String16E.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"HeapProfiler.addHeapSnapshotChunk"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler8Frontend20addHeapSnapshotChunkERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler8Frontend20addHeapSnapshotChunkERKNS_8String16E
	.type	_ZN12v8_inspector8protocol12HeapProfiler8Frontend20addHeapSnapshotChunkERKNS_8String16E, @function
_ZN12v8_inspector8protocol12HeapProfiler8Frontend20addHeapSnapshotChunkERKNS_8String16E:
.LFB5757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L4376
	movq	%rdi, %r12
	movl	$48, %edi
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	leaq	8(%rbx), %rdi
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
	movw	%ax, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r13), %rax
	movq	(%r12), %r12
	leaq	-80(%rbp), %r13
	leaq	.LC37(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, 40(%rbx)
	movq	(%r12), %rax
	movq	%rbx, -104(%rbp)
	movq	24(%rax), %r14
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rax
	movq	%r12, %rdi
	leaq	-88(%rbp), %rsi
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	*%r14
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4378
	movq	(%rdi), %rax
	call	*24(%rax)
.L4378:
	movq	-96(%rbp), %r12
	testq	%r12, %r12
	je	.L4379
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4380
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L4381
	movq	(%rdi), %rax
	call	*24(%rax)
.L4381:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4382
	call	_ZdlPv@PLT
.L4382:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4379:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4383
	call	_ZdlPv@PLT
.L4383:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4376
	movq	(%rdi), %rax
	call	*24(%rax)
.L4376:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4399
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4380:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4379
.L4399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5757:
	.size	_ZN12v8_inspector8protocol12HeapProfiler8Frontend20addHeapSnapshotChunkERKNS_8String16E, .-_ZN12v8_inspector8protocol12HeapProfiler8Frontend20addHeapSnapshotChunkERKNS_8String16E
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl23getObjectByHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC38:
	.string	"objectGroup"
.LC39:
	.string	"result"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl23getObjectByHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl23getObjectByHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl23getObjectByHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl23getObjectByHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB5835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$296, %rsp
	movl	%esi, -300(%rbp)
	movq	(%r8), %r13
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -312(%rbp)
	movq	%rcx, -320(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L4401
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	jne	.L4402
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L4464
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L4405:
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r15, %rdi
	je	.L4407
	movq	%rax, -296(%rbp)
	call	_ZdlPv@PLT
	movq	-296(%rbp), %r8
.L4407:
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r8, -328(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-328(%rbp), %r8
	xorl	%edi, %edi
	leaq	-240(%rbp), %rax
	movq	%rax, -296(%rbp)
	leaq	-256(%rbp), %rsi
	movq	%rax, -256(%rbp)
	movq	$0, -248(%rbp)
	movw	%di, -240(%rbp)
	movq	$0, -224(%rbp)
	testq	%r8, %r8
	je	.L4408
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L4436
.L4408:
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L4436:
	leaq	.LC38(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r15, %rdi
	je	.L4409
	movq	%rax, -328(%rbp)
	call	_ZdlPv@PLT
	movq	-328(%rbp), %r8
.L4409:
	xorl	%esi, %esi
	testq	%r8, %r8
	leaq	-184(%rbp), %r13
	movb	$0, -208(%rbp)
	movq	%r13, -200(%rbp)
	movq	$0, -192(%rbp)
	movw	%si, -184(%rbp)
	movq	$0, -168(%rbp)
	movq	%r8, -328(%rbp)
	je	.L4406
	leaq	.LC38(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	xorl	%ecx, %ecx
	movq	%r15, -112(%rbp)
	movq	%r12, %rsi
	movq	-328(%rbp), %r8
	movq	$0, -104(%rbp)
	movw	%cx, -96(%rbp)
	movq	$0, -80(%rbp)
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L4410
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L4410:
	leaq	-200(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rdi
	movb	$1, -208(%rbp)
	movq	%rax, -168(%rbp)
	cmpq	%r15, %rdi
	je	.L4406
	call	_ZdlPv@PLT
.L4406:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L4465
	movq	%r14, %rsi
	leaq	-280(%rbp), %rdi
	movq	$0, -288(%rbp)
	leaq	-136(%rbp), %rbx
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r14), %rsi
	movq	(%rsi), %rax
	movq	56(%rax), %r9
	movzbl	-208(%rbp), %eax
	movq	%rbx, -152(%rbp)
	movb	%al, -160(%rbp)
	movq	-200(%rbp), %rax
	cmpq	%r13, %rax
	je	.L4466
	movq	%rax, -152(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -136(%rbp)
.L4416:
	movq	-192(%rbp), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %r15
	movq	%r12, %rdi
	movw	%dx, -184(%rbp)
	leaq	-288(%rbp), %r8
	leaq	-256(%rbp), %rdx
	movq	%r15, %rcx
	movq	%rax, -144(%rbp)
	movq	-168(%rbp), %rax
	movq	%r13, -200(%rbp)
	movq	$0, -192(%rbp)
	movq	%rax, -120(%rbp)
	call	*%r9
	movq	-152(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4417
	call	_ZdlPv@PLT
.L4417:
	cmpl	$2, -112(%rbp)
	je	.L4467
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L4468
.L4420:
	movq	-280(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L4469
	movl	-300(%rbp), %esi
	leaq	-264(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r14, -264(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4419
	movq	(%rdi), %rax
	call	*24(%rax)
.L4419:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4424
	call	_ZdlPv@PLT
.L4424:
	movq	-280(%rbp), %r12
	testq	%r12, %r12
	je	.L4425
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4425:
	movq	-288(%rbp), %r12
	testq	%r12, %r12
	je	.L4414
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4427
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4414:
	movq	-200(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4428
	call	_ZdlPv@PLT
.L4428:
	movq	-256(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L4400
	call	_ZdlPv@PLT
.L4400:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4470
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4464:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L4405
	.p2align 4,,10
	.p2align 3
.L4401:
	movq	-112(%rbp), %rdi
.L4402:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L4471
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L4404:
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	leaq	-184(%rbp), %r13
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-240(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, -296(%rbp)
	movq	%rax, -256(%rbp)
	movq	$0, -248(%rbp)
	movw	%r8w, -240(%rbp)
	movq	$0, -224(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	xorl	%r9d, %r9d
	movb	$0, -208(%rbp)
	movq	%r13, -200(%rbp)
	movq	$0, -192(%rbp)
	movw	%r9w, -184(%rbp)
	movq	$0, -168(%rbp)
	jmp	.L4406
	.p2align 4,,10
	.p2align 3
.L4465:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-300(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4414
	call	_ZdlPv@PLT
	jmp	.L4414
	.p2align 4,,10
	.p2align 3
.L4469:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L4419
	.p2align 4,,10
	.p2align 3
.L4467:
	movq	8(%r14), %rdi
	movq	-320(%rbp), %rcx
	movq	-312(%rbp), %rdx
	movl	-300(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L4419
	.p2align 4,,10
	.p2align 3
.L4471:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L4404
	.p2align 4,,10
	.p2align 3
.L4466:
	movdqu	-184(%rbp), %xmm0
	movups	%xmm0, -136(%rbp)
	jmp	.L4416
	.p2align 4,,10
	.p2align 3
.L4468:
	movq	-288(%rbp), %rsi
	leaq	-264(%rbp), %rdi
	call	_ZNK12v8_inspector8protocol7Runtime12RemoteObject7toValueEv@PLT
	movq	-264(%rbp), %rax
	leaq	.LC39(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	leaq	-272(%rbp), %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4421
	call	_ZdlPv@PLT
.L4421:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4420
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4420
	.p2align 4,,10
	.p2align 3
.L4427:
	call	*%rax
	jmp	.L4414
.L4470:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5835:
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl23getObjectByHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl23getObjectByHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_.str1.1,"aMS",@progbits,1
.LC40:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB9800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$1152921504606846975, %rdx
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rdi, -120(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rax, -112(%rbp)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rcx, -72(%rbp)
	cmpq	%rdx, %rax
	je	.L4523
	movq	%rsi, %r14
	movq	%rsi, %r13
	subq	-72(%rbp), %r14
	testq	%rax, %rax
	je	.L4497
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L4524
.L4474:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, -96(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -104(%rbp)
	leaq	8(%rax), %rbx
.L4496:
	movq	(%r12), %rax
	movq	$0, (%r12)
	movq	-96(%rbp), %r12
	movq	%rax, (%r12,%r14)
	movq	-72(%rbp), %rax
	cmpq	%rax, %r13
	je	.L4476
	movq	%r13, -56(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L4490:
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%rax, (%r12)
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L4477
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4478
	movq	32(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L4479
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L4480
	movq	%rbx, -80(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -88(%rbp)
	jmp	.L4483
	.p2align 4,,10
	.p2align 3
.L4526:
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4481:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L4525
.L4483:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4481
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4526
	call	*%rdx
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.L4483
	.p2align 4,,10
	.p2align 3
.L4525:
	movq	-80(%rbp), %rbx
	movq	-88(%rbp), %r12
	movq	(%r14), %r15
.L4480:
	testq	%r15, %r15
	je	.L4484
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4484:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4479:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L4485
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4486
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r14), %rdi
	movq	%rax, (%r14)
	leaq	104(%r14), %rax
	cmpq	%rax, %rdi
	je	.L4487
	call	_ZdlPv@PLT
.L4487:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L4488
	call	_ZdlPv@PLT
.L4488:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L4489
	call	_ZdlPv@PLT
.L4489:
	movl	$136, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4485:
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4477:
	addq	$8, %rbx
	addq	$8, %r12
	cmpq	%rbx, -56(%rbp)
	jne	.L4490
.L4527:
	movq	-56(%rbp), %r13
	movq	-96(%rbp), %rsi
	movq	%r13, %rax
	subq	-72(%rbp), %rax
	leaq	8(%rsi,%rax), %rbx
.L4476:
	movq	-112(%rbp), %rax
	cmpq	%rax, %r13
	je	.L4491
	subq	%r13, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L4499
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L4493:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L4493
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	leaq	(%rbx,%rdi), %rdx
	addq	%r13, %rdi
	movq	%rdi, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L4494
.L4492:
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L4494:
	leaq	8(%rbx,%rsi), %rbx
.L4491:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L4495
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L4495:
	movq	-96(%rbp), %xmm0
	movq	-120(%rbp), %rax
	movq	%rbx, %xmm2
	movq	-104(%rbp), %rsi
	punpcklqdq	%xmm2, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4478:
	.cfi_restore_state
	movq	%r13, %rdi
	addq	$8, %rbx
	addq	$8, %r12
	call	*%rax
	cmpq	%rbx, -56(%rbp)
	jne	.L4490
	jmp	.L4527
	.p2align 4,,10
	.p2align 3
.L4486:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L4485
	.p2align 4,,10
	.p2align 3
.L4524:
	testq	%rcx, %rcx
	jne	.L4475
	movq	$0, -104(%rbp)
	movl	$8, %ebx
	movq	$0, -96(%rbp)
	jmp	.L4496
	.p2align 4,,10
	.p2align 3
.L4497:
	movl	$8, %ebx
	jmp	.L4474
.L4499:
	movq	%rbx, %rdx
	jmp	.L4492
.L4475:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L4474
.L4523:
	leaq	.LC40(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9800:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.rodata._ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC41:
	.string	"array expected"
	.section	.text._ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"axG",@progbits,_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB7160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L4529
	cmpl	$7, 8(%rsi)
	movq	%rsi, %r12
	jne	.L4529
	movq	%rdx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -168(%rbp)
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	24(%r12), %rax
	subq	16(%r12), %rax
	movq	%rax, %rdx
	movq	%rax, -136(%rbp)
	sarq	$3, %rax
	testq	%rdx, %rdx
	js	.L4649
	testq	%rax, %rax
	jne	.L4533
.L4552:
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L4650
	movq	-160(%rbp), %rax
	movq	$0, (%rax)
	movq	-168(%rbp), %rax
	movq	8(%rax), %rcx
	movq	(%rax), %r12
	movq	%rcx, -128(%rbp)
	cmpq	%r12, %rcx
	je	.L4570
	.p2align 4,,10
	.p2align 3
.L4571:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L4572
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4573
	movq	32(%r15), %r14
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r14, %r14
	je	.L4574
	movq	8(%r14), %rbx
	movq	(%r14), %r13
	cmpq	%r13, %rbx
	jne	.L4578
	jmp	.L4575
	.p2align 4,,10
	.p2align 3
.L4652:
	movq	%rdi, -120(%rbp)
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movq	-120(%rbp), %rdi
	movl	$40, %esi
	call	_ZdlPvm@PLT
.L4576:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L4651
.L4578:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L4576
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4652
	call	*%rax
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L4578
	.p2align 4,,10
	.p2align 3
.L4651:
	movq	(%r14), %r13
.L4575:
	testq	%r13, %r13
	je	.L4579
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L4579:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4574:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L4580
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4581
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	104(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4582
	call	_ZdlPv@PLT
.L4582:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4583
	call	_ZdlPv@PLT
.L4583:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4584
	call	_ZdlPv@PLT
.L4584:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4580:
	movl	$40, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4572:
	addq	$8, %r12
	cmpq	%r12, -128(%rbp)
	jne	.L4571
.L4658:
	movq	-168(%rbp), %rax
	movq	(%rax), %r12
.L4570:
	testq	%r12, %r12
	je	.L4585
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4585:
	movq	-168(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4528:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4653
	movq	-160(%rbp), %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4533:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_Znwm@PLT
	movq	-168(%rbp), %rdx
	movq	%rax, -152(%rbp)
	movq	8(%rdx), %rcx
	movq	(%rdx), %r14
	movq	%rcx, -120(%rbp)
	cmpq	%r14, %rcx
	je	.L4536
	movq	%r15, -184(%rbp)
	movq	%r12, -176(%rbp)
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L4550:
	movq	(%r14), %rax
	movq	$0, (%r14)
	movq	%rax, (%r12)
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L4537
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4538
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	movq	32(%r15), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L4539
	movq	8(%rax), %rbx
	movq	(%rax), %r13
	cmpq	%r13, %rbx
	je	.L4540
	movq	%r12, -144(%rbp)
	jmp	.L4543
	.p2align 4,,10
	.p2align 3
.L4655:
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4541:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L4654
.L4543:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L4541
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4655
	call	*%rax
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L4543
	.p2align 4,,10
	.p2align 3
.L4654:
	movq	-128(%rbp), %rax
	movq	-144(%rbp), %r12
	movq	(%rax), %r13
.L4540:
	testq	%r13, %r13
	je	.L4544
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L4544:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4539:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L4545
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4546
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	104(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4547
	call	_ZdlPv@PLT
.L4547:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4548
	call	_ZdlPv@PLT
.L4548:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4549
	call	_ZdlPv@PLT
.L4549:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4545:
	movl	$40, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4537:
	addq	$8, %r14
	addq	$8, %r12
	cmpq	%r14, -120(%rbp)
	jne	.L4550
	movq	-168(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r15
	movq	(%rax), %r14
.L4536:
	testq	%r14, %r14
	je	.L4551
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4551:
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %r13
	movq	%rax, %xmm0
	addq	-136(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 16(%r13)
	movq	16(%r12), %rax
	movups	%xmm0, 0(%r13)
	cmpq	%rax, 24(%r12)
	je	.L4552
	leaq	-96(%rbp), %rax
	movq	%r15, -120(%rbp)
	xorl	%ebx, %ebx
	movq	%rax, -144(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -152(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L4569:
	movq	-144(%rbp), %r14
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	-120(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-152(%rbp), %rdi
	je	.L4553
	call	_ZdlPv@PLT
.L4553:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol9ListValue2atEm@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	je	.L4554
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r13)
.L4555:
	movq	-104(%rbp), %r15
	testq	%r15, %r15
	je	.L4556
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4557
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	movq	32(%r15), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L4558
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L4559
	movq	%rbx, -176(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -184(%rbp)
	jmp	.L4562
	.p2align 4,,10
	.p2align 3
.L4657:
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4560:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L4656
.L4562:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L4560
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4657
	call	*%rdx
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L4562
	.p2align 4,,10
	.p2align 3
.L4656:
	movq	-136(%rbp), %rax
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r12
	movq	(%rax), %r14
.L4559:
	testq	%r14, %r14
	je	.L4563
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4563:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4558:
	movq	8(%r15), %r14
	testq	%r14, %r14
	je	.L4564
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4565
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r14), %rdi
	movq	%rax, (%r14)
	leaq	104(%r14), %rax
	cmpq	%rax, %rdi
	je	.L4566
	call	_ZdlPv@PLT
.L4566:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L4567
	call	_ZdlPv@PLT
.L4567:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L4568
	call	_ZdlPv@PLT
.L4568:
	movl	$136, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4564:
	movl	$40, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4556:
	movq	24(%r12), %rax
	subq	16(%r12), %rax
	addq	$1, %rbx
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L4569
	movq	-120(%rbp), %r15
	jmp	.L4552
	.p2align 4,,10
	.p2align 3
.L4554:
	movq	-128(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L4555
	.p2align 4,,10
	.p2align 3
.L4557:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L4556
	.p2align 4,,10
	.p2align 3
.L4565:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L4564
	.p2align 4,,10
	.p2align 3
.L4573:
	movq	%r15, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -128(%rbp)
	jne	.L4571
	jmp	.L4658
	.p2align 4,,10
	.p2align 3
.L4529:
	leaq	.LC41(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-160(%rbp), %rax
	movq	$0, (%rax)
	jmp	.L4528
	.p2align 4,,10
	.p2align 3
.L4650:
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rdx
	movq	%rdx, (%rax)
	jmp	.L4528
	.p2align 4,,10
	.p2align 3
.L4581:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4580
	.p2align 4,,10
	.p2align 3
.L4538:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L4537
	.p2align 4,,10
	.p2align 3
.L4546:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4545
.L4653:
	call	__stack_chk_fail@PLT
.L4649:
	leaq	.LC28(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7160:
	.size	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB5732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L4660
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r12
	je	.L4661
.L4660:
	leaq	.LC13(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, (%rbx)
.L4659:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4783
	addq	$120, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4661:
	.cfi_restore_state
	movl	$40, %edi
	leaq	-96(%rbp), %r13
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rcx
	cmpl	$6, 8(%r12)
	movq	%r15, %rdi
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 32(%rax)
	movq	$0x000000000, 16(%rax)
	movl	$0, 24(%rax)
	movq	%rax, -120(%rbp)
	movl	$0, %eax
	cmovne	%rax, %r12
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC29(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -136(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdi
	je	.L4664
	call	_ZdlPv@PLT
.L4664:
	leaq	.LC29(%rip), %rsi
	movq	%r15, %rdi
	leaq	-104(%rbp), %r14
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol7Runtime9CallFrame9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	movq	-120(%rbp), %rcx
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	8(%rcx), %r8
	movq	%rax, 8(%rcx)
	testq	%r8, %r8
	je	.L4666
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4667
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r8), %rdi
	movq	%rax, (%r8)
	leaq	104(%r8), %rax
	cmpq	%rax, %rdi
	je	.L4668
	movq	%r8, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L4668:
	movq	48(%r8), %rdi
	leaq	64(%r8), %rax
	cmpq	%rax, %rdi
	je	.L4669
	movq	%r8, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L4669:
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L4670
	movq	%r8, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L4670:
	movl	$136, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L4671:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L4666
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4673
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r8), %rdi
	movq	%rax, (%r8)
	leaq	104(%r8), %rax
	cmpq	%rax, %rdi
	je	.L4674
	movq	%r8, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L4674:
	movq	48(%r8), %rdi
	leaq	64(%r8), %rax
	cmpq	%rax, %rdi
	je	.L4675
	movq	%r8, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L4675:
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L4676
	movq	%r8, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L4676:
	movl	$136, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L4666:
	leaq	.LC30(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r8
	cmpq	-128(%rbp), %rdi
	je	.L4677
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L4677:
	leaq	.LC30(%rip), %rsi
	movq	%r15, %rdi
	movq	%r8, -136(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %r8
	movq	$0x000000000, -104(%rbp)
	testq	%r8, %r8
	je	.L4680
	movq	(%r8), %rax
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L4679
.L4680:
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L4679:
	movsd	-104(%rbp), %xmm0
	movq	-120(%rbp), %rax
	leaq	.LC31(%rip), %rsi
	movq	%r13, %rdi
	movsd	%xmm0, 16(%rax)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r8
	cmpq	-128(%rbp), %rdi
	je	.L4681
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L4681:
	leaq	.LC31(%rip), %rsi
	movq	%r15, %rdi
	movq	%r8, -136(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %r8
	movl	$0, -104(%rbp)
	testq	%r8, %r8
	je	.L4684
	movq	(%r8), %rax
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	*48(%rax)
	testb	%al, %al
	jne	.L4683
.L4684:
	leaq	.LC16(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L4683:
	movl	-104(%rbp), %eax
	movq	-120(%rbp), %rcx
	leaq	.LC32(%rip), %rsi
	movq	%r13, %rdi
	movl	%eax, 24(%rcx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	cmpq	-128(%rbp), %rdi
	je	.L4685
	call	_ZdlPv@PLT
.L4685:
	leaq	.LC32(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_12HeapProfiler23SamplingHeapProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-120(%rbp), %rsi
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	32(%rsi), %rcx
	movq	%rax, 32(%rsi)
	movq	%rcx, -144(%rbp)
	movq	%rcx, %rax
	testq	%rcx, %rcx
	je	.L4719
	movq	8(%rcx), %rcx
	movq	(%rax), %r12
	movq	%rcx, -128(%rbp)
	cmpq	%r12, %rcx
	je	.L4687
	movq	%rbx, -152(%rbp)
	movq	%r15, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L4701:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L4688
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4689
	movq	32(%r15), %r13
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L4690
	movq	8(%r13), %rbx
	movq	0(%r13), %r14
	cmpq	%r14, %rbx
	je	.L4691
	movq	%r12, -136(%rbp)
	movq	%r14, %r12
	jmp	.L4694
	.p2align 4,,10
	.p2align 3
.L4785:
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4692:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L4784
.L4694:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L4692
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rax
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4785
	call	*%rdx
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L4694
	.p2align 4,,10
	.p2align 3
.L4784:
	movq	-136(%rbp), %r12
	movq	0(%r13), %r14
.L4691:
	testq	%r14, %r14
	je	.L4695
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4695:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4690:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L4696
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4697
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	104(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4698
	call	_ZdlPv@PLT
.L4698:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4699
	call	_ZdlPv@PLT
.L4699:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4700
	call	_ZdlPv@PLT
.L4700:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4696:
	movl	$40, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4688:
	addq	$8, %r12
	cmpq	%r12, -128(%rbp)
	jne	.L4701
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rbx
	movq	-160(%rbp), %r15
	movq	(%rax), %r12
.L4687:
	testq	%r12, %r12
	je	.L4702
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4702:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	-104(%rbp), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L4719
	movq	8(%rax), %rcx
	movq	(%rax), %r12
	movq	%rcx, -128(%rbp)
	cmpq	%r12, %rcx
	je	.L4703
	movq	%rbx, -152(%rbp)
	movq	%r15, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L4717:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L4704
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4705
	movq	32(%r15), %r13
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L4706
	movq	8(%r13), %rbx
	movq	0(%r13), %r14
	cmpq	%r14, %rbx
	je	.L4707
	movq	%r12, -136(%rbp)
	movq	%r14, %r12
	jmp	.L4710
	.p2align 4,,10
	.p2align 3
.L4787:
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4708:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L4786
.L4710:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L4708
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rax
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4787
	call	*%rdx
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L4710
	.p2align 4,,10
	.p2align 3
.L4786:
	movq	-136(%rbp), %r12
	movq	0(%r13), %r14
.L4707:
	testq	%r14, %r14
	je	.L4711
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4711:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4706:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L4712
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4713
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	104(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4714
	call	_ZdlPv@PLT
.L4714:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4715
	call	_ZdlPv@PLT
.L4715:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4716
	call	_ZdlPv@PLT
.L4716:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4712:
	movl	$40, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4704:
	addq	$8, %r12
	cmpq	%r12, -128(%rbp)
	jne	.L4717
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rbx
	movq	-160(%rbp), %r15
	movq	(%rax), %r12
.L4703:
	testq	%r12, %r12
	je	.L4718
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4718:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4719:
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L4788
	movq	-120(%rbp), %rax
	movq	%rax, (%rbx)
	jmp	.L4659
	.p2align 4,,10
	.p2align 3
.L4788:
	movq	-120(%rbp), %rdi
	movq	$0, (%rbx)
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4659
	.p2align 4,,10
	.p2align 3
.L4705:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L4704
	.p2align 4,,10
	.p2align 3
.L4689:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L4688
	.p2align 4,,10
	.p2align 3
.L4697:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4696
	.p2align 4,,10
	.p2align 3
.L4713:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4712
	.p2align 4,,10
	.p2align 3
.L4673:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L4666
	.p2align 4,,10
	.p2align 3
.L4667:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L4671
.L4783:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5732:
	.size	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode5cloneEv
	.type	_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode5cloneEv, @function
_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode5cloneEv:
.LFB5734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4790
	movq	(%rdi), %rax
	call	*24(%rax)
.L4790:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4796
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4796:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5734:
	.size	_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode5cloneEv, .-_ZNK12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode5cloneEv
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB9850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movabsq	$1152921504606846975, %rdi
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L4824
	movq	%rsi, %r13
	movq	%rsi, %rbx
	subq	%r12, %rsi
	testq	%rax, %rax
	je	.L4811
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L4825
.L4799:
	movq	%r14, %rdi
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdx
	movq	%rax, -56(%rbp)
	addq	%rax, %r14
	movq	%r14, -64(%rbp)
	leaq	8(%rax), %r14
.L4810:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rcx
	movq	$0, (%rdx)
	movq	%rax, (%rcx,%rsi)
	cmpq	%r12, %r13
	je	.L4801
	movq	%rcx, %r14
	movq	%r12, %r15
	jmp	.L4804
	.p2align 4,,10
	.p2align 3
.L4827:
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L4802:
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	je	.L4826
.L4804:
	movq	(%r15), %rsi
	movq	$0, (%r15)
	movq	%rsi, (%r14)
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L4802
	movq	(%rdi), %rsi
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %rax
	movq	24(%rsi), %rsi
	cmpq	%rax, %rsi
	je	.L4827
	call	*%rsi
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	jne	.L4804
	.p2align 4,,10
	.p2align 3
.L4826:
	movq	-56(%rbp), %rcx
	movq	%r13, %rax
	subq	%r12, %rax
	leaq	8(%rcx,%rax), %r14
.L4801:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r13
	je	.L4805
	movq	%rax, %r15
	subq	%r13, %r15
	leaq	-8(%r15), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	addq	$1, %rsi
	testq	%rdi, %rdi
	je	.L4813
	movq	%rsi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L4807:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L4807
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%r14,%r8), %rdx
	leaq	0(%r13,%r8), %rbx
	cmpq	%rax, %rsi
	je	.L4808
.L4806:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L4808:
	leaq	8(%r14,%rdi), %r14
.L4805:
	testq	%r12, %r12
	je	.L4809
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4809:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r14, %xmm2
	movq	-64(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4825:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L4800
	movq	$0, -64(%rbp)
	movl	$8, %r14d
	movq	$0, -56(%rbp)
	jmp	.L4810
	.p2align 4,,10
	.p2align 3
.L4811:
	movl	$8, %r14d
	jmp	.L4799
.L4813:
	movq	%r14, %rdx
	jmp	.L4806
.L4800:
	cmpq	%rdi, %r9
	cmovbe	%r9, %rdi
	leaq	0(,%rdi,8), %r14
	jmp	.L4799
.L4824:
	leaq	.LC40(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9850:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB5738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L4829
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r14
	je	.L4830
.L4829:
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-120(%rbp), %rax
	movq	$0, (%rax)
.L4828:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4957
	movq	-120(%rbp), %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4830:
	.cfi_restore_state
	movl	$24, %edi
	leaq	-96(%rbp), %r15
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE(%rip), %rcx
	cmpl	$6, 8(%r14)
	movq	%r12, %rdi
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rax, -144(%rbp)
	movl	$0, %eax
	cmovne	%rax, %r14
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC33(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r13
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdi
	je	.L4833
	call	_ZdlPv@PLT
.L4833:
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-104(%rbp), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-144(%rbp), %rcx
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	8(%rcx), %rdi
	movq	%rax, 8(%rcx)
	testq	%rdi, %rdi
	je	.L4835
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4835
	movq	(%rdi), %rax
	call	*24(%rax)
.L4835:
	leaq	.LC34(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-128(%rbp), %rdi
	je	.L4837
	call	_ZdlPv@PLT
.L4837:
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	testq	%r14, %r14
	je	.L4838
	cmpl	$7, 8(%r14)
	jne	.L4838
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, %r13
	movups	%xmm0, (%rax)
	movq	24(%r14), %rbx
	subq	16(%r14), %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	testq	%rbx, %rbx
	js	.L4958
	testq	%rax, %rax
	jne	.L4842
.L4849:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L4840
	movq	8(%r13), %rbx
	movq	0(%r13), %r15
	cmpq	%r15, %rbx
	je	.L4856
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %r14
	jmp	.L4859
	.p2align 4,,10
	.p2align 3
.L4960:
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L4857:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L4959
.L4859:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L4857
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L4960
	call	*%rax
	jmp	.L4857
	.p2align 4,,10
	.p2align 3
.L4838:
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L4840:
	movq	-144(%rbp), %rax
	movq	16(%rax), %r14
	movq	%r13, 16(%rax)
	testq	%r14, %r14
	je	.L4861
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	je	.L4862
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %r13
	jmp	.L4865
	.p2align 4,,10
	.p2align 3
.L4962:
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L4863:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L4961
.L4865:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L4863
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	je	.L4962
	call	*%rax
	jmp	.L4863
	.p2align 4,,10
	.p2align 3
.L4961:
	movq	(%r14), %r15
.L4862:
	testq	%r15, %r15
	je	.L4866
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4866:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4861:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L4963
	movq	-120(%rbp), %rax
	movq	-144(%rbp), %rcx
	movq	%rcx, (%rax)
	jmp	.L4828
	.p2align 4,,10
	.p2align 3
.L4963:
	movq	-120(%rbp), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev(%rip), %rdx
	movq	$0, (%rax)
	movq	-144(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4964
	movq	-144(%rbp), %rax
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE(%rip), %rcx
	movq	16(%rax), %r12
	movq	%rcx, (%rax)
	testq	%r12, %r12
	je	.L4867
	movq	8(%r12), %rbx
	movq	(%r12), %r14
	cmpq	%r14, %rbx
	je	.L4868
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %r13
	jmp	.L4871
	.p2align 4,,10
	.p2align 3
.L4966:
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L4869:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L4965
.L4871:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4869
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	je	.L4966
	call	*%rax
	jmp	.L4869
	.p2align 4,,10
	.p2align 3
.L4965:
	movq	(%r12), %r14
.L4868:
	testq	%r14, %r14
	je	.L4872
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4872:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4867:
	movq	-144(%rbp), %rax
	movq	8(%rax), %r13
	testq	%r13, %r13
	je	.L4873
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev(%rip), %rbx
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L4874
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	32(%r13), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L4875
	movq	8(%rax), %r12
	movq	(%rax), %r14
	cmpq	%r14, %r12
	jne	.L4879
	jmp	.L4876
	.p2align 4,,10
	.p2align 3
.L4968:
	call	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	movl	$40, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4877:
	addq	$8, %r14
	cmpq	%r14, %r12
	je	.L4967
.L4879:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L4877
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	je	.L4968
	call	*%rax
	jmp	.L4877
	.p2align 4,,10
	.p2align 3
.L4967:
	movq	-128(%rbp), %rax
	movq	(%rax), %r14
.L4876:
	testq	%r14, %r14
	je	.L4880
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4880:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4875:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L4881
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4882
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4883
	call	_ZdlPv@PLT
.L4883:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4884
	call	_ZdlPv@PLT
.L4884:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4885
	call	_ZdlPv@PLT
.L4885:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4881:
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4873:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L4828
	.p2align 4,,10
	.p2align 3
.L4959:
	movq	0(%r13), %r15
.L4856:
	testq	%r15, %r15
	je	.L4860
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4860:
	movq	%r13, %rdi
	movl	$24, %esi
	xorl	%r13d, %r13d
	call	_ZdlPvm@PLT
	jmp	.L4840
	.p2align 4,,10
	.p2align 3
.L4842:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	8(%r13), %rcx
	movq	0(%r13), %r8
	movq	%rax, -152(%rbp)
	cmpq	%r8, %rcx
	je	.L4844
	movq	%r14, -160(%rbp)
	movq	%rcx, %r14
	movq	%rbx, -168(%rbp)
	movq	%r8, %rbx
	movq	%r12, -176(%rbp)
	movq	%rax, %r12
	jmp	.L4847
	.p2align 4,,10
	.p2align 3
.L4970:
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L4845:
	addq	$8, %rbx
	addq	$8, %r12
	cmpq	%rbx, %r14
	je	.L4969
.L4847:
	movq	(%rbx), %rdx
	movq	$0, (%rbx)
	movq	%rdx, (%r12)
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4845
	movq	(%rdi), %rdx
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L4970
	call	*%rdx
	jmp	.L4845
	.p2align 4,,10
	.p2align 3
.L4969:
	movq	-160(%rbp), %r14
	movq	-168(%rbp), %rbx
	movq	-176(%rbp), %r12
	movq	0(%r13), %r8
.L4844:
	testq	%r8, %r8
	je	.L4848
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L4848:
	movq	-152(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, 16(%r13)
	movups	%xmm0, 0(%r13)
	movq	24(%r14), %rax
	cmpq	%rax, 16(%r14)
	je	.L4849
	xorl	%ebx, %ebx
	jmp	.L4855
	.p2align 4,,10
	.p2align 3
.L4971:
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r13)
.L4852:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4853
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4854
	movl	$32, %esi
	call	_ZdlPvm@PLT
.L4853:
	movq	24(%r14), %rax
	subq	16(%r14), %rax
	addq	$1, %rbx
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jbe	.L4849
.L4855:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-128(%rbp), %rdi
	je	.L4850
	call	_ZdlPv@PLT
.L4850:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol9ListValue2atEm@PLT
	movq	-136(%rbp), %rdi
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	jne	.L4971
	movq	-136(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L4852
	.p2align 4,,10
	.p2align 3
.L4854:
	call	*%rax
	jmp	.L4853
	.p2align 4,,10
	.p2align 3
.L4964:
	movq	-144(%rbp), %rdi
	call	*%rax
	jmp	.L4828
	.p2align 4,,10
	.p2align 3
.L4874:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L4873
.L4882:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4881
.L4957:
	call	__stack_chk_fail@PLT
.L4958:
	leaq	.LC28(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5738:
	.size	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile5cloneEv
	.type	_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile5cloneEv, @function
_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile5cloneEv:
.LFB5740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4973
	movq	(%rdi), %rax
	call	*24(%rax)
.L4973:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4979
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4979:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5740:
	.size	_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile5cloneEv, .-_ZNK12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile5cloneEv
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB9880:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L4994
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L4990
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L4995
.L4982:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L4989:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L4996
	testq	%r13, %r13
	jg	.L4985
	testq	%r9, %r9
	jne	.L4988
.L4986:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4996:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L4985
.L4988:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L4986
	.p2align 4,,10
	.p2align 3
.L4995:
	testq	%rsi, %rsi
	jne	.L4983
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L4989
	.p2align 4,,10
	.p2align 3
.L4985:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L4986
	jmp	.L4988
	.p2align 4,,10
	.p2align 3
.L4990:
	movl	$4, %r14d
	jmp	.L4982
.L4994:
	leaq	.LC40(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4983:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L4982
	.cfi_endproc
.LFE9880:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB5748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L4998
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r14
	je	.L4999
.L4998:
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, 0(%r13)
.L4997:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5064
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4999:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationE(%rip), %rcx
	cmpl	$6, 8(%r14)
	movq	%r12, %rdi
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	%rax, -144(%rbp)
	movl	$0, %eax
	cmovne	%rax, %r14
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	-96(%rbp), %rax
	leaq	.LC36(%rip), %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r15
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdi
	je	.L5002
	call	_ZdlPv@PLT
.L5002:
	leaq	.LC36(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	testq	%r15, %r15
	je	.L5003
	cmpl	$7, 8(%r15)
	jne	.L5003
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movabsq	$2305843009213693951, %rdx
	movq	$0, 16(%rax)
	movq	%rax, %r14
	movups	%xmm0, (%rax)
	movq	24(%r15), %rax
	subq	16(%r15), %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	ja	.L5065
	testq	%rax, %rax
	jne	.L5066
.L5007:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L5005
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L5018
	call	_ZdlPv@PLT
.L5018:
	movq	%r14, %rdi
	movl	$24, %esi
	xorl	%r14d, %r14d
	call	_ZdlPvm@PLT
.L5005:
	movq	-144(%rbp), %rax
	movq	8(%rax), %r15
	movq	%r14, 8(%rax)
	testq	%r15, %r15
	je	.L5019
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L5020
	call	_ZdlPv@PLT
.L5020:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L5019:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L5067
	movq	-144(%rbp), %rax
	movq	%rax, 0(%r13)
	jmp	.L4997
	.p2align 4,,10
	.p2align 3
.L5067:
	movq	-144(%rbp), %rax
	movq	$0, 0(%r13)
	leaq	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD0Ev(%rip), %rdx
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5068
	movq	-144(%rbp), %rax
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationE(%rip), %rcx
	movq	8(%rax), %r12
	movq	%rcx, (%rax)
	testq	%r12, %r12
	je	.L5021
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5022
	call	_ZdlPv@PLT
.L5022:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5021:
	movq	-144(%rbp), %rdi
	movl	$16, %esi
	call	_ZdlPvm@PLT
	jmp	.L4997
	.p2align 4,,10
	.p2align 3
.L5003:
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L5005
	.p2align 4,,10
	.p2align 3
.L5066:
	leaq	0(,%rax,4), %rbx
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	(%r14), %r8
	movq	8(%r14), %rdx
	movq	%rax, %rcx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L5069
	testq	%r8, %r8
	jne	.L5009
.L5010:
	movq	%rcx, %xmm0
	leaq	(%rcx,%rbx), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 16(%r14)
	movups	%xmm0, (%r14)
	movq	16(%r15), %rax
	cmpq	%rax, 24(%r15)
	je	.L5007
	leaq	-100(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%rax, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L5017:
	movq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	-120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-128(%rbp), %rdi
	je	.L5011
	call	_ZdlPv@PLT
.L5011:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol9ListValue2atEm@PLT
	movl	$0, -100(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L5014
	movq	(%rax), %rax
	movq	-136(%rbp), %rsi
	call	*48(%rax)
	testb	%al, %al
	jne	.L5013
.L5014:
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L5013:
	movl	-100(%rbp), %eax
	movq	8(%r14), %rsi
	cmpq	16(%r14), %rsi
	je	.L5015
	movl	%eax, (%rsi)
	addq	$1, %rbx
	addq	$4, 8(%r14)
	movq	24(%r15), %rax
	subq	16(%r15), %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L5017
	jmp	.L5007
	.p2align 4,,10
	.p2align 3
.L5015:
	movq	-136(%rbp), %rdx
	movq	%r14, %rdi
	addq	$1, %rbx
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	24(%r15), %rax
	subq	16(%r15), %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	ja	.L5017
	jmp	.L5007
	.p2align 4,,10
	.p2align 3
.L5068:
	movq	-144(%rbp), %rdi
	call	*%rax
	jmp	.L4997
	.p2align 4,,10
	.p2align 3
.L5069:
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%r8, -136(%rbp)
	call	memmove@PLT
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L5009:
	movq	%r8, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rcx
	jmp	.L5010
.L5064:
	call	__stack_chk_fail@PLT
.L5065:
	leaq	.LC28(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5748:
	.size	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNK12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification5cloneEv
	.type	_ZNK12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification5cloneEv, @function
_ZNK12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification5cloneEv:
.LFB5750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5071
	movq	(%rdi), %rax
	call	*24(%rax)
.L5071:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5077
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5077:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5750:
	.size	_ZNK12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification5cloneEv, .-_ZNK12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification5cloneEv
	.section	.text._ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol12HeapProfiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m,"axG",@progbits,_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol12HeapProfiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol12HeapProfiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	.type	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol12HeapProfiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m, @function
_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol12HeapProfiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m:
.LFB10822:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax,%rsi,8), %r10
	testq	%r10, %r10
	je	.L5095
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%r10), %rdx
	movl	$2147483648, %ebx
	movq	64(%rdx), %r8
	cmpq	%r8, %rcx
	je	.L5098
	.p2align 4,,10
	.p2align 3
.L5080:
	movq	(%rdx), %r9
	testq	%r9, %r9
	je	.L5085
	movq	64(%r9), %r8
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	8(%rdi)
	cmpq	%rdx, %rsi
	jne	.L5085
	movq	%r9, %rdx
	cmpq	%r8, %rcx
	jne	.L5080
.L5098:
	movq	8(%r11), %r8
	movq	16(%rdx), %r15
	movq	8(%rdx), %r13
	movq	(%r11), %r14
	cmpq	%r15, %r8
	movq	%r15, %r9
	cmovbe	%r8, %r9
	testq	%r9, %r9
	je	.L5081
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L5082:
	movzwl	0(%r13,%rax,2), %r12d
	cmpw	%r12w, (%r14,%rax,2)
	jne	.L5080
	addq	$1, %rax
	cmpq	%rax, %r9
	jne	.L5082
.L5081:
	subq	%r15, %r8
	cmpq	%rbx, %r8
	jge	.L5080
	movabsq	$-2147483649, %rax
	cmpq	%rax, %r8
	jle	.L5080
	testl	%r8d, %r8d
	jne	.L5080
	popq	%rbx
	movq	%r10, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5085:
	.cfi_restore_state
	xorl	%r10d, %r10d
	popq	%rbx
	popq	%r12
	movq	%r10, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5095:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	%r10, %rax
	ret
	.cfi_endproc
.LFE10822:
	.size	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol12HeapProfiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m, .-_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol12HeapProfiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	.section	.text.unlikely._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE,"ax",@progbits
	.align 2
.LCOLDB42:
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE,"ax",@progbits
.LHOTB42:
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE:
.LFB5823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$72, %rdi
	subq	$104, %rsp
	movl	%esi, -132(%rbp)
	movq	32(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	jne	.L5100
	movq	(%rdx), %rax
	movq	8(%rdx), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L5103
	.p2align 4,,10
	.p2align 3
.L5102:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%r14)
	cmpq	%rax, %rsi
	jne	.L5102
	testq	%rcx, %rcx
	je	.L5103
.L5100:
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	80(%rbx)
	movq	%rdx, %rsi
	movq	%r14, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol12HeapProfiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	testq	%rax, %rax
	je	.L5104
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	movq	%rdx, -144(%rbp)
	je	.L5104
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	movq	-144(%rbp), %rdx
	movq	48(%rdx), %rax
	addq	56(%rdx), %rbx
	movq	%rbx, %rdi
	testb	$1, %al
	jne	.L5127
.L5105:
	movq	(%r15), %rdx
	movl	-132(%rbp), %esi
	movq	%r12, %r9
	leaq	-120(%rbp), %r8
	movq	$0, (%r15)
	movq	%r13, %rcx
	movq	%rdx, -120(%rbp)
	movq	%r14, %rdx
	call	*%rax
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5106
	movq	(%rdi), %rax
	call	*24(%rax)
.L5106:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5128
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5127:
	.cfi_restore_state
	movq	(%rbx), %rdx
	movq	-1(%rdx,%rax), %rax
	jmp	.L5105
	.p2align 4,,10
	.p2align 3
.L5103:
	movq	$1, 32(%r14)
	movl	$1, %ecx
	jmp	.L5100
.L5128:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.cfi_startproc
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold:
.LFSB5823:
.L5104:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	-112(%rbp), %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	movq	48, %rax
	ud2
	.cfi_endproc
.LFE5823:
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.section	.text.unlikely._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold
.LCOLDE42:
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
.LHOTE42:
	.section	.text._ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_,"axG",@progbits,_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	.type	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_, @function
_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_:
.LFB8475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	32(%rsi), %r12
	testq	%r12, %r12
	jne	.L5130
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rcx
	cmpq	%rcx, %rax
	je	.L5133
	.p2align 4,,10
	.p2align 3
.L5132:
	movq	%r12, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%r12, %rdx
	movq	%rdx, %r12
	movsbq	-2(%rax), %rdx
	addq	%rdx, %r12
	movq	%r12, 32(%rbx)
	cmpq	%rax, %rcx
	jne	.L5132
	testq	%r12, %r12
	je	.L5133
.L5130:
	xorl	%edx, %edx
	movq	%r12, %rax
	movq	%r12, %rcx
	movq	%r13, %rdi
	divq	8(%r13)
	movq	%rdx, %r15
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol12HeapProfiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	testq	%rax, %rax
	je	.L5134
	movq	(%rax), %rdx
	leaq	48(%rdx), %rax
	testq	%rdx, %rdx
	je	.L5134
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5133:
	.cfi_restore_state
	movq	$1, 32(%rbx)
	movl	$1, %r12d
	jmp	.L5130
	.p2align 4,,10
	.p2align 3
.L5134:
	movl	$72, %edi
	call	_Znwm@PLT
	movq	(%rbx), %rdx
	movq	$0, (%rax)
	movq	%rax, %r14
	leaq	24(%rax), %rax
	movq	%rax, 8(%r14)
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdx
	je	.L5171
	movq	%rdx, 8(%r14)
	movq	16(%rbx), %rdx
	movq	%rdx, 24(%r14)
.L5137:
	movq	%rax, (%rbx)
	movq	8(%rbx), %rdx
	xorl	%eax, %eax
	leaq	32(%r13), %rdi
	movw	%ax, 16(%rbx)
	movq	32(%rbx), %rax
	movl	$1, %ecx
	movq	$0, 8(%rbx)
	movq	8(%r13), %rsi
	movq	%rdx, 16(%r14)
	movq	24(%r13), %rdx
	movq	%rax, 40(%r14)
	movq	$0, 48(%r14)
	movq	$0, 56(%r14)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %rbx
	testb	%al, %al
	jne	.L5138
	movq	0(%r13), %r8
.L5139:
	salq	$3, %r15
	movq	%r12, 64(%r14)
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L5148
	movq	(%rdx), %rdx
	movq	%rdx, (%r14)
	movq	(%rax), %rax
	movq	%r14, (%rax)
.L5149:
	addq	$1, 24(%r13)
	addq	$24, %rsp
	leaq	48(%r14), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5138:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L5172
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L5173
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r13), %r10
	movq	%rax, %r8
.L5141:
	movq	16(%r13), %rsi
	movq	$0, 16(%r13)
	testq	%rsi, %rsi
	je	.L5143
	xorl	%edi, %edi
	leaq	16(%r13), %r9
	jmp	.L5144
	.p2align 4,,10
	.p2align 3
.L5145:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L5146:
	testq	%rsi, %rsi
	je	.L5143
.L5144:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	64(%rcx), %rax
	divq	%rbx
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L5145
	movq	16(%r13), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r13)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L5152
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L5144
	.p2align 4,,10
	.p2align 3
.L5143:
	movq	0(%r13), %rdi
	cmpq	%rdi, %r10
	je	.L5147
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L5147:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rbx, 8(%r13)
	divq	%rbx
	movq	%r8, 0(%r13)
	movq	%rdx, %r15
	jmp	.L5139
	.p2align 4,,10
	.p2align 3
.L5171:
	movdqu	16(%rbx), %xmm0
	movups	%xmm0, 24(%r14)
	jmp	.L5137
	.p2align 4,,10
	.p2align 3
.L5148:
	movq	16(%r13), %rdx
	movq	%r14, 16(%r13)
	movq	%rdx, (%r14)
	testq	%rdx, %rdx
	je	.L5150
	movq	64(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r13)
	movq	%r14, (%r8,%rdx,8)
	movq	0(%r13), %rax
	addq	%r15, %rax
.L5150:
	leaq	16(%r13), %rdx
	movq	%rdx, (%rax)
	jmp	.L5149
	.p2align 4,,10
	.p2align 3
.L5152:
	movq	%rdx, %rdi
	jmp	.L5146
.L5172:
	leaq	48(%r13), %r8
	movq	$0, 48(%r13)
	movq	%r8, %r10
	jmp	.L5141
.L5173:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8475:
	.size	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_, .-_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE.str1.8,"aMS",@progbits,1
	.align 8
.LC44:
	.string	"HeapProfiler.addInspectedHeapObject"
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE.str1.1,"aMS",@progbits,1
.LC45:
	.string	"HeapProfiler.collectGarbage"
.LC46:
	.string	"HeapProfiler.disable"
.LC47:
	.string	"HeapProfiler.enable"
.LC48:
	.string	"HeapProfiler.getHeapObjectId"
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE.str1.8
	.align 8
.LC49:
	.string	"HeapProfiler.getObjectByHeapObjectId"
	.align 8
.LC50:
	.string	"HeapProfiler.getSamplingProfile"
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE.str1.1
.LC51:
	.string	"HeapProfiler.startSampling"
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE.str1.8
	.align 8
.LC52:
	.string	"HeapProfiler.startTrackingHeapObjects"
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE.str1.1
.LC53:
	.string	"HeapProfiler.stopSampling"
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE.str1.8
	.align 8
.LC54:
	.string	"HeapProfiler.stopTrackingHeapObjects"
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE.str1.1
.LC55:
	.string	"HeapProfiler.takeHeapSnapshot"
.LC56:
	.string	"HeapProfiler"
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE
	.type	_ZN12v8_inspector8protocol12HeapProfiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE, @function
_ZN12v8_inspector8protocol12HeapProfiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE:
.LFB5869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movl	$192, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%r13, %rsi
	leaq	-80(%rbp), %r13
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol14DispatcherBaseC2EPNS0_15FrontendChannelE@PLT
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler14DispatcherImplE(%rip), %rax
	movss	.LC43(%rip), %xmm0
	movq	%r12, 184(%rbx)
	movq	%rax, (%rbx)
	leaq	120(%rbx), %rax
	leaq	-96(%rbp), %r12
	movq	%rax, 72(%rbx)
	movq	%r12, %rdi
	leaq	72(%rbx), %r14
	leaq	176(%rbx), %rax
	movq	%rax, 128(%rbx)
	leaq	.LC44(%rip), %rsi
	movq	$1, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	movq	$1, 136(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 168(%rbx)
	movq	$0, 176(%rbx)
	movss	%xmm0, 104(%rbx)
	movss	%xmm0, 160(%rbx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl22addInspectedHeapObjectEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5175
	call	_ZdlPv@PLT
.L5175:
	leaq	.LC45(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl14collectGarbageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5176
	call	_ZdlPv@PLT
.L5176:
	leaq	.LC46(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5177
	call	_ZdlPv@PLT
.L5177:
	leaq	.LC47(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5178
	call	_ZdlPv@PLT
.L5178:
	leaq	.LC48(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl15getHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5179
	call	_ZdlPv@PLT
.L5179:
	leaq	.LC49(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl23getObjectByHeapObjectIdEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5180
	call	_ZdlPv@PLT
.L5180:
	leaq	.LC50(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl18getSamplingProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5181
	call	_ZdlPv@PLT
.L5181:
	leaq	.LC51(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl13startSamplingEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5182
	call	_ZdlPv@PLT
.L5182:
	leaq	.LC52(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl24startTrackingHeapObjectsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5183
	call	_ZdlPv@PLT
.L5183:
	leaq	.LC53(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl12stopSamplingEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5184
	call	_ZdlPv@PLT
.L5184:
	leaq	.LC54(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl23stopTrackingHeapObjectsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5185
	call	_ZdlPv@PLT
.L5185:
	leaq	.LC55(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol12HeapProfiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl16takeHeapSnapshotEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5186
	call	_ZdlPv@PLT
.L5186:
	leaq	128(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINS_8String16ES3_St4hashIS3_ESt8equal_toIS3_ESaISt4pairIKS3_S3_EEE@PLT
	leaq	.LC56(%rip), %rsi
	movq	%r12, %rdi
	movq	%rbx, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	leaq	-104(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol14UberDispatcher15registerBackendERKNS_8String16ESt10unique_ptrINS0_14DispatcherBaseESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5187
	call	_ZdlPv@PLT
.L5187:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L5174
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5189
	movq	144(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol12HeapProfiler14DispatcherImplE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L5194
	.p2align 4,,10
	.p2align 3
.L5190:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L5195
	call	_ZdlPv@PLT
.L5195:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L5199
	.p2align 4,,10
	.p2align 3
.L5196:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L5200
	call	_ZdlPv@PLT
.L5200:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev@PLT
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5174:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5224
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5225:
	.cfi_restore_state
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L5190
.L5193:
	movq	%rbx, %r13
.L5194:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L5191
	call	_ZdlPv@PLT
.L5191:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L5225
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L5193
	jmp	.L5190
	.p2align 4,,10
	.p2align 3
.L5226:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L5196
.L5198:
	movq	%rbx, %r13
.L5199:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L5226
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L5198
	jmp	.L5196
	.p2align 4,,10
	.p2align 3
.L5189:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L5174
.L5224:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5869:
	.size	_ZN12v8_inspector8protocol12HeapProfiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE, .-_ZN12v8_inspector8protocol12HeapProfiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE
	.section	.text._ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl11canDispatchERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl11canDispatchERKNS_8String16E
	.type	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl11canDispatchERKNS_8String16E, @function
_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl11canDispatchERKNS_8String16E:
.LFB5822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32(%rsi), %rcx
	movq	%rsi, %r8
	leaq	72(%rdi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rcx, %rcx
	jne	.L5228
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L5231
	.p2align 4,,10
	.p2align 3
.L5230:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%r8)
	cmpq	%rax, %rsi
	jne	.L5230
	testq	%rcx, %rcx
	je	.L5231
.L5228:
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	80(%rdi)
	movq	%r9, %rdi
	movq	%rdx, %rsi
	movq	%r8, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol12HeapProfiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L5227
	cmpq	$0, (%rax)
	setne	%r8b
.L5227:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5231:
	.cfi_restore_state
	movq	$1, 32(%r8)
	movl	$1, %ecx
	jmp	.L5228
	.cfi_endproc
.LFE5822:
	.size	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl11canDispatchERKNS_8String16E, .-_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl11canDispatchERKNS_8String16E
	.weak	_ZTVN12v8_inspector8protocol23InternalRawNotificationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol23InternalRawNotificationE,"awG",@progbits,_ZTVN12v8_inspector8protocol23InternalRawNotificationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol23InternalRawNotificationE, @object
	.size	_ZTVN12v8_inspector8protocol23InternalRawNotificationE, 48
_ZTVN12v8_inspector8protocol23InternalRawNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol23InternalRawNotificationD1Ev
	.quad	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime9CallFrameE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime9CallFrameE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, 48
_ZTVN12v8_inspector8protocol7Runtime9CallFrameE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.weak	_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE,"awG",@progbits,_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE, @object
	.size	_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE, 48
_ZTVN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNode17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD1Ev
	.quad	_ZN12v8_inspector8protocol12HeapProfiler23SamplingHeapProfileNodeD0Ev
	.weak	_ZTVN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleE,"awG",@progbits,_ZTVN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleE, @object
	.size	_ZTVN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleE, 48
_ZTVN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSample17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD1Ev
	.quad	_ZN12v8_inspector8protocol12HeapProfiler25SamplingHeapProfileSampleD0Ev
	.weak	_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE,"awG",@progbits,_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE, @object
	.size	_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE, 48
_ZTVN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfile17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD1Ev
	.quad	_ZN12v8_inspector8protocol12HeapProfiler19SamplingHeapProfileD0Ev
	.weak	_ZTVN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationE,"awG",@progbits,_ZTVN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationE, @object
	.size	_ZTVN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationE, 48
_ZTVN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotification17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD1Ev
	.quad	_ZN12v8_inspector8protocol12HeapProfiler32AddHeapSnapshotChunkNotificationD0Ev
	.weak	_ZTVN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationE,"awG",@progbits,_ZTVN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationE, @object
	.size	_ZTVN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationE, 48
_ZTVN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotification17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD1Ev
	.quad	_ZN12v8_inspector8protocol12HeapProfiler27HeapStatsUpdateNotificationD0Ev
	.weak	_ZTVN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationE,"awG",@progbits,_ZTVN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationE, @object
	.size	_ZTVN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationE, 48
_ZTVN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotification17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD1Ev
	.quad	_ZN12v8_inspector8protocol12HeapProfiler28LastSeenObjectIdNotificationD0Ev
	.weak	_ZTVN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationE,"awG",@progbits,_ZTVN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationE, @object
	.size	_ZTVN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationE, 48
_ZTVN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotification17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD1Ev
	.quad	_ZN12v8_inspector8protocol12HeapProfiler38ReportHeapSnapshotProgressNotificationD0Ev
	.weak	_ZTVN12v8_inspector8protocol12HeapProfiler14DispatcherImplE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol12HeapProfiler14DispatcherImplE,"awG",@progbits,_ZTVN12v8_inspector8protocol12HeapProfiler14DispatcherImplE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol12HeapProfiler14DispatcherImplE, @object
	.size	_ZTVN12v8_inspector8protocol12HeapProfiler14DispatcherImplE, 48
_ZTVN12v8_inspector8protocol12HeapProfiler14DispatcherImplE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD1Ev
	.quad	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImplD0Ev
	.quad	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl11canDispatchERKNS_8String16E
	.quad	_ZN12v8_inspector8protocol12HeapProfiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.globl	_ZN12v8_inspector8protocol12HeapProfiler8Metainfo7versionE
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler8Metainfo7versionE,"a"
	.type	_ZN12v8_inspector8protocol12HeapProfiler8Metainfo7versionE, @object
	.size	_ZN12v8_inspector8protocol12HeapProfiler8Metainfo7versionE, 4
_ZN12v8_inspector8protocol12HeapProfiler8Metainfo7versionE:
	.string	"1.3"
	.globl	_ZN12v8_inspector8protocol12HeapProfiler8Metainfo13commandPrefixE
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler8Metainfo13commandPrefixE,"a"
	.align 8
	.type	_ZN12v8_inspector8protocol12HeapProfiler8Metainfo13commandPrefixE, @object
	.size	_ZN12v8_inspector8protocol12HeapProfiler8Metainfo13commandPrefixE, 14
_ZN12v8_inspector8protocol12HeapProfiler8Metainfo13commandPrefixE:
	.string	"HeapProfiler."
	.globl	_ZN12v8_inspector8protocol12HeapProfiler8Metainfo10domainNameE
	.section	.rodata._ZN12v8_inspector8protocol12HeapProfiler8Metainfo10domainNameE,"a"
	.align 8
	.type	_ZN12v8_inspector8protocol12HeapProfiler8Metainfo10domainNameE, @object
	.size	_ZN12v8_inspector8protocol12HeapProfiler8Metainfo10domainNameE, 13
_ZN12v8_inspector8protocol12HeapProfiler8Metainfo10domainNameE:
	.string	"HeapProfiler"
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC43:
	.long	1065353216
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
