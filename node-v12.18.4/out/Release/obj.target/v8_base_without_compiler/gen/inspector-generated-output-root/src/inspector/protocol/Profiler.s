	.file	"Profiler.cpp"
	.text
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv, @function
_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv:
.LFB4422:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movq	8(%rsi), %rcx
	leaq	24(%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L5
	movq	%rcx, (%rdi)
	movq	24(%rsi), %rcx
	movq	%rcx, 16(%rdi)
.L3:
	movq	16(%rsi), %rcx
	movq	%rdx, 8(%rsi)
	xorl	%edx, %edx
	movq	$0, 16(%rsi)
	movq	%rcx, 8(%rax)
	movw	%dx, 24(%rsi)
	movq	40(%rsi), %rdx
	movq	%rdx, 32(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movdqu	24(%rsi), %xmm0
	movups	%xmm0, 16(%rdi)
	jmp	.L3
	.cfi_endproc
.LFE4422:
	.size	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv, .-_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv:
.LFB4423:
	.cfi_startproc
	endbr64
	movdqu	48(%rsi), %xmm1
	movq	64(%rsi), %rdx
	pxor	%xmm0, %xmm0
	movq	%rdi, %rax
	movq	$0, 64(%rsi)
	movq	%rdx, 16(%rdi)
	movups	%xmm1, (%rdi)
	movups	%xmm0, 48(%rsi)
	ret
	.cfi_endproc
.LFE4423:
	.size	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv, .-_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler16PositionTickInfoD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD2Ev, @function
_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD2Ev:
.LFB6187:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6187:
	.size	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD2Ev, .-_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD1Ev,_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler13CoverageRangeD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler13CoverageRangeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD2Ev, @function
_ZN12v8_inspector8protocol8Profiler13CoverageRangeD2Ev:
.LFB6215:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6215:
	.size	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD2Ev, .-_ZN12v8_inspector8protocol8Profiler13CoverageRangeD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD1Ev,_ZN12v8_inspector8protocol8Profiler13CoverageRangeD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev, @function
_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev:
.LFB6189:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6189:
	.size	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev, .-_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler13CoverageRangeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev, @function
_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev:
.LFB6217:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6217:
	.size	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev, .-_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv:
.LFB5133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L11
	movq	(%rdi), %rax
	call	*24(%rax)
.L11:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L18
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L18:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5133:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv:
.LFB5132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L19
	movq	(%rdi), %rax
	call	*24(%rax)
.L19:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L26:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5132:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev:
.LFB5119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L27
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5119:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev,_ZN12v8_inspector8protocol7Runtime9CallFrameD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler10TypeObjectD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler10TypeObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler10TypeObjectD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler10TypeObjectD2Ev, @function
_ZN12v8_inspector8protocol8Profiler10TypeObjectD2Ev:
.LFB6314:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	cmpq	%rdi, %r8
	je	.L32
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	ret
	.cfi_endproc
.LFE6314:
	.size	_ZN12v8_inspector8protocol8Profiler10TypeObjectD2Ev, .-_ZN12v8_inspector8protocol8Profiler10TypeObjectD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler10TypeObjectD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler10TypeObjectD1Ev,_ZN12v8_inspector8protocol8Profiler10TypeObjectD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler10TypeObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev, @function
_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev:
.LFB6316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6316:
	.size	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev, .-_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger8LocationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger8LocationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger8LocationD2Ev
	.type	_ZN12v8_inspector8protocol8Debugger8LocationD2Ev, @function
_ZN12v8_inspector8protocol8Debugger8LocationD2Ev:
.LFB5511:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	cmpq	%rdi, %r8
	je	.L37
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	ret
	.cfi_endproc
.LFE5511:
	.size	_ZN12v8_inspector8protocol8Debugger8LocationD2Ev, .-_ZN12v8_inspector8protocol8Debugger8LocationD2Ev
	.weak	_ZN12v8_inspector8protocol8Debugger8LocationD1Ev
	.set	_ZN12v8_inspector8protocol8Debugger8LocationD1Ev,_ZN12v8_inspector8protocol8Debugger8LocationD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger8LocationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger8LocationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev
	.type	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev, @function
_ZN12v8_inspector8protocol8Debugger8LocationD0Ev:
.LFB5513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L40
	call	_ZdlPv@PLT
.L40:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5513:
	.size	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev, .-_ZN12v8_inspector8protocol8Debugger8LocationD0Ev
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotificationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev
	.type	_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev, @function
_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev:
.LFB4419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
.L43:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L42
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4419:
	.size	_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev, .-_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev
	.weak	_ZN12v8_inspector8protocol23InternalRawNotificationD1Ev
	.set	_ZN12v8_inspector8protocol23InternalRawNotificationD1Ev,_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev
	.section	.text._ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv:
.LFB5522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L49
	movq	(%rdi), %rax
	call	*24(%rax)
.L49:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L56:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5522:
	.size	_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv:
.LFB5521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L57
	movq	(%rdi), %rax
	call	*24(%rax)
.L57:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L64:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5521:
	.size	_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImplD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler14DispatcherImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler14DispatcherImplD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImplD2Ev, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImplD2Ev:
.LFB6572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler14DispatcherImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L70
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L96:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L66
.L69:
	movq	%rbx, %r13
.L70:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L96
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L69
.L66:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L75
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L97:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L72
.L74:
	movq	%rbx, %r13
.L75:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L97
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L74
.L72:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev@PLT
	.cfi_endproc
.LFE6572:
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImplD2Ev, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImplD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler14DispatcherImplD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler14DispatcherImplD1Ev,_ZN12v8_inspector8protocol8Profiler14DispatcherImplD2Ev
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotificationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev
	.type	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev, @function
_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev:
.LFB4421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L99
	call	_ZdlPv@PLT
.L99:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L100
	call	_ZdlPv@PLT
.L100:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4421:
	.size	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev, .-_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev, @function
_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev:
.LFB6250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	48(%rdi), %r15
	movq	%rax, (%rdi)
	testq	%r15, %r15
	je	.L106
	movq	8(%r15), %rbx
	movq	(%r15), %r13
	cmpq	%r13, %rbx
	je	.L107
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %r14
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L125:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L108:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L124
.L110:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L108
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L125
	call	*%rax
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L110
	.p2align 4,,10
	.p2align 3
.L124:
	movq	(%r15), %r13
.L107:
	testq	%r13, %r13
	je	.L111
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L111:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L106:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L112
	call	_ZdlPv@PLT
.L112:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6250:
	.size	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev, .-_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler16FunctionCoverageD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD2Ev, @function
_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD2Ev:
.LFB6248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %r15
	movq	%rax, (%rdi)
	testq	%r15, %r15
	je	.L127
	movq	8(%r15), %r13
	movq	(%r15), %r12
	cmpq	%r12, %r13
	je	.L128
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %r14
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L146:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L129:
	addq	$8, %r12
	cmpq	%r12, %r13
	je	.L145
.L131:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L129
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L146
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L131
	.p2align 4,,10
	.p2align 3
.L145:
	movq	(%r15), %r12
.L128:
	testq	%r12, %r12
	je	.L132
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L132:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L127:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L126
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6248:
	.size	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD2Ev, .-_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD1Ev,_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime9CallFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, @function
_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev:
.LFB5121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L148
	call	_ZdlPv@PLT
.L148:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L149
	call	_ZdlPv@PLT
.L149:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L150
	call	_ZdlPv@PLT
.L150:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5121:
	.size	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev, .-_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.section	.text._ZN12v8_inspector8protocol16InternalResponseD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16InternalResponseD0Ev
	.type	_ZN12v8_inspector8protocol16InternalResponseD0Ev, @function
_ZN12v8_inspector8protocol16InternalResponseD0Ev:
.LFB4412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L153
	movq	(%rdi), %rax
	call	*24(%rax)
.L153:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L154
	call	_ZdlPv@PLT
.L154:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4412:
	.size	_ZN12v8_inspector8protocol16InternalResponseD0Ev, .-_ZN12v8_inspector8protocol16InternalResponseD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImplD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler14DispatcherImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler14DispatcherImplD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImplD0Ev, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImplD0Ev:
.LFB6574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler14DispatcherImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L164
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L190:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L160
.L163:
	movq	%rbx, %r13
.L164:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L161
	call	_ZdlPv@PLT
.L161:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L190
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L163
.L160:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L165
	call	_ZdlPv@PLT
.L165:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L169
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L191:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L166
.L168:
	movq	%rbx, %r13
.L169:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L191
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L168
.L166:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L170
	call	_ZdlPv@PLT
.L170:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6574:
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImplD0Ev, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImplD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD2Ev, @function
_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD2Ev:
.LFB6341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L192
	movq	8(%r13), %rbx
	movq	0(%r13), %r15
	cmpq	%r15, %rbx
	je	.L194
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %r14
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L209:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r14, (%r12)
	cmpq	%rax, %rdi
	je	.L197
	call	_ZdlPv@PLT
.L197:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L195:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L208
.L198:
	movq	(%r15), %r12
	testq	%r12, %r12
	je	.L195
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L209
	addq	$8, %r15
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r15, %rbx
	jne	.L198
	.p2align 4,,10
	.p2align 3
.L208:
	movq	0(%r13), %r15
.L194:
	testq	%r15, %r15
	je	.L199
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L199:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6341:
	.size	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD2Ev, .-_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD1Ev,_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev, @function
_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev:
.LFB6343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L211
	movq	8(%r13), %rbx
	movq	0(%r13), %r15
	cmpq	%r15, %rbx
	jne	.L216
	testq	%r15, %r15
	je	.L217
	.p2align 4,,10
	.p2align 3
.L231:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L217:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L211:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r14), %rdi
	movq	%rax, (%r14)
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L215
	call	_ZdlPv@PLT
.L215:
	movl	$48, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L213:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L229
.L216:
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.L213
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L230
	addq	$8, %r15
	movq	%r14, %rdi
	call	*%rax
	cmpq	%r15, %rbx
	jne	.L216
	.p2align 4,,10
	.p2align 3
.L229:
	movq	0(%r13), %r15
	testq	%r15, %r15
	jne	.L231
	jmp	.L217
	.cfi_endproc
.LFE6343:
	.size	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev, .-_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev, @function
_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev:
.LFB6374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movq	88(%rdi), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L233
	movq	8(%rax), %rdx
	movq	(%rax), %rbx
	movq	%rdx, -72(%rbp)
	cmpq	%rbx, %rdx
	je	.L234
	movq	%rdi, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L244:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L235
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L236
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L237
	movq	8(%r13), %r14
	movq	0(%r13), %r15
	cmpq	%r15, %r14
	je	.L238
	movq	%r14, -64(%rbp)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L270:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r14), %rdi
	movq	%rax, (%r14)
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L241
	call	_ZdlPv@PLT
.L241:
	movl	$48, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L239:
	addq	$8, %r15
	cmpq	%r15, -64(%rbp)
	je	.L269
.L242:
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.L239
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L270
	movq	%r14, %rdi
	addq	$8, %r15
	call	*%rax
	cmpq	%r15, -64(%rbp)
	jne	.L242
	.p2align 4,,10
	.p2align 3
.L269:
	movq	0(%r13), %r15
.L238:
	testq	%r15, %r15
	je	.L243
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L243:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L237:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L235:
	addq	$8, %rbx
	cmpq	%rbx, -72(%rbp)
	jne	.L244
.L271:
	movq	-56(%rbp), %rax
	movq	-80(%rbp), %r12
	movq	(%rax), %rbx
.L234:
	testq	%rbx, %rbx
	je	.L245
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L245:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L233:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L246
	call	_ZdlPv@PLT
.L246:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L247
	call	_ZdlPv@PLT
.L247:
	addq	$40, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movq	%r12, %rdi
	addq	$8, %rbx
	call	*%rax
	cmpq	%rbx, -72(%rbp)
	jne	.L244
	jmp	.L271
	.cfi_endproc
.LFE6374:
	.size	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev, .-_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev, @function
_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev:
.LFB6284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	88(%rdi), %r14
	movq	%rax, (%rdi)
	testq	%r14, %r14
	je	.L273
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -72(%rbp)
	cmpq	%r13, %rax
	je	.L274
	.p2align 4,,10
	.p2align 3
.L284:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L275
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L276
	movq	48(%r15), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rbx, %rbx
	je	.L277
	movq	8(%rbx), %rax
	movq	(%rbx), %r8
	movq	%rax, -56(%rbp)
	cmpq	%r8, %rax
	jne	.L281
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L310:
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L279:
	addq	$8, %r8
	cmpq	%r8, -56(%rbp)
	je	.L309
.L281:
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L279
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rcx
	movq	%r8, -64(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L310
	call	*%rax
	movq	-64(%rbp), %r8
	addq	$8, %r8
	cmpq	%r8, -56(%rbp)
	jne	.L281
	.p2align 4,,10
	.p2align 3
.L309:
	movq	(%rbx), %r8
.L278:
	testq	%r8, %r8
	je	.L282
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L282:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L277:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L283
	call	_ZdlPv@PLT
.L283:
	movl	$64, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L275:
	addq	$8, %r13
	cmpq	%r13, -72(%rbp)
	jne	.L284
.L311:
	movq	(%r14), %r13
.L274:
	testq	%r13, %r13
	je	.L285
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L285:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L273:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L286
	call	_ZdlPv@PLT
.L286:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L287
	call	_ZdlPv@PLT
.L287:
	addq	$40, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	movq	%r15, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -72(%rbp)
	jne	.L284
	jmp	.L311
	.cfi_endproc
.LFE6284:
	.size	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev, .-_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD2Ev, @function
_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD2Ev:
.LFB6372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L313
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	movq	%rdx, -72(%rbp)
	cmpq	%r14, %rdx
	je	.L314
	movq	%rdi, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L324:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L315
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L316
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L317
	movq	8(%r13), %rbx
	movq	0(%r13), %r15
	cmpq	%r15, %rbx
	jne	.L322
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L350:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L321
	movq	%r8, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r8
.L321:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L319:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L349
.L322:
	movq	(%r15), %r8
	testq	%r8, %r8
	je	.L319
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L350
	addq	$8, %r15
	movq	%r8, %rdi
	call	*%rax
	cmpq	%r15, %rbx
	jne	.L322
	.p2align 4,,10
	.p2align 3
.L349:
	movq	0(%r13), %r15
.L318:
	testq	%r15, %r15
	je	.L323
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L323:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L317:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L315:
	addq	$8, %r14
	cmpq	%r14, -72(%rbp)
	jne	.L324
	movq	-56(%rbp), %rax
	movq	-80(%rbp), %rbx
	movq	(%rax), %r14
.L314:
	testq	%r14, %r14
	je	.L325
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L325:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L313:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L326
	call	_ZdlPv@PLT
.L326:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L312
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L312:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6372:
	.size	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD2Ev, .-_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD1Ev,_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler14ScriptCoverageD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD2Ev, @function
_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD2Ev:
.LFB6282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	88(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L352
	movq	8(%r13), %rax
	movq	0(%r13), %r12
	movq	%rax, -72(%rbp)
	cmpq	%r12, %rax
	je	.L353
	.p2align 4,,10
	.p2align 3
.L363:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L354
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L355
	movq	48(%r15), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r14, %r14
	je	.L356
	movq	8(%r14), %rax
	movq	(%r14), %r8
	movq	%rax, -56(%rbp)
	cmpq	%r8, %rax
	jne	.L360
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L389:
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L358:
	addq	$8, %r8
	cmpq	%r8, -56(%rbp)
	je	.L388
.L360:
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L358
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rcx
	movq	%r8, -64(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L389
	call	*%rax
	movq	-64(%rbp), %r8
	addq	$8, %r8
	cmpq	%r8, -56(%rbp)
	jne	.L360
	.p2align 4,,10
	.p2align 3
.L388:
	movq	(%r14), %r8
.L357:
	testq	%r8, %r8
	je	.L361
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L361:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L356:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L362
	call	_ZdlPv@PLT
.L362:
	movl	$64, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L354:
	addq	$8, %r12
	cmpq	%r12, -72(%rbp)
	jne	.L363
.L390:
	movq	0(%r13), %r12
.L353:
	testq	%r12, %r12
	je	.L364
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L364:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L352:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L365
	call	_ZdlPv@PLT
.L365:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L351
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore_state
	movq	%r15, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -72(%rbp)
	jne	.L363
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L351:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6282:
	.size	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD2Ev, .-_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD1Ev,_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD0Ev, @function
_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD0Ev:
.LFB6442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	leaq	80(%r12), %rax
	cmpq	%rax, %rdi
	je	.L392
	call	_ZdlPv@PLT
.L392:
	movq	48(%r12), %r13
	testq	%r13, %r13
	je	.L393
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L394
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L395
	call	_ZdlPv@PLT
.L395:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L393:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L396
	call	_ZdlPv@PLT
.L396:
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L393
	.cfi_endproc
.LFE6442:
	.size	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD0Ev, .-_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD2Ev, @function
_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD2Ev:
.LFB6440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L402
	call	_ZdlPv@PLT
.L402:
	movq	48(%rbx), %r12
	testq	%r12, %r12
	je	.L403
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L404
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L405
	call	_ZdlPv@PLT
.L405:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L403:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L401
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L404:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L403
	.cfi_endproc
.LFE6440:
	.size	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD2Ev, .-_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD1Ev,_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler7ProfileD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler7ProfileD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev, @function
_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev:
.LFB6149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L412
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L413
	call	_ZdlPv@PLT
.L413:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L412:
	movq	32(%r13), %r12
	testq	%r12, %r12
	je	.L414
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L415
	call	_ZdlPv@PLT
.L415:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L414:
	movq	8(%r13), %r15
	testq	%r15, %r15
	je	.L416
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -72(%rbp)
	cmpq	%r14, %rax
	je	.L417
	.p2align 4,,10
	.p2align 3
.L434:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L418
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L419
	movq	88(%r12), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rbx, %rbx
	je	.L420
	movq	8(%rbx), %rax
	movq	(%rbx), %r8
	movq	%rax, -56(%rbp)
	cmpq	%r8, %rax
	jne	.L424
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L479:
	movl	$16, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L422:
	addq	$8, %r8
	cmpq	%r8, -56(%rbp)
	je	.L478
.L424:
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L422
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rdx
	movq	%r8, -64(%rbp)
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L479
	call	*%rax
	movq	-64(%rbp), %r8
	addq	$8, %r8
	cmpq	%r8, -56(%rbp)
	jne	.L424
	.p2align 4,,10
	.p2align 3
.L478:
	movq	(%rbx), %r8
.L421:
	testq	%r8, %r8
	je	.L425
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L425:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L420:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L426
	call	_ZdlPv@PLT
.L426:
	movq	32(%r12), %rbx
	testq	%rbx, %rbx
	je	.L427
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L428
	call	_ZdlPv@PLT
.L428:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L427:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	je	.L429
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L430
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L431
	call	_ZdlPv@PLT
.L431:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L432
	call	_ZdlPv@PLT
.L432:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L433
	call	_ZdlPv@PLT
.L433:
	movl	$136, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L429:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L418:
	addq	$8, %r14
	cmpq	%r14, -72(%rbp)
	jne	.L434
.L480:
	movq	(%r15), %r14
.L417:
	testq	%r14, %r14
	je	.L435
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L435:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L416:
	addq	$40, %rsp
	movq	%r13, %rdi
	movl	$48, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_restore_state
	movq	%r12, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -72(%rbp)
	jne	.L434
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L430:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L429
	.cfi_endproc
.LFE6149:
	.size	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev, .-_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	cmpl	$2, -112(%rbp)
	je	.L494
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L483
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L483:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L484
	call	_ZdlPv@PLT
.L484:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L481
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L481:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L495
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L483
.L495:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6585:
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImpl16startTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl16startTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl16startTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImpl16startTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*64(%rax)
	cmpl	$2, -112(%rbp)
	je	.L509
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L498
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L498:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L499
	call	_ZdlPv@PLT
.L499:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L496
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L496:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L510
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L509:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L498
.L510:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6618:
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl16startTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImpl16startTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImpl15stopTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl15stopTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl15stopTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImpl15stopTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*88(%rax)
	cmpl	$2, -112(%rbp)
	je	.L524
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L513
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L513:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L514
	call	_ZdlPv@PLT
.L514:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L511
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L511:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L525
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L513
.L525:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6646:
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl15stopTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImpl15stopTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImpl19stopPreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19stopPreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19stopPreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19stopPreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*80(%rax)
	cmpl	$2, -112(%rbp)
	je	.L539
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L528
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L528:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L529
	call	_ZdlPv@PLT
.L529:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L526
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L526:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L540
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L528
.L540:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6645:
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19stopPreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19stopPreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*16(%rax)
	cmpl	$2, -112(%rbp)
	je	.L554
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L543
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L543:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L544
	call	_ZdlPv@PLT
.L544:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L541
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L541:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L555
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L543
.L555:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6578:
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImpl5startEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl5startEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl5startEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImpl5startEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*48(%rax)
	cmpl	$2, -112(%rbp)
	je	.L569
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L558
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L558:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L559
	call	_ZdlPv@PLT
.L559:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L556
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L556:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L570
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L558
.L570:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6615:
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl5startEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImpl5startEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Profiler14DispatcherImpl20startPreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"params"
.LC1:
	.string	"callCount"
.LC2:
	.string	"boolean value expected"
.LC3:
	.string	"detailed"
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImpl20startPreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl20startPreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl20startPreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImpl20startPreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movl	%esi, -132(%rbp)
	movq	(%r8), %r13
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -152(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	testq	%rax, %rax
	je	.L572
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	jne	.L573
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L607
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L576:
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r15, %rdi
	je	.L578
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L578:
	testq	%r8, %r8
	movq	%r8, -144(%rbp)
	je	.L595
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-144(%rbp), %r8
	movb	$0, -120(%rbp)
	leaq	-120(%rbp), %rsi
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*32(%rax)
	testb	%al, %al
	jne	.L580
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L580:
	movzbl	-120(%rbp), %eax
	movb	$1, -133(%rbp)
	movb	%al, -144(%rbp)
.L579:
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%r15, %rdi
	je	.L589
	call	_ZdlPv@PLT
.L589:
	testq	%r13, %r13
	je	.L596
	leaq	.LC3(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movb	$0, -120(%rbp)
	movq	0(%r13), %rax
	movq	%r13, %rdi
	leaq	-120(%rbp), %rsi
	call	*32(%rax)
	testb	%al, %al
	jne	.L581
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L581:
	movzbl	-120(%rbp), %eax
	movl	$1, %r13d
	movb	%al, -134(%rbp)
.L577:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L608
	leaq	-120(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r14), %rsi
	movq	%r12, %rdi
	movzbl	-134(%rbp), %ecx
	movzbl	-133(%rbp), %edx
	movzbl	-144(%rbp), %ebx
	movq	(%rsi), %rax
	movb	%cl, -121(%rbp)
	leaq	-122(%rbp), %rcx
	movb	%dl, -124(%rbp)
	leaq	-124(%rbp), %rdx
	movb	%r13b, -122(%rbp)
	movb	%bl, -123(%rbp)
	call	*56(%rax)
	cmpl	$2, -112(%rbp)
	je	.L609
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L586
	movl	-132(%rbp), %esi
	movq	%r12, %rdx
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L586:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L587
	call	_ZdlPv@PLT
.L587:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L571
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L571:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L610
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L572:
	movq	-112(%rbp), %rdi
.L573:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L611
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L575:
	movb	$0, -144(%rbp)
	xorl	%r13d, %r13d
	movb	$0, -133(%rbp)
	movb	$0, -134(%rbp)
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L608:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	-132(%rbp), %esi
	movl	$-32602, %edx
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L571
	call	_ZdlPv@PLT
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L611:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L609:
	movq	8(%r14), %rdi
	movq	-160(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movl	-132(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L596:
	movb	$0, -134(%rbp)
	xorl	%r13d, %r13d
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L595:
	movb	$0, -144(%rbp)
	movb	$0, -133(%rbp)
	jmp	.L579
.L610:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6616:
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl20startPreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImpl20startPreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler11ProfileNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev, @function
_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev:
.LFB6044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	88(%rdi), %r15
	movq	%rax, (%rdi)
	testq	%r15, %r15
	je	.L613
	movq	8(%r15), %rbx
	movq	(%r15), %r13
	cmpq	%r13, %rbx
	je	.L614
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %r14
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L648:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L615:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L647
.L617:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L615
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L648
	call	*%rax
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L617
	.p2align 4,,10
	.p2align 3
.L647:
	movq	(%r15), %r13
.L614:
	testq	%r13, %r13
	je	.L618
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L618:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L613:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L619
	call	_ZdlPv@PLT
.L619:
	movq	32(%r12), %r13
	testq	%r13, %r13
	je	.L620
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L621
	call	_ZdlPv@PLT
.L621:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L620:
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L622
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L623
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	104(%r13), %rax
	cmpq	%rax, %rdi
	je	.L624
	call	_ZdlPv@PLT
.L624:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L625
	call	_ZdlPv@PLT
.L625:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L626
	call	_ZdlPv@PLT
.L626:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L622:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L623:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L622
	.cfi_endproc
.LFE6044:
	.size	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev, .-_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler7ProfileD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler7ProfileD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler7ProfileD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler7ProfileD2Ev, @function
_ZN12v8_inspector8protocol8Profiler7ProfileD2Ev:
.LFB6147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	40(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L650
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L651
	call	_ZdlPv@PLT
.L651:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L650:
	movq	32(%rbx), %r12
	testq	%r12, %r12
	je	.L652
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L653
	call	_ZdlPv@PLT
.L653:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L652:
	movq	8(%rbx), %r14
	testq	%r14, %r14
	je	.L649
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -64(%rbp)
	cmpq	%r13, %rax
	je	.L655
	.p2align 4,,10
	.p2align 3
.L672:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L656
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L657
	movq	88(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L658
	movq	8(%r15), %rax
	movq	(%r15), %rbx
	movq	%rax, -56(%rbp)
	cmpq	%rbx, %rax
	jne	.L662
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L714:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L660:
	addq	$8, %rbx
	cmpq	%rbx, -56(%rbp)
	je	.L713
.L662:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L660
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L714
	call	*%rax
	addq	$8, %rbx
	cmpq	%rbx, -56(%rbp)
	jne	.L662
	.p2align 4,,10
	.p2align 3
.L713:
	movq	(%r15), %rbx
.L659:
	testq	%rbx, %rbx
	je	.L663
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L663:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L658:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L664
	call	_ZdlPv@PLT
.L664:
	movq	32(%r12), %rbx
	testq	%rbx, %rbx
	je	.L665
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L666
	call	_ZdlPv@PLT
.L666:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L665:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	je	.L667
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L668
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L669
	call	_ZdlPv@PLT
.L669:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L670
	call	_ZdlPv@PLT
.L670:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L671
	call	_ZdlPv@PLT
.L671:
	movl	$136, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L667:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L656:
	addq	$8, %r13
	cmpq	%r13, -64(%rbp)
	jne	.L672
	movq	(%r14), %r13
.L655:
	testq	%r13, %r13
	je	.L673
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L673:
	addq	$24, %rsp
	movq	%r14, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L657:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L649:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L668:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L667
	.cfi_endproc
.LFE6147:
	.size	_ZN12v8_inspector8protocol8Profiler7ProfileD2Ev, .-_ZN12v8_inspector8protocol8Profiler7ProfileD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler7ProfileD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler7ProfileD1Ev,_ZN12v8_inspector8protocol8Profiler7ProfileD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler11ProfileNodeD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler11ProfileNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD2Ev, @function
_ZN12v8_inspector8protocol8Profiler11ProfileNodeD2Ev:
.LFB6042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	88(%rdi), %r15
	movq	%rax, (%rdi)
	testq	%r15, %r15
	je	.L716
	movq	8(%r15), %r13
	movq	(%r15), %r12
	cmpq	%r12, %r13
	je	.L717
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %r14
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L748:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L718:
	addq	$8, %r12
	cmpq	%r12, %r13
	je	.L747
.L720:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L718
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L748
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L720
	.p2align 4,,10
	.p2align 3
.L747:
	movq	(%r15), %r12
.L717:
	testq	%r12, %r12
	je	.L721
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L721:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L716:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L722
	call	_ZdlPv@PLT
.L722:
	movq	32(%rbx), %r12
	testq	%r12, %r12
	je	.L723
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L724
	call	_ZdlPv@PLT
.L724:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L723:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L715
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L726
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L727
	call	_ZdlPv@PLT
.L727:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L728
	call	_ZdlPv@PLT
.L728:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L729
	call	_ZdlPv@PLT
.L729:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L726:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE6042:
	.size	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD2Ev, .-_ZN12v8_inspector8protocol8Profiler11ProfileNodeD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD1Ev,_ZN12v8_inspector8protocol8Profiler11ProfileNodeD2Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD0Ev
	.type	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD0Ev, @function
_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD0Ev:
.LFB6406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L750
	call	_ZdlPv@PLT
.L750:
	movq	56(%r12), %r14
	testq	%r14, %r14
	je	.L751
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L752
	movq	40(%r14), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r13, %r13
	je	.L753
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L754
	call	_ZdlPv@PLT
.L754:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L753:
	movq	32(%r14), %r13
	testq	%r13, %r13
	je	.L755
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L756
	call	_ZdlPv@PLT
.L756:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L755:
	movq	8(%r14), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L757
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	%rcx, -72(%rbp)
	cmpq	%r15, %rcx
	je	.L758
	.p2align 4,,10
	.p2align 3
.L775:
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.L759
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L760
	movq	88(%r13), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rbx, %rbx
	je	.L761
	movq	8(%rbx), %rax
	movq	(%rbx), %r8
	movq	%rax, -64(%rbp)
	cmpq	%r8, %rax
	jne	.L765
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L830:
	movl	$16, %esi
	call	_ZdlPvm@PLT
	movq	-56(%rbp), %r8
.L763:
	addq	$8, %r8
	cmpq	%r8, -64(%rbp)
	je	.L829
.L765:
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L763
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	%r8, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L830
	call	*%rax
	movq	-56(%rbp), %r8
	addq	$8, %r8
	cmpq	%r8, -64(%rbp)
	jne	.L765
	.p2align 4,,10
	.p2align 3
.L829:
	movq	(%rbx), %r8
.L762:
	testq	%r8, %r8
	je	.L766
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L766:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L761:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L767
	call	_ZdlPv@PLT
.L767:
	movq	32(%r13), %r8
	testq	%r8, %r8
	je	.L768
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L769
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L769:
	movl	$24, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L768:
	movq	16(%r13), %r8
	testq	%r8, %r8
	je	.L770
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L771
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r8), %rdi
	movq	%rax, (%r8)
	leaq	104(%r8), %rax
	cmpq	%rax, %rdi
	je	.L772
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L772:
	movq	48(%r8), %rdi
	leaq	64(%r8), %rax
	cmpq	%rax, %rdi
	je	.L773
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L773:
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L774
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L774:
	movl	$136, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L770:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L759:
	addq	$8, %r15
	cmpq	%r15, -72(%rbp)
	jne	.L775
.L831:
	movq	-80(%rbp), %rax
	movq	(%rax), %r15
.L758:
	testq	%r15, %r15
	je	.L776
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L776:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L757:
	movl	$48, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L751:
	movq	48(%r12), %r13
	testq	%r13, %r13
	je	.L777
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L778
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L779
	call	_ZdlPv@PLT
.L779:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L777:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L780
	call	_ZdlPv@PLT
.L780:
	addq	$40, %rsp
	movq	%r12, %rdi
	movl	$112, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L760:
	.cfi_restore_state
	movq	%r13, %rdi
	addq	$8, %r15
	call	*%rax
	cmpq	%r15, -72(%rbp)
	jne	.L775
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L771:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L752:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L778:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L777
	.cfi_endproc
.LFE6406:
	.size	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD0Ev, .-_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD0Ev
	.section	.text._ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD2Ev
	.type	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD2Ev, @function
_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD2Ev:
.LFB6404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L833
	call	_ZdlPv@PLT
.L833:
	movq	56(%rbx), %r13
	testq	%r13, %r13
	je	.L834
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L835
	movq	40(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L836
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L837
	call	_ZdlPv@PLT
.L837:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L836:
	movq	32(%r13), %r12
	testq	%r12, %r12
	je	.L838
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L839
	call	_ZdlPv@PLT
.L839:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L838:
	movq	8(%r13), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L840
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -72(%rbp)
	cmpq	%r14, %rcx
	je	.L841
	.p2align 4,,10
	.p2align 3
.L858:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L842
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L843
	movq	88(%r12), %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r15, %r15
	je	.L844
	movq	8(%r15), %rax
	movq	(%r15), %r8
	movq	%rax, -64(%rbp)
	cmpq	%r8, %rax
	jne	.L848
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L913:
	movl	$16, %esi
	call	_ZdlPvm@PLT
	movq	-56(%rbp), %r8
.L846:
	addq	$8, %r8
	cmpq	%r8, -64(%rbp)
	je	.L912
.L848:
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L846
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	%r8, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L913
	call	*%rax
	movq	-56(%rbp), %r8
	addq	$8, %r8
	cmpq	%r8, -64(%rbp)
	jne	.L848
	.p2align 4,,10
	.p2align 3
.L912:
	movq	(%r15), %r8
.L845:
	testq	%r8, %r8
	je	.L849
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L849:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L844:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L850
	call	_ZdlPv@PLT
.L850:
	movq	32(%r12), %r8
	testq	%r8, %r8
	je	.L851
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L852
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L852:
	movl	$24, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L851:
	movq	16(%r12), %r8
	testq	%r8, %r8
	je	.L853
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L854
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r8), %rdi
	movq	%rax, (%r8)
	leaq	104(%r8), %rax
	cmpq	%rax, %rdi
	je	.L855
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L855:
	movq	48(%r8), %rdi
	leaq	64(%r8), %rax
	cmpq	%rax, %rdi
	je	.L856
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L856:
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L857
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L857:
	movl	$136, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L853:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L842:
	addq	$8, %r14
	cmpq	%r14, -72(%rbp)
	jne	.L858
	movq	-80(%rbp), %rax
	movq	(%rax), %r14
.L841:
	testq	%r14, %r14
	je	.L859
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L859:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L840:
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L834:
	movq	48(%rbx), %r12
	testq	%r12, %r12
	je	.L860
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L861
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L862
	call	_ZdlPv@PLT
.L862:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L860:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L832
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L832:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L843:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L854:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L861:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L835:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L834
	.cfi_endproc
.LFE6404:
	.size	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD2Ev, .-_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD2Ev
	.weak	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD1Ev
	.set	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD1Ev,_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD2Ev
	.section	.rodata._ZN12v8_inspector8protocol8Profiler14DispatcherImpl19setSamplingIntervalEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"interval"
.LC5:
	.string	"integer value expected"
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImpl19setSamplingIntervalEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19setSamplingIntervalEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19setSamplingIntervalEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19setSamplingIntervalEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	%rdx, -144(%rbp)
	movq	(%r8), %r15
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	testq	%rax, %rax
	je	.L916
	cmpl	$6, 8(%rax)
	jne	.L916
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L946
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-136(%rbp), %rax
.L919:
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r15, %rdi
	je	.L921
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L921:
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r8, -136(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %r8
	movl	$0, -120(%rbp)
	testq	%r8, %r8
	je	.L920
	movq	(%r8), %rax
	leaq	-120(%rbp), %rsi
	movq	%r8, %rdi
	call	*48(%rax)
	testb	%al, %al
	jne	.L923
.L920:
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L923:
	movl	-120(%rbp), %eax
	movq	%rbx, %rdi
	movl	%eax, -136(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L947
	leaq	-120(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	movl	-136(%rbp), %edx
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*40(%rax)
	cmpl	$2, -112(%rbp)
	je	.L948
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L929
	movq	%r12, %rdx
	movl	%r14d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseE@PLT
.L929:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L930
	call	_ZdlPv@PLT
.L930:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L914
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L914:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L949
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L946:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-136(%rbp), %rax
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L916:
	leaq	-96(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L950
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
.L918:
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movl	$0, -120(%rbp)
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L947:
	leaq	_ZN12v8_inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r12, %rcx
	movl	$-32602, %edx
	movl	%r14d, %esi
	call	_ZN12v8_inspector8protocol14DispatcherBase19reportProtocolErrorEiNS0_16DispatchResponse9ErrorCodeERKNS_8String16EPNS0_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L914
	call	_ZdlPv@PLT
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L950:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L948:
	movq	8(%r13), %rdi
	movq	-152(%rbp), %rcx
	movl	%r14d, %esi
	movq	-144(%rbp), %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L929
.L949:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6614:
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19setSamplingIntervalEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19setSamplingIntervalEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol16InternalResponseD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16InternalResponseD2Ev
	.type	_ZN12v8_inspector8protocol16InternalResponseD2Ev, @function
_ZN12v8_inspector8protocol16InternalResponseD2Ev:
.LFB4410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L952
	movq	(%rdi), %rax
	call	*24(%rax)
.L952:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L951
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L951:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4410:
	.size	_ZN12v8_inspector8protocol16InternalResponseD2Ev, .-_ZN12v8_inspector8protocol16InternalResponseD2Ev
	.weak	_ZN12v8_inspector8protocol16InternalResponseD1Ev
	.set	_ZN12v8_inspector8protocol16InternalResponseD1Ev,_ZN12v8_inspector8protocol16InternalResponseD2Ev
	.section	.rodata._ZN12v8_inspector8protocol8Profiler16PositionTickInfo9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"object expected"
.LC7:
	.string	"line"
.LC8:
	.string	"ticks"
	.section	.text._ZN12v8_inspector8protocol8Profiler16PositionTickInfo9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler16PositionTickInfo9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L959
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r12
	je	.L960
.L959:
	leaq	.LC6(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, 0(%r13)
.L958:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L985
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L960:
	.cfi_restore_state
	movl	$16, %edi
	leaq	-96(%rbp), %r15
	call	_Znwm@PLT
	cmpl	$6, 8(%r12)
	movq	%rbx, %rdi
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16PositionTickInfoE(%rip), %rax
	movq	%rax, (%r14)
	movl	$0, %eax
	cmovne	%rax, %r12
	movq	$0, 8(%r14)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC7(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r9
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L963
	movq	%r9, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r9
.L963:
	leaq	.LC7(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r9, -128(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %r9
	movl	$0, -100(%rbp)
	testq	%r9, %r9
	je	.L966
	movq	(%r9), %rax
	leaq	-100(%rbp), %rsi
	movq	%r9, %rdi
	call	*48(%rax)
	testb	%al, %al
	jne	.L965
.L966:
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L965:
	movl	-100(%rbp), %eax
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, 8(%r14)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	cmpq	-120(%rbp), %rdi
	je	.L967
	call	_ZdlPv@PLT
.L967:
	leaq	.LC8(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movl	$0, -100(%rbp)
	testq	%r12, %r12
	je	.L970
	movq	(%r12), %rax
	leaq	-100(%rbp), %rsi
	movq	%r12, %rdi
	call	*48(%rax)
	testb	%al, %al
	jne	.L969
.L970:
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L969:
	movl	-100(%rbp), %eax
	movq	%rbx, %rdi
	movl	%eax, 12(%r14)
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L986
	movq	%r14, 0(%r13)
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L986:
	movq	(%r14), %rax
	movq	$0, 0(%r13)
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L987
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L987:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L958
.L985:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6485:
	.size	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler16PositionTickInfo9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNK12v8_inspector8protocol8Profiler16PositionTickInfo7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler16PositionTickInfo7toValueEv
	.type	_ZNK12v8_inspector8protocol8Profiler16PositionTickInfo7toValueEv, @function
_ZNK12v8_inspector8protocol8Profiler16PositionTickInfo7toValueEv:
.LFB6486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$96, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r15, (%r12)
	movl	$24, %edi
	movl	8(%rbx), %r13d
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	movl	%r13d, 16(%rax)
	leaq	-96(%rbp), %r13
	movl	$2, 8(%rax)
	movq	%r13, %rdi
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, %r15
	cmpq	%rax, %rdi
	je	.L989
	call	_ZdlPv@PLT
.L989:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L990
	movq	(%rdi), %rax
	call	*24(%rax)
.L990:
	movq	(%r12), %r8
	movl	12(%rbx), %ebx
	movl	$24, %edi
	movq	%r8, -120(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	movq	%r13, %rdi
	movq	%rdx, (%rax)
	movl	$2, 8(%rax)
	movl	%ebx, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L991
	call	_ZdlPv@PLT
.L991:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L988
	movq	(%rdi), %rax
	call	*24(%rax)
.L988:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1001
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1001:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6486:
	.size	_ZNK12v8_inspector8protocol8Profiler16PositionTickInfo7toValueEv, .-_ZNK12v8_inspector8protocol8Profiler16PositionTickInfo7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Profiler16PositionTickInfo17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16PositionTickInfo17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler16PositionTickInfo17serializeToBinaryEv:
.LFB6195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler16PositionTickInfo7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1002
	movq	(%rdi), %rax
	call	*24(%rax)
.L1002:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1009
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1009:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6195:
	.size	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler16PositionTickInfo17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler16PositionTickInfo15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16PositionTickInfo15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler16PositionTickInfo15serializeToJSONEv:
.LFB6194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler16PositionTickInfo7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1010
	movq	(%rdi), %rax
	call	*24(%rax)
.L1010:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1017
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1017:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6194:
	.size	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler16PositionTickInfo15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Profiler16PositionTickInfo5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler16PositionTickInfo5cloneEv
	.type	_ZNK12v8_inspector8protocol8Profiler16PositionTickInfo5cloneEv, @function
_ZNK12v8_inspector8protocol8Profiler16PositionTickInfo5cloneEv:
.LFB6487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Profiler16PositionTickInfo7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1019
	movq	(%rdi), %rax
	call	*24(%rax)
.L1019:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1025
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1025:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6487:
	.size	_ZNK12v8_inspector8protocol8Profiler16PositionTickInfo5cloneEv, .-_ZNK12v8_inspector8protocol8Profiler16PositionTickInfo5cloneEv
	.section	.rodata._ZN12v8_inspector8protocol8Profiler13CoverageRange9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"startOffset"
.LC10:
	.string	"endOffset"
.LC11:
	.string	"count"
	.section	.text._ZN12v8_inspector8protocol8Profiler13CoverageRange9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler13CoverageRange9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler13CoverageRange9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler13CoverageRange9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1027
	cmpl	$6, 8(%rsi)
	movq	%rsi, %rbx
	je	.L1028
.L1027:
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, 0(%r13)
.L1026:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1060
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1028:
	.cfi_restore_state
	movl	$24, %edi
	leaq	-96(%rbp), %r15
	call	_Znwm@PLT
	cmpl	$6, 8(%rbx)
	movq	%r12, %rdi
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler13CoverageRangeE(%rip), %rax
	movq	%rax, (%r14)
	movl	$0, %eax
	cmovne	%rax, %rbx
	movq	$0, 8(%r14)
	movl	$0, 16(%r14)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC9(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r9
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L1031
	movq	%r9, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r9
.L1031:
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	movq	%r9, -128(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %r9
	movl	$0, -100(%rbp)
	testq	%r9, %r9
	je	.L1034
	movq	(%r9), %rax
	leaq	-100(%rbp), %rsi
	movq	%r9, %rdi
	call	*48(%rax)
	testb	%al, %al
	jne	.L1033
.L1034:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L1033:
	movl	-100(%rbp), %eax
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	movl	%eax, 8(%r14)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r9
	cmpq	-120(%rbp), %rdi
	je	.L1035
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r9
.L1035:
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	movq	%r9, -128(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %r9
	movl	$0, -100(%rbp)
	testq	%r9, %r9
	je	.L1038
	movq	(%r9), %rax
	leaq	-100(%rbp), %rsi
	movq	%r9, %rdi
	call	*48(%rax)
	testb	%al, %al
	jne	.L1037
.L1038:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L1037:
	movl	-100(%rbp), %eax
	movq	%r15, %rdi
	leaq	.LC11(%rip), %rsi
	movl	%eax, 12(%r14)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r15
	cmpq	-120(%rbp), %rdi
	je	.L1039
	call	_ZdlPv@PLT
.L1039:
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movl	$0, -100(%rbp)
	testq	%r15, %r15
	je	.L1042
	movq	(%r15), %rax
	leaq	-100(%rbp), %rsi
	movq	%r15, %rdi
	call	*48(%rax)
	testb	%al, %al
	jne	.L1041
.L1042:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L1041:
	movl	-100(%rbp), %eax
	movq	%r12, %rdi
	movl	%eax, 16(%r14)
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L1061
	movq	%r14, 0(%r13)
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1061:
	movq	(%r14), %rax
	movq	$0, 0(%r13)
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1062
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1062:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1026
.L1060:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6488:
	.size	_ZN12v8_inspector8protocol8Profiler13CoverageRange9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler13CoverageRange9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNK12v8_inspector8protocol8Profiler13CoverageRange7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler13CoverageRange7toValueEv
	.type	_ZNK12v8_inspector8protocol8Profiler13CoverageRange7toValueEv, @function
_ZNK12v8_inspector8protocol8Profiler13CoverageRange7toValueEv:
.LFB6489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	-120(%rbp), %r8
	movl	$24, %edi
	movq	%r8, 0(%r13)
	movl	8(%rbx), %r12d
	call	_Znwm@PLT
	leaq	.LC9(%rip), %rsi
	movl	%r12d, 16(%rax)
	leaq	-96(%rbp), %r12
	movl	$2, 8(%rax)
	movq	%r12, %rdi
	movq	%r15, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L1064
	call	_ZdlPv@PLT
.L1064:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1065
	movq	(%rdi), %rax
	call	*24(%rax)
.L1065:
	movq	0(%r13), %r8
	movl	12(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -136(%rbp)
	movl	%edx, -128(%rbp)
	call	_Znwm@PLT
	movl	-128(%rbp), %edx
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	movl	%edx, 16(%rax)
	movq	%r15, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L1066
	call	_ZdlPv@PLT
.L1066:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1067
	movq	(%rdi), %rax
	call	*24(%rax)
.L1067:
	movq	0(%r13), %r8
	movl	16(%rbx), %ebx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	call	_Znwm@PLT
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	movq	%r15, (%rax)
	movl	%ebx, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L1068
	call	_ZdlPv@PLT
.L1068:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1063
	movq	(%rdi), %rax
	call	*24(%rax)
.L1063:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1081
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1081:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6489:
	.size	_ZNK12v8_inspector8protocol8Profiler13CoverageRange7toValueEv, .-_ZNK12v8_inspector8protocol8Profiler13CoverageRange7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Profiler13CoverageRange17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler13CoverageRange17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler13CoverageRange17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler13CoverageRange17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler13CoverageRange17serializeToBinaryEv:
.LFB6225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler13CoverageRange7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1082
	movq	(%rdi), %rax
	call	*24(%rax)
.L1082:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1089
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1089:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6225:
	.size	_ZN12v8_inspector8protocol8Profiler13CoverageRange17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler13CoverageRange17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler13CoverageRange15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler13CoverageRange15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler13CoverageRange15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler13CoverageRange15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler13CoverageRange15serializeToJSONEv:
.LFB6224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler13CoverageRange7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1090
	movq	(%rdi), %rax
	call	*24(%rax)
.L1090:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1097
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1097:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6224:
	.size	_ZN12v8_inspector8protocol8Profiler13CoverageRange15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler13CoverageRange15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Profiler13CoverageRange5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler13CoverageRange5cloneEv
	.type	_ZNK12v8_inspector8protocol8Profiler13CoverageRange5cloneEv, @function
_ZNK12v8_inspector8protocol8Profiler13CoverageRange5cloneEv:
.LFB6490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Profiler13CoverageRange7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Profiler13CoverageRange9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1099
	movq	(%rdi), %rax
	call	*24(%rax)
.L1099:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1105
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1105:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6490:
	.size	_ZNK12v8_inspector8protocol8Profiler13CoverageRange5cloneEv, .-_ZNK12v8_inspector8protocol8Profiler13CoverageRange5cloneEv
	.section	.rodata._ZN12v8_inspector8protocol8Profiler10TypeObject9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"name"
.LC13:
	.string	"string value expected"
	.section	.text._ZN12v8_inspector8protocol8Profiler10TypeObject9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler10TypeObject9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler10TypeObject9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler10TypeObject9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1107
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r12
	je	.L1108
.L1107:
	leaq	.LC6(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, 0(%r13)
.L1106:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1145
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1108:
	.cfi_restore_state
	movl	$48, %edi
	leaq	-96(%rbp), %rbx
	call	_Znwm@PLT
	xorl	%edi, %edi
	cmpl	$6, 8(%r12)
	movq	%rax, %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	movq	%rax, -104(%rbp)
	movq	%rax, 8(%r15)
	movl	$0, %eax
	cmovne	%rax, %r12
	movw	%di, 24(%r15)
	movq	%r14, %rdi
	movq	$0, 16(%r15)
	movq	$0, 40(%r15)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC12(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	leaq	-80(%rbp), %r12
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r10
	cmpq	%r12, %rdi
	je	.L1111
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %r10
.L1111:
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	movq	%r10, -112(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-112(%rbp), %r10
	xorl	%esi, %esi
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movw	%si, -80(%rbp)
	movq	$0, -64(%rbp)
	testq	%r10, %r10
	je	.L1114
	movq	(%r10), %rax
	movq	%rbx, %rsi
	movq	%r10, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L1113
.L1114:
	leaq	.LC13(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L1113:
	movq	-96(%rbp), %rdx
	movq	8(%r15), %rdi
	movq	-88(%rbp), %rax
	cmpq	%r12, %rdx
	je	.L1146
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -104(%rbp)
	je	.L1147
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	24(%r15), %rsi
	movq	%rdx, 8(%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%r15)
	testq	%rdi, %rdi
	je	.L1120
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L1118:
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movw	%ax, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 40(%r15)
	cmpq	%r12, %rdi
	je	.L1121
	call	_ZdlPv@PLT
.L1121:
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L1148
	movq	%r15, 0(%r13)
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1148:
	movq	(%r15), %rax
	movq	$0, 0(%r13)
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1149
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r15), %rdi
	movq	%rax, (%r15)
	cmpq	%rdi, -104(%rbp)
	je	.L1122
	call	_ZdlPv@PLT
.L1122:
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1146:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L1116
	cmpq	$1, %rax
	je	.L1150
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L1116
	movq	%r12, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	8(%r15), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L1116:
	xorl	%ecx, %ecx
	movq	%rax, 16(%r15)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1147:
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, 8(%r15)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 16(%r15)
.L1120:
	movq	%r12, -96(%rbp)
	leaq	-80(%rbp), %r12
	movq	%r12, %rdi
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1149:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1106
.L1150:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	8(%r15), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L1116
.L1145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6501:
	.size	_ZN12v8_inspector8protocol8Profiler10TypeObject9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler10TypeObject9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol8Profiler8Frontend5flushEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler8Frontend5flushEv
	.type	_ZN12v8_inspector8protocol8Profiler8Frontend5flushEv, @function
_ZN12v8_inspector8protocol8Profiler8Frontend5flushEv:
.LFB6523:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE6523:
	.size	_ZN12v8_inspector8protocol8Profiler8Frontend5flushEv, .-_ZN12v8_inspector8protocol8Profiler8Frontend5flushEv
	.section	.text._ZN12v8_inspector8protocol8Profiler8Frontend23sendRawJSONNotificationENS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler8Frontend23sendRawJSONNotificationENS_8String16E
	.type	_ZN12v8_inspector8protocol8Profiler8Frontend23sendRawJSONNotificationENS_8String16E, @function
_ZN12v8_inspector8protocol8Profiler8Frontend23sendRawJSONNotificationENS_8String16E:
.LFB6524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rsi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	movq	24(%rax), %r14
	movq	(%rsi), %rax
	cmpq	%rdx, %rax
	je	.L1166
	movq	%rdx, (%rsi)
	xorl	%edx, %edx
	movq	16(%rsi), %rcx
	leaq	-64(%rbp), %r13
	movw	%dx, 16(%rsi)
	movq	32(%rsi), %rdx
	leaq	-112(%rbp), %rbx
	movq	8(%rsi), %rdi
	movq	%rcx, -112(%rbp)
	movq	$0, 8(%rsi)
	movq	%rdx, -96(%rbp)
	movq	%r13, -80(%rbp)
	cmpq	%rbx, %rax
	je	.L1154
	movq	%rax, -80(%rbp)
	movq	%rcx, -64(%rbp)
.L1156:
	movq	%rdi, -72(%rbp)
	xorl	%eax, %eax
	movl	$72, %edi
	movq	%rdx, -48(%rbp)
	movq	%rbx, -128(%rbp)
	movq	$0, -120(%rbp)
	movw	%ax, -112(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rcx
	leaq	24(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	movq	-80(%rbp), %rdx
	cmpq	%r13, %rdx
	je	.L1167
	movq	%rdx, 8(%rax)
	movq	-64(%rbp), %rdx
	movq	%rdx, 24(%rax)
.L1158:
	movq	-72(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, 64(%rax)
	movq	%rax, -136(%rbp)
	leaq	-136(%rbp), %rsi
	movq	%rdx, 16(%rax)
	movq	-48(%rbp), %rdx
	movups	%xmm0, 48(%rax)
	movq	%rdx, 40(%rax)
	call	*%r14
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1159
	movq	(%rdi), %rax
	call	*24(%rax)
.L1159:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1152
	call	_ZdlPv@PLT
.L1152:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1168
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1166:
	.cfi_restore_state
	movq	32(%rsi), %rdx
	xorl	%ecx, %ecx
	leaq	-64(%rbp), %r13
	movdqu	16(%rsi), %xmm1
	movq	8(%rsi), %rdi
	movw	%cx, 16(%rsi)
	leaq	-112(%rbp), %rbx
	movq	$0, 8(%rsi)
	movq	%rdx, -96(%rbp)
	movq	%r13, -80(%rbp)
	movaps	%xmm1, -112(%rbp)
.L1154:
	movdqa	-112(%rbp), %xmm2
	movaps	%xmm2, -64(%rbp)
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1167:
	movdqa	-64(%rbp), %xmm3
	movups	%xmm3, 24(%rax)
	jmp	.L1158
.L1168:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6524:
	.size	_ZN12v8_inspector8protocol8Profiler8Frontend23sendRawJSONNotificationENS_8String16E, .-_ZN12v8_inspector8protocol8Profiler8Frontend23sendRawJSONNotificationENS_8String16E
	.section	.text._ZN12v8_inspector8protocol8Profiler8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE
	.type	_ZN12v8_inspector8protocol8Profiler8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE, @function
_ZN12v8_inspector8protocol8Profiler8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE:
.LFB6525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	(%rsi), %rbx
	movl	$72, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %r15
	movq	8(%rsi), %r12
	movq	0(%r13), %rax
	movq	24(%rax), %r14
	movq	$0, 16(%rsi)
	movups	%xmm0, (%rsi)
	call	_Znwm@PLT
	movq	%r12, %xmm1
	movq	%rbx, %xmm0
	movq	%r13, %rdi
	leaq	24(%rax), %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%r15, 64(%rax)
	leaq	-64(%rbp), %rsi
	movq	%rdx, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rcx
	xorl	%edx, %edx
	movq	%rcx, (%rax)
	movq	$0, 16(%rax)
	movw	%dx, 24(%rax)
	movq	$0, 40(%rax)
	movq	%rax, -64(%rbp)
	movups	%xmm0, 48(%rax)
	call	*%r14
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1169
	movq	(%rdi), %rax
	call	*24(%rax)
.L1169:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1176
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1176:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6525:
	.size	_ZN12v8_inspector8protocol8Profiler8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE, .-_ZN12v8_inspector8protocol8Profiler8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm.str1.1,"aMS",@progbits,1
.LC14:
	.string	"vector::reserve"
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm:
.LFB7220:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	ja	.L1193
	movq	(%rdi), %r12
	movq	16(%rdi), %rax
	movq	%rdi, %r15
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L1194
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1194:
	.cfi_restore_state
	movq	8(%rdi), %rbx
	xorl	%r13d, %r13d
	movq	%rbx, %rax
	subq	%r12, %rax
	movq	%rax, -56(%rbp)
	leaq	0(,%rsi,8), %rax
	movq	%rax, -64(%rbp)
	testq	%rsi, %rsi
	je	.L1180
	movq	%rax, %rdi
	call	_Znwm@PLT
	movq	8(%r15), %rbx
	movq	(%r15), %r12
	movq	%rax, %r13
.L1180:
	cmpq	%r12, %rbx
	je	.L1181
	movq	%r13, %r14
	.p2align 4,,10
	.p2align 3
.L1185:
	movq	(%r12), %rcx
	movq	$0, (%r12)
	movq	%rcx, (%r14)
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1182
	movq	(%rdi), %rcx
	addq	$8, %r12
	addq	$8, %r14
	call	*24(%rcx)
	cmpq	%rbx, %r12
	jne	.L1185
.L1183:
	movq	(%r15), %r12
.L1181:
	testq	%r12, %r12
	je	.L1186
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1186:
	movq	-56(%rbp), %r14
	movq	%r13, (%r15)
	addq	%r13, %r14
	addq	-64(%rbp), %r13
	movq	%r14, 8(%r15)
	movq	%r13, 16(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1182:
	.cfi_restore_state
	addq	$8, %r12
	addq	$8, %r14
	cmpq	%r12, %rbx
	jne	.L1185
	jmp	.L1183
.L1193:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7220:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_.str1.1,"aMS",@progbits,1
.LC15:
	.string	"basic_string::_M_create"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_:
.LFB10636:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L1214
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	8(%rsi), %r15
	cmpq	%r13, %rdx
	je	.L1205
	movq	16(%rdi), %rax
.L1197:
	cmpq	%r15, %rax
	jb	.L1217
	leaq	(%r15,%r15), %rdx
	testq	%r15, %r15
	je	.L1201
.L1220:
	movq	(%r12), %rsi
	cmpq	$1, %r15
	je	.L1218
	testq	%rdx, %rdx
	je	.L1201
	movq	%r13, %rdi
	call	memmove@PLT
	movq	(%rbx), %r13
.L1201:
	xorl	%eax, %eax
	movq	%r15, 8(%rbx)
	movw	%ax, 0(%r13,%r15,2)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1217:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r15
	ja	.L1219
	addq	%rax, %rax
	movq	%r15, %r14
	cmpq	%rax, %r15
	jnb	.L1200
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r14
.L1200:
	leaq	2(%r14,%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdi, %rdx
	je	.L1204
	call	_ZdlPv@PLT
.L1204:
	movq	%r13, (%rbx)
	leaq	(%r15,%r15), %rdx
	movq	%r14, 16(%rbx)
	testq	%r15, %r15
	je	.L1201
	jmp	.L1220
	.p2align 4,,10
	.p2align 3
.L1214:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1205:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, %eax
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1218:
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	movq	(%rbx), %r13
	jmp	.L1201
.L1219:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10636:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.section	.rodata._ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"id"
.LC17:
	.string	"location"
.LC18:
	.string	"title"
	.section	.text._ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1222
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r12
	je	.L1223
.L1222:
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, (%r14)
.L1221:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1286
	addq	$104, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1223:
	.cfi_restore_state
	movl	$104, %edi
	leaq	-96(%rbp), %rbx
	call	_Znwm@PLT
	xorl	%edi, %edi
	cmpl	$6, 8(%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationE(%rip), %rax
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	movq	%rax, -128(%rbp)
	movq	%rax, 8(%r15)
	leaq	80(%r15), %rax
	movq	%rax, -144(%rbp)
	movq	%rax, 64(%r15)
	movl	$0, %eax
	cmovne	%rax, %r12
	movw	%di, 24(%r15)
	movq	%r13, %rdi
	movups	%xmm0, 80(%r15)
	movq	$0, 16(%r15)
	movq	$0, 40(%r15)
	movq	$0, 48(%r15)
	movb	$0, 56(%r15)
	movq	$0, 96(%r15)
	movq	$0, 72(%r15)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r8
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L1226
	movq	%r8, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L1226:
	leaq	.LC16(%rip), %rsi
	movq	%r13, %rdi
	movq	%r8, -136(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-120(%rbp), %rax
	xorl	%esi, %esi
	movq	-136(%rbp), %r8
	movq	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	movw	%si, -80(%rbp)
	movq	$0, -64(%rbp)
	testq	%r8, %r8
	je	.L1229
	movq	(%r8), %rax
	movq	%rbx, %rsi
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L1228
.L1229:
	leaq	.LC13(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L1228:
	movq	8(%r15), %rdi
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rax
	cmpq	-120(%rbp), %rdx
	je	.L1287
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -128(%rbp)
	je	.L1288
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	24(%r15), %rsi
	movq	%rdx, 8(%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%r15)
	testq	%rdi, %rdi
	je	.L1235
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L1233:
	xorl	%edx, %edx
	movq	$0, -88(%rbp)
	movw	%dx, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 40(%r15)
	cmpq	-120(%rbp), %rdi
	je	.L1236
	call	_ZdlPv@PLT
.L1236:
	leaq	.LC17(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -136(%rbp)
	cmpq	-120(%rbp), %rdi
	je	.L1237
	call	_ZdlPv@PLT
.L1237:
	leaq	.LC17(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %rsi
	leaq	-104(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	movq	-104(%rbp), %rax
	movq	48(%r15), %r8
	movq	$0, -104(%rbp)
	movq	%rax, 48(%r15)
	testq	%r8, %r8
	je	.L1239
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1240
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1241
	movq	%r8, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L1241:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1242:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1239
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1244
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1245
	movq	%r8, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L1245:
	movl	$64, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1239:
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	cmpq	-120(%rbp), %rdi
	je	.L1246
	call	_ZdlPv@PLT
.L1246:
	testq	%r12, %r12
	je	.L1247
	leaq	.LC18(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-120(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	movw	%ax, -80(%rbp)
	movq	$0, -64(%rbp)
	movq	(%r12), %rax
	call	*56(%rax)
	testb	%al, %al
	je	.L1289
.L1248:
	leaq	64(%r15), %rdi
	movq	%rbx, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movb	$1, 56(%r15)
	movq	-96(%rbp), %rdi
	movq	%rax, 96(%r15)
	cmpq	-120(%rbp), %rdi
	je	.L1247
	call	_ZdlPv@PLT
.L1247:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L1290
	movq	%r15, (%r14)
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1289:
	leaq	.LC13(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1290:
	movq	(%r15), %rax
	movq	$0, (%r14)
	leaq	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1291
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationE(%rip), %rax
	movq	64(%r15), %rdi
	movq	%rax, (%r15)
	cmpq	%rdi, -144(%rbp)
	je	.L1250
	call	_ZdlPv@PLT
.L1250:
	movq	48(%r15), %r12
	testq	%r12, %r12
	je	.L1251
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1252
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1253
	call	_ZdlPv@PLT
.L1253:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1251:
	movq	8(%r15), %rdi
	cmpq	%rdi, -128(%rbp)
	je	.L1254
	call	_ZdlPv@PLT
.L1254:
	movl	$104, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1287:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L1231
	cmpq	$1, %rax
	je	.L1292
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L1231
	movq	-120(%rbp), %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	8(%r15), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L1231:
	xorl	%ecx, %ecx
	movq	%rax, 16(%r15)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1288:
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, 8(%r15)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 16(%r15)
.L1235:
	movq	-120(%rbp), %rax
	movq	%rax, -96(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%rax, %rdi
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1291:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1240:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L1242
	.p2align 4,,10
	.p2align 3
.L1252:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1251
.L1292:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	8(%r15), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L1231
.L1286:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6513:
	.size	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC19:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB11928:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L1307
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1303
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1308
.L1295:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1302:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1309
	testq	%r13, %r13
	jg	.L1298
	testq	%r9, %r9
	jne	.L1301
.L1299:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1309:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1298
.L1301:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1308:
	testq	%rsi, %rsi
	jne	.L1296
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1298:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1299
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1303:
	movl	$4, %r14d
	jmp	.L1295
.L1307:
	leaq	.LC19(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1296:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L1295
	.cfi_endproc
.LFE11928:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB11951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movabsq	$1152921504606846975, %rdi
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L1337
	movq	%rsi, %r13
	movq	%rsi, %rbx
	subq	%r12, %rsi
	testq	%rax, %rax
	je	.L1324
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L1338
.L1312:
	movq	%r14, %rdi
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdx
	movq	%rax, -56(%rbp)
	addq	%rax, %r14
	movq	%r14, -64(%rbp)
	leaq	8(%rax), %r14
.L1323:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rcx
	movq	$0, (%rdx)
	movq	%rax, (%rcx,%rsi)
	cmpq	%r12, %r13
	je	.L1314
	movq	%rcx, %r14
	movq	%r12, %r15
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1340:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L1315:
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	je	.L1339
.L1317:
	movq	(%r15), %rsi
	movq	$0, (%r15)
	movq	%rsi, (%r14)
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1315
	movq	(%rdi), %rsi
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rax
	movq	24(%rsi), %rsi
	cmpq	%rax, %rsi
	je	.L1340
	call	*%rsi
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	jne	.L1317
	.p2align 4,,10
	.p2align 3
.L1339:
	movq	-56(%rbp), %rcx
	movq	%r13, %rax
	subq	%r12, %rax
	leaq	8(%rcx,%rax), %r14
.L1314:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r13
	je	.L1318
	movq	%rax, %r15
	subq	%r13, %r15
	leaq	-8(%r15), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	addq	$1, %rsi
	testq	%rdi, %rdi
	je	.L1326
	movq	%rsi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1320:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1320
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%r14,%r8), %rdx
	leaq	0(%r13,%r8), %rbx
	cmpq	%rax, %rsi
	je	.L1321
.L1319:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L1321:
	leaq	8(%r14,%rdi), %r14
.L1318:
	testq	%r12, %r12
	je	.L1322
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1322:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r14, %xmm2
	movq	-64(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1338:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L1313
	movq	$0, -64(%rbp)
	movl	$8, %r14d
	movq	$0, -56(%rbp)
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1324:
	movl	$8, %r14d
	jmp	.L1312
.L1326:
	movq	%r14, %rdx
	jmp	.L1319
.L1313:
	cmpq	%rdi, %r9
	cmovbe	%r9, %rdi
	leaq	0(,%rdi,8), %r14
	jmp	.L1312
.L1337:
	leaq	.LC19(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11951:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.rodata._ZN12v8_inspector8protocol8Profiler11ProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"callFrame"
.LC21:
	.string	"hitCount"
.LC22:
	.string	"children"
.LC23:
	.string	"array expected"
.LC24:
	.string	"deoptReason"
.LC25:
	.string	"positionTicks"
	.section	.text._ZN12v8_inspector8protocol8Profiler11ProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler11ProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler11ProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler11ProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1342
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r13
	je	.L1343
.L1342:
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-120(%rbp), %rax
	movq	$0, (%rax)
.L1341:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1524
	movq	-120(%rbp), %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1343:
	.cfi_restore_state
	movl	$96, %edi
	leaq	-96(%rbp), %rbx
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rcx, (%rax)
	leaq	64(%rax), %rcx
	cmpl	$6, 8(%r13)
	movq	%rcx, 48(%rax)
	movq	$0, 16(%rax)
	movb	$0, 24(%rax)
	movl	$0, 28(%rax)
	movq	$0, 32(%rax)
	movb	$0, 40(%rax)
	movq	$0, 80(%rax)
	movq	$0, 56(%rax)
	movq	$0, 88(%rax)
	movl	$0, 8(%rax)
	movups	%xmm0, 64(%rax)
	movq	%rax, -136(%rbp)
	movl	$0, %eax
	cmovne	%rax, %r13
	movq	%rcx, -160(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdi
	je	.L1346
	call	_ZdlPv@PLT
.L1346:
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movl	$0, -104(%rbp)
	leaq	-104(%rbp), %rax
	testq	%r14, %r14
	je	.L1525
	movq	%rax, -144(%rbp)
	movq	%rax, %rsi
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*48(%rax)
	testb	%al, %al
	jne	.L1348
.L1349:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L1348:
	movl	-104(%rbp), %eax
	movq	-136(%rbp), %rcx
	leaq	.LC20(%rip), %rsi
	movq	%rbx, %rdi
	movl	%eax, 8(%rcx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-128(%rbp), %rdi
	je	.L1350
	call	_ZdlPv@PLT
.L1350:
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-144(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol7Runtime9CallFrame9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	movq	-136(%rbp), %rcx
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	16(%rcx), %r15
	movq	%rax, 16(%rcx)
	testq	%r15, %r15
	je	.L1352
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %r14
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1353
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r15), %rdi
	movq	%rax, (%r15)
	leaq	104(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1354
	call	_ZdlPv@PLT
.L1354:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1355
	call	_ZdlPv@PLT
.L1355:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1356
	call	_ZdlPv@PLT
.L1356:
	movl	$136, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1357:
	movq	-104(%rbp), %r15
	testq	%r15, %r15
	je	.L1352
	movq	(%r15), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1359
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r15), %rdi
	movq	%rax, (%r15)
	leaq	104(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1360
	call	_ZdlPv@PLT
.L1360:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1361
	call	_ZdlPv@PLT
.L1361:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1362
	call	_ZdlPv@PLT
.L1362:
	movl	$136, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1352:
	leaq	.LC21(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-128(%rbp), %rdi
	je	.L1363
	call	_ZdlPv@PLT
.L1363:
	testq	%r14, %r14
	je	.L1364
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movl	$0, -104(%rbp)
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	-144(%rbp), %rsi
	call	*48(%rax)
	testb	%al, %al
	je	.L1526
.L1365:
	movq	-136(%rbp), %rcx
	movl	-104(%rbp), %eax
	movb	$1, 24(%rcx)
	movl	%eax, 28(%rcx)
.L1364:
	leaq	.LC22(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r15
	cmpq	-128(%rbp), %rdi
	je	.L1366
	call	_ZdlPv@PLT
.L1366:
	testq	%r15, %r15
	je	.L1367
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	cmpl	$7, 8(%r15)
	jne	.L1527
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movabsq	$2305843009213693951, %rdx
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -152(%rbp)
	movq	24(%r15), %rax
	subq	16(%r15), %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	ja	.L1394
	testq	%rax, %rax
	jne	.L1371
.L1376:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L1369
	movq	-152(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1384
	call	_ZdlPv@PLT
.L1384:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	$0, -152(%rbp)
.L1369:
	movq	-136(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	32(%rax), %r14
	movq	%rcx, 32(%rax)
	testq	%r14, %r14
	je	.L1367
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1385
	call	_ZdlPv@PLT
.L1385:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1367:
	leaq	.LC24(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-128(%rbp), %rdi
	je	.L1386
	call	_ZdlPv@PLT
.L1386:
	testq	%r14, %r14
	je	.L1387
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	movw	%ax, -80(%rbp)
	movq	$0, -64(%rbp)
	movq	(%r14), %rax
	call	*56(%rax)
	testb	%al, %al
	je	.L1528
.L1388:
	movq	-136(%rbp), %r15
	movq	%rbx, %rsi
	leaq	48(%r15), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movb	$1, 40(%r15)
	movq	-96(%rbp), %rdi
	movq	%rax, 80(%r15)
	cmpq	-128(%rbp), %rdi
	je	.L1387
	call	_ZdlPv@PLT
.L1387:
	leaq	.LC25(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-128(%rbp), %rdi
	je	.L1390
	call	_ZdlPv@PLT
.L1390:
	testq	%r14, %r14
	je	.L1391
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	cmpl	$7, 8(%r14)
	jne	.L1529
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, %r15
	movups	%xmm0, (%rax)
	movq	24(%r14), %r13
	subq	16(%r14), %r13
	movq	%r13, %rax
	sarq	$3, %rax
	testq	%r13, %r13
	js	.L1394
	testq	%rax, %rax
	jne	.L1395
.L1402:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L1393
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L1409
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %r13
	jmp	.L1412
	.p2align 4,,10
	.p2align 3
.L1531:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L1410:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1530
.L1412:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1410
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	je	.L1531
	call	*%rax
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1526:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1528:
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1530:
	movq	(%r15), %r14
.L1409:
	testq	%r14, %r14
	je	.L1413
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1413:
	movq	%r15, %rdi
	movl	$24, %esi
	xorl	%r15d, %r15d
	call	_ZdlPvm@PLT
.L1393:
	movq	-136(%rbp), %rax
	movq	88(%rax), %r13
	movq	%r15, 88(%rax)
	testq	%r13, %r13
	je	.L1391
	movq	8(%r13), %rbx
	movq	0(%r13), %r15
	cmpq	%r15, %rbx
	je	.L1414
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %r14
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1533:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L1415:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L1532
.L1417:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1415
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	je	.L1533
	call	*%rax
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1532:
	movq	0(%r13), %r15
.L1414:
	testq	%r15, %r15
	je	.L1418
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1418:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1391:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	movq	-120(%rbp), %rax
	jne	.L1419
	movq	-136(%rbp), %rcx
	movq	%rcx, (%rax)
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1419:
	movq	$0, (%rax)
	movq	-136(%rbp), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rdx
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1534
	movq	-136(%rbp), %rax
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rcx
	movq	88(%rax), %r12
	movq	%rcx, (%rax)
	testq	%r12, %r12
	je	.L1420
	movq	8(%r12), %rbx
	movq	(%r12), %r14
	cmpq	%r14, %rbx
	je	.L1421
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %r13
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1536:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L1422:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1535
.L1424:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1422
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	je	.L1536
	call	*%rax
	jmp	.L1422
	.p2align 4,,10
	.p2align 3
.L1535:
	movq	(%r12), %r14
.L1421:
	testq	%r14, %r14
	je	.L1425
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1425:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1420:
	movq	-136(%rbp), %rax
	movq	48(%rax), %rdi
	cmpq	%rdi, -160(%rbp)
	je	.L1426
	call	_ZdlPv@PLT
.L1426:
	movq	-136(%rbp), %rax
	movq	32(%rax), %r12
	testq	%r12, %r12
	je	.L1427
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1428
	call	_ZdlPv@PLT
.L1428:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1427:
	movq	-136(%rbp), %rax
	movq	16(%rax), %r12
	testq	%r12, %r12
	je	.L1429
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1430
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1431
	call	_ZdlPv@PLT
.L1431:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1432
	call	_ZdlPv@PLT
.L1432:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1433
	call	_ZdlPv@PLT
.L1433:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1429:
	movq	-136(%rbp), %rdi
	movl	$96, %esi
	call	_ZdlPvm@PLT
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1395:
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	8(%r15), %rcx
	movq	(%r15), %r8
	movq	%rax, -152(%rbp)
	cmpq	%r8, %rcx
	je	.L1397
	movq	%r14, -168(%rbp)
	movq	%rcx, %r14
	movq	%r12, -176(%rbp)
	movq	%r8, %r12
	movq	%rbx, -184(%rbp)
	movq	%rax, %rbx
	jmp	.L1400
	.p2align 4,,10
	.p2align 3
.L1538:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L1398:
	addq	$8, %r12
	addq	$8, %rbx
	cmpq	%r12, %r14
	je	.L1537
.L1400:
	movq	(%r12), %rdx
	movq	$0, (%r12)
	movq	%rdx, (%rbx)
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1398
	movq	(%rdi), %rdx
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1538
	call	*%rdx
	jmp	.L1398
	.p2align 4,,10
	.p2align 3
.L1371:
	leaq	0(,%rax,4), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%rax, %rcx
	movq	-152(%rbp), %rax
	movq	(%rax), %r8
	movq	8(%rax), %rax
	movq	%rax, %rdx
	movq	%rax, -168(%rbp)
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L1539
	testq	%r8, %r8
	jne	.L1374
.L1375:
	movq	-152(%rbp), %rdx
	movq	%rcx, %xmm0
	leaq	(%rcx,%r14), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 16(%rdx)
	movups	%xmm0, (%rdx)
	movq	16(%r15), %rax
	cmpq	%rax, 24(%r15)
	je	.L1376
	movq	%r13, -168(%rbp)
	xorl	%r14d, %r14d
	movq	%rdx, %r13
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1541:
	movl	%eax, (%rsi)
	addq	$4, 8(%r13)
.L1523:
	movq	24(%r15), %rax
	subq	16(%r15), %rax
	addq	$1, %r14
	sarq	$3, %rax
	cmpq	%r14, %rax
	jbe	.L1540
.L1383:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-128(%rbp), %rdi
	je	.L1377
	call	_ZdlPv@PLT
.L1377:
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol9ListValue2atEm@PLT
	movl	$0, -104(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1380
	movq	(%rax), %rax
	movq	-144(%rbp), %rsi
	call	*48(%rax)
	testb	%al, %al
	jne	.L1379
.L1380:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L1379:
	movl	-104(%rbp), %eax
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	jne	.L1541
	movq	-144(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L1523
	.p2align 4,,10
	.p2align 3
.L1540:
	movq	-168(%rbp), %r13
	jmp	.L1376
	.p2align 4,,10
	.p2align 3
.L1529:
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L1393
	.p2align 4,,10
	.p2align 3
.L1527:
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, -152(%rbp)
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1525:
	movq	%rax, -144(%rbp)
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1359:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1353:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1534:
	movq	-136(%rbp), %rdi
	call	*%rax
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1537:
	movq	-168(%rbp), %r14
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %rbx
	movq	(%r15), %r8
.L1397:
	testq	%r8, %r8
	je	.L1401
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L1401:
	movq	-152(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 16(%r15)
	movups	%xmm0, (%r15)
	movq	24(%r14), %rax
	cmpq	%rax, 16(%r14)
	je	.L1402
	xorl	%r13d, %r13d
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1542:
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r15)
.L1405:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1406
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1407
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L1406:
	movq	24(%r14), %rax
	subq	16(%r14), %rax
	addq	$1, %r13
	sarq	$3, %rax
	cmpq	%r13, %rax
	jbe	.L1402
.L1408:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-128(%rbp), %rdi
	je	.L1403
	call	_ZdlPv@PLT
.L1403:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol9ListValue2atEm@PLT
	movq	-144(%rbp), %rdi
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	8(%r15), %rsi
	cmpq	16(%r15), %rsi
	jne	.L1542
	movq	-144(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16PositionTickInfoESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L1405
	.p2align 4,,10
	.p2align 3
.L1407:
	call	*%rax
	jmp	.L1406
	.p2align 4,,10
	.p2align 3
.L1430:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1429
.L1539:
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%r8, -168(%rbp)
	call	memmove@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %rcx
.L1374:
	movq	%r8, %rdi
	movq	%rcx, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %rcx
	jmp	.L1375
.L1524:
	call	__stack_chk_fail@PLT
.L1394:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6479:
	.size	_ZN12v8_inspector8protocol8Profiler11ProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler11ProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler11ProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler11ProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler11ProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler11ProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler11ProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB12005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$1152921504606846975, %rdx
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rdi, -112(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rax, -104(%rbp)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rcx, -72(%rbp)
	cmpq	%rdx, %rax
	je	.L1603
	movq	%rsi, %r14
	movq	%rsi, %r13
	subq	-72(%rbp), %r14
	testq	%rax, %rax
	je	.L1571
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1604
.L1545:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, -88(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -96(%rbp)
	leaq	8(%rax), %rbx
.L1570:
	movq	(%r12), %rax
	movq	$0, (%r12)
	movq	-88(%rbp), %r12
	movq	%rax, (%r12,%r14)
	movq	-72(%rbp), %rax
	cmpq	%rax, %r13
	je	.L1547
	movq	%r13, -56(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1564:
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%rax, (%r12)
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L1548
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1549
	movq	88(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1550
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L1551
	movq	%rbx, -80(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1606:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L1552:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1605
.L1554:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1552
	movq	(%rdi), %rdx
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1606
	call	*%rdx
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.L1554
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	-80(%rbp), %rbx
	movq	(%r14), %r15
.L1551:
	testq	%r15, %r15
	je	.L1555
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1555:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1550:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1556
	call	_ZdlPv@PLT
.L1556:
	movq	32(%r13), %r14
	testq	%r14, %r14
	je	.L1557
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1558
	call	_ZdlPv@PLT
.L1558:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1557:
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L1559
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1560
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r14), %rdi
	movq	%rax, (%r14)
	leaq	104(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1561
	call	_ZdlPv@PLT
.L1561:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1562
	call	_ZdlPv@PLT
.L1562:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1563
	call	_ZdlPv@PLT
.L1563:
	movl	$136, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1559:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1548:
	addq	$8, %rbx
	addq	$8, %r12
	cmpq	%rbx, -56(%rbp)
	jne	.L1564
.L1607:
	movq	-56(%rbp), %r13
	movq	-88(%rbp), %rcx
	movq	%r13, %rax
	subq	-72(%rbp), %rax
	leaq	8(%rcx,%rax), %rbx
.L1547:
	movq	-104(%rbp), %rax
	cmpq	%rax, %r13
	je	.L1565
	subq	%r13, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L1573
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1567:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1567
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	leaq	(%rbx,%rdi), %rdx
	addq	%r13, %rdi
	movq	%rdi, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L1568
.L1566:
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1568:
	leaq	8(%rbx,%rsi), %rbx
.L1565:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L1569
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1569:
	movq	-88(%rbp), %xmm0
	movq	-112(%rbp), %rax
	movq	%rbx, %xmm2
	movq	-96(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1549:
	.cfi_restore_state
	movq	%r13, %rdi
	addq	$8, %rbx
	addq	$8, %r12
	call	*%rax
	cmpq	%rbx, -56(%rbp)
	jne	.L1564
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1560:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1604:
	testq	%rcx, %rcx
	jne	.L1546
	movq	$0, -96(%rbp)
	movl	$8, %ebx
	movq	$0, -88(%rbp)
	jmp	.L1570
	.p2align 4,,10
	.p2align 3
.L1571:
	movl	$8, %ebx
	jmp	.L1545
.L1573:
	movq	%rbx, %rdx
	jmp	.L1566
.L1546:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L1545
.L1603:
	leaq	.LC19(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12005:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler11ProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler11ProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler11ProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"axG",@progbits,_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler11ProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler11ProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler11ProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler11ProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB8350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1609
	cmpl	$7, 8(%rsi)
	movq	%rsi, %r14
	jne	.L1609
	movq	%rdx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, -160(%rbp)
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	24(%r14), %rax
	subq	16(%r14), %rax
	movq	%rax, %rdx
	movq	%rax, -136(%rbp)
	sarq	$3, %rax
	testq	%rdx, %rdx
	js	.L1756
	testq	%rax, %rax
	jne	.L1613
.L1635:
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L1757
	movq	-152(%rbp), %rax
	movq	$0, (%rax)
	movq	-160(%rbp), %rax
	movq	8(%rax), %rdx
	movq	(%rax), %r12
	movq	%rdx, -120(%rbp)
	cmpq	%r12, %rdx
	je	.L1656
	.p2align 4,,10
	.p2align 3
.L1657:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L1658
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1659
	movq	88(%r15), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r14, %r14
	je	.L1660
	movq	8(%r14), %rbx
	movq	(%r14), %r13
	cmpq	%r13, %rbx
	jne	.L1664
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1759:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L1662:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L1758
.L1664:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1662
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1759
	call	*%rax
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1664
	.p2align 4,,10
	.p2align 3
.L1758:
	movq	(%r14), %r13
.L1661:
	testq	%r13, %r13
	je	.L1665
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1665:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1660:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1666
	call	_ZdlPv@PLT
.L1666:
	movq	32(%r15), %r13
	testq	%r13, %r13
	je	.L1667
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1668
	call	_ZdlPv@PLT
.L1668:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1667:
	movq	16(%r15), %r13
	testq	%r13, %r13
	je	.L1669
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1670
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	104(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1671
	call	_ZdlPv@PLT
.L1671:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1672
	call	_ZdlPv@PLT
.L1672:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1673
	call	_ZdlPv@PLT
.L1673:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1669:
	movl	$96, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1658:
	addq	$8, %r12
	cmpq	%r12, -120(%rbp)
	jne	.L1657
.L1765:
	movq	-160(%rbp), %rax
	movq	(%rax), %r12
.L1656:
	testq	%r12, %r12
	je	.L1674
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1674:
	movq	-160(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1608:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1760
	movq	-152(%rbp), %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1613:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_Znwm@PLT
	movq	-160(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movq	8(%rcx), %rdx
	movq	(%rcx), %r13
	movq	%rdx, -120(%rbp)
	cmpq	%r13, %rdx
	je	.L1616
	movq	%r14, -168(%rbp)
	movq	%rax, %r12
	movq	%r15, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L1633:
	movq	0(%r13), %rax
	movq	$0, 0(%r13)
	movq	%rax, (%r12)
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L1617
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1618
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	movq	88(%r15), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L1619
	movq	8(%rax), %rbx
	movq	(%rax), %r14
	cmpq	%r14, %rbx
	jne	.L1623
	jmp	.L1620
	.p2align 4,,10
	.p2align 3
.L1762:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L1621:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1761
.L1623:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1621
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1762
	call	*%rax
	addq	$8, %r14
	cmpq	%r14, %rbx
	jne	.L1623
	.p2align 4,,10
	.p2align 3
.L1761:
	movq	-128(%rbp), %rax
	movq	(%rax), %r14
.L1620:
	testq	%r14, %r14
	je	.L1624
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1624:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1619:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1625
	call	_ZdlPv@PLT
.L1625:
	movq	32(%r15), %r14
	testq	%r14, %r14
	je	.L1626
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1627
	call	_ZdlPv@PLT
.L1627:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1626:
	movq	16(%r15), %r14
	testq	%r14, %r14
	je	.L1628
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1629
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r14), %rdi
	movq	%rax, (%r14)
	leaq	104(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1630
	call	_ZdlPv@PLT
.L1630:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1631
	call	_ZdlPv@PLT
.L1631:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1632
	call	_ZdlPv@PLT
.L1632:
	movl	$136, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1628:
	movl	$96, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1617:
	addq	$8, %r13
	addq	$8, %r12
	cmpq	%r13, -120(%rbp)
	jne	.L1633
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %r14
	movq	-176(%rbp), %r15
	movq	(%rax), %r13
.L1616:
	testq	%r13, %r13
	je	.L1634
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1634:
	movq	-144(%rbp), %rax
	movq	-160(%rbp), %r12
	movq	%rax, %xmm0
	addq	-136(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 16(%r12)
	movq	24(%r14), %rax
	movups	%xmm0, (%r12)
	cmpq	%rax, 16(%r14)
	je	.L1635
	leaq	-96(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%rax, -136(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -144(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L1655:
	movq	-136(%rbp), %r13
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-144(%rbp), %rdi
	je	.L1636
	call	_ZdlPv@PLT
.L1636:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol9ListValue2atEm@PLT
	movq	-120(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol8Profiler11ProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L1637
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r12)
.L1638:
	movq	-104(%rbp), %r13
	testq	%r13, %r13
	je	.L1639
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1640
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	88(%r13), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L1641
	movq	8(%rax), %rdx
	movq	(%rax), %r9
	cmpq	%r9, %rdx
	je	.L1642
	movq	%r12, -168(%rbp)
	movq	%r9, %r12
	movq	%rbx, -176(%rbp)
	movq	%rdx, %rbx
	jmp	.L1645
	.p2align 4,,10
	.p2align 3
.L1764:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L1643:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L1763
.L1645:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1643
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1764
	call	*%rax
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1645
	.p2align 4,,10
	.p2align 3
.L1763:
	movq	-128(%rbp), %rax
	movq	-168(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	(%rax), %r9
.L1642:
	testq	%r9, %r9
	je	.L1646
	movq	%r9, %rdi
	call	_ZdlPv@PLT
.L1646:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1641:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1647
	call	_ZdlPv@PLT
.L1647:
	movq	32(%r13), %r9
	testq	%r9, %r9
	je	.L1648
	movq	(%r9), %rdi
	testq	%rdi, %rdi
	je	.L1649
	movq	%r9, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r9
.L1649:
	movl	$24, %esi
	movq	%r9, %rdi
	call	_ZdlPvm@PLT
.L1648:
	movq	16(%r13), %r9
	testq	%r9, %r9
	je	.L1650
	movq	(%r9), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1651
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r9), %rdi
	movq	%rax, (%r9)
	leaq	104(%r9), %rax
	cmpq	%rax, %rdi
	je	.L1652
	movq	%r9, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r9
.L1652:
	movq	48(%r9), %rdi
	leaq	64(%r9), %rax
	cmpq	%rax, %rdi
	je	.L1653
	movq	%r9, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r9
.L1653:
	movq	8(%r9), %rdi
	leaq	24(%r9), %rax
	cmpq	%rax, %rdi
	je	.L1654
	movq	%r9, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r9
.L1654:
	movl	$136, %esi
	movq	%r9, %rdi
	call	_ZdlPvm@PLT
.L1650:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1639:
	movq	24(%r14), %rax
	subq	16(%r14), %rax
	addq	$1, %rbx
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L1655
	jmp	.L1635
	.p2align 4,,10
	.p2align 3
.L1637:
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler11ProfileNodeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L1638
	.p2align 4,,10
	.p2align 3
.L1640:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1651:
	movq	%r9, %rdi
	call	*%rax
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L1659:
	movq	%r15, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -120(%rbp)
	jne	.L1657
	jmp	.L1765
	.p2align 4,,10
	.p2align 3
.L1609:
	leaq	.LC23(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-152(%rbp), %rax
	movq	$0, (%rax)
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1757:
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %rcx
	movq	%rcx, (%rax)
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1670:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1669
	.p2align 4,,10
	.p2align 3
.L1618:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1629:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1628
.L1760:
	call	__stack_chk_fail@PLT
.L1756:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8350:
	.size	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler11ProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler11ProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Profiler7Profile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC26:
	.string	"nodes"
.LC27:
	.string	"startTime"
.LC29:
	.string	"double value expected"
.LC30:
	.string	"endTime"
.LC31:
	.string	"samples"
.LC32:
	.string	"timeDeltas"
	.section	.text._ZN12v8_inspector8protocol8Profiler7Profile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler7Profile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler7Profile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler7Profile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1767
	cmpl	$6, 8(%rsi)
	je	.L1768
.L1767:
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-120(%rbp), %rax
	movq	$0, (%rax)
.L1766:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2036
	movq	-120(%rbp), %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1768:
	.cfi_restore_state
	movl	$48, %edi
	leaq	-96(%rbp), %r15
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdx
	movups	%xmm0, 32(%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	cmpl	$6, 8(%rdx)
	movq	%rax, -144(%rbp)
	movq	$0, 8(%rax)
	movl	$0, %eax
	cmove	%rdx, %rax
	movq	%rax, %rbx
	movq	%rax, -128(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC26(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r13
	leaq	-80(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rax, %rdi
	je	.L1771
	call	_ZdlPv@PLT
.L1771:
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-104(%rbp), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler11ProfileNodeESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-144(%rbp), %rcx
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	8(%rcx), %rdx
	movq	%rax, 8(%rcx)
	movq	%rdx, -168(%rbp)
	testq	%rdx, %rdx
	je	.L1773
	movq	8(%rdx), %rcx
	movq	(%rdx), %r13
	movq	%rcx, -160(%rbp)
	cmpq	%r13, %rcx
	je	.L1774
	movq	%r12, -176(%rbp)
	movq	%r15, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L1791:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L1775
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1776
	movq	88(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L1777
	movq	8(%r12), %rbx
	movq	(%r12), %r14
	cmpq	%r14, %rbx
	jne	.L1781
	jmp	.L1778
	.p2align 4,,10
	.p2align 3
.L2038:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L1779:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2037
.L1781:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1779
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2038
	call	*%rax
	addq	$8, %r14
	cmpq	%r14, %rbx
	jne	.L1781
	.p2align 4,,10
	.p2align 3
.L2037:
	movq	(%r12), %r14
.L1778:
	testq	%r14, %r14
	je	.L1782
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1782:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1777:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1783
	call	_ZdlPv@PLT
.L1783:
	movq	32(%r15), %r12
	testq	%r12, %r12
	je	.L1784
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1785
	call	_ZdlPv@PLT
.L1785:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1784:
	movq	16(%r15), %r12
	testq	%r12, %r12
	je	.L1786
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1787
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1788
	call	_ZdlPv@PLT
.L1788:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1789
	call	_ZdlPv@PLT
.L1789:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1790
	call	_ZdlPv@PLT
.L1790:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1786:
	movl	$96, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1775:
	addq	$8, %r13
	cmpq	%r13, -160(%rbp)
	jne	.L1791
	movq	-168(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r15
	movq	(%rax), %r13
.L1774:
	testq	%r13, %r13
	je	.L1792
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1792:
	movq	-168(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	-104(%rbp), %rax
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L1773
	movq	8(%rax), %rdx
	movq	(%rax), %r13
	movq	%rdx, -160(%rbp)
	cmpq	%r13, %rdx
	je	.L1794
	movq	%r12, -176(%rbp)
	movq	%r15, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L1811:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L1795
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1796
	movq	88(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L1797
	movq	8(%r12), %rbx
	movq	(%r12), %r14
	cmpq	%r14, %rbx
	jne	.L1801
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L2040:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L1799:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2039
.L1801:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1799
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2040
	call	*%rax
	addq	$8, %r14
	cmpq	%r14, %rbx
	jne	.L1801
	.p2align 4,,10
	.p2align 3
.L2039:
	movq	(%r12), %r14
.L1798:
	testq	%r14, %r14
	je	.L1802
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1802:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1797:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1803
	call	_ZdlPv@PLT
.L1803:
	movq	32(%r15), %r12
	testq	%r12, %r12
	je	.L1804
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1805
	call	_ZdlPv@PLT
.L1805:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1804:
	movq	16(%r15), %r12
	testq	%r12, %r12
	je	.L1806
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1807
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1808
	call	_ZdlPv@PLT
.L1808:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1809
	call	_ZdlPv@PLT
.L1809:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1810
	call	_ZdlPv@PLT
.L1810:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1806:
	movl	$96, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1795:
	addq	$8, %r13
	cmpq	%r13, -160(%rbp)
	jne	.L1811
	movq	-168(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r15
	movq	(%rax), %r13
.L1794:
	testq	%r13, %r13
	je	.L1812
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1812:
	movq	-168(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1773:
	leaq	.LC27(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r13
	cmpq	-136(%rbp), %rdi
	je	.L1813
	call	_ZdlPv@PLT
.L1813:
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	$0x000000000, -104(%rbp)
	testq	%r13, %r13
	je	.L1816
	movq	0(%r13), %rax
	movq	-152(%rbp), %rsi
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L1815
.L1816:
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L1815:
	movsd	-104(%rbp), %xmm0
	movq	-144(%rbp), %rax
	leaq	.LC30(%rip), %rsi
	movq	%r15, %rdi
	movsd	%xmm0, 16(%rax)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r13
	cmpq	-136(%rbp), %rdi
	je	.L1817
	call	_ZdlPv@PLT
.L1817:
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	$0x000000000, -104(%rbp)
	testq	%r13, %r13
	je	.L1820
	movq	0(%r13), %rax
	movq	-152(%rbp), %rsi
	movq	%r13, %rdi
	call	*40(%rax)
	testb	%al, %al
	jne	.L1819
.L1820:
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L1819:
	movsd	-104(%rbp), %xmm0
	movq	-144(%rbp), %rax
	leaq	.LC31(%rip), %rsi
	movq	%r15, %rdi
	movsd	%xmm0, 24(%rax)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-136(%rbp), %rdi
	je	.L1821
	call	_ZdlPv@PLT
.L1821:
	testq	%r14, %r14
	je	.L1822
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	cmpl	$7, 8(%r14)
	jne	.L2041
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movabsq	$2305843009213693951, %rdx
	movq	$0, 16(%rax)
	movq	%rax, %r13
	movups	%xmm0, (%rax)
	movq	24(%r14), %rax
	subq	16(%r14), %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	ja	.L1845
	testq	%rax, %rax
	jne	.L1826
.L1831:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L1824
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1839
	call	_ZdlPv@PLT
.L1839:
	movq	%r13, %rdi
	movl	$24, %esi
	xorl	%r13d, %r13d
	call	_ZdlPvm@PLT
.L1824:
	movq	-144(%rbp), %rax
	movq	32(%rax), %r14
	movq	%r13, 32(%rax)
	testq	%r14, %r14
	je	.L1822
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1840
	call	_ZdlPv@PLT
.L1840:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1822:
	leaq	.LC32(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-136(%rbp), %rdi
	je	.L1841
	call	_ZdlPv@PLT
.L1841:
	testq	%r14, %r14
	je	.L1842
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	cmpl	$7, 8(%r14)
	jne	.L2042
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movabsq	$2305843009213693951, %rdx
	movq	$0, 16(%rax)
	movq	%rax, %r13
	movups	%xmm0, (%rax)
	movq	24(%r14), %rax
	subq	16(%r14), %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	ja	.L1845
	testq	%rax, %rax
	jne	.L1846
.L1851:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L1844
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1859
	call	_ZdlPv@PLT
.L1859:
	movq	%r13, %rdi
	movl	$24, %esi
	xorl	%r13d, %r13d
	call	_ZdlPvm@PLT
.L1844:
	movq	-144(%rbp), %rax
	movq	40(%rax), %r14
	movq	%r13, 40(%rax)
	testq	%r14, %r14
	je	.L1842
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1860
	call	_ZdlPv@PLT
.L1860:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1842:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	movq	-120(%rbp), %rax
	jne	.L1861
	movq	-144(%rbp), %rcx
	movq	%rcx, (%rax)
	jmp	.L1766
	.p2align 4,,10
	.p2align 3
.L1861:
	movq	$0, (%rax)
	movq	-144(%rbp), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev(%rip), %rdx
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2043
	movq	-144(%rbp), %rax
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rcx
	movq	40(%rax), %r12
	movq	%rcx, (%rax)
	testq	%r12, %r12
	je	.L1862
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1863
	call	_ZdlPv@PLT
.L1863:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1862:
	movq	-144(%rbp), %rax
	movq	32(%rax), %r12
	testq	%r12, %r12
	je	.L1864
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1865
	call	_ZdlPv@PLT
.L1865:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1864:
	movq	-144(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L1866
	movq	8(%rax), %rdx
	movq	(%rax), %r12
	movq	%rdx, -128(%rbp)
	cmpq	%r12, %rdx
	je	.L1867
	.p2align 4,,10
	.p2align 3
.L1884:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L1868
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1869
	movq	88(%r15), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r14, %r14
	je	.L1870
	movq	8(%r14), %rbx
	movq	(%r14), %r13
	cmpq	%r13, %rbx
	jne	.L1874
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L2045:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L1872:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L2044
.L1874:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1872
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2045
	call	*%rax
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1874
	.p2align 4,,10
	.p2align 3
.L2044:
	movq	(%r14), %r13
.L1871:
	testq	%r13, %r13
	je	.L1875
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1875:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1870:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1876
	call	_ZdlPv@PLT
.L1876:
	movq	32(%r15), %r13
	testq	%r13, %r13
	je	.L1877
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1878
	call	_ZdlPv@PLT
.L1878:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1877:
	movq	16(%r15), %r13
	testq	%r13, %r13
	je	.L1879
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1880
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	104(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1881
	call	_ZdlPv@PLT
.L1881:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1882
	call	_ZdlPv@PLT
.L1882:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1883
	call	_ZdlPv@PLT
.L1883:
	movl	$136, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1879:
	movl	$96, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1868:
	addq	$8, %r12
	cmpq	%r12, -128(%rbp)
	jne	.L1884
	movq	-136(%rbp), %rax
	movq	(%rax), %r12
.L1867:
	testq	%r12, %r12
	je	.L1885
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1885:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1866:
	movq	-144(%rbp), %rdi
	movl	$48, %esi
	call	_ZdlPvm@PLT
	jmp	.L1766
	.p2align 4,,10
	.p2align 3
.L1796:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1795
	.p2align 4,,10
	.p2align 3
.L1776:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1775
	.p2align 4,,10
	.p2align 3
.L1869:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1868
	.p2align 4,,10
	.p2align 3
.L1787:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1786
	.p2align 4,,10
	.p2align 3
.L1807:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1846:
	leaq	0(,%rax,4), %rbx
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	0(%r13), %r8
	movq	8(%r13), %rdx
	movq	%rax, %rcx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L2046
	testq	%r8, %r8
	jne	.L1849
.L1850:
	movq	%rcx, %xmm0
	leaq	(%rcx,%rbx), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 16(%r13)
	movups	%xmm0, 0(%r13)
	movq	16(%r14), %rax
	cmpq	%rax, 24(%r14)
	je	.L1851
	xorl	%ebx, %ebx
	jmp	.L1858
	.p2align 4,,10
	.p2align 3
.L2047:
	movl	%eax, (%rsi)
	addq	$4, 8(%r13)
.L2035:
	movq	24(%r14), %rax
	subq	16(%r14), %rax
	addq	$1, %rbx
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jbe	.L1851
.L1858:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1852
	call	_ZdlPv@PLT
.L1852:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol9ListValue2atEm@PLT
	movl	$0, -104(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1855
	movq	(%rax), %rax
	movq	-152(%rbp), %rsi
	call	*48(%rax)
	testb	%al, %al
	jne	.L1854
.L1855:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L1854:
	movl	-104(%rbp), %eax
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	jne	.L2047
	movq	-152(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L2035
	.p2align 4,,10
	.p2align 3
.L1826:
	leaq	0(,%rax,4), %rbx
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	0(%r13), %r8
	movq	8(%r13), %rdx
	movq	%rax, %rcx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L2048
	testq	%r8, %r8
	jne	.L1829
.L1830:
	movq	%rcx, %xmm0
	leaq	(%rcx,%rbx), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 16(%r13)
	movups	%xmm0, 0(%r13)
	movq	24(%r14), %rax
	cmpq	%rax, 16(%r14)
	je	.L1831
	xorl	%ebx, %ebx
	jmp	.L1838
	.p2align 4,,10
	.p2align 3
.L2049:
	movl	%eax, (%rsi)
	addq	$4, 8(%r13)
.L2033:
	movq	24(%r14), %rax
	subq	16(%r14), %rax
	addq	$1, %rbx
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jbe	.L1831
.L1838:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1832
	call	_ZdlPv@PLT
.L1832:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol9ListValue2atEm@PLT
	movl	$0, -104(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1835
	movq	(%rax), %rax
	movq	-152(%rbp), %rsi
	call	*48(%rax)
	testb	%al, %al
	jne	.L1834
.L1835:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L1834:
	movl	-104(%rbp), %eax
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	jne	.L2049
	movq	-152(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L1880:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1879
	.p2align 4,,10
	.p2align 3
.L2042:
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L2041:
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L1824
	.p2align 4,,10
	.p2align 3
.L2043:
	movq	-144(%rbp), %rdi
	call	*%rax
	jmp	.L1766
.L2048:
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%r8, -160(%rbp)
	call	memmove@PLT
	movq	-160(%rbp), %r8
	movq	%rax, %rcx
.L1829:
	movq	%r8, %rdi
	movq	%rcx, -160(%rbp)
	call	_ZdlPv@PLT
	movq	-160(%rbp), %rcx
	jmp	.L1830
.L2046:
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%r8, -128(%rbp)
	call	memmove@PLT
	movq	-128(%rbp), %r8
	movq	%rax, %rcx
.L1849:
	movq	%r8, %rdi
	movq	%rcx, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rcx
	jmp	.L1850
.L2036:
	call	__stack_chk_fail@PLT
.L1845:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6482:
	.size	_ZN12v8_inspector8protocol8Profiler7Profile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler7Profile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC33:
	.string	"profile"
	.section	.text._ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2051
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r12
	je	.L2052
.L2051:
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-120(%rbp), %rax
	movq	$0, (%rax)
.L2050:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2325
	movq	-120(%rbp), %rax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2052:
	.cfi_restore_state
	movl	$112, %edi
	call	_Znwm@PLT
	xorl	%edi, %edi
	pxor	%xmm0, %xmm0
	cmpl	$6, 8(%r12)
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	movq	%rax, -160(%rbp)
	movq	%rax, 8(%rbx)
	leaq	88(%rbx), %rax
	movw	%di, 24(%rbx)
	movq	%r13, %rdi
	movq	%rax, 72(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movb	$0, 64(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 80(%rbx)
	movups	%xmm0, 48(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 88(%rbx)
	movq	%rax, -168(%rbp)
	movl	$0, %eax
	cmovne	%rax, %r12
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	-96(%rbp), %rax
	leaq	.LC16(%rip), %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdi
	je	.L2055
	call	_ZdlPv@PLT
.L2055:
	leaq	.LC16(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rax
	xorl	%esi, %esi
	movq	$0, -88(%rbp)
	movw	%si, -80(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -64(%rbp)
	testq	%r14, %r14
	je	.L2058
	movq	(%r14), %rax
	movq	-136(%rbp), %rsi
	movq	%r14, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L2057
.L2058:
	leaq	.LC13(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L2057:
	movq	8(%rbx), %rdi
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rax
	cmpq	-128(%rbp), %rdx
	je	.L2326
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -160(%rbp)
	je	.L2327
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	24(%rbx), %rsi
	movq	%rdx, 8(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%rbx)
	testq	%rdi, %rdi
	je	.L2064
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L2062:
	xorl	%edx, %edx
	movq	$0, -88(%rbp)
	movw	%dx, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 40(%rbx)
	cmpq	-128(%rbp), %rdi
	je	.L2065
	call	_ZdlPv@PLT
.L2065:
	movq	-136(%rbp), %r14
	leaq	.LC17(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-128(%rbp), %rdi
	je	.L2066
	call	_ZdlPv@PLT
.L2066:
	leaq	.LC17(%rip), %rsi
	movq	%r13, %rdi
	leaq	-104(%rbp), %r15
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol8Debugger8Location9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE@PLT
	movq	-104(%rbp), %rax
	movq	48(%rbx), %r14
	movq	$0, -104(%rbp)
	movq	%rax, 48(%rbx)
	testq	%r14, %r14
	je	.L2068
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2069
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r14), %rdi
	movq	%rax, (%r14)
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2070
	call	_ZdlPv@PLT
.L2070:
	movl	$64, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2071:
	movq	-104(%rbp), %r14
	testq	%r14, %r14
	je	.L2068
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2073
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r14), %rdi
	movq	%rax, (%r14)
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2074
	call	_ZdlPv@PLT
.L2074:
	movl	$64, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2068:
	movq	-136(%rbp), %r14
	leaq	.LC33(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-128(%rbp), %rdi
	je	.L2075
	call	_ZdlPv@PLT
.L2075:
	leaq	.LC33(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol8Profiler7Profile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	56(%rbx), %rcx
	movq	$0, -104(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rax, 56(%rbx)
	testq	%rcx, %rcx
	je	.L2077
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2078
	movq	40(%rcx), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%r14, %r14
	je	.L2079
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2080
	call	_ZdlPv@PLT
.L2080:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2079:
	movq	-144(%rbp), %rax
	movq	32(%rax), %r14
	testq	%r14, %r14
	je	.L2081
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2082
	call	_ZdlPv@PLT
.L2082:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2081:
	movq	-144(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L2083
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -152(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L2084
	movq	%r12, -192(%rbp)
	movq	%r13, -200(%rbp)
	movq	%rbx, -184(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2101:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L2085
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2086
	movq	88(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2087
	movq	8(%r14), %r12
	movq	(%r14), %r15
	cmpq	%r15, %r12
	jne	.L2091
	jmp	.L2088
	.p2align 4,,10
	.p2align 3
.L2329:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L2089:
	addq	$8, %r15
	cmpq	%r15, %r12
	je	.L2328
.L2091:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2089
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2329
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %r12
	jne	.L2091
	.p2align 4,,10
	.p2align 3
.L2328:
	movq	(%r14), %r15
.L2088:
	testq	%r15, %r15
	je	.L2092
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2092:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2087:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2093
	call	_ZdlPv@PLT
.L2093:
	movq	32(%r13), %r12
	testq	%r12, %r12
	je	.L2094
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2095
	call	_ZdlPv@PLT
.L2095:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2094:
	movq	16(%r13), %r12
	testq	%r12, %r12
	je	.L2096
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2097
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2098
	call	_ZdlPv@PLT
.L2098:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2099
	call	_ZdlPv@PLT
.L2099:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2100
	call	_ZdlPv@PLT
.L2100:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2096:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2085:
	addq	$8, %rbx
	cmpq	%rbx, -152(%rbp)
	jne	.L2101
	movq	-176(%rbp), %rax
	movq	-184(%rbp), %rbx
	movq	-192(%rbp), %r12
	movq	-200(%rbp), %r13
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2084:
	testq	%rdi, %rdi
	je	.L2102
	call	_ZdlPv@PLT
.L2102:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2083:
	movq	-144(%rbp), %rdi
	movl	$48, %esi
	call	_ZdlPvm@PLT
.L2103:
	movq	-104(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	testq	%rcx, %rcx
	je	.L2077
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2105
	movq	40(%rcx), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%r14, %r14
	je	.L2106
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2107
	call	_ZdlPv@PLT
.L2107:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2106:
	movq	-152(%rbp), %rax
	movq	32(%rax), %r14
	testq	%r14, %r14
	je	.L2108
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2109
	call	_ZdlPv@PLT
.L2109:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2108:
	movq	-152(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L2110
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -144(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L2111
	movq	%r12, -192(%rbp)
	movq	%r13, -200(%rbp)
	movq	%rbx, -184(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2128:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L2112
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2113
	movq	88(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2114
	movq	8(%r14), %r12
	movq	(%r14), %r15
	cmpq	%r15, %r12
	jne	.L2118
	jmp	.L2115
	.p2align 4,,10
	.p2align 3
.L2331:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L2116:
	addq	$8, %r15
	cmpq	%r15, %r12
	je	.L2330
.L2118:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2116
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2331
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %r12
	jne	.L2118
	.p2align 4,,10
	.p2align 3
.L2330:
	movq	(%r14), %r15
.L2115:
	testq	%r15, %r15
	je	.L2119
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2119:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2114:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2120
	call	_ZdlPv@PLT
.L2120:
	movq	32(%r13), %r12
	testq	%r12, %r12
	je	.L2121
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2122
	call	_ZdlPv@PLT
.L2122:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2121:
	movq	16(%r13), %r12
	testq	%r12, %r12
	je	.L2123
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2124
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2125
	call	_ZdlPv@PLT
.L2125:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2126
	call	_ZdlPv@PLT
.L2126:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2127
	call	_ZdlPv@PLT
.L2127:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2123:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2112:
	addq	$8, %rbx
	cmpq	%rbx, -144(%rbp)
	jne	.L2128
	movq	-176(%rbp), %rax
	movq	-184(%rbp), %rbx
	movq	-192(%rbp), %r12
	movq	-200(%rbp), %r13
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2111:
	testq	%rdi, %rdi
	je	.L2129
	call	_ZdlPv@PLT
.L2129:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2110:
	movq	-152(%rbp), %rdi
	movl	$48, %esi
	call	_ZdlPvm@PLT
.L2077:
	movq	-136(%rbp), %r15
	leaq	.LC18(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	cmpq	-128(%rbp), %rdi
	je	.L2130
	call	_ZdlPv@PLT
.L2130:
	testq	%r12, %r12
	je	.L2131
	leaq	.LC18(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rax
	movq	%r12, %rdi
	movq	$0, -88(%rbp)
	movq	$0, -64(%rbp)
	movq	-136(%rbp), %rsi
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	movw	%ax, -80(%rbp)
	movq	(%r12), %rax
	call	*56(%rax)
	testb	%al, %al
	je	.L2332
.L2132:
	movq	-136(%rbp), %rsi
	leaq	72(%rbx), %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movb	$1, 64(%rbx)
	movq	-96(%rbp), %rdi
	movq	%rax, 104(%rbx)
	cmpq	-128(%rbp), %rdi
	je	.L2131
	call	_ZdlPv@PLT
.L2131:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L2333
	movq	-120(%rbp), %rax
	movq	%rbx, (%rax)
	jmp	.L2050
	.p2align 4,,10
	.p2align 3
.L2332:
	leaq	.LC13(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L2132
	.p2align 4,,10
	.p2align 3
.L2333:
	movq	-120(%rbp), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD0Ev(%rip), %rdx
	movq	$0, (%rax)
	movq	(%rbx), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2334
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationE(%rip), %rax
	movq	72(%rbx), %rdi
	movq	%rax, (%rbx)
	cmpq	%rdi, -168(%rbp)
	je	.L2134
	call	_ZdlPv@PLT
.L2134:
	movq	56(%rbx), %r13
	testq	%r13, %r13
	je	.L2135
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2136
	movq	40(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L2137
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2138
	call	_ZdlPv@PLT
.L2138:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2137:
	movq	32(%r13), %r12
	testq	%r12, %r12
	je	.L2139
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2140
	call	_ZdlPv@PLT
.L2140:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2139:
	movq	8(%r13), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L2141
	movq	8(%rax), %rdx
	movq	(%rax), %r12
	movq	%rdx, -128(%rbp)
	cmpq	%r12, %rdx
	je	.L2142
	movq	%rbx, -144(%rbp)
	movq	%r13, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L2159:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L2143
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2144
	movq	88(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2145
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	jne	.L2149
	jmp	.L2146
	.p2align 4,,10
	.p2align 3
.L2336:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L2147:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L2335
.L2149:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2147
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2336
	call	*%rax
	jmp	.L2147
	.p2align 4,,10
	.p2align 3
.L2335:
	movq	(%r14), %r15
.L2146:
	testq	%r15, %r15
	je	.L2150
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2150:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2145:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2151
	call	_ZdlPv@PLT
.L2151:
	movq	32(%r13), %r14
	testq	%r14, %r14
	je	.L2152
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2153
	call	_ZdlPv@PLT
.L2153:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2152:
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L2154
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2155
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r14), %rdi
	movq	%rax, (%r14)
	leaq	104(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2156
	call	_ZdlPv@PLT
.L2156:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2157
	call	_ZdlPv@PLT
.L2157:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2158
	call	_ZdlPv@PLT
.L2158:
	movl	$136, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2154:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2143:
	addq	$8, %r12
	cmpq	%r12, -128(%rbp)
	jne	.L2159
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rbx
	movq	-152(%rbp), %r13
	movq	(%rax), %r12
.L2142:
	testq	%r12, %r12
	je	.L2160
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2160:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2141:
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2135:
	movq	48(%rbx), %r12
	testq	%r12, %r12
	je	.L2161
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2162
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2163
	call	_ZdlPv@PLT
.L2163:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2161:
	movq	8(%rbx), %rdi
	cmpq	%rdi, -160(%rbp)
	je	.L2164
	call	_ZdlPv@PLT
.L2164:
	movl	$112, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2050
	.p2align 4,,10
	.p2align 3
.L2326:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L2060
	cmpq	$1, %rax
	je	.L2337
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L2060
	movq	-128(%rbp), %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	8(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L2060:
	xorl	%ecx, %ecx
	movq	%rax, 16(%rbx)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2086:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2113:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2112
	.p2align 4,,10
	.p2align 3
.L2327:
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, 8(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 16(%rbx)
.L2064:
	movq	-128(%rbp), %rax
	movq	%rax, -96(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2124:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2123
	.p2align 4,,10
	.p2align 3
.L2144:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2143
	.p2align 4,,10
	.p2align 3
.L2097:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2096
	.p2align 4,,10
	.p2align 3
.L2069:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2334:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2050
	.p2align 4,,10
	.p2align 3
.L2105:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2077
	.p2align 4,,10
	.p2align 3
.L2078:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2103
	.p2align 4,,10
	.p2align 3
.L2073:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2068
	.p2align 4,,10
	.p2align 3
.L2155:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2162:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2161
	.p2align 4,,10
	.p2align 3
.L2136:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2135
.L2337:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	8(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L2060
.L2325:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6510:
	.size	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB12040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movabsq	$1152921504606846975, %rdi
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L2365
	movq	%rsi, %r13
	movq	%rsi, %rbx
	subq	%r12, %rsi
	testq	%rax, %rax
	je	.L2352
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L2366
.L2340:
	movq	%r14, %rdi
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdx
	movq	%rax, -56(%rbp)
	addq	%rax, %r14
	movq	%r14, -64(%rbp)
	leaq	8(%rax), %r14
.L2351:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rcx
	movq	$0, (%rdx)
	movq	%rax, (%rcx,%rsi)
	cmpq	%r12, %r13
	je	.L2342
	movq	%rcx, %r14
	movq	%r12, %r15
	jmp	.L2345
	.p2align 4,,10
	.p2align 3
.L2368:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2343:
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	je	.L2367
.L2345:
	movq	(%r15), %rsi
	movq	$0, (%r15)
	movq	%rsi, (%r14)
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2343
	movq	(%rdi), %rsi
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rax
	movq	24(%rsi), %rsi
	cmpq	%rax, %rsi
	je	.L2368
	call	*%rsi
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	jne	.L2345
	.p2align 4,,10
	.p2align 3
.L2367:
	movq	-56(%rbp), %rcx
	movq	%r13, %rax
	subq	%r12, %rax
	leaq	8(%rcx,%rax), %r14
.L2342:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r13
	je	.L2346
	movq	%rax, %r15
	subq	%r13, %r15
	leaq	-8(%r15), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	addq	$1, %rsi
	testq	%rdi, %rdi
	je	.L2354
	movq	%rsi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L2348:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2348
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%r14,%r8), %rdx
	leaq	0(%r13,%r8), %rbx
	cmpq	%rax, %rsi
	je	.L2349
.L2347:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L2349:
	leaq	8(%r14,%rdi), %r14
.L2346:
	testq	%r12, %r12
	je	.L2350
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2350:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%r14, %xmm2
	movq	-64(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2366:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L2341
	movq	$0, -64(%rbp)
	movl	$8, %r14d
	movq	$0, -56(%rbp)
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2352:
	movl	$8, %r14d
	jmp	.L2340
.L2354:
	movq	%r14, %rdx
	jmp	.L2347
.L2341:
	cmpq	%rdi, %r9
	cmovbe	%r9, %rdi
	leaq	0(,%rdi,8), %r14
	jmp	.L2340
.L2365:
	leaq	.LC19(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12040:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.rodata._ZN12v8_inspector8protocol8Profiler16FunctionCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC34:
	.string	"functionName"
.LC35:
	.string	"ranges"
.LC36:
	.string	"isBlockCoverage"
	.section	.text._ZN12v8_inspector8protocol8Profiler16FunctionCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler16FunctionCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2370
	cmpl	$6, 8(%rsi)
	je	.L2371
.L2370:
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-120(%rbp), %rax
	movq	$0, (%rax)
.L2369:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2489
	movq	-120(%rbp), %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2371:
	.cfi_restore_state
	movl	$64, %edi
	leaq	-96(%rbp), %r15
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rcx, (%rax)
	leaq	24(%rax), %rcx
	movq	%rcx, 8(%rax)
	movq	%rcx, -160(%rbp)
	movq	-136(%rbp), %rcx
	movw	%r8w, 24(%rax)
	cmpl	$6, 8(%rcx)
	movq	%rax, -144(%rbp)
	movq	$0, 16(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movb	$0, 56(%rax)
	movl	$0, %eax
	cmove	%rcx, %rax
	movq	%rax, %rbx
	movq	%rax, -136(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC34(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r13
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdi
	je	.L2374
	call	_ZdlPv@PLT
.L2374:
	movq	%r12, %rdi
	leaq	.LC34(%rip), %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %rax
	xorl	%edi, %edi
	movq	$0, -88(%rbp)
	movw	%di, -80(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -64(%rbp)
	testq	%r13, %r13
	je	.L2377
	movq	0(%r13), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L2376
.L2377:
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L2376:
	movq	-144(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	8(%rax), %rdi
	movq	-88(%rbp), %rax
	cmpq	-128(%rbp), %rdx
	je	.L2490
	movq	-80(%rbp), %rcx
	movq	-144(%rbp), %rbx
	cmpq	%rdi, -160(%rbp)
	je	.L2491
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	24(%rbx), %rsi
	movq	%rdx, 8(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%rbx)
	testq	%rdi, %rdi
	je	.L2383
	movq	%rdi, -96(%rbp)
	movq	%rbx, %rcx
	movq	%rsi, -80(%rbp)
.L2381:
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movw	%ax, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 40(%rcx)
	cmpq	-128(%rbp), %rdi
	je	.L2384
	call	_ZdlPv@PLT
.L2384:
	leaq	.LC35(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	-128(%rbp), %rdi
	je	.L2385
	call	_ZdlPv@PLT
.L2385:
	leaq	.LC35(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	testq	%r14, %r14
	je	.L2386
	cmpl	$7, 8(%r14)
	jne	.L2386
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, %r13
	movups	%xmm0, (%rax)
	movq	24(%r14), %rbx
	subq	16(%r14), %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	testq	%rbx, %rbx
	js	.L2492
	testq	%rax, %rax
	jne	.L2390
.L2397:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L2388
	movq	8(%r13), %rbx
	movq	0(%r13), %r14
	cmpq	%r14, %rbx
	jne	.L2407
	jmp	.L2404
	.p2align 4,,10
	.p2align 3
.L2494:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2405:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2493
.L2407:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2405
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2494
	call	*%rax
	jmp	.L2405
	.p2align 4,,10
	.p2align 3
.L2386:
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L2388:
	movq	-144(%rbp), %rax
	movq	48(%rax), %r14
	movq	%r13, 48(%rax)
	testq	%r14, %r14
	je	.L2409
	movq	8(%r14), %rbx
	movq	(%r14), %r13
	cmpq	%r13, %rbx
	jne	.L2413
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2496:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2411:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L2495
.L2413:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2411
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2496
	call	*%rax
	jmp	.L2411
	.p2align 4,,10
	.p2align 3
.L2495:
	movq	(%r14), %r13
.L2410:
	testq	%r13, %r13
	je	.L2414
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2414:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2409:
	leaq	.LC36(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r13
	cmpq	-128(%rbp), %rdi
	je	.L2415
	call	_ZdlPv@PLT
.L2415:
	leaq	.LC36(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movb	$0, -104(%rbp)
	testq	%r13, %r13
	je	.L2418
	movq	0(%r13), %rax
	leaq	-104(%rbp), %rsi
	movq	%r13, %rdi
	call	*32(%rax)
	testb	%al, %al
	jne	.L2417
.L2418:
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L2417:
	movzbl	-104(%rbp), %eax
	movq	-144(%rbp), %rdx
	movq	%r12, %rdi
	movb	%al, 56(%rdx)
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L2497
	movq	-120(%rbp), %rax
	movq	-144(%rbp), %rdx
	movq	%rdx, (%rax)
	jmp	.L2369
	.p2align 4,,10
	.p2align 3
.L2497:
	movq	-120(%rbp), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rdx
	movq	$0, (%rax)
	movq	-144(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2498
	movq	-144(%rbp), %rax
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rcx
	movq	48(%rax), %r12
	movq	%rcx, (%rax)
	testq	%r12, %r12
	je	.L2419
	movq	8(%r12), %rbx
	movq	(%r12), %r14
	cmpq	%r14, %rbx
	je	.L2420
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %r13
	jmp	.L2423
	.p2align 4,,10
	.p2align 3
.L2500:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2421:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2499
.L2423:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2421
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	je	.L2500
	call	*%rax
	jmp	.L2421
	.p2align 4,,10
	.p2align 3
.L2499:
	movq	(%r12), %r14
.L2420:
	testq	%r14, %r14
	je	.L2424
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2424:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2419:
	movq	-144(%rbp), %rax
	movq	8(%rax), %rdi
	cmpq	%rdi, -160(%rbp)
	je	.L2425
	call	_ZdlPv@PLT
.L2425:
	movq	-144(%rbp), %rdi
	movl	$64, %esi
	call	_ZdlPvm@PLT
	jmp	.L2369
	.p2align 4,,10
	.p2align 3
.L2493:
	movq	0(%r13), %r14
.L2404:
	testq	%r14, %r14
	je	.L2408
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2408:
	movq	%r13, %rdi
	movl	$24, %esi
	xorl	%r13d, %r13d
	call	_ZdlPvm@PLT
	jmp	.L2388
	.p2align 4,,10
	.p2align 3
.L2490:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L2379
	cmpq	$1, %rax
	je	.L2501
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L2379
	movq	-128(%rbp), %rsi
	call	memmove@PLT
	movq	-144(%rbp), %rdx
	movq	-88(%rbp), %rax
	movq	8(%rdx), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L2379:
	movq	-144(%rbp), %rcx
	xorl	%esi, %esi
	movq	%rax, 16(%rcx)
	movw	%si, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L2390:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	8(%r13), %rcx
	movq	0(%r13), %r8
	movq	%rax, -152(%rbp)
	cmpq	%r8, %rcx
	je	.L2392
	movq	%r14, -168(%rbp)
	movq	%rcx, %r14
	movq	%rbx, -176(%rbp)
	movq	%rax, %rbx
	movq	%r12, -184(%rbp)
	movq	%r8, %r12
	jmp	.L2395
	.p2align 4,,10
	.p2align 3
.L2503:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2393:
	addq	$8, %r12
	addq	$8, %rbx
	cmpq	%r12, %r14
	je	.L2502
.L2395:
	movq	(%r12), %rdx
	movq	$0, (%r12)
	movq	%rdx, (%rbx)
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2393
	movq	(%rdi), %rdx
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2503
	call	*%rdx
	jmp	.L2393
	.p2align 4,,10
	.p2align 3
.L2502:
	movq	-168(%rbp), %r14
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r12
	movq	0(%r13), %r8
.L2392:
	testq	%r8, %r8
	je	.L2396
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L2396:
	movq	-152(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, 16(%r13)
	movups	%xmm0, 0(%r13)
	movq	16(%r14), %rax
	cmpq	%rax, 24(%r14)
	je	.L2397
	leaq	-104(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%rax, -152(%rbp)
	jmp	.L2403
	.p2align 4,,10
	.p2align 3
.L2504:
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r13)
.L2400:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2401
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2402
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2401:
	movq	24(%r14), %rax
	subq	16(%r14), %rax
	addq	$1, %rbx
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jbe	.L2397
.L2403:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-128(%rbp), %rdi
	je	.L2398
	call	_ZdlPv@PLT
.L2398:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol9ListValue2atEm@PLT
	movq	-152(%rbp), %rdi
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol8Profiler13CoverageRange9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	jne	.L2504
	movq	-152(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler13CoverageRangeESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L2400
	.p2align 4,,10
	.p2align 3
.L2402:
	call	*%rax
	jmp	.L2401
	.p2align 4,,10
	.p2align 3
.L2491:
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, 8(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 16(%rbx)
.L2383:
	movq	-128(%rbp), %rax
	movq	-144(%rbp), %rcx
	movq	%rax, -96(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L2498:
	movq	-144(%rbp), %rdi
	call	*%rax
	jmp	.L2369
.L2501:
	movzwl	-80(%rbp), %eax
	movq	-144(%rbp), %rdx
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	8(%rdx), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L2379
.L2489:
	call	__stack_chk_fail@PLT
.L2492:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6491:
	.size	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler16FunctionCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16FunctionCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16FunctionCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16FunctionCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16FunctionCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16FunctionCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB12073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$1152921504606846975, %rdx
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rdi, -112(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rax, -104(%rbp)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rcx, -72(%rbp)
	cmpq	%rdx, %rax
	je	.L2549
	movq	%rsi, %r14
	movq	%rsi, %r13
	subq	-72(%rbp), %r14
	testq	%rax, %rax
	je	.L2526
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L2550
.L2507:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, -88(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -96(%rbp)
	leaq	8(%rax), %rbx
.L2525:
	movq	(%r12), %rax
	movq	$0, (%r12)
	movq	-88(%rbp), %r12
	movq	%rax, (%r12,%r14)
	movq	-72(%rbp), %rax
	cmpq	%rax, %r13
	je	.L2509
	movq	%r13, -56(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2519:
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%rax, (%r12)
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L2510
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2511
	movq	48(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2512
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2513
	movq	%rbx, -80(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	jmp	.L2516
	.p2align 4,,10
	.p2align 3
.L2552:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2514:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2551
.L2516:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2514
	movq	(%rdi), %rdx
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2552
	call	*%rdx
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.L2516
	.p2align 4,,10
	.p2align 3
.L2551:
	movq	-80(%rbp), %rbx
	movq	(%r14), %r15
.L2513:
	testq	%r15, %r15
	je	.L2517
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2517:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2512:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2518
	call	_ZdlPv@PLT
.L2518:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2510:
	addq	$8, %rbx
	addq	$8, %r12
	cmpq	%rbx, -56(%rbp)
	jne	.L2519
.L2553:
	movq	-56(%rbp), %r13
	movq	-88(%rbp), %rcx
	movq	%r13, %rax
	subq	-72(%rbp), %rax
	leaq	8(%rcx,%rax), %rbx
.L2509:
	movq	-104(%rbp), %rax
	cmpq	%rax, %r13
	je	.L2520
	subq	%r13, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L2528
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L2522:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2522
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	leaq	(%rbx,%rdi), %rdx
	addq	%r13, %rdi
	movq	%rdi, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L2523
.L2521:
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L2523:
	leaq	8(%rbx,%rsi), %rbx
.L2520:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L2524
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2524:
	movq	-88(%rbp), %xmm0
	movq	-112(%rbp), %rax
	movq	%rbx, %xmm2
	movq	-96(%rbp), %rsi
	punpcklqdq	%xmm2, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2511:
	.cfi_restore_state
	movq	%r13, %rdi
	addq	$8, %rbx
	addq	$8, %r12
	call	*%rax
	cmpq	%rbx, -56(%rbp)
	jne	.L2519
	jmp	.L2553
	.p2align 4,,10
	.p2align 3
.L2550:
	testq	%rcx, %rcx
	jne	.L2508
	movq	$0, -96(%rbp)
	movl	$8, %ebx
	movq	$0, -88(%rbp)
	jmp	.L2525
	.p2align 4,,10
	.p2align 3
.L2526:
	movl	$8, %ebx
	jmp	.L2507
.L2528:
	movq	%rbx, %rdx
	jmp	.L2521
.L2508:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L2507
.L2549:
	leaq	.LC19(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12073:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16FunctionCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16FunctionCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler16FunctionCoverageESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"axG",@progbits,_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler16FunctionCoverageESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler16FunctionCoverageESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler16FunctionCoverageESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler16FunctionCoverageESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB8475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2555
	cmpl	$7, 8(%rsi)
	movq	%rsi, %r12
	jne	.L2555
	movq	%rdx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, %r15
	movups	%xmm0, (%rax)
	movq	24(%r12), %rax
	subq	16(%r12), %rax
	movq	%rax, %rcx
	movq	%rax, -136(%rbp)
	sarq	$3, %rax
	testq	%rcx, %rcx
	js	.L2654
	testq	%rax, %rax
	jne	.L2559
.L2574:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L2655
	movq	-152(%rbp), %rax
	movq	(%r15), %r12
	movq	$0, (%rax)
	movq	8(%r15), %rax
	movq	%rax, -120(%rbp)
	cmpq	%r12, %rax
	je	.L2588
	movq	%r15, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L2589:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L2590
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2591
	movq	48(%r15), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L2592
	movq	8(%r13), %rbx
	movq	0(%r13), %r14
	cmpq	%r14, %rbx
	jne	.L2596
	jmp	.L2593
	.p2align 4,,10
	.p2align 3
.L2657:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2594:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2656
.L2596:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2594
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2657
	call	*%rax
	addq	$8, %r14
	cmpq	%r14, %rbx
	jne	.L2596
	.p2align 4,,10
	.p2align 3
.L2656:
	movq	0(%r13), %r14
.L2593:
	testq	%r14, %r14
	je	.L2597
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2597:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2592:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2598
	call	_ZdlPv@PLT
.L2598:
	movl	$64, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2590:
	addq	$8, %r12
	cmpq	%r12, -120(%rbp)
	jne	.L2589
.L2663:
	movq	-128(%rbp), %r15
	movq	(%r15), %r12
.L2588:
	testq	%r12, %r12
	je	.L2599
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2599:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2554:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2658
	movq	-152(%rbp), %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2559:
	.cfi_restore_state
	movq	%rcx, %rdi
	call	_Znwm@PLT
	movq	8(%r15), %rcx
	movq	(%r15), %r14
	movq	%rax, -144(%rbp)
	movq	%rcx, -120(%rbp)
	cmpq	%r14, %rcx
	je	.L2562
	movq	%r15, -160(%rbp)
	movq	%r13, -176(%rbp)
	movq	%r12, -168(%rbp)
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L2572:
	movq	(%r14), %rax
	movq	$0, (%r14)
	movq	%rax, (%r12)
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L2563
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2564
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, (%r15)
	movq	48(%r15), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L2565
	movq	8(%rax), %rbx
	movq	(%rax), %r13
	cmpq	%r13, %rbx
	jne	.L2569
	jmp	.L2566
	.p2align 4,,10
	.p2align 3
.L2660:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2567:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L2659
.L2569:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2567
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2660
	call	*%rax
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L2569
	.p2align 4,,10
	.p2align 3
.L2659:
	movq	-128(%rbp), %rax
	movq	(%rax), %r13
.L2566:
	testq	%r13, %r13
	je	.L2570
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2570:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2565:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2571
	call	_ZdlPv@PLT
.L2571:
	movl	$64, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2563:
	addq	$8, %r14
	addq	$8, %r12
	cmpq	%r14, -120(%rbp)
	jne	.L2572
	movq	-160(%rbp), %r15
	movq	-168(%rbp), %r12
	movq	-176(%rbp), %r13
	movq	(%r15), %r14
.L2562:
	testq	%r14, %r14
	je	.L2573
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2573:
	movq	-144(%rbp), %rax
	movq	%rax, %xmm0
	addq	-136(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 16(%r15)
	movq	16(%r12), %rax
	movups	%xmm0, (%r15)
	cmpq	%rax, 24(%r12)
	je	.L2574
	leaq	-96(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%rax, -136(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -144(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L2587:
	movq	-136(%rbp), %r14
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-144(%rbp), %rdi
	je	.L2575
	call	_ZdlPv@PLT
.L2575:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol9ListValue2atEm@PLT
	movq	-120(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	8(%r15), %rsi
	cmpq	16(%r15), %rsi
	je	.L2576
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r15)
.L2577:
	movq	-104(%rbp), %r14
	testq	%r14, %r14
	je	.L2578
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2579
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, (%r14)
	movq	48(%r14), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L2580
	movq	8(%rax), %rcx
	movq	(%rax), %r8
	cmpq	%r8, %rcx
	je	.L2581
	movq	%rbx, -160(%rbp)
	movq	%r8, %rbx
	movq	%r12, -168(%rbp)
	movq	%rcx, %r12
	jmp	.L2584
	.p2align 4,,10
	.p2align 3
.L2662:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2582:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	je	.L2661
.L2584:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2582
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2662
	call	*%rax
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L2584
	.p2align 4,,10
	.p2align 3
.L2661:
	movq	-128(%rbp), %rax
	movq	-160(%rbp), %rbx
	movq	-168(%rbp), %r12
	movq	(%rax), %r8
.L2581:
	testq	%r8, %r8
	je	.L2585
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L2585:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2580:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2586
	call	_ZdlPv@PLT
.L2586:
	movl	$64, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2578:
	movq	24(%r12), %rax
	subq	16(%r12), %rax
	addq	$1, %rbx
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L2587
	jmp	.L2574
	.p2align 4,,10
	.p2align 3
.L2576:
	movq	-120(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16FunctionCoverageESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L2577
	.p2align 4,,10
	.p2align 3
.L2579:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2578
	.p2align 4,,10
	.p2align 3
.L2591:
	movq	%r15, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -120(%rbp)
	jne	.L2589
	jmp	.L2663
	.p2align 4,,10
	.p2align 3
.L2555:
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-152(%rbp), %rax
	movq	$0, (%rax)
	jmp	.L2554
	.p2align 4,,10
	.p2align 3
.L2655:
	movq	-152(%rbp), %rax
	movq	%r15, (%rax)
	jmp	.L2554
	.p2align 4,,10
	.p2align 3
.L2564:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2563
.L2658:
	call	__stack_chk_fail@PLT
.L2654:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8475:
	.size	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler16FunctionCoverageESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler16FunctionCoverageESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Profiler14ScriptCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC37:
	.string	"scriptId"
.LC38:
	.string	"url"
.LC39:
	.string	"functions"
	.section	.text._ZN12v8_inspector8protocol8Profiler14ScriptCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler14ScriptCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2665
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r12
	je	.L2666
.L2665:
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-120(%rbp), %rax
	movq	$0, (%rax)
.L2664:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2826
	movq	-120(%rbp), %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2666:
	.cfi_restore_state
	movl	$96, %edi
	xorl	%ebx, %ebx
	leaq	-96(%rbp), %r15
	call	_Znwm@PLT
	xorl	%r11d, %r11d
	cmpl	$6, 8(%r12)
	movq	%r13, %rdi
	movq	%rax, %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE(%rip), %rax
	movq	%rax, (%r14)
	leaq	24(%r14), %rax
	movq	%rax, -136(%rbp)
	movq	%rax, 8(%r14)
	leaq	64(%r14), %rax
	movw	%r11w, 24(%r14)
	movq	%rax, 48(%r14)
	movw	%bx, 64(%r14)
	leaq	-80(%rbp), %rbx
	movq	$0, 16(%r14)
	movq	$0, 40(%r14)
	movq	$0, 56(%r14)
	movq	$0, 80(%r14)
	movq	$0, 88(%r14)
	movq	%rax, -144(%rbp)
	movl	$0, %eax
	cmovne	%rax, %r12
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC37(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%rbx, %rdi
	je	.L2669
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r8
.L2669:
	leaq	.LC37(%rip), %rsi
	movq	%r13, %rdi
	movq	%r8, -128(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %r8
	xorl	%r10d, %r10d
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movw	%r10w, -80(%rbp)
	movq	$0, -64(%rbp)
	testq	%r8, %r8
	je	.L2672
	movq	(%r8), %rax
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L2671
.L2672:
	leaq	.LC13(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L2671:
	movq	-96(%rbp), %rdx
	movq	8(%r14), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rdx
	je	.L2827
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -136(%rbp)
	je	.L2828
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	24(%r14), %rsi
	movq	%rdx, 8(%r14)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%r14)
	testq	%rdi, %rdi
	je	.L2678
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L2676:
	xorl	%r8d, %r8d
	movq	$0, -88(%rbp)
	movw	%r8w, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 40(%r14)
	cmpq	%rbx, %rdi
	je	.L2679
	call	_ZdlPv@PLT
.L2679:
	leaq	.LC38(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%rbx, %rdi
	je	.L2680
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r8
.L2680:
	leaq	.LC38(%rip), %rsi
	movq	%r13, %rdi
	movq	%r8, -128(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %r8
	xorl	%esi, %esi
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movw	%si, -80(%rbp)
	movq	$0, -64(%rbp)
	testq	%r8, %r8
	je	.L2683
	movq	(%r8), %rax
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L2682
.L2683:
	leaq	.LC13(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L2682:
	movq	-96(%rbp), %rdx
	movq	48(%r14), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rdx
	je	.L2829
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -144(%rbp)
	je	.L2830
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	64(%r14), %rsi
	movq	%rdx, 48(%r14)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 56(%r14)
	testq	%rdi, %rdi
	je	.L2689
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L2687:
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movw	%ax, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 80(%r14)
	cmpq	%rbx, %rdi
	je	.L2690
	call	_ZdlPv@PLT
.L2690:
	leaq	.LC39(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%rbx, %rdi
	je	.L2691
	call	_ZdlPv@PLT
.L2691:
	leaq	.LC39(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-104(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler16FunctionCoverageESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	88(%r14), %rcx
	movq	$0, -104(%rbp)
	movq	%rax, 88(%r14)
	movq	%rcx, %rax
	movq	%rcx, -152(%rbp)
	testq	%rcx, %rcx
	je	.L2717
	movq	8(%rcx), %rcx
	movq	(%rax), %r12
	movq	%rcx, -128(%rbp)
	cmpq	%r12, %rcx
	je	.L2693
	movq	%r14, -160(%rbp)
	movq	%r13, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L2703:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L2694
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2695
	movq	48(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2696
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	jne	.L2700
	jmp	.L2697
	.p2align 4,,10
	.p2align 3
.L2832:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2698:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L2831
.L2700:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2698
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2832
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L2700
	.p2align 4,,10
	.p2align 3
.L2831:
	movq	(%r14), %r15
.L2697:
	testq	%r15, %r15
	je	.L2701
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2701:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2696:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2702
	call	_ZdlPv@PLT
.L2702:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2694:
	addq	$8, %r12
	cmpq	%r12, -128(%rbp)
	jne	.L2703
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %r14
	movq	-168(%rbp), %r13
	movq	(%rax), %r12
.L2693:
	testq	%r12, %r12
	je	.L2704
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2704:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	-104(%rbp), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2717
	movq	8(%rax), %rbx
	movq	(%rax), %r12
	movq	%rbx, -128(%rbp)
	cmpq	%r12, %rbx
	je	.L2705
	movq	%r14, -160(%rbp)
	movq	%r13, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L2715:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L2706
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2707
	movq	48(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2708
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	jne	.L2712
	jmp	.L2709
	.p2align 4,,10
	.p2align 3
.L2834:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2710:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L2833
.L2712:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2710
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2834
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L2712
	.p2align 4,,10
	.p2align 3
.L2833:
	movq	(%r14), %r15
.L2709:
	testq	%r15, %r15
	je	.L2713
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2713:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2708:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2714
	call	_ZdlPv@PLT
.L2714:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2706:
	addq	$8, %r12
	cmpq	%r12, -128(%rbp)
	jne	.L2715
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %r14
	movq	-168(%rbp), %r13
	movq	(%rax), %r12
.L2705:
	testq	%r12, %r12
	je	.L2716
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2716:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2717:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L2835
	movq	-120(%rbp), %rax
	movq	%r14, (%rax)
	jmp	.L2664
	.p2align 4,,10
	.p2align 3
.L2835:
	movq	-120(%rbp), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev(%rip), %rdx
	movq	$0, (%rax)
	movq	(%r14), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2836
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE(%rip), %rax
	movq	%rax, (%r14)
	movq	88(%r14), %rax
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L2718
	movq	8(%rax), %rdx
	movq	(%rax), %r12
	movq	%rdx, -128(%rbp)
	cmpq	%r12, %rdx
	je	.L2719
	movq	%r14, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L2729:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L2720
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2721
	movq	48(%r14), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r13, %r13
	je	.L2722
	movq	8(%r13), %rbx
	movq	0(%r13), %r15
	cmpq	%r15, %rbx
	jne	.L2726
	jmp	.L2723
	.p2align 4,,10
	.p2align 3
.L2838:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2724:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L2837
.L2726:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2724
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2838
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L2726
	.p2align 4,,10
	.p2align 3
.L2837:
	movq	0(%r13), %r15
.L2723:
	testq	%r15, %r15
	je	.L2727
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2727:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2722:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2728
	call	_ZdlPv@PLT
.L2728:
	movl	$64, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2720:
	addq	$8, %r12
	cmpq	%r12, -128(%rbp)
	jne	.L2729
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %r14
	movq	(%rax), %r12
.L2719:
	testq	%r12, %r12
	je	.L2730
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2730:
	movq	-152(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2718:
	movq	48(%r14), %rdi
	cmpq	%rdi, -144(%rbp)
	je	.L2731
	call	_ZdlPv@PLT
.L2731:
	movq	8(%r14), %rdi
	cmpq	%rdi, -136(%rbp)
	je	.L2732
	call	_ZdlPv@PLT
.L2732:
	movl	$96, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2664
	.p2align 4,,10
	.p2align 3
.L2707:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2706
	.p2align 4,,10
	.p2align 3
.L2695:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2694
	.p2align 4,,10
	.p2align 3
.L2829:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L2685
	cmpq	$1, %rax
	je	.L2839
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L2685
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	48(%r14), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L2685:
	xorl	%ecx, %ecx
	movq	%rax, 56(%r14)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L2687
	.p2align 4,,10
	.p2align 3
.L2827:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L2674
	cmpq	$1, %rax
	je	.L2840
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L2674
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	8(%r14), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L2674:
	xorl	%r9d, %r9d
	movq	%rax, 16(%r14)
	movw	%r9w, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L2676
	.p2align 4,,10
	.p2align 3
.L2721:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2720
	.p2align 4,,10
	.p2align 3
.L2830:
	movq	%rax, %xmm0
	movq	%rcx, %xmm4
	movq	%rdx, 48(%r14)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 56(%r14)
.L2689:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L2687
	.p2align 4,,10
	.p2align 3
.L2828:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdx, 8(%r14)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 16(%r14)
.L2678:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L2676
	.p2align 4,,10
	.p2align 3
.L2836:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2664
.L2840:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	8(%r14), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L2674
.L2839:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	48(%r14), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L2685
.L2826:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6498:
	.size	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler14ScriptCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler10TypeObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler10TypeObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler10TypeObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler10TypeObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler10TypeObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB12107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$1152921504606846975, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -88(%rbp)
	movq	%rax, -80(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L2869
	movq	%rsi, %r13
	movq	%rsi, %rbx
	subq	%r12, %rsi
	testq	%rax, %rax
	je	.L2856
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L2870
.L2843:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, -64(%rbp)
	addq	%rax, %r14
	movq	%r14, -72(%rbp)
	leaq	8(%rax), %r14
.L2855:
	movq	(%r15), %rax
	movq	-64(%rbp), %rcx
	movq	$0, (%r15)
	movq	%rax, (%rcx,%rsi)
	cmpq	%r12, %r13
	je	.L2845
	movq	%rcx, %r14
	movq	%r12, %r15
	jmp	.L2849
	.p2align 4,,10
	.p2align 3
.L2872:
	movq	8(%r8), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	leaq	24(%r8), %rsi
	movq	%rax, (%r8)
	cmpq	%rsi, %rdi
	je	.L2848
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L2848:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L2846:
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r13
	je	.L2871
.L2849:
	movq	(%r15), %rsi
	movq	$0, (%r15)
	movq	%rsi, (%r14)
	movq	(%r15), %r8
	testq	%r8, %r8
	je	.L2846
	movq	(%r8), %rsi
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rax
	movq	24(%rsi), %rsi
	cmpq	%rax, %rsi
	je	.L2872
	addq	$8, %r15
	movq	%r8, %rdi
	addq	$8, %r14
	call	*%rsi
	cmpq	%r15, %r13
	jne	.L2849
	.p2align 4,,10
	.p2align 3
.L2871:
	movq	-64(%rbp), %rcx
	movq	%r13, %rax
	subq	%r12, %rax
	leaq	8(%rcx,%rax), %r14
.L2845:
	movq	-80(%rbp), %rax
	cmpq	%rax, %r13
	je	.L2850
	subq	%r13, %rax
	leaq	-8(%rax), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	addq	$1, %rsi
	testq	%rdi, %rdi
	je	.L2858
	movq	%rsi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L2852:
	movdqu	0(%r13,%rax), %xmm1
	movups	%xmm1, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2852
	movq	%rsi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rbx
	leaq	(%r14,%rbx), %rdx
	addq	%r13, %rbx
	cmpq	%rax, %rsi
	je	.L2853
.L2851:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L2853:
	leaq	8(%r14,%rdi), %r14
.L2850:
	testq	%r12, %r12
	je	.L2854
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2854:
	movq	-64(%rbp), %xmm0
	movq	-88(%rbp), %rax
	movq	%r14, %xmm2
	movq	-72(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2870:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L2844
	movq	$0, -72(%rbp)
	movl	$8, %r14d
	movq	$0, -64(%rbp)
	jmp	.L2855
	.p2align 4,,10
	.p2align 3
.L2856:
	movl	$8, %r14d
	jmp	.L2843
.L2858:
	movq	%r14, %rdx
	jmp	.L2851
.L2844:
	cmpq	%rdx, %rdi
	cmovbe	%rdi, %rdx
	leaq	0(,%rdx,8), %r14
	jmp	.L2843
.L2869:
	leaq	.LC19(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12107:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler10TypeObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler10TypeObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.rodata._ZN12v8_inspector8protocol8Profiler16TypeProfileEntry9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC40:
	.string	"offset"
.LC41:
	.string	"types"
	.section	.text._ZN12v8_inspector8protocol8Profiler16TypeProfileEntry9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2874
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r14
	je	.L2875
.L2874:
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-120(%rbp), %rax
	movq	$0, (%rax)
.L2873:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2978
	movq	-120(%rbp), %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2875:
	.cfi_restore_state
	movl	$24, %edi
	leaq	-96(%rbp), %r13
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rcx
	cmpl	$6, 8(%r14)
	movq	%r12, %rdi
	movq	%rcx, (%rax)
	movq	$0, 16(%rax)
	movl	$0, 8(%rax)
	movq	%rax, -144(%rbp)
	movl	$0, %eax
	cmovne	%rax, %r14
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC40(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r15
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdi
	je	.L2878
	call	_ZdlPv@PLT
.L2878:
	leaq	.LC40(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movl	$0, -104(%rbp)
	testq	%r15, %r15
	je	.L2881
	leaq	-104(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	(%r15), %rax
	call	*48(%rax)
	testb	%al, %al
	jne	.L2880
.L2881:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L2880:
	movl	-104(%rbp), %eax
	movq	-144(%rbp), %rcx
	leaq	.LC41(%rip), %rsi
	movq	%r13, %rdi
	movl	%eax, 8(%rcx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r15
	cmpq	-128(%rbp), %rdi
	je	.L2882
	call	_ZdlPv@PLT
.L2882:
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	testq	%r15, %r15
	je	.L2883
	cmpl	$7, 8(%r15)
	jne	.L2883
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, %r14
	movups	%xmm0, (%rax)
	movq	24(%r15), %rbx
	subq	16(%r15), %rbx
	movq	%rbx, %rax
	movq	%rbx, -136(%rbp)
	sarq	$3, %rax
	testq	%rbx, %rbx
	js	.L2979
	testq	%rax, %rax
	jne	.L2887
.L2895:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L2885
	movq	8(%r14), %rbx
	movq	(%r14), %r13
	cmpq	%r13, %rbx
	jne	.L2907
	jmp	.L2903
	.p2align 4,,10
	.p2align 3
.L2981:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r15), %rdi
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2906
	call	_ZdlPv@PLT
.L2906:
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2904:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L2980
.L2907:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L2904
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2981
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2904
	.p2align 4,,10
	.p2align 3
.L2883:
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L2885:
	movq	-144(%rbp), %rax
	movq	16(%rax), %r13
	movq	%r14, 16(%rax)
	testq	%r13, %r13
	je	.L2909
	movq	8(%r13), %rbx
	movq	0(%r13), %r15
	cmpq	%r15, %rbx
	je	.L2910
	movq	%r12, -128(%rbp)
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %r14
	movq	%r15, %r12
	jmp	.L2914
	.p2align 4,,10
	.p2align 3
.L2983:
	movq	8(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	leaq	24(%r15), %rdx
	movq	%rax, (%r15)
	cmpq	%rdx, %rdi
	je	.L2913
	call	_ZdlPv@PLT
.L2913:
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2911:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L2982
.L2914:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L2911
	movq	(%r15), %rdx
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	je	.L2983
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L2911
	.p2align 4,,10
	.p2align 3
.L2982:
	movq	-128(%rbp), %r12
	movq	0(%r13), %r15
.L2910:
	testq	%r15, %r15
	je	.L2915
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2915:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2909:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L2984
	movq	-120(%rbp), %rax
	movq	-144(%rbp), %rcx
	movq	%rcx, (%rax)
	jmp	.L2873
	.p2align 4,,10
	.p2align 3
.L2984:
	movq	-120(%rbp), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rdx
	movq	$0, (%rax)
	movq	-144(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2985
	movq	-144(%rbp), %rax
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rcx
	movq	16(%rax), %r12
	movq	%rcx, (%rax)
	testq	%r12, %r12
	je	.L2916
	movq	8(%r12), %rbx
	movq	(%r12), %r14
	cmpq	%r14, %rbx
	je	.L2917
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %r13
	jmp	.L2921
	.p2align 4,,10
	.p2align 3
.L2987:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r15), %rdi
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2920
	call	_ZdlPv@PLT
.L2920:
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2918:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2986
.L2921:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L2918
	movq	(%r15), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	je	.L2987
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2918
	.p2align 4,,10
	.p2align 3
.L2986:
	movq	(%r12), %r14
.L2917:
	testq	%r14, %r14
	je	.L2922
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2922:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2916:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L2873
	.p2align 4,,10
	.p2align 3
.L2980:
	movq	(%r14), %r13
.L2903:
	testq	%r13, %r13
	je	.L2908
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2908:
	movq	%r14, %rdi
	movl	$24, %esi
	xorl	%r14d, %r14d
	call	_ZdlPvm@PLT
	jmp	.L2885
	.p2align 4,,10
	.p2align 3
.L2887:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	8(%r14), %rcx
	movq	(%r14), %rbx
	movq	%rax, -152(%rbp)
	cmpq	%rbx, %rcx
	je	.L2889
	movq	%r12, -168(%rbp)
	movq	%rbx, %r12
	movq	%rcx, %rbx
	movq	%r13, -176(%rbp)
	movq	%rax, %r13
	movq	%r14, -160(%rbp)
	jmp	.L2893
	.p2align 4,,10
	.p2align 3
.L2989:
	movq	8(%r14), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	leaq	24(%r14), %rcx
	movq	%rax, (%r14)
	cmpq	%rcx, %rdi
	je	.L2892
	call	_ZdlPv@PLT
.L2892:
	movl	$48, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2890:
	addq	$8, %r12
	addq	$8, %r13
	cmpq	%r12, %rbx
	je	.L2988
.L2893:
	movq	(%r12), %rcx
	movq	$0, (%r12)
	movq	%rcx, 0(%r13)
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L2890
	movq	(%r14), %rcx
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rax
	movq	24(%rcx), %rcx
	cmpq	%rax, %rcx
	je	.L2989
	movq	%r14, %rdi
	call	*%rcx
	jmp	.L2890
	.p2align 4,,10
	.p2align 3
.L2988:
	movq	-160(%rbp), %r14
	movq	-168(%rbp), %r12
	movq	-176(%rbp), %r13
	movq	(%r14), %rbx
.L2889:
	testq	%rbx, %rbx
	je	.L2894
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L2894:
	movq	-152(%rbp), %rbx
	movq	%rbx, %xmm0
	addq	-136(%rbp), %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, 16(%r14)
	movups	%xmm0, (%r14)
	movq	16(%r15), %rax
	cmpq	%rax, 24(%r15)
	je	.L2895
	leaq	-104(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%rax, -136(%rbp)
	jmp	.L2902
	.p2align 4,,10
	.p2align 3
.L2990:
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r14)
.L2898:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L2899
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2900
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L2901
	movq	%r8, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %r8
.L2901:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L2899:
	movq	24(%r15), %rax
	subq	16(%r15), %rax
	addq	$1, %rbx
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jbe	.L2895
.L2902:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-128(%rbp), %rdi
	je	.L2896
	call	_ZdlPv@PLT
.L2896:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol9ListValue2atEm@PLT
	movq	-136(%rbp), %rdi
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol8Profiler10TypeObject9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	8(%r14), %rsi
	cmpq	16(%r14), %rsi
	jne	.L2990
	movq	-136(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler10TypeObjectESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L2898
	.p2align 4,,10
	.p2align 3
.L2900:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L2899
	.p2align 4,,10
	.p2align 3
.L2985:
	movq	-144(%rbp), %rdi
	call	*%rax
	jmp	.L2873
.L2978:
	call	__stack_chk_fail@PLT
.L2979:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6504:
	.size	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16TypeProfileEntryESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16TypeProfileEntryESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16TypeProfileEntryESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16TypeProfileEntryESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16TypeProfileEntryESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB12140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movabsq	$1152921504606846975, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -64(%rbp)
	movq	8(%rdi), %rax
	movq	(%rdi), %rsi
	movq	%rdi, -120(%rbp)
	movq	%rax, -112(%rbp)
	subq	%rsi, %rax
	movq	%rsi, -72(%rbp)
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L3035
	movq	%r12, %r14
	subq	-72(%rbp), %r14
	testq	%rax, %rax
	je	.L3012
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L3036
.L2993:
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, -96(%rbp)
	addq	%rax, %rbx
	movq	%rbx, -104(%rbp)
	leaq	8(%rax), %rbx
.L3011:
	movq	0(%r13), %rax
	movq	-96(%rbp), %rcx
	movq	$0, 0(%r13)
	movq	%rax, (%rcx,%r14)
	movq	-72(%rbp), %rax
	cmpq	%rax, %r12
	je	.L2995
	movq	%r12, -56(%rbp)
	movq	%rcx, %r14
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3005:
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%rax, (%r14)
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2996
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2997
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2998
	movq	8(%r12), %rax
	movq	(%r12), %r13
	cmpq	%r13, %rax
	je	.L2999
	movq	%rbx, -80(%rbp)
	movq	%rax, %rbx
	movq	%r12, -88(%rbp)
	jmp	.L3003
	.p2align 4,,10
	.p2align 3
.L3038:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3002
	call	_ZdlPv@PLT
.L3002:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3000:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L3037
.L3003:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L3000
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3038
	addq	$8, %r13
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r13, %rbx
	jne	.L3003
	.p2align 4,,10
	.p2align 3
.L3037:
	movq	-88(%rbp), %r12
	movq	-80(%rbp), %rbx
	movq	(%r12), %r13
.L2999:
	testq	%r13, %r13
	je	.L3004
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3004:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2998:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2996:
	addq	$8, %rbx
	addq	$8, %r14
	cmpq	%rbx, -56(%rbp)
	jne	.L3005
.L3039:
	movq	-56(%rbp), %r12
	movq	-96(%rbp), %rcx
	movq	%r12, %rax
	subq	-72(%rbp), %rax
	leaq	8(%rcx,%rax), %rbx
.L2995:
	movq	-112(%rbp), %rax
	cmpq	%rax, %r12
	je	.L3006
	subq	%r12, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L3014
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L3008:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L3008
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rdi
	leaq	(%rbx,%rdi), %rdx
	addq	%r12, %rdi
	movq	%rdi, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L3009
.L3007:
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L3009:
	leaq	8(%rbx,%rsi), %rbx
.L3006:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L3010
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3010:
	movq	-96(%rbp), %xmm0
	movq	-120(%rbp), %rax
	movq	%rbx, %xmm2
	movq	-104(%rbp), %rdx
	punpcklqdq	%xmm2, %xmm0
	movq	%rdx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2997:
	.cfi_restore_state
	movq	%r15, %rdi
	addq	$8, %rbx
	addq	$8, %r14
	call	*%rax
	cmpq	%rbx, -56(%rbp)
	jne	.L3005
	jmp	.L3039
	.p2align 4,,10
	.p2align 3
.L3036:
	testq	%rcx, %rcx
	jne	.L2994
	movq	$0, -104(%rbp)
	movl	$8, %ebx
	movq	$0, -96(%rbp)
	jmp	.L3011
	.p2align 4,,10
	.p2align 3
.L3012:
	movl	$8, %ebx
	jmp	.L2993
.L3014:
	movq	%rbx, %rdx
	jmp	.L3007
.L2994:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	leaq	0(,%rdx,8), %rbx
	jmp	.L2993
.L3035:
	leaq	.LC19(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12140:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16TypeProfileEntryESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16TypeProfileEntryESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text._ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler16TypeProfileEntryESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"axG",@progbits,_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler16TypeProfileEntryESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler16TypeProfileEntryESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler16TypeProfileEntryESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler16TypeProfileEntryESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB8574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L3041
	cmpl	$7, 8(%rsi)
	movq	%rsi, %r13
	jne	.L3041
	movq	%rdx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, %r14
	movups	%xmm0, (%rax)
	movq	24(%r13), %rax
	subq	16(%r13), %rax
	movq	%rax, %rcx
	movq	%rax, -136(%rbp)
	sarq	$3, %rax
	testq	%rcx, %rcx
	js	.L3140
	testq	%rax, %rax
	jne	.L3045
.L3060:
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L3141
	movq	-160(%rbp), %rax
	movq	(%r14), %r12
	movq	$0, (%rax)
	movq	8(%r14), %rax
	movq	%rax, -120(%rbp)
	cmpq	%r12, %rax
	je	.L3074
	movq	%r14, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L3075:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L3076
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3077
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3078
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	je	.L3079
	movq	%r12, -128(%rbp)
	movq	%r15, %r12
	jmp	.L3083
	.p2align 4,,10
	.p2align 3
.L3143:
	movq	8(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	leaq	24(%r15), %rdx
	movq	%rax, (%r15)
	cmpq	%rdx, %rdi
	je	.L3082
	call	_ZdlPv@PLT
.L3082:
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3080:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L3142
.L3083:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L3080
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3143
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%r12, %rbx
	jne	.L3083
	.p2align 4,,10
	.p2align 3
.L3142:
	movq	-128(%rbp), %r12
	movq	(%r14), %r15
.L3079:
	testq	%r15, %r15
	je	.L3084
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3084:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3078:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3076:
	addq	$8, %r12
	cmpq	%r12, -120(%rbp)
	jne	.L3075
.L3149:
	movq	-136(%rbp), %r14
	movq	(%r14), %r12
.L3074:
	testq	%r12, %r12
	je	.L3085
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3085:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3040:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3144
	movq	-160(%rbp), %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3045:
	.cfi_restore_state
	movq	%rcx, %rdi
	call	_Znwm@PLT
	movq	8(%r14), %rcx
	movq	(%r14), %rbx
	movq	%rax, -152(%rbp)
	movq	%rcx, -120(%rbp)
	cmpq	%rbx, %rcx
	je	.L3048
	movq	%r13, -176(%rbp)
	movq	%r15, -184(%rbp)
	movq	%r14, -168(%rbp)
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L3058:
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%rax, (%r14)
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3049
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3050
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, (%r12)
	movq	16(%r12), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L3051
	movq	8(%rax), %r13
	movq	(%rax), %r15
	cmpq	%r15, %r13
	je	.L3052
	movq	%rbx, -144(%rbp)
	movq	%r15, %rbx
	movq	%r13, %r15
	jmp	.L3056
	.p2align 4,,10
	.p2align 3
.L3146:
	movq	8(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	leaq	24(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L3055
	call	_ZdlPv@PLT
.L3055:
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3053:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3145
.L3056:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L3053
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3146
	addq	$8, %rbx
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L3056
	.p2align 4,,10
	.p2align 3
.L3145:
	movq	-128(%rbp), %rax
	movq	-144(%rbp), %rbx
	movq	(%rax), %r15
.L3052:
	testq	%r15, %r15
	je	.L3057
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3057:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3051:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3049:
	addq	$8, %rbx
	addq	$8, %r14
	cmpq	%rbx, -120(%rbp)
	jne	.L3058
	movq	-168(%rbp), %r14
	movq	-176(%rbp), %r13
	movq	-184(%rbp), %r15
	movq	(%r14), %rbx
.L3048:
	testq	%rbx, %rbx
	je	.L3059
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L3059:
	movq	-152(%rbp), %rax
	movq	%rax, %xmm0
	addq	-136(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 16(%r14)
	movq	16(%r13), %rax
	movups	%xmm0, (%r14)
	cmpq	%rax, 24(%r13)
	je	.L3060
	leaq	-96(%rbp), %rax
	movq	%r15, -120(%rbp)
	xorl	%ebx, %ebx
	movq	%rax, -144(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -152(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L3073:
	movq	-144(%rbp), %r15
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEm@PLT
	movq	-120(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-152(%rbp), %rdi
	je	.L3061
	call	_ZdlPv@PLT
.L3061:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol9ListValue2atEm@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	8(%r14), %rsi
	cmpq	16(%r14), %rsi
	je	.L3062
	movq	-104(%rbp), %rax
	movq	$0, -104(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r14)
.L3063:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L3064
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3065
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, (%r12)
	movq	16(%r12), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L3066
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	cmpq	%r15, %rcx
	je	.L3067
	movq	%rbx, -168(%rbp)
	movq	%rcx, %rbx
	movq	%r12, -176(%rbp)
	jmp	.L3071
	.p2align 4,,10
	.p2align 3
.L3148:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3070
	call	_ZdlPv@PLT
.L3070:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3068:
	addq	$8, %r15
	cmpq	%r15, %rbx
	je	.L3147
.L3071:
	movq	(%r15), %r12
	testq	%r12, %r12
	je	.L3068
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3148
	addq	$8, %r15
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r15, %rbx
	jne	.L3071
	.p2align 4,,10
	.p2align 3
.L3147:
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	-176(%rbp), %r12
	movq	(%rax), %r15
.L3067:
	testq	%r15, %r15
	je	.L3072
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3072:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3066:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3064:
	movq	24(%r13), %rax
	subq	16(%r13), %rax
	addq	$1, %rbx
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L3073
	movq	-120(%rbp), %r15
	jmp	.L3060
	.p2align 4,,10
	.p2align 3
.L3062:
	movq	-128(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol8Profiler16TypeProfileEntryESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L3063
	.p2align 4,,10
	.p2align 3
.L3065:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3064
	.p2align 4,,10
	.p2align 3
.L3077:
	movq	%r13, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -120(%rbp)
	jne	.L3075
	jmp	.L3149
	.p2align 4,,10
	.p2align 3
.L3041:
	leaq	.LC23(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-160(%rbp), %rax
	movq	$0, (%rax)
	jmp	.L3040
	.p2align 4,,10
	.p2align 3
.L3141:
	movq	-160(%rbp), %rax
	movq	%r14, (%rax)
	jmp	.L3040
	.p2align 4,,10
	.p2align 3
.L3050:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3049
.L3144:
	call	__stack_chk_fail@PLT
.L3140:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8574:
	.size	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler16TypeProfileEntryESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler16TypeProfileEntryESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC42:
	.string	"entries"
	.section	.text._ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB6507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -120(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L3151
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r12
	je	.L3152
.L3151:
	movq	-128(%rbp), %rdi
	leaq	.LC6(%rip), %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-120(%rbp), %rax
	movq	$0, (%rax)
.L3150:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3312
	movq	-120(%rbp), %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3152:
	.cfi_restore_state
	movl	$96, %edi
	xorl	%ebx, %ebx
	leaq	-96(%rbp), %r13
	call	_Znwm@PLT
	xorl	%r11d, %r11d
	movq	-128(%rbp), %rdi
	cmpl	$6, 8(%r12)
	movq	%rax, %r15
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE(%rip), %rax
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	movq	%rax, -152(%rbp)
	movq	%rax, 8(%r15)
	leaq	64(%r15), %rax
	movw	%r11w, 24(%r15)
	movq	%rax, 48(%r15)
	movw	%bx, 64(%r15)
	leaq	-80(%rbp), %rbx
	movq	$0, 16(%r15)
	movq	$0, 40(%r15)
	movq	$0, 56(%r15)
	movq	$0, 80(%r15)
	movq	$0, 88(%r15)
	movq	%rax, -160(%rbp)
	movl	$0, %eax
	cmovne	%rax, %r12
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC37(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%rbx, %rdi
	je	.L3155
	call	_ZdlPv@PLT
.L3155:
	movq	-128(%rbp), %rdi
	leaq	.LC37(%rip), %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	xorl	%r10d, %r10d
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movw	%r10w, -80(%rbp)
	movq	$0, -64(%rbp)
	testq	%r14, %r14
	je	.L3158
	movq	(%r14), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L3157
.L3158:
	movq	-128(%rbp), %rdi
	leaq	.LC13(%rip), %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L3157:
	movq	-96(%rbp), %rdx
	movq	8(%r15), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rdx
	je	.L3313
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -152(%rbp)
	je	.L3314
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	24(%r15), %rsi
	movq	%rdx, 8(%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%r15)
	testq	%rdi, %rdi
	je	.L3164
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L3162:
	xorl	%r8d, %r8d
	movq	$0, -88(%rbp)
	movw	%r8w, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 40(%r15)
	cmpq	%rbx, %rdi
	je	.L3165
	call	_ZdlPv@PLT
.L3165:
	leaq	.LC38(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%rbx, %rdi
	je	.L3166
	call	_ZdlPv@PLT
.L3166:
	movq	-128(%rbp), %rdi
	leaq	.LC38(%rip), %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	xorl	%esi, %esi
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movw	%si, -80(%rbp)
	movq	$0, -64(%rbp)
	testq	%r14, %r14
	je	.L3169
	movq	(%r14), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L3168
.L3169:
	movq	-128(%rbp), %rdi
	leaq	.LC13(%rip), %rsi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L3168:
	movq	-96(%rbp), %rdx
	movq	48(%r15), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rdx
	je	.L3315
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -160(%rbp)
	je	.L3316
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	64(%r15), %rsi
	movq	%rdx, 48(%r15)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 56(%r15)
	testq	%rdi, %rdi
	je	.L3175
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L3173:
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movw	%ax, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 80(%r15)
	cmpq	%rbx, %rdi
	je	.L3176
	call	_ZdlPv@PLT
.L3176:
	leaq	.LC42(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%rbx, %rdi
	je	.L3177
	call	_ZdlPv@PLT
.L3177:
	movq	-128(%rbp), %rbx
	leaq	.LC42(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-104(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol16ValueConversionsISt6vectorISt10unique_ptrINS0_8Profiler16TypeProfileEntryESt14default_deleteIS5_EESaIS8_EEE9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	88(%r15), %rcx
	movq	$0, -104(%rbp)
	movq	%rax, 88(%r15)
	movq	%rcx, %rax
	movq	%rcx, -168(%rbp)
	testq	%rcx, %rcx
	je	.L3203
	movq	8(%rcx), %rcx
	movq	(%rax), %r12
	movq	%rcx, -136(%rbp)
	cmpq	%r12, %rcx
	je	.L3179
	movq	%r15, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L3189:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L3180
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3181
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3182
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	je	.L3183
	movq	%r12, -144(%rbp)
	movq	%r15, %r12
	jmp	.L3187
	.p2align 4,,10
	.p2align 3
.L3318:
	movq	8(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	leaq	24(%r15), %rdx
	movq	%rax, (%r15)
	cmpq	%rdx, %rdi
	je	.L3186
	call	_ZdlPv@PLT
.L3186:
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3184:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L3317
.L3187:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L3184
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3318
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%r12, %rbx
	jne	.L3187
	.p2align 4,,10
	.p2align 3
.L3317:
	movq	-144(%rbp), %r12
	movq	(%r14), %r15
.L3183:
	testq	%r15, %r15
	je	.L3188
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3188:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3182:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3180:
	addq	$8, %r12
	cmpq	%r12, -136(%rbp)
	jne	.L3189
	movq	-168(%rbp), %rax
	movq	-176(%rbp), %r15
	movq	(%rax), %r12
.L3179:
	testq	%r12, %r12
	je	.L3190
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3190:
	movq	-168(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	-104(%rbp), %rax
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L3203
	movq	8(%rax), %rcx
	movq	(%rax), %r12
	movq	%rcx, -136(%rbp)
	cmpq	%r12, %rcx
	je	.L3191
	movq	%r15, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L3201:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L3192
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3193
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3194
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	je	.L3195
	movq	%r12, -144(%rbp)
	movq	%r15, %r12
	jmp	.L3199
	.p2align 4,,10
	.p2align 3
.L3320:
	movq	8(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	leaq	24(%r15), %rdx
	movq	%rax, (%r15)
	cmpq	%rdx, %rdi
	je	.L3198
	call	_ZdlPv@PLT
.L3198:
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3196:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L3319
.L3199:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L3196
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3320
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%r12, %rbx
	jne	.L3199
	.p2align 4,,10
	.p2align 3
.L3319:
	movq	-144(%rbp), %r12
	movq	(%r14), %r15
.L3195:
	testq	%r15, %r15
	je	.L3200
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3200:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3194:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3192:
	addq	$8, %r12
	cmpq	%r12, -136(%rbp)
	jne	.L3201
	movq	-168(%rbp), %rax
	movq	-176(%rbp), %r15
	movq	(%rax), %r12
.L3191:
	testq	%r12, %r12
	je	.L3202
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3202:
	movq	-168(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3203:
	movq	-128(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L3321
	movq	-120(%rbp), %rax
	movq	%r15, (%rax)
	jmp	.L3150
	.p2align 4,,10
	.p2align 3
.L3321:
	movq	-120(%rbp), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev(%rip), %rdx
	movq	$0, (%rax)
	movq	(%r15), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3322
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE(%rip), %rax
	movq	%rax, (%r15)
	movq	88(%r15), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L3204
	movq	8(%rax), %rcx
	movq	(%rax), %r12
	movq	%rcx, -128(%rbp)
	cmpq	%r12, %rcx
	je	.L3205
	movq	%r15, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L3215:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L3206
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3207
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3208
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	je	.L3209
	movq	%r12, -136(%rbp)
	movq	%r15, %r12
	jmp	.L3213
	.p2align 4,,10
	.p2align 3
.L3324:
	movq	8(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	leaq	24(%r15), %rdx
	movq	%rax, (%r15)
	cmpq	%rdx, %rdi
	je	.L3212
	call	_ZdlPv@PLT
.L3212:
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3210:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L3323
.L3213:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L3210
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3324
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%r12, %rbx
	jne	.L3213
	.p2align 4,,10
	.p2align 3
.L3323:
	movq	-136(%rbp), %r12
	movq	(%r14), %r15
.L3209:
	testq	%r15, %r15
	je	.L3214
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3214:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3208:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3206:
	addq	$8, %r12
	cmpq	%r12, -128(%rbp)
	jne	.L3215
	movq	-144(%rbp), %rax
	movq	-168(%rbp), %r15
	movq	(%rax), %r12
.L3205:
	testq	%r12, %r12
	je	.L3216
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3216:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3204:
	movq	48(%r15), %rdi
	cmpq	%rdi, -160(%rbp)
	je	.L3217
	call	_ZdlPv@PLT
.L3217:
	movq	8(%r15), %rdi
	cmpq	%rdi, -152(%rbp)
	je	.L3218
	call	_ZdlPv@PLT
.L3218:
	movl	$96, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3150
	.p2align 4,,10
	.p2align 3
.L3181:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3180
	.p2align 4,,10
	.p2align 3
.L3193:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3192
	.p2align 4,,10
	.p2align 3
.L3315:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L3171
	cmpq	$1, %rax
	je	.L3325
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L3171
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	48(%r15), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L3171:
	xorl	%ecx, %ecx
	movq	%rax, 56(%r15)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L3173
	.p2align 4,,10
	.p2align 3
.L3313:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L3160
	cmpq	$1, %rax
	je	.L3326
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L3160
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	8(%r15), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L3160:
	xorl	%r9d, %r9d
	movq	%rax, 16(%r15)
	movw	%r9w, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L3162
	.p2align 4,,10
	.p2align 3
.L3207:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3206
	.p2align 4,,10
	.p2align 3
.L3316:
	movq	%rax, %xmm0
	movq	%rcx, %xmm4
	movq	%rdx, 48(%r15)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 56(%r15)
.L3175:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L3173
	.p2align 4,,10
	.p2align 3
.L3314:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdx, 8(%r15)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 16(%r15)
.L3164:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L3162
	.p2align 4,,10
	.p2align 3
.L3322:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L3150
.L3326:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	8(%r15), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L3160
.L3325:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	48(%r15), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L3171
.L3312:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6507:
	.size	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.str1.8,"aMS",@progbits,1
	.align 8
.LC43:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag:
.LFB12457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	jne	.L3328
	testq	%rdx, %rdx
	jne	.L3344
.L3328:
	subq	%r13, %rbx
	movq	%rbx, %r14
	sarq	%r14
	cmpq	$15, %rbx
	ja	.L3329
	movq	(%r12), %rdi
.L3330:
	cmpq	$2, %rbx
	je	.L3345
	testq	%rbx, %rbx
	je	.L3333
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L3333:
	xorl	%eax, %eax
	movq	%r14, 8(%r12)
	movw	%ax, (%rdi,%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3345:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L3333
	.p2align 4,,10
	.p2align 3
.L3329:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L3346
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r14, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L3330
.L3344:
	leaq	.LC43(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L3346:
	leaq	.LC15(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12457:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.section	.text._ZNK12v8_inspector8protocol8Profiler11ProfileNode7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler11ProfileNode7toValueEv
	.type	_ZNK12v8_inspector8protocol8Profiler11ProfileNode7toValueEv, @function
_ZNK12v8_inspector8protocol8Profiler11ProfileNode7toValueEv:
.LFB6480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r15, 0(%r13)
	movl	$24, %edi
	movl	8(%rbx), %r12d
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	leaq	.LC16(%rip), %rsi
	movl	%r12d, 16(%rax)
	leaq	-96(%rbp), %r12
	movl	$2, 8(%rax)
	movq	%r12, %rdi
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L3348
	call	_ZdlPv@PLT
.L3348:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3349
	movq	(%rdi), %rax
	call	*24(%rax)
.L3349:
	movq	0(%r13), %r8
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	leaq	-112(%rbp), %r15
	movq	%r8, -128(%rbp)
	call	_ZNK12v8_inspector8protocol7Runtime9CallFrame7toValueEv@PLT
	movq	-104(%rbp), %rax
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L3350
	call	_ZdlPv@PLT
.L3350:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3351
	movq	(%rdi), %rax
	call	*24(%rax)
.L3351:
	cmpb	$0, 24(%rbx)
	jne	.L3398
.L3352:
	movq	32(%rbx), %rcx
	testq	%rcx, %rcx
	movq	%rcx, -136(%rbp)
	je	.L3355
	movq	0(%r13), %rax
	movl	$40, %edi
	movq	%rax, -160(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	-136(%rbp), %rcx
	movq	-128(%rbp), %rax
	movq	8(%rcx), %rsi
	subq	(%rcx), %rsi
	leaq	16(%rax), %rdi
	sarq	$2, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	-136(%rbp), %rcx
	movq	8(%rcx), %rax
	movq	(%rcx), %rdx
	movq	%rax, -136(%rbp)
	cmpq	%rax, %rdx
	je	.L3356
	.p2align 4,,10
	.p2align 3
.L3359:
	movl	(%rdx), %ecx
	movl	$24, %edi
	movq	%rdx, -152(%rbp)
	movl	%ecx, -144(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	-128(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rcx, (%rax)
	movl	-144(%rbp), %ecx
	movl	$2, 8(%rax)
	movl	%ecx, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-104(%rbp), %rdi
	movq	-152(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L3357
	movq	(%rdi), %rax
	movq	%rdx, -144(%rbp)
	call	*24(%rax)
	movq	-144(%rbp), %rdx
	addq	$4, %rdx
	cmpq	%rdx, -136(%rbp)
	jne	.L3359
.L3356:
	movq	-128(%rbp), %rax
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-160(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L3360
	call	_ZdlPv@PLT
.L3360:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3355
	movq	(%rdi), %rax
	call	*24(%rax)
.L3355:
	cmpb	$0, 40(%rbx)
	jne	.L3399
.L3362:
	movq	88(%rbx), %rdx
	testq	%rdx, %rdx
	movq	%rdx, -128(%rbp)
	je	.L3347
	movq	0(%r13), %rax
	movl	$40, %edi
	movq	%rax, -144(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	-128(%rbp), %rdx
	leaq	16(%rbx), %rdi
	movq	8(%rdx), %rsi
	subq	(%rdx), %rsi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	-128(%rbp), %rdx
	movq	8(%rdx), %rcx
	movq	(%rdx), %rax
	movq	%rcx, -128(%rbp)
	cmpq	%rcx, %rax
	je	.L3366
	.p2align 4,,10
	.p2align 3
.L3369:
	movq	(%rax), %rsi
	movq	%r15, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNK12v8_inspector8protocol8Profiler16PositionTickInfo7toValueEv
	movq	-112(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%rdx, -104(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-104(%rbp), %rdi
	movq	-136(%rbp), %rax
	testq	%rdi, %rdi
	je	.L3367
	movq	(%rdi), %rdx
	call	*24(%rdx)
	movq	-136(%rbp), %rax
	addq	$8, %rax
	cmpq	%rax, -128(%rbp)
	jne	.L3369
.L3366:
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	movq	%rbx, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-144(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L3370
	call	_ZdlPv@PLT
.L3370:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3347
	movq	(%rdi), %rax
	call	*24(%rax)
.L3347:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3400
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3357:
	.cfi_restore_state
	addq	$4, %rdx
	cmpq	%rdx, -136(%rbp)
	jne	.L3359
	jmp	.L3356
	.p2align 4,,10
	.p2align 3
.L3367:
	addq	$8, %rax
	cmpq	%rax, -128(%rbp)
	jne	.L3369
	jmp	.L3366
	.p2align 4,,10
	.p2align 3
.L3399:
	movq	0(%r13), %r8
	movl	$56, %edi
	movq	%r8, -136(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rcx
	movq	48(%rbx), %rsi
	leaq	32(%rax), %rdx
	movq	%rcx, (%rax)
	leaq	16(%rax), %rdi
	movq	%rdx, 16(%rax)
	movq	56(%rbx), %rdx
	movl	$4, 8(%rax)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rax, -128(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-128(%rbp), %rax
	movq	80(%rbx), %rdx
	movq	%r12, %rdi
	leaq	.LC24(%rip), %rsi
	movq	%rdx, 48(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L3363
	call	_ZdlPv@PLT
.L3363:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3362
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L3362
	.p2align 4,,10
	.p2align 3
.L3398:
	movq	0(%r13), %r8
	movl	28(%rbx), %edx
	movl	$24, %edi
	movq	%r8, -136(%rbp)
	movl	%edx, -128(%rbp)
	call	_Znwm@PLT
	movl	-128(%rbp), %edx
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r12, %rdi
	movl	$2, 8(%rax)
	leaq	.LC21(%rip), %rsi
	movl	%edx, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L3353
	call	_ZdlPv@PLT
.L3353:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3352
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L3352
.L3400:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6480:
	.size	_ZNK12v8_inspector8protocol8Profiler11ProfileNode7toValueEv, .-_ZNK12v8_inspector8protocol8Profiler11ProfileNode7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Profiler11ProfileNode17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler11ProfileNode17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler11ProfileNode17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler11ProfileNode17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler11ProfileNode17serializeToBinaryEv:
.LFB6065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler11ProfileNode7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3401
	movq	(%rdi), %rax
	call	*24(%rax)
.L3401:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3408
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3408:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6065:
	.size	_ZN12v8_inspector8protocol8Profiler11ProfileNode17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler11ProfileNode17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler11ProfileNode15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler11ProfileNode15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler11ProfileNode15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler11ProfileNode15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler11ProfileNode15serializeToJSONEv:
.LFB6064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler11ProfileNode7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3409
	movq	(%rdi), %rax
	call	*24(%rax)
.L3409:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3416
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3416:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6064:
	.size	_ZN12v8_inspector8protocol8Profiler11ProfileNode15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler11ProfileNode15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Profiler11ProfileNode5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler11ProfileNode5cloneEv
	.type	_ZNK12v8_inspector8protocol8Profiler11ProfileNode5cloneEv, @function
_ZNK12v8_inspector8protocol8Profiler11ProfileNode5cloneEv:
.LFB6481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Profiler11ProfileNode7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Profiler11ProfileNode9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3418
	movq	(%rdi), %rax
	call	*24(%rax)
.L3418:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3424
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3424:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6481:
	.size	_ZNK12v8_inspector8protocol8Profiler11ProfileNode5cloneEv, .-_ZNK12v8_inspector8protocol8Profiler11ProfileNode5cloneEv
	.section	.text._ZNK12v8_inspector8protocol8Profiler7Profile7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler7Profile7toValueEv
	.type	_ZNK12v8_inspector8protocol8Profiler7Profile7toValueEv, @function
_ZNK12v8_inspector8protocol8Profiler7Profile7toValueEv:
.LFB6483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$96, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r15, (%r14)
	movl	$40, %edi
	movq	8(%rbx), %r13
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	8(%r13), %rsi
	subq	0(%r13), %rsi
	leaq	16(%r12), %rdi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	8(%r13), %rcx
	movq	0(%r13), %rax
	leaq	-104(%rbp), %r13
	movq	%rcx, -120(%rbp)
	cmpq	%rcx, %rax
	je	.L3426
	leaq	-112(%rbp), %rdx
	leaq	-104(%rbp), %r13
	movq	%rdx, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L3429:
	movq	(%rax), %rsi
	movq	-136(%rbp), %rdi
	movq	%rax, -128(%rbp)
	call	_ZNK12v8_inspector8protocol8Profiler11ProfileNode7toValueEv
	movq	-112(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rdx, -104(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-104(%rbp), %rdi
	movq	-128(%rbp), %rax
	testq	%rdi, %rdi
	je	.L3427
	movq	(%rdi), %rdx
	call	*24(%rdx)
	movq	-128(%rbp), %rax
	addq	$8, %rax
	cmpq	%rax, -120(%rbp)
	jne	.L3429
.L3426:
	movq	%r12, -104(%rbp)
	leaq	-96(%rbp), %r12
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L3430
	call	_ZdlPv@PLT
.L3430:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3431
	movq	(%rdi), %rax
	call	*24(%rax)
.L3431:
	movq	(%r14), %r8
	movsd	16(%rbx), %xmm0
	movl	$24, %edi
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %r15
	movq	%r8, -136(%rbp)
	movsd	%xmm0, -128(%rbp)
	call	_Znwm@PLT
	movsd	-128(%rbp), %xmm0
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	movl	$3, 8(%rax)
	movq	%r15, (%rax)
	movsd	%xmm0, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %r8
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L3432
	call	_ZdlPv@PLT
.L3432:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3433
	movq	(%rdi), %rax
	call	*24(%rax)
.L3433:
	movq	(%r14), %r8
	movsd	24(%rbx), %xmm0
	movl	$24, %edi
	movq	%r8, -136(%rbp)
	movsd	%xmm0, -128(%rbp)
	call	_Znwm@PLT
	movsd	-128(%rbp), %xmm0
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	movl	$3, 8(%rax)
	movq	%r15, (%rax)
	movsd	%xmm0, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %r8
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L3434
	call	_ZdlPv@PLT
.L3434:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3435
	movq	(%rdi), %rax
	call	*24(%rax)
.L3435:
	movq	32(%rbx), %rcx
	testq	%rcx, %rcx
	movq	%rcx, -136(%rbp)
	je	.L3436
	movq	(%r14), %rax
	movl	$40, %edi
	movq	%rax, -160(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	-136(%rbp), %rcx
	movq	-128(%rbp), %rax
	movq	8(%rcx), %rsi
	subq	(%rcx), %rsi
	leaq	16(%rax), %rdi
	sarq	$2, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	-136(%rbp), %rcx
	movq	8(%rcx), %rax
	movq	(%rcx), %rdx
	movq	%rax, -136(%rbp)
	cmpq	%rax, %rdx
	je	.L3437
	.p2align 4,,10
	.p2align 3
.L3440:
	movl	(%rdx), %ecx
	movl	$24, %edi
	movq	%rdx, -152(%rbp)
	movl	%ecx, -144(%rbp)
	call	_Znwm@PLT
	movl	-144(%rbp), %ecx
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	movl	$2, 8(%rax)
	movq	%r15, (%rax)
	movl	%ecx, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-104(%rbp), %rdi
	movq	-152(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L3438
	movq	(%rdi), %rax
	movq	%rdx, -144(%rbp)
	call	*24(%rax)
	movq	-144(%rbp), %rdx
	addq	$4, %rdx
	cmpq	%rdx, -136(%rbp)
	jne	.L3440
.L3437:
	movq	-128(%rbp), %rax
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-160(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L3441
	call	_ZdlPv@PLT
.L3441:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3436
	movq	(%rdi), %rax
	call	*24(%rax)
.L3436:
	movq	40(%rbx), %rcx
	testq	%rcx, %rcx
	movq	%rcx, -128(%rbp)
	je	.L3425
	movq	(%r14), %rax
	movl	$40, %edi
	movq	%rax, -152(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	-128(%rbp), %rcx
	leaq	16(%rbx), %rdi
	movq	8(%rcx), %rsi
	subq	(%rcx), %rsi
	sarq	$2, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	-128(%rbp), %rcx
	movq	8(%rcx), %rax
	movq	(%rcx), %rdx
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdx
	je	.L3444
	.p2align 4,,10
	.p2align 3
.L3447:
	movl	(%rdx), %ecx
	movl	$24, %edi
	movq	%rdx, -144(%rbp)
	movl	%ecx, -136(%rbp)
	call	_Znwm@PLT
	movl	-136(%rbp), %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$2, 8(%rax)
	movq	%r15, (%rax)
	movl	%ecx, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-104(%rbp), %rdi
	movq	-144(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L3445
	movq	(%rdi), %rax
	movq	%rdx, -136(%rbp)
	call	*24(%rax)
	movq	-136(%rbp), %rdx
	addq	$4, %rdx
	cmpq	%rdx, -128(%rbp)
	jne	.L3447
.L3444:
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	movq	%rbx, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-152(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L3448
	call	_ZdlPv@PLT
.L3448:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3425
	movq	(%rdi), %rax
	call	*24(%rax)
.L3425:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3475
	addq	$120, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3445:
	.cfi_restore_state
	addq	$4, %rdx
	cmpq	%rdx, -128(%rbp)
	jne	.L3447
	jmp	.L3444
	.p2align 4,,10
	.p2align 3
.L3427:
	addq	$8, %rax
	cmpq	%rax, -120(%rbp)
	jne	.L3429
	jmp	.L3426
	.p2align 4,,10
	.p2align 3
.L3438:
	addq	$4, %rdx
	cmpq	%rdx, -136(%rbp)
	jne	.L3440
	jmp	.L3437
.L3475:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6483:
	.size	_ZNK12v8_inspector8protocol8Profiler7Profile7toValueEv, .-_ZNK12v8_inspector8protocol8Profiler7Profile7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Profiler7Profile17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler7Profile17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler7Profile17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler7Profile17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler7Profile17serializeToBinaryEv:
.LFB6164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler7Profile7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3476
	movq	(%rdi), %rax
	call	*24(%rax)
.L3476:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3483
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3483:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6164:
	.size	_ZN12v8_inspector8protocol8Profiler7Profile17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler7Profile17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler7Profile15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler7Profile15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler7Profile15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler7Profile15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler7Profile15serializeToJSONEv:
.LFB6163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler7Profile7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3484
	movq	(%rdi), %rax
	call	*24(%rax)
.L3484:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3491
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3491:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6163:
	.size	_ZN12v8_inspector8protocol8Profiler7Profile15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler7Profile15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Profiler7Profile5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler7Profile5cloneEv
	.type	_ZNK12v8_inspector8protocol8Profiler7Profile5cloneEv, @function
_ZNK12v8_inspector8protocol8Profiler7Profile5cloneEv:
.LFB6484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Profiler7Profile7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Profiler7Profile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3493
	movq	(%rdi), %rax
	call	*24(%rax)
.L3493:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3499
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3499:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6484:
	.size	_ZNK12v8_inspector8protocol8Profiler7Profile5cloneEv, .-_ZNK12v8_inspector8protocol8Profiler7Profile5cloneEv
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImpl4stopEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl4stopEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl4stopEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImpl4stopEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-184(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -192(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	leaq	-192(%rbp), %rdx
	movq	%r14, %rdi
	movq	(%rsi), %rax
	call	*72(%rax)
	cmpl	$2, -112(%rbp)
	je	.L3593
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L3594
.L3503:
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3595
.L3535:
	leaq	-168(%rbp), %rcx
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, -168(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3502
	movq	(%rdi), %rax
	call	*24(%rax)
.L3502:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3507
	call	_ZdlPv@PLT
.L3507:
	movq	-184(%rbp), %r12
	testq	%r12, %r12
	je	.L3508
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3508:
	movq	-192(%rbp), %r13
	testq	%r13, %r13
	je	.L3500
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3510
	movq	40(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L3511
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3512
	call	_ZdlPv@PLT
.L3512:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3511:
	movq	32(%r13), %r12
	testq	%r12, %r12
	je	.L3513
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3514
	call	_ZdlPv@PLT
.L3514:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3513:
	movq	8(%r13), %rax
	movq	%rax, -216(%rbp)
	testq	%rax, %rax
	je	.L3515
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -208(%rbp)
	cmpq	%r14, %rcx
	je	.L3516
	.p2align 4,,10
	.p2align 3
.L3533:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3517
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3518
	movq	88(%r12), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rbx, %rbx
	je	.L3519
	movq	8(%rbx), %rax
	movq	(%rbx), %r15
	movq	%rax, -200(%rbp)
	cmpq	%r15, %rax
	jne	.L3523
	jmp	.L3520
	.p2align 4,,10
	.p2align 3
.L3597:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L3521:
	addq	$8, %r15
	cmpq	%r15, -200(%rbp)
	je	.L3596
.L3523:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3521
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3597
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, -200(%rbp)
	jne	.L3523
	.p2align 4,,10
	.p2align 3
.L3596:
	movq	(%rbx), %r15
.L3520:
	testq	%r15, %r15
	je	.L3524
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3524:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3519:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3525
	call	_ZdlPv@PLT
.L3525:
	movq	32(%r12), %rbx
	testq	%rbx, %rbx
	je	.L3526
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3527
	call	_ZdlPv@PLT
.L3527:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3526:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	je	.L3528
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3529
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3530
	call	_ZdlPv@PLT
.L3530:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3531
	call	_ZdlPv@PLT
.L3531:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3532
	call	_ZdlPv@PLT
.L3532:
	movl	$136, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3528:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3517:
	addq	$8, %r14
	cmpq	%r14, -208(%rbp)
	jne	.L3533
	movq	-216(%rbp), %rax
	movq	(%rax), %r14
.L3516:
	testq	%r14, %r14
	je	.L3534
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3534:
	movq	-216(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3515:
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3500:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3598
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3595:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L3502
	.p2align 4,,10
	.p2align 3
.L3594:
	movq	-192(%rbp), %rsi
	leaq	-168(%rbp), %rdi
	leaq	-160(%rbp), %r15
	call	_ZNK12v8_inspector8protocol8Profiler7Profile7toValueEv
	movq	-168(%rbp), %rax
	leaq	.LC33(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	leaq	-176(%rbp), %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3504
	call	_ZdlPv@PLT
.L3504:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3503
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L3535
	jmp	.L3595
	.p2align 4,,10
	.p2align 3
.L3593:
	movq	8(%rbx), %rdi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L3502
	.p2align 4,,10
	.p2align 3
.L3518:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3517
	.p2align 4,,10
	.p2align 3
.L3529:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L3528
	.p2align 4,,10
	.p2align 3
.L3510:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3500
.L3598:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6619:
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl4stopEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImpl4stopEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.rodata._ZN12v8_inspector8protocol8Profiler8Frontend22consoleProfileFinishedERKNS_8String16ESt10unique_ptrINS0_8Debugger8LocationESt14default_deleteIS8_EES6_INS1_7ProfileES9_ISC_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS3_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC44:
	.string	"Profiler.consoleProfileFinished"
	.section	.text._ZN12v8_inspector8protocol8Profiler8Frontend22consoleProfileFinishedERKNS_8String16ESt10unique_ptrINS0_8Debugger8LocationESt14default_deleteIS8_EES6_INS1_7ProfileES9_ISC_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler8Frontend22consoleProfileFinishedERKNS_8String16ESt10unique_ptrINS0_8Debugger8LocationESt14default_deleteIS8_EES6_INS1_7ProfileES9_ISC_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS3_EE
	.type	_ZN12v8_inspector8protocol8Profiler8Frontend22consoleProfileFinishedERKNS_8String16ESt10unique_ptrINS0_8Debugger8LocationESt14default_deleteIS8_EES6_INS1_7ProfileES9_ISC_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS3_EE, @function
_ZN12v8_inspector8protocol8Profiler8Frontend22consoleProfileFinishedERKNS_8String16ESt10unique_ptrINS0_8Debugger8LocationESt14default_deleteIS8_EES6_INS1_7ProfileES9_ISC_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS3_EE:
.LFB6516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -192(%rbp)
	movq	%r8, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L3599
	movl	$112, %edi
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rcx, %r13
	call	_Znwm@PLT
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	leaq	8(%rbx), %rdi
	movq	%rax, 8(%rbx)
	leaq	88(%rbx), %rax
	movq	%rax, 72(%rbx)
	movq	$0, 16(%rbx)
	movw	%cx, 24(%rbx)
	movq	$0, 40(%rbx)
	movb	$0, 64(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 80(%rbx)
	movups	%xmm0, 48(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 88(%rbx)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r15), %rax
	movq	%rax, 40(%rbx)
	movq	(%r14), %rax
	movq	$0, (%r14)
	movq	48(%rbx), %r14
	movq	%rax, 48(%rbx)
	testq	%r14, %r14
	je	.L3601
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3602
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r14), %rdi
	movq	%rax, (%r14)
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3603
	call	_ZdlPv@PLT
.L3603:
	movl	$64, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3601:
	movq	0(%r13), %rax
	movq	56(%rbx), %r14
	movq	$0, 0(%r13)
	movq	%rax, 56(%rbx)
	testq	%r14, %r14
	je	.L3604
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3605
	movq	40(%r14), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler7ProfileE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r13, %r13
	je	.L3606
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3607
	call	_ZdlPv@PLT
.L3607:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3606:
	movq	32(%r14), %r13
	testq	%r13, %r13
	je	.L3608
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3609
	call	_ZdlPv@PLT
.L3609:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3608:
	movq	8(%r14), %rax
	movq	%rax, -216(%rbp)
	testq	%rax, %rax
	je	.L3610
	movq	8(%rax), %rdx
	movq	(%rax), %r13
	movq	%rdx, -208(%rbp)
	cmpq	%r13, %rdx
	je	.L3611
	movq	%r14, -224(%rbp)
	movq	%rbx, -232(%rbp)
	.p2align 4,,10
	.p2align 3
.L3628:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L3612
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3613
	movq	88(%r14), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rbx, %rbx
	je	.L3614
	movq	8(%rbx), %r12
	movq	(%rbx), %r15
	cmpq	%r15, %r12
	jne	.L3618
	jmp	.L3615
	.p2align 4,,10
	.p2align 3
.L3705:
	movl	$16, %esi
	call	_ZdlPvm@PLT
.L3616:
	addq	$8, %r15
	cmpq	%r15, %r12
	je	.L3704
.L3618:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3616
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3705
	call	*%rax
	addq	$8, %r15
	cmpq	%r15, %r12
	jne	.L3618
	.p2align 4,,10
	.p2align 3
.L3704:
	movq	(%rbx), %r15
.L3615:
	testq	%r15, %r15
	je	.L3619
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3619:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3614:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3620
	call	_ZdlPv@PLT
.L3620:
	movq	32(%r14), %r12
	testq	%r12, %r12
	je	.L3621
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3622
	call	_ZdlPv@PLT
.L3622:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3621:
	movq	16(%r14), %r12
	testq	%r12, %r12
	je	.L3623
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3624
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime9CallFrameE(%rip), %rax
	movq	88(%r12), %rdi
	movq	%rax, (%r12)
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3625
	call	_ZdlPv@PLT
.L3625:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3626
	call	_ZdlPv@PLT
.L3626:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3627
	call	_ZdlPv@PLT
.L3627:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3623:
	movl	$96, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3612:
	addq	$8, %r13
	cmpq	%r13, -208(%rbp)
	jne	.L3628
	movq	-216(%rbp), %rax
	movq	-224(%rbp), %r14
	movq	-232(%rbp), %rbx
	movq	(%rax), %r13
.L3611:
	testq	%r13, %r13
	je	.L3629
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3629:
	movq	-216(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3610:
	movl	$48, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3604:
	movq	-184(%rbp), %rax
	leaq	-96(%rbp), %r14
	leaq	-80(%rbp), %r13
	cmpb	$0, (%rax)
	jne	.L3706
.L3630:
	movq	-192(%rbp), %rax
	leaq	.LC44(%rip), %rsi
	movq	%r14, %rdi
	movq	(%rax), %r12
	movq	(%r12), %rax
	movq	%rbx, -168(%rbp)
	movq	24(%rax), %r15
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-160(%rbp), %rdi
	leaq	-168(%rbp), %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rax
	movq	%r12, %rdi
	movq	$0, -160(%rbp)
	leaq	-152(%rbp), %rsi
	movq	%rax, -152(%rbp)
	call	*%r15
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3635
	movq	(%rdi), %rax
	call	*24(%rax)
.L3635:
	movq	-160(%rbp), %r12
	testq	%r12, %r12
	je	.L3636
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3637
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L3638
	movq	(%rdi), %rax
	call	*24(%rax)
.L3638:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3639
	call	_ZdlPv@PLT
.L3639:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3636:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3640
	call	_ZdlPv@PLT
.L3640:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3599
	movq	(%rdi), %rax
	call	*24(%rax)
.L3599:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3707
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3706:
	.cfi_restore_state
	movq	8(%rax), %rsi
	leaq	-128(%rbp), %r15
	leaq	24(%rax), %rdx
	movq	%r15, -144(%rbp)
	cmpq	%rdx, %rsi
	je	.L3708
	movq	24(%rax), %rax
	movq	%rsi, -144(%rbp)
	movq	%rax, -128(%rbp)
.L3632:
	movq	-184(%rbp), %rcx
	leaq	-96(%rbp), %r14
	leaq	-80(%rbp), %r13
	movq	%r14, %rdi
	movq	%r13, -96(%rbp)
	movq	%rdx, 8(%rcx)
	xorl	%edx, %edx
	movq	16(%rcx), %rax
	movw	%dx, 24(%rcx)
	movq	40(%rcx), %rdx
	movq	$0, 16(%rcx)
	movq	%rdx, -112(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-112(%rbp), %rax
	leaq	72(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movb	$1, 64(%rbx)
	movq	%rax, 104(%rbx)
	cmpq	%r13, %rdi
	je	.L3633
	call	_ZdlPv@PLT
.L3633:
	movq	-144(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3630
	call	_ZdlPv@PLT
	jmp	.L3630
	.p2align 4,,10
	.p2align 3
.L3708:
	movdqu	24(%rax), %xmm1
	movq	%r15, %rsi
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm1, -128(%rbp)
	jmp	.L3632
	.p2align 4,,10
	.p2align 3
.L3613:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L3612
	.p2align 4,,10
	.p2align 3
.L3624:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3623
	.p2align 4,,10
	.p2align 3
.L3605:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L3604
	.p2align 4,,10
	.p2align 3
.L3637:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3636
	.p2align 4,,10
	.p2align 3
.L3602:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L3601
.L3707:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6516:
	.size	_ZN12v8_inspector8protocol8Profiler8Frontend22consoleProfileFinishedERKNS_8String16ESt10unique_ptrINS0_8Debugger8LocationESt14default_deleteIS8_EES6_INS1_7ProfileES9_ISC_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS3_EE, .-_ZN12v8_inspector8protocol8Profiler8Frontend22consoleProfileFinishedERKNS_8String16ESt10unique_ptrINS0_8Debugger8LocationESt14default_deleteIS8_EES6_INS1_7ProfileES9_ISC_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS3_EE
	.section	.rodata._ZN12v8_inspector8protocol8Profiler8Frontend21consoleProfileStartedERKNS_8String16ESt10unique_ptrINS0_8Debugger8LocationESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS3_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC45:
	.string	"Profiler.consoleProfileStarted"
	.section	.text._ZN12v8_inspector8protocol8Profiler8Frontend21consoleProfileStartedERKNS_8String16ESt10unique_ptrINS0_8Debugger8LocationESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler8Frontend21consoleProfileStartedERKNS_8String16ESt10unique_ptrINS0_8Debugger8LocationESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS3_EE
	.type	_ZN12v8_inspector8protocol8Profiler8Frontend21consoleProfileStartedERKNS_8String16ESt10unique_ptrINS0_8Debugger8LocationESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS3_EE, @function
_ZN12v8_inspector8protocol8Profiler8Frontend21consoleProfileStartedERKNS_8String16ESt10unique_ptrINS0_8Debugger8LocationESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS3_EE:
.LFB6521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L3709
	movq	%rdi, %r13
	movl	$104, %edi
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rcx, %r12
	call	_Znwm@PLT
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	leaq	8(%rbx), %rdi
	movq	%rax, 8(%rbx)
	leaq	80(%rbx), %rax
	movq	%rax, 64(%rbx)
	movq	$0, 16(%rbx)
	movw	%dx, 24(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	movb	$0, 56(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 72(%rbx)
	movups	%xmm0, 80(%rbx)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r15), %rax
	movq	%rax, 40(%rbx)
	movq	(%r14), %rax
	movq	$0, (%r14)
	movq	48(%rbx), %r14
	movq	%rax, 48(%rbx)
	testq	%r14, %r14
	je	.L3711
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3712
	leaq	16+_ZTVN12v8_inspector8protocol8Debugger8LocationE(%rip), %rax
	movq	8(%r14), %rdi
	movq	%rax, (%r14)
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3713
	call	_ZdlPv@PLT
.L3713:
	movl	$64, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3711:
	cmpb	$0, (%r12)
	leaq	-96(%rbp), %r15
	leaq	-80(%rbp), %r14
	jne	.L3744
.L3714:
	movq	0(%r13), %r12
	leaq	.LC45(%rip), %rsi
	movq	%r15, %rdi
	movq	(%r12), %rax
	movq	%rbx, -168(%rbp)
	movq	24(%rax), %r13
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-160(%rbp), %rdi
	leaq	-168(%rbp), %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector8protocol16InternalResponse18createNotificationERKNS_8String16ESt10unique_ptrINS0_12SerializableESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rax
	movq	%r12, %rdi
	movq	$0, -160(%rbp)
	leaq	-152(%rbp), %rsi
	movq	%rax, -152(%rbp)
	call	*%r13
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3719
	movq	(%rdi), %rax
	call	*24(%rax)
.L3719:
	movq	-160(%rbp), %r12
	testq	%r12, %r12
	je	.L3720
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3721
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L3722
	movq	(%rdi), %rax
	call	*24(%rax)
.L3722:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3723
	call	_ZdlPv@PLT
.L3723:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3720:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3724
	call	_ZdlPv@PLT
.L3724:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3709
	movq	(%rdi), %rax
	call	*24(%rax)
.L3709:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3745
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3744:
	.cfi_restore_state
	movq	8(%r12), %rsi
	leaq	-128(%rbp), %rax
	leaq	24(%r12), %rcx
	movq	%rax, -144(%rbp)
	cmpq	%rcx, %rsi
	je	.L3746
	movq	24(%r12), %rdx
	movq	%rsi, -144(%rbp)
	movq	%rdx, -128(%rbp)
.L3716:
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	movq	16(%r12), %rdx
	leaq	-96(%rbp), %r15
	movw	%ax, 24(%r12)
	leaq	-80(%rbp), %r14
	movq	%r15, %rdi
	movq	%rcx, 8(%r12)
	movq	40(%r12), %rcx
	movq	$0, 16(%r12)
	movq	%rdx, -136(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rcx, -112(%rbp)
	movq	%r14, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-112(%rbp), %rdx
	leaq	64(%rbx), %rdi
	movq	%r15, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-96(%rbp), %rdi
	movq	-64(%rbp), %rdx
	movb	$1, 56(%rbx)
	movq	-184(%rbp), %rax
	cmpq	%r14, %rdi
	movq	%rdx, 96(%rbx)
	je	.L3717
	call	_ZdlPv@PLT
	movq	-184(%rbp), %rax
.L3717:
	movq	-144(%rbp), %rdi
	cmpq	%rax, %rdi
	je	.L3714
	call	_ZdlPv@PLT
	jmp	.L3714
	.p2align 4,,10
	.p2align 3
.L3746:
	movdqu	24(%r12), %xmm1
	movq	%rax, %rsi
	movaps	%xmm1, -128(%rbp)
	jmp	.L3716
	.p2align 4,,10
	.p2align 3
.L3721:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3720
	.p2align 4,,10
	.p2align 3
.L3712:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L3711
.L3745:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6521:
	.size	_ZN12v8_inspector8protocol8Profiler8Frontend21consoleProfileStartedERKNS_8String16ESt10unique_ptrINS0_8Debugger8LocationESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS3_EE, .-_ZN12v8_inspector8protocol8Profiler8Frontend21consoleProfileStartedERKNS_8String16ESt10unique_ptrINS0_8Debugger8LocationESt14default_deleteIS8_EEN30v8_inspector_protocol_bindings4glue6detail10ValueMaybeIS3_EE
	.section	.text._ZNK12v8_inspector8protocol8Profiler10TypeObject7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler10TypeObject7toValueEv
	.type	_ZNK12v8_inspector8protocol8Profiler10TypeObject7toValueEv, @function
_ZNK12v8_inspector8protocol8Profiler10TypeObject7toValueEv:
.LFB6502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movl	$96, %edi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r12, (%r14)
	movl	$56, %edi
	call	_Znwm@PLT
	movq	8(%r13), %rsi
	movq	%rax, %rbx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movq	16(%r13), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	40(%r13), %rax
	leaq	-80(%rbp), %r13
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
	movq	%rbx, -88(%rbp)
	movq	%rax, 48(%rbx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	leaq	-88(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3748
	call	_ZdlPv@PLT
.L3748:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3747
	movq	(%rdi), %rax
	call	*24(%rax)
.L3747:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3755
	addq	$64, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3755:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6502:
	.size	_ZNK12v8_inspector8protocol8Profiler10TypeObject7toValueEv, .-_ZNK12v8_inspector8protocol8Profiler10TypeObject7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Profiler10TypeObject17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler10TypeObject17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler10TypeObject17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler10TypeObject17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler10TypeObject17serializeToBinaryEv:
.LFB6320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler10TypeObject7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3756
	movq	(%rdi), %rax
	call	*24(%rax)
.L3756:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3763
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3763:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6320:
	.size	_ZN12v8_inspector8protocol8Profiler10TypeObject17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler10TypeObject17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler10TypeObject15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler10TypeObject15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler10TypeObject15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler10TypeObject15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler10TypeObject15serializeToJSONEv:
.LFB6319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler10TypeObject7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3764
	movq	(%rdi), %rax
	call	*24(%rax)
.L3764:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3771
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3771:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6319:
	.size	_ZN12v8_inspector8protocol8Profiler10TypeObject15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler10TypeObject15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Profiler10TypeObject5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler10TypeObject5cloneEv
	.type	_ZNK12v8_inspector8protocol8Profiler10TypeObject5cloneEv, @function
_ZNK12v8_inspector8protocol8Profiler10TypeObject5cloneEv:
.LFB6503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Profiler10TypeObject7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Profiler10TypeObject9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3773
	movq	(%rdi), %rax
	call	*24(%rax)
.L3773:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3779
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3779:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6503:
	.size	_ZNK12v8_inspector8protocol8Profiler10TypeObject5cloneEv, .-_ZNK12v8_inspector8protocol8Profiler10TypeObject5cloneEv
	.section	.text._ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry7toValueEv
	.type	_ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry7toValueEv, @function
_ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry7toValueEv:
.LFB6505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$96, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r15, (%r12)
	movl	$24, %edi
	movl	8(%rbx), %r13d
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rdx
	leaq	.LC40(%rip), %rsi
	movq	%rdx, (%rax)
	movl	%r13d, 16(%rax)
	leaq	-96(%rbp), %r13
	movl	$2, 8(%rax)
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rax, %rdi
	je	.L3781
	call	_ZdlPv@PLT
.L3781:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3782
	movq	(%rdi), %rax
	call	*24(%rax)
.L3782:
	movq	(%r12), %rax
	movl	$40, %edi
	movq	16(%rbx), %r15
	movq	%rax, -128(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	8(%r15), %rsi
	subq	(%r15), %rsi
	leaq	16(%rbx), %rdi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	8(%r15), %rcx
	movq	(%r15), %rax
	movq	%rcx, -120(%rbp)
	cmpq	%rcx, %rax
	je	.L3783
	movq	%rax, %r15
	leaq	-112(%rbp), %rax
	movq	%rax, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L3786:
	movq	(%r15), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK12v8_inspector8protocol8Profiler10TypeObject7toValueEv
	movq	-112(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%rdx, -104(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3784
	movq	(%rdi), %rdx
	addq	$8, %r15
	call	*24(%rdx)
	cmpq	%r15, -120(%rbp)
	jne	.L3786
.L3783:
	leaq	.LC41(%rip), %rsi
	movq	%r13, %rdi
	movq	%rbx, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L3787
	call	_ZdlPv@PLT
.L3787:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3780
	movq	(%rdi), %rax
	call	*24(%rax)
.L3780:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3797
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3784:
	.cfi_restore_state
	addq	$8, %r15
	cmpq	%r15, -120(%rbp)
	jne	.L3786
	jmp	.L3783
.L3797:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6505:
	.size	_ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry7toValueEv, .-_ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Profiler16TypeProfileEntry17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry17serializeToBinaryEv:
.LFB6350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3798
	movq	(%rdi), %rax
	call	*24(%rax)
.L3798:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3805
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3805:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6350:
	.size	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler16TypeProfileEntry15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry15serializeToJSONEv:
.LFB6349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3806
	movq	(%rdi), %rax
	call	*24(%rax)
.L3806:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3813
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3813:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6349:
	.size	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry5cloneEv
	.type	_ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry5cloneEv, @function
_ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry5cloneEv:
.LFB6506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3815
	movq	(%rdi), %rax
	call	*24(%rax)
.L3815:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3821
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3821:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6506:
	.size	_ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry5cloneEv, .-_ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry5cloneEv
	.section	.text._ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile7toValueEv
	.type	_ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile7toValueEv, @function
_ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile7toValueEv:
.LFB6508:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r15, 0(%r13)
	movl	$56, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rsi
	movq	%rax, %r12
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%r12)
	leaq	32(%r12), %rax
	leaq	16(%r12), %rdi
	movq	%rax, 16(%r12)
	movq	16(%rbx), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	40(%rbx), %rax
	movq	%r12, -104(%rbp)
	leaq	.LC37(%rip), %rsi
	movq	%rax, 48(%r12)
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdi
	je	.L3823
	call	_ZdlPv@PLT
.L3823:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3824
	movq	(%rdi), %rax
	call	*24(%rax)
.L3824:
	movq	0(%r13), %r8
	movl	$56, %edi
	movq	%r8, -136(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rcx
	movq	48(%rbx), %rsi
	leaq	32(%rax), %rdx
	movq	%rcx, (%rax)
	leaq	16(%rax), %rdi
	movq	%rdx, 16(%rax)
	movq	56(%rbx), %rdx
	movl	$4, 8(%rax)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rax, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-120(%rbp), %rax
	movq	80(%rbx), %rdx
	movq	%r12, %rdi
	leaq	.LC38(%rip), %rsi
	movq	%rdx, 48(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-128(%rbp), %rdi
	je	.L3825
	call	_ZdlPv@PLT
.L3825:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3826
	movq	(%rdi), %rax
	call	*24(%rax)
.L3826:
	movq	0(%r13), %rax
	movl	$40, %edi
	movq	88(%rbx), %r15
	movq	%rax, -136(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	8(%r15), %rsi
	subq	(%r15), %rsi
	leaq	16(%rbx), %rdi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	8(%r15), %rcx
	movq	(%r15), %rax
	movq	%rcx, -120(%rbp)
	cmpq	%rcx, %rax
	je	.L3827
	movq	%rax, %r15
	leaq	-112(%rbp), %rax
	movq	%rax, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L3830:
	movq	(%r15), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK12v8_inspector8protocol8Profiler16TypeProfileEntry7toValueEv
	movq	-112(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%rdx, -104(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3828
	movq	(%rdi), %rdx
	addq	$8, %r15
	call	*24(%rdx)
	cmpq	%r15, -120(%rbp)
	jne	.L3830
.L3827:
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	movq	%rbx, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-128(%rbp), %rdi
	je	.L3831
	call	_ZdlPv@PLT
.L3831:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3822
	movq	(%rdi), %rax
	call	*24(%rax)
.L3822:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3844
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3828:
	.cfi_restore_state
	addq	$8, %r15
	cmpq	%r15, -120(%rbp)
	jne	.L3830
	jmp	.L3827
.L3844:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6508:
	.size	_ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile7toValueEv, .-_ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile17serializeToBinaryEv:
.LFB6383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3845
	movq	(%rdi), %rax
	call	*24(%rax)
.L3845:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3852
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3852:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6383:
	.size	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile15serializeToJSONEv:
.LFB6382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3853
	movq	(%rdi), %rax
	call	*24(%rax)
.L3853:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3860
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3860:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6382:
	.size	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile5cloneEv
	.type	_ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile5cloneEv, @function
_ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile5cloneEv:
.LFB6509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3862
	movq	(%rdi), %rax
	call	*24(%rax)
.L3862:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3868
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3868:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6509:
	.size	_ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile5cloneEv, .-_ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile5cloneEv
	.section	.rodata._ZN12v8_inspector8protocol8Profiler14DispatcherImpl15takeTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC46:
	.string	"result"
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImpl15takeTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl15takeTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl15takeTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImpl15takeTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-184(%rbp), %rdi
	subq	$216, %rsp
	movl	%esi, -208(%rbp)
	movq	%rbx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -192(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	leaq	-192(%rbp), %rdx
	movq	%r14, %rdi
	movq	(%rsi), %rax
	call	*104(%rax)
	cmpl	$2, -112(%rbp)
	je	.L3951
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L3952
.L3872:
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3953
.L3905:
	movl	-208(%rbp), %esi
	leaq	-168(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, -168(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3871
	movq	(%rdi), %rax
	call	*24(%rax)
.L3871:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3882
	call	_ZdlPv@PLT
.L3882:
	movq	-184(%rbp), %r12
	testq	%r12, %r12
	je	.L3883
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3883:
	movq	-192(%rbp), %rax
	movq	%rax, -224(%rbp)
	testq	%rax, %rax
	je	.L3869
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -240(%rbp)
	movq	%rax, -200(%rbp)
	cmpq	%rax, %rcx
	je	.L3885
	.p2align 4,,10
	.p2align 3
.L3903:
	movq	-200(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L3886
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3887
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE(%rip), %rax
	movq	%rax, (%r12)
	movq	88(%r12), %rax
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L3888
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	%rcx, -232(%rbp)
	cmpq	%r15, %rcx
	je	.L3889
	movq	%r12, -248(%rbp)
	.p2align 4,,10
	.p2align 3
.L3899:
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.L3890
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3891
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3892
	movq	8(%r14), %rbx
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	jne	.L3897
	jmp	.L3893
	.p2align 4,,10
	.p2align 3
.L3955:
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE(%rip), %rax
	movq	8(%r8), %rdi
	movq	%rax, (%r8)
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L3896
	movq	%r8, -216(%rbp)
	call	_ZdlPv@PLT
	movq	-216(%rbp), %r8
.L3896:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L3894:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L3954
.L3897:
	movq	(%r12), %r8
	testq	%r8, %r8
	je	.L3894
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3955
	addq	$8, %r12
	movq	%r8, %rdi
	call	*%rax
	cmpq	%r12, %rbx
	jne	.L3897
	.p2align 4,,10
	.p2align 3
.L3954:
	movq	(%r14), %r12
.L3893:
	testq	%r12, %r12
	je	.L3898
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3898:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3892:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3890:
	addq	$8, %r15
	cmpq	%r15, -232(%rbp)
	jne	.L3899
	movq	-208(%rbp), %rax
	movq	-248(%rbp), %r12
	movq	(%rax), %r15
.L3889:
	testq	%r15, %r15
	je	.L3900
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3900:
	movq	-208(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3888:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3901
	call	_ZdlPv@PLT
.L3901:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3902
	call	_ZdlPv@PLT
.L3902:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3886:
	addq	$8, -200(%rbp)
	movq	-200(%rbp), %rax
	cmpq	%rax, -240(%rbp)
	jne	.L3903
	movq	-224(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -200(%rbp)
.L3885:
	movq	-200(%rbp), %rax
	testq	%rax, %rax
	je	.L3904
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3904:
	movq	-224(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3869:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3956
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3953:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L3871
	.p2align 4,,10
	.p2align 3
.L3952:
	movl	$40, %edi
	movq	-192(%rbp), %r15
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	8(%r15), %rsi
	subq	(%r15), %rsi
	leaq	16(%rbx), %rdi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	(%r15), %rax
	movq	8(%r15), %rdx
	leaq	-168(%rbp), %r15
	movq	%rax, %r13
	leaq	-176(%rbp), %rax
	movq	%rdx, -200(%rbp)
	movq	%rax, -216(%rbp)
	cmpq	%rdx, %r13
	je	.L3878
	.p2align 4,,10
	.p2align 3
.L3879:
	movq	0(%r13), %rsi
	movq	-216(%rbp), %rdi
	call	_ZNK12v8_inspector8protocol8Profiler17ScriptTypeProfile7toValueEv
	movq	-176(%rbp), %rax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rax, -168(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3876
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*24(%rax)
	cmpq	%r13, -200(%rbp)
	jne	.L3879
.L3878:
	movq	%rbx, -168(%rbp)
	leaq	-160(%rbp), %rbx
	leaq	.LC46(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3875
	call	_ZdlPv@PLT
.L3875:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3872
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L3905
	jmp	.L3953
	.p2align 4,,10
	.p2align 3
.L3891:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3890
	.p2align 4,,10
	.p2align 3
.L3876:
	addq	$8, %r13
	cmpq	%r13, -200(%rbp)
	jne	.L3879
	jmp	.L3878
	.p2align 4,,10
	.p2align 3
.L3951:
	movq	8(%rbx), %rdi
	movl	-208(%rbp), %esi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L3871
	.p2align 4,,10
	.p2align 3
.L3887:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3886
.L3956:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6648:
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl15takeTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImpl15takeTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZNK12v8_inspector8protocol8Profiler16FunctionCoverage7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler16FunctionCoverage7toValueEv
	.type	_ZNK12v8_inspector8protocol8Profiler16FunctionCoverage7toValueEv, @function
_ZNK12v8_inspector8protocol8Profiler16FunctionCoverage7toValueEv:
.LFB6496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$96, %edi
	leaq	-104(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r13, (%r14)
	movl	$56, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rsi
	movq	%rax, %r12
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%r12)
	leaq	32(%r12), %rax
	leaq	16(%r12), %rdi
	movq	%rax, 16(%r12)
	movq	16(%rbx), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	40(%rbx), %rax
	movq	%r12, -104(%rbp)
	leaq	.LC34(%rip), %rsi
	movq	%rax, 48(%r12)
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdi
	je	.L3958
	call	_ZdlPv@PLT
.L3958:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3959
	movq	(%rdi), %rax
	call	*24(%rax)
.L3959:
	movq	48(%rbx), %rdx
	movq	(%r14), %rax
	movl	$40, %edi
	movq	%rdx, -120(%rbp)
	movq	%rax, -136(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	-120(%rbp), %rdx
	leaq	16(%r13), %rdi
	movq	8(%rdx), %rsi
	subq	(%rdx), %rsi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	-120(%rbp), %rdx
	movq	8(%rdx), %rcx
	movq	(%rdx), %rax
	movq	%rcx, -120(%rbp)
	cmpq	%rcx, %rax
	je	.L3960
	leaq	-112(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L3963:
	movq	(%rax), %rsi
	movq	-152(%rbp), %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK12v8_inspector8protocol8Profiler13CoverageRange7toValueEv
	movq	-112(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rdx, -104(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-104(%rbp), %rdi
	movq	-144(%rbp), %rax
	testq	%rdi, %rdi
	je	.L3961
	movq	(%rdi), %rdx
	call	*24(%rdx)
	movq	-144(%rbp), %rax
	addq	$8, %rax
	cmpq	%rax, -120(%rbp)
	jne	.L3963
.L3960:
	leaq	.LC35(%rip), %rsi
	movq	%r12, %rdi
	movq	%r13, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-128(%rbp), %rdi
	je	.L3964
	call	_ZdlPv@PLT
.L3964:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3965
	movq	(%rdi), %rax
	call	*24(%rax)
.L3965:
	movzbl	56(%rbx), %ebx
	movl	$24, %edi
	movq	(%r14), %r13
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol16FundamentalValueE(%rip), %rcx
	leaq	.LC36(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, 8(%rax)
	movq	%rcx, (%rax)
	movb	%bl, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-128(%rbp), %rdi
	je	.L3966
	call	_ZdlPv@PLT
.L3966:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3957
	movq	(%rdi), %rax
	call	*24(%rax)
.L3957:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3979
	addq	$120, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3961:
	.cfi_restore_state
	addq	$8, %rax
	cmpq	%rax, -120(%rbp)
	jne	.L3963
	jmp	.L3960
.L3979:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6496:
	.size	_ZNK12v8_inspector8protocol8Profiler16FunctionCoverage7toValueEv, .-_ZNK12v8_inspector8protocol8Profiler16FunctionCoverage7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Profiler16FunctionCoverage17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16FunctionCoverage17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler16FunctionCoverage17serializeToBinaryEv:
.LFB6259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler16FunctionCoverage7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3980
	movq	(%rdi), %rax
	call	*24(%rax)
.L3980:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3987
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3987:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6259:
	.size	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler16FunctionCoverage17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler16FunctionCoverage15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler16FunctionCoverage15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler16FunctionCoverage15serializeToJSONEv:
.LFB6258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler16FunctionCoverage7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3988
	movq	(%rdi), %rax
	call	*24(%rax)
.L3988:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3995
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3995:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6258:
	.size	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler16FunctionCoverage15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Profiler16FunctionCoverage5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler16FunctionCoverage5cloneEv
	.type	_ZNK12v8_inspector8protocol8Profiler16FunctionCoverage5cloneEv, @function
_ZNK12v8_inspector8protocol8Profiler16FunctionCoverage5cloneEv:
.LFB6497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Profiler16FunctionCoverage7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3997
	movq	(%rdi), %rax
	call	*24(%rax)
.L3997:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4003
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4003:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6497:
	.size	_ZNK12v8_inspector8protocol8Profiler16FunctionCoverage5cloneEv, .-_ZNK12v8_inspector8protocol8Profiler16FunctionCoverage5cloneEv
	.section	.text._ZNK12v8_inspector8protocol8Profiler14ScriptCoverage7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage7toValueEv
	.type	_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage7toValueEv, @function
_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage7toValueEv:
.LFB6499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r15, 0(%r13)
	movl	$56, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rsi
	movq	%rax, %r12
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%r12)
	leaq	32(%r12), %rax
	leaq	16(%r12), %rdi
	movq	%rax, 16(%r12)
	movq	16(%rbx), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	40(%rbx), %rax
	movq	%r12, -104(%rbp)
	leaq	.LC37(%rip), %rsi
	movq	%rax, 48(%r12)
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	cmpq	%rax, %rdi
	je	.L4005
	call	_ZdlPv@PLT
.L4005:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4006
	movq	(%rdi), %rax
	call	*24(%rax)
.L4006:
	movq	0(%r13), %r8
	movl	$56, %edi
	movq	%r8, -136(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rcx
	movq	48(%rbx), %rsi
	leaq	32(%rax), %rdx
	movq	%rcx, (%rax)
	leaq	16(%rax), %rdi
	movq	%rdx, 16(%rax)
	movq	56(%rbx), %rdx
	movl	$4, 8(%rax)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rax, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-120(%rbp), %rax
	movq	80(%rbx), %rdx
	movq	%r12, %rdi
	leaq	.LC38(%rip), %rsi
	movq	%rdx, 48(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-128(%rbp), %rdi
	je	.L4007
	call	_ZdlPv@PLT
.L4007:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4008
	movq	(%rdi), %rax
	call	*24(%rax)
.L4008:
	movq	0(%r13), %rax
	movl	$40, %edi
	movq	88(%rbx), %r15
	movq	%rax, -136(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	8(%r15), %rsi
	subq	(%r15), %rsi
	leaq	16(%rbx), %rdi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	8(%r15), %rcx
	movq	(%r15), %rax
	movq	%rcx, -120(%rbp)
	cmpq	%rcx, %rax
	je	.L4009
	movq	%rax, %r15
	leaq	-112(%rbp), %rax
	movq	%rax, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L4012:
	movq	(%r15), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK12v8_inspector8protocol8Profiler16FunctionCoverage7toValueEv
	movq	-112(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%rdx, -104(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4010
	movq	(%rdi), %rdx
	addq	$8, %r15
	call	*24(%rdx)
	cmpq	%r15, -120(%rbp)
	jne	.L4012
.L4009:
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	movq	%rbx, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-128(%rbp), %rdi
	je	.L4013
	call	_ZdlPv@PLT
.L4013:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4004
	movq	(%rdi), %rax
	call	*24(%rax)
.L4004:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4026
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4010:
	.cfi_restore_state
	addq	$8, %r15
	cmpq	%r15, -120(%rbp)
	jne	.L4012
	jmp	.L4009
.L4026:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6499:
	.size	_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage7toValueEv, .-_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Profiler14ScriptCoverage17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler14ScriptCoverage17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler14ScriptCoverage17serializeToBinaryEv:
.LFB6293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4027
	movq	(%rdi), %rax
	call	*24(%rax)
.L4027:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4034
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4034:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6293:
	.size	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler14ScriptCoverage17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler14ScriptCoverage15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler14ScriptCoverage15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler14ScriptCoverage15serializeToJSONEv:
.LFB6292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4035
	movq	(%rdi), %rax
	call	*24(%rax)
.L4035:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4042
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4042:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6292:
	.size	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler14ScriptCoverage15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Profiler14ScriptCoverage5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage5cloneEv
	.type	_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage5cloneEv, @function
_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage5cloneEv:
.LFB6500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4044
	movq	(%rdi), %rax
	call	*24(%rax)
.L4044:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4050
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4050:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6500:
	.size	_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage5cloneEv, .-_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage5cloneEv
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImpl21getBestEffortCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl21getBestEffortCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl21getBestEffortCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImpl21getBestEffortCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-184(%rbp), %rdi
	subq	$200, %rsp
	movl	%esi, -208(%rbp)
	movq	%rbx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -192(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	leaq	-192(%rbp), %rdx
	movq	%r14, %rdi
	movq	(%rsi), %rax
	call	*32(%rax)
	cmpl	$2, -112(%rbp)
	je	.L4133
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L4134
.L4054:
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L4135
.L4087:
	movl	-208(%rbp), %esi
	leaq	-168(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, -168(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4053
	movq	(%rdi), %rax
	call	*24(%rax)
.L4053:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4064
	call	_ZdlPv@PLT
.L4064:
	movq	-184(%rbp), %r12
	testq	%r12, %r12
	je	.L4065
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4065:
	movq	-192(%rbp), %rax
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L4051
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -224(%rbp)
	movq	%rax, -200(%rbp)
	cmpq	%rax, %rdx
	je	.L4067
	.p2align 4,,10
	.p2align 3
.L4085:
	movq	-200(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L4068
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4069
	movq	88(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L4070
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -216(%rbp)
	cmpq	%r13, %rax
	je	.L4071
	movq	%r12, -232(%rbp)
	movq	%r14, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L4081:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L4072
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4073
	movq	48(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L4074
	movq	8(%r12), %rbx
	movq	(%r12), %r14
	cmpq	%r14, %rbx
	jne	.L4078
	jmp	.L4075
	.p2align 4,,10
	.p2align 3
.L4137:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4076:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L4136
.L4078:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4076
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L4137
	call	*%rax
	addq	$8, %r14
	cmpq	%r14, %rbx
	jne	.L4078
	.p2align 4,,10
	.p2align 3
.L4136:
	movq	(%r12), %r14
.L4075:
	testq	%r14, %r14
	je	.L4079
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4079:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4074:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L4080
	call	_ZdlPv@PLT
.L4080:
	movl	$64, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4072:
	addq	$8, %r13
	cmpq	%r13, -216(%rbp)
	jne	.L4081
.L4139:
	movq	-240(%rbp), %r14
	movq	-232(%rbp), %r12
	movq	(%r14), %r13
.L4071:
	testq	%r13, %r13
	je	.L4082
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L4082:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4070:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4083
	call	_ZdlPv@PLT
.L4083:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4084
	call	_ZdlPv@PLT
.L4084:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4068:
	addq	$8, -200(%rbp)
	movq	-200(%rbp), %rax
	cmpq	%rax, -224(%rbp)
	jne	.L4085
	movq	-208(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -200(%rbp)
.L4067:
	movq	-200(%rbp), %rax
	testq	%rax, %rax
	je	.L4086
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L4086:
	movq	-208(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4051:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4138
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4135:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L4053
	.p2align 4,,10
	.p2align 3
.L4134:
	movl	$40, %edi
	movq	-192(%rbp), %r15
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	8(%r15), %rsi
	subq	(%r15), %rsi
	leaq	16(%rbx), %rdi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	(%r15), %rax
	movq	8(%r15), %rcx
	leaq	-168(%rbp), %r15
	movq	%rax, %r13
	leaq	-176(%rbp), %rax
	movq	%rcx, -200(%rbp)
	movq	%rax, -216(%rbp)
	cmpq	%rcx, %r13
	je	.L4060
	.p2align 4,,10
	.p2align 3
.L4061:
	movq	0(%r13), %rsi
	movq	-216(%rbp), %rdi
	call	_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage7toValueEv
	movq	-176(%rbp), %rax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rax, -168(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4058
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*24(%rax)
	cmpq	%r13, -200(%rbp)
	jne	.L4061
.L4060:
	movq	%rbx, -168(%rbp)
	leaq	-160(%rbp), %rbx
	leaq	.LC46(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4057
	call	_ZdlPv@PLT
.L4057:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4054
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L4087
	jmp	.L4135
	.p2align 4,,10
	.p2align 3
.L4073:
	movq	%r15, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -216(%rbp)
	jne	.L4081
	jmp	.L4139
	.p2align 4,,10
	.p2align 3
.L4058:
	addq	$8, %r13
	cmpq	%r13, -200(%rbp)
	jne	.L4061
	jmp	.L4060
	.p2align 4,,10
	.p2align 3
.L4133:
	movq	8(%rbx), %rdi
	movl	-208(%rbp), %esi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L4053
	.p2align 4,,10
	.p2align 3
.L4069:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4068
.L4138:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6586:
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl21getBestEffortCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImpl21getBestEffortCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImpl19takePreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19takePreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19takePreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19takePreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB6647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-184(%rbp), %rdi
	subq	$200, %rsp
	movl	%esi, -208(%rbp)
	movq	%rbx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -192(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	leaq	-192(%rbp), %rdx
	movq	%r14, %rdi
	movq	(%rsi), %rax
	call	*96(%rax)
	cmpl	$2, -112(%rbp)
	je	.L4222
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L4223
.L4143:
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L4224
.L4176:
	movl	-208(%rbp), %esi
	leaq	-168(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, -168(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4142
	movq	(%rdi), %rax
	call	*24(%rax)
.L4142:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4153
	call	_ZdlPv@PLT
.L4153:
	movq	-184(%rbp), %r12
	testq	%r12, %r12
	je	.L4154
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4154:
	movq	-192(%rbp), %rax
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L4140
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -224(%rbp)
	movq	%rax, -200(%rbp)
	cmpq	%rax, %rdx
	je	.L4156
	.p2align 4,,10
	.p2align 3
.L4174:
	movq	-200(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L4157
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4158
	movq	88(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L4159
	movq	8(%r14), %rax
	movq	(%r14), %r13
	movq	%rax, -216(%rbp)
	cmpq	%r13, %rax
	je	.L4160
	movq	%r12, -232(%rbp)
	movq	%r14, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L4170:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L4161
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4162
	movq	48(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L4163
	movq	8(%r12), %rbx
	movq	(%r12), %r14
	cmpq	%r14, %rbx
	jne	.L4167
	jmp	.L4164
	.p2align 4,,10
	.p2align 3
.L4226:
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4165:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L4225
.L4167:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4165
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L4226
	call	*%rax
	addq	$8, %r14
	cmpq	%r14, %rbx
	jne	.L4167
	.p2align 4,,10
	.p2align 3
.L4225:
	movq	(%r12), %r14
.L4164:
	testq	%r14, %r14
	je	.L4168
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4168:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4163:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L4169
	call	_ZdlPv@PLT
.L4169:
	movl	$64, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L4161:
	addq	$8, %r13
	cmpq	%r13, -216(%rbp)
	jne	.L4170
.L4228:
	movq	-240(%rbp), %r14
	movq	-232(%rbp), %r12
	movq	(%r14), %r13
.L4160:
	testq	%r13, %r13
	je	.L4171
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L4171:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L4159:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4172
	call	_ZdlPv@PLT
.L4172:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4173
	call	_ZdlPv@PLT
.L4173:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4157:
	addq	$8, -200(%rbp)
	movq	-200(%rbp), %rax
	cmpq	%rax, -224(%rbp)
	jne	.L4174
	movq	-208(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -200(%rbp)
.L4156:
	movq	-200(%rbp), %rax
	testq	%rax, %rax
	je	.L4175
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L4175:
	movq	-208(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L4140:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4227
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4224:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L4142
	.p2align 4,,10
	.p2align 3
.L4223:
	movl	$40, %edi
	movq	-192(%rbp), %r15
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	8(%r15), %rsi
	subq	(%r15), %rsi
	leaq	16(%rbx), %rdi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	(%r15), %rax
	movq	8(%r15), %rcx
	leaq	-168(%rbp), %r15
	movq	%rax, %r13
	leaq	-176(%rbp), %rax
	movq	%rcx, -200(%rbp)
	movq	%rax, -216(%rbp)
	cmpq	%rcx, %r13
	je	.L4149
	.p2align 4,,10
	.p2align 3
.L4150:
	movq	0(%r13), %rsi
	movq	-216(%rbp), %rdi
	call	_ZNK12v8_inspector8protocol8Profiler14ScriptCoverage7toValueEv
	movq	-176(%rbp), %rax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rax, -168(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4147
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*24(%rax)
	cmpq	%r13, -200(%rbp)
	jne	.L4150
.L4149:
	movq	%rbx, -168(%rbp)
	leaq	-160(%rbp), %rbx
	leaq	.LC46(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4146
	call	_ZdlPv@PLT
.L4146:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4143
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L4176
	jmp	.L4224
	.p2align 4,,10
	.p2align 3
.L4162:
	movq	%r15, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -216(%rbp)
	jne	.L4170
	jmp	.L4228
	.p2align 4,,10
	.p2align 3
.L4147:
	addq	$8, %r13
	cmpq	%r13, -200(%rbp)
	jne	.L4150
	jmp	.L4149
	.p2align 4,,10
	.p2align 3
.L4222:
	movq	8(%rbx), %rdi
	movl	-208(%rbp), %esi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L4142
	.p2align 4,,10
	.p2align 3
.L4158:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4157
.L4227:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6647:
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19takePreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19takePreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZNK12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification7toValueEv
	.type	_ZNK12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification7toValueEv, @function
_ZNK12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification7toValueEv:
.LFB6514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$96, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r14, (%r12)
	movl	$56, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rsi
	movq	%rax, %r13
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	32(%r13), %rax
	leaq	16(%r13), %rdi
	movq	%rax, 16(%r13)
	movq	16(%rbx), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	40(%rbx), %rax
	movq	%r13, -104(%rbp)
	leaq	.LC16(%rip), %rsi
	movq	%rax, 48(%r13)
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r14
	cmpq	%r14, %rdi
	je	.L4230
	call	_ZdlPv@PLT
.L4230:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4231
	movq	(%rdi), %rax
	call	*24(%rax)
.L4231:
	movq	(%r12), %r8
	movq	48(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r8, -120(%rbp)
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv@PLT
	movq	-104(%rbp), %rax
	leaq	.LC17(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %r8
	leaq	-112(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4232
	call	_ZdlPv@PLT
.L4232:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4233
	movq	(%rdi), %rax
	call	*24(%rax)
.L4233:
	cmpb	$0, 56(%rbx)
	jne	.L4248
.L4229:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4249
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4248:
	.cfi_restore_state
	movq	(%r12), %r8
	movl	$56, %edi
	movq	%r8, -128(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rcx
	movq	64(%rbx), %rsi
	leaq	32(%rax), %rdx
	movq	%rcx, (%rax)
	leaq	16(%rax), %rdi
	movq	%rdx, 16(%rax)
	movq	72(%rbx), %rdx
	movl	$4, 8(%rax)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rax, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-120(%rbp), %rax
	movq	96(%rbx), %rdx
	movq	%r13, %rdi
	leaq	.LC18(%rip), %rsi
	movq	%rdx, 48(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4235
	call	_ZdlPv@PLT
.L4235:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4229
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4229
.L4249:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6514:
	.size	_ZNK12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification7toValueEv, .-_ZNK12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification17serializeToBinaryEv:
.LFB6451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4250
	movq	(%rdi), %rax
	call	*24(%rax)
.L4250:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4257
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4257:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6451:
	.size	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification15serializeToJSONEv:
.LFB6450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4258
	movq	(%rdi), %rax
	call	*24(%rax)
.L4258:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4265
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4265:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6450:
	.size	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification5cloneEv
	.type	_ZNK12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification5cloneEv, @function
_ZNK12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification5cloneEv:
.LFB6515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4267
	movq	(%rdi), %rax
	call	*24(%rax)
.L4267:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4273
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4273:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6515:
	.size	_ZNK12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification5cloneEv, .-_ZNK12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification5cloneEv
	.section	.text._ZNK12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification7toValueEv
	.type	_ZNK12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification7toValueEv, @function
_ZNK12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification7toValueEv:
.LFB6511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r15, 0(%r13)
	movl	$56, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rsi
	movq	%rax, %r12
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%r12)
	leaq	32(%r12), %rax
	leaq	16(%r12), %rdi
	movq	%rax, 16(%r12)
	movq	16(%rbx), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	40(%rbx), %rax
	movq	%r12, -104(%rbp)
	leaq	.LC16(%rip), %rsi
	movq	%rax, 48(%r12)
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rdi
	je	.L4275
	call	_ZdlPv@PLT
.L4275:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4276
	movq	(%rdi), %rax
	call	*24(%rax)
.L4276:
	movq	0(%r13), %r8
	movq	48(%rbx), %rsi
	movq	%r14, %rdi
	leaq	-112(%rbp), %r15
	movq	%r8, -128(%rbp)
	call	_ZNK12v8_inspector8protocol8Debugger8Location7toValueEv@PLT
	movq	-104(%rbp), %rax
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L4277
	call	_ZdlPv@PLT
.L4277:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4278
	movq	(%rdi), %rax
	call	*24(%rax)
.L4278:
	movq	0(%r13), %r8
	movq	56(%rbx), %rsi
	movq	%r14, %rdi
	movq	%r8, -128(%rbp)
	call	_ZNK12v8_inspector8protocol8Profiler7Profile7toValueEv
	movq	-104(%rbp), %rax
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L4279
	call	_ZdlPv@PLT
.L4279:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4280
	movq	(%rdi), %rax
	call	*24(%rax)
.L4280:
	cmpb	$0, 64(%rbx)
	jne	.L4298
.L4274:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4299
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4298:
	.cfi_restore_state
	movq	0(%r13), %r8
	movl	$56, %edi
	movq	%r8, -128(%rbp)
	call	_Znwm@PLT
	movq	72(%rbx), %rsi
	movq	%rax, %r15
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%r15)
	leaq	32(%r15), %rax
	leaq	16(%r15), %rdi
	movq	%rax, 16(%r15)
	movq	80(%rbx), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	104(%rbx), %rax
	movq	%r12, %rdi
	movq	%r15, -104(%rbp)
	leaq	.LC18(%rip), %rsi
	movq	%rax, 48(%r15)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L4282
	call	_ZdlPv@PLT
.L4282:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4274
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4274
.L4299:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6511:
	.size	_ZNK12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification7toValueEv, .-_ZNK12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification7toValueEv
	.section	.text._ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification17serializeToBinaryEv:
.LFB6418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4300
	movq	(%rdi), %rax
	call	*24(%rax)
.L4300:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4307
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4307:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6418:
	.size	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification17serializeToBinaryEv, .-_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification15serializeToJSONEv, @function
_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification15serializeToJSONEv:
.LFB6417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4308
	movq	(%rdi), %rax
	call	*24(%rax)
.L4308:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4315
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4315:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6417:
	.size	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification15serializeToJSONEv, .-_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification15serializeToJSONEv
	.section	.text._ZNK12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification5cloneEv
	.type	_ZNK12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification5cloneEv, @function
_ZNK12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification5cloneEv:
.LFB6512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4317
	movq	(%rdi), %rax
	call	*24(%rax)
.L4317:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4323
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4323:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6512:
	.size	_ZNK12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification5cloneEv, .-_ZNK12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification5cloneEv
	.section	.text._ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Profiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m,"axG",@progbits,_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Profiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Profiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	.type	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Profiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m, @function
_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Profiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m:
.LFB13471:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax,%rsi,8), %r10
	testq	%r10, %r10
	je	.L4341
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%r10), %rdx
	movl	$2147483648, %ebx
	movq	64(%rdx), %r8
	cmpq	%r8, %rcx
	je	.L4344
	.p2align 4,,10
	.p2align 3
.L4326:
	movq	(%rdx), %r9
	testq	%r9, %r9
	je	.L4331
	movq	64(%r9), %r8
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	8(%rdi)
	cmpq	%rdx, %rsi
	jne	.L4331
	movq	%r9, %rdx
	cmpq	%r8, %rcx
	jne	.L4326
.L4344:
	movq	8(%r11), %r8
	movq	16(%rdx), %r15
	movq	8(%rdx), %r13
	movq	(%r11), %r14
	cmpq	%r15, %r8
	movq	%r15, %r9
	cmovbe	%r8, %r9
	testq	%r9, %r9
	je	.L4327
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L4328:
	movzwl	0(%r13,%rax,2), %r12d
	cmpw	%r12w, (%r14,%rax,2)
	jne	.L4326
	addq	$1, %rax
	cmpq	%rax, %r9
	jne	.L4328
.L4327:
	subq	%r15, %r8
	cmpq	%rbx, %r8
	jge	.L4326
	movabsq	$-2147483649, %rax
	cmpq	%rax, %r8
	jle	.L4326
	testl	%r8d, %r8d
	jne	.L4326
	popq	%rbx
	movq	%r10, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4331:
	.cfi_restore_state
	xorl	%r10d, %r10d
	popq	%rbx
	popq	%r12
	movq	%r10, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4341:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	%r10, %rax
	ret
	.cfi_endproc
.LFE13471:
	.size	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Profiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m, .-_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Profiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	.section	.text.unlikely._ZN12v8_inspector8protocol8Profiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE,"ax",@progbits
	.align 2
.LCOLDB47:
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE,"ax",@progbits
.LHOTB47:
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE:
.LFB6577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$72, %rdi
	subq	$104, %rsp
	movl	%esi, -132(%rbp)
	movq	32(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	jne	.L4346
	movq	(%rdx), %rax
	movq	8(%rdx), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L4349
	.p2align 4,,10
	.p2align 3
.L4348:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%r14)
	cmpq	%rax, %rsi
	jne	.L4348
	testq	%rcx, %rcx
	je	.L4349
.L4346:
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	80(%rbx)
	movq	%rdx, %rsi
	movq	%r14, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Profiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	testq	%rax, %rax
	je	.L4350
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	movq	%rdx, -144(%rbp)
	je	.L4350
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	movq	-144(%rbp), %rdx
	movq	48(%rdx), %rax
	addq	56(%rdx), %rbx
	movq	%rbx, %rdi
	testb	$1, %al
	jne	.L4373
.L4351:
	movq	(%r15), %rdx
	movl	-132(%rbp), %esi
	movq	%r12, %r9
	leaq	-120(%rbp), %r8
	movq	$0, (%r15)
	movq	%r13, %rcx
	movq	%rdx, -120(%rbp)
	movq	%r14, %rdx
	call	*%rax
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4352
	movq	(%rdi), %rax
	call	*24(%rax)
.L4352:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4374
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4373:
	.cfi_restore_state
	movq	(%rbx), %rdx
	movq	-1(%rdx,%rax), %rax
	jmp	.L4351
	.p2align 4,,10
	.p2align 3
.L4349:
	movq	$1, 32(%r14)
	movl	$1, %ecx
	jmp	.L4346
.L4374:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN12v8_inspector8protocol8Profiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.cfi_startproc
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold:
.LFSB6577:
.L4350:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	-112(%rbp), %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	movq	48, %rax
	ud2
	.cfi_endproc
.LFE6577:
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.section	.text.unlikely._ZN12v8_inspector8protocol8Profiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold
.LCOLDE47:
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
.LHOTE47:
	.section	.text._ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_,"axG",@progbits,_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	.type	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_, @function
_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_:
.LFB10118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	32(%rsi), %r12
	testq	%r12, %r12
	jne	.L4376
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rcx
	cmpq	%rcx, %rax
	je	.L4379
	.p2align 4,,10
	.p2align 3
.L4378:
	movq	%r12, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%r12, %rdx
	movq	%rdx, %r12
	movsbq	-2(%rax), %rdx
	addq	%rdx, %r12
	movq	%r12, 32(%rbx)
	cmpq	%rax, %rcx
	jne	.L4378
	testq	%r12, %r12
	je	.L4379
.L4376:
	xorl	%edx, %edx
	movq	%r12, %rax
	movq	%r12, %rcx
	movq	%r13, %rdi
	divq	8(%r13)
	movq	%rdx, %r15
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Profiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	testq	%rax, %rax
	je	.L4380
	movq	(%rax), %rdx
	leaq	48(%rdx), %rax
	testq	%rdx, %rdx
	je	.L4380
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4379:
	.cfi_restore_state
	movq	$1, 32(%rbx)
	movl	$1, %r12d
	jmp	.L4376
	.p2align 4,,10
	.p2align 3
.L4380:
	movl	$72, %edi
	call	_Znwm@PLT
	movq	(%rbx), %rdx
	movq	$0, (%rax)
	movq	%rax, %r14
	leaq	24(%rax), %rax
	movq	%rax, 8(%r14)
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdx
	je	.L4417
	movq	%rdx, 8(%r14)
	movq	16(%rbx), %rdx
	movq	%rdx, 24(%r14)
.L4383:
	movq	%rax, (%rbx)
	movq	8(%rbx), %rdx
	xorl	%eax, %eax
	leaq	32(%r13), %rdi
	movw	%ax, 16(%rbx)
	movq	32(%rbx), %rax
	movl	$1, %ecx
	movq	$0, 8(%rbx)
	movq	8(%r13), %rsi
	movq	%rdx, 16(%r14)
	movq	24(%r13), %rdx
	movq	%rax, 40(%r14)
	movq	$0, 48(%r14)
	movq	$0, 56(%r14)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %rbx
	testb	%al, %al
	jne	.L4384
	movq	0(%r13), %r8
.L4385:
	salq	$3, %r15
	movq	%r12, 64(%r14)
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L4394
	movq	(%rdx), %rdx
	movq	%rdx, (%r14)
	movq	(%rax), %rax
	movq	%r14, (%rax)
.L4395:
	addq	$1, 24(%r13)
	addq	$24, %rsp
	leaq	48(%r14), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4384:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L4418
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L4419
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r13), %r10
	movq	%rax, %r8
.L4387:
	movq	16(%r13), %rsi
	movq	$0, 16(%r13)
	testq	%rsi, %rsi
	je	.L4389
	xorl	%edi, %edi
	leaq	16(%r13), %r9
	jmp	.L4390
	.p2align 4,,10
	.p2align 3
.L4391:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L4392:
	testq	%rsi, %rsi
	je	.L4389
.L4390:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	64(%rcx), %rax
	divq	%rbx
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L4391
	movq	16(%r13), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r13)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L4398
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L4390
	.p2align 4,,10
	.p2align 3
.L4389:
	movq	0(%r13), %rdi
	cmpq	%rdi, %r10
	je	.L4393
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L4393:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rbx, 8(%r13)
	divq	%rbx
	movq	%r8, 0(%r13)
	movq	%rdx, %r15
	jmp	.L4385
	.p2align 4,,10
	.p2align 3
.L4417:
	movdqu	16(%rbx), %xmm0
	movups	%xmm0, 24(%r14)
	jmp	.L4383
	.p2align 4,,10
	.p2align 3
.L4394:
	movq	16(%r13), %rdx
	movq	%r14, 16(%r13)
	movq	%rdx, (%r14)
	testq	%rdx, %rdx
	je	.L4396
	movq	64(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r13)
	movq	%r14, (%r8,%rdx,8)
	movq	0(%r13), %rax
	addq	%r15, %rax
.L4396:
	leaq	16(%r13), %rdx
	movq	%rdx, (%rax)
	jmp	.L4395
	.p2align 4,,10
	.p2align 3
.L4398:
	movq	%rdx, %rdi
	jmp	.L4392
.L4418:
	leaq	48(%r13), %r8
	movq	$0, 48(%r13)
	movq	%r8, %r10
	jmp	.L4387
.L4419:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10118:
	.size	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_, .-_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	.section	.rodata._ZN12v8_inspector8protocol8Profiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE.str1.1,"aMS",@progbits,1
.LC49:
	.string	"Profiler.disable"
.LC50:
	.string	"Profiler.enable"
	.section	.rodata._ZN12v8_inspector8protocol8Profiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE.str1.8,"aMS",@progbits,1
	.align 8
.LC51:
	.string	"Profiler.getBestEffortCoverage"
	.section	.rodata._ZN12v8_inspector8protocol8Profiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE.str1.1
.LC52:
	.string	"Profiler.setSamplingInterval"
.LC53:
	.string	"Profiler.start"
.LC54:
	.string	"Profiler.startPreciseCoverage"
.LC55:
	.string	"Profiler.startTypeProfile"
.LC56:
	.string	"Profiler.stop"
.LC57:
	.string	"Profiler.stopPreciseCoverage"
.LC58:
	.string	"Profiler.stopTypeProfile"
.LC59:
	.string	"Profiler.takePreciseCoverage"
.LC60:
	.string	"Profiler.takeTypeProfile"
.LC61:
	.string	"Profiler"
	.section	.text._ZN12v8_inspector8protocol8Profiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE
	.type	_ZN12v8_inspector8protocol8Profiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE, @function
_ZN12v8_inspector8protocol8Profiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE:
.LFB6676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movl	$192, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%r13, %rsi
	leaq	-80(%rbp), %r13
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol14DispatcherBaseC2EPNS0_15FrontendChannelE@PLT
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler14DispatcherImplE(%rip), %rax
	movss	.LC48(%rip), %xmm0
	movq	%r12, 184(%rbx)
	movq	%rax, (%rbx)
	leaq	120(%rbx), %rax
	leaq	-96(%rbp), %r12
	movq	%rax, 72(%rbx)
	movq	%r12, %rdi
	leaq	72(%rbx), %r14
	leaq	176(%rbx), %rax
	movq	%rax, 128(%rbx)
	leaq	.LC49(%rip), %rsi
	movq	$1, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	movq	$1, 136(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 168(%rbx)
	movq	$0, 176(%rbx)
	movss	%xmm0, 104(%rbx)
	movss	%xmm0, 160(%rbx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl7disableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4421
	call	_ZdlPv@PLT
.L4421:
	leaq	.LC50(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl6enableEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4422
	call	_ZdlPv@PLT
.L4422:
	leaq	.LC51(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl21getBestEffortCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4423
	call	_ZdlPv@PLT
.L4423:
	leaq	.LC52(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19setSamplingIntervalEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4424
	call	_ZdlPv@PLT
.L4424:
	leaq	.LC53(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl5startEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4425
	call	_ZdlPv@PLT
.L4425:
	leaq	.LC54(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl20startPreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4426
	call	_ZdlPv@PLT
.L4426:
	leaq	.LC55(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl16startTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4427
	call	_ZdlPv@PLT
.L4427:
	leaq	.LC56(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl4stopEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4428
	call	_ZdlPv@PLT
.L4428:
	leaq	.LC57(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19stopPreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4429
	call	_ZdlPv@PLT
.L4429:
	leaq	.LC58(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl15stopTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4430
	call	_ZdlPv@PLT
.L4430:
	leaq	.LC59(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl19takePreciseCoverageEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rdx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4431
	call	_ZdlPv@PLT
.L4431:
	leaq	.LC60(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseIN12v8_inspector8String16ESt4pairIKS2_MNS1_8protocol8Profiler14DispatcherImplEFviRS4_RKNS5_15ProtocolMessageESt10unique_ptrINS5_15DictionaryValueESt14default_deleteISD_EEPNS5_12ErrorSupportEEESaISL_ENS_10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS2_
	leaq	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl15takeTypeProfileEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4432
	call	_ZdlPv@PLT
.L4432:
	leaq	128(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINS_8String16ES3_St4hashIS3_ESt8equal_toIS3_ESaISt4pairIKS3_S3_EEE@PLT
	leaq	.LC61(%rip), %rsi
	movq	%r12, %rdi
	movq	%rbx, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	leaq	-104(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector8protocol14UberDispatcher15registerBackendERKNS_8String16ESt10unique_ptrINS0_14DispatcherBaseESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4433
	call	_ZdlPv@PLT
.L4433:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L4420
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol8Profiler14DispatcherImplD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4435
	movq	144(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol8Profiler14DispatcherImplE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L4440
	.p2align 4,,10
	.p2align 3
.L4436:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L4441
	call	_ZdlPv@PLT
.L4441:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L4445
	.p2align 4,,10
	.p2align 3
.L4442:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L4446
	call	_ZdlPv@PLT
.L4446:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev@PLT
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4420:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4470
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4471:
	.cfi_restore_state
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L4436
.L4439:
	movq	%rbx, %r13
.L4440:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L4437
	call	_ZdlPv@PLT
.L4437:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L4471
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L4439
	jmp	.L4436
	.p2align 4,,10
	.p2align 3
.L4472:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L4442
.L4444:
	movq	%rbx, %r13
.L4445:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L4472
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L4444
	jmp	.L4442
	.p2align 4,,10
	.p2align 3
.L4435:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4420
.L4470:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6676:
	.size	_ZN12v8_inspector8protocol8Profiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE, .-_ZN12v8_inspector8protocol8Profiler10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE
	.section	.text._ZN12v8_inspector8protocol8Profiler14DispatcherImpl11canDispatchERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl11canDispatchERKNS_8String16E
	.type	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl11canDispatchERKNS_8String16E, @function
_ZN12v8_inspector8protocol8Profiler14DispatcherImpl11canDispatchERKNS_8String16E:
.LFB6576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32(%rsi), %rcx
	movq	%rsi, %r8
	leaq	72(%rdi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rcx, %rcx
	jne	.L4474
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L4477
	.p2align 4,,10
	.p2align 3
.L4476:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%r8)
	cmpq	%rax, %rsi
	jne	.L4476
	testq	%rcx, %rcx
	je	.L4477
.L4474:
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	80(%rdi)
	movq	%r9, %rdi
	movq	%rdx, %rsi
	movq	%r8, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol8Profiler14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L4473
	cmpq	$0, (%rax)
	setne	%r8b
.L4473:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4477:
	.cfi_restore_state
	movq	$1, 32(%r8)
	movl	$1, %ecx
	jmp	.L4474
	.cfi_endproc
.LFE6576:
	.size	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl11canDispatchERKNS_8String16E, .-_ZN12v8_inspector8protocol8Profiler14DispatcherImpl11canDispatchERKNS_8String16E
	.weak	_ZTVN12v8_inspector8protocol23InternalRawNotificationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol23InternalRawNotificationE,"awG",@progbits,_ZTVN12v8_inspector8protocol23InternalRawNotificationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol23InternalRawNotificationE, @object
	.size	_ZTVN12v8_inspector8protocol23InternalRawNotificationE, 48
_ZTVN12v8_inspector8protocol23InternalRawNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol23InternalRawNotificationD1Ev
	.quad	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime9CallFrameE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime9CallFrameE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime9CallFrameE, 48
_ZTVN12v8_inspector8protocol7Runtime9CallFrameE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrame17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime9CallFrameD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Debugger8LocationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Debugger8LocationE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Debugger8LocationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Debugger8LocationE, @object
	.size	_ZTVN12v8_inspector8protocol8Debugger8LocationE, 48
_ZTVN12v8_inspector8protocol8Debugger8LocationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Debugger8Location15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Debugger8Location17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Debugger8LocationD1Ev
	.quad	_ZN12v8_inspector8protocol8Debugger8LocationD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE, 48
_ZTVN12v8_inspector8protocol8Profiler11ProfileNodeE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler11ProfileNode15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler11ProfileNode17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler11ProfileNodeD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler7ProfileE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler7ProfileE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler7ProfileE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler7ProfileE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler7ProfileE, 48
_ZTVN12v8_inspector8protocol8Profiler7ProfileE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler7Profile15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler7Profile17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler7ProfileD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler7ProfileD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler16PositionTickInfoE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler16PositionTickInfoE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler16PositionTickInfoE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler16PositionTickInfoE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler16PositionTickInfoE, 48
_ZTVN12v8_inspector8protocol8Profiler16PositionTickInfoE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler16PositionTickInfo17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler16PositionTickInfoD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler13CoverageRangeE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler13CoverageRangeE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler13CoverageRangeE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler13CoverageRangeE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler13CoverageRangeE, 48
_ZTVN12v8_inspector8protocol8Profiler13CoverageRangeE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler13CoverageRange15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler13CoverageRange17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler13CoverageRangeD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE, 48
_ZTVN12v8_inspector8protocol8Profiler16FunctionCoverageE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler16FunctionCoverage17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler16FunctionCoverageD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE, 48
_ZTVN12v8_inspector8protocol8Profiler14ScriptCoverageE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler14ScriptCoverage17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler14ScriptCoverageD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler10TypeObjectE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE, 48
_ZTVN12v8_inspector8protocol8Profiler10TypeObjectE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler10TypeObject15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler10TypeObject17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler10TypeObjectD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler10TypeObjectD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE, 48
_ZTVN12v8_inspector8protocol8Profiler16TypeProfileEntryE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntry17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler16TypeProfileEntryD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE, 48
_ZTVN12v8_inspector8protocol8Profiler17ScriptTypeProfileE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfile17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler17ScriptTypeProfileD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationE, 48
_ZTVN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotification17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler34ConsoleProfileFinishedNotificationD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationE, 48
_ZTVN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotification17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler33ConsoleProfileStartedNotificationD0Ev
	.weak	_ZTVN12v8_inspector8protocol8Profiler14DispatcherImplE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol8Profiler14DispatcherImplE,"awG",@progbits,_ZTVN12v8_inspector8protocol8Profiler14DispatcherImplE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol8Profiler14DispatcherImplE, @object
	.size	_ZTVN12v8_inspector8protocol8Profiler14DispatcherImplE, 48
_ZTVN12v8_inspector8protocol8Profiler14DispatcherImplE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol8Profiler14DispatcherImplD1Ev
	.quad	_ZN12v8_inspector8protocol8Profiler14DispatcherImplD0Ev
	.quad	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl11canDispatchERKNS_8String16E
	.quad	_ZN12v8_inspector8protocol8Profiler14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.globl	_ZN12v8_inspector8protocol8Profiler8Metainfo7versionE
	.section	.rodata._ZN12v8_inspector8protocol8Profiler8Metainfo7versionE,"a"
	.type	_ZN12v8_inspector8protocol8Profiler8Metainfo7versionE, @object
	.size	_ZN12v8_inspector8protocol8Profiler8Metainfo7versionE, 4
_ZN12v8_inspector8protocol8Profiler8Metainfo7versionE:
	.string	"1.3"
	.globl	_ZN12v8_inspector8protocol8Profiler8Metainfo13commandPrefixE
	.section	.rodata._ZN12v8_inspector8protocol8Profiler8Metainfo13commandPrefixE,"a"
	.align 8
	.type	_ZN12v8_inspector8protocol8Profiler8Metainfo13commandPrefixE, @object
	.size	_ZN12v8_inspector8protocol8Profiler8Metainfo13commandPrefixE, 10
_ZN12v8_inspector8protocol8Profiler8Metainfo13commandPrefixE:
	.string	"Profiler."
	.globl	_ZN12v8_inspector8protocol8Profiler8Metainfo10domainNameE
	.section	.rodata._ZN12v8_inspector8protocol8Profiler8Metainfo10domainNameE,"a"
	.align 8
	.type	_ZN12v8_inspector8protocol8Profiler8Metainfo10domainNameE, @object
	.size	_ZN12v8_inspector8protocol8Profiler8Metainfo10domainNameE, 9
_ZN12v8_inspector8protocol8Profiler8Metainfo10domainNameE:
	.string	"Profiler"
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC48:
	.long	1065353216
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
