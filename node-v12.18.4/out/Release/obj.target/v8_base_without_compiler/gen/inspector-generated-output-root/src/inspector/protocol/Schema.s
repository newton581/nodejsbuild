	.file	"Schema.cpp"
	.text
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv, @function
_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv:
.LFB4422:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movq	8(%rsi), %rcx
	leaq	24(%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L5
	movq	%rcx, (%rdi)
	movq	24(%rsi), %rcx
	movq	%rcx, 16(%rdi)
.L3:
	movq	16(%rsi), %rcx
	movq	%rdx, 8(%rsi)
	xorl	%edx, %edx
	movq	$0, 16(%rsi)
	movq	%rcx, 8(%rax)
	movw	%dx, 24(%rsi)
	movq	40(%rsi), %rdx
	movq	%rdx, 32(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movdqu	24(%rsi), %xmm0
	movups	%xmm0, 16(%rdi)
	jmp	.L3
	.cfi_endproc
.LFE4422:
	.size	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv, .-_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv:
.LFB4423:
	.cfi_startproc
	endbr64
	movdqu	48(%rsi), %xmm1
	movq	64(%rsi), %rdx
	pxor	%xmm0, %xmm0
	movq	%rdi, %rax
	movq	$0, 64(%rsi)
	movq	%rdx, 16(%rdi)
	movups	%xmm1, (%rdi)
	movups	%xmm0, 48(%rsi)
	ret
	.cfi_endproc
.LFE4423:
	.size	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv, .-_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol6Schema6DomainD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol6Schema6DomainD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol6Schema6DomainD2Ev
	.type	_ZN12v8_inspector8protocol6Schema6DomainD2Ev, @function
_ZN12v8_inspector8protocol6Schema6DomainD2Ev:
.LFB4441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	72(%rbx), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	56(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L7
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4441:
	.size	_ZN12v8_inspector8protocol6Schema6DomainD2Ev, .-_ZN12v8_inspector8protocol6Schema6DomainD2Ev
	.weak	_ZN12v8_inspector8protocol6Schema6DomainD1Ev
	.set	_ZN12v8_inspector8protocol6Schema6DomainD1Ev,_ZN12v8_inspector8protocol6Schema6DomainD2Ev
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotificationD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev
	.type	_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev, @function
_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev:
.LFB4419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L11
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4419:
	.size	_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev, .-_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev
	.weak	_ZN12v8_inspector8protocol23InternalRawNotificationD1Ev
	.set	_ZN12v8_inspector8protocol23InternalRawNotificationD1Ev,_ZN12v8_inspector8protocol23InternalRawNotificationD2Ev
	.section	.text._ZN12v8_inspector8protocol6Schema14DispatcherImplD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol6Schema14DispatcherImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol6Schema14DispatcherImplD2Ev
	.type	_ZN12v8_inspector8protocol6Schema14DispatcherImplD2Ev, @function
_ZN12v8_inspector8protocol6Schema14DispatcherImplD2Ev:
.LFB4554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol6Schema14DispatcherImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L23
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L49:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L19
.L22:
	movq	%rbx, %r13
.L23:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L20
	call	_ZdlPv@PLT
.L20:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L49
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L22
.L19:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L28
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L50:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L25
.L27:
	movq	%rbx, %r13
.L28:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L50
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L27
.L25:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev@PLT
	.cfi_endproc
.LFE4554:
	.size	_ZN12v8_inspector8protocol6Schema14DispatcherImplD2Ev, .-_ZN12v8_inspector8protocol6Schema14DispatcherImplD2Ev
	.weak	_ZN12v8_inspector8protocol6Schema14DispatcherImplD1Ev
	.set	_ZN12v8_inspector8protocol6Schema14DispatcherImplD1Ev,_ZN12v8_inspector8protocol6Schema14DispatcherImplD2Ev
	.section	.text._ZN12v8_inspector8protocol23InternalRawNotificationD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol23InternalRawNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev
	.type	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev, @function
_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev:
.LFB4421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L52
	call	_ZdlPv@PLT
.L52:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L53
	call	_ZdlPv@PLT
.L53:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4421:
	.size	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev, .-_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev
	.section	.text._ZN12v8_inspector8protocol6Schema6DomainD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol6Schema6DomainD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol6Schema6DomainD0Ev
	.type	_ZThn8_N12v8_inspector8protocol6Schema6DomainD0Ev, @function
_ZThn8_N12v8_inspector8protocol6Schema6DomainD0Ev:
.LFB7233:
	.cfi_startproc
	endbr64
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movups	%xmm0, -8(%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L60
	call	_ZdlPv@PLT
.L60:
	popq	%rbx
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7233:
	.size	_ZThn8_N12v8_inspector8protocol6Schema6DomainD0Ev, .-_ZThn8_N12v8_inspector8protocol6Schema6DomainD0Ev
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol6Schema6DomainD0Ev
	.type	_ZN12v8_inspector8protocol6Schema6DomainD0Ev, @function
_ZN12v8_inspector8protocol6Schema6DomainD0Ev:
.LFB4443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	72(%r12), %rax
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	56(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L64
	call	_ZdlPv@PLT
.L64:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4443:
	.size	_ZN12v8_inspector8protocol6Schema6DomainD0Ev, .-_ZN12v8_inspector8protocol6Schema6DomainD0Ev
	.section	.text._ZN12v8_inspector8protocol6Schema6DomainD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol6Schema6DomainD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol6Schema6DomainD1Ev
	.type	_ZThn8_N12v8_inspector8protocol6Schema6DomainD1Ev, @function
_ZThn8_N12v8_inspector8protocol6Schema6DomainD1Ev:
.LFB7234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rbx), %rax
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	movq	48(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L66
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7234:
	.size	_ZThn8_N12v8_inspector8protocol6Schema6DomainD1Ev, .-_ZThn8_N12v8_inspector8protocol6Schema6DomainD1Ev
	.section	.text._ZN12v8_inspector8protocol6Schema14DispatcherImplD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol6Schema14DispatcherImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol6Schema14DispatcherImplD0Ev
	.type	_ZN12v8_inspector8protocol6Schema14DispatcherImplD0Ev, @function
_ZN12v8_inspector8protocol6Schema14DispatcherImplD0Ev:
.LFB4556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol6Schema14DispatcherImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L75
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L101:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L71
.L74:
	movq	%rbx, %r13
.L75:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L101
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L74
.L71:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L80
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L102:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L77
.L79:
	movq	%rbx, %r13
.L80:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L102
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L79
.L77:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L81
	call	_ZdlPv@PLT
.L81:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4556:
	.size	_ZN12v8_inspector8protocol6Schema14DispatcherImplD0Ev, .-_ZN12v8_inspector8protocol6Schema14DispatcherImplD0Ev
	.section	.rodata._ZN12v8_inspector8protocol6Schema6Domain9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"object expected"
.LC1:
	.string	"name"
.LC2:
	.string	"string value expected"
.LC3:
	.string	"version"
	.section	.text._ZN12v8_inspector8protocol6Schema6Domain9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol6Schema6Domain9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol6Schema6Domain9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol6Schema6Domain9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE:
.LFB4490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L104
	cmpl	$6, 8(%rsi)
	movq	%rsi, %rbx
	je	.L105
.L104:
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, (%r12)
.L103:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	movl	$96, %edi
	xorl	%r13d, %r13d
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm2
	movq	%rdx, %xmm1
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -128(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm1
	xorl	%r11d, %r11d
	movq	%r15, %rdi
	movq	%rax, %r14
	leaq	32(%rax), %rax
	cmpl	$6, 8(%rbx)
	movups	%xmm1, -32(%rax)
	movq	%rax, -112(%rbp)
	movq	%rax, 16(%r14)
	leaq	72(%r14), %rax
	movq	%rax, -136(%rbp)
	movq	%rax, 56(%r14)
	movl	$0, %eax
	cmovne	%rax, %rbx
	movw	%r13w, 72(%r14)
	leaq	-96(%rbp), %r13
	movw	%r11w, 32(%r14)
	movq	$0, 24(%r14)
	movq	$0, 48(%r14)
	movq	$0, 64(%r14)
	movq	$0, 88(%r14)
	call	_ZN12v8_inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r11
	leaq	-80(%rbp), %rax
	movq	%rax, -104(%rbp)
	cmpq	%rax, %rdi
	je	.L108
	movq	%r11, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r11
.L108:
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%r11, -144(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-104(%rbp), %rax
	xorl	%r10d, %r10d
	movq	-144(%rbp), %r11
	movq	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	movw	%r10w, -80(%rbp)
	movq	$0, -64(%rbp)
	testq	%r11, %r11
	je	.L111
	movq	(%r11), %rax
	movq	%r13, %rsi
	movq	%r11, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L110
.L111:
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L110:
	movq	16(%r14), %rdi
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rax
	cmpq	-104(%rbp), %rdx
	je	.L166
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -112(%rbp)
	je	.L167
	movq	%rax, %xmm0
	movq	%rcx, %xmm4
	movq	32(%r14), %rsi
	movq	%rdx, 16(%r14)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 24(%r14)
	testq	%rdi, %rdi
	je	.L117
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L115:
	xorl	%r8d, %r8d
	movq	$0, -88(%rbp)
	movw	%r8w, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 48(%r14)
	cmpq	-104(%rbp), %rdi
	je	.L118
	call	_ZdlPv@PLT
.L118:
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8protocol15DictionaryValue3getERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r8
	cmpq	-104(%rbp), %rdi
	je	.L119
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L119:
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%r8, -144(%rbp)
	call	_ZN12v8_inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-104(%rbp), %rax
	xorl	%esi, %esi
	movq	-144(%rbp), %r8
	movq	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	movw	%si, -80(%rbp)
	movq	$0, -64(%rbp)
	testq	%r8, %r8
	je	.L122
	movq	(%r8), %rax
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L121
.L122:
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L121:
	movq	56(%r14), %rdi
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rax
	cmpq	-104(%rbp), %rdx
	je	.L168
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -136(%rbp)
	je	.L169
	movq	%rax, %xmm0
	movq	%rcx, %xmm5
	movq	72(%r14), %rsi
	movq	%rdx, 56(%r14)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 64(%r14)
	testq	%rdi, %rdi
	je	.L128
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L126:
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movw	%ax, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 88(%r14)
	cmpq	-104(%rbp), %rdi
	je	.L129
	call	_ZdlPv@PLT
.L129:
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L170
	movq	%r14, (%r12)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L170:
	movq	(%r14), %rax
	movq	$0, (%r12)
	leaq	_ZN12v8_inspector8protocol6Schema6DomainD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L171
	movdqa	-128(%rbp), %xmm3
	movq	56(%r14), %rdi
	movups	%xmm3, (%r14)
	cmpq	%rdi, -136(%rbp)
	je	.L130
	call	_ZdlPv@PLT
.L130:
	movq	16(%r14), %rdi
	cmpq	%rdi, -112(%rbp)
	je	.L131
	call	_ZdlPv@PLT
.L131:
	movl	$96, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L168:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L124
	cmpq	$1, %rax
	je	.L172
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L124
	movq	-104(%rbp), %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	56(%r14), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L124:
	xorl	%ecx, %ecx
	movq	%rax, 64(%r14)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L166:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L113
	cmpq	$1, %rax
	je	.L173
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L113
	movq	-104(%rbp), %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	16(%r14), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L113:
	xorl	%r9d, %r9d
	movq	%rax, 24(%r14)
	movw	%r9w, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%rax, %xmm0
	movq	%rcx, %xmm7
	movq	%rdx, 56(%r14)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 64(%r14)
.L128:
	movq	-104(%rbp), %rax
	movq	%rax, -96(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%rax, %rdi
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L167:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%rdx, 16(%r14)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 24(%r14)
.L117:
	movq	-104(%rbp), %rax
	movq	%rax, -96(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%rax, %rdi
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L171:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L103
.L172:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	56(%r14), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L124
.L173:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	16(%r14), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L113
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4490:
	.size	_ZN12v8_inspector8protocol6Schema6Domain9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol6Schema6Domain9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	.section	.text._ZN12v8_inspector8protocol6Schema3API6Domain14fromJSONStringERKNS_10StringViewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol6Schema3API6Domain14fromJSONStringERKNS_10StringViewE
	.type	_ZN12v8_inspector8protocol6Schema3API6Domain14fromJSONStringERKNS_10StringViewE, @function
_ZN12v8_inspector8protocol6Schema3API6Domain14fromJSONStringERKNS_10StringViewE:
.LFB4503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	movq	%r13, %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN12v8_inspector8protocol10StringUtil9parseJSONERKNS_10StringViewE@PLT
	movq	-112(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L185
	leaq	-104(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZN12v8_inspector8protocol6Schema6Domain9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdi
	leaq	8(%rax), %rdx
	testq	%rax, %rax
	cmovne	%rdx, %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L176
	movq	(%rdi), %rax
	call	*24(%rax)
.L176:
	movq	%r14, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L186
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movq	$0, (%r12)
	jmp	.L176
.L186:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4503:
	.size	_ZN12v8_inspector8protocol6Schema3API6Domain14fromJSONStringERKNS_10StringViewE, .-_ZN12v8_inspector8protocol6Schema3API6Domain14fromJSONStringERKNS_10StringViewE
	.section	.text._ZN12v8_inspector8protocol6Schema3API6Domain10fromBinaryEPKhm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol6Schema3API6Domain10fromBinaryEPKhm
	.type	_ZN12v8_inspector8protocol6Schema3API6Domain10fromBinaryEPKhm, @function
_ZN12v8_inspector8protocol6Schema3API6Domain10fromBinaryEPKhm:
.LFB4504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	movq	%r13, %rsi
	leaq	-112(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZN12v8_inspector8protocol5Value11parseBinaryEPKhm@PLT
	movq	-112(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L198
	leaq	-104(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN12v8_inspector8protocol6Schema6Domain9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdi
	leaq	8(%rax), %rdx
	testq	%rax, %rax
	cmovne	%rdx, %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L189
	movq	(%rdi), %rax
	call	*24(%rax)
.L189:
	movq	%r15, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L199
	addq	$80, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movq	$0, (%r12)
	jmp	.L189
.L199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4504:
	.size	_ZN12v8_inspector8protocol6Schema3API6Domain10fromBinaryEPKhm, .-_ZN12v8_inspector8protocol6Schema3API6Domain10fromBinaryEPKhm
	.section	.text._ZN12v8_inspector8protocol6Schema8Frontend5flushEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol6Schema8Frontend5flushEv
	.type	_ZN12v8_inspector8protocol6Schema8Frontend5flushEv, @function
_ZN12v8_inspector8protocol6Schema8Frontend5flushEv:
.LFB4505:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE4505:
	.size	_ZN12v8_inspector8protocol6Schema8Frontend5flushEv, .-_ZN12v8_inspector8protocol6Schema8Frontend5flushEv
	.section	.text._ZN12v8_inspector8protocol6Schema8Frontend23sendRawJSONNotificationENS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol6Schema8Frontend23sendRawJSONNotificationENS_8String16E
	.type	_ZN12v8_inspector8protocol6Schema8Frontend23sendRawJSONNotificationENS_8String16E, @function
_ZN12v8_inspector8protocol6Schema8Frontend23sendRawJSONNotificationENS_8String16E:
.LFB4506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rsi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	movq	24(%rax), %r14
	movq	(%rsi), %rax
	cmpq	%rdx, %rax
	je	.L215
	movq	%rdx, (%rsi)
	xorl	%edx, %edx
	movq	16(%rsi), %rcx
	leaq	-64(%rbp), %r13
	movw	%dx, 16(%rsi)
	movq	32(%rsi), %rdx
	leaq	-112(%rbp), %rbx
	movq	8(%rsi), %rdi
	movq	%rcx, -112(%rbp)
	movq	$0, 8(%rsi)
	movq	%rdx, -96(%rbp)
	movq	%r13, -80(%rbp)
	cmpq	%rbx, %rax
	je	.L203
	movq	%rax, -80(%rbp)
	movq	%rcx, -64(%rbp)
.L205:
	movq	%rdi, -72(%rbp)
	xorl	%eax, %eax
	movl	$72, %edi
	movq	%rdx, -48(%rbp)
	movq	%rbx, -128(%rbp)
	movq	$0, -120(%rbp)
	movw	%ax, -112(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rcx
	leaq	24(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	movq	-80(%rbp), %rdx
	cmpq	%r13, %rdx
	je	.L216
	movq	%rdx, 8(%rax)
	movq	-64(%rbp), %rdx
	movq	%rdx, 24(%rax)
.L207:
	movq	-72(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, 64(%rax)
	movq	%rax, -136(%rbp)
	leaq	-136(%rbp), %rsi
	movq	%rdx, 16(%rax)
	movq	-48(%rbp), %rdx
	movups	%xmm0, 48(%rax)
	movq	%rdx, 40(%rax)
	call	*%r14
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L208
	movq	(%rdi), %rax
	call	*24(%rax)
.L208:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L201
	call	_ZdlPv@PLT
.L201:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L217
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movq	32(%rsi), %rdx
	xorl	%ecx, %ecx
	leaq	-64(%rbp), %r13
	movdqu	16(%rsi), %xmm1
	movq	8(%rsi), %rdi
	movw	%cx, 16(%rsi)
	leaq	-112(%rbp), %rbx
	movq	$0, 8(%rsi)
	movq	%rdx, -96(%rbp)
	movq	%r13, -80(%rbp)
	movaps	%xmm1, -112(%rbp)
.L203:
	movdqa	-112(%rbp), %xmm2
	movaps	%xmm2, -64(%rbp)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L216:
	movdqa	-64(%rbp), %xmm3
	movups	%xmm3, 24(%rax)
	jmp	.L207
.L217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4506:
	.size	_ZN12v8_inspector8protocol6Schema8Frontend23sendRawJSONNotificationENS_8String16E, .-_ZN12v8_inspector8protocol6Schema8Frontend23sendRawJSONNotificationENS_8String16E
	.section	.text._ZN12v8_inspector8protocol6Schema8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol6Schema8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE
	.type	_ZN12v8_inspector8protocol6Schema8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE, @function
_ZN12v8_inspector8protocol6Schema8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE:
.LFB4507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	(%rsi), %rbx
	movl	$72, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %r15
	movq	8(%rsi), %r12
	movq	0(%r13), %rax
	movq	24(%rax), %r14
	movq	$0, 16(%rsi)
	movups	%xmm0, (%rsi)
	call	_Znwm@PLT
	movq	%r12, %xmm1
	movq	%rbx, %xmm0
	movq	%r13, %rdi
	leaq	24(%rax), %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%r15, 64(%rax)
	leaq	-64(%rbp), %rsi
	movq	%rdx, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol23InternalRawNotificationE(%rip), %rcx
	xorl	%edx, %edx
	movq	%rcx, (%rax)
	movq	$0, 16(%rax)
	movw	%dx, 24(%rax)
	movq	$0, 40(%rax)
	movq	%rax, -64(%rbp)
	movups	%xmm0, 48(%rax)
	call	*%r14
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L218
	movq	(%rdi), %rax
	call	*24(%rax)
.L218:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L225
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L225:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4507:
	.size	_ZN12v8_inspector8protocol6Schema8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE, .-_ZN12v8_inspector8protocol6Schema8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::reserve"
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm:
.LFB5141:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	ja	.L242
	movq	(%rdi), %r12
	movq	16(%rdi), %rax
	movq	%rdi, %r15
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L243
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	movq	8(%rdi), %rbx
	xorl	%r13d, %r13d
	movq	%rbx, %rax
	subq	%r12, %rax
	movq	%rax, -56(%rbp)
	leaq	0(,%rsi,8), %rax
	movq	%rax, -64(%rbp)
	testq	%rsi, %rsi
	je	.L229
	movq	%rax, %rdi
	call	_Znwm@PLT
	movq	8(%r15), %rbx
	movq	(%r15), %r12
	movq	%rax, %r13
.L229:
	cmpq	%r12, %rbx
	je	.L230
	movq	%r13, %r14
	.p2align 4,,10
	.p2align 3
.L234:
	movq	(%r12), %rcx
	movq	$0, (%r12)
	movq	%rcx, (%r14)
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L231
	movq	(%rdi), %rcx
	addq	$8, %r12
	addq	$8, %r14
	call	*24(%rcx)
	cmpq	%rbx, %r12
	jne	.L234
.L232:
	movq	(%r15), %r12
.L230:
	testq	%r12, %r12
	je	.L235
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L235:
	movq	-56(%rbp), %r14
	movq	%r13, (%r15)
	addq	%r13, %r14
	addq	-64(%rbp), %r13
	movq	%r14, 8(%r15)
	movq	%r13, 16(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	addq	$8, %r12
	addq	$8, %r14
	cmpq	%r12, %rbx
	jne	.L234
	jmp	.L232
.L242:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5141:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.str1.1,"aMS",@progbits,1
.LC6:
	.string	"basic_string::_M_create"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag:
.LFB6674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	jne	.L245
	testq	%rdx, %rdx
	jne	.L261
.L245:
	subq	%r13, %rbx
	movq	%rbx, %r14
	sarq	%r14
	cmpq	$15, %rbx
	ja	.L246
	movq	(%r12), %rdi
.L247:
	cmpq	$2, %rbx
	je	.L262
	testq	%rbx, %rbx
	je	.L250
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L250:
	xorl	%eax, %eax
	movq	%r14, 8(%r12)
	movw	%ax, (%rdi,%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L246:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L263
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r14, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L247
.L261:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L263:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6674:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.section	.text._ZNK12v8_inspector8protocol6Schema6Domain7toValueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol6Schema6Domain7toValueEv
	.type	_ZNK12v8_inspector8protocol6Schema6Domain7toValueEv, @function
_ZNK12v8_inspector8protocol6Schema6Domain7toValueEv:
.LFB4495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$96, %edi
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movq	-120(%rbp), %r8
	movl	$56, %edi
	movq	%r8, (%r12)
	call	_Znwm@PLT
	movq	16(%r15), %rsi
	movq	%rax, %rbx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movq	24(%r15), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	48(%r15), %rax
	movq	%r13, %rdi
	movq	%rbx, -104(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rax, 48(%rbx)
	leaq	-80(%rbp), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-120(%rbp), %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L265
	call	_ZdlPv@PLT
.L265:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L266
	movq	(%rdi), %rax
	call	*24(%rax)
.L266:
	movq	(%r12), %r8
	movl	$56, %edi
	movq	%r8, -128(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN12v8_inspector8protocol11StringValueE(%rip), %rcx
	movq	56(%r15), %rsi
	leaq	32(%rax), %rdx
	movq	%rcx, (%rax)
	leaq	16(%rax), %rdi
	movq	%rdx, 16(%rax)
	movq	64(%r15), %rdx
	movl	$4, 8(%rax)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rax, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-120(%rbp), %rax
	movq	88(%r15), %rdx
	movq	%r13, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rdx, 48(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-128(%rbp), %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L267
	call	_ZdlPv@PLT
.L267:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L264
	movq	(%rdi), %rax
	call	*24(%rax)
.L264:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L277
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L277:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4495:
	.size	_ZNK12v8_inspector8protocol6Schema6Domain7toValueEv, .-_ZNK12v8_inspector8protocol6Schema6Domain7toValueEv
	.section	.text._ZNK12v8_inspector8protocol6Schema6Domain5cloneEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol6Schema6Domain5cloneEv
	.type	_ZNK12v8_inspector8protocol6Schema6Domain5cloneEv, @function
_ZNK12v8_inspector8protocol6Schema6Domain5cloneEv:
.LFB4496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK12v8_inspector8protocol6Schema6Domain7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN12v8_inspector8protocol6Schema6Domain9fromValueEPNS0_5ValueEPNS0_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L279
	movq	(%rdi), %rax
	call	*24(%rax)
.L279:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L285
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L285:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4496:
	.size	_ZNK12v8_inspector8protocol6Schema6Domain5cloneEv, .-_ZNK12v8_inspector8protocol6Schema6Domain5cloneEv
	.section	.text._ZNK12v8_inspector8protocol6Schema6Domain12toJSONStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol6Schema6Domain12toJSONStringEv
	.type	_ZNK12v8_inspector8protocol6Schema6Domain12toJSONStringEv, @function
_ZNK12v8_inspector8protocol6Schema6Domain12toJSONStringEv:
.LFB4497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-88(%rbp), %r13
	leaq	-80(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol6Schema6Domain7toValueEv
	movq	-88(%rbp), %rsi
	movq	%r14, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L287
	movq	(%rdi), %rax
	call	*24(%rax)
.L287:
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector16StringBufferImpl5adoptERNS_8String16E@PLT
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	%rax, (%r12)
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L286
	call	_ZdlPv@PLT
.L286:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L294
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L294:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4497:
	.size	_ZNK12v8_inspector8protocol6Schema6Domain12toJSONStringEv, .-_ZNK12v8_inspector8protocol6Schema6Domain12toJSONStringEv
	.set	.LTHUNK2,_ZNK12v8_inspector8protocol6Schema6Domain12toJSONStringEv
	.section	.text._ZThn8_NK12v8_inspector8protocol6Schema6Domain12toJSONStringEv,"ax",@progbits
	.p2align 4
	.globl	_ZThn8_NK12v8_inspector8protocol6Schema6Domain12toJSONStringEv
	.type	_ZThn8_NK12v8_inspector8protocol6Schema6Domain12toJSONStringEv, @function
_ZThn8_NK12v8_inspector8protocol6Schema6Domain12toJSONStringEv:
.LFB7242:
	.cfi_startproc
	endbr64
	subq	$8, %rsi
	jmp	.LTHUNK2
	.cfi_endproc
.LFE7242:
	.size	_ZThn8_NK12v8_inspector8protocol6Schema6Domain12toJSONStringEv, .-_ZThn8_NK12v8_inspector8protocol6Schema6Domain12toJSONStringEv
	.section	.text._ZNK12v8_inspector8protocol6Schema6Domain11writeBinaryEPSt6vectorIhSaIhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector8protocol6Schema6Domain11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZNK12v8_inspector8protocol6Schema6Domain11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZNK12v8_inspector8protocol6Schema6Domain11writeBinaryEPSt6vectorIhSaIhEE:
.LFB4500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movq	%r8, %rsi
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol6Schema6Domain7toValueEv
	movq	-32(%rbp), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L295
	movq	(%rdi), %rax
	call	*24(%rax)
.L295:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L302
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L302:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4500:
	.size	_ZNK12v8_inspector8protocol6Schema6Domain11writeBinaryEPSt6vectorIhSaIhEE, .-_ZNK12v8_inspector8protocol6Schema6Domain11writeBinaryEPSt6vectorIhSaIhEE
	.section	.text._ZN12v8_inspector8protocol6Schema6Domain17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol6Schema6Domain17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol6Schema6Domain17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol6Schema6Domain17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol6Schema6Domain17serializeToBinaryEv:
.LFB4450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol6Schema6Domain7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L303
	movq	(%rdi), %rax
	call	*24(%rax)
.L303:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L310
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L310:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4450:
	.size	_ZN12v8_inspector8protocol6Schema6Domain17serializeToBinaryEv, .-_ZN12v8_inspector8protocol6Schema6Domain17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol6Schema6Domain15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol6Schema6Domain15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol6Schema6Domain15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol6Schema6Domain15serializeToJSONEv, @function
_ZN12v8_inspector8protocol6Schema6Domain15serializeToJSONEv:
.LFB4449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol6Schema6Domain7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L311
	movq	(%rdi), %rax
	call	*24(%rax)
.L311:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L318
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L318:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4449:
	.size	_ZN12v8_inspector8protocol6Schema6Domain15serializeToJSONEv, .-_ZN12v8_inspector8protocol6Schema6Domain15serializeToJSONEv
	.section	.rodata._ZN12v8_inspector8protocol6Schema14DispatcherImpl10getDomainsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"domains"
	.section	.text._ZN12v8_inspector8protocol6Schema14DispatcherImpl10getDomainsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol6Schema14DispatcherImpl10getDomainsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.type	_ZN12v8_inspector8protocol6Schema14DispatcherImpl10getDomainsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, @function
_ZN12v8_inspector8protocol6Schema14DispatcherImpl10getDomainsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE:
.LFB4561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-184(%rbp), %rdi
	subq	$184, %rsp
	movl	%esi, -212(%rbp)
	movq	%rbx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -192(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	leaq	-192(%rbp), %rdx
	movq	%r14, %rdi
	movq	(%rsi), %rax
	call	*16(%rax)
	cmpl	$2, -112(%rbp)
	je	.L367
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L368
.L322:
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L369
.L342:
	movl	-212(%rbp), %esi
	leaq	-168(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, -168(%rbp)
	call	_ZN12v8_inspector8protocol14DispatcherBase12sendResponseEiRKNS0_16DispatchResponseESt10unique_ptrINS0_15DictionaryValueESt14default_deleteIS6_EE@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L321
	movq	(%rdi), %rax
	call	*24(%rax)
.L321:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L332
	call	_ZdlPv@PLT
.L332:
	movq	-184(%rbp), %r12
	testq	%r12, %r12
	je	.L333
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L333:
	movq	-192(%rbp), %r14
	testq	%r14, %r14
	je	.L319
	movq	8(%r14), %rbx
	movq	(%r14), %r13
	cmpq	%r13, %rbx
	je	.L335
	leaq	80+_ZTVN12v8_inspector8protocol6Schema6DomainE(%rip), %rax
	leaq	_ZN12v8_inspector8protocol6Schema6DomainD0Ev(%rip), %r15
	leaq	-64(%rax), %rdx
	movq	%rax, %xmm2
	movq	%rdx, %xmm1
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -208(%rbp)
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L371:
	movdqa	-208(%rbp), %xmm0
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	movups	%xmm0, (%r12)
	cmpq	%rax, %rdi
	je	.L338
	call	_ZdlPv@PLT
.L338:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L339
	call	_ZdlPv@PLT
.L339:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L336:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L370
.L340:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L336
	movq	(%r12), %rax
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	je	.L371
	addq	$8, %r13
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r13, %rbx
	jne	.L340
	.p2align 4,,10
	.p2align 3
.L370:
	movq	(%r14), %r13
.L335:
	testq	%r13, %r13
	je	.L341
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L341:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L319:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L372
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L368:
	movl	$40, %edi
	movq	-192(%rbp), %r15
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8protocol9ListValueC1Ev@PLT
	movq	8(%r15), %rsi
	subq	(%r15), %rsi
	leaq	16(%rbx), %rdi
	sarq	$3, %rsi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector8protocol5ValueESt14default_deleteIS3_EESaIS6_EE7reserveEm
	movq	(%r15), %rax
	movq	8(%r15), %rcx
	leaq	-168(%rbp), %r15
	movq	%rax, %r13
	leaq	-176(%rbp), %rax
	movq	%rcx, -208(%rbp)
	movq	%rax, -224(%rbp)
	cmpq	%rcx, %r13
	je	.L328
	.p2align 4,,10
	.p2align 3
.L329:
	movq	0(%r13), %rsi
	movq	-224(%rbp), %rdi
	call	_ZNK12v8_inspector8protocol6Schema6Domain7toValueEv
	movq	-176(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rdx, -168(%rbp)
	call	_ZN12v8_inspector8protocol9ListValue9pushValueESt10unique_ptrINS0_5ValueESt14default_deleteIS3_EE@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L326
	movq	(%rdi), %rdx
	addq	$8, %r13
	call	*24(%rdx)
	cmpq	%r13, -208(%rbp)
	jne	.L329
.L328:
	movq	%rbx, -168(%rbp)
	leaq	-160(%rbp), %rbx
	leaq	.LC7(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue8setValueERKNS_8String16ESt10unique_ptrINS0_5ValueESt14default_deleteIS6_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L325
	call	_ZdlPv@PLT
.L325:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L322
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L342
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L326:
	addq	$8, %r13
	cmpq	%r13, -208(%rbp)
	jne	.L329
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L367:
	movq	8(%rbx), %rdi
	movl	-212(%rbp), %esi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L321
.L372:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4561:
	.size	_ZN12v8_inspector8protocol6Schema14DispatcherImpl10getDomainsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE, .-_ZN12v8_inspector8protocol6Schema14DispatcherImpl10getDomainsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE
	.section	.text._ZThn8_NK12v8_inspector8protocol6Schema6Domain11writeBinaryEPSt6vectorIhSaIhEE,"ax",@progbits
	.p2align 4
	.globl	_ZThn8_NK12v8_inspector8protocol6Schema6Domain11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZThn8_NK12v8_inspector8protocol6Schema6Domain11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZThn8_NK12v8_inspector8protocol6Schema6Domain11writeBinaryEPSt6vectorIhSaIhEE:
.LFB7235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	-8(%rdi), %rsi
	leaq	-32(%rbp), %r8
	movq	%r8, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol6Schema6Domain7toValueEv
	movq	-32(%rbp), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L373
	movq	(%rdi), %rax
	call	*24(%rax)
.L373:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L380
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L380:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7235:
	.size	_ZThn8_NK12v8_inspector8protocol6Schema6Domain11writeBinaryEPSt6vectorIhSaIhEE, .-_ZThn8_NK12v8_inspector8protocol6Schema6Domain11writeBinaryEPSt6vectorIhSaIhEE
	.section	.text._ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol6Schema14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m,"axG",@progbits,_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol6Schema14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol6Schema14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	.type	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol6Schema14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m, @function
_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol6Schema14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m:
.LFB6765:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax,%rsi,8), %r10
	testq	%r10, %r10
	je	.L398
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%r10), %rdx
	movl	$2147483648, %ebx
	movq	64(%rdx), %r8
	cmpq	%r8, %rcx
	je	.L401
	.p2align 4,,10
	.p2align 3
.L383:
	movq	(%rdx), %r9
	testq	%r9, %r9
	je	.L388
	movq	64(%r9), %r8
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	8(%rdi)
	cmpq	%rdx, %rsi
	jne	.L388
	movq	%r9, %rdx
	cmpq	%r8, %rcx
	jne	.L383
.L401:
	movq	8(%r11), %r8
	movq	16(%rdx), %r15
	movq	8(%rdx), %r13
	movq	(%r11), %r14
	cmpq	%r15, %r8
	movq	%r15, %r9
	cmovbe	%r8, %r9
	testq	%r9, %r9
	je	.L384
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L385:
	movzwl	0(%r13,%rax,2), %r12d
	cmpw	%r12w, (%r14,%rax,2)
	jne	.L383
	addq	$1, %rax
	cmpq	%rax, %r9
	jne	.L385
.L384:
	subq	%r15, %r8
	cmpq	%rbx, %r8
	jge	.L383
	movabsq	$-2147483649, %rax
	cmpq	%rax, %r8
	jle	.L383
	testl	%r8d, %r8d
	jne	.L383
	popq	%rbx
	movq	%r10, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	.cfi_restore_state
	xorl	%r10d, %r10d
	popq	%rbx
	popq	%r12
	movq	%r10, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	%r10, %rax
	ret
	.cfi_endproc
.LFE6765:
	.size	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol6Schema14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m, .-_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol6Schema14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	.section	.text.unlikely._ZN12v8_inspector8protocol6Schema14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE,"ax",@progbits
	.align 2
.LCOLDB8:
	.section	.text._ZN12v8_inspector8protocol6Schema14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE,"ax",@progbits
.LHOTB8:
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol6Schema14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.type	_ZN12v8_inspector8protocol6Schema14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE, @function
_ZN12v8_inspector8protocol6Schema14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE:
.LFB4559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$72, %rdi
	subq	$104, %rsp
	movl	%esi, -132(%rbp)
	movq	32(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	jne	.L403
	movq	(%rdx), %rax
	movq	8(%rdx), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L406
	.p2align 4,,10
	.p2align 3
.L405:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%r14)
	cmpq	%rax, %rsi
	jne	.L405
	testq	%rcx, %rcx
	je	.L406
.L403:
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	80(%rbx)
	movq	%rdx, %rsi
	movq	%r14, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol6Schema14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	testq	%rax, %rax
	je	.L407
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	movq	%rdx, -144(%rbp)
	je	.L407
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	movq	-144(%rbp), %rdx
	movq	48(%rdx), %rax
	addq	56(%rdx), %rbx
	movq	%rbx, %rdi
	testb	$1, %al
	jne	.L430
.L408:
	movq	(%r15), %rdx
	movl	-132(%rbp), %esi
	movq	%r12, %r9
	leaq	-120(%rbp), %r8
	movq	$0, (%r15)
	movq	%r13, %rcx
	movq	%rdx, -120(%rbp)
	movq	%r14, %rdx
	call	*%rax
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L409
	movq	(%rdi), %rax
	call	*24(%rax)
.L409:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L431
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	movq	(%rbx), %rdx
	movq	-1(%rdx,%rax), %rax
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L406:
	movq	$1, 32(%r14)
	movl	$1, %ecx
	jmp	.L403
.L431:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN12v8_inspector8protocol6Schema14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.cfi_startproc
	.type	_ZN12v8_inspector8protocol6Schema14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold, @function
_ZN12v8_inspector8protocol6Schema14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold:
.LFSB4559:
.L407:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	-112(%rbp), %rdi
	call	_ZN12v8_inspector8protocol12ErrorSupportC1Ev@PLT
	movq	48, %rax
	ud2
	.cfi_endproc
.LFE4559:
	.section	.text._ZN12v8_inspector8protocol6Schema14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.size	_ZN12v8_inspector8protocol6Schema14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE, .-_ZN12v8_inspector8protocol6Schema14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.section	.text.unlikely._ZN12v8_inspector8protocol6Schema14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.size	_ZN12v8_inspector8protocol6Schema14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold, .-_ZN12v8_inspector8protocol6Schema14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE.cold
.LCOLDE8:
	.section	.text._ZN12v8_inspector8protocol6Schema14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
.LHOTE8:
	.section	.rodata._ZN12v8_inspector8protocol6Schema10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"Schema.getDomains"
.LC11:
	.string	"Schema"
	.section	.text._ZN12v8_inspector8protocol6Schema10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol6Schema10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE
	.type	_ZN12v8_inspector8protocol6Schema10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE, @function
_ZN12v8_inspector8protocol6Schema10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE:
.LFB4589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movl	$192, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN12v8_inspector8protocol14DispatcherBaseC2EPNS0_15FrontendChannelE@PLT
	leaq	16+_ZTVN12v8_inspector8protocol6Schema14DispatcherImplE(%rip), %rax
	leaq	72(%r15), %rbx
	movss	.LC9(%rip), %xmm0
	movq	%rax, (%r15)
	leaq	120(%r15), %rax
	leaq	.LC10(%rip), %rsi
	movq	%r13, 184(%r15)
	leaq	-96(%rbp), %r13
	movq	%rax, -128(%rbp)
	movq	%r13, %rdi
	movq	%rax, 72(%r15)
	leaq	176(%r15), %rax
	movq	$1, 80(%r15)
	movq	$0, 88(%r15)
	movq	$0, 96(%r15)
	movq	$0, 112(%r15)
	movq	$0, 120(%r15)
	movq	%rax, 128(%r15)
	movq	$1, 136(%r15)
	movq	$0, 144(%r15)
	movq	$0, 152(%r15)
	movq	$0, 168(%r15)
	movq	$0, 176(%r15)
	movss	%xmm0, 104(%r15)
	movss	%xmm0, 160(%r15)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-64(%rbp), %r14
	testq	%r14, %r14
	jne	.L433
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %rdx
	leaq	(%rax,%rdx,2), %rcx
	cmpq	%rcx, %rax
	je	.L436
	.p2align 4,,10
	.p2align 3
.L435:
	movq	%r14, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%r14, %rdx
	movsbq	-2(%rax), %r14
	addq	%rdx, %r14
	movq	%r14, -64(%rbp)
	cmpq	%rax, %rcx
	jne	.L435
	testq	%r14, %r14
	je	.L436
.L433:
	xorl	%edx, %edx
	movq	%r14, %rax
	movq	%r14, %rcx
	movq	%rbx, %rdi
	divq	80(%r15)
	movq	%rdx, -120(%rbp)
	movq	-120(%rbp), %rsi
	movq	%r13, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol6Schema14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	testq	%rax, %rax
	je	.L437
	movq	(%rax), %rax
	leaq	-80(%rbp), %rbx
	leaq	48(%rax), %r10
	testq	%rax, %rax
	je	.L437
.L454:
	leaq	_ZN12v8_inspector8protocol6Schema14DispatcherImpl10getDomainsEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EEPNS0_12ErrorSupportE(%rip), %rax
	movq	$0, 8(%r10)
	movq	%rax, (%r10)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L455
	call	_ZdlPv@PLT
.L455:
	leaq	128(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINS_8String16ES3_St4hashIS3_ESt8equal_toIS3_ESaISt4pairIKS3_S3_EEE@PLT
	leaq	.LC11(%rip), %rsi
	movq	%r13, %rdi
	movq	%r15, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	leaq	-104(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol14UberDispatcher15registerBackendERKNS_8String16ESt10unique_ptrINS0_14DispatcherBaseESt14default_deleteIS6_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L456
	call	_ZdlPv@PLT
.L456:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L432
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol6Schema14DispatcherImplD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L458
	movq	144(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol6Schema14DispatcherImplE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L459
	.p2align 4,,10
	.p2align 3
.L464:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L460
	call	_ZdlPv@PLT
.L460:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L466
	.p2align 4,,10
	.p2align 3
.L470:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L467
	call	_ZdlPv@PLT
.L467:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol14DispatcherBaseD2Ev@PLT
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L432:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L517
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L464
.L465:
	movq	%rbx, %r13
.L459:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L462
	call	_ZdlPv@PLT
.L462:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L518
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L465
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L519:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L470
.L471:
	movq	%rbx, %r13
.L466:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L519
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L471
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L436:
	movq	$1, -64(%rbp)
	movl	$1, %r14d
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L437:
	movl	$72, %edi
	leaq	-80(%rbp), %rbx
	call	_Znwm@PLT
	movq	$0, (%rax)
	movq	%rax, %r10
	leaq	24(%rax), %rax
	movq	%rax, 8(%r10)
	movq	-96(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L520
	movq	%rax, 8(%r10)
	movq	-80(%rbp), %rax
	movq	%rax, 24(%r10)
.L440:
	movq	-88(%rbp), %rax
	movq	96(%r15), %rdx
	leaq	104(%r15), %rdi
	movq	%rbx, -96(%rbp)
	movq	80(%r15), %rsi
	movq	$0, 48(%r10)
	movl	$1, %ecx
	movq	%rax, 16(%r10)
	xorl	%eax, %eax
	movw	%ax, -80(%rbp)
	movq	-64(%rbp), %rax
	movq	$0, 56(%r10)
	movq	%rax, 40(%r10)
	movq	%r10, -136(%rbp)
	movq	$0, -88(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	-136(%rbp), %r10
	testb	%al, %al
	movq	%rdx, %r8
	jne	.L441
	movq	72(%r15), %r11
.L442:
	movq	%r14, 64(%r10)
	movq	-120(%rbp), %r14
	salq	$3, %r14
	leaq	(%r11,%r14), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L451
	movq	(%rdx), %rdx
	movq	%rdx, (%r10)
	movq	(%rax), %rax
	movq	%r10, (%rax)
.L452:
	addq	$1, 96(%r15)
	addq	$48, %r10
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L441:
	cmpq	$1, %rdx
	je	.L521
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L522
	leaq	0(,%rdx,8), %rdx
	movq	%r10, -144(%rbp)
	movq	%rdx, %rdi
	movq	%r8, -136(%rbp)
	movq	%rdx, -120(%rbp)
	call	_Znwm@PLT
	movq	-120(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-144(%rbp), %r10
	movq	-136(%rbp), %r8
	movq	%rax, %r11
.L444:
	movq	88(%r15), %rsi
	movq	$0, 88(%r15)
	testq	%rsi, %rsi
	je	.L446
	leaq	88(%r15), %rax
	xorl	%r9d, %r9d
	movq	%rax, -120(%rbp)
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L448:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L449:
	testq	%rsi, %rsi
	je	.L446
.L447:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	64(%rcx), %rax
	divq	%r8
	leaq	(%r11,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L448
	movq	88(%r15), %rax
	movq	%rax, (%rcx)
	movq	-120(%rbp), %rax
	movq	%rcx, 88(%r15)
	movq	%rax, (%rdi)
	cmpq	$0, (%rcx)
	je	.L473
	movq	%rcx, (%r11,%r9,8)
	movq	%rdx, %r9
	testq	%rsi, %rsi
	jne	.L447
	.p2align 4,,10
	.p2align 3
.L446:
	movq	72(%r15), %rdi
	cmpq	%rdi, -128(%rbp)
	je	.L450
	movq	%r8, -136(%rbp)
	movq	%r11, -128(%rbp)
	movq	%r10, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
	movq	-128(%rbp), %r11
	movq	-120(%rbp), %r10
.L450:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r8, 80(%r15)
	divq	%r8
	movq	%r11, 72(%r15)
	movq	%rdx, -120(%rbp)
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L520:
	movdqa	-80(%rbp), %xmm1
	movups	%xmm1, 24(%r10)
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L451:
	movq	88(%r15), %rdx
	movq	%r10, 88(%r15)
	movq	%rdx, (%r10)
	testq	%rdx, %rdx
	je	.L453
	movq	64(%rdx), %rax
	xorl	%edx, %edx
	divq	80(%r15)
	movq	%r10, (%r11,%rdx,8)
	movq	72(%r15), %rax
	addq	%r14, %rax
.L453:
	leaq	88(%r15), %rdx
	movq	%rdx, (%rax)
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L458:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L473:
	movq	%rdx, %r9
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L521:
	movq	$0, 120(%r15)
	movq	-128(%rbp), %r11
	jmp	.L444
.L517:
	call	__stack_chk_fail@PLT
.L522:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE4589:
	.size	_ZN12v8_inspector8protocol6Schema10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE, .-_ZN12v8_inspector8protocol6Schema10Dispatcher4wireEPNS0_14UberDispatcherEPNS1_7BackendE
	.section	.text._ZN12v8_inspector8protocol6Schema14DispatcherImpl11canDispatchERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector8protocol6Schema14DispatcherImpl11canDispatchERKNS_8String16E
	.type	_ZN12v8_inspector8protocol6Schema14DispatcherImpl11canDispatchERKNS_8String16E, @function
_ZN12v8_inspector8protocol6Schema14DispatcherImpl11canDispatchERKNS_8String16E:
.LFB4558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32(%rsi), %rcx
	movq	%rsi, %r8
	leaq	72(%rdi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rcx, %rcx
	jne	.L524
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L527
	.p2align 4,,10
	.p2align 3
.L526:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%r8)
	cmpq	%rax, %rsi
	jne	.L526
	testq	%rcx, %rcx
	je	.L527
.L524:
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	80(%rdi)
	movq	%r9, %rdi
	movq	%rdx, %rsi
	movq	%r8, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_MNS0_8protocol6Schema14DispatcherImplEFviRS3_RKNS4_15ProtocolMessageESt10unique_ptrINS4_15DictionaryValueESt14default_deleteISC_EEPNS4_12ErrorSupportEEESaISK_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENSM_18_Mod_range_hashingENSM_20_Default_ranged_hashENSM_20_Prime_rehash_policyENSM_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmS7_m
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L523
	cmpq	$0, (%rax)
	setne	%r8b
.L523:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	.cfi_restore_state
	movq	$1, 32(%r8)
	movl	$1, %ecx
	jmp	.L524
	.cfi_endproc
.LFE4558:
	.size	_ZN12v8_inspector8protocol6Schema14DispatcherImpl11canDispatchERKNS_8String16E, .-_ZN12v8_inspector8protocol6Schema14DispatcherImpl11canDispatchERKNS_8String16E
	.weak	_ZTVN12v8_inspector8protocol23InternalRawNotificationE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol23InternalRawNotificationE,"awG",@progbits,_ZTVN12v8_inspector8protocol23InternalRawNotificationE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol23InternalRawNotificationE, @object
	.size	_ZTVN12v8_inspector8protocol23InternalRawNotificationE, 48
_ZTVN12v8_inspector8protocol23InternalRawNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol23InternalRawNotification15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol23InternalRawNotificationD1Ev
	.quad	_ZN12v8_inspector8protocol23InternalRawNotificationD0Ev
	.weak	_ZTVN12v8_inspector8protocol6Schema6DomainE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol6Schema6DomainE,"awG",@progbits,_ZTVN12v8_inspector8protocol6Schema6DomainE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol6Schema6DomainE, @object
	.size	_ZTVN12v8_inspector8protocol6Schema6DomainE, 112
_ZTVN12v8_inspector8protocol6Schema6DomainE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol6Schema6Domain15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol6Schema6Domain17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol6Schema6DomainD1Ev
	.quad	_ZN12v8_inspector8protocol6Schema6DomainD0Ev
	.quad	_ZNK12v8_inspector8protocol6Schema6Domain12toJSONStringEv
	.quad	_ZNK12v8_inspector8protocol6Schema6Domain11writeBinaryEPSt6vectorIhSaIhEE
	.quad	-8
	.quad	0
	.quad	_ZThn8_NK12v8_inspector8protocol6Schema6Domain12toJSONStringEv
	.quad	_ZThn8_NK12v8_inspector8protocol6Schema6Domain11writeBinaryEPSt6vectorIhSaIhEE
	.quad	_ZThn8_N12v8_inspector8protocol6Schema6DomainD1Ev
	.quad	_ZThn8_N12v8_inspector8protocol6Schema6DomainD0Ev
	.weak	_ZTVN12v8_inspector8protocol6Schema14DispatcherImplE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol6Schema14DispatcherImplE,"awG",@progbits,_ZTVN12v8_inspector8protocol6Schema14DispatcherImplE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol6Schema14DispatcherImplE, @object
	.size	_ZTVN12v8_inspector8protocol6Schema14DispatcherImplE, 48
_ZTVN12v8_inspector8protocol6Schema14DispatcherImplE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol6Schema14DispatcherImplD1Ev
	.quad	_ZN12v8_inspector8protocol6Schema14DispatcherImplD0Ev
	.quad	_ZN12v8_inspector8protocol6Schema14DispatcherImpl11canDispatchERKNS_8String16E
	.quad	_ZN12v8_inspector8protocol6Schema14DispatcherImpl8dispatchEiRKNS_8String16ERKNS0_15ProtocolMessageESt10unique_ptrINS0_15DictionaryValueESt14default_deleteISA_EE
	.globl	_ZN12v8_inspector8protocol6Schema8Metainfo7versionE
	.section	.rodata._ZN12v8_inspector8protocol6Schema8Metainfo7versionE,"a"
	.type	_ZN12v8_inspector8protocol6Schema8Metainfo7versionE, @object
	.size	_ZN12v8_inspector8protocol6Schema8Metainfo7versionE, 4
_ZN12v8_inspector8protocol6Schema8Metainfo7versionE:
	.string	"1.3"
	.globl	_ZN12v8_inspector8protocol6Schema8Metainfo13commandPrefixE
	.section	.rodata._ZN12v8_inspector8protocol6Schema8Metainfo13commandPrefixE,"a"
	.align 8
	.type	_ZN12v8_inspector8protocol6Schema8Metainfo13commandPrefixE, @object
	.size	_ZN12v8_inspector8protocol6Schema8Metainfo13commandPrefixE, 8
_ZN12v8_inspector8protocol6Schema8Metainfo13commandPrefixE:
	.string	"Schema."
	.globl	_ZN12v8_inspector8protocol6Schema8Metainfo10domainNameE
	.section	.rodata._ZN12v8_inspector8protocol6Schema8Metainfo10domainNameE,"a"
	.type	_ZN12v8_inspector8protocol6Schema8Metainfo10domainNameE, @object
	.size	_ZN12v8_inspector8protocol6Schema8Metainfo10domainNameE, 7
_ZN12v8_inspector8protocol6Schema8Metainfo10domainNameE:
	.string	"Schema"
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC9:
	.long	1065353216
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
